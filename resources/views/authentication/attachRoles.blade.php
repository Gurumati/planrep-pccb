<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 1/4/17
 * Time: 7:13 PM
 */
?>
@extends('layouts.master')
@section('page_header')
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="/auth/users">
                    <i class="icon-arrow-left52 position-left"></i>
                    <span class="text-semibold">{{ $page_title }}</span>
                </a></h4>
        </div>
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <?php
            if(isset($breadcrumbs)){
            $count = count($breadcrumbs);
            $i = 1;
            foreach ($breadcrumbs as $key=>$value){
            $title = $key;
            $url = $value;
            if($i == $count){
            ?>
            <li class="active"><?php echo $title; ?></li>
            <?php
            } else {
            ?>
            <li><a href="<?php echo $url; ?>"><i class="fa fa-gear position-left"></i> <?php echo $title; ?></a></li>
            <?php
            }
            $i++;
            }
            }
            ?>
        </ul>
    </div>
@stop
@section('content')
    <!-- Basic layout-->
    <div class="row">
        <div class="col-md-12">
            <form action="/attachroles" method="POST">
                {{ csrf_field() }}
                <input name="email" type="hidden" value={{$email}}>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h5 class="panel-title">Attach Roles</h5>
                    </div>

                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                                <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span
                                            class="sr-only">Close</span></button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            @foreach($roles as $role)
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label><?= $role->name ?>:</label>
                                    <input tabindex="1" type="checkbox" name="role[]" id="{{$role->name}}" value="{{$role->name}}">
                                </div>

                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="panel-footer">

                        <div class="col-md-12">
                            <div class="text-right">
                                <a href="/auth/users" class="btn btn-warning pull-left">Cancel <i class="fa fa-backward"></i></a>
                                <button type="submit" class="btn btn-success">
                                    Attach <i class="fa fa-save position-right"></i></button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /basic layout -->
@stop
