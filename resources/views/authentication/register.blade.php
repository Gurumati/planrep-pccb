<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 1/4/17
 * Time: 7:13 PM
 */
?>
@extends('layouts.master')
@section('page_header')
    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="/auth/users">
                    <i class="icon-arrow-left52 position-left"></i>
                    <span class="text-semibold">{{ $page_title }}</span>
                </a></h4>
        </div>
    </div>
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <?php
            if(isset($breadcrumbs)){
            $count = count($breadcrumbs);
            $i = 1;
            foreach ($breadcrumbs as $key=>$value){
            $title = $key;
            $url = $value;
            if($i == $count){
            ?>
            <li class="active"><?php echo $title; ?></li>
            <?php
            } else {
            ?>
            <li><a href="<?php echo $url; ?>"><i class="fa fa-gear position-left"></i> <?php echo $title; ?></a></li>
            <?php
            }
            $i++;
            }
            }
            ?>
        </ul>
    </div>
@stop
@section('content')
    <!-- Basic layout-->
    <div class="row">
        <div class="col-md-12">
            <form action="/register" method="POST">
                {{ csrf_field() }}
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h5 class="panel-title">Register User</h5>
                    </div>

                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                                <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span
                                            class="sr-only">Close</span></button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Email:</label>
                                    <input type="email" name="email" class="form-control" value="{!! old('email') !!}"
                                           placeholder="example@example.com">
                                </div>
                                <div class="form-group">
                                    <label>First Name:</label>
                                    <input type="text" name="first_name" value="{!! old('first_name') !!}" class="form-control"
                                           placeholder="John">
                                </div>
                                <div class="form-group">
                                    <label>Last Name:</label>
                                    <input type="text" name="last_name" value="{!! old('last_name') !!}" class="form-control"
                                           placeholder="Magufuli">
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Administration Hierarchy:</label>
                                    <select name="admin_hierarchy_id" class="select-search">
                                        <?php
                                        $admin_hierarchy_id = old('admin_hierarchy_id');
                                        foreach ($adminHierarchies as $value){
                                            $id = $value->id;
                                            $name = $value->name;
                                            if($id == $admin_hierarchy_id){
                                                echo "<option value='$id' selected='selected'>$name</option>";
                                            } else {
                                                echo "<option value='$id'>$name</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Section:</label>
                                    <select name="section_id" class="select-search">
                                        <?php
                                        $section_id = old('section_id');
                                        foreach ($sections as $value){
                                            $id = $value->id;
                                            $name = $value->name;
                                            $code = $value->code;
                                            if($id == $section_id){
                                                echo "<option value='$id' selected='selected'>$name</option>";
                                            } else {
                                                echo "<option value='$id'>$name</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">

                        <div class="col-md-12">
                            <div class="text-right">
                                <a href="/auth/users" class="btn btn-warning pull-left">Cancel <i class="fa fa-backward"></i></a>
                                <button type="submit" class="btn btn-success">
                                    Register <i class="fa fa-save position-right"></i></button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /basic layout -->
@stop
