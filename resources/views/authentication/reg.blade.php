@extends('master')
@section('page_header')
    <link rel="stylesheet"  href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <div class="page-header-content">
        <div class="page-title">
            <h4><a href="/auth/users">
                    <i class="icon-arrow-left52 position-left"></i>
                    <span class="text-semibold">{{ $page_title }}</span>
                </a></h4>
        </div>
    </div>
@stop
@section('content')
    <div class="container">
        <div class="rows">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel-body">
                    <form action="" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <input type="email" name="email" class="form-control" placeholder="example@example.com">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" name="first_name" class="form-control" placeholder="First Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" name="middle_name" class="form-control" placeholder="Middle Name">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" name="last_name" class="form-control" placeholder="Last Name">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" name="password" class="form-control" placeholder="Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" name="password_confirm" class="form-control" placeholder="Confirm Password">
                            </div>
                        </div>

                        {{--<div class="form-group">--}}
                        {{--<div class="input-group">--}}
                        {{--<span class="input-group-addon"><i class="fa fa-envelope"></i></span>--}}
                        {{--<input type="email" name="email" class="form-control" placeholder="example@example.com">--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                        {{--<div class="input-group">--}}
                        {{--<span class="input-group-addon"><i class="fa fa-envelope"></i></span>--}}
                        {{--<input type="email" name="email" class="form-control" placeholder="example@example.com">--}}
                        {{--</div>--}}
                        {{--</div>--}}


                    </form>
                </div>
            </div>
        </div>

    </div>
@stop

