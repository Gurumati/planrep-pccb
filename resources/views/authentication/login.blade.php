<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 1/9/17
 * Time: 7:26 PM
 */
?>
        <!DOCTYPE html>
<html lang="en" ng-app="master-module" class="login-html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        <?php echo (isset($title)) ? $title : 'PlanRep'; ?>
    </title>

    <!-- Global stylesheets -->
    <link href="/bower_components/font-roboto/dist/styles/roboto.min.css" rel="stylesheet" type="text/css">
    <link href="/bower_components/angular-material/angular-material.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/core.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/colors.css" rel="stylesheet" type="text/css">
    <link href="/assets/css/login-form.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/css/pr-progress.css">

    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="/assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="/assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="/assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/loaders/blockui.min.js"></script>
    <script type="text/javascript" src="/js/language_en.js"></script>
    <script type="text/javascript" src="/js/language_sw.js"></script>
    <!-- angular js -->

    <script type="text/javascript" src="/bower_components/angular/angular.min.js"></script>
    <!-- angular toa baadae -->
    <script type="text/javascript" src="/bower_components/ngToast/dist/ngToast.js"></script>
    <script type="text/javascript" src="/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular-timeago/dist/angular-timeago.min.js"></script>
    <!-- angular js -->
    <script type="text/javascript" src="/lib/angular/angular-route.min.js"></script>
    <script type="text/javascript" src="/lib/angular/angular-resource.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular-animate/angular-animate.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular-aria/angular-aria.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular-messages/angular-messages.min.js"></script>
    <script type="text/javascript" src="/bower_components/angular-material/angular-material.min.js"></script>
    <script type="text/javascript"
            src="/bower_components/angular-material-icons/angular-material-icons.min.js"></script>
    <script type="text/javascript" src="/lib/angular/angular-touch.min.js"></script>
    <script type="text/javascript" src="/lib/angular/ui-bootstrap-tpls-2.5.0.min.js"></script>
    <script type="text/javascript" src="/lib/angular/angular-ui.min.js"></script>
    <script type="text/javascript" src="/lib/translate/angular-translate.min.js"></script>
    <script type="text/javascript" src="/lib/angular.treeview-master/angular.treeview-2.js"></script>
    <script type="text/javascript" src="/lib/tree-dropdown/tree-dropdown.js"></script>
    <link rel='stylesheet' href="/lib/tree-dropdown/tree-dropdown.css"/>
    <script type="text/javascript" src="/lib/jquery/jquery.form.min.js"></script>
    <script type="text/javascript" src="/lib/underscore/underscore-min.js"></script>
    <script type="text/javascript" src="/lib/textAngular/textAngular-rangy.min.js"></script>
    <script type="text/javascript" src="/lib/textAngular/textAngular-sanitize.min.js"></script>
    <script type="text/javascript" src="/lib/textAngular/textAngular.min.js"></script>
    <script type="text/javascript" src="/lib/angular/angular-filter.min.js"></script>
    <script type="text/javascript" src="/lib/angular.select2/select.min.js"></script>
    <script type="text/javascript" src="/js/localStorageKeys.js"></script>
    <script type="text/javascript" src="/js/services.js"></script>
    <script type="text/javascript" src="/js/login-controller.js"></script>
    <script type="text/javascript" src="/js/master-module.js"></script>

    <script type="text/javascript" src="/lib/localStorage/local-storage.js"></script>
    <script type="text/javascript" src="/js/authorization-service.js"></script>
    {{--FILE UPLOAD--}}
    <script type="text/javascript" src="/lib/ng-file-upload/ng-file-upload-shim.min.js"></script>
    <!-- for no html5 browsers support -->
    <script type="text/javascript" src="/lib/ng-file-upload/ng-file-upload.min.js"></script>
    <!-- /core JS files -->
    <!-- Custom JS & CSS files -->
    @if(isset($js_scripts))
        @foreach($js_scripts as $js)
            <script type="text/javascript" src="{{ asset($js) }}"></script>
        @endforeach
    @endif
    @if(isset($css_scripts))
        @foreach($css_scripts as $css)
            <script type="text/javascript" src="{{ asset($css) }}"></script>
    @endforeach
@endif
<!-- /Custom JS & CSS files -->
    <style type="text/css">
        /**
         * hide when angular is not yet loaded and initialized
         */
        [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
            display: none !important;
        }
    </style>
</head>
<body class="new-body" ng-cloak>

    <div class="log-container">
        <ng-include src="'/pages/new-login-form.html'"></ng-include>
    </div>
</body>
</html>
