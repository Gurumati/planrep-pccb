@extends('layouts.master')
@section("app")
    ng-app="dashboard-module"
@stop
@section('content')
    <div>
        <ng-view></ng-view>
    </div>
@stop
@section("module-script")
    <link href="/lib/c3/c3.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/lib/c3/d3.min.js"></script>
    <script type="text/javascript" src="/lib/c3/c3.min.js"></script>
    <script type="text/javascript" src="/lib/c3/c3-angular.min.js"></script>
    <script type="text/javascript" src="/bower_components/fusioncharts/fusioncharts.js"></script>
    <script type="text/javascript" src="/js/shared/directives/angular-fusioncharts.min.js"></script>
    <script type="text/javascript" src="/js/dashboard/dashboard-module.js"></script>
    <script type="text/javascript" src="/js/dashboard/dashboard-service.js"></script>
    <script type="text/javascript" src="/js/dashboard/controllers/dashboard-controller.js"></script>
    <script type="text/javascript" src="/js/dashboard/controllers/access-denied-controller.js"></script>
    <script type="text/javascript" src="/lib/google_maps/angular-google-maps.min.js"></script>
    <script type="text/javascript" src="/lib/google_maps/lodash.min.js"></script>
    <script type="text/javascript" src="/bower_components/fusioncharts/themes/fusioncharts.theme.fint.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOHV8p-6QVIO55ApBWfyghNazwtKG1K6g" type="text/javascript"></script>
    <script type="text/javascript" src="/lib/google_maps/lodash.min.js"></script>
@stop
