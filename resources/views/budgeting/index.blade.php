@extends('layouts.master')
@section("app")
    ng-app="budgeting-module"
@stop
@section('content')
    <div >
        <ng-view></ng-view>
    </div>
@stop
@section('module-script')
    <script type="text/javascript" src="/js/budgeting/budgeting-module.js"></script>
    <script type="text/javascript" src="/js/budgeting/budgeting-service.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/budgeting-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/projection-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/activity-facility-fund-source-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/input-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/create-or-update-input-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/budgeting-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/pe-budget-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/ceiling-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/admin-hierarchy-ceiling-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/budget-submission-form-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/budget-submission-sub-form-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/budget-submission-line-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/ceiling-sector-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/scrutinization-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/expenditure-centre-value-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/bdc-main-group-fund-source-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/bdc-group-financial-year-value-controller.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/current-financial-year-plans.js"></script>
    <script type="text/javascript" src="/js/budgeting/controllers/scrutinization-budget-controller.js"></script>
@stop
