<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 1/4/17
 * Time: 7:13 PM
 */
?>
@extends('layouts.master')
@section("app")
    ng-app="user-module"
@stop
@section('content')
    <script type="text/javascript" src="{{ asset('/js/user/user-module.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/user/user-services.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/user/controllers/user-controller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/user/controllers/user-profile-controller.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/user/controllers/messages-controller.js') }}"></script>
    <!-- Basic layout-->
    <div >
        <ng-view></ng-view>
    </div>
@stop
