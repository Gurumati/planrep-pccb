<!DOCTYPE html>
<html lang="en" @yield('app')>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        <?php echo (isset($title)) ? $title : ''; ?>
    </title>

    <!-- Global stylesheets -->
    <link  href="{{ asset('/css/fonts.css') }}" rel="stylesheet"
          type="text/css">
    <link href="{{ asset('/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/colors.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/master-style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/angular-ui.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/lib/angular.treeview-master/css/angular.treeview.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/lib/textAngular/textAngular.css') }}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    <script type="text/javascript" src="{{ asset('/js/language_en.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/language_sw.js') }}"></script>
    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/lib/jquery/jquery-ui-1.9.2.custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/app.js') }}"></script>

    <!-- angular js -->
    <script type="text/javascript" src="{{ asset('/lib/angular/angular-165.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/lib/angular/angular-route.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/lib/angular/angular-resource.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/lib/angular/angular-animate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/lib/angular/angular-touch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/lib/angular/ui-bootstrap-tpls-2.5.0.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/lib/angular/angular-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/lib/translate/angular-translate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/lib/angular.treeview-master/angular.treeview-2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/lib/tree-dropdown/tree-dropdown.js') }}"></script>
    <link rel='stylesheet' href="{{ asset('/lib/tree-dropdown/tree-dropdown.css') }}" />
    <script type="text/javascript" src="{{ asset('/lib/jquery/jquery.form.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/lib/underscore/underscore-min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/lib/textAngular/textAngular-rangy.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/lib/textAngular/textAngular-sanitize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/lib/textAngular/textAngular.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/lib/angular/angular-filter.min.js') }}"></script>

{{--<script type="text/javascript" src="{{ asset('/lib/ui-bootstrap-tpls.min.js') }}"></script>--}}
    <!-- Angular end here -->

    <script type="text/javascript" src="{{ asset('/js/services.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/master-module.js') }}"></script>

    <!-- Angular end here -->

    <!-- Custom JS & CSS files -->
    @if(isset($js_scripts))
        @foreach($js_scripts as $js)
            <script type="text/javascript" src="{{ asset($js) }}"></script>
        @endforeach
    @endif
    @if(isset($css_scripts))
        @foreach($css_scripts as $css)
            <link href="{{ asset($css) }}" rel="stylesheet" type="text/css"/>
        @endforeach
    @endif
<!-- /Custom JS & CSS files -->
    <script>
        $(document).ready(function () {
            $('#confirm-delete').on('show.bs.modal', function (e) {
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });
        });
    </script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/tables/footable/footable.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/pages/table_responsive.js') }}"></script>
    {{--FILE UPLOAD--}}
    <script type="text/javascript" src="{{ asset('/lib/ng-file-upload/ng-file-upload-shim.min.js') }}"></script> <!-- for no html5 browsers support -->
    <script type="text/javascript" src="{{ asset('/lib/ng-file-upload/ng-file-upload.min.js') }}"></script>
    {{--<script src="https://js.pusher.com/4.0/pusher.min.js"></script>--}}
    <script>
        /*// Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;
        var pusher = new Pusher('05dcf9c9d7fd0c6956b3', {
            encrypted: true
        });
        var channel = pusher.subscribe('my-channel');
        channel.bind('my-event', function(data) {
            alert(data.message);
        });*/
    </script>
</head>

<body class="navbar-top" ng-controller="HeaderController">
<div id="loader" style="display:none;"></div>
<!-- Main navbar -->
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-header">
        <a class="navbar-brand" href="/"><img src="{{ asset('/assets/images/logo.png') }}" alt=""></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
            </li>
        </ul>
        {{--<ul nav navbar-nav>--}}
            {{--<div ng-show="online">You're online</div>--}}
            {{--<div ng-hide="online">You're offline</div>--}}
        {{--</ul>--}}

        <ul class="nav navbar-nav navbar-right">
            <li><a ng-click="changeLanguage('en')">English</a></li>
            <li><a ng-click="changeLanguage('sw')">Swahili</a></li>
            <li>
                <a href="#">
                    <i class="icon-cog3"></i>
                    <span class="visible-xs-inline-block position-right">Icon link</span>
                </a>
            </li>

            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ asset('/images/user.png') }}" alt="">
                    <span>
                        @if(Sentinel::check())
                            {{ Sentinel::getUser()->first_name }}
                        @else
                            Guest
                        @endif
                    </span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="{{ url('/user/#!/profile') }}"><i class="icon-user-plus"></i> My profile</a></li>
                    <li><a href="{{ url('/user/#!/messages') }}"><span class="badge badge-warning pull-right">58</span> <i
                                    class="icon-comment-discussion"></i> Messages</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ url('/user/#!/profile') }}"><i class="icon-cog5"></i> Account settings</a></li>
                    <li>
                        <form action="/logout" method="post" id="logout-form">
                            {{csrf_field()}}
                            <a href="#" onclick="document.getElementById('logout-form').submit()">  &nbsp; &nbsp; &nbsp;
                                <i class="fa fa-sign-out"></i>   &nbsp; &nbsp;Logout</a>
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main sidebar-fixed">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            {{--<a href="#" class="media-left">
                                <img src="{{ asset('/images/user.png') }}" class="img-circle img-sm" alt="">
                            </a>--}}
                            <div class="media-body">
                                {{--<span class="media-heading text-semibold">{{ Sentinel::getUser()->first_name }} {{ Sentinel::getUser()->last_name }}</span>--}}
                                <div class="text-size-mini text-muted" style="vertical-align: middle">
                                   {{-- <i class="icon-gear text-size-small"></i> &nbsp;--}}
                                    <table>
                                        <tr>
                                            {{--<td>--}}
                                                {{--<a><i class='fa fa-globe' style='color: rgba(255, 255, 255, 0.75);;font-size:40px' aria-hidden='true'></i></a>--}}
                                            {{--</td>--}}
                                            <td >
                                                <?php
                                                $admin_hierarchy_id = Sentinel::getUser()->admin_hierarchy_id;
                                                $section_id = Sentinel::getUser()->section_id;
                                                $adminHierarchy=\App\Models\Setup\AdminHierarchy::with('admin_hierarchy_level')->where('id',$admin_hierarchy_id)->first();
                                                $section=\App\Models\Setup\Section::find($section_id);
                                                if($adminHierarchy){
                                                    echo "<div style='margin:auto;font-size:16px;font-weight:bold;color: rgba(255, 255, 255, 0.75);'>".$adminHierarchy->name." ".$adminHierarchy->admin_hierarchy_level->name."</div>";
                                                }
                                                if($section){
                                                    echo "<div style='margin:auto;font-size:16px;font-weight:bold;color: rgba(255, 255, 255, 0.75);'>(".$section->name.")</div>";
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    </table>


                                </div>
                            </div>

                            {{--<div class="media-right media-middle">
                                <ul class="icons-list">
                                    <li>
                                        <a href="#"><i class="icon-cog3"></i></a>
                                    </li>
                                </ul>
                            </div>--}}
                        </div>
                    </div>
                </div>
                <!-- /user menu -->

<br/>
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <!-- Main -->
                            @include('includes.menu_reports')
                            <!-- /main -->
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-default">
                @yield('page_header')
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
            @yield('content')
            <!-- Footer -->
                <div class="footer text-muted">
                    @include('includes.footer')
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
<audio id="audio-alert" src="{{ asset('/audio/alert.mp3') }}" preload="auto"></audio>
<audio id="audio-fail" src="{{ asset('/audio/fail.mp3') }}" preload="auto"></audio>
</body>
</html>