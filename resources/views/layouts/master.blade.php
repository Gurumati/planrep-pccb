<!DOCTYPE html>
<html lang="en" @yield('app')>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>
    <?php echo (isset($title)) ? $title : 'PlanRep'; ?>
  </title>
  <!-- Global stylesheets -->
  <link href="/css/fonts.css" rel="stylesheet" type="text/css">
  <link href="/bower_components/angular-material/angular-material.min.css" rel="stylesheet" type="text/css">
  <link href="/bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css">
  <link href="/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  <link href="/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="/assets/css/core.css" rel="stylesheet" type="text/css">
  <link href="/assets/css/components.css" rel="stylesheet" type="text/css">
  <link href="/assets/css/colors.css" rel="stylesheet" type="text/css">
  <link href="/css/master-style.css" rel="stylesheet" type="text/css">
  <link href="/css/angular-ui.min.css" rel="stylesheet" type="text/css">
  <link href="/lib/angular.treeview-master/css/angular.treeview.css" rel="stylesheet" type="text/css">
  <link href="/lib/textAngular/textAngular.css" rel="stylesheet" type="text/css">
  <link href="/css/select2.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="/css/pr-progress.css">
  <link rel="stylesheet" href="/css/app.css">
  <link rel="stylesheet" href="/bower_components/ngToast/dist/ngToast-animations.css">
  <link rel="stylesheet" href="/bower_components/ngToast/dist/ngToast.css">
  <link rel="stylesheet" href="/bower_components/angular-ui-select/dist/select.css">
  <link rel='stylesheet' href="/lib/tree-dropdown/tree-dropdown.css" />
  <script type="text/javascript" src="/bower_components/angular/angular.min.js"></script>
  <style type="text/css">
    /**
       * hide when angular is not yet loaded and initialized
       */
    [ng\:cloak],
    [ng-cloak],
    [data-ng-cloak],
    [x-ng-cloak],
    .ng-cloak,
    .x-ng-cloak {
      display: none !important;
    }

    .nav ul a {
      white-space: normal;
      width: 300px;
    }

    .myDiv:hover {
      background-color: #EAEAEA;
      color: white;
    }


    .my-animation.ng-enter {
      animation: flipInY 1s;
    }

    .my-animation.ng-leave {
      animation: flipOutY 1s;
    }
  </style>
</head>

<body class="navbar-top" ng-controller="HeaderController">
  <toast></toast>
  <div id="loader" style="display:none;"></div>
  <div ng-show="!pageLoaded" style="position: absolute;top:40%;left: 40%;width:20%;z-index: 1000">
    <div class="text-center" style="width:100%"> PlanRep is loading, please wait....</div>
    <div class="pr-progress" style="height: 10px !important;">
      <div class="indeterminate"></div>
    </div>
  </div>
  <div ng-show="modalShown" id="myModal" ng-cloak="">
    <div class="modal-backdrop login-backdrop" style="opacity: .5"></div>
    <div class="Login-modal">
      <ng-include src="'/pages/login-form.html'"></ng-include>
    </div>
  </div>
  <div ng-show="modalForcePasswordShown" ng-cloak="">
    <div class="modal-backdrop login-backdrop" style="opacity: .5"></div>
    <div class="SomeLogin-modal">
      <ng-include src="'/pages/force-change-password.html'"></ng-include>
    </div>
  </div>
  <!-- <div ng-show="resertPassowrdModalShown" id="myModal" ng-cloak="">
      <div class="modal-backdrop" style="opacity: .5"></div>
      <div style="position:fixed;width:100%;left:30%;margin:auto;z-index: 1051">
        <ng-include src="'/pages/change-password-form.html'"></ng-include>
      </div>
  </div> -->
  <!-- Main navbar -->
  <div ng-hide="isPopupWindow" class="navbar navbar-inverse navbar-fixed-top" ng-cloak>
    <div class="navbar-header" style="text-align:center">
      <div layout="row" layout-align="space-between center">
        <h2 set-head-title class="admin-title">PLANREP</h2>
        <md-button class="sidebar-toggle zidebar-toggle" ng-click="toggleHamburger()">
          <ng-md-icon icon="dehaze" class="sidebar-close-icon" size="36"></ng-md-icon>
        </md-button>
      </div>
      <ul class="nav navbar-nav visible-xs-block">
        <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        <li><a class="sidebar-mobile-main-toggle"><ng-md-icon icon="sort" size="48"></ng-md-icon></a></li>
      </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
      <ul class="nav navbar-nav">
        <li class="hamburger" ng-if="showIt">
          <a class="sidebar-control sidebar-main-toggle hidden-xs" ng-click="showSideBar($event)">
            <ng-md-icon icon="dehaze" size="48"></ng-md-icon>
          </a>
        </li>
      </ul>
      <p class="navbar-text">
        {{--<span class="sector_header" ng-bind="sector | uppercase"></span>--}}
      </p>


      <!--a href="resources\assets\pdf\UserManual.pdf"  style="font-size: 15px; color: #ffffff; float: none; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);" download>Download User Manual</a-->


      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown language-switch">
          <a class="dropdown-toggle" data-toggle="dropdown" ng-click="changeLanguage('en')" ng-show="language==='en' || language ===undefined">
            <img src="/assets/images/flags/gb.png" class="position-left" alt="">
            English
            <span class="caret"></span>
          </a>
          <a class="dropdown-toggle" data-toggle="dropdown" ng-click="changeLanguage('en')" ng-show="language==='sw'">
            <img src="/assets/images/flags/tz.png" class="position-left" alt="">
            Swahili
            <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li><a ng-click="changeLanguage('en')" class="deutsch"><img src="/assets/images/flags/gb.png" alt=""> English</a></li>
            <li><a ng-click="changeLanguage('sw')" class="ukrainian"><img src="/assets/images/flags/tz.png" alt=""> Swahili</a></li>
          </ul>
        </li>

        <!-- <li class="active">
          <a class="dropdown-toggle" data-toggle="dropdown" type="button" class="btn btn-primary" aria-expanded="false">
            <span class="fa fa-bell" aria-hidden="true"></span> <span ng-show="unreads > 0" class="badge badge-danger">@{{ unreads }}</span>
          </a>
          <ul class="dropdown-menu dropdown-menu-right">
            <div ng-show="unreads > 0" style="background-color: #296F66; color: white; text-align: center; font-size: medium">Unread Notifications </div>
            <div ng-show="unreads == 0" style=" color: #296F66; text-align: center; font-size: medium">No new notifications </div>
            <li ng-show="unreads > 0" class="divider"></li>
            <li class="note" ng-repeat="plan_data in plan| orderBy: '- created_at' | limitTo: 5"><a href="/read_notification/@{{ plan_data.id}}">
                <div class="col-sm-1" style="float: left"><i class="@{{ plan_data.data.notification_icon }}"></i></div>
                <div class="col-sm-11"> @{{ plan_data.data.message_text }}
                  <div><small class="text-muted"><i class="fa fa-clock-o"></i><time-ago from-time='@{{plan_data.created_at}}'></time-ago>
                    </small></div>
                </div>
              </a>

            </li>
            <li class="divider"></li>
            <a href="/notifications">
              <div style="border-color:#296F66; text-align: center; font-size: medium" class="myDiv"> View More </div>
            </a>
          </ul>
        </li> -->

        <li class="dropdown dropdown-user">
          <a class="dropdown-toggle" data-toggle="dropdown">
            <!-- <img ng-src="@{{profilePictureUrl}}" alt="" class="circled" style="width: 35px; height: 35px;"> -->
            <span>
              <span ng-bind="fullName"></span> -
              <span ng-bind="roles"></span>
            </span>
            <i class="caret"></i>
          </a>
          <ul class="dropdown-menu dropdown-menu-right">
            <li><a href="/setup/#!/user-profile"><i class="icon-user-plus"></i> My profile</a></li>
            <li><a href="/user/#!/messages">
                <span class="badge badge-warning pull-right">@{{messages.length}}</span>
                <i class="icon-comment-discussion"></i> Messages</a></li>
            <li class="divider"></li>
            <li>
              <a href ng-click="logout()"> &nbsp; &nbsp; &nbsp;
                <i class="fa fa-sign-out"></i>Logout</a>
            </li>
          </ul>
        </li>

      </ul>

      <div layout-align="space-between center">
        <h1 set-head-title class="admin-title">Prevention and Combating of Corruption Bureau</h1>
      </div>
    </div>


  </div>
  <!-- /main navbar -->
  <!-- Page container -->
  <div class="page-container" ng-cloak>
    <!-- Page content -->
    <div class="page-content">
      <!-- Main sidebar -->

      <div ng-hide="isPopupWindow" class="sidebar sidebar-main sidebar-fixed">
        <div class="sidebar-content">

          <div class="divider"></div>

          <div class="header-contents">
            <div style="margin-top:2px; text-align: center;">
              <span class="media-heading text-white" ng-bind="adminHierarchy"></span>
              <span style="margin-top:-4px" class="media-heading text-semibold" ng-bind="section"></span>
            </div>
          </div>
          <!-- User menu -->
          <!-- <div class="divider"></div> -->

          <div layout="row-" class="md-toolbar-tools-bottom inset profile-container" layout-align="center">
            <div class="picture-container">
              <a href="#">
                <img src="/assets/images/pccb-logo.png" class="cirled" width="90%" alt="">
              </a>
            </div>
          </div>

          <div class="divider"></div>
          <!-- /user menu -->

          <!-- Main navigation -->
          <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
              <ul class="navigation navigation-main navigation-accordion">
                <!-- Main -->
                @include('includes.menu')
                <!-- /main -->
              </ul>
            </div>
          </div>
          <!-- /main navigation -->
        </div>
      </div>
      <!-- /main sidebar -->

      <!-- Main content -->
      <div class="content-wrapper">
        <!-- Page header -->
        <div class="page-header page-header-default">
          @yield('page_header')
        </div>
        <!-- /page header -->
        <!-- Content area -->
        <div class="content">
          @yield('content')
          <!-- Footer -->
          <div class="footer text-muted" style="width:77% !important">
            @include('includes.footer')
          </div>
          <!-- /footer -->
        </div>
        <!-- /content area -->
      </div>
      <!-- /main content -->
    </div>
    <!-- /page content -->
  </div>
  <!-- /page container -->
  <audio id="audio-alert" src="/audio/alert.mp3" preload="auto"></audio>
  <audio id="audio-fail" src="/audio/fail.mp3" preload="auto"></audio>
  <script type="text/javascript" src="/js/language_en.js"></script>
  <script type="text/javascript" src="/js/language_sw.js"></script>
  <!-- Core JS files -->
  <script type="text/javascript" src="/assets/js/core/libraries/jquery.min.js"></script>
  <script type="text/javascript" src="/bower_components/jquery-ui/jquery-ui.min.js"></script>
  <script type="text/javascript" src="/assets/js/core/libraries/bootstrap.min.js"></script>
  <script type="text/javascript" src="/assets/js/plugins/loaders/blockui.min.js"></script>
  <script type="text/javascript" src="/assets/js/core/app.js"></script>
  <!-- angular js -->
  <script type="text/javascript" src="/bower_components/ngToast/dist/ngToast.js"></script>
  <script type="text/javascript" src="/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
  <script type="text/javascript" src="/bower_components/angular-timeago/dist/angular-timeago.min.js"></script>
  <script type="text/javascript" src="/bower_components/angular-route/angular-route.min.js"></script>
  <script type="text/javascript" src="/bower_components/angular-resource/angular-resource.min.js"></script>
  <script type="text/javascript" src="/bower_components/angular-animate/angular-animate.min.js"></script>
  <script type="text/javascript" src="/bower_components/angular-aria/angular-aria.min.js"></script>
  <script type="text/javascript" src="/bower_components/angular-messages/angular-messages.min.js"></script>
  <script type="text/javascript" src="/bower_components/angular-material/angular-material.min.js"></script>
  <script type="text/javascript" src="/bower_components/angular-material-icons/angular-material-icons.min.js"></script>
  <script type="text/javascript" src="/bower_components/angular-touch/angular-touch.min.js"></script>
  <script type="text/javascript" src="/bower_components/socket-io-client/socket.io.js"></script>
  <script type="text/javascript" src="/bower_components/angular-ui-select/dist/select.js"></script>
  <script type="text/javascript" src="/lib/angular/ui-bootstrap-tpls-2.5.0.min.js"></script>
  <script type="text/javascript" src="/bower_components/angular-ui/build/angular-ui.min.js"></script>
  <script type="text/javascript" src="/bower_components/angular-translate/angular-translate.min.js"></script>
  <script type="text/javascript" src="/lib/angular.treeview-master/angular.treeview-2.js"></script>
  <script type="text/javascript" src="/lib/tree-dropdown/tree-dropdown.js"></script>
  <script type="text/javascript" src="/lib/jquery/jquery.form.min.js"></script>
  <script type="text/javascript" src="/lib/underscore/underscore-min.js"></script>
  <script type="text/javascript" src="/lib/textAngular/textAngular-rangy.min.js"></script>
  <script type="text/javascript" src="/lib/textAngular/textAngular-sanitize.min.js"></script>
  <script type="text/javascript" src="/bower_components/textAngular/dist/textAngular.min.js"></script>
  <script type="text/javascript" src="/bower_components/angular-filter/dist/angular-filter.min.js"></script>
  <script type="text/javascript" src="/js/localStorageKeys.js"></script>
  <script type="text/javascript" src="/js/services.js"></script>
  <script type="text/javascript" src="/js/login-controller.js"></script>
  <script type="text/javascript" src="/js/master-module.js"></script>
  <script type="text/javascript" src="/lib/localStorage/local-storage.js"></script>
  <script type="text/javascript" src="/js/authorization-service.js"></script>
  <script type="text/javascript" src="/js/shared/directives/draggable.js"></script>
  <script type="text/javascript" src="/assets/js/core/libraries/jquery_ui/interactions.min.js"></script>
  <script type="text/javascript" src="/assets/js/plugins/tables/footable/footable.min.js"></script>
  <script type="text/javascript" src="/assets/js/pages/table_responsive.js"></script>
  <script type="text/javascript" src="/lib/ng-file-upload/ng-file-upload-shim.min.js"></script>
  <script type="text/javascript" src="/lib/ng-file-upload/ng-file-upload.min.js"></script>
  <script type="text/javascript" src="/js/shared/directives/common-directives.js"></script>
  <script type="text/javascript" src="/js/shared/directives/main-filter.js"></script>
  <script type="text/javascript" src="/js/shared/directives/plan-chain-filter.js"></script>
  @yield('module-script')
</body>

</html>
