<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 1/4/17
 * Time: 7:13 PM
 */
?>
@extends('layouts.master')
@section("app")
    ng-app="execution-module"
@stop
@section('content')
    <div >
        <ng-view></ng-view>
    </div>
@stop
@section("module-script")
    <link href="/lib/c3/c3.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/lib/c3/d3.min.js"></script>
    <script type="text/javascript" src="/lib/c3/c3.min.js"></script>
    <script type="text/javascript" src="/lib/c3/c3-angular.min.js"></script>
    <script type="text/javascript" src="/js/execution/execution-module.js"></script>
    <script type="text/javascript" src="/js/execution/execution-services.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/execution-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/import-method-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/received-fund-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/data-source-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/received-fund-item-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/budget-export-account-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/budget-transaction-type-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/budget-export-transaction-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/budget-import-issue-type-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/budget-import-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/budget-reallocation-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/budget-mass-reallocation-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/reallocation-within-vote-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/budget-reallocation-approve-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/open-budget-reallocation-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/performance-indicator-financial-year-value-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/revenue-export-account-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/coa-segment-feedback-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/budget-accounts-controller.js"></script>
    <link href="/lib/highchart/highcharts.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/lib/highchart/highstock.js"></script>
    <script type="text/javascript" src="/lib/highchart/exporting.js"></script>
    <script type="text/javascript" src="/lib/highchart/highcharts-ng.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/activity-implementation-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/budget-export-to-financials-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/activity-implementation-plan-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/activity-implementation-history-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/approve-revenue-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/setup-segment-export-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/pre-execution-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/export-pe-budget-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/export-transaction-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/exported-transactions-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/activity-overall-achievement-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/export-reallocation-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/budget-reallocation-approval-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/activity-expenditure-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/revenue-allocation-expenditure-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/reallocation-feedback-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/opras-setup-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/revenue-export-feedback-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/actual-transaction-controller.js"></script>
    <script type="text/javascript" src="/js/execution/controllers/ceiling-export-controller.js"></script>
@stop
