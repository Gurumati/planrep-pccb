<?php
/**
 * Created by PhpStorm.
 * User: frank
 * Date: 1/4/17
 * Time: 7:13 PM
 */
?>
@extends('layouts.master')
@section("app")
    ng-app="setup-module"
@stop
@section('content')
    <div>
        <ng-view></ng-view>
    </div>
@stop
@section("module-script")
    <script type="text/javascript" src="/js/setup/setup-module.js"></script>
    <script type="text/javascript" src="/js/setup/setup-routes.js"></script>
    <script type="text/javascript" src="/js/setup/setup-service.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/financial-year-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/access-right-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/section-level-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/section-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/budget-class-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/decision-level-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/facility-type-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/demo-section-information-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/facility-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/admin-hierarchy-level-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/admin-hierarchy-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/reference-type-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/funder-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/responsible-person-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/unit-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/user-group-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/activity-category-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/gfs-code-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/procurement-type-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/account-type-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/plan-chain-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/project-funder-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/period-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/fund-source-category-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/project-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/fund-source-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/gfs-code-category-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/generic-template-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/plan-chain-type-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/role-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/user-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/users-tracking-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/audit-trails-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/add-or-remove-user-facility-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/sector-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/reference-document-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/reference-document-type-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/configuration-setting-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/target-type-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/admin-hierarchy-project-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/sector-project-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/priority-area-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/intervention-category-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/intervention-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/cas-plan-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/cas-plan-table-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/cas-plan-content-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/sector-problem-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/admin-hierarchy-gfs-code-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/section-gfs-code-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/cas-plan-table-item-value-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/lga-level-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/cas-group-types-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/cas-group-column-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/link-level-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/task-nature-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/expenditure-centre-group-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/expenditure-centre-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/bdc-main-group-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/bdc-group-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/expenditure-centre-gfs-code-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/sector-expenditure-sub-centre-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/sector-expenditure-sub-centre-group-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/gfs-code-sub-category-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/bod-list-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/bod-version-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/baseline-statistics-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/fund-type-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/ceiling-chain-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/calendar-event-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/calendar-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/bod-intervention-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/calendar-event-reminder-recipient-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/notification-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/period-group-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/report-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/cas-plan-table-select-option-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/user-profile-key-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/pe-submission-form-report-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/user-profile-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/activity-status-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/assessor-assignment-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/activity-task-nature-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/performance-indicator-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/facility-ownership-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/facility-physical-state-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/facility-star-rating-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/facility-custom-detail-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/facility-custom-detail-mapping-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/national-target-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/planning-matrix-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/generic-sector-problem-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/generic-target-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/generic-activity-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/project-data-form-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/project-data-form-content-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/project-data-form-questions-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/link-specs-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/asset-condition-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/asset-use-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/transport-facility-type-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/upload-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/budget-submission-select-options-Controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/move-activity-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/move-ceiling-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/historical-data-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/fix-activity-code-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/expenditure-category-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/project-output-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/project-type-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/fund-source-budget-class-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/account-return-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/version-type-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/version-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/bank-account-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/metadata-source-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/metadata-type-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/metadata-item-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/procurement-method-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/advertisements-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/feature-notification-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/geo-location-level-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/geo-location-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/logo-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/activity-surplus-category-controller.js"></script>

@stop
