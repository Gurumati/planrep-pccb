@extends('layouts.master')
@section("app")
    ng-app="notification-module"
@stop
@section('content')
    <div>
        <ng-view></ng-view>
    </div>
@stop
@section("module-script")
    <script type="text/javascript" src="/js/notifications/notification-module.js"></script>
    <script type="text/javascript" src="/js/notifications/notification-services.js"></script>
    <script type="text/javascript" src="/js/notifications/controllers/notification-controller.js"></script>
@stop
