<li>
    <a href="/">
        <div layout="row">
            <div class="inset">
                <ng-md-icon icon="pie_chart"></ng-md-icon>
            </div>
            <div class="inset menu-text" translate="DASHBOARD"></div>
        </div>
    </a>
</li>
<li ng-show="hasPermission('settings.manage')">
    <a href>
        <div layout="row">
            <div class="inset">
                <ng-md-icon icon="settings"></ng-md-icon>
            </div>
            <div class="inset menu-text" translate="MENU_SETTINGS"></div>
        </div>
    </a>
    <ul>
        <li ng-show="hasPermission('settings.manage.configurations')">
            <a href="/setup#!/configuration-settings">
                <span translate="CONFIGURATIONS"></span>
            </a>
        </li>
        <li ng-show="hasPermission('settings.manage.advertisement')">
            <a href="/setup#!/advertisements-management">
                <span translate="ADVERTISEMENT"></span>
            </a>
        </li>


        <li ng-show="hasPermission('settings.manage.logo')">
            <a href="/setup#!/logo-management">
                <span translate="Bureau_LOGO"></span>
            </a>
        </li>
        <li>

        <li ng-show="hasPermission('settings.manage.move.activity')">
            <a href="/setup#!/move-activity">
                <span translate="MOVE_ACTIVITIES"></span>
            </a>
        </li>
        <li ng-show="hasPermission('settings.manage.move.activity')">
            <a href="/setup#!/move-ceiling">
                <span translate="Move Ceiling"></span>
            </a>
        </li>
        <li ng-show="hasPermission('settings.manage.move.activity')">
            <a href="/setup#!/fix-activity-code">
                <span translate="FIX_ACTIVITY_CODE"></span>
            </a>
        </li>
        <li ng-show="hasPermission('settings.manage.admin_settings')">
            <a href="#">
                <span translate="ADMIN_SETTINGS"></span>
            </a>
            <ul>
                <li><a href="/setup#!/admin-hierarchy-levels">
                        <span translate="ADMIN_HIERARCHY_LEVELS"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/admin-hierarchies">
                        <span translate="ADMIN_HIERARCHIES"></span>
                    </a>
                </li>

                <li>
                    <a href="/setup#!/decision-levels">
                        <span translate="DECISION_LEVELS"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/sectors">
                        <span translate="SECTORS"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/planning-unit-levels">
                        <span translate="PLANNING_UNIT_LEVELS"></span>
                    </a>
                </li>

                <li>
                    <a href="/setup#!/planning-units">
                        <span translate="PLANNING_UNITS"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/calendar-events">
                        <span translate="CALENDAR_EVENTS"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/calendars">
                        <span translate="CALENDARS"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/calendar-event-reminder-recipients">
                        <span translate="CALENDAR_EVENT_REMINDER_RECIPIENTS"></span>
                    </a>
                </li>
                <li>
                    <a href="notifications">
                        <span translate="NOTIFICATIONS"></span>
                    </a>
                </li>
            </ul>
        </li>
        <li ng-show="hasPermission('settings.manage.location_settings')">
            <a href="#">
                <span translate="LOCATION_SETTINGS"></span>
            </a>
            <ul>
                <li><a href="/setup#!/geo-location-levels">
                        <span translate="LOCATION_LEVELS"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/geo-locations">
                        <span translate="GEO_LOCATIONS"></span>
                    </a>
                </li>
            </ul>
        </li>
        <li ng-show="hasPermission('settings.manage.users') || hasPermission('settings.manage.roles') || hasPermission('settings.manage.rights')">
            <a href="#">
                <span translate="USER_MANAGEMENT"></span>
            </a>
            <ul>
                <li ng-show="hasPermission('settings.manage.rights')"><a href="/setup#!/rights">
                        <span translate="USER_RIGHTS"></span>
                    </a>
                </li>
                <li ng-show="hasPermission('settings.manage.roles')">
                    <a href="/setup#!/roles">
                        <span translate="USER_ROLES"></span>
                    </a>
                </li>
                <li ng-show="hasPermission('settings.manage.users')">
                    <a href="/setup#!/users">
                        <span translate="USERS"></span>
                    </a>
                </li>
                <li ng-show="hasPermission('settings.manage.rights')">
                    <a href="/setup#!/userDetails">
                        <span translate="TITLE_USERS_TRAKING"></span>
                    </a>
                </li>
                <li ng-show="hasPermission('settings.manage.rights')">
                    <a href="/setup#!/auditTrails/all">
                        <span translate="AUDIT_TRAILS"></span>
                    </a>
                </li>
            </ul>
        </li>
        <li ng-show="hasPermission('settings.manage.facilities_settings')">
            <a href="#">
                <span translate="SERVICE_PROVIDER"></span>
            </a>
            <ul>
                <li ng-show="hasPermission('settings.manage.facilities')">
                    <a href="/setup#!/facilities">
                        <span translate="SERVICE_PROVIDERS"></span>
                    </a>
                </li>
                <li><a href="/setup#!/lga-levels">
                        <span translate="PISC_LEVELS"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/facility-types">
                        <span translate="SERVICE_PROVIDER_TYPES"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/facility-ownerships">
                        <span translate="FACILITY_OWNERSHIPS"></span>
                    </a>
                </li>
            </ul>
        </li>
        <li ng-show="hasPermission('settings.manage.gfs_codes_settings')">
            <a href="#">
                <span translate="GFS_CODE_MANAGEMENT"></span>
            </a>
            <ul>
                <li><a href="/setup#!/ifrs-gfscodes-mapping">
                        <span translate="IFRS_GFS_MAPPING"></span>
                    </a>
                </li>

                <li><a href="/setup#!/account-types">
                        <span translate="ACCOUNT_TYPES"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/gfs-code-categories">
                        <span translate="GFS_CODE_CATEGORIES"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/gfs-code-sub-categories">
                        <span translate="GFS_CODE_SUB_CATEGORIES"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/gfs-codes">
                        <span translate="GFS_CODES"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/procurement-methods">
                        <span translate="PROCUREMENT_METHODS"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/procurement-types">
                        <span translate="PROCUREMENT_TYPES"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/units">
                        <span translate="UNITS"></span>
                    </a>
                </li>
            </ul>
        </li>
        <li ng-show="hasPermission('settings.manage.finance_settings')">
            <a href="#">
                <span translate="FINANCE"></span>
            </a>
            <ul>
                <li><a href="/setup#!/fund-types">
                        <span translate="FUND_TYPES"></span>
                    </a>
                </li>
                <li><a href="/setup#!/fund-source-categories">
                        <span translate="FUND_SOURCE_CATEGORIES"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/budget-classes">
                        <span translate="BUDGET_CLASSES"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/fund-sources">
                        <span translate="FUND_SOURCES"></span>
                    </a>
                </li>

                <li>
                    <a href="/setup#!/fund-source-budget-classes">
                        <span translate="FUND_SOURCE_BUDGET_CLASSES"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/bank-accounts">
                        <span translate="BANK_ACCOUNTS"></span>
                    </a>
                </li>
                <li>
                    <a href="/budgeting#!/ceilings">
                        <span translate="CEILINGS"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/projects">
                        <span translate="PROJECTS"></span>
                    </a>
                </li>
            </ul>
        </li>
        <li ng-show="hasPermission('settings.manage.planning_settings')">
            <a href="#">
                <span translate="PLANNING"></span>
            </a>
            <ul>
                <li><a href="/setup#!/activity-types">
                        <span translate="ACTIVITY_TYPES"></span>
                    </a>
                </li>
                <li><a href="/setup#!/activity-surplus-categories">
                        <span translate="ACTIVITY_SURPLUS_CATEGORIES"></span>
                    </a>
                </li>
                <li><a href="/setup#!/activity-task-natures">
                        <span translate="ACTIVITY_TASK_NATURES"></span>
                    </a>
                </li>
                <li><a href="/setup#!/activity-statuses">
                        <span translate="ACTIVITY_STATUS"></span>
                    </a>
                </li>
                <li><a href="/setup#!/task-natures" translate="TASK_NATURES"></a></li>
                <li>
                    <a href="/setup#!/reference-document-types" translate="MENU_REFERENCE_DOCUMENT_TYPES"></a>
                </li>
                <li><a href="/setup#!/reference-documents/1">
                        <span translate="GUIDELINES"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/planning-sequences">
                        <span translate="PLANNING_SEQUENCE"></span>
                    </a>
                </li>
                <li>
                    <a href="#"><span translate="PERFORMANCE_INDICATORS"></span></a>
                    <ul>
                        <li>
                            <a href="/setup#!/performance-indicators">
                                <span translate="INDICATORS"></span>
                            </a>
                        </li>
                    </ul>
                </li>
                {{-- <li>--}}
                {{-- <a href="#" translate="LINK_PRIORITY_AREAS"></a>--}}
                {{-- <ul>--}}
                {{-- <li><a href="/setup#!/link-levels" translate="LINK_LEVELS"></a></li>--}}
                {{-- <li><a href="/setup#!/priority-areas" translate="PRIORITY_AREAS"></a></li>--}}
                {{-- <li><a href="/setup#!/intervention-categories" translate="INTERVENTION_CATEGORIES"></a></li>--}}
                {{-- <li><a href="/setup#!/interventions" translate="INTERVENTIONS"></a></li>--}}
                {{-- </ul>--}}
                {{-- </li>--}}
                <li>
                    <a href="#" translate="LINK_COMPREHENSIVE_PLANS"></a>
                    <ul>
                        <li><a href="/setup#!/cas-plans" translate="CAS_PLANS"></a></li>
                        <li><a href="/setup#!/cas-group-types" translate="CAS_GROUP_TYPES"></a></li>
                        <li><a href="/setup#!/cas-group-columns" translate="CAS_GROUP_COLUMNS"></a></li>
                        <li><a href="/setup#!/cas-select-options" translate="CAS_PLAN_TABLE_SELECT_OPTION"></a></li>
                        <li><a href="/setup#!/baseline-statistics"><span translate="BASELINE_DATA"></span></a></li>
                        <li><a href="/setup#!/cas-plan-contents" translate="COMPREHENSIVE_SECTOR_PLAN_CONTENTS"></a></li>
                        <li><a href="/setup#!/cas-plan-tables" translate="COMPREHENSIVE_PLANS_TABLE"></a></li>
                        <li><a href="/setup#!/cas-plan-table-item-values" translate="COMPREHENSIVE_FORMS"></a></li>
                        <li><a href="/setup#!/account-returns"><span translate="ACCOUNT_RETURNS"></span></a></li>
                    </ul>
                </li>

                {{-- <li>--}}
                {{-- <a href="/setup#!/national-targets">--}}
                {{-- <span translate="NATIONAL_TARGETS"></span>--}}
                {{-- </a>--}}
                {{-- </li>--}}
                <li>
                <a href="/setup#!/planning-matrices">
                <span translate="PLANNING_MATRICES"></span>
                </a>
                </li>
                <li>
                    <a href="/setup#!/generic-targets">
                        <span translate="GENERIC_TARGETS"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/generic-activities">
                        <span translate="GENERIC_ACTIVITIES"></span>
                    </a>
                </li>
                {{-- <li>--}}
                {{-- <a href="#">--}}
                {{-- <span translate="TRANSPORT_FACILITY"></span>--}}
                {{-- </a>--}}
                {{-- <ul>--}}
                {{-- <li><a href="/setup#!/asset-conditions">--}}
                {{-- <span translate="ASSET_CONDITIONS"></span>--}}
                {{-- </a>--}}
                {{-- </li>--}}
                {{-- <li><a href="/setup#!/asset-uses">--}}
                {{-- <span translate="ASSET_USES"></span>--}}
                {{-- </a>--}}
                {{-- </li>--}}
                {{-- <li><a href="/setup#!/transport-facility-types">--}}
                {{-- <span translate="TRANSPORT_FACILITY_TYPES"></span>--}}
                {{-- </a>--}}
                {{-- </li>--}}
                {{-- </ul>--}}
                {{-- </li>--}}
            </ul>
        </li>
        <li ng-show="hasPermission('settings.manage.budget_settings')">
            <a href="#">
                <span translate="BUDGET"></span>
            </a>
            <ul>
                <li><a href="/setup#!/ceiling-chains"><span translate="CEILING_CHAIN"></span></a></li>
                <li><a href="/planning#!/admin-sector-mapping"><span translate="ADMIN_SECTOR_MAPPING"></span></a></li>
                <li><a href="/budgeting#!/ceilings"> <span translate="MENU_CEILING_MANAGEMENT"></span></a></li>
                <li>
                    <a href="#" translate="LINK_BUDGET_DISTRIBUTION"></a>
                    <ul>
                        <li><a href="/setup#!/expenditure-centre-groups" translate="EXPENDITURE_CENTRE_GROUPS"></a></li>
                        <li><a href="/setup#!/expenditure-centres" translate="EXPENDITURE_CENTRES"></a></li>
                    </ul>
                </li>
                <li ng-show="hasPermission('settings.manage.budgeting.submission_form')">
                    <a href="#" translate="PE_FORMS_SETUP"></a>
                    <ul>
                        <li><a href="/budgeting#!/budget-submission-forms"><span translate="MENU_PE_BUDGET_SUBMISSION_FORMS"></span></a></li>
                        <li><a href="/budgeting#!/budget-submission-sub-forms"> <span translate="MENU_BUDGET_SUBMISSION_SUB_FORMS"></span></a></li>
                        <li><a href="/setup#!/pe-submission-form-report"> <span translate="MENU_PE_SUBMISSION_FORM_REPORT"></span></a></li>
                        <li><a href="/setup#!/select-options"> <span translate="MENU_SELECT_OPTION"></span></a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li ng-show="hasPermission('settings.manage.execution')">
            <a href="#"><span translate="EXECUTION"></span></a>
            <ul>
                <li><a href="/execution#!/data-sources">
                        <span translate="DATA_SOURCES"></span>
                    </a>
                </li>
                <li><a href="/execution#!/import-methods">
                        <span translate="IMPORT_METHODS"></span>
                    </a>
                </li>
                <li>
                    <a href="/execution#!/budget-transaction-types">
                        <span translate="BUDGET_TRANSACTION_TYPES"></span>
                    </a>
                </li>
                <li>
                    <a href="/execution#!/budget-export-transactions">
                        <span translate="BUDGET_EXPORT_TRANSACTIONS"></span>
                    </a>
                </li>

                <li>
                    <a href="/setup#!/project-types">
                        <span translate="PROJECT_TYPES"></span>
                    </a>
                </li>
                <li>
                    <a href="/setup#!/expenditure-categories">
                        <span translate="EXPENDITURE_CATEGORIES"></span>
                    </a>
                </li>

                <li>
                    <a href="/setup#!/project-outputs">
                        <span translate="PROJECT_OUTPUTS"></span>
                    </a>
                </li>
            </ul>
        </li>
        <li ng-show="hasPermission('settings.manage.financial_year_settings')">
            <a href="#" translate="FINANCIAL_YEAR_MANAGEMENT"></a>
            <ul>
                <li><a href="/setup#!/financial-years" translate="FINANCIAL_YEARS"></a></li>
                <li><a href="/setup#!/period-groups" translate="PERIOD_GROUPS"></a></li>
                <li><a href="/setup#!/financial-year-periods" translate="PERIODS"></a></li>
                <li><a href="/setup#!/versions" translate="VERSIONS"></a></li>
                <li><a href="/setup#!/version-types" translate="VERSION_TYPES"></a></li>
            </ul>
        </li>
        <li ng-show="hasPermission('settings.manage.project_data_forms')">
            <a href="#" translate="PROJECT_DATA_FORMS"></a>
            <ul>
                <li><a href="/setup#!/project-data-forms" translate="PROJECT_DATA_FORMS"></a></li>
                <li><a href="/setup#!/project-data-form-contents" translate="PROJECT_DATA_FORM_CONTENTS"></a></li>
                <li><a href="/setup#!/project-data-form-questions" translate="PROJECT_DATA_FORM_QUESTIONS"></a></li>
            </ul>
        </li>
        <li ng-show="hasPermission('settings.manage.national_references')">
            <a href="/setup#!/national-references" translate="NATIONAL_REFERENCES"></a></li>

{{--        <li ng-show="hasPermission('settings.manage.assessment')">--}}
{{--            <a href="#">--}}
{{--                <span translate="ASSESSMENT"></span>--}}
{{--            </a>--}}
{{--            <ul>--}}
{{--                <li><a href="/assessment#!/cas-assessment-categories">--}}
{{--                        <span translate="CAS_ASSESSMENT_CATEGORIES"></span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li><a href="/assessment#!/cas-assessment-states">--}}
{{--                        <span translate="CAS_ASSESSMENT_STATES"></span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li><a href="/assessment#!/cas-assessment-category-versions">--}}
{{--                        <span translate="CAS_ASSESSMENT_CATEGORY_VERSIONS"></span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li><a href="/assessment#!/cas-assessment-criteria-options">--}}
{{--                        <span translate="CAS_ASSESSMENT_CRITERIA_OPTIONS"></span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li><a href="/assessment#!/cas-assessment-sub-criteria-options">--}}
{{--                        <span translate="CAS_ASSESSMENT_SUB_CRITERIA_OPTIONS"></span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li><a href="/assessment#!/cas-assessment-category-version-states">--}}
{{--                        <span translate="CAS_ASSESSMENT_CATEGORY_VERSION_STATES"></span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li><a href="/assessment#!/cas-assessment-rounds">--}}
{{--                        <span translate="CAS_ASSESSMENT_ROUNDS"></span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li><a href="/assessment#!/cas-assessment-possible-score">--}}
{{--                        <span translate="CAS_ASSESSMENT_POSSIBLE_SCORES"></span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li><a href="/assessment#!/cas-assessment-report-set">--}}
{{--                        <span translate="CAS_ASSESSMENT_REPORT_SET"></span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </li>--}}
{{--        <li ng-show="hasPermission('settings.manage.template_files_upload')">--}}
{{--            <a href="/setup#!/template-files-upload" translate="TEMPLATE_FILE_UPLOADS"></a>--}}
{{--        </li>--}}

    </ul>
</li>
<li ng-show="hasPermission('planning')">
    <a href="#">
        <div layout="row">
            <div class="inset">
                <ng-md-icon icon="subject"></ng-md-icon>
            </div>
            <div class="inset menu-text" translate="MENU_PLANNING"></div>
        </div>
    </a>
    <ul>
        <!-- <li ng-show="hasPermission('planning.manage.facilities')">
            <a href="/planning#!/facilities">
                <span translate="SERVICE_PROVIDERS"></span>
            </a>
        </li> -->
        {{-- <li ng-show="hasPermission('planning.manage.assets')">--}}
        {{-- <a href="/planning#!/transport-facilities"> <span--}}
        {{-- translate="Tansport Facilities"></span></a>--}}
        {{-- </li>--}}
        <li ng-show="hasPermission('planning.manage.reference_documents')">
            <a href="/planning#!/reference-documents/0" translate="MENU_STRATEGIC_PLAN_DOCUMENTS"></a>
        </li>
        <li ng-show="hasPermission('planning.manage.objectives')">
            <a href="/setup#!/planning-sequences">
                <span translate="PLANNING_SEQUENCE"></span>
            </a>
        </li>
        <li ng-show="hasPermission('planning.manage.targets')">
            <a href="/planning#!/targets">
                <span translate="MENU_LONG_TARGETS"></span>
            </a>
        </li>
        <li>
            <a href="#"><span translate="PERFORMANCE_INDICATORS"></span></a>
            <ul>
                <li>
                    <a href="/setup#!/performance-indicators">
                        <span translate="INDICATORS"></span>
                    </a>
                </li>
                <li ng-show="hasPermission('planning.manage.performance_indicator_base_line_values')">
                    <a href="/planning#!/performance-indicator-baseline-values"><span translate="BASELINE_VALUES"></span></a>
                </li>
            </ul>
        </li>
        {{-- <li ng-show="hasPermission('planning.manage.responsible-persons')">--}}
        {{-- <a href="/setup#!/responsible-persons">--}}
        {{-- <span translate="RESPONSIBLE_PERSONS"></span>--}}
        {{-- </a>--}}
        {{-- </li>--}}
        <li has-permission="planning.manage.plans">
            <a href="#" translate="PLANNING_ACTIVITIES"></a>
            <ul>
                <li ng-repeat="b in user.budgetTypes" has-permission="planning.activity.@{{b.budgetType}}">
                    <a href="/planning#!/plans/1/@{{b.budgetType}}">
                        <span>@{{::b.budgetType | translate}}</span>
                        <span ng-if="b.budgetType !=='CARRYOVER'">of
                            <span style="font-size: 9px" class="badge @{{(b.financialYear !== null)?'badge-success':'badge-warning'}}">
                                @{{:: (b.financialYear !== null) ? b.financialYear.name:'Notfound'}}
                            </span>
                        </span>
                        <span ng-if="b.budgetType ==='CARRYOVER'">from
                            <span style="font-size: 9px" class="badge badge-warning">
                                @{{:: (b.previousFinancialYear !== null) ? b.previousFinancialYear.name:'Notfound'}}
                            </span>
                        </span>

                    </a>
                </li>
            </ul>
        </li>
        <li ng-show="hasPermission('planning.manage.comprehensive_plans')">
            <a href="#">
                <span translate="MENU_COMPREHENSIVE_PLANS"></span>
            </a>
            <ul>
                <li ng-show="hasPermission('planning')">
                    <a href="/planning#!/baseline-data-values">
                        <span translate="BASELINE_DATA"></span>
                    </a>
                </li>

                <li ng-show="hasPermission('planning.manage.comprehensive_plans')">
                    <a href="/planning#!/comprehensive-plans">
                        <span translate="MENU_COMPREHENSIVE_PLANS"></span>
                    </a>
                </li>
            </ul>
        </li>
        <li ng-show="hasPermission('planning.manage.project_profile_data_forms')">
            <a href="/planning#!/project-data-form-answers" translate="PROJECT_DATA_FORM_VALUES"></a>
        </li>
        <li ng-show="hasPermission('planning.manage.historical_data')">
            <a href="/planning#!/historical-data" translate="HISTORICAL_DATA"></a>
        </li>
    </ul>
</li>
<li ng-show="hasPermission('budgeting')">
    <a href="#">
        <div layout="row">
            <div class="inset">
                <ng-md-icon icon="view_module"></ng-md-icon>
            </div>
            <div class="inset menu-text" translate="MENU_BUDGETING"></div>
        </div>
    </a>
    <ul>
        <li ng-show="hasPermission('budgeting.manage.projections')">
            <a href="budgeting#!/projections"> <span translate="PROJECTION"></span></a>
        </li>
        <li has-permission="budgeting.manage.ceilings">
            <a href="#">Ceiling Amounts</a>
            <ul>
                <li ng-repeat="b in user.budgetTypes" has-permission="budgeting.ceiling.@{{b.budgetType}}">
                    <a href="/budgeting#!/admin-hierarchy-ceilings/budgetType/@{{b.budgetType}}">
                        <span>@{{::b.budgetType | translate}}</span>
                        <span ng-if="b.budgetType !=='CARRYOVER'">of
                            <span style="font-size: 9px" class="badge @{{(b.financialYear !== null)?'badge-success':'badge-warning'}}">
                                @{{:: (b.financialYear !== null) ? b.financialYear.name:'Notfound'}}
                            </span>
                        </span>
                        <span ng-if="b.budgetType ==='CARRYOVER'">from
                            <span style="font-size: 9px" class="badge badge-warning">
                                @{{:: (b.previousFinancialYear !== null) ? b.previousFinancialYear.name:'Notfound'}}
                            </span>
                        </span>
                    </a>
                </li>
            </ul>
        </li>
        <li ng-show="hasPermission('budgeting.manage.submission.form')">
            <a href="#">Personal Emoluments</a>
            <ul>
                <li><a href="/budgeting#!/budgetSubmissionLines"> <span translate="MENU_BUDGET_SUBMISSION_LINES"></span></a></li>
                <li><a href="/setup#!/select-options"> <span translate="MENU_SELECT_OPTION"></span></a></li>
            </ul>
        </li>

        <li has-permission="budgeting.manage.budgets">
            <a translate="Costing" href="#"></a>
            <ul>
                <li ng-repeat="b in user.budgetTypes" has-permission="budgeting.input.@{{b.budgetType}}">
                    <a href="/budgeting#!/plans/2/@{{b.budgetType}}">
                        <span>@{{::b.budgetType | translate}}</span>
                        <span ng-if="b.budgetType !=='CARRYOVER'">of
                            <span style="font-size: 9px" class="badge @{{(b.financialYear !== null)?'badge-success':'badge-warning'}}">
                                @{{:: (b.financialYear !== null) ? b.financialYear.name:'Notfound'}}
                            </span>
                        </span>
                        <span ng-if="b.budgetType ==='CARRYOVER'">from
                            <span style="font-size: 9px" class="badge badge-warning">
                                @{{:: (b.previousFinancialYear !== null) ? b.previousFinancialYear.name:'Notfound'}}
                            </span>
                        </span>
                    </a>
                </li>
            </ul>
        </li>
        <li ng-show="hasPermission('budgeting.manage.scrutinization')"><a href="/budgeting#!/scrutinization/"><span translate="MENU_SCRUTINIZATION"></span></a></li>
        <li ng-show="hasPermission('budgeting.manage.approve_budget')">
            <a href="#">
                <span translate="CURRENT_FINANCIAL_YEAR_BUDGET"></span>
            </a>
            <ul>
                <li ng-repeat="b in user.budgetTypes" has-permission="execution.export.@{{b.budgetType}}">
                    <a href="/budgeting#!/current-financial-year-plans/@{{b.budgetType}}">
                        <span>@{{::b.budgetType | translate}}</span>
                        <span ng-if="b.budgetType !=='CARRYOVER'">of
                            <span style="font-size: 9px" class="badge @{{(b.financialYear !== null)?'badge-success':'badge-warning'}}">
                                @{{:: (b.financialYear !== null) ? b.financialYear.name:'Notfound'}}
                            </span>
                        </span>
                        <span ng-if="b.budgetType ==='CARRYOVER'">from
                            <span style="font-size: 9px" class="badge badge-warning">
                                @{{:: (b.previousFinancialYear !== null) ? b.previousFinancialYear.name:'Notfound'}}
                            </span>
                        </span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>

<li ng-show="hasPermission('execution')">
    <a href="#">
        <div layout="row">
            <div class="inset">
                <ng-md-icon icon="slow_motion_video"></ng-md-icon>
            </div>
            <div class="inset menu-text" translate="EXECUTION"></div>
        </div>
    </a>
    <ul>

        <!-- <li>
            <a href="#">
                <span translate="RECEIVED_FUNDS"></span>
            </a>
            <ul>
                <li><a href="/execution#!/received-funds">
                        <span translate="RECEIVED_FUNDS"></span>
                    </a>
                </li>
                <li><a href="/execution#!/received-fund-items">
                        <span translate="RECEIVED_FUND_ITEMS"></span>
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="#">
                <span translate="Export Ceiling"></span>
            </a>
            <ul>
                <li><a href="/execution#!/export-ceiling" has-permission='execution.manage.budget_reallocation'>
                        <span translate="Ceiling Amounts"></span>
                    </a>
                </li>
            </ul>
        </li> -->

        <li>
            <a href="#">
                <span translate="Export Budget"></span>
            </a>
            <ul>
                <li ng-repeat="b in user.budgetTypes" ng-if="b.budgetType !== 'CURRENT'" has-permission="execution.export.@{{b.budgetType}}">
                    <a href="/execution#!/budget-export-accounts/@{{b.budgetType}}">
                        <span>@{{::b.budgetType | translate}}</span>
                        <span ng-if="b.budgetType !=='CARRYOVER'">of
                            <span style="font-size: 9px" class="badge @{{(b.financialYear !== null)?'badge-success':'badge-warning'}}">
                                @{{:: (b.financialYear !== null) ? b.financialYear.name:'Notfound'}}
                            </span>
                        </span>
                        <span ng-if="b.budgetType ==='CARRYOVER'">from
                            <span style="font-size: 9px" class="badge badge-warning">
                                @{{:: (b.previousFinancialYear !== null) ? b.previousFinancialYear.name:'Notfound'}}
                            </span>
                        </span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">
                <span translate="Export Revenue"></span>
            </a>
            <ul>
                <li ng-repeat="b in user.budgetTypes" ng-if="b.budgetType !== 'CURRENT'" has-permission="execution.export.@{{b.budgetType}}">
                    <a href="/execution#!/revenue-export-accounts/@{{b.budgetType}}">
                        <span>@{{::b.budgetType | translate}}</span>
                        <span ng-if="b.budgetType !=='CARRYOVER'">of
                            <span style="font-size: 9px" class="badge @{{(b.financialYear !== null)?'badge-success':'badge-warning'}}">
                                @{{:: (b.financialYear !== null) ? b.financialYear.name:'Notfound'}}
                            </span>
                        </span>
                        <span ng-if="b.budgetType ==='CARRYOVER'">from
                            <span style="font-size: 9px" class="badge badge-warning">
                                @{{:: (b.previousFinancialYear !== null) ? b.previousFinancialYear.name:'Notfound'}}
                            </span>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="/execution#!/revenue-export-feedback">
                        <span translate="">Export Revenue Feedback</span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">
                <span translate="Export Reallocation"></span>
            </a>
            <ul>
                <li ng-repeat="b in user.budgetTypes" ng-if="b.budgetType !== 'CURRENT'" has-permission="execution.export.@{{b.budgetType}}">
                    <a href="/execution#!/export-reallocation/@{{b.budgetType}}">
                        <span>@{{::b.budgetType | translate}}</span>
                        <span ng-if="b.budgetType !=='CARRYOVER'">of
                            <span style="font-size: 9px" class="badge @{{(b.financialYear !== null)?'badge-success':'badge-warning'}}">
                                @{{:: (b.financialYear !== null) ? b.financialYear.name:'Notfound'}}
                            </span>
                        </span>
                        <span ng-if="b.budgetType ==='CARRYOVER'">from
                            <span style="font-size: 9px" class="badge badge-warning">
                                @{{:: (b.previousFinancialYear !== null) ? b.previousFinancialYear.name:'Notfound'}}
                            </span>
                        </span>
                    </a>
                </li>
            </ul>
        </li>
        {{-- export activities for muse--}}
        <li ng-show="hasPermission('execution.manage.exports')">
            <a href="#"> <span translate="ACTIVITIES_EXPORT"></span>
            </a>
            <ul>
                <li><a href="/execution#!/muse-setup-segments-export">
                        <span translate="SEGMENTS_EXPORT"></span>
                    </a>
                </li>
                <li><a href="/execution#!/coa-segment-feedback">
                        <span translate="COA_SEGMENT_FEEDBACK"></span>
                    </a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#">
                <span translate="Export Transaction"></span>
            </a>
            <ul>
                <li ng-repeat="b in user.budgetTypes" ng-if="b.budgetType !== 'CURRENT'" has-permission="execution.export.@{{b.budgetType}}">
                    <a href="/execution#!/export-transaction/@{{b.budgetType}}">
                        <span>@{{::b.budgetType | translate}}</span>
                        <span ng-if="b.budgetType !=='CARRYOVER'">of
                            <span style="font-size: 9px" class="badge @{{(b.financialYear !== null)?'badge-success':'badge-warning'}}">
                                @{{:: (b.financialYear !== null) ? b.financialYear.name:'Notfound'}}
                            </span>
                        </span>
                        <span ng-if="b.budgetType ==='CARRYOVER'">from
                            <span style="font-size: 9px" class="badge badge-warning">
                                @{{:: (b.previousFinancialYear !== null) ? b.previousFinancialYear.name:'Notfound'}}
                            </span>
                        </span>
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="#">
                <span translate="REALLOCATION"></span>
            </a>
            <ul>
                <li><a href="/execution#!/open-budget-reallocations" has-permission='execution.manage.budget_reallocation'>
                        <span translate="BUDGET_REALLOCATIONS"></span>
                    </a>
                </li>
                <li has-permission='execution.manage.approve_budget_reallocation'>
                    <a href="/execution#!/budget-reallocation-approval">
                        <span translate="APPROVE_BUDGET_REALLOCATION"></span>
                    </a>
                </li>
                <li has-permission='execution.manage.mass-reallocation'>
                    <a href="/execution#!/budget-mass-reallocations">
                        <span translate="BUDGET_MASS_REALLOCATIONS"></span>
                    </a>
                </li>
                <li has-permission='execution.manage.reallocation-btn-hierarchies'>
                    <a href="/execution#!/budget-reallocation-btn-hierarchies">
                        <span translate="REALLOCATION_WITHIN_VOTE"></span>
                    </a>
                </li>
                <li has-permission='execution.manage.mass-reallocation'>
                    <a href="/execution#!/reallocation-feedback">
                        <span translate="REALLOCATION_FEEDBACK"></span>
                    </a>
                </li>
            </ul>
        </li>
        <li ng-show="hasPermission('execution.manage.activity')">
            <a href="#">
                <span translate="Actual Implementations"></span>
            </a>
            <ul>
                <li>
                    <a href="/execution#!/activity-implementation">
                        <span translate="ACTIVITY_IMPLEMENTATION_MENU"></span>
                    </a>
                </li>


                <!-- <li>
                    <a href="/execution#!/activity-overall-achievement">
                        <span translate="ACTIVITY_OVERALL_ACHIEVEMENT"></span>
                    </a>
                </li>
                <li ng-show="hasPermission('execution')">
                    <a href="/execution#!/revenue-allocation-expenditure">
                        <span translate="REVENUE_ALLOCATION_EXPENDITURE"></span>
                    </a>
                </li> -->
                <li ng-show="hasPermission('execution')">
                    <a href="/execution#!/actual-transaction">
                        <span translate="Actual Transactions"></span>
                    </a>
                </li>

            </ul>
        </li>
    </ul>
</li>
<!-- <li ng-show="hasPermission('assessment')">
    <a href="#">
        <div layout="row">
            <div class="inset">
                <ng-md-icon icon="dialpad"></ng-md-icon>
            </div>
            <div class="inset menu-text" translate="ASSESSMENT"></div>
        </div>
    </a>
    <ul>
        <li ng-show="hasPermission('budgeting.manage.assessment')">
            <a href="/assessment#!/cas-assessment-results/0/0">
                <span translate="MENU_ASSESSMENT"></span>
            </a>
        </li>
        <li ng-show="hasPermission('budgeting.manage.assessors')"><a href="/setup#!/assessor-assignment-setup" translate="ASSESSOR_ASSIGNMENT"></a></li>
    </ul>
</li> -->
<li ng-show="hasPermission('report')">
    <a href="#">
        <div layout="row">
            <div class="inset">
                <ng-md-icon icon="show_chart"></ng-md-icon>
            </div>
            <div class="inset menu-text" translate="REPORTS"></div>
        </div>
    </a>
    <ul>
        {{-- <li><a href="/report#!/projections"> <span translate="LINK_PROJECTION"></span></a></li>
         <li><a href="/report#!/ceilings"> <span translate="LINK_CEILING"></span></a></li>--}}
        {{--<li><a href="/report#!/budget-aggregations"> <span translate="BUDGET_AGGREGATION"></span></a></li>
        <li><a href="/report#!/budget-summary"> <span translate="BUDGET_SUMMARY"></span></a></li>--}}
        <li><a href="/report#!/cas-preview"> <span translate="CAS_PREVIEW"></span></a></li>
    </ul>
</li>
