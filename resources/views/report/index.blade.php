@extends('layouts.master')
@section("app")
    ng-app="report-module"
@stop
@section('content')
    <div >
        <ng-view></ng-view>
    </div>
@stop
@section("module-script")
    <link href="/lib/c3/c3.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/lib/c3/d3.min.js"></script>
    <script type="text/javascript" src="/lib/c3/c3.min.js"></script>
    <script type="text/javascript" src="/lib/c3/c3-angular.min.js"></script>
    <script type="text/javascript" src="/js/report/report-module.js"></script>
    <script type="text/javascript" src="/js/report/report-services.js"></script>
    <script type="text/javascript" src="/js/report/controllers/report-controller.js"></script>
    <script type="text/javascript" src="/js/report/controllers/comprehensive-plan-preview-controller.js"></script>
    <script type="text/javascript" src="/js/report/controllers/pe-submission-report-controller.js"></script>
    <script type="text/javascript" src="/js/report/controllers/printout-controller.js"></script>
    <script type="text/javascript" src="/js/report/controllers/projection-controller.js"></script>
    <script type="text/javascript" src="/js/report/controllers/ceiling-controller.js"></script>
    <script type="text/javascript" src="/js/report/controllers/procurement-controller.js"></script>
    <script type="text/javascript" src="/js/report/controllers/bar-chart-controller.js"></script>
    <script type="text/javascript" src="/js/report/controllers/pie-chart-controller.js"></script>
    <script type="text/javascript" src="/js/report/controllers/ab-controller.js"></script>
    <script type="text/javascript" src="/js/report/controllers/budget-aggregation-controller.js"></script>
    <script type="text/javascript" src="/js/report/controllers/facility-status-report-controller.js"></script>
    <script type="text/javascript" src="/js/report/controllers/budget-summary-controller.js"></script>
    <script type="text/javascript" src="/js/report/controllers/cdr-controller.js"></script>
    <script type="text/javascript" src="/js/report/controllers/cdr-consolidated-controller.js"></script>
@stop
