<?php
/**
 * Created by PhpStorm.
 * User: mrisho
 * Date: 20/Apr/2017
 * Time: 03:51
 */



?>
@extends('layouts.master_reports')

@section('content')


    <?php if($report == "noFile")  { ?>


    <div class="row">
        <div class="col-md-12">
            <form action="/display_report" method="POST">
                {{ csrf_field() }}
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h5 class="panel-title"><?= $reportTittle ?> Parameters</h5>
                    </div>

                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                                <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span
                                            class="sr-only">Close</span></button>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <?php if(in_array('region_id',$parameters)) { ?>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <table >
                                        <tr style="padding-bottom: 1em;">
                                            <td width="15%">Region</td>
                                            <td>
                                                <select name="region_id" class="form-control select-search">
                                                    <?php
                                                    foreach ($region as $value) {
                                                        $id = $value->id;
                                                        $name = $value->name;
                                                        echo "<option value='$id'>$name</option>";
                                                    }
                                                    ?>
                                                </select></td>
                                        </tr>
                                    </table></div></div>
                                        <?php } ?>
                                            <?php if(in_array('admin_hierarchy_id',$parameters)) { ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <table >
                                            <tr height="20">
                                                <td width="15%">Council</td>
                                                <td>
                                                    <select name="admin_hierarchy_id" class="form-control select-search">
                                                        <?php
                                                        foreach ($admin_hierarchy as $value) {
                                                            $id = $value->id;
                                                            $name = $value->name;
                                                            echo "<option value='$id'>$name</option>";
                                                        }
                                                        ?>
                                                    </select></td>
                                            </tr></table></div></div>
                                            <?php } ?>
                                            <?php if(in_array('budget_class_id',$parameters)) { ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <table >
                                            <tr style="padding-bottom: 1em;">
                                                <td width="15%">Budget Class</td>
                                                <td>
                                                    <select name="budget_class_id" class="form-control select-search">
                                                        <?php
                                                        foreach ($budget_classes as $value) {
                                                            $id = $value->id;
                                                            $name = $value->name;
                                                            echo "<option value='$id'>$name</option>";
                                                        }
                                                        ?>
                                                    </select></td>
                                            </tr></table></div></div>
                                            <?php } ?>
                                        <?php if(in_array('fund_source_id',$parameters)) { ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <table >
                                            <tr style="padding-bottom: 1em;">
                                            <td width="15%">Fund Source</td>
                                            <td>
                                                <select name="fund_source_id" class="form-control select-search">
                                                    <?php
                                                    foreach ($fund_source as $value) {
                                                        $id = $value->id;
                                                        $name = $value->name;
                                                        echo "<option value='$id'>$name</option>";
                                                    }
                                                    ?>
                                                </select></td>
                                            </tr></table></div></div>
                                        <?php } ?>

                                            <?php if(in_array('sector_dept_id',$parameters)) { ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <table >
                                            <tr style="padding-bottom: 1em;">
                                                <td width="15%">Sector</td>
                                                <td>
                                                    <select name="sector_dept_id" class="form-control select-search">
                                                        <?php
                                                        foreach ($sections as $value) {
                                                            $code = $value->id;
                                                            $name = $value->name;
                                                            echo "<option value='$code'>$name</option>";
                                                        }
                                                        ?>
                                                    </select></td>
                                            </tr></table></div></div>
                                            <?php } ?>

                                <?php if(in_array('financial_year_id',$parameters)) { ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <table >
                                            <tr style="padding-bottom: 1em;">
                                                <td width="15%">Sector</td>
                                                <td>
                                                    <select name="sector_dept_id" class="form-control select-search">
                                                        <?php
                                                        foreach ($sections as $value) {
                                                            $code = $value->code;
                                                            $name = $value->name;
                                                            echo "<option value='$code'>$name</option>";
                                                        }
                                                        ?>
                                                    </select></td>
                                            </tr></table></div></div>
                                <?php } ?>

                                <?php if(in_array('period_type_id',$parameters)) { ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <table >
                                            <tr style="padding-bottom: 1em;">
                                                <td width="15%">Sector</td>
                                                <td>
                                                    <select name="sector_dept_id" class="form-control select-search">
                                                        <?php
                                                        foreach ($sections as $value) {
                                                            $code = $value->code;
                                                            $name = $value->name;
                                                            echo "<option value='$code'>$name</option>";
                                                        }
                                                        ?>
                                                    </select></td>
                                            </tr></table></div></div>
                                <?php } ?>

                                <?php if(in_array('period_id',$parameters)) { ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <table >
                                            <tr style="padding-bottom: 1em;">
                                                <td width="15%">Sector</td>
                                                <td>
                                                    <select name="sector_dept_id" class="form-control select-search">
                                                        <?php
                                                        foreach ($sections as $value) {
                                                            $code = $value->code;
                                                            $name = $value->name;
                                                            echo "<option value='$code'>$name</option>";
                                                        }
                                                        ?>
                                                    </select></td>
                                            </tr></table></div></div>
                                <?php } ?>

                                </div>
                            </div>

                        </div>
                        <div class="panel-footer">
                            <input name="id" type="hidden" value="<?=$report_id ?>">
                            <div class="col-md-12">
                                <div class="text-right">
                                    <a href="/report_home" class="btn btn-warning pull-left">Cancel <i class="fa fa-backward"></i></a>
                                    <button type="submit" class="btn btn-success">
                                        Submit <i class="fa fa-save position-right"></i></button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php }  else { ?>
    <?php
    ?>
    <object width="1000" height="1200" data="<?= $report ?>" type="application/pdf">
        <embed src="<?= $report ?>" type="application/pdf"/>
    </object>
    <?php } ?>

@stop
