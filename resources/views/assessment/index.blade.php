@extends('layouts.master')
@section("app")
    ng-app="assessment-module"
@stop
@section('content')
    <div>
        <ng-view></ng-view>
    </div>
@stop

@section('module-script')
    <script type="text/javascript" src="/js/assessment/assessment-module.js"></script>
    <script type="text/javascript" src="/js/assessment/assessment-services.js"></script>
    <script type="text/javascript" src="/js/assessment/controllers/assessment-controller.js"></script>
    <script type="text/javascript" src="/js/assessment/controllers/cas-assessment-category-controller.js"></script>
    <script type="text/javascript" src="/js/assessment/controllers/cas-assessment-category-version-controller.js"></script>
    <script type="text/javascript" src="/js/assessment/controllers/cas-assessment-round-controller.js"></script>
    <script type="text/javascript" src="/js/assessment/controllers/cas-assessment-state-controller.js"></script>
    <script type="text/javascript" src="/js/assessment/controllers/cas-assessment-sub-criteria-possible-score-controller.js"></script>
    <script type="text/javascript" src="/js/assessment/controllers/cas-assessment-sub-criteria-report-set-controller.js"></script>
    <script type="text/javascript" src="/js/assessment/controllers/cas-assessment-category-version-state-controller.js"></script>
    <script type="text/javascript" src="/js/assessment/controllers/cas-assessment-criteria-option-controller.js"></script>
    <script type="text/javascript" src="/js/assessment/controllers/cas-assessment-sub-criteria-option-controller.js"></script>
    <script type="text/javascript" src="/js/assessment/controllers/cas-assessment-result-controller.js"></script>
@stop
