@extends('layouts.master')
@section("app")
    ng-app="planning-module"
@stop
@section('content')
    <div >
       <ng-view></ng-view>
    </div>
@stop
@section('module-script')
    <script type="text/javascript" src="/js/planning/planning-module.js"></script>
    <script type="text/javascript" src="/js/planning/planning-service.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/planning-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/test-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/planning-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/planning-target-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/activity-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/create-or-update-activity-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/add-or-remove-activity-facility-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/admin-sector-mapping-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/pe-contribution-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/annual-target-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/long-term-target-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/sector-target-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/mtef-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/mtef-sector-problem-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/comprehensive-plans-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/cas-plan-table-item-admin-hierarchy-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/baseline-statistic-values-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/performance-indicator-baseline-value-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/transport-facility-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/facility-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/reference-document-controller.js"></script>
    <script type="text/javascript" src="/js/setup/controllers/project-data-form-question-item-values-controller.js"></script>
    <script type="text/javascript" src="/js/planning/controllers/historical-data-controller.js"></script>

@stop
