<?php

return [
    "private-key" => env("ESB_CLIENT_PRIVATE_KEY"),
    "public-key" => env("ESB_CLIENT_PUBLIC_KEY"),
    "esb-public-key" => env("GOV_ESB_PUBLIC_KEY"),
    "client-id" => env("ESB_CLIENT_ID"),
    "client-secret" => env("ESB_CLIENT_SECRET"),
    "nida-user-id" => env("NIDA_ID"),
    "esb-token-url" => env("GOVESB_TOKEN_URL"),
    "esb-engine-url" => env("GOVESB_ENGINE_URL"),
    "psssf-budget-api-code" => env("PSSSF_BUDGET_API_CODE"),
];
