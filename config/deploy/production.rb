set :stage, :production # eg production
set :branch, "dev" # git branch to pull

# server "41.59.225.121", user: "admin", roles: %w{web app db}, primary: true
# set :deploy_to, "/var/www/html/planrep"

server "196.192.79.62", user: "deploy", roles: %w{web app db}, primary: true
#server "41.59.85.254", user: "deploy", roles: %w{web app db}, primary: true
set :deploy_to, "/var/www/html/planrep"

# server "196.192.79.62",
#   user: "deploy",
#   roles: %w{web app},
#   ssh_options: { forward_agent: true,user: fetch(:user), keys: %w(~/.ssh/authorized_keys) }

set :enable_ssl, false
