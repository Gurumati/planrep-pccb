<?php

use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Jobs\SendDataToRabbitMQJob;
use App\Jobs\PublishDataToRabbitMQJob;
use App\Jobs\sendNotifications;
use App\Jobs\UpdateCeilingExportResponse;
use App\Models\Auth\User;
use App\Models\Setup\ConfigurationSetting;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Execution\BudgetExportAccount;
use App\Models\Execution\BudgetExportTransaction;
use App\Http\Services\Execution\BudgetExportToFinancialSystemService;
use App\Http\Services\Execution\BudgetExportTransactionItemService;
use Illuminate\Support\Facades\Log;

class SendDataToFinancialSystems
{
    protected $error_message = array();
    protected $failed_transaction = array();

    public function sendGLAccountSegments($admin_hierarchy_id)
    {
        //get current user
        $user_id = UserServices::getUser()->id;
        $users = User::where('id', $user_id)->get();
        //get segment numbers
        $numbers = $this->getSegmentNumbers($admin_hierarchy_id);
        $counter = 1;
        foreach ($numbers as $item) {
            $segment_number = $item->segment_number;
            if ($item->sub_vote == '056') {
                $item->sub_vote = '000';
            }
            $message = array('id' => $counter,
                'Company' => $item->sub_vote,
                'COACode' => $item->coa_code,
                'SegmentNbr' => $item->segment_number);
            $coa_segments = $this->loadSegments($admin_hierarchy_id, $segment_number);
            $segment_values = array();
            foreach ($coa_segments as $segment) {
                //get segment values
                array_push($segment_values, array('SegmentCode' => $segment->segment_code,
                    'SegmentName' => $segment->segment_name,
                    'SegmentDesc' => $segment->segment_description,
                    'NormalBalance' => $segment->normal_balance,
                    'Category' => $segment->category,
                    'UID' => "$segment->id"));
            }

            $queue_name = ConfigurationSetting::getValueByKey('COA_SEGMENT_QUEUE_NAME');
            $company = $item->sub_vote;

            $options = array(
                'headers' => array('x-match' => 'all', 'Company' => $company, 'type' => 'Transaction'),
                'exchange' => 'COASegmentsExchange');
            $message['SegmentValues'] = $segment_values;
            try {
                $message = array($message);
                dispatch(new SendDataToRabbitMQJob($queue_name, $message, $options, 'updateSegmentValues'));
            } catch (Exception $e) {
                //get exception message, send notification
                $error_msg = $e;
                $url = url('/notifications#!/');
                $object_id = 0;
                $job = new sendNotifications($users, $object_id, $url, $error_msg, "fa fa-book");
                dispatch($job->delay(10));
            }

            $counter++;
        }

    }


    public function sendBudget($admin_hierarchy_id, $financial_year_id, $financial_system_code, $transaction_id, $budget_type)
    {
        ini_set('max_execution_time', 3600);
        //get company
        $admin_hierarchy = DB::table('admin_hierarchies as c')
            ->join('admin_hierarchies as r', 'c.parent_id', 'r.id')
            ->where('c.id', $admin_hierarchy_id)
            ->select('c.code as council', 'r.code as region')
            ->first();

        $company = $admin_hierarchy->council;
        $vote = $admin_hierarchy->region;

        //get apply date - the last date of the financial year of the execution
        //get previous financial year id for MUSE testing
       // $previous_fyear_id = $financial_year_id-1;
        $apply_date = DB::table('financial_years')
            ->where('id', $financial_year_id) //return $financial_year_id after test completion
            ->select('end_date')
            ->first()->end_date;
        $end_date = date("Y-m-d", strtotime($apply_date));
        $end_time = "23:59:59";
        $apply_date = $end_date." ".$end_time;

        //credit account
        $control = $this->getControlAccount('000', '0000');
        $credit_account = $control['credit_account'];

        $options = array(
            'headers' => array('x-match' => 'all', 'Company' => $company, 'type' => 'Transaction'),
            'exchange' => 'BudgetExchange'
        );

        // $queue_name = ConfigurationSetting::getValueByKey('BUDGET_QUEUE_NAME');
        //$queue_name = 'PLANREPOTR_TO_OTRMIS_DATA_QUEUE';

        //get data
        $budget = DB::table('budget_export_to_financial_systems as b')
            ->join('budget_export_transaction_items as i', 'b.budget_export_transaction_item_id', 'i.id')
            ->join('budget_export_accounts as a', 'i.budget_export_account_id', 'a.id')
            ->join('activities as ac', 'a.activity_id', 'ac.id')
            ->join('activity_facility_fund_source_inputs as ai', 'ai.id', 'a.activity_facility_fund_source_input_id')
            ->join('units as u', 'ai.unit_id', 'u.id')
            ->join('budget_export_transactions as t', 't.id', 'i.budget_export_transaction_id')
            ->where('b.admin_hierarchy_id', $admin_hierarchy_id)
            ->where('b.financial_year_id', $financial_year_id)
            ->where('a.financial_system_code', $financial_system_code)
            ->where(function ($q) {
                $q->where('b.is_sent', false)->orWhereNull('b.is_sent');
            })
            ->where('i.budget_reallocation_item_id', null)
            ->where('ai.budget_type', $budget_type)
            ->where('t.id', $transaction_id)
            ->where('i.is_credit', true)
            ->where('a.is_delivered', false)
            ->where('a.amount', '>', 0)
            ->select('b.id','ai.id as input_id', 'b.chart_of_accounts', 'b.amount', 'b.description', 't.control_number', 'ac.description as acdescription',
                     'u.name', 'ai.unit_price', 'ai.frequency', 'ai.quantity')
            ->groupBy('b.id','ai.id', 'b.chart_of_accounts', 'b.amount', 'b.description', 't.control_number', 'ac.description', 'ai.unit_price', 'ai.frequency', 'ai.quantity','u.name');

        //generate budget import items ready to export
        $accounts = array();
        $message = array();
        if ($budget->count() > 0) {
            foreach ($budget->get() as $data) {

                if($company == 'TR196'){

                    $input_id = $data ->input_id;
                    $quarters = DB::select("select p.name as name,percentage from activity_facility_fund_source_input_periods affsip
                                                join periods p on affsip.period_id = p.id
                                                where activity_facility_fund_source_input_id = $input_id");

                    $quartersArray = array();
                    foreach($quarters as $quarter ){

                        $quarterObject = array(
                            'periodName' => $quarter->name,
                            'percentage' => $quarter->percentage
                        );

                        array_push($quartersArray,$quarterObject);
                    }
                    $description = str_limit($data->description, 40);
                    //GL account with Pipe to financial systems
                     $newGlAccount =  str_replace('-', '|',$data->chart_of_accounts );
                     $item = array(
                        'acc' => $newGlAccount,
                        'activityDescription' => $data->acdescription,
                        'amount' => $data->amount,
                        'unitPrice' => $data->unit_price,
                        'frequency' => $data->frequency,
                        'quantity' => $data->quantity,
                        'unitOfMeasure' => $data->name,
                        'period' => $quartersArray,
                        'uid' => $data->id);

                    array_push($accounts, $item);

                    $transaction_control_number = $data->control_number;

                }else{

                    $description = str_limit($data->description, 40);
                    //GL account with Pipe to financial systems
                     $newGlAccount =  str_replace('-', '|',$data->chart_of_accounts );
                     $item = array('acc' => $newGlAccount,
                        'amount' => $data->amount,
                        'uid' => $data->id);

                    array_push($accounts, $item);

                    $transaction_control_number = $data->control_number;

                }

            }

            $book = ConfigurationSetting::where('key', 'BUDGET_BOOK_ID')->first()->value;

            //get journal code
            $journal_code = BudgetExportAccount::getJournalCode($budget_type);
            if($company == 'TR196'){$receiver = 'NAVISION';}else{$receiver = 'MUSE';}
            $messageHeader = array(
                "sender" => "PLANREPOTR",
                "receiver" => $receiver,
                "msgId" => $transaction_control_number,
                "applyDate" => $apply_date,
                "messageType" => "APPROVED",
                "createdAt" => date('Y-m-d H:i:s')
            );
            $messageSummary = array(
                "company"=> $company,
                "trxCtrlNum" => $transaction_control_number,
                "bookID" => $book,
                "journalCode"=>$journal_code,
                "currencyCode"=>"TZS",
                "applyDate"=>$apply_date,
                "description"=>"APPROVED"
            );

            $messageDetails = $accounts;

            $item = array(
                "messageHeader" => $messageHeader,
                "messageSummary" =>$messageSummary,
                "messageDetails" => $messageDetails
            );

            $data = json_encode($item);
            $plainText = $data;
            $privateKey = openssl_pkey_get_private("file://" . base_path() . "/config/keys/private.pem");
            $publicKey = openssl_pkey_get_public("file://" . base_path() . "/config/keys/public.pem");

            // Make a signature
            openssl_sign($plainText, $signature, $privateKey, OPENSSL_ALGO_SHA256);
            $signature = base64_encode($signature);

            // Verify signature 0 for fail & 1 for success
            //            $res = openssl_verify($plainText,base64_decode($signature),$pubString,OPENSSL_ALGO_SHA256);

            $payloadToMuse = array(
                'message' => $item,
                'digitalSignature'=>$signature
            );

            if ($journal_code != null) {
                try {
                    if($company == 'TR196'){
                        $response_message = "Budget sent to GovESB. Please wait for Notification";
                        $response =  app('App\Http\Controllers\Execution\SocialSecurityIntegrationController')->sendRequestToNavision($payloadToMuse);

                    }else{
                        $response_message = "Budget sent to MoFP Gateway. Please wait for Notification";
                        $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($payloadToMuse);
                    }
                    //dispatch(new PublishDataToRabbitMQJob($queue_name,$payloadToMuse, $options,'budget'));
                    if($response == "ACCEPTED"){
                    foreach($messageDetails as $detail){
                       $uid = $detail["uid"];
                       DB::table('budget_export_to_financial_systems')->where('id',$uid)->update(['is_sent' => true]);
                       DB::table('budget_export_responses')
                       ->insert([
                           'budget_export_to_financial_system_id' => $uid,
                           'is_sent' => true,
                           'is_delivered' => false,
                           'response' => $response,
                           'financial_system_code'=> $receiver,
                           'created_at' =>\Carbon\Carbon::now()
                       ]);
                    }
                    DB::table('budget_export_transactions')->where('id', $transaction_id)->update(['is_sent' => true]);
                }
                    //code...
                } catch (\Throwable $th) {
                    //throw $th;
                }
              //  dispatch(new SendDataToRabbitMQJob($queue_name, $message, $options, 'budget'));
               // $response_message = "Budget has been sent to MUSE. Please wait for Notification";
                //update transaction

            }
        } else {
            //nothing to export
            //$response_message = "No Budget to Export, Please select proper budget type or  contact administrator";
            $response_message = "Budget already exported, no new Budget to export";
        }
        return $response_message;
    }


    public function sendProjectBudget($admin_hierarchy_id, $financial_year_id, $financial_system_code, $transaction_id, $budget_type){

        ini_set('max_execution_time', 3600);

        $admin_hierarchy = DB::table('admin_hierarchies as c')
        ->join('admin_hierarchies as r', 'c.parent_id', 'r.id')
        ->where('c.id', $admin_hierarchy_id)
        ->select('c.code as council', 'r.code as region')
        ->first();

            $company = $admin_hierarchy->council;
            $vote = $admin_hierarchy->region;


        $projectBudgetArray = array();
        $projects = DB::select("select p.id as id,p.code as code,p.name as description from activities a
                                    join mtef_sections s on a.mtef_section_id = s.id
                                    join mtefs m on s.mtef_id = m.id
                                    join projects p on  p.id = a.project_id
                                 where m.admin_hierarchy_id=$admin_hierarchy_id
                                 and m.financial_year_id = $financial_year_id
                                 and p.code != '0000'
                                 group by p.id");


        if (sizeof($projects) > 0 ){
            foreach ($projects as $project){
                            $project_id =  $project->id;
                            $project_code =  $project->code;
                            $project_name =  $project->description;
                            /** Find target */
                $activities = DB::select("select a2.id as actid,sec.code costcentrecode,fs.code financiercode,CONCAT(substring(a2.code from 1 for 1),substring(a2.code from 4)) activitycode,a2.description activitydescription from activities a2
                                            join mtef_sections s on a2.mtef_section_id = s.id
                                            join sections sec on s.section_id = sec.id
                                            join mtefs m on s.mtef_id = m.id
                                            join projects p on  p.id = a2.project_id
                                            join activity_facilities af on a2.id = af.activity_id
                                            join activity_facility_fund_sources affs on af.id = affs.activity_facility_id
                                            join fund_sources fs on affs.fund_source_id = fs.id
                                    where financial_year_id = $financial_year_id
                                    and admin_hierarchy_id = $admin_hierarchy_id
                                    and p.id in ($project_id)
                                    group by project_id, p.code,p.name,a2.id,a2.code, a2.description,fs.code,sec.code");

                $activityArray = array();
                $projectAmount = 0;
                foreach ($activities as $activity){

                    $activity_id = $activity->actid;
                    $inputs = DB::select('SELECT
                                            gc.code as gfscode,gc.description as gfsdescription,COALESCE (SUM(i.unit_price * i.quantity * i.frequency),0) AS amount
                                            FROM  activity_facility_fund_source_inputs AS i
                                            JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
                                            JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
                                            JOIN activities AS a ON a.id = af.activity_id
                                            JOIN gfs_codes AS gc on i.gfs_code_id = gc.id
                                            WHERE a.id in ($activity_id)
                                            group by gc.description, gc.code
                                        ');
                            
                            $inputArray = array();
                            foreach($inputs as $input){

                                $inputObject = array(
                                    "gfsCode"=> $input->gfscode,
                                    "gfsDescription"=> $input->gfsdescription,
                                    "amount"=> number_format($input->amount,2,'.','')
                                );
                                array_push($inputArray,$inputObject);
                                
                            }

                            $activityObject = array(
                                "financierCode"=> $activity->financiercode,
                                "subVote"=> $activity->costcentrecode,
                                "activityCode"=> $activity->activitycode,
                                "activityName"=> $activity->activitydescription,
                                "inputs" => $inputArray
                            );

                            array_push($activityArray,$activityObject);
                }
                            /** objective Object */
                            $projectActivityObject = array(
                                "uid" => $project_id,
                                "projectName" => $project_name,
                                "projectCode" => $project_code,
                                "projectAmount" => number_format($projectAmount,2,'.',''),
                                "activities" => $activityArray
                            );
                            array_push($projectBudgetArray,$projectActivityObject);

            }

        $company = DB::table('admin_hierarchies')->where('id',$admin_hierarchy_id)->first()->code;
        $financialYear = DB::table('financial_years')->where('id',$financial_year_id)->first()->name;

        $options = array(
            'headers' => array('x-match' => 'all', 'Company' => $company, 'type' => 'Transaction'),
            'exchange' => 'BudgetExchange'
        );

        // $queue_name = ConfigurationSetting::getValueByKey('BUDGET_QUEUE_NAME');
        $queue_name = 'PLANREPOTR_TO_OTRMIS_DATA_QUEUE_OBJECTIVE';

        $control_number = app('App\Http\Controllers\Execution\MuseIntegrationController')->transactionNumber("PROJECTBUDGET",12);

        $messageHeader = array(
            "sender" => "PLANREPOTR",
            "receiver" => $financial_system_code,
            "msgId" => $control_number,
            // "applyDate" => date('Y-m-d'),
            "messageType" => "PROJECTBUDGET",
            "createdAt" => date('Y-m-d H:i:s')
        );
        $messageSummary = array(
            "company" => $company,
            "trxCtrlNum" => $control_number,
            "applyDate"=> date('Y-m-d'),
            "financialYear" => $financialYear,
            "description"=>"PROJECTS, ACTIVITIES AND BUDGET"
        );
        $item = array(
            "messageHeader"  => $messageHeader,
            "messageSummary" => $messageSummary,
            "messageDetails" => $projectBudgetArray
        );

        $payloadToNpmis = array(
            'message' => $item,
            'digitalSignature'=>app('App\Http\Controllers\Execution\NpimsIntegrationController')->signature(json_encode($item))
        );

        if(count($projectBudgetArray)>0){
            $response =  app('App\Http\Controllers\Execution\NpimsIntegrationController')->sendRequestToNpmis($payloadToNpmis);
            //dispatch(new PublishDataToRabbitMQJob($queue_name,$payload, $options,'objective'));

            // if($response != ""){
            //     //update a table with is_sent or is_delivered attributes
            // }
            $feedback = ["successMessage" => count($projectBudgetArray)." ProjectActivities details was sent. You will be notified when its received"];

            $messageDetails = $payloadToNpmis["message"]["messageDetails"];

            if($response == "ACCEPTED"){
                foreach($messageDetails as $detail){
                   $uid = $detail["uid"];
                   DB::table('project_budget_exports')->insert(['project_id' => $uid,'amount'=>$detail["projectAmount"],'admin_hierarchy_id' => $admin_hierarchy_id,'financial_year_id' => $financial_year_id,'system_code'=> $financial_system_code,
                   'is_sent' => true,'response'=>'ACCEPTED','transaction_type'=>'PROJECTBUDGET','created_at'=>date('Y-m-d H:i:s')]);
                }
            }

        }else{
            $feedback = ["successMessage" => "No new project budget to be exported"];
        }
     } else {
            //nothing to export
            $feedback = "No project budget to Export, Please select proper budget type or  contact administrator";
        }
        return response()->json($feedback, 200);



    }

    /** send aggregate budget */
    public function sendAggregateBudget($admin_hierarchy_id, $financial_year_id, $transaction, $budget_type)
    {
        //get company
        $admin_hierarchy = DB::table('admin_hierarchies as c')
            ->join('admin_hierarchies as r', 'c.parent_id', 'r.id')
            ->where('c.id', $admin_hierarchy_id)
            ->select('c.code as council', 'r.code as region')
            ->first();
        $company = $admin_hierarchy->council;
        $vote = $admin_hierarchy->region;
        //get apply date - the last date of the financial year of the execution
        $apply_date = DB::table('financial_years')
            ->where('id', $financial_year_id)
            ->select('end_date')
            ->first()->end_date;
        $apply_date = date("d-m-Y", strtotime($apply_date));

        //credit account
        $control = $this->getControlAccount('000', '0000');
        $credit_account = $control['credit_account'];

        $options = array(
            'headers' => array('x-match' => 'all', 'Company' => $company, 'type' => 'Transaction'),
            'exchange' => 'BudgetExchange'
        );

        $queue_name = ConfigurationSetting::getValueByKey('BUDGET_QUEUE_NAME');
        //get data
        $budget = DB::table('aggregate_facility_budgets')
            ->where('admin_hierarchy_id', $admin_hierarchy_id)
            ->where('financial_year_id', $financial_year_id)
            ->where('is_sent', false)
            ->where('timestamp', $transaction)
            ->select('*');
        //generate budget import items ready to export
        $debit_account = array();
        $credit_amount = 0;
        $message = array();
        if ($budget->count() > 0) {
            foreach ($budget->get() as $data) {
                $description = str_limit('Transfer', 40);
                $item = array('Acc' => $data->chart_of_accounts,
                    'Amount' => $data->amount,
                    'UID' => "$data->id");

                array_push($debit_account, $item);
                $credit_amount += $data->amount;

                $transaction_control_number = $data->timestamp;
            }

            $book = ConfigurationSetting::where('key', 'BUDGET_BOOK_ID')->first()->value;

            //get journal code
            $journal_code = BudgetExportAccount::getJournalCode($budget_type);
            $messageHeader = array(
                "sender" => "PLANREPOTR",
                "receiver" => "MUSE",
                "bookID" => $book,
                "msgId" => $transaction_control_number,
                "applyDate" => $apply_date,
                "messageType" => "APPROVED",
                "createdAt" => date('Y-m-d H:i:s')
            );
            $messageDetails = $debit_account;

            $item = array(
                "messageHeader" => $messageHeader,
                "messageDetails" => $messageDetails
            );

            $data = json_encode($item);
            $plainText = $data;
            $privateKey = openssl_pkey_get_private("file://" . base_path() . "/config/keys/private.pem");
            $publicKey = openssl_pkey_get_public("file://" . base_path() . "/config/keys/public.pem");

            // Make a signature
            openssl_sign($plainText, $signature, $privateKey, OPENSSL_ALGO_SHA256);
            $signature = base64_encode($signature);

            // Verify signature 0 for fail & 1 for success
            //            $res = openssl_verify($plainText,base64_decode($signature),$pubString,OPENSSL_ALGO_SHA256);

            $payloadToMuse = array(
                'message' => $item,
                'digitalSignature'=>$signature
            );
            array_push($message, $payloadToMuse);
            if ($journal_code != null) {
                dispatch(new SendDataToRabbitMQJob($queue_name, $message, $options, 'budget'));
                //update aggregate budget
                DB::table('aggregate_facility_budgets')
                    ->where('admin_hierarchy_id', $admin_hierarchy_id)
                    ->where('financial_year_id', $financial_year_id)
                    ->where('is_sent', false)
                    ->where('timestamp', $transaction)
                    ->update(['is_sent' => true]);

                $response_message = "Budget has been sent to MUSE. Please wait for Notification";
            }
        } else {
            //nothing to export
            $response_message = "No Budget to Export, Please select proper budget type or  contact administrator";
        }
        return $response_message;
    }

    public function sendRevenue($admin_hierarchy_id, $financial_year_id, $transaction_id)
    {
        //get company
        $admin_hierarchy = DB::table('admin_hierarchies as c')
            ->join('admin_hierarchies as r', 'c.parent_id', 'r.id')
            ->where('c.id', $admin_hierarchy_id)
            ->select('c.code as council', 'r.code as region')
            ->first();
        $company = $admin_hierarchy->council;
        $vote = $admin_hierarchy->region;
        //get apply date - the last date of the financial year of the execution
        //$previous_fyear_id = $financial_year_id-1;
        $apply_date = DB::table('financial_years')
            ->where('id', $financial_year_id) //return $financial_year_id after test completion
            ->select('end_date')
            ->first()->end_date;
        $end_date = date("Y-m-d", strtotime($apply_date));
        $end_time = "23:59:59";
        $apply_date = $end_date." ".$end_time;

        //credit account
        $control = $this->getControlAccount('000', '0000');
        $credit_account = $control['credit_account'];

        $options = array(
            'headers' => array('x-match' => 'all', 'Company' => $company, 'type' => 'Transaction'),
            'exchange' => 'BudgetExchange'
        );

        $queue_name = ConfigurationSetting::getValueByKey('BUDGET_QUEUE_NAME');

        /**
         * revenues for own sources and ceilings should be sent to MUSE
         **/
        $financial_system_code = array('MUSE', 'NAVISION');
        //get data
        $budget = DB::table('budget_export_to_financial_systems as b')
            ->join('revenue_export_transaction_items as i', 'b.revenue_export_transaction_item_id', 'i.id')
            ->join('revenue_export_accounts as a', 'i.revenue_export_account_id', 'a.id')
            ->join('revenue_export_transactions as t', 't.id', 'i.revenue_export_transaction_id')
            ->where('b.admin_hierarchy_id', $admin_hierarchy_id)
            ->whereIn('a.financial_system_code', $financial_system_code)
            ->where(function ($q) {
                $q->where('b.is_sent', false)->orWhereNull('b.is_sent');
            })
            ->where('b.amount', '>', 0)
            ->where('b.financial_year_id', $financial_year_id)
            ->where('t.id', $transaction_id)
            ->select('b.*', 't.control_number')
            ->get();
        //generate revenue import items ready to export
        $debit_account = array();
        $credit_amount = 0;
        $message = array();
        if ($budget->count() > 0) {
            foreach ($budget as $data) {
                $description = str_limit($data->description, 40);
                $newGlAccount =  str_replace('-', '|',$data->chart_of_accounts );
                $item = array('acc' => $newGlAccount,
                    'amount' => $data->amount,
                    'uid' => "$data->id");

                array_push($debit_account, $item);
                $credit_amount += $data->amount;

                $transaction_control_number = $data->control_number;
            }

            $crd_acc = array('acc' => $credit_account, 'amount' => "$credit_amount", 'uid' => "1");

            $book = ConfigurationSetting::getValueByKey('REVENUE_BOOK_ID');
            //new export implementation start
            if($company == 'TR196'){
                $receiver = 'NAVISION';
            }else{
                $receiver = 'MUSE';
            }
            $messageHeader = array(
                "sender" => "PLANREPOTR",
                "receiver" => $receiver,
                "msgId" => $transaction_control_number,
                "applyDate" => $apply_date,
                "messageType" => "REVENUEPROJECTION",
                "createdAt" => date('Y-m-d H:i:s')
            );
            $messageSummary = array(
                "company"=> $company,
                "trxCtrlNum" => $transaction_control_number,
                "bookID" => $book,
               // "journalCode"=>$journal_code,
               // "currencyCode"=>"TZS",
                "applyDate"=>$apply_date,
                "description"=>"REVENUE"
            );

            $messageDetails = $debit_account;

            $item = array(
                "messageHeader" => $messageHeader,
                "messageSummary" => $messageSummary,
                "messageDetails" => $messageDetails
            );

            $data = json_encode($item);
            $plainText = $data;
            $privateKey = openssl_pkey_get_private("file://" . base_path() . "/config/keys/private.pem");
            $publicKey = openssl_pkey_get_public("file://" . base_path() . "/config/keys/public.pem");

            // Make a signature
            openssl_sign($plainText, $signature, $privateKey, OPENSSL_ALGO_SHA256);
            $signature = base64_encode($signature);

            $payloadToMuse = array(
                'message' => $item,
                'digitalSignature'=>$signature
            );

            //new export implementation end

            if ($payloadToMuse != null) {
                try {
                    if($company == 'TR196'){
                        $response_message = "Revenue sent to GovESB. Please wait for Notification";
                        $response =  app('App\Http\Controllers\Execution\SocialSecurityIntegrationController')->sendRequestToNavision($payloadToMuse);

                    }else{
                        $response_message = "Revenue sent to MoFP Gateway. Please wait for Notification";
                        $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($payloadToMuse);
                    }
                    if($response == "ACCEPTED"){
                        foreach($messageDetails as $detail){
                        $uid = $detail["uid"];
                        DB::table('budget_export_to_financial_systems')->where('id',$uid)->update(['is_sent' => true]);

                        DB::table('revenue_export_accounts as rea')
                        ->join('revenue_export_transaction_items as reti','rea.id','reti.revenue_export_account_id')
                        ->join('budget_export_to_financial_systems as betfs','reti.id','betfs.revenue_export_transaction_item_id')
                        ->where('betfs.id',$uid)
                        ->update(['rea.is_sent' => true]);
                        }
                   }
                } catch (\Throwable $th) {
                    //throw $th;
                $response_message = 'Revenue Budget has been sent to MUSE. Please wait for Nofication';

                }
            }

           // dispatch(new SendDataToRabbitMQJob($queue_name, $message, $options, 'revenue'));
           // $response_message = 'Revenue Budget has been sent to MUSE. Please wait for Nofication';
        } else {
            //nothing to export
           // $response_message = 'No Revenue Budget to export. Please contact administrator';
            $response_message = 'Revenue Budget has been  re-exported to MUSE.';
        }
        return $response_message;
    }


    public function sendRevenueToFFARS($admin_hierarchy_id, $financial_year_id)
    {
        //get company
        $admin_hierarchy = DB::table('admin_hierarchies as c')
            ->join('admin_hierarchies as r', 'c.parent_id', 'r.id')
            ->where('c.id', $admin_hierarchy_id)
            ->select('c.code as council', 'r.code as region')
            ->first();
        $company = $admin_hierarchy->council;
        $vote = $admin_hierarchy->region;
        //get apply date - the last date of the financial year of the execution
        $apply_date = DB::table('financial_years')
            ->where('id', $financial_year_id)
            ->select('end_date')
            ->first()->end_date;
        $apply_date = date("d-m-Y", strtotime($apply_date));

        //credit account
        $control = $this->getControlAccount('000', '0000');
        $credit_account = $control['credit_account'];

        $options = array(
            'headers' => array('x-match' => 'all', 'type' => 'Revenue'),
            'exchange' => 'FacilityBudgetExchange'
        );

        $queue_name = ConfigurationSetting::getValueByKey('BUDGET_TO_FFARS_QUEUE_NAME');
        $book = ConfigurationSetting::getValueByKey('BUDGET_BOOK_ID');

        /**
         * revenues for individual facilities
         **/
        $financial_system_code = array('FFARS');

        //get wards
        $arr = DB::table('admin_hierarchies')
            ->where('parent_id', $admin_hierarchy_id)
            ->where('is_active', true)
            ->pluck('id')
            ->all();

        $facilities = DB::table('facilities')->whereIn('admin_hierarchy_id', $arr)->get();
        //loop facilities
        $counter = 0; //facility counter
        foreach ($facilities as $fac) {
            //get data
            $budget = DB::table('budget_export_to_financial_systems as b')
                ->join('revenue_export_transaction_items as i', 'b.revenue_export_transaction_item_id', 'i.id')
                ->join('revenue_export_accounts as a', 'i.revenue_export_account_id', 'a.id')
                ->join('revenue_export_transactions as t', 't.id', 'i.revenue_export_transaction_id')
                ->join('admin_hierarchy_ceilings as c', 'c.id', 'a.admin_hierarchy_ceiling_id')
                ->where('b.admin_hierarchy_id', $admin_hierarchy_id)
                ->whereIn('a.financial_system_code', $financial_system_code)
                ->where(function ($q) {
                    $q->where('b.is_sent', false)->orWhereNull('b.is_sent');
                })
                ->where('b.amount', '>', 0)
                ->where('b.financial_year_id', $financial_year_id)
                ->where('c.facility_id', $fac->id)
                ->select('b.*', 't.control_number')
                ->get();

            //generate revenue import items ready to export
            $debit_account = array();
            $credit_amount = 0;
            $message = array();
            if ($budget->count() > 0) {
                foreach ($budget as $data) {
                    $description = str_limit($data->description, 40);
                    $item = array('Acc' => $data->chart_of_accounts,
                        'Amount' => $data->amount,
                        'UID' => "$data->id");

                    array_push($debit_account, $item);
                    $credit_amount += $data->amount;

                    $transaction_control_number = $data->control_number;
                }

                $crd_acc = array('Acc' => $credit_account, 'Amount' => "$credit_amount", 'UID' => "1");


                $item = array('id' => 1,
                    'BookID' => $book,
                    'facility_code' => $fac->facility_code,
                    'ApplyDate' => $apply_date,
                    'DebitAcc' => array($crd_acc),
                    'CreditAcc' => $debit_account
                );

                array_push($message, $item);
                dispatch(new SendDataToRabbitMQJob($queue_name, $message, $options, 'revenue'));
                $counter++;
            }
        }

        if ($counter > 0) {
            $response_message = $counter . ' Facility Revenues has been sent to FFARS. Please wait for Nofication';
        } else {
            $response_message = 'No Revenue Budget to export. Please contact administrator';
        }
        return $response_message;
    }

    /** generate gl company */
    public function generateGLAccount($company, $admin_hierarchy_id, $financial_year_id, $type)
    {
        if ($type == 'EXPENSE') {
            $GLBudget = DB::table('budget_export_accounts  as gl')
                ->join('gfs_codes as gfs', 'gfs.id', 'gl.gfs_code_id')
                ->where('admin_hierarchy_id', $admin_hierarchy_id)
                ->where('financial_year_id', $financial_year_id)
                ->where('financial_system_code', 'MUSE')
                ->where('gl.is_delivered', false)
                ->select('gl.id', 'gl.chart_of_accounts', 'gfs.name', 'gfs.code')
                ->get();
        } else {
            $system_code = array('MUSE', 'LGRCIS');
            $GLBudget = DB::table('revenue_export_accounts  as gl')
                ->join('gfs_codes as gfs', 'gfs.id', 'gl.gfs_code_id')
                ->where('admin_hierarchy_id', $admin_hierarchy_id)
                ->where('financial_year_id', $financial_year_id)
                ->whereIn('financial_system_code', $system_code)
                ->where('gl.is_delivered', false)
                ->select('gl.id', 'gl.chart_of_accounts', 'gfs.name', 'gfs.code')
                ->get();
        }

        foreach ($GLBudget as $item) {
            $this->createGLAccount($item->id, true, false, $company, $type);
            //create for treasury
            $this->createGLAccount($item->id, true, false, '000', $type);
        }
    }

    public function sendGLAccount($admin_hierarchy_id, $financial_year_id, $type, $account_type = null)
    {
        //get company
        $admin_hierarchy = DB::table('admin_hierarchies as c')
            ->join('admin_hierarchies as r', 'c.parent_id', 'r.id')
            ->where('c.id', $admin_hierarchy_id)
            ->select('c.code as council', 'r.code as region')
            ->first();
        $company = $admin_hierarchy->council;
        $vote = $admin_hierarchy->region;
        //set gl accounts
        $this->generateGLAccount($company, $admin_hierarchy_id, $financial_year_id, $type);
        $companies = [$company, '000'];
        $counter = 1;
        foreach ($companies as $item) {
            $queue_name = ConfigurationSetting::getValueByKey('GL_ACCOUNT_NAME');
            $options = array(
                'exchange' => 'GLAccountsExchange'
            );
            //get headers
            if ($item == '000') {
                $book = ConfigurationSetting::getValueByKey('BUDGET_BOOK_ID');
                $options['headers'] = array('x-match' => 'all', 'Company' => $item, 'type' => 'Transaction');
            } else {
                $book = ConfigurationSetting::getValueByKey('MAIN_BOOK_ID');
                $options['headers'] = array('x-match' => 'all', 'Company' => $item, 'type' => 'Transaction');
            }
            $message = array("id" => $counter,
                "Company" => $item,
                "COACode" => $book);
            //get gl accounts
            $GLAccounts = $this->getGLAccountBooks($item, $admin_hierarchy_id, $financial_year_id, $type, $account_type);

            if (count($GLAccounts) > 0) {
                $items = array();
                foreach ($GLAccounts as $data) {
                    //create gl account
                    $segments = explode("-", $data->chart_of_accounts);
                    $gfs_code = $segments[count($segments) - 1];
                    unset($segments[count($segments) - 1]);
                    //append the gfs code to the beginning
                    $glAcc = array_prepend($segments, $gfs_code);
                    //check if segment has been created

                    $GL = implode("|", $glAcc);

                    //check gfs code name
                    $item = array(
                        "GLAccount" => $GL,
                        "GLAcctDispGLAcctDisp" => $data->chart_of_accounts,
                        "GLAcctDispGLShortAcct" => $data->chart_of_accounts,
                        "SegValue1" => $glAcc[0],
                        "SegValue2" => $glAcc[1],
                        "SegValue3" => $glAcc[2],
                        "SegValue4" => $glAcc[3],
                        "SegValue5" => $glAcc[4],
                        "SegValue6" => $glAcc[5],
                        "SegValue7" => $glAcc[6],
                        "SegValue8" => $glAcc[7],
                        "SegValue9" => $glAcc[8],
                        "SegValue10" => $glAcc[9],
                        "SegValue11" => '',
                        "SegValue12" => '',
                        "SegValue13" => '',
                        "SegValue14" => '',
                        "SegValue15" => '',
                        "SegValue16" => '',
                        "SegValue17" => '',
                        "SegValue18" => '',
                        "SegValue19" => '',
                        "SegValue20" => '',
                        "PreservDesc" => false,
                        "PreserveActivation" => false,
                        "Active" => true,
                        "UID" => "$data->id"
                    );
                    array_push($items, $item);

                }
                $message['GLAcctDetails'] = $items;
                //dispatch gl accounts
                dispatch(new SendDataToRabbitMQJob($queue_name, array($message), $options, 'glAcc'));
                $counter++;
            }
        }
        $response_msg = "GL Accounts have been sent to MUSE";
        return $response_msg;
    }

    public function checkAccountSegments($segments, $admin_hierarchy_id)
    {
        $error_message = array();
        //segment 1 - gfs code
        $gfs_code = $segments[0];
        if ($this->segmentExist($segments[0], 1, $admin_hierarchy_id) == false) {
            if (strlen($gfs_code) != 8) {
                $message = "[$gfs_code]-Invalid length of gfs code";
                $error_message[] = $message;
            } else {
                $this->insertSegment('MAIN', 1, $segments[0], 'NA', 'NA', ' ', ' ', $admin_hierarchy_id);
                //check if exist in treasurer
                $this->checkSegmentInTreasurerAccount(1, $segments[0]);
            }
        }

        //segment 2 - vote
        if (strlen($segments[1]) != 3) {
            $message = "[$segments[1]] - Invalid length of vote";
            $error_message[] = $message;
        }

        //segment 3 - sub vote
        if (strlen($segments[2]) != 4) {
            $message = "[$segments[2]] - Invalid length of sub vote";
            $error_message[] = $message;
        }

        //segment 4 - cost center
        if ($this->segmentExist($segments[3], 4, $admin_hierarchy_id) == false) {
            if (strlen($segments[3]) != 4) {
                $message = "[$segments[3]] - Invalid length of cost center";
                $error_message[] = $message;
            } else {
                $this->insertSegment('MAIN', 4, $segments[3], 'NA', 'NA', ' ', ' ', $admin_hierarchy_id);
                //check if exist in treasurer
                $this->checkSegmentInTreasurerAccount(4, $segments[3]);
            }
        }

        //segment 5 - sub-budget-class
        if (strlen($segments[4]) != 3) {
            $message = "[$segments[4]]-Invalid length of sub budget class";
            $error_message[] = $message;
        }

        //segment 6 - service provider - facility
        if ($this->segmentExist($segments[5], 6, $admin_hierarchy_id) == false) {
            if (strlen($segments[5]) != 8) {
                $message = "[$segments[5]] - Invalid length of facility";
                $error_message[] = $message;
            } else {
                $this->insertSegment('MAIN', 6, $segments[5], 'NA', 'NA', ' ', ' ', $admin_hierarchy_id);
                //check if exist in treasurer
                $this->checkSegmentInTreasurerAccount(6, $segments[5]);
            }
        }

        //segment 7 - project
        if ($this->segmentExist($segments[6], 7, $admin_hierarchy_id) == false) {
            if (strlen($segments[6]) != 4) {
                $message = "[$segments[6]] - Invalid length of project code";
                $error_message[] = $message;
            } else {
                $this->insertSegment('MAIN', 7, $segments[6], 'NA', 'NA', ' ', ' ', $admin_hierarchy_id);
                //check if exist in treasurer
                $this->checkSegmentInTreasurerAccount(7, $segments[6]);
            }
        }


        //segment 8 - activities
        if ($this->segmentExist($segments[7], 8, $admin_hierarchy_id) == false) {

            if (strlen($segments[7]) != 8) {
                $message = "[$segments[7]] - Invalid length of activities";
                $error_message[] = $message;
            } else {
                //create new segment
                $this->insertSegment('MAIN', 8, $segments[7], 'NA', 'NA', ' ', ' ', $admin_hierarchy_id);
                //check if exist in treasurer
                $this->checkSegmentInTreasurerAccount(8, $segments[7]);
            }

        }

        //segment 9 - fund types
        if (strlen($segments[8]) != 1) {
            $message = "[$segments[8]] - Invalid length of fund types";
            $error_message[] = $message;
        }

        //segment 10 - Fund sources
        if (strlen($segments[9]) != 3) {
            $message = "[$segments[9]] - Invalid length of fund sources";
            $error_message[] = $message;
        }
        return $error_message;
    }

    public function segmentExist($segment_code, $segment_number, $admin_hierarchy_id)
    {
        $result = DB::table('coa_segments as s')
            ->join('coa_segment_company as c', 'c.coa_segment_id', 's.id')
            ->where('s.segment_number', $segment_number)
            ->where('s.segment_code', $segment_code)
            ->where('c.admin_hierarchy_id', $admin_hierarchy_id)
            ->select('s.id');
        if ($result->count() > 0) {
            return true;
        } else {
            return false;
        }
    }

    //get category for gfs codes
    public function getGfsCategory($gfs_code)
    {
        $category = DB::table('gfs_codes as gfs')
            ->join('account_types as t', 't.id', 'gfs.account_type_id')
            ->where('gfs.code', $gfs_code)
            ->select('t.code')
            ->first();
        $category = isset($category->code) ? $category->code : null;
        switch ($category) {
            case '01':
                return ['category' => 'SALES_REV', 'normal_balance' => 'C'];
                //Return revenue
                break;
            case '02':
                //Return asset
                return ['category' => 'SALES_REV', 'normal_balance' => 'D'];
                break;
            case '03':
                //Return expense
                return ['category' => 'EXPENSES', 'normal_balance' => 'D'];
                break;
            case '04':
                //return liabilities
                return ['category' => 'EQUITY', 'normal_balance' => 'C'];
                break;
            case '05':
                //return net income
                return ['category' => 'EQUITY', 'normal_balance' => 'C'];
                break;
            case '06':
                //return liabilities
                return ['category' => 'EQUITY', 'normal_balance' => 'C'];
                break;
            default:
                //Do nothing
                return null;

        }
    }

    public function insertSegment($coa_code, $segment_number, $segment_code, $segment_name, $segment_description, $normal_balance, $category, $admin_hierarchy_id)
    {
        //get category for gfs codes
        if ($segment_number == 1) {
            $result = $this->getGfsCategory($segment_code);
            $category = $result['category'];
            $normal_balance = $result['normal_balance'];
        }
        //insert segment
        $id = DB::table('coa_segments')
            ->insertGetId(['coa_code' => $coa_code, 'segment_number' => $segment_number, 'segment_code' => $segment_code, 'segment_name' => $segment_name,
                'segment_description' => $segment_description, 'normal_balance' => $normal_balance, 'category' => $category, 'is_active' => true]);

        //insert company
        DB::table('coa_segment_company')
            ->insert(['coa_segment_id' => $id, 'admin_hierarchy_id' => $admin_hierarchy_id]);

    }

    public function checkSegmentInTreasurerAccount($segment_number, $segment_code)
    {
        //check if exist in treasurer
        $id = DB::table('admin_hierarchies')->where('code', '056')->select('id')->first();
        if (isset($id->id)) {
            $exist = $this->segmentExist($segment_code, $segment_number, $id->id);
            if (!$exist) {
                $book = ConfigurationSetting::where('key', 'BUDGET_BOOK_ID')->first()->value;
                //insert segment
                $this->insertSegment($book, $segment_number, $segment_code, 'NA', 'NA', '', '', $id->id);
            }
        }
    }

    public function controlAccExist($vote, $company)
    {
        //get budget control account
        $result = $this->getControlAccount($vote, $company);
        $credit_account = $result['credit_account'];
        $glAcc = $result['glAcc'];
        //check if exist
        $result = DB::table('budget_control_accounts')
            ->where('code', $credit_account)
            ->where('is_sent', false)
            ->select('id')
            ->get()->count();
        if ($result == 0) {
            $book = ConfigurationSetting::getValueByKey('BUDGET_BOOK_ID');
            $control_acc = array(
                "id" => 1,
                "Company" => '000',
                "COACode" => $book,
                "GLAccount" => $glAcc,
                "AccountDesc" => 'Budget Control',
                "GLAcctDispGLAcctDisp" => $credit_account,
                "GLAcctDispGLShortAcct" => $credit_account,
                "GLAcctDispAccountDesc" => 'Budget Control',
                "SegValue1" => '99999993',
                "SegValue2" => $vote,
                "SegValue3" => $company,
                "SegValue4" => '0000',
                "SegValue5" => '000',
                "SegValue6" => '00000000',
                "SegValue7" => '0000',
                "SegValue8" => '00000000',
                "SegValue9" => '0',
                "SegValue10" => '000',
                "SegValue11" => '',
                "SegValue12" => '',
                "SegValue13" => '',
                "SegValue14" => '',
                "SegValue15" => '',
                "SegValue16" => '',
                "SegValue17" => '',
                "SegValue18" => '',
                "SegValue19" => '',
                "SegValue20" => '',
                "PreservDesc" => false,
                "PreserveActivation" => false,
                "Active" => true
            );
            return $control_acc;
        } else {
            return 'Exist';
        }
    }

    public function sendCeilings($admin_hierarchy_id, $financial_system_code, $type, $publish)
    {

        $is_facility = $financial_system_code == 'FFARS' ? true : false;
        $financial_year = DB::table('financial_years')->where('status', 1)->first();
        $admin_hierarchy = DB::table('admin_hierarchies')->find($admin_hierarchy_id);
        $projection = false;
        if ($type == "Projection") {
            $projection = true;
            $ceilings = DB::table('admin_hierarchy_ceilings as a')->
            join('ceilings as c', 'c.id', 'a.ceiling_id')->
            join('gfs_codes as g', 'g.id', 'c.gfs_code_id')->
            join('fund_sources as f', 'f.id', 'g.fund_source_id')->
            join('admin_hierarchy_ceiling_periods as p', 'p.admin_hierarchy_ceiling_id', '=', 'a.id')->
            join('periods as r', 'r.id', '=', 'p.period_id')->
            join('sections as s', 's.id', '=', 'a.section_id')->
            join('section_levels as l', 'l.id', '=', 's.section_level_id')->
            where('a.financial_year_id', $financial_year->id)->
            where('a.admin_hierarchy_id', $admin_hierarchy_id)->
            where('a.is_facility', $is_facility)->
            where('f.can_project', true)->
            select('a.id as UID', 'g.code as gfs_code', 'p.amount as amount', 'r.name as period')->get();
        } else {
            $ceilings = DB::table('admin_hierarchy_ceilings as a')->
            join('ceilings as c', 'c.id', 'a.ceiling_id')->
            join('gfs_codes as g', 'g.id', 'c.gfs_code_id')->
            join('fund_sources as f', 'f.id', 'g.fund_source_id')->
            join('sections as s', 's.id', '=', 'a.section_id')->
            join('section_levels as l', 'l.id', '=', 's.section_level_id')->
            where('a.financial_year_id', $financial_year->id)->
            where('a.admin_hierarchy_id', $admin_hierarchy_id)->
            where('a.is_facility', $is_facility)->
            where('f.can_project', false)->
            get(['a.id as UID', 'g.code as gfs_code', DB::raw("'annual' as period"), 'a.amount as amount']);

        }

        $data = ['company' => $admin_hierarchy->code, 'financial_system_code' => $financial_system_code, 'financial_year' => $financial_year->name, 'message' => $ceilings];
        $queue_name = $financial_system_code;
        $options = array(
            'headers' => array('x-match' => 'all', 'Company' => $admin_hierarchy->code, 'type' => 'Transaction'),
            'exchange' => $financial_system_code . $type . 'Exchange', 'queue_name' => $financial_system_code,
            'exchange_type' => 'headers',
            'routing_key' => ''
        );

        if ($publish) {
            sendToMUSE($data, $options);
        }

        dispatch(new UpdateCeilingExportResponse($data, $publish));

    }

    public function generateSegments($admin_hierarchy_id, $financial_year_id, $budget_type)
    {
        //dispatch segments
        $this->sendGLAccountSegments($admin_hierarchy_id);
        //dispatch segments for treasurer
        $id = DB::table('admin_hierarchies')->where('code', '056')->select('id')->first();
        if (isset($id->id)) {
            $this->sendGLAccountSegments($id->id);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * *this function returns all segments which are not sent to MUSE only
     */
    public function coaSegmentExist($admin_hierarchy_id, $financial_year_id, $budget_type, $account_type = null)
    {
        //get gl accounts for budget
        if ($account_type == 'facility') {
            $GLAccounts = DB::table('aggregate_facility_budgets  as ag')
                ->where('ag.admin_hierarchy_id', $admin_hierarchy_id)
                ->where('ag.financial_year_id', $financial_year_id)
                ->where('ag.is_sent', false)
                ->select('ag.chart_of_accounts')
                ->groupBy('ag.chart_of_accounts')
                ->get();
        } else {
            $GLAccounts = DB::table('budget_export_accounts  as gl')
                ->join('gfs_codes as gfs', 'gfs.id', 'gl.gfs_code_id')
                ->join('activities as ac', 'ac.id', 'gl.activity_id')
                ->where('gl.admin_hierarchy_id', $admin_hierarchy_id)
                ->where('gl.financial_year_id', $financial_year_id)
                ->where('gl.financial_system_code', 'MUSE')
                ->where('ac.budget_type', $budget_type)
                ->where('gl.is_delivered', false)
                ->select('gl.chart_of_accounts', 'gfs.name', 'gfs.code')
                ->groupBy('gl.chart_of_accounts', 'gfs.name', 'gfs.code')
                ->get();

        }

        if (count($GLAccounts) > 0) {
            foreach ($GLAccounts as $data) {
                //create gl account
                $segments = explode("-", $data->chart_of_accounts);
                $gfs_code = $segments[count($segments) - 1];
                unset($segments[count($segments) - 1]);
                //append the gfs code to the beginning
                $glAcc = array_prepend($segments, $gfs_code);
                $error_message = $this->checkAccountSegments($glAcc, $admin_hierarchy_id);
                if (count($error_message) > 0) {
                    array_push($this->failed_transaction, $error_message);
                }
            }
        }

        $data = ['error' => $this->failed_transaction];
        return $data;
    }

    /**
     * @param Request $request
     * @return  JsonResponse
     * this function check if all gl accounts has been sent to MUSE only
     */
    public function glAccountExist($admin_hierarchy_id, $financial_year_id)
    {
        //get gl accounts for budget
        $GLAccounts = DB::table('budget_export_accounts  as gl')
            ->join('company_gl_accounts as l', 'l.budget_export_account_id', 'gl.id')
            ->where('gl.admin_hierarchy_id', $admin_hierarchy_id)
            ->where('gl.financial_year_id', $financial_year_id)
            ->where('gl.financial_system_code', 'MUSE')
            ->where('l.is_delivered', false)
            ->select('gl.chart_of_accounts')
            ->get();
        $gl = array();
        foreach ($GLAccounts as $data) {
            array_push($gl, $data->chart_of_accounts);
        }
        return $gl;
    }

    public static function getControlAccount($vote, $company)
    {
        $budget_control_code = ConfigurationSetting::where('key', 'BUDGET_CONTROL_CODE')->first()->value;
        $array = explode('-', $budget_control_code);
        unset($array[0]);
        unset($array[1]);
        array_unshift($array, $company);
        array_unshift($array, $vote);
        $credit_account = implode('-', $array);
        $last = $array[count($array) - 1];
        unset($array[$last]);
        array_prepend($array, $last);
        $glAcc = implode('|', $array);
        $result = array('credit_account' => $credit_account, 'glAcc' => $glAcc);
        return $result;
    }

    public function sendReallocation($data)
    {
        $admin_hierarchy_id = $data->admin_hierarchy_id;
        $financial_year_id = $data->financial_year_id;
        $budget_type = $data->budget_type;
        $system_code = $data->financial_system_code;
        $data_error = array();


        $result = DB::table('budget_export_to_financial_systems as e')
                            ->join('budget_export_transaction_items as t','e.budget_export_transaction_item_id','t.id')
                            ->join('budget_reallocation_items as i','i.id','t.budget_reallocation_item_id')
                            ->join('budget_reallocations as r','i.budget_reallocation_id','r.id')
                            ->join('budget_export_accounts as acf', 'acf.id','i.budget_export_account_id')
                            ->join('activities as ac', 'ac.id', 'acf.activity_id')
                            ->join('budget_export_accounts as act', 'act.id','i.budget_export_to_account_id')
                            ->leftJoin('budget_reallocation_responses as rl','rl.budget_reallocation_item_id','i.id')
                            ->join('gfs_codes as gff','acf.gfs_code_id','gff.id')
                            ->join('gfs_codes as gft','act.gfs_code_id','gft.id')
                            ->where('acf.admin_hierarchy_id',$admin_hierarchy_id)
                            ->where('acf.financial_year_id',$financial_year_id)
                            ->where('ac.budget_type', $budget_type)
//                            ->where('e.queue_name', strtolower($system_code))
                            ->where('i.is_approved',true)
                            ->where('e.is_sent',false)
                            ->select('i.id','act.financial_system_code as toFs','acf.financial_system_code as FromFs',
                                'e.id as export_transaction_id','r.reference_code','i.from_chart_of_accounts',
                                'i.to_chart_of_accounts','acf.id as from_account','act.id as to_account','rl.is_approved as rejected',
                                     'i.amount','i.is_approved',DB::raw("to_char(i.created_at,'DD-MM-YYYY') as date"),
                                'gff.description as from_gfs','gft.description as to_gfs','rl.response as comments')
                             ->get();

        $accounts = array();
        $messageToMUSE = array();
        $messageToFfars = array();
         //check if there is data
        if (count($result) > 0) {

            //get company
            $admin_hierarchy = DB::table('admin_hierarchies as c')
                ->join('admin_hierarchies as r', 'c.parent_id', 'r.id')
                ->join('admin_hierarchies as r2', 'r.parent_id', 'r2.id')
                ->where('c.id', $admin_hierarchy_id)
                ->select('c.code as council', 'r.code as region','r2.code as vote')
                ->first();

            $company = $admin_hierarchy->council;

            //get apply date - the last date of the financial year of the execution
            $apply_date = DB::table('financial_years')
            ->where('id', $financial_year_id) //return $financial_year_id after test completion
            ->select('end_date')
            ->first()->end_date;
            $end_date = date("Y-m-d", strtotime($apply_date));
            $end_time = "23:59:59";
            $apply_date = $end_date." ".$end_time;



            $book = ConfigurationSetting::getValueByKey('BUDGET_BOOK_ID');

            // $MUSEOptions = array(
            //     'headers' => array('x-match' => 'all', 'Company' => $company, 'type' => 'Transaction'),
            //     'exchange' => 'BudgetExchange'
            // );
            // $MUSEQueueName = 'PLANREPOTR_TO_MUSE_BUDGET_REALLOCATION_QUEUE';

            // $ffarsOptions = array(
            //     'headers' => array('x-match' => 'all', 'type' => 'REALLOCATION_BUDGET'),
            //     'exchange' => 'ReallocationBudgetExchange'
            // );
            // $ffarsQueueName = 'PLANREPOTR_TO_FFARS_BUDGET_REALLOCATION_QUEUE';

            foreach ($result as $item) {

                $transaction_control_number = 'REALLOCATION_' . $item->id;
                //check if exist
                // $result = DB::table('budget_control_accounts')
                //     ->where('code', $item->from_chart_of_accounts)
                //     ->orWhere('code', $item->to_chart_of_accounts)
                //     ->where('is_sent', false)
                //     ->select('id')
                //     ->get()->count();

                // if ($result == 0) {
                    //if ($system_code == 'MUSE') {


                        //check if reallocation is to ffars
                        // $arr = explode('-', $item->to_chart_of_accounts);
                        // if ($arr[5] != '00000000') {
                        //     $transfer_code = $this->getTransferCode($arr[3]);
                        //     $arr[9] = $transfer_code;
                        //     $arr[5] = '00000000';
                        //     $arr[7] = '0000';
                        //     $arr[10] = '000000';
                        //     $to_chart_of_account = implode('-', $arr);
                        //     //change budget type to supplementary
                        //     $this->changetoAdditionalBudget($item->to_account);
                        // } else {
                        //     $to_chart_of_account = $item->to_chart_of_accounts;
                        // }

                        // $crd_acc = array(
                        //     'glAccount' => $item->from_chart_of_accounts,
                        //     'amount' => "$item->amount",
                        //     'uid' => "$item->export_transaction_id"
                        // );

                        // $dbt_acc = array('glAccount' => $to_chart_of_account,
                        //     'amount' => $item->amount,
                        //     'uid' => "$item->export_transaction_id");

                        $from_chart_of_accounts =  str_replace('-', '|',$item->from_chart_of_accounts );
                        $to_chart_of_accounts =  str_replace('-', '|',$item->to_chart_of_accounts );
                        $MUSE_input_data = array(
                            'fromGlAccount' => $from_chart_of_accounts,
                            'toGlAccount' => $to_chart_of_accounts,
                            'amount' => "$item->amount",
                            'uid' => "$item->export_transaction_id"
                        );

                        $journalCode = $this->reallocJornalCode($item->from_chart_of_accounts, $item->to_chart_of_accounts);

                        // $MUSE_input_data = array(
                        //     'reallocationFrom' => array($dbt_acc),
                        //     'reallocationTo' => array($crd_acc));

                            array_push($accounts,$MUSE_input_data);

                        //}
                    }

//new export implementation start
                        $messageHeader = array(
                            "sender" => "PLANREPOTR",
                            "receiver" => "MUSE",
                            //'bookID' => $book,
                            "msgId" => $transaction_control_number,
                            "applyDate" => $apply_date,
                            "messageType" => "REALLOCATION",
                            "createdAt" => date('Y-m-d H:i:s')
                        );

                        $messageSummary = array(
                            "company"=> $company,
                            "trxCtrlNum" => $transaction_control_number,
                            "applyDate"=> $apply_date,
                            "description"=>"BUDGET REALLOCATION"
                        );

                        $messageDetails = $accounts;

                        $item = array(
                            "messageHeader" => $messageHeader,
                            "messageSummary" => $messageSummary,
                            "messageDetails" => $messageDetails
                        );

                        $data = json_encode($item);
                        $plainText = $data;
                        $privateKey = openssl_pkey_get_private("file://" . base_path() . "/config/keys/private.pem");
                        $publicKey = openssl_pkey_get_public("file://" . base_path() . "/config/keys/public.pem");

                        // Make a signature
                        openssl_sign($plainText, $signature, $privateKey, OPENSSL_ALGO_SHA256);
                        $signature = base64_encode($signature);

                        // Verify signature 0 for fail & 1 for success
                        //            $res = openssl_verify($plainText,base64_decode($signature),$pubString,OPENSSL_ALGO_SHA256);

                        $payloadToMuse = array(
                            'message' => $item,
                            'digitalSignature'=>$signature
                        );
//new export implementation end
                        array_push($messageToMUSE, $payloadToMuse);

                    if (sizeof($messageToMUSE) > 0) {
                       // dispatch(new SendDataToRabbitMQJob($MUSEQueueName, $messageToMUSE, $MUSEOptions, 'budget'));
                       $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($payloadToMuse);
                       if($response == "ACCEPTED"){
                        foreach($messageDetails as $detail){
                           $uid = $detail["uid"];
                           DB::table('budget_export_to_financial_systems')->where('id',$uid)->update(['is_sent' => true]);
                        }
                       // DB::table('budget_export_transactions')->where('id', $transaction_id)->update(['is_sent' => true]);
                    }
                        unset($messageToMUSE);
                    }
                    if (sizeof($messageToFfars) > 0) {
                        dispatch(new SendDataToRabbitMQJob($ffarsQueueName, $messageToFfars, $ffarsOptions, 'budget'));
                        unset($messageToFfars);
                    }
                }// else {
                //     array_push($data_error, $data);
                // }




        //return msg
        if (count($data_error) > 0) {
            $error = ['errorMessage' => 'FAILED_TO_EXPORT_REALLOCATION', 'items' => $data_error];
            return $error;

        } else {
            $data = ['successMessage' => 'REALLOCATION_EXPORTED_SUCCESSFULLY'];
            return $data;
        }

    }

    //get journal code for budget reallocation
    public function reallocJornalCode($from, $to)
    {
        $from_budget_class = explode('-', $from);
        $to_budget_class = explode('-', $to);
        if ($from_budget_class[3] == $to_budget_class[3]) {
            return 'BR';
        } else {
            return 'BV';
        }
    }

    public static function sendSetupSegments($data, $type)
    {
        $queueName = ConfigurationSetting::getValueByKey('COA_SEGMENT_FFARS_QUEUE_NAME');
        $options = array(
            'headers' => array('x-match' => 'all', 'type' => $type),
            'exchange' => 'FFARSSegmentExchange'
        );
        //get current user
        $user_id = UserServices::getUser()->id;
        $users = User::where('id', $user_id)->get();
        try {
            dispatch((new SendDataToRabbitMQJob($queueName, $data, $options, 'updateFFARSSegments'))->delay(10));
        } catch (\Exception $e) {
            //get exception message, send notification
            $error_msg = $e->getMessage();
            $url = url('/notifications#!/');
            $object_id = 0;
            $job = new sendNotifications($users, $object_id, $url, $error_msg, "fa fa-book");
            dispatch($job->delay(10));

        }
    }

    public function setupSegmentExist($admin_hierarchy_id, $financial_year_id)
    {
        //get gl accounts for budget
        $GLBudget = DB::table('budget_export_accounts  as gl')
            ->join('gfs_codes as gfs', 'gfs.id', 'gl.gfs_code_id')
            ->where('admin_hierarchy_id', $admin_hierarchy_id)
            ->where('financial_year_id', $financial_year_id)
            ->where('financial_system_code', 'FFARS')
            ->where(function ($q) {
                $q->where('gl.is_sent', false)->orWhereNull('gl.is_sent');
            })
            ->select('gl.id', 'gl.chart_of_accounts', 'gfs.name', 'gfs.code');
        //get gl accounts for revenue
        $GLRevenue = DB::table('revenue_export_accounts  as gl')
            ->join('gfs_codes as gfs', 'gfs.id', 'gl.gfs_code_id')
            ->where('admin_hierarchy_id', $admin_hierarchy_id)
            ->where('financial_year_id', $financial_year_id)
            ->where(function ($q) {
                $q->where('gl.is_sent', false)->orWhereNull('gl.is_sent');
            })
            ->where('financial_system_code', 'FFARS')
            ->select('gl.id', 'gl.chart_of_accounts', 'gfs.name', 'gfs.code');

        $GLAccounts = array();
        if ($GLBudget->count() > 0 && $GLRevenue->count() > 0) {
            $GLBudget = $GLBudget->get();
            $GLRevenue = $GLRevenue->get();
            $GLAccounts = array_merge($GLBudget->toArray(), $GLRevenue->toArray());
        } else if ($GLBudget->count() > 0) {
            $GLBudget = $GLBudget->get();
            $GLAccounts = $GLBudget->toArray();
        } else if ($GLRevenue->count() > 0) {
            $GLRevenue = $GLRevenue->get();
            $GLAccounts = $GLRevenue->toArray();
        }

        if (count($GLAccounts) > 0) {
            foreach ($GLAccounts as $data) {
                //create gl account
                $segments = explode("-", $data->chart_of_accounts);
                unset($segments[count($segments) - 1]);
                //append the gfs code to the beginning
                $glAcc = array_prepend($segments, $data->code);
                $error_message = $this->checkSetupSegments($glAcc);
                if (count($error_message) > 0) {
                    array_push($this->failed_transaction, $error_message);
                }
            }
        }
        $data = ['error' => $this->failed_transaction];
        return $data;
    }

    public function checkSetupSegments($segments)
    {
        $error_message = array();
        //segment 1 - gfs code
        $gfs_code = $segments[0];
        if (strlen($gfs_code) != 8) {
            $message = "[$gfs_code]-Invalid length of gfs code";
            $error_message[] = $message;
        } else if (strlen($gfs_code) == 8) {
            //check if it is exported
            $num = DB::table('gfs_codes')->where('code', $gfs_code)->where('exported_to_ffars', true)->select('id')->count();
            if ($num == 0) {
                $message = "[$gfs_code] - Not sent to FFARS";
                $error_message[] = $message;
            }
        }

        //segment 2 - vote
        if (strlen($segments[1]) != 3) {
            $message = "[$segments[1]] - Invalid length of vote";
            $error_message[] = $message;
        }

        //segment 3 - sub vote
        if (strlen($segments[2]) != 4) {
            $message = "[$segments[2]] - Invalid length of sub vote";
            $error_message[] = $message;
        }

        //segment 4 - cost center
        if (strlen($segments[3]) != 4) {
            $message = "[$segments[3]] - Invalid length of cost center";
            $error_message[] = $message;
        }

        //segment 5 - sub-budget-class
        if (strlen($segments[4]) != 3) {
            $message = "[$segments[4]]-Invalid length of sub budget class";
            $error_message[] = $message;
        } elseif (strlen($segments[4]) == 3) {
            $num = DB::table('budget_classes')->where('code', $segments[4])->where('exported_to_ffars', true)->select('id')->count();
            if ($num == 0) {
                $message = "[$segments[4]]- Sub budget class not sent to FFARS";
                $error_message[] = $message;
            }
        }

        //segment 6 - service provider - facility

        if (strlen($segments[5]) != 8) {
            $message = "[$segments[5]] - Invalid length of facility";
            $error_message[] = $message;
        } else if (strlen($segments[5]) != 8) {
            $num = DB::table('facilities')->where('code', $segments[4])->where('exported_to_ffars', true)->select('id')->count();
            if ($num == 0) {
                $message = "[$segments[5]] - Facility not sent to FFARS";
                $error_message[] = $message;
            }
        }


        //segment 7 - project
        if (strlen($segments[6]) != 4) {
            $message = "[$segments[6]] - Invalid length of project code";
            $error_message[] = $message;
        } else if (strlen($segments[6]) == 4) {
            $num = DB::table('projects')->where('code', $segments[4])->where('exported_to_ffars', true)->select('id')->count();
            if ($num == 0) {
                $message = "[$segments[6]] - Project not sent to FFARS";
                $error_message[] = $message;
            }
        }

        /**
         * activities
         */
        if (strlen($segments[7]) != 8) {
            $message = "[$segments[7]] - Invalid length of activities";
            $error_message[] = $message;
        } else if (strlen($segments[7]) == 8) {

            $num = DB::table('ffars_activities')->where('activity_code', $segments[4])->where('exported_to_ffars', true)->select('id')->count();
            if ($num == 0) {
                $message = "[$segments[7]] - Activities not sent to FFARS";
                $error_message[] = $message;
            }

        }


        //segment 9 - fund types
        if (strlen($segments[8]) != 1) {
            $message = "[$segments[8]] - Invalid length of fund types";
            $error_message[] = $message;
        } else if (strlen($segments[8]) == 1) {
            $num = DB::table('fund_types')->where('current_budget_code', $segments[4])->where('exported_to_ffars', true)->select('id')->count();
            if ($num == 0) {
                $message = "[$segments[8]] - Fund types not sent to FFARS";
                $error_message[] = $message;
            }

        }

        //segment 10 - Fund sources
        if (strlen($segments[9]) != 3) {
            $message = "[$segments[9]] - Invalid length of fund sources";
            $error_message[] = $message;
        } else if (strlen($segments[9]) == 3) {
            $num = DB::table('fund_sources')->where('code', $segments[4])->where('exported_to_ffars', true)->select('id')->count();
            if ($num == 0) {
                $message = "[$segments[9]] - Fund sources not sent to FFARS";
                $error_message[] = $message;
            }
        }
        unset($error_message);
        $error_message = array();
        return $error_message;

    }

    public function sendFacilityBudget($admin_hierarchy_id, $financial_year_id, $financial_system_code, $transaction_id, $budget_type)
    {
        //get company
        $admin_hierarchy = DB::table('admin_hierarchies as c')
            ->join('admin_hierarchies as r', 'c.parent_id', 'r.id')
            ->where('c.id', $admin_hierarchy_id)
            ->select('c.code as council', 'r.code as region')
            ->first();

        $company = $admin_hierarchy->council;
        $vote = $admin_hierarchy->region;

        //get apply date - the last date of the financial year of the execution
        $apply_date = DB::table('financial_years')
            ->where('id', $financial_year_id)
            ->select('end_date')
            ->first()->end_date;
        $apply_date = date("d-m-Y", strtotime($apply_date));

        //credit account
        $control = $this->getControlAccount('000', '0000');
        $credit_account = $control['credit_account'];

        $options = array(
            'headers' => array('x-match' => 'all', 'type' => 'BUDGET'),
            'exchange' => 'FacilityBudgetExchange'
        );

        $queue_name = ConfigurationSetting::getValueByKey('BUDGET_TO_FFARS_QUEUE_NAME');

        $book = ConfigurationSetting::getValueByKey('BUDGET_BOOK_ID');
        //get wards
        $arr = DB::table('admin_hierarchies')
            ->where('parent_id', $admin_hierarchy_id)
            ->where('is_active', true)
            ->pluck('id')
            ->all();

        //get facilities with activities and budget
        $facilities = DB::table('facilities as f')
            ->join('activity_facilities as af', 'af.facility_id', 'f.id')
            ->join('activities as ac', 'ac.id', 'af.activity_id')
            ->join('budget_export_accounts as a', 'a.activity_id', 'ac.id')
            ->join('budget_export_transaction_items as it', 'it.budget_export_account_id', 'a.id')
            ->where('f.is_active', true)
            ->whereIn('f.admin_hierarchy_id', $arr)
            ->where('it.budget_export_transaction_id', $transaction_id)
            ->select('f.*')
            ->get();

        //loop facilities
        $counter = 0; //facility counter
        foreach ($facilities as $fac) {
            //check if all activities has been sent to ffars
            if ($this->isDelivered($fac->id, $financial_year_id)) {
                //get data
                $budget = DB::table('budget_export_to_financial_systems as b')
                    ->join('budget_export_transaction_items as i', 'b.budget_export_transaction_item_id', 'i.id')
                    ->join('budget_export_accounts as a', 'i.budget_export_account_id', 'a.id')
                    ->join('budget_export_transactions as t', 't.id', 'i.budget_export_transaction_id')
                    ->join('activity_facility_fund_source_inputs as affi', 'affi.id', 'a.activity_facility_fund_source_input_id')
                    ->join('activity_facility_fund_sources as aff', 'aff.id', 'affi.activity_facility_fund_source_id')
                    ->join('activity_facilities as af', 'af.id', 'aff.activity_facility_id')
                    ->join('activities as ac', 'ac.id', 'a.activity_id')
                    ->join('facilities as f', 'f.id', 'af.facility_id')
                    ->where('b.admin_hierarchy_id', $admin_hierarchy_id)
                    ->where('b.financial_year_id', $financial_year_id)
                    ->where('a.financial_system_code', $financial_system_code)
                    ->where('ac.budget_type', $budget_type)
                    ->where('t.id', $transaction_id)
                    ->where(function ($q) {
                        $q->where('b.is_sent', false)->orWhereNull('b.is_sent');
                    })
                    ->where('i.budget_reallocation_item_id', null)
                    ->where('i.is_credit', true)
                    ->where('f.id', $fac->id)
                    ->select('b.id', 'b.chart_of_accounts', 'b.amount', 'b.description', 't.control_number', 'f.facility_code');

                //generate budget import items ready to export
                $debit_account = array();
                $credit_amount = 0;
                $message = array();
                if ($budget->count() > 0) {
                    $journal_code = BudgetExportAccount::getJournalCode($budget_type);
                    foreach ($budget->get() as $data) {
                        $item = array('Acc' => $data->chart_of_accounts,
                            'Amount' => $data->amount,
                            'UID' => "$data->id");

                        array_push($debit_account, $item);
                        $credit_amount += $data->amount;
                    }

                    $crd_acc = array('Acc' => $credit_account, 'Amount' => "$credit_amount", 'UID' => "1");
                    $item = array('BookID' => $book,
                        'FacilityCode' => $fac->facility_code,
                        'JournalCode' => $journal_code,
                        'ApplyDate' => $apply_date,
                        'DebitAcc' => $debit_account,
                        'CreditAcc' => array($crd_acc));
                    array_push($message, $item);
                    dispatch(new SendDataToRabbitMQJob($queue_name, $message, $options, 'budget'));
                    $counter++;
                } else {
                    $response_message = "No Budget to FFARS, Please contact administrator";
                }
            } else {
                $response_message = "Some of activities were not delivered to FFARS";
            }
        }

        if ($counter > 0) {
            $response_message = $counter . " Facility Budget has been sent to FFARS. Please wait for Notification";
            DB::table('budget_export_transactions')->where('id', $transaction_id)->update(['is_sent' => true]);
        }
        return $response_message;
    }

    /** check if all activities has been sent to ffars */
    public function isDelivered($facility_id, $financial_year_id)
    {
        $facility = DB::table('facilities')->where('id', $facility_id)->select('facility_code')->first();
        $count = DB::table('ffars_activities')
            ->where('is_delivered_to_ffars', false)
            ->where('facility_code', $facility->facility_code)
            ->where('financial_year_id', $financial_year_id)
            ->select('id')->count();
        if ($count > 0) {
            return false;
        } else {
            return true;
        }
    }

    /** resend ffars budget **/
    public function resendFFARSBudget()
    {
        //financial system code
        $financial_system_code = 'FFARS';
        //get current financial year
        $financial_year = FinancialYearServices::getPlanningFinancialYear();
        $financial_year_id = $financial_year->id;
        //get apply date - the last date of the financial year of the execution
        $apply_date = DB::table('financial_years')
            ->where('id', $financial_year_id)
            ->select('end_date')
            ->first()->end_date;
        $apply_date = date("d-m-Y", strtotime($apply_date));

        //credit account
        $control = $this->getControlAccount('000', '0000');
        $credit_account = $control['credit_account'];

        $options = array(
            'headers' => array('x-match' => 'all', 'type' => 'BUDGET'),
            'exchange' => 'FacilityBudgetExchange'
        );

        $queue_name = ConfigurationSetting::getValueByKey('BUDGET_TO_FFARS_QUEUE_NAME');

        $book = ConfigurationSetting::getValueByKey('BUDGET_BOOK_ID');
        //get facilities with activities and budget
        $facilities = DB::table('facility_budget_export_responses as e')
            ->join('facilities as f', 'f.id', 'e.facility_id')
            ->where('e.is_delivered', false)
            ->select('f.id', 'f.facility_code')
            ->get();
        //loop facilities
        $counter = 0; //facility counter
        foreach ($facilities as $fac) {
            //get data
            $budget = DB::table('budget_export_to_financial_systems as b')
                ->join('budget_export_transaction_items as i', 'b.budget_export_transaction_item_id', 'i.id')
                ->join('budget_export_accounts as a', 'i.budget_export_account_id', 'a.id')
                ->join('budget_export_transactions as t', 't.id', 'i.budget_export_transaction_id')
                ->join('activity_facility_fund_source_inputs as affi', 'affi.id', 'a.activity_facility_fund_source_input_id')
                ->join('activity_facility_fund_sources as aff', 'aff.id', 'affi.activity_facility_fund_source_id')
                ->join('activity_facilities as af', 'af.id', 'aff.activity_facility_id')
                ->join('facilities as f', 'f.id', 'af.facility_id')
                ->where('b.financial_year_id', $financial_year_id)
                ->where('a.financial_system_code', $financial_system_code)
                ->where('b.is_sent', true)
                ->where('i.budget_reallocation_item_id', null)
                ->where('f.id', $fac->id)
                ->where('b.amount', '>', 0)
                ->select('b.id', 'b.chart_of_accounts', 'b.amount', 'b.description', 't.control_number', 'f.facility_code');

            //generate budget import items ready to export
            $debit_account = array();
            $credit_amount = 0;
            $message = array();
            if ($budget->count() > 0) {
                foreach ($budget->get() as $data) {
                    $item = array('Acc' => $data->chart_of_accounts,
                        'Amount' => $data->amount,
                        'UID' => "$data->id");

                    array_push($debit_account, $item);
                    $credit_amount += $data->amount;
                }

                $crd_acc = array('Acc' => $credit_account, 'Amount' => "$credit_amount", 'UID' => "1");
                $item = array('BookID' => $book,
                    'FacilityCode' => $fac->facility_code,
                    'JournalCode' => 'BA',
                    'ApplyDate' => $apply_date,
                    'DebitAcc' => $debit_account,
                    'CreditAcc' => array($crd_acc));
                array_push($message, $item);
                dispatch(new SendDataToRabbitMQJob($queue_name, $message, $options, 'budget'));

                $counter++;
            }
        }

        if ($counter > 0) {
            $response_message = $counter . " Facility Budget has been sent to FFARS. Please wait for Notification";
        } else {
            $response_message = "No Budget to FFARS, Please contant administrator";
        }
        return $response_message;
    }

    /** send transaction reversal */
    public function reverseBudget($transaction_id)
    {
        //financial system code
        $financial_system_code = 'FFARS';
        //get current financial year
        $financial_year = FinancialYearServices::getPlanningFinancialYear();
        $financial_year_id = $financial_year->id;
        //get apply date - the last date of the financial year of the execution
        $apply_date = DB::table('financial_years')
            ->where('id', $financial_year_id)
            ->select('end_date')
            ->first()->end_date;
        $apply_date = date("d-m-Y", strtotime($apply_date));

        //credit account
        $control = $this->getControlAccount('000', '0000');
        $credit_account = $control['credit_account'];

        $options = array(
            'headers' => array('x-match' => 'all', 'type' => 'BUDGET'),
            'exchange' => 'FacilityBudgetExchange'
        );

        $queue_name = ConfigurationSetting::getValueByKey('BUDGET_TO_FFARS_QUEUE_NAME');

        $book = ConfigurationSetting::getValueByKey('BUDGET_BOOK_ID');

        //get data
        $budget = DB::table('budget_export_to_financial_systems as b')
            ->join('budget_export_transaction_items as i', 'b.budget_export_transaction_item_id', 'i.id')
            ->join('budget_export_accounts as a', 'i.budget_export_account_id', 'a.id')
            ->join('budget_export_transactions as t', 't.id', 'i.budget_export_transaction_id')
            ->join('activity_facility_fund_source_inputs as affi', 'affi.id', 'a.activity_facility_fund_source_input_id')
            ->join('activity_facility_fund_sources as aff', 'aff.id', 'affi.activity_facility_fund_source_id')
            ->join('activity_facilities as af', 'af.id', 'aff.activity_facility_id')
            ->join('facilities as f', 'f.id', 'af.facility_id')
            ->where(function ($q) {
                $q->where('b.is_sent', false)->orWhereNull('b.is_sent');
            })
            ->where('t.id', $transaction_id)
            ->select('b.id', 'b.chart_of_accounts', 'b.amount', 'b.description', 't.control_number', 'f.facility_code');

        //generate budget import items ready to export
        $debit_account = array();
        $credit_amount = 0;
        $message = array();
        if ($budget->count() > 0) {
            foreach ($budget->get() as $data) {
                $item = array('Acc' => $data->chart_of_accounts,
                    'Amount' => $data->amount,
                    'UID' => $data->id);

                array_push($debit_account, $item);
                $credit_amount += $data->amount;
                $facility_code = $data->facility_code;
            }

            $crd_acc = array('Acc' => $credit_account, 'Amount' => "$credit_amount", 'UID' => 1);
            $item = array('BookID' => $book,
                'FacilityCode' => $facility_code,
                'JournalCode' => 'BD',
                'ApplyDate' => $apply_date,
                'CreditAcc' => array($crd_acc),
                'DebitAcc' => $debit_account);
            array_push($message, $item);
            dispatch(new SendDataToRabbitMQJob($queue_name, $message, $options, 'budget'));
            $response_message = " Transactions has been sent to FFARS. Please wait for Notification";
        } else {
            $response_message = "No Transaction to FFARS, Please contant administrator";
        }
        return $response_message;
    }

    /**create gl account */
    public function createGLAccount($budget_export_account_id, $is_sent, $is_delivered, $company, $type)
    {
        if ($type == 'EXPENSE') {
            //check if exist
            $count = DB::table('company_gl_accounts')
                ->where('budget_export_account_id', $budget_export_account_id)
                ->where('company', $company)
                ->select('id')
                ->count();
            if ($count == 0) {
                DB::table('company_gl_accounts')->insert(['budget_export_account_id' => $budget_export_account_id, 'is_sent' => $is_sent,
                    'is_delivered' => $is_delivered, 'company' => $company]);
            }
        } else {
            //check if exist
            $count = DB::table('company_gl_accounts')
                ->where('revenue_export_account_id', $budget_export_account_id)
                ->where('company', $company)
                ->select('id')
                ->count();
            if ($count == 0) {
                DB::table('company_gl_accounts')->insert(['revenue_export_account_id' => $budget_export_account_id, 'is_sent' => $is_sent,
                    'is_delivered' => $is_delivered, 'company' => $company]);
            }
        }

    }

    /**check if all segments has been sent and delivered to MUSE */
    public function segmentDeliveryStatus($admin_hierarchy_id)
    {
        $coa_segments = DB::table('coa_segments as s')
            ->join('coa_segment_company as c', 's.id', 'c.coa_segment_id')
            ->join('admin_hierarchies as a', 'a.id', 'c.admin_hierarchy_id')
            ->where('c.is_delivered', false)
            ->where('c.is_cleared', false)
            ->where('a.id', $admin_hierarchy_id)
            ->select('s.id', 'c.id as segment_id', 'a.code as sub_vote');

        if ($coa_segments->count() > 0) {
            return false;  //there are undelivered segments
        } else {
            return true;   //all segments has been delivered
        }
    }

    /**check for an error message with segment */
    public function segmentDeliveryError($admin_hierarchy_id)
    {
        $coa_segments = DB::table('coa_segments as s')
            ->join('coa_segment_company as c', 's.id', 'c.coa_segment_id')
            ->join('admin_hierarchies as a', 'a.id', 'c.admin_hierarchy_id')
            ->where('c.is_sent', true)
            ->where('c.is_delivered', false)
            ->where('c.is_cleared', false)
            ->whereNotNull('c.response')
            ->where('a.id', $admin_hierarchy_id)
            ->select('s.id', 'c.id as segment_id', 'a.code as sub_vote');
        if ($coa_segments->count() > 0) {
            return true;  //there is an error which are not resolved
        } else {
            return false;   //there is no error
        }
    }

    public function loadSegments($admin_hierarchy_id, $segment_number)
    {
        $segment = DB::table('coa_segments as c')
            ->join('coa_segment_company as sc', 'sc.coa_segment_id', '=', 'c.id')
            ->join('admin_hierarchies as a', 'a.id', '=', 'sc.admin_hierarchy_id')
            ->where('sc.is_delivered', false)
            ->where('c.segment_number', $segment_number)
            ->where('a.id', $admin_hierarchy_id)
            ->where('sc.is_cleared', false)
            ->select('sc.id', 'a.code as sub_vote', 'c.segment_number', 'c.segment_code', 'c.segment_name',
                'c.segment_description', 'c.category', 'c.normal_balance')
            ->get();
        return $segment;
    }

    //get segment numbers
    public function getSegmentNumbers($admin_hierarchy_id)
    {
        $segment_numbers = DB::table('coa_segments as c')
            ->join('coa_segment_company as sc', 'sc.coa_segment_id', '=', 'c.id')
            ->join('admin_hierarchies as a', 'a.id', '=', 'sc.admin_hierarchy_id')
            ->where('sc.is_delivered', false)
            ->where('sc.is_cleared', false)
            ->where('a.id', $admin_hierarchy_id)
            ->select('c.segment_number', 'a.code as sub_vote', 'c.coa_code')
            ->groupBy('c.segment_number', 'a.code', 'c.coa_code')
            ->get();
        return $segment_numbers;
    }

    //get gl Account books
    public function getGLAccountBooks($book, $admin_hierarchy_id, $financial_year_id, $type, $account_type = null)
    {
        if ($type == 'EXPENSE') {
            if ($account_type == 'facility') {
                $GLAccounts = DB::table('aggregate_facility_budgets  as ag')
                    ->where('ag.admin_hierarchy_id', $admin_hierarchy_id)
                    ->where('ag.financial_year_id', $financial_year_id)
                    ->where('ag.gl_is_delivered', false)
                    ->select('ag.id', 'ag.chart_of_accounts')
                    ->groupBy('ag.id', 'ag.chart_of_accounts')
                    ->get();
            } else {
                $GLAccounts = DB::table('budget_export_accounts  as gl')
                    ->join('company_gl_accounts as l', 'l.budget_export_account_id', 'gl.id')
                    ->join('gfs_codes as gfs', 'gfs.id', 'gl.gfs_code_id')
                    ->where('gl.admin_hierarchy_id', $admin_hierarchy_id)
                    ->where('gl.financial_year_id', $financial_year_id)
                    ->where('l.company', $book)
                    ->where('gl.financial_system_code', 'MUSE')
                    ->where('l.is_delivered', false)
                    ->select('gl.id', 'gl.chart_of_accounts', 'gfs.name', 'gfs.code', 'l.company')
                    ->get();
            }

        } else {
            $system_code = ['MUSE', 'LGRCIS'];
            $GLAccounts = DB::table('revenue_export_accounts  as gl')
                ->join('company_gl_accounts as l', 'l.revenue_export_account_id', 'gl.id')
                ->join('gfs_codes as gfs', 'gfs.id', 'gl.gfs_code_id')
                ->where('gl.admin_hierarchy_id', $admin_hierarchy_id)
                ->where('gl.financial_year_id', $financial_year_id)
                ->where('l.company', $book)
                ->whereIn('gl.financial_system_code', $system_code)
                ->where('l.is_delivered', false)
                ->select('gl.id', 'gl.chart_of_accounts', 'gfs.name', 'gfs.code', 'l.company')
                ->get();
        }

        return $GLAccounts;
    }

    /**check if all segments has been sent to MUSE */
    public function allSegmenstsHasBeenSent($admin_hierarchy_id, $financial_year_id, $budget_type)
    {
        $sent = DB::select("SELECT count(*) as count FROM coa_segment_company c
                                JOIN coa_segments s ON c.coa_segment_id = s.id
                                WHERE admin_hierarchy_id = $admin_hierarchy_id
                                AND c.is_sent = true AND c.is_delivered = true AND
                                s.is_active = true");

        $not_sent = DB::select("SELECT count(*) as count FROM coa_segment_company c
                            JOIN coa_segments s ON c.coa_segment_id = s.id
                            WHERE admin_hierarchy_id = $admin_hierarchy_id
                            AND  c.is_delivered = false AND
                            s.is_active = true");
        //all transactions has been sent to MUSE
        $gl_accounts = DB::table('budget_export_accounts  as gl')
            ->join('activities as ac', 'ac.id', 'gl.activity_id')
            ->where('gl.admin_hierarchy_id', $admin_hierarchy_id)
            ->where('gl.financial_year_id', $financial_year_id)
            ->where('gl.financial_system_code', 'MUSE')
            ->where('ac.budget_type', $budget_type)
            ->where('gl.is_sent', false)
            ->select('gl.id')
            ->count();
        if ($sent[0]->count > 0 && $not_sent[0]->count == 0 && $gl_accounts == 0) {
            return true;
        } else {
            return false;
        }
        return $count;
    }

    //send segments for revenue
    public function sendRevenueSegments($admin_hierarchy_id, $financial_year_id)
    {
        //get gl accounts for budget
        $GLAccounts = DB::table('revenue_export_accounts  as gl')
            ->join('gfs_codes as gfs', 'gfs.id', 'gl.gfs_code_id')
            ->where('gl.admin_hierarchy_id', $admin_hierarchy_id)
            ->where('gl.financial_year_id', $financial_year_id)
            ->whereIn('gl.financial_system_code', ['MUSE', 'LGRCIS'])
            ->where('gl.is_sent', false)
            ->select('gl.chart_of_accounts', 'gfs.name', 'gfs.code')
            ->groupBy('gl.chart_of_accounts', 'gfs.name', 'gfs.code')
            ->get();;
        if (count($GLAccounts) > 0) {
            foreach ($GLAccounts as $data) {
                //create gl account
                $segments = explode("-", $data->chart_of_accounts);
                unset($segments[count($segments) - 1]);
                //append the gfs code to the beginning
                $glAcc = array_prepend($segments, $data->code);
                $error_message = $this->checkAccountSegments($glAcc, $admin_hierarchy_id);
                if (count($error_message) > 0) {
                    array_push($this->failed_transaction, $error_message);
                }
            }
        }
        if (count($this->failed_transaction) == 0) {
            //send segments
            $this->sendGLAccountSegments($admin_hierarchy_id);
        }

    }

    public function sendFFARSBudgetToMUSE($admin_hierarchy_id)
    {
        $budget = DB::select('activity_facility_fund_source_inputs as i')
            ->join('activity_facility_fund_sources AS aff', 'aff.id', '=', 'i.activity_facility_fund_source_id')
            ->join('activity_facilities AS af', 'af.id', '=', 'aff.activity_facility_id')
            ->join('activities AS a', 'a.id', '=', 'af.activity_id')
            ->join('mtef_sections AS ms', 'ms.id', '=', 'a.mtef_section_id')
            ->join('mtefs AS m', 'm.id', '=', 'ms.mtef_id')
            ->join('admin_hierarchies AS adm', 'adm.id', '=', 'm.admin_hierarchy_id')
            ->join('admin_hierarchies AS reg', 'reg.id', '=', 'adm.parent_id')
            ->join('sections AS s', 's.id', '=', 'ms.section_id')
            ->join('budget_classes AS bc', 'bc.id', '=', 'a.budget_class_id')
            ->join('fund_sources AS fs', 'fs.id', '=', 'aff.fund_source_id')
            ->join('facilities AS fac', 'fac.id', '=', 'af.facility_id')
            ->join('fund_sources AS fs', 'fs.id', '=', 'aff.fund_source_id')
            ->where('a.is_facility_account', '=', true)
            ->where('adm.id', '=', $admin_hierarchy_id)
            ->where('i.deleted_at', '=', null)
            ->get();
    }

    /** get facility code from COA */
    public function getFalicityCode($chart_of_account)
    {
        $arr = explode('-', $chart_of_account);
        return $arr[4];
    }

    /** check transfer code */
    public function getTransferCode($code)
    {
        $result = DB::table('sections')->where('code', $code)->first();
        return BudgetExportAccount::getGfsCode($result->id);
    }

    /** change budget to additional */
    public function changetoAdditionalBudget($id)
    {
        $result = DB::table('budget_export_accounts')
            ->where('id', $id)
            ->first();
        if (isset($result->id)) {
            //update activity type
            DB::table('activities')
                ->where('id', $result->activity_id)
                ->update(['budget_type' => 'SUPPLEMENTARY']);
            //upgate activity input
            DB::table('activity_facility_fund_source_inputs')
                ->where('id', $result->activity_facility_fund_source_input_id)
                ->update(['budget_type' => 'SUPPLEMENTARY']);
            //check if transaction has been created
            $count = DB::table('budget_export_transaction_items')
                ->where('budget_export_account_id', $result->id)
                ->whereNull('budget_reallocation_item_id')
                ->select('id')
                ->count();
            if ($count == 0) {
                $this->createTransaction($result);
            }
        }
    }

    /** send to ERMS */
    public static function sendCeillingToErms($data){



        $messageDetails = array();
        foreach ($data as $info){
         $financialYearId = $info["financial_year_id"];
         $adminHierarchyId = $info["admin_hierarchy_id"];

        /** Get Company code  */
            $admin_hierarchy = DB::table('admin_hierarchies as c')
                 ->join('admin_hierarchies as r', 'c.parent_id', 'r.id')
             ->where('c.id', $adminHierarchyId)
             ->select('c.code as institutionCode', 'c.name as institutionName' ,'r.code as region')
            ->first();
            $company = $admin_hierarchy->institutionCode;
            $institutionName = $admin_hierarchy->institutionName;



            /** Get Financial Year  */
            $apply_date = DB::table('financial_years')
                ->where('id', $financialYearId)
                ->select('end_date','name')
                ->first();


            $payload = array(
                "fundSourceName" => $info["ceiling"]["gfs_code"]["fund_source"]["name"],
                "subBudgetClassName" => $info["ceiling"]["budget_class"]["name"],
                "fundSoureCode" => $info["ceiling"]["gfs_code"]["fund_source"]["code"],
                "subBudgetClassCode" => $info["ceiling"]["budget_class"]["code"],
                "amount" => $info["amount"],
                "uid" => $info["id"],
            );
            array_push($messageDetails,$payload);
        }
        $control_number = app('App\Http\Controllers\Execution\MuseIntegrationController')->transactionNumber("CEILING",10);
        $messageHeader = array(
            "sender" => "PLANREPOTR",
            "receiver" => "ERMS",
            "msgId" => $control_number,
            "applyDate" => $apply_date->end_date,
            "messageType" => "CEILING",
            "createdAt" => date('Y-m-d H:i:s')
        );

        $messageSummary = array(
            "company"=> $company,
            "trxCtrlNum" => $control_number,
            "financialYear" => "2021/22",
            "applyDate"=>$apply_date->end_date,
            "description"=>"CEILING FOR ".$institutionName,
        );

        $item = array(
            "messageHeader" => $messageHeader,
            "messageSummary" =>$messageSummary,
            "messageDetails" => $messageDetails
        );


        $data = json_encode($item);
        $plainText = $data;
        $privateKey = openssl_pkey_get_private("file://" . base_path() . "/config/keys/private.pem");
        $publicKey = openssl_pkey_get_public("file://" . base_path() . "/config/keys/public.pem");

        // Make a signature
        openssl_sign($plainText, $signature, $privateKey, OPENSSL_ALGO_SHA256);
        $signature = base64_encode($signature);

        $payloadToErms = array(
            'message' => $item,
            'digitalSignature'=>$signature
        );

        //new export implementation end

        if ($payloadToErms != null) {
            try {
               $response_message = 'Ceiling Data has been sent to ERMS. Please wait for Nofication';
                $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($payloadToErms);
                if($response == "ACCEPTED"){
                    foreach($messageDetails as $detail){
                    $uid = $detail["uid"];
                    DB::table('admin_hierarchy_ceilings')->where('id',$uid)->update(['is_exported' => true]);
                    }
                   // DB::table('admin_hierarchy_ceilings')->where('id', $transaction_id)->update(['is_sent' => true]);
               }
            } catch (\Throwable $th) {
                //throw $th;
            $response_message = 'Ceiling Data has been sent to ERMS. Please wait for Nofication';

            }
        }

    return $response_message;


    }


    /** create transaction */
    public function createTransaction($data)
    {

        DB::transaction(function () use ($data) {
            $userId = UserServices::getUser()->id;
            $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;

            $transaction = new BudgetExportTransaction();
            $transaction->budget_transaction_type_id = 2;
            $transaction->description = "ADDITIONAL  BUDGET";
            $transaction->save();
            //enter transaction control number
            $control_number = 'ADDITIONAL-' . $transaction->id;
            $transaction->control_number = $control_number;
            $transaction->save();
            //end of control number update

            $transaction_item = BudgetExportTransactionItemService::create(
                $transaction->id,
                $data->id,
                $data->amount,
                null,
                false,
                $userId
            );

            BudgetExportToFinancialSystemService::create(
                $data->admin_hierarchy_id,
                $transaction_item->id,
                $data->chart_of_accounts,
                'ADDITIONAL_BUDGET',
                $data->financial_year_id,
                $data->amount,
                $data->financial_system_code,
                $userId
            );
        });
    }
}
