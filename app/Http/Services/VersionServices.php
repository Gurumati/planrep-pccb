<?php

namespace App\Http\Services;

use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\AdminHierarchyLevel;
use App\Models\Setup\FinancialYear;
use App\Models\Setup\Version;
use Illuminate\Support\Facades\DB;

class VersionServices
{
    public static function getVersionId($financialYearId, $type)
    {
        $version = Version::whereHas("financial_year_versions", function ($fyv) use ($financialYearId) {
            $fyv->where("financial_year_id", $financialYearId);
        })->whereHas("type", function ($versionType) use ($type) {
            $versionType->where("code", $type);
        })->first();
        return $version->id;
    }
}
