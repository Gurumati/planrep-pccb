<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/10/2017
 * Time: 5:21 PM
 */

namespace App\Http\Services\Execution;

use App\Models\Execution\BudgetExportTransactionItem;


class BudgetExportTransactionItemService
{

    public static function create($exportTransactionId,$budgetExportAccountId,$amount,$reallocationId,$isCredit,$createdBy){

        $transactionItem = new BudgetExportTransactionItem();
        $transactionItem->budget_export_transaction_id = $exportTransactionId;
        $transactionItem->budget_export_account_id = $budgetExportAccountId;
        $transactionItem->budget_reallocation_item_id = $reallocationId;
        $transactionItem->amount = $amount;
        $transactionItem->created_by = $createdBy;
        $transactionItem->is_credit = $isCredit;
        $transactionItem->save();

        return $transactionItem;
    }
}