<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/10/2017
 * Time: 5:21 PM
 */

namespace App\Http\Services\Execution;

use App\Models\Execution\RevenueExportTransactionItem;

class RevenueExportTransactionItemService{

    public static function create($exportTransactionId,$revenueExportAccountId,$amount,$createdBy){

        $transactionItem = new RevenueExportTransactionItem();
        $transactionItem->revenue_export_transaction_id = $exportTransactionId;
        $transactionItem->revenue_export_account_id = $revenueExportAccountId;
        $transactionItem->amount = $amount;
        $transactionItem->created_by = $createdBy;
        $transactionItem->save();
        return $transactionItem;
    }
}