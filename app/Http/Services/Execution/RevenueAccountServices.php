<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 11/23/17
 * Time: 3:19 PM
 */

namespace App\Http\Services\Execution;


use App\Models\Budgeting\AdminHierarchyCeiling;
use App\Models\Execution\RevenueExportAccount;
use App\Models\Setup\ConfigurationSetting;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RevenueAccountServices
{
    /**
     * Approve revenue (Ceiling)
     */
    public static function approveAllRevenue($adminHierarchyId, $financialYearId, $budgetType){

        //$ownSourceProjection = RevenueAccountServices::getOwnSourceRevenue($adminHierarchyId,$financialYearId,$budgetType,'%');
        $facilityRevenue = RevenueAccountServices::getFacilityRevenue($adminHierarchyId,$financialYearId,$budgetType,'%');

        // $data = $ownSourceProjection->merge($facilityRevenue);
        foreach ($facilityRevenue as $revenue){
           RevenueAccountServices::approveOne($revenue);
        }
        $query = AdminHierarchyCeiling::where('admin_hierarchy_id',$adminHierarchyId)->
                where('financial_year_id',$financialYearId)->
                where('budget_type',$budgetType);
        if($budgetType == 'CURRENT') {
            $finalBudgetType =  'APPROVED';
        } else {
            $finalBudgetType = $budgetType;
            $query = $query->where('amount', '>', 0.00)->whereNotNull('facility_id');
        }

        $query->update(array('is_approved'=>true,'is_locked'=>true,'budget_type'=>$finalBudgetType));

    }

    public static function approveSelectedRevenue($revenueIds){

        $data = DB::table('ceilings as c')
            ->join('admin_hierarchy_ceilings as ac','c.id','ac.ceiling_id')
            ->join('gfs_codes as g','g.id','c.gfs_code_id')
            ->join('fund_sources as f','f.id','g.fund_source_id')
            ->join('sections as s','s.id','ac.section_id')
            ->join('budget_classes as b','b.id','c.budget_class_id')
            ->join('fund_source_categories as fc','fc.id','f.fund_source_category_id')
            ->join('fund_types as ft','ft.id','fc.fund_type_id')
            ->join('admin_hierarchies as ah', 'ah.id', 'ac.admin_hierarchy_id')
            ->join('admin_hierarchies as ap', 'ap.id', 'ah.parent_id')
            ->join('admin_hierarchies as ap2', 'ap2.id', 'ap.parent_id')
            ->leftjoin('facilities as fa', 'fa.id', 'ac.facility_id')
            ->leftjoin('facility_types as fat','fat.id','fa.facility_type_id')
            ->whereIn('ac.id',$revenueIds)
            ->select('fat.code as facility_type_code','ac.budget_type as budget_type', 's.id as section_id',
                     's.name as section','s.code as cost_center','g.id as gfs_code_id',
                     'g.code as gfs_code','g.description as description','f.description as fund_source',
                     'ft.current_budget_code as fund_type','c.id as ceiling_id','c.name as ceiling',
                     'ac.id as admin_hierarchy_ceiling_id','ac.amount','ac.is_facility','b.code as budget_class',
                     'f.code as fund_source_code','ac.is_approved', 'ah.id as admin_hierarchy_id',
                     'ac.financial_year_id',
                     'ah.code as council', 'ap.code as sub_vote','ap2.code as vote', 'fa.facility_code as facility_code')
            ->get();
        foreach ($data as $item){
//            $financial_system_code = ($item->facility_type_code == null || $item->facility_type_code == '001')?'MUSE':'MUSE';
            $financial_system_code = 'MUSE';
            $facility_code = $item->facility_code !== null?$item->facility_code:'00000000';
            $item->financial_system_code = $financial_system_code;
            $item->chart_of_account = $item->vote.'-'.$item->sub_vote.'-'.$item->council.'-'.$item->cost_center.'-0000000-'.'00000000-'.$item->budget_class.'-0000-000-000000-'.$item->fund_type.'-00000-'.$item->fund_source_code.'-'.$item->gfs_code;
            RevenueAccountServices::approveOne($item);
        }

    }
    public static function approveOne($revenue){
         DB::transaction(function () use ($revenue){
            $exist = RevenueExportAccount::where('admin_hierarchy_id', $revenue->admin_hierarchy_id)
                                         ->where('financial_year_id', $revenue->financial_year_id)
                                         ->where('chart_of_accounts', $revenue->chart_of_account)
                                         ->where('admin_hierarchy_ceiling_id', $revenue->admin_hierarchy_ceiling_id)
                                         ->select('id','financial_system_code');
            if($exist->count() == 0 || $revenue->financial_system_code != $exist->first()->financial_system_code){
                $account = new RevenueExportAccount(array(
                    "admin_hierarchy_id"=>$revenue->admin_hierarchy_id,
                    "section_id"=>$revenue->section_id,
                    "financial_year_id"=>$revenue->financial_year_id,
                    "gfs_code_id"=>$revenue->gfs_code_id,
                    "chart_of_accounts"=>$revenue->chart_of_account,
                    "financial_system_code"=>$revenue->financial_system_code,
                    "amount"=>$revenue->amount,
                    "admin_hierarchy_ceiling_id"=>$revenue->admin_hierarchy_ceiling_id,
                    "is_sent"=>false,
                    "is_delivered"=>false
                ));
                $account->save();
            }

        });
    }

    public static function getOwnSourceRevenue($admin_hierarchy_id,$financial_year_id, $budgetType, $searchString){

        $admin_hierarchy = RevenueAccountServices::getVote($admin_hierarchy_id);
        $vote = $admin_hierarchy->vote;
        $sub_vote = $admin_hierarchy->sub_vote;
        $council = $admin_hierarchy->council;
        $reEnfanced = ConfigurationSetting::getValueByKey('RE_ENFANCED_OWN_SOURCE_REVENUE');

        $searchString = "%".trim(strtolower($searchString))."%";
        $councilRevenue = DB::table('ceilings as c')
            ->join('admin_hierarchy_ceilings as ac','c.id','ac.ceiling_id')
            ->join('gfs_codes as gc','gc.id','c.gfs_code_id')
            ->join('gfscode_fundsources as gf','gf.gfs_code_id','gc.id')
            ->join('fund_sources as fs','fs.id','gf.fund_source_id')
            ->join('sections as s','s.id','ac.section_id')
            ->join('section_levels as sl','sl.id','s.section_level_id')
            ->join('budget_classes as b','b.id','c.budget_class_id')
            ->join('fund_source_budget_classes as fb', function($join) {
                $join->on('fb.budget_class_id','b.id')
                    ->on('fb.fund_source_id','fs.id');
            })
            ->join('fund_types as ft','ft.id','fb.fund_type_id')
            ->where('c.is_aggregate', false)
            ->where('ac.admin_hierarchy_id',$admin_hierarchy_id)
            ->where('ac.financial_year_id',$financial_year_id)
            ->where('ac.budget_type',$budgetType)
            ->whereNull('ac.deleted_at')
            ->where('ac.amount','>', 0.00)
            ->where('sl.hierarchy_position',1)
            ->where('fs.can_project', true)
            ->select('ac.budget_type as budget_type', 'ac.admin_hierarchy_id as admin_hierarchy_id','ac.financial_year_id as financial_year_id',
                        's.id as section_id','s.name as section','s.code as cost_center','gc.id as gfs_code_id','gc.code as gfs_code','gc.description as description',
                        'fs.description as fund_source','ft.current_budget_code as fund_type','ft.carried_over_budget_code as c_fund_type',
                        'c.id as ceiling_id','c.name as ceiling','ac.id as admin_hierarchy_ceiling_id','ac.amount','ac.is_facility','b.code as budget_class',
                        'fs.code as fund_source_code','ac.is_approved')
            ->get();

            $facilityRevenue=DB::table('ceilings as c')
                ->join('admin_hierarchy_ceilings as ac','c.id','ac.ceiling_id')
                ->join('gfs_codes as gc','gc.id','c.gfs_code_id')
                ->join('gfscode_fundsources as gf','gf.gfs_code_id','gc.id')
                ->join('fund_sources as fs','fs.id','gf.fund_source_id')
                ->join('sections as s','s.id','ac.section_id')
                ->join('section_levels as sl','sl.id','s.section_level_id')
                ->join('budget_classes as b','b.id','c.budget_class_id')
                ->join('fund_source_budget_classes as fb', function($join) {
                    $join->on('fb.budget_class_id','b.id')
                        ->on('fb.fund_source_id','fs.id');
                })
                ->join('fund_types as ft','ft.id','fb.fund_type_id')
                ->where('c.is_aggregate', false)
                ->where('ac.admin_hierarchy_id',$admin_hierarchy_id)
                ->where('ac.financial_year_id',$financial_year_id)
                ->where('ac.budget_type',$budgetType)
                ->whereNull('ac.deleted_at')
                ->where('ac.amount','>', 0.00)
                ->where('sl.hierarchy_position',1)
                ->whereIn('fs.id', $reEnfanced)
//                ->whereIn('s.code', ['508B','508D','508E','507B','509B'])
                ->where('ac.is_facility', false)
                ->whereNull('ac.facility_id')
                ->whereNull('ac.use_status')
                ->select('ac.budget_type as budget_type', 'ac.admin_hierarchy_id as admin_hierarchy_id','ac.financial_year_id as financial_year_id',
                            's.id as section_id','s.name as section','s.code as cost_center','gc.id as gfs_code_id','gc.code as gfs_code','gc.description as description',
                            'fs.description as fund_source','ft.current_budget_code as fund_type','ft.carried_over_budget_code as c_fund_type',
                            'c.id as ceiling_id','c.name as ceiling','ac.id as admin_hierarchy_ceiling_id','ac.amount','ac.is_facility','b.code as budget_class',
                            'fs.code as fund_source_code','ac.is_approved')
                ->get();

        foreach ($councilRevenue as &$item){
            $financial_system_code = 'MUSE';

//            $codes = ConfigurationSetting::getValueByKey('OWN_SOURCE_REVENUE_DEFAULT_COA_COST_CENTRE');
            $codes = ConfigurationSetting::where('key','OWN_SOURCE_REVENUE_DEFAULT_COA_COST_CENTRE')->first();
            $codes2 = explode(',',$codes->value);
             $sectionCode = DB::table('mtefs as m')
                ->join('mtef_sections as ms','ms.mtef_id','m.id')
                ->join('sections as s','s.id','ms.section_id')
                ->where('m.admin_hierarchy_id',$admin_hierarchy_id)
                ->whereIn('s.code',$codes2)
                 ->select('s.code')
                ->first();
            //$item->cost_center = $sectionCode->code;
            $item->cost_center = isset($sectionCode->code)?$sectionCode->code:'201A';
            $item->financial_system_code = $financial_system_code;
            $item->facility_code = isset($item->facility_code)?$item->facility_code:'00000000';
            $fundType = ($budgetType == 'CARRYOVER')?$item->c_fund_type: $item->fund_type;
            $item->chart_of_account = $vote.'-'.$sub_vote.'-'.$council.'-'.$item->cost_center.'-0000000-'.'00000000-'.$item->budget_class.'-0000-000-000000-'.$fundType.'-00000-'.$item->fund_source_code.'-'.$item->gfs_code;
        }
        foreach ($facilityRevenue as &$item){
            $financial_system_code = 'MUSE';
            
            //            $codes = ConfigurationSetting::getValueByKey('OWN_SOURCE_REVENUE_DEFAULT_COA_COST_CENTRE');
            $codes = ConfigurationSetting::where('key','OWN_SOURCE_REVENUE_DEFAULT_COA_COST_CENTRE')->first();
            $codes2 = explode(',',$codes->value);
            $sectionCode = DB::table('mtefs as m')
                ->join('mtef_sections as ms','ms.mtef_id','m.id')
                ->join('sections as s','s.id','ms.section_id')
                ->where('m.admin_hierarchy_id',$admin_hierarchy_id)
                ->whereIn('s.code',$codes2)
                ->select('s.code')
                ->first();
            //$item->cost_center = $sectionCode->code;
            $item->cost_center = isset($sectionCode->code)?$sectionCode->code:'201A';
            $item->financial_system_code = $financial_system_code;
            $item->facility_code = isset($item->facility_code)?$item->facility_code:'00000000';
            $fundType = ($budgetType == 'CARRYOVER')?$item->c_fund_type: $item->fund_type;
            $item->chart_of_account = $vote.'-'.$sub_vote.'-'.$council.'-'.$item->cost_center.'-0000000-'.'00000000-'.$item->budget_class.'-0000-000-000000-'.$fundType.'-00000-'.$item->fund_source_code.'-'.$item->gfs_code;
        }
        //$own_source = $councilRevenue->merge($facilityRevenue);
        $own_source = $councilRevenue;
        return $own_source;
    }

    // Facility revenue(Ceiling)
    public static function getFacilityRevenue($admin_hierarchy_id, $financial_year_id, $budgetType, $searchString){
        $admin_hierarchy = RevenueAccountServices::getVote($admin_hierarchy_id);
        $vote = $admin_hierarchy->vote;
        $sub_vote = $admin_hierarchy->sub_vote;
        $council = $admin_hierarchy->council;

        $searchString = "%".trim(strtolower($searchString))."%";
        $revenue = DB::table('ceilings as c')
            ->join('admin_hierarchy_ceilings as ac','c.id','ac.ceiling_id')
            ->join('gfs_codes as gc','gc.id','c.gfs_code_id')
            ->join('gfscode_fundsources as gf','gf.gfs_code_id','gc.id')
            ->join('fund_sources as f','f.id','gf.fund_source_id')
            ->join('sections as s','s.id','ac.section_id')
            // ->join('facilities as fa','fa.id','ac.facility_id')
            // ->join('facility_types as fat','fat.id','fa.facility_type_id')
            ->join('budget_classes as b','b.id','c.budget_class_id')
            ->join('fund_source_categories as fc','fc.id','f.fund_source_category_id')
            ->join('fund_types as ft','ft.id','fc.fund_type_id')
            ->where('ac.admin_hierarchy_id',$admin_hierarchy_id)
            ->where('ac.financial_year_id',$financial_year_id)
            ->where('ac.budget_type',$budgetType)
            ->where('ac.section_id',1)
            ->whereRaw("LOWER(gc.description)::varchar LIKE ? " , [$searchString])
            ->whereNull('ac.use_status')
            ->whereNull('ac.deleted_at')
            ->where('ac.amount','>',0.00)
            ->select('ac.budget_type as budget_type', 'ac.admin_hierarchy_id as admin_hierarchy_id','ac.financial_year_id as financial_year_id','s.id as section_id',
                    's.name as section','s.code as cost_center','gc.id as gfs_code_id','gc.code as gfs_code','gc.description as description',
                    'f.description as fund_source','ft.current_budget_code as fund_type','ft.carried_over_budget_code as c_fund_type',
                    'c.id as ceiling_id','c.name as ceiling','ac.id as admin_hierarchy_ceiling_id','ac.amount','ac.is_facility','b.code as budget_class',
                    'f.code as fund_source_code','ac.is_approved')
            ->get();
        foreach($revenue as &$item){
            $financial_system_code = 'MUSE';
            //            $codes = ConfigurationSetting::getValueByKey('OWN_SOURCE_REVENUE_DEFAULT_COA_COST_CENTRE');
            $codes = ConfigurationSetting::where('key','OWN_SOURCE_REVENUE_DEFAULT_COA_COST_CENTRE')->first();
            $codes2 = explode(',',$codes->value);
            $sectionCode = DB::table('mtefs as m')
                ->join('mtef_sections as ms','ms.mtef_id','m.id')
                ->join('sections as s','s.id','ms.section_id')
                ->where('m.admin_hierarchy_id',$admin_hierarchy_id)
                ->whereIn('s.code',$codes2)
                ->select('s.code')
                ->first();
            //$item->cost_center = $sectionCode->code;
            $item->cost_center = isset($sectionCode->code)?$sectionCode->code:'201A';

            $item->financial_system_code=$financial_system_code;
            $fundType = ($budgetType == 'CARRYOVER')?$item->c_fund_type: $item->fund_type;
            $item->chart_of_account = $vote.'-'.$sub_vote.'-'.$council.'-'.$item->cost_center.'-0000000-'.'00000000-'.$item->budget_class.'-0000-000-000000-'.$fundType.'-00000-'.$item->fund_source_code.'-'.$item->gfs_code;
        }
        return $revenue;
    }

    public static function getVote($admin_hierarchy_id)
    {
        $admin_hierarchy = DB::table('admin_hierarchies as a')
            ->join('admin_hierarchies as p','a.parent_id','p.id')
            ->join('admin_hierarchies as p2','p.parent_id','p2.id')
            ->where('a.id',$admin_hierarchy_id)
            ->select('a.code as council','p.code as sub_vote','p2.code as vote')
            ->first();
        return $admin_hierarchy;
    }
}
