<?php

namespace App\Http\Services\Execution;


use App\Models\Execution\BudgetExportToFinancialSystem;
use App\Models\Setup\ConfigurationSetting;
use Illuminate\Support\Facades\Log;


class BudgetExportToFinancialSystemService
{

    public static function create($adminHierarchyId,$transactionItemId,$chartOfAccount,$description,$financialYearId,$amount,$financialSystemCode,$createdBy){
        $export = new BudgetExportToFinancialSystem();

        $export->budget_export_transaction_item_id = $transactionItemId;
        $export->admin_hierarchy_id = $adminHierarchyId;
        $export->chart_of_accounts = $chartOfAccount;
        $export->description =$description;
        $export->is_sent = false;
        $export->financial_year_id = $financialYearId;
        $export->amount = $amount;
        $systemQueueKey = $financialSystemCode.'_BUDGET_EXPORT_QUEUE_NAME';
        $queue = ConfigurationSetting::where('key',$systemQueueKey)->first();
        $export->queue_name =($queue != null)?$queue->value:'';
        $export->created_by = $createdBy;
        $export->save();
        return $export;
    }

    public static function addRevenue($adminHierarchyId,$transactionItemId,$chartOfAccount,$description,$financialYearId,$amount,$financialSystemCode,$createdBy){
        // $check = BudgetExportToFinancialSystem::where('chart_of_accounts',$chartOfAccount)->where('admin_hierarchy_id',$adminHierarchyId)->get();
        // return $check;
        // if($check->count() == 0){

            $export = new BudgetExportToFinancialSystem();
            
            $export->revenue_export_transaction_item_id = $transactionItemId;
            $export->admin_hierarchy_id = $adminHierarchyId;
            $export->chart_of_accounts = $chartOfAccount;
            $export->description =$description;
            $export->is_sent = false;
            $export->financial_year_id = $financialYearId;
            $export->amount = $amount;
            $systemQueueKey = $financialSystemCode.'_BUDGET_EXPORT_QUEUE_NAME';
            $queue = ConfigurationSetting::where('key',$systemQueueKey)->first();
            $export->queue_name =($queue != null)?$queue->value:'';
            $export->created_by = $createdBy;
            $export->save();
            return $export;
        // }else{
        //     return $check;
        // }
       
    }

}