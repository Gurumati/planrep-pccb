<?php
namespace App\Http\Services\Planning;

use App\Http\Controllers\Flatten;
use App\Http\Services\FinancialYearServices;
use App\Models\Setup\FinancialYear;
use App\Http\Services\UserServices;
use App\Models\Setup\ActivityCategory;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\Period;
use App\Models\Setup\PeriodGroup;
use App\Models\Setup\PriorityArea;
use App\Models\Setup\Project;
use App\Models\Setup\ReferenceType;
use App\Models\Setup\Section;
use Illuminate\Http\Request;
use App\Models\Setup\SurplusCategory;
use App\Models\Setup\TaskNature;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Models\Setup\Version;
use Illuminate\Support\Facades\Log;


class PlanningServices
{

    public function getPlanningData($mtefSectionId,$targetId){
         $mtef = DB::table('mtefs as m')
            ->join('mtef_sections as ms','ms.mtef_id','m.id')
            ->where('ms.id',$mtefSectionId)
            ->select('m.*')
            ->first();

        $sectorId = DB::table('sections as s')
            ->join('mtef_sections as ms','ms.section_id','s.id')
            ->where('ms.id',$mtefSectionId)
            ->select('s.*')
            ->first()->sector_id;


         $cofogs = $this->getCofogs($targetId);
         $planningBudgetClasses = $this->getPlanningBudgetClasses($sectorId);
         $activityCategories = $this->getPlanningActivityCategories();
         $referenceTypes = $this->getReferenceTypeWithReference('ACTIVITY',$targetId);
         $planningPeriods = $this->getPlanningPeriods($mtef->financial_year_id);
         $taskNatures = $this->getTaskNatures();
         $surplusCat = $this->getSurplusCategories();
         $piscType = $this->getPiscTypes();

         $data =['budgetClasses'=>$planningBudgetClasses,
                 'activityCategory'=>$activityCategories,'referenceTypes'=>$referenceTypes,'mtef'=>$mtef,
                 'periods'=>$planningPeriods,'taskNatures'=>$taskNatures,
                 'cofogs'=>$cofogs,'surplusCategories' =>$surplusCat,'piscType' =>$piscType];

         return $data;
    }

    public function getActivityCategories()
    {
        $activityCategories = $this->getPlanningActivityCategories();
        $data = ['activityCategory'=>$activityCategories];
        return $data;
    }

    public function getPlanningBudgetClasses($sectorId) {
        $configBudgetIdsClassesToExclude = ConfigurationSetting::getValueByKey('BUDGET_CLASS_TO_EXCLUDE_IN_PLANNING');
        $idsToExclude = ($configBudgetIdsClassesToExclude !== null)?$configBudgetIdsClassesToExclude:[];
        $planningBudgetClasses = BudgetClass::getPlanningBudgetClasses($idsToExclude,$sectorId);
        return $planningBudgetClasses;
    }

    private  function getPlanningActivityCategories() {
        $activityCategories = ActivityCategory::where('is_active', true)->OrderBy('created_at','asc')->get();
        return $activityCategories;
    }

    private function getPlanningProjects() {
        $projects = Project::with('fund_sources')->orderBy('created_at', 'desc')->get();
        return $projects;
    }

    public function getPlanningFundSources() {

        $ceilingFundSources = DB::table('gfs_codes as g')->
            join('ceilings as c', 'g.id', 'c.gfs_code_id')->
            whereNotNull('g.fund_source_id')->distinct('g.fund_source_id')->get();

        $aggregateCeilings = DB::table('ceilings')->where('is_aggregate', true)->get();
        $ids = array();
        foreach ($ceilingFundSources as $g) {
            $ids[] = $g->fund_source_id;
        }
        foreach ($aggregateCeilings as $ac) {
            $ids[] = $ac->aggregate_fund_source_id;
        }
        $fundSources = DB::table('fund_sources as fs')->
            join('fund_source_budget_classes as fsbc', 'fs.id', 'fsbc.fund_source_id')->
            whereIn('fs.id', $ids)->
            select('fs.id', 'fs.name', 'fsbc.budget_class_id')->get();
        return $fundSources;
    }

    public static function getUserPlanningFundSources($mtefSectionId,$budgetType){
        $userSectionId = UserServices::getUser()->section_id;
        
        //Remove this if statement if you need all fund sources to be 
        //displayed even when carryover budget type is selected
        if($budgetType=='CARRYOVER'){
            // $financialYear = FinancialYear::getByBudgetType($budgetType);
            // $financial_year_id = $financialYear->id;
            // $previousYearId = $financial_year_id - 1;

            $fundSources = DB::table('fund_sources as fs')->
            join('activity_facility_fund_sources as afs', 'fs.id', 'afs.fund_source_id')->
            join('activity_facilities as af', 'af.id', 'afs.activity_facility_id')->
            join('activities as a', 'a.id', 'af.activity_id')->
            // join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')->
            // join('sections as s', 's.id', 'ms.section_id')->
            // join('mtefs as m', 'm.id', 'ms.mtef_id')->
            // where('ms.section_id',$userSectionId)->
            // where('m.financial_year_id', $previousYearId)->
            where('a.mtef_section_id',$mtefSectionId)->
            where('a.budget_type', $budgetType)->
            distinct('fs.id')->
            select('fs.id', 'fs.name')->get();
        }else{
            $fundSources = DB::table('fund_sources as fs')->
            join('activity_facility_fund_sources as afs', 'fs.id', 'afs.fund_source_id')->
            join('activity_facilities as af', 'af.id', 'afs.activity_facility_id')->
            join('activities as a', 'a.id', 'af.activity_id')->
            where('a.mtef_section_id',$mtefSectionId)->
            distinct('fs.id')->
            select('fs.id', 'fs.name')->get();
        }
        return $fundSources;
    }

    public function getReferenceTypeWithReference($linkLevel,$targetId) {

        ini_set('max_execution_time', '600');
        $userSectionId = UserServices::getUser()->section_id;
        $userSections = Section::where('id',$userSectionId)->get();
        $flatten = new Flatten();
        $sectorsIds = $flatten->flattenSectionWithChildrenGetSectors($userSections);
        //$targetId = Input::get('targetId');
        $versionId = Version::getCurrentVersionId('NRT');
        $itemIds = Version::getVesionItemIds($versionId, 'NRT');

        $referenceType = ReferenceType::with(
            ['references'=>function($reference) use($linkLevel, $targetId){
                $reference->where('link_level',$linkLevel);
                if($targetId != null){
                    $parentIds = DB::table('long_term_target_references')->where('long_term_target_id',$targetId)->pluck('reference_id');
                    $reference->whereIn('parent_id',$parentIds);
                }
                $reference->orderBy('code');
            }])
            ->whereIn('id',$itemIds)
            ->whereHas('referenceTypeSectors',function  ($query) use($sectorsIds){
                $query->whereIn('sectors.id',$sectorsIds);
            })
        ->get();

        return $referenceType;
    }

    public function getPlanningPeriods($financialYearId) {
        $configuration=ConfigurationSetting::where('key','PLANNING_PERIODS_GROUPS')->first();
        $periodGroup=null;
        if($configuration !=null){
            $periodGroup=PeriodGroup::find(intval($configuration->value));
            if($periodGroup == null){
                $periodGroup=PeriodGroup::where('number',3)->first();
            }
        }else{
            $periodGroup=PeriodGroup::where('number',3)->first();
        }
        $all = Period::where('financial_year_id',$financialYearId)->where('period_group_id',$periodGroup->id)->orderBy('start_date', 'asc')->get();
        return $all;
    }

    private function getTaskNatures() {
        return TaskNature::where('is_active', true)->get();
    }

    public function getCofogs ($targetId) {
        $cofog_id = DB::table('long_term_target_references')->select('cofog_id')->where('long_term_target_id',$targetId)->first();
        return DB::table('cofogs as child')
        ->join('cofogs as parent','parent.id','child.parent_id')
        ->select('child.id as childid','child.name as childname','child.code as childcode','parent.name as parentname','parent.code as parentcode')
        ->where('child.parent_id',$cofog_id->cofog_id)->get();

    }

    public function getSurplusCategories()
    {
        return SurplusCategory::where('is_active',true)->whereNull('deleted_at')->get();
    }

    public function getPiscTypes()
    {
        return DB::table('pisc_types as pt')
            ->join('admin_hierarchies as ah','pt.id','ah.pisc_type_id')
            ->where('ah.id',UserServices::getUser()->admin_hierarchy_id)
            ->select('pt.code')
            ->first();
    }

}
