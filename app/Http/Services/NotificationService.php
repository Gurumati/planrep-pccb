<?php

namespace App\Http\Services;

use App\Jobs\NotificationJob;

class NotificationService
{
    public static function notify($users, $message)
    {
        $notification_url = url('/notifications#!/');
        $object_id = 0;
        $icon = "fa fa-book";
        $job = new NotificationJob($users, $object_id, $notification_url, $message, $icon);
        dispatch($job->delay(10));
    }
}