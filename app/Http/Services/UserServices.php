<?php

namespace App\Http\Services;

use App\DTOs\SetupDTOs\SectionDTO;
use App\Models\Auth\FailedLoginAttempt;
use App\Models\Auth\User;
use App\Models\Budgeting\BudgetType;
use Carbon\Carbon;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Psy\Exception\ErrorException;

class UserServices {


    public function __construct() {

    }

//provide many-to-many relationship between User and Role
    // public function roles()
    // {
    //     return $this
    //         ->belongsToMany('App\Http\Services\RoleServices')
    //         ->withTimestamps();
    // }

    // public function authorizeRoles($roles)
    // {
    //     if ($this->hasAnyRole($roles)) {
    //         return true;
    //     }
    //     abort(401, 'This action is unauthorized.');
    // }
    // public function hasAnyRole($roles)
    // {
    //     if (is_array($roles)) {
    //         foreach ($roles as $role) {
    //         if ($this->hasRole($role)) {
    //             return true;
    //         }
    //         }
    //     } else {
    //         if ($this->hasRole($roles)) {
    //         return true;
    //         }
    //     }
    //     return false;
    // }
    // public function hasRole($role)
    // {
    //     if ($this->roles()->where('name', $role)->first()) {
    //         return true;
    //     }
    //     return false;
    // }

    public static function getUser(){
        return Sentinel::getUser();
    }

    public static function isLogin(){
        return Sentinel::check();
    }

    public static function hasDefaultPassword(){
        $hasher = Sentinel::getHasher();
        $user = Sentinel::getUser();
        if($hasher->check('12345', $user->password)){
            return true;
        }else{
            return false;
        }
    }

    public static function hasPermission($rights){
        return Sentinel::hasAnyAccess($rights);
    }

    public static  function authenticate($credentials,$request =null){
        // find the user
        $user = User::where('user_name', $request->user_name)->first();
        try {
              DB::table('throttle')->delete();  //disable throttle.. comment this line to enable throttle
              $remember_me =false;
              $response = Sentinel::authenticate($credentials, $remember_me);

            if ($response) {
                
               // delete login attempt if login attempt is successful
                FailedLoginAttempt::where('user_id', $user->id)->delete();

                if (isset($response->admin_hierarchy_id)) {
                    $admin_hierarchy = DB::table('admin_hierarchies')->where('id', $response->admin_hierarchy_id)->first();
                    $response->admin_hierarchy = $admin_hierarchy;
                    $response->admin_hierarchy_level = DB::table('admin_hierarchy_levels')->where('id', $admin_hierarchy->admin_hierarchy_level_id)->first();
                } else {
                    $response->admin_hierarchy = ["name"=>"","id"=>""];
                }
                if (isset($response->section_id)) {
                    $response->section = SectionDTO::find($response->section_id);
                    if($response->admin_hierarchy_level->hierarchy_position <= 2){
                        $response->section->name = "";
                    }

                } else {
                    $response->section = ["name"=>"","id"=>""];
                }

                if (isset($response->section) && isset($response->section->sector_id)) {
                    $sector = DB::table('sectors')->where('id', $response->section->sector_id)->first();
                    if($sector != null){
                        $response->sector=$sector;
                    }else{
                        $response->sector = ["name"=>"","id"=>""];
                    }
                } else {
                    $response->sector = ["name"=>"","id"=>""];
                }
                session_start();
                $clientIpAddress=$_SERVER['REMOTE_ADDR'];
                $session_id=session_id();
                $userAgent = $request->header('User-Agent');;
                session_destroy();
                $response->budgetTypes = BudgetType::getWithFinancialYears();
                $response->roles = DB::table('roles as r')->join('role_users as ru', 'ru.role_id', 'r.id')->
                where('ru.user_id', $response->id)->select('r.*')->get();
                DB::table('sessions')->insert(array('user_id' => $response->id,
                                    'ip_address'=>$clientIpAddress,
                                    'user_agent'=>$userAgent,
                                    'session'=>$session_id,
                                    'created_at'=> date("Y-m-d H:i:s")
                                ));
                track_activity($response,$response,action_name());
                return response()->json($response, 200);
            }
            else {
                $message = ["error" => 'INVALID_CREDENTIALS'];

                // check if the user exists but inactive
                if ($user && !\App\Models\Auth\Activation::where('user_id',$user->id)->first())
                    return response()->json(['errorMessage' => 'Your Account is not active. Please, contact your system administrator.', 'status'=>401]);

                if (!$user)
                   return response()->json($message, 401);

                // store login attempt only if user account is found
                FailedLoginAttempt::query()->create([
                    'user_id' => $user->id,
                    'ip_address' => $request->ip(),
                    'event_time_stamp' => Carbon::now(),
                    'message' => 'Failed login attempt',
                ]);

                // count failed login attempts and send warning message if 2 attempts reached and disabled login if 3 reached
                $login_attempts = FailedLoginAttempt::where('user_id', $user->id)->count();
                if($login_attempts == 3)
                    return response()->json(['errorMessage' => 'You have 2 attempts before your account is locked', 'status'=>500]);

                if ($login_attempts == 4)
                    return response()->json(['errorMessage' => 'You have 1 attempt before your account is locked', 'status'=>500]);

                if ($login_attempts >= 5) {
                    // lock/deactivate the user since attempts reached 5
                    Activation::remove($user);
                    // Clear login attempts so that the user is able to login again after activated later
                    FailedLoginAttempt::where('user_id', $user->id)->delete();
                    return response()->json(['errorMessage' => 'Your account is locked due to too many failed login attempts! Please, contact your system administrator.', 'status'=>500]);
                }
                return response()->json($message, 500);

            }

            }
            catch (NotActivatedException $e) {
            if ($e){
                return response()->json(['errorMessage' => 'Your Account is not active. Please, contact your system administrator.','status'=>500]);
            }
            return redirect()->back()->with(['error' => "Your Account is not activated. Please, contact your system administrator."]);
        } catch (ThrottlingException $e) {
            $delay = $e->getDelay();
            $loginError="Maximum login trials reached! You are banned for " . $delay . " seconds";
            $message=["error"=>$loginError];
            return response()->json($message, 401);
        } catch (ErrorException $errorException) {
            $loginError="Error:Check with system admin";
            $message=["error"=>$loginError];
            return response()->json($message, 401);
        }

    }

    public static function activateUser($userId){
        $user = self::findById($userId);
        Activation::remove($user);
        $activation = Activation::create($user);
        if(Activation::complete($user,$activation->code)) {
            return true;
        }
        else{
            return false;
        }
    }

    public static function deActivateUser($userId){
        $user = self::findById($userId);
        if(Activation::remove($user)) {
            return true;
        }
        else{
            return false;
        }
    }

    public static function findById($user_id){
        return Sentinel::findById($user_id);
    }

    public static function logout(){
        $userId = isset(UserServices::getUser()->id);
        session_start();
        $session_id=session_id();
        session_destroy();
        DB::table('sessions')->where('user_id', $userId)->where('is_active',true)->where('session',$session_id)->update([
            'is_active' => false,
            'updated_at'=> date("Y-m-d H:i:s")
         ]);
        return Sentinel::logout();
    }

    public static function register($credentials){
        $user = Sentinel::register($credentials);
        Activation::create($user);
        return $user;
    }

    public static function update($user, $credentials){
        return Sentinel::update($user, $credentials);
    }

    public static function registerAndActivate($credentials){
        return Sentinel::registerAndActivate($credentials);
    }
}
