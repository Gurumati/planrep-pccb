<?php

/**
 * @resource BudgetExportService
 *
 * This Service provide shared service for various budget account (GfsCode Account) maniputation after budget approval
 */

namespace App\Http\Services\Budgeting;


use App\Models\Execution\BudgetExportAccount;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use App\Models\Execution\FfarsActivity;
use App\Models\Execution\FfarsActualControlId;
use App\Models\Setup\GfsCode;
use App\Models\Setup\FundSource;
use App\Models\Setup\BudgetClass;
use Illuminate\Support\Facades\Log;

class BudgetExportAccountService
{

    public static function createBudgetAccounts($admin_hierarchy_id, $financial_year_id, $Ids, $budget_type = null)
    {
        $reference_number = strtotime("now");
        $accountCreated = [];
        $inputs = BudgetExportAccount::loadInputs($admin_hierarchy_id, $financial_year_id, $Ids, $budget_type);
         if (count($inputs) > 0) {
            foreach ($inputs as $input) {
                /**
                 * Check if account exist
                 */
                $count = BudgetExportAccount::where('activity_facility_fund_source_input_id', $input->activity_facility_fund_source_input_id)->count();

                if ($count == 0) {

                    /**
                     * Create Account which doest exist
                     */
                    $budget_export_account = new BudgetExportAccount();
                    $budget_export_account->admin_hierarchy_id = $input->admin_hierarchy_id;
                    $budget_export_account->section_id = $input->section_id;
                    $budget_export_account->financial_year_id = $input->financial_year_id;
                    $budget_export_account->activity_facility_fund_source_input_id = $input->activity_facility_fund_source_input_id;
                    $budget_export_account->activity_id = $input->activity_id;
                    $budget_export_account->gfs_code_id = $input->gfs_code_id;
                    $budget_export_account->financial_system_code = 'MUSE';
//                    $budget_export_account->financial_system_code = ($input->ftypecode == '001') ? 'MUSE' : null;
                    $budget_export_account->amount = $input->amount;
                    $budget_export_account->reference_number = $reference_number;

                    /**
                     * generate Chart of account (COA)
                     * add COFOG and Geo Location to COA 2020-05-01 for OTR
                     * and later to be used by central gov and lgas
                     * mazigo joe 2020-05-04 12:56
                     */
                    $coa = BudgetExportAccountService::getChartOfAccount(
                        $input->vote,
                        $input->sub_vote,
                        $input->council,
                        $input->cost_centre,
                        $input->sub_budget_class_code,
                        $input->service_output_code,
                        $input->facility_code,
                        $input->project_code,
                        $input->activity_code,
                        $input->fund_type_code,
                        $input->fund_source_code,
                        $input->gfs_code,
                        $input->location_code,
                        $input->cofog_code,
                        $input->ftypecode
                    );
                    $budget_export_account->chart_of_accounts = $coa;
                    $budget_export_account->save();

                    if (isset($Ids))
                        array_push($accountCreated, $budget_export_account);
                }

            }
        }

        return $accountCreated;
    }

    public static function removeBudgetAccounts($admin_hierarchy_id, $financial_year_id, $budget_type = null)
    {
        try {
            //get inputs
            $inputs = BudgetExportAccount::loadInputs($admin_hierarchy_id, $financial_year_id, $budget_type);
            foreach ($inputs as $input) {
                DB::table('budget_export_accounts')
                    ->where('admin_hierarchy_id', $admin_hierarchy_id)
                    ->where('financial_year_id', $financial_year_id)
                    ->where('activity_facility_fund_source_input_id', $input->activity_facility_fund_source_input_id)
                    ->delete();
            }
            return true;
        } catch (QueryException $exception) {
            return false;
        }

    }

    public static function removeFFARSactivities($admin_hierarchy_id, $financial_year_id)
    {
        try {
            DB::table('ffars_activities')
                ->where('admin_hierarchy_id', $admin_hierarchy_id)
                ->where('financial_year_id', $financial_year_id)
                ->where('is_delivered_to_ffars', false)
                ->delete();
            return true;
        } catch (QueryException $exception) {
            return false;
        }
    }

    public static function getChartOfAccount(
        $vote,
        $sub_vote,
        $council,
        $cost_centre,
        $sub_budget_class_code,
        $service_output_code,
        $facility_code,
        $project_code,
        $activity_code,
        $fund_type_code,
        $fund_source_code,
        $gfs_code,
        $location_code,
        $cofog_code,
        $ftypecode
    ) {

            if($ftypecode == 2){

                $zone = substr($facility_code,3,1);
                $reg = substr($location_code,-6,-4);
                $zero = '0000';
                $location_code = $zone.$reg.$zero;
                $facility_code = '00000000'; 

            }elseif($ftypecode == 3 || $ftypecode == 5){

                $facility_code = '00000000';
                $reg = substr($location_code,-7,-4);
                $zero = '0000';
                $location_code = $reg.$zero;

            }elseif($ftypecode == 6){

                $facility_code = '00000000';
                $location_code = $location_code;

            }elseif($ftypecode == 7){

                $zone = substr($facility_code,3,1);
                $loc = substr($location_code,1);
                $location_code = $zone.$loc;
                $facility_code = $facility_code;

            }else{
                $facility_code = '00000000';
                $location_code = '0000000';
            }
            

            $a = substr($activity_code,0,1);
            $b = substr($activity_code,-5);
            $new_activity_code = $a.$b;

            $so_code_len = strlen($service_output_code);
            if($so_code_len == 4){
                $so_code = substr($service_output_code, 0, -1);
            }elseif($new_activity_code == '000000'){
                $so_code = '001';
            }else{
                $so_code = $service_output_code;
            }

            $a = substr($activity_code,0,1);
            $b = substr($activity_code,-5);
            $new_activity_code = $a.$b;

        $coa = str_pad($vote, 3, '0', STR_PAD_LEFT) . "-" .$sub_vote."-".$council . "-" . $cost_centre . "-" .$location_code."-".$facility_code."-". $sub_budget_class_code . "-". $project_code . "-" .$so_code."-". $new_activity_code . "-" . $fund_type_code . "-".$cofog_code."-" . $fund_source_code . "-" . $gfs_code;
        return $coa;
    }

    /** generate ffars activities */
    public static function createActivities($mtef, $budget_type = null)
    {
        $mtefId = $mtef->id;
        $sql = "SELECT
                  ah.name as admin_hierarchy,
                  ft.name as facility_type,
                  f.name as facility,
                  p.name as project,
                  b.name as sub_budget_class,
                  p.code as project_code,
                  b.code as sub_budget_class_code,
                  a.description,
                  a.id as activity_id,
                  a.code as activity_code,
                  f.facility_code as facility_code,
                  a.budget_type,
                  (SELECT p.start_date from activity_periods ap join periods p on p.id = ap.period_id where ap.activity_id = a.id limit 1) as apply_date
                FROM activities a
                join budget_classes b on a.budget_class_id = b.id
                join projects p on a.project_id = p.id
                join activity_facilities af on a.id = af.activity_id
                join facilities f on af.facility_id = f.id
                join facility_types ft on f.facility_type_id = ft.id
                join mtef_sections m on a.mtef_section_id = m.id
                join mtefs mt on mt.id = m.mtef_id
                join admin_hierarchies ah on mt.admin_hierarchy_id = ah.id
                join activity_facility_fund_sources affs on af.id = affs.activity_facility_id
                join activity_facility_fund_source_inputs affi on affi.activity_facility_fund_source_id = affs.id
                left join ffars_activities fa on
                  (fa.admin_hierarchy_id = ah.id and fa.financial_year_id::integer = mt.financial_year_id
                  and fa.budget_type = '$budget_type' and fa.activity_code = a.code and fa.facility_code = f.facility_code)
                where
                mt.id = '$mtefId' and a.budget_type = '$budget_type' and ft.id in (8,9,10,11,12,14)
                and fa.id is null and f.deleted_at is null and a.deleted_at is null and f.is_active = true
                GROUP BY
                ah.id, ft.id,  p.id, b.id, a.id, f.id";
        $activities = DB::select($sql);
        foreach ($activities as $activity) {
                    //check if activity exists
            if (BudgetExportAccountService::ffarsActivityExists($activity, $mtef->admin_hierarchy_id, $mtef->financial_year_id, $activity->budget_type) == false) {
                $fa = new FfarsActivity();
                $fa->admin_hierarchy = $activity->admin_hierarchy;
                $fa->admin_hierarchy_id = $mtef->admin_hierarchy_id;
                $fa->financial_year_id = $mtef->financial_year_id;
                $fa->facility_type = $activity->facility_type;
                $fa->facility = $activity->facility;
                $fa->project = $activity->project;
                $fa->sub_budget_class = $activity->sub_budget_class;
                $fa->sub_budget_class_code = $activity->sub_budget_class_code;
                $fa->project_code = $activity->project_code;
                $fa->activity_code = $activity->activity_code;
                $fa->description = $activity->description;
                $fa->apply_date = $activity->apply_date;
                $fa->facility_code = $activity->facility_code;
                $fa->budget_type = $activity->budget_type;
                $fa->save();
            }
        }
    }
    //ffars activity exist
    public static function ffarsActivityExists($activity, $admin_hierarchy_id, $financial_year_id, $budget_type)
    {
        $result = FfarsActivity::where('admin_hierarchy_id', $admin_hierarchy_id)
            ->where('facility_code', $activity->facility_code)
            ->where('activity_code', $activity->activity_code)
            ->where('financial_year_id', $financial_year_id)
            ->where('budget_type', $budget_type)
            ->select('id')
            ->count();
        if ($result > 0) {
            return true;
        } else {
            return false;
        }
    }

    /** regenerate chart of accounts */
    public static function resetBudget($admin_hierarchy_id, $financial_year_id, $budget_type)
    {
        $reference_number = strtotime("now");
        $accountUpdated = [];
        $inputs = BudgetExportAccount::loadInputs($admin_hierarchy_id, $financial_year_id, null, $budget_type);
        if (count($inputs) > 0) {
            foreach ($inputs as $input) {
                 //get budget export account id
                $result = DB::table('budget_export_accounts')
                    ->where('activity_facility_fund_source_input_id', $input->activity_facility_fund_source_input_id)
                    ->select('id', 'financial_system_code');
                /** update budget export accounts for facility accounts only */
                if ($result->count() == 1) {
                    $account_id = $result->get();
                    if ($account_id[0]->financial_system_code == 'MUSE') {
                        $budget_export_account = BudgetExportAccount::find($account_id[0]->id);
                        $budget_export_account->reference_number = $reference_number;
                        $coa = BudgetExportAccountService::getChartOfAccount(
                            $input->vote,
                            $input->sub_vote,
                            $input->council,
                            $input->cost_centre,
                            $input->sub_budget_class_code,
                            $input->service_output_code,
                            $input->facility_code,
                            $input->project_code,
                            $input->activity_code,
                            $input->fund_type_code,
                            $input->fund_source_code,
                            $input->gfs_code,
                            $input->location_code,
                            $input->cofog_code,
                            $input->ftypecode
                        );
                        $budget_export_account->chart_of_accounts = $coa;
                        $budget_export_account->save();
                        array_push($accountUpdated, $budget_export_account->id);
                    }
                } else {
                    //create input
                    $budget_export_account = new BudgetExportAccount();
                    $budget_export_account->admin_hierarchy_id = $input->admin_hierarchy_id;
                    $budget_export_account->section_id = $input->section_id;
                    $budget_export_account->financial_year_id = $input->financial_year_id;
                    $budget_export_account->activity_facility_fund_source_input_id = $input->activity_facility_fund_source_input_id;
                    $budget_export_account->activity_id = $input->activity_id;
                    $budget_export_account->gfs_code_id = $input->gfs_code_id;
                   // $budget_export_account->financial_system_code = ($input->is_facility_account) ? 'FFARS' : 'EPICOR';
                    $budget_export_account->financial_system_code = 'MUSE';
                    $budget_export_account->amount = $input->amount;
                    $budget_export_account->reference_number = $reference_number;

                    /**
                     * generate Chart of account (COA)
                     */
                    $coa = BudgetExportAccountService::getChartOfAccount(
                        $input->vote,
                        $input->sub_vote,
                        $input->council,
                        $input->cost_centre,
                        $input->sub_budget_class_code,
                        $input->service_output_code,
                        $input->facility_code,
                        $input->project_code,
                        $input->activity_code,
                        $input->fund_type_code,
                        $input->fund_source_code,
                        $input->gfs_code,
                        $input->location_code,
                        $input->cofog_code,
                        $input->ftypecode
                    );
                    $budget_export_account->chart_of_accounts = $coa;

                    $budget_export_account->save();
                }
            }
        }
        return $accountUpdated;
    }

    /** function to regenerate budget export accounts */
    public static function regenerateAllAccounts()
    {
        $financial_year_id = 1;
        $admin_hierarchies = DB::table('tmp_reset_council')
            ->where('status', false)
            ->select('id')
            ->get();
        foreach ($admin_hierarchies as $item) {
            BudgetExportAccountService::resetBudget($item->id, $financial_year_id, 'APPROVED');
            BudgetExportAccountService::resetBudget($item->id, $financial_year_id, 'SUPPLEMENTARY');
            BudgetExportAccountService::resetBudget($item->id, $financial_year_id, 'CARRYOVER');
            //update tmp_reset_council
            DB::statement("UPDATE tmp_reset_council set status = true where id = $item->id");
        }
    }

    public static function regenerateMissingAccounts(){
        $missing_accounts = DB::table('budget_import_missing_accounts')
                            ->where('financial_system_code', 'FFARS')
                            ->where('resolved', false)
                            ->get();
        foreach($missing_accounts as $item){
            $coa = $item->chart_of_accounts;
            $new_coa = substr_replace($coa, '%',41,1);
            //compare coa
            $result = BudgetExportAccount::where('chart_of_accounts','ilike',$new_coa)
                           ->where('financial_system_code','FFARS')
                           ->get();
            if(isset($result[0])){
                //update account
                BudgetExportAccount::where('id',$result[0]->id)
                                     ->update(['chart_of_accounts'=>$coa]);

                   //Insert a record in the ffars_control_ids table
                    /**
                    * Insert the transaction
                    */
                    //get priod
                    $date_entry =  $item->JEDate;
                    $query = "select * from periods where '$date_entry'::date between start_date and end_date and periods.period_group_id != 8";
                    $get_period = DB::select(DB::raw($query));
                    $period_id = isset($get_period[0]->id) ? $get_period[0]->id: null;

                    $array = explode('-', $coa);
                    $gfs_code_from_ffars = $array[9];
                    $gfs_code = GfsCode::where('code','=',$gfs_code_from_ffars)->first();
                    $gfs_code_id = isset($gfs_code->id) ? $gfs_code->id : null;

                    $budget_class_from_ffars = $array[3];
                    $budget_class = BudgetClass::where('code','=',$budget_class_from_ffars)->first();
                    $budget_class_id = isset($budget_class->id) ? $budget_class->id : null;

                    $fund_source_from_ffars = $array[8];
                    $fund_source = FundSource::where('code','=',$fund_source_from_ffars)->first();
                    $fund_source_id = isset($fund_source->id) ? $fund_source->id : null;

                    $expenditure_data = array(
                        'budget_export_account_id' => $result[0]->id,
                        'period_id' => $period_id,
                        'cancelled' => false,
                        'BookID' => $item->bookID,
                        'FiscalYear' => $item->financial_year_id,
                        'JEDate' => $item->JEDate,
                        'Account' => $coa,
                        'date_imported' => $item->created_at,
                        'gfs_code_id' => $gfs_code_id,
                        'budget_class_id' => $budget_class_id,
                        'fund_source_id' => $fund_source_id,
                        'debit_amount' => $item->debit_amount,
                        'credit_amount' => $item->credit_amount,
                        'budget_type' => 'NA',
                        'created_at' => \Carbon\Carbon::now()
                    );
                    DB::table('budget_import_items')->insert([$expenditure_data]);

                    //flag missing account as resolved
                    DB::table('budget_import_missing_accounts')
                    ->where('id', $item->id)
                    ->update(['resolved'=>true]);
            }
        }
    }
}
