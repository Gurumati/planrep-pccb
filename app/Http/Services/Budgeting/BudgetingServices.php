<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 8/28/2017
 * Time: 12:50 PM
 */

namespace App\Http\Services\Budgeting;

use App\Http\Services\FinancialYearServices;
use App\Http\Services\VersionServices;
use App\Models\Setup\BudgetClassVersion;
use App\Models\Setup\FundSource;
use Illuminate\Support\Facades\DB;


class BudgetingServices
{

    public static function getTotalUsedByCeiling($mtefSectionId, $budgetTypes, $budgetClassId, $fundSourceId){
        $inputsTotals = DB::table('activity_facility_fund_source_inputs as inp')->
                        join('activity_facility_fund_sources as fu','fu.id','inp.activity_facility_fund_source_id')->
                        join('activity_facilities as fa','fa.id','fu.activity_facility_id')->
                        join('activities as a','a.id','fa.activity_id')->
                        where('a.mtef_section_id',$mtefSectionId)->
                        whereIn('a.budget_type',$budgetTypes)->
                        where('a.budget_class_id',$budgetClassId)->
                        where('fu.fund_source_id',$fundSourceId)->
                        whereNull('inp.deleted_at')->
                        select(DB::Raw('Coalesce(SUM(inp.quantity*inp.frequency*inp.unit_price),0) as total'))->get();
        return $inputsTotals[0]->total;
    }
    public static function getTotalUsedByFacilityCeiling($mtefSectionId, $budgetTypes, $budgetClassId, $fundSourceId, $facilityId){
        $inputsTotals = DB::table('activity_facility_fund_source_inputs as inp')
        ->join('activity_facility_fund_sources as fu', 'fu.id', 'inp.activity_facility_fund_source_id')
        ->join('activity_facilities as fa', 'fa.id', 'fu.activity_facility_id')
        ->join('activities as a', 'a.id', 'fa.activity_id')->
            // where('a.is_facility_account',true)->
            where('a.mtef_section_id', $mtefSectionId)
            ->whereIn('inp.budget_type', $budgetTypes)
            ->where('a.budget_class_id', $budgetClassId)
            ->where('fa.facility_id', $facilityId)
            ->where('fu.fund_source_id', $fundSourceId)
            ->whereNull('inp.deleted_at')
            ->select(DB::Raw('Coalesce(SUM(inp.quantity*inp.frequency*inp.unit_price),0) as total'))->get();

        return $inputsTotals[0]->total;
    }
    public static function getTheCeiling($budgetClassId,$fundSourceId){
        $planningYear = FinancialYearServices::getPlanningFinancialYear();
        $financialYearId =isset($planningYear)?$planningYear->id:FinancialYearServices::getExecutionFinancialYear()->id;
        $fundSource = FundSource::where('id', $fundSourceId)->first();

        $versionFsId = VersionServices ::getVersionId($financialYearId,'FS');
        $versionBcId = VersionServices ::getVersionId($financialYearId,'BC');
        $fundSourceIds = FundSource::getIdsByVersion($versionFsId);
        $budgetClassIds = BudgetClassVersion::where('version_id',$versionBcId)->pluck('budget_class_id')->toArray();

        if ($fundSource->can_project) {
            $theCeiling = DB::table('ceilings')
            ->where('budget_class_id', $budgetClassId)
            ->where('aggregate_fund_source_id', $fundSourceId)->first();
        } else {
            $theCeiling = DB::table('ceilings as c')
            ->join('gfs_codes as gfs', 'c.gfs_code_id', 'gfs.id')
            ->join('gfscode_fundsources as gfsf','gfsf.gfs_code_id','gfs.id')
            ->where('c.budget_class_id', $budgetClassId)
            ->where('gfsf.fund_source_id', $fundSourceId)
            ->whereIn('c.budget_class_id', $budgetClassIds)
            ->whereIn('gfsf.fund_source_id', $fundSourceIds)
            ->select('c.*')->first();
        }
        return $theCeiling;
    }
    public static function getFacilityCeiling($mtefSectionId,$budgetTypes,$budgetClassId,$fundSourceId,$facilityId,$financialYearId = null){
        if(!isset($financialYearId)){
            $planningYear = FinancialYearServices::getPlanningFinancialYear();
            $financialYearId =isset($planningYear)?$planningYear->id:FinancialYearServices::getExecutionFinancialYear()->id;
            }
    
            $versionFsId = VersionServices ::getVersionId($financialYearId,'FS');
            $versionBcId = VersionServices ::getVersionId($financialYearId,'BC');
            $fundSourceIds = FundSource::getIdsByVersion($versionFsId);
            $budgetClassIds = BudgetClassVersion::where('version_id',$versionBcId)->pluck('budget_class_id')->toArray();
    
            $mtefSection = DB::table('mtef_sections')->find($mtefSectionId);
            $mtef = DB::table('mtefs')->find($mtefSection->mtef_id);
            $fundSource = FundSource::where('id', $fundSourceId)->first();
    
            if ($fundSource->can_project) {
                $ceilings = DB::table('admin_hierarchy_ceilings as adc')
                ->join('ceilings as c', 'c.id', 'adc.ceiling_id')
                ->where('c.budget_class_id', $budgetClassId)
                ->where('c.aggregate_fund_source_id', $fundSourceId)
                ->where('adc.facility_id', $facilityId)
                ->where('adc.admin_hierarchy_id', $mtef->admin_hierarchy_id)
                ->where('adc.section_id', $mtefSection->section_id)
                ->where('adc.financial_year_id', $mtef->financial_year_id)
                ->whereIn('adc.budget_type', $budgetTypes)
                ->where('c.is_active', true)
                ->select(DB::Raw('Coalesce(SUM(adc.amount),0) as total'))->get();
            } else {
                $ceilings = DB::table('admin_hierarchy_ceilings as adc')
                ->join('ceilings as c', 'c.id', 'adc.ceiling_id')
                ->join('gfs_codes as gfs', 'gfs.id', 'c.gfs_code_id')
                ->join('gfscode_fundsources as gfsf','gfsf.gfs_code_id','gfs.id')
                ->where('c.budget_class_id', $budgetClassId)
                ->where('c.budget_class_id', $budgetClassId)
                ->where('gfsf.fund_source_id', $fundSourceId)
                ->whereIn('c.budget_class_id', $budgetClassIds)
                ->whereIN('gfsf.fund_source_id', $fundSourceIds)
                ->where('adc.facility_id', $facilityId)
                ->where('adc.admin_hierarchy_id', $mtef->admin_hierarchy_id)
                ->where('adc.section_id', $mtefSection->section_id)
                ->where('adc.financial_year_id', $mtef->financial_year_id)
                ->whereIn('adc.budget_type', $budgetTypes)
                ->where('c.is_active', true)
                ->select(DB::Raw('Coalesce(SUM(adc.amount),0) as total'))->get();
            }
    
            return $ceilings[0]->total;
    }

    public static function getCeiling($mtefSectionId,$budgetTypes,$budgetClassId,$fundSourceId){

        $mtefSection = DB::table('mtef_sections')->find($mtefSectionId);
        $mtef = DB::table('mtefs')->find($mtefSection->mtef_id);
        $fundSource = FundSource::where('id',$fundSourceId)->first();

        if($fundSource->can_project){
            $ceilings = DB::table('admin_hierarchy_ceilings as adc')->
                join('ceilings as c','c.id','adc.ceiling_id')->
                where('c.budget_class_id',$budgetClassId)->
                where('c.aggregate_fund_source_id',$fundSourceId)->
                where('adc.is_facility',false)->
                where('adc.financial_year_id',$mtef->financial_year_id)->
                 whereIn('adc.budget_type',$budgetTypes)->
                where('adc.admin_hierarchy_id',$mtef->admin_hierarchy_id)->
                where('adc.section_id',$mtefSection->section_id)->
                where('c.is_active',true)->
                select(DB::Raw('Coalesce(SUM(adc.amount),0) as total'))->get();

        }
        else{
            $ceilings = DB::table('admin_hierarchy_ceilings as adc')->
                    join('ceilings as c','c.id','adc.ceiling_id')->
                    join('gfs_codes as gfs','gfs.id','c.gfs_code_id')->
                    where('c.budget_class_id',$budgetClassId)->
                    where('gfs.fund_source_id',$fundSourceId)->
                    where('adc.is_facility',false)->
                    where('adc.admin_hierarchy_id',$mtef->admin_hierarchy_id)->
                    where('adc.section_id',$mtefSection->section_id)->
                    where('adc.financial_year_id',$mtef->financial_year_id)->
                    whereIn('adc.budget_type',$budgetTypes)->
                    where('c.is_active',true)->
                    select(DB::Raw('Coalesce(SUM(adc.amount),0) as total'))->get();

        }
        return  $ceilings[0]->total;

    }

}