<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 4/20/2017
 * Time: 12:13 PM
 */

namespace App\Http\Services\Budgeting;

use App\Http\Controllers\Flatten;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Budgeting\Scrutinization;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\Section;
use Illuminate\Support\Facades\DB;


class ScrutinizationService {

    public function getSubmittedHierarchies() {

        $userAdminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $userDecisionLevelId = UserServices::getUser()->decision_level_id;
        $userAdminHierarchy = AdminHierarchy::find($userAdminHierarchyId);
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $financialYearId = $financialYear->id;

        $adminHierarchies = AdminHierarchy::where('id', $userAdminHierarchyId)->get();
        $flatten = new Flatten();
        $adminIds = $flatten->flattenAdminHierarchyWithChild($adminHierarchies);

        $submittedHierarchies = DB::table('mtefs as m')->
        join('decision_levels as d', 'd.id', 'm.decision_level_id')->
        join('decision_level_admin_hierarchies as dal', 'dal.decision_level_id', 'd.id')->
        where('m.financial_year_id', $financialYearId)->
        where('m.decision_level_id', $userDecisionLevelId)->
        whereIn('m.admin_hierarchy_id', $adminIds)->
        where('dal.admin_hierarchy_level_id', $userAdminHierarchy->admin_hierarchy_level_id)->
        select('m.*')->get();

        return $submittedHierarchies;
    }

    public function getSubmittedMtefSections() {
        $userSectionId = UserServices::getUser()->section_id;
        $userSection = Section::find($userSectionId);
        $userAdminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $financialYearId = $financialYear->id;
        $sections = Section::with('childSections')->where('id', $userSectionId)->get();
        $flatten = new Flatten();
        $sectionIds = $flatten->flattenSectionWithChild($sections);
        $myAdminSectionSubmittedBudgets = DB::table('mtef_sections as ms')->
            join('mtefs as m', 'm.id', 'ms.mtef_id')->
            join('decision_levels as d', 'd.id', 'm.decision_level_id')->
            where('m.financial_year_id', $financialYearId)->
            where('d.is_default', true)->
            whereIn('ms.section_id', $sectionIds)->
            where('m.admin_hierarchy_id', $userAdminHierarchyId)->
            where('ms.section_level_id', $userSection->section_level_id)->
            select('ms.id','ms.section_id')->get();

        return $myAdminSectionSubmittedBudgets;
    }
    public  function getScrutinizationData($scutinizedAdminId, $scrutinizedSectionId,$sectionIds){
        $financialYear=FinancialYearServices::getPlanningFinancialYear();
        $admin_hierarchy_id=UserServices::getUser()->admin_hierarchy_id;
        $section_id=UserServices::getUser()->section_id;
        $decision_level_id=UserServices::getUser()->decision_level_id;
        $user_id=UserServices::getUser()->id;
        $opened=Scrutinization::where('admin_hierarchy_id',$admin_hierarchy_id)->
        whereIn('section_id',$sectionIds)->
        where('financial_year_id',$financialYear->id)->
        where('decision_level_id',$decision_level_id)->
        where('scrutinized_admin_hierarchy_id',$scutinizedAdminId)->
        where('scrutinized_section_id',$scrutinizedSectionId)->
        whereIn('status',[0,1])->first();

        if($opened == null) {
            $scrutinization = new Scrutinization();
            $scrutinization->admin_hierarchy_id = $admin_hierarchy_id;
            $scrutinization->section_id = $section_id;
            $scrutinization->financial_year_id = $financialYear->id;
            $scrutinization->decision_level_id = $decision_level_id;
            $scrutinization->scrutinized_admin_hierarchy_id =$scutinizedAdminId ;
            $scrutinization->scrutinized_section_id =$scrutinizedSectionId;
            $scrutinization->status = 0;
            $scrutinization->user_id=$user_id;
            $scrutinization->comments = "";
            $scrutinization->save();
            $opened=$scrutinization;

        }

        return $opened;

    }

    public  function getAllScrutinizationData($scutinizedAdminId){
        $financialYear=FinancialYearServices::getPlanningFinancialYear();
        $admin_hierarchy_id=UserServices::getUser()->admin_hierarchy_id;
        $decision_level_id=UserServices::getUser()->decision_level_id;
        $opened=Scrutinization::where('admin_hierarchy_id',$admin_hierarchy_id)->
            where('financial_year_id',$financialYear->id)->
            where('decision_level_id',$decision_level_id)->
            where('scrutinized_admin_hierarchy_id',$scutinizedAdminId)->
            whereIn('status',[0,1])->get();

        return $opened;

    }
    public  function getAllScrutinizationDataBySection($scutinizedAdminId,$scutinizedSectionId){

        $financialYear=FinancialYearServices::getPlanningFinancialYear();
        $admin_hierarchy_id=UserServices::getUser()->admin_hierarchy_id;
        $decision_level_id=UserServices::getUser()->decision_level_id;
        $userSectionId=UserServices::getUser()->section_id;
        $query = Scrutinization::where('admin_hierarchy_id',$admin_hierarchy_id)->
        where('financial_year_id',$financialYear->id)->
        where('section_id',$userSectionId)->
        where('decision_level_id',$decision_level_id)->
        where('scrutinized_admin_hierarchy_id',$scutinizedAdminId)->
        where('scrutinized_section_id',$scutinizedSectionId)->
        whereIn('status',[0,1]);

        $opened=$query->first();
        $count=$query->count();

        if($count > 0){
            return $opened->id;
        } else {
            return null;
        }



    }
    public  function getAllScrutinizationDataByAdmin($scutinizedAdminId){

        $financialYear=FinancialYearServices::getPlanningFinancialYear();
        $admin_hierarchy_id=UserServices::getUser()->admin_hierarchy_id;
        $decision_level_id=UserServices::getUser()->decision_level_id;
        $userSectionId=UserServices::getUser()->section_id;
        $query = Scrutinization::where('admin_hierarchy_id',$admin_hierarchy_id)->
            where('financial_year_id',$financialYear->id)->
            where('section_id',$userSectionId)->
            where('decision_level_id',$decision_level_id)->
            where('scrutinized_admin_hierarchy_id',$scutinizedAdminId)->
            whereIn('status',[0,1])->get();

       return $query;

    }

    public  function getScrutinizationRound($scutinizedAdminId, $scrutinizedSectionId,$sectionIds){
        $financialYear=FinancialYearServices::getPlanningFinancialYear();
        $admin_hierarchy_id=UserServices::getUser()->admin_hierarchy_id;
        $section_id=UserServices::getUser()->section_id;
        $decision_level_id=UserServices::getUser()->decision_level_id;
        $user_id=UserServices::getUser()->id;

        $round=Scrutinization::where('admin_hierarchy_id',$admin_hierarchy_id)->
            whereIn('section_id',$sectionIds)->
            where('financial_year_id',$financialYear->id)->
            where('decision_level_id',$decision_level_id)->
            where('scrutinized_admin_hierarchy_id',$scutinizedAdminId)->
            where('scrutinized_section_id',$scrutinizedSectionId)->
            count();
        return $round;

    }
    public  function getScrutinizationStatus($Ids,$isTopSection)
    {
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section_id = UserServices::getUser()->section_id;
        $decision_level_id = UserServices::getUser()->decision_level_id;
        $user_id = UserServices::getUser()->id;
        if ($isTopSection){
            $statuses = Scrutinization::where('admin_hierarchy_id', $admin_hierarchy_id)->
            whereIn('section_id', $Ids)->
            where('financial_year_id', $financialYear->id)->
            where('decision_level_id', $decision_level_id)->
            whereIn('scrutinized_section_id', $Ids)->
            whereIn('status', [0, 1])->select('scrutinized_section_id as section_id', 'scrutinized_admin_hierarchy_id as admin_hierarchy_id', 'status', 'recommendation')->get();
         }else{
            $statuses = Scrutinization::where('admin_hierarchy_id', $admin_hierarchy_id)->
            whereIn('section_id', $Ids)->
            where('financial_year_id', $financialYear->id)->
            where('decision_level_id', $decision_level_id)->
            whereIn('scrutinized_section_id', $Ids)->
            whereIn('status', [0, 1])->select('scrutinized_section_id as section_id', 'scrutinized_admin_hierarchy_id as admin_hierarchy_id', 'status', 'recommendation')->get();
         }
        return $statuses;

    }


}