<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 11/4/17
 * Time: 12:44 PM
 */

namespace App\Http\Services;


use App\Models\Auth\Role;

class RoleServices
{
//provide many-to-many relationship between User and Role
    // public function users()
    // {
    //     return $this
    //         ->belongsToMany('App\Http\Services\UserServices')
    //         ->withTimestamps();
    // }


   public static function findById($id){
       return Role::find($id);
   }
}