<?php

namespace App\Http\Services;

use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\AdminHierarchyLevel;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\FinancialYear;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FinancialYearServices {


    public function __construct() {

    }

    public function getFinancialYearByEventAndHierarchyPosition($eventNumber, $hierarchyPosition) {

        $today = date("Y-m-d");
        return DB::table('financial_years as f')->
                join('calendars as c', 'f.id', 'c.financial_year_id')->
                join('calendar_events as ce', 'ce.id', 'c.calendar_event_id')->
                whereIn('f.status', [1,2])->
                where('ce.number', $eventNumber)->
                where('c.hierarchy_position', $hierarchyPosition)->
                where('c.start_date', '<=', $today)->
                where('c.end_date', '>=', $today)->
                orderBy('f.start_date', 'desc')->select('f.*')->first();
    }
    public function getFinancialYearByEvent($eventNumber) {
        $userAdminHierarchy=AdminHierarchy::find(UserServices::getUser()->admin_hierarachy_id);
        $userAdminHierarchyLevel=AdminHierarchyLevel::find($userAdminHierarchy->admin_hierarchy_position);
        $today = date("Y-m-d");
        return DB::table('financial_years as f')->
                join('calendars as c', 'f.id', 'c.financial_year_id')->
                join('calendar_events as ce', 'ce.id', 'c.calendar_event_id')->
                whereIn('f.status', [1,2])->
                where('ce.number', $eventNumber)->
                where('c.hierarchy_position',$userAdminHierarchyLevel->hierarchy_position)->
                where('c.start_date', '<=', $today)->
                where('c.end_date', '>=', $today)->
                orderBy('f.start_date', 'desc')->select('f.*')->first();
    }

    public function getTotalFinancialYearByEventAndHierarchyPosition($eventNumber, $hierarchyPosition) {
        $today = date("Y-m-d");
        return DB::table('financial_years as f')->
                join('calendars as c', 'f.id', 'c.financial_year_id')->
                join('calendar_events as ce', 'ce.id', 'c.calendar_event_id')->
                whereIn('f.status', 1)->
                where('ce.number', $eventNumber)->
                where('c.hierarchy_position', $hierarchyPosition)->
                where('c.start_date', '<=', $today)->
                where('c.end_date', '>=', $today)->
                orderBy('f.start_date', 'desc')->select('f.*')->count();
    }

    public static function getExecutionFinancialYear(){
        return FinancialYear::where('status',2)->first();
    }

    public static function getPlanningFinancialYear(){
        $financialYear = FinancialYear::where('status',1)->first();
         if(!isset($financialYear)){
           $financialYearId = ConfigurationSetting::getValueByKey('AUTO_FINANCIAL_YEAR_TO_REPORT');
           $financialYear = FinancialYear::find($financialYearId);
        }
        return $financialYear;
    }

    public static function getNextFinancialYear(){
        return FinancialYear::where('status',3)->first();
    }
}
