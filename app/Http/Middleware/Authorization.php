<?php

namespace App\Http\Middleware;

use App\Http\Services\UserServices;
use Closure;

class Authorization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * @param ...rights =array of rights
     */
    public function handle($request, Closure $next,...$rights)
    {
        if(UserServices::hasPermission($rights)){
            return $next($request);
        } else {
            $message="UNAUTHORIZED";
            return response()->json($message, 403);
        }

    }
}
