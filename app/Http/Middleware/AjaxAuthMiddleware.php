<?php

namespace App\Http\Middleware;

use App\Http\Services\UserServices;
use Closure;

class AjaxAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(UserServices::isLogin()){
            if(!UserServices::hasDefaultPassword()) {
                return $next($request);
            }
            else{
                $message="Please change default password";
                return response()->json($message, 402);
            }
        } else {
            $message="Please Login";
            return response()->json($message, 401);
        }
    }
}
