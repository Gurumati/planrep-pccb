<?php

namespace App\Http\Controllers\Security;

use App\Models\Security\Audit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuditController extends Controller
{
    public function __construct() {
        // apply all necessary middlewares here
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $audits = Audit::with('user')->get();
        return response()->json($audits, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Security\Audit  $audit
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $audit = Audit::findOrFail($id);
        return response()->json($audit);
    }

    /**
     * Display the all resource by a specific user based on id of the user.
     *
     * @param  \App\Models\Auth\User  $user_id
     * @return \Illuminate\Http\Response
     */
    public function auditsByUser($user_id) {
        $audits = Audit::where('user_id', $user_id)->get();
        return response()->json($audits);
    }
}
