<?php

namespace App\Http\Controllers\Report;

use App\Models\Setup\Facility;
use App\Models\Setup\FundSource;
use App\Models\Setup\GfsCode;
use App\Models\Setup\Section;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Budgeting\AdminHierarchyCeiling;
use App\Models\Budgeting\Ceiling;
use App\Models\Execution\RevenueExportAccount;
use App\Models\Setup\BudgetClass;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ReceivedFundMissingAccountsController extends Controller
{
    public static function matchEpicorRevenue()
    {
        $missing = DB::table('received_fund_missing_accounts')
            ->select([
                'financial_system_code',
                'chart_of_accounts',
                'debit_amount',
                'credit_amount',
                'admin_hierarchy_id',
                'financial_year_id',
                'transaction_type',
                'bookID',
                DB::raw('"JEDate"::date'),
                'id'
            ])
            ->where([
                'transaction_type' => 'REVENUE',
                'financial_system_code' => 'EPICOR'
            ])->take(100000)->get();

        foreach ($missing as $data) {
            $missing_account_id = $data->id;
            $debit_amount = $data->debit_amount;
            $credit_amount = $data->credit_amount;
            $je_date = $data->JEDate;
            $chart_of_accounts = $data->chart_of_accounts;
            $account = explode("-", $data->chart_of_accounts);
            $gfs = $account[9];

            /**
             * Get the period_id from the JEDate
             */
            $query = "select * from periods where '" . $je_date . "' between start_date and end_date";
            $get_period = DB::select(DB::raw($query));
            $period_id = isset($get_period[0]->id) ? $get_period[0]->id : null;

            /**
             * Check if the gfs_code exists
             **/
            $gfs_code = GfsCode::where('code', '=', $gfs)->first();
            $gfs_code_id = isset($gfs_code->id) ? $gfs_code->id : null;
            $admin_hierarchy_id = $data->admin_hierarchy_id;
            $financial_year_id = $data->financial_year_id;

            /**
             * Get the section code
             */
            $section_code = $account[2];
            $section = Section::where('code', $section_code)->first();
            $section_id = isset($section->id) ? $section->id : null;

            /**
             * Get the fund source code
             */
            $fund_source_code = $account[8];
            $fund_source = FundSource::where('code', $fund_source_code)->first();
            $fund_source_id = isset($fund_source->id) ? $fund_source->id : null;

            /**
             * Get the budget class
             */
            $budget_class_code = $account[3];
            $budget_class = BudgetClass::where('code', $budget_class_code)->first();
            $budget_class_id = isset($budget_class->id) ? $budget_class->id : null;

            /**
             * Get the facility code
             */
            $facility_code = $account[4];
            $facility = Facility::where('facility_code', $facility_code);
            if ($facility_code == '00000000') {
                $facility = $facility->where('admin_hierarchy_id', $admin_hierarchy_id);
            }
            $facility = $facility->first();
            $facility_id = isset($facility->id) ? $facility->id : null;

            if ($gfs_code_id !== null and
                $section_id !== null and
                $fund_source_id !== null and
                $facility_id !== null) {

                try {
                    DB::beginTransaction();

                    $ceiling = ReceivedFundMissingAccountsController::getOrCreateCeiling(
                        $budget_class_id,
                        $fund_source_id,
                        $gfs_code_id);

                    $adminCeiling = ReceivedFundMissingAccountsController::getOrCreateAdminHierarchyCeiling(
                        $ceiling,
                        $financial_year_id,
                        $admin_hierarchy_id,
                        $section_id,
                        $facility_id);

                    $exportAccount = ReceivedFundMissingAccountsController::getOrCreateRevenueExportAccount(
                        $adminCeiling->id,
                        $financial_year_id,
                        $admin_hierarchy_id,
                        $section_id,
                        $gfs_code_id,
                        $chart_of_accounts);

                    if ($exportAccount !== null) {
                        /***
                         *  Read the returned id and removed the entry from unknown transactions
                         *  Insert an entry to the received_fund_items table
                         */

                        $revenue_data = [
                            'revenue_export_account_id' => $exportAccount->id,
                            'admin_hierarchy_ceiling_id' => $exportAccount->admin_hierarchy_ceiling_id,
                            'period_id' => $period_id,
                            'fund_source_id' => $fund_source_id,
                            'gfs_code_id' => $gfs_code_id,
                            'account' => $chart_of_accounts,
                            'date_received' => Carbon::now(),
                            'created_at' => Carbon::now(),
                            'debit_amount' => $debit_amount,
                            'credit_amount' => $credit_amount,
                            'fiscal_year' => 'RESOLVEDEPICOR',
                            'JEDate' => $je_date
                        ];

                      //  DB::transaction(function () use ($missing_account_id, $revenue_data) {
                            DB::table('received_fund_items')->insert([$revenue_data]);
                            DB::table('received_fund_missing_accounts')->delete($missing_account_id);
                            echo 'Transaction generated successfully' . "\xA";
                     //   });
                    } else {
                        echo 'Transaction matching failed' . "\xA";
                        DB::rollback();
                    }
                    DB::commit();
                } catch (\Exception $e) {
                    Log::error($e);
                    DB::rollback();
                }
            }
        }
    }

    public static function matchFfarsRevenue()
    {
        $missing = DB::table('received_fund_missing_accounts')
            ->select([
                'financial_system_code',
                'chart_of_accounts',
                'debit_amount',
                'credit_amount',
                'admin_hierarchy_id',
                'financial_year_id',
                'transaction_type',
                'bookID',
                DB::raw('"JEDate"::date'),
                'id'
            ])
            ->where([
                'transaction_type' => 'REVENUE',
                'financial_system_code' => 'FFARS'
            ])->take(100000)->get();

        foreach ($missing as $data) {
            $missing_account_id = $data->id;
            $debit_amount = $data->debit_amount;
            $credit_amount = $data->credit_amount;
            $je_date = $data->JEDate;
            $chart_of_accounts = $data->chart_of_accounts;
            $account = explode("-", $data->chart_of_accounts);
            $gfs = $account[9];

            /**
             * Get the period_id from the JEDate
             */
            $query = "select * from periods where '" . $je_date . "' between start_date and end_date";
            $get_period = DB::select(DB::raw($query));
            $period_id = isset($get_period[0]->id) ? $get_period[0]->id : null;

            /**
             * Check if the gfs_code exists
             **/
            $gfs_code = GfsCode::where('code', '=', $gfs)->first();
            $gfs_code_id = isset($gfs_code->id) ? $gfs_code->id : null;
            $admin_hierarchy_id = $data->admin_hierarchy_id;
            $financial_year_id = $data->financial_year_id;

            /**
             * Get the section code
             */
            $section_code = $account[2];
            $section = Section::where('code', $section_code)->first();
            $section_id = isset($section->id) ? $section->id : null;


            $budget_class_code = $account[3];
            $budget_class = BudgetClass::where('code', $budget_class_code)->first();
            $budget_class_id = isset($budget_class->id) ? $budget_class->id : null;

            /**
             * Get the fund source code
             */
            $fund_source_code = $account[8];
            $fund_source = FundSource::where('code', $fund_source_code)->first();
            $fund_source_id = isset($fund_source->id) ? $fund_source->id : null;

            /**
             * Get the facility_id
             */
            $facility_code = $account[4];
            $facility = Facility::where('facility_code', $facility_code)->first();
            $facility_id = isset($facility->id) ? $facility->id : null;

            if ($gfs_code_id !== null and $section_id !== null and $fund_source_id !== null and $facility_id !== null) {

                $ceiling = ReceivedFundMissingAccountsController::getOrCreateCeiling(
                    $budget_class_id,
                    $fund_source_id,
                    $gfs_code_id);

                $adminCeiling = ReceivedFundMissingAccountsController::getOrCreateAdminHierarchyCeiling(
                    $ceiling,
                    $financial_year_id,
                    $admin_hierarchy_id,
                    $section_id,
                    $facility_id);

                $exportAccount = ReceivedFundMissingAccountsController::getOrCreateRevenueExportAccount(
                    $adminCeiling->id,
                    $financial_year_id,
                    $admin_hierarchy_id,
                    $section_id,
                    $gfs_code_id,
                    $chart_of_accounts);

                if ($exportAccount !== null) {

                    /***
                     *  Read the returned id and removed the entry from unknown transactions
                     *  Insert an entry to the received_fund_items table
                     */
                    $revenue_data = [
                        'revenue_export_account_id' => $exportAccount->id,
                        'admin_hierarchy_ceiling_id' => $exportAccount->admin_hierarchy_ceiling_id,
                        'period_id' => $period_id,
                        'fund_source_id' => $fund_source_id,
                        'gfs_code_id' => $gfs_code_id,
                        'account' => $chart_of_accounts,
                        'date_received' => Carbon::now(),
                        'created_at' => Carbon::now(),
                        'debit_amount' => $debit_amount,
                        'credit_amount' => $credit_amount,
                        'fiscal_year' => 'RESOLVEDFFARS',
                        'JEDate' => $je_date
                    ];

                    DB::transaction(function () use ($missing_account_id, $revenue_data) {
                        DB::table('received_fund_missing_accounts')->delete($missing_account_id);
                        DB::table('received_fund_items')->insert([$revenue_data]);
                        echo 'Transaction fixed successfully' . "\xA";
                    });
                } else {
                    echo 'Transaction matching failed' . "\xA";
                }
            }

        }
    }

    public static function getOrCreateCeiling($budgetClassId, $fundSourceId, $gfsCodeId)
    {
        $ceiling = DB::table('ceilings as c')->
        join('gfs_codes as gfs', 'gfs.id', 'c.gfs_code_id')->
        where('c.budget_class_id', $budgetClassId)->
        where('gfs.id', $gfsCodeId)->
        first();

        if ($ceiling == null) {
            $ceiling = DB::table('ceilings')
                ->insertGetId([
                    'budget_class_id' => $budgetClassId,
                    'gfs_code_id' => $gfsCodeId,
                    'name' => 'Missing Ceiling from Revenue Import'
                ]);
            DB::table('gfs_codes')
                ->where('id', $gfsCodeId)
                ->update([
                    'fund_source_id' => $fundSourceId
                ]);
        } else {
            $ceiling = $ceiling->id;
        }
        return $ceiling;
    }

    public static function getOrCreateAdminHierarchyCeiling(
        $ceilingId,
        $financialYearId,
        $adminHierarchyId,
        $sectionId,
        $facilityId)
    {
        $adminCeiling = AdminHierarchyCeiling::
        where('ceiling_id', $ceilingId)->
        where('admin_hierarchy_id', $adminHierarchyId)->
        where('financial_year_id', $financialYearId)->
        where('section_id', $sectionId)->
        where('facility_id', $facilityId)->
        where('budget_type', 'APPROVED')->
        first();

        if ($adminCeiling == null) {
            $adminCeiling = AdminHierarchyCeiling::create([
                'ceiling_id' => $ceilingId,
                'admin_hierarchy_id' => $adminHierarchyId,
                'financial_year_id' => $financialYearId,
                'section_id' => $sectionId,
                'facility_id' => $facilityId,
                'budget_type' => 'APPROVED',
                'amount' => 0.00
            ]);
        }

        return $adminCeiling;
    }

    public static function getOrCreateRevenueExportAccount(
        $adminHierarchyCeilingId,
        $financialYearId,
        $adminHierarchyId,
        $sectionId,
        $gfsCodeId,
        $chartOfAccounts)
    {

        $revenueAccount = RevenueExportAccount::where('admin_hierarchy_ceiling_id', $adminHierarchyCeilingId)->
        where('admin_hierarchy_id', $adminHierarchyId)->
        where('section_id', $sectionId)->
        where('financial_year_id', $financialYearId)->
        where('gfs_code_id', $gfsCodeId)->
        where('financial_system_code', 'EPICOR')->
        first();

        if ($revenueAccount == null) {
            $revenueAccount = RevenueExportAccount::create([
                'admin_hierarchy_id' => $adminHierarchyId,
                'admin_hierarchy_ceiling_id' => $adminHierarchyCeilingId,
                'financial_year_id' => $financialYearId,
                'section_id' => $sectionId,
                'gfs_code_id' => $gfsCodeId,
                'financial_system_code' => 'EPICOR',
                'chart_of_accounts' => $chartOfAccounts
            ]);
        }
        return $revenueAccount;
    }
}
