<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Planning\PerformanceIndicator;
use App\Models\Planning\PerformanceIndicatorFinancialYearValue;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PerformanceIndicatorFinancialYearValueController extends Controller {

    public function getAllPaginated($perPage) {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section_id = UserServices::getUser()->section_id;
        $financial_year_id = FinancialYearServices::getExecutionFinancialYear()->id;
        $all = PerformanceIndicatorFinancialYearValue::with(
            'performance_indicator_baseline_value', 'financial_year','data_source',
            'performance_indicator_baseline_value.start_financial_year',
            'performance_indicator_baseline_value.end_financial_year',
            'performance_indicator_baseline_value.admin_hierarchy',
            'performance_indicator_baseline_value.performance_indicator',
            'performance_indicator_baseline_value.performance_indicator.plan_chain',
            'performance_indicator_baseline_value.performance_indicator.section')
            ->whereHas('performance_indicator_baseline_value.admin_hierarchy', function ($query) use ($admin_hierarchy_id) {
                    $query->where('id', $admin_hierarchy_id);
                })
            ->whereHas('financial_year', function ($query) use ($financial_year_id) {
                $query->where('id', $financial_year_id);
            })
            ->whereHas('performance_indicator_baseline_value.performance_indicator.section',function ($query) use ($section_id) {
                    $query->where('id', $section_id);
                })->paginate($perPage);
        return $all;
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = PerformanceIndicatorFinancialYearValue::find($data->id);
            $obj->actual_value = $data->actual_value;
            $obj->data_source_id = $data->data_source_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }
}
