<?xml version="1.0" encoding="UTF-8"?>
<!-- Created with Jaspersoft Studio version 6.3.1.final using JasperReports Library version 6.3.1  -->
<!-- 2017-12-07T11:30:37 -->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="2_1_1_annual_approved_vs_actual" pageWidth="595" pageHeight="842" columnWidth="535" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="ced95029-b569-4027-895c-af2843a4e088">
	<property name="com.jaspersoft.studio.data.sql.tables" value=""/>
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="One Empty Record"/>
	<property name="com.jaspersoft.studio.data.sql.SQLQueryDesigner.sash.w1" value="240"/>
	<property name="com.jaspersoft.studio.data.sql.SQLQueryDesigner.sash.w2" value="754"/>
	<style name="Title" fontName="Times New Roman" fontSize="50" isBold="true"/>
	<style name="SubTitle" forecolor="#736343" fontName="SansSerif" fontSize="18"/>
	<style name="Column header" forecolor="#666666" fontName="SansSerif" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="SansSerif" fontSize="12"/>
	<style name="Row" mode="Transparent">
		<conditionalStyle>
			<conditionExpression><![CDATA[$V{REPORT_COUNT}%2 == 0]]></conditionExpression>
			<style backcolor="#E6DAC3"/>
		</conditionalStyle>
	</style>
	<parameter name="financial_year_id" class="java.lang.Integer"/>
	<parameter name="admin_hierarchy_id" class="java.lang.Integer"/>
	<parameter name="logo" class="java.lang.String"/>
	<parameter name="footer_text" class="java.lang.String"/>
	<queryString language="SQL">
		<![CDATA[select case when cl.can_project then 'Council Own Sources' else 'Government Subvention' end GroupName,
cl.budget_class,sum(cl.ceiling) ceiling_amount,sum(exp.expenditure) expenditure,cl.can_project, (select fy.name from financial_years fy where fy.id = 1) as year_name
from (
      SELECT
         bc.name as budget_class, bc.id bdtclass_id,fund_sources.can_project,fund_sources.id fund_source_id,
         sum(a.amount)          ceiling
       FROM admin_hierarchy_ceilings a

         INNER JOIN ceilings c ON c.id = a.ceiling_id
    inner join gfs_codes on gfs_codes.id = c.gfs_code_id
    inner join fund_sources on fund_sources.id = gfs_codes.fund_source_id
         INNER JOIN budget_classes bc ON bc.id = c.budget_class_id
       GROUP BY bc.name,bc.id,fund_sources.can_project,fund_sources.id) cl
  left join
  (
    SELECT
      sum(e.amount) expenditure,
      ac.budget_class_id, fs.id fund_source_id
    FROM
      (SELECT
         id,
         gfs_code_id,
         section_id,
         activity_facility_fund_source_input_id input_id
       FROM budget_export_accounts
       WHERE
         financial_year_id =  $P{financial_year_id} 
         AND admin_hierarchy_id =  $P{admin_hierarchy_id} ) b
      INNER JOIN budget_import_items e ON b.id = e.budget_export_account_id
      INNER JOIN activity_facility_fund_source_inputs inputs ON inputs.id = b.input_id
      INNER JOIN sections cc ON cc.id = b.section_id
      INNER JOIN sections dept ON dept.id = cc.parent_id
      INNER JOIN activity_facility_fund_sources affs ON affs.id = inputs.activity_facility_fund_source_id
      inner join fund_sources fs on fs.id = affs.fund_source_id
      INNER JOIN activity_facilities acf ON acf.id = affs.activity_facility_id
      INNER JOIN activities ac ON acf.activity_id = ac.id
    GROUP BY ac.budget_class_id,fs.id
  ) exp on cl.bdtclass_id = exp.budget_class_id and cl.fund_source_id = exp.fund_source_id
  group by  cl.budget_class,cl.can_project]]>
	</queryString>
	<field name="groupname" class="java.lang.String"/>
	<field name="budget_class" class="java.lang.String"/>
	<field name="ceiling_amount" class="java.math.BigDecimal"/>
	<field name="expenditure" class="java.math.BigDecimal"/>
	<field name="can_project" class="java.lang.Boolean"/>
	<field name="year_name" class="java.lang.String"/>
	<variable name="sum_of_ceilings_in_group" class="java.lang.Double" resetType="Group" resetGroup="Group1" incrementType="Group" incrementGroup="Group1" calculation="Sum">
		<variableExpression><![CDATA[$F{ceiling_amount}]]></variableExpression>
	</variable>
	<variable name="sum_of_expenditures_in_group" class="java.lang.Double" resetType="Group" resetGroup="Group1" incrementType="Group" incrementGroup="Group1" calculation="Sum">
		<variableExpression><![CDATA[$F{expenditure}]]></variableExpression>
	</variable>
	<variable name="group_counter" class="java.lang.Integer" incrementType="Group" incrementGroup="Group1" calculation="Count">
		<variableExpression><![CDATA[$F{groupname}]]></variableExpression>
	</variable>
	<variable name="total_sums_of_ceilings" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$V{sum_of_ceilings_in_group}]]></variableExpression>
	</variable>
	<variable name="total_sums_of_exp" class="java.lang.Double" calculation="Sum">
		<variableExpression><![CDATA[$V{sum_of_expenditures_in_group}]]></variableExpression>
	</variable>
	<group name="Group1">
		<groupExpression><![CDATA[$F{groupname}]]></groupExpression>
		<groupHeader>
			<band height="18">
				<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				<textField>
					<reportElement x="30" y="0" width="123" height="18" uuid="9fb6b94c-46d6-4f9b-b7ff-f2086081838e">
						<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					</reportElement>
					<textElement>
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{groupname}]]></textFieldExpression>
				</textField>
				<textField evaluationTime="Group" evaluationGroup="Group1" isBlankWhenNull="true">
					<reportElement x="1" y="0" width="26" height="18" uuid="832c2fdf-7cd8-4712-b32c-a7482d121b1a">
						<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					</reportElement>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{group_counter}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
	</group>
	<group name="Group2">
		<groupExpression><![CDATA[$F{groupname}]]></groupExpression>
		<groupFooter>
			<band height="20">
				<line>
					<reportElement x="329" y="0" width="1" height="19" uuid="8a9abde0-619e-4781-bd6b-36aa1306fe0d"/>
				</line>
				<line>
					<reportElement x="153" y="0" width="1" height="19" uuid="268c677f-7a87-46a0-8f9e-787ad6a34a21"/>
				</line>
				<staticText>
					<reportElement x="2" y="1" width="151" height="18" uuid="ae735ee5-1208-422e-a81f-0431c1c553bd">
						<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
					</reportElement>
					<textElement>
						<font isBold="true"/>
					</textElement>
					<text><![CDATA[Sub-Total]]></text>
				</staticText>
				<line>
					<reportElement x="0" y="0" width="1" height="19" uuid="ff44f7cf-da25-460c-816b-f485bf376162"/>
				</line>
				<line>
					<reportElement x="512" y="0" width="1" height="19" uuid="0eda42ca-7a27-450e-8183-7aa76819bc89"/>
				</line>
				<line>
					<reportElement x="554" y="0" width="1" height="19" uuid="cf164ccb-ef37-4fc6-b513-0bfec1832ccb"/>
				</line>
				<line>
					<reportElement x="0" y="19" width="555" height="1" uuid="d75852eb-5c15-49ae-b050-13257482f02c"/>
				</line>
				<textField pattern="#,###.00#;(#,###.00#-)">
					<reportElement x="156" y="-1" width="173" height="20" uuid="a41cb310-46fc-4089-ba6e-b47c25434b77"/>
					<textElement textAlignment="Right">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{sum_of_ceilings_in_group}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement x="332" y="-1" width="180" height="20" uuid="807003b5-7a7c-484f-a709-e55d5af9fef4"/>
					<textElement textAlignment="Right">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{sum_of_expenditures_in_group}]]></textFieldExpression>
				</textField>
				<textField pattern="#,###.###;(#,###.###-)" isBlankWhenNull="true">
					<reportElement x="516" y="0" width="32" height="20" uuid="e83bb19a-c886-4402-a17c-919a8fc7c5c8">
						<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
						<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
					</reportElement>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{expenditure}  == null ? "-" : PRODUCT( ($V{sum_of_expenditures_in_group}.floatValue()/$V{sum_of_ceilings_in_group}.floatValue()),100.0)]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="78" splitType="Stretch">
			<line>
				<reportElement positionType="FixRelativeToBottom" x="0" y="77" width="555" height="1" forecolor="#000000" uuid="1a61a836-d137-48b1-ad67-6ff64600bf93"/>
				<graphicElement>
					<pen lineWidth="1.0" lineColor="#000000"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement style="Column header" x="1" y="-2" width="152" height="79" forecolor="#000000" uuid="59c2e534-dffd-49c0-b356-cd9cb809cbd9"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" isBold="true"/>
				</textElement>
				<text><![CDATA[Description]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="330" y="56" width="182" height="21" forecolor="#000000" uuid="94ecf9dd-6961-48b0-b783-2b3661b159ae"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Actual Expenditures (Tshs.)]]></text>
			</staticText>
			<textField>
				<reportElement x="154" y="-2" width="400" height="32" uuid="2071e5eb-a692-4581-a23b-8b1524edbeae"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{year_name}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="513" y="56" width="41" height="21" uuid="11d82487-77b9-4028-b831-f7620dd1e91a"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[%]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="154" y="56" width="175" height="21" forecolor="#000000" uuid="3602e1c9-a86c-4197-9b45-b7ff3cbfa716"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Annual Estimates (Tshs.)]]></text>
			</staticText>
			<line>
				<reportElement positionType="FixRelativeToBottom" x="154" y="30" width="401" height="1" forecolor="#000000" uuid="e539fa4c-d409-4ade-9d8d-829d9812bc5b"/>
				<graphicElement>
					<pen lineWidth="1.0" lineColor="#000000"/>
				</graphicElement>
			</line>
			<line>
				<reportElement positionType="FixRelativeToBottom" x="0" y="-2" width="555" height="1" forecolor="#000000" uuid="6222500c-3b5e-4f4b-86cb-d0e946d2fda2"/>
				<graphicElement>
					<pen lineWidth="1.0" lineColor="#000000"/>
				</graphicElement>
			</line>
			<line>
				<reportElement x="0" y="-2" width="1" height="79" uuid="46647121-1ec8-43f9-8297-0fcd32cc1a0e"/>
			</line>
			<line>
				<reportElement x="153" y="-2" width="1" height="79" uuid="c5cb86a8-f4b3-4c6f-acc9-b9c8dd752216"/>
			</line>
			<line>
				<reportElement x="329" y="55" width="1" height="23" uuid="05e4f251-849b-433f-969b-a2562e0465bb"/>
			</line>
			<line>
				<reportElement x="512" y="55" width="1" height="23" uuid="607807d8-6a7a-495c-a681-6520e668cea3"/>
			</line>
			<line>
				<reportElement x="554" y="37" width="1" height="41" uuid="17e16035-b4c4-42bf-be46-4cf94db73b42"/>
			</line>
			<line>
				<reportElement x="554" y="-2" width="1" height="39" uuid="b851347c-3dd6-45d1-84c9-466a1d7ec5a1"/>
			</line>
			<line>
				<reportElement positionType="FixRelativeToBottom" x="154" y="55" width="400" height="1" forecolor="#000000" uuid="ceb412af-3117-44fe-b269-5c7688939c46"/>
				<graphicElement>
					<pen lineWidth="1.0" lineColor="#000000"/>
				</graphicElement>
			</line>
			<staticText>
				<reportElement x="154" y="31" width="400" height="24" uuid="242c27c3-383f-4568-85da-151a5b9788ec"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Expenditure]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="21" splitType="Stretch">
			<textField>
				<reportElement stretchType="RelativeToBandHeight" isPrintRepeatedValues="false" x="2" y="0" width="151" height="20" isRemoveLineWhenBlank="true" isPrintInFirstWholeBand="true" uuid="220b5c27-4820-44db-98a9-3fdece60f437"/>
				<textElement verticalAlignment="Middle">
					<font isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{budget_class}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00#;(#,##0.00#-)" isBlankWhenNull="true">
				<reportElement stretchType="RelativeToBandHeight" x="156" y="0" width="173" height="20" uuid="2c0fbc7d-7301-4c5e-a4a5-f6e4dd99850d">
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{ceiling_amount}]]></textFieldExpression>
			</textField>
			<textField pattern="#,###.###;(#,###.###-)" isBlankWhenNull="true">
				<reportElement x="332" y="0" width="180" height="20" uuid="8c47b4a4-55a3-431b-b3db-2ee371e92f5f">
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{expenditure}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="0" width="1" height="21" uuid="f3749452-ecd4-49e7-94ff-b885febb568e"/>
			</line>
			<line>
				<reportElement x="153" y="0" width="1" height="20" uuid="81525c50-fde1-42da-844a-e25a915fe170"/>
			</line>
			<line>
				<reportElement x="329" y="0" width="1" height="20" uuid="e062eca4-850c-4881-9a07-bca18630753f"/>
			</line>
			<line>
				<reportElement x="512" y="0" width="1" height="20" uuid="9031f49e-251e-428d-b121-b2499e2fd62b"/>
			</line>
			<line>
				<reportElement x="0" y="20" width="555" height="1" uuid="9f6a992c-a6b7-4f7a-a0e3-e6223ab89480"/>
			</line>
			<line>
				<reportElement x="554" y="0" width="1" height="21" uuid="8c6a39d4-51d4-41c8-8989-15f84a492154"/>
			</line>
			<line>
				<reportElement x="0" y="-22" width="1" height="21" uuid="517dc339-12b6-4d89-8166-387c6b52f2d6">
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
			</line>
			<line>
				<reportElement x="153" y="-18" width="1" height="17" uuid="7696b0b1-4b66-41fb-9608-ca08214c8c40"/>
			</line>
			<line>
				<reportElement x="329" y="-18" width="1" height="17" uuid="6fa854bc-098f-4f5e-ac09-6ee7d921a508"/>
			</line>
			<line>
				<reportElement x="512" y="-18" width="1" height="17" uuid="0eaf2a73-aa21-4af8-9a18-0741c9f80dbd"/>
			</line>
			<line>
				<reportElement x="554" y="-18" width="1" height="18" uuid="53501730-33bb-4257-8eea-1b42c930bcb4"/>
			</line>
			<line>
				<reportElement x="0" y="-1" width="554" height="1" uuid="a069cb06-f72a-4159-8fb7-fe8e87b58c65"/>
			</line>
			<textField pattern="#,###.###;(#,###.###-)" isBlankWhenNull="true">
				<reportElement x="516" y="0" width="32" height="20" uuid="d95748c6-82ad-4429-8a57-2899dcf34ac5">
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{expenditure}  == null ? "-" : PRODUCT( ($F{expenditure}.floatValue()/$F{ceiling_amount}.floatValue()),100.0)]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="25" splitType="Stretch">
			<frame>
				<reportElement mode="Opaque" x="-21" y="1" width="597" height="24" forecolor="#D0B48E" backcolor="#F2EBDF" uuid="183682bc-d976-4756-83e0-6625a3f98ed1"/>
				<textField evaluationTime="Report">
					<reportElement style="Column header" x="533" y="0" width="40" height="20" forecolor="#736343" uuid="0193f9b3-1559-491a-8580-b6988863b6a1"/>
					<textElement verticalAlignment="Middle">
						<font size="10" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement style="Column header" x="453" y="0" width="80" height="20" forecolor="#736343" uuid="d00b105e-494b-418b-8ac9-8b1b4824f4f0"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="10" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
				</textField>
				<textField pattern="EEEEE dd MMMMM yyyy">
					<reportElement style="Column header" x="175" y="0" width="197" height="20" forecolor="#736343" uuid="0616f3fe-0354-456f-8911-ec30ec51a5ae"/>
					<textElement verticalAlignment="Middle">
						<font size="10" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement x="22" y="0" width="152" height="20" forecolor="#736343" uuid="27a75a4e-1158-48a0-9949-afcd0eca6ccb"/>
					<box>
						<pen lineWidth="0.0" lineStyle="Solid" lineColor="#666666"/>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#666666"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#666666"/>
						<bottomPen lineWidth="0.0" lineStyle="Solid" lineColor="#666666"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#666666"/>
					</box>
					<textElement>
						<font fontName="SansSerif" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[$P{footer_text}]]></textFieldExpression>
				</textField>
			</frame>
		</band>
	</pageFooter>
	<summary>
		<band height="37" splitType="Stretch">
			<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
			<line>
				<reportElement x="329" y="0" width="1" height="19" uuid="8b2cc937-9295-4d0a-944f-f2bfc4cf1ac0"/>
			</line>
			<line>
				<reportElement x="153" y="0" width="1" height="19" uuid="50f7c88b-5c2e-438a-8766-80db367365f4"/>
			</line>
			<staticText>
				<reportElement x="2" y="1" width="151" height="18" uuid="30b80ba8-cc8a-42cd-a520-ebdfb45c9a49">
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.height" value="pixel"/>
				</reportElement>
				<textElement>
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Total]]></text>
			</staticText>
			<line>
				<reportElement x="0" y="0" width="1" height="19" uuid="d539908f-5cc5-4b4c-be17-9dde0cdde63c"/>
			</line>
			<line>
				<reportElement x="512" y="0" width="1" height="19" uuid="80491dde-27fe-4ab1-a815-a087059b2b8b"/>
			</line>
			<line>
				<reportElement x="554" y="0" width="1" height="19" uuid="6167bbfa-1f07-4f2e-b055-6a4ee6ebcee8"/>
			</line>
			<textField pattern="#0.00#;(#0.00#-)" isBlankWhenNull="true">
				<reportElement x="516" y="1" width="32" height="19" uuid="3cad83ce-a101-4264-9476-e9f8c105d520">
					<property name="com.jaspersoft.studio.unit.width" value="pixel"/>
					<property name="com.jaspersoft.studio.unit.x" value="pixel"/>
				</reportElement>
				<textElement textAlignment="Center" verticalAlignment="Top">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{expenditure}  == null ? "-" : PRODUCT( ($V{total_sums_of_exp}.floatValue()/$V{total_sums_of_ceilings}.floatValue()),100.0)]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="0" y="19" width="555" height="1" uuid="2b86ae5f-fc76-44c7-ad3b-7b6f7a523a2c"/>
			</line>
			<textField pattern="#,##0.00#;(#,##0.00#-)">
				<reportElement x="156" y="1" width="173" height="18" uuid="c86b1ddc-f712-4a41-bac5-809c105f9e19"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{total_sums_of_ceilings}]]></textFieldExpression>
			</textField>
			<textField pattern="#,###.###;(#,###.###-)" isBlankWhenNull="true">
				<reportElement x="332" y="2" width="180" height="17" uuid="b50d95dc-f2dd-4b3b-be64-551db1e17912"/>
				<textElement textAlignment="Right">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{total_sums_of_exp}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
