<?xml version="1.0" encoding="UTF-8"?>
<!-- Created with Jaspersoft Studio version 6.4.2.final using JasperReports Library version 6.4.1  -->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="summary_of_carry_over_funds_per_programmer_and_physical_implementation" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="df013db5-f76e-44d3-b0df-bcbc46d93160">
	<property name="com.jaspersoft.studio.data.sql.tables" value=""/>
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="One Empty Record"/>
	<style name="Title" fontName="Times New Roman" fontSize="50" isBold="true"/>
	<style name="SubTitle" forecolor="#736343" fontName="SansSerif" fontSize="18"/>
	<style name="Column header" forecolor="#666666" fontName="SansSerif" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="SansSerif" fontSize="12"/>
	<style name="Row" mode="Transparent">
		<conditionalStyle>
			<conditionExpression><![CDATA[$V{REPORT_COUNT}%2 == 0]]></conditionExpression>
			<style backcolor="#E6DAC3"/>
		</conditionalStyle>
	</style>
	<parameter name="footer_text" class="java.lang.String"/>
	<parameter name="logo" class="java.lang.String"/>
	<parameter name="financial_year_id" class="java.lang.Integer"/>
	<parameter name="admin_hierarchy_id" class="java.lang.Integer"/>
	<queryString language="SQL">
		<![CDATA[select a.description_of_actv, a.council, a.project_name, a.total_amount_of_activity, a.implementation_statuses, b.received_amount,a.comments from
(
select activities.id as activity_id, activities.description as description_of_actv, admin_hierarchies.name as council, projects.name as project_name,
	sum(activity_facility_fund_source_inputs.quantity * activity_facility_fund_source_inputs.unit_price) as total_amount_of_activity, 
    activity_implementations.achievements as implementation_statuses, activity_statuses.name as comments
from activity_facility_fund_source_inputs
inner join activity_facility_fund_sources on activity_facility_fund_source_inputs.activity_facility_fund_source_id = activity_facility_fund_sources.id
inner join activity_facilities on activity_facility_fund_sources.activity_facility_id=activity_facilities.id
inner join activities on activity_facilities.activity_id = activities.id
inner join mtef_sections on mtef_sections.id = activities.mtef_section_id
inner join mtefs on mtefs.id = mtef_sections.mtef_id
inner join financial_years on mtefs.financial_year_id = financial_years.id
inner join admin_hierarchies on admin_hierarchies.id = mtefs.admin_hierarchy_id
left join projects on activities.project_id = projects.id
left join activity_implementations on activities.id = activity_implementations.activity_id
left join activity_statuses on activities.activity_status_id = activity_statuses.id
where financial_years.id =  $P{financial_year_id}  and admin_hierarchies.id =  $P{admin_hierarchy_id} 
group by 
	admin_hierarchies.name, 
    activity_facility_fund_source_inputs.quantity,
    activity_facility_fund_source_inputs.unit_price, 
    activity_facility_fund_source_inputs.frequency,
    activities.description,
    activities.id,
    admin_hierarchies.name,
    activity_implementations.achievements,
    projects.name,
    activity_statuses.name
    ) a
    left join 
    
(select sum(bii.amount) as received_amount, actv.description as activity_description, actv.id as activity_id
from budget_import_items bii
inner join budget_export_accounts bea on bii.budget_export_account_id = bea.id
inner join activities actv on bea.activity_id = actv.id
group by 
actv.id,
actv.description) b

on a.activity_id = b.activity_id
group by 
a.description_of_actv,
a.council,
a.total_amount_of_activity,
a.implementation_statuses,
a.project_name,
a.comments,
b.received_amount]]>
	</queryString>
	<field name="description_of_actv" class="java.lang.String"/>
	<field name="council" class="java.lang.String"/>
	<field name="project_name" class="java.lang.String"/>
	<field name="total_amount_of_activity" class="java.math.BigDecimal"/>
	<field name="implementation_statuses" class="java.lang.String"/>
	<field name="received_amount" class="java.math.BigDecimal"/>
	<field name="comments" class="java.lang.String"/>
	<variable name="balance" class="java.lang.Double">
		<variableExpression><![CDATA[$F{total_amount_of_activity}.doubleValue()-$F{received_amount}.doubleValue()]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="32" splitType="Stretch">
			<staticText>
				<reportElement style="Column header" x="27" y="0" width="85" height="31" forecolor="#030200" uuid="b846f302-ce59-47a7-9a21-b6b1019f3f36"/>
				<textElement>
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Project Name]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="113" y="0" width="206" height="31" forecolor="#030200" uuid="8b496711-845f-40b3-8f52-80283d5ddbe0"/>
				<textElement>
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Activity]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="320" y="0" width="82" height="31" forecolor="#030200" uuid="b89f510b-44b3-40e8-99b1-1af6eea446da"/>
				<textElement>
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Amount Rolled Over]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="404" y="0" width="114" height="31" forecolor="#030200" uuid="f9fbfe2a-adb7-4d97-9c8f-dc2e203ded1e"/>
				<textElement>
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Implementation Status to Date]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="520" y="0" width="78" height="31" forecolor="#030200" uuid="e596f3a7-a883-4a47-8862-991eec97b92a"/>
				<textElement>
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Fund Utilized]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="600" y="0" width="84" height="31" forecolor="#030200" uuid="91026bfa-fdf7-4545-a19f-c5e0b7bc5800"/>
				<textElement>
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Fund Balance to Date]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="686" y="0" width="116" height="31" forecolor="#030200" uuid="3155075b-ebaa-4ab2-b01e-092dd55ae385"/>
				<textElement>
					<font size="10" isBold="true"/>
				</textElement>
				<text><![CDATA[Comments]]></text>
			</staticText>
			<line>
				<reportElement x="25" y="31" width="777" height="1" uuid="6a9d620c-2a65-49ac-bb66-24600b61fdcc"/>
			</line>
			<line>
				<reportElement x="25" y="0" width="777" height="1" uuid="0897d418-4e2a-4839-a639-7d8954b90c0d"/>
			</line>
			<line>
				<reportElement x="25" y="1" width="1" height="30" uuid="c4010263-08f0-418a-ab29-5c43f6e19904"/>
			</line>
			<line>
				<reportElement x="111" y="1" width="1" height="30" uuid="a04caa0a-97e7-46bd-a4ef-81bfd7c399de"/>
			</line>
			<line>
				<reportElement x="319" y="1" width="1" height="30" uuid="824dbf5a-4f8b-4694-ba65-3754d1b272a0"/>
			</line>
			<line>
				<reportElement x="402" y="1" width="1" height="30" uuid="4201851a-b58c-4283-8e18-1a6465dbd31d"/>
			</line>
			<line>
				<reportElement x="519" y="1" width="1" height="30" uuid="13423b42-acd8-4b85-87fa-39d02d7e78d6"/>
			</line>
			<line>
				<reportElement x="598" y="1" width="1" height="30" uuid="f1320acf-52bb-432c-8004-14807e3156d8"/>
			</line>
			<line>
				<reportElement x="684" y="1" width="1" height="30" uuid="57d9aa58-f1b5-42fe-9c58-735caf596b46"/>
			</line>
			<line>
				<reportElement x="801" y="1" width="1" height="30" uuid="f07b5f45-1f12-4595-9929-e05806043939"/>
			</line>
		</band>
	</columnHeader>
	<detail>
		<band height="31" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement x="25" y="0" width="85" height="30" uuid="c6b418d6-7c5c-4424-be88-e9469325cccc"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{project_name}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="113" y="0" width="206" height="30" uuid="cf9c6a8b-9237-430e-9beb-a6dda5b57a38"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{description_of_actv}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00#" isBlankWhenNull="true">
				<reportElement x="319" y="0" width="82" height="30" uuid="e8038198-6cee-47d0-a38b-46c60869163c"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{total_amount_of_activity}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="404" y="0" width="114" height="30" uuid="de7782ee-a9e9-47b4-ac8a-97b685b01e07"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{implementation_statuses}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.0##" isBlankWhenNull="true">
				<reportElement x="520" y="0" width="77" height="30" uuid="bc1ca784-da28-4ddf-b9a9-108d8407b12f"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{received_amount}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="686" y="0" width="116" height="30" uuid="b6efce4d-c9c2-49c5-9adc-6e5aa80936bb"/>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{comments}]]></textFieldExpression>
			</textField>
			<textField pattern="#,###.00#" isBlankWhenNull="true">
				<reportElement x="599" y="0" width="84" height="30" uuid="1ad45cad-fbc0-43a9-85c9-987a35a6d5ea"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{total_amount_of_activity}.doubleValue()-$F{received_amount}.doubleValue()]]></textFieldExpression>
			</textField>
			<line>
				<reportElement x="25" y="30" width="777" height="1" uuid="d18487b3-7739-4883-985a-c53034944bf0"/>
			</line>
			<line>
				<reportElement x="25" y="0" width="1" height="30" uuid="4a54a791-453f-4de8-81b4-6902f8ffdc10"/>
			</line>
			<line>
				<reportElement x="111" y="0" width="1" height="30" uuid="4422cb22-a448-4bd7-aac2-f71dd4e0a162"/>
			</line>
			<line>
				<reportElement x="319" y="0" width="1" height="30" uuid="a9033079-c98c-4a93-92d3-4195728e9bf8"/>
			</line>
			<line>
				<reportElement x="402" y="0" width="1" height="30" uuid="e431333c-c5fc-4449-91f9-4c2114b20ccb"/>
			</line>
			<line>
				<reportElement x="519" y="0" width="1" height="30" uuid="e96a2869-cc12-409a-9831-4282bd279059"/>
			</line>
			<line>
				<reportElement x="598" y="0" width="1" height="30" uuid="646399b9-4024-4e52-b69a-7d504c759e26"/>
			</line>
			<line>
				<reportElement x="684" y="0" width="1" height="30" uuid="178ba47f-1ea4-46ba-9233-818ccaf3403e"/>
			</line>
			<line>
				<reportElement x="801" y="0" width="1" height="30" uuid="637df803-9f1a-4e39-943f-e208281d90fe"/>
			</line>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="25" splitType="Stretch">
			<frame>
				<reportElement mode="Opaque" x="-21" y="1" width="843" height="24" forecolor="#D0B48E" backcolor="#F2EBDF" uuid="5d8169bd-4a75-48c8-8a68-6d3ad5ba9402"/>
				<textField evaluationTime="Report">
					<reportElement style="Column header" x="783" y="1" width="40" height="20" forecolor="#736343" uuid="e5e27efa-b599-499b-9ca3-848cb511cb7b"/>
					<textElement verticalAlignment="Middle">
						<font size="10" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField>
					<reportElement style="Column header" x="703" y="1" width="80" height="20" forecolor="#736343" uuid="18cfe1ca-f7d6-48b0-9827-28578b42a5e0"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="10" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
				</textField>
				<textField pattern="EEEEE dd MMMMM yyyy">
					<reportElement style="Column header" x="22" y="1" width="197" height="20" forecolor="#736343" uuid="fbce24bb-3cb1-44a3-8eec-8c067ddbe5b5"/>
					<textElement verticalAlignment="Middle">
						<font size="10" isBold="false"/>
					</textElement>
					<textFieldExpression><![CDATA[new java.util.Date()]]></textFieldExpression>
				</textField>
			</frame>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
