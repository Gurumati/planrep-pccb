<?xml version="1.0" encoding="UTF-8"?>
<!-- Created with Jaspersoft Studio version 6.11.0.final using JasperReports Library version 6.5.1  -->
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="nutrition_budget_by_intervention" pageWidth="842" pageHeight="595" orientation="Landscape" columnWidth="802" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20" uuid="1622d4cc-dc9b-44de-b672-9e4ce348b666">
	<property name="com.jaspersoft.studio.data.defaultdataadapter" value="SocialWelfareAdapter.xml"/>
	<property name="com.jaspersoft.studio.data.sql.tables" value=""/>
	<property name="com.jaspersoft.studio.data.sql.SQLQueryDesigner.sash.w1" value="166"/>
	<property name="com.jaspersoft.studio.data.sql.SQLQueryDesigner.sash.w2" value="826"/>
	<parameter name="footer_text" class="java.lang.String"/>
	<parameter name="logo" class="java.lang.String"/>
	<parameter name="budget_type" class="java.lang.String"/>
	<parameter name="admin_hierarchy_id" class="java.lang.Integer"/>
	<parameter name="financial_year_id" class="java.lang.Integer"/>
	<parameter name="period_id" class="java.lang.String"/>
	<queryString>
		<![CDATA[select 
  fy.name as fy,
  ahp.name as region,
  ah.name as council,
  ob.description as objective,
  pa.description as priority_area,
  it.description as intervention,
  sum(ai.frequency * ai.quantity * ai.unit_price) as budget
from
mtefs m
join mtef_sections ms on ms.mtef_id = m.id
join activities ac on ac.mtef_section_id = ms.id
join activity_facilities af on af.activity_id = ac.id
join activity_facility_fund_sources aff on aff.activity_facility_id = af.id
join activity_facility_fund_source_inputs ai on ai.activity_facility_fund_source_id = aff.id
join mtef_annual_targets mt on mt.id = ac.mtef_annual_target_id
join long_term_targets lt on lt.id = mt.long_term_target_id
join plan_chains so on so.id = lt.plan_chain_id
join plan_chains ob on so.parent_id = ob.id
join fund_sources f on f.id = aff.fund_source_id
join admin_hierarchies ah on ah.id = m.admin_hierarchy_id
join admin_hierarchies ahp on ahp.id = ah.parent_id
join financial_years fy on fy.id = m.financial_year_id
join interventions it on it.id = ac.intervention_id
join priority_areas pa on pa.id = it.priority_area_id
 join sections s on ms.section_id = s.id
where 
fy.id =   $P{financial_year_id}  and (ob.code = 'Y' OR s.id=98) and ac.budget_type =   $P{budget_type} 
group by
fy.name, ahp.name, ah.name, ob.description, pa.description, it.description
order by
fy.name, ahp.name, ah.name, ob.description, pa.description]]>
	</queryString>
	<field name="fy" class="java.lang.String">
		<property name="com.jaspersoft.studio.field.label" value="fy"/>
	</field>
	<field name="region" class="java.lang.String">
		<property name="com.jaspersoft.studio.field.label" value="region"/>
	</field>
	<field name="council" class="java.lang.String">
		<property name="com.jaspersoft.studio.field.label" value="council"/>
	</field>
	<field name="objective" class="java.lang.String">
		<property name="com.jaspersoft.studio.field.label" value="objective"/>
	</field>
	<field name="priority_area" class="java.lang.String">
		<property name="com.jaspersoft.studio.field.label" value="priority_area"/>
	</field>
	<field name="intervention" class="java.lang.String">
		<property name="com.jaspersoft.studio.field.label" value="intervention"/>
	</field>
	<field name="budget" class="java.math.BigDecimal">
		<property name="com.jaspersoft.studio.field.label" value="budget"/>
	</field>
	<variable name="sub_total" class="java.math.BigDecimal" resetType="Group" resetGroup="council" calculation="Sum">
		<variableExpression><![CDATA[$F{budget}]]></variableExpression>
	</variable>
	<variable name="grand_total" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{budget}]]></variableExpression>
	</variable>
	<group name="council">
		<groupExpression><![CDATA[$F{council}]]></groupExpression>
		<groupHeader>
			<band height="68">
				<textField>
					<reportElement x="2" y="0" width="278" height="20" uuid="6c6dc99b-4a99-4b17-9d07-e05b13d4f337"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Council : "+$F{council}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="2" y="45" width="308" height="20" uuid="2f2ce3db-0d85-412f-af59-1494ffd8fc50"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Priority Area]]></text>
				</staticText>
				<staticText>
					<reportElement x="336" y="45" width="330" height="20" uuid="33f6f25b-7035-49f0-90c5-d40807ac4687"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Intervention]]></text>
				</staticText>
				<staticText>
					<reportElement x="673" y="45" width="128" height="20" uuid="e2911480-5907-4a7c-9440-b3149b14cb22"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
						<paragraph rightIndent="5"/>
					</textElement>
					<text><![CDATA[Amount]]></text>
				</staticText>
				<textField>
					<reportElement x="2" y="22" width="798" height="20" uuid="e8f3956a-7f08-4730-97d1-e5f53d7bf574"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="false" isItalic="true"/>
					</textElement>
					<textFieldExpression><![CDATA["Objective : "+$F{objective}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="67" width="802" height="1" uuid="6129d551-a04a-42ee-951d-8bf389efac60"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="25">
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="24" width="802" height="1" uuid="39ec0265-c7fe-4307-ac53-13d676f8cb7d"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="22" width="802" height="1" uuid="ffe2c6ff-ab20-44fb-bb0f-e13c3a1717b8"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="1" width="802" height="1" uuid="0805a56e-b7af-4bdd-b34b-ef7fb84f8712"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
				<textField pattern="#,##0.00#;(#,##0.00#-)">
					<reportElement x="673" y="3" width="126" height="16" uuid="db471934-a950-4ef1-a961-7271778c3668"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="8" isBold="true"/>
						<paragraph rightIndent="4"/>
					</textElement>
					<textFieldExpression><![CDATA[$V{sub_total}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement x="2" y="1" width="308" height="20" uuid="b61fca6a-e1ae-4dbe-b20e-c8cf168e16c0"/>
					<textElement verticalAlignment="Middle">
						<font size="8" isBold="true"/>
					</textElement>
					<text><![CDATA[Total]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="92" splitType="Stretch">
			<image>
				<reportElement x="25" y="2" width="80" height="69" uuid="c97d4610-114e-4411-a236-8d554836ba4d"/>
				<imageExpression><![CDATA[$P{logo}]]></imageExpression>
			</image>
			<staticText>
				<reportElement x="0" y="71" width="131" height="21" uuid="a6661151-1f8e-4bf4-8811-fa9e4e848067"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[The United Republic of Tanzania]]></text>
			</staticText>
			<staticText>
				<reportElement x="5" y="10" width="796" height="30" uuid="b48bbf29-33be-4a97-8a8b-768aae378842"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="14" isBold="true"/>
				</textElement>
				<text><![CDATA[Nutrition Budget by Intervention]]></text>
			</staticText>
			<textField pattern="#,##0.00#;(#,##0.00#-)">
				<reportElement x="690" y="70" width="110" height="20" uuid="24c0bf39-cdfc-4301-be26-4e9bc9dde31f"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="12" isBold="true"/>
					<paragraph rightIndent="4"/>
				</textElement>
				<textFieldExpression><![CDATA["FY : "+$F{fy}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="7" y="47" width="793" height="20" uuid="b29a6b68-185c-4de1-8365-64dce218e448"/>
				<textElement textAlignment="Center">
					<font size="14" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["Region: "+$F{region}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<detail>
		<band height="18" splitType="Stretch">
			<textField isStretchWithOverflow="true">
				<reportElement isPrintRepeatedValues="false" x="2" y="1" width="328" height="16" isPrintWhenDetailOverflows="true" uuid="e6e329eb-c5a2-4e72-83d6-f9f1bc15813a"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{priority_area}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true">
				<reportElement x="336" y="2" width="330" height="16" uuid="56671807-cc92-4239-8627-5048f979d09b"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{intervention}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00#;(#,##0.00#-)">
				<reportElement x="673" y="1" width="126" height="16" uuid="c2fe8223-a812-41dd-abd2-969f263266b6"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="false"/>
					<paragraph rightIndent="4"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{budget}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<pageFooter>
		<band height="24" splitType="Stretch">
			<textField evaluationTime="Report">
				<reportElement x="783" y="3" width="40" height="20" forecolor="#000000" uuid="9e913b57-00a7-46a7-9e17-33c07f83939b"/>
				<textElement verticalAlignment="Middle">
					<font fontName="SansSerif" size="7" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="703" y="3" width="80" height="20" forecolor="#000000" uuid="90254b64-e3ea-4cd9-bfff-28db2e73ca27"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="SansSerif" size="7" isBold="false"/>
				</textElement>
				<textFieldExpression><![CDATA["Page "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="22" y="0" width="100" height="24" forecolor="#000000" uuid="64aa6937-bf0b-419f-86a7-b8629f56ce78"/>
				<textElement textAlignment="Left" verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{footer_text}]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="25" splitType="Stretch">
			<line>
				<reportElement positionType="FixRelativeToBottom" x="0" y="1" width="802" height="1" uuid="460e7587-7d80-4c83-8660-1e7720f9cedf"/>
				<graphicElement>
					<pen lineWidth="0.5" lineColor="#999999"/>
				</graphicElement>
			</line>
			<line>
				<reportElement positionType="FixRelativeToBottom" x="0" y="22" width="802" height="1" uuid="3d2e8a60-e139-4cab-bf9f-c80cbb42c5bc"/>
				<graphicElement>
					<pen lineWidth="0.5" lineColor="#999999"/>
				</graphicElement>
			</line>
			<line>
				<reportElement positionType="FixRelativeToBottom" x="0" y="24" width="802" height="1" uuid="aff5ccad-db7e-42d8-b957-9c03266a349f"/>
				<graphicElement>
					<pen lineWidth="0.5" lineColor="#999999"/>
				</graphicElement>
			</line>
			<textField pattern="#,##0.00#;(#,##0.00#-)">
				<reportElement x="673" y="3" width="126" height="16" uuid="2cff566d-56dd-4565-b274-b4834c557000"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8" isBold="true"/>
					<paragraph rightIndent="4"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{grand_total}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="2" y="1" width="308" height="20" uuid="9080c586-1426-4c98-b853-787fc43ae790"/>
				<textElement verticalAlignment="Middle">
					<font size="8" isBold="true"/>
				</textElement>
				<text><![CDATA[Grand Total]]></text>
			</staticText>
		</band>
	</summary>
</jasperReport>
