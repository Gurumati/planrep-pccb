<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Report\Ceiling;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\FinancialYear;
use Illuminate\Http\Request;

class CeilingController extends Controller {
    private $adminHierarchies = [];

    public function adminHierarchyLevel($id) {
        array_push($this->adminHierarchies, $id);
        $adminHierarchyId = AdminHierarchy::where('parent_id', $id)->get();
        foreach ($adminHierarchyId as $key => $value) {
            array_push($this->adminHierarchies, $value->id);
            //check if has children
            $count = AdminHierarchy::where('parent_id', $value->id)->count();
            if ($count > 0) {
                $this->adminHierarchyLevel($value->id);
            }
        }
    }

    public function fetchAll() {
        ini_set('max_execution_time', 600);
        $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $this->adminHierarchyLevel($admin_hierarchy_id);
        $admins = implode(",", $this->adminHierarchies);
        $all = Ceiling::ceiling($admins, $financial_year_id);
        $data = ['ceilings' => $all];
        return response()->json($data, 200);
    }

    public function hierarchy_ceilings(Request $request) {
        ini_set('max_execution_time', 600);
        $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
        if(isset($request->admin_hierarchy_id)){
            $admin_hierarchy_id = $request->admin_hierarchy_id;
            $all = Ceiling::ceiling($admin_hierarchy_id, $financial_year_id);
            $data = ['ceilings' => $all];
            return response()->json($data, 200);
        } else {
            $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
            $this->adminHierarchyLevel($admin_hierarchy_id);
            $admins = implode(",", $this->adminHierarchies);
            $all = Ceiling::ceiling($admins, $financial_year_id);
            $data = ['ceilings' => $all];
            return response()->json($data, 200);
        }
    }

    public function hierarchyYearCeilings(Request $request) {
        ini_set('max_execution_time', 600);
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        if(isset($request->financial_year_id)){
            $financial_year_id = $request->financial_year_id;
            $all = Ceiling::ceiling($admin_hierarchy_id, $financial_year_id);
            $data = ['ceilings' => $all];
            return response()->json($data, 200);
        } else {
            $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
            $all = Ceiling::ceiling($admin_hierarchy_id, $financial_year_id);
            $data = ['ceilings' => $all];
            return response()->json($data, 200);
        }
    }
}
