<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\Facility;
use App\Models\Setup\FacilityCustomDetail;
use App\Models\Setup\FacilityCustomDetailValue;
use App\Models\Setup\FinancialYear;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class FacilityReportController extends Controller {

    public function getAllPaginated($admin_hierarchy_id = null, $perPage = 10) {
        if ($admin_hierarchy_id == null) {
            return Facility::with('facility_type', 'admin_hierarchy','ownership','nearest_similar_facility','furthest_ward_served','furthest_village_served', 'physical_state', 'star_rating', 'custom_details', 'custom_details.facility_custom_detail')->paginate($perPage);
        } else {
            return Facility::with('facility_type', 'admin_hierarchy','ownership','nearest_similar_facility','furthest_ward_served','furthest_village_served', 'physical_state', 'star_rating', 'custom_details', 'custom_details.facility_custom_detail')
                ->where('admin_hierarchy_id', $admin_hierarchy_id)->paginate($perPage);
        }
    }

    public function summary(Request $request) {
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        if (empty($admin_hierarchy_id)) {
            $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        }
        $all = $this->getAllPaginated($admin_hierarchy_id, $request->perPage ? $request->perPage : 10);
        $data = ["items" => $all];
        return response()->json($data,200);
    }


    public function printPDF() {
        $financial_year = FinancialYear::whereIn('id',[1,2])->first()->name;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $html = "";
        $sql_string = "SELECT ft.name as facility_type,
          f.id as facility_id,f.name as facility,concat(f.furthest_village_served_distance,'km from ',v.name)
          as location_and_access,concat(n.nearest_similar_facility_distance,'km from ',n.name)
          as nearest_similar_facility_distance,f.catchment_population,
          CASE WHEN f.operation_year ISNULL THEN '-' ELSE f.operation_year END AS operation_year,f.population_within_area,
          CASE WHEN fp.name ISNULL THEN '' ELSE fp.name END AS physical_status,
          a.name as admin_hierarchy,
          CASE WHEN fo.name ISNULL THEN '' ELSE fo.name END AS facility_ownership,
          CASE WHEN w.name ISNULL THEN '' ELSE w.name END AS nearest_ward_served,
          CASE WHEN v.name ISNULL THEN '' ELSE v.name END AS nearest_village_served,
          CASE WHEN fp.name ISNULL THEN '' ELSE fp.name END AS physical_state,
          CASE WHEN fs.name ISNULL THEN '' ELSE fs.name END AS star_rating
        from facilities f
          left join admin_hierarchies a on f.admin_hierarchy_id = a.id
          left join facility_types ft on ft.id = f.facility_type_id
          left join facility_ownerships fo on fo.id = f.facility_ownership_id
          left join facilities n on n.id = f.nearest_similar_facility_id
          left join admin_hierarchies w on w.id = f.admin_hierarchy_id
          left join admin_hierarchies v on v.id = f.admin_hierarchy_id
          left join facility_physical_states fp on fp.id = f.physical_state_id
          left join facility_star_ratings fs on fs.id = f.star_rating_id
        where f.deleted_at ISNULL and a.id = '$admin_hierarchy_id'";
        $results = DB::select($sql_string);
        $admin_hierarchy = AdminHierarchy::find($admin_hierarchy_id);
        $html .= "<div>";
        $html .= "<table style='width: 100%;border-bottom: 2px solid #444;'>";
        $html .= "<tr>";
        $html .= "<td style='vertical-align: center;text-align: center'>";
        $html .= "<img src='images/coa.png' width='100'/><br /><span>The United Republic of Tanzania</span>";
        $html .= "</td>";
        $html .= "<td style='vertical-align: top;text-align: left'>";
        $html .= "<h3>ANNEX 14 - Table 1: Status of Health Facilities - Mapping ".$financial_year."</h3><br />";
        $html .= "<h5>Council: ".$admin_hierarchy->name."</h5><br />";
        $html .= "</td>";
        $html .= "</tr>";
        $html .= "</table>";
        $html .="</div>";
        $html .="<table style='border-collapse: collapse;border: 1px solid #444;width: 100%;padding:4px;font-size:10px;'>";
        $html .= "<tr style='background-color: gold;border: 1px solid #444;'>";
        $html .= "<th style='border: 1px solid #444;padding: 4px;'>Type of Facility</th>";
        $html .= "<th style='border: 1px solid #444;padding: 4px;'>Name of Facility</th>";
        $html .= "<th style='border: 1px solid #444;padding: 4px;'>Location & Access</th>";
        $html .= "<th style='border: 1px solid #444;padding: 4px;'>Distance to the nearest Facility</th>";
        $html .= "<th style='border: 1px solid #444;padding: 4px;'>Catchment Population</th>";
        $html .= "<th style='border: 1px solid #444;padding: 4px;'>Year of Population</th>";
        $html .= "<th style='border: 1px solid #444;padding: 4px;'>Physical Status</th>";
        $html .= "<th style='border: 1px solid #444;padding: 4px;'>";
        $facility_custom_details = FacilityCustomDetail::orderBy('created_at','asc')->get();
        $html .="<table style='border-collapse: collapse;width: 100%;padding:4px;'>";
        $html .= "<tr>";
        foreach ($facility_custom_details as $item) {
            $html .= "<th style='padding: 4px;'>".$item->name."</th>";
        }
        $html .= "</tr>";
        $html .= "</table>";
        $html .="</th>";
        $html .= "</tr>";
        if(count($results) >0 ){
            foreach ($results as $result) {
                $facility_id = $result->facility_id;
                $activities = DB::table("activity_fac");
                $values = FacilityCustomDetailValue::with('facility_custom_detail')->where('facility_id',$facility_id)->get();
                $html .= "<tr style='border: 1px solid #444;'>";
                $html .= "<td style='border: 1px solid #444;padding: 4px;'>".$result->facility_type."</td>";
                $html .= "<td style='border: 1px solid #444;padding: 4px;'>".$result->facility."</td>";
                $html .= "<td style='border: 1px solid #444;padding: 4px;'>".$result->location_and_access."</td>";
                $html .= "<td style='border: 1px solid #444;padding: 4px;'>".$result->nearest_similar_facility_distance."</td>";
                $html .= "<td style='border: 1px solid #444;padding: 4px;'>".$result->catchment_population."</td>";
                $html .= "<td style='border: 1px solid #444;padding: 4px;'>".$result->operation_year."</td>";
                $html .= "<td style='border: 1px solid #444;padding: 4px;'>".$result->physical_status."</td>";
                $html .= "<td style='border: 1px solid #444;padding: 4px;'>";
                $html .="<table style='border-collapse: collapse;width: 100%;padding:4px;'>";
                $html .= "<tr>";
                foreach ($values as $item) {
                    $html .= "<th style='padding: 4px;'>".$item->detail_value."</th>";
                }
                $html .= "</tr>";
                $html .= "</table>";
                $html .="</td>";
                $html .= "</tr>";
            }
        }
        $html .= "</table><br /><br />";
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($html)->setPaper('A4', 'landscape');
        return $pdf->download('Annex_14_table_1_status_of_facilities_mapping_'.$financial_year.'.pdf');
    }
}
