<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Report\Budget;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\FinancialYear;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BudgetController extends Controller {

    private $adminHierarchies = [];

    public function adminHierarchyLevel($id) {
        array_push($this->adminHierarchies, $id);
        $adminHierarchyId = AdminHierarchy::where('parent_id', $id)->get();
        foreach ($adminHierarchyId as $key => $value) {
            array_push($this->adminHierarchies, $value->id);
            //check if has children
            $count = AdminHierarchy::where('parent_id', $value->id)->count();
            if ($count > 0) {
                $this->adminHierarchyLevel($value->id);
            }
        }
    }

    public function summary(Request $request) {
        ini_set('max_execution_time', 600);
        $financial_year_id = $request->financial_year_id;
        if (empty($financial_year_id)) {
            $financial_year = FinancialYearServices::getPlanningFinancialYear();
            $financial_year_id = $financial_year->id;
        }
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $groupOption = $request->groupByOption;
        if (empty($admin_hierarchy_id) || is_null($admin_hierarchy_id)) {
            $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
            $this->adminHierarchyLevel($admin_hierarchy_id);
            $admins = implode(",", $this->adminHierarchies);
            $all = Budget::summary($financial_year_id, $groupOption, $admins);
            $data = ['items' => $all];
            return response()->json($data, 200);
        } else {
            $all = Budget::summary($financial_year_id, $groupOption, $admin_hierarchy_id);
            $data = ['items' => $all];
            return response()->json($data, 200);
        }
    }

    public function summaryV2(Request $request){
        $financial_year_id = $request->financial_year_id;
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $budget_class = $request->budget_class;
        $type = $request->type;
        $result = Budget::summaryV2($financial_year_id,$admin_hierarchy_id,$budget_class,$type);
        $data = ['items' => $result];
        return response()->json($data, 200);
    }
}
