<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Budgeting\ActivityFacilityFundSourceInputBreakdown;
use App\Models\Setup\FinancialYear;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use JasperPHP\JasperPHP;

class ProcurementController extends Controller {

    public function paginated(Request $request) {
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $financial_year_id = $request->financial_year_id;

        if ($admin_hierarchy_id == null || $admin_hierarchy_id == '') {
            $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        }

        if ($financial_year_id == null || $financial_year_id == '') {
            $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
        }

        if (($admin_hierarchy_id == null || $admin_hierarchy_id == '') && ($financial_year_id == null || $financial_year_id == '')) {
            $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
            $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
        }

        $sql = "select 
                act.description as activity,
                s.name as cost_centre,
                s.code as cost_centre_code,
                string_agg(p.name,',') as period,
                fy.name as financial_year,
                a.name as council, 
                ac.unit, 
                ac.unit_price, 
                ac.quantity, 
                ac.frequency, 
                ac.total,
                ac.gfs_description,
                ac.gfs_code,
                sec.name as sector
                from
                admin_hierarchies a inner join  mtefs m on m.admin_hierarchy_id = a.id
                inner join  mtef_sections ms on ms.mtef_id = m.id
                inner join  sections s on s.id = ms.section_id
                inner join sectors sec on s.sector_id = sec.id
                inner join financial_years fy on fy.id = m.financial_year_id
                inner join activities act on act.mtef_section_id = ms.id
                inner join
            (
            select
             a.id,u.name as unit,afi.unit_price,afi.quantity,afi.frequency,
             sum(afi.quantity*afi.unit_price*afi.frequency) as total,
             gfs.code as gfs_code,
             gfs.description as gfs_description
             from
             activities a inner join activity_facilities af on a.id = af.activity_id
             inner join activity_facility_fund_sources aff on aff.activity_facility_id = af.id
             inner join activity_facility_fund_source_inputs afi on afi.activity_facility_fund_source_id = aff.id
             inner join gfs_codes gfs on afi.gfs_code_id = gfs.id
             left join units u on u.id = afi.unit_id
             where afi.is_procurement = true
             group by a.id,u.id, afi.id,gfs.code,gfs.description
             ) ac on ac.id = act.id
            inner join activity_periods ap on ap.activity_id = act.id
            inner join periods p on p.id = ap.period_id where a.id =  $admin_hierarchy_id  and fy.id = $financial_year_id
            group by a.id, act.id, s.id,fy.id, ac.unit,ac.unit_price, ac.quantity, ac.frequency, ac.total, ac.gfs_description,ac.gfs_code,sec.name
            order by s.code asc";
        $all = DB::select($sql);
        $data = ['procurements' => $all];
        return response()->json($data, 200);
    }

    public function items($id) {
        $items = ActivityFacilityFundSourceInputBreakdown::where('activity_facility_fund_source_input_id', $id)->get();
        $data = ['items' => $items];
        return response()->json($data, 200);
    }

    public function printPDF() {
        require __DIR__ . '/../../../../vendor/autoload.php';
        $input = __DIR__ . '/../../../../app/Http/Controllers/Report/reports/other/procurement_plan.jrxml';
        $output = __DIR__ . '/../../../../public/reports/';
        $compiled = app_path() . '/Http/Controllers/Report/reports/other/procurement_plan.jasper';
        $jasper = new JasperPHP;
        $jasper->compile($input)->execute();
        $jasper->process($compiled, $output, $this->options('planrep4','planrep','Passw0rd123'))->output();
        /*$file = $jasper->process($compiled, $output, $options)->output();
        echo $file;die();*/
        $file = URL::asset('/reports/procurement_plan.pdf');
        return view('report/onscreen_pdf', ['report' => $file]);
    }

    private function options($db,$db_username,$db_password){
        $logo = __DIR__ . '/../../../../public/images/logo.png';

        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;

        $options = [
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => [
                'admin_hierarchy_id' => $admin_hierarchy_id,
                'financial_year_id' => $financial_year_id,
                'logo' => $logo,
                'footer_text' => "PlanRep",
            ],
            'db_connection' => [
                'driver' => 'postgres',
                'host' => env('DB_HOST', 'localhost'),
                'port' => env('DB_PORT', '5432'),
                'database' => env('DB_DATABASE', $db),
                'username' => env('DB_USERNAME', $db_username),
                'password' => env('DB_PASSWORD', $db_password),
            ]
        ];
        return $options;
    }
}
