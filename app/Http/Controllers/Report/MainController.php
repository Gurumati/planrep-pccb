<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Jobs\GenerateSingleReportFile;
use App\Models\Planning\Activity;
use App\Models\Planning\CasPlanTableDetail;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\CasPlanTable;
use App\Models\Setup\FundSource;
use App\Models\Setup\logo;
use App\Models\Setup\Period;
use App\Models\Setup\PriorityArea;
use App\Models\Setup\Report;
use App\Models\Setup\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use JasperPHP\JasperPHP;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class MainController extends Controller
{

    private $input;
    private $output;
    private $compiled;

    public function index()
    {
        $title = "Report|" . $this->app_initial;
        return view('report/index', ['js_scripts' => $this->form_scripts, 'title' => $title]);
    }

    public function setValues($path_in, $path_out, $name, $format)
    {
        require __DIR__ . '/../../../../vendor/autoload.php';
        $basePathIn = __DIR__ . '/reports/';
        $basePathOut = __DIR__ . '/../../../../resources/assets/pdf/';
        $this->output = $basePathOut . $path_out;
        $this->compiled = $basePathIn . $path_in . "/" . $name . ".jasper";
        $this->input = $basePathIn . $path_in . "/" . $name . ".jrxml";
    }

    public function report_home()
    {
        return view('report/report_home', ['menu' => $this->menu()]);
    }

    public function get_children_menu($id)
    {
        $children = Report::where('parent_id', $id)->get();
        return $children;
    }

    public function count_children_menu($id)
    {
        $children = Report::where('parent_id', $id)->count();
        return $children;
    }

    public function menu()
    {

        $children = Report::where('parent_id', 2)->get();
        $menu_string = "";
        ob_start();
        foreach ($children as $child) {

            echo "<li><a href=" . url('display_report/' . $child->id) . "><i class='icon-coin-dollar'></i> <span>$child->description</span></a>";
            if ($this->count_children_menu($child->id) > 0) {
                echo "<ul>";
                echo $this->print_child_menu($child->id);
                echo "</ul>";
            }
            echo "</li>";
        }
        $menu_string = ob_get_contents();
        ob_end_clean();
        if ($this->get_children_menu(0))
            return $menu_string;
    }

    public function print_child_menu($id)
    {
        $children = $this->get_children_menu($id);
        $menu_string = "";
        ob_start();
        foreach ($children as $child) {

            echo "<li><a href=" . url('display_report/' . $child->id) . "> <span>$child->description</span></a>";
            if ($this->count_children_menu($child->id) > 0) {
                echo "<ul>";
                echo $this->print_child_menu($child->id);
                echo "</ul>";
            }
            echo "</li>";
        }
        $menu_string = ob_get_contents();
        ob_end_clean();
        return $menu_string;
    }

    public function report_no_parameters($report)
    {
        require __DIR__ . '/../../../../vendor/autoload.php';
        $input = __DIR__ . '/reports/mtef/' . $report . '.jrxml';
        $output = __DIR__ . '/../../../../resources/assets/pdf';
        $compiled = __DIR__ . '/reports/mtef/' . $report . '.jasper';
        $logo = __DIR__ . '/reports/mtef/images/logo.png';

        $connection = Config::get('app');
        $options = [
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => ['admin_hierarchy_id' => UserServices::getUser()->admin_hierarchy_id, 'logo' => $logo],
            'db_connection' => [
                'driver'   => 'postgres',
                'host'     => $connection['REPORTING_DB_HOST'],
                'port'     => $connection['REPORTING_DB_PORT'],
                'database' => $connection['REPORTING_DB_DATABASE'],
                'username' => $connection['REPORTING_DB_USERNAME'],
                'password' => $connection['REPORTING_DB_PASSWORD'],
            ]
        ];

        $jasper = new JasperPHP;
        $jasper->compile($input)->execute();

        $jasper->process(
            $compiled,
            $output,
            $options
        )->execute();

        $title = "Report|" . $this->app_initial;

        $file = URL::asset('/pdf/' . $report . '.pdf');
        return view('report/display_onscreen', [
            'js_scripts' => $this->form_scripts,
            'report' => $file, 'menu' => $this->menu()
        ]);
    }

    public function display_report($id)
    {
        $report = Report::where('id', $id)->first();
        $parameters = explode(',', $report->parameters);

        if (in_array('admin_hierarchy_id', $parameters))
            $admin_hierarchy = AdminHierarchy::where('id', UserServices::getUser()->admin_hierarchy_id)->get();
        else
            $admin_hierarchy = null;

        if (in_array('region_id', $parameters))
            $regions = AdminHierarchy::where('admin_hierarchy_level_id', 2)->get();
        else
            $regions = null;

        if (in_array('sector_dept_id', $parameters))
            $sections = Section::where('section_level_id', 2)->get();
        else
            $sections = null;

        if (in_array('fund_source_id', $parameters))
            $fund_sources = FundSource::where('fund_source_id', '!=', null)->get();
        else
            $fund_sources = null;

        if (in_array('budget_class_id', $parameters))
            $budget_classes = BudgetClass::get();
        else
            $budget_classes = null;
        return view('report/display_report', [
            'js_scripts' => $this->form_scripts,
            'report' => 'noFile', 'menu' => $this->menu(), 'reportTittle' => $report->description,
            'sections' => $sections,
            'fund_source' => $fund_sources,
            'report_id' => $id,
            'regions' => $regions,
            'admin_hierarchy' => $admin_hierarchy,
            'budget_classes' => $budget_classes,
            'parameters' => $parameters
        ]);
    }

    public function barChart($report_name, $id)
    {
        return ['name' => $report_name, 'id' => $id];
    }

    public function reportList()
    {
        $admin_hierarchy = AdminHierarchy::where('id', UserServices::getUser()->admin_hierarchy_id)->first();
        $name = $admin_hierarchy->name;
        $reports = [];
        $report = [];
        $report[0] = ['name' => 'Partner Shares', 'path' => 'Partner Shares'];
        $report[1] = ['name' => 'Burden of Disease', 'path' => 'burdenOfDisease'];
        $reports = ['report' => $report, 'council' => $name, 'name' => 'Burden of Disease'];
        return $reports;
    }

    public function getDownload($name)
    {
        $outputPdfRtf = __DIR__ . '/../../../../resources/assets/pdf';
        $file = $outputPdfRtf . "/" . $name;

        return Response::make(file_get_contents($file), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename=' . $name
        ]);
    }

    public function dashlets()
    {
        return response()->json([
            'title' => 'Report Center',
        ]);
    }

    public function export_pdf($id, $report, $folder)
    {
        require __DIR__ . '/../../../../vendor/autoload.php';
        $input = __DIR__ . '/reports/' . $folder . '/' . $report . '.jrxml';
        $output = __DIR__ . '/../../../../resources/assets/pdf';
        $compiled = __DIR__ . '/reports/' . $folder . '/' . $report . '.jasper';

        $logo = __DIR__ . '/reports/mtef/images/logo.png';
        $params['id'] = $id;
        $params['logo'] = $logo;
        //'params' => ['admin_hierarchy_id' => UserServices::getUser()->admin_hierarchy_id, 'logo' => $logo],

        $options = $this->getOptions($params);
        $jasper = new JasperPHP;

        // compiling jrxml file into jasper file
        $jasper->compile($input)->execute();

        // echo
        $jasper->process(
            $compiled,
            $output,
            $options
        )->execute();
        $file = URL::asset('/pdf/' . $report . '.pdf');
        $message = ['fileUrl' => $file];
        return $message;
    }

    ///comprehensive plan report

    public function casReport(Request $request)
    {
        $data = json_decode($request->getContent());
        $is_assessment_preview = false;
        $is_attachment = false;
        $url = "";
        $table_id = 0;
        $parameters = $data->reportParameter;
        $params = [];
        foreach ($parameters as $key => $value) {
            if ($key !=  'pisc_logo'and $key !=  'logo' and $key != 'footer_text' and $key != 'is_assessment_report' and $key != 'table_id' and $key != 'budget_type')
                $params[$key] = $value;

            if ($key ==  'is_assessment_report')
                $is_assessment_preview = true;

            if ($key ==  'table_id')
                $table_id  = $value;
        }

        if ($is_assessment_preview) {
            $admin_hierarchy_id  = $params['admin_hierarchy_id'];
            $financial_year_id   = $params['financial_year_id'];
            $table = CasPlanTable::find($table_id);
            $tableType = isset($table->type) ? $table->type : null;
            $tableReportUrl = isset($table->report_url) ? $table->report_url : null;

            //Type 1 = Jasper Reports
            //Type 2 = Uploaded files
            if ($tableReportUrl == null and $tableType == 1) {
                $detail = CasPlanTableDetail::where('cas_plan_table_id', $table_id)
                    ->where('admin_hierarchy_id', $admin_hierarchy_id)
                    ->where('financial_year_id', $financial_year_id)
                    ->first();

                $url = "uploads/comprehensive_plans/" . $detail->url;
                $is_attachment = true;
            }
        }

        if ($is_attachment) {
            $message = ['reportUrl' => $url];
        } else {

            if (isset($params['admin_hierarchy_id'])) {
                $council = $params['admin_hierarchy_id'];
                //get council name
                $_result = DB::table('admin_hierarchies')
                    ->where('id', $council)
                    ->select('name')
                    ->first();
                $council_name =  preg_replace('/\s+/', '', $_result->name);
            } else {
                $council = '0000';
            }

            if (isset($params['financial_year_id'])) {
                //get financial year name
                $_result = DB::table('financial_years')
                    ->where('id', $params['financial_year_id'])
                    ->select('name')
                    ->first();
                $financial_year =  str_replace('/', '_', $_result->name);
            }
            require base_path('vendor/autoload.php');

            $pisc_path = DB::table('logos')
                        ->where('admin_hierarchy_id', $params['admin_hierarchy_id'])
                        ->select('path')
                        ->first();
            $pisc_logo = null;
            $input = __DIR__ . '/' . $data->report_url . '.jrxml';
            $output =   public_path('reports/' . $council);
            $compiled = __DIR__ . '/' . $data->report_url . '.jasper';
            $logo = __DIR__ . '/reports/mtef/images/logo.png';
             if($pisc_path){
                $pisc_logo = public_path().'/'.$pisc_path->path;
             }

            $jasper = new JasperPHP;
            $params_one = $jasper->listParameters($input)->execute();


            foreach ($params_one as $parameter_description) {


                if (str_contains($parameter_description, 'footer_text')) {
                    $params['footer_text'] = 'Planrep Version 1.0';
                }

                if (str_contains($parameter_description, 'logo')) {
                    $params['logo'] = $logo;
                }

                if (str_contains($parameter_description, 'pisc_logo')) {
                    if($pisc_logo != null){
                        $params['pisc_logo'] = $pisc_logo;
                    }
                }

                if (str_contains($parameter_description, 'sub_report')) {
                    $params['sub_report']  = __DIR__ . '/' . $data->report_url . '_comments.jasper';
                }

                if (str_contains($parameter_description, 'checked')) {
                    $params['checked'] = __DIR__ . '/reports/mtef/images/ok.png';
                }

                if (str_contains($parameter_description, 'cancelled')) {
                    $params['cancelled'] = __DIR__ . '/reports/mtef/images/cancel.png';
                }

                if (str_contains($parameter_description, 'region_id')) {
                    $params['region_id'] = $params['admin_hierarchy_id'];
                    //remove admin_hierarchy_id from params
                    unset($params['admin_hierarchy_id']);
                }

                if (str_contains($parameter_description, 'budget_type')) {
                    $params['budget_type'] = isset($parameters->budget_type) ? $parameters->budget_type : 'CURRENT';
                }

                if (str_contains($parameter_description, 'intervention_id')) {
                    $array = $params['intervention_id'];
                    $string = '';
                    foreach ($array as $value) {
                        $string .=  $value . ',';
                    }
                    $params['intervention_id'] = rtrim($string, ',');
                }
            }

            //create directory if it doesn't exist
            if (!file_exists($output)) {
                mkdir($output, 0777, true);
            }
            $options = $this->getOptions($params);

            if (isset($data->report_type)) {
                $format = $data->report_type;
            } else {
                $format = 'pdf';
            }

            $options['format'] = array($format);
            //setup line break policy
            $options['FORCE_LINEBREAK_POLICY'] = true;


            // compiling jrxml file into jasper file
            $jasper->compile($input)->execute();
            $jasper->process(
                $compiled,
                $output,
                $options
            )->execute();
            if (isset($data->report_type)) {
                $report_type = $data->report_type;
            } else {
                $report_type = 'pdf';
            }

            $title = "Report|" . $this->app_initial;
            $fullPath = explode('/', $data->report_url);
            $fileName = end($fullPath);

            $file = ('/reports/' . $council . '/' . $fileName . '.' . $report_type);
            if (isset($council_name) && isset($financial_year) && $is_assessment_preview == false) {
                $file_name = $council_name . '_' . $fileName . '_' . $financial_year;
                $new_file = '/reports/' . $council . '/' . $file_name . '.' . $report_type;
                //rename file
                if (file_exists(public_path($new_file))) {
                    File::delete(public_path($new_file));
                }
                Storage::move($file, $new_file); // keep the same folder to just rename
                $file = $new_file;
            }
            $flag = false;
            $message = ['reportUrl' => $file, 'noContent' => $flag];
        }
        return response()->json($message);
    }

    public static function getOptions($params)
    {
        $connection = Config::get('app');
        $options = [
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => $params,
            'db_connection' => [
                'driver'   => 'postgres',
                'host'     => $connection['REPORTING_DB_HOST'],
                'port'     => $connection['REPORTING_DB_PORT'],
                'database' => $connection['REPORTING_DB_DATABASE'],
                'username' => $connection['REPORTING_DB_USERNAME'],
                'password' => $connection['REPORTING_DB_PASSWORD'],
            ]
        ];

        return $options;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    //comprehensive plan report parameter options
    public function casReportParams(Request $request)
    {
        $all = [];
        $data = json_decode($request->getContent());
        $admin_hierarchy_id = isset($data->admin_hierarchy_id) ? $data->admin_hierarchy_id : null;
        $financial_year_id  = isset($data->financial_year_id) ? $data->financial_year_id : 
        (FinancialYearServices::getExecutionFinancialYear()!=null?FinancialYearServices::getExecutionFinancialYear()->id:FinancialYearServices::getPlanningFinancialYear()->id);
        
        $params = $data->parameters;

        foreach ($params as $key => $parameter) {
            $parameter = trim($parameter);
            if ($parameter == 'sector_dept_id') {
                $sections = Section::where('section_level_id', 2)->get();
                $all['sector_dept_id'] = $sections;
            }
            /** cost centre for health only */
            if ($parameter == 'cost_centre_id') {
                $sections = Section::where('section_level_id', 4)
                    ->where('parent_id', 81)
                    ->select('id', 'name')
                    ->get();
                $all['cost_centre_id'] = $sections;
            }

            /** get interventions for health only */
            if ($parameter == 'intervention_id') {
                $sections = PriorityArea::with('interventions')->whereHas('sectors', function ($q) {
                    $q->where('sector_id', 1);
                })->whereHas('priority_area_versions', function ($q2) {
                    $q2->whereHas('version', function ($q3) {
                        $q3->where('is_current', true);
                    });
                })->orderBy('number')
                    ->get();
                $all['intervention_id'] = $sections;
            }

            if ($parameter == 'fund_source_id') {
                $fund_sources = FundSource::get();
                $all['fund_source_id'] = $fund_sources;
            }

            if ($parameter == 'budget_class_id') {
                $budget_classes = BudgetClass::get();
                $all['budget_class_id'] = $budget_classes;
            }

            if ($parameter == 'exchange_rate') {
                //get value
                $exchange_rate = DB::table('baseline_statistic_values as bv')
                    ->where('bv.admin_hierarchy_id', $admin_hierarchy_id)
                    ->where('bv.baseline_statistic_id', 1)
                    ->first()->value;
                if (!($exchange_rate > 0)) {
                    $exchange_rate = DB::table('baseline_statistic_values as bv')
                        ->where('bv.admin_hierarchy_id', 382)
                        ->where('bv.financial_year_id', $financial_year_id)
                        ->where('bv.baseline_statistic_id', 1)
                        ->first()->value;
                    //$exchange_rate = '2230'; //default exchange rate
                }
                $all['exchange_rate'] = $exchange_rate;
            }

            if ($parameter == 'is_facility_account') {
                $all['is_facility_account'] = false;
            }

            if ($parameter == 'init_page_number') {
                $all['init_page_number'] = 1;
            }

            if ($parameter == 'fund_source_pe') {
                //get fund sources
                $fund_source_id = DB::table('budget_submission_forms')
                    ->distinct()
                    ->orderBy('fund_sources','DESC')
                    ->first()->fund_sources;

                $ids = explode(',', $fund_source_id);
                $fund_sources = DB::table('fund_sources')
                    ->whereIn('id', $ids)
                    ->select('id', 'name')
                    ->get();
                $all['fund_source_pe'] = $fund_sources;
            }

            if ($parameter == 'checked') {
                $all['checked'] = __DIR__ . '/reports/mtef/images/ok.png';
            }

            if ($parameter == 'cancelled') {
                $all['cancelled'] = __DIR__ . '/reports/mtef/images/cancel.png';
            }

            if ($parameter == 'footer_text') {
                $all['footer_text'] = 'PlanRep V01';
            }

            if ($parameter == 'control_code') {
                $control_code = DB::table('budget_classes')
                    ->distinct()
                    ->select('control_code as code')
                    ->whereNotNull('control_code')
                    ->get();
                $all['control_code'] = $control_code;
            }

            if ($parameter == 'project_id') {
                $projects = DB::table('projects')
                    ->join('activities', 'projects.id', '=', 'activities.project_id')
                    ->join('mtef_sections', 'activities.mtef_section_id', '=', 'mtef_sections.id')
                    ->join('mtefs', 'mtef_sections.mtef_id', '=', 'mtefs.id')
                    ->select('projects.id as id', 'projects.name as name')
                    ->where('mtefs.financial_year_id', '=', $financial_year_id)
                    ->where('mtefs.admin_hierarchy_id', '=', $admin_hierarchy_id)
                    ->distinct()
                    ->get();
                $all['project_id'] = $projects;
            }
            /** Return 4 quarters */
            if ($parameter == 'period_id') {
                $periods = DB::table('periods')
                    ->where('financial_year_id', $financial_year_id)
                    ->orderBy('id')
                    ->get();
                $all['period_id'] = $periods;
            }
        }
        $message = ['parameter_data' => $all];
        return response()->json($message);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateSingleDocument(Request $request)
    {
        $data = json_decode($request->getContent());
        $cas_plan_id = $data->cas_plan_id;
        $financial_year_id = $data->financial_year_id;
        $admin_hierarchy_id = $data->admin_hierarchy_id;

        //Dispatch the process to a job for execution
        dispatch(new GenerateSingleReportFile($cas_plan_id, $financial_year_id, $admin_hierarchy_id));
        return response()->json(["successMessage" => "THE_REPORT_IS_BEING_PROCESSED_YOU_WILL_BE_NOTIFIED_WHEN_COMPLETED"], 200);
    }

    public function cdr(Request $request)
    {
        set_time_limit(600);
        $financialYearId = $request->financialYearId;
        //$periodId = $request->periodId;
        $adminHierarchyId = $request->adminHierarchyId;
        if (isset($request->perPage)) {
            $perPage = $request->perPage;
        } else {
            $perPage = 10;
        }
        return response()->json(['items' => Activity::cdr($financialYearId, $adminHierarchyId, $perPage)], 200);
    }

    public function downloadCdr(Request $request)
    {
        set_time_limit(600);
        $financialYearId = $request->financialYearId;
        //$periodId = $request->periodId;
        $adminHierarchyId = $request->adminHierarchyId;
        $items = Activity::cdr($financialYearId, $adminHierarchyId);
        $array = array();
        foreach ($items as $value) {
            $data = array();
            $data['REGION'] = $value->region;
            $data['COUNCIL'] = $value->council;
            $data['DEPARTMENT'] = $value->department;
            $data['COST_CENTRE'] = $value->cost_center;
            $data['FUND_SOURCE'] = $value->fund_source;
            $data['PROJECT_NAME'] = $value->project_name;
            $data['ACTIVITY'] = $value->activity;
            $data['BUDGET_TYPE'] = $value->budget_type;
            $data['FACILITY'] = $value->facility;
            $data['LGA_LEVEL'] = $value->lga_level;
            $data['FACILITY_TYPE'] = $value->facility_type;
            $data['PROJECT_TYPE'] = $value->project_type;
            $data['EXPENDITURE_CATEGORY'] = $value->expenditure_category;
            $data['PLANNED_PERIOD'] = $value->period;
            $data['IMPLEMENTED_PERIOD'] = $value->implemented_quarter;
            $data['PROJECT_OUTPUT'] = $value->project_output;
            $data['PLANNED_VALUE'] = $value->planned_value;
            $data['ACHIEVED_VALUE'] = $value->achievement_value;
            $data['ACTUAL_IMPLEMENTATION'] = $value->actual_implementation;
            $data['REMARKS'] = $value->remarks;
            $data['BUDGET'] = $value->budget;
            $data['EXPENDITURE'] = $value->expenditure;
            array_push($array, $data);
        }

        Excel::create('CDR_' . time(), function ($excel) use ($array) {
            $excel->sheet('DATA', function ($sheet) use ($array) {
                $sheet->fromArray($array);
            });
        })->export('csv');
    }


    //Consolidated CDR Report

    public function consolidatedCdr(Request $request)
    {
        set_time_limit(6000);
        $financialYearId = $request->financialYearId;
        $regionId = $request->regionId;
        if (isset($request->perPage)) {
            $perPage = $request->perPage;
        } else {
            $perPage = 10;
        }
        return response()->json(['items' => Activity::consolidatedCdr($financialYearId, $regionId, $perPage)], 200);
    }

    //consolidated cdr file download

    public function consolidatedCdrDownload(Request $request)
    {
        set_time_limit(0);
        $financialYearId = $request->financialYearId;
        $regionId = $request->regionId;
        $items = Activity::consolidatedCdr($financialYearId, $regionId);
        $array = array();
        foreach ($items as $value) {
            $data = array();
            $data['REGION'] = $value->region;
            $data['COUNCIL'] = $value->council;
            $data['DEPARTMENT'] = $value->department;
            $data['COST_CENTRE'] = $value->cost_center;
            $data['FUND_SOURCE'] = $value->fund_source;
            $data['PROJECT_NAME'] = $value->project_name;
            $data['ACTIVITY'] = $value->activity;
            $data['BUDGET_TYPE'] = $value->budget_type;
            $data['FACILITY'] = $value->facility;
            $data['LGA_LEVEL'] = $value->lga_level;
            $data['FACILITY_TYPE'] = $value->facility_type;
            $data['PROJECT_TYPE'] = $value->project_type;
            $data['EXPENDITURE_CATEGORY'] = $value->expenditure_category;
            $data['PLANNED_PERIOD'] = $value->period;
            $data['IMPLEMENTED_PERIOD'] = $value->implemented_quarter;
            $data['PROJECT_OUTPUT'] = $value->project_output;
            $data['PLANNED_VALUE'] = $value->planned_value;
            $data['ACHIEVED_VALUE'] = $value->achievement_value;
            $data['ACTUAL_IMPLEMENTATION'] = $value->actual_implementation;
            $data['REMARKS'] = $value->remarks;
            $data['BUDGET'] = $value->budget;
            $data['EXPENDITURE'] = $value->expenditure;
            array_push($array, $data);
        }

        Excel::create('CDR_' . time(), function ($excel) use ($array) {
            $excel->sheet('DATA', function ($sheet) use ($array) {
                $sheet->fromArray($array);
            });
        })->export('csv');
    }
}
