<?php

namespace App\Http\Controllers\Report;

use App\Models\Setup\Facility;
use App\Models\Setup\FinancialYear;
use App\Models\Setup\FundSource;
use App\Models\Setup\GfsCode;
use App\Models\Setup\Section;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BudgetImportMissingAccountsController extends Controller
{
    public static function match(){
        $financial_systems = array('EPICOR','FFARS');
        foreach ($financial_systems as $financial_system_code){
            $missing = DB::table('budget_import_missing_accounts')
                ->select([
                    'financial_system_code',
                    'chart_of_accounts',
                    'debit_amount',
                    'credit_amount',
                    'admin_hierarchy_id',
                    'financial_year_id',
                    'transaction_type',
                    'bookID',
                    DB::raw('"JEDate"::date'),
                    'id'
                ])
                ->where([
                    'financial_year_id' => 2,
                    'transaction_type'  => 'EXPENDITURE',
                    'financial_system_code' => $financial_system_code
                ])->take(100000)->get();

            foreach ($missing as $data)
            {
                $missing_account_id = $data->id;
                $book_id = $data->bookID;
                $debit_amount = $data->debit_amount;
                $credit_amount = $data->credit_amount;
                $je_date = $data->JEDate;
                $chart_of_accounts = $data->chart_of_accounts;
                $account = explode("-",$data->chart_of_accounts);
                $gfs = $account[9];

                /**
                 * Get the period_id from the JEDate
                 */
                $query = "select * from periods where '".$je_date."' between start_date and end_date";
                $get_period = DB::select(DB::raw($query));
                $period_id = isset($get_period[0]->id) ? $get_period[0]->id : null;

                /**
                 * Check if the gfs_code exists
                 **/
                $gfs_code = GfsCode::where('code','=',$gfs)->first();
                $gfs_code_id = isset($gfs_code->id) ? $gfs_code->id : null;
                $admin_hierarchy_id = $data->admin_hierarchy_id;
                $financial_year_id  = $data->financial_year_id;

                /**
                 * Get the section code
                 */
                $section_code = $account[2];
                $section = Section::where('code',$section_code)->first();
                $section_id = isset($section->id) ? $section->id : null;

                /**
                 * Get the fund source code
                 */
                $fund_source_code = $account[8];
                $fund_source = FundSource::where('code',$fund_source_code)->first();
                $fund_source_id = isset($fund_source->id) ? $fund_source->id : null;

                if ($financial_system_code == 'EPICOR'){
                    if($gfs_code_id !== null and $section_id !== null and $fund_source_id !== null){

                        $result = DB::table('budget_export_accounts as x')
                            ->join('activity_facility_fund_source_inputs as i','x.activity_facility_fund_source_input_id','=','i.id')
                            ->join('activity_facility_fund_sources as aff','i.activity_facility_fund_source_id','=','aff.id')
                            ->join('fund_sources as fs','fs.id','=','aff.fund_source_id')
                            ->join('activities as a','a.id','=','x.activity_id')
                            ->where([
                                'x.gfs_code_id'     => $gfs_code_id,
                                'x.financial_year_id' => $financial_year_id,
                                'x.admin_hierarchy_id' => $admin_hierarchy_id,
                                'x.section_id' => $section_id,
                                'fs.id' => $fund_source_id
                            ])->select([
                                'x.id as budget_export_account_id',
                                'a.budget_class_id',
                                'a.budget_type'])
                            ->first();

                        if ($result !== null){
                            $budget_export_account_id = isset($result->budget_export_account_id) ? $result->budget_export_account_id : null;

                            /***
                             *  Read the returned id and removed the entry from unknown transactions
                             *  Insert an entry to the budget_import_items table
                             */

                            $expenditure_data = [
                                'budget_export_account_id' => $budget_export_account_id,
                                'period_id' => $period_id,
                                'BookID' => $book_id,
                                'FiscalYear' => 'RSLVD',
                                'JEDate' => $je_date,
                                'Account' => $chart_of_accounts,
                                'date_imported' => Carbon::now(),
                                'gfs_code_id' => $gfs_code_id,
                                'budget_class_id' => $result->budget_class_id,
                                'fund_source_id' => $fund_source_id,
                                'debit_amount' => $debit_amount,
                                'credit_amount' => $credit_amount,
                                'budget_type' => $result->budget_type
                            ];

                            DB::transaction(function () use ($missing_account_id, $expenditure_data) {
                                DB::table('budget_import_missing_accounts')->delete($missing_account_id);
                                DB::table('budget_import_items')->insert([$expenditure_data]);
                                echo 'Transaction generated successfully' . "\xA";
                            });
                        }
                    }
                } else {
                    /**
                     * The expenditure is from FFARS, Get the facility_id
                     */
                    $facility_code = $account[4];
                    $facility = Facility::where('facility_code',$facility_code)->first();
                    $facility_id = isset($facility->id) ? $facility->id : null;

                    if($gfs_code_id !== null and $section_id !== null and $fund_source_id !== null and $facility_id !== null){

                        $result = DB::table('budget_export_accounts as x')
                            ->join('activity_facility_fund_source_inputs as i','x.activity_facility_fund_source_input_id','=','i.id')
                            ->join('activity_facility_fund_sources as aff','i.activity_facility_fund_source_id','=','aff.id')
                            ->join('activity_facilities as af', 'af.id','=','aff.activity_facility_id')
                            ->join('facilities as f','f.id','=','af.facility_id')
                            ->join('fund_sources as fs','fs.id','=','aff.fund_source_id')
                            ->join('activities as a','a.id','=','x.activity_id')
                            ->where([
                                'x.gfs_code_id'     => $gfs_code_id,
                                'x.financial_year_id' => $financial_year_id,
                                'x.admin_hierarchy_id' => $admin_hierarchy_id,
                                'x.section_id' => $section_id,
                                'fs.id' => $fund_source_id,
                                'f.id' => $facility_id
                            ])->select([
                                'x.id as budget_export_account_id',
                                'a.budget_class_id',
                                'a.budget_type'])
                            ->first();

                        if ($result !== null){
                            $budget_export_account_id = isset($result->budget_export_account_id) ? $result->budget_export_account_id : null;

                            /***
                             *  Read the returned id and removed the entry from unknown transactions
                             *  Insert an entry to the budget_import_items table
                             */

                            $expenditure_data = [
                                'budget_export_account_id' => $budget_export_account_id,
                                'period_id' => $period_id,
                                'BookID' => $book_id,
                                'FiscalYear' => 'RSLVDFFARS',
                                'JEDate' => $je_date,
                                'Account' => $chart_of_accounts,
                                'date_imported' => Carbon::now(),
                                'gfs_code_id' => $gfs_code_id,
                                'budget_class_id' => $result->budget_class_id,
                                'fund_source_id' => $fund_source_id,
                                'debit_amount' => $debit_amount,
                                'credit_amount' => $credit_amount,
                                'budget_type' => $result->budget_type
                            ];

                            DB::transaction(function () use ($missing_account_id, $expenditure_data) {
                                DB::table('budget_import_missing_accounts')->delete($missing_account_id);
                                DB::table('budget_import_items')->insert([$expenditure_data]);
                                echo 'Transaction generated successfully' . "\xA";
                            });
                        }
                    }
                }
            }
        }
    }

    public static function resolveRevenueYear(){
        /**
         * Check the revenue issues emanating from wrong financial year ids
         */
        $revenue = DB::table('received_fund_items as i')
                 ->join('revenue_export_accounts as x','i.revenue_export_account_id','=','x.id')
                 ->where('JEDate','>=','2019-07-01')
                 ->select(['i.*'])
                 ->get();

        foreach ($revenue as $data){
            $received_fund_item_id = $data->id;
            $chart_of_accounts     = $data->account;
            $je_date               = $data->JEDate;

            /**
             * Get the financial_year_id from the JEDate
             */
            $query              = "select * from financial_years where '".$je_date."' between start_date and end_date";
            $get_financial_year = DB::select(DB::raw($query));
            $financial_year_id  = isset($get_financial_year[0]->id) ? $get_financial_year[0]->id : null;

            /**
             * Get the period_id from the JEDate
             */
            $query = "select * from periods where '".$je_date."' between start_date and end_date";
            $get_period = DB::select(DB::raw($query));
            $period_id = isset($get_period[0]->id) ? $get_period[0]->id : null;

            $revenue_export_account = DB::table('revenue_export_accounts')
                                    ->where('financial_year_id',$financial_year_id)
                                    ->where('chart_of_accounts',$chart_of_accounts)
                                    ->get();
            $revenue_export_account_id  = isset($revenue_export_account[0]->id) ? $revenue_export_account[0]->id : null;
            $admin_hierarchy_ceiling_id = isset($revenue_export_account[0]->admin_hierarchy_ceiling_id) ?
                                                $revenue_export_account[0]->admin_hierarchy_ceiling_id : null;

            if ($admin_hierarchy_ceiling_id !== null and $revenue_export_account_id !== null and $period_id !== null){
                DB::table('received_fund_items')
                    ->where('id', $received_fund_item_id)
                    ->update([
                        'revenue_export_account_id' => $revenue_export_account_id,
                        'admin_hierarchy_ceiling_id' => $admin_hierarchy_ceiling_id,
                        'period_id' => $period_id,
                        'fiscal_year' => 'CORRECTED'
                    ]);
                echo 'Transaction date fixed successfully' . "\xA";
            } else {
                echo 'Transaction date fix failed' . "\xA";
            }
        }
    }

    public static function removeRevenueDuplicates(){
        DB::delete("DELETE from received_fund_items i where exists (select 1
                  from received_fund_items i2 where i2.credit_amount = i.credit_amount and
                  i2.debit_amount = i.debit_amount and i2.\"JEDate\" = i.\"JEDate\" and
                  i2.id < i.id )");
    }

    public static function resolveExpenditureDates(){
        $query = "SELECT i.id AS budget_import_item_id, i.debit_amount, i.credit_amount, 
                  x.id AS budget_export_account_id, i.\"Account\" AS chart_of_accounts,
                  i.\"JEDate\" AS je_date,x.financial_year_id, x.admin_hierarchy_id, i.\"BookID\" 
                  FROM budget_import_items AS i 
                  INNER JOIN budget_export_accounts AS x on x.id = i.budget_export_account_id
                  WHERE i.\"JEDate\"::date >= '2019-07-01'::date AND x.financial_year_id = 1";
        $expenditure = DB::select(DB::raw($query));

        foreach ($expenditure as $data){
            /**
             * Get the financial_year_id from the JEDate
             */
            $query             = "SELECT id FROM financial_years WHERE '".$data->je_date."' 
                                  BETWEEN start_date AND end_date";
            $get_year          = DB::select(DB::raw($query));
            $financial_year_id = isset($get_year[0]->id) ? $get_year[0]->id : null;

            $budget_export_account = DB::table('budget_export_accounts')
                                   ->where('chart_of_accounts',$data->chart_of_accounts)
                                   ->where('financial_year_id', $financial_year_id)
                                   ->first();
            $budget_export_account_id = isset($budget_export_account->id) ? $budget_export_account->id : null;

            if ($budget_export_account !== null){
                DB::table('budget_import_items as i')
                    ->where('id',$data->budget_import_item_id)
                    ->update([
                      'budget_export_account_id' => $budget_export_account_id,
                      'updated_at' => Carbon::now()
                    ]);
                echo "Match found ". "\xA";
            } else {
                $missing_data = array(
                    'chart_of_accounts'  => $data->chart_of_accounts,
                    'financial_year_id'  => $financial_year_id,
                    'admin_hierarchy_id' => $data->admin_hierarchy_id,
                    'debit_amount' => $data->debit_amount,
                    'credit_amount' => $data->credit_amount,
                    'JEDate' => $data->je_date,
                    'bookID' => $data->BookID,
                    'reason' => 'GL_ACCOUNT_MISSING_IN_PLANREP',
                    'created_at' => Carbon::now(),
                    'financial_system_code' => 'EPICOR',
                    'transaction_type' => 'EXPENDITURE'
                );

                DB::transaction(function () use ($missing_data, $data) {
                    DB::table('budget_import_missing_accounts')->insert([$missing_data]);

                    DB::table('budget_import_items')->where(['id'=> $data->budget_import_item_id])->delete();
                    echo "Match not found". "\xA";
                });
            }
        }
    }
}
