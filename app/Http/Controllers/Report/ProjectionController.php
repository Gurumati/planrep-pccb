<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Report\Projection;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\FinancialYear;
use Illuminate\Http\Request;

class ProjectionController extends Controller {
    private $adminHierarchies = [];

    public function adminHierarchyLevel($id) {
        array_push($this->adminHierarchies, $id);
        $adminHierarchyId = AdminHierarchy::where('parent_id', $id)->get();
        foreach ($adminHierarchyId as $key => $value) {
            array_push($this->adminHierarchies, $value->id);
            //check if has children
            $count = AdminHierarchy::where('parent_id', $value->id)->count();
            if ($count > 0) {
                $this->adminHierarchyLevel($value->id);
            }
        }
    }

    public function fetchAll() {
        ini_set('max_execution_time', 300);
        $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $this->adminHierarchyLevel($admin_hierarchy_id);
        $admins = implode(",", $this->adminHierarchies);
        $all = Projection::projection($admins, $financial_year_id);
        $data = ['projections' => $all];
        return response()->json($data, 200);
    }

    public function hierarchy_projections(Request $request) {
        ini_set('max_execution_time', 300);
        $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
        if(isset($request->admin_hierarchy_id)){
            $admin_hierarchy_id = $request->admin_hierarchy_id;
            $all = Projection::projection($admin_hierarchy_id, $financial_year_id);
            $data = ['projections' => $all];
            return response()->json($data, 200);
        } else {
            $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
            $this->adminHierarchyLevel($admin_hierarchy_id);
            $admins = implode(",", $this->adminHierarchies);
            $all = Projection::projection($admins, $financial_year_id);
            $data = ['projections' => $all];
            return response()->json($data, 200);
        }
    }

    public function hierarchyYearProjections(Request $request) {
        ini_set('max_execution_time', 300);
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        if(isset($request->financial_year_id)){
            $financial_year_id = $request->financial_year_id;
            $all = Projection::projection($admin_hierarchy_id, $financial_year_id);
            $data = ['projections' => $all];
            return response()->json($data, 200);
        } else {
            $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
            $all = Projection::projection($admin_hierarchy_id, $financial_year_id);
            $data = ['projections' => $all];
            return response()->json($data, 200);
        }
    }
}
