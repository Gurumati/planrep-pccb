<?php

namespace App\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use App\Models\Budgeting\BudgetSubmissionDefinition;
use App\Models\Budgeting\BudgetSubmissionForm;
use App\Models\Report\PESubmissionFormsReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PESubmissionFormsReportController extends Controller
{
    public function index()
    {
        $all = PESubmissionFormsReport::all();
        $data = ['reports'=>$all];
        return response()->json($data);
    }


    public function getReport(Request $request)
    {
      $data = json_decode($request->getContent());
      $financial_year_id = $data->financial_year_id;
      $admin_hierarchy_id = $data->admin_hierarchy_id;
      $fund_source_id = $data->fund_source_id;
      $report_id = $data->report_id;

      //Get the query to be run from the pe_submission_forms_reports table
      $pe_submission_forms_report_data = PESubmissionFormsReport::where('id', $report_id)->first();
      $parameters = $pe_submission_forms_report_data->params;
      $params = explode(',',$parameters);
      $data_array = $params;
      array_push($data_array,$admin_hierarchy_id,$financial_year_id,$fund_source_id);
      //replace params
      $sql_query = $pe_submission_forms_report_data->sql_query;
      $all = DB::select($sql_query, $data_array);
      $submission_form_columns = $this->getSubmissionFormColumns($params[0]);
      $message = ['columns'=>$submission_form_columns, 'column_values'=>$all];

      return response()->json($message);

    }


    public function getSubmissionFormColumns($form_id){
        $submissionForm = DB::table('budget_submission_forms')
            ->join('budget_submission_sub_forms', 'budget_submission_forms.id', '=', 'budget_submission_sub_forms.budget_submission_form_id')
            ->where('budget_submission_sub_forms.id', $form_id)
            ->select('budget_submission_forms.id')
            ->first();
        //use submission form id to find form definition

        $parentForms = BudgetSubmissionDefinition::whereNull('parent_id')->with('childForm')->where('budget_submission_form_id', $submissionForm->id)->orderBy('sort_order')->get();
        return $parentForms;
    }
    public function getPeFundSources(){
        $pe_fund_source = BudgetSubmissionForm::first()->fund_sources;
        $fund_source = explode(',',$pe_fund_source);
        $pe_fund_sources = DB::table('fund_sources')->whereIn('id',$fund_source)->select('id', 'name')->get();
        return response()->json($pe_fund_sources);
    }

    public function delete($id){}
}
