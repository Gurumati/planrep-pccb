<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Models\Auth\AccessRight;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class AccessRightController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $accessRights = AccessRight::all();
        return response()->json($accessRights);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $accessRight = new AccessRight();
            $accessRight->name = $data->name;
            $accessRight->description = $data->description;
            $accessRight->save();
            //Return language success key and all data
            $message = ["successMessage" => "SUCCESSFUL_ACCESS_RIGHT_CREATED", "accessRights" => AccessRight::all()];
            return response()->json($message, 200);

        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_ACCESS_RIGHT_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $accessRight = AccessRight::find($data->id);
            $accessRight->name = $data->name;
            $accessRight->description = $data->description;
            $accessRight->save();
            //Return language success key and all data
            $message = ["successMessage" => "SUCCESSFUL_ACCESS_RIGHT_UPDATED", "accessRights" => AccessRight::all()];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_ACCESS_RIGHT_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id) {
        $accessRight = AccessRight::find($id);
        $accessRight->delete();
        //Return language success key and all data
        $message = ["successMessage" => "SUCCESSFUL_ACCESS_RIGHT_DELETED", "accessRights" => AccessRight::all()];
        return response()->json($message, 200);
    }
}
