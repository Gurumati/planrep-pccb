<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Models\Auth\UserGroup;
use Illuminate\Http\Request;


class UserGroupController extends Controller
{   
 private $limit = 2;
    public function index()
    {
        $userGroups = UserGroup::paginate($this->limit);
        return response()->json($userGroups);
    }

    public function store(Request $request)
    {
        try {
            $data = json_decode($request->getContent());
            $userGroup = new UserGroup();
            $userGroup->name = $data->name;
            $userGroup->description = $data->description;
            $userGroup->sort_order = $data->sort_order;
            $userGroup->is_active = True;
            $userGroup->save();
            //Return language success key and all data
            $message = ["successMessage" => "SUCCESSFUL_USER_GROUP_CREATED", "userGroups" => UserGroup::paginate($this->limit)];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_USER_GROUP_YEAR_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }


    }

    public function update(Request $request)
    {
        try{
            $data = json_decode($request->getContent());
            $userGroup = UserGroup::find($data->id);
            $userGroup->name = $data->name;
            $userGroup->description = $data->description;
            $userGroup->sort_order = $data->sort_order;
            $userGroup->is_active = True;
            $userGroup->save();
            //Return language success key and all data
            $message = ["successMessage" => "SUCCESSFUL_USER_GROUP_UPDATED", "userGroups" => UserGroup::paginate($this->limit)];
            return response()->json($message, 200);
        } catch (QueryException $exception){
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_USER_GROUP_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }
    public function toggleUserGroup(Request $request) {
        //
        $data = json_decode($request->getContent());
        $object = UserGroup::find($data->id);
        $object->is_active = $data->is_active;
        $object->save();
        $all = UserGroup::paginate($this->limit);
        if ($data->is_active == false) {
            $feedback = ["action" => "USER_GROUP_DEACTIVATED", "alertType" => "warning", "financialYears" => $all];
        } else {
            $feedback = ["action" => "USER_GROUP_ACTIVATED", "alertType" => "success", "financialYears" => $all];
        }
        return response()->json($feedback, 200);
    }

    public function delete($id)
    {
        $userGroup = UserGroup::find($id);
        $userGroup->delete();
        $message = ["successMessage" => "SUCCESSFULLY_USER_GROUP_DELETED", "userGroups" => UserGroup::paginate($this->limit)];
        return response()->json($message, 200);
    }
}

