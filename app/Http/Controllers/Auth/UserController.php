<?php

namespace App\Http\Controllers\Auth;

use Activation;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\RoleServices;
use App\Http\Services\UserServices;
use App\Models\Auth\Role;
use App\Models\Auth\RoleUser;
use App\Models\Auth\User;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\Section;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Mail;
use Reminder;
use Swift_TransportException;
use Validator;

/**
 * @resource User
 *
 * This Controller manage users of the system
 */
class UserController extends Controller {
    private $treeIds=[];

    private function allUsers($perPage, $queryString) {
        $flatten = new Flatten();
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section_id = UserServices::getUser()->section_id;
        $users = null;
        $Ids=[];

        if ($admin_hierarchy_id != null && $section_id != null) {

            $userAdminHierarchy = $flatten->getUserHierarchyTree();

            $Ids = $flatten->flattenAdminHierarchyWithChild($userAdminHierarchy);

            $sections = Section::where('id', $section_id)->get();

            $sectionIds = $flatten->flattenSectionWithChild($sections);

            $users = User::paginated($Ids,$sectionIds);
        }
        else {
            $users = User::with('roles')->where('id', UserServices::getUser()->id)->paginate($perPage);
        }
        return $users;
    }

    public function addFacility($userId, $facilityId){
        User::addFacility($userId, $facilityId);
        return response()->json(['succesMessage'=>"Facility Added"], Response::HTTP_OK);
    }

    public function removeFacility($userId, $facilityId){
        User::removeFacility($userId, $facilityId);
        return response()->json(['succesMessage'=>"Facility Added"], Response::HTTP_OK);
    }

    public function levelUsers(Request $request)
    {
        $searchString = $request->searchText;
        $users = User::where('deleted_at',null)->where('user_name', 'ILIKE', '%' . $searchString . '%')
            ->orWhere('email', 'ILIKE', '%' . $searchString . '%')
            ->orWhere('cheque_number', 'ILIKE', '%' . $searchString . '%')
            ->orWhere('first_name', 'ILIKE', '%' . $searchString . '%')
            ->orWhere('last_name', 'ILIKE', '%' . $searchString . '%')
            ->select('id', 'user_name', 'first_name', 'last_name', 'cheque_number', 'email')
            ->orderBy('admin_hierarchy_id')
            ->orderBy('user_name')->get();
        $data = ["users" => $users];
        return response()->json($data, Response::HTTP_OK);
    }

    public function activateUser($userId){

        $activation = UserServices::activateUser($userId);
        if($activation){
            $message = ["successMessage" => "SUCCESSFUL_ACTIVATE_USER"];
            track_activity(User::find($userId),UserServices::getUser(),action_name());
            return response()->json($message, 200);
        }
        else{
            $message = ["errorMessage" => "ERROR_ACTIVATE_USER"];
            return response()->json($message, 400);
        }
    }

    public function deActivateUser($userId){

        $activation = UserServices::deActivateUser($userId);
        if($activation){
            $message = ["successMessage" => "SUCCESSFUL_DE_ACTIVATE_USER"];
            track_activity(User::find($userId),UserServices::getUser(),action_name());
            return response()->json($message, 200);
        }
        else{
            $message = ["errorMessage" => "ERROR_DE_ACTIVATE_USER"];
            return response()->json($message, 400);
        }
    }

    public function index2() {
        $queryString = Input::get('searchQuery');
        $queryString = isset($queryString) ? Input::get('searchQuery') : null;
        $users = $this->allUsers(Input::get('perPage'), $queryString);
        return response()->json($users);
    }

    public function validateChequeNumber($userId,$chequesNumber){
        $existing=DB::table('users')->where('id','<>',$userId)->where('cheque_number',$chequesNumber)->count();
        if($existing >0){
            $message = ["exist" => true];
            return response()->json($message, 200);
        }
        else{
            $message = ["exist" => false];
            return response()->json($message, 200);
        }
    }

    public function validateEmail($userId,$email){
        $existing=DB::table('users')->where('id','<>',$userId)->where('email',$email)->count();
        if($existing >0){
            $message = ["exist" => true];
            return response()->json($message, 200);
        }
        else{
            $message = ["exist" => false];
            return response()->json($message, 200);
        }
    }

    public function validateUserName($userId,$userName){
        $existing = DB::table('users')->where('id','<>',$userId)->whereRaw("LOWER(user_name) = '" . strtolower($userName)."'")->count();
        if($existing >0){
            $message = ["exist" => true];
            return response()->json($message, 200);
        }
        else{
            $message = ["exist" => false];
            return response()->json($message, 200);
        }
    }

    public function store2(Request $request) {
        try {
            $user = null;

            DB::beginTransaction();

                $data = json_decode($request->getContent());
                if (!isset($data->id)) {
                    $credentials = [
                        'email' => $data->email,
                        'user_name' => $data->user_name,
                        'password' => '12345',
                        'first_name' => $data->first_name,
                        'last_name' => $data->last_name,
                        'title' => (isset($data->title)) ? $data->title : null,
                        'cheque_number' => $data->cheque_number,
                        'admin_hierarchy_id' => (isset($data->admin_hierarchy_id)) ? $data->admin_hierarchy_id : null,
                        'decision_level_id' => (isset($data->decision_level_id)) ? $data->decision_level_id : null,
                        'section_id' => (isset($data->section_id)) ? $data->section_id : null,
                        'is_superuser' => (isset($data->is_superuser)) ? $data->is_superuser : false,
                        'is_facility_user' => (isset($data->is_facility_user)) ? $data->is_facility_user : false
                    ];
                    $user = UserServices::registerAndActivate($credentials);
                    if($user->is_facility_user){
                        //Insert Home Facility
                        DB::table('user_facilities')->insert(['user_id'=>$user->id,'facility_id'=>$data->homeFacility->id,'is_home'=>'true']);
                    }
                    track_activity($user,UserServices::getUser(),'create_user');
                } else {
                    $existingUser = UserServices::findById($data->id);
                    $credentials = [
                        'email' => $data->email,
                        'user_name' => $data->user_name,
                        'first_name' => $data->first_name,
                        'last_name' => $data->last_name,
                        'title' => (isset($data->title)) ? $data->title : null,
                        'cheque_number' => $data->cheque_number,
                        'admin_hierarchy_id' => (isset($data->admin_hierarchy_id)) ? $data->admin_hierarchy_id : null,
                        'decision_level_id' => (isset($data->decision_level_id)) ? $data->decision_level_id : null,
                        'section_id' => (isset($data->section_id)) ? $data->section_id : null,
                        'is_superuser' => (isset($data->is_superuser)) ? $data->is_superuser : false,
                        'is_facility_user' => (isset($data->is_facility_user)) ? $data->is_facility_user : false,

                    ];
                    $user = UserServices::update($existingUser, $credentials);
                    if($existingUser->admin_hierarchy_id != $user->admin_hierarchy_id || $existingUser->section_id != $user->section_id || !$user->is_facility_user){
                        //Clear Facilities
                        DB::table('user_facilities')->where('user_id',$user->id)->delete();

                    }
                    if($user->is_facility_user){
                        //Insert Home Facility
                        DB::table('user_facilities')->insert(['user_id'=>$user->id,'facility_id'=>$data->homeFacility->id,'is_home'=>'true']);
                    }
                    track_activity($user,UserServices::getUser(),'updated_user');

                    $userRoles = RoleUser::where('user_id',$user->id)->get();
                    foreach ($userRoles as $userRole){
                        $userRole->delete();
                        track_activity(Role::find($userRole->role_id),UserServices::getUser(),'remove_user_role');
                    }
                    $user->removePermission('*')->save();

                }

                foreach ($data->roles as $r) {

                    RoleUser::create(array(
                        "user_id"=>$user->id,
                        "role_id"=>$r->id));
                    track_activity(Role::find($r->id),UserServices::getUser(),'add_user_role');
                }

                $user->save();

            DB::commit();
            $users = $this->allUsers(Input::get('perPage'), null);
            $message = ["successMessage" => "SUCCESSFUL_USER_UPDATED", "users" => $users, 'user'=>$user];

            return response()->json($message, 200);
        } catch (QueryException $e) {
            DB::rollback();
            $message = ["errorMessage" => "ERROR_CREATING_USER"];
            return response()->json($message, 400);
        }
    }

    public function getUserEvents($userId){

        return User::getPaginatedEvents($userId);
    }

    public function update2(Request $request) {

        $data = json_decode($request->getContent());
        $user = UserServices::findById(1);

        $credentials = [
            'email' => $data->email,
        ];

        $user = UserServices::update($user, $credentials);

        foreach ($data->roles as $r) {
            $role = RoleServices::findById($r);
            $role->users()->attach($user);
        }
        $permissions = [];
        foreach ($data->permissions as $p) {
            $permissions[$p] = true;
        }
        $user->permissions = $permissions;
        $user->save();
        $users = $this->allUsers(Input::get('perPage'), null);
        $message = ["successMessage" => "SUCCESSFUL_USER_UPDATED", "users" => $users];
        return response()->json($message, 200);
    }

    public function updatePassword(Request $request) {
        if ($request->oldPassword){

            $currentPwd = UserServices::getUser()->password;
            if (Hash::check($request->oldPassword,$currentPwd)){
                try {
                    $data = json_decode($request->getContent());
                    $user = UserServices::findById($data->id);
                    $newUserPassword = Hash::make($request->newPassword);
                    User::where('id',$data->id)
                     ->update([
                    'password' => $newUserPassword,
                    'password_reset' => true
                     ]);

                    $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
                    $users = $this->allUsers($admin_hierarchy_id, null);
                    $message = ["successMessage" => "SUCCESSFUL_PASSWORD_RESET", "users" => $users];
                    track_activity($user,UserServices::getUser(),action_name());
                    return response()->json($message, 200);
                }
                catch (\Exception $exception){
                    $message = ["errorMessage" => "ERROR_UPDATING_PASSWORD"];
                    return response()->json($message, 500);
                }
            }else{
                return response()->json(['errorMessage'=>'Incorrect old Password']);
            }

        }else if ($request->newUserPassword && $request->confirmPassword) {
            $currentPwd = UserServices::getUser()->password;
            if(!Hash::check($request->newUserPassword,$currentPwd)){
                try {
                $userId = UserServices::getUser()->id;
                $newUserPassword = Hash::make($request->newUserPassword);
                User::where('id',$userId)
                ->update([
                    'password' => $newUserPassword,
                    'password_reset' => true
                ]);
                $message = ["successMessage" => "SUCCESS!, PLEASE WAIT"];
                return response()->json($message, 200);

                } catch (\Throwable $th) {
                    $message = ["errorMessage" => "ERROR_UPDATING_PASSWORD"];
                    return response()->json($message, 500);
                }
            }else{
                ////use another password
                $message = ["errorMessage" => "Please enter a different password"];
                return response()->json($message, 500);
            }

        } else{ ///admin
            try {
                $data = json_decode($request->getContent());
                $user = UserServices::findById($data->id);
                $newUserPassword = Hash::make($data->newPassword);
                User::where('id',$data->id)
               ->update([
                'password' => $newUserPassword,
                'password_reset' => false
               ]);
                $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
                $users = $this->allUsers($admin_hierarchy_id, null);
                $message = ["successMessage" => "SUCCESSFUL_PASSWORD_RESET", "users" => $users];
                track_activity($user,UserServices::getUser(),action_name());
                return response()->json($message, 200);
            }
            catch (\Exception $exception){
                $message = ["errorMessage" => "ERROR_UPDATING_PASSWORD"];
                return response()->json($message, 500);
            }
        }
    }

    public function create() {
        if (UserServices::isLogin() && UserServices::getUser()->roles()->first()->slug == 'dra') {
            return view('auth.user.create',
                [
                    'js_scripts' => $this->form_scripts,
                    'title' => "User Registration|" . $this->app_initial,
                    'page_title' => 'User Registration',
                    'sections' => Section::all(),
                    'adminHierarchies' => AdminHierarchy::all(),
                    'roles' => Role::all(),
                    'breadcrumbs' => ['Setup' => url('/setup'), 'Users' => url('users'), 'Create' => url('#')]
                ]);
        } else {
            return redirect('/authentication/login');
        }
    }

    private function sendActivationEmail($user, $code) {
        Mail::send('emails.activation', ['user' => $user, 'code' => $code], function ($message) use ($user) {
            $message->to($user->email);
            $message->subject("Account Activation, Planrep System");
        });
    }

    private function sendPasswordResetEmail($user, $code) {
        Mail::send('emails.reset_password', ['user' => $user, 'code' => $code], function ($message) use ($user) {
            $message->to($user->email);
            $message->subject("Password Reset, PlanRep System");
        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%@]).*$/|confirmed',
            'password_confirmation' => 'min:6',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'role_id' => 'required|integer',
            'admin_hierarchy_id' => 'required|integer',
            'section_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return redirect('auth/user/create')->withErrors($validator)->withInput();
        } else {
            $user = UserServices::registerAndActivate($request->all());

            $user->section_id = $request->section_id;
            $user->admin_hierarchy_id = $request->admin_hierarchy_id;
            $user->save();

            $role_id = $request->role_id;
            $role = RoleServices::findById($role_id);
            $role->users()->attach($user);
            //$this->sendActivationEmail($user, $activation->code);
            return redirect('/auth/users')->with('success', 'User Created Successfully!');
        }
    }

    public function login() {
        return view('authentication.login',
            [
                'js_scripts' => $this->form_scripts,
                'title' => "Login|" . $this->app_initial,
                'page_title' => 'Login',
            ]);
    }

    public function forgot_password() {
        return view('auth.user.forgot_password',
            [
                'js_scripts' => $this->form_scripts,
                'title' => "Forgot Password|" . $this->app_initial,
                'page_title' => 'Forgot Password',
            ]);
    }

    public function new_password($email, $reset_code) {
        $user = User::byEmail($email);
        if (count($user) == 0) {
            abort(404);
        }
        if ($reminder = Reminder::exists($user)) {
            if ($reset_code == $reminder->code) {
                return view('auth.user.new_password',
                    [
                        'js_scripts' => $this->form_scripts,
                        'title' => "New Password|" . $this->app_initial,
                        'page_title' => 'New Password',
                    ]);
            } else {
                return redirect('/authentication/login')->with(['error' => 'User Does not exist!']);
            }
        } else {
            return redirect()->back()->with(['success' => 'Login to continue.']);
        }
    }

    public function create_new_password(Request $request, $email, $reset_code) {
        $this->validate($request, [
            'password' => 'confirmed|required|min:5|max:10',
            'password_confirmation' => 'required|min:5|max:10',
        ]);
        $user = User::byEmail($email);
        if (count($user) == 0) {
            abort(404);
        }
        if ($reminder = Reminder::exists($user)) {
            if ($reset_code == $reminder->code) {
                Reminder::complete($user, $reset_code, $request->password);
                return redirect('/authentication/login')->with(['success' => 'Password Changed Successfully!. Login to continue']);
            } else {
                return redirect('/authentication/login')->with(['error' => 'User Does not exist!']);
            }
        } else {
            return redirect()->back()->with(['success' => 'Login to continue.']);
        }
    }

    public function reset_password(Request $request) {
        try {
            $user = User::whereEmail($request->email)->first();
            if (count($user) == 0) {
                return redirect()->back()->with(['success' => 'Reset Code was sent to your email']);
            } else {
                $reminder = Reminder::exists($user) ?: Reminder::create($user);
                $this->sendPasswordResetEmail($user, $reminder->code);
                return redirect()->back()->with(['success' => 'Reset Code was sent to your email']);
            }
        } catch (Swift_TransportException $exception) {
            return redirect()->back()->with(['error' => "Connection Could not be established. Check your internet connection and try again"]);
        }
    }


    public function activate($email, $activationCode) {
        $user = User::whereEmail($email)->first();
        if (Activation::complete($user, $activationCode)) {
            return redirect('/authentication/login');
        } else {
            return redirect('/authentication/login');
        }
    }

    private function indexUsers() {
        $flatten = new Flatten();
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section_id = UserServices::getUser()->section_id;
        $users = null;
        if ($admin_hierarchy_id != null && $section_id != null) {
            $admin_hierarchies = AdminHierarchy::where('id', $admin_hierarchy_id)->get();
            $sections = Section::where('id', $section_id)->get();
            $Ids = $flatten->flattenAdminHierarchyWithChild($admin_hierarchies);
            $sectionIds = $flatten->flattenSectionWithChild($sections);

            $users = User::with('admin_hierarchy', 'section', 'roles')->
            whereIn('admin_hierarchy_id', $Ids)->whereIn('section_id', $sectionIds)->
            select('id', 'user_name', 'first_name', 'last_name', 'admin_hierarchy_id', 'section_id', 'cheque_number', 'email', 'decision_level_id', 'is_superuser')->
            orderBy('user_name')->get();

        } else {
            $users = User::with('roles')->where('id', UserServices::getUser()->id)->get();
        }
        return $users;
    }

    public function index() {
        $users = $this->indexUsers(10,null);
        $data = ["users" => $users];
        return response()->json($data,200);
    }
}
