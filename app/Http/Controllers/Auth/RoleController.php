<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Auth\Role;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @resource Role
 *
 * This Controller manage roles
*/
class RoleController extends Controller
{
    /**
     * index()
     *
     * this method return all roles
     *
     * @response {
          data : [{id:1, name:'admin'}]
     * }
     */
    public function index()
    {
        $roles=Role::all();

        return response()->json($roles);

    }

    /**
     * roleByUser()
     *
     * this method return roles by login user
     *
     */
    public function rolesByUser()
    {
        try{
            $user = UserServices::getUser();
            $userLevelAdmId=$user->admin_hierarchy_id;
            $userLevel=DB::table('admin_hierarchy_levels as al')->
                          join('admin_hierarchies as ah','al.id','ah.admin_hierarchy_level_id')->
                          where('ah.id',$userLevelAdmId)->select('al.*')->first();

            $roles=DB::table('roles as r')->join('admin_hierarchy_levels as al','al.id','r.admin_hierarchy_level_id')->
                                            where('al.hierarchy_position','>=',$userLevel->hierarchy_position)->
                                            where(function ($q) use ($user){
                                                if(!$user->is_superuser){
                                                    $q->where('is_super_admin',false);
                                                }
                                            })->
                                            select('r.*')->get();
            return response()->json($roles);
        }
        catch(QueryException $e){
            $data=['errorMessage'=>'ERROR_ON_GETTING_PERMISSION'];
            return response()->json($data,400);
        }

    }

    /**
     * store()
     *
     * this method create a new role
     *
     */
    public function store(Request $request)
    {
        $data=json_decode($request->getContent());
        $role = new Role();
        $role->name = $data->name;
        $role->is_super_admin = isset($data->is_super_admin)?$data->is_super_admin:false;
        $role->slug = $data->slug;
        $role->admin_hierarchy_level_id = $data->admin_hierarchy_level_id;
        $permissions="{";
        foreach ($data->permissions as $p){
            $permissions = $permissions. "\"".$p."\":true".",";
        }
        $role->permissions = rtrim($permissions,",")."}";
        $role->save();
        $message = ["successMessage" => "SUCCESSFUL_ROLE_CREATED", "roles" => Role::all()];
        return response()->json($message, 200);
    }


    /**
     * update()
     *
     * this method update an existing role.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $data=json_decode($request->getContent());
        $role = Role::find($data->id);
        $role->name=$data->name;
        $role->is_super_admin = isset($data->is_super_admin)?$data->is_super_admin:false;
        $role->slug=$data->slug;
        $role->admin_hierarchy_level_id=$data->admin_hierarchy_level_id;
        $permissions="{";
        foreach ($data->permissions as $p){
            $permissions = $permissions. "\"".$p."\":true".",";
        }
        $role->permissions = rtrim($permissions,",")."}";
        $role->save();
        $message = ["successMessage" => "SUCCESSFUL_ROLE_UPDATED", "roles" => Role::all()];
        return response()->json($message, 200);
    }

}
