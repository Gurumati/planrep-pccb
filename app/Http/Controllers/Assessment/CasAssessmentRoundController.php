<?php

namespace App\Http\Controllers\Assessment;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Assessment\CasAssessmentRound;
use Illuminate\Http\Request;

class CasAssessmentRoundController extends Controller
{
    public function index()
    {
        $all = CasAssessmentRound::all();
        return response()->json($all);
    }

    public function store(Request $request)
    {
        try{
            $data = json_decode($request->getContent());
            $obj = new CasAssessmentRound();
            $obj->name = $data->name;
            $obj->number = $data->number;
            $obj->save();
            $all = CasAssessmentRound::all();
            $message = ["successMessage" => "CREATE_CAS_ASSESSMENT_ROUND_SUCCESS", "casAssessmentRounds" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request)
    {
        try{
            $data = json_decode($request->getContent());
            $obj = CasAssessmentRound::find($data->id);
            $obj->name = $data->name;
            $obj->number = $data->number;
            $obj->save();
            $all = CasAssessmentRound::all();
            $message = ["successMessage" => "UPDATE_CAS_ASSESSMENT_ROUND_SUCCESS", "casAssessmentRounds" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id)
    {
        try{
            $obj = CasAssessmentRound::find($id);
            $obj->delete();
            $all = CasAssessmentRound::all();
            $message = ["successMessage" => "DELETE_CAS_ASSESSMENT_ROUND_SUCCESS", "casAssessmentRounds" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }
}
