<?php

namespace App\Http\Controllers\Assessment;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Assessment\CasAssessmentCategory;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class CasAssessmentCategoryController extends Controller
{
    public function index() {
        $all = CasAssessmentCategory::with('cas_plan','period_group')->orderBy('created_at','desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return CasAssessmentCategory::with('cas_plan','period_group')->orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new CasAssessmentCategory();
            $obj->name = $data->name;
            $obj->cas_plan_id = $data->cas_plan_id;
            $obj->period_group_id = $data->period_group_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "casAssessmentCategories" => $all];
            return response()->json($message, 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = CasAssessmentCategory::find($data->id);
            $obj->name = $data->name;
            $obj->cas_plan_id = $data->cas_plan_id;
            $obj->period_group_id = $data->period_group_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "casAssessmentCategories" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = CasAssessmentCategory::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "casAssessmentCategories" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = CasAssessmentCategory::with('cas_plan','period_group')->orderBy('created_at', 'desc')
            ->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        CasAssessmentCategory::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedCASAssessmentCategories" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        CasAssessmentCategory::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedCASAssessmentCategories" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = CasAssessmentCategory::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedCASAssessmentCategories" => $all];
        return response()->json($message, 200);
    }
    
}
