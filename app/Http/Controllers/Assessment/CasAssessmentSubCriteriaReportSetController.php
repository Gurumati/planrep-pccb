<?php

namespace App\Http\Controllers\Assessment;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Assessment\CasAssessmentSubCriteriaReportSet;
use Illuminate\Http\Request;

class CasAssessmentSubCriteriaReportSetController extends Controller
{
    public function index($criteria_option)
    {
        $all = CasAssessmentSubCriteriaReportSet::
               where('cas_assessment_sub_criteria_option_id',$criteria_option)
               ->with('casPlanContent')
               ->get();
        return response()->json($all);
    }

    public function store(Request $request)
    {
        try{
            $data = json_decode($request->getContent());
            $obj = new CasAssessmentSubCriteriaReportSet();
            $obj->cas_assessment_sub_criteria_option_id = $data->cas_assessment_sub_criteria_option_id;
            $data_array = array();
            foreach ($data->cas_plan_content_id as $key=>$value)
            {
                $new_data = array('cas_assessment_sub_criteria_option_id'=>$obj->cas_assessment_sub_criteria_option_id,
                                    'cas_plan_content_id'=>$value );
                array_push($data_array, $new_data);
            }
            $obj->insert($data_array);
            $all = CasAssessmentSubCriteriaReportSet::
                   where('cas_assessment_sub_criteria_option_id',$obj->cas_assessment_sub_criteria_option_id)
                   ->with('casPlanContent')
                   ->get();
            $message = ["successMessage" => "CREATE_CAS_ASSESSMENT_REPORT_SET_SUCCESS",
                        "casAssessmentReportSets" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request)
    {
        try{
            $data = json_decode($request->getContent());
            $obj = CasAssessmentSubCriteriaReportSet::find($data->id);
            $obj->cas_assessment_sub_criteria_option_id = $data->cas_assessment_sub_criteria_option_id;
            $obj->cas_plan_content_id = $data->cas_plan_content_id;
            $obj->save();
            $all = CasAssessmentSubCriteriaReportSet::
                   where('cas_assessment_sub_criteria_option_id',$obj->cas_assessment_sub_criteria_option_id)
                   ->with('casPlanContent')
                   ->get();
            $message = ["successMessage" => "UPDATE_CAS_ASSESSMENT_REPORT_SET_SUCCESS",
                        "casAssessmentReportSets" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id)
    {
        try{
            $obj = CasAssessmentSubCriteriaReportSet::find($id);
            $criteria_id = $obj->cas_assessment_sub_criteria_option_id;
            $obj->delete();
            $all = CasAssessmentSubCriteriaReportSet::
                   where('cas_assessment_sub_criteria_option_id',$criteria_id)
                   ->with('casPlanContent')
                   ->get();
            $message = ["successMessage" => "DELETE_CAS_ASSESSMENT_REPORT_SET_SUCCESS",
                        "casAssessmentReportSets" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

}
