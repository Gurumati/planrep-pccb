<?php

namespace App\Http\Controllers\Assessment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    public function index() {
        $title = "Assessment|" . $this->app_initial;
        return view('assessment.index', ['js_scripts' => $this->form_scripts, 'title' => $title]);
    }

    public function jsonBudgets() {
        return response()->json([
            'lga' => 'KALAMBO DC',
            'year' => '2018/2019'
        ]);
    }
}
