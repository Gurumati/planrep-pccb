<?php

namespace App\Http\Controllers\Assessment;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Assessment\CasAssessmentSubCriteriaPossibleScore;
use Illuminate\Http\Request;

class CasAssessmentSubCriteriaPossibleScoreController extends Controller
{
    public function index($criteria_option)
    {
        $all = CasAssessmentSubCriteriaPossibleScore::
               where('cas_assessment_sub_criteria_option_id',$criteria_option)
               ->get();
        return response()->json($all);
    }

    public function store(Request $request)
    {
        try{
            $data = json_decode($request->getContent());
            $obj = new CasAssessmentSubCriteriaPossibleScore();
            $obj->cas_assessment_sub_criteria_option_id = $data->cas_assessment_sub_criteria_option_id;
            $obj->description = $data->description;
            $obj->value = $data->value;
            $obj->save();
            $all = CasAssessmentSubCriteriaPossibleScore::
            where('cas_assessment_sub_criteria_option_id',$obj->cas_assessment_sub_criteria_option_id)
                ->get();
            $message = ["successMessage" => "CREATE_CAS_ASSESSMENT_OPTION_SUCCESS",
                        "casAssessmentCriteriaScores" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request)
    {
        try{
            $data = json_decode($request->getContent());
            $obj = CasAssessmentSubCriteriaPossibleScore::find($data->id);
            $obj->cas_assessment_sub_criteria_option_id = $data->cas_assessment_sub_criteria_option_id;
            $obj->description = $data->description;
            $obj->value = $data->value;
            $obj->save();
            $all = CasAssessmentSubCriteriaPossibleScore::
            where('cas_assessment_sub_criteria_option_id',$obj->cas_assessment_sub_criteria_option_id)
                ->get();
            $message = ["successMessage" => "UPDATE_CAS_ASSESSMENT_POSSIBLE_SCORE_SUCCESS",
                        "casAssessmentCriteriaScores" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id)
    {
        try{
            $obj = CasAssessmentSubCriteriaPossibleScore::find($id);
            $criteria_id = $obj->cas_assessment_sub_criteria_option_id;
            $obj->delete();
            $all = CasAssessmentSubCriteriaPossibleScore::
            where('cas_assessment_sub_criteria_option_id',$criteria_id)
                ->get();
            $message = ["successMessage" => "DELETE_CAS_ASSESSMENT_POSSIBLE_SCORES_SUCCESS",
                        "casAssessmentCriteriaScores" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

}
