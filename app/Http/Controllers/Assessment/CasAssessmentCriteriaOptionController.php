<?php

namespace App\Http\Controllers\Assessment;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Assessment\CasAssessmentCriteriaOption;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class CasAssessmentCriteriaOptionController extends Controller
{
    public function index() {
        $all = CasAssessmentCriteriaOption::with('cas_assessment_category_version','cas_assessment_category_version.financial_year','cas_assessment_category_version.reference_document','cas_assessment_category_version.cas_assessment_state')->orderBy('created_at','desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage, $category_version_id) {
        return CasAssessmentCriteriaOption::with('cas_assessment_category_version','cas_assessment_category_version.financial_year','cas_assessment_category_version.reference_document','cas_assessment_category_version.cas_assessment_state')
                                            ->where('cas_assessment_category_version_id',$category_version_id)
                                            ->orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $category_version_id = $request->category_version_id;
        $all = $this->getAllPaginated($request->perPage, $category_version_id);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        $obj = new CasAssessmentCriteriaOption();
        $obj->number = $data->number;
        $obj->name = $data->name;
        $obj->how_to_assess = 'blank';
        $obj->how_to_score = 'blank';
        $obj->cas_assessment_category_version_id = $data->cas_assessment_category_version_id;
        $obj->save();
        $all = $this->getAllPaginated($request->perPage,$obj->cas_assessment_category_version_id);
        $message = ["successMessage" => "CREATE_SUCCESS", "criteriaOptions" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = CasAssessmentCriteriaOption::find($data->id);
            $obj->number = $data->number;
            $obj->name = $data->name;
//            $obj->how_to_assess = $data->how_to_assess;
//            $obj->how_to_score = $data->how_to_score;
            $obj->cas_assessment_category_version_id = $data->cas_assessment_category_version_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage, $obj->cas_assessment_category_version_id);
            $message = ["successMessage" => "UPDATE_SUCCESS", "criteriaOptions" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = CasAssessmentCriteriaOption::find($id);
        $cas_assessment_category_version_id = $obj->cas_assessment_category_version_id;
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'), $cas_assessment_category_version_id);
        $message = ["successMessage" => "DELETE_SUCCESS", "criteriaOptions" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = CasAssessmentCriteriaOption::with('cas_assessment_category_version','cas_assessment_category_version.financial_year','cas_assessment_category_version.reference_document','cas_assessment_category_version.cas_assessment_state')->orderBy('created_at', 'desc')
            ->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        CasAssessmentCriteriaOption::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedCriteriaOptions" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        CasAssessmentCriteriaOption::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedCriteriaOptions" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = CasAssessmentCriteriaOption::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedCriteriaOptions" => $all];
        return response()->json($message, 200);
    }

    public function CriteriaByCategory($category_id) {
        $all = CasAssessmentCriteriaOption::where('cas_assessment_category_version_id',$category_id)
               ->orderBy('number','asc')->get();
        return response()->json($all);
    }
}
