<?php

namespace App\Http\Controllers\assessment;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Setup\CalendarController;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Jobs\sendNotifications;
use App\Models\Assessment\CasAssessmentCategoryVersion;
use App\Models\Assessment\CasAssessmentCriteriaOption;
use App\Models\Assessment\CasAssessmentResult;
use App\Models\Assessment\CasAssessmentResultDetail;
use App\Models\Assessment\CasAssessmentResultDetailReply;
use App\Models\Assessment\CasAssessmentResultReply;
use App\Models\Assessment\CasAssessmentRound;
use App\Models\Assessment\CasAssessmentSubCriteriaOption;
use App\Models\Assessment\CasAssessmentSubCriteriaPossibleScore;
use App\Models\Assessment\CasAssessmentSubCriteriaReportSet;
use App\Models\Auth\User;
use App\Models\Planning\Mtef;
use App\Models\Planning\ReferenceDocument;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\AdminHierarchyLevel;
use App\Models\Setup\AdminHierarchyParent;
use App\Models\Setup\Period;
use App\Models\Setup\ReferenceDocumentType;
use App\Models\Setup\Section;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\Calendar;
use App\Models\Setup\CalendarEvent;
use App\Models\Setup\FinancialYear;
use Illuminate\Support\Facades\Log;
use App\Models\Setup\AssessorAssignment;


class CasAssessmentResultDetailController extends Controller
{
    public function index()
    {
        $all = CasAssessmentResult::with('cas_assessment_result_details')->orderBy('code', 'desc')->get();
        return response()->json($all);
    }

    public function council_results()
    {
        $all = CasAssessmentResult::with('cas_assessment_result_details')->orderBy('code', 'desc')->get();
        return response()->json($all);
    }

    public function getStrategicPlan($adminHierarchId,$financialYearId)
    {
        $today = date(FinancialYear::where('id',$financialYearId)->select('start_date')->first()->start_date). ' 00:00:00';
        $referenceDocuments = ReferenceDocument::with('referenceDocumentType')
        ->with('sFinancialYear')
        ->with('eFinancialYear')
        ->whereHas('eFinancialYear', function ($q) use ($today) {
            $q->whereDate('end_date', '>=', $today);
        })
        ->where('admin_hierarchy_id',$adminHierarchId)
        ->where('is_active', true)
        ->orderBy('id','desc')
        ->get();
        return response()->json($referenceDocuments);
    }

    public function sub_criteria_report_set($sub_criteria)
    {
        $all = CasAssessmentSubCriteriaReportSet::with('casPlanContent')
            ->with('casPlanContent.casPlanTable')
            ->with('casPlanContent.casPlanTable')
            ->with('casPlanContent.casPlanTable.casPlanTableDetails')
            ->where('cas_assessment_sub_criteria_option_id', $sub_criteria)
            ->get();
        return response()->json($all);
    }

    public static function getIsCalendar()
    {
        $event_true = false;
        $event_number = ConfigurationSetting::getValueByKey('ASSESSMENT_EVENT');
        $calendarEvent = CalendarEvent::where('number',$event_number)->first();
        $calendarEventId = isset($calendarEvent->id) ? $calendarEvent: null;

        $user_id = UserServices::getUser()->id;
        $user = User::find($user_id);
        $admin_hierarchy = AdminHierarchy::find($user->admin_hierarchy_id);
        $admin_hierarchy_level = AdminHierarchyLevel::find($admin_hierarchy->admin_hierarchy_level_id);
        $hierarchy_position = $admin_hierarchy_level->hierarchy_position;

        $calendar_count = Calendar::where('calendar_event_id',$calendarEventId)
            ->where('hierarchy_position',$hierarchy_position)
            ->where('start_date','>=',date('Y-m-d H:i:s'))
            ->where('end_date','<=',date('Y-m-d H:i:s'))
            ->count();
        if($calendar_count > 0)
            $event_true = true;

        return $event_true;
    }

    public function my_assessment_results(Request $request)
    {
        $category_version_id = $request->category_version_id;
        $assessment_group    = $request->assessment_group;
        $financialYear       = json_decode($request->financial_year, true);
        $fy_id = $financialYear['id'];
        $user_id = UserServices::getUser()->id;
        $is_calendar = $this->getIsCalendar();
        $region_name = "";
        $councils = [];

        $user_admin_hierarchy = UserServices::getUser()->admin_hierarchy_id;
        $adminHierarchy = AdminHierarchy::where('id', $user_admin_hierarchy)->first();
        $user_level = AdminHierarchyLevel::where('id', $adminHierarchy->admin_hierarchy_level_id)
            ->first();

        if ($user_level->hierarchy_position == 3) {
            $region = AdminHierarchy::where('id', $adminHierarchy->parent_id)->first();
            $region_name = $region->name;
            $councils[0] = $adminHierarchy->id;
        }

        if ($user_level->hierarchy_position == 2) {
            $region_name = $adminHierarchy->name;

            if ($assessment_group == 2) {
                $councilsObject = AdminHierarchy::where('parent_id', $user_admin_hierarchy)->get();
                foreach ($councilsObject as $councilObject) {
                    $councils[] = $councilObject->id;
                }
            }
        }

        $periods = Period::where('financial_year_id', $fy_id)->get();
        $periodsArray = [];
        foreach ($periods as $period) {
            $periodsArray[] = $period->id;
        }

        if ($assessment_group == 1) {
            $all = CasAssessmentResult::with(['mtef', 'round', 'period', 'mtef.admin_hierarchy_parent', 'period.financial_year', 'admin_hierarchy_level'])
                    ->whereIn('period_id', $periodsArray)
                    ->where('cas_assessment_category_version_id', $category_version_id)
                    ->where('user_id', $user_id)
                    ->get();
        } else {
            $adminLevels = AdminHierarchyLevel::where('hierarchy_position', $user_level->hierarchy_position)
                ->get();

            $levelIds = [];
            foreach ($adminLevels as $adminLevel) {
                $levelIds[] = $adminLevel->id;
            }
            if ($user_level->hierarchy_position == 1) {
                $all = CasAssessmentResult::with(['mtef', 'round', 'period', 'mtef.admin_hierarchy_parent', 'period.financial_year', 'admin_hierarchy_level'])
                    ->whereIn('period_id', $periodsArray)
                    ->where('cas_assessment_category_version_id', $category_version_id)
                    ->where('is_confirmed', true)
                    ->whereNotIn('admin_hierarchy_level_id', $levelIds)
                    ->get();
            } else {
                if ($user_level->hierarchy_position == 2) {

                    $mtefs = Mtef::whereIn('admin_hierarchy_id', $councils)
                        ->where('financial_year_id', $fy_id)
                        ->where('plan_type', 'CURRENT')
                        ->get();

                    $mtefArray = [];
                    foreach ($mtefs as $mtef) {
                        $mtefArray[] = $mtef->id;
                    }
                    $all = CasAssessmentResult::with(['mtef', 'round', 'period', 'mtef.admin_hierarchy_parent', 'period.financial_year', 'admin_hierarchy_level'])
                            ->whereIn('period_id', $periodsArray)
                            ->where('cas_assessment_category_version_id', $category_version_id)
                            //->where('is_confirmed', true)
                            ->whereIn('mtef_id', $mtefArray)
                            ->whereNotIn('admin_hierarchy_level_id', $levelIds)
                            ->get();
                } else {
                    $mtef = Mtef::where('admin_hierarchy_id', $adminHierarchy->id)
                            ->where('financial_year_id', $fy_id)
                            ->where('plan_type', 'CURRENT')
                            ->first();

                    $all = CasAssessmentResult::with(['mtef', 'round', 'period', 'mtef.admin_hierarchy_parent', 'period.financial_year', 'admin_hierarchy_level'])
                            ->whereIn('period_id', $periodsArray)
                            ->where('cas_assessment_category_version_id', $category_version_id)
                            //->where('is_confirmed', true)
                            ->where('mtef_id', $mtef->id)
                            ->whereNotIn('admin_hierarchy_level_id', $levelIds)
                            ->get();
                }
            }
        }


        $user = UserServices::findById($user_id);
        $can_assess = false;
        if ($user->hasAccess(['budgeting.manage.assessment.write'])) {
            if ($is_calendar)
                $can_assess = true;
        }

        $message = ["can_assess" => $can_assess, "assessment_results" => $all, "region_name" => $region_name];
        return response()->json($message, 200);
    }

    public function category_assessment_results()
    {
        $periods = [];
        $all = CasAssessmentResult::with(['admin_hierarchy', 'round', 'period'])
            ->whereIn('period_id', $periods)
            ->where('admin_')
            ->orderBy('code', 'desc')->get();
        return response()->json($all);
    }

    public function council_round_results($category_version_id, $period_id, $round_id, $admin_hierarchy_id)
    {

        $period = Period::find($period_id);
        $fy_id = $period->financial_year_id;
        $user_id = UserServices::getUser()->id;


        if($admin_hierarchy_id == 0)
        {
            $count = CasAssessmentResult::where('id', $round_id)
                ->count();
        }
        else
        {
            $admin_hierarchy = AdminHierarchy::where('id', $admin_hierarchy_id)->first();
            $user_admin_hierarchy = UserServices::getUser()->admin_hierarchy_id;
            $userAdminHierarchy = AdminHierarchy::where('id', $user_admin_hierarchy)->first();
            $user_level = AdminHierarchyLevel::where('id', $userAdminHierarchy->admin_hierarchy_level_id)
                ->first();

            $mtef = Mtef::where('admin_hierarchy_id', $admin_hierarchy_id)
                ->where('financial_year_id', $fy_id)
                ->where('plan_type', 'CURRENT')
                ->first();


            $count = CasAssessmentResult::where('mtef_id', $mtef->id)
                ->where('cas_assessment_round_id', $round_id)
                ->where('cas_assessment_category_version_id', $category_version_id)
                ->where('period_id', $period->id)
                ->where('admin_hierarchy_level_id',$user_level->id)
                ->count();
        }

        if ($count < 1) {
            $obj = new CasAssessmentResult();
            $obj->period_id = $period_id;
            $obj->mtef_id = $mtef->id;
            $obj->cas_assessment_round_id = $round_id;
            $obj->highest_available_score = $this->getHighestScore($category_version_id);
            $obj->cas_assessment_category_version_id = $category_version_id;
            $obj->cas_assessment_state_id = 3;
            $obj->admin_hierarchy_level_id = $user_level->id;
            $obj->is_confirmed = false;
            $obj->is_selected = false;
            $obj->score_value = 0;
            $obj->user_id = $user_id;
            $obj->created_by = $user_id;
            $obj->save();
        }

        if($admin_hierarchy_id == 0)
        {
            $all = CasAssessmentResult::where('id',$round_id )
                ->with(['user'])
                ->with('user.admin_hierarchy')
                ->with('admin_hierarchy_level')
                ->first();

        }
        else {

            $all = CasAssessmentResult::where('mtef_id',$mtef->id )
                ->where('cas_assessment_round_id', $round_id)
                ->where('cas_assessment_category_version_id', $category_version_id)
                ->where('period_id', $period_id)
                ->where('admin_hierarchy_level_id', $user_level->id)
                ->with('user')
                ->with('user.admin_hierarchy')
                ->with('admin_hierarchy_level')
                ->first();
        }

        return response()->json($all);
    }

    public function update_result(Request $request)
    {
        $data = json_decode($request->getContent());
        try {
            $obj = CasAssessmentResult::find($data->id);

            $user_origin = User::where('id', $obj->user_id)
                ->with("admin_hierarchy")->first();

            $user_new = User::where('id', $data->user_id)
                ->with("admin_hierarchy")->first();

            $hp_origin = $user_origin->admin_hierarchy->admin_hierarchy_level->hierarchy_position;
            $hp_new = $user_new->admin_hierarchy->admin_hierarchy_level->hierarchy_position;

            if($hp_new < $hp_origin)
            {
                $obj->is_confirmed = $data->is_confirmed;
            }
            else {

                $obj->remarks = $data->remarks;
                $obj->score_value = $data->score_value;
                $obj->is_confirmed = $data->is_confirmed;
                $obj->is_selected = $data->is_selected;
                $obj->user_id = $data->user_id;
            }
            $obj->save();
            $all = CasAssessmentResult::where('id', $data->id)
                ->first();

            $users = User::where('user_name','mosham')->get();
             $user = User::find(1145);
//            $user->notify(new ComprehensivePlan($obj));
            $user_admin_hierarchy = UserServices::getUser()->admin_hierarchy_id;

            $adminHierarchy = AdminHierarchy::find($user_admin_hierarchy);
            $user_level = AdminHierarchyLevel::find($adminHierarchy->admin_hierarchy_level_id);

            $fullName =  UserServices::getUser()->first_name." ".UserServices::getUser()->last_name;
            $adminHierarchy_name = $adminHierarchy->name;
            $adminHierarchy_level = $user_level->name;
            $message_text = $fullName." from ".$adminHierarchy_name."
            ".$adminHierarchy_level." have submitted assessment results";

            $url = url('/assessment#!/cas-assessment-results/1/0');

            $job = new sendNotifications($users,$obj->id,$url,$message_text,"fa fa-book");
            $this::dispatch($job);

            $message = ["successMessage" => "UPDATE_SUCCESS", "casAssessmentResult" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function insert_result_details(Request $request)
    {
        $data = json_decode($request->getContent());
        try {
            $obj = CasAssessmentResult::find($data->id);
            $obj->period_id = $data->period_id;
            $obj->mtef_id = $data->mtef_id;
            $obj->cas_assessment_round_id = $data->round_id;
            $obj->cas_assessment_category_version_id = $data->cas_assessment_category_version_id;
            $obj->cas_assessment_state_id = $data->cas_assessment_state_id;
            $obj->admin_hierarchy_level_id = $data->admin_hierarchy_level_id;
            $obj->is_confirmed = $data->is_confirmed;
            // $obj->score_value = 0;
            $obj->user_id = $data->user_id;
            $obj->save();
            $all = CasAssessmentResult::where('id', $data->id)
                ->first();
            $message = ["successMessage" => "UPDATE_SUCCESS", "casAssessmentResult" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update_result_details(Request $request)
    {
        $data = json_decode($request->getContent());
        $score = CasAssessmentSubCriteriaPossibleScore::find($data->cas_assessment_sub_criteria_possible_score_id);
        $score_value = $score->value;
        try {
            $obj = CasAssessmentResultDetail::find($data->id);
            $obj->remarks = $data->remarks;
            $obj->cas_assessment_sub_criteria_possible_score_id = $data->cas_assessment_sub_criteria_possible_score_id;
            $obj->sub_criteria_score = $score_value;
            $obj->free_score_value = 0;
            $obj->save();
            $all = CasAssessmentResultDetail::where('id', $data->id)
                ->first();
            $message = ["successMessage" => "UPDATE_SUCCESS", "casAssessmentResult" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->getMessage();
            Log::error($error_code);
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function criteria_round_results($criteria_id, $result_id)
    {
        $all = CasAssessmentResult::with('cas_assessment_result_details')
            ->orderBy('code', 'desc')->get();
        return response()->json($all);
    }

    public function council_results_details($result_id)
    {
        $all = CasAssessmentResult::with('cas_assessment_result_details')->orderBy('code', 'desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage)
    {
        return CasAssessmentResultDetail::with('cas_assessment_result')->orderBy('created_at', 'desc')->paginate($perPage);
    }

    public function getHighestScore($category_version_id)
    {
        $scoreValue = 0;

        $criteria = CasAssessmentCriteriaOption::where('cas_assessment_category_version_id', $category_version_id)->get();
        foreach ($criteria as $criterion) {

            $subCriteria = CasAssessmentSubCriteriaOption::where('cas_assessment_criteria_option_id', $criterion->id)->get();
            foreach ($subCriteria as $subCriterion) {

                $scoreValue += $subCriterion->score_value;
                // $scoreValue += 2;
            }
        }
        return $scoreValue;
    }

    public function filteredCriteria($category_version_id)
    {
        return CasAssessmentCriteriaOption::where('cas_assessment_category_version_id', $category_version_id)->orderBy('created_at', 'desc')->get();

    }

    public function filteredSubCriteria($criteria_option_id, $result_id)
    {
        $subCriteriaIds = [];
        $subCriteria = CasAssessmentSubCriteriaOption::where('cas_assessment_criteria_option_id', $criteria_option_id)
            ->orderBy('created_at', 'desc')->get();
        foreach ($subCriteria as $subCriterion) {
            $subCriteriaIds[] = $subCriterion->id;

            $count = CasAssessmentResultDetail::with('cas_assessment_result')
                ->with('cas_assessment_sub_criteria')
                ->where('cas_assessment_result_id', $result_id)
                ->where('cas_assessment_sub_criteria_option_id', $subCriterion->id)
                ->orderBy('created_at', 'desc')
                ->count();

            if ($count < 1) {
                $obj = new CasAssessmentResultDetail();
                $obj->cas_assessment_result_id = $result_id;
                $obj->cas_assessment_sub_criteria_option_id = $subCriterion->id;
                $obj->remarks = "";
                $obj->sub_criteria_score = 0;
                $obj->free_score_value = 0;
                $obj->save();
            }

        }

        return CasAssessmentResultDetail::with('cas_assessment_sub_criteria')
            ->where('cas_assessment_result_id', $result_id)
            ->whereIn('cas_assessment_sub_criteria_option_id', $subCriteriaIds)
            ->orderBy('created_at', 'desc')
            ->get();

        // return CasAssessmentSubCriteriaOption::where('cas_assessment_criteria_option_id', $criteria_option_id)->orderBy('created_at','desc')->get();

    }

    public function filteredSubCriteriaScores($sub_criteria_option_id)
    {
        return CasAssessmentSubCriteriaPossibleScore::where('cas_assessment_sub_criteria_option_id', $sub_criteria_option_id)->orderBy('created_at', 'desc')->get();
    }

    public function filteredCategoryVersions(Request $request)
    {
        $id = $request->id;
        $financial_year_id = $request->financial_year;
        $financialYear = FinancialYear::find($financial_year_id);
        $user_id = UserServices::getUser()->id;
        $is_calendar = $this->getIsCalendar();
        $docType = ReferenceDocumentType::where('name', 'AssessmentGuidelines')->first();

        $refDocument = DB::table('reference_documents as r')
                     ->join('financial_years as eF', 'eF.id', '=', 'r.end_financial_year')
                     ->where('r.reference_document_type_id', '=', $docType->id)
                     ->where('eF.end_date', '>=', $financialYear['end_date'])->select('r.*')->first();

        $all = CasAssessmentCategoryVersion::with('cas_assessment_category', 'cas_assessment_state',
                                           'cas_assessment_category.cas_plan', 'financial_year',
                                           'reference_document')
                                           ->orderBy('created_at', 'desc')
                                           ->where('reference_document_id', $refDocument->id)
                                           ->where('financial_year_id', $financialYear['id'])
                                           ->where('cas_assessment_category_id', $id)
                                           ->get();
        $user = UserServices::findById($user_id);
        $can_assess = false;
        if ($user->hasAccess(['budgeting.manage.assessment.write'])) {
            if ($is_calendar)
                $can_assess = true;
        }

        $user_profile = User::where('id', $user_id)
                         ->with('admin_hierarchy')->first();

        $message = ["can_assess" => $can_assess, "category_versions" => $all,"user_profile" =>$user_profile, "default" => $id];
        return response()->json($message, 200);
    }

    public function CategoryVersions(Request $request){

      if(isset($request->financial_year_id)){
         $financial_year_id = $request->financial_year_id;
      }else {
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $financial_year_id = $financialYear->id;
//        $financial_year_id = ConfigurationSetting::getValueByKey('AUTO_FINANCIAL_YEAR_TO_REPORT');
      }
      $financialYear = FinancialYear::find($financial_year_id);

      $user_id = UserServices::getUser()->id;
     // $is_calendar = $this->getIsCalendar();
      $user = UserServices::findById($user_id);

      //get the latest category versions
      $all = CasAssessmentCategoryVersion::with('cas_assessment_category', 'cas_assessment_state',
                                           'cas_assessment_category.cas_plan', 'financial_year',
                                           'reference_document')
                                           ->orderBy('created_at', 'desc')
                                           ->where('financial_year_id', $financial_year_id)
                                           ->get();

      $can_assess = false;
      if ($user->hasAccess(['budgeting.manage.assessment.write'])) {
            //if ($is_calendar)
                $can_assess = true;
      }

      $user_profile = User::where('id', $user_id)
                         ->with('admin_hierarchy')->first();

      $message = ["can_assess" => $can_assess, "category_versions" => $all,"user_profile" =>$user_profile, 'financial_year' => $financialYear];
      return response()->json($message, 200);
    }

    public function rounds($category_version_id, $council_id)
    {
        $all = CasAssessmentRound::get();
        return response()->json($all);
    }

    public function Assignedrounds($category_version_id, $council_id)
    {
        
        $user_id= UserServices::getUser()->id;

        $assignedAdminHierarchies =AssessorAssignment::where('user_id', $user_id)
        ->where('cas_assessment_category_version_id',$category_version_id)
        ->pluck('admin_hierarchy_id')->toArray();

        if(sizeof($assignedAdminHierarchies) > 0){
            $selectedRound = CasAssessmentRound::
            join('assessor_assignments as aa','aa.cas_assessment_round_id','cas_assessment_rounds.id')
            ->where('aa.admin_hierarchy_id',$council_id)
            ->where('aa.cas_assessment_category_version_id',$category_version_id)
            ->select('cas_assessment_rounds.*')
            ->get();
        }else{
            $selectedRound = CasAssessmentRound::get();
        }
        return response()->json($selectedRound);
    }

    public function periods($category_version_id, $council_id, $round_id, $financial_year_id)
    {
        $all = Period::with('financial_year')
            ->where('financial_year_id', $financial_year_id)
            ->get();
        return response()->json($all);
    }

    public function AssignedPeriods($category_version_id, $council_id, $round_id, $financial_year_id)
    {
        $user_id= UserServices::getUser()->id;

        $assignedPeriods = Period::with('financial_year')->where('financial_year_id', $financial_year_id);

        $assignedAdminHierarchies =AssessorAssignment::where('user_id', $user_id)
        ->where('cas_assessment_category_version_id',$category_version_id)
        ->pluck('admin_hierarchy_id')->toArray();
        if(sizeof($assignedAdminHierarchies) > 0){
            $assignedPeriods->join('assessor_assignments as aa','periods.id','aa.period_id')
            ->where('cas_assessment_round_id', $round_id)
            ->where('admin_hierarchy_id', $council_id)
            ->where('cas_assessment_category_version_id', $category_version_id)
            ->select('periods.*')->distinct();
        }
        return response()->json($assignedPeriods->get());
    }

    public function councils($category_version_id)
    {

        $adminLevels = AdminHierarchyLevel::where('hierarchy_position', 4)
            ->get();
        $user_admin_hierarchy = UserServices::getUser()->admin_hierarchy_id;
        $user_id= UserServices::getUser()->id;
        $adminHierarchy = AdminHierarchy::where('id', $user_admin_hierarchy)->first();
        $user_level = AdminHierarchyLevel::where('id', $adminHierarchy->admin_hierarchy_level_id)
            ->first();


        $levelIds = [];
        foreach ($adminLevels as $adminLevel) {
            $levelIds[] = $adminLevel->id;
        }
        $level = $user_level->hierarchy_position;
        $adminHierarchy = AdminHierarchyParent::with('admin_hierarchy_level');
        if ($level == 4) {
            $adminHierarchy->whereIn('admin_hierarchy_level_id', $levelIds)
                ->where('id', $user_admin_hierarchy);
        }
        if ($level == 1) {
            $adminHierarchy->whereIn('admin_hierarchy_level_id', $levelIds);
        }
        if ($level == 3) {
            $adminHierarchy->whereIn('admin_hierarchy_level_id', $levelIds)
                ->where('parent_id', $user_admin_hierarchy);
        }

        $assignedAdminHierarchies =AssessorAssignment::where('user_id', $user_id)
            ->where('cas_assessment_category_version_id',$category_version_id)
            ->pluck('admin_hierarchy_id')->toArray();
        if(sizeof($assignedAdminHierarchies) > 0){
            $adminHierarchy->whereIn('id',$assignedAdminHierarchies);
        }

        return response()->json($adminHierarchy->get());
    }

    public function get_reply($id,$category,$category_id)
    {
        $user_id = UserServices::getUser()->id;
        $user_admin_hierarchy = UserServices::getUser()->admin_hierarchy_id;
        $adminHierarchy = AdminHierarchy::where('id', $user_admin_hierarchy)->first();
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $reply_id = 0;
        $reply = "";

        if($category == 2 ) {

            $reply =  CasAssessmentResultReply::where('cas_assessment_result_id', $category_id)
                ->with('cas_assessment_result')
                ->with('user')
                ->with('user.admin_hierarchy')
                ->get();
        }

        if($category == 3 ) {

            $reply =  CasAssessmentResultDetailReply::where('cas_assessment_result_detail_id', $category_id)
               ->with('cas_assessment_result_detail')
                ->with('user')
                ->with('user.admin_hierarchy')
                ->get();
        }

        if($category == 4 || $category == 5) {
            $count = CasAssessmentResultReply::where('cas_assessment_result_id', $category_id)
                ->where('updated_by', $user_id)
                ->count();

            if($count == 0)
            {
                DB::transaction(function () use ($category_id, $user_id, $adminHierarchy,$reply_id) {

                    $id = DB::table('cas_assessment_result_replies')->insertGetId(
                        [
                            'cas_assessment_result_id' => $category_id,
                            'comment' => "",
                            'created_by' => $user_id,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_by' => $user_id,
                            'updated_at' => date('Y-m-d H:i:s'),
                            'admin_hierarchy_level_id' => $adminHierarchy->admin_hierarchy_level_id,

                        ]);

               $resultDetails =  CasAssessmentResultDetail::
                              where('cas_assessment_result_id', $category_id)->get();

                foreach ($resultDetails as $resultDetail)
                {
                    $obj = new CasAssessmentResultDetailReply();
                    $obj->cas_assessment_result_detail_id = $resultDetail->id;
                    $obj->cas_assessment_result_reply_id = $id;
                    $obj->comment = "";
                    $obj->created_by = $user_id;
                    $obj->created_at = date('Y-m-d H:i:s');
                    $obj->updated_by = $user_id;
                    $obj->updated_at = date('Y-m-d H:i:s');
                    $obj->is_addressed = false;
                    $obj->save();

                }
                    $reply_id = $id;
                });

            }



            if($category == 4)
                $reply =  CasAssessmentResultReply::where('cas_assessment_result_id', $category_id)
                    ->with('cas_assessment_result')
                    ->with('user')
                    ->with('user.admin_hierarchy')
                    ->get();
            if($category == 5)
                $reply =  CasAssessmentResultDetailReply::where('cas_assessment_result_detail_id', $category_id)
                    ->with('cas_assessment_result_detail')
                    ->with('user')
                    ->with('user.admin_hierarchy')
                    ->get();

        }

        $message = ["user_id" => $user_id, "reply" => $reply, "reply_id"=> $reply_id];
        return response()->json($message, 200);
    }

    public function update_reply($id,$comment,$category,$category_id,$status)
    {
        $user_id = UserServices::getUser()->id;
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $reply_id = 0;

        if($category == 2 ) {

            $obj = CasAssessmentResultReply::find($id);
            $obj->comment = $comment;
            $obj->created_by = $user_id;
            $obj->created_at = date('Y-m-d H:i:s');
            $obj->updated_by = $user_id;
            $obj->updated_at = date('Y-m-d H:i:s');
            $obj->save();

            $reply =  CasAssessmentResultReply::where('cas_assessment_result_id', $category_id)
                ->with('cas_assessment_result')
                ->with('user')
                ->get();
        }

        if($category == 3 ) {

            $obj = CasAssessmentResultDetailReply::find($id);
            $obj->comment = $comment;
            $obj->created_by = $user_id;
            $obj->created_at = date('Y-m-d H:i:s');
            $obj->updated_by = $user_id;
            $obj->updated_at = date('Y-m-d H:i:s');
            $obj->is_addressed = $status;
            $obj->save();

            $reply =  CasAssessmentResultDetailReply::where('cas_assessment_result_detail_id', $category_id)
                ->with('cas_assessment_result_detail')
                ->with('user')
                ->get();
        }

        $message = ["user_id" => $user_id, "reply" => $reply, "reply_id"=> $reply_id];
        return response()->json($message, 200);

    }


}
