<?php

namespace App\Http\Controllers\Assessment;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Assessment\CasAssessmentSubCriteriaOption;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class CasAssessmentSubCriteriaOptionController extends Controller {
    public function index() {
        $all = CasAssessmentSubCriteriaOption::orderBy('created_at','desc')->get();
        return response()->json($all);
    }

    public function subCriteriaByCriteria($criteria_id) {
        $all = CasAssessmentSubCriteriaOption::
        where('cas_assessment_criteria_option_id',$criteria_id)->orderBy('created_at','desc')->get();
        return response()->json($all);
    }

    public function fetchAll() {
        $all = CasAssessmentSubCriteriaOption::with('cas_assessment_criteria_option')->orderBy('created_at','desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage, $criteria_option_id) {
        return CasAssessmentSubCriteriaOption::with('cas_assessment_criteria_option')
            ->where('cas_assessment_criteria_option_id',$criteria_option_id)
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

    public function paginated(Request $request) {
        $criteria_option_id = $request->CriteriaOption;
        $all = $this->getAllPaginated($request->perPage, $criteria_option_id);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new CasAssessmentSubCriteriaOption();
            $obj->serial_number = $data->serial_number;
            $obj->name = $data->how_to_assess;
            $obj->how_to_assess = $data->how_to_assess;
            $obj->how_to_score = 'blank';
            $obj->is_free_score = (isset($data->is_free_score)) ? true : false;
            $obj->score_value = $data->score_value;
            $obj->cas_assessment_criteria_option_id = $data->cas_assessment_criteria_option_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage, $obj->cas_assessment_criteria_option_id);
            $message = ["successMessage" => "CREATE_SUCCESS", "subCriteriaOptions" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::error($exception);
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try {
            $obj = CasAssessmentSubCriteriaOption::find($data->id);
            $obj->serial_number = $data->serial_number;
            $obj->name = $data->how_to_assess;
            $obj->how_to_assess = $data->how_to_assess;
//            $obj->how_to_score = 'blank';
            $obj->is_free_score = $data->is_free_score;
            $obj->score_value = $data->score_value;
//            $obj->cas_assessment_criteria_option_id = $data->cas_assessment_criteria_option_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage, $obj->cas_assessment_criteria_option_id);
            $message = ["successMessage" => "UPDATE_SUCCESS", "subCriteriaOptions" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = CasAssessmentSubCriteriaOption::find($id);
        $cas_assessment_criteria_option_id = $obj->cas_assessment_criteria_option_id;
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'),$cas_assessment_criteria_option_id);
        $message = ["successMessage" => "DELETE_SUCCESS", "subCriteriaOptions" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = CasAssessmentSubCriteriaOption::with('cas_assessment_criteria_option')
            ->orderBy('created_at', 'desc')
            ->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        CasAssessmentSubCriteriaOption::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedSubCriteriaOptions" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        CasAssessmentSubCriteriaOption::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedSubCriteriaOptions" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = CasAssessmentSubCriteriaOption::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedSubCriteriaOptions" => $all];
        return response()->json($message, 200);
    }

    public function toggleFreeScore(Request $request) {
        $data = json_decode($request->getContent());
        $object = CasAssessmentSubCriteriaOption::find($data->id);
        $object->is_free_score = $data->is_free_score;
        $object->save();
        if ($data->is_free_score == false) {
            $feedback = ["action" => "SCORE_VALUE_IS_NOT_FREE", "alertType" => "warning"];
        } else {
            $feedback = ["action" => "SCORE_VALUE_IS_FREE", "alertType" => "success"];
        }
        return response()->json($feedback, 200);
    }
}
