<?php

namespace App\Http\Controllers\Assessment;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Assessment\CasAssessmentCategoryVersion;
use App\Models\Assessment\CasAssessmentCriteriaOption;
use App\Models\Assessment\CasAssessmentSubCriteriaOption;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CasAssessmentCategoryVersionController extends Controller
{
    public function index() {
        $all = CasAssessmentCategoryVersion::with('cas_assessment_category','cas_assessment_state','cas_assessment_category.cas_plan','financial_year','reference_document')->orderBy('created_at','desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return CasAssessmentCategoryVersion::with('cas_assessment_category','cas_assessment_state','cas_assessment_category.cas_plan','financial_year','reference_document')->orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new CasAssessmentCategoryVersion();
            $obj->financial_year_id = $data->financial_year_id;
            $obj->reference_document_id = $data->reference_document_id;
            $obj->minimum_passmark = $data->minimum_passmark;
            $obj->cas_assessment_state_id = $data->cas_assessment_state_id;
            $obj->cas_assessment_category_id = $data->cas_assessment_category_id;
            $obj->save();
            //check if u need to copy other params
            if($data->copy_all){
                $this->copyAllAssessmentSettings($obj->id);
            }
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "versions" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = CasAssessmentCategoryVersion::find($data->id);
            $obj->reference_document_id = $data->reference_document_id;
            $obj->minimum_passmark = $data->minimum_passmark;
            $obj->cas_assessment_state_id = $data->cas_assessment_state_id;
            $obj->cas_assessment_category_id = $data->cas_assessment_category_id;
            $obj->financial_year_id = $data->financial_year_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "versions" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = CasAssessmentCategoryVersion::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "versions" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = CasAssessmentCategoryVersion::with('cas_assessment_category','cas_assessment_state','cas_assessment_category.cas_plan','financial_year','reference_document')->orderBy('created_at', 'desc')
            ->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        CasAssessmentCategoryVersion::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedVersions" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        CasAssessmentCategoryVersion::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedVersions" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = CasAssessmentCategoryVersion::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedVersions" => $all];
        return response()->json($message, 200);
    }

    /** function to copy all other assessment settings: including
     *  assessment criteria
     *  assessment sub criteria
     *  assessment possible options
     *  assessment report set
     */
    public function copyAllAssessmentSettings($assessment_category_version_id){
        /** get previous assessment category version of the same assessment category */
        $copied_version = DB::select("SELECT id FROM cas_assessment_category_versions
                                      WHERE cas_assessment_category_id = (SELECT cas_assessment_category_id FROM
                                      cas_assessment_category_versions WHERE id = $assessment_category_version_id)
                                      AND id != $assessment_category_version_id
                                      AND deleted_at is null ORDER BY id DESC limit 1");
        /** copy assessment criteria */
        if(isset($copied_version[0]->id)){
          $copied_version_id =  $copied_version[0]->id;
          //get criteria
          $copied_assessment_criteria = DB::table('cas_assessment_criteria_options')
                                         ->where('cas_assessment_category_version_id', $copied_version_id)
                                         ->get();
          foreach($copied_assessment_criteria as $item){
              $copied_id = $item->id;
              $_obj = new  CasAssessmentCriteriaOption();
              $_obj->cas_assessment_category_version_id = $assessment_category_version_id;
              $_obj->number = $item->number;
              $_obj->name = $item->name;
              $_obj->how_to_assess = $item->how_to_assess;
              $_obj->how_to_score =  $item->how_to_score;
              try{
                $_obj->save();
              $new_id = $_obj->id;
              //get assessment sub criteria options
              $copied_assessment_sub_criteria = DB::table('cas_assessment_sub_criteria_options')
                                                 ->where('cas_assessment_criteria_option_id', $copied_id)
                                                 ->get();
              foreach($copied_assessment_sub_criteria as $sub_item){
                  $copied_sub_id = $sub_item->id;
                  $obj = new CasAssessmentSubCriteriaOption();
                  $obj->cas_assessment_criteria_option_id = $new_id;
                  $obj->serial_number = $sub_item->serial_number;
                  $obj->name = $sub_item->name;
                  $obj->how_to_assess = $sub_item->how_to_assess;
                  $obj->how_to_score = $sub_item->how_to_score;
                  $obj->score_value = $sub_item->score_value;
                  $obj->is_free_score = $sub_item->is_free_score;
                  $obj->save();
                  $new_sub_id = $obj->id;
                  if($copied_sub_id != '' && $new_sub_id != ''){

                    //copy assessment sub criteria option
                    DB::statement("INSERT INTO cas_assessment_sub_criteria_possible_scores
                                    (cas_assessment_sub_criteria_option_id, value, description)
                                    SELECT $new_sub_id, value, description
                                    FROM cas_assessment_sub_criteria_possible_scores
                                    WHERE cas_assessment_sub_criteria_option_id = $copied_sub_id");
                    //copy assessment report
                    DB::statement("INSERT INTO cas_assessment_sub_criteria_report_sets
                                    (cas_assessment_sub_criteria_option_id, item_to_access_id, cas_plan_content_id, name, report_path)
                                    SELECT $new_sub_id, item_to_access_id, cas_plan_content_id, name, report_path
                                    FROM cas_assessment_sub_criteria_report_sets
                                    WHERE cas_assessment_sub_criteria_option_id = $copied_sub_id");

                  }
              }
            }catch(\Exception $e){
                // do task when error
                Log::error(print_r($e->getMessage(), true));
            }
          }
        }
    }
}
