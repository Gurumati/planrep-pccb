<?php

namespace App\Http\Controllers\Assessment;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Assessment\CasAssessmentCategoryVersionState;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class CasAssessmentCategoryVersionStateController extends Controller
{
    public function index() {
        $all = CasAssessmentCategoryVersionState::with('cas_assessment_category_version','cas_assessment_state','cas_assessment_category_version.financial_year','cas_assessment_category_version.cas_assessment_category')->orderBy('created_at','desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return CasAssessmentCategoryVersionState::with('cas_assessment_category_version','cas_assessment_state','cas_assessment_category_version.financial_year','cas_assessment_category_version.cas_assessment_category')->orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new CasAssessmentCategoryVersionState();
            $obj->cas_assessment_category_version_id = $data->cas_assessment_category_version_id;
            $obj->cas_assessment_state_id = $data->cas_assessment_state_id;
            $obj->min_value = $data->min_value;
            $obj->max_value = $data->max_value;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "versionStates" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = CasAssessmentCategoryVersionState::find($data->id);
            $obj->cas_assessment_category_version_id = $data->cas_assessment_category_version_id;
            $obj->cas_assessment_state_id = $data->cas_assessment_state_id;
            $obj->min_value = $data->min_value;
            $obj->max_value = $data->max_value;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "versionStates" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = CasAssessmentCategoryVersionState::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "versionStates" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = CasAssessmentCategoryVersionState::with('cas_assessment_category_version','cas_assessment_state','cas_assessment_category_version.financial_year','cas_assessment_category_version.cas_assessment_category')->orderBy('created_at', 'desc')
            ->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        CasAssessmentCategoryVersionState::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedVersionStates" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        CasAssessmentCategoryVersionState::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedVersionStates" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = CasAssessmentCategoryVersionState::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedVersionStates" => $all];
        return response()->json($message, 200);
    }
}
