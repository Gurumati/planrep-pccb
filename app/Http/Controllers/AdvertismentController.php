<?php

namespace App\Http\Controllers;

use App\Http\Services\UserServices;
use App\Models\Ads\Advertisment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class AdvertismentController extends Controller
{

    public function index()
    {
        $items = Advertisment::where('status',true)
            ->select('description','ad_url')
            ->orderBy('id', 'asc')->get();
        return response()->json(['items' => $items], 200);
    }

    public function getAllPaginated($perPage)
    {
        return Advertisment::orderBy('id', 'asc')->paginate($perPage);
    }

    public function paginated(Request $request)
    {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json(['items' => $all], 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $description = $request->description;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $created_by = UserServices::getUser()->id;
        $urls = $request['urls'];
       for($i=0;$i<sizeof($urls);$i++){
           Advertisment::create([
               'description' =>$description,
               'ad_url' => $urls[$i]['ad_url'],
               'start_date' => $start_date,
               'end_date' => $end_date,
               'created_by' => $created_by,
           ]);
       }
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "CREATE_SUCCESS", "items" => $all];
        return response()->json($message, 200);

    }
    public function uploadAdvertisement(Request $request)
    {
        $validation_rule = ["file" => "required|mimes:jpeg,jpg,png"];

        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $validator = Validator::make($request->all(), $validation_rule);
                if (!$validator->fails()) {
                    $file = $request->file;
                    $path = $file->store('uploads/banners');
                    return $path;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function removeAdvertisement(Request $request)
    {
        $file_url = $request->file_url;

        File::delete($file_url);
        return $file_url;
    }
    public function update(Request $request)
    {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, Advertisment::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            $obj = Advertisment::find($request->id);
            $obj->update($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        }
    }

    public function changeAdStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;
        if($status){
            Advertisment::where('id',$id)->update([
                'status' => false,
                'updated_by' =>UserServices::getUser()->id,
            ]);
        }
        if(!$status) {
            Advertisment::where('id',$id)->update([
                'status' => true
            ]);
        }
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }

    public function delete($id)
    {
        $item = Advertisment::find($id);
        $item->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }
}
