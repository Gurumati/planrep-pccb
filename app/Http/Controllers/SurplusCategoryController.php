<?php

namespace App\Http\Controllers;

use App\Http\Services\UserServices;
use App\Models\Ads\Advertisment;
use App\Models\Setup\SurplusCategory;
use App\Models\Setup\TaskNature;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class SurplusCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $all = SurplusCategory::all();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return SurplusCategory::whereNull('deleted_at')
            ->orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = SurplusCategory::create([
            'name' => $request->name,
            'code' => $request->code,
            'created_by' => UserServices::getUser()->id,
        ]);
       return  $this->getAllPaginated(10);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Setup\SurplusCategory  $surplusCategory
     * @return \Illuminate\Http\Response
     */
    public function show(SurplusCategory $surplusCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Setup\SurplusCategory  $surplusCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(SurplusCategory $surplusCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Setup\SurplusCategory  $surplusCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = json_decode($request->getContent());
        try{
            $obj = SurplusCategory::find($data->id);
            $obj->name = $data->name;
            $obj->code = $data->code;
            $obj->is_active = isset($data->is_active)?$data->is_active: true;
            $obj->save();
            return  $this->getAllPaginated($request->perPage);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function toggleActive(Request $request)
    {
        $id = $request->id;
        $status = $request->is_active;
        if($status == 'true'){
            SurplusCategory::where('id',$id)->update([
                'is_active' => false,
                'updated_by' =>UserServices::getUser()->id,
            ]);
        }
        else {
            SurplusCategory::where('id',$id)->update([
                'is_active' => true,
                'updated_by' =>UserServices::getUser()->id,
            ]);
        }
        return  $this->getAllPaginated(10);
    }
    /*
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Setup\SurplusCategory  $surplusCategory
     * @return \Illuminate\Http\Response
     */
    public function delete($surplusCategory)
    {
        $obj = SurplusCategory::find($surplusCategory);
        $obj->delete();
        return $this->getAllPaginated(10);
    }
}
