<?php

namespace App\Http\Controllers\Notifications;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Auth\User;
use Illuminate\Http\Request;


class UserNotificationController extends Controller
{
    public function index() {
        $title = "Notifications|" . $this->app_initial;
        return view('notifications.index', ['js_scripts' => $this->form_scripts, 'title' => $title]);
    }

    public function all_notifications(Request $request)
    {
        $user = UserServices::getUser();
        $n = 0;
        foreach ($user->unreadNotifications as $notification) {
            $n++;
        }
        $message = $user->notifications()->orderBy('created_at','desc')->paginate($request->perPage);

        return response()->json($message, 200);
    }

    public function unread_notifications($user_id)
    {
        $user = User::find($user_id);
        $n = 0;
        foreach ($user->unreadNotifications as $notification) {
            $n++;
        }
//

        $message = ["unreads" => $n, "notifications" => $user->unreadNotifications];
        return response()->json($message, 200);
    }

    public function read_notifications($notification_id)
    {
        $user = UserServices::getUser();

        $data = "{}";
        $notification = $user->notifications()->where('id',$notification_id)->first();
        if ($notification)
        {
            $notification->markAsRead();
            $data = $notification->data;

        }

        return redirect($data['url']);
    }
}
