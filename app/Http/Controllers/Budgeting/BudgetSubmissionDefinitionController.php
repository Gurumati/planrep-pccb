<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Budgeting\BudgetSubmissionDefinition;
use App\Models\Budgeting\BudgetSubmissionSubForm;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class BudgetSubmissionDefinitionController extends Controller
{
    public function index($id){
        $budgetSubmissionDefinitions = BudgetSubmissionDefinition::where('budget_submission_form_id',$id)->get();
        return response()->json($budgetSubmissionDefinitions);
    }

    public function paginateIndex(Request $request){
        $form_id = $request->form_id;
        $all = BudgetSubmissionDefinition::where('budget_submission_form_id',$form_id)->paginate($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request){
        $userID = UserServices::getUser()->id;
        $data = json_decode($request->getContent());
        $form_id = $data->budget_submission_form_id;
        $budgetSubmissionDefinition = new BudgetSubmissionDefinition();
        $budgetSubmissionDefinition->field_name = $data->field_name;
        $budgetSubmissionDefinition->gfs_code_id = isset($data->gfs_code_id)?$data->gfs_code_id:null;
        $budgetSubmissionDefinition->unit_id = isset($data->unit_id)?$data->unit_id:null;
        $budgetSubmissionDefinition->parent_id = isset($data->parent_id)?$data->parent_id:null;
        $budgetSubmissionDefinition->is_input = $data->is_input;
        $budgetSubmissionDefinition->has_breakdown = $data->has_breakdown;
        $budgetSubmissionDefinition->sort_order = $data->sort_order;
        $budgetSubmissionDefinition->created_by = $userID;
        $budgetSubmissionDefinition->budget_submission_form_id = $data->budget_submission_form_id;
        $budgetSubmissionDefinition->column_number = isset($data->column_number)?$data->column_number:null;
        $budgetSubmissionDefinition->formula = isset($data->formula)?$data->formula:null;
        $budgetSubmissionDefinition->type = isset($data->type)?$data->type:null;
        $budgetSubmissionDefinition->output_type = isset($data->output_type)?$data->output_type:null;
        $budgetSubmissionDefinition->is_vertical_total = isset($data->is_vertical_total)?$data->is_vertical_total:false;
        $budgetSubmissionDefinition->select_option = isset($data->select_option)?$data->select_option:null;
        $budgetSubmissionDefinition->is_active = $data->is_active;
        $budgetSubmissionDefinition->save();
        //Return language success key and all data
        $all = BudgetSubmissionDefinition::where('budget_submission_form_id',$form_id)
            ->paginate($request->perPage);
        $message = ["successMessage" => "COLUMN_CREATED_SUCCESSFULLY", "budgetSubmissionDefinitions" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request){
        try {
            $userID = UserServices::getUser()->id;
            $data = json_decode($request->getContent());
            $budgetSubmissionDefinition = BudgetSubmissionDefinition::find($data->id);
            $budgetSubmissionDefinition->field_name = $data->field_name;
            $budgetSubmissionDefinition->gfs_code_id = isset($data->gfs_code_id)?$data->gfs_code_id:null;
            $budgetSubmissionDefinition->unit_id = isset($data->unit_id)?$data->unit_id:null;
            $budgetSubmissionDefinition->parent_id = isset($data->parent_id)?$data->parent_id:null;
            $budgetSubmissionDefinition->is_input = $data->is_input;
            $budgetSubmissionDefinition->has_breakdown = $data->has_breakdown;
            $budgetSubmissionDefinition->sort_order = $data->sort_order;
            $budgetSubmissionDefinition->budget_submission_form_id = $data->budget_submission_form_id;
            $budgetSubmissionDefinition->updated_by = $userID;
            $budgetSubmissionDefinition->column_number = isset($data->column_number)?$data->column_number:null;
            $budgetSubmissionDefinition->formula = isset($data->formula)?$data->formula:null;
            $budgetSubmissionDefinition->type = isset($data->type)?$data->type:null;
            $budgetSubmissionDefinition->output_type = isset($data->output_type)?$data->output_type:null;
            $budgetSubmissionDefinition->is_vertical_total = isset($data->is_vertical_total)?$data->is_vertical_total:false;
            $budgetSubmissionDefinition->select_option = isset($data->select_option)?$data->select_option:null;
            $budgetSubmissionDefinition->save();
            //Return language success key and all data
            $all = BudgetSubmissionDefinition::where('budget_submission_form_id',$budgetSubmissionDefinition->budget_submission_form_id)
                                               ->paginate($request->perPage);
            $message = ["successMessage" => "BUDGET_SUBMISSION_FORM_UPDATED_SUCCESSFULLY", "budgetSubmissionDefinitions" =>$all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_BUDGET_SUBMISSION_FORM_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

    public function delete(Request $request){
        $userID = UserServices::getUser()->id;
        $data = json_decode($request->getContent());
        $budgetSubmissionDefinition = BudgetSubmissionDefinition::find($data->id);
        $budget_submission_form_id = $data->budget_submission_form_id;
       // $budget_submission_definition_id = $budgetSubmissionDefinition->id = $data->id;
        $budgetSubmissionDefinition->deleted_by = $userID;

        $budgetSubmissionDefinition->delete();
        $all = BudgetSubmissionDefinition::where('budget_submission_form_id',$budgetSubmissionDefinition->budget_submission_form_id)
            ->paginate($request->perPage);
        $message = ["successMessage" => "SUCCESSFULLY_COLUMN_DELETED", "budgetSubmissionDefinitions"=>$all];
        return response()->json($message, 200);
    }

    public function loadParent($parent_column, $form_id){
        if($parent_column != 0)
        {
            $parentColumns = BudgetSubmissionDefinition::whereNull('parent_id')
                ->where('budget_submission_form_id',$form_id)
                ->where('id','!=',$parent_column)
                ->select('id','field_name')
                ->get();
        }else {
            $parentColumns = BudgetSubmissionDefinition::whereNull('parent_id')
                ->where('budget_submission_form_id',$form_id)
                ->select('id','field_name')
                ->get();
        }
        return response()->json($parentColumns);
    }

    public function generateBudgetFormTemplate($id)
    {
        //get form id
        $submissionForm = BudgetSubmissionSubForm::where('id',$id)
                          ->select('budget_submission_form_id')
                          ->first();
        $form_id = $submissionForm->budget_submission_form_id;
        $budgetSubmissionDefinitions = BudgetSubmissionDefinition::where('budget_submission_form_id',$form_id)->get();

        $data_array[]  = 'Sub Vote';
        foreach($budgetSubmissionDefinitions as $value)
        {
           if(sizeof($value->children) == 0)
           {
               $data_array[] = $value->field_name;
           }

        }
        $fileName = 'PE Form'.$id;
        $fullPath = Excel::create($fileName,function($excel) use ($data_array) {
            // Set the spreadsheet title, creator, and description
            $excel->setTitle('PE Form');
            $excel->setCreator('DICT')->setCompany('tamisemi.go.tz ');
            $excel->setDescription('PE Form Template');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('PE_Form', function($sheet) use ($data_array) {
                $sheet->fromArray($data_array, null, 'A1', false, false);
            });

        })->store('xlsx',public_path('/uploads/pe_templates'), true);

        return $fullPath;
    }

}
