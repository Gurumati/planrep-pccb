<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Budgeting\BudgetType;
use App\Models\Planning\ActivityFacilityFundSource;

class ActivityFacilityFundSourceController extends Controller
{

    public function find($id){
        return ['activityFacilityFundSource'=>ActivityFacilityFundSource::getOne($id)];
    }

    public function paginate($mtefSectionId,$budgetType){

        if(!UserServices::hasPermission(['budgeting.input.'.$budgetType])){
            return response()->json(null,403);
        }
        if(!in_array($budgetType , BudgetType::getAll())){
            return response()->json(['errorMessage'=>'Invalid budget type'],400);
        }
        return ['activityFacilityFundSources'=>ActivityFacilityFundSource::paginated($mtefSectionId,$budgetType)];
    }
}
