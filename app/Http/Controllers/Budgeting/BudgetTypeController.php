<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Models\Budgeting\BudgetType;

class BudgetTypeController extends Controller
{

    public function index(){

        return ['budgetTypes'=>BudgetType::getAll()];
    }
}
