<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Setup\ExpenditureCentre;
use App\Models\Setup\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BudgetDistributionController extends Controller
{

    public function getByExpenditureCentres(Request $request){
        $flatten=new Flatten();
        $financialyear=FinancialYearServices::getPlanningFinancialYear();
        $admin_hierarchy_id=UserServices::getUser()->admin_hierarchy_id;
        $section_id=UserServices::getUser()->section_id;
        $userSection=Section::where('id',$section_id)->get();
        $sectorIds=$flatten->flattenSectionWithChildrenGetSectors($userSection);

        $totalBudget=$this->getBudgetTotal($admin_hierarchy_id,$financialyear->id,$section_id);
        $expenditureCentres=ExpenditureCentre::where('is_active',true)->select('name','code','id')->get();
        foreach ($expenditureCentres as &$c){
            $gfsCodes=DB::table('bdc_expenditure_centre_gfs_codes')->where('expenditure_centre_id',$c->id)->get();
            $values=DB::table('bdc_expenditure_centre_values')->where('expenditure_centre_id',$c->id)->
                        where('financial_year_id',$financialyear->id)->first();
            if($values){
                $c->min_percentage=round($values->min_value,2);
                $c->max_percentage=round($values->max_value,2);
            }

            $gfsCodeIds=$flatten->getIds($gfsCodes,'gfs_code_id');
            $subTotal=$this->getByGfsCodes($admin_hierarchy_id,$financialyear->id,$section_id,$gfsCodeIds);
            $c->total_allocated=$subTotal;
            if($totalBudget == 0 || $totalBudget ==null){
                $c->allocated_percentage=0;
            }else{
                $c->allocated_percentage=round((($c->total/$totalBudget)*100),2);
            }
        }
        $mainGroups=$this->subCentresExpenditure($admin_hierarchy_id,$financialyear->id,$section_id,$sectorIds,$totalBudget);

        $message=['totalBudget'=>$totalBudget,'expenditureCentres'=>$expenditureCentres,'mainGroups'=>$mainGroups];

        return response()->json($message);
    }

    public function subCentresExpenditure($adminHierarchyId,$financialYearId,$sectionId,$sectorIds,$totalBudget){
        $flatten=new Flatten();
        $Ids=[];

        $mainGroups=DB::table('bdc_main_group_fund_sources as mf')->
                  join('bdc_main_groups as m','mf.main_group_id','m.id')->
                  leftJoin('fund_sources as f','mf.fund_source_id','f.id')->
                  whereIn('m.sector_id',$sectorIds)->
                  where('mf.financial_year_id',$financialYearId)->select('mf.*','m.name as main_group','f.name as fund_source')->
                  get();

        foreach ($mainGroups as &$main){
            $main->groups=[];
            $total_allocated=0;
            $groups=DB::table('bdc_groups as g')->
                where('g.bdc_main_group_fund_source_id',$main->id)->
                get();
            foreach ($groups as &$g){
                $g->subCentres=[];

                $groupSubCentres=DB::table('bdc_sector_expenditure_sub_centres as sub')->
                join('bdc_sector_expenditure_sub_center_groups as subg','subg.bdc_sector_expenditure_sub_centre_id','sub.id')->
                where('bdc_group_id',$g->id)->select('sub.*')->
                get();
                foreach ($groupSubCentres as $subCentre){
                    $g->subCentres[]=$subCentre;
                    $gfs=DB::table('bdc_sector_expenditure_sub_centre_gfs_codes')->
                    where('bdc_sector_expenditure_sub_centre_id',$subCentre->id)->
                    get();
                    if(sizeof($gfs) >0)
                        $Ids=array_merge($Ids,$flatten->getIds($gfs,'gfs_code_id'));
                }
                $g->total_allocated=$this->getByGfsCodes($adminHierarchyId,$financialYearId,$sectionId,$Ids);
                $total_allocated=$total_allocated+$g->total_allocated;
                $main->groups[]=$g;
            }
            $main->total_allocated=$total_allocated;
            if($totalBudget >0){
                $main->percentage_allocated=round((($total_allocated/$totalBudget)*100),2);
            }
            else{
                $main->percentage_allocated=0;
            }

        }

        return $mainGroups;
    }

    private function getBudgetTotal($admin_hierarch_id,$financial_year_id,$section_id){

        $total=DB::table('activity_facility_fund_source_inputs as affi')->
                    join('activity_facility_fund_sources as aff','aff.id','affi.activity_facility_fund_source_id')->
                    join('activity_facilities as af','af.id','aff.activity_facility_id')->
                    join('activities as a','a.id','af.activity_id')->
                    join('mtef_sections as ms','ms.id','a.mtef_section_id')->
                    join('mtefs as m','m.id','ms.mtef_id')->
                    where('m.admin_hierarchy_id',$admin_hierarch_id)->
                    where('m.financial_year_id',$financial_year_id)->
                    where('ms.section_id',$section_id)->
                    select(DB::raw('COALESCE(SUM(affi.unit_price * affi.frequency * affi.quantity),0) as total'))->get();
        if($total){
            return $total[0]->total;
        }else{
            return 0;
        }
    }
    private function getByGfsCodes($admin_hierarch_id,$financial_year_id,$section_id,$gfsCodeIds){

        $total=DB::table('activity_facility_fund_source_inputs as affi')->
                    join('activity_facility_fund_sources as aff','aff.id','affi.activity_facility_fund_source_id')->
                    join('activity_facilities as af','af.id','aff.activity_facility_id')->
                    join('activities as a','a.id','af.activity_id')->
                    join('mtef_sections as ms','ms.id','a.mtef_section_id')->
                    join('mtefs as m','m.id','ms.mtef_id')->
                    where('m.admin_hierarchy_id',$admin_hierarch_id)->
                    where('m.financial_year_id',$financial_year_id)->
                    where('ms.section_id',$section_id)->
                    whereIn('affi.gfs_code_id',$gfsCodeIds)->
                    select(DB::raw('COALESCE(SUM(affi.unit_price * affi.frequency * affi.quantity),0) as total'))->get();
        if($total){
            return $total[0]->total;
        }else{
            return 0;
        }
    }

}
