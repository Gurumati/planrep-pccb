<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Budgeting\BudgetSubmissionForm;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class BudgetSubmissionFormController extends Controller
{
    public function index(){
        $submissionForms = BudgetSubmissionForm::with('budgetClass')->get();
        return response()->json($submissionForms);
    }


    public function store(Request $request){
        $userID = UserServices::getUser()->id;
        $data = json_decode($request->getContent());
        $submissionForms = new BudgetSubmissionForm();
        $submissionForms->name = $data->name;
        $submissionForms->description = $data->description;
        $submissionForms->budget_classes = implode(',',$data->budget_classes);
        $submissionForms->fund_sources = implode(',',$data->fund_sources);
        $submissionForms->created_by = $userID;
        $submissionForms->save();
        //Return language success key and all data
        $message = ["successMessage" => "BUDGET_SUBMISSION_FORM_CREATED_SUCCESSFULLY", "budgetSubmissionForms" => BudgetSubmissionForm::with('budgetClass')->get()];
        return response()->json($message, 200);
    }

    public function update(Request $request){
        try {
            $data = json_decode($request->getContent());
            $submissionForms = BudgetSubmissionForm::find($data->id);
            $submissionForms->name = $data->name;
            $submissionForms->description = $data->description;
            $submissionForms->budget_classes = implode(",",$data->budget_classes);
            $submissionForms->fund_sources = implode(",",$data->fund_sources);
            $submissionForms->save();
            //Return language success key and all data
            $message = ["successMessage" => "BUDGET_SUBMISSION_FORM_UPDATED_SUCCESSFULLY", "budgetSubmissionForms" => BudgetSubmissionForm::with('budgetClass')->get()];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_BUDGET_SUBMISSION_FORM_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

    public function delete($id){
        $budgetSubmissionForm = BudgetSubmissionForm::find($id);
        $budgetSubmissionForm->delete();
        $message = ["successMessage" => "SUCCESSFULLY_BUDGET_SUBMISSION_FORM_DELETED", "budgetSubmissionForms" => BudgetSubmissionForm::with('budgetClass')->get()];
        return response()->json($message, 200);
    }

    public function peFundSources()
    {
        //get fund sources
        $fund_source_id = DB::table('budget_submission_forms')
                          ->distinct()
                          ->orderBy('fund_sources')
                          ->first()->fund_sources;

        $ids = explode(',',$fund_source_id);
        $fund_sources = DB::table('fund_sources')
                        ->whereIn('id',$ids)
                        ->select('id','name')
                        ->get();

        return response()->json($fund_sources);
    }

}
