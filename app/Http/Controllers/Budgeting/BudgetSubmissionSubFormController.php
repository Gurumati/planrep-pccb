<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Models\Budgeting\BudgetSubmissionSubForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BudgetSubmissionSubFormController extends Controller
{

    public function index()
    {
        //
        $budgetSubForm = BudgetSubmissionSubForm::with('parent')->whereNull('deleted_at')->whereNotNull('budget_submission_form_id')->orderBy('sort_order')->get();
        return response()->json($budgetSubForm);
    }

    public function store(Request $request)
    {
        $data = json_decode($request->getContent());

            try {

                $budgetForm = new BudgetSubmissionSubForm();
                $budgetForm->code = $data->code;
                $budgetForm->name = $data->name;
                if($request->exists('budget_submission_form_id'))
                {
                    $budgetForm->budget_submission_form_id = $data->budget_submission_form_id;
                }
                if($request->exists('parent_id'))
                {
                    $budgetForm->parent_id = $data->parent_id;
                }
                $budgetForm->is_lowest = $data->is_lowest;
                $budgetForm->sort_order = $data->sort_order;
                $budgetForm->save();
                //Return language success key and all data
                $budgetForms = BudgetSubmissionSubForm::whereNull('parent_id')->with('child_budgetForm')->get();
                $message = ["successMessage" => "SUCCESSFUL_BUDGET_SUBMISSION_FORM_LEVEL_CREATED", "budgetSubmissionSubForms" => $budgetForms];
                return response()->json($message, 200);

            } catch (QueryException $exception) {
                $error_code = $exception->errorInfo[1];
                //ERROR CODE 7 CATCH Duplicate
                if ($error_code == 7) {
                    $message = ["errorMessage" => "ERROR_BUDGET_SUBMISSION_FORM_LEVEL_CODE_EXISTS"];
                    return response()->json($message, 400);
                } else {
                    $message = ["errorMessage" => 'ERROR_OTHERS'];
                    return response()->json($message, 400);
                }
            }
    }

    public function update(Request $request)
    {
        //
        $data = json_decode($request->getContent());

            try {

                $budgetForm = BudgetSubmissionSubForm::find($data->id);
                $budgetForm->code = $data->code;
                $budgetForm->name = $data->name;
                $budgetForm->budget_submission_form_id = $data->budget_submission_form_id;
                $budgetForm->parent_id = $data->parent_id;
                $budgetForm->is_lowest = $data->is_lowest;
                $budgetForm->sort_order = $data->sort_order;
                $budgetForm->save();
                //Return language success key and all data
                $budgetForms = BudgetSubmissionSubForm::whereNull('parent_id')->with('child_budgetForm')->get();
                $message = ["successMessage" => "SUCCESSFUL_BUDGET_SUBMISSION_FORM_LEVEL_UPDATED", "budgetSubmissionSubForms" => $budgetForms];
                return response()->json($message, 200);

            } catch (QueryException $exception) {
                $error_code = $exception->errorInfo[1];
                //ERROR CODE 7 CATCH Duplicate
                if ($error_code == 7) {
                    $message = ["errorMessage" => "ERROR_BUDGET_SUBMISSION_FORM_LEVEL_CODE_EXISTS"];
                    return response()->json($message, 400);
                } else {
                    $message = ["errorMessage" => 'ERROR_OTHERS'];
                    return response()->json($message, 400);
                }
            }
    }

    public function delete($id)
    {
        //
        $budgetForm = BudgetSubmissionSubForm::find($id);
        try{
          $budgetForm->delete();
          $budgetForms = BudgetSubmissionSubForm::whereNull('parent_id')->with('child_budgetForm')->get();
          $message = ["successMessage" => "SUCCESSFUL_BUDGET_SUBMISSION_FORM_LEVEL_DELETED", "budgetSubmissionSubForms" => $budgetForms];
          return response()->json($message, 200);
        }catch (QueryException $exception) {
          $message = ["errorMessage" => 'ERROR_OTHERS'];
          return response()->json($message, 400);
        }

    }

    public function parentFormLevel()
    {
            $parentForms = DB::table('budget_submission_forms')
                ->select('budget_submission_forms.*')
                ->where('deleted_at',null)
                ->get();

        return response()->json($parentForms, 200);
    }

    public function parentSubForm($id)
    {
        if($id == 0){
            $parentForms = DB::table('budget_submission_sub_forms')
                ->select('budget_submission_sub_forms.*')
                ->where('deleted_at',null)
                ->get();
        }else
        {
            $parentForms = DB::table('budget_submission_sub_forms')
                ->select('budget_submission_sub_forms.*')
                ->where('deleted_at',null)
                ->where('id','!=',$id)
                ->get();
        }

        return response()->json($parentForms, 200);
    }

    //a function to get number of employess for all pscs
    public function getEmployeesDetails(Request $request)
    {
        
        $financial_year_id = $request->financialYearId;
        $budget_type = $request->budgetType;
        $query = "select
                admin_hierarchy institutionName,trno trNumber,
                sum(Nmber_emp) noOfEmployees
            from (select
                    a.admin_hierarchy, a.trno,
                    max( case when (a.column_number = 101)  then a.field_value else 0 end) as Nmber_emp
            
                from
                    (select
                        MAX(a.code) as trno,
                        a.name as admin_hierarchy,
                        s.id,
                        bf.column_number,
                        sum(cast(replace(bv.field_value,',','')::numeric as float))  as field_value
                    from
                        admin_hierarchies a
                            inner join admin_hierarchies a1 on a.parent_id = a1.id
                            inner join admin_hierarchies a2 on a1.parent_id = a2.id
                            inner join mtefs m on a.id = m.admin_hierarchy_id
                            inner join mtef_sections ms on m.id = ms.mtef_id
                            inner join activities ac on ac.mtef_section_id = ms.id
                            inner join budget_submission_lines bl on bl.activity_id =ac.id
                            inner join sections s on bl.section_id = s.id
                            inner join section_levels  sl on s.section_level_id = sl.id
                            inner join budget_submission_line_values bv on bv.budget_submission_line_id = bl.id
                            inner join budget_submission_sub_forms bs on bs.id = bl.budget_submission_sub_form_id
                            inner join budget_submission_forms bsf on bsf.id = bs.budget_submission_form_id
                            inner join budget_submission_definitions bf on bf.id = bv.budget_submission_definition_id
                            inner join fund_sources f on f.id = bl.fund_source_id
                            inner join financial_years fy on m.financial_year_id = fy.id
                    where
                        bl.budget_submission_sub_form_id in(7,8,9)  and
                        m.financial_year_id =  $financial_year_id   and
                        bv.field_value != '' and ac.budget_type =   '$budget_type' and sl.hierarchy_position = 4
                    group by
                        a.name,
                        f.name,
                        s.name,
                        s.code,
                        s.id,
                        bf.id) a
                group by a.admin_hierarchy, a.column_number, a.id,a.trno
                order by a.id) main
            group by admin_hierarchy,trNumber";

            $data = DB::select(DB::raw($query));

            return response()->json(["institutionEmployees" => $data], 200);


    }

}
