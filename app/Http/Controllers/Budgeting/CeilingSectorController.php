<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Models\Budgeting\CeilingSector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CeilingSectorController extends Controller {
    private $limit = 5;

    private function all() {
        return CeilingSector::with('sector')->with('ceiling')->get();
    }

    public function index() {
        return response()->json($this->all());
    }

    public function getAllPaginated($perPage) {
        return CeilingSector::with('sector')->with('ceiling')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function ceilingSectors($ceilingId) {
        $all = CeilingSector::with('sector')->where('ceiling_id', $ceilingId)->get();
        return response()->json($all);
    }

    public function delete($id) {
        $object = CeilingSector::find($id);
        $object->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFULLY_CEILING_SECTOR_REMOVED", "ceiling_sectors" => $all];
        return response()->json($message, 200);
    }
}
