<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Budgeting\Ceiling;
use App\Models\Budgeting\CeilingBudgetClass;
use App\Models\Budgeting\CeilingSector;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\GfsCode;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use App\Models\Budgeting\AdminHierarchyCeiling;
use App\Models\Setup\BudgetClassVersion;
use App\Models\Setup\FundSourceVersion;
use App\Models\Setup\Section;
use App\Models\Setup\Version;

class CeilingController extends Controller
{
    public function index()
    {
        $all = Ceiling::with('budget_class')->with('gfs_code')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage)
    {
        $fy =FinancialYearServices::getPlanningFinancialYear();
        $financialYearId = isset($fy->id)?$fy->id:FinancialYearServices::getExecutionFinancialYear();

        $versionBcId = Version::getVersionByFinancialYear($financialYearId, 'BC');
        $budgetClassIds =BudgetClassVersion::select('budget_class_id')->where('version_id',$versionBcId)->pluck('budget_class_id')->toArray();

        $versionId = Version::getVersionByFinancialYear($financialYearId, 'FS');
        $fundSourceIds =FundSourceVersion::select('fund_source_id')->where('version_id',$versionId)->pluck('fund_source_id')->toArray();

        $gfsCode = Ceiling::with('budget_class', 'sectors', 'gfs_code', 'gfs_code.fund_source_gfs.fund_source', 'aggregate_fund_source')
            ->where(function ($query) use($budgetClassIds,$fundSourceIds){
                $query->whereHas('gfs_code.fund_source_gfs.fund_source', function ($query) use($fundSourceIds) {
                    $query->where('can_project', false)->whereIn('fund_sources.id',$fundSourceIds);
                    $queryString = Input::get('searchQuery');
                    if (isset($queryString)) {
                        $query->where('name', 'LIKE', '%' . $queryString . '%');
                    }
                })->orWhereNull('gfs_code_id');
            })->whereIn('budget_class_id',$budgetClassIds)
            ->orderBy('name')->paginate($perPage);

            foreach ($gfsCode as &$gfs) {
                $fundSourceGfs = isset($gfs->gfs_code->fund_source_gfs)?$gfs->gfs_code->fund_source_gfs:[];
                $fundSources = sizeof($fundSourceGfs)>0?$fundSourceGfs:[];
                if (isset($gfs->gfs_code)) {
                    foreach ($fundSources as &$fund) {
                        if(in_array($fund->fund_source_id, $fundSourceIds)){
                            $gfs->gfs_code->fund_source = $fund->fund_source; 
                        }
                    }
                }
                unset($gfs->fund_source_gfs);
               
            }

            return $gfsCode;
    }

    public function paginated(Request $request)
    {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request)
    {
        $data = json_decode($request->getContent());
        $obj = new Ceiling();
        $gfs_code = GfsCode::find($data->gfs_code_id);
        $budget_class = BudgetClass::find($data->budget_class_id);
        try {
            $obj->name = $gfs_code->name . "-" . $budget_class->name;
            $obj->gfs_code_id = $data->gfs_code_id;
            $obj->budget_class_id = $data->budget_class_id;
            $obj->created_by = UserServices::getUser()->id;
            $obj->save();
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }

        //Initialize ceilings
        $ceiling_id = $obj->id;

        //sectors
        $sectorArray = $data->sector_id;

        for ($i = 0; $i < count($sectorArray); $i++) {
            $sector_id = $sectorArray[$i];
            $ceiling_sector = new CeilingSector();
            $ceiling_sector->sector_id = $sector_id;
            $ceiling_sector->ceiling_id = $ceiling_id;
            $ceiling_sector->created_by = UserServices::getUser()->id;
            $ceiling_sector->save();
        }

        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_CEILING_CREATED", "ceilings" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request)
    {
        $data = json_decode($request->getContent());
        $obj = Ceiling::find($data->id);
        try {
            $obj->budget_class_id = $data->budget_class_id;
            $obj->gfs_code_id = $data->gfs_code_id;
            $obj->updated_by = UserServices::getUser()->id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_CEILING_UPDATED", "ceilings" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id)
    {
        $object = Ceiling::find($id);
        $countUsage= AdminHierarchyCeiling::where('ceiling_id',$id)->where('amount','>',0)->count();
        if($countUsage == 0) {
            AdminHierarchyCeiling::where('ceiling_id',$id)->where('amount',0)->delete();
            DB::table('ceiling_sectors')->where('ceiling_id', $id)->delete();
            $object->delete();
        }
        else{
            $message = ["errorMessage" => "Cannot deactivate ceiling becaused it is used "];
            return response()->json($message, 500);
        }
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFULLY_CEILING_DELETED", "ceilings" => $all];
        return response()->json($message, 200);
    }
    public function toggleActivate($id)
    {
        $object = Ceiling::find($id);
        if($object->is_active){
            $countUsage= AdminHierarchyCeiling::where('ceiling_id',$id)->where('amount','>',0)->count();
            if($countUsage == 0) {
                $object->is_active = false;
            }
            else{
                $message = ["errorMessage" => "Cannot deactivate ceiling becaused it is used "];
                return response()->json($message, 500);
            }
        }
        else{
            $object->is_active = true;
        }
        $object->save();
        $message = ["successMessage" => "SUCCESSFULLY_CEILING_DELETED"];
        return response()->json($message, 200);
    }

    public function allCeilingSectors($id)
    {
        $all = CeilingSector::where('ceiling_id', $id)->with('sector')->get();
        $message = ["ceiling_sectors" => $all];
        return response()->json($message);
    }

    public function fundSourceCeilings($fundSourceId)
    {
        $fundSourceCeilings = DB::table('ceilings as c')
            ->join('gfs_codes as g', 'g.id', '=', 'c.gfs_code_id')
            ->join('fund_sources as f', 'f.id', '=', 'g.fund_source_id')
            ->join('budget_classes as b', 'b.id', '=', 'c.budget_class_id')
            ->where('f.id', $fundSourceId)
            ->select('c.*', 'b.name as budget_class')
            ->get();

        $message = ["fund_source_ceilings" => $fundSourceCeilings];
        return response()->json($fundSourceCeilings);
    }

    public function addSector(Request $request)
    {
        $data = json_decode($request->getContent());
        $ceiling_id = $data->ceiling_id;
        $sector_id = $data->sector_id;
        try {
            $count=CeilingSector::where('ceiling_id',ceiling_id)->where('sector_id',$sector_id)->count();
            if($count == 0){
                $obj = new CeilingSector();
                $obj->ceiling_id = $ceiling_id;
                $obj->sector_id = $sector_id;
                $obj->created_by = UserServices::getUser()->id;
                $obj->save();
            }
            $all = CeilingSector::where('ceiling_id', $data->ceiling_id)->with('sector')->get();
            $message = ["ceiling_sectors" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete_ceiling_sector($ceiling_sector_id, $ceiling_id)
    {
        $object = CeilingSector::find($ceiling_sector_id);
        $object->delete();
        $all = CeilingSector::where('ceiling_id', $ceiling_id)->with('sector')->get();
        $message = ["ceiling_sectors" => $all];
        return response()->json($message);
    }

    public function removeCeilingSector($ceilingId, $sectorId)
    {
        $sectionIds = Section::where('sector_id',$sectorId)->pluck('id');
        $countUsage= AdminHierarchyCeiling::where('ceiling_id',$ceilingId)
                    ->where('amount','>',0)
                    ->whereIn('section_id',$sectionIds)
                    ->count();
        if($countUsage > 0){
            $message = ["errorMessage" => "Cannot remove sector because the ceiling is used by sections of the sector"];
            return response()->json($message, 500);
        }
        DB::table('ceiling_sectors')->where('sector_id', $sectorId)->where('ceiling_id', $ceilingId)->delete();
        $message = ["successMessage" => "SECTOR_REMOVED_SUCCESSFUL"];
        return response()->json($message, 200);

    }

    public function addCeilingSector($ceilingId, $sectorId)
    {
        $count=CeilingSector::where('ceiling_id',$ceilingId)->where('sector_id',$sectorId)->count();
        if($count == 0){
            DB::table('ceiling_sectors')->insert(array('sector_id' => $sectorId, 'ceiling_id' => $ceilingId));
        }
        $message = ["successMessage" => "SECTOR_ADDED_SUCCESSFUL"];
        return response()->json($message, 200);

    }

    public function toggleExtendsToFacility($ceilingId, $extendsToFacility)
    {
        $x = ($extendsToFacility == 'true') ? true : false;
        DB::table('ceilings')->where('id', $ceilingId)->update(array('extends_to_facility' => $x));
        $message = ["successMessage" => "SECTOR_ADDED_SUCCESSFUL"];
        return response()->json($message, 200);
    }

    public function getCeiling($financialYearId, $budgetType, $adminHierarchyId, $sectiorId){
      
        $ceilings =  DB::table('ceilings as c')
            ->join('admin_hierarchy_ceilings as admc','admc.ceiling_id','c.id')
            ->join('sections as sec','sec.id','admc.section_id')
            ->join('budget_classes as bc','bc.id','c.budget_class_id')
            ->leftJoin('gfs_codes as gfs','gfs.id','c.gfs_code_id')
            ->leftJoin('fund_sources as fu','fu.id','gfs.fund_source_id')
            ->leftJoin('fund_sources as fu2','fu2.id','c.aggregate_fund_source_id')
            ->where('admc.financial_year_id', $financialYearId)
            ->where('admc.admin_hierarchy_id', $adminHierarchyId)
            ->where('admc.budget_type', $budgetType)
            ->where('sec.sector_id', $sectiorId)
            ->distinct('c.id')
            ->select('c.*','fu.name as fund','fu2.name as fund2','bc.name as budget_class')
            ->orderBy('c.name')
            ->get();

        $newCeilings =  DB::table('ceilings as c')
            ->join('ceiling_sectors as cs','cs.ceiling_id','c.id')
            ->join('budget_classes as bc','bc.id','c.budget_class_id')
            ->leftJoin('gfs_codes as gfs','gfs.id','c.gfs_code_id')
            ->leftJoin('fund_sources as fu','fu.id','gfs.fund_source_id')
            ->leftJoin('fund_sources as fu2','fu2.id','c.aggregate_fund_source_id')
            ->where('cs.sector_id', $sectiorId)
            ->distinct('c.id')
            ->select('c.*','fu.name as fund','fu2.name as fund2','bc.name as budget_class')
            ->orderBy('c.name')
            ->get();

        return ['ceilings'=>$ceilings, 'newCeilings'=>$newCeilings];
    }


}
