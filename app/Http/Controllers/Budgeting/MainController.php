<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;


class MainController extends Controller {
    public function __construct() {
    }

    public function index() {
        $title = "Budgeting|" . $this->app_initial;
        return view('budgeting.index', ['js_scripts' => $this->form_scripts, 'title' => $title]);
    }

    public function jsonBudgets() {
        return response()->json([
            'lga' => 'KALAMBO DC',
            'year' => '2018/2019'
        ]);
    }
}
