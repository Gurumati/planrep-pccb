<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Budgeting\ExpenditureCentreValue;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class ExpenditureCentreValueController extends Controller
{
    public function index() {
        $financial_year_id = FinancialYearServices::getPlanningFinancialYear()->id;
        $all = ExpenditureCentreValue::with('financial_year','expenditure_centre')
            ->where('is_active',true)
            ->where('financial_year_id',$financial_year_id)
            ->orderBy('created_at','desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return ExpenditureCentreValue::with('financial_year','expenditure_centre')->orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $financial_year_id = FinancialYearServices::getPlanningFinancialYear()->id;
        try{
            $data = json_decode($request->getContent());
            $obj = new ExpenditureCentreValue();
            $obj->min_value = $data->min_value;
            $obj->max_value = $data->max_value;
            $obj->financial_year_id = $financial_year_id;
            $obj->expenditure_centre_id = $data->expenditure_centre_id;
            $obj->is_active = true;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_EXPENDITURE_CENTRE_VALUE_CREATED", "expenditureCentreValues" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = ExpenditureCentreValue::find($data->id);
            $obj->min_value = $data->min_value;
            $obj->max_value = $data->max_value;
            $obj->expenditure_centre_id = $data->expenditure_centre_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_EXPENDITURE_CENTRE_VALUE_UPDATED", "expenditureCentreValues" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = ExpenditureCentreValue::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_EXPENDITURE_CENTRE_VALUE_DELETED", "expenditureCentreValues" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = ExpenditureCentreValue::with('financial_year','expenditure_centre')
            ->orderBy('created_at', 'desc')
            ->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        ExpenditureCentreValue::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "EXPENDITURE_CENTRE_VALUE_RESTORED", "trashedExpenditureCentreValues" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        ExpenditureCentreValue::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "EXPENDITURE_CENTRE_VALUE_DELETED_PERMANENTLY", "trashedExpenditureCentreValues" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = ExpenditureCentreValue::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EXPENDITURE_CENTRE_VALUE_DELETED_PERMANENTLY", "trashedExpenditureCentreValues" => $all];
        return response()->json($message, 200);
    }

    public function toggleActive(Request $request) {
        $data = json_decode($request->getContent());
        $object = ExpenditureCentreValue::find($data->id);
        $object->is_active = $data->is_active;
        $object->save();
        if ($data->is_active == false) {
            $feedback = ["action" => "EXPENDITURE_CENTRE_VALUE_DEACTIVATED", "alertType" => "warning"];
        } else {
            $feedback = ["action" => "EXPENDITURE_CENTRE_VALUE_ACTIVATED", "alertType" => "success"];
        }
        return response()->json($feedback, 200);
    }
}
