<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Models\Budgeting\Scrutiny;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Planning\MtefSection;
use App\Models\Planning\Mtef;
use App\Models\Budgeting\AdminHierarchyCeiling;
use App\Models\Planning\MtefSectionComment;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\Section;
use App\Http\Services\UserServices;
use Illuminate\Http\Request;
use App\Models\Auth\User;

class ScrutinyController extends Controller
{

    /**
     * Get Admin hierarchies which has mtef sections with decision level id equals to passed params bellow
     * Decision level is optained by using hierarchy level position of the passed $parentAdminHierarchyId and section level id of passed $parentSectionId (See DecisionLevel)
     *
     * @param $parentAdminHierarchyId
     * @param $parentSectionId
     * @return array (Objects){region, council, metfId}
     */
    public function getDistinctSubmittedHierarchy($parentAdminHierarchyId, $parentSectionId){

       return Scrutiny::getDistinctSubmittedHierarchy($parentAdminHierarchyId, $parentSectionId);
    }

    /**
     *
     * @param $parentAdminHierarchyId
     * @param $parentSectionId
     * @param $mtefId
     * @return array (Objects){sector,sectorId}
     */
    public function getDistinctSubmittedSectors($parentAdminHierarchyId, $parentSectionId,$mtefId){
        return ['sectors' => Scrutiny::getDistinctSubmittedSectors($parentAdminHierarchyId, $parentSectionId,$mtefId)];
    }

    /**
     * @param $parentAdminHierarchyId
     * @param $parentSectionId
     * @param $mtefId
     * @param $sectorId
     * sectorId replaced by departmentId
     * @return array (Objects){costCentre, costCentreId, mtefSectionId}
     */
    public function getDistinctSubmittedCostCentres($parentAdminHierarchyId, $parentSectionId,$mtefId,$sectorId){

        $costCentreNotBudgeting = ConfigurationSetting::getValueByKey('COST_CENTRE_NOT_BUDGETING');
        $allCostCentres = Section::where('sector_id',$sectorId)
            ->whereNotIn('id',$costCentreNotBudgeting)
            ->select('id','name')
            ->get();

        return ['costCentres' => Scrutiny::getDistinctSubmittedCostCentres($parentAdminHierarchyId, $parentSectionId,$mtefId, $sectorId),
                'users'=>User::sameLevelUsers(),'allCostCentre'=>$allCostCentres];
    }
    /**
     * @param $parentAdminHierarchyId
     * @param $parentSectionId
     * @param $mtefId
     * @param $sectorId
     * @return array (Objects){costCentre, costCentreId, mtefSectionId}
     */
    public function getDistinctSubmittedDepartments($parentAdminHierarchyId, $parentSectionId, $mtefId, $secId){
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
         $costCentreNotBudgeting = ConfigurationSetting::getValueByKey('COST_CENTRE_NOT_BUDGETING');

        $allDepts = DB::table('admin_hierarchy_sections as ads')
            ->join('sections as s','s.id','ads.section_id')
            ->join('section_levels as sl','sl.id','s.section_level_id')
            ->where('admin_hierarchy_id',$admin_hierarchy_id)
            ->where('sl.hierarchy_position',4)
            ->select('s.*')
            ->get();
        if(!$secId){
            $sectorId = $allDepts[0]->sector_id;
        }else {
            $sectorId = $secId;
        }
        return ['departments' => Scrutiny::getDistinctSubmittedDepartments($parentAdminHierarchyId, $parentSectionId,$mtefId, $sectorId),
                'users'=>User::sameLevelUsers(),'allDepartments'=>$allDepts ];
    }

    public function assignUserBySector($mtefId, $sectorId, $userId){
        try{
            MtefSection::where('mtef_id', $mtefId)
            ->where('decision_level_id',UserServices::getUser()->decision_level_id)
            ->whereHas('section', function($s) use($sectorId){
                $s->where('sector_id',$sectorId);
            })->update(['user_id' =>($userId == "0")?null: $userId]);
            return ['successMessage'=>'Successful assign user'];
        }
        catch(\Exception $e){
            Log::error($e);
            return response(['errorMessage'=>'Error Assign User','exception'=>$e],500);
        }
    }
    public function assignUserByCostCentre($mtefSectionId,  $userId){
        try{
            MtefSection::where('id', $mtefSectionId)->update(['user_id' =>($userId == "0")?null: $userId]);
            return ['successMessage'=>'Successful assign user'];
        }
        catch(\Exception $e){
            Log::error($e);
            return response(['errorMessage'=>'Error Assign User','exception'=>$e],500);
        }
    }

    public function moveByMtef(Request $request, $mtefId, $fromDecisionLevelId,$toDecisionLevelId){
        try{
            
            $data = $request->all();
            $mtef = Mtef::where('id',$mtefId)->first();
            $mtefSection =  MtefSection::where('mtef_id',$mtefId);
            $secIds = $mtefSection->pluck('section_id')->toArray();
            $sectionAtThisLevel = $mtefSection->where('decision_level_id',$fromDecisionLevelId)->count();
            $sectionWithBudget = $this->getSectionWithBudget($mtefId);
            if($sectionAtThisLevel === $sectionWithBudget){
                Scrutiny::moveByMtef($mtefId, $fromDecisionLevelId,$toDecisionLevelId);
                $mtefSectons = MtefSection::where('mtef_id',$mtefId)->get();
                foreach($mtefSectons as $ms){
                    MtefSectionComment::addComments($ms->id, $data, $fromDecisionLevelId, $toDecisionLevelId);
                }
            }else{
                return response(['errorMessage'=>'Make Sure All Departments Submitted Their Budget'],500); 
            }
            return ['successMessage'=>'BUDGETS_SUCCESSFULLY_MOVED'];
        }
        catch (\Exception $e){
            return response(['errorMessage'=>'ERROR_MOVING_BUDGETS'],500);
        }
    }

    public function getSectionWithBudget($mtefId){
        $sectionWithBudget = Mtef::join('mtef_sections as ms','ms.mtef_id','mtefs.id')
                                ->join('activities as a','a.mtef_section_id','ms.id')
                                ->join('activity_facilities as af','af.activity_id','a.id')
                                ->join('activity_facility_fund_sources as affs','affs.activity_facility_id','af.id')
                                ->join('activity_facility_fund_source_inputs as affsi','affsi.activity_facility_fund_source_id','affs.id')
                                ->distinct('ms.section_id')
                                ->where('mtefs.id',$mtefId)
                                ->where('affsi.unit_price','>',0)
                                ->where('a.code','<>','00000000')
                                ->count('ms.section_id');
        return $sectionWithBudget;

    }
    public function moveBySector(Request $request, $mtefId, $sectorId,$fromDecisionLevelId,$toDecisionLevelId){
        try{
            $data = $request->all();
            Scrutiny::moveBySector($mtefId,$sectorId, $fromDecisionLevelId,$toDecisionLevelId);
            $mtefSectons = MtefSection::where('mtef_id', $mtefId)->whereHas('section', function($s) use($sectorId){
                $s->where('sector_id',$sectorId);
            })->get();
            foreach ($mtefSectons as $ms) {
                MtefSectionComment::addComments($ms->id, $data, $fromDecisionLevelId, $toDecisionLevelId);
            }
            return ['successMessage'=>'BUDGETS_SUCCESSFULLY_MOVED'];
        }
        catch (\Exception $e){
            Log::error($e);
            return response(['errorMessage'=>'ERROR_MOVING_BUDGETS'],500);
        }
    }
    public function moveByDepartment(Request $request, $mtefId, $sectorId,$fromDecisionLevelId,$toDecisionLevelId){
        try{
            $data = $request->all();
            Scrutiny::getDistinctSubmittedDepartments($mtefId,$sectorId, $fromDecisionLevelId,$toDecisionLevelId);
            $mtefSectons = MtefSection::where('mtef_id', $mtefId)
                ->whereHas('section', function($s) use($sectorId){
                $s->where('sector_id',$sectorId);
            })->get();
            foreach ($mtefSectons as $ms) {
                MtefSectionComment::addComments($ms->id, $data, $fromDecisionLevelId, $toDecisionLevelId);
            }
            return ['successMessage'=>'BUDGETS_SUCCESSFULLY_MOVED'];
        }
        catch (\Exception $e){
            return response(['errorMessage'=>'ERROR_MOVING_BUDGETS'],500);
        }
    }

    public function moveByMtefSection(Request $request, $mtefSectionId, $fromDecisionLevelId,$toDecisionLevelId){
        try{
            $data = $request->all();
            Scrutiny::moveByMtefSection($mtefSectionId, $fromDecisionLevelId,$toDecisionLevelId);
            MtefSectionComment::addComments($mtefSectionId,$data,$fromDecisionLevelId, $toDecisionLevelId);
            return ['successMessage'=>'BUDGETS_SUCCESSFULLY_MOVED'];
        }
        catch (\Exception $e){
            return response(['errorMessage'=>'ERROR_MOVING_BUDGETS'],500);
        }
    }

    public function assignUser($mtefSectionId, $userId)
    {
        try {
            Scrutiny::assignUser($mtefSectionId, $userId);
            return response()->json(["successMessage" => "USER_ASSIGNED_SUCCESSFULL"], 200);
        } catch (QueryException $e) {
            return response()->json(["errorMessage" => "ERRO_ASSIGN_USER"], 400);
        }
    }
}
