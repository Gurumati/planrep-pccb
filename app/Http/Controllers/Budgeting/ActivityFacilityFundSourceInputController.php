<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Models\Planning\Activity;
use App\Models\Planning\ActivityFacilityFundSourceInput;
use App\Models\Planning\ActivityFacilityFundSourceInputBreakdown;
use App\Models\Planning\ActivityFacilityFundSourceInputForward;
use App\Models\Planning\MtefSection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Services\UserServices;
use App\Models\Setup\ActivityFacilityFundSourceInputPeriod;

class ActivityFacilityFundSourceInputController extends Controller
{

    /**
     * @param $activityFacilityFundSourceId
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate($activityFacilityFundSourceId){

        $perPage = Input::get('perPage',10);
        return ['activityFacilityFundSourceInputs'=>ActivityFacilityFundSourceInput::with(['input_comments'])->where('activity_facility_fund_source_id',$activityFacilityFundSourceId)->paginate($perPage),
                'totalBudget'=>DB::table('activity_facility_fund_source_inputs')->
                                    where('activity_facility_fund_source_id',$activityFacilityFundSourceId)->
                                    sum(DB::Raw('unit_price*frequency*quantity'))];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createOrUpdate(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, ActivityFacilityFundSourceInput::rules(),[]);

        /**
         * Validate if locked
         */
        $validator->after(function ($validator) use($data) {
            if(isset($data['activity_id']) && $data['activity_id'] !== null){
                if (Activity::where('id',$data['activity_id'])->where('locked',true)->count() > 0) {
                    $validator->errors()->add('id', 'This Activity is Locked');
                }
            }
            if($data['budget_type'] ==='CURRENT' && MtefSection::where('id',$data['mtef_section_id'])->where('is_locked',true)->count() > 0){
                $validator->errors()->add('id', 'This Plan is Locked');
            }
        });

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "ERROR_ON_CREATE_OR_UPDATE_INPUT"];
            return response()->json($message, 400);
        }

        try {
             DB::beginTransaction();

              $addedInputId = ActivityFacilityFundSourceInput::createOrUpdate($data)->id;
              if(isset($data['periods'])){
                  ActivityFacilityFundSourceInputPeriod::saveAll($addedInputId, $data['periods']);
              }

              DB::commit();

            $message = ["successMessage" => "SUCCESSFUL_SAVE_ACTIVITIES",'addedInput'=>ActivityFacilityFundSourceInput::find($addedInputId)];
            return response()->json($message);
        } catch (\Exception $exception) {
            Log::error($exception);
            DB::rollback();
            $message = ["errorMessage" => "ERROR_CREATING_INPUTS"];
            return response()->json($message, 500);
        }

    }

    /**
     * Delete Activity Facility Fund Source input
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id){
        try{
            DB::beginTransaction();
                ActivityFacilityFundSourceInputForward::where('activity_facility_fund_source_input_id',$id)->forceDelete();
                ActivityFacilityFundSourceInputBreakdown::where('activity_facility_fund_source_input_id',$id)->forceDelete();
                ActivityFacilityFundSourceInputPeriod::where('activity_facility_fund_source_input_id', $id)->forceDelete();
                $input = ActivityFacilityFundSourceInput::find($id);
                $input->forceDelete();
            DB::commit();
            track_activity($input, UserServices::getUser(), 'delete_inputs');
            $message = ["successMessage" => "SUCCESSFUL_DELETE_INPUT"];
            return response()->json($message);
        }
        catch (\Exception $e){
            Log::error($e);
            DB::rollback();
            $message = ["errorMessage" => "ERROR_CREATING_INPUTS",'exception'=>$e];
            return response()->json($message, 500);
        }
    }



}
