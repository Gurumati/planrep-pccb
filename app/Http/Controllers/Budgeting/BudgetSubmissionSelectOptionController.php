<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Budgeting\BudgetSubmissionSelectOption;
use Illuminate\Support\Facades\Log;

class BudgetSubmissionSelectOptionController extends Controller
{
    public function index()
    {
        $all = BudgetSubmissionSelectOption::whereNull('parent_id')->get();
        return response()->json($all);
    }

    public function getOptions($select_group)
    {
        $options = explode(',',$select_group);

        $all = BudgetSubmissionSelectOption::whereIn('parent_id',$options)
            ->where('admin_hierarchy_id',UserServices::getUser()->admin_hierarchy_id)
            ->get();
        return response()->json($all);
    }


}
