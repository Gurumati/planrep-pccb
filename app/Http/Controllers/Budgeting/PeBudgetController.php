<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Budgeting\PeBudget;
use App\Models\Planning\Mtef;
use App\Models\Setup\Section;
use Illuminate\Http\Request;

class PeBudgetController extends Controller
{
    private $limit = 10;

    public function index() {
        $all = PeBudget::with('section')->with('mtef')->get();
        return response()->json($all);
    }

    public function paginated() {
        $all = PeBudget::with('section')->with('mtef')->paginate($this->limit);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        $obj = new PeBudget();
        $obj->section_id = $data->section_id;
        $obj->mtef_id = $data->mtef_id;
        $obj->created_by = UserServices::getUser()->id;
        $obj->save();
        $all = PeBudget::with('section')->with('mtef')->paginate($this->limit);
        $message = ["successMessage" => "SUCCESSFUL_PE_BUDGET_CREATED", "peBudgets" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        $obj = PeBudget::find($data->id);
        $obj->section_id = $data->section_id;
        $obj->mtef_id = $data->mtef_id;
        $obj->updated_by = UserServices::getUser()->id;
        $obj->save();
        $all = PeBudget::with('section')->with('mtef')->paginate($this->limit);
        $message = ["successMessage" => "SUCCESSFUL_PE_BUDGET_UPDATED", "peBudgets" => $all];
        return response()->json($message, 200);
    }

    public function delete($id) {
        $object = PeBudget::find($id);
        $object->delete();
        $all = PeBudget::with('section')->with('mtef')->paginate($this->limit);
        $message = ["successMessage" => "SUCCESSFULLY_PE_BUDGET_DELETED", "peBudgets" => $all];
        return response()->json($message, 200);
    }

    public function allSections() {
        $all = Section::all();
        return response()->json($all);
    }

    public function allMtefs() {
        $all = Mtef::all();
        return response()->json($all);
    }
}
