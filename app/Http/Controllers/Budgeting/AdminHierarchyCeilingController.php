<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Http\Services\Budgeting\BudgetingServices;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Budgeting\AdminHierarchyCeiling;
use App\Models\Budgeting\BudgetType;
use App\Models\Budgeting\Ceiling;
use App\Models\Budgeting\CeilingSector;
use App\Models\Execution\AccountBalance;
use App\Models\Planning\Mtef;
use App\Models\Planning\MtefSection;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\AdminHierarchyLevel;
use App\Models\Setup\DecisionLevel;
use App\Models\Setup\Facility;
use App\Models\Setup\Section;
use App\Models\Setup\SectionLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Models\Setup\ConfigurationSetting;
use Exception;
use Illuminate\Support\Facades\Input;

class AdminHierarchyCeilingController extends Controller
{

    private $topMostSectionLevel;
    private $financialYear;

    public function index()
    {
        $all = AdminHierarchyCeiling::with('ceiling')->orderBy('id')->get();
        return response()->json($all);
    }

    /**
     * @param $budgetType
     * @param $financialYearId
     * @param $adminHierarchyId
     * @param $sectionId
     * @return array
     */
    public function paginate($budgetType, $financialYearId, $adminHierarchyId, $sectionId)
    {

        if (!UserServices::hasPermission(['budgeting.ceiling.' . $budgetType])) {
            return response()->json(null, 403);
        }

        $adminHierarchyId = 3;
        $adminHierarchyId = ($adminHierarchyId != null) ? $adminHierarchyId : UserServices::getUser()->admin_hierarchy_id;

        $sectionId = ($sectionId != null) ? $sectionId : UserServices::getUser()->section_id;

        
        $financialYearId = ($financialYearId != null) ? $financialYearId : FinancialYearServices::getPlanningFinancialYear()->id;

        $budgetType = ($budgetType != null) ? $budgetType : BudgetType::getDefault();

        return ['adminHierarchyCeilings' => AdminHierarchyCeiling::paginated($budgetType, $financialYearId, $adminHierarchyId, $sectionId), 'atStart' => AdminHierarchyCeiling::isAtStartCeilingChain($adminHierarchyId, $sectionId)];

    }

    public function getByCeiling($ceilingId){
        $similarCeilings = Ceiling::getSimilarCeiling($ceilingId);
        return ['admCeilings'=>AdminHierarchyCeiling::getByCeiling($ceilingId),'similar'=>$similarCeilings];
    }

    /**
     * Get Ceiling to initiate i.e ceiling which has not been added to admin hierarchy at a start section and budget type and financial year
     *
     * @param $budgetType
     * @param $adminHierarchyId
     * @param $sectionId
     * @param $financialYearId
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|null
     */
    public function paginateCeilingToInitiate($budgetType, $financialYearId, $adminHierarchyId, $sectionId)
    {

        return AdminHierarchyCeiling::paginateCeilingToInitiate($budgetType, $financialYearId, $adminHierarchyId, $sectionId);
    }

    /**
     * Initiate Admin hierarchy ceiling
     *
     * @param $ceilingId
     * @param $budgetType
     * @param $financialYearId
     * @param $adminHierarchyId
     * @param $sectionId
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function initiateAdminHierarchyCeiling($ceilingId, $budgetType, $financialYearId, $adminHierarchyId, $sectionId)
    {

        $exist = AdminHierarchyCeiling::where('ceiling_id', $ceilingId)->where('budget_type', $budgetType)->where('financial_year_id', $financialYearId)->where('admin_hierarchy_id', $adminHierarchyId)->where('section_id', $sectionId)->whereNull('facility_id');
        if ($exist->count() > 0) {
            return response()->json(['errorMessage' => 'CEILING_ALREADY_INITIATED'], 400);
        }

        try {
            AdminHierarchyCeiling::initiateAdminHierarchyCeiling($ceilingId, $budgetType, $financialYearId, $adminHierarchyId, $sectionId, null);
            return ['successMessage' => 'SUCCESSFULLY_INITIATE_CEILING'];
        } catch (\Exception $e) {
            return response()->json(['errorMessage' => 'FAILED_TO_INITIATE_CEILING'], 500);
        }
    }

    /**
     * Update admin hierarchy ceiling amount
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {

        $data = $request->all();
        $validator = Validator::make($data, AdminHierarchyCeiling::rules(), ['id' => ['required']]);

        /**
         * Validate if locked
         */
        $validator->after(function ($validator) use ($data) {
            if (AdminHierarchyCeiling::where('id', $data['id'])->where('is_locked', true)->first() != null) {
                $validator->errors()->add('id', 'This ceiling is Locked');
            }
        });

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "CANNOT_UPDATE_CEILING"];
            return response()->json($message, 400);
        }
        try {
            $admc = AdminHierarchyCeiling::find($data['id']);

            // Validate if amount is less than decemination amount
            $childSum = AdminHierarchyCeiling::firstLowerLevel($admc->ceiling_id, $admc->budget_type, $admc->financial_year_id, $admc->admin_hierarchy_id, $admc->section_id, true);
            if ($childSum > $data['amount']) {
                return response()->json(['errorMessage' => 'New Amount is less than total allocated amount of '+$childSum], 500);
            }
            $admc->update(['amount' => $data['amount'], 'updated_by' => UserServices::getUser()->id]);
            track_activity(AdminHierarchyCeiling::find($data['id']), UserServices::getUser(), 'update_ceiling_amount');
            $message = ["successMessage" => "SUCCESSFUL_CEILING_UPDATED"];
            return response()->json($message, 200);
        } catch (\Exception $e) {
            Log::error($e);
            $message = ["errorMessage" => "SOMETHING_WENT_WRONG"];
            return response()->json($message, 500);
        }
    }

    /**
     * @param $ceilingId
     * @param $budgetType
     * @param $financialYearId
     * @param $parentAdminHierarchyId
     * @param $parentSectionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function firstLowerLevel($ceilingId, $budgetType, $financialYearId, $parentAdminHierarchyId, $parentSectionId)
    {
        try {
            $all = AdminHierarchyCeiling::firstLowerLevel($ceilingId, $budgetType, $financialYearId, $parentAdminHierarchyId, $parentSectionId, false);
            return response()->json(['adminHierarchyCeilings' => $all]);
        } catch (\Exception $exception) {
            Log::error($exception);
            return response()->json(['errorMessage' => 'SOME_THING_WENT_WRONG'], 500);
        }
    }

    /**
     * @param Request $request
     * @param $adminHierarchyCeilingId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateLowerLevel(Request $request, $adminHierarchyCeilingId)
    {
        $data = json_decode($request->getContent());
        try {
            DB::beginTransaction();
            foreach ($data as $adminCeiling) :
                $admc = AdminHierarchyCeiling::with(['ceiling', 'ceiling.gfs_code'])->where('id', $adminCeiling->id)->first();
                if (isset($admc->facility_id)) {
                    $fundSourceId = (isset($admc->ceiling->gfs_code)) ? $admc->ceiling->gfs_code->fund_source_id : $admc->ceiling->aggregate_fund_source_id;
                    $mtefSectionId = DB::table('mtef_sections as ms')
                        ->join('mtefs as m', 'm.id', 'ms.mtef_id')
                        ->where('m.admin_hierarchy_id', $admc->admin_hierarchy_id)
                        ->where('m.financial_year_id', $admc->financial_year_id)
                        ->where('ms.section_id', $admc->section_id)
                        ->select('ms.*')
                        ->first()->id;
                    $amountSum = BudgetingServices::getTotalUsedByFacilityCeiling($mtefSectionId, [$admc->budget_type], $admc->ceiling->budget_class_id, $fundSourceId, $admc->facility_id);
                    $errorMessage = 'New Amount is less than total BUDGETED amount of';

                } else {
                    $amountSum = AdminHierarchyCeiling::firstLowerLevel($admc->ceiling_id, $admc->budget_type, $admc->financial_year_id, $admc->admin_hierarchy_id, $admc->section_id, true);
                    $errorMessage = 'New Amount is less than total ALLOCATED amount of';
                }
                if ($amountSum > $adminCeiling->amount) {
                    DB::rollback();
                    return response()->json(['errorMessage' => $errorMessage, 'amount' => $amountSum, 'sectionId' => $admc->section_id, 'facilityId' => $admc->facility_id], 500);
                }
                $admc->amount = $adminCeiling->amount;
                $admc->updated_by = UserServices::getUser()->id;
                $admc->save();
            track_activity($admc, UserServices::getUser(), 'save_allocation');
            endforeach;
            AdminHierarchyCeiling::updateStatusCodeIfExists($adminHierarchyCeilingId);
            DB::commit();
            $message = ["successMessage" => "SUCCESSFUL_ALLOCATE_CEILING"];
            return response()->json($message, 200);
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollback();
            $message = ["errorMessage" => "ERROR_UPDATING_CEILINGS"];
            return response()->json($message, 500);
        }
    }
    /**
     * @param $mtefSectionId
     * @param $budgetType
     * @param $budgetClassId
     * @param $fundSourceId
     * @param $isFacilityAccount
     * @param $facilityId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFundSourceCeiling($mtefSectionId, $budgetType, $budgetClassId, $fundSourceId, $isFacilityAccount, $facilityId,$financialYearId)
    {
        if (!in_array($budgetType, BudgetType::getAll())) {
            return response()->json(['errorMessage' => 'Invalid budget type'], 400);
        }

        $theCeiling = BudgetingServices::getTheCeiling($budgetClassId, $fundSourceId);
        $budgetTypes = ($budgetType == 'APPROVED' || $budgetType == 'DISAPPROVED' || $budgetType == 'PENDING') ? ['APPROVED', 'DISAPPROVED', 'CURRENT', 'PENDING'] : [$budgetType];

        $ceiling = BudgetingServices::getFacilityCeiling($mtefSectionId, $budgetTypes, $budgetClassId, $fundSourceId, $facilityId,$financialYearId);
        $totalUsed = BudgetingServices::getTotalUsedByFacilityCeiling($mtefSectionId, $budgetTypes, $budgetClassId, $fundSourceId, $facilityId);

        //Push previous year ceiling to facility
        $financialYearId = DB::table('mtefs as m')
            ->join('mtef_sections as ms', 'ms.mtef_id', 'm.id')
            ->where('ms.id', $mtefSectionId)
            ->select('m.financial_year_id')
            ->first()->financial_year_id;

        if ($financialYearId == 1 && $totalUsed > 0 && $ceiling == 0) {
            $mtef = DB::table('mtefs as m')->join('mtef_sections as ms', 'm.id', 'ms.mtef_id')->select('m.*', 'ms.section_id')->where('ms.id', $mtefSectionId)->first();
            AdminHierarchyCeiling::updateOrCreate([
                'ceiling_id' => $theCeiling->id,
                'admin_hierarchy_id' => $mtef->admin_hierarchy_id,
                'financial_year_id' => $mtef->financial_year_id,
                'section_id' => $mtef->section_id,
                'facility_id' => $facilityId,
                'budget_type' => $budgetType,
            ], ['amount' => $totalUsed, 'is_locked' => false, 'is_facility' => true, 'is_approved' => true]);
            
            $ceiling = BudgetingServices::getFacilityCeiling($mtefSectionId, $budgetTypes, $budgetClassId, $fundSourceId, $facilityId,$financialYearId);
        }

        $data = array_merge(
            ['totalCeiling' => $ceiling, 'totalUsed' => $totalUsed, 'ceilingBalance' => ($ceiling - $totalUsed)]
            // $this->getMedicalSuppliesAllocation($mtefSectionId, $budgetTypes, $budgetClassId, $fundSourceId, $facilityId, $totalUsed)
        );

        return response()->json($data, 200);
    }

    public function delete($id){
        try{
            $admc = AdminHierarchyCeiling::find($id);
            $c =Ceiling::find($admc->ceiling_id);
            $similar = DB::table('admin_hierarchy_ceilings as adc')
                ->join('ceilings as c','c.id','adc.ceiling_id')
                ->where('c.budget_class_id', $c->budget_class_id)
                ->where('adc.admin_hierarchy_id',$admc->admin_hierarchy_id)
                ->where('adc.section_id',$admc->section_id)
                ->where('adc.financial_year_id',$admc->financial_year_id)
                ->where('adc.budget_type',$admc->budget_type)
                ->where('adc.id','<>',$id);
            if($c->gfs_code_id != null){
                $similar->where('c.gfs_code_id', $c->gfs_code_id);
            }
            if($admc->facility_id != null){
                $similar->where('facility_id', $admc->facility_id);
            }
            if($similar->count() == 0 && $admc->amount > 0){
                return response()->json(['errorMessage'=>'Cannot delete this ceiling because it is not a duplicate and may be used'], 500);
            }
            AccountBalance::where('admin_hierarchy_ceiling_id', $id)->forceDelete();
            AdminHierarchyCeiling::find($id)->forceDelete();
            return ['successMessage'=>'Admin ceiling deleted'];
        }
        catch(Exception $e){
            Log::error($e);
            return response()->json(['errorMessage'=>'Error deleting Admin ceiling', 'exeption'=>$e], 500);
        }
    }

    public function getNextCeilingChain($adminHierarchyId, $sectionId)
    {

        $adminHierarchyId = ($adminHierarchyId != null) ? $adminHierarchyId : UserServices::getUser()->admin_hierarchy_id;

        $adminHierarchy = AdminHierarchy::find($adminHierarchyId);

        $adminHierarchyLevel = AdminHierarchyLevel::find($adminHierarchy->admin_hierarchy_level_id);

        $userSectionId = ($sectionId != null) ? $sectionId : UserServices::getUser()->section_id;

        $userSection = Section::find($userSectionId);

        $userSectionLevel = SectionLevel::find($userSection->section_level_id);

        $currentCeilingChain = DB::table('ceiling_chains')->where('for_admin_hierarchy_level_position', $adminHierarchyLevel->hierarchy_position)->where('admin_hierarchy_level_position', $adminHierarchyLevel->hierarchy_position)->where('section_level_position', $userSectionLevel->hierarchy_position)->first();
        if ($currentCeilingChain == null) {
            $message = ['nextCeilingChain' => null];
            return response()->json($message, 200);
        }
        $nextCeilingChain = DB::table('ceiling_chains')->where('id', $currentCeilingChain->next_id)->first();
        $message = ['nextCeilingChain' => $nextCeilingChain];
        return response()->json($message, 200);
    }

    public function insertLowerSections($ceilingId, $adminHierarchyId, $sectionId, $amount)
    {
        $adminHierarchy = AdminHierarchy::where('id', $adminHierarchyId)->first();
        $ceilingSectors = CeilingSector::where('ceiling_id', $ceilingId)->get();
        $sectors = [];

        foreach ($ceilingSectors as $ceilingSector) {
            $sectors[] = $ceilingSector->sector_id;
        }


        $count = $childSections = Section::where('parent_id', $sectionId)
            ->whereIn('sector_id', $sectors)
            ->count();


        if ($count > 0)
            $amount = $amount / $count;

        $childSections = Section::where('parent_id', $sectionId)
            ->whereIn('sector_id', $sectors)
            ->get();



        foreach ($childSections as $childSection) :
            $countMtef = Mtef::where('financial_year_id', $this->financialYear->id)
            ->where('admin_hierarchy_id', $adminHierarchyId)
            ->where('plan_type', 'CURRENT')
            ->count();
        if ($countMtef > 0) {

            $mtef = Mtef::where('financial_year_id', $this->financialYear->id)
                ->where('admin_hierarchy_id', $adminHierarchyId)
                ->where('plan_type', 'CURRENT')
                ->first();
            $mtefId = $mtef->id;
            $countMtefSections = MtefSection::where('mtef_id', $mtef->id)->where('section_id', $childSection->id)->count();
            if ($countMtefSections > 0) {
                $mtefSection = MtefSection::where('mtef_id', $mtef->id)->where('section_id', $childSection->id)->first();
                $mtef_section_id = $mtefSection->id;
            } else {

                $mtef_section_id = DB::table('mtef_sections')->insertGetId(
                    array(
                        "mtef_id" => $mtefId,
                        "section_id" => $childSection->id,
                        "section_level_id" => $this->topMostSectionLevel->id
                    )
                );
            }


        } else {
            $defaultDecisionLevel = DecisionLevel::where('admin_hierarchy_level_id', $adminHierarchy->admin_hierarchy_level_id)
                ->where('is_default', true)->first();

            $mtefId = DB::table('mtefs')->insertGetId(
                array(
                    "financial_year_id" => $this->financialYear->id,
                    "admin_hierarchy_id" => $adminHierarchy->id,
                    "locked" => false,
                    "decision_level_id" => $defaultDecisionLevel->id,
                    "plan_type" => "CURRENT",
                    "is_final" => false
                )
            );

            $mtef_section_id = DB::table('mtef_sections')->insertGetId(
                array(
                    "mtef_id" => $mtefId,
                    "section_id" => $childSection->id,
                    "section_level_id" => $childSection->section_level_id
                )
            );
        }

        DB::table('admin_hierarchy_ceilings')->insert(
            [
                'ceiling_id' => $ceilingId,
                'amount' => $amount,
                'created_at' => date('Y-m-d G:i:s'),
                'updated_at' => null,
                'deleted_at' => null,
                'created_by' => UserServices::getUser()->id,
                'updated_by' => null,
                'deleted_by' => null,
                'dissemination_status' => 0,
                'use_status' => 0,
                'mtef_section_id' => $mtef_section_id,
            ]
        );

        endforeach;

    }

    /**
     * @param $adminHierarchyCeilingId
     * @param $isLocked
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function toggleLocked($adminHierarchyCeilingId, $isLocked)
    {
        try {
            AdminHierarchyCeiling::toggleLocked($adminHierarchyCeilingId, $isLocked);
            $successMessage = ($isLocked == 0) ? 'CEILING_LOCKED_SUCCESSFULLY' : 'CEILING_UNLOCKED_SUCCESSFULLY';
            return ['successMessage' => $successMessage];
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['errorMessage' => 'ERROR_CHANGING_CEILING'], 500);
        }
    }

    /**
     * @description
     * Toggle all admin hierarchy ceilings locked or unlocked
     *
     * @param $isLocked {0,1}
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function toggleAllLocked()
    {
        try {

            $adminHierarchyId = Input::get('adminHierarchyId');
            $financialYearId = Input::get('financialYearId');
            $budgetType = Input::get('budgetType');
            $ceilingId = Input::get('ceilingId');
            $status = Input::get('status');

            $query = DB::table('admin_hierarchy_ceilings')->
              where('admin_hierarchy_id',$adminHierarchyId)->
              where('financial_year_id',$financialYearId)->
              where('budget_type',$budgetType);

            if ($ceilingId != null) {
              $query = $query->where('ceiling_id', $ceilingId);
            }
            $update = ['is_locked'=> ($status == 0) ? false : true];
            if($status == 0 && $budgetType != 'APPROVED') {
               $update['is_approved'] = false;
            }
            $query->update($update);

            $successMessage = ($status == 1) ? 'CEILING_LOCKED_SUCCESSFULLY' : 'CEILING_UNLOCKED_SUCCESSFULLY';
            return ['successMessage' => $successMessage];
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['errorMessage' => 'ERROR_CHANGING_CEILING'], 500);
        }
    }

    public function approveByAdminHierarchy(Request $request)
    {
        set_time_limit(10000);
        $financialYearId = $request->financialYearId;
        $adminHierarchyId = $request->adminHierarchyId;
        $budget_type = $request->budget_type;
        try {
            DB::beginTransaction();
                AdminHierarchyCeiling::approveByAdminHierarchy($financialYearId, $adminHierarchyId, $budget_type);
            DB::commit();
            return response()->json(['successMessage' => 'Ceiling has been approved'], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(['errorMessage' => 'Error occured while approving ceiling'], 500);
            //return $e;
        }
    }

    public function disApproveByAdminHierarchy(Request $request)
    {
        $financialYearId = $request->financialYearId;
        $adminHierarchyId = $request->adminHierarchyId;
        $budget_type = $request->budget_type;
        try {
            AdminHierarchyCeiling::disApproveByAdminHierarchy($financialYearId, $adminHierarchyId, $budget_type);
            return response()->json(['successMessage' => 'Ceiling has been disapproved'], 200);
        } catch (Exception $e) {
            return response()->json(['errorMessage' => 'Error occured while disapproving ceiling'], 500);
        }
    }

    public function updateDisseminationStatus($adminArea, $section, $ceiling, $status)
    {
        $mtef = Mtef::where('financial_year_id', $this->financialYear->id)
            ->where('admin_hierarchy_id', $adminArea)
            ->where('plan_type', 'CURRENT')
            ->first();
        $mtefId = $mtef->id;
        $countMtefSections = MtefSection::where('mtef_id', $mtef->id)->where('section_id', $section)->count();
        if ($countMtefSections > 0) {
            $mtefSection = MtefSection::where('mtef_id', $mtef->id)->where('section_id', $section)->first();
            $mtef_section_id = $mtefSection->id;
            $obj = AdminHierarchyCeiling::where('mtef_section_id', $mtef_section_id)->where('ceiling_id', $ceiling)->first();
            $obj->dissemination_status = $status;
            $obj->save();
        }
    }

    public function pushToFacility()
    {
        set_time_limit(10000);

        // 1. Find all Hq facilities
        // 2. Find all ceilings
        // 3. Find budget and ceiling
        // 4. If has budget insert ceiling

        $hqFacilityType = ConfigurationSetting::where('key', 'HQ_FACILITY_TYPE')->first();
        $facilityType = DB::table('facility_types')->where('id', (int)$hqFacilityType->value)->first();

        // $fCeiiling = AdminHierarchyCeiling::where('is_facility',true)
        //             ->distinct('ceiling_id')
        //             ->pluck('ceiling_id')
        //             ->toArray();

        $cCeiling = DB::table('admin_hierarchy_ceilings as adc')
            ->join('sections as sec', 'sec.id', 'adc.section_id')
            ->join('section_levels as sl', 'sl.id', 'sec.section_level_id')
            ->join('ceilings as c', 'c.id', 'adc.ceiling_id')
            ->leftJoin('gfs_codes as gfs', 'gfs.id', 'c.gfs_code_id')
            ->where('sl.hierarchy_position', 4)
            ->where('adc.amount', '>', 0)
            ->whereNull('adc.facility_id')
            ->where('adc.financial_year_id', 1)
            ->select('adc.*', 'c.budget_class_id', 'gfs.fund_source_id', 'c.aggregate_fund_source_id')
            ->limit(1000)
            ->get();

        foreach ($cCeiling as $c) {
            $hq = Facility::where('admin_hierarchy_id', $c->admin_hierarchy_id)->where('facility_type_id', $facilityType->id)->where('is_active', true)
                ->first();
            $mtefSectionId = DB::table('mtef_sections as ms')
                ->join('mtefs as m', 'm.id', 'ms.mtef_id')
                ->where('m.admin_hierarchy_id', $c->admin_hierarchy_id)
                ->where('m.financial_year_id', $c->financial_year_id)
                ->where('ms.section_id', $c->section_id)
                ->select('ms.id as mtef_section_id')
                ->first()->mtef_section_id;
            $fundSourceId = isset($c->fund_source_id) ? $c->fund_source_id : $c->aggregate_fund_source_id;

            $budget = BudgetingServices::getTotalUsedByFacilityCeiling($mtefSectionId, [$c->budget_type], $c->budget_class_id, $fundSourceId, $hq->id);

            $exist = AdminHierarchyCeiling::where('ceiling_id', $c->ceiling_id)
                ->where('admin_hierarchy_id', $c->admin_hierarchy_id)
                ->where('section_id', $c->section_id)
                ->where('financial_year_id', $c->financial_year_id)
                ->where('facility_id', $hq->id)
                ->where('budget_type', $c->budget_type)
                ->count();

            if ($exist == 0 && $budget > 0) {
                AdminHierarchyCeiling::create([
                    'ceiling_id' => $c->ceiling_id,
                    'amount' => $budget,
                    'dissemination_status' => 0,
                    'use_status' => 0,
                    'admin_hierarchy_id' => $c->admin_hierarchy_id,
                    'financial_year_id' => $c->financial_year_id,
                    'section_id' => $c->section_id,
                    'facility_id' => $hq->id,
                    'is_approved' => true,
                    'budget_type' => $c->budget_type
                ]);
            }

        }
        return sizeof($cCeiling);
    }

    public function importCarryOverCeiling($financialYearId, $adminHierarchyId, $sectionId, $facilityId, $fundSourceId, $budgetClassId, $amount){
      return  AdminHierarchyCeiling::importCarryOverCeiling($financialYearId, $adminHierarchyId, $sectionId, $facilityId, $fundSourceId, $budgetClassId, $amount);
    }

    public function getAdminCeiling($financialYearId, $budgetType, $adminHierarchyId, $sectiorId, $ceilingId){

        $adminCeilings =  DB::table('ceilings as c')
            ->join('admin_hierarchy_ceilings as admc','admc.ceiling_id','c.id')
            ->join('sections as sec','sec.id','admc.section_id')
            ->join('budget_classes as bc','bc.id','c.budget_class_id')
            ->where('admc.financial_year_id', $financialYearId)
            ->where('admc.admin_hierarchy_id', $adminHierarchyId)
            ->where('admc.budget_type', $budgetType)
            ->where('sec.sector_id', $sectiorId)
            ->where('c.id', $ceilingId)
            ->select('admc.*','c.name as ceiling','bc.name as budget_class')
            ->paginate(30);

        return ['adminCeilings'=>$adminCeilings];
    }

    public function moveAdminCeiling($financialYearId, $budgetType, $adminHierarchyId, $sectiorId, $ceilingId, $newCeilingId){

        $isValid = DB::table('ceilings as c')
               ->join('ceiling_sectors as cs','cs.ceiling_id','c.id')
               ->where('c.id', $newCeilingId)
               ->where('cs.sector_id', $sectiorId)
               ->count('c.id');
        if($isValid == 0){
            return response()->json(['errorMessage'=>'The destination ceiling selected not in the selected sector'], 500);
        }
        $query =  DB::table('ceilings as c')
            ->join('admin_hierarchy_ceilings as admc','admc.ceiling_id','c.id')
            ->join('sections as sec','sec.id','admc.section_id')
            ->join('budget_classes as bc','bc.id','c.budget_class_id')
            ->where('admc.financial_year_id', $financialYearId)
            ->where('admc.admin_hierarchy_id', $adminHierarchyId)
            ->where('admc.budget_type', $budgetType);

        $ids= $query->where('sec.sector_id', $sectiorId)
            ->where('c.id', $ceilingId)
            ->pluck('admc.id')
            ->toArray();
        AdminHierarchyCeiling::whereIn('id', $ids)->update(['ceiling_id'=>$newCeilingId]);
        $topLevel =  DB::table('admin_hierarchy_ceilings as admc')
                    ->where('admc.ceiling_id',$newCeilingId)
                    ->where('admc.financial_year_id', $financialYearId)
                    ->where('admc.admin_hierarchy_id', $adminHierarchyId)
                    ->where('admc.budget_type', $budgetType)
                    ->where('admc.section_id', 1)
                    ->count('admc.id');

        if($topLevel == 0){
            $childSum = AdminHierarchyCeiling::firstLowerLevel($newCeilingId, $budgetType, $financialYearId, $adminHierarchyId, 1 , true);
            AdminHierarchyCeiling::create([
                'ceiling_id' => $newCeilingId,
                'amount' => $childSum,
                'admin_hierarchy_id'=>$adminHierarchyId,
                'financial_year_id'=>$financialYearId,
                'section_id'=>1,
                'budget_type'=>$budgetType,
                'is_facility'=>false,
                'is_locked'=>false,
                'is_approved'=>false,
            ]);
        }

        return ['successMessage'=>'Successfully moved admin hierarchy ceilings'];
    }

}
