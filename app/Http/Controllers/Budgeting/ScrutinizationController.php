<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\NotificationService;
use App\Http\Services\UserServices;
use App\Models\Auth\User;
use App\Models\Budgeting\Scrutinization;
use App\Models\Budgeting\ScrutinizationRound;
use App\Models\Planning\MtefSectionComment;
use App\Models\Planning\MtefSectionItemComment;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\DecisionLevel;
use App\Models\Setup\Section;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Input;


class ScrutinizationController extends Controller
{

    public function forwardToSection(Request $request)
    {
        try {
            $movement = json_decode($request->getContent());
            DB::transaction(function () use ($movement) {

                $round = DB::table('scrutinization_rounds')->where('mtef_section_id', $movement->mtef_section_id)->where('status', 3)->count();
                $round = $round + 1;
                $scrutinRoundId = $this->createScrutinRound($movement, $round);

                $scrutind = $this->createScrutin($scrutinRoundId, $movement);
                $this->moveMtefSection(
                    $scrutind,
                    $movement->mtef_section_id,
                    $movement->to_decision_level_id,
                    $movement->from_decision_level_id,
                    $movement->forward_message,
                    $movement->forward_message,
                    $movement->direction,
                    true
                );

            });

            $toDecisionLevel = DecisionLevel::find($movement->to_decision_level_id);
            //Added to handle carryover and reallocation notification since they don't scrutinized
            if($movement->from_decision_level_id == 1 && $movement->to_decision_level_id == 1){
                $message = ["successMessage" => "SUCCESSFUL_SENT_NOTIFICATION", "decisionLevel" => $toDecisionLevel];
            }else{
                $message = ["successMessage" => "SUCCESSFUL_FORWARD_BUDGET", "decisionLevel" => $toDecisionLevel];
            }
            return response()->json($message);
        } catch (QueryException $e) {
            $message = ["errorMessage" => "ERROR_FORWARD_BUDGET", "exception" => $e];
            return response()->json($message, 400);
        }
    }
    private function createScrutinRound($movement, $round)
    {
        $Id = DB::table('scrutinization_rounds')->insertGetId(array(
            "round" => $round,
            "status" => 1,
            "start_decision_level_id" => $movement->from_decision_level_id,
            "mtef_section_id" => $movement->mtef_section_id
        ));
        return $Id;
    }
    private function createScrutin($scrutinRoundId, $movement)
    {

        $flatten = new Flatten();

        $toDecisionLevel = DecisionLevel::find($movement->to_decision_level_id);
        $mtefSection = DB::table('mtef_sections as ms')->join('mtefs as m', 'm.id', 'ms.mtef_id')->where('ms.id', $movement->mtef_section_id)->select('ms.*', 'm.admin_hierarchy_id')->first();

        //Added to handle carryover and reallocation notification since they don't scrutinized
        if($movement->from_decision_level_id == 1 && $movement->to_decision_level_id == 1){
            $nextAdminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        }else{
            $userParentAdminHierarchies = AdminHierarchy::with('parentArray', 'parentArray.parentArray', 'parentArray.parentArray.parentArray')->where('id', $mtefSection->admin_hierarchy_id)->get();
            $nextAdminHierarchyId = $flatten->getFirstParentByPosition($userParentAdminHierarchies, $toDecisionLevel->admin_hierarchy_level_position);
        }

        $scrutinId = DB::table('scrutinizations')->insertgetId(array(
            "admin_hierarchy_id" => $nextAdminHierarchyId,
            "section_id" => $mtefSection->section_id,
            "decision_level_id" => $movement->to_decision_level_id,
            "scrutinization_round_id" => $scrutinRoundId,
            "status" => 0,
            "created_at" => date('Y-m-d h:i:sa')
        ));
        return $scrutinId;

    }

    public function moveMtefSection($scrutind, $mtefSectionId, $toDecisionLevelId, $fromDecisionLevelId, $forwardMessage, $comments, $direction, $is_locked)
    {

        DB::transaction(function () use ($scrutind, $mtefSectionId, $toDecisionLevelId, $fromDecisionLevelId, $comments, $direction, $forwardMessage, $is_locked) {


            DB::table('mtef_sections')->where('id', $mtefSectionId)->update(array(
                'decision_level_id' => $toDecisionLevelId,
                'is_locked' => $is_locked
            ));
            $decisionLevels = DecisionLevel::find($toDecisionLevelId);

            $mtefSectionComment = MtefSectionComment::where('mtef_section_id', $mtefSectionId)->where('scrutinization_id', $scrutind)->first();
            if ($mtefSectionComment == null) {
                $mtefSectionComment = new MtefSectionComment();
                $mtefSectionComment->decision_level = $decisionLevels->name;
            }
            $mtefSectionComment->mtef_section_id = $mtefSectionId;
            $mtefSectionComment->scrutinization_id = $scrutind;
            
            //Added to handle carryover and reallocation notification since they don't scrutinized
            if($fromDecisionLevelId == 1 && $toDecisionLevelId == 1){
                $mtefSectionComment->from_decision_level_id = 6;
            }else{
                $mtefSectionComment->from_decision_level_id = $fromDecisionLevelId;
            }
            
            $mtefSectionComment->to_decision_level_id = $toDecisionLevelId;

            if ($comments != null) {
                $mtefSectionComment->comments = $comments;
            }
            if ($forwardMessage != null) {
                $mtefSectionComment->forward_message = $forwardMessage;
            }
            $mtefSectionComment->direction = $direction;
            $mtefSectionComment->save();

        });
    }

    public function returnMtefSection(Request $request)
    {
        try {
            $movement = json_decode($request->getContent());
            $this->_returnMtefSection($movement);
            $message = ["successMessage" => "SUCCESSFUL_RETURN_BUDGET"];
            return response()->json($message);
        } catch (QueryException $e) {
            $message = ["errorMessage" => "ERROR_RETURN_BUDGET", "exception" => $e];
            return response()->json($message, 400);
        }
    }

    public function clearScrutin($id)
    {
        try {
            DB::transaction(function () use ($id) {
                DB::table('scrutinization_comments')->where('scrutinization_id', $id)->delete();
                DB::table('scrutinizations')->where('id', $id)->delete();
            });
            return response()->json(['SUCCESS_MESSAGE' => 'SUCCESS_DELETE_SCRUTIN']);
        } catch (\Exception $exception) {
            return response()->json(['ERROR_MESSAGE' => 'ERROR_DELETE_SCRUTIN'], 500);
        }
    }

    private function _returnMtefSection($movement)
    {

        DB::transaction(function () use ($movement) {

            $is_locked = true;
            $initiator = DB::table('scrutinization_rounds as scr')->join('scrutinizations as sc', 'scr.id', 'sc.scrutinization_round_id')->where('sc.id', $movement->scrutinId)->first();
            $scrutin = Scrutinization::find($movement->scrutinId);
            $scrutin->status = 3;
            $scrutin->save();
            $scrutinRound = ScrutinizationRound::find($scrutin->scrutinization_round_id);

            if ($initiator->start_decision_level_id == $movement->to_decision_level_id) {
                $is_locked = false;
                $scrutinRound->status = 3;
                $scrutinRound->save();
                $scrutinId = $scrutin->id;
            } else {
                $is_locked = true;
                $scrutinId = $this->createScrutin($scrutinRound->id, $movement);

            }
            $this->moveMtefSection(
                $scrutinId,
                $movement->mtef_section_id,
                $movement->to_decision_level_id,
                $movement->from_decision_level_id,
                null,
                $movement->comments,
                $movement->direction,
                $is_locked
            );

            //Close Scrutin
            $Ids = DB::table('scrutinizations as s')->join('scrutinization_rounds as so', 'so.id', 's.scrutinization_round_id')->where('so.mtef_section_id', $scrutinRound->mtef_section_id)->where('s.admin_hierarchy_id', $scrutin->admin_hierarchy_id)->where('s.section_id', $scrutin->section_id)->where('s.decision_level_id', $scrutin->decision_level_id)->pluck('s.id');

            DB::table('scrutinizations')->whereIn('id', $Ids)->update(array(
                'status' => 3
            ));

            $userSpecs = DB::table('mtef_sections as ms')->join('mtefs as m', 'm.id', 'ms.mtef_id')->where('ms.id', $scrutinRound->mtef_section_id)->select('ms.section_id', 'm.admin_hierarchy_id')->first();
            if (isset($userSpecs)) {
                $message = "Cost centre budget has been returned";
                $users = User::where('admin_hierarchy_id', $userSpecs->admin_hierarchy_id)->where('section_id', $userSpecs->section_id)->get();
                NotificationService::notify($users, $message);

            }
        });
    }

    public function getSubmittedAdminCountByUser()
    {
        $canAssignUser = UserServices::hasPermission(['budgeting.scrutinization.assign_users']);
        $userId = UserServices::getUser()->id;
        $sectionId = UserServices::getUser()->section_id;
        $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $decision_level_id = UserServices::getUser()->decision_level_id;
        $sections = Section::where('id', $sectionId)->get();
        $flatten = new Flatten();
        $sectionIds = $flatten->flattenSectionWithChild($sections);

        return Scrutinization::getSubmittedAdminCountByUser($adminHierarchyId, $decision_level_id, $sectionIds, $canAssignUser, $userId);
    }

    public function getSubmittedDptCountByUser()
    {
        $canAssignUser = UserServices::hasPermission(['budgeting.scrutinization.assign_users']);
        $userId = UserServices::getUser()->id;
        $sectionId = UserServices::getUser()->section_id;
        $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $decision_level_id = UserServices::getUser()->decision_level_id;
        $sections = Section::where('id', $sectionId)->get();
        $flatten = new Flatten();
        $sectionIds = $flatten->flattenSectionWithChild($sections);

        return Scrutinization::getSubmittedDeptCountByUser($adminHierarchyId, $decision_level_id, $sectionIds, $canAssignUser, $userId);
    }

    public function getSubmittedCostCentreCountByUser()
    {
        $canAssignUser = UserServices::hasPermission(['budgeting.scrutinization.assign_users']);
        $userId = UserServices::getUser()->id;
        $sectionId = UserServices::getUser()->section_id;
        $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $decision_level_id = UserServices::getUser()->decision_level_id;
        $sections = Section::where('id', $sectionId)->get();
        $flatten = new Flatten();
        $sectionIds = $flatten->flattenSectionWithChild($sections);

        return Scrutinization::getSubmittedCostCentreCountByUser($adminHierarchyId, $decision_level_id, $sectionIds, $canAssignUser, $userId);
    }

    public function getP1Admin()
    {

        $canAssignUser = UserServices::hasPermission(['budgeting.scrutinization.assign_users']);
        $userId = UserServices::getUser()->id;
        $sectionId = UserServices::getUser()->section_id;
        $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $decision_level_id = UserServices::getUser()->decision_level_id;
        $sections = Section::where('id', $sectionId)->get();
        $flatten = new Flatten();
        $sectionIds = $flatten->flattenSectionWithChild($sections);
        $sectionIdString = implode(',', $sectionIds);
        $p1Admin = Scrutinization::getP1Admin($adminHierarchyId, $decision_level_id, $sectionIds, $canAssignUser, $userId);
        foreach ($p1Admin as &$admin) {
            $p1AdminIdString = " and parent1_id =" . $admin->id . " ";
            $admin->status = Scrutinization::getStatus($adminHierarchyId, $decision_level_id, $sectionIdString, $canAssignUser, $userId, $p1AdminIdString);
        }
        return response()->json($p1Admin);
    }

    public function getAdmin()
    {

        $canAssignUser = UserServices::hasPermission(['budgeting.scrutinization.assign_users']);
        $userId = UserServices::getUser()->id;
        $sectionId = UserServices::getUser()->section_id;
        $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $decision_level_id = UserServices::getUser()->decision_level_id;
        $sections = Section::where('id', $sectionId)->get();
        $flatten = new Flatten();
        $sectionIds = $flatten->flattenSectionWithChild($sections);
        $sectionIdString = implode(',', $sectionIds);

        $p1AdminId = Input::get('p1', '0');
        $p1AdminStringId = " and parent1_id =" . $p1AdminId . " ";

        $admins = Scrutinization::getAdmin($adminHierarchyId, $decision_level_id, $sectionIds, $canAssignUser, $userId, $p1AdminId);

        foreach ($admins as &$admin) {
            $adminString = $p1AdminStringId . " and admin_id=" . $admin->id . " ";
            $admin->status = Scrutinization::getStatus($adminHierarchyId, $decision_level_id, $sectionIdString, $canAssignUser, $userId, $adminString);
        }
        return response()->json($admins);
    }

    public function getSectors()
    {

        $canAssignUser = UserServices::hasPermission(['budgeting.scrutinization.assign_users']);
        $userId = UserServices::getUser()->id;
        $sectionId = UserServices::getUser()->section_id;
        $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $decision_level_id = UserServices::getUser()->decision_level_id;
        $sections = Section::where('id', $sectionId)->get();

        $flatten = new Flatten();
        $sectionIds = $flatten->flattenSectionWithChild($sections);
        $sectionIdString = implode(',', $sectionIds);

        $p1AdminId = Input::get('p1', '0');
        $p1AdminStringIds = " and parent1_id =" . $p1AdminId . " ";

        $adminStringIds = Input::get('admin', '0');
        $adminIds = ($adminStringIds !== "") ? explode(',', $adminStringIds) : [];
        $adminStringIds = ($adminStringIds !== "") ? $adminStringIds : "0";
        $adminStringIds = " and admin_id IN(" . $adminStringIds . ") ";

        $sectors = Scrutinization::getSectors($adminHierarchyId, $decision_level_id, $sectionIds, $canAssignUser, $userId, $p1AdminId, $adminIds);
        foreach ($sectors as &$sector) {
            $sectString = $p1AdminStringIds . $adminStringIds . " and sector_id=" . $sector->id . " ";
            $sector->status = Scrutinization::getStatus($adminHierarchyId, $decision_level_id, $sectionIdString, $canAssignUser, $userId, $sectString);
        }
        return response()->json($sectors);
    }

    public function getScrutins()
    {

        $canAssignUser = UserServices::hasPermission(['budgeting.scrutinization.assign_users']);

        $userId = UserServices::getUser()->id;
        $sectionId = UserServices::getUser()->section_id;
        $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $decision_level_id = UserServices::getUser()->decision_level_id;
        $sections = Section::where('id', $sectionId)->get();

        $flatten = new Flatten();
        $sectionIds = $flatten->flattenSectionWithChild($sections);

        $p1AdminId = Input::get('p1', '0');

        $adminStringIds = Input::get('admin', '0');
        $adminIds = ($adminStringIds !== "") ? explode(',', $adminStringIds) : [];

        $sectorIds = Input::get('sector', '0');
        $sectorIds = ($sectorIds !== "") ? explode(',', $sectorIds) : [];

        $statusIds = Input::get('status', '0');
        $statusIds = ($statusIds !== "") ? explode(',', $statusIds) : [];

        $isAssigned = Input::get('isAssigned', '0');
        $isAssigned = ($isAssigned !== "") ? explode(',', $isAssigned) : [];

        $scutins = Scrutinization::getScrutins($adminHierarchyId, $decision_level_id, $sectionIds, $canAssignUser, $userId, $p1AdminId, $adminIds, $sectorIds, $statusIds, $isAssigned);

        return $scutins;
    }

    public function getUserExpectedScrutins()
    {
        return response()->json(Flatten::userBudgetingSections());
    }

    public function getUserScrutinizations()
    {
        $user = User::find(UserServices::getUser()->id);
        $users = $this->sameLevelusers($user);
        $userAdmin = AdminHierarchy::find($user->admin_hierarchy_id);
        $userAdminLevel = DB::table('admin_hierarchy_levels')->where('id', $userAdmin->admin_hierarchy_level_id)->first();
        $userIsTopSection = false;
        $useSection = DB::table('sections')->where('id', UserServices::getUser()->section_id)->first();
        $userSectionLevel = DB::table('section_levels')->where('id', $useSection->section_level_id)->first();
        if (isset($useSection) && $useSection->parent_id == null) {
            $userIsTopSection = true;
        }

        $data = [
            'users' => $users,
            'meId' => $user->id,
            'userHierarchyPosition' => $userAdminLevel->hierarchy_position,
            'nextDecisionLevels' => $this->getUserNextDecisionLevels(),
            'userHierarchies' => $this->getMySupervisedAdminHierarchies(),
            'userSections' => $this->getMySupervisedSections(),
            'userIsTopSection' => $userIsTopSection,
            'userSectionLevel' => $userSectionLevel,
            'submitted' => $this->getSubmitted($userAdminLevel->hierarchy_position, $userSectionLevel->hierarchy_position)

        ];
        return response()->json($data);

    }
    public function getSubmitted($adminHierarchyPosition, $sectionLevelPosition)
    {
        if ($adminHierarchyPosition < 4) {
            return $this->getSubmittedAdminCountByUser();
        } else if ($adminHierarchyPosition === 4 && $sectionLevelPosition === 1) {
            return $this->getSubmittedDptCountByUser();
        } else {
            return $this->getSubmittedCostCentreCountByUser();
        }
    }

    private function sameLevelusers($user)
    {

        $sections = Section::where('id', $user->section_id)->get();
        $flatten = new Flatten();
        $ids = $flatten->flattenSectionWithChild($sections);

        $users = DB::table('users as u')->join('sections as s', 's.id', 'u.section_id')->leftJoin('sectors as sec', 'sec.id', 's.sector_id')->where('u.admin_hierarchy_id', $user->admin_hierarchy_id)->where('u.decision_level_id', $user->decision_level_id)->whereIn('u.section_id', $ids)->select('u.id', 'u.first_name', 'u.last_name', 'sec.name as sector')->get();

        return $users;
    }

    public function getUserNextDecisionLevels()
    {
        $decisionLevels = [];
        try {
            $nextDecisionLevels = DB::table('decision_level_next_decisions')->where('decision_level_id', UserServices::getUser()->decision_level_id)->get();
            $Ids = [];
            foreach ($nextDecisionLevels as $next) {
                $Ids[] = $next->next_level_id;
            }
            $decisionLevels = DecisionLevel::whereIn('id', $Ids)->get();
            return $decisionLevels;
        } catch (QueryException $e) {
            return $decisionLevels;
        }

    }

    public function getMySupervisedAdminHierarchies()
    {
        $flatten = new Flatten();
        $adminTree = $flatten->getUserHierarchyTree();
        $name = $flatten->flattenUserAdminHierarchiesGetNames($adminTree);

        return $name;
    }

    public function getMySupervisedSections()
    {
        $flatten = new Flatten();
        $userSection = Section::where('id', UserServices::getUser()->section_id)->get();
        $sectionName = $flatten->flattenUserSectionGetNames($userSection);

        return $sectionName;
    }

    public function getScrutinizationById($id)
    {
        return response()->json(Scrutinization::getById($id), 200);
    }

    public function forwardAllByUser(Request $request)
    {
        $data = json_decode($request->getContent());
        DB::transaction(function () use ($data) {
            $scutins = $data->scrutinizations;
            $userDecisionLevelId = UserServices::getUser()->decision_level_id;
            foreach ($scutins as $scrutin) {
                $movement = new \stdClass();
                $movement->from_decision_level_id = $userDecisionLevelId;
                $movement->to_decision_level_id = $data->to_decision_level_id;
                $movement->mtef_section_id = $scrutin->mtef_section_id;
                $scrutinId = $this->createScrutin($scrutin->scrutinization_round_id, $movement);
                $this->moveMtefSection($scrutinId, $scrutin->mtef_section_id, $data->to_decision_level_id, $userDecisionLevelId, $data->comments, null, $data->direction, true);
                $this->saveStatus($scrutin->id, 3);
            }
        });
        $data = ["successMessage" => "FORWARD_SUCCESSFUL"];
        return response()->json($data, 200);


    }

    private function saveStatus($scutinId, $status)
    {
        DB::table('scrutinizations')->where('id', $scutinId)->update(array(
            'status' => $status
        ));
    }

    public function getReturnDecisionLevels($scrutinId, $mtefSectionId)
    {
        try {
            $Ids = [];
            $sender = DB::table('mtef_section_comments as msc')
                ->where('scrutinization_id', $scrutinId)->where('mtef_section_id', $mtefSectionId)->first();

            $Ids[] = $sender->from_decision_level_id;
            $initiator = DB::table('scrutinization_rounds as scr')->join('scrutinizations as sc', 'scr.id', 'sc.scrutinization_round_id')->where('sc.id', $scrutinId)->where('mtef_section_id', $mtefSectionId)->first();

            if ($initiator->start_decision_level_id != $sender->from_decision_level_id) {
                $Ids[] = $initiator->start_decision_level_id;
            }

            $decisionLevels = DB::table('decision_levels')->whereIn('id', $Ids)->get();
            $data = ['decisionLevels' => $decisionLevels];
            return response()->json($data, 200);
        } catch (QueryException $e) {
            $data = ['errorMessage' => "ERROR_ON_GETTING_DECISION_LEVELS"];
            return response()->json($data, 400);
        }
    }

    public function getAllReturnDecisionLevels()
    {
        try {
            $userDecisionLevelId = UserServices::getUser()->decision_level_id;
            $decisionLevels = DB::table('decision_levels as dl')->join('decision_level_next_decisions as dn', 'dl.id', 'dn.decision_level_id')->where('dn.next_level_id', $userDecisionLevelId)->select('dl.*')->get();
            $data = ['decisionLevels' => $decisionLevels];
            return response()->json($data, 200);
        } catch (QueryException $e) {
            $data = ['errorMessage' => "ERROR_ON_GETTING_DECISION_LEVELS"];
            return response()->json($data, 400);
        }
    }

    public function returnAllByUser(Request $request)
    {
        $data = json_decode($request->getContent());
        DB::transaction(function () use ($data) {
            $scutins = $data->scrutinizations;
            $userDecisionLevelId = UserServices::getUser()->decision_level_id;
            foreach ($scutins as $scrutin) {
                $movement = new \stdClass();
                $movement->from_decision_level_id = $userDecisionLevelId;
                $movement->to_decision_level_id = $data->to_decision_level_id;
                $movement->mtef_section_id = $scrutin->mtef_section_id;
                $movement->scrutinId = $scrutin->id;
                $movement->mtef_section_id = $scrutin->mtef_section_id;
                $movement->comments = $data->comments;
                $movement->direction = -1;

                $this->_returnMtefSection($movement);
            }
        });
        $data = ["successMessage" => "BUDGET_RETURN_SUCCESSFUL"];
        return response()->json($data, 200);


    }

    public function saveItemComments(Request $request)
    {
        $data = $request->all();

        try {

            if (isset($data['id'])) {
                $comment = MtefSectionItemComment::find($data['id']);
                $comment->update($data);
            } else {
                $comment = new MtefSectionItemComment($data);
                $comment->commented_by = UserServices::getUser()->title;
                $comment->date_commented = date('Y-m-d');
                $comment->save();
            }


            $data = ["successMessage" => "COMMENT_SAVED", "comment" => $comment];
            return response()->json($data, 200);

        } catch (\Exception $e) {
            Log::error($e);
            $data = ["errorMessage" => "ERROR_SAVE_COMMENTS", "error" => $e];
            return response()->json($data, 400);
        }
    }

    public function updateStatus($scutinId, $status)
    {
        try {
            $this->saveStatus($scutinId, $status);
            $data = ["successMessage" => "STATUS_CHANGED"];
            return response()->json($data, 200);
        } catch (QueryException $e) {
            $data = ["errorMessage" => "ERRO_SAVING_STATUS"];
            return response()->json($data, 400);
        }
    }

    public function assignUser(Request $request, $userId)
    {
        try {
            $data = $request->all();
            foreach ($data['scrutinIds'] as $scrutinId) {
                DB::table('scrutinizations')->where('id', $scrutinId)->update(array(
                    "user_id" => $userId,
                    "is_assigned" => true
                ));
            }
            $data = ["successMessage" => "USER_ASSIGNED_SUCCESSFULL", "scrutinizations" => $this->getScrutins()];
            return response()->json($data, 200);
        } catch (QueryException $e) {
            $data = ["errorMessage" => "ERRO_ASSIGN_USER"];
            return response()->json($data, 400);
        }
    }

    public function setAddressed(Request $request, $itemType, $itemId)
    {
        $user = UserServices::getUser();
        $today = date('Y-m-d h:i:sa');
        $addressedBy = $user->first_name . ' ' . $user->last_name;
        $c = $request->all();
        if ($itemType == 'INPUT' || $itemType == 'ACTIVITY' || $itemType == 'TARGET') {
            DB::table('mtef_section_item_comments')->where('item_type', $itemType)->where('id', $itemId)->update(array(
                "addressed" => true,
                "addressed_by" => $addressedBy,
                "date_addressed" => $today,
                "address_comments" => $c['address_comments']
            ));
        } else if ($itemType == 'MTEF_SECTION') {
            DB::table('mtef_section_comments')->where('id', $itemId)->update(array(
                "addressed" => true,
                "addressed_by" => $addressedBy,
                "date_addressed" => $today,
                "address_comments" => $c['address_comments']
            ));
        }

        $data = ["successMessage" => "UPDATED_SUCCESSFUL"];
        return response()->json($data, 200);
    }

    public function setRecommendation($scrutinId, $recommendation, $status)
    {
        try {

            $scrutin = Scrutinization::find($scrutinId);
            $scrutin->status = $status;
            $scrutin->recommendation = $recommendation;
            $scrutin->save();
            $data = ['successMessage' => "SUCCESSFUL_SET_RECOMMENDATION", "scrutin" => $scrutin];
            return response()->json($data, 200);
        } catch (QueryException $e) {
            $data = ['errorMessage' => "ERROR_SETTING_RECOMMENDATION"];
            return response()->json($data, 400);
        }
    }

//    *************************************************************************************************

}
