<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminHierarchyCeilingPeriodController extends Controller {

    public function getAllPaginated($perPage) {
        $financial_year = FinancialYearServices::getPlanningFinancialYear();
        $financial_year_id = $financial_year->id;
        $gfs_codes = DB::table('gfs_codes')
            ->join('ceilings', 'ceilings.gfs_code_id', '=', 'gfs_codes.id')
            ->join('admin_hierarchy_ceilings', 'admin_hierarchy_ceilings.ceiling_id', '=', 'ceilings.id')
            ->join('admin_hierarchy_ceiling_periods', 'admin_hierarchy_ceiling_periods.admin_hierarchy_ceiling_id', '=', 'admin_hierarchy_ceilings.id')
            ->join('periods', 'periods.id', '=', 'admin_hierarchy_ceiling_periods.period_id')
            ->where('admin_hierarchy_ceilings.financial_year_id', $financial_year_id)
            ->whereNull('admin_hierarchy_ceiling_periods.deleted_at')
            ->select('gfs_codes.id','gfs_codes.name','gfs_codes.code','periods.name as period','admin_hierarchy_ceiling_periods.amount')
            ->paginate($perPage);
        return $gfs_codes;
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }
}
