<?php

namespace App\Http\Controllers\Budgeting;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Budgeting\AdminHierarchyCeilingDoc;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class AdminHierarchyCeilingDocController extends Controller
{
    public function upload(Request $request) {
        $id = Input::get('id');
        $financialYearId    = Input::get('financialYearId');
        $adminHierarchyId = Input::get('adminHierarchyId');
        $budgetType = Input::get('budgetType');
        $fileName = null;
        if (Input::hasFile('file')) {
            if($budgetType=='SUPPLEMENTARY'){
                $destinationPath = 'uploads/reallocations'; // upload path
            }else{
                $destinationPath = 'uploads/carry_overs'; // upload path
            }
            $extension = Input::file('file')->getClientOriginalExtension(); // getting image extension
            $fileName = md5(time()).'.'.$extension; // renaming image
            Input::file('file')->storeAs($destinationPath, $fileName); // uploading file to given path
            $documentUrl = $destinationPath.'/'.$fileName;
            if (!isset($id)) {
                $doc = new AdminHierarchyCeilingDoc();
                $doc->admin_hierarchy_id = $adminHierarchyId;
                $doc->financial_year_id = $financialYearId;
                $doc->budget_type = $budgetType;
                $doc->document_url = $documentUrl;
                $doc->save();
            } else {
                $acd = AdminHierarchyCeilingDoc::find($id);
                $acd->document_url = $documentUrl;
                $acd->save();
            }
        }
        $docs = AdminHierarchyCeilingDoc::where([
            'admin_hierarchy_id'=>$adminHierarchyId,
            'financial_year_id'=>$financialYearId,
            'budget_type'=> $budgetType,
            'deleted_at'=> null,
        ])->get();

        return ['ceilingDocs'=>$docs];
    }

    public function get($financialYearId, $adminHierarchyId, $budgetType) {
        $docs = AdminHierarchyCeilingDoc::where([
            'admin_hierarchy_id'=>$adminHierarchyId,
            'financial_year_id'=>$financialYearId,
            'budget_type'=> $budgetType,
            'deleted_at'=> null,
        ])->get();

        return ['ceilingDocs'=>$docs];
    }

    public function delete(Request $request)
    {
        DB::table('admin_hierarchy_ceiling_docs')
            ->where('id',$request->id)
            ->update(['deleted_at'=> Carbon::now()]);

        $docs = AdminHierarchyCeilingDoc::where([
            'admin_hierarchy_id'=>$request->admin_hierarchy_id,
            'financial_year_id'=>$request->financial_year_id,
            'budget_type'=> $request->budget_type,
            'deleted_at'=> null,
        ])->get();

        return ['ceilingDocs'=>$docs];
    }
}
