<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Budgeting\BdcGroupFinancialYearValue;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class BdcGroupFinancialYearValueController extends Controller
{
    public function index() {
        $financial_year_id = FinancialYearServices::getPlanningFinancialYear()->id;
        $all = BdcGroupFinancialYearValue::with('financial_year','bdc_group')
            ->where('financial_year_id',$financial_year_id)
            ->orderBy('created_at','desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return BdcGroupFinancialYearValue::with('financial_year','bdc_group')->orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $financial_year_id = FinancialYearServices::getPlanningFinancialYear()->id;
        try{
            $data = json_decode($request->getContent());
            $obj = new BdcGroupFinancialYearValue();
            $obj->group_id = $data->group_id;
            $obj->financial_year_id = $financial_year_id;
            $obj->percent = $data->percent;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_GROUP_FINANCIAL_YEAR_VALUE_CREATED", "groupFinancialYearValues" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = BdcGroupFinancialYearValue::find($data->id);
            $obj->group_id = $data->group_id;
            $obj->percent = $data->percent;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_GROUP_FINANCIAL_YEAR_VALUE_UPDATED", "groupFinancialYearValues" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = BdcGroupFinancialYearValue::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_GROUP_FINANCIAL_YEAR_VALUE_DELETED", "groupFinancialYearValues" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = BdcGroupFinancialYearValue::with('financial_year','bdc_group')
            ->orderBy('created_at', 'desc')
            ->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        BdcGroupFinancialYearValue::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "GROUP_FINANCIAL_YEAR_VALUE_RESTORED", "trashedGroupFinancialYearValues" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        BdcGroupFinancialYearValue::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "GROUP_FINANCIAL_YEAR_VALUE_DELETED_PERMANENTLY", "trashedGroupFinancialYearValues" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = BdcGroupFinancialYearValue::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "GROUP_FINANCIAL_YEAR_VALUE_DELETED_PERMANENTLY", "trashedGroupFinancialYearValues" => $all];
        return response()->json($message, 200);
    }
}
