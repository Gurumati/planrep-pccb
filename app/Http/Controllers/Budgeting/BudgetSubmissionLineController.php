<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Budgeting\BudgetSubmissionDefinition;
use App\Models\Setup\BudgetSubmissionSelectOption;
use App\Models\Budgeting\BudgetSubmissionLine;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\Mtef;
use App\Models\Setup\Section;
use Carbon\Carbon;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Planning\Activity;

class BudgetSubmissionLineController extends Controller {
    //
    public function pe_transaction($fund_source_id, $sub_form_id, $activity_id, $section_id, $facility_id, $budget_line, $budget_submission_line_id)
    {
        DB::transaction(function () use ($fund_source_id, $sub_form_id, $activity_id, $section_id, $facility_id, $budget_line, $budget_submission_line_id) {
            foreach ($budget_line as $key=>$value)
            {
                if($value != ''){
                    $columns = $value;
                    if(sizeof($columns) > 0)
                    {
                        if($budget_submission_line_id == null){
                            $budget_submission_line_id = DB::table('budget_submission_lines')
                                ->insertGetId(['budget_submission_sub_form_id' => $sub_form_id,
                                    'activity_id' => $activity_id,
                                    'section_id' => 27,
                                    'fund_source_id' => $fund_source_id,
                                    'budget_submission_select_option_id' => $section_id==27?null:$section_id]);
                        }

                        //insert activity facility
                        $activityFacilityExist = DB::table('activity_facilities')
                            ->where('facility_id',$facility_id)
                            ->where('activity_id',$activity_id)
                            ->select('id')
                            ->first();
                        if(!isset($activityFacilityExist->id))
                        {
                            $activity_facility_id = DB::table('activity_facilities')
                                ->insertGetId(['facility_id'=>$facility_id,'activity_id'=>$activity_id]);
                        }else
                        {
                            $activity_facility_id =  $activityFacilityExist->id;
                        }
                        //insert into activity facility fund source
                        $activityFacilityFundSourceExist = DB::table('activity_facility_fund_sources')
                            ->where('activity_facility_id',$activity_facility_id)
                            ->where('fund_source_id',$fund_source_id)
                            ->select('id')
                            ->first();
                        if(!isset($activityFacilityFundSourceExist->id))
                        {
                            $activity_facility_fund_source_id = DB::table('activity_facility_fund_sources')
                                ->insertGetId(['activity_facility_id'=>$activity_facility_id,'fund_source_id'=>$fund_source_id]);
                        }else
                        {
                            $activity_facility_fund_source_id =  $activityFacilityFundSourceExist->id;
                        }

                        //loop each column to insert data
                        foreach ($columns as $key => $value) {
                            $field_value = !empty($value['field_value']) ? $value['field_value'] : '0';
                            //check if contains separator
                            if($value['type'] == 'currency' || $value['type'] == 'formula')
                            {
                                $field_value = (int)str_replace(',','',$field_value);
                            }
                            //insert into activity input :: check whether activity input exist
                            $budgetQuery = DB::table('budget_submission_line_values')
                                ->where('budget_submission_line_id', $budget_submission_line_id)
                                ->where('budget_submission_definition_id', $value['id'])
                                ->select('activity_facility_fund_source_input_id', 'id')
                                ->first();

                            //check if the definition value is input
                            if ($value['is_input']) {
                                if (!isset($budgetQuery)) {
                                    //insert activity input:: this is a default activity inputs
                                    $activity_exist = DB::table('budget_submission_lines')
                                        ->join('budget_submission_line_values', 'budget_submission_line_values.budget_submission_line_id', '=', 'budget_submission_lines.id')
                                        ->where('budget_submission_lines.activity_id', $activity_id)
                                        ->where('budget_submission_lines.budget_submission_sub_form_id', $sub_form_id)
                                        ->where('budget_submission_line_values.budget_submission_definition_id', $value['id'])
                                        ->select('budget_submission_line_values.activity_facility_fund_source_input_id')
                                        ->first();
                                    if (!isset($activity_exist)) {
                                        $activity_input_id = DB::table('activity_facility_fund_source_inputs')
                                            ->insertGetId([
                                                'activity_facility_fund_source_id' => $activity_facility_fund_source_id,
                                                'gfs_code_id' => $value['gfs_code_id'],
                                                'unit_id' => $value['unit_id'],
                                                'unit_price' => $field_value,
                                                'frequency' => 1,
                                                'created_at' => Carbon::now(),
                                                'has_breakdown' => $value['has_breakdown'],
                                                'is_procurement' => false,
                                                'is_in_kind' => false,
                                                'quantity' => 1]);
                                    } else
                                    {
                                        $activity_input_id = $activity_exist->activity_facility_fund_source_input_id;
                                    }

                                    //insert submission line values

                                    DB::table('budget_submission_line_values')
                                        ->insert(['budget_submission_line_id' => $budget_submission_line_id,
                                            'budget_submission_definition_id' => $value['id'],
                                            'field_value' => $field_value,
                                            'created_at' => Carbon::now(),
                                            'activity_facility_fund_source_input_id' => $activity_input_id]);

                                } else {
                                    //get activity input
                                    $activity_input_id = $budgetQuery->activity_facility_fund_source_input_id;
                                    $budget_submission_line_values_id = $budgetQuery->id;
                                    //update budget submission line values
                                    DB::table('budget_submission_line_values')
                                        ->where('id', $budget_submission_line_values_id)
                                        ->update(['field_value' => $field_value]);
                                }
                                //update activity inputs :: sum up field values from all sections
                                $fieldLineValues = DB::table('budget_submission_line_values')
                                    ->where('activity_facility_fund_source_input_id', $activity_input_id)
                                    ->where('budget_submission_definition_id', $value['id'])
                                    ->select(DB::raw('sum(cast(field_value as double precision))'))
                                    ->get();

                                DB::table('activity_facility_fund_source_inputs')
                                    ->where('id', $activity_input_id)
                                    ->update(['unit_price' => $fieldLineValues[0]->sum]);
                                //end
                                //loop activity input breakdown
                                if(isset($value['child_form'])){
                                    foreach ($value['child_form'] as $key2 => $value2) {
                                        $budget_submission_definition_id = $value2['id'];
                                        $field_value = !empty($value2['field_value']) ? $value2['field_value'] : '0';

                                        //check if exists
                                        $input_breakdown = DB::table('budget_submission_line_values')
                                            ->where('budget_submission_line_id', $budget_submission_line_id)
                                            ->where('budget_submission_definition_id', $budget_submission_definition_id)
                                            ->select('budget_submission_line_values.id')
                                            ->first();
                                        if (isset($input_breakdown)) {
                                            $submission_line_id = $input_breakdown->id;
                                            DB::table('budget_submission_line_values')
                                                ->where('id', $submission_line_id)
                                                ->update(['field_value' => $field_value]);

                                        } else {
                                            DB::table('budget_submission_line_values')
                                                ->insert(['budget_submission_line_id' => $budget_submission_line_id,
                                                    'budget_submission_definition_id' => $budget_submission_definition_id,
                                                    'activity_facility_fund_source_input_id' => $activity_input_id,
                                                    'field_value' => $field_value,
                                                    'created_at' => Carbon::now()]);

                                            $activity_breakdown_exist = DB::table('activity_facility_fund_source_input_breakdowns')
                                                ->where('activity_facility_fund_source_input_id', $activity_input_id)
                                                ->where('budget_submission_definition_id', $budget_submission_definition_id)
                                                ->select('activity_facility_fund_source_input_breakdowns.id')
                                                ->first();
                                            if (!isset($activity_breakdown_exist)) {
                                                DB::table('activity_facility_fund_source_input_breakdowns')->insert(
                                                    ['item' => $value2['field_name'],
                                                        'budget_submission_definition_id' => $value2['id'],
                                                        'activity_facility_fund_source_input_id' => $activity_input_id,
                                                        'unit_id' => $value2['unit_id'],
                                                        'unit_price' => $field_value,
                                                        'frequency' => 1,
                                                        'created_at' => Carbon::now(),
                                                        'quantity' => 1
                                                    ]);
                                            }

                                        }
                                        //update activity inputs breakdown :: sum up field values from all sections
                                        $fieldLineValues = DB::table('budget_submission_line_values')
                                            ->where('activity_facility_fund_source_input_id', $activity_input_id)
                                            ->where('budget_submission_definition_id', $budget_submission_definition_id)
                                            ->select(DB::raw('sum(cast(field_value as double precision))'))
                                            ->get();

                                        DB::table('activity_facility_fund_source_input_breakdowns')
                                            ->where('activity_facility_fund_source_input_id', $activity_input_id)
                                            ->where('budget_submission_definition_id', $budget_submission_definition_id)
                                            ->update(['unit_price' => $fieldLineValues[0]->sum]);
                                        //end
                                    }
                                    //end loop
                                }

                            } else {
                                $activity_input_id = null;
                                if (!isset($budgetQuery)) {
                                    //insert submission line values
                                    DB::table('budget_submission_line_values')
                                        ->insert(['budget_submission_line_id' => $budget_submission_line_id,
                                            'budget_submission_definition_id' => $value['id'],
                                            'field_value' => $field_value,
                                            'created_at' => Carbon::now(),
                                            'activity_facility_fund_source_input_id' => $activity_input_id]);
                                } else {
                                    //update budget submission line values
                                    $budget_submission_line_values_id = $budgetQuery->id;
                                    DB::table('budget_submission_line_values')
                                        ->where('id', $budget_submission_line_values_id)
                                        ->update(['field_value' => $field_value]);
                                }
                            }
                        }

                    }

                }
            }

        });

    }

    public function store(Request $request) {
        $data = json_decode($request->getContent(), true);
        $fund_source_id =    $data['fund_source_id'];
        $sub_form_id    =    $data['sub_form_id'];
        $activity_id    =    $data['activity_id'];
        $section_id     =    $data['section_id'];
        $facility_id    =    $data['facility_id'];
        $budget_line    =    $data['columns'];
        try {
            $this->pe_transaction($fund_source_id, $sub_form_id, $activity_id, $section_id, $facility_id, $budget_line, null);
            //message
            $mtefBudget = DB::table('budget_submission_line_values')
                ->join('budget_submission_lines', 'budget_submission_line_values.budget_submission_line_id', '=', 'budget_submission_lines.id')
                ->where('budget_submission_lines.budget_submission_sub_form_id', $sub_form_id)
                ->where('budget_submission_lines.activity_id', $activity_id)
                ->select('budget_submission_line_values.id', 'budget_submission_line_values.budget_submission_definition_id', 'budget_submission_lines.activity_id',
                    'budget_submission_line_values.field_value', 'budget_submission_lines.section_id','budget_submission_line_values.budget_submission_line_id')
                ->get();
            //get submission lines
            $mtefBudgetLine = DB::table('budget_submission_lines')
                ->where('budget_submission_lines.budget_submission_sub_form_id', $sub_form_id)
                ->where('budget_submission_lines.activity_id', $activity_id)
                ->select('id')
                ->get();
            $message = ["successMessage" => "PE_BUDGET_ITEM_UPDATED_SUCCESSFULLY", 'lineData'=>$mtefBudgetLine,'lineValues'=>$mtefBudget];
            return response()->json($message, 200);
        } catch (QueryException $e) {
            Log::error('BUDGET SUBMISSION LINE ITEMS ERROR' . $e);
            $message = ["errorMessage" => "BUDGET_SUBMISSION_LINE_ITEMS_FAILED"];
            return response()->json($message, 400);
        }

    }

    public function budgetDefinition($form_id) {
        $submissionForm = DB::table('budget_submission_forms')
            ->join('budget_submission_sub_forms', 'budget_submission_forms.id', '=', 'budget_submission_sub_forms.budget_submission_form_id')
            ->where('budget_submission_sub_forms.id', $form_id)
            ->select('budget_submission_forms.id')
            ->first();
        //use submission form id to find form definition
        $parentForms = BudgetSubmissionDefinition::whereNull('parent_id')->with('childForm')->where('budget_submission_form_id', $submissionForm->id)->orderBy('sort_order')->get();
        return response()->json($parentForms);
    }

    public function edit(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        try {
            $activity_id        = $data['activity_id'];
            $form_id            = $data['form_id']; //budget sub_form id
            $section_id         = $data['section_id'];
            $columns            = $data['columns'];
            $submission_line_id = $data['budget_submission_line_id'];
            $facility_id        = $data['facility_id'];

            //get fund source
            $result = BudgetSubmissionLine::find($submission_line_id);
            $fund_source_id = $result->fund_source_id;

            if (isset($submission_line_id)) {
                foreach ($columns as $key => $value) {

                    if(isset($value['field_value']))
                    {
                        $field_value = $value['field_value'];
                        //check if contain comma
                        if($value['type'] == 'currency' || $value['type'] == 'formula')
                        {
                            $field_value = (int)str_replace(',','',$field_value);
                        }
                        $budget_submission_definition_id = $value['id'];

                        //update line value
                        DB::table('budget_submission_line_values')
                            ->where('budget_submission_line_id',$submission_line_id)
                            ->where('budget_submission_definition_id',$budget_submission_definition_id)
                            ->update(['field_value'=>$field_value]);
                        //check if exist
                        $exist = DB::table('budget_submission_line_values')
                            ->where('budget_submission_line_id',$submission_line_id)
                            ->where('budget_submission_definition_id',$budget_submission_definition_id)
                            ->select('id')
                            ->count();
                        if($exist > 0){
                            //update line value
                            DB::table('budget_submission_line_values')
                                ->where('budget_submission_line_id',$submission_line_id)
                                ->where('budget_submission_definition_id',$budget_submission_definition_id)
                                ->update(['field_value'=>$field_value]);
                        }else {
                            //create line value
                            $this->pe_transaction($fund_source_id, $form_id, $activity_id, $section_id, $facility_id, array(array($value)), $submission_line_id);
                        }

                    }

                    //check if there is a child
                    if(sizeof($value['child_form']) > 0)
                    {
                        foreach($value['child_form'] as $key2=>$value2)
                        {
                            if(isset($value2['field_value'])) {
                                $field_value = $value2['field_value'];
                                $budget_submission_definition_id = $value2['id'];
                                //check if exist
                                $exist = DB::table('budget_submission_line_values')
                                    ->where('budget_submission_line_id',$submission_line_id)
                                    ->where('budget_submission_definition_id',$budget_submission_definition_id)
                                    ->select('id')
                                    ->count();
                                if($exist > 0){
                                    //update line value
                                    DB::table('budget_submission_line_values')
                                        ->where('budget_submission_line_id',$submission_line_id)
                                        ->where('budget_submission_definition_id',$budget_submission_definition_id)
                                        ->update(['field_value'=>$field_value]);
                                }
                            }

                        }
                    }
                }

                //update activity data & its input
                $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
                DB::transaction(function () use ($admin_hierarchy_id) {
                    //get all activity inputs
                    $activity_inputs = DB::table('activity_facility_fund_source_inputs as ai')
                        ->join('budget_submission_line_values as b','ai.id','b.activity_facility_fund_source_input_id')
                        ->join('activity_facility_fund_sources as aff','ai.activity_facility_fund_source_id','aff.id')
                        ->join('activity_facilities as af','aff.activity_facility_id','af.id')
                        ->join('activities as a','a.id','af.activity_id')
                        ->join('mtef_sections as ms','ms.id','a.mtef_section_id')
                        ->join('mtefs as m','m.id','ms.mtef_id')
                        ->where('m.admin_hierarchy_id', $admin_hierarchy_id)
                        ->select('ai.id')
                        ->distinct()
                        ->get();

                    foreach ($activity_inputs as $values) {
                        //activity inputs apply to submission values with null parent only
                        $fieldLineValues = DB::table('budget_submission_line_values')
                            ->join('budget_submission_definitions', 'budget_submission_line_values.budget_submission_definition_id', '=', 'budget_submission_definitions.id')
                            ->where('budget_submission_definitions.parent_id', null)
                            ->where('budget_submission_line_values.activity_facility_fund_source_input_id', $values->id)
                            ->select('budget_submission_definitions.gfs_code_id','budget_submission_definitions.has_breakdown',DB::raw('sum(cast(field_value as double precision))'))
                            ->groupBy('budget_submission_definitions.id')
                            ->get();
                        if($fieldLineValues[0]->sum == null){
                            $fieldLineValues[0]->sum = 0;
                        }

                        //update activity inputs
                        DB::table('activity_facility_fund_source_inputs')
                            ->where('id', $values->id)
                            ->update(['unit_price' => $fieldLineValues[0]->sum,
                                'frequency'=>1,
                                'quantity'=>1,
                                'has_breakdown'=>  $fieldLineValues[0]->has_breakdown ]);

                        //update activity inputs breakdown :: sum up field values from all sections
                        $fieldLineValuesBreakdown = DB::table('budget_submission_line_values')
                            ->join('budget_submission_definitions', 'budget_submission_line_values.budget_submission_definition_id', '=', 'budget_submission_definitions.id')
                            ->where('budget_submission_definitions.parent_id', '!=', null)
                            ->where('budget_submission_definitions.is_input',true)
                            ->where('budget_submission_line_values.activity_facility_fund_source_input_id', $values->id)
                            ->select(DB::raw('sum(cast(field_value as double precision))'), 'budget_submission_definitions.id as field_id')
                            ->groupBy('budget_submission_definitions.id')
                            ->get();

                        foreach ($fieldLineValuesBreakdown as $inputFields) {
                            DB::table('activity_facility_fund_source_input_breakdowns')
                                ->where('activity_facility_fund_source_input_id', $values->id)
                                ->where('budget_submission_definition_id', $inputFields->field_id)
                                ->update(['unit_price' => $inputFields->sum]);
                        }
                        //end
                    }
                });
                //end
                $mtefBudget = DB::table('budget_submission_line_values')
                    ->join('budget_submission_lines', 'budget_submission_line_values.budget_submission_line_id', '=', 'budget_submission_lines.id')
                    ->where('budget_submission_lines.budget_submission_sub_form_id', $form_id)
                    ->where('budget_submission_lines.activity_id', $activity_id)
                    ->select('budget_submission_line_values.id', 'budget_submission_line_values.budget_submission_definition_id', 'budget_submission_lines.activity_id',
                        'budget_submission_line_values.field_value', 'budget_submission_lines.section_id','budget_submission_line_values.budget_submission_line_id')
                    ->get();
                //get submission lines
                $mtefBudgetLine = DB::table('budget_submission_lines')
                    ->where('budget_submission_lines.budget_submission_sub_form_id', $form_id)
                    ->where('budget_submission_lines.activity_id', $activity_id)
                    ->select('id')
                    ->get();
                $message = ["successMessage" => "PE_BUDGET_ITEM_UPDATED_SUCCESSFULLY", 'lineData'=>$mtefBudgetLine,'lineValues'=>$mtefBudget];
                return response()->json($message, 200);
            } else {
                $message = ["errorMessage" => "NO_PE_BUDGET_TO_DELETE"];
                return response()->json($message, 400);
            }
        } catch (QueryException $e) {
            $message = ["errorMessage" => "BUDGET_SUBMISSION_LINE_ITEMS_FAILED"];
            return response()->json($message, 400);
        }
    }

    public function delete(Request $request) {
        $data = json_decode($request->getContent(), true);
        try {
            $activity_id  = $data['activity_id'];
            $form_id      = $data['form_id'];
            $section_id   = $data['section_id'];
            $submission_line_id = $data['budget_submission_line_id'];

            if (isset($submission_line_id)) {
                $submission_toDelete = BudgetSubmissionLine::find($submission_line_id);
                $submission_toDelete->budgetSubmissionLineValues()->delete();
                $submission_toDelete->delete();


                //update activity data & its input
                DB::transaction(function () use ($activity_id) {

                    //get activity facility
                    $activityFacilityExist = DB::table('activity_facilities')
                        ->where('activity_id',$activity_id)
                        ->select('id')
                        ->first();
                    $activity_facility_id = $activityFacilityExist->id;

                    //get activity facility fund source
                    $activityFacilityFundSourceExist = DB::table('activity_facility_fund_sources')
                        ->where('activity_facility_id',$activity_facility_id)
                        ->select('id')
                        ->get();

                    foreach($activityFacilityFundSourceExist as $v)
                    {
                        $activity_facility_fund_source_id = $v->id;
                        //get all activity inputs
                        $activity_inputs = DB::table('activity_facility_fund_source_inputs')
                            ->where('activity_facility_fund_source_id', $activity_facility_fund_source_id)
                            ->select('id')
                            ->get();
                        foreach ($activity_inputs as $values) {
                            //activity inputs apply to submission values with null parent only
                            $fieldLineValues = DB::table('budget_submission_line_values')
                                ->join('budget_submission_definitions', 'budget_submission_line_values.budget_submission_definition_id', '=', 'budget_submission_definitions.id')
                                ->where('budget_submission_definitions.parent_id', null)
                                ->where('budget_submission_line_values.activity_facility_fund_source_input_id', $values->id)
                                ->select(DB::raw('sum(cast(field_value as double precision))'))
                                ->get();
                            if($fieldLineValues[0]->sum == null)
                            {
                                $fieldLineValues[0]->sum = 0;
                            }
                            //update activity inputs
                            DB::table('activity_facility_fund_source_inputs')
                                ->where('id', $values->id)
                                ->update(['unit_price' => 0]);

                            //update activity inputs breakdown :: sum up field values from all sections
                            $fieldLineValuesBreakdown = DB::table('budget_submission_line_values')
                                ->join('budget_submission_definitions', 'budget_submission_line_values.budget_submission_definition_id', '=', 'budget_submission_definitions.id')
                                ->where('budget_submission_definitions.parent_id', '!=', null)
                                ->where('budget_submission_line_values.activity_facility_fund_source_input_id', $values->id)
                                ->select(DB::raw('sum(cast(field_value as double precision))'), 'budget_submission_definitions.id as field_id')
                                ->groupBy('budget_submission_definitions.id')
                                ->get();

                            foreach ($fieldLineValuesBreakdown as $inputFields) {

                                DB::table('activity_facility_fund_source_input_breakdowns')
                                    ->where('activity_facility_fund_source_input_id', $values->id)
                                    ->where('budget_submission_definition_id', $inputFields->field_id)
                                    ->update(['unit_price' => 0]);
                            }

                            //end

                        }
                    }

                });
                //end
                $mtefBudget = DB::table('budget_submission_line_values')
                    ->join('budget_submission_lines', 'budget_submission_line_values.budget_submission_line_id', '=', 'budget_submission_lines.id')
                    ->where('budget_submission_lines.budget_submission_sub_form_id', $form_id)
                    ->where('budget_submission_lines.activity_id', $activity_id)
                    ->select('budget_submission_line_values.id', 'budget_submission_line_values.budget_submission_definition_id', 'budget_submission_lines.activity_id',
                        'budget_submission_line_values.field_value', 'budget_submission_lines.section_id','budget_submission_line_values.budget_submission_line_id')
                    ->get();
                //get submission lines
                $mtefBudgetLine = DB::table('budget_submission_lines')
                    ->where('budget_submission_lines.budget_submission_sub_form_id', $form_id)
                    ->where('budget_submission_lines.activity_id', $activity_id)
                    ->select('id')
                    ->get();
                $message = ["successMessage" => "PE_BUDGET_ITEM_DELETED_SUCCESSFULLY", 'lineData'=>$mtefBudgetLine,'lineValues'=>$mtefBudget];
                return response()->json($message, 200);
            } else {
                $message = ["errorMessage" => "NO_PE_BUDGET_TO_DELETE"];
                return response()->json($message, 400);
            }
        } catch (QueryException $e) {
            $message = ["errorMessage" => "BUDGET_SUBMISSION_LINE_ITEMS_FAILED"];
            return response()->json($message, 400);
        }
    }

    public function loadSection() {
//        $sections = Section::doesntHave('childSections')->select('id', 'code', 'name')->get();
        $sections = DB::table('sections as s')
            ->join('admin_hierarchy_sections as ahs','ahs.section_id','=','s.id')
            ->join('section_levels as sl','s.section_level_id','=','sl.id')
            ->where('ahs.admin_hierarchy_id',UserServices::getUser()->admin_hierarchy_id)
            ->select('s.id','s.code','s.name','s.section_level_id','sl.hierarchy_position')->get();
        return response()->json($sections);
    }

    public function getChildren(){
        $all = BudgetSubmissionSelectOption::where('parent_id', 1)
            ->where('admin_hierarchy_id', 3)
            ->get();
        return response()->json($all);
    }

    public function loadBudgetClasses($sub_form_id, $financialYearId){
        $budgetClasses = DB::table('budget_submission_sub_forms')
            ->join('budget_submission_forms','budget_submission_sub_forms.budget_submission_form_id','=','budget_submission_forms.id')
            ->where('budget_submission_sub_forms.id',$sub_form_id)
            ->select('budget_submission_sub_forms.id','budget_submission_sub_forms.name','budget_submission_forms.budget_classes','budget_submission_forms.fund_sources')
            ->first();
        $budget_classes = explode(",",$budgetClasses->budget_classes);
        $fund_sources   = explode(",",$budgetClasses->fund_sources);
        $data = array();
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        try{
            foreach ($budget_classes as $key=>$value)
            {
                //check if mtef is locked
                $mtef = DB::table('mtefs')
                    ->where('admin_hierarchy_id',$admin_hierarchy_id)
                    ->where('financial_year_id',$financialYearId)
                    ->select('id','locked')
                    ->first();
                //return error message when mtef is not initialized
                if(!isset($mtef->id)){
                    $message = (object) array("errorMessage" => "BUDGET_IS_NOT_INITIALIZED");
                    return response()->json($message, 500);
                }
                //insert default facility
                $facility_type_id = ConfigurationSetting::getValueByKey('COUNCIL_FACILITY_TYPE');

                $facility_code = isset($facility_code)?$facility_code:'00000000';
                $facility_desc = isset($facility_desc)?$facility_desc:'PISC Facility';
                $facilityExist = DB::table('facilities')
                    ->where('facility_type_id',$facility_type_id)
                    ->where('admin_hierarchy_id',$admin_hierarchy_id)
                    ->where('facility_code',$facility_code)
                    ->select('id')
                    ->first();
                if(!isset($facilityExist->id))
                {
                    //insert council facility
                    $facility_id = DB::table('facilities')
                        ->insertGetId(['name'=>$facility_desc,'facility_code'=>$facility_code,
                            'facility_type_id'=>$facility_type_id, 'admin_hierarchy_id'=>$admin_hierarchy_id,
                            'is_active'=>true]);
                }else
                {
                    $facility_id = $facilityExist->id;
                }
                //get budget class
                $budget_class = DB::table('budget_classes')
                    ->where('id',$value)
                    ->select('name')
                    ->first();
                //get fund sources
                $fundSource = DB::table('fund_source_budget_classes')
                    ->join('fund_sources','fund_source_budget_classes.fund_source_id','fund_sources.id')
                    ->whereIn('fund_source_budget_classes.fund_source_id',$fund_sources)
                    ->where('fund_source_budget_classes.budget_class_id',$value)
                    ->select('fund_sources.id','fund_sources.name')
                    ->get();

                array_push($data, array('mtef_id'=>$mtef->id,'budget_class_id'=>$value,
                    'budget_class_name'=>$budget_class->name,
                    'fund_sources'=>$fundSource,
                    'facility_id' => $facility_id,
                    'activity_locked' => $mtef->locked));
            }
            $message = ["budget_classes"=>$data];
            return response()->json($message, 200);
        }catch (QueryException $e) {
//         Log::error('FAILED_TO_INITIALIZE_PE_BUDGET' . $e);
            $message = ["errorMessage" => "FAILED_TO_INITIALIZE_PE_BUDGET"];
            return response()->json($message, 400);
        }
    }

    public function loadBudget(Request $request) {
        $sub_form_id     = $request->form_id;
        $mtef_id         = $request->mtef_id;
        $budget_class_id = $request->budget_class_id;

        $mtefBudget = DB::table('budget_submission_line_values as v')
            ->join('budget_submission_lines as l', 'v.budget_submission_line_id', '=', 'l.id')
            ->join('activities as ac','ac.id','l.activity_id')
            ->join('mtef_sections as ms', 'ms.id','ac.mtef_section_id')
            ->where('l.budget_submission_sub_form_id', $sub_form_id)
            ->where('ms.mtef_id', $mtef_id)
            ->where('ac.budget_class_id', $budget_class_id)
            ->select('v.id', 'v.budget_submission_definition_id', 'l.activity_id',
                'v.field_value', 'l.section_id','v.budget_submission_line_id')
            ->get();
        //get submission lines
        $mtefBudgetLine = DB::table('budget_submission_lines as l')
            ->join('activities as ac','ac.id','l.activity_id')
            ->join('mtef_sections as ms', 'ms.id','ac.mtef_section_id')
            ->where('l.budget_submission_sub_form_id', $sub_form_id)
            ->where('ms.mtef_id', $mtef_id)
            ->select('l.id')
            ->get();
        $result = ['lineData'=>$mtefBudgetLine,'lineValues'=>$mtefBudget];
        return response()->json($result);
    }

    public function uploadPE()
    {
        $columns = Input::get('columns');
        $activity_id = Input::get('activity_id');
        $fund_source_id = Input::get('fund_source_id');
        $sub_form_id =Input::get('sub_form_id');
        $facility_id = Input::get('facility_id');
        $dataColumn  = array();


        //fwrite($file, print_r($columns,true));
        //get PE FILE
        if(Input::hasFile('file'))
        {
            $path = Input::file('file')->getRealPath();
            $data = Excel::selectSheets('PE_Form')->load($path, function ($reader) { })->all();
            if (!empty($data) && $data->count()) {

                foreach ($data as $key=>$value)
                {
                    //loop columns
                    foreach ($columns as $column) {
                        $item = str_slug($column['field_name'],'_');
                        if(isset($value->$item))
                        {
                            $column['field_value'] = $value->$item;
                        }

                        //loop child columns
                        if(isset($column['child_form']))
                        {

                            foreach ($column['child_form'] as $childColumn)
                            {
                                $item = str_slug($childColumn['field_name'],'_');
                                if(isset($value->$item))
                                {
                                    $childColumn['field_value'] = $value->$item;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function resetContributions(Request $request)
    {
        $submissionlines = $request->items;
        $section_id = $request->activity['section_id'];
        $mtef_id         = $request->activity['mtef_id'];
        $budget_class_id = $request->activity['budget_class_id'];
        $facility_id = $request->activity['facility_id'];
        $activity_id = $request->activity['activity_id'];
        $fund_source_id= 135;
//insert activity facility
        $activityFacilityExist = DB::table('activity_facilities')
            ->where('facility_id',$facility_id)
            ->where('activity_id',$activity_id)
            ->select('id')
            ->first();
        if(!isset($activityFacilityExist->id))
        {
            $activity_facility_id = DB::table('activity_facilities')
                ->insertGetId(['facility_id'=>$facility_id,'activity_id'=>$activity_id]);
        }else
        {
            $activity_facility_id =  $activityFacilityExist->id;
        }
        //insert into activity facility fund source
        $activityFacilityFundSourceExist = DB::table('activity_facility_fund_sources')
            ->where('activity_facility_id',$activity_facility_id)
            ->where('fund_source_id',$fund_source_id)
            ->select('id')
            ->first();
        if(!isset($activityFacilityFundSourceExist->id))
        {
            $activity_facility_fund_source_id = DB::table('activity_facility_fund_sources')
                ->insertGetId(['activity_facility_id'=>$activity_facility_id,'fund_source_id'=>$fund_source_id]);
        }else
        {
            $activity_facility_fund_source_id =  $activityFacilityFundSourceExist->id;
        }
        $activity_input_id = DB::table('activity_facility_fund_source_inputs')
            ->insertGetId([
                'activity_facility_fund_source_id' => $activity_facility_fund_source_id,
                'gfs_code_id' => 2153,
                'unit_id' =>1,
                'unit_price' => 0,
                'frequency' => 1,
                'created_at' => Carbon::now(),
                'has_breakdown' => false,
                'is_procurement' => false,
                'is_in_kind' => false,
                'quantity' => 1]);
        $input_id = $activity_input_id;

        $definitions = [10,11,12,13,14];
        foreach ($submissionlines as $item) {
            $budget_submission_line_id = $item['budget_submission_line_id'];
            //insert submission line values
            foreach ($definitions as $definition) {
                DB::table('budget_submission_line_values')
                    ->insert(['budget_submission_line_id' => $item['budget_submission_line_id'],
                        'budget_submission_definition_id' => $definition,
                        'field_value' => '0',
                        'created_at' => Carbon::now(),
                        'activity_facility_fund_source_input_id' => $input_id]);
            }
        }
        return response()->json(['message'=>'Contributions reset succeeded','status'=>200]);
    }
    /** load activity */
    public function loadActivity(Request $request){
        $mtef_id         = $request->mtef_id;
        $section_id      = 27;
        $budget_class_id = $request->budget_class_id;
        $fund_source_id = $request->fund_source_id;
        $financial_year_id = $request->financial_year_id;
        $adminId = UserServices::getUser()->admin_hierarchy_id;
        $sectionIds = Section::where('section_level_id',4);

        $activity = DB::table('mtef_sections as ms')
            ->join('activities as ac', 'ac.mtef_section_id', 'ms.id')
            ->where('ac.code', '00000000')
            ->where('ms.mtef_id', $mtef_id)
            ->where('ms.section_id', $section_id)
            ->where('ac.budget_class_id', $budget_class_id)
            ->select('ac.id')
            ->first();
        if(!isset($activity->id)){
            //create activity
            $mtefSectionExists = DB::table('mtef_sections')
                ->where('mtef_id',$mtef_id)
                ->where('section_id',$section_id)
                ->select('id')
                ->first();
            if(!isset($mtefSectionExists->id))
            {
                //get section level
                $sectionLevel =  DB::table('sections')
                    ->where('id',$section_id)
                    ->select('section_level_id')
                    ->first();
                $section_level_id = $sectionLevel->section_level_id;
                //insert mtef section
                $mtef_section_id = DB::table('mtef_sections')
                    ->insertGetId(['mtef_id' => $mtef_id,
                        'section_id' => $section_id,
                        'section_level_id'=>$section_level_id
                    ]);
            }else
            {
                $mtef_section_id = $mtefSectionExists->id;
            }
            //get budget class templates
            $budgetClassTemplates = DB::table('budget_class_templates')
                ->select('id','description','code','type')
                ->get();
            foreach ($budgetClassTemplates as $template)
            {
                switch ($template->type)
                {
                    case 1:
                        $objective_code = $template->code;
                        //$objective_desc = $template->description;
                        break;
                    case 2:
                        $service_output_code = $template->code;
                        //$service_output__desc = $template->description;
                        break;
                    case 3:
                        $target_code = $template->code;
                        //$target_desc = $template->description;
                        break;
                    case 4:
                        $activity_code = $template->code;
                        //$activity_desc = $template->description;
                        break;
                    case 5:
                        $facility_code = $template->code;
                        $facility_desc = $template->description;
                }
            }

            //insert objective
            $objectiveCodeExist = DB::table('configuration_settings')
                ->where('key', 'OBJECTIVE_CODE')
                ->select('value')
                ->first();
            $plan_chain_type_id = $objectiveCodeExist->value;
            if($plan_chain_type_id != null)
            {
                $objective_code = isset($objective_code)?$objective_code:'0'; //use default code
                $objective_desc = isset($objective_desc)?$objective_desc:'Pay Personal Emoluments'; //use default code
                //check if objective is available
                $objectiveId = DB::table('plan_chains as p')
                    ->join('plan_chain_versions as pv', 'p.id','pv.plan_chain_id')
                    ->join('versions as v','v.id','pv.version_id')
                    ->where('v.is_current',true)
                    ->where('p.code',$objective_code)
                    ->select('p.id')
                    ->first();
                if(!isset($objectiveId->id))
                {
                    $message = ["errorMessage" => "OBJECTIVE IS NOT INITIALIZED"];
                    return response()->json($message, 404);
                }else
                {
                    $objective_id = $objectiveId->id;
                }
            }
            //objective end
            //insert service output
            $serviceOutputCodeExist = DB::table('configuration_settings')
                ->where('key', 'SERVICE_OUTPUT_CODE')
                ->select('value')
                ->first();
            $plan_chain_type2_id = $serviceOutputCodeExist->value;
            if($plan_chain_type2_id != null)
            {
                $service_output_code = isset($service_output_code)?$service_output_code:'0'; //use default code
                $service_output__desc = isset($service_output__desc)?$service_output__desc:'Pay Personal Emoluments'; //use default code
                //check if objective is available
                $serviceOutputId = DB::table('plan_chains as p')
                    ->join('plan_chain_versions as pv', 'p.id','pv.plan_chain_id')
                    ->join('versions as v','v.id','pv.version_id')
                    ->where('v.is_current',true)
                    ->where('p.code',$service_output_code)
                    ->select('p.id')
                    ->first();
                if(!isset($serviceOutputId->id))
                {
                    $message = (object) array("errorMessage" => "SERVICE OUTPUT IS NOT INITIALIZED");
                    return response()->json($message, 500);
                }else
                {
                    $service_output_id = $serviceOutputId->id;
                }
            }
            //service output end
            //get refrence doc
            $refDocument = DB::table('reference_documents as r')
                ->join('financial_years as eF','eF.id','=','r.end_financial_year')
                ->join('admin_hierarchies as a', 'a.id', 'r.admin_hierarchy_id')
                ->join('mtefs as m', 'm.admin_hierarchy_id', 'a.id')
                ->where('m.id','=',$mtef_id)
                ->where('eF.end_date','>=',date('Y-m-d H:i:s'))
                ->select('r.*')
                ->first();
            //insert long term target
            $longTermTargetExist = DB::table('long_term_targets')
                ->where('plan_chain_id',$service_output_id)
                ->where('reference_document_id',$refDocument->id)
                ->select('id')
                ->first();
            if(!isset($longTermTargetExist->id))
            {
                $long_term_target_id = DB::table('long_term_targets')
                    ->insertGetId(['plan_chain_id'=>$service_output_id,
                        'reference_document_id'=>$refDocument->id,
                        'description'=>$service_output__desc,
                        'is_active'=>true,
                        'code'=>$service_output_code]);
            }else
            {
                $long_term_target_id = $longTermTargetExist->id;
            }
            //insert annual target
            $annualTargetExist =  DB::table('mtef_annual_targets')
                ->where('mtef_id',$mtef_id)
                ->where('long_term_target_id',$long_term_target_id)
                ->where('section_id',$section_id)
                ->select('id')
                ->first();
            if(!isset($annualTargetExist->id))
            {
                $target_code = isset($target_code)?$target_code:'0000';
                $target_desc = isset($target_desc)?$target_desc:'Pay personal Emoluments';
                $annual_target_id = DB::table('mtef_annual_targets')
                    ->insertGetId(['description'=>$target_desc,
                        'code'=>$target_code,
                        'mtef_id'=>$mtef_id,
                        'long_term_target_id'=>$long_term_target_id,
                        'section_id'=>$section_id]);
            }else
            {
                $annual_target_id = $annualTargetExist->id;
            }
            //insert activity
            $activityExists = DB::table('activities as ac')
                ->join('mtef_sections as ms', 'ms.id', 'ac.mtef_section_id')
                ->where('ac.budget_class_id',$budget_class_id)
                ->where('ac.mtef_annual_target_id',$annual_target_id)
                ->where('ms.mtef_id',$mtef_id)
                ->where('ms.section_id', $section_id)
                ->select('ac.id')
                ->first();
            if(!isset($activityExists->id))
            {
                $activity_code = isset($activity_code)?$activity_code:'00000000';
                $activity_desc = isset($activity_desc)?$activity_desc:'Pay Personal Emoluments';
                $projects      = DB::table('projects')->where('code', '0000')->select('id')->first();
                if(isset($projects)){
                    $project_id = $projects->id;
                    $activity_id = DB::table('activities')
                        ->insertGetId(['budget_class_id'=>$budget_class_id,
                            'mtef_annual_target_id'=>$annual_target_id,
                            'mtef_section_id'=>$mtef_section_id,
                            'project_id'=>$project_id,
                            'code'=>$activity_code,
                            'description'=>$activity_desc,
                            'locked'=>false]);
                }

            }else
            {
                $activity_id = $activityExists->id;
            }
        }else {
            $activity_id = $activity->id;
        }

        if ($budget_class_id == 556677) {// if cealing is not including employees contributions change 556677 to 4
            $peBudgeted = DB::select(DB::raw("select sum(amount) amount from
            (select distinct round(coalesce(affsi.unit_price*affsi.quantity*affsi.frequency,0),2) amount
            from activity_facility_fund_source_inputs affsi
                join activity_facility_fund_source_input_breakdowns affsib on affsi.id=affsib.activity_facility_fund_source_input_id
                join activity_facility_fund_sources affs on affsi.activity_facility_fund_source_id = affs.id
                join activity_facilities af on affs.activity_facility_id = af.id
                join activities a on af.activity_id = a.id
                join budget_classes bc on a.budget_class_id = bc.id
                join mtef_sections ms on a.mtef_section_id = ms.id
                join mtefs m on ms.mtef_id = m.id
            where budget_class_id=$budget_class_id
            and section_id =$section_id
            and a.code = '00000000'
            and a.deleted_at IS NULL
            and has_breakdown is true
            and m.admin_hierarchy_id = $adminId
            and affsib.budget_submission_definition_id not in (50,51,52,53,54)
            and financial_year_id=$financial_year_id ) sub"));
        } else {
            $peBudgeted = DB::select(DB::raw("select sum(amount) amount from
        (select distinct round(coalesce(affsi.unit_price*affsi.quantity*affsi.frequency,0),2) amount, affsi.id
        from activity_facility_fund_source_inputs affsi
            join activity_facility_fund_source_input_breakdowns affsib on affsi.id=affsib.activity_facility_fund_source_input_id
            join activity_facility_fund_sources affs on affsi.activity_facility_fund_source_id = affs.id
            join activity_facilities af on affs.activity_facility_id = af.id
            join activities a on af.activity_id = a.id
            join budget_classes bc on a.budget_class_id = bc.id
            join mtef_sections ms on a.mtef_section_id = ms.id
            join mtefs m on ms.mtef_id = m.id
        where budget_class_id=$budget_class_id
        and section_id =$section_id
        and a.code = '00000000'
        and a.deleted_at IS NULL
        and has_breakdown is true
        and m.admin_hierarchy_id = $adminId
        and financial_year_id=$financial_year_id ) sub"));
        }
        
        
        $peCeiling = DB::select(DB::raw("select coalesce(sum(amount),0) amount from ceilings c
            join admin_hierarchy_ceilings ahc on c.id = ahc.ceiling_id
            where budget_class_id=$budget_class_id
            and admin_hierarchy_id=$adminId
            and section_id = $section_id
            and ahc.is_facility is false
            and financial_year_id=$financial_year_id"));
            
        $blnc = $peCeiling[0]->amount - $peBudgeted[0]->amount;
        $result = ['activity_id'=>$activity_id,
            'totalPECeiling'=>$peCeiling[0]->amount,
            'totalPEBudgeted'=>$peBudgeted[0]->amount,
            'balance' => $blnc
        ];
        return response()->json($result);
    }

}
