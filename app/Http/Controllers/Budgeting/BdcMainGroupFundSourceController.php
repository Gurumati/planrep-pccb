<?php

namespace App\Http\Controllers\Budgeting;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Budgeting\BdcMainGroupFundSource;
use App\Models\Setup\FinancialYear;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class BdcMainGroupFundSourceController extends Controller
{
    public function index() {
        $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
        $all = BdcMainGroupFundSource::with('financial_year','bdc_main_group','bdc_main_group.sector','fund_source','reference_document')
            ->where('is_active',true)
            ->where('financial_year_id',$financial_year_id)
            ->orderBy('created_at','desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
        return BdcMainGroupFundSource::with('financial_year','bdc_main_group','bdc_main_group.sector','fund_source','reference_document')
            ->where('is_active',true)
            ->where('financial_year_id',$financial_year_id)
            ->orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
        try{
            $data = json_decode($request->getContent());
            $obj = new BdcMainGroupFundSource();
            $obj->main_group_id = $data->main_group_id;
            $obj->financial_year_id = $financial_year_id;
            $obj->fund_source_id = $data->fund_source_id;
            $obj->budget_percent = $data->budget_percent;
            $obj->reference_document_id = isset($data->reference_document_id)?$data->reference_document_id:null;
            $obj->is_active = true;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_MAIN_GROUP_FUND_SOURCE_CREATED", "mainGroupFundSources" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = BdcMainGroupFundSource::find($data->id);
            $obj->main_group_id = $data->main_group_id;
            $obj->fund_source_id = $data->fund_source_id;
            $obj->budget_percent = $data->budget_percent;
            $obj->reference_document_id = $data->reference_document_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_MAIN_GROUP_FUND_SOURCE_UPDATED", "mainGroupFundSources" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = BdcMainGroupFundSource::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_MAIN_GROUP_FUND_SOURCE_DELETED", "mainGroupFundSources" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
        $all = BdcMainGroupFundSource::with('financial_year','bdc_main_group','bdc_main_group.sector','fund_source','reference_document')
            ->where('is_active',true)
            ->where('financial_year_id',$financial_year_id)
            ->orderBy('created_at','desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        BdcMainGroupFundSource::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "MAIN_GROUP_FUND_SOURCE_RESTORED", "trashedMainGroupFundSources" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        BdcMainGroupFundSource::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "MAIN_GROUP_FUND_SOURCE_DELETED_PERMANENTLY", "trashedMainGroupFundSources" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = BdcMainGroupFundSource::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "MAIN_GROUP_FUND_SOURCE_DELETED_PERMANENTLY", "trashedMainGroupFundSources" => $all];
        return response()->json($message, 200);
    }

    public function toggleActive(Request $request) {
        $data = json_decode($request->getContent());
        $object = BdcMainGroupFundSource::find($data->id);
        $object->is_active = $data->is_active;
        $object->save();
        if ($data->is_active == false) {
            $feedback = ["action" => "MAIN_GROUP_FUND_SOURCE_DEACTIVATED", "alertType" => "warning"];
        } else {
            $feedback = ["action" => "MAIN_GROUP_FUND_SOURCE_ACTIVATED", "alertType" => "success"];
        }
        return response()->json($feedback, 200);
    }
}
