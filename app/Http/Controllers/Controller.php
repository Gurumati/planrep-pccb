<?php

namespace App\Http\Controllers;

use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Http\Services\VersionServices;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $app_name = "Planning & Budget Execution Management System";
    public $app_initial = "PlanRep";
    public $is_logged_in = false;
    public $form_scripts = [];

    protected function is_logged_in()
    {
        if (!UserServices::isLogin()) {
            return redirect('auth/user/login')->with(['warning' => 'You are not yet logged in. Please login to continue']);
        }
    }

    public function sendToMUSE($data, $type)
    {
        $message = new AMQPMessage(json_encode($data));
        $queue_name = "PLANREPOTR_ACTIVITIES_TO_MUSE";
        $exchange = "MUSESegmentExchange";
        $exchange_type = "headers";
        $routing_key = "";
        $headers = new AMQPTable([
            'x-match' => 'all',
            'type' => $type,
        ]);
        $queue_bindings = new AMQPTable([
            'x-match' => 'all',
            'type' => $type,
        ]);
        sendToQueue($message, $queue_name, $exchange, $exchange_type, $routing_key, $headers, $queue_bindings);
    }

    public function send($data, $type)
    {
        $message = new AMQPMessage(json_encode($data));
        $queue_name = "PLANREP_OTR_FACILITIES_TO_MUSE_QUEUE";
        $exchange = "MUSEFACILITYExchange";
        $exchange_type = "headers";
        $routing_key = "";
        $headers = new AMQPTable([
            'x-match' => 'all',
            'type' => $type,
        ]);
        $queue_bindings = new AMQPTable([
            'x-match' => 'all',
            'type' => $type,
        ]);
        sendToQueue($message, $queue_name, $exchange, $exchange_type, $routing_key, $headers, $queue_bindings);
    }

    public function sendOpras($data, $type)
    {
        $message = new AMQPMessage(json_encode($data));
        $queue_name = "SEGMENT_TO_OPRAS_QUEUE";
        $exchange = "OPRASSegmentExchange";
        $exchange_type = "headers";
        $routing_key = "";
        $headers = new AMQPTable([
            'x-match' => 'all',
            'type' => $type,
        ]);
        $queue_bindings = new AMQPTable([
            'x-match' => 'all',
            'type' => $type,
        ]);
        sendToQueue($message, $queue_name, $exchange, $exchange_type, $routing_key, $headers, $queue_bindings);
    }

    public function segmentData($facility, $send = true)
    {
        $message = array();
        $item['id'] = $facility->id;
        $item['facilityCode'] = $facility->facility_code;
        $item['name'] = $facility->name;
        switch ($facility->facility_type->code) {
            case '002':
                $item['facilityType'] = 'ZN'; //Zonal office
                break;
            case '003':
                $item['facilityType'] = 'CP'; //Campus
                break;
            case '004':
                $item['facilityType'] = 'PT'; //Plants
                break;
            case '005':
                $item['facilityType'] = 'RO'; //Regional office
                break;
            case '006':
                $item['facilityType'] = 'DO'; //District office
                break;
            default:
                $item['facilityType'] = 'HQ'; //Head Quarters
        }
        $item['wardCode'] = $facility->admin_hierarchy->ward_code;
        $item['phoneNumber'] = $facility->phone_number;
        $item['email'] = $facility->email;
        $accounts = array();
        foreach ($facility->bank_accounts as $bank_account) {
            $itm['accountNumber'] = $bank_account->account_number;
            $itm['accountName'] = $bank_account->account_name;
            $itm['bankName'] = $bank_account->bank;
            array_push($accounts, $itm);
        }
        $item['bankAccounts'] = $accounts;
        $item['postalAddress'] = $facility->postal_address;
        $item['deletedAt'] = null;
        $item['active'] = $facility->is_active;

        // header and Signature
        $messageHeader = [
            "Sender" => "PLANREPOTR",
            "Receiver" => "MUSE",
            "MsgId" => "",
            "MessageType" => "FACILITIES",
            "Created_at" => date('Y-m-d H:i:s'),
        ];
        $item2 = [
            "messageHeader" => $messageHeader,
            "messageDetails" => $item,
        ];
        $data = json_encode($item2);
        $plainText = $data;
        $privateKey = openssl_pkey_get_private("file://" . base_path() . "/config/keys/private.pem");
        $publicKey = openssl_pkey_get_public("file://" . base_path() . "/config/keys/public.pem");

        // Make a signature
        openssl_sign($plainText, $signature, $privateKey, OPENSSL_ALGO_SHA256);
        $signature = base64_encode($signature);

        $payloadToMuse = [
            'message' => $data,
            'digitalSignature' => $signature,
        ];
        array_push($message, $payloadToMuse);
        if ($send == true) {
            try {
                $this->send($message[0], "FACILITIES");
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
    }

    public function clearCacheByKey($key)
    {
        try {
            Cache::forget($key);
            return 'Cache cleared';
        } catch (\Exception $e) {
            return 'failed to clear Cache';
        }
    }

    public function clearAllCache()
    {
        try {
            Cache::flush();
            return 'Cache cleared';
        } catch (\Exception $e) {
            return 'failed to clear Cache';
        }
    }

    /**
     * @param Request $request
     * @param null $calendar
     * @return mixed
     */
    public function getFinancialYearId(Request $request, $calendar = null)
    {
        if (isset($request->financialYearId)) {
            $financialYearId = $request->financialYearId;
        } else {
            if (!is_null($calendar)) {
                if ($calendar == 'PLANNING') {
                    $financialYear = FinancialYearServices::getPlanningFinancialYear();
                } else {
                    $financialYear = FinancialYearServices::getExecutionFinancialYear();
                }
            } else {
                $financialYear = FinancialYearServices::getPlanningFinancialYear();
                if (is_null($financialYear)) {
                    $financialYear = FinancialYearServices::getExecutionFinancialYear();
                }
            }
            $financialYearId = $financialYear->id;
        }
        return $financialYearId;
    }

    /**
     * @param Request $request
     * @param $financialYearId
     * @param $type
     * @return mixed
     */
    public function getVersionId(Request $request, $financialYearId, $type)
    {
        if ($request->versionId) {
            $versionId = $request->versionId;
        } else {
            $versionId = VersionServices::getVersionId($financialYearId, $type);
        }
        return $versionId;
    }

    /**
     * @param $financialYearId
     * @param $type
     * @return mixed
     */
    public function getInputVersionId($financialYearId, $type)
    {
        if (Input::get('versionId')) {
            $versionId = Input::get('versionId');
        } else {
            $versionId = VersionServices::getVersionId($financialYearId, $type);
        }
        return $versionId;
    }
}