<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;

class AASAdminController extends Controller
{
    public function __construct() {
        $this->middleware('aas_admin');
    }
    public function index(){
        if(UserServices::isLogin()){
            return view('dashboard.aas-administration',['js_scripts' => $this->form_scripts]);
        } else {
            return redirect('/auth/user/login');
        }
    }
}
