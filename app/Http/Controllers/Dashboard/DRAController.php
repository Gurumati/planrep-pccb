<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;

class DRAController extends Controller {
    public function __construct() {
        $this->middleware('dra');
    }

    public function index() {
        if (UserServices::isLogin()){
            return view('dashboard.dra', ['js_scripts' => $this->form_scripts]);
        } else {
            return redirect('auth/user/login');
        }
    }
}
