<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Budgeting\AdminHierarchyCeiling;
use App\Models\Dashboard\Dashboard;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\CeilingChain;
use App\Models\Setup\Facility;
use App\Models\Setup\Section;
use App\Models\Setup\SectionLevel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends Controller
{


    public function index(){
        if (UserServices::isLogin()){
            return view('dashboard.index',['js_scripts' => $this->form_scripts]);
        } else {
            return redirect('auth/user/login');
        }
    }

    public function getAdminHierarchyBudgetWithChildren($budgetType,$financialYearId,$adminHierarchyId,$sectionId){

        $budgets =[];
        $userChildTotal = 0;
        $userAdminHierarchyLevel = DB::table('admin_hierarchy_levels as l')->
                                      join('admin_hierarchies as a','a.admin_hierarchy_level_id','l.id')->
                                      where('a.id',$adminHierarchyId)->
                                      select('l.*')->first();

        $sectionStringIds = get_section_ids_string($sectionId);

        $peBudgetClassIdString = Flatten::getPeBudgetClassIdString();
        $peFundSourceIdString = Flatten::getPeFundSourceIdString();

        if($userAdminHierarchyLevel != null && $userAdminHierarchyLevel->hierarchy_position < 3) {
            $selectedChildAdmins = AdminHierarchy::where('parent_id', $adminHierarchyId)->get();
            foreach ($selectedChildAdmins as $admin) {
                $adm = new \StdClass();
                $adm->adminId = $admin->id;
                $adm->label = $admin->name;
                $adm->value = Dashboard::getAggregateBudget($budgetType,$financialYearId,get_admin_areas_ids_string($admin->id),$sectionStringIds,$peBudgetClassIdString,$peFundSourceIdString);
                $budgets[] = $adm;
                $userChildTotal = $userChildTotal + $admin->value;
            }
        }
        $data =['childHierarchyBudget'=>$budgets];
        return response()->json($data, 200);
    }

    public function getIncompleteCeilingAndBudget($budgetType,$financialYearId,$adminHierarchyId,$sectionId, $isTotal){
        $flatten= new Flatten();
        $sectionIds = get_section_ids_string($sectionId);
        $userAdminHierarchy = AdminHierarchy::where('id', $adminHierarchyId)->get();
        $adminIds = $flatten->flattenAdminHierarchyWithChildGetStringIds($userAdminHierarchy);
        return Dashboard::getIncompleteCeilingAndBudget($budgetType, $financialYearId, $adminIds, $sectionIds, $isTotal);
    }
    public function getTotalPECeilingAndTotalPEBudget($budgetType,$financialYearId,$adminHierarchyId,$sectionId)
    {
        $budgets =[];
        $sectionStringIds = get_section_ids_string($sectionId);

        $peBudgetClassIdString = Flatten::getPeBudgetClassIdString();
        $peFundSourceIdString = Flatten::getPeFundSourceIdString();

        $selectedAdminHierarchyLevel = DB::table('admin_hierarchy_levels as l')->
        join('admin_hierarchies as a','a.admin_hierarchy_level_id','l.id')->
        where('a.id',$adminHierarchyId)->
        select('l.*')->first();


        if($selectedAdminHierarchyLevel != null && $selectedAdminHierarchyLevel->hierarchy_position < 3) {

            $selectedChildrenAdminAreas = AdminHierarchy::where('parent_id', $adminHierarchyId)->get();
            foreach ($selectedChildrenAdminAreas as $childAdminArea) {

                $adm = new \StdClass();
                $childAdminAreaId = $childAdminArea->id;
                $adm->adminId = $childAdminAreaId;
                $adm->label = $childAdminArea->name;
                $adm->budget = Dashboard::getAggregatePEBudget($budgetType,$financialYearId,get_admin_areas_ids_string($childAdminAreaId),$sectionStringIds,$peBudgetClassIdString,$peFundSourceIdString );
                $adm->ceiling = Dashboard::getAggregatePECeiling($budgetType,$financialYearId,get_admin_areas_ids_string($childAdminAreaId),$sectionId,$peBudgetClassIdString);
                $adm->completion = ($adm->ceiling !=0)?($adm->budget/$adm->ceiling)*100:0;
                $budgets[] = $adm;
            }
        }


        return response()->json($budgets, 200);

    }

    public function getUserSection()
    {
        $user = UserServices::getUser();
        $name = $user->section->name;
        return response()->json($name, 200);
    }

    public function getPEBudgetByPECeiling($budgetType,$financialYearId,$adminHierarchyId,$sectionId)
    {
        $data = Dashboard::getPEBudgetAndPECeiling($budgetType,$financialYearId,$adminHierarchyId,$sectionId,get_section_ids_string($sectionId));
        return response()->json($data, 200);
    }

    public function getTotalBudgetByClass($budgetType,$financialYearId,$adminHierarchyId,$sectionId)
    {
        $data = Dashboard::getTotalBudgetByClass($budgetType,$financialYearId,$adminHierarchyId,$sectionId,get_section_ids_string($sectionId));
        return response()->json($data, 200);
    }

    public function getTotalCeilingAgainstTotalBudget($budgetType,$financialYearId,$adminHierarchyId,$sectionId){
        $budgets =[];
        $sectionStringIds = get_section_ids_string($sectionId);

        $peBudgetClassIdString = Flatten::getPeBudgetClassIdString();
        $peFundSourceIdString = Flatten::getPeFundSourceIdString();

        $selectedAdminHierarchyLevel = DB::table('admin_hierarchy_levels as l')->
                                        join('admin_hierarchies as a','a.admin_hierarchy_level_id','l.id')->
                                        where('a.id',$adminHierarchyId)->
                                        select('l.*')->first();


        if($selectedAdminHierarchyLevel != null && $selectedAdminHierarchyLevel->hierarchy_position < 3) {

            $selectedChildrenAdminAreas = AdminHierarchy::where('parent_id', $adminHierarchyId)->get();
            foreach ($selectedChildrenAdminAreas as $childAdminArea) {

                $adm = new \StdClass();
                $childAdminAreaId = $childAdminArea->id;
                $adm->adminId = $childAdminAreaId;
                $adm->label = $childAdminArea->name;
                $adm->budget = Dashboard::getAggregateBudget($budgetType,$financialYearId,get_admin_areas_ids_string($childAdminAreaId),$sectionStringIds,$peBudgetClassIdString,$peFundSourceIdString );
                $adm->ceiling = Dashboard::getAggregateCeiling($budgetType,$financialYearId,get_admin_areas_ids_string($childAdminAreaId),$sectionId,$peBudgetClassIdString);
                $adm->completion = ($adm->ceiling !=0)?($adm->budget/$adm->ceiling)*100:0;
                $budgets[] = $adm;
            }
        }


        return response()->json($budgets, 200);
    }

    public function getProcurableAgainstTotalBudget($budgetType,$financialYearId,$adminHierarchyId,$sectionId){

        $data = Dashboard::getProcurableAgainstTotalBudget($budgetType,$financialYearId,$adminHierarchyId,$sectionId,get_section_ids_string($sectionId));
        return response()->json($data, 200);
    }

    public function getBudgetAndCeilingBySector($budgetType,$financialYearId,$adminHierarchyId,$sectionId){
        $adminAreaIds = $adminHierarchyId;
        $data = Dashboard::getAggregateBudgetAndCeilingBySector($budgetType,$financialYearId,$adminAreaIds,$sectionId);
        return response()->json($data, 200);
    }

    public function getPercentageByStatus($budgetType,$financialYearId,$adminHierarchyId,$sectionId){

        $query = "select 'APPROVED' as label, count(m.id) as value from mtefs as m
                LEFT JOIN decision_levels dl on dl.id = m.decision_level_id
                where m.financial_year_id = ".$financialYearId." and m.admin_hierarchy_id in (".get_admin_areas_ids_string($adminHierarchyId).") and m.is_final=true
                UNION ALL
                select 'NOT APPROVED' as status, count(m.id) as total from mtefs as m
                  LEFT JOIN decision_levels dl on dl.id = m.decision_level_id
                where m.financial_year_id = ".$financialYearId." and m.admin_hierarchy_id in (".get_admin_areas_ids_string($adminHierarchyId).") and m.is_final=false;";

        $data = DB::select($query);
        $data =['byStatus'=>$data];
        return response()->json($data, 200);

    }

    public function getPercentageByDecisionLevel($budgetType,$financialYearId,$adminHierarchyId,$sectionId){

        $query ="Select decisionLevel as label, sum(total) as value from
                  (
                    select name as decisionLevel, 0 as total from decision_levels
                    UNION DISTINCT
                    select dl.name as decisionLevel, count(m.id) as total from mtefs as m
                    LEFT JOIN decision_levels dl on dl.id = m.decision_level_id
                    where m.financial_year_id = ".$financialYearId." and m.admin_hierarchy_id in (".get_admin_areas_ids_string($adminHierarchyId).")
                    GROUP BY dl.name
                  ) x
                  GROUP BY decisionLevel";
        $data = DB::select($query);

        $data =['byDecisionLevel'=>$data];
        return response()->json($data, 200);
    }


    public function budgetBySector($budgetType,$financialYearId,$adminHierarchyId,$sectionId){

        $query = "Select y.sector as label ,coalesce(SUM(y.budget+x.budget),0) as value FROM
                  (Select id, name as sector, 0 as budget from sectors where id IN (".get_sector_ids_string($sectionId)."))  as y
                   left JOIN (
                  SELECT
                    max(se.id) AS sectorId,
                    se.name                                      AS sector,
                    SUM(i.unit_price * i.quantity * i.frequency) AS budget
                  FROM activity_facility_fund_source_inputs AS i
                    INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
                    INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
                    INNER JOIN activities AS a ON a.id = af.activity_id
                    INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
                    INNER JOIN mtefs AS m ON m.id = ms.mtef_id
                    INNER JOIN sections AS s ON s.id = ms.section_id
                    INNER JOIN sectors AS se ON se.id = s.sector_id
                  WHERE a.budget_type='".$budgetType."' and a.code <>'000000' and i.deleted_at is null and m.admin_hierarchy_id = ".$adminHierarchyId." AND m.financial_year_id = ".$financialYearId." and s.id IN (".get_section_ids_string($sectionId).")
                  GROUP BY se.name
                ) as x on x.sectorId = y.id GROUP BY y.sector";

        $data['bySector'] = DB::select($query);
        return response()->json($data, 200);

    }

    public function budgetByBudgetClass($budgetType,$financialYearId,$adminHierarchyId,$sectionId){

        $query ="SELECT
                  bcp.name  AS budgetClass,
                  bc.name   AS label,
                  SUM(i.unit_price * i.quantity * i.frequency) AS value
                FROM activity_facility_fund_source_inputs AS i
                  INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
                  INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
                  INNER JOIN activities AS a ON a.id = af.activity_id
                  INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
                  INNER JOIN mtefs AS m ON m.id = ms.mtef_id
                  INNER JOIN sections AS s ON s.id = ms.section_id
                  INNER JOIN budget_classes AS bc ON bc.id = a.budget_class_id
                  INNER JOIN budget_classes AS bcp ON bcp.id = bc.parent_id
                WHERE a.budget_type= '".$budgetType."' and a.code <>'000000' and i.deleted_at is null and m.admin_hierarchy_id = ".$adminHierarchyId." AND m.financial_year_id = ".$financialYearId." and  s.id in (".get_section_ids_string($sectionId).")
                GROUP BY bcp.name, bc.name";

        $data['byBudgetClass'] = DB::select($query);
        return response()->json($data, 200);

    }

    public function budgetByFundSource($budgetType,$financialYearId,$adminHierarchyId,$sectionId){
        try {
            $query = "SELECT
                  fu.name   AS label,
                  SUM(i.unit_price * i.quantity * i.frequency) AS value
                FROM activity_facility_fund_source_inputs AS i
                  INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
                  INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
                  INNER JOIN activities AS a ON a.id = af.activity_id
                  INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
                  INNER JOIN mtefs AS m ON m.id = ms.mtef_id
                  INNER JOIN sections AS s ON s.id = ms.section_id
                  INNER JOIN fund_sources AS fu ON fu.id = aff.fund_source_id
                WHERE a.budget_type= '".$budgetType."' and a.code <>'000000' and i.deleted_at is null and m.admin_hierarchy_id = " . $adminHierarchyId . " AND m.financial_year_id = " .$financialYearId. " and  s.id in (" . get_section_ids_string($sectionId). ")
                GROUP BY fu.name";

            $data['byFundSource'] = DB::select($query);
            return response()->json($data, 200);
        }
        catch (\Exception $exception){
            Log::error($exception);
            return response()->json(['byFundSource'=>[]],500);
        }
    }

    public function budgetByCostCentre($budgetType,$financialYearId,$adminHierarchyId,$sectionId){
        $query = "SELECT
                  s.name   AS label,
                  SUM(i.unit_price * i.quantity * i.frequency) AS value
                 FROM activity_facility_fund_source_inputs AS i
                  INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
                  INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
                  INNER JOIN activities AS a ON a.id = af.activity_id
                  INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
                  INNER JOIN mtefs AS m ON m.id = ms.mtef_id
                  INNER JOIN sections AS s ON s.id = ms.section_id
                  WHERE a.budget_type='".$budgetType."' and a.code <>'000000' and i.deleted_at is null and m.admin_hierarchy_id = ".$adminHierarchyId." AND m.financial_year_id = ".$financialYearId." and  s.id in (".get_section_ids_string($sectionId).")
                GROUP BY s.name";

        $data['byCostCentre'] = DB::select($query);
        return response()->json($data, 200);
    }

    public function getProjection($budgetType,$financialYearId,$adminHierarchyId,$sectionId){

        $query ="SELECT f.name as fundSource,sum(adc.amount) as total from admin_hierarchy_ceilings as adc
                 INNER JOIN ceilings as c on c.id = adc.ceiling_id
                 INNER JOIN gfs_codes as gfs on gfs.id =c.gfs_code_id
                 INNER JOIN fund_sources as f ON f.id = gfs.fund_source_id
                 WHERE adc.budget_type= '".$budgetType."' and adc.admin_hierarchy_id in (".get_admin_areas_ids_string($adminHierarchyId).") and
                 adc.financial_year_id=".$financialYearId." and
                 c.is_aggregate=false and
                 adc.is_facility=false and f.can_project =true
                 GROUP BY f.name";

        $result = DB::select($query);
        $data = $result;
        return response()->json($data, 200);
    }

    public function budgetByCeiling($budgetType,$financialYearId,$adminHierarchyId,$sectionId){

        $data = Dashboard::getBudgetAndCeiling($budgetType,$financialYearId,$adminHierarchyId,$sectionId,get_section_ids_string($sectionId));
        return response()->json($data, 200);
    }

    public function getTotalCeilingProblem($budgetType, $financialYearId, $adminHierarchyId, $sectionId){
        try {
            $flatten = new Flatten();
            $userAdminHierarchy = AdminHierarchy::where('id', $adminHierarchyId)->get();
            $Ids = $flatten->flattenAdminHierarchyWithChildGetStringIds($userAdminHierarchy);
            $total = Dashboard::getProblemCeiling($budgetType,$financialYearId,$Ids,true);
           // return response()->json(['total'=>$total[0]->count], 200);
            return response()->json(['total'=>0], 200);
        }
        catch (\Exception $exception){
            Log::error($exception);
            return response()->json(['errorMessage'=>'ERROR_GETTING_TOTAL'], 500);
        }
    }

    public function getCeilingProblem($budgetType,$financialYearId,$adminHierarchyId, $sectionId){
        try {
            $flatten = new Flatten();
            $userAdminHierarchy = AdminHierarchy::where('id', $adminHierarchyId)->get();
            $Ids = $flatten->flattenAdminHierarchyWithChildGetStringIds($userAdminHierarchy);
            $data = Dashboard::getProblemCeiling($budgetType,$financialYearId,$Ids,false);
            return response()->json(['data'=>$data], 200);
        }
        catch (\Exception $exception){
            Log::error($exception);
            return response()->json(['errorMessage'=>'ERROR_GETTING_TOTAL'], 500);
        }
    }

    public function getAllocationProblem($budgetType,$financialYearId,$adminHierarchyId,$sectionId){
        try {
            $flatten = new Flatten();

            $userAdminHierarchy = AdminHierarchy::where('id', $adminHierarchyId)->get();
            $adminIds = $flatten->flattenAdminHierarchyWithChildGetStringIds($userAdminHierarchy);
            $sectionLevelPosition =SectionLevel::getBySectionId($sectionId)->hierarchy_position;
            $currentCeilingPosition = CeilingChain::getCurrentCeilingChain(3,$sectionLevelPosition);

            if($sectionLevelPosition === 4) {
                $data = Dashboard::getProblemByFacilityAllocation($budgetType,$adminIds, $financialYearId, $sectionId, Facility::getAllFacilityIdsStringByAdminAndSection($adminHierarchyId,$sectionId));
            }else{
                $nextCeilingPosition = AdminHierarchyCeiling::getNextCeilingChain($currentCeilingPosition->next_id);
                $sectionLevel = SectionLevel::getByPosition( $nextCeilingPosition ->section_level_position);
                $sections = Section::where('id', $sectionId)->get();
                $nextSectionIds = $flatten->flattenSectionWithChildOnlyWithoutSector($sections, $sectionId,  $sectionLevel->id);
                $nextSectionIdsString = '0';
                if(sizeof($nextSectionIds)>0){
                    $nextSectionIdsString =implode(',',$nextSectionIds);
                }
                $data = Dashboard::getProblemAllocation($budgetType,$adminIds, $financialYearId, $sectionId, $nextSectionIdsString);
            }
            return response()->json(['data'=>$data], 200);
        }
        catch (\Exception $exception){
            Log::error($exception);
            return response()->json(['errorMessage'=>'ERROR_GETTING_DATA'], 500);
        }
    }

    public function downloadIncomplete($budgetType, $financialYearId, $adminHierarchyId) {

        $result = Dashboard::getIncomplete($budgetType, $financialYearId, $adminHierarchyId);
        $array = array();
        foreach($result as $item){
            $data = array();
            $data['ADMIN_AREA'] = $item->adminarea;
            $data['SECTION'] =  $item->section;
            $data['FACILITY'] = $item->facility;
            $data['FUND_SOURCE'] =  $item->fund_source;
            $data['BUDGET_CLASS'] = $item->budget_class;
            $data['CEILING'] = $item->ceiling;
            $data['BUDGET'] = $item->budget;
            array_push($array, $data);
        }
        Excel::create('INCOMPLETE BUDGET AND CEILING' . time(), function ($excel) use ($array) {
            $excel->sheet('DATA', function ($sheet) use ($array) {
                $sheet->fromArray($array);
            });
        })->export('csv');
    }


}
