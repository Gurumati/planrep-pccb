<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 4/8/2017
 * Time: 11:09 AM
 */

namespace App\Http\Controllers;
use App\Http\Services\UserServices;
use App\Models\Planning\AdminHierarchySectMappings;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\ReferenceDocumentType;
use App\Models\Setup\Section;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SharedService {

  public static function getUserStrategicPlan($financialYear){

    $docType              =  null;
    $strategicPlanDocType =  ConfigurationSetting::where('key','STRATEGIC_LONG_TERM_REFERENCE_DOCUMENT_TYPE')->first();
    $value = $strategicPlanDocType->value;
    if($value != null){
      $docType = ReferenceDocumentType::where('id',(int)$value)->first();
    } else {
      $docType = ReferenceDocumentType::where('name','StrategicPlan')->first();
    }

    try {
    $refDocument=DB::table('reference_documents as r')
      ->join('financial_years as sF','sF.id','=','r.start_financial_year')
      ->join('financial_years as eF','eF.id','=','r.end_financial_year')
      ->where('r.admin_hierarchy_id','=',UserServices::getUser()->admin_hierarchy_id)
      ->where('sF.start_date','<=',$financialYear->start_date)
      ->where('r.reference_document_type_id','=',$docType->id)
      ->where('eF.end_date','>=',$financialYear->end_date)->select('r.*')->first();
      } catch (QueryException $e){
        $refDocument =['id'=>null];
      }
    return $refDocument;
  }

    public static function userCanTarget(){
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $userAdminHierarchy=AdminHierarchy::find($admin_hierarchy_id);
        $sectionId = UserServices::getUser()->section_id;

        $canTarget = DB::table('admin_hierarchy_lev_sec_mappings as sm')
            ->distinct()
            ->where('sm.can_target', true)
            ->where('sm.admin_hierarchy_level_id',$userAdminHierarchy->admin_hierarchy_level_id)
            ->where('sm.section_id',$sectionId)
            ->count();
        if($canTarget >0)
            return true;
        else
            return false;
    }
  public function returnCanTargetSection($sectionId,$adminHierarchyId) {

    $end      =  false;
    $inside   =  false;
    $value    =  0;
    $lSection =  $sectionId;
    $p        =  [$sectionId];
    $arr      =  [];

    while($end == false) {
      $sections = Section::find($lSection);
      //  $sectionLevel = SectionLevel::find($sections->section_level_id);
      $adminHierarchy = AdminHierarchy::find($adminHierarchyId);
      $mapping = AdminHierarchySectMappings::where('admin_hierarchy_level_id',$adminHierarchy->admin_hierarchy_level_id)->where('section_id', $lSection)->first();
      if($mapping !=null && $mapping->can_target) {
        $value =  $lSection;
        $arr[] =  $lSection;
        $end   =  true;
      } else {
        $count = Section::where('id',$sections->parent_id)->count();
        if($count > 0 ) {
          $lSection = $sections->parent_id;
        } else {
          while($inside == false) {
            $count = Section::whereIn('parent_id',$p)->count();
            if($count > 0 ) {
              $sections = Section::whereIn('parent_id',$p)->get();
              $p = [];
              foreach ($sections as $section) {
                // $arr[] = $section->id;
                $mappingN = AdminHierarchySectMappings::where('admin_hierarchy_level_id',$adminHierarchy->admin_hierarchy_level_id)
                  ->where('section_id', $section->id)
                  ->where('can_target',true)
                  ->count();
                if($mappingN > 0 ) {
                  $mapping = AdminHierarchySectMappings::where('admin_hierarchy_level_id', $adminHierarchy->admin_hierarchy_level_id)
                    ->where('section_id', $section->id)
                    ->where('can_target', true)
                    ->first();
                  $arr[] = $mapping->section_id;
                  $inside = true;
                  $end = true;
                } else {
                  $p[] = $section->id;

                }
              }
            } else {
              $value = $lSection;
              $arr[] =  $lSection;
              $inside = true;
              $end = true;
            }
            $value = $lSection;
            $arr[] =  $lSection;
            $end = true;
          }
        }
      }
    }
    return $arr;
  }
}
