<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/30/2017
 * Time: 10:58 AM
 */

namespace App\Http\Controllers;

use App\Http\Services\UserServices;
use App\Models\CacheKeys;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\GeoLocation;
use App\Models\Setup\Section;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Flatten
{
    private $sectionIds = array();
    private $planChainIds = array();
    private $sectorIds = array();
    private $adminSectionIds = array();
    private $adminHierarchies = array();
    private $gfsCodeIds = array();
    private $adminLevelIds = array();
    private $sectionLevelIds = array();
    private $nextSectionId = null;
    private $Id = null;
    private $Ids = array();
    private $hierarchiesNames = array();
    private $sectionNames = array();
    private $idString = '';
    private $sectorIdString = '';
    private $adminIdsString = '';
    private $csWsIds = array();
    private $adminIds = array();


    public function flattenSectionWithChild($array)
    {

        foreach ($array as $value) {
            $this->sectionIds[] = $value->id;
            if (isset($value->childSections) && sizeof($value->childSections) > 0) {
                $this->flattenSectionWithChild($value->childSections);
            }
        }
        return $this->sectionIds;
    }

    public function flattenAdminWithChildOnly($array, $parentId, $adminLevelIds)
    {
        foreach ($array as $value) {
            if ($value->id != $parentId && in_array($value->admin_hierarchy_level_id, $adminLevelIds)) {
                $this->adminIds[] = $value->id;
            }
            if (isset($value->children) && sizeof($value->children) > 0) {
                $this->flattenAdminWithChildOnly($value->children,$parentId, $adminLevelIds);
            }
        }
        return $this->adminIds;
    }

    public function flattenSectionWithChildOnly($array, $parent, $sectors, $levelId)
    {

        foreach ($array as $value) {

            if ($value->section_level_id == $levelId && $value->id != $parent && ($value->sector_id == null || in_array($value->sector_id, $sectors))) {
                $this->sectionIds[] = $value->id;
            }
            if (isset($value->childSections) && sizeof($value->childSections) > 0) {
                $this->flattenSectionWithChildOnly($value->childSections, $parent, $sectors, $levelId);
            }
        }
        return $this->sectionIds;
    }
    public function flattenSectionWithChildOnlyWithoutSector($array, $parent, $levelId)
    {

        foreach ($array as $value) {
            if ($value->id != $parent && $value->section_level_id == $levelId) {
                $this->csWsIds[] = $value->id;
            }
            if (isset($value->childSections) && sizeof($value->childSections) > 0) {
                $this->flattenSectionWithChildOnlyWithoutSector($value->childSections, $parent, $levelId);
            }
        }
        return $this->csWsIds;
    }

    public function flattenSection($array)
    {
        foreach ($array as $value) {
            $this->sectionIds[] = $value->id;
        }
        return $this->sectionIds;
    }

    public function getFirstParentByPosition($userParentAdminHierarchies, $hierarchyPosition)
    {
        foreach ($userParentAdminHierarchies as $value) {

            if ($value->admin_hierarchy_level->hierarchy_position == $hierarchyPosition) {
                $this->Id = $value->id;
                break;
            } else if (sizeof($value->parentArray) > 0) {
                $this->getFirstParentByPosition($value->parentArray, $hierarchyPosition);
            }
        }
        return $this->Id;

    }

    public function flattenSectionWithChildGetLevels($array)
    {

        foreach ($array as $value) {
            $this->sectionLevelIds[] = $value->section_level_id;
            if (sizeof($value->childSections) > 0) {
                $this->flattenSectionWithChildGetLevels($value->childSections);
            }
        }
        return $this->sectionLevelIds;
    }

    public function flattenSectionWithChildIdsAsString($array)
    {

        foreach ($array as $value) {
            $this->idString = $this->idString . $value->id . ',';
            if (sizeof($value->childSections) > 0) {
                $this->flattenSectionWithChildIdsAsString($value->childSections);
            }
        }
        return trim($this->idString, ',');
    }

    public function flattenSectionWithChildrenGetSectorsIdsString($array)
    {
        foreach ($array as $value) {
            $this->sectorIdString = $this->sectorIdString . $value->sector_id . ',';
            if (sizeof($value->childSections) > 0) {
                $this->flattenSectionWithChildrenGetSectorsIdsString($value->childSections);
            }
        }
        return trim($this->sectorIdString, ',');
    }

    public function flattenSections($array)
    {
        foreach ($array as $value) {
            $this->sectionIds[] = $value->id;
        }
        return $this->sectionIds;
    }

    public function flattenSectors($array)
    {
        foreach ($array as $value) {
            $this->sectorIds[] = $value->id;
        }
        return $this->sectorIds;
    }

    public function flattenAdminHierarchies($array)
    {
        foreach ($array as $value) {
            $this->adminHierarchies[] = $value->id;
        }
        return $this->adminHierarchies;
    }

    public function flattenAdminHierarchyWithChild($array)
    {
        foreach ($array as $value) {
            $this->adminSectionIds[] = $value->id;
            if ($value->admin_hierarchy_level->hierarchy_position < 4) {
                if (sizeof($value->children) > 0) {
                    $this->flattenAdminHierarchyWithChild($value->children);
                }
            }

        }
        return $this->adminSectionIds;
    }

    public function flattenGeoLocationWithChild($array)
    {
        foreach ($array as $value) {
            $this->locationSectionIds[] = $value->id;
            if ($value->geo_location_level->geo_level_position < 3) {
                if (sizeof($value->children) > 0) {
                    $this->flattenGeoLocationWithChild($value->children);
                }
            }

        }
        return $this->locationSectionIds;
    }

    public function flattenAdminHierarchyWithChildToALevel($array, $level)
    {
        foreach ($array as $value) {
            $this->adminSectionIds[] = $value->id;
            if ($value->admin_hierarchy_level->hierarchy_position < $level) {
                if (sizeof($value->children) > 0) {
                    $this->flattenAdminHierarchyWithChildToALevel($value->children, $level);
                }
            }

        }
        return $this->adminSectionIds;
    }

    public function flattenAdminHierarchyWithChildGetStringIds($array)
    {
        foreach ($array as $value) {
            $this->adminIdsString = $this->adminIdsString . $value->id . ',';
            if ($value->admin_hierarchy_level->hierarchy_position < 4) {
                if (sizeof($value->children) > 0) {
                    $this->flattenAdminHierarchyWithChildGetStringIds($value->children);
                }
            }

        }
        return trim($this->adminIdsString, ',');
    }

    public function flattenAdminHierarchyWithChildGelLevels($array)
    {

        foreach ($array as $value) {
            $this->adminLevelIds[] = $value->admin_hierarchy_level_id;
            if ($value->admin_hierarchy_level->hierarchy_position < 4) {
                if (sizeof($value->children) > 0) {
                    $this->flattenAdminHierarchyWithChildGelLevels($value->children);
                }
            }

        }
        return $this->adminLevelIds;
    }

    public function flattenUserAdminHierarchiesGetNames($array)
    {
        foreach ($array as $value) {
            $this->hierarchiesNames[] = $value->name;
            if ($value->admin_hierarchy_level->hierarchy_position < 4) {
                if (sizeof($value->children) > 0) {
                    $this->flattenUserAdminHierarchiesGetNames($value->children);
                }
            }
        }
        return $this->hierarchiesNames;
    }

    public function flattenSectionGetLevels($array)
    {

        foreach ($array as $value) {
            $this->sectionIds[] = $value->section_level_id;
            if (sizeof($value->childSections) > 0) {
                $this->flattenSectionWithChild($value->childSections);
            }
        }
        return $this->sectionIds;
    }

    public function flattenSectionWithParent($array)
    {
        foreach ($array as $value) {
            $this->sectionIds[] = $value->id;
            if (sizeof($value->parent) > 0) {
                $this->flattenSectionWithParent($value->parent);
            }
        }
        return $this->sectionIds;
    }

    public function flattenPlanChainWithParent($array)
    {
        foreach ($array as $value) {
            $this->planChainIds[] = $value->id;
            if (sizeof($value->parent) > 0) {
                $this->flattenPlanChainWithParent($value->parent);
            }
        }
        return $this->planChainIds;
    }

    public function getNextForwardSectionId($array, $levelId)
    {
        foreach ($array as $value) {
            if ($value->section_level_id == $levelId) {
                $this->nextSectionId = $value->id;
            }
            if (sizeof($value->parent) > 0) {
                $this->getNextForwardSectionId($value->parent, $levelId);
            }
        }
        return $this->nextSectionId;
    }

    public function flattenSectionWithSameSectorParent($array, $sectorId)
    {
        foreach ($array as $value) {
            if ($value->sector_id == $sectorId) {
                $this->sectionIds[] = $value->id;
            }
            if (sizeof($value->parent) > 0) {
                $this->flattenSectionWithSameSectorParent($value->parent, $sectorId);
            }
        }
        return $this->sectionIds;
    }

    public function getAutoBudgetTemplatesCodes()
    {
        $autoBudgetClasses = BudgetClass::with('budgetClassTemplates')->where('is_automatic', true)->get();
        $codes = array();
        foreach ($autoBudgetClasses as $b) {
            foreach ($b->budgetClassTemplates as $template) {
                $codes[] = $template->code;
            }
        }
        return $codes;
    }

    public function flattenBudgetClassGetIds($array)
    {
        foreach ($array as $value) {
            $this->Ids[] = $value->id;
            if (sizeof($value->children) > 0) {
                $this->flattenBudgetClassGetIds($value->children);
            }
        }
        return $this->Ids;
    }

    public function flattenSectionWithParentGetSectors($array)
    {
        foreach ($array as $value) {
            $this->sectorIds[] = $value->sector_id;
            if (sizeof($value->parent) > 0) {
                $this->flattenSectionWithParent($value->parent);
            }
        }
        return $this->sectorIds;
    }

    public function flattenAdminHierarchiesWithParentGetIds($value)
    {
        //foreach ($array as $value) {
        $this->Ids[] = $value->id;
        if (isset($value->parent)) {
            $this->flattenAdminHierarchiesWithParentGetIds($value->parent);
        }
        // }
        return $this->Ids;
    }

    public function flattenSectionWithChildrenGetSectors($array)
    {
        foreach ($array as $value) {
            $this->sectorIds[] = $value->sector_id;
            if (sizeof($value->childSections) > 0) {
                $this->flattenSectionWithChildrenGetSectors($value->childSections);
            }
        }

        return $this->sectorIds;
    }


    public function getCellingGfsCodes()
    {
        $gfsCode = DB::table('gfs_codes as gfs')->
        join('fund_sources as fc', 'fc.id', 'gfs.fund_source_id')->
        where('fc.can_project', false)->select('gfs.*')->get();

        foreach ($gfsCode as $code) {
            $this->gfsCodeIds[] = $code->id;
        }

        return $this->gfsCodeIds;

    }

    public function getIds($array, $column)
    {
        $Ids = [];
        foreach ($array as $a) {
            $Ids[] = $a->gfs_code_id;
        }
        return $Ids;
    }

    public function getUserExistingReferenceDocumentTypesIds()
    {
        $today = date('Y-m-d');
        $userAdminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $userReferenceDocuments = DB::table('reference_documents as rd')->
        join('financial_years as sf', 'sf.id', 'rd.start_financial_year')->
        join('financial_years as ef', 'ef.id', 'rd.end_financial_year')->
        where('ef.end_date', '>=', $today)->
        where('rd.admin_hierarchy_id', $userAdminHierarchyId)->
        select('rd.*')->
        get();
        $Ids = [];
        foreach ($userReferenceDocuments as $r) {
            $Ids[] = $r->reference_document_type_id;
        }
        return $Ids;
    }

    public function getUserHierarchyTree()
    {
        return AdminHierarchy::getAdminHierarchyTree(UserServices::getUser()->admin_hierarchy_id);

    }

    public function getUserLocationTree()
    {
        return GeoLocation::getGeolocationTree(UserServices::getUser()->admin_hierarchy_id);

    }

    public function getHierarchyTreeById($adminHierarchyId)
    {
        $adminLevelPosition = DB::table('admin_hierarchy_levels as al')->
                                        join('admin_hierarchies as a', 'al.id', 'a.admin_hierarchy_level_id')->
                                        where('a.id', $adminHierarchyId)->first()->hierarchy_position;
        $withString = array();
        if ($adminLevelPosition == 1) {
            $withString = array();
            array_push($withString, 'children');
            array_push($withString, 'children.children');
            array_push($withString, 'children.children.children');
        } else if ($adminLevelPosition == 2) {
            $withString = array();
            array_push($withString, 'children');
            array_push($withString, 'children.children');
        } else if ($adminLevelPosition == 3){
            $withString = array();
            array_push($withString, 'children');
        }

        $adminHierarchy = AdminHierarchy::with($withString)->where('id', $adminHierarchyId)->select('id', 'name', 'parent_id', 'admin_hierarchy_level_id')->get();


        return $adminHierarchy;
    }

    public function getLocationTreeById($geoLocationId)
    {
        $locationLevelPosition = DB::table('geographical_location_levels as gl')->
                                        join('geographical_locations as g', 'gl.id', 'g.geo_location_level_id')->
                                        where('g.id', $geoLocationId)->first()->geo_level_position;
        $withString = array();
        if ($locationLevelPosition == 1) {
            $withString = array();
            array_push($withString, 'children');
            array_push($withString, 'children.children');
        } else if ($locationLevelPosition == 2) {
            $withString = array();
            array_push($withString, 'children');
        }

        $geoLocation = GeoLocation::with($withString)->where('id', $geoLocationId)->select('id', 'name', 'parent_id', 'geo_location_level_id')->get();

        return $geoLocation;
    }

    public function flattenUserSectionGetNames($array)
    {
        foreach ($array as $value) {
            $this->sectionNames[] = $value->name;
            if (isset($value->childSections) && sizeof($value->childSections) > 0) {
                $this->flattenUserSectionGetNames($value->childSections);
            }
        }
        return $this->sectionNames;
    }

    public function userTargetingSections()
    {
        $userSectionId = UserServices::getUser()->section_id;
        $userAdminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $userAdminHierarchy = DB::table('admin_hierarchies')->where('id', $userAdminHierarchyId)->first();
        $userSections = Section::where('id', $userSectionId)->get();
        $flatten = new Flatten();
        $sectionIds = $flatten->flattenSectionWithChild($userSections);

        $canTarget = DB::table('admin_hierarchy_lev_sec_mappings as sm')
            ->join('sections as s', 's.id', 'sm.section_id')
            ->leftJoin('sectors as sec', 'sec.id', 's.sector_id')
            ->leftJoin('section_levels as sl', 'sl.id', 's.section_level_id')
            ->distinct()
            ->where('sm.can_target', True)
            ->where('sm.admin_hierarchy_level_id', $userAdminHierarchy->admin_hierarchy_level_id)
            ->whereIn('sm.section_id', $sectionIds)
            ->select('s.id', 's.name', 's.code', 's.sector_id', 'sec.name as sector', 'sl.name as section_level')->orderBy('s.sector_id')->get();
        return $canTarget;
    }

    public static function userBudgetingSections()
    {
        $excluded = ConfigurationSetting::getValueByKey("COST_CENTRE_NOT_BUDGETING");
        if($excluded === null){
            $excluded =[0];
        }
        $userSectionId = UserServices::getUser()->section_id;
        $userAdminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $userAdminHierarchy = DB::table('admin_hierarchies')->where('id', $userAdminHierarchyId)->first();
        $flatten = new Flatten();

        if ($userSectionId != null) {
            $userSections = Section::where('id', $userSectionId)->get();
        } else {
            $userSections = Section::whereNull('parent_id')->get();
        }
        $sectionIds = $flatten->flattenSectionWithChild($userSections);

        $canBudget = DB::table('admin_hierarchy_lev_sec_mappings as sm')
            ->join('sections as s', 's.id', 'sm.section_id')
            ->leftJoin('sectors as sec', 'sec.id', 's.sector_id')
            ->leftJoin('section_levels as sl', 'sl.id', 's.section_level_id')
            ->join('admin_hierarchy_sections as ahs', 's.id', 'ahs.section_id')
            ->distinct()
            ->where('sm.can_budget', true)
            ->whereNotIn('s.id',$excluded)
            ->where('sm.admin_hierarchy_level_id', $userAdminHierarchy->admin_hierarchy_level_id)
            ->where('ahs.admin_hierarchy_id', $userAdminHierarchyId)
            ->whereIn('sm.section_id', $sectionIds)
            ->select('s.id', 's.name', 's.code', 's.sector_id', 'sec.name as sector', 'sl.name as section_level')->orderBy('s.code')->get();
            return $canBudget;
        
    }
    public static function userBudgetingSectionIdString()
    {
        return Cache::rememberForever(CacheKeys::BUDGETING_SECTION_IDS_STRINGS, function(){
            $ids ='0,';
            $canBudget = DB::table('mtef_sections')->distinct('section_id')->pluck('section_id');
            foreach ($canBudget as $c){
                $ids= $ids.$c.',';
            }
            return trim($ids,',');
        });

    }

    public static function getPeBudgetClassIdString(){

        $peBudgetClassIds = ConfigurationSetting::getValueByKey('PE_CEILINGS');
        $peBudgetClassIdString ='0,';
        if(isset($peBudgetClassIds)) {
            $peBudgetClassIdString = $peBudgetClassIdString . implode(",", $peBudgetClassIds);
        }
        else{
            $peBudgetClassIdString = trim($peBudgetClassIdString,',');
        }
        return $peBudgetClassIdString;

    }
    public static function getPeFundSourceIdString(){

        // return Cache::rememberForever(CacheKeys::PE_FUND_SOURCE_IDS_STRING, function(){
            $peFundSourceIds = ConfigurationSetting::getValueByKey('PE_FUND_SOURCES');
            $peFundSourceIdString ='0,';
            if(isset($peFundSourceIds)) {
                $peFundSourceIdString = $peFundSourceIdString . implode(",", $peFundSourceIds);
            }
            else{
                $peFundSourceIdString = trim($peFundSourceIdString,',');
            }
            return $peFundSourceIdString;
        // });

    }


    public function userTargetingSectionsIds()
    {
        $Ids = [];
        foreach ($this->userTargetingSections() as $s) {
            $Ids[] = $s->id;
        }
        return $Ids;
    }

}
