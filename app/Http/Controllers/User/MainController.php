<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Auth\User;
use App\Models\User\Message;

class MainController extends Controller {
    public function index() {
        $title = "User Dashboard|" . $this->app_initial;
        return view('user.index', ['js_scripts' => $this->form_scripts, 'title' => $title]);
    }

    public function profile() {
        $user_id = UserServices::getUser()->id;
        $user = User::find($user_id);
        return response()->json($user);
    }

    public function messages() {
       $messages = Message::orderBy('created_at','desc')->get();
        return response()->json($messages);
    }
}
