<?php

namespace App\Http\Controllers\Authentication;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class LoginController extends Controller {

    use AuthenticatesUsers;

    public $maxLoginAttempts = 3; // Default is 5
    public $lockoutTime = 60; // Default is 1

    protected function login() {
        return view("authentication.login");
    }

    public function user() {
        $message = ['adm_hierarchy' => UserServices::getUser()->admin_hierarchy_id, "section" => UserServices::getUser()->section_id];
        return response()->json($message, 200);
    }

    protected function postLogin(Request $request) {
        return UserServices::authenticate($request->all(), $request);
    }

    protected function logout() {
        UserServices::logout();
        return redirect('/login')->with(['success' => 'Login to Continue']);
    }

    //     protected function hasTooManyLoginAttempts(Request $request)
    // {
    //     // return $this->limiter()->tooManyAttempts(
    //     //     $this->throttleKey($request), $this->maxAttempts, $this->decayMinutes
    //     // );
    //     return $this->limiter()->tooManyAttempts(
    //         $this->throttleKey($request), 3, 10
    //     );
    // }
}
