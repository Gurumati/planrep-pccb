<?php

namespace App\Http\Controllers\Authentication;

use Activation;
use App\Http\Controllers\Controller;
use App\Models\Authentication\User;

class ActivationController extends Controller
{
    public function activate($email, $activationCode)
    {
        $user  = User::whereEmail($email)->first();

        if(Activation::Complete($user, $activationCode))
        {
          return redirect("/login");
        }
        else
        {

        }

    }
    //
}
