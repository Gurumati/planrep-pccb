<?php

namespace App\Http\Controllers\Authentication;

use Activation;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Authentication\Role;
use App\Models\Authentication\User;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\Section;
use Illuminate\Http\Request;
use Validator;


class RegistrationController extends Controller {
    public function register() {
        return view("authentication.register"
            ,
            [
                'js_scripts' => $this->form_scripts,
                'title' => "User Registration|" . $this->app_initial,
                'page_title' => 'User Registration',
                'sections' => Section::all(),
                'adminHierarchies' => AdminHierarchy::all(),
                'roles' => Role::all(),
                'breadcrumbs' => ['Setup' => url('/setup'), 'Users' => url('auth/users'), 'Register' => url('#')]
            ]
        );

    }

    //

    public function postRegister(Request $request) {
        $array = $request->all();
        $array["password"] = $array['last_name'];
        $user = UserServices::register($array);

        return redirect("/attachroles/" . $user->email);
    }

    public function attachRoles($email) {
        return view("authentication.attachRoles"
            ,
            [
                'js_scripts' => $this->form_scripts,
                'title' => "Attach Roles|" . $this->app_initial,
                'page_title' => 'Attach Roles',
                'roles' => Role::all(),
                'email' => $email,
                'breadcrumbs' => ['Setup' => url('/setup'), 'Users' => url('auth/users'), 'Register' => url('#')]
            ]
        );
    }

    public function postAttachRoles(Request $request) {
        $roles = $request->all()["role"];

        foreach ($roles as $role) {
            $foundRole = Role::where('name',$role->name)->first();

            $user = User::whereEmail($request->input("email"))->first();
            $foundRole->users()->attach($user);

        }

        return redirect("/users");


    }


}
