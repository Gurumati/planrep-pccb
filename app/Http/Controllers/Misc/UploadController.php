<?php

namespace App\Http\Controllers\Misc;
use App\Http\Controllers\Controller;
use App\Models\Setup\AdminHierarchyLevel;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class UploadController extends Controller {

    public function showUploadFile(Request $request) {
        $file = Input::file('fileToUpload');
        $fileName='public\uploads\\'.$file->getClientOriginalName();
        $file->move('uploads', $file->getClientOriginalName());
        Excel::load($fileName, function($reader) {
            $reader->each(function($sheet) {
                $currentRow=2;
                $sheet->each(function($row) use ($currentRow) {
                    if($row['type'] ==null && $row['name'] == null && $row['code'] && $row['parent']==null){

                    }
                    else{
                        if($row['type'] == null) {
                            $message = ["errorMessage" => "ERROR_UPLOAD_COLUMN_REQUIRED", "rowNumber" => $currentRow,"columnName"=>"Type"];
                            return response()->json($message, 400);
                        }else{
                            $adminLevel=AdminHierarchyLevel::where('name','like',$row['type'])->count();
                            if($adminLevel == 0) {
                                $message = ["errorMessage" => "ERROR_UPLOAD_TYPE_DOEST_EXISTS", "rowNumber" => $currentRow];
                                return response()->json($message, 400);
                            }
                        }
                    }
                });
            });

        });
    }

    public function isNull($column){

    }

}
