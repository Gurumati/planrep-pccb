<?php

namespace App\Http\Controllers\Misc;

use App\Http\Controllers\Controller;
use App\Http\Services\CustomPager;
use App\Models\Execution\ActivityExpenditure;
use App\Models\Setup\PlanChain;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainController extends Controller {
    public function base_url(){
        $base_url = env('APP_URL');
        $response = ['base_url'=>$base_url];
        return response()->json($response);
    }
    public function pusher(){
        $options = array('encrypted' => true);
        $pusher = new \Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'), $options);
        $data['message'] = 'hello world';
        $pusher->trigger('my-channel', 'my-event', $data);
    }

    public function oprasSetups(Request $request)
    {
       $segmentId = $request->segment_id;
       $financialYearId = $request->financialYearId;
        $perPage = 20;
       if ($segmentId==1){
           $items = PlanChain::whereNull("parent_id")->get();
          return response()->json(['qryData' => $items]);
       }
       if ($segmentId==2){
           set_time_limit(6000);
           if (isset($request->perPage)) {
               $perPage = $request->perPage;
           } else {
               $perPage = 10;
           }
           return response()->json(['annualTargets' => ActivityExpenditure::annualTargets($financialYearId, $perPage)], 200);

       }
       if ($segmentId==3){
           set_time_limit(6000);
           if (isset($request->perPage)) {
               $perPage = $request->perPage;
           } else {
               $perPage = 10;
           }
           return response()->json(['annualActivities' => ActivityExpenditure::activities($financialYearId, $perPage)], 200);
       }
    }
}
