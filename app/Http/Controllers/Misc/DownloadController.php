<?php

namespace App\Http\Controllers\Misc;

use App\Http\Controllers\Controller;

class DownloadController extends Controller {
    /*public function download($file_name) {
        $file_path = public_path('downloads/'.$file_name);
        return response()->download($file_path);
    }*/

    public function download($file_name) {
        $file_path = public_path('templates/'.$file_name);
        return response()->download($file_path);
    }
}
