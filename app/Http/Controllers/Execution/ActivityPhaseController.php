<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Execution\ActivityPhase;
use App\Models\Execution\ActivityPhaseMilestone;
use App\Models\Execution\ActivityProjectOutput;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class ActivityPhaseController extends Controller
{
    public function index()
    {
        $sections = Flatten::userBudgetingSections();
        $keyArray = array();
        foreach ($sections as $section) {
            $id = $section->id;
            array_push($keyArray, $id);
        }

        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $financialYearId = $financialYear->id;

        $user = UserServices::getUser();
        $admin_hierarchy_id = $user->admin_hierarchy_id;
        $all = DB::table("activity_phases as ap")
            ->join("activities as a", "ap.activity_id", "=", "a.id")
            ->join("activity_phase_milestones as am", "am.activity_phase_id", "=", "ap.id")
            ->join("mtef_sections as ms", "ms.id", "=", "a.mtef_section_id")
            ->join("sections as s", "s.id", "=", "ms.section_id")
            ->join("mtefs as m", "m.id", "=", "ms.mtef_id")
            ->join("financial_years as f", "f.id", "=", "m.financial_year_id")
            ->join("admin_hierarchies as ah", "ah.id", "=", "m.admin_hierarchy_id")
            ->select("ap.*", "a.description as activity", DB::raw("string_agg(am.description,',') as milestones"))
            ->whereIn('s.id', $keyArray)->where("f.id", $financialYearId)->where("ah.id", $admin_hierarchy_id)
            ->groupBy("ap.id", "a.id")
            ->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage)
    {
        $sections = Flatten::userBudgetingSections();
        $keyArray = array();
        foreach ($sections as $section) {
            $id = $section->id;
            array_push($keyArray, $id);
        }

        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $financialYearId = $financialYear->id;

        $user = UserServices::getUser();
        $admin_hierarchy_id = $user->admin_hierarchy_id;

        $all = DB::table("activity_phases as ap")
            ->join("activities as a", "ap.activity_id", "=", "a.id")
            ->join("activity_phase_milestones as am", "am.activity_phase_id", "=", "ap.id")
            ->join("mtef_sections as ms", "ms.id", "=", "a.mtef_section_id")
            ->join("sections as s", "s.id", "=", "ms.section_id")
            ->join("mtefs as m", "m.id", "=", "ms.mtef_id")
            ->join("financial_years as f", "f.id", "=", "m.financial_year_id")
            ->join("admin_hierarchies as ah", "ah.id", "=", "m.admin_hierarchy_id")
            ->select("ap.*", "a.description as activity", DB::raw("string_agg(am.description,',') as milestones"))
            ->whereIn('s.id', $keyArray)->where("f.id", $financialYearId)->where("ah.id", $admin_hierarchy_id)
            ->groupBy("ap.id", "a.id")
            ->paginate($perPage);
        return $all;
    }

    public function paginated(Request $request)
    {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function sectionBudgetClassesWithActivity(Request $request)
    {
        $sectionId = $request->sectionId;
        $user = UserServices::getUser();
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        if(!isset($financialYear)){
            $financialYear = FinancialYearServices::getExecutionFinancialYear();
        }
        $financialYearId = $financialYear->id;
        $adminHierarchyId = $user->admin_hierarchy_id;
        $result = ActivityPhase::budgetClasses($adminHierarchyId, $financialYearId, $sectionId);
        return response()->json(['items' => $result], 200);
    }

    public function activities(Request $request)
    {

        $sectionId = $request->sectionId;
        $budgetClassId = $request->budgetClassId;
        $fundSourceId = $request->fundSourceId;

        $user = UserServices::getUser();
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $financialYearId = $financialYear->id;
        $admin_hierarchy_id = $user->admin_hierarchy_id;
        $activities = ActivityPhase::activities($financialYearId, $admin_hierarchy_id, $sectionId, $budgetClassId, $fundSourceId, $request->perPage, $request->page);
        return response()->json(["activities" => $activities], 200);
    }

    public function restoreLostData(Request $request)
    {

        $sectionId = $request->sectionId;
        $budgetClassId = $request->budgetClassId;
        $fundSourceId = $request->fundSourceId;

        $user = UserServices::getUser();
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $financialYearId = $financialYear->id;
        $admin_hierarchy_id = $user->admin_hierarchy_id;
        $activities = ActivityPhase::lostData($financialYearId, $admin_hierarchy_id, $sectionId, $budgetClassId, $fundSourceId, $request->perPage, $request->page);
        foreach ($activities as $activity) {
            $activityId = $activity->id;
            $facilityId = $activity->facility_id;
            //MILESTONES
            $milestones = DB::select("select m.id from activity_phase_milestones m join activity_phases p on p.id = m.activity_phase_id join activities a on a.id = p.activity_id where a.id = '$activityId'");
            foreach ($milestones as $milestone) {
                $mil = ActivityPhaseMilestone::find($milestone->id);
                $mil->facility_id = $facilityId;
                $mil->save();
            }

            //OUTPUT
            $outputs = ActivityProjectOutput::where("activity_id", $activityId)->get();
            foreach ($outputs as $output) {
                $output->facility_id = $facilityId;
                $output->save();
            }
        }
    }

    public function costCentreFundSources(Request $request)
    {
        $section_id = $request->sectionId;
        $budgetClassId = $request->budgetClassId;
        $user = UserServices::getUser();
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        if(!isset($financialYear)){
            $financialYear = FinancialYearServices::getExecutionFinancialYear();
        }
        $financialYearId = $financialYear->id;
        $admin_hierarchy_id = $user->admin_hierarchy_id;
        $result = ActivityPhase::costCentreFundSources($admin_hierarchy_id, $financialYearId, $section_id, $budgetClassId);
        return response()->json(["fund_sources" => $result], 200);
    }

    public function milestones(Request $request)
    {
        $result = $this->allPhases($request->activityId, $request->facility_id);
        return response()->json(["milestones" => $result], 200);
    }

    public function outputs(Request $request)
    {
        $activityId = $request->activityId;
        $facilityId = $request->facility_id;
        $result = ActivityProjectOutput::with('project_output', 'project_output.expenditure_category', 'project_output.expenditure_category.project_type')
            ->where('activity_id', $activityId)
            ->where('facility_id', $facilityId)
            ->get();
        return response()->json(["outputs" => $result], 200);
    }

    public function periods(Request $request)
    {
        $activityId = $request->activityId;
        $sql = "select p.id,p.name,p.start_date,p.end_date from periods p
                join activity_periods ap on ap.period_id = p.id
                join activities a on ap.activity_id = a.id
                where a.id = '$activityId' order by p.name asc";
        $result = DB::select($sql);
        return response()->json(["periods" => $result], 200);
    }


    public function addPhase(Request $request)
    {

        $activityId = $request->activityId;
        $facilityId = $request->facility_id;
        $data = array_merge($request->all(), ["activity_id" => $activityId]);
        $validator = Validator::make($data, ActivityPhase::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errorMessage" => "Form Error", "errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            DB::transaction(function () use ($request, $activityId, $facilityId) {
                $af = new ActivityPhase();
                $af->activity_id = $activityId;
                $af->description = $request->description;
                $af->save();
                $activity_phase_id = $af->id;
                $milestones = $request['milestones'];
                foreach ($milestones as $milestone) {
                    $afm = new ActivityPhaseMilestone();
                    $afm->activity_phase_id = $activity_phase_id;
                    $afm->description = $milestone['description'];
                    $afm->amount = $milestone['amount'];
                    $afm->period_id = $milestone['period_id'];
                    $afm->facility_id = $facilityId;
                    $afm->start_date = date("Y-m-d", strtotime($milestone['start_date']));
                    $afm->end_date = date("Y-m-d", strtotime($milestone['end_date']));
                    $afm->working_days = $this->number_of_working_days(date("Y-m-d", strtotime($milestone['start_date'])), date("Y-m-d", strtotime($milestone['end_date'])));
                    $afm->save();
                }
                if (isset($request['outputs']) && count($request['outputs']) > 0) {
                    $outputs = $request['outputs'];
                    foreach ($outputs as $output) {
                        $activityProjectOutput = new ActivityProjectOutput();
                        $activityProjectOutput->activity_id = $activityId;
                        $activityProjectOutput->project_output_id = $output['projectOutputId'];
                        $activityProjectOutput->facility_id = $facilityId;
                        $activityProjectOutput->value = $output['value'];
                        $activityProjectOutput->save();
                    }
                }
            });
            $all = $this->allPhases($request->activityId, $facilityId);
            $message = ["successMessage" => "Activity Main Task, Sub-milestones and Project Output Added Successfully", "milestones" => $all];
            return response()->json($message, 200);
        }
    }

    public function removePhase(Request $request)
    {
        $id = $request->id;
        $activity_id = $request->activityId;
        $facilityId = $request->facility_id;
        $mils = ActivityPhaseMilestone::where("activity_phase_id", $request->id)->get();
        foreach ($mils as $mil) {
            $mil->delete();
        }
        $afm = ActivityPhase::find($id);
        $afm->delete();
        $all = $this->allPhases($activity_id, $facilityId);
        $message = ["successMessage" => "Phase Removed Successfully", "phases" => $all];
        return response()->json($message, 200);
    }

    public function updateMainTaskData(Request $request)
    {
        $id = $request->id;
        $activity_id = $request->activityId;
        $facilityId = $request->facility_id;
        $description = $request->description;
        $ap = ActivityPhase::find($id);
        $ap->description = $description;
        $ap->save();
        $result = $this->allPhases($activity_id, $facilityId);
        $message = ["successMessage" => "Phase Updated Successfully", "milestones" => $result];
        return response()->json($message, 201);
    }

    public function addMileStone(Request $request)
    {
        try {
            $afm = new ActivityPhaseMilestone();
            $afm->activity_phase_id = $request->activity_phase_id;
            $afm->description = $request->description;
            $afm->amount = $request->amount;
            $afm->period_id = $request->period_id;
            $afm->facility_id = $request->facility_id;
            $afm->start_date = date("Y-m-d", strtotime($request->start_date));
            $afm->end_date = date("Y-m-d", strtotime($request->end_date));
            $afm->working_days = $this->number_of_working_days(date("Y-m-d", strtotime($request->start_date)), date("Y-m-d", strtotime($request->end_date)));
            $afm->save();
            $all = $this->allPhases($request->activityId, $request->facility_id);
            $message = ["successMessage" => "Milestone Added Successfully", "milestones" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function updateMileStoneData(Request $request)
    {
        $afm = ActivityPhaseMilestone::find($request->id);
        $afm->activity_phase_id = $request->activity_phase_id;
        $afm->description = $request->description;
        $afm->amount = $request->amount;
        $afm->period_id = $request->period_id;
        $afm->facility_id = $request->facility_id;
        $afm->start_date = date("Y-m-d", strtotime($request->start_date));
        $afm->end_date = date("Y-m-d", strtotime($request->end_date));
        $afm->working_days = $this->number_of_working_days(date("Y-m-d", strtotime($request->start_date)), date("Y-m-d", strtotime($request->end_date)));
        $afm->save();
        $all = $this->allPhases($request->activityId, $request->facility_id);
        $message = ["successMessage" => "Milestone Updated Successfully", "milestones" => $all];
        return response()->json($message, 200);
    }

    public function removeMileStone(Request $request)
    {
        $id = $request->id;
        $activityId = $request->activityId;
        $facilityId = $request->facility_id;
        $afm = ActivityPhaseMilestone::find($id);
        $afm->delete();
        $all = $this->allPhases($activityId, $facilityId);
        $message = ["successMessage" => "Milestone Removed Successfully", "milestones" => $all];
        return response()->json($message, 200);
    }

    public function addProjectOutput(Request $request)
    {
        $activityId = $request->activityId;
        $facilityId = $request->facility_id;
        //check if exist
        $result = ActivityProjectOutput::where('activity_id', $activityId)
                                    ->where('facility_id', $facilityId);
        if($result->count() == 0){
            $activityProjectOutput = new ActivityProjectOutput();
        }  else{
            $activityProjectOutput = $result->first();
        }
        $activityProjectOutput->activity_id = $activityId;
        $activityProjectOutput->project_output_id = $request->projectOutputId;
        $activityProjectOutput->facility_id = $request->facility_id;
        $activityProjectOutput->value = $request->value;
        $activityProjectOutput->save();
       
        $result = ActivityProjectOutput::with('project_output', 'project_output.expenditure_category', 'project_output.expenditure_category.project_type')
            ->where('activity_id', $activityId)
            ->where("facility_id", $facilityId)
            ->get();
        return response()->json(["outputs" => $result, "successMessage" => "Project Output Added successfully!"], 200);
    }

    public function editProjectOutput(Request $request)
    {
        $activityId = $request->activityId;
        $facilityId = $request->facility_id;
        $id = $request->id;
        try {
            $activityProjectOutput = ActivityProjectOutput::find($id);
            $activityProjectOutput->project_output_id = $request->projectOutputId;
            $activityProjectOutput->value = $request->value;
            $activityProjectOutput->save();
        } catch (\Exception $e) {

        }
        $result = ActivityProjectOutput::with('project_output', 'project_output.expenditure_category', 'project_output.expenditure_category.project_type')
            ->where('activity_id', $activityId)
            ->where("facility_id", $facilityId)
            ->get();
        return response()->json(["outputs" => $result, "successMessage" => "Project Output Updated successfully!"], 200);
    }

    public function removeOutput(Request $request)
    {
        $id = $request->id;
        $activityId = $request->activityId;
        $facilityId = $request->facility_id;
        try {
            $afm = ActivityProjectOutput::find($id);
            $afm->delete();
        } catch (\Exception $e) {

        }
        $result = ActivityProjectOutput::with('project_output', 'project_output.expenditure_category', 'project_output.expenditure_category.project_type')
            ->where('activity_id', $activityId)
            ->where("facility_id", $facilityId)
            ->get();
        return response()->json(["outputs" => $result, "successMessage" => "Project Output removed successfully!"], 200);
    }


    public function update(Request $request)
    {
        try {
            $af = ActivityPhase::find($request->id);
            $af->activity_id = $request->activity_id;
            $af->description = $request->description;
            $af->start_date = date("Y-m-d", strtotime($request->start_date));
            $af->end_date = date("Y-m-d", strtotime($request->end_date));
            $af->save();
            $all = $this->getAllPaginated(10);
            $message = ["successMessage" => "Activity Phase Updated Successfully!", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id)
    {
        $data = ActivityPhase::find($id);
        $data->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "Activity Phase Deleted Successfully!", "items" => $all];
        return response()->json($message, 200);
    }

    public function allPhases($activityId, $facilityId)
    {
        $result = DB::table("activity_phase_milestones as m")
            ->join("activity_phases as p", "m.activity_phase_id", "p.id")
            ->join("periods as q", "m.period_id", "q.id")
            ->join("activities as a", "p.activity_id", "a.id")
            ->select("m.id", "m.description", "m.amount", "m.start_date", "m.end_date", "m.working_days", "q.id as period_id", "q.name as quarter", "p.id as activity_phase_id", "p.description as phase")
            ->where("a.id", $activityId)
            ->where("m.facility_id", $facilityId)
            ->get();
        return $result;
    }

    public function number_of_working_days($from, $to)
    {
        $workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
        $holidayDays = ['*-12-25', '*-01-01', '*-04-26', '*-05-01', '*-07-07', '*-08-08', '*-12-09']; # variable and fixed holidays

        $from = new \DateTime($from);
        $to = new \DateTime($to);
        $to->modify('+1 day');
        $interval = new \DateInterval('P1D');
        $periods = new \DatePeriod($from, $interval, $to);

        $days = 0;
        foreach ($periods as $period) {
            if (!in_array($period->format('N'), $workingDays)) continue;
            if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
            if (in_array($period->format('*-m-d'), $holidayDays)) continue;
            $days++;
        }
        return $days;
    }

    public function activityProjectOutputs(Request $request)
    {
        $activityId = $request->activity_id;
        $result = $this->getUncleanProjectOutput($activityId);

        return response()->json(['outputs' => $result], 200);
    }

    public function activityMilestones(Request $request)
    {
        $activityId = $request->activity_id;
        $result = $this->getUncleanMilestones($activityId);
        return response()->json(['milestones' => $result], 200);
    }

    public function getUncleanProjectOutput($activityId)
    {
        $result = ActivityProjectOutput::with('project_output', 'project_output.expenditure_category', 'project_output.expenditure_category.project_type')
            ->where('activity_id', $activityId)
            ->whereNull("facility_id")
            ->orWhere("facility_id", 0)
            ->get();
        return $result;
    }

    public function getUncleanMilestones($activityId)
    {
        $result = DB::table("activity_phase_milestones as m")
            ->join("activity_phases as p", "m.activity_phase_id", "p.id")
            ->join("periods as q", "m.period_id", "q.id")
            ->join("activities as a", "p.activity_id", "a.id")
            ->select("m.id", "m.description", "m.amount", "m.start_date", "m.end_date", "m.working_days", "q.id as period_id", "q.name as quarter", "p.id as activity_phase_id", "p.description as phase")
            ->where("a.id", $activityId)
            ->whereNull("m.facility_id")
            ->orWhere("m.facility_id", 0)
            ->get();
        return $result;
    }

    public function retrieveMilestone(Request $request)
    {
        $activity_phase_id = $request->activity_phase_id;
        $facilityId = $request->facilityId;
        $activityId = $request->activityId;
        $milestones = ActivityPhaseMilestone::where("activity_phase_id", $activity_phase_id)->get();
        foreach ($milestones as $milestone) {
            $milestone->facility_id = $facilityId;
            $milestone->save();
        }
        $result = $this->getUncleanMilestones($activityId);
        return response()->json(['milestones' => $result, 'successMessage' => "Milestone Retrieved Successfully!"], 200);
    }

    public function retrieveProjectOutput(Request $request)
    {
        $facilityId = $request->facilityId;
        $activityId = $request->activityId;
        $outputs = ActivityProjectOutput::where("activity_id", $activityId)->get();
        foreach ($outputs as $output) {
            $output->facility_id = $facilityId;
            $output->save();
        }
        $result = $this->getUncleanProjectOutput($activityId);
        return response()->json(['outputs' => $result, 'successMessage' => "Sub-milestones Retrieved Successfully!"], 200);
    }
}
