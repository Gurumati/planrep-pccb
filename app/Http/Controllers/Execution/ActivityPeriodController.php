<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Services\CustomPager;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Planning\ActivityPeriod;
use App\Models\Setup\ConfigurationSetting;
use Illuminate\Http\Request;
use App\Models\Setup\Section;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Flatten;

class ActivityPeriodController extends Controller
{

    public function getAllPaginated($budgetClassId, $fundSourceId, $budgetType, $periodId, $perPage,$searchQuery = null)
    {

        $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        //$financialYear = FinancialYearServices::getExecutionFinancialYear();
        $financialYearId = ConfigurationSetting::getValueByKey('AUTO_FINANCIAL_YEAR_TO_REPORT');
        $section_id = UserServices::getUser()->section_id;
        $section = Section::where('id', $section_id)->get();
        $flatten = new Flatten();
        $sectorIds = implode(',',$flatten->flattenSectionWithChildrenGetSectors($section));
        $sql = "select activity.*,percentage.completion_percentage from(select distinct a.id,a.description,a.code,ap.overall_achievement from activities a
                join activity_facilities af on af.activity_id = a.id
                join activity_periods ap on a.id = ap.activity_id
                join activity_facility_fund_sources afs on af.id = afs.activity_facility_id
                join mtef_sections m2 on a.mtef_section_id = m2.id
                join mtefs m3 on m2.mtef_id = m3.id
                where m3.admin_hierarchy_id = $adminHierarchyId
                  and m3.financial_year_id = $financialYearId
                  and a.deleted_at is null
                  and ap.period_id = $periodId
                  and a.budget_type = '$budgetType'
                  and a.budget_class_id = $budgetClassId
                  and afs.fund_source_id = $fundSourceId) activity
                join (select a.id,round (avg(af.completion_percentage)::DECIMAL, 2)::TEXT as completion_percentage from activities a
                join activity_facilities af on af.activity_id = a.id
                join activity_periods ap on a.id = ap.activity_id
                join activity_facility_fund_sources afs on af.id = afs.activity_facility_id
                join mtef_sections m2 on a.mtef_section_id = m2.id
                join sections sc on sc.id = m2.section_id
                join sectors st on st.id = sc.sector_id
                join mtefs m3 on m2.mtef_id = m3.id
                where m3.admin_hierarchy_id = $adminHierarchyId
                  and m3.financial_year_id = $financialYearId
                  and a.deleted_at is null
                  and ap.period_id = $periodId
                  and st.id in ($sectorIds)
                  and a.budget_type = '$budgetType'
                  and a.budget_class_id = $budgetClassId
                  and afs.fund_source_id = $fundSourceId group by a.id) percentage on activity.id = percentage.id";
        if(!is_null($searchQuery)){
            $sql .= " and (activity.code ILIKE '%$searchQuery%' OR activity.description ILIKE '%$searchQuery%')";
        }
        $sql .=  " order by activity.code asc";
        $items = DB::select($sql);
        return CustomPager::paginate($items, $perPage);
    }

    public function paginated(Request $request)
    {
        $budgetClassId = $request->budgetClassId;
        $fundSourceId = $request->fundSourceId;
        $budgetType = $request->budgetType;
        $periodId = $request->periodId;
        $perPage = $request->perPage;
        if (isset($request->searchQuery)) {
            $searchQuery = $request->searchQuery;
            $all = $this->getAllPaginated($budgetClassId, $fundSourceId, $budgetType, $periodId, $perPage, $searchQuery);
        } else {
            $all = $this->getAllPaginated($budgetClassId, $fundSourceId, $budgetType, $periodId, $perPage);
        }
        return response()->json($all);
    }

    public function comment(Request $request)
    {
        $activityId = $request->activityId;
        $periodId = $request->periodId;
        $achievement = $request->overall_achievement;

        DB::statement("UPDATE activity_periods SET overall_achievement = '$achievement' 
                      WHERE activity_id = $activityId 
                      AND period_id = $periodId");

        $budgetClassId = $request->budgetClassId;
        $fundSourceId = $request->fundSourceId;
        $budgetType = $request->budgetType;
        $periodId = $request->periodId;
        $perPage = $request->perPage;
        $all = $this->getAllPaginated($budgetClassId, $fundSourceId, $budgetType, $periodId, $perPage);
        $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }
}
