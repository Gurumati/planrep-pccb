<?php

namespace App\Http\Controllers\Execution;

use App\EsbHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Models\Execution\BudgetExportTransaction;
use App\Models\Execution\ReadDataFromMuse;
use App\Models\Execution\ReadDataFromErms;
use Carbon\Carbon;

use Illuminate\Support\Facades\Log;

class SocialSecurityIntegrationController extends Controller
{

    public function getDataFromNavision(Request $request)
    {
        try {
            $esbHelper = new EsbHelper();

            $esbRequest = $esbHelper->verifyAndGetData($request->getContent());

            if (!$esbRequest) {
                return $esbHelper->failureResponse("Signature verification failed!", "json");
            }

            $requestId = $esbRequest["requestId"];
            $requestPayload = $esbRequest["esbBody"];

            Log::info("request from pssf: " . json_encode($requestPayload));
            $data = json_encode($requestPayload);
            $orgMsgId = $data["message"]["messageHeader"]["msgId"];
            $messageType = $data["message"]["messageHeader"]["messageType"];
            try {
                if($messageType != "RESPONSE"){
                   return $this->acknowledgement($orgMsgId,$messageType);
                }
              } catch (\Throwable $th) {
                  return;
              } finally {
                if($messageType == "RESPONSE"){
                  $this->handleResponse($data);
                 } else {
                   $this->handleImplementation($data);
                 }
              }
            // $data = ["success" => true];
            // $this->handleImplementation(json_encode($requestPayload));
            // return $esbHelper->successResponse($data, "json");

        } catch (\Exception $exception) {
            Log::error($exception->getTraceAsString());
            return $esbHelper->failureResponse($exception->getMessage(), "json");
        }
       
    }


    public function sendRequestToNavision($data)
    {

        $govEsbHelper = new EsbHelper();
        //$temurl = "10.1.67.188:8000/api/mof/budget";

        Log::info("send Request to NAVISION");
//        Log::info(json_encode($data));
        $messageType = $data["message"]["messageHeader"]["messageType"];
        if (isset($data["message"]["messageSummary"]["orgMessageType"]))
            $orgMessageType = $data["message"]["messageSummary"]["orgMessageType"];
        $client = new GuzzleHttpClient();
        if ($messageType == "PROJECT") {
            $response = $client->request("POST", "http://" . $temurl,
                [//"auth" => [$username, $password],
                    "headers" => [
                        "Accept" => "application/json",
                        "Content-type" => "application/json",
                        "service-code" => "SRVC014"
                        // "SRVC"=> "010"
                    ],
                    "json" => $data,

                ]);
        } elseif ($messageType == "APPROVED" || $messageType == "REVENUEPROJECTION" || $messageType == "REALLOCATION") {
            Log::info("NIMEFIKA");
            try {
//                $data['serviceCode'] = "SRVC056";
                $pssfBudgetApiCode = config("govesb.psssf-budget-api-code");

                // $data  = json_decode('[{"acc":"065|2031|TR196|201B|0000000|00000000|103|0000|E02|E01S10|1|00000|10A|21113133","activityDescription":"To facilitate Availability, Repair & Maintenance of Office Assets  and equipments by June 2023","amount":"9600000.00","unitPrice":"800000.00","quantity":"1.00","frequency":"12.00","uid":1131271},{"acc":"065|2031|TR196|201B|0000000|00000000|103|0000|E02|E01S10|1|00000|10A|22019101","activityDescription":"To facilitate Availability, Repair & Maintenance of Office Assets  and equipments by June 2023","amount":"62000000.04","unitPrice":"5166666.67","quantity":"1.00","frequency":"12.00","uid":1131444}]');
                $response = $govEsbHelper->requestData($pssfBudgetApiCode, $data);
                Log::info("Response form govesb: ". json_encode($response));
                if (!$response) {
                    //Signature verification failed.
                    Log::info("Signature verification failed");
                    return;
                }

                $requestId = $response["requestId"];
                $success = $response["success"];

                if (!$success) {
                    Log::info("Govesb requestFAiled!");
                    return;
                }

                $pssfResponse = $response["esbBody"];
                Log::info("PSSF RESPONSE: ". json_encode($pssfResponse));
            } catch (\Exception $exception){
                Log::error($exception->getTraceAsString());
            }
        } elseif ($orgMessageType == "EXPENDITURE" || $orgMessageType == "REVENUE" || $orgMessageType == "ALLOCATION") {
            $response = $client->request("POST", "http://" . $temurl,
                [//"auth" => [$username, $password],
                    "headers" => [
                        "Accept" => "application/json",
                        "Content-type" => "application/json",
                        "service-code" => "SRVC056"
                        // "SRVC"=> "010"
                    ],
                    "json" => $data,

                ]);
        }
        ///digital signature verification
        $acknowledgement = json_decode($response->getBody()->getContents(), true);
        if ($acknowledgement['message']['messageSummary']['responseStatus'] == "ACCEPTED") {
            Log::info("Ack from GovESB");
            Log::info($acknowledgement);
            return $acknowledgement['message']['messageSummary']['responseStatus'];
        } else {
            return "";
        }

    }

    public function acknowledgement($orgMsgId, $orgMessageType)
    {

        $messageHeader = array(
            "sender" => "PLANREPOTR",
            "receiver" => "NAVISION",
            "msgId" => $this->transactionNumber("ACKNOWLEDGEMENT", 6),
            "messageType" => "ACKNOWLEDGEMENT",
            "createdAt" => date("Y-m-d h:i:sa"),
        );

        $messageSummary = array(
            "orgMsgId" => $orgMsgId,
            "orgMessageType" => $orgMessageType,
            "responseStatus" => "ACCEPTED",
            "responseStatusDesc" => "Accepted Successfully",
        );

        $item = array(
            "messageHeader" => $messageHeader,
            "messageSummary" => $messageSummary
        );

        $signature = $this->signature($item);

        $payloadToMuse = array(
            'message' => $item,
            'digitalSignature' => $signature
        );
        $acknowledgement = $this->sendRequestTomuse($payloadToMuse);
        //return $payloadToMuse;
    }


    public function response($orgMsgId, $orgMessageType, $responseStatus, $responseStatusDesc, $arrayOfObject)
    {
        $messageHeader = array(
            "sender" => "PLANREPOTR",
            "receiver" => "MUSE",
            "msgId" => $this->transactionNumber("RESPONSE", 10),
            "messageType" => "RESPONSE",
            "createdAt" => date("Y-m-d h:i:sa"),
        );

        $messageSummary = array(
            "orgMsgId" => $orgMsgId,
            "orgMessageType" => $orgMessageType,
            "responseStatus" => $responseStatus,
            "responseStatusDesc" => $responseStatusDesc,
        );

        $messageDetail = $arrayOfObject;

        $item = array(
            "messageHeader" => $messageHeader,
            "messageSummary" => $messageSummary,
            "messageDetail" => $messageDetail
        );
        $signature = $this->signature($item);

        $payloadToMuse = array(
            'message' => $item,
            'digitalSignature' => $signature
        );
        return $payloadToMuse;
    }

    public function signature($data)
    {
        $data = json_encode($data);
        $plainText = $data;
        $privateKey = openssl_pkey_get_private("file://" . base_path() . "/config/keys/private.pem");
        $publicKey = openssl_pkey_get_public("file://" . base_path() . "/config/keys/public.pem");

        // Make a signature
        openssl_sign($plainText, $signature, $privateKey, OPENSSL_ALGO_SHA256);
        return $signature = base64_encode($signature);
    }


    public function transactionNumber($description, $budgetTransactionTypeId)
    {
        $transaction = new BudgetExportTransaction();
        $transaction->budget_transaction_type_id = $budgetTransactionTypeId;
        $transaction->description = $description;
        $transaction->save();
        //enter transaction control number
        $control_number = $description . '_' . $transaction->id;
        $transaction->control_number = $control_number;
        $transaction->save();
        return $control_number;
    }

    public function handleImplementation($data)
    {

        $orgMsgId = $data["message"]["messageHeader"]["msgId"];
        $orgMessageType = $data["message"]["messageHeader"]["messageType"];

        switch ($orgMessageType) {
            case 'EXPENDITURE':
                ReadDataFromMuse::processExpenditureData($data);
                break;

            case 'ALLOCATION':
                ReadDataFromMuse::processAllocation($data);
                break;
            case 'REVENUE':
                ReadDataFromMuse::processRevenue($data);
                break;
            case 'BUDGET':
                ReadDataFromErms::processBudgetData($data);
                break;
            case 'CEILINGDISSEMINATION':
                ReadDataFromErms::processCeilingDisData($data);
                break;
            default:

        }

    }

    public function handleResponse($data)
    {
        $orgMessageType = $data["message"]["messageSummary"]["orgMessageType"];

        $orgMsgId = $data["message"]["messageHeader"]["msgId"];
        //return $this->acknowledgement($orgMsgId,$orgMessageType);

        switch ($orgMessageType) {
            case 'APPROVED':
                ReadDataFromMuse::getBudgetFeedbck($data);
                break;
            case 'ACTIVITY':
                ReadDataFromMuse::getActivityFeedbabck($data);
                break;
            case 'REVENUEPROJECTION':
                ReadDataFromMuse::getRevenueProjectionFeedbabck($data);
                break;
            case 'REALLOCATION':
                ReadDataFromMuse::getReallocationFeedbabck($data);
                break;
            case 'PROJECT':
                ReadDataFromMuse::getProjectFeedbabck($data);
                break;
            case 'FACILITY':
                ReadDataFromMuse::getFacilitybabck($data);
                break;
            case 'CEILING':
                ReadDataFromErms::getCeilingFeedbck($data);
                break;
        }

    }


}
