<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Execution\BudgetImportIssueType;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class BudgetImportIssueTypeController extends Controller
{
    public function index() {
        $all = BudgetImportIssueType::all();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return BudgetImportIssueType::orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $budgetImportIssueType = new BudgetImportIssueType();
            $budgetImportIssueType->name = $data->name;
            $budgetImportIssueType->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "BUDGET_IMPORT_ISSUE_TYPE_CREATED_SUCCESSFULLY", "budgetImportIssueTypes" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }


    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $budgetImportIssueType = BudgetImportIssueType::find($data->id);
            $budgetImportIssueType->name = $data->name;
            $budgetImportIssueType->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "BUDGET_IMPORT_ISSUE_TYPE_UPDATED_SUCCESSFULLY", "budgetImportIssueTypes" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $budgetImportIssueType = BudgetImportIssueType::find($id);
        $budgetImportIssueType->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "BUDGET_IMPORT_ISSUE_TYPE_DELETED_SUCCESSFULLY", "budgetImportIssueTypes" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = BudgetImportIssueType::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        BudgetImportIssueType::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "BUDGET_IMPORT_ISSUE_TYPE_RESTORED", "trashedBudgetImportIssueTypes" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        BudgetImportIssueType::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "BUDGET_IMPORT_ISSUE_TYPE_DELETED_PERMANENTLY", "trashedBudgetImportIssueTypes" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = BudgetImportIssueType::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "BUDGET_IMPORT_ISSUE_TYPE_DELETED_PERMANENTLY", "trashedBudgetImportIssueTypes" => $all];
        return response()->json($message, 200);
    }
}
