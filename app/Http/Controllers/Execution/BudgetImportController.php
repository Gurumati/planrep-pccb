<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Execution\BudgetImport;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class BudgetImportController extends Controller
{

    public function index() {
        $all = BudgetImport::all();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return BudgetImport::with('data_source','import_method')->orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $budgetImport = new BudgetImport();
            $budgetImport->reference_code = $data->reference_code;
            $budgetImport->data_source_id = $data->data_source_id;
            $budgetImport->import_method_id = $data->import_method_id;
            $budgetImport->created_by = UserServices::getUser()->id;
            $budgetImport->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "BUDGET_IMPORT_CREATED_SUCCESSFULLY", "budgetImports" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }


    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $budgetImport = BudgetImport::find($data->id);
            $budgetImport->reference_code = $data->reference_code;
            $budgetImport->data_source_id = $data->data_source_id;
            $budgetImport->import_method_id = $data->import_method_id;
            $budgetImport->created_by = UserServices::getUser()->id;
            $budgetImport->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "BUDGET_IMPORT_UPDATED_SUCCESSFULLY", "budgetImports" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $budgetImport = BudgetImport::find($id);
        $budgetImport->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "BUDGET_IMPORT_DELETED_SUCCESSFULLY", "budgetImports" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = BudgetImport::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        BudgetImport::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "BUDGET_IMPORT_RESTORED", "trashedBudgetImports" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        BudgetImport::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "BUDGET_IMPORT_DELETED_PERMANENTLY", "trashedBudgetImports" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = BudgetImport::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "BUDGET_IMPORT_DELETED_PERMANENTLY", "trashedBudgetImports" => $all];
        return response()->json($message, 200);
    }
}
