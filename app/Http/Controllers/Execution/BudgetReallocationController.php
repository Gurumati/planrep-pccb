<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\Budgeting\BudgetExportAccountService;
use App\Http\Services\Execution\BudgetExportToFinancialSystemService;
use App\Http\Services\Execution\BudgetExportTransactionItemService;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Jobs\sendNotifications;
use App\Models\Execution\BudgetExportAccount;
use App\Models\Execution\BudgetExportTransaction;
use App\Models\Execution\BudgetReallocation;
use App\Models\Execution\BudgetReallocationItem;
use App\Models\Execution\BudgetReallocationResponse;
use App\Models\Planning\Activity;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\Section;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use SendDataToFinancialSystems;
use App\Models\Setup\GfsCode;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Log\Logger;

class BudgetReallocationController extends Controller
{

    public function getReallocationActivities($budgetType, $financialYearId, $adminHierarchyId, $sectionId, $facilityId, $fundSourceId) {
       $activities = Activity::getReallocationActivities($budgetType, $financialYearId, $adminHierarchyId, $sectionId, $facilityId, $fundSourceId);
       return ['activities'=>$activities ];
    }

    //Get COA to be source of reallocation
    public function getReallocationAccount($activityFundSourceId)
    {

        $accountCanReallocate = BudgetExportAccount::getReallocationAccount($activityFundSourceId);
        $data = ['reallocationAccounts' => $accountCanReallocate];
        return response()->json($data);
    }

    //Get COA to be source of reallocation
    public function getToReallocationAccount()
    {
        $accountCanReallocate = BudgetExportAccount::getToReallocationAccount();
        $data = ['reallocationAccounts' => $accountCanReallocate];
        return response()->json($data);
    }

    //Get Mtef Section from a section to be used to create new activity and or new inputs for reallocation
    public function getMtefSectionToReallocate($sectionId)
    {
        $financialYearId = FinancialYearServices::getExecutionFinancialYear()->id;

        $mtefSection = DB::table('mtef_sections as ms')->join('mtefs as m', 'm.id', 'ms.mtef_id')->where('ms.section_id', $sectionId)->where('m.admin_hierarchy_id', UserServices::getUser()->admin_hierarchy_id)->where('m.financial_year_id', $financialYearId)->select('ms.id')->first();

        return response()->json($mtefSection);

    }

    //Get Item reallocation  already create for a specific COA
    public function getAccountReallocations($accountId)
    {
        $reallocations = DB::table('budget_reallocation_items as bri')
            ->join('budget_export_accounts as bec', 'bec.id', 'bri.budget_export_to_account_id')
            ->join('activities as a', 'a.id', 'bec.activity_id')
            ->join('sections as s', 's.id', 'bec.section_id')
            ->join('gfs_codes as gfs', 'gfs.id', 'bec.gfs_code_id')
            ->where('bri.budget_export_account_id', $accountId)
            ->select('bri.id','gfs.code as gfs_code', 'gfs.description as description', 'bri.amount', 'bec.chart_of_accounts', 'a.description as activity','a.code as activityCode', 's.name as section', 'bri.is_approved')
            ->orderBy('bri.created_at', 'desc')
            ->get();

        return response()->json(['reallocations' => $reallocations]);
    }

    public function deleteItem($itemId){
        try{
            DB::beginTransaction();
            BudgetReallocationResponse::where('budget_reallocation_item_id',$itemId)->forceDelete();
            BudgetReallocationItem::where('id',$itemId)->forceDelete();
            DB::commit();
            return response()->json(['successMessage' => 'Item Deleted']);
        }
        catch(\Exception $e){
            DB::rollback();
            Log::error($e);
            return response()->json(['errorMessage' => 'Error Deleting Item', 'exception'=>$e], 500);
        }
    }

    public function reUseItem($itemId){
        try{
            DB::beginTransaction();
            $reallocation = BudgetReallocationItem::find($itemId);
            $reallocation->update(['is_rejected'=>false]);
            BudgetReallocation::where('id', $reallocation->budget_reallocation_id)->update(['is_open'=>true]);
            DB::commit();
            return response()->json(['successMessage' => 'Item Reused']);
        }
        catch(\Exception $e){
            DB::rollback();
            Log::error($e);
            return response()->json(['errorMessage' => 'Error Re Ussing Item', 'exception'=>$e], 500);
        }
    }

    public function getOpen(){

            $reallocations = BudgetReallocation::with(['admin_hierarchy','financial_year','decision_level','decision_level.next_decisions'])
            ->where('admin_hierarchy_id' ,UserServices::getUser()->admin_hierarchy_id)
            ->where('financial_year_id', FinancialYearServices::getExecutionFinancialYear()->id)
            ->where('id','<>',198)
            ->orderBy('id','desc')
            ->get();

        foreach($reallocations as &$r){
            $r->total = DB::table('budget_reallocation_items')->where('budget_reallocation_id',$r->id)->count();
            $r->totalAmount = DB::table('budget_reallocation_items')->where('budget_reallocation_id',$r->id)->sum('amount');
            $r->rejected = DB::table('budget_reallocation_items')->where('budget_reallocation_id',$r->id)->where('is_rejected',true)->count();
        }

        return ['reallocations' => $reallocations];
    }

    public function getOpenById($id){
        $reallocation = BudgetReallocation::with(['admin_hierarchy','financial_year','decision_level','decision_level.next_decisions'])
        ->where('id' ,$id)
        ->first();

        return ['reallocation' => $reallocation];
    }

    //TODO implement open reallocation
    public function open(Request $request){

        $defaultDecisionLevel = ConfigurationSetting::where('key', 'PLANREP_REALLOCATION_DEFAULT_DECISION_LEVEL')->first();
        $reallocationDataSource = ConfigurationSetting::where('key', 'PLANREP_REALLOCATION_DATA_SOURCE')->first();
        $reallocationImport = ConfigurationSetting::where('key', 'PLANREP_REALLOCATION_IMPORT_METHOD')->first();

        if(!isset($defaultDecisionLevel) || is_null($defaultDecisionLevel->value)){
            return response()->json(['errorMessage'=>'No default Decision level found for reallocation'], 400);
        }
        if(!isset($reallocationDataSource) || is_null($reallocationDataSource->value)){
            return response()->json(['errorMessage'=>'No default Data source found for reallocation'], 400);
        }
        if(!isset( $reallocationImport) || is_null($reallocationImport->value)){
            return response()->json(['errorMessage'=>'No default Import method found for reallocation'], 400);
        }

        if(!FinancialYearServices::getExecutionFinancialYear()){
            return response()->json(['errorMessage'=>'No Execution Financial Year'], 400);
        }

            $data = array_merge($request->all(),
            [
                'admin_hierarchy_id'=>UserServices::getUser()->admin_hierarchy_id,
                'financial_year_id'=> FinancialYearServices::getExecutionFinancialYear()->id,
                'data_source_id' => $reallocationDataSource->value,
                'import_method_id' => $reallocationImport->value,
                'decision_level_id' => $defaultDecisionLevel->value,
                'is_open' => true
            ]) ;

        try{
            if(isset($data['id']) && $data['id'] != ''){
                if(isset($data['hasNewDocument']) && $data['hasNewDocument'] == 'true' || (isset($data['file_to_upload']) && $data['file_to_upload'] != '')){
                    $existingFileUri =  ltrim(BudgetReallocation::find($data['id'])->document_url, '/');
                    Storage::delete($existingFileUri);
                    $document_url = null;
                    if($data['file_to_upload'] != ''){
                        $document_url = BudgetReallocation::uploadDoc('');
                    }
                    $data = array_merge($data,['document_url'=>$document_url]);
                }
                if(isset($data['hasNewDocument2']) && $data['hasNewDocument2'] == 'true'  || (isset( $data['file_to_upload2']) && $data['file_to_upload2'] != '')){
                        $existingFileUri2 =  ltrim(BudgetReallocation::find($data['id'])->document_url2, '/');
                        Storage::delete($existingFileUri2);
                        $document_url2 = null;
                        if($data['file_to_upload2'] != ''){
                            $document_url2 = BudgetReallocation::uploadDoc('2');
                        }
                        $data = array_merge($data,['document_url2'=>$document_url2]);

                }
                if(isset($data['hasNewDocument3']) && $data['hasNewDocument3'] == 'true' || (isset($data['file_to_upload3']) && $data['file_to_upload3'] != '' )){
                        $existingFileUri3 =  ltrim(BudgetReallocation::find($data['id'])->document_url3, '/');
                        Storage::delete($existingFileUri3);
                        if($data['file_to_upload3'] != ''){
                            $document_url3 = BudgetReallocation::uploadDoc('3');
                        }
                        else{
                            $document_url3 = null;
                        }
                        $data = array_merge($data,['document_url3'=>$document_url3]);
                }
                $reallocation = BudgetReallocation::find($data['id'])->update($data);

            }
            else{
                if(null !== Input::file('file_to_upload')){
                    $document_url = BudgetReallocation::uploadDoc('');
                    $data = array_merge($data,['document_url'=>$document_url]);
                }
                if(null !== Input::file('file_to_upload2')){
                    $document_url2 = BudgetReallocation::uploadDoc('2');
                    $data = array_merge($data,['document_url2'=>$document_url2]);
                }
                if(null !== Input::file('file_to_upload3')){
                    $document_url3 = BudgetReallocation::uploadDoc('3');
                    $data = array_merge($data,['document_url3'=>$document_url3]);
                }
                $reallocation = BudgetReallocation::create($data);
            }

            $message = ["successMessage" => "CREATE_SUCCESSFUL","reallocation"=>$reallocation];
            return response()->json($message, 200);
        }
        catch(\Exception $e){
            Log::error($e);
            $message = ["errorMessage" => "ERROR_CREATING",'error'=>$e];
            return response()->json($message, 500);
        }
    }

    public function submit($id, $decionLevelId){
        $reallocation = BudgetReallocation::find($id);
        DB::table('budget_reallocation_approvals')->insert(
            [
                'budget_reallocation_id'=>$id,
                'decision_level_id'=>$decionLevelId,
                'created_at'=>date('Y-m-d')
            ]);
        $reallocation->is_open = false;
        $reallocation->decision_level_id = $decionLevelId;
        $reallocation->save();
        return response()->json(['successMessage'=>'Reallocation submited'], 200);
    }
    public function getItems($id){
        $perPage = Input::get('perPage',10);
        $rejected = Input::get('rejected',0);
        return BudgetReallocationItem::where('budget_reallocation_id',$id)->where(function($q) use($rejected){
            if($rejected == 1){
                $q->where('is_rejected',true);
            }
        })->paginate($perPage);
    }

    public function createReallocation(Request $request, $reallocationId)
    {
        try {
            $data = json_decode($request->getContent());

            DB::transaction(function () use ($data, $reallocationId) {

                $fAccount = $data->fromAccount;
                $totalDeduction = 0;
                foreach ($fAccount->toAccounts as $toAccount) {
                    $item = new BudgetReallocationItem();
                    $item->from_chart_of_accounts = $fAccount->chart_of_accounts;
                    $item->to_chart_of_accounts = $toAccount->chart_of_accounts;
                    $item->amount = $toAccount->amount;
                    $item->budget_export_account_id = $fAccount->id;
                    $item->budget_export_to_account_id = $toAccount->id;
                    $item->budget_reallocation_id = $reallocationId;
                    $item->created_by = UserServices::getUser()->id;
                    $item->comments = (isset($data->comments))?$data->comments:null;
                    $item->is_approved = false;
                    $item->save();
                    $toBudgetAccount = BudgetExportAccount::find($toAccount->id);
                    $toBudgetAccount->is_locked = true;
                    $toBudgetAccount->updated_by = UserServices::getUser()->id;
                    $toBudgetAccount->save();
                    $totalDeduction = $totalDeduction + $toAccount->amount;
                }
                $fromBudgetAccount = BudgetExportAccount::find($fAccount->id);
                $fromBudgetAccount->is_locked = true;
                $fromBudgetAccount->updated_by = UserServices::getUser()->id;
                $fromBudgetAccount->save();
            });
            $data = ['successMessage' => 'REALLOCATION_CREATED_SUCCESSFULLY'];
            return response()->json($data);
        } catch (QueryException $e) {
            Log::error($e);
            $error = ['errorMessage' => 'ERROR_ON_CREATING'];
            return response()->json($error, 500);
        }
    }

    //Get reallocation Items to be approved
    public function getReallocationToApprove($budgetType, $financialYearId, $adminHierarchyId, $sectionId)
    {
        $flatten = new Flatten();
        $userAdminHierarchies = $flatten->getHierarchyTreeById($adminHierarchyId);
        $adminIds = $flatten->flattenAdminHierarchyWithChild($userAdminHierarchies);
        $userSections = Section::where('id', $sectionId)->get();
        $sectionIds = $flatten->flattenSectionWithChild($userSections);
        $perPage = Input::get('perPage',10);

        $reallocations = BudgetReallocationItem::with('budget_export_account', 'budget_reallocation')->whereHas('budget_export_account', function ($q) use ($adminIds, $sectionIds, $financialYearId) {
            $q->whereIn('admin_hierarchy_id', $adminIds)->whereIn('section_id', $sectionIds)->where('financial_year_id', $financialYearId);
        })
            ->where('is_approved', false)
            ->where('is_rejected', false)
            ->paginate($perPage);
        foreach ($reallocations as &$r) {
            $r->from_gfs = GfsCode::where('code', substr($r->from_chart_of_accounts, -8))->first()->description;
            $r->to_gfs = GfsCode::where('code', substr($r->to_chart_of_accounts, -8))->first()->description;
        }

        return response()->json($reallocations);

    }

    //Approve Reallocation items
    public function approveReallocation(Request $request)
    {
        try {
            $data = json_decode($request->getContent());

            DB::transaction(function () use ($data) {
                $userId = UserServices::getUser()->id;
                $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;

                $transactionType = ConfigurationSetting::where('key', 'REALLOCATION_TRANSACTION_TYPE')->first();
                $budgetGroupName = ConfigurationSetting::where('key', 'REALLOCATION_BUDGET_GROUP_NAME')->first();
                $transaction = new BudgetExportTransaction();
                $transaction->budget_transaction_type_id = $transactionType->value;
                $transaction->description = "APPROVED REALLOCATIONS";
                $transaction->save();
                //enter transaction control number
                $control_number = 'REALLOC-' . $transaction->id;
                $transaction->control_number = $control_number;
                $transaction->save();
                //end of control number update

                foreach ($data->reallocationsToApprove as $reallocation) {

                    $fromTransactionItem = BudgetExportTransactionItemService::create(
                        $transaction->id,
                        $reallocation->budget_export_account_id,
                        (-1 * $reallocation->amount),
                        $reallocation->id,
                        false,
                        $reallocation->created_by
                    );

                    $fromAccount = BudgetExportAccount::find($reallocation->budget_export_account_id);
                    $fromAccount->amount = $fromAccount->amount - $reallocation->amount;
                    $fromAccount->updated_by = UserServices::getUser()->id;
                    $fromAccount->save();

                    $toTransactionItem = BudgetExportTransactionItemService::create(
                        $transaction->id,
                        $reallocation->budget_export_to_account_id,
                        $reallocation->amount,
                        $reallocation->id,
                        true,
                        $reallocation->created_by
                    );


                    $toAccount = BudgetExportAccount::find($reallocation->budget_export_to_account_id);
                    $toAccount->amount = $toAccount->amount + $reallocation->amount;
                    $toAccount->updated_by = UserServices::getUser()->id;
                    $toAccount->save();

                    BudgetExportToFinancialSystemService::create(
                        $adminHierarchyId,
                        $toTransactionItem->id,
                        $toAccount->chart_of_accounts,
                        $budgetGroupName->value,
                        $toAccount->financial_year_id,
                        $reallocation->amount,
                        $toAccount->financial_system_code,
                        $userId
                    );

                    $reallocationItem = BudgetReallocationItem::find($reallocation->id);
                    $reallocationItem->is_approved = true;
                    $reallocationItem->comments = isset($data->comments) ? $data->comments : 'Approved';
                    $reallocationItem->approved_by = UserServices::getUser()->id;
                    $reallocationItem->save();

                    $this->tryUnlockAccounts($reallocation);

                }

                //TODO Call Export JOB -- call export job will be done at PO-RALG during data export

            });
            $data = ['successMessage' => 'REALLOCATION_APPROVED_SUCCESS_FULL'];
            return response()->json($data);
        } catch (QueryException $e) {
            $error = ['errorMessage' => 'ERROR_APPROVING_REALLOCATION'];
            return response()->json($error, 500);
        }
    }

    /**
     * @param Request $request
     * this function insert or update responses/ rejected budget reallocation items
     * it is going to update/insert responses to budget_reallocation_responses table
     * and send notifications to users
     */
    public function rejectReallocation(Request $request)
    {
        $userId = UserServices::getUser()->id;
        try {
            $data = json_decode($request->getContent());

            foreach ($data->reallocationsToApprove as $reallocation) {
                //insert response
                $obj = new BudgetReallocationResponse();
                $obj->updateOrCreate(
                    ['budget_reallocation_item_id' => $reallocation->id, 'created_by' => $userId],
                    ['response' => $data->comments, 'is_approved' => false]
                );
                //send notification
                //get user
                BudgetReallocationItem::find($reallocation->id)->update(['is_rejected' => true, 'comments' => $data->comments]);
                $user_id = DB::table('budget_reallocation_items')
                    ->where('id', $reallocation->id)
                    ->select('created_by')
                    ->first()->created_by;

                $users['id'] = array($userId, $user_id);
                $message = "Budget reallocation has been rejected. Message: " . $data->comments;
                $url = url('/notifications#!/');
                $object_id = 0;
                $job = new sendNotifications($users, $object_id, $url, $message, "fa fa-book");
                $this::dispatch($job->delay(10));
                return ['successMessage' => 'Reallocations Disapproved'];

            }

        } catch (Exception $e) {
            Log::error($e);
            return response()->json(['errorMessage' => 'Error Disapprove', $error => $e]);
        }

    }


    private function tryUnlockAccounts($reallocation)
    {
        $otherFromExists = DB::table('budget_export_accounts as acc')->join('budget_reallocation_items as al', 'acc.id', 'al.budget_export_account_id')->where('acc.id', $reallocation->budget_export_account_id)->where('al.id', '<>', $reallocation->id)->first();
        if ($otherFromExists == null) {
            DB::table('budget_export_accounts')->where('id', $reallocation->budget_export_account_id)->update(array('is_locked' => false));
        }

        $otherToExists = DB::table('budget_export_accounts as acc')->join('budget_reallocation_items as al', 'acc.id', 'al.budget_export_account_id')->where('acc.id', $reallocation->budget_export_to_account_id)->where('al.id', '<>', $reallocation->id)->first();
        if ($otherToExists == null) {
            DB::table('budget_export_accounts')->where('id', $reallocation->budget_export_to_account_id)->update(array('is_locked' => false));
        }
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * this function retrieve approved reallocation which has not been sent to epicor
     *
     */
    public function getApprovedReallocation(Request $request)
    {
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $financial_year_id  = $request->financial_year_id;
        $budget_type = $request->budget_type;
        $system_code = $request->system_code;
        $perPage = (int)$request->perPage;

        if(!UserServices::hasPermission(['execution.export.'.$budget_type])){
            return response()->json(null,403);
        }

        $reallocation = DB::table('budget_export_to_financial_systems as e')
                            ->join('budget_export_transaction_items as t','e.budget_export_transaction_item_id','t.id')
                            ->join('budget_reallocation_items as i','i.id','t.budget_reallocation_item_id')
                            ->join('budget_reallocations as r','i.budget_reallocation_id','r.id')
                            ->join('budget_export_accounts as acf', 'acf.id','i.budget_export_account_id')
                            ->join('activities as ac', 'ac.id', 'acf.activity_id')
                            ->join('budget_export_accounts as act', 'act.id','i.budget_export_to_account_id')
                            ->leftJoin('budget_reallocation_responses as rl','rl.budget_reallocation_item_id','i.id')
                            ->join('gfs_codes as gff','acf.gfs_code_id','gff.id')
                            ->join('gfs_codes as gft','act.gfs_code_id','gft.id')
                            ->where('acf.admin_hierarchy_id',$admin_hierarchy_id)
                            ->where('acf.financial_year_id',$financial_year_id)
                            ->where('ac.budget_type', $budget_type)
//                            ->where('e.queue_name', strtolower($system_code))
                            ->where('i.is_approved',true)
                            ->where('e.is_sent',false)
                            ->select('i.id','act.financial_system_code as toFs','acf.financial_system_code as FromFs',
                                'e.id as export_transaction_id','r.reference_code','i.from_chart_of_accounts',
                                'i.to_chart_of_accounts','acf.id as from_account','act.id as to_account','rl.is_approved as rejected',
                                     'i.amount','i.is_approved',DB::raw("to_char(i.created_at,'DD-MM-YYYY') as date"),
                                'gff.description as from_gfs','gft.description as to_gfs','rl.response as comments')
                             ->paginate($perPage);
        return response()->json($reallocation);
    }

    public function getFailedReallocation(Request $request){
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $financial_year_id  = $request->financial_year_id;
        $budget_type = $request->budget_type;
        $system_code = $request->system_code;
        $perPage = (int)$request->perPage;

        $reallocation = DB::table('budget_export_to_financial_systems as e')
                            ->join('budget_export_transaction_items as t','e.budget_export_transaction_item_id','t.id')
                            ->join('budget_reallocation_items as i','i.id','t.budget_reallocation_item_id')
                            ->join('budget_reallocations as r','i.budget_reallocation_id','r.id')
                            ->join('budget_export_accounts as acf', 'acf.id','i.budget_export_account_id')
                            ->join('activities as ac', 'ac.id', 'acf.activity_id')
                            ->join('budget_export_accounts as act', 'act.id','i.budget_export_to_account_id')
                            ->leftJoin('budget_reallocation_responses as rl','rl.budget_reallocation_item_id','i.id')
                            ->join('gfs_codes as gff','acf.gfs_code_id','gff.id')
                            ->join('gfs_codes as gft','act.gfs_code_id','gft.id')
                            ->leftJoin('budget_export_responses as er', 'er.budget_export_to_financial_system_id', 'e.id')
                            ->where('acf.admin_hierarchy_id',$admin_hierarchy_id)
                            ->where('acf.financial_year_id',$financial_year_id)
                            ->where('ac.budget_type', $budget_type)
//                            ->where('acf.financial_system_code', $system_code)
                            ->where('i.is_approved',true)
                            ->where('e.is_sent',true)
                            ->where('er.is_delivered', false)
                            ->distinct()
                            ->select('i.id','e.id as export_transaction_id','r.reference_code','i.from_chart_of_accounts','i.to_chart_of_accounts','acf.id as from_account','act.id as to_account','rl.is_approved as rejected',
                                     'i.amount','i.is_approved',DB::raw("to_char(i.created_at,'DD-MM-YYYY') as date"),'gff.description as from_gfs','gft.description as to_gfs','er.response as comments')
                             ->paginate($perPage);
        return response()->json($reallocation);
    }

    public function createAndGetReallocationToAccounts(Request $request, $mtefSectionId)
    {
        $data = json_decode($request->getContent());
        $mtef = DB::table('mtefs as m')
            ->join('mtef_sections as ms', 'm.id', 'ms.mtef_id')
            ->where('ms.id', $mtefSectionId)
            ->select('m.*')
            ->first();
        $Ids = '';
        foreach ($data->inputIds as $inputId) {
            $Ids = $Ids . $inputId . ',';
        }
        $Ids = trim($Ids, ',');
        $accounts = BudgetExportAccountService::createBudgetAccounts($mtef->admin_hierarchy_id, $mtef->financial_year_id, $Ids);
        return response()->json(['reallocationToAccounts' => BudgetExportAccount::getByInputIds($data->inputIds)]);
    }


    public function sendReallocation(Request $request)
    {
        $data = json_decode($request->getContent());
        $obj = new SendDataToFinancialSystems();
        $message = $obj->sendReallocation($data);
        if (isset($message['successMessage'])) {
            return response()->json($message, 200);
        } else {
            return response()->json($message, 500);
        }
    }
}
