<?php

namespace App\Http\Controllers\Execution;

use App\Models\Setup\ConfigurationSetting;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Services\Execution\RevenueAccountServices;
use App\Models\Planning\Activity;
use App\Http\Services\FinancialYearServices;
use App\Models\Planning\AdminHierarchySectMappings;
use Carbon\Carbon;
use App\Http\Services\UserServices;
use Illuminate\Support\Facades\Log;

class ActivityExpenditureController extends Controller
{
    public function index(Request $request){
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $financial_year_id = $request->financial_year_id;
        $transaction_type   = $request->transaction_type;
        $system_code        = $request->system_code;
        $perPage            = isset($request->perPage)?$request->perPage:10;
        $currentPage        = $request->currentPage;
        $cost_centre        = isset($request->cost_centre)?'%'.$request->cost_centre.'%':null;
        if($transaction_type == 'ALLOCATION'){
            $transaction_type = ['ALLOCATION', 'WITHDRAWAL'];
        }
        if($transaction_type == 'EXPENDITURE'){
            $transaction_type = ['EXPENDITURE'];
        }
        /** get data */
        if($transaction_type == 'REVENUE'){
            $obj = DB::table('received_fund_missing_accounts')
                    ->where('admin_hierarchy_id', $admin_hierarchy_id)
                    ->where('financial_year_id', $financial_year_id)
                    ->where('financial_system_code', $system_code)
                    ->where('resolved', false)
                    ->where('chart_of_accounts', 'not like', '%99999995')
                    ->where('chart_of_accounts', 'not like', '%99999993');
            if($cost_centre != null){
                $obj = $obj->where('chart_of_accounts', 'like', $cost_centre);
            }
            $data = $obj ->paginate($perPage);
        }else {

            $obj = DB::table('budget_import_missing_accounts')
                    ->where('admin_hierarchy_id', $admin_hierarchy_id)
                    ->where('financial_year_id', $financial_year_id)
                    ->where('financial_system_code', $system_code)
                    ->whereIn('transaction_type', $transaction_type)
                    ->where('resolved', false)
                    ->where('chart_of_accounts','not like', '%33081107')
                    ->where('chart_of_accounts', 'not like', '%99999993')
                    ->where('chart_of_accounts', 'not like', '%26312107')
                    ->where('chart_of_accounts', 'not like', '%26312106')
                    ->where('chart_of_accounts', 'not like', '%6202010%')
                    ->where('chart_of_accounts', 'not like', '%6202011%');
            if($cost_centre != null){
                $obj = $obj->where('chart_of_accounts', 'like', $cost_centre);
            }
            $data = $obj ->paginate($perPage);
        }
        return response()->json($data);
    }

    public function getMatchedItems(Request $request){
        $id                = $request->id;
        $chart_of_accounts = $request->chart_of_accounts;
        $transaction_type  = $request->transaction_type;
        $financial_year = ConfigurationSetting::getValueByKey('AUTO_FINANCIAL_YEAR_TO_REPORT');
        if($transaction_type == 'REVENUE' ){
           //get revenues
           $received_fund = $this->getReceived($id);
           if($received_fund->transaction_type == $transaction_type){
               //get attributes
               $array   = explode('-', $received_fund->chart_of_accounts);
               $council = $array['1'];
               $cc      = $array['2'];
               $sbc     = $array['3'];
               $facility= $array['4'];
               $activity= $array['6'];
               $fund_type= $array['7'];
               $fs      = $array['8'];
               $gfs     = $array['9'];
               //check if gfs and fund source are correct
               $gfs_fs = DB::table('gfs_codes as gfs')
                         ->join('fund_sources as fs', 'fs.id', 'gfs.fund_source_id')
                         ->where('gfs.code', $gfs)
                        ->where('fs.code', $fs);
               if($gfs_fs->count() == 0){
                  //get the correct gfs
                $result =   DB::table('gfs_codes as gfs')
                            ->join('fund_sources as fs', 'fs.id', 'gfs.fund_source_id')
                            ->where('fs.code', $fs)
                            ->select('gfs.code')
                            ->first();
                if(isset($result->code)){
                    $array['9'] = $result->code;
                    $new_coa = implode('-', $array);
                    //update coa
                    DB::table('received_fund_missing_accounts')
                        ->where('id', $id)
                        ->update(['chart_of_accounts'=>$new_coa]);
                    $gfs = $result->code;
                }
               }
               //then process
               $results = $this->getRevenueMatch($council, $fs, $cc, $financial_year, $gfs, $received_fund->financial_system_code, $facility, $fund_type, $sbc);
                if(count($results) > 0){
                     //get chart of accounts
                     $data  = array();
                     foreach($results as $item){
                         //get corresponding chart of account
                         $revenue = $this->getRevenue($item->id);
                         if(isset($revenue->id)){
                             array_push($data, $revenue);
                         }else {
                             //generate revenue export account
                             RevenueAccountServices::approveSelectedRevenue(array($item->id));
                             $revenue = $this->getRevenue($item->id);
                             if(isset($revenue->id)){
                                array_push($data, $revenue);
                             }
                         }
                     }
                }else {
                    $error = "No matched revenue found!";
                }

           }else {
               $error = 'No matched revenue recieved by PlanRep';
           }
        }
        else {
          //get budget
          $received_budget = $this->getExpenditure($id);

          if($received_budget->transaction_type = $transaction_type){
            //get attributes
            $array   = explode('-', $received_budget->chart_of_accounts);
            $council = $array['1'];
            $cc      = $array['2'];
            $sbc     = $array['3'];
            $facility= $array['4'];
            $activity= $array['6'];
            $fs      = $array['8'];
            $gfs     = $array['9'];
            //then process
            $is_facility = ($received_budget->financial_system_code == 'FFARS')?true:false;
            $results = DB::table('mtefs as m')
                        ->join('mtef_sections as ms', 'ms.mtef_id', 'm.id')
                        ->join('activities as ac', 'ac.mtef_section_id', 'ms.id')
                        ->join('admin_hierarchies as ah', 'ah.id', 'm.admin_hierarchy_id')
                        ->join('sections as s', 's.id', 'ms.section_id')
                        ->join('activity_facilities as af', 'af.activity_id', 'ac.id')
                        ->join('activity_facility_fund_sources as aff', 'aff.activity_facility_id', 'af.id')
                        ->join('activity_facility_fund_source_inputs as ai', 'ai.activity_facility_fund_source_id', 'aff.id')
                        ->join('fund_sources as fs', 'fs.id', 'aff.fund_source_id')
                        ->join('facilities as fa', 'fa.id', 'af.facility_id')
                        ->leftjoin('gfs_codes as gfs', 'gfs.id', 'ai.gfs_code_id')
                        ->where('s.code', $cc)
                        ->where('ah.code', $council)
                        ->where('ac.is_facility_account', $is_facility)
                        ->where('fa.facility_code', $facility)
                        ->where('fs.code', $fs)
                        ->where('gfs.code',$gfs)
                        ->where('m.financial_year_id', $financial_year)
                        ->select('ai.id','ac.id as activity_id','ac.budget_type', 'fs.code', 's.code')
                        ->orderBy('fs.code', 'desc')
                        ->orderBy('s.code', 'desc');
            if($results->count() > 0){
                //get chart of accounts
                $data  = array();
                $items = $results->get();
                foreach($items as $item){
                    //get corresponding chart of account
                    $budget = $this->getBudget($item->id);
                    if(isset($budget->id)){
                        array_push($data, $budget);
                    }else {
                        //generate revenue export account
                        Activity::approve($item->activity_id, $item->budget_type);
                        $budget = $this->getBudget($item->id);
                        array_push($data, $budget);
                    }
                }
            }else {
                $error = "No matched budget found!";
            }
          }else {
            $error = 'No matched budget recieved by PlanRep';
          }
        }
        if(isset($data)){
            return response()->json(['items'=>$data]);
        }else {
            return response()->json(['items'=>$error], 404);
        }
    }

    public function getRevenue($id){
        $revenue = DB::table('revenue_export_accounts')
                    ->where('admin_hierarchy_ceiling_id', $id)
                    ->first();
        return $revenue;
    }

    public function getBudget($id){
        $budget = DB::table('budget_export_accounts')
                    ->where('activity_facility_fund_source_input_id', $id)
                    ->first();
        return $budget;
    }

    public function getReceived($id){
        $received = DB::table('received_fund_missing_accounts')
                       ->where('id', $id)
                       ->first();
        return $received;
    }

    public function getExpenditure($id){
        $item = DB::table('budget_import_missing_accounts')
                ->where('id', $id)
                ->first();
        return $item;
    }
    /** match transaction */
    public function matchItem(Request $request){
       $item_id          = $request->id;
       $transaction_type = $request->transaction_type;
       $transaction_id   = $request->transaction_id;
       $success          = $this->matchValues($item_id, $transaction_id, $transaction_type);
       if($success){
        return response()->json(['successMessage'=>'Transaction has been resolved successfully']);
       }else {
        return response()->json(['errorMessage'=>'Failed to match item'], 500);
       }
    }

    //get period
    public function getPeriod($je_date){
        $query = "select * from periods where '".$je_date."'::date between start_date and end_date and periods.period_group_id != 8";
        $get_period = DB::select(DB::raw($query));
        $period_id = isset($get_period[0]->id) ? $get_period[0]->id: null;
        return $period_id;
    }

    //get revenue
    public function getRevenueMatch($council, $fs, $code, $financial_year_id, $gfs, $system_code, $facility, $fund_type, $bc, $first_call = true){
        //get budget type
        $result = DB::table('fund_sources as f')
                    ->join('fund_source_budget_classes as fb', 'fb.fund_source_id', 'f.id')
                    ->join('budget_classes as b', 'b.id', 'fb.budget_class_id')
                    ->join('fund_types as ft', 'ft.id', 'fb.fund_type_id')
                    ->where('f.code', $fs)
                    ->where('b.code', $bc)
                    ->where('ft.current_budget_code', $fund_type)
                    ->orWhere('ft.carried_over_budget_code', $fund_type)
                    ->select('ft.current_budget_code', 'ft.carried_over_budget_code');
        if($result->count() > 0){
           $item = $result->first();
           $budget_type = isset($item->carried_over_budget_code)?'CARRYOVER':'SUPPLEMENTARY';
        }else {
            $budget_type = 'APPROVED';
        }
        if($system_code == 'MUSE'){
            $own_source =   DB::table('ceilings as c')
                            ->join('admin_hierarchy_ceilings as ac','c.id','ac.ceiling_id')
                            ->join('gfs_codes as g','g.id','c.gfs_code_id')
                            ->join('fund_sources as f','f.id','g.fund_source_id')
                            ->join('sections as s','s.id','ac.section_id')
                            ->join('budget_classes as b','b.id','c.budget_class_id')
                            ->join('fund_source_categories as fc','fc.id','f.fund_source_category_id')
                            ->join('fund_types as ft','ft.id','fc.fund_type_id')
                            ->join('admin_hierarchies as ah', 'ah.id', 'ac.admin_hierarchy_id')
                            ->where('c.extends_to_facility', false)
                            ->where('c.is_aggregate', false)
                            ->where('f.can_project', true)
                            ->where('ah.code',$council)
                            ->where('ac.financial_year_id',$financial_year_id)
                            ->where('f.code', $fs)
                            ->where('ac.deleted_at',null)
                            ->where('g.code', $gfs)
                            ->select('ac.id', 'f.code', 's.code')
                            ->distinct()
                            ->limit(5)
                            ->get();

            //other revenue
            $budgetingSectionLevelsIds =  AdminHierarchySectMappings::getBudgetingSectionLevelIds();
            $revenue  = DB::table('ceilings as c')
                        ->join('admin_hierarchy_ceilings as ac','c.id','ac.ceiling_id')
                        ->join('gfs_codes as g','g.id','c.gfs_code_id')
                        ->join('fund_sources as f','f.id','g.fund_source_id')
                        ->join('sections as s','s.id','ac.section_id')
                        ->join('admin_hierarchy_lev_sec_mappings as m','s.id','m.section_id')
                        ->join('budget_classes as b','b.id','c.budget_class_id')
                        ->join('fund_source_categories as fc','fc.id','f.fund_source_category_id')
                        ->join('fund_types as ft','ft.id','fc.fund_type_id')
                        ->join('admin_hierarchies as ah', 'ah.id', 'ac.admin_hierarchy_id')
                        ->where('c.extends_to_facility', false)
                        ->where('c.is_aggregate', false)
                        ->where('ah.code',$council)
                        ->where('ac.financial_year_id',$financial_year_id)
                        ->whereIn('s.section_level_id',$budgetingSectionLevelsIds)
                        ->where('s.code', $code)
                        ->where('f.code', $fs)
                        ->where('g.code', $gfs)
                        ->where('ac.deleted_at',null)
                        ->select('ac.id', 'f.code', 's.code')
                        ->distinct()
                        ->limit(5)
                        ->get();

            //merge
            $data  = $own_source->merge($revenue);
            $is_facility = false;
            $is_aggregate = false;
            $extend_facility = false;
            }else {
            //ffars revenue
            $ffars_revenue = DB::table('ceilings as c')
                            ->join('admin_hierarchy_ceilings as ac','c.id','ac.ceiling_id')
                            ->join('gfs_codes as g','g.id','c.gfs_code_id')
                            ->join('fund_sources as f','f.id','g.fund_source_id')
                            ->join('sections as s','s.id','ac.section_id')
                            ->join('facilities as fa','fa.id','ac.facility_id')
                            ->join('budget_classes as b','b.id','c.budget_class_id')
                            ->join('fund_source_categories as fc','fc.id','f.fund_source_category_id')
                            ->join('fund_types as ft','ft.id','fc.fund_type_id')
                            ->join('admin_hierarchies as ah', 'ah.id', 'ac.admin_hierarchy_id')
                            ->where('ac.is_facility', true)
                            ->where('ah.code',$council)
                            ->where('ac.financial_year_id',$financial_year_id)
                            ->where('ac.deleted_at',null)
                            ->where('s.code', $code)
                            ->where('f.code', $fs)
                            ->where('g.code', $gfs)
                            ->select('ac.id', 'f.code', 's.code')
                            ->distinct()
                            ->limit(5)
                            ->get();

            $data = $ffars_revenue;
            $is_facility = true;
            $is_aggregate = false;
            $extend_facility = true;
        }
        //check if data is empty
        if($data->isEmpty() && $first_call){
                $this->generateDummyCeiling($financial_year_id, $council, $code, $facility, $is_facility, $budget_type,  $fs, $bc, $gfs, $is_aggregate, $extend_facility);
                return $this->getRevenueMatch($council, $fs, $code, $financial_year_id, $gfs, $system_code, $facility, $fund_type, $bc, false);
        }
      return $data;
    }

    //generate ceiling with zero amount for accepting revenues
    public function generateDummyCeiling($financial_year_id, $council, $section, $facility, $is_facility, $budget_type, $fund_source_code, $budget_class_code, $code, $is_aggregate, $extend_to_facility){
      $fund_source = DB::table('fund_sources')->where('code', $fund_source_code)->first();
      $budget_class = DB::table('budget_classes')->where('code', $budget_class_code)->first();
      $gfs_code = DB::table('gfs_codes')->where('code', $code)->first();
      $admin_hierarchy_id = DB::table('admin_hierarchies')->where('code', $council)->first()->id;
      $section = DB::table('sections')->where('code', $section)->first();

      if($facility == '00000000'){
        $facility_data = DB::table('facilities')->where('admin_hierarchy_id', $admin_hierarchy_id)->first();
      }else {
        $facility_data = DB::table('facilities')->where('facility_code', $facility)->first();
      }
      $facility_id = isset($facility_data->id)?$facility_data->id:5;
      //get user
      $userId =UserServices::getUser()->id;
      //check if gfscode and fundsource are valid
      $result = DB::table('gfs_codes as gfs')
                ->where('gfs.fund_source_id', $fund_source->id)
                ->where('gfs.id', $gfs_code->id);
      if($result->count() > 0){
        $ceiling_data = array(
            'created_at' => \Carbon\Carbon::now(),
            'created_by' => $userId,
            'name' => $fund_source->name,
            'budget_class_id' => $budget_class->id,
            'is_active' => true,
            'gfs_code_id' => $gfs_code->id,
            'is_aggregate' => $is_aggregate,
            'extends_to_facility' => $extend_to_facility
        );
        $ceiling_id = DB::table('ceilings')->insertGetId($ceiling_data);

        //crate hierarchy ceiling
        $admin_ceiling = array(
            'ceiling_id' => $ceiling_id,
            'amount' => 0,
            'created_by' => $userId,
            'created_at' => \Carbon\Carbon::now(),
            'admin_hierarchy_id' => $admin_hierarchy_id,
            'financial_year_id' => $financial_year_id,
            'section_id' => $section->id,
            'facility_id' => $facility_id,
            'is_facility' => $is_facility,
            'budget_type' => $budget_type
        );

        DB::table('admin_hierarchy_ceilings')->insert($admin_ceiling);
      }
    }

    //match items
    public function matchValues($item_id, $transaction_id, $transaction_type){
        //match item
       if($transaction_type == 'REVENUE'){
        //get revenue
        $revenue   = DB::table('revenue_export_accounts')
                     ->where('id', $item_id)
                     ->first();
        $received  = $this->getReceived($transaction_id);
        $period_id = $this->getPeriod($received->JEDate);
        $fund_source = DB::table('gfs_codes')
                       ->where('id', $revenue->gfs_code_id)
                       ->select('fund_source_id')
                       ->first();
        $financial_year = DB::table('financial_years')
                          ->where('id', $revenue->financial_year_id)
                          ->select('name')
                          ->first();
        //The account has been found.
        $revenue_data = array(
             'admin_hierarchy_ceiling_id' => $revenue->admin_hierarchy_ceiling_id,
             'date_received' => $received->JEDate,
             'reference_code' =>microtime(true) * 1000,
             'period_id' => $period_id,
             'fund_source_id' => $fund_source->fund_source_id,
             'gfs_code_id' => $revenue->gfs_code_id,
             'revenue_export_account_id' => $revenue->id,
             'debit_amount' =>$received->debit_amount,
             'credit_amount' => $received->credit_amount,
             'fiscal_year' => $financial_year->name,
             'account' => $revenue->chart_of_accounts,
             'JEDate' => $received->JEDate
         );

         try {
             DB::transaction(function() use ($revenue_data, $received) {
                 DB::table('received_fund_items')->insert([$revenue_data]);
                 if($received->financial_system_code == 'FFARS'){
                     $control_ids_data = array(
                         'transaction_id' => $received->id,
                         'transaction_type' => $received->transaction_type,
                         'created_at' => \Carbon\Carbon::now()
                     );
                     DB::table('ffars_actual_control_ids')->insert([$control_ids_data]);
                 }else {
                     $control_ids_data = array(
                         'message_id' => $received->id,
                         'created_at' => \Carbon\Carbon::now()
                     );
                     DB::table('gl_actual_summary_control_ids')->insert([$control_ids_data]);
                 }
                 //update as resolved
                 DB::table('received_fund_missing_accounts')->where('id', $received->id)->update(['resolved'=>true]);
             });
             return true;
         } catch (Exception $e){
             Log::debug($e->getMessage());
             //return response()->json(['errorMessage'=>'Failed to match item'], 500);
             return false;
         }
        }else {
            //get budget
            $budget  = DB::table('budget_export_accounts')
                            ->where('id', $item_id)
                            ->first();
            $expenditure  = $this->getExpenditure($transaction_id);
            $period_id = $this->getPeriod($expenditure->JEDate);
            $fund_source = DB::table('gfs_codes')
                        ->where('id', $budget->gfs_code_id)
                        ->select('fund_source_id')
                        ->first();
            $financial_year = DB::table('financial_years')
                            ->where('id', $budget->financial_year_id)
                            ->select('name')
                            ->first();
            $budget_class = DB::table('activities')
                            ->where('id', $budget->activity_id)
                            ->select('budget_class_id','budget_type')
                            ->first();
            //The account has been found.
            $expenditure_data = array(
                'budget_export_account_id' => $budget->id,
                'period_id' => $period_id,
                'cancelled' => false,
                'created_at' => \Carbon\Carbon::now(),
                'BookID' => $expenditure->bookID,
                'FiscalYear' => $financial_year->name,
                'JEDate' => $expenditure->JEDate,
                'Account' => $budget->chart_of_accounts,
                'date_imported' => date("Y-m-d"),
                'gfs_code_id' => $budget->gfs_code_id,
                'budget_class_id' => $budget_class->budget_class_id,
                'fund_source_id' => $fund_source->fund_source_id,
                'debit_amount'  => (double)$expenditure->debit_amount,
                'credit_amount' => (double)$expenditure->credit_amount,
                'budget_type' => $budget_class->budget_type
            );
            try{
                //save data
                DB::transaction(function() use ($expenditure_data, $expenditure) {
                DB::table('budget_import_items')->insert([$expenditure_data]);
                if($expenditure->financial_system_code == 'FFARS'){
                    $control_ids_data = array(
                        'transaction_id' => $expenditure->id,
                        'transaction_type' => $expenditure->transaction_type,
                        'created_at' => \Carbon\Carbon::now()
                    );
                    DB::table('ffars_actual_control_ids')->insert([$control_ids_data]);
                }else {
                    $control_ids_data = array(
                        'message_id' => $expenditure->id,
                        'created_at' => \Carbon\Carbon::now()
                    );
                    DB::table('gl_actual_summary_control_ids')->insert([$control_ids_data]);
                }
                //update as resolved
                DB::table('budget_import_missing_accounts')->where('id', $expenditure->id)->update(['resolved'=>true]);
                });
                return true;
            } catch (Exception $e){
                Log::debug($e->getMessage());
                //return response()->json(['errorMessage'=>'Failed to match item'], 500);
                return false;
            }
        }
    }

    //automatch items
    public function autoMatchItems(Request $request){
        $transaction_type = $request->transaction_type;
        $items = $request->items;
        //get coa
        foreach($items as $id){
            if($transaction_type == 'REVENUE'){
                $result = DB::table('received_fund_missing_accounts')
                          ->where('id', $id)
                          ->select('*')
                          ->first();
                $coa = substr_replace($result->chart_of_accounts, '%', -14, 1);
                //
                $match = DB::table('revenue_export_accounts')
                         ->where('chart_of_accounts', 'like', $coa)
                         ->pluck('id')
                         ->toArray();
                if(count($match) == 0){
                    //get attributes
                    $array     = explode('-', $result->chart_of_accounts);
                    $council   = $array['1'];
                    $cc        = $array['2'];
                    $sbc       = $array['3'];
                    $facility  = $array['4'];
                    $activity  = $array['6'];
                    $fund_type = $array['7'];
                    $fs        = $array['8'];
                    $gfs       = $array['9'];
                    //then get corresponding revenue match
                    $results = $this->getRevenueMatch($council, $fs, $cc, $result->financial_year_id, $gfs, $result->financial_system_code, $facility, $fund_type, $sbc);
                    if(count($results) > 0){
                        //generate revenue export account
                        RevenueAccountServices::approveSelectedRevenue(array($results[0]->id));
                        $match = DB::table('revenue_export_accounts')
                                    ->where('admin_hierarchy_ceiling_id', $results[0]->id)
                                    ->pluck('id')
                                    ->toArray();
                    }
                }
            }else {
                $result = DB::table('budget_import_missing_accounts')
                          ->where('id', $id)
                          ->select('chart_of_accounts')
                          ->first();
                $coa = substr_replace($result->chart_of_accounts, '%', -14, 1);
                //
                $match = DB::table('budget_export_accounts')
                         ->where('chart_of_accounts', 'like', $coa)
                         ->pluck('id')
                         ->toArray();
            }
            //match value
            if(count($match) > 0){
                $status = $this->matchValues($match[0], $id, $transaction_type);
            }else {
                $status = false;
            }
        }
        if($status == true){
            return response()->json(['successMessage'=> count($items).' Transactions have been resolved successfully']);
        }else {
            return response()->json(['errorMessage'=>'Failed to match items'], 500);
        }
    }
}
