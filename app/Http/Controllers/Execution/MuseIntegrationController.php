<?php

namespace App\Http\Controllers\Execution;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Models\Execution\BudgetExportTransaction;
use App\Models\Execution\ReadDataFromMuse;
use App\Models\Execution\ReadDataFromErms;
use Carbon\Carbon;

use Illuminate\Support\Facades\Log;

class MuseIntegrationController extends Controller
{
     //Kupokea allocation
    //Kupokea actual revenue
   //Kupokea actua expenditure
  //Kupokea check balance
 //Kupokea all responses

  public function getDataFromMuse(Request $request){

    $data = $request->all();
    Log::info("Received Request from MUSE");
    Log::info(json_encode($data));

    $orgMsgId = $data["message"]["messageHeader"]["msgId"];
    $messageType = $data["message"]["messageHeader"]["messageType"];
   
    try {
      if($messageType != "RESPONSE"){
         return $this->acknowledgement($orgMsgId,$messageType);
      }
    } catch (\Throwable $th) {
        return;
    } finally {
      if($messageType == "RESPONSE" || $messageType == "BUDGET_BALANCE_RESPONSE"){
        //$this->acknowledgement($orgMsgId,$messageType);
        $this->handleResponse($data);
       } else {
         $this->handleImplementation($data);
       }
    }
  }

   public function sendRequestTomuse($data){
    
    $messageType = $data["message"]["messageHeader"]["messageType"];

        $ip_server = $_SERVER['SERVER_ADDR'];

        if($ip_server == '10.1.1.43'){

          $temurl ="10.1.67.188:8000/api/mof/budget";
          if($messageType == "APPROVED"||$messageType == "REVENUEPROJECTION"||$messageType == "REALLOCATION"){
            $servicecode = "SRVC063";
          }else{
            //$servicecode = "SRVC056";         
          }

        }elseif($ip_server == '10.1.65.26'){
          $temurl ="154.118.230.150:8006/api/mof/budget";
          if($messageType == "APPROVED"||$messageType == "REVENUEPROJECTION"||$messageType == "REALLOCATION"){
            $servicecode = "SRVC016";
          }else{
            $servicecode = "SRVC056";         
          }

        }

        Log::info("send Request to MUSE");
        Log::info(json_encode($data));
        if(isset($data["message"]["messageSummary"]["orgMessageType"]))
        $orgMessageType = $data["message"]["messageSummary"]["orgMessageType"];
    $client = new GuzzleHttpClient();
    if($messageType == "FACILITY"){
    $response = $client->request("POST", "http://" . $temurl,
        [//"auth" => [$username, $password],
            "headers" => [
                "Accept" => "application/json",
                "Content-type" => "application/json",
                "service-code" => $servicecode
               // "SRVC"=> "010"
            ],
            "json" => $data,
           
        ]);
          }elseif($messageType == "PROJECT"){
      $response = $client->request("POST", "http://" . $temurl,
        [//"auth" => [$username, $password],
            "headers" => [
                "Accept" => "application/json",
                "Content-type" => "application/json",
                "service-code" => $servicecode
               // "SRVC"=> "010"
            ],
            "json" => $data,
           
        ]);
          }elseif($messageType == "CEILING"){
            $response = $client->request("POST", "http://" . $temurl,
              [//"auth" => [$username, $password],
                  "headers" => [
                      "Accept" => "application/json",
                      "Content-type" => "application/json",
                      "service-code" => $servicecode
                     // "SRVC"=> "010"
                  ],
                  "json" => $data,
                  
              ]);
          }elseif($messageType == "APPROVED"||$messageType == "REVENUEPROJECTION"||$messageType == "REALLOCATION"){
            $response = $client->request("POST", "http://" . $temurl,
              [//"auth" => [$username, $password],
                  "headers" => [
                      "Accept" => "application/json",
                      "Content-type" => "application/json",
                      //"service-code" => "SRVC016" // LIVE
                      "service-code" => $servicecode
                      // "SRVC"=> "010"
                  ],
                  "json" => $data,
                  
              ]);
          }elseif($orgMessageType == "EXPENDITURE"||$orgMessageType == "REVENUE"||$orgMessageType == "ALLOCATION"){
            $response = $client->request("POST", "http://" . $temurl,
              [//"auth" => [$username, $password],
                  "headers" => [
                      "Accept" => "application/json",
                      "Content-type" => "application/json",
                      "service-code" => $servicecode
                      // "SRVC"=> "010"
                  ],
                  "json" => $data,
                  
              ]);
          }
         ///digital signature verification
        $acknowledgement = json_decode($response->getBody()->getContents(),true);
        if($acknowledgement['message']['messageSummary']['responseStatus']=="ACCEPTED"){
          Log::info("Ack from MoFP Bus");
          Log::info($acknowledgement);
          return $acknowledgement['message']['messageSummary']['responseStatus'];
        }else{
          return "";
        }
    
   }

  public function acknowledgement($orgMsgId,$orgMessageType){

    $messageHeader = array(
      "sender" => "PLANREPOTR",
      "receiver" => "MUSE",
      "msgId" => $this->transactionNumber("ACKNOWLEDGEMENT",6),
      "messageType" => "ACKNOWLEDGEMENT",
      "createdAt" => date("Y-m-d h:i:sa"),
    );

    $messageSummary = array(
    "orgMsgId" => $orgMsgId,
    "orgMessageType" => $orgMessageType,
		"responseStatus" => "ACCEPTED",
		"responseStatusDesc" => "Accepted Successfully",
    );

    $item = array(
      "messageHeader" => $messageHeader,
      "messageSummary" => $messageSummary
    );

    $signature = $this->signature($item);

    $payloadToMuse = array(
            'message' => $item,
            'digitalSignature'=>$signature
    );
    //$acknowledgement = $this->sendRequestTomuse($payloadToMuse);
    return $payloadToMuse;
}


   public function response($orgMsgId,$orgMessageType,$responseStatus,$responseStatusDesc,$arrayOfObject){
    $messageHeader = array(
      "sender" => "PLANREPOTR",
      "receiver" => "MUSE",
      "msgId" => $this->transactionNumber("RESPONSE",10),
      "messageType" => "RESPONSE",
      "createdAt" => date("Y-m-d h:i:sa"),
    );

    $messageSummary = array(
    "orgMsgId" => $orgMsgId,
    "orgMessageType" => $orgMessageType,
		"responseStatus" => $responseStatus,
		"responseStatusDesc" => $responseStatusDesc,
    );

    $messageDetail =$arrayOfObject;

    $item = array(
      "messageHeader" => $messageHeader,
      "messageSummary" => $messageSummary,
      "messageDetail" => $messageDetail
    );
    $signature = $this->signature($item);

    $payloadToMuse = array(
      'message' => $item,
      'digitalSignature'=>$signature
     );
    return $payloadToMuse;
   }

  public function signature($data){
    $data = json_encode($data);
    $plainText = $data;
    $privateKey = openssl_pkey_get_private("file://" .base_path() . "/config/keys/private.pem");
    $publicKey  = openssl_pkey_get_public("file://" .base_path() . "/config/keys/public.pem");

    // Make a signature
    openssl_sign($plainText, $signature, $privateKey, OPENSSL_ALGO_SHA256);
    return $signature = base64_encode($signature);
  }


  public function transactionNumber($description,$budgetTransactionTypeId){
    $transaction = new BudgetExportTransaction();
    $transaction->budget_transaction_type_id = $budgetTransactionTypeId;
    $transaction->description = $description;
    $transaction->save();
    //enter transaction control number
    $control_number = $description.'_' . $transaction->id;
    $transaction->control_number = $control_number;
    $transaction->save();
    return $control_number;
  }

  public function handleImplementation($data){
   
      $orgMsgId = $data["message"]["messageHeader"]["msgId"];
      $orgMessageType = $data["message"]["messageHeader"]["messageType"];
      // $applyDate = $data["message"]["messageSummary"]["applyDate"];
      // $date = isset($applyDate) ? $applyDate : null;
      // if($date){
      //     $resposedata = $this->response($orgMsgId,$orgMessageType,"REJECTED"," applayDate is required on Payload Summary",array());
      //     try {
      //       $this->sendRequestTomuse($resposedata);
      //     } catch (\Throwable $th) {
      //               //throw $th;
      //     }
      //   return;
      // }

     
      switch ($orgMessageType) {
        case 'EXPENDITURE':
          ReadDataFromMuse::processExpenditureData($data);
        break;

        case 'ALLOCATION':
          ReadDataFromMuse::processAllocation($data);
        break;
          case 'REVENUE':
            ReadDataFromMuse::processRevenue($data);
          break;
          case 'BUDGET':
            ReadDataFromErms::processBudgetData($data);
          break;
          case 'CEILINGDISSEMINATION':
            ReadDataFromErms::processCeilingDisData($data);
          break;
          default:  
  
      }

  }

  public function handleResponse($data){

  if(isset($data["message"]["messageSummary"]["orgMessageType"])){
   $orgMessageType = $data["message"]["messageSummary"]["orgMessageType"];
   Log::info($data['message']['messageSummary']['responseStatus']);
   Log::info($data["message"]["messageSummary"]["orgMessageType"]);
  }
   $messageType = $data["message"]["messageHeader"]["messageType"];
   $orgMsgId = $data["message"]["messageHeader"]["msgId"];
   $messageType == 'BUDGET_BALANCE_RESPONSE'?$orgMessageType = 'BALANCE':$orgMessageType=$orgMessageType;
   //return $this->acknowledgement($orgMsgId,$orgMessageType);

    switch ($orgMessageType) {
       case 'APPROVED':
          ReadDataFromMuse::getBudgetFeedbck($data);
        break;
        case 'ACTIVITY':
          ReadDataFromMuse::getActivityFeedbabck($data);
        break; 
        case 'REVENUEPROJECTION':
          ReadDataFromMuse::getRevenueProjectionFeedbabck($data);
        break;
        case 'REALLOCATION':
          ReadDataFromMuse::getReallocationFeedbabck($data);
        break;
        case 'BALANCE':
          ReadDataFromMuse::getBalanceRequestFeedback($data);
        break;
        case 'PROJECT':
          ReadDataFromMuse::getProjectFeedbabck($data);
        break;
        case 'FACILITY':
          ReadDataFromMuse::getFacilitybabck($data);
        break;
        case 'CEILING':
          ReadDataFromErms::getCeilingFeedbck($data);
        break;
    }
    
  }


   
}
