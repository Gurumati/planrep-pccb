<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Budgeting\AdminHierarchyCeiling;
use App\Models\Budgeting\BudgetType;
use App\Models\Execution\BudgetTransactionType;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use mysql_xdevapi\Exception;
use SendDataToFinancialSystems;

class CeilingExportAmountController extends Controller {

    public function getFinancialYears(Request $request) {
        $financialYears = DB::table('financial_years')->orderBy('id', 'desc')->get();
       $data = array("financialYears" => $financialYears);
      return  $data;
    }


    public function getCeilings(Request $request){
        $financialYearId = $request->financialYear;
        $budgetType = $request->budgetType;
        $adminHierarchyId = $request->adminHierarchyId;
        $sectionId = $request->sectionId;

        $adminHierarchyId = ($adminHierarchyId != null) ? $adminHierarchyId : UserServices::getUser()->admin_hierarchy_id;

        $sectionId = ($sectionId != null) ? $sectionId : UserServices::getUser()->section_id;

        $financialYearId = ($financialYearId != null) ? $financialYearId : FinancialYearServices::getPlanningFinancialYear()->id;

        $budgetType = ($budgetType != null) ? $budgetType : BudgetType::getDefault();

        return ['adminHierarchyCeilings' => AdminHierarchyCeiling::ceilingToExport($budgetType, $financialYearId, $adminHierarchyId, $sectionId), 'atStart' => AdminHierarchyCeiling::isAtStartCeilingChain($adminHierarchyId, $sectionId)];
    }


    public function exportToFinancialSystem(Request $request){
        try{
            SendDataToFinancialSystems::sendCeillingToErms($request->data);
            $message = ["successMessage" => "CEILING EXPORTED TO ERMS"];
            return response()->json($message, 200);
        }catch (\Exception $e){
            $message = ["errorMessage" => "CEILING NOT EXPORTED TO ERMS"];
            return response()->json($message, 500);
        }
    }


}
