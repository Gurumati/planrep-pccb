<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;

class MainController extends Controller
{
    public function index() {
        $title = "Execution|" . $this->app_initial;
        return view('execution.index', ['js_scripts' => $this->form_scripts, 'title' => $title]);
    }

    public function jsonBudgets() {
        return response()->json([
            'lga' => 'KALAMBO DC',
            'year' => '2018/2019'
        ]);
    }
}
