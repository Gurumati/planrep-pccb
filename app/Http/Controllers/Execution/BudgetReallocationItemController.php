<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Execution\BudgetReallocationItem;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class BudgetReallocationItemController extends Controller
{
    public function index() {
        $all = BudgetReallocationItem::with('budget_export_account','budget_reallocation')->orderBy('created_at','desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return BudgetReallocationItem::with('budget_export_account','budget_reallocation')->orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new BudgetReallocationItem();
            $obj->from_chart_of_accounts = $data->from_chart_of_accounts;
            $obj->to_chart_of_accounts = $data->to_chart_of_accounts;
            $obj->amount = $data->amount;
            $obj->budget_export_account_id = $data->budget_export_account_id;
            $obj->budget_reallocation_id = $data->budget_reallocation_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "budgetReallocationItems" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 400);
        }
    }


    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = BudgetReallocationItem::find($data->id);
            $obj->from_chart_of_accounts = $data->from_chart_of_accounts;
            $obj->to_chart_of_accounts = $data->to_chart_of_accounts;
            $obj->amount = $data->amount;
            $obj->budget_export_account_id = $data->budget_export_account_id;
            $obj->budget_reallocation_id = $data->budget_reallocation_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPATE_SUCCESS", "budgetReallocationItems" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 400);
        }
    }

    public function delete($id) {
        $obj = BudgetReallocationItem::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETED_SUCCESS", "budgetReallocationItems" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = BudgetReallocationItem::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        $message = ["trashedBudgetReallocationItems" => $all];
        return response()->json($message, 200);
    }

    public function restore($id) {
        BudgetReallocationItem::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedBudgetReallocationItems" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        BudgetReallocationItem::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENTLY_DELETE_SUCCESS", "trashedBudgetReallocationItems" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = BudgetReallocationItem::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedBudgetReallocationItems" => $all];
        return response()->json($message, 200);
    }
}
