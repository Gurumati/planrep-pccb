<?php

namespace App\Http\Controllers\Execution;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use App\Models\Execution\BudgetExportTransaction;
use App\Models\Execution\ReadDataFromNpmis;
use Carbon\Carbon;

use Illuminate\Support\Facades\Log;

class NpimsIntegrationController extends Controller
{
    
    public function getProjectBudgetRequest(Request $request){

      
      $data = $request->all();

      $trNumber = $data["message"]["messageSummary"]["company"];
      $messageType = $data["message"]["messageHeader"]["messageType"];
      $orgMsgId = $data["message"]["messageHeader"]["msgId"];


      try {
        if($messageType != "RESPONSE"){
           return $this->acknowledgement($orgMsgId,$messageType);
        }
      } catch (\Throwable $th) {
          return;
      } finally {
        if($messageType == "RESPONSE"){
          return $this->acknowledgement($orgMsgId,$messageType);
          //$this->handleResponse($data); //Uncomment to handle response
         } else {
           $this->handleImplementation($data);
         }
      }
      
    }

      //Receive all requests from NPMIS
    public function getDataFromNpims(Request $request){

        $data = $request->all();
        Log::info("Received Request from NPMIS");
        Log::info(json_encode($data));
    
        $orgMsgId = $data["message"]["messageHeader"]["msgId"];
        $orgMessageType = $data["message"]["messageHeader"]["messageType"];
       
        try {
          if($orgMessageType != "RESPONSE"){
             return $this->acknowledgement($orgMsgId,$orgMessageType);
          }
        } catch (\Throwable $th) {
            return;
        } finally {
          if($orgMessageType == "RESPONSE"){
            $this->handleResponse($data);
           } else {
             $this->handleImplementation($data);
           }
        }
      }

      //Send all request to NPMIS
      public function sendRequestToNpmis($data){

            //$temurl ="154.118.230.150:8006/api/mof/budget"; // ;live endpoint
            //$temurl ="10.1.1.177:8006/api/mof/direct-request ";
            $temurl ="10.1.67.188:8000/api/v1/request";
            $messageType = $data["message"]["messageHeader"]["messageType"];
            //$orgMessageType = $data["message"]["messageSummary"]["orgMessageType"];
        $client = new GuzzleHttpClient();
        if($messageType == "OBJECTIVE"){
          $response = $client->request("POST", "http://" . $temurl,
            [
                "headers" => [
                    "Accept" => "application/json",
                    "Content-type" => "application/json",
                    "service-code" => "SRVC033"
                ],
                "json" => $data,
               
            ]);
        }elseif($messageType == "PROCESSED" || $messageType == "REJECTED"){
                $response = $client->request("POST", "http://" . $temurl,
                  [
                      "headers" => [
                          "Accept" => "application/json",
                          "Content-type" => "application/json",
                          "service-code" => "SRVC032"
                      ],
                      "json" => $data,
                      
                  ]);
        
        }elseif($messageType == "PROJECT_BUDGET"){
                $response = $client->request("POST", "http://" . $temurl,
                  [
                      "headers" => [
                          "Accept" => "application/json",
                          "Content-type" => "application/json",
                          "service-code" => "SRVC034"
                      ],
                      "json" => $data,
                      
                  ]);
        
        }elseif($messageType == "PROJECT_REALLOCATION"){
                $response = $client->request("POST", "http://" . $temurl,
                  [
                      "headers" => [
                          "Accept" => "application/json",
                          "Content-type" => "application/json",
                          "service-code" => "SRVC033"
                      ],
                      "json" => $data,
                      
                  ]);
        }
             ///digital signature verification
            $acknowledgement = json_decode($response->getBody()->getContents(),true);
            if($acknowledgement['message']['messageSummary']['responseStatus']=="ACCEPTED"){
              Log::info("NPMIS");
              Log::info($acknowledgement['message']['messageSummary']['responseStatus']);
              Log::info($acknowledgement);
              return $acknowledgement['message']['messageSummary']['responseStatus'];
            }else{
              return "";
            }
        
       }

       public function acknowledgement($orgMsgId,$orgMessageType){

        $messageHeader = array(
          "sender" => "PLANREPOTR",
          "receiver" => "NPMIS",
          "msgId" => $this->transactionNumber("ACKNOWLEDGEMENT",6),
          "messageType" => "ACKNOWLEDGEMENT",
          "createdAt" => date("Y-m-d h:i:sa"),
        );
    
        $messageSummary = array(
        "orgMsgId" => $orgMsgId,
        "orgMessageType" => $orgMessageType,
        "responseStatus" => "ACCEPTED",
        "responseStatusDesc" => "Accepted Successfully",
        );
    
        $item = array(
          "messageHeader" => $messageHeader,
          "messageSummary" => $messageSummary
        );
    
        $signature = $this->signature($item);
    
        $payloadToNpmis = array(
                'message' => $item,
                'digitalSignature'=>$signature
        );

      return $payloadToNpmis;

    }

       public function handleResponse($data){
        $orgMessageType = $data["message"]["messageSummary"]["orgMessageType"];
       
        $orgMsgId = $data["message"]["messageHeader"]["msgId"];
        //return $this->acknowledgement($orgMsgId,$orgMessageType);
     
         switch ($orgMessageType) {
            case 'OBJECTIVE':
              ReadDataFromNpmis::getObjectiveFeedback($data);
             break;
             case 'PROJECT_BUDGET':
              ReadDataFromNpmis::getProjectBudgetFeedback($data);
             break; 
             case 'REVENUEPROJECTION':
               ReadDataFromMuse::getRevenueProjectionFeedbabck($data);
             break;
             case 'REALLOCATION':
               ReadDataFromMuse::getReallocationFeedbabck($data);
             break;
             case 'PROJECT':
               ReadDataFromMuse::getProjectFeedbabck($data);
             break;
             case 'FACILITY':
               ReadDataFromMuse::getFacilitybabck($data);
             break;
             case 'CEILING':
               ReadDataFromErms::getCeilingFeedbck($data);
             break;
         }
         
       }

      //A function that process data from NPMIS
      public function handleImplementation($data){
   
        $orgMsgId = $data["message"]["messageHeader"]["msgId"];
        $orgMessageType = $data["message"]["messageHeader"]["messageType"];
       
        switch ($orgMessageType) {
          case 'PROJECT':
            ReadDataFromNpmis::processProjectData($data);
          break;
          case 'PROJECT_BUDGET' || 'PROJECT_REALLOCATION':
            ReadDataFromNpmis::processProjectBudgetData($data);
          break;  
          case 'PROJECTCEILING':
            ReadDataFromNpmis::processProjectCeiling($data);
          break;

            default:  
    
        }
  
    } 

      //A response payload after successfull processing data
      public function response($orgMsgId,$orgMessageType,$responseStatus,$responseStatusDesc,$arrayOfObject){
        $messageHeader = array(
          "sender" => "PLANREPOTR",
          "receiver" => "NPMIS",
          "msgId" => $this->transactionNumber("RESPONSE",10),
          "messageType" => "RESPONSE",
          "createdAt" => date("Y-m-d h:i:sa"),
        );
    
        $messageSummary = array(
        "orgMsgId" => $orgMsgId,
        "orgMessageType" => $orgMessageType,
            "responseStatus" => $responseStatus,
            "responseStatusDesc" => $responseStatusDesc,
        );
    
        $messageDetail =$arrayOfObject;
    
        $item = array(
          "messageHeader" => $messageHeader,
          "messageSummary" => $messageSummary,
          "messageDetails" => $messageDetail
        );
        $signature = $this->signature($item);
    
        $payloadToMuse = array(
          'message' => $item,
          'digitalSignature'=>$signature
         );
        return $payloadToMuse;
       }

       public function signature($data){
        $data = json_encode($data);
        $plainText = $data;
        $privateKey = openssl_pkey_get_private("file://" .base_path() . "/config/keys/private.pem");
        $publicKey  = openssl_pkey_get_public("file://" .base_path() . "/config/keys/public.pem");
    
        // Make a signature
        openssl_sign($plainText, $signature, $privateKey, OPENSSL_ALGO_SHA256);
        return $signature = base64_encode($signature);
      }
    
    
      public function transactionNumber($description,$budgetTransactionTypeId){
        $transaction = new BudgetExportTransaction();
        $transaction->budget_transaction_type_id = $budgetTransactionTypeId;
        $transaction->description = $description;
        $transaction->save();
        //enter transaction control number
        $control_number = $description.'_' . $transaction->id;
        $transaction->control_number = $control_number;
        $transaction->save();
        return $control_number;
      }
    
}
