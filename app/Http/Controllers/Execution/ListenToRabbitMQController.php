<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Jobs\ConsumeDataFromQueueJob;
use App\Models\Setup\ConfigurationSetting;


class ListenToRabbitMQController extends Controller
{
    public function getCoaSegmentFeedBack()
    {
        $options = array(
            'time' => 60,
            'empty_queue_timeout' => 5,
            'connection_name' => 'default_connection',
            'exchange' => 'COASegmentsExchange',
            'vhost' => '/',
            'headers' => array('Company'=> '000', 'Type' => 'FeedBack', 'x-match'=> 'all')
        );

        //Get the name of the queue name from the configuration settings
        $qn = ConfigurationSetting::where('key','COA_SEGMENT_FEEDBACK_QUEUE')->first();
        $queue_name = isset($qn->value)?$qn->value:null;
        //$queue_name = 'COASegments_Queue_FeedBack';
        $action     = 'getCoaSegmentFeedBack';
        dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
        return response()->json(["successMessage" => "Success"],200);
    }

    public function getGLAccountsQueueFeedBack()
    {
        $options = array(
            'time' => 60,
            'empty_queue_timeout' => 5,
            'exchange_type'=>'headers',
            'exchange' => 'GLAccountsExchange'
        );

        //Get the name of the queue name from the configuration settings
        $qn = ConfigurationSetting::where('key','COA_SEGMENT_FEEDBACK_QUEUE')->first();
        $queue_name = isset($qn->value)?$qn->value:null;

        $queue_name = 'GLAccounts_Queue_FeedBack';
        $action     = 'getGLAccountsQueueFeedBack';
        dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
        return response()->json(["successMessage" => "Success"],200);
    }

    public function getBudgetQueueFeedBack(){
        $options = array(
            'time' => 60,
            'empty_queue_timeout' => 5,
            'connection_name' => 'default_connection',
            'exchange' => 'BudgetExchange',
            'vhost' => '/',
            'headers' => array('Company'=> '000', 'Type' => 'FeedBack', 'x-match'=> 'all')
        );
        $queue_name = 'BUDGET_Queue_FeedBack';
        $action     = 'getBudgetQueueFeedBack';
        dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
        return response()->json(["successMessage" => "Success"],200);
    }

    public static function getGLActualSummary(){
        $options = array(
            'time' => 60,
            'empty_queue_timeout' => 5,
            'connection_name' => 'default_connection',
//            'exchange' => 'BudgetExchange',
            'vhost' => '/',
            'headers' => array('Company'=> '000', 'Type' => 'FeedBack', 'x-match'=> 'all')
        );
        $queue_name = 'GLActualSummary_Queue_172.16.18.199';
        $action     = 'getGLActualSummary';
        dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
    }

    public static function getFacilityExpenditure(){
        $options = array('exchange'=>'ACTUALS_FROM_FFARS_EXCHANGE');
        $queue_name = 'ACTUAL_FROM_FFARS_QUEUE';
        $action     = 'getFacilityExpenditure';
        dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
    }

    public function getFacilityBudgetFeedBack(){
        $options = array(
            'time' => 60,
            'connection_name' => 'default_connection',
            'exchange' => 'BUDGET_TO_FFARS_EXCHANGE',
            'vhost' => '/',
        );
        $queue_name = 'BUDGET_TO_FFARS_QUEUE_FEEDBACK';
        $action     = 'getFacilityBudgetFeedBack';
        dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
        return response()->json(["successMessage" => "Success"],200);
    }

    public static function getFfarsAccountBalances(){
        $options = array(
            'time' => 60,
            'connection_name' => 'default_connection',
            'exchange' => 'BALANCES_FROM_FFARS_EXCHANGE',
            'vhost' => '/',
        );
        $queue_name = 'BALANCES_FROM_FFARS_QUEUE';
        $action     = 'getFfarsAccountBalances';
        dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
        return response()->json(["successMessage" => "Success"],200);
    }
}
