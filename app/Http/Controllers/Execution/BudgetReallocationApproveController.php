<?php

namespace App\Http\Controllers\Execution;

use Illuminate\Http\Request;
use App\Http\Services\UserServices;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Models\Setup\AdminHierarchy;
use Illuminate\Support\Facades\DB;
use App\Models\Execution\BudgetReallocationItem;
use App\Models\Setup\GfsCode;
use Illuminate\Support\Facades\Input;
use App\Models\Execution\BudgetReallocationApprove;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Execution\BudgetExportTransaction;
use App\Http\Services\Execution\BudgetExportTransactionItemService;
use App\Models\Execution\BudgetExportAccount;
use App\Http\Services\Execution\BudgetExportToFinancialSystemService;
use App\Models\Execution\BudgetReallocationResponse;
use App\Jobs\sendNotifications;
use App\Models\Auth\User;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Execution\BudgetBalanceTransaction;
use App\Models\Execution\BudgetReallocation;
use App\Models\Setup\DecisionLevel;

class BudgetReallocationApproveController extends Controller
{
    public function getReallocations(Request $request){
        $admin_hierarchy_id = $request->id;
        $perPage = isset($request->perPage)?$request->perPage: 10;
        $user = UserServices::getUser();
        $decision_level_id = $user->decision_level_id;
        $is_export =  isset($request->is_export)?$request->is_export:false;
        $document_url_pre = '';
        $trnumber = DB::table('admin_hierarchies')->where('id',$admin_hierarchy_id)->first()->code;

        $getFyId = DB::select("select * from budget_reallocations where admin_hierarchy_id = $admin_hierarchy_id order by id desc limit 1");
        $financial_year_id  = isset($getFyId[0]->financial_year_id) ? $getFyId[0]->financial_year_id: null;
        $financial_year_name = DB::table('financial_years')->where('id',$financial_year_id)->first()->name;

        if($is_export == 'true'){
            $perPage = 10000;
        }

        $query = BudgetReallocationItem::with('budget_export_account', 'budget_reallocation')
                        ->whereHas('budget_export_account', function ($q) use ($admin_hierarchy_id) {
                            $q->where('admin_hierarchy_id', $admin_hierarchy_id);
                        })->whereHas('budget_reallocation', function($q1) use ($decision_level_id){
                            $q1->whereHas('budget_reallocation_approve', function($q2) use ($decision_level_id){
                                $q2->where('decision_level_id', $decision_level_id)
                                ->where('is_open', true);
                            });
                })->where('is_approved', false)
                ->where('is_rejected', false);

        $reallocations = $query->paginate($perPage);
        $totalAmount = $query->sum('amount');

        $prev_decision_level = $user->decision_level_id - 1;

        $bri = $query->select('budget_reallocation_id')->first();
        $transaction_id = $admin_hierarchy_id.$bri->budget_reallocation_id.date('dmYHis');

        $array = array();
        foreach ($reallocations as &$r) {
            $r->from_gfs = GfsCode::where('code', substr($r->from_chart_of_accounts, -8))->first()->description;
            $r->to_gfs = GfsCode::where('code', substr($r->to_chart_of_accounts, -8))->first()->description;
            //get balance
            $expenditure = DB::select("select sum(debit_amount::numeric(12,2)) - sum(credit_amount::numeric(12,2)) as amount
                                                FROM budget_import_items
                                              where budget_export_account_id = $r->id");
            $r->budget_balance =  $r['budget_export_account']['amount'] -  $expenditure[0]->amount;
            //decision level
            $reallocation_id = $r->budget_reallocation_id;
            $url = BudgetReallocationApprove::where('budget_reallocation_id', $reallocation_id)
                                             ->where('decision_level_id', $prev_decision_level)
                                             ->select('document_url')
                                             ->first();
            if(isset($url->document_url)){
                $path = $url->document_url;
                $document_url_pre = 'uploads/reallocations/'.$path;
            }
            /**apprnd array if is export **/
            if($is_export == 'true'){
                $data = array();
                $data['FROM_ACCOUNT'] = $r->from_chart_of_accounts;
                $data['DESCRIPTION'] =  $r->from_gfs;
                $data['FROM_AMOUNT'] =  $r->budget_balance;
                $data['TO_ACCOUNT'] = $r->to_chart_of_accounts;
                $data['TO_GFS'] = $r->to_gfs;
                $data['TO_AMOUNT'] = $r->amount;
                array_push($array, $data);
            }
            
            $balanceRequestItems[] = $r['from_chart_of_accounts'];

            $bbt = new BudgetBalanceTransaction();
            $bbt->chart_of_accounts = $r->from_chart_of_accounts;
            $bbt->budget = $r['budget_export_account']['amount'];
            $bbt->request_time = date('Y-m-d H:i:s');
            $bbt->transaction_id = $transaction_id;
            $bbt->financial_year_id = $financial_year_id;
            $bbt->save();

        }


        $trnumber == 'TR196'?$receiver = 'NAVISION':$receiver = 'MUSE';

        $messageHeader = array(
            "sender" => "PLANREPOTR",
            "receiver" => $receiver,
            "msgId" => $transaction_id,
            "messageType" => "BUDGET_BALANCE_REQUEST",
            "paymentType"=> "NA"
        );
        $messageSummary = array(
            "financialYear"=> $financial_year_name,
            "createdAt" => date('Y-m-d H:i:s')
        );

        $messageDetails = $balanceRequestItems;

        $item = array(
            "messageHeader" => $messageHeader,
            "messageSummary" => $messageSummary,
            "messageDetails" => $messageDetails
        );

        $data = json_encode($item);
        $plainText = $data;
        $privateKey = openssl_pkey_get_private("file://" . base_path() . "/config/keys/private.pem");
        $publicKey = openssl_pkey_get_public("file://" . base_path() . "/config/keys/public.pem");

        // Make a signature
        openssl_sign($plainText, $signature, $privateKey, OPENSSL_ALGO_SHA256);
        $signature = base64_encode($signature);

        $payloadToMuse = array(
            'message' => $item,
            'digitalSignature'=>$signature
        );


        try {
                if($trnumber == 'TR196'){
                    $response =  app('App\Http\Controllers\Execution\SocialSecurityIntegrationController')->sendRequestToNavision($payloadToMuse);

                }else{
                    $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($payloadToMuse);
                }
            } catch (\Throwable $th) {
                //throw $th;
            }

        if($is_export == 'true'){
            Excel::create('BUDGET_REALLOCATION' . time(), function ($excel) use ($array) {
                $excel->sheet('DATA', function ($sheet) use ($array) {
                    $sheet->fromArray($array);
                });
            })->export('csv');
        }else {
            return response()->json(['reallocations'=>$reallocations, 'totalAmount'=>$totalAmount, 'document_url'=>$document_url_pre]);
        }
    }

    public function returnReallocation($id, $decisionLevelId){

        try{
            DB::beginTransaction();
                $user = UserServices::getUser();
                $defaultDecisionLevel = ConfigurationSetting::where('key', 'PLANREP_REALLOCATION_DEFAULT_DECISION_LEVEL')->first();
                $isOpen = ($defaultDecisionLevel->value == $decisionLevelId)?true :false;
                BudgetReallocation::find($id)->update(['decision_level_id'=>$decisionLevelId, 'is_open'=>$isOpen]);
                DB::table('budget_reallocation_approvals')
                ->where('budget_reallocation_id',$id)
                ->where('decision_level_id',$user->decision_level_id)
                ->where('is_open', true)
                ->update(['is_open'=>false]);
            DB::commit();
            return ['successMessage'=>'Reallocation Returned'];
        }
        catch(\Exception $e){
            DB::rollback();
            Log::error($e);
            return response()->json(['errorMessage' => 'Error Returning reallocation', 'exception'=>$e], 500);
        }

    }

    public function getCouncils(){
        $user = UserServices::getUser();
       //get admin hierarchies
       $admin_hierarchies = DB::select("SELECT DISTINCT ah.id FROM
                            budget_reallocation_approvals ap
                            join budget_reallocations r on ap.budget_reallocation_id = r.id
                            join budget_reallocation_items it on it.budget_reallocation_id = r.id
                            join budget_export_accounts ac on ac.id = it.budget_export_account_id
                            join budget_export_accounts ac1 on ac1.id = it.budget_export_account_id
                            join admin_hierarchies ah on ac.admin_hierarchy_id = ah.id
                            WHERE ap.decision_level_id = $user->decision_level_id
                            and ap.is_open = true and it.is_approved = false and it.is_rejected=false");
        $array = $this->toArray($admin_hierarchies);
        $array_list = array_pluck($array, 'id');
        $hierarchies = [];
        //get councils for council users
        if($user->decision_level_id == 3 &&  in_array($user->admin_hierarchy_id, $array_list)){
            $admin_hierarchy = $user->admin_hierarchy_id;
            $hierarchies = AdminHierarchy::where('id', $admin_hierarchy)->get();
        //get councils for regions
        }else if($user->decision_level_id == 4){
            $admin_hierarchy = $user->admin_hierarchy_id;
            $hierarchies = AdminHierarchy::whereIn('id', $array_list)
                           ->where('parent_id', $admin_hierarchy)->get();
        //get councils for ministry level
        }else if($user->decision_level_id == 5){
            $hierarchies = AdminHierarchy::whereIn('id', $array_list)->get();
        }
        //get piscs for TR level
        else if($user->decision_level_id == 1){
            $hierarchies = AdminHierarchy::whereIn('id', $array_list)->get();
        }
        $returnDecisionLevel = DecisionLevel::getReturnDecisionLevels($user->decision_level_id);
        $data = ['admin_hierarchies' => $hierarchies, 'user' => $user,'returnDecisionLevels'=>$returnDecisionLevel];
        return response()->json($data);
    }

    /** convert to array */
    public function toArray($result){
        $result = array_map(function ($value) {
            return (array)$value;
        }, $result);
        return $result;
    }

    /** approve reallocation */
    public function approveReallocation(Request $request){
        $items    = Input::get('data');
        $comments = Input::get('comments');
        $user     = UserServices::getUser();
        $fileName = null;
        if($user->decision_level_id != 1){
            if (Input::hasFile('file')) {
                $destinationPath = 'uploads/reallocations'; // upload path
                $extension = Input::file('file')->getClientOriginalExtension(); // getting image extension
                $fileName = md5(time()).'.'.$extension; // renaming image
                Input::file('file')->storeAs($destinationPath, $fileName); // uploading file to given path
            }
        }

        $next_decision_level = $user->decision_level_id == 1?null:$user->decision_level_id + 1;
        $close_reallocation = $user->decision_level_id == 1?true:false;

        /** check user decision level */
        foreach ($items as $data) {
            //appproved items instantiated
            $approved_items = array();
            foreach($data as $key => $value){
                $budget_reallocation_id = $value['budget_reallocation']['id'];
                //close the current desicision level
                BudgetReallocationApprove::where('budget_reallocation_id', $budget_reallocation_id)
                    ->where('decision_level_id', $user->decision_level_id )
                    ->update(['user_id'=>$user->id, 'comments'=>$comments, 'document_url'=>$fileName, 'is_open'=>$close_reallocation, 'is_approved'=>true]);
                //open the next decision level
                if($next_decision_level != null){
                    //check if budget reallocation for next level exist
                    $exists = BudgetReallocationApprove::where('budget_reallocation_id', $budget_reallocation_id)
                                ->where('decision_level_id', $next_decision_level)
                                ->where('is_open', true)
                                ->select('id')
                                ->first();
                    if(!isset($exists->id)){
                        //open reallocation to the next level
                        $obj = new BudgetReallocationApprove();
                        $obj->budget_reallocation_id = $budget_reallocation_id;
                        $obj->decision_level_id = $next_decision_level;
                        $obj->is_open = true;
                        $obj->user_id = $user->id;
                        $obj->comments = 'FORWARDED FOR ACTION';
                        $obj->save();
                    }
                }else {
                    //if the decision level is final then approve reallocation items
                    BudgetReallocationItem::where('id', $value['id'])->update(['is_approved'=>true, 'comments'=>$comments, 'approved_by'=>$user->id]);
                    //push item to approved items
                    array_push($approved_items, $value);
                }
            }
            if(count($approved_items) > 0){
                //convert items to object and then create transactions
                $this->createTransaction($approved_items);
            }
        }
        $success = ['successMessage' => 'REALLOCATION_BUDGET_APPROVED'];
        return response()->json($success);
    }

    /** create transaction */
    public function createTransaction($data){
        try {

            DB::transaction(function () use ($data) {
                $userId = UserServices::getUser()->id;
                $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;

                $transactionType = ConfigurationSetting::where('key', 'REALLOCATION_TRANSACTION_TYPE')->first();
                $budgetGroupName = ConfigurationSetting::where('key', 'REALLOCATION_BUDGET_GROUP_NAME')->first();
                $transaction = new BudgetExportTransaction();
                $transaction->budget_transaction_type_id = $transactionType->value;
                $transaction->description = "APPROVED REALLOCATIONS";
                $transaction->save();
                //enter transaction control number
                $control_number = 'REALLOC-' . $transaction->id;
                $transaction->control_number = $control_number;
                $transaction->save();
                //end of control number update
                foreach ($data as $reallocation) {
                    $fromTransactionItem = BudgetExportTransactionItemService::create(
                        $transaction['id'],
                        $reallocation['budget_export_account_id'],
                        (-1 * $reallocation['amount']),
                        $reallocation['id'],
                        false,
                        $reallocation['created_by']
                    );

                    $fromAccount = BudgetExportAccount::find($reallocation['budget_export_account_id']);
                    $fromAccount->amount = $fromAccount->amount - $reallocation['amount'];
                    $fromAccount->updated_by = UserServices::getUser()->id;
                    $fromAccount->save();

                    $toTransactionItem = BudgetExportTransactionItemService::create(
                        $transaction->id,
                        $reallocation['budget_export_to_account_id'],
                        $reallocation['amount'],
                        $reallocation['id'],
                        true,
                        $reallocation['created_by']
                    );


                    $toAccount = BudgetExportAccount::find($reallocation['budget_export_to_account_id']);
                    $toAccount->amount = $toAccount->amount + $reallocation['amount'];
                    $toAccount->updated_by = UserServices::getUser()->id;
                    $toAccount->save();

                    BudgetExportToFinancialSystemService::create(
                        $adminHierarchyId,
                        $toTransactionItem->id,
                        $toAccount->chart_of_accounts,
                        $budgetGroupName->value,
                        $toAccount->financial_year_id,
                        $reallocation['amount'],
                        $toAccount->financial_system_code,
                        $userId
                    );

                    $reallocationItem = BudgetReallocationItem::find($reallocation['id']);
                    $reallocationItem->is_approved = true;
                    $reallocationItem->comments = isset($data->comments) ? $data->comments : 'Approved';
                    $reallocationItem->approved_by = UserServices::getUser()->id;
                    $reallocationItem->save();

                    $this->tryUnlockAccounts($reallocation);

                }
            });
        } catch (QueryException $e) {
           echo $e->errorMessage();
        }
    }

    private function tryUnlockAccounts($reallocation)
    {
        $otherFromExists = DB::table('budget_export_accounts as acc')->join('budget_reallocation_items as al', 'acc.id', 'al.budget_export_account_id')->where('acc.id', $reallocation['budget_export_account_id'])->where('al.id', '<>', $reallocation['id'])->first();
        if ($otherFromExists == null) {
            DB::table('budget_export_accounts')->where('id', $reallocation['budget_export_account_id'])->update(array('is_locked' => false));
        }

        $otherToExists = DB::table('budget_export_accounts as acc')->join('budget_reallocation_items as al', 'acc.id', 'al.budget_export_account_id')->where('acc.id', $reallocation['budget_export_to_account_id'])->where('al.id', '<>', $reallocation['id'])->first();
        if ($otherToExists == null) {
            DB::table('budget_export_accounts')->where('id', $reallocation['budget_export_to_account_id'])->update(array('is_locked' => false));
        }
    }

    /** reject reallocations */
    public function rejectReallocation(Request $request){
        $user = UserServices::getUser();
        $data = json_decode($request->getContent());

        $next_decision_level = $user->decision_level_id == 5?null:$user->decision_level_id - 1;

        $items    = $data->reallocationsToApprove;
        $comments = $data->comments;
        /** check user decision level */
        foreach($items as $key => $value){
            $budget_reallocation_id = $value->budget_reallocation->id;
            //update items to reallocate
            BudgetReallocationItem::where('id', $value->id)->update(['is_rejected' => true, 'is_approved'=>false, 'comments'=>$comments]);

            //insert response
            $obj = new BudgetReallocationResponse();
            $obj->updateOrCreate(
                ['budget_reallocation_item_id' => $value->id, 'created_by' => $user->id],
                ['response' => $comments, 'is_approved' => false]
            );
            $user_id = DB::table('budget_reallocation_items')
                ->where('id',  $value->id)
                ->select('created_by')
                ->first()->created_by;
        }

        $_users = User::where('id',$user_id)->get();
        $message = "Budget reallocation has been rejected. Message: " . $comments;
        $url = url('/notifications#!/');
        $object_id = 0;
        $job = new sendNotifications($_users, $object_id, $url, $message, "fa fa-book");
        //dispatch($job->delay(10));

        $success = ['successMessage' => 'Reallocation item(s) rejected successfull'];
        return response()->json($success);
    }

}
