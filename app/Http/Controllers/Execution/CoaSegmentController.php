<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Jobs\generateCoaSegments;
use App\Jobs\sendNotifications;
use App\Models\Auth\User;
use App\Models\Execution\CoaSegment;
use App\Models\Execution\CoaSegmentCompany;
use App\Models\Setup\Facility;
use App\Models\Setup\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;


class CoaSegmentController extends Controller
{
    public $changes;

    public function runSegmentJob()
    {
        $user_id = UserServices::getUser()->id;
        $users = User::where('id', $user_id)->get();
        $url = url('/notifications#!/');
        $message_text = "The chart of account segments have been updated. Would you want to send them to Epicor?";
        $object_id = 0;
        $icon = "fa fa-list";

        $job = new generateCoaSegments();
        $this::dispatch($job);

        $job = new sendNotifications($users, $object_id, $url, $message_text, $icon);
        $this::dispatch($job->delay(60));
    }

    public static function addFacilities()
    {
        $thisClass = new CoaSegmentController();
        $user_id = UserServices::getUser()->id;

        //Segment 6
        $facilities = Facility::all();

        $status = DB::transaction(function () use ($facilities, $thisClass, $user_id) {
            foreach ($facilities as $facility) {
                if ($facility->code != null and $facility->name != null) {
                    $thisClass::insertSegment($user_id, "BUDGETP", "6", $facility->code, "Facility", $facility->name, "", "");
                }
            }
        });
    }

    public static function addProjects()
    {
        $thisClass = new CoaSegmentController();

        //Segment 7
        $projects = Project::all();

        $status = DB::transaction(function () use ($projects, $thisClass) {
            foreach ($projects as $project) {
                $thisClass::insertSegment($project->id, "BUDGETP", "7", $project->code, "Project", $project->name, "", "");
            }
        });
    }

    public static function insertSegment($user_id, $coa_code, $segment_number, $segment_code, $segment_name, $segment_description, $normal_balance, $category)
    {
        $count = CoaSegment::where('segment_code', $segment_code)->count();
        if ($count < 1) {
            $segment_id = DB::table('coa_segments')->insertGetId(

                array(
                    "coa_code" => $coa_code,
                    "segment_number" => $segment_number,
                    "segment_code" => $segment_code,
                    "segment_name" => $segment_name,
                    "segment_description" => $segment_description,
                    "normal_balance" => $normal_balance,
                    "created_by" => $user_id,
                    "is_active" => false,
                    "category" => $category
                )
            );
        } else {
            $segment = CoaSegment::where('segment_code', $segment_code)->first();
            $segment_id = $segment->id;
        }
        return $segment_id;
    }

    public function getSegments(Request $request)
    {
        $perPage = (int)$request->perPage;
        $admin_hierarchy_id = (int)$request->admin_hierarchy_id;
        if($perPage == 0){
            $perPage = 10;
        }
        $all = DB::table("coa_segment_company as cs")
            ->join("coa_segments as c","c.id","cs.coa_segment_id")
            ->join("admin_hierarchies as a","cs.admin_hierarchy_id", "a.id")
            ->where("is_cleared",false)
            ->where("is_delivered", false)
            ->where("a.id",$admin_hierarchy_id)
            ->whereNotNull("cs.response")
            ->select("cs.*","c.coa_code")
            ->paginate($perPage);
        return response()->json($all,200);
    }

    public function clearCoaSegments($id)
    {
        $c = CoaSegmentCompany::find($id);
        $c->is_cleared = true;
        $c->save();
        $all = DB::table("coa_segment_company as cs")
            ->join("coa_segments as c","c.id","cs.coa_segment_id")
            ->where("is_cleared",false)
            ->whereNotNull("cs.response")
            ->select("cs.*","c.coa_code")
            ->paginate(10);
        $message = ["successMessage" => "STATUS_CLEARED_SUCCESSFULLY", "coa_segments" => $all];
        return response()->json($message);
    }
}

