<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Execution\ReceivedFund;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class ReceivedFundController extends Controller
{
    public function index() {
        $all = ReceivedFund::with('data_source','import_method')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return ReceivedFund::with('data_source','import_method')->orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new ReceivedFund();
            $obj->reference_code = $data->reference_code;
            $obj->data_source_id = $data->data_source_id;
            $obj->import_method_id = $data->import_method_id;
            $obj->created_by = UserServices::getUser()->id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "FUND_RECEIVED_SUCCESSFULLY", "receivedFunds" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = ReceivedFund::find($data->id);
            $obj->reference_code = $data->reference_code;
            $obj->data_source_id = $data->data_source_id;
            $obj->import_method_id = $data->import_method_id;
            $obj->updated_by = UserServices::getUser()->id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "FUND_RECEIVED_INFORMATION_UPDATED_SUCCESSFULLY", "receivedFunds" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = ReceivedFund::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "FUND_RECEIVED_INFORMATION_DELETED_SUCCESSFULLY", "receivedFunds" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = ReceivedFund::with('data_source','import_method')->orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        ReceivedFund::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "FUND_RECEIVED_RESTORED", "trashedReceivedFunds" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        ReceivedFund::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "FUND_RECEIVED_DELETED_PERMANENTLY", "trashedReceivedFunds" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = ReceivedFund::onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $message = ["successMessage" => "FUND_RECEIVED_DELETED_PERMANENTLY"];
        return response()->json($message, 200);
    }
}
