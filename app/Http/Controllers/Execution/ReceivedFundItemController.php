<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Execution\ReceivedFundItem;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\FinancialYear;
use App\Models\Setup\Section;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class ReceivedFundItemController extends Controller {
    public function index() {
        $financial_year_id = FinancialYearServices::getExecutionFinancialYear()->id;
        //Execution event

        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section_id = UserServices::getUser()->section_id;

        $query_result = DB::table('received_fund_items')
            ->join('admin_hierarchy_ceilings','received_fund_items.admin_hierarchy_ceiling_id', '=', 'admin_hierarchy_ceilings.id')
            ->join('periods','received_fund_items.period_id', '=', 'periods.id')
            ->join('fund_sources','received_fund_items.fund_source_id', '=', 'fund_sources.id')
            ->join('gfs_codes','received_fund_items.gfs_code_id', '=', 'gfs_codes.id')
            ->where('admin_hierarchy_ceilings.section_id','=',$section_id)
            ->where('admin_hierarchy_ceilings.admin_hierarchy_id','=',$admin_hierarchy_id)
            ->where('admin_hierarchy_ceilings.financial_year_id','=',$financial_year_id)
            ->select('received_fund_items.amount as received_amount',
                'received_fund_items.reference_code as received_amount_reference_number',
                'periods.name as quarter', 'fund_sources.name as fund_source_name',
                'gfs_codes.name as gfs_code_name', 'admin_hierarchy_ceilings.amount
                                     as ceiling_amount', 'received_fund_items.date_received')
            ->get();
        return response()->json($query_result);
    }

    public function getAllPaginated($perPage) {
        $financial_year_id = isset(FinancialYearServices::getExecutionFinancialYear()->id)?FinancialYearServices::getExecutionFinancialYear()->id:null;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section_id = UserServices::getUser()->section_id;
        $query_result = DB::table('received_fund_items')
            ->join('admin_hierarchy_ceilings','received_fund_items.admin_hierarchy_ceiling_id', '=', 'admin_hierarchy_ceilings.id')
            ->join('periods','received_fund_items.period_id', '=', 'periods.id')
            ->join('fund_sources','received_fund_items.fund_source_id', '=', 'fund_sources.id')
            ->join('gfs_codes','received_fund_items.gfs_code_id', '=', 'gfs_codes.id')
            ->where('admin_hierarchy_ceilings.section_id','=',$section_id)
            ->where('admin_hierarchy_ceilings.admin_hierarchy_id','=',$admin_hierarchy_id)
            ->where('admin_hierarchy_ceilings.financial_year_id','=',$financial_year_id)
            ->select('received_fund_items.amount as received_amount',
                'received_fund_items.reference_code as received_amount_reference_number',
                'periods.name as quarter', 'fund_sources.name as fund_source_name',
                'gfs_codes.name as gfs_code_name',
                'admin_hierarchy_ceilings.amount as ceiling_amount',
                'received_fund_items.date_received')
            ->paginate($perPage);
        return ($query_result);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $fund_source_id = $data->fund_source_id;
            $gfs_code_id = $data->gfs_code_id;
            $amount = $data->amount;
            $date_received = $data->date_received;
            $period_id = $data->period_id;
            $received_fund_id = $data->received_fund_id;
            $reference_code = (isset($data->reference_code)) ? $data->reference_code : null;

            $admin_hierarchy_ceiling_id = DB::table('admin_hierarchy_ceilings')
                ->join('ceilings', 'ceilings.id', '=', 'admin_hierarchy_ceilings.ceiling_id')
                ->join('gfs_codes', 'gfs_codes.id', '=', 'ceilings.gfs_code_id')
                ->join('fund_sources', 'fund_sources.id', '=', 'gfs_codes.fund_source_id')
                ->where('gfs_codes.id', $gfs_code_id)
                ->where('gfs_codes.is_active', true)
                ->where('fund_sources.id', $fund_source_id)
                ->select('admin_hierarchy_ceilings.id as admin_hierarchy_ceiling_id')
                ->distinct()->first()->admin_hierarchy_ceiling_id;

            $obj = new ReceivedFundItem();
            $obj->admin_hierarchy_ceiling_id = $admin_hierarchy_ceiling_id;
            $obj->amount = (int)$amount;
            $obj->date_received = $date_received;
            $obj->reference_code = $reference_code;
            $obj->period_id = $period_id;
            $obj->received_fund_id = $received_fund_id;
            $obj->fund_source_id = $fund_source_id;
            $obj->gfs_code_id = $gfs_code_id;
            $obj->received_fund_id = $received_fund_id;
            $obj->created_by = UserServices::getUser()->id;
            $obj->save();
            $all = $this->getAllPaginated(10);
            $message = ["successMessage" => "FUND_ITEM_ADDED_SUCCESSFULLY", "receivedFundItems" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        $fund_source_id = $data->fund_source_id;
        $gfs_code_id = $data->gfs_code_id;
        $amount = $data->amount;
        $date_received = $data->date_received;
        $period_id = $data->period_id;
        $received_fund_id = $data->received_fund_id;
        $reference_code = (isset($data->reference_code)) ? $data->reference_code : null;

        $admin_hierarchy_ceiling_id = DB::table('admin_hierarchy_ceilings')
            ->join('ceilings', 'ceilings.id', '=', 'admin_hierarchy_ceilings.ceiling_id')
            ->join('gfs_codes', 'gfs_codes.id', '=', 'ceilings.gfs_code_id')
            ->join('fund_sources', 'fund_sources.id', '=', 'gfs_codes.fund_source_id')
            ->where('gfs_codes.id', $gfs_code_id)
            ->where('gfs_codes.is_active', true)
            ->where('fund_sources.id', $fund_source_id)
            ->select('admin_hierarchy_ceilings.id as admin_hierarchy_ceiling_id')
            ->distinct()->first()->admin_hierarchy_ceiling_id;
        try {
            $obj = ReceivedFundItem::find($data->id);
            $obj->admin_hierarchy_ceiling_id = $admin_hierarchy_ceiling_id;
            $obj->amount = (int)$amount;
            $obj->date_received = $date_received;
            $obj->reference_code = $reference_code;
            $obj->period_id = $period_id;
            $obj->received_fund_id = $received_fund_id;
            $obj->fund_source_id = $fund_source_id;
            $obj->gfs_code_id = $gfs_code_id;
            $obj->updated_by = UserServices::getUser()->id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "FUND_ITEM_INFORMATION_UPDATED_SUCCESSFULLY", "receivedFundItems" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = ReceivedFundItem::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "FUND_ITEM_INFORMATION_DELETED_SUCCESSFULLY", "receivedFundItems" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = ReceivedFundItem::with('admin_hierarchy_ceiling', 'period', 'received_fund')->orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        ReceivedFundItem::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "FUND_ITEM_RESTORED", "trashedReceivedFundItems" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        ReceivedFundItem::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "FUND_ITEM_DELETED_PERMANENTLY", "trashedReceivedFundItems" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = ReceivedFundItem::onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $message = ["successMessage" => "FUND_ITEM_DELETED_PERMANENTLY"];
        return response()->json($message, 200);
    }

    public function pdf() {
        $financial_year_id = FinancialYearServices::getExecutionFinancialYear()->id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section_id = UserServices::getUser()->section_id;
        $admin_hierarchy = AdminHierarchy::find($admin_hierarchy_id);
        $section = Section::find($section_id);
        $financial_year = FinancialYear::find($financial_year_id);
        $results = DB::table('received_fund_items')
            ->join('admin_hierarchy_ceilings','received_fund_items.admin_hierarchy_ceiling_id', '=', 'admin_hierarchy_ceilings.id')
            ->join('periods','received_fund_items.period_id', '=', 'periods.id')
            ->join('fund_sources','received_fund_items.fund_source_id', '=', 'fund_sources.id')
            ->join('gfs_codes','received_fund_items.gfs_code_id', '=', 'gfs_codes.id')
            ->where('admin_hierarchy_ceilings.section_id','=',$section_id)
            ->where('admin_hierarchy_ceilings.admin_hierarchy_id','=',$admin_hierarchy_id)
            ->where('admin_hierarchy_ceilings.financial_year_id','=',$financial_year_id)
            ->select('received_fund_items.amount as received_amount',
                'received_fund_items.reference_code as received_amount_reference_number',
                'periods.name as quarter', 'fund_sources.name as fund_source_name',
                'gfs_codes.name as gfs_code_name',
                'admin_hierarchy_ceilings.amount as ceiling_amount',
                'received_fund_items.date_received')
            ->get();
        $html = "";
        $style = "style='border: 1px solid #444;padding:4px;font-size:9px'";
        $html .= "<div style='height: 120px;'>";
        $html .= "<div style='float: left;width: 30%;'>";
        $html .= "<img src='".public_path()."/images/coa.png' height='100'/>";
        $html .= "<h4>The United Republic of Tanzania</h4>";
        $html .= "</div>";
        $html .= "<div style='float: right;width: 66%;'>";
        $html .= "<div>";
        $html .= "<h1 style='color: green;'>Funds Received: ".$financial_year->name."</h1>";
        $html .= "<h3>".$admin_hierarchy->name."[".$section->name."]</h3>";
        $html .= "</div>";
        $html .= "</div>";
        $html .= "</div>";
        $html .= "<br />";
        $html .= "<hr>";
        $html .= "<div>";
        $html .= "<table style='border-collapse: collapse;border: 1px solid #444;width: 100%;'>";
        $html .= "<tr style='background-color: gold'>";
        $html .= "<th $style>SNO</th>";
        $html .= "<th $style>FUND_SOURCE</th>";
        $html .= "<th $style>REVENUE_SOURCE</th>";
        $html .= "<th $style>CEILING</th>";
        $html .= "<th $style>FUND_RECEIVED</th>";
        $html .= "<th $style>DATE_RECEIVED</th>";
        $html .= "<th $style>RECEIPT_NUMBER</th>";
        $html .= "<th $style>REFERENCE_CODE</th>";
        $html .= "<th $style>PERIOD</th>";
        $html .= "</tr>";
        $total = 0;
        $i = 1;
        foreach ($results as $result) {
            $fund_source = $result->fund_source_name;
            $gfs_code = "[".$result->gfs_code_name."]".$result->gfs_code_name;
            $ceiling = $result->ceiling_amount;
            $fund_received = $result->received_amount;
            $date_received = $result->date_received;
            $receipt_number = $result->received_amount_reference_number;
            $reference_code = $result->received_amount_reference_number;
            $period = $result->quarter;
            $total = $total + $fund_received;
            $html .= "<tr>";
            $html .= "<td $style >$i</td>";
            $html .= "<td $style >$fund_source</td>";
            $html .= "<td $style>$gfs_code</td>";
            $html .= "<td $style><span style='text-align: right'>".number_format($ceiling)."</span></td>";
            $html .= "<td $style><span style='text-align: right'>".number_format($fund_received)."</span></td>";
            $html .= "<td $style>$date_received</td>";
            $html .= "<td $style>$receipt_number</td>";
            $html .= "<td $style>$reference_code</td>";
            $html .= "<td $style>$period</td>";
            $html .= "</tr>";
            $i++;
        }
        $html .= "<tr>";
        $html .= "<td $style></td>";
        $html .= "<td $style></td>";
        $html .= "<td $style></td>";
        $html .= "<td $style><span style='text-align: right'>".number_format($total)."</span></td>";
        $html .= "<td $style></td>";
        $html .= "<td $style></td>";
        $html .= "<td $style></td>";
        $html .= "<td $style></td>";
        $html .= "<td $style></td>";
        $html .= "</tr>";
        $html .= "</table>";
        $html .= "</div>";
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($html)->setPaper('a4', 'landscape')->setWarnings(false);
        return $pdf->download('ReceivedFunds.pdf');
    }
}
