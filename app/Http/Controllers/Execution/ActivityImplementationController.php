<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\CustomPager;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Execution\ActivityImplementation;
use App\Models\Execution\ActivityImplementationAttachment;
use App\Models\Planning\ActivityFacility;
use App\Models\Planning\ActivityPeriod;
use App\Models\Setup\Period;
use App\Models\Setup\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Models\Setup\ConfigurationSetting;

class ActivityImplementationController extends Controller
{
    public function index()
    {
        $all = ActivityImplementation::with('activity', 'attachments')->get();
        return response()->json($all);
    }

    public function activityFacilityBudget(Request $request)
    {
        $activityId = $request->activity_id;
        $facilityId = $request->facility_id;
        return response()->json(["budget" => ActivityImplementation::activityFacilityBudget($activityId, $facilityId)]);
    }

    public function activityFacilityBudgetExpenditure(Request $request)
    {
        $activityId = $request->activity_id;
        $period_id = $request->period_id;
        try {
            $ap = ActivityPeriod::where("activity_id", $activityId)
                ->where("period_id", $period_id)
                ->first();
            return response()->json(['achievement' => $ap->overall_achievement]);
        } catch (\Exception $exception) {
            return response()->json(['achievement' => ""]);
        }
    }

    public function subBudgetClasses(Request $request)
    {
//        $financialYearId = ConfigurationSetting::getValueByKey('AUTO_FINANCIAL_YEAR_TO_REPORT');;
        $financialYearId = FinancialYearServices::getExecutionFinancialYear();
        $financialYearId = $financialYearId->id;
        $versionId = $this->getVersionId($request, $financialYearId, 'BC');
        $userSectionId = UserServices::getUser()->section_id;
        $userSections = Section::where('id', $userSectionId)->get();
        $flatten = new Flatten();
        $sectionIds = $flatten->flattenSectionWithChild($userSections);
        $costCentreString = implode(',', $sectionIds);
        $user = UserServices::getUser();
        $adminHierarchyId = $user->admin_hierarchy_id;
        $result = ActivityImplementation::budgetClasses($adminHierarchyId, $financialYearId, $costCentreString, $versionId);
        return response()->json(['items' => $result], 200);
    }

    public function costCentreFundSources(Request $request)
    {
        $financialYearId = $request->financialYearId;
        $facilityId = $request->facilityId;
        $versionId = $this->getVersionId($request, $financialYearId, 'FS');
        $userSectionId = UserServices::getUser()->section_id;
        $userSections = Section::where('id', $userSectionId)->get();
        $flatten = new Flatten();
        $sectionIds = $flatten->flattenSectionWithChild($userSections);
        $costCentreString = implode(',', $sectionIds);
        $budgetClassId = $request->budgetClassId;
        $user = UserServices::getUser();
        $admin_hierarchy_id = $user->admin_hierarchy_id;
        $result = ActivityImplementation::costCentreFundSources($admin_hierarchy_id, $financialYearId, $costCentreString, $facilityId, $versionId);
        return response()->json(["fund_sources" => $result], 200);
    }

    public function facilityTypes(Request $request) {

        $userSectionId = UserServices::getUser()->section_id;
        $userSections = Section::where('id', $userSectionId)->get();
        $flatten = new Flatten();
        $sectionIds = $flatten->flattenSectionWithChild($userSections);

        $ftTypes= DB::table('facility_types as ft')
            ->join('facility_type_sections as fs', 'fs.facility_type_id','ft.id')
            ->whereIn('fs.section_id', $sectionIds)
            ->distinct('ft.id')
            ->select('ft.id', 'ft.name')
            ->get();
        return ['facilityTypes'=> $ftTypes];
    }

    public function facilities(Request $request)
    {
        $facilityTypeId = $request->facilityTypeId;
        $adminId =  UserServices::getUser()->admin_hierarchy_id;

        $sql = "select f.id, f.name, ft.name as facility_type from facilities as f 
                join admin_hierarchies as ad1 on ad1.id = f.admin_hierarchy_id 
                join facility_types as ft on ft.id = f.facility_type_id
                left join admin_hierarchies as ad2 on ad2.id = ad1.parent_id
                left join admin_hierarchies as ad3 on ad3.id = ad2.parent_id
                where f.is_active=true and f.facility_type_id=$facilityTypeId and (ad1.id = $adminId or ad2.id = $adminId or ad3.id = $adminId)";
         $items = DB::select($sql);
        return response()->json(["facilities" => $items], 200);
    }

    public function implementationHistory(Request $request) {
        $activityId = $request->activityId;
        $facilityId = $request->facilityId;
       
        $actf = DB::table('activity_facilities as af')
        ->join('activities as a', 'a.id', 'af.activity_id')
        ->join('activity_periods as ap', 'ap.activity_id', 'a.id')
        ->join('periods as p', 'p.id', 'ap.period_id')
        ->leftjoin('projects as pr', 'p.id', 'a.project_id')
        ->leftjoin('activity_project_outputs as apo','apo.activity_id','a.id')
        ->leftjoin('project_outputs as po','apo.project_output_id','po.id')
        ->join('activity_statuses as ac','ac.id','af.activity_status_id')
        ->join('activity_implementations as imp', function($imp){
            $imp->on('imp.activity_id', 'a.id')->on('af.facility_id','imp.facility_id')->on('imp.period_id','p.id');
        })
        ->where('a.id', $activityId)
        ->where('af.facility_id', $facilityId)
        ->select('p.name as period_name',
          'a.description',
          'a.code',
          'pr.name as project',
          'pr.id as project_id',
          'pr.code as project_code',
          'a.indicator',
          'ac.name as activity_status',
          'ac.updated_at as updated_at',
          'a.id as activity_id',
          'ap.period_id as period_id',
          'ap.overall_achievement as overall_achievement',
          'af.facility_id',
          'po.name as project_output',
          'apo.value as planned_value',
          'imp.achievement',
          'imp.achievement_value',
          'imp.completion_percentage',
          'imp.remarks',
          'imp.latitude',
          'imp.longitude',
          'imp.start_date',
          'imp.end_date',
          'imp.project_output_implemented_value',
          'imp.procurement_method_id')
        ->get();
        return response()->json(['activityHistory' =>$actf]);

    }
   
    public function paginated(Request $request)
    {
        $perPage = Input::get('perPage',10);
        $facilityId = $request->facilityId;
        $budgetType = $request->budgetType;
       // $budgetClassId = $request->budgetClassId;
        $fundSourceId = $request->fundSourceId;
        $periodId = $request->periodId;
        $financialYearId = $request->financialYearId;
        $section_id = UserServices::getUser()->section_id;
        $section = Section::where('id', $section_id)->get();
        $flatten = new Flatten();
        $sectionIds = $flatten->flattenSectionWithChild($section);
        
        $actf = DB::table('activity_facilities as af')
            ->join('activity_facility_fund_sources as affs', 'af.id', 'affs.activity_facility_id')
            ->join('fund_sources as fs', 'fs.id', 'affs.fund_source_id')
            ->join('activities as a', 'a.id', 'af.activity_id')
            ->join('activity_periods as ap', 'ap.activity_id', 'a.id')
            ->join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')
            ->join('mtefs as m', 'm.id' ,'ms.mtef_id')
            ->join('sections as s', 's.id', 'ms.section_id')
            ->join('facilities as f', 'f.id', 'af.facility_id')
            ->join('projects as p', 'p.id', 'a.project_id')
            ->leftJoin('activity_statuses as ast','ast.id','af.activity_status_id')
            ->where('f.id', $facilityId)
            ->where('fs.id', $fundSourceId)
            ->where('a.budget_type', $budgetType)
            ->where('m.financial_year_id', $financialYearId)
            ->where('ap.period_id', $periodId)
            ->whereIn('ms.section_id',  $sectionIds)
            ->distinct('a.id','af.id')
            ->select('a.id as activity_id',
                'a.code as code', 
                's.name as cost_center',
                'af.id as activity_facility_id',
                'ap.period_id as period_id',
                'ap.overall_achievement as overall_achievement',
                'a.description as description', 
                'ast.name as activity_status',
                'f.name as facility',
                'f.id as facility_id',
                'p.name as project',
                'p.id as project_id',
                'p.code as project_code',
                'a.indicator',
                'ast.name as facility_activity_status',
                'a.indicator_value')
            ->orderBy('af.id')
            ->paginate($perPage);

        foreach($actf as &$af) {

           $af->budget = DB::select('
               select coalesce(sum(quantity*frequency*unit_price),0.00) as budget
               from activity_facility_fund_source_inputs as inp
               join activity_facility_fund_sources as aff on inp.activity_facility_fund_source_id = aff.id
               where  aff.activity_facility_id='.$af->activity_facility_id)[0]->budget;

            $afexp = DB::select('select expenditure from vw_activity_facility_quarter_expenditures 
            where period_id='.$periodId.'and activity_facility_id='.$af->activity_facility_id);

            $af->expenditure = (sizeof($afexp) == 1) ? $afexp[0]->expenditure: 0;
               
           $af->implementation = ActivityImplementation::with('attachments')
                    ->where('activity_id', $af->activity_id)
                    ->where('facility_id', $af->facility_id)
                    ->where('period_id', $af->period_id)
                    ->first();

       }
        return $actf;

    }

    public function paginatedPlan(Request $request)
    {

        if (isset($request->perPage)) {
            $perPage = $request->perPage;
        } else {
            $perPage = 10;
        }

        $sections = Flatten::userBudgetingSections();
        $keyArray = array();
        foreach ($sections as $section) {
            $id = $section->id;
            array_push($keyArray, $id);
        }

        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $financialYearId = $financialYear->id;
        $user = UserServices::getUser();
        $adminHierarchyId = $user->admin_hierarchy_id;

        $items = DB::table("activities as a")->
        join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')->
        join('mtefs as m', 'm.id', 'ms.mtef_id')->
        join('sections as sec', 'sec.id', 'ms.section_id')->
        join('activity_periods as actPer', 'actPer.activity_id', 'a.id')->
        join('periods as per', 'per.id', 'actPer.period_id')->
        join('period_groups as perGr', 'perGr.id', 'per.period_group_id')->
        leftjoin('responsible_persons as rp', 'a.responsible_person_id', 'rp.id')->
        leftjoin('activity_statuses as act_stat', 'act_stat.id', 'a.activity_status_id')->
        leftjoin('activity_facilities as acfac', 'acfac.activity_id', 'a.id')->
        leftjoin('facilities as fac', 'fac.id', 'acfac.facility_id')->
        select('a.*', 'per.name as period_name', 'per.start_date as period_start_date', 'per.end_date as period_end_date', 'rp.name as responsible_person', 'rp.title as title', 'sec.name as cost_center', 'fac.name as facility_name')->
        whereIn("ms.section_id", $keyArray)->
        where('m.financial_year_id', $financialYearId)->
        where('m.admin_hierarchy_id', $adminHierarchyId)->
        where('perGr.number', 3)->
        orderBy("per.start_date", 'asc')->
        paginate($perPage);
        return response()->json($items);
    }

    public function getActivityImplementationHistory(Request $request)
    {
        $activityId = $request->id;
        $facilityId = $request->facility_id;
        $items = ActivityImplementation::with('activity', 'status', 'attachments', 'period', 'facility', 'facility.facility_type', 'procurement_method')
            ->where("activity_id", "=", $activityId)
            ->where("facility_id", "=", $facilityId)
            ->orderBy("created_at", "desc")
            ->get();
        return response()->json(["activity_implementations" => $items], 200);
    }

    public function upload_document(Request $request)
    {
        $validation_rule = ["file" => "required|mimes:pdf,jpeg,jpg,doc,docx,dot,png,xls,xlsx,csv"];

        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $validator = Validator::make($request->all(), $validation_rule);
                if (!$validator->fails()) {
                    $file = $request->file;
                    $path = $file->store('uploads/execution');
                    return $path;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function upload_document_remove(Request $request)
    {
        $file_url = $request->file_url;

        File::delete($file_url);
        return $file_url;
    }

    public function store(Request $request)
    {
     //   $period = $request->period;
      //  $periodId = $period['id'];
        $data = $request->all();
        $validator = Validator::make($data, ActivityImplementation::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        DB::transaction(function () use ($data) {
            $obj = null;
            if (isset($data['id'])) {
                $obj = ActivityImplementation::find($data['id']);
                $obj->updated_by = UserServices::getUser()->id;
            } else {
                $obj = new ActivityImplementation();
                $obj->created_by = UserServices::getUser()->id;
            }
            $obj->activity_id = $data['activity_id'];
            $obj->facility_id = $data['facility_id'];
            $obj->status_id = $data['status_id'];
            $obj->start_date = isset($data['start_date']) ? date("Y-m-d", strtotime($data['start_date'])) : null;
            $obj->end_date = isset($data['end_date']) ? date("Y-m-d", strtotime($data['end_date'])) : null;
            $obj->procurement_method_id = isset($data['procurement_method_id']) ? $data['procurement_method_id'] : null;
            $obj->project_output_implemented_value = $data['project_output_implemented_value'];
            $obj->description = isset($data['description']) ? $data['description'] : "";
            $obj->achievement = isset($data['achievement']) ? $data['achievement'] : "";
            $obj->achievement_value = isset($data['achievement_value']) ? $data['achievement_value'] : '';
            $obj->completion_percentage = isset($data['completion_percentage']) ? $data['completion_percentage'] : 0;
            $obj->remarks = isset($data['remarks']) ? $data['remarks'] : 'None';
            $obj->period_id = $data['period_id'];
            $obj->latitude = isset($data['latitude']) ? $data['latitude'] : null;
            $obj->longitude = isset($data['longitude']) ? $data['longitude'] : null;
            $obj->save();

            $activity = ActivityFacility::where('activity_id', $data['activity_id'])
                ->where('facility_id', $data['facility_id'])
                ->first();
            $activity->activity_status_id = $data['status_id'];

            if (isset($data['completion_percentage'])) {
                $activity->completion_percentage = $data['completion_percentage'];
            }
            $activity->updated_by = UserServices::getUser()->id;
            $activity->save();
            $activityId = $data['activity_id'];
            $periodId = $data['period_id'];
            $achievement = $data['overall_achievement'];

            DB::statement("UPDATE activity_periods SET overall_achievement = '$achievement' 
                      WHERE activity_id = $activityId 
                      AND period_id = $periodId");

            $files_associative_array = array_combine($data['url_names'], $data['urls']);

            foreach ($files_associative_array as $name => $path) {

                $attachment_obj = new ActivityImplementationAttachment();
                $attachment_obj->document_url = $path;
                $attachment_obj->description = $name;
                $attachment_obj->implementation_id = $obj->id;
                $attachment_obj->save();
            }
        });
        $message = ["successMessage" => "SUCCESSFUL_ACTIVITY_IMPLEMENTATION_CREATED"];
        return response()->json($message, 200);
    }

    public function update(Request $request)
    {
        $period = $request->period;
        $periodId = $period['id'];
        $data = array_merge($request->all(), ['period_id' => $periodId]);
        $id = $data['id'];
        $validator = Validator::make($data, ActivityImplementation::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }

        DB::transaction(function () use ($request, $periodId) {
            $data = $request->all();
            $obj = ActivityImplementation::find($data['id']);
            $obj->activity_id = $data['activity_id'];
            $obj->facility_id = $data['facility_id'];
            $obj->status_id = $data['status_id'];
            $obj->description = $data['description'];
            $obj->achievement = $data['achievement'];
            $obj->start_date = isset($data['start_date']) ? date("Y-m-d", strtotime($data['start_date'])) : null;
            $obj->end_date = isset($data['end_date']) ? date("Y-m-d", strtotime($data['end_date'])) : null;
            $obj->procurement_method_id = isset($data['procurement_method_id']) ? $data['procurement_method_id'] : null;
            $obj->project_output_implemented_value = $data['project_output_implemented_value'];
            $obj->achievement_value = isset($data['achievement_value']) ? $data['achievement_value'] : '';
            $obj->completion_percentage = isset($data['completion_percentage']) ? $data['completion_percentage'] : 0;
            $obj->remarks = isset($data['remarks']) ? $data['remarks'] : 'None';
            $obj->latitude = isset($data['latitude']) ? $data['latitude'] : null;
            $obj->longitude = isset($data['longitude']) ? $data['longitude'] : null;
            $obj->updated_by = UserServices::getUser()->id;
            $obj->save();

            $activity = ActivityFacility::where('activity_id', $data['activity_id'])
                ->where('facility_id', $data['facility_id'])
                ->first();
            $activity->activity_status_id = $data['status_id'];
            if (isset($data['completion_percentage'])) {
                $activity->completion_percentage = $data['completion_percentage'];
            }
            $activity->updated_by = UserServices::getUser()->id;
            $activity->save();

            $activityId = $data['activity_id'];
            $achievement = $data['overall_achievement'];

            DB::statement("UPDATE activity_periods 
                      SET overall_achievement = '$achievement' 
                      WHERE activity_id = $activityId 
                      AND period_id = $periodId");

            $files_associative_array = array_combine($data['url_names'], $data['urls']);
            ActivityImplementationAttachment::where('implementation_id', $obj->id)->delete();
            foreach ($files_associative_array as $name => $path) {
                $attachment_obj = new ActivityImplementationAttachment();
                $attachment_obj->document_url = $path;
                $attachment_obj->description = $name;
                $attachment_obj->implementation_id = $obj->id;
                $attachment_obj->save();
            }
        });
        $data = $request->all();
        $activityId = $data['activity_id'];
        $facilityId = $data['facility_id'];
        $items = ActivityImplementation::with('activity', 'status', 'attachments', 'period')
            ->where("activity_id", "=", $activityId)
            ->where("facility_id", "=", $facilityId)
            ->orderBy("created_at", "desc")->
            get();

        $message = ["successMessage" => "SUCCESSFUL_ACTIVITY_IMPLEMENTATION_CREATED", "items" => $items];
        return response()->json($message, 200);
    }

    public function delete($id)
    {
        $object = ActivityImplementation::find($id);
        ActivityImplementationAttachment::where('activity_id', '=', $object->activity_id)->delete();
        $object->delete();
        $message = ["successMessage" => 'SUCCESSFUL_ACTIVITY_IMPLEMENTATION_DELETED'];
        return response()->json($message, 200);
    }

    public function activityFacilityProjectOut(Request $request)
    {
        $facilityId = $request->facilityId;
        $activityId = $request->activityId;
        $item = DB::table("project_outputs as po")
            ->join("activity_project_outputs as apo", "po.id", "=", "apo.project_output_id")
            ->where("apo.activity_id", $activityId)
            ->where("apo.facility_id", $facilityId)
            ->orderBy('apo.created_at', 'desc')
            ->select("po.name as project_output", "apo.value as planned_value")
            ->first();
        return response()->json(['data' => $item], 200);
    }

}

