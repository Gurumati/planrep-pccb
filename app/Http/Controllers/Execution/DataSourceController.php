<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Execution\DataSource;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class DataSourceController extends Controller {
  public function index() {
    $all = DataSource::all();
    return response()->json($all);
  }

  public function getAllPaginated($perPage) {
    return DataSource::orderBy('created_at','desc')->paginate($perPage);
  }

  public function paginated(Request $request) {
    $all = $this->getAllPaginated($request->perPage);
    return response()->json($all);
  }

  public function store(Request $request) {
    try{
      $data = json_decode($request->getContent());
      $dataSource = new DataSource();
      $dataSource->name = $data->name;
      $dataSource->code = $data->code;
      $dataSource->save();
      $all = $this->getAllPaginated($request->perPage);
      $message = ["successMessage" => "DATA_SOURCE_CREATED_SUCCESSFULLY", "dataSources" => $all];
      return response()->json($message, 200);
    } catch (QueryException $exception) {
      $error_code = $exception->errorInfo[1];
      Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
      $message = ["errorMessage" => "DATABASE_ERROR"];
      return response()->json($message, 500);
    }
  }


  public function update(Request $request) {
    $data = json_decode($request->getContent());
    try{
      $dataSource = DataSource::find($data->id);
      $dataSource->name = $data->name;
      $dataSource->code = $data->code;
      $dataSource->save();
      $all = $this->getAllPaginated($request->perPage);
      $message = ["successMessage" => "DATA_SOURCE_UPDATED_SUCCESSFULLY", "dataSources" => $all];
      return response()->json($message, 200);
    } catch (QueryException $exception) {
      $error_code = $exception->errorInfo[1];
      Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
      $message = ["errorMessage" => "DATABASE_ERROR"];
      return response()->json($message, 500);
    }
  }

  public function delete($id) {
    $dataSource = DataSource::find($id);
    $dataSource->delete();
    $all = $this->getAllPaginated(Input::get('perPage'));
    $message = ["successMessage" => "DATA_SOURCE_DELETED_SUCCESSFULLY", "dataSources" => $all];
    return response()->json($message, 200);
  }

  private function allTrashed() {
    $all = DataSource::orderBy('created_at', 'desc')->onlyTrashed()->get();
    return $all;
  }

  public function trashed() {
    $all = $this->allTrashed();
    return response()->json($all);
  }

  public function restore($id) {
    DataSource::withTrashed()->where('id', $id)->restore();
    $all = $this->allTrashed();
    $message = ["successMessage" => "DATA_SOURCE_RESTORED", "trashedDataSources" => $all];
    return response()->json($message, 200);
  }

  public function permanentDelete($id) {
    DataSource::withTrashed()->where('id', $id)->forceDelete();
    $all = $this->allTrashed();
    $message = ["successMessage" => "DATA_SOURCE_DELETED_PERMANENTLY", "trashedDataSources" => $all];
    return response()->json($message, 200);
  }

  public function emptyTrash() {
    $trashes = DataSource::orderBy('created_at', 'desc')->onlyTrashed()->get();
    foreach ($trashes as $trash) {
      $trash->forceDelete();
    }
    $all = $this->allTrashed();
    $message = ["successMessage" => "DATA_SOURCE_DELETED_PERMANENTLY", "trashedDataSources" => $all];
    return response()->json($message, 200);
  }
}
