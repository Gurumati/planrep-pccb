<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\NotificationService;
use App\Http\Services\UserServices;
use App\Jobs\SendDataToRabbitMQJob;
use App\Jobs\PublishDataToRabbitMQJob;
use App\Models\Execution\ActivityExpenditure;
use App\Models\Execution\FfarsActivity;
use App\Models\Execution\MuseActivity;
use App\Models\Execution\SetupSegmentExport;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\Facility;
use App\Models\Setup\FundType;
use App\Models\Setup\GfsCode;
use App\Models\Setup\PlanChain;
use App\Models\Setup\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;
use App\Models\Execution\BudgetExportAccount;
use App\Models\Execution\BudgetExportTransaction;
use App\Models\Setup\AdminHierarchy;




class SetupSegmentSenderController extends Controller
{

    /**
     * @param $item
     */
    private static function tinkerSend($item, $type)
    {
        $message = new AMQPMessage(json_encode($item));
        $queue_name = "SEGMENT_TO_FFARS_QUEUE";
        $exchange = "FFARSSegmentExchange";
        $exchange_type = "headers";
        $routing_key = "";
        $headers = new AMQPTable([
            'x-match' => 'all',
            'type' => $type
        ]);
        $queue_bindings = new AMQPTable([
            'x-match' => 'all',
            'type' => $type
        ]);
        sendToQueue($message, $queue_name, $exchange, $exchange_type, $routing_key, $headers, $queue_bindings);
    }

    private  function tinkerSendOpras($item, $type)
    {
        $message = new AMQPMessage(json_encode($item));
        $queue_name = "SEGMENT_TO_OPRAS_QUEUE";
        $exchange = "OPRASSegmentExchange";
        $exchange_type = "headers";
        $routing_key = "";
        $headers = new AMQPTable([
            'x-match' => 'all',
            'type' => $type
        ]);
        $queue_bindings = new AMQPTable([
            'x-match' => 'all',
            'type' => $type
        ]);
        sendToQueue($message, $queue_name, $exchange, $exchange_type, $routing_key, $headers, $queue_bindings);
    }

    public function exportAllSelectedFacilities(Request $request)
    {
        ini_set('max_execution_time', 3600);
        $facilities = $request->all();
        foreach ($facilities as $facility) {
            $item['id'] = $facility['id'];
            $item['facilityCode'] = $facility['facility_code'];
            $item['name'] = $facility['name'];
            switch ($facility['facility_type']['code']) {
                case '002':
                    $item['facilityType'] = 'P';
                    break;
                case '003':
                    $item['facilityType'] = 'S';
                    break;
                case '004':
                    $item['facilityType'] = 'DP';
                    break;
                case '005':
                    $item['facilityType'] = 'HC';
                    break;
                case '006':
                    $item['facilityType'] = 'HO';
                    break;
                default:
                    $item['facilityType'] = 'CH';
            }
            $item['wardCode'] = $facility['admin_hierarchy']['ward_code'];
            $item['phoneNumber'] = $facility['phone_number'];
            $item['email'] = $facility['email'];
            $accounts = array();
            foreach ($facility['bank_accounts'] as $bank_account) {
                $itm['accountNumber'] = $bank_account['account_number'];
                $itm['accountName'] = $bank_account['account_name'];
                $itm['bankName'] = $bank_account['bank'];
                array_push($accounts, $itm);
            }
            $item['bankAccounts'] = $accounts;
            $item['postalAddress'] = $facility['postal_address'];
            $item['deletedAt'] = $facility['deleted_at'];
            $item['is_active'] = $facility['is_active'];
            try {
                $this->send($item, "FACILITIES");
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
        $notification_message = "Facility Segments has been sent to FFARS";
        $user_id = UserServices::getUser()->id;
        $users = User::whereIn('id', [$user_id])->get();
        NotificationService::notify($users, $notification_message);
        $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public function exportAllFacilities()
    {

        $facilityArray = array();
       // $facilities = Facility::with("facility_type", "admin_hierarchy", "bank_accounts")
        $facilities = Facility::with("admin_hierarchy")
            ->where('is_delivered_to_muse', false)
            ->where('is_active', true)
            ->where('facility_ownership_id', 1)
            ->get();
        foreach ($facilities as $facility) {
           $facilityObject = array(
               "facilityCode" => $facility["facility_code"],
               "facilityName" => $facility["name"],
               "uid" => $facility["id"],
               "institutionCode" => $facility["admin_hierarchy"]["code"],
               "institutionName" => $facility["admin_hierarchy"]["name"],
               "facilityType" => $facility["facility_type"]["name"]
           );
           array_push($facilityArray,$facilityObject);
        }
       
        $control_number = app('App\Http\Controllers\Execution\MuseIntegrationController')->transactionNumber("FACILITY",9);

        $messageHeader = array(
            "sender" => "PLANREPOTR",
            "receiver" => "ERMS",
            "msgId" => $control_number,
            "applyDate" => date('Y-m-d'),
            "messageType" => "FACILITY",
            "createdAt" => date('Y-m-d H:i:s')
        );
        $messageSummary = array(
            "company"=> "00000",
            "trxCtrlNum" => $control_number,
            "applyDate"=> date('Y-m-d'),
            "description"=>"FACILITY CODE AND NAME, AND PARENT DETAIL"
        );
        $item = array(
            "messageHeader"  => $messageHeader,
            "messageSummary" => $messageSummary,
            "messageDetails" => $facilityArray
        );

        $payloadToMuse = array(
            'message' => $item,
            'digitalSignature'=>app('App\Http\Controllers\Execution\MuseIntegrationController')->signature(json_encode($item))
        );
        
        if(count($facilityArray) > 0){
            //send
            $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($payloadToMuse);
            if($response != ""){
                foreach($facilityArray as $value){
                    $facilities = Facility::where('id', $value['uid'])->update([
                        "exported_to_muse" => true
                    ]);
                }
                $feedback = ["successMessage" => "All service providers sent to MUSE"];
            }else{
                $feedback = ["successMessage" => "MUSE not accepted Service Providers"];
            }

        } else{
            $feedback = ["successMessage" => "All service providers have been delivered to MUSE"];
        }

        // $notification_message = "Facility Segments has been sent to FFARS";
        // $user_id = UserServices::getUser()->id;
        // $users = User::whereIn('id', [$user_id])->get();
        // NotificationService::notify($users, $notification_message);
       // $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return $facilityArray;// response()->json($feedback, 200);
    }

    public function exportAllSelectedSubBudgetClasses(Request $request)
    {
        ini_set('max_execution_time', 3600);
        $items = $request->all();
        foreach ($items as $value) {
            $item['id'] = $value['id'];
            $item['code'] = $value['code'];
            $item['description'] = $value['name'];
            $item['deletedAt'] = $value['deleted_at'];

            try {
                $this->send($item, "SUBBUDGETCLASSES");
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
        $notification_message = "Sub-Budget Class Segments has been sent to FFARS";
        $user_id = UserServices::getUser()->id;
        $users = User::whereIn('id', [$user_id])->get();
        NotificationService::notify($users, $notification_message);
        $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public function exportAllSubBudgetClasses()
    {
        ini_set('max_execution_time', 3600);
        $items = BudgetClass::where('exported_to_ffars', false)->get();
        foreach ($items as $value) {
            $item['id'] = $value->id;
            $item['code'] = $value->code;
            $item['description'] = $value->name;
            $item['deletedAt'] = $value->deleted_at;
            try {
                $this->send($item, "SUBBUDGETCLASSES");
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
        $notification_message = "Sub-Budget Class Segments has been sent to FFARS";
        $user_id = UserServices::getUser()->id;
        $users = User::whereIn('id', [$user_id])->get();
        NotificationService::notify($users, $notification_message);
        $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public static function tinkerExportAllSubBudgetClasses()
    {
        ini_set('max_execution_time', 3600);
        $items = BudgetClass::all();
        foreach ($items as $value) {
            $item['id'] = $value->id;
            $item['code'] = $value->code;
            $item['description'] = $value->name;
            $item['deletedAt'] = $value->deleted_at;
            try {
                self::tinkerSend($item, 'SUBBUDGETCLASSES');
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
        $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public function exportAllObjectivesToOpras(Request $request)
    {

        ini_set('max_execution_time', 3600);
        $items = PlanChain::where("parent_id",null)
            ->select('id','description','code')
            ->get();
        $items->toArray();
        foreach ($items as $value) {
            $objectiveId = $value->id;
            $targets = ActivityExpenditure::annualTargets($request->financialYearId,$objectiveId);
                $arrayTargets = [];
            foreach ($targets as $target) {
                $targetId = $target->target_id;
                $activities = ActivityExpenditure::activities($request->financialYearId,$targetId);
                $target->activities = $activities;
                array_push($arrayTargets,$target);
            }
            $value->targets = $targets;
            try {
                $this->sendOpras($value, 'OBJECTIVES');
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
        $notification_message = "Objectives Segments has been sent to Opras";
        $user_id = UserServices::getUser()->id;
        $users = User::whereIn('id', [$user_id])->get();
        NotificationService::notify($users, $notification_message);
        $feedback = ["successMessage" => "Data is being processed and sent to Opras. You will be notified when its done..."];
        return response()->json($feedback, 200);
    }

    public function exportAllSelectedFundTypes(Request $request)
    {
        ini_set('max_execution_time', 3600);
        $items = $request->all();
        foreach ($items as $value) {
            $item['id'] = $value['id'];
            $item['code'] = $value['current_budget_code'];
            $item['description'] = $value['name'];
            $item['deletedAt'] = $value['deleted_at'];
            try {
                $this->send($item, "FUNDTYPES");
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
        $notification_message = "Fund Type Segments has been sent to FFARS";
        $user_id = UserServices::getUser()->id;
        $users = User::whereIn('id', [$user_id])->get();
        NotificationService::notify($users, $notification_message);
        $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public function exportAllFundTypes()
    {
        ini_set('max_execution_time', 3600);
        $items = FundType::where('exported_to_ffars', false)->get();
        foreach ($items as $value) {
            $item['id'] = $value->id;
            $item['code'] = $value->current_budget_code;
            $item['description'] = $value->name;
            $item['deletedAt'] = $value->deleted_at;
            try {
                $this->send($item, "FUNDTYPES");
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
        $notification_message = "Fund Type Segments has been sent to FFARS";
        $user_id = UserServices::getUser()->id;
        $users = User::whereIn('id', [$user_id])->get();
        NotificationService::notify($users, $notification_message);
        $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public static function tinkerExportAllFundTypes()
    {
        ini_set('max_execution_time', 3600);
        $items = FundType::all();
        foreach ($items as $value) {
            $item['id'] = $value->id;
            $item['code'] = $value->current_budget_code;
            $item['description'] = $value->name;
            $item['deletedAt'] = $value->deleted_at;
            try {
                self::tinkerSend($item, 'FUNDTYPES');
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }

        $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public function exportAllSelectedFundSources(Request $request)
    {
        ini_set('max_execution_time', 3600);
        $items = $request->all();
        foreach ($items as $value) {
            $item['id'] = $value['id'];
            $item['code'] = $value['code'];
            $item['name'] = $value['name'];
            $item['description'] = $value['description'];
            $item['fundTypeCode'] = $value['fund_source_category']['fund_type']['current_budget_code'];
            $item['deletedAt'] = $value['deleted_at'];
            try {
                $this->send($item, "FUNDSOURCES");
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
        $notification_message = "Fund Source Segments has been sent to FFARS";
        $user_id = UserServices::getUser()->id;
        $users = User::whereIn('id', [$user_id])->get();
        NotificationService::notify($users, $notification_message);
        $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public function exportAllFundSources()
    {
        ini_set('max_execution_time', 3600);
        //$items = FundSource::with('fund_source_category.fund_type')->where('exported_to_ffars', false)->get();
        $items = DB::select('SELECT  f.*, ft.current_budget_code
                             FROM
                             fund_sources f join
                             fund_source_budget_classes b on f.id = b.fund_source_id join
                             fund_types ft on ft.id = b.fund_type_id
                             where f.exported_to_ffars = false');
        foreach ($items as $value) {
            try {
                $item['id'] = $value->id;
                $item['code'] = $value->code;
                $item['name'] = $value->name;
                $item['description'] = $value->description;
                $item['fundTypeCode'] = $value->current_budget_code;
                $item['deletedAt'] = $value->deleted_at;
                $this->send($item, "FUNDSOURCES");
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
        $notification_message = "Fund Source Segments has been sent to FFARS";
        $user_id = UserServices::getUser()->id;
        $users = User::whereIn('id', [$user_id])->get();
        NotificationService::notify($users, $notification_message);
        $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public static function tinkerExportAllFundSources()
    {
        ini_set('max_execution_time', 3600);
        $items = DB::select('SELECT  f.*, ft.current_budget_code
                             FROM
                             fund_sources f join
                             fund_source_budget_classes b on f.id = b.fund_source_id join
                             fund_types ft on ft.id = b.fund_type_id');
        foreach ($items as $value) {
            $item['id'] = $value->id;
            $item['code'] = $value->code;
            $item['name'] = $value->name;
            $item['description'] = $value->description;
            $item['fundTypeCode'] = $value->current_budget_code;
            $item['deletedAt'] = $value->deleted_at;
            try {
                self::tinkerSend($item, 'FUNDSOURCES');
            } catch (\Exception $exception) {
                Log::error($exception);
            }

        }
        $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public function exportAllSelectedProjects(Request $request)
    {
        ini_set('max_execution_time', 3600);
        $selectedProjectArray = array();
        $items = $request->all();
        foreach ($items as $value) {
            $selectedObject = array(
                "uid" => $value['id'],
                "code" => $value['code'],
                "description" => $value['name']
               );
               array_push($selectedProjectArray,$selectedObject); 
        }
        $control_number = app('App\Http\Controllers\Execution\MuseIntegrationController')->transactionNumber("PROJECT",8);

        $messageHeader = array(
            "sender" => "PLANREPOTR",
            "receiver" => "ERMS",
            "msgId" => $control_number,
            "applyDate" => date('Y-m-d'),
            "messageType" => "PROJECT",
            "createdAt" => date('Y-m-d H:i:s')
        );
        $messageSummary = array(
            "company"=> "00000",
            "trxCtrlNum" => $control_number,
            "applyDate"=> date('Y-m-d'),
            "description"=>"PROJECT CODE AND DESCRIPTION"
        );
        $item = array(
            "messageHeader"  => $messageHeader,
            "messageSummary" => $messageSummary,
            "messageDetails" => $selectedProjectArray
        );

        $payloadToMuse = array(
            'message' => $item,
            'digitalSignature'=>app('App\Http\Controllers\Execution\MuseIntegrationController')->signature(json_encode($item))
        );

        if(count($selectedProjectArray)>0){
            $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($payloadToMuse);
            if($response != ""){
                foreach($selectedProjectArray as $value){
                    Project::where('code', $value['code'])->update([
                        "exported_to_muse" => true
                    ]);
                }
            }
            $feedback = ["successMessage" => count($selectedProjectArray)." Projects was sent to MUSE. You will be notified when its done"];
        }else{
            $feedback = ["successMessage" => "No new Project to be exported to MUSE"];
        }

        // $notification_message = "Project Segments has been sent to FFARS";
        // $user_id = UserServices::getUser()->id;
        // $users = User::whereIn('id', [$user_id])->get();
        // NotificationService::notify($users, $notification_message);
        // $feedback = ["successMessage" => "YYData is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public function exportAllProjects()
    {
        ini_set('max_execution_time', 3600);
        $items = Project::where('exported_to_muse', false)->where('deleted_at',null)->get();
        $projectArray = array();
        foreach ($items as $value) {
            $object = array(
             "uid" => $value->id,
             "code" => $value->code,
             "description" => $value->name
            );
            array_push($projectArray,$object);
        }

        $control_number = app('App\Http\Controllers\Execution\MuseIntegrationController')->transactionNumber("PROJECT",8);

        $messageHeader = array(
            "sender" => "PLANREPOTR",
            "receiver" => "ERMS",
            "msgId" => $control_number,
            "applyDate" => date('Y-m-d'),
            "messageType" => "PROJECT",
            "createdAt" => date('Y-m-d H:i:s')
        );
        $messageSummary = array(
            "trxCtrlNum" => $control_number,
            "applyDate"=> date('Y-m-d'),
            "description"=>"PROJECT CODE AND DESCRIPTION"
        );
        $item = array(
            "messageHeader"  => $messageHeader,
            "messageSummary" => $messageSummary,
            "messageDetails" => $projectArray
        );

        $payloadToMuse = array(
            'message' => $item,
            'digitalSignature'=>app('App\Http\Controllers\Execution\MuseIntegrationController')->signature(json_encode($item))
        );

        if(count($projectArray)>0){
            $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($payloadToMuse);
            if($response != ""){
                Project::where('exported_to_muse', false)->where('deleted_at',null)->update([
                    "exported_to_muse" => true
                ]);
            }
            $feedback = ["successMessage" => count($projectArray)." Projects was sent to MUSE. You will be notified when its done"];
        }else{
            $feedback = ["successMessage" => "No new Project to be exported to MUSE"];
        }
        return response()->json($feedback, 200);
    }

    public static function tinkerExportAllProjects()
    {
        ini_set('max_execution_time', 3600);
        $items = Project::all();
        foreach ($items as $value) {
            $item['id'] = $value->id;
            $item['code'] = $value->code;
            $item['description'] = $value->name;
            $item['deletedAt'] = $value->deleted_at;
            try {
                self::tinkerSend($item, "PROJECTS");
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }

        $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public function exportAllSelectedGfsCodes(Request $request)
    {
        ini_set('max_execution_time', 3600);
        $gfsCodes = $request->all();
        foreach ($gfsCodes as $value) {
            $item['id'] = $value['id'];
            $item['code'] = $value['code'];
            $item['description'] = $value['name'];
            switch ($value['account_type']['code']) {
                case '01':
                    $item['category'] = 'REV';
                    break;
                    break;
                case '03':
                    $item['category'] = 'EXP';
                    break;
                default:
                    $item['category'] = 'REV';
            }
            $item['deletedAt'] = $value['deleted_at'];
            try {
                $this->send($item, "GFSCODES");
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
        $notification_message = "GFS Code Segments has been sent to FFARS";
        $user_id = UserServices::getUser()->id;
        $users = User::whereIn('id', [$user_id])->get();
        NotificationService::notify($users, $notification_message);
        $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public function exportAllGfsCodes()
    {
        ini_set('max_execution_time', 3600);
        $gfsCodes = GfsCode::with('account_type')->get();
        foreach ($gfsCodes as $value) {
            $item['id'] = $value->id;
            $item['code'] = $value->code;
            $item['description'] = $value->name;
            switch ($value->account_type->code) {
                case '01':
                    $item['category'] = 'REV';
                    break;
                    break;
                case '03':
                    $item['category'] = 'EXP';
                    break;
                default:
                    $item['category'] = 'REV';
            }
            $item['deletedAt'] = $value->deleted_at;
            try {
                $this->send($item, "GFSCODES");
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
        $notification_message = "GFS Code Segments has been sent to FFARS";
        $user_id = UserServices::getUser()->id;
        $users = User::whereIn('id', [$user_id])->get();
        NotificationService::notify($users, $notification_message);
        $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public static function tinkerExportAllGfsCodes()
    {
        ini_set('max_execution_time', 3600);
        $gfsCodes = GfsCode::all();
        foreach ($gfsCodes as $value) {
            $item['id'] = $value->id;
            $item['code'] = $value->code;
            $item['description'] = $value->name;
            switch ($value->account_type->code) {
                case '01':
                    $item['category'] = 'REV';
                    break;
                    break;
                case '03':
                    $item['category'] = 'EXP';
                    break;
                default:
                    $item['category'] = 'REV';
            }
            $item['deletedAt'] = $value->deleted_at;
            try {
                self::tinkerSend($item, "GFSCODES");
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
        $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    /* This function fetch and export objectives, service outputs and targets details per psc */
    public function exportAllObjectives(Request $request)
    {
        
        ini_set('max_execution_time', 3600);
        $financial_year_id = $request->financialYearId;
        $admin_hierarchy_id = $request->adminHierarchyId;
        $budget_type = $request->budgetType;
        $systemCode = $request->system_code;

        if ($systemCode == 0) {
            $system_code = 'OTRMIS';
        } elseif($systemCode == 1) {
            $system_code = 'MUSE';
        }elseif($systemCode == 2) {
            $system_code = 'NPMIS';
        }elseif($systemCode == 3){
            $system_code = 'ERMS';
        }

        $objectiveArray = array();
        $objectives = DB::table("plan_chains")
                              ->where('plan_chain_type_id', 2)  
                              ->where('admin_hierarchy_id', $admin_hierarchy_id)
                              ->where('is_active', true)
                              ->orWhereNull('admin_hierarchy_id')
                              ->whereNotIn('id', [738,739,1,2]) 
                              ->orderBy('code', 'asc') 
                              ->select('*')
                              ->get();

        if (sizeof($objectives) > 0 ){
            foreach ($objectives as $objective){
                            $objective_id =  $objective->id;
                            $objective_code =  $objective->code;
                            $objective_name =  $objective->description;
                            /** Find target */
                $targets = DB::table("plan_chains as pc")
                                ->join('plan_chains as pc2','pc.parent_id','=','pc2.id')
                                ->join('long_term_targets as ltt','pc.id','=','ltt.plan_chain_id')
                                ->join('mtef_annual_targets as mat','ltt.id','=','mat.long_term_target_id')
                                ->join('mtefs as m','mat.mtef_id','=','m.id')
                                ->join('mtef_sections as ms','m.id','=','ms.mtef_id')
                                ->join('sections as s','ms.section_id','=','s.id')
                                ->where('pc2.id',$objective_id)
                                ->whereNotIn('pc2.id',[1,2])
                                ->where('pc.admin_hierarchy_id',$admin_hierarchy_id)
                                ->where('m.financial_year_id',$financial_year_id)
                                ->select('s.code as costcentrecode','mat.code as targetcode','mat.description as name')
                                ->groupBy('s.code','mat.code','mat.description')
                                ->get();

                $targetArray = array();
                foreach ($targets as $target){
                                $targetObject = array(
                                    "subVote"=> $target->costcentrecode,
                                    "targetcode"=> $target->targetcode,
                                    "targetname"=> $target->name
                                );
                
                                array_push($targetArray,$targetObject);
                }
                            /** objective Object */
                            $objectiveObject = array(
                                "uid" => $objective_id,
                                "objectiveName" => $objective_name,
                                "objectiveCode" => $objective_code,
                                "targets" => $targetArray
                            );
                array_push($objectiveArray,$objectiveObject);
                }
                    //update setup_segment_exports
        //         foreach ($result as $value) {
        //             $exist = DB::table('setup_segment_exports')
        //             ->where('segment_detail_id',$value->uid)->get();
                    
        //         if ($exist->count() > 0){
        //             //  Log::info('Exists');
                
        //             }else {
        //                 $obj = new SetupSegmentExport();
        //                 $obj->segment_detail_id = $value->uid;
        //                 $obj->apply_date = Carbon::now();
        //                 $obj->is_sent = true;
        //                 $obj->admin_hierarchy_id = $admin_hierarchy_id;
        //                 $obj->financial_year_id = $financial_year_id;
        //                 $obj->budget_type = $budget_type;
        //                 $obj->segment_name = "OBJECTIVE";
        //                 $obj->system_code = $system_code;
        //                 $obj->save();

        //                // $parsedvalue = json_decode( json_encode($value), true);

        //                 $object = array(
        //                 "uid" => $value->uid,
        //                 "objCode" => $value->objcode,
        //                 "objDescription" => $value->objdescription,
        //                 "serviceOutputCode" => $value->socode,
        //                 "serviceOutputDescription" => $value->sodescription,
        //                 "targetCode" => $value->targetcode,
        //                 "targetDescription" => $value->targetdescription
        //                 );
        //                 array_push($objectiveArray,$object);
        //             }        
        // }
        $company = DB::table('admin_hierarchies')->where('id',$admin_hierarchy_id)->first()->code;
        $financialYear = DB::table('financial_years')->where('id',$financial_year_id)->first()->name;

        $options = array(
            'headers' => array('x-match' => 'all', 'Company' => $company, 'type' => 'Transaction'),
            'exchange' => 'BudgetExchange'
        );

        // $queue_name = ConfigurationSetting::getValueByKey('BUDGET_QUEUE_NAME');
        $queue_name = 'PLANREPOTR_TO_OTRMIS_DATA_QUEUE_OBJECTIVE';

        $control_number = app('App\Http\Controllers\Execution\MuseIntegrationController')->transactionNumber("OBJECTIVE",11);

        $messageHeader = array(
            "sender" => "PLANREPOTR",
            "receiver" => $system_code,
            "msgId" => $control_number,
            //"applyDate" => date('Y-m-d'),
            "messageType" => "OBJECTIVE",
            "createdAt" => date('Y-m-d H:i:s')
        );
        $messageSummary = array(
            "company" => $company,
            "trxCtrlNum" => $control_number,
            "applyDate"=> date('Y-m-d'),
            "financialYear" => $financialYear,
           // "description"=>"OBJECTIVES, SERVICE OUTPUTS AND TARGETS"
        );
        $item = array(
            "messageHeader"  => $messageHeader,
            "messageSummary" => $messageSummary,
            "messageDetails" => $objectiveArray
        );

        $payloadToNpmis = array(
            'message' => $item,
            'digitalSignature'=>app('App\Http\Controllers\Execution\NpimsIntegrationController')->signature(json_encode($item))
        );

        if(count($objectiveArray)>0){
            $response =  app('App\Http\Controllers\Execution\NpimsIntegrationController')->sendRequestToNpmis($payloadToNpmis);
            //dispatch(new PublishDataToRabbitMQJob($queue_name,$payload, $options,'objective'));

            $messageDetails = $payloadToNpmis["message"]["messageDetails"];

            $feedback = ["successMessage" => count($objectiveArray)." Objectives details was sent. You will be notified when its received"];

            if($response == "ACCEPTED"){
                foreach($messageDetails as $detail){
                   $uid = $detail["uid"];
                   DB::table('setup_segment_exports')->insert(['segment_detail_id' => $uid,'apply_date'=>date('Y-m-d'), 'is_sent' => true,
                   'response'=>'ACCEPTED','admin_hierarchy_id' => $admin_hierarchy_id,'financial_year_id' => $financial_year_id,
                   'budget_type'=>'APPROVED','segment_name'=>'OBJECTIVE','created_at'=>date('Y-m-d H:i:s'),'system_code'=> $system_code]);
                }
            }
            
        }else{
            $feedback = ["successMessage" => "No new Objectives to be exported"];
        }
     } else {
            //nothing to export
            $feedback = "No Objective to Export, Please select proper budget type or  contact administrator";
        }
        return response()->json($feedback, 200);
    }

    /* This function fetch and export number of employees(exiting on and not on payroll) from each psc */
    public function exportAllInstitutionEmployees(Request $request)
    {
        
        ini_set('max_execution_time', 3600);
        $financial_year_id = $request->financialYearId;
        $budget_type = $request->budgetType;
        $systemCode = $request->system_code;
        if ($systemCode == 0) {
            $system_code = 'OTRMIS';
        } elseif($systemCode == 1) {
            $system_code = 'MUSE';
        }elseif($systemCode == 2){
            $system_code = 'ERMS';
        }

        $query ="select
                    admin_hierarchy_id as id,admin_hierarchy as institutionname,trno as trnumber,
                    sum(Nmber_emp) as noofemployees
                from (select
                       a.admin_hierarchy_id, a.admin_hierarchy, a.trno,
                        max( case when (a.column_number = 101)  then a.field_value else 0 end) as Nmber_emp
                    from
                        (select
                            MAX(a.code) as trno,
                            a.name as admin_hierarchy,
                            a.id as admin_hierarchy_id,
                            s.id,
                            bf.column_number,
                            sum(cast(replace(bv.field_value,',','')::numeric as float))  as field_value
                        from
                            admin_hierarchies a
                                inner join admin_hierarchies a1 on a.parent_id = a1.id
                                inner join admin_hierarchies a2 on a1.parent_id = a2.id
                                inner join mtefs m on a.id = m.admin_hierarchy_id
                                inner join mtef_sections ms on m.id = ms.mtef_id
                                inner join activities ac on ac.mtef_section_id = ms.id
                                inner join budget_submission_lines bl on bl.activity_id =ac.id
                                inner join sections s on bl.section_id = s.id
                                inner join section_levels  sl on s.section_level_id = sl.id
                                inner join budget_submission_line_values bv on bv.budget_submission_line_id = bl.id
                                inner join budget_submission_sub_forms bs on bs.id = bl.budget_submission_sub_form_id
                                inner join budget_submission_forms bsf on bsf.id = bs.budget_submission_form_id
                                inner join budget_submission_definitions bf on bf.id = bv.budget_submission_definition_id
                                inner join fund_sources f on f.id = bl.fund_source_id
                                inner join financial_years fy on m.financial_year_id = fy.id
                        where
                            bl.budget_submission_sub_form_id in(7,8)  and
                            m.financial_year_id =  $financial_year_id   and
                            bv.field_value != '' and ac.budget_type =   '$budget_type' and sl.hierarchy_position = 4
                        group by
                            a.id,
                            a.name,
                            f.name,
                            s.name,
                            s.code,
                            s.id,
                            bf.id) a
                    group by a.admin_hierarchy, a.column_number, a.id,a.trno,a.admin_hierarchy_id
                    order by a.id) main
                group by id,admin_hierarchy,trnumber";

                $items = DB::select(DB::raw($query));

        $institutionEmployeeArray = array();
        foreach ($items as $value) {
            $object = array(
             "uid" => $value->id,
             "trNumber" => $value->trnumber,
             "numberOfEmployees" => $value->noofemployees
            );
            array_push($institutionEmployeeArray,$object);
        }

        $options = array(
            'headers' => array('x-match' => 'all', 'Company' => '00000', 'type' => 'Transaction'),
            'exchange' => 'BudgetExchange'
        );

        // $queue_name = ConfigurationSetting::getValueByKey('BUDGET_QUEUE_NAME');
        $queue_name = 'PLANREPOTR_TO_OTRMIS_DATA_QUEUE';

        $control_number = app('App\Http\Controllers\Execution\MuseIntegrationController')->transactionNumber("EMPLOYEE",12);

        $messageHeader = array(
            "sender" => "PLANREPOTR",
            "receiver" => $system_code,
            "msgId" => $control_number,
            "applyDate" => date('Y-m-d'),
            "messageType" => "EMPLOYEE",
            "createdAt" => date('Y-m-d H:i:s')
        );
        $messageSummary = array(
            "trxCtrlNum" => $control_number,
            "applyDate"=> date('Y-m-d'),
            "description"=>"NUMBER OF EMPLOYEES FROM EACH PSC"
        );
        $item = array(
            "messageHeader"  => $messageHeader,
            "messageSummary" => $messageSummary,
            "messageDetails" => $institutionEmployeeArray
        );

        $payloadToMuse = array(
            'message' => $item,
            'digitalSignature'=>app('App\Http\Controllers\Execution\MuseIntegrationController')->signature(json_encode($item))
        );
        if(count($institutionEmployeeArray)>0){
            //$response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($payloadToMuse);
            dispatch(new PublishDataToRabbitMQJob($queue_name,$payloadToMuse, $options,'employee'));

            // if($response != ""){
            //     //update a table with is_sent or is_delivered attributes
            // }
            $feedback = ["successMessage" => count($institutionEmployeeArray)." PSCs employee details was sent. You will be notified when its received"];
        }else{
            $feedback = ["successMessage" => "No new data to be exported"];
        }
        return response()->json($feedback, 200);
    }

    /**
     * activities to be sent to financial systems by budget_type
    */
    public function activitiesToFinancialSystems(Request $request)
    {
        $financial_year_id = $request->financialYearId;
        $adminHierarchyId = $request->adminHierarchyId;
        $facility_type = $request->facility_type;
        $filterOption= $request->filterOption;
        $budget_type= $request->budget_type;
        $activities = null;
        $sql = "select a.id,a.description activity,
       a.code activity_code,ah.code code,
       p.name project,p.code project_code,
       f.name facility,f.facility_code,
       ma.exported_to_muse,
       ma.delivered_to_muse,
       ma.muse_response,
       ma.apply_date,
       bc.name budget_class,bc.code budget_class_code
       from activities a
       left join muse_activities ma on a.id = ma.activity_id
       join mtef_sections ms on a.mtef_section_id = ms.id
       join mtefs m on ms.mtef_id = m.id
       join admin_hierarchies ah on m.admin_hierarchy_id = ah.id
       join facilities f on ah.id = f.admin_hierarchy_id
       join facility_types ft on f.facility_type_id = ft.id
       join projects p on a.project_id = p.id
       join budget_classes bc on a.budget_class_id = bc.id
                where
                  m.admin_hierarchy_id=$adminHierarchyId
                  and m.financial_year_id=$financial_year_id
                  and a.budget_type='$budget_type'
                and ft.name='$facility_type' and a.code !='000000' ";
        $data = DB::select(DB::raw($sql));
        return response()->json(["items" => $data]);
    }
    public function facilityLevelActivities(Request $request)
    {
        $financial_year_id = $request->financialYearId;
        $adminHierarchyId = $request->adminHierarchyId;
        $facility_type = $request->facility_type;
        $filterOption= $request->filterOption;
        $activities = null;
        switch ($filterOption){
            case 1:
                //all
                if(isset($request->searchQuery)){
                    $query = $request->searchQuery;
                    $activities = FfarsActivity::where('financial_year_id', $financial_year_id)
                        ->where("admin_hierarchy_id", $adminHierarchyId)
                        ->where("facility_type", $facility_type)
                        ->where("activity_code","ILIKE","%".$query."%")
                        ->orWhere("sub_budget_class_code","ILIKE","%".$query."%")
                        ->orWhere("sub_budget_class","ILIKE","%".$query."%")
                        ->orWhere("description","ILIKE","%".$query."%")
                        ->orWhere("facility_code","ILIKE","%".$query."%")
                        ->orWhere("project_code","ILIKE","%".$query."%")
                        ->orWhere("ffars_response","ILIKE","%".$query."%")
                        ->orderBy('facility_code', 'asc')
                        ->orderBy('exported_to_ffars','asc')
                        ->paginate($request->perPage);
                } else{
                    $activities = FfarsActivity::where('financial_year_id', $financial_year_id)
                        ->where("admin_hierarchy_id", $adminHierarchyId)
                        ->where("facility_type", $facility_type)
                        ->orderBy('facility_code', 'asc')
                        ->orderBy('exported_to_ffars','asc')
                        ->paginate($request->perPage);
                }

                break;

            case 2:
                //exported
                if(isset($request->searchQuery)){
                    $query = $request->searchQuery;
                    $activities = FfarsActivity::where('financial_year_id', $financial_year_id)
                        ->where("admin_hierarchy_id", $adminHierarchyId)
                        ->where("facility_type", $facility_type)
                        ->where("exported_to_ffars", true)
                        ->where("activity_code","ILIKE","%".$query."%")
                        ->orWhere("sub_budget_class_code","ILIKE","%".$query."%")
                        ->orWhere("sub_budget_class","ILIKE","%".$query."%")
                        ->orWhere("description","ILIKE","%".$query."%")
                        ->orWhere("facility_code","ILIKE","%".$query."%")
                        ->orWhere("project_code","ILIKE","%".$query."%")
                        ->orWhere("ffars_response","ILIKE","%".$query."%")
                        ->orderBy('facility_code', 'asc')
                        ->orderBy('exported_to_ffars','asc')
                        ->paginate($request->perPage);
                }else{
                    $activities = FfarsActivity::where('financial_year_id', $financial_year_id)
                        ->where("admin_hierarchy_id", $adminHierarchyId)
                        ->where("facility_type", $facility_type)
                        ->where("exported_to_ffars", true)
                        ->orderBy('facility_code', 'asc')
                        ->orderBy('exported_to_ffars','asc')
                        ->paginate($request->perPage);
                }

                break;

            case 3:
                //not exported
                if(isset($request->searchQuery)){
                    $query = $request->searchQuery;
                    $activities = FfarsActivity::where('financial_year_id', $financial_year_id)
                        ->where("admin_hierarchy_id", $adminHierarchyId)
                        ->where("facility_type", $facility_type)
                        ->where("exported_to_ffars", false)
                        ->where("activity_code","ILIKE","%".$query."%")
                        ->orWhere("sub_budget_class_code","ILIKE","%".$query."%")
                        ->orWhere("sub_budget_class","ILIKE","%".$query."%")
                        ->orWhere("description","ILIKE","%".$query."%")
                        ->orWhere("facility_code","ILIKE","%".$query."%")
                        ->orWhere("project_code","ILIKE","%".$query."%")
                        ->orWhere("ffars_response","ILIKE","%".$query."%")
                        ->orderBy('facility_code', 'asc')
                        ->orderBy('exported_to_ffars','asc')
                        ->paginate($request->perPage);
                }else{
                    $activities = FfarsActivity::where('financial_year_id', $financial_year_id)
                        ->where("admin_hierarchy_id", $adminHierarchyId)
                        ->where("facility_type", $facility_type)
                        ->where("exported_to_ffars", false)
                        ->orderBy('facility_code', 'asc')
                        ->orderBy('exported_to_ffars','asc')
                        ->paginate($request->perPage);
                }
                break;

            default:
                $activities = FfarsActivity::where('financial_year_id', $financial_year_id)
                    ->where("admin_hierarchy_id", $adminHierarchyId)
                    ->where("facility_type", $facility_type)
                    ->orderBy('facility_code', 'asc')
                    ->orderBy('exported_to_ffars','asc')
                    ->paginate($request->perPage);
        }

        $data = ["items" => $activities];
        return response()->json($data, Response::HTTP_OK);
    }

    public function exportAllSelectedActivities(Request $request)
    {
        ini_set('max_execution_time', 3600);
        $activities = $request->all();
        $activityArray = array();
       
    
        foreach($activities as $value){
            $object = array(
                "uid" => $value['id'],
                "code" => $value['code'],
               "description" => $value['description'],
               //"facilityCode" => $value['facility_code']
            );
            //$company = $value['code'];
            array_push($activityArray, $object);
        }
        //Get administrative ID and Current Financial Year
        //  $c = AdminHierarchy::where('code', $company)->first();
        //  $admin_hierarchy_id = isset($c->id) ? $c->id : null;
        
        //Check the current active period from the periods table. The period_group should not be annual
        $currentData = date('Y-m-d H:i:s');
        $financial_year_query = "select * from financial_years where '".$currentData."'::date between start_date and end_date";
        $get_financial_year = DB::select(DB::raw($financial_year_query));
        $financial_year_id  = isset($get_financial_year[0]->id) ? $get_financial_year[0]->id: null;
        
        $options = array(
            'headers' => array('x-match' => 'all', 'Company' => '00000', 'type' => 'Transaction'),
            'exchange' => 'BudgetExchange'
        );

        // $queue_name = ConfigurationSetting::getValueByKey('BUDGET_QUEUE_NAME');
        $queue_name = 'PLANREPOTR_TO_OTRMIS_DATA_QUEUE';


        $control_number = app('App\Http\Controllers\Execution\MuseIntegrationController')->transactionNumber("ACTIVITY",7);
        $messageHeader = array(
            "sender" => "PLANREPOTR",
            "receiver" => "OTRMIS",
            "msgId" => $control_number,
            "applyDate" => date('Y-m-d'),
            "messageType" => "ACTIVITY",
            "createdAt" => date('Y-m-d H:i:s')
        );
        $messageSummary = array(
            "company"=> '00000',
            "trxCtrlNum" => $control_number,
            "applyDate"=> date('Y-m-d'),
            "description"=>"SELECTED ACTIVITY CODE AND DESCRIPTION"
        );

        $item = array(
            "messageHeader"  => $messageHeader,
            "messageSummary" => $messageSummary,
            "messageDetails" => $activityArray
        );

        $payloadToMuse = array(
            'message' => $item,
            'digitalSignature'=>app('App\Http\Controllers\Execution\MuseIntegrationController')->signature(json_encode($item))
        );

        if(count($activityArray)>0){
            //$response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($payloadToMuse);
            dispatch(new PublishDataToRabbitMQJob($queue_name,$payloadToMuse, $options,'activity'));
            // if($response == "ACCEPTED"){
            //     foreach($activityArray as $detail){
            //         $uid = $detail['uid'];
            //         $count = DB::table("muse_activities")->where("activity_id",$uid)->where("admin_hierarchy_id",$admin_hierarchy_id)->where("financial_year_id",$financial_year_id)->count();
            //         if($count == 0){
            //             $obj = new MuseActivity();
            //             $obj->activity_id = $uid;
            //             $obj->apply_date = Carbon::now();
            //             $obj->admin_hierarchy_id = $admin_hierarchy_id;
            //             $obj->financial_year_id = $financial_year_id;
            //             $obj->budget_type = "";
            //             $obj->exported_to_muse = true;
            //             $obj->save();
            //         }
            //      }
            // }
        }


       
        // foreach ($activities as $value) {
        //     $item['id'] = $value['id'];
        //     $item['subBudgetClassCode'] = $value['sub_budget_class_code'];
        //     $item['projectCode'] = $value['project_code'];
        //     $item['activityCode'] = $value['activity_code'];
        //     $item['description'] = $value['description'];
        //     $item['applyDate'] = $value['apply_date'];
        //     $item['facilityCode'] = $value['facility_code'];
        //     $item['deletedAt'] = $value['deleted_at'];
        //     //get journal code
        //     $item['budgetType'] = BudgetExportAccount::getJournalCode($value['budget_type']);
        //     try {
        //         $this->send($item, "ACTIVITIES");
        //     } catch (\Exception $exception) {
        //         Log::error($exception);
        //     }
        // }
        // $notification_message = "Activity Segments has been sent to FFARS";
        // $user_id = UserServices::getUser()->id;
        // $users = User::whereIn('id', [$user_id])->get();
        // NotificationService::notify($users, $notification_message);
        $feedback = ["successMessage" => "Data is being processed and sent to MUSE. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public function exportAllActivities(Request $request)
    {
        ini_set('max_execution_time', 6000);
        $financial_year_id = $request->financialYearId;
        $adminHierarchyId = $request->adminHierarchyId;
        $budget_type = $request->budgetType;
        $systemCode = $request->system_code;

        if ($systemCode == 0) {
            $system_code = 'OTRMIS';
        } elseif($systemCode == 1) {
            $system_code = 'MUSE';
        }elseif($systemCode == 2) {
            $system_code = 'NPMIS';
        }elseif($systemCode == 3){
            $system_code = 'ERMS';
        }
        $message = array();
        $activities = array();
        $company = DB::table('admin_hierarchies')->where('id',$adminHierarchyId)->first()->code;
        $financialYear = DB::table('financial_years')->where('id',$financial_year_id)->first()->name;

        $queue_name = 'PLANREP_OTR_ACTIVITIES_TO_MUSE';
        $options = array(
            'headers' => array('x-match' => 'all', 'Company' => $company, 'type' => 'Transaction'),
            'exchange' => 'MUSEACTIVITIESExchange'
        );
        $data = "select distinct a.id uid,a.description activityDescription, f.facility_code facilityCode, fy.name fYear,
              a.code activityCode,ah.code adminCode,a.budget_type budgetType from activities a
            join mtef_sections ms on a.mtef_section_id = ms.id
            join mtefs m on ms.mtef_id = m.id
            join financial_years fy on m.financial_year_id = fy.id
            join activity_facilities af on a.id = af.activity_id
            join facilities f on af.facility_id = f.id
            join admin_hierarchies ah on ah.id = m.admin_hierarchy_id
            join facility_types ft on f.facility_type_id = ft.id
            left join muse_activities ma on a.id = ma.activity_id
              where
              m.admin_hierarchy_id=$adminHierarchyId
              and m.financial_year_id=$financial_year_id
              and a.budget_type='$budget_type'";
            //   and ft.name='$facility_type'";

        $res = DB::select(DB::raw($data));
        if (sizeof($res) > 0 ){
            //update muse_activities
            foreach ($res as $value) {
                
                $exist = DB::table('muse_activities')
                    ->where('activity_id',$value->uid)->get();
                if ($exist->count() > 0){
                //  Log::info('Exists');
            
                }else {
                    $obj = new MuseActivity();
                    $obj->activity_id = $value->uid;
                    $obj->apply_date = Carbon::now();
                    $obj->admin_hierarchy_id = $adminHierarchyId;
                    $obj->financial_year_id = $financial_year_id;
                    $obj->budget_type = $budget_type;
                    $obj->exported_to_muse = true;
                    $obj->save();

                    $parsedvalue = json_decode( json_encode($value), true);
                    
                    $activityArray = array(
                       "uid" =>  $parsedvalue["uid"],
                       "code" =>  $parsedvalue["activitycode"],
                       "description" => $parsedvalue["activitydescription"],
                       //"facilityCode" => $parsedvalue['facilitycode']
                    );
                    array_push($activities,$activityArray);
                }
            }

            //Generate transaction Id
            if(count($activities)>0){
                $control_number = app('App\Http\Controllers\Execution\MuseIntegrationController')->transactionNumber("ACTIVITY",7);
            }else{
                $control_number = ""; 
            }
            
            $messageHeader = array(
                "sender" => "PLANREPOTR",
                "receiver" => $system_code,
                "msgId" => $control_number,
                "applyDate" => date('Y-m-d'),
                "messageType" => "ACTIVITY",
                "createdAt" => date('Y-m-d H:i:s')
            );
            $messageSummary = array(
                "company"=> $company,
                "trxCtrlNum" => $control_number,
                "applyDate"=> date('Y-m-d'),
                "financialYear" => $financialYear, 
                "description"=>"ACTIVITY CODE AND DESCRIPTION"
            );

            $item = array(
                "messageHeader"  => $messageHeader,
                "messageSummary" => $messageSummary,
                "messageDetails" => $activities
            );
           
            $payloadToMuse = array(
                'message' => $item,
                'digitalSignature'=>app('App\Http\Controllers\Execution\MuseIntegrationController')->signature(json_encode($item))
            );

             array_push($message, $payloadToMuse);
             log::info(json_encode($payloadToMuse));
            if (count($activities)>0) {
                $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($payloadToMuse);
                //dispatch(new SendDataToRabbitMQJob($queue_name, $message, $options, 'muse_activities'));
                $response_message = count($activities)." Activities have been sent. Please wait for Notification";
            }else{
                $response_message = "No new activity to be sent";
            }
        } else {
            //nothing to export
            $response_message = "No Activity to Export, Please select proper budget type or  contact administrator";
        }
        $feedback = ["successMessage" => $response_message];
        return response()->json($feedback, 200);

    }
  

    public static function tinkerExportAllFacilities()
    {

        $facilities = Facility::with("facility_type", "admin_hierarchy", "bank_accounts")
            ->where('exported_to_ffars', false)
            ->where('is_delivered_to_ffars', false)
            ->where('is_active', true)
            ->get();
        foreach ($facilities as $facility) {
            $item['id'] = $facility->id;
            $item['facilityCode'] = $facility->facility_code;
            $item['name'] = $facility->name;
            switch ($facility->facility_type->code) {
                case '002':
                    $item['facilityType'] = 'P';
                    break;
                case '003':
                    $item['facilityType'] = 'S';
                    break;
                case '004':
                    $item['facilityType'] = 'DP';
                    break;
                case '005':
                    $item['facilityType'] = 'HC';
                    break;
                case '006':
                    $item['facilityType'] = 'HO';
                    break;
                default:
                    $item['facilityType'] = 'CH';
            }
            $item['wardCode'] = $facility->admin_hierarchy->ward_code;
            $item['phoneNumber'] = $facility->phone_number;
            $item['email'] = $facility->email;
            $accounts = array();
            foreach ($facility->bank_accounts as $bank_account) {
                $itm['accountNumber'] = $bank_account->account_number;
                $itm['accountName'] = $bank_account->account_name;
                $itm['bankName'] = $bank_account->bank;
                array_push($accounts, $itm);
            }
            $item['bankAccounts'] = $accounts;
            $item['postalAddress'] = $facility->postal_address;
            $item['deletedAt'] = $facility->deleted_at;
            try {

                $type = 'FACILITIES';
                $message = new AMQPMessage(json_encode($item));
                $queue_name = "SEGMENT_TO_FFARS_QUEUE";
                $exchange = "FFARSSegmentExchange";
                $exchange_type = "headers";
                $routing_key = "";
                $headers = new AMQPTable([
                    'x-match' => 'all',
                    'type' => $type
                ]);
                $queue_bindings = new AMQPTable([
                    'x-match' => 'all',
                    'type' => $type
                ]);
                sendToQueue($message, $queue_name, $exchange, $exchange_type, $routing_key, $headers, $queue_bindings);
                echo response()->json(['DATA-SENT' => $item]) . "\n";
            } catch (\Exception $exception) {
                Log::error($exception);
            }
        }
        $feedback = ["successMessage" => "Data is being processed and sent to FFARS. You will be notified when its done"];
        return response()->json($feedback, 200);
    }

    public function ffarsActivityAdminHierarchies()
    {
        $sql = "select distinct admin_hierarchy_id as id, admin_hierarchy as name from ffars_activities;";
        $result = DB::select($sql);
        return response()->json(['items' => $result], 200);
    }

}
