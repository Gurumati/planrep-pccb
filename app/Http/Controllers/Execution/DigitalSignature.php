<?php

namespace App\Http\Controllers\Execution;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DigitalSignature extends Controller
{
    //
    public function signingData($data){
        $data = json_encode($data);
        $plainText = $data;
        $privateKey = openssl_pkey_get_private("file://" .base_path() . "/config/keys/private.pem");
    
        // Make a signature
        openssl_sign($plainText, $signature, $privateKey, OPENSSL_ALGO_SHA256);
        return $signature = base64_encode($signature);
      }

    //   public function verifyingSignature($data){
    //     $data = json_encode($data);
    //     $plainText = $data;
    //     $publicKey  = openssl_pkey_get_public("file://" .base_path() . "/config/keys/public.pem");
    
    //     $ok = openssl_verify($data, $signature, $publicKey);

    //     return $signature = base64_encode($signature);
    //   }
}
