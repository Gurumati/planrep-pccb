<?php

namespace App\Http\Controllers\Execution;

use App\Http\Services\CustomPager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class RevenueAllocationExpenditureController extends Controller
{
    public function index(Request $request)
    {
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $financial_year_id  = $request->financial_year_id;
        $transaction_type   = $request->transaction_type;
        $system_code = $request->system_code;
        $perPage = isset($request->perPage) ? $request->perPage : 25;
        /** get data */
        if ($transaction_type == 'REVENUE') {
            if (isset($request->facility_id)) {
                $facility_id = $request->facility_id;
                $sql = "select r.\"JEDate\" as date,r.account as chart_of_account,r.debit_amount,r.credit_amount,f.name as facility from received_fund_items r
                    join revenue_export_accounts rea on r.revenue_export_account_id = rea.id
                    join admin_hierarchy_ceilings ahc on ahc.id = r.admin_hierarchy_ceiling_id
                    join facilities f on ahc.facility_id = f.id
                    where rea.financial_year_id = '$financial_year_id'
                      and rea.admin_hierarchy_id = '$admin_hierarchy_id'
                      and f.id = '$facility_id'
                      and rea.financial_system_code = '$system_code'
                      and r.deleted_at is null
                      order by r.id desc";
                $items = DB::select($sql);
                $data = CustomPager::paginate($items,$perPage);
            } else {
                $sql = "select r.\"JEDate\" as date,r.account as chart_of_account,r.debit_amount,r.credit_amount,'' as facility from received_fund_items r
                    join revenue_export_accounts rea on r.revenue_export_account_id = rea.id
                    where rea.financial_year_id = '$financial_year_id'
                      and rea.admin_hierarchy_id = '$admin_hierarchy_id'
                      and rea.financial_system_code = '$system_code'
                      and r.deleted_at is null
                      order by r.id desc";
                $items = DB::select($sql);
                $data = CustomPager::paginate($items,$perPage);
            }
        } else if($transaction_type == 'EXPENDITURE') {
            if (isset($request->facility_id)) {
                $facility_id = $request->facility_id;
                $sql = "select i.\"JEDate\" as date, i.\"Account\" as chart_of_account,i.\"BookID\" as book,i.debit_amount,i.credit_amount,i.\"BookID\" as book,f.name as facility from budget_import_items i
                    join budget_export_accounts bea on i.budget_export_account_id = bea.id
                    join activity_facility_fund_source_inputs affsi on bea.activity_facility_fund_source_input_id = affsi.id
                    join activity_facility_fund_sources affs on affsi.activity_facility_fund_source_id = affs.id
                    join activity_facilities af on affs.activity_facility_id = af.id
                    join facilities f on af.facility_id = f.id
                    where bea.admin_hierarchy_id = '$admin_hierarchy_id'
                      and af.facility_id = '$facility_id'
                      and bea.financial_year_id = $financial_year_id
                      and i.deleted_at is null
                      and bea.financial_system_code = '$system_code'
                       order by i.id desc";
                $items = DB::select($sql);
                $data = CustomPager::paginate($items,$perPage);
            } else {
                $sql = "select i.\"JEDate\" as date, i.\"Account\" as chart_of_account,i.\"BookID\" as book,i.debit_amount,i.credit_amount,i.\"BookID\" as book,f.name as facility from budget_import_items i
                join budget_export_accounts bea on i.budget_export_account_id = bea.id
                join activity_facility_fund_source_inputs affsi on bea.activity_facility_fund_source_input_id = affsi.id
                join activity_facility_fund_sources affs on affsi.activity_facility_fund_source_id = affs.id
                join activity_facilities af on affs.activity_facility_id = af.id
                join facilities f on af.facility_id = f.id
                where bea.admin_hierarchy_id = '$admin_hierarchy_id'
                  and bea.financial_year_id = $financial_year_id
                  and i.deleted_at is null
                  and bea.financial_system_code = '$system_code'
                  order by i.id desc";
                $items = DB::select($sql);
                $data = CustomPager::paginate($items,$perPage);
            }
        } else {
            if (isset($request->facility_id)) {
                $facility_id = $request->facility_id;
                $sql = "select faffs.\"JEDate\" as date,faffs.gl_account as chart_of_account, fu.debit_amount,fu.credit_amount,faffs.\"BookID\" as book,f.name as facility from fund_allocated_transaction_items fu
                join budget_export_accounts bea on fu.budget_export_account_id = bea.id
                join fund_allocated_from_financial_systems faffs on fu.fund_allocated_from_financial_system_id = faffs.id
                join activity_facility_fund_source_inputs affsi on bea.activity_facility_fund_source_input_id = affsi.id
                join activity_facility_fund_sources affs on affsi.activity_facility_fund_source_id = affs.id
                join activity_facilities af on affs.activity_facility_id = af.id
                join facilities f on af.facility_id = f.id
                where bea.admin_hierarchy_id = $admin_hierarchy_id
                and bea.financial_year_id = $financial_year_id
                and bea.financial_system_code = '$system_code'
                and f.id = $facility_id
                order by fu.id desc;";
                $items = DB::select($sql);
                $data = CustomPager::paginate($items,$perPage);
            } else {
                $sql = "select faffs.\"JEDate\" as date,faffs.gl_account as chart_of_account, fu.debit_amount,fu.credit_amount,faffs.\"BookID\" as book,f.name as facility from fund_allocated_transaction_items fu
                join budget_export_accounts bea on fu.budget_export_account_id = bea.id
                join fund_allocated_from_financial_systems faffs on fu.fund_allocated_from_financial_system_id = faffs.id
                join activity_facility_fund_source_inputs affsi on bea.activity_facility_fund_source_input_id = affsi.id
                join activity_facility_fund_sources affs on affsi.activity_facility_fund_source_id = affs.id
                join activity_facilities af on affs.activity_facility_id = af.id
                join facilities f on af.facility_id = f.id
                where bea.admin_hierarchy_id = $admin_hierarchy_id
                and bea.financial_year_id = $financial_year_id
                and bea.financial_system_code = '$system_code'
                order by fu.id desc;";
                $items = DB::select($sql);
                $data = CustomPager::paginate($items,$perPage);
            }
        }
        return response()->json($data);
    }
}
