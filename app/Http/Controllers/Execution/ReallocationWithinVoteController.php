<?php

namespace App\Http\Controllers\Execution;

use App\Models\Setup\AdminHierarchy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\Execution\BudgetExportTransactionItemService;
use App\Http\Services\Execution\BudgetExportToFinancialSystemService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Setup\ConfigurationSetting;
use App\Http\Services\FinancialYearServices;
use App\Jobs\sendNotifications;
use App\Models\Execution\BudgetReallocation;
use Exception;
use Illuminate\Database\QueryException;
use App\Models\Execution\BudgetReallocationItem;
use App\Models\Execution\BudgetExportAccount;
use App\Http\Services\UserServices;
use App\Models\Budgeting\AdminHierarchyCeiling;
use App\Models\Budgeting\Ceiling;
use App\Models\Execution\BudgetExportTransaction;
use Illuminate\Pagination\Paginator;
use App\Models\Execution\BudgetReallocationResponse;
use Carbon\Carbon;
use stdClass;

class ReallocationWithinVoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * get all votes available.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getVotes()
    {
        $data =  AdminHierarchy::where('parent_id', 1)->get();
        return response()->json(['votes' => $data], 200);
    }
    /**
     * return all admin hierarchies based on selected Vote.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPiscs(Request $request)
    {
        $sql = "select ah.* from admin_hierarchies ah
                join admin_hierarchies ah1 on ah1.id = ah.parent_id
                where ah1.parent_id =$request->voteId ";
        $data =  DB::select(DB::raw($sql));
        return response()->json(['adminHierarchies' => $data], 200);
    }

    /**
     * return all sub-busget class based on selected fundsource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSubBudgetClasses(Request $request)
    {
        $subBudgetClasses = DB::table('budget_classes as bc')
            ->join('fund_source_budget_classes as fsbc', 'bc.id', 'fsbc.budget_class_id')
            ->where('fsbc.fund_source_id', $request->fundSourceId)
            ->select('bc.*')
            ->get();
        return response()->json(['subBudgetClasses' => $subBudgetClasses], 200);
    }



    /**
     * return all reallocations.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteItem(Request $request)
    {
        try {
            DB::beginTransaction();
            BudgetReallocationResponse::where('budget_reallocation_item_id', $request->id)->forceDelete();
            BudgetReallocationItem::where('id', $request->id)->forceDelete();
            DB::commit();
            $items = $this->getAllReallocationsItems($request->reallocationId);
            return response()->json(['successMessage' => 'Item Deleted', 'reallocationItem' => $items]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['errorMessage' => 'Error Deleting Item', 'exception' => $e], 500);
        }
    }

    /**
     * return all admin hierarchies based on selected Vote.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFundSources(Request $request)
    {
        $sql = "select distinct fs.* from mtefs m
                join mtef_sections ms on m.id = ms.mtef_id
                join activities a on ms.id = a.mtef_section_id
                join activity_facilities af on a.id = af.activity_id
                join activity_facility_fund_sources affs on af.id = affs.activity_facility_id
                join fund_sources fs on affs.fund_source_id = fs.id
                where m.admin_hierarchy_id = $request->adminId
                and m.financial_year_id = $request->financialYearId";
        $data =  DB::select(DB::raw($sql));
        return response()->json(['fundSources' => $data], 200);
    }

    public function getActivities(Request $request)
    {
        $adminHierarchyId = $request->adminId;
        $financialYearId = $request->financialYearId;
        $budget_type = $request->budgetType;
        $fundSourceId = $request->fundSourceId;
        $subBudgetClass = $request->subBudgetClass;
        $perPage = isset($request->perPage) ? $request->perPage : 25;

        $activities =  DB::select(DB::raw("select * from(
            select Distinct  bea.*, ah.name as
            admin_hierarchy,gc.code as gfs_code,gc.description as gfs_code_description,
            a.description as activity,s.name as section,
            coalesce(x.expenditure,0) expenditure,
            (bea.amount-coalesce(x.expenditure,0)) to_allocate,
            coalesce(allocated,0) allocated
            from budget_export_accounts bea
            join admin_hierarchies ah on bea.admin_hierarchy_id = ah.id
            join gfs_codes gc on bea.gfs_code_id = gc.id
            join activities a on bea.activity_id = a.id
            join sections s on bea.section_id = s.id
            join activity_facilities af on a.id = af.activity_id
            join activity_facility_fund_sources affs on af.id = affs.activity_facility_id
            join activity_facility_fund_source_inputs affsi on affs.id = affsi.activity_facility_fund_source_id
            left join (select budget_export_account_id,gfs_code_id,budget_class_id,fund_source_id,SUM(coalesce(credit_amount,0)-coalesce(debit_amount,0) )  expenditure from budget_import_items
            group by  id) x on bea.id = x.budget_export_account_id and x.fund_source_id = affs.fund_source_id and x.gfs_code_id = gc.id and x.budget_class_id = a.budget_class_id
            left join (select from_chart_of_accounts,sum(coalesce(amount,0)) as allocated from  budget_reallocation_items group by  from_chart_of_accounts ) y on  y.from_chart_of_accounts=bea.chart_of_accounts
            where admin_hierarchy_id = $adminHierarchyId
          and financial_year_id = $financialYearId
          and affs.fund_source_id = $fundSourceId
          and a.budget_class_id = $subBudgetClass) allocations
        where coalesce(allocated,0)  < to_allocate"));

        $object = (object) ['data' => $activities];
        return response()->json(['activities' => $object], 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getOpenedReallocation(Request $request)
    {
        $sql = "select * from budget_reallocations br
        where financial_year_id = $request->finencialYear and deleted_at is null
        and admin_hierarchy_id = $request->piscFrom
        and (SPLIT_PART(br.reallocation_desc, '-', 2))::INTEGER = $request->piscTo";
        $reallocations =  DB::select(DB::raw($sql));
        return response()->json(['reallocations' => $reallocations], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getReallocationItems(Request $request)
    {
        $items = $this->getAllReallocationsItems($request->reallocationId);
        return response()->json(['reallocationItem' => $items], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllRealocation(Request $request)
    {

        $finencialYear = $request->has("financialYear") == null ? "" : "and br.financial_year_id=$request->financialYear";
        $sql = "select br.id,reference_code,document_url,document_url2,document_url3,br.created_at,ah1.id as piscFromId,ah1.code as piscFromCode,ah1.name as piscFromName,ah2.id as piscToId,ah2.code as piscToCode,ah2.name as piscToName,SUM(coalesce(bri.amount,0)) total_amount
        from  budget_reallocations br
        join admin_hierarchies ah1 on br.admin_hierarchy_id = ah1.id
        join  admin_hierarchies ah2 on ah2.id =  (SPLIT_PART(br.reallocation_desc, '-', 2))::INTEGER
        left join budget_reallocation_items bri on br.id = bri.budget_reallocation_id
        where br.reallocation_desc is not null and br.deleted_at is null " . $finencialYear .
            "group by br.id, reference_code, document_url, document_url2, document_url3, br.created_at, ah1.id, ah1.code, ah1.name, ah2.id, ah2.code, ah2.name";
        $reallocations =  DB::select(DB::raw($sql));
        return response()->json(['reallocations' => $reallocations], 200);
    }

    public function getAllReallocationsItems(string $reallocationId)
    {
        $reallocationItem = DB::select(DB::raw("select bri.id,bri.budget_reallocation_id,bri.fund_source_id,bri.sub_budget_class_id,bri.from_chart_of_accounts,bri.amount,bri.is_approved,
        bri.is_rejected,gc.code as gfs_code,gc.name as gfs_code_name,bc.id as sbc_id,bc.name as sbc,bri.created_by,
        fs.name as fundSource,bri.budget_export_account_id from budget_reallocation_items bri
        join budget_export_accounts bea on bri.budget_export_account_id = bea.id
        join gfs_codes gc on bea.gfs_code_id = gc.id
        join activities a on bea.activity_id = a.id
        join budget_classes bc on a.budget_class_id = bc.id
        join activity_facilities af on a.id = af.activity_id
        join activity_facility_fund_sources affs on af.id = affs.activity_facility_id
        join fund_sources fs on affs.fund_source_id = fs.id
        where budget_reallocation_id = $reallocationId"));
        return $reallocationItem;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createPiscReallocation(Request $request)
    {
        $attachement = $request->attachement;
        $toDay   = date("Y-m-d");

        $defaultDecisionLevel = ConfigurationSetting::where('key', 'PLANREP_REALLOCATION_DEFAULT_DECISION_LEVEL')->first();
        $reallocationDataSource = ConfigurationSetting::where('key', 'PLANREP_REALLOCATION_DATA_SOURCE')->first();
        $reallocationImport = ConfigurationSetting::where('key', 'PLANREP_REALLOCATION_IMPORT_METHOD')->first();

        if (!isset($defaultDecisionLevel) || is_null($defaultDecisionLevel->value)) {
            return response()->json(['errorMessage' => 'No default Decision level found for reallocation'], 400);
        }
        if (!isset($reallocationDataSource) || is_null($reallocationDataSource->value)) {
            return response()->json(['errorMessage' => 'No default Data source found for reallocation'], 400);
        }
        if (!isset($reallocationImport) || is_null($reallocationImport->value)) {
            return response()->json(['errorMessage' => 'No default Import method found for reallocation'], 400);
        }

        if (!FinancialYearServices::getExecutionFinancialYear()) {
            return response()->json(['errorMessage' => 'No Execution Finencial Year'], 400);
        }

        $count = DB::table('budget_reallocations')
            ->where('reallocation_desc', $request->inputData['piscFrom'] . '-' . $request->inputData['piscTo'])
            ->where('is_open', true)
            ->count();
        if ($count > 0) {
            return response()->json(['errorMessage' => 'Reallocation Opened'], 400);
        }

        $data = [
            'reference_code' => $request->inputData['ref_no'],
            'comments' => $request->inputData['comments'],
            'admin_hierarchy_id' => $request->inputData['piscFrom'],
            'financial_year_id' => FinancialYearServices::getExecutionFinancialYear()->id,
            'data_source_id' => $reallocationDataSource->value,
            'import_method_id' => $reallocationImport->value,
            'decision_level_id' => $defaultDecisionLevel->value,
            'is_open' => true,
            'reallocation_desc' => $request->inputData['piscFrom'] . '-' . $request->inputData['piscTo']
        ];

        $reallocation = BudgetReallocation::create($data);
        $document_Count = 1;
        $document_Collumn = 'document_url';
        foreach ($attachement as &$file) {
            $name    = $toDay . "_" . $file['file'] . "_.pdf";
            $path    = public_path('uploads/reference_documents/' . $name);
            $pdf_decoded = base64_decode($file['pdf']);
            $pdf = fopen($path, 'w');
            fwrite($pdf, $pdf_decoded);
            fclose($pdf);
            if ($document_Count > 1) {
                $document_Collumn = 'document_url' . $document_Count;
            }
            BudgetReallocation::where('id', $reallocation->id)
                ->update([
                    $document_Collumn => $path
                ]);
            $document_Count++;
        }
        return response()->json(['reallocations' => $reallocation], 200);
    }

    function urlsafe_b64decode($string)
    {
        $data = str_replace(array('-', '_'), array('+', '/'), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function storePiscReallocations(Request $request)
    {
        try {
            $budgetToAllocate = $request->budget;
            DB::transaction(function () use ($budgetToAllocate) {

                $accounts = $budgetToAllocate['budget'];
                $totalDeduction = 0;
                foreach ($accounts as $account) {
                    $item = new BudgetReallocationItem();
                    $item->from_chart_of_accounts = $account['chart_of_accounts'];
                    $item->to_chart_of_accounts = 'Ceiling';
                    $item->amount = $account['amount'];
                    $item->budget_export_account_id = $account['id'];
                    $item->budget_export_to_account_id = $account['id'];
                    $item->fund_source_id = $budgetToAllocate['fundSource'];
                    $item->sub_budget_class_id = $budgetToAllocate['budgetClass'];
                    $item->budget_reallocation_id = $budgetToAllocate['budgetReallocationId'];
                    $item->created_by = UserServices::getUser()->id;
                    $item->comments = (isset($account['comments'])) ? $account['comments'] : null;
                    $item->is_approved = false;
                    $item->save();
                    $fromBudgetAccount = BudgetExportAccount::find($account['id']);
                    $fromBudgetAccount->is_locked = true;
                    $fromBudgetAccount->updated_by = UserServices::getUser()->id;
                    $fromBudgetAccount->save();
                    $totalDeduction = $totalDeduction + $account['amount'];
                }
            });
            $data = ['successMessage' => 'REALLOCATION_CREATED_SUCCESSFULLY'];
            return response()->json($data);
        } catch (QueryException $e) {
            $error = ['errorMessage' => 'ERROR_ON_CREATING'];
            return response()->json($error, 500);
        }
    }



    /**
     * Return Items.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request)
    {
        $Items = array();
        if (is_array($request->data)) {
            $items = $request->data;
        } else {
            $reallocationItems = DB::table('budget_reallocation_items')
                ->whereNull('deleted_at')
                ->where('is_approved', false)
                ->where('is_rejected', false)
                ->get();

            if (sizeof($reallocationItems) <= 1) {
                return response()->json(['errorMessage' => $request->action == 1 ? 'NOTHING_TO_APPROVE' : 'NOTHING_TO_REJECT'], 400);
            }

            $items = json_decode($reallocationItems, true);
        }

        $totalAllocationAmount = new stdClass();

        if ($request->action == 1) {
            if ($request->status) {
                $transactionType = ConfigurationSetting::where('key', 'REALLOCATION_TRANSACTION_TYPE')->first();
                $budgetGroupName = ConfigurationSetting::where('key', 'REALLOCATION_BUDGET_GROUP_NAME')->first();

                $transaction = new BudgetExportTransaction ();
                $transaction->budget_transaction_type_id = $transactionType->value;
                $transaction->description = "APPROVED REALLOCATIONS";
                $transaction->save();

                //enter transaction control number
                $control_number = 'REALLOC-' . $transaction->id;
                $transaction->control_number = $control_number;
                $transaction->save();
                //end of control number update

                foreach ($items as &$item) {

                    $fromTransactionItem = BudgetExportTransactionItemService::create(
                        $transaction->id,
                        $item['budget_export_account_id'],
                        (-1 * $item['amount']),
                        $item['id'],
                        false,
                        $item['created_by']
                    );

                    $fromAccount = BudgetExportAccount::find($item['budget_export_account_id']);

                    $fromAccount->amount = $fromAccount->amount - $item['amount'];
                    $fromAccount->updated_by = UserServices::getUser()->id;
                    $fromAccount->save();


                    $reallocationItem = BudgetReallocationItem::find($item['id']);
                    $reallocationItem->is_approved = true;
                    $reallocationItem->comments = isset($request->comments) ? $request->comments : 'Approved';
                    $reallocationItem->approved_by = UserServices::getUser()->id;
                    $reallocationItem->save();

                    $str = strval($item['sub_budget_class_id']);
                    if (!property_exists($totalAllocationAmount, $str)) {
                        $totalAllocationAmount->$str = $item['amount'];
                    } else {
                        $totalAllocationAmount->$str = $totalAllocationAmount->$str + $item['amount'];
                    }
                }
            } else {
            }

            foreach ($totalAllocationAmount as $key => $ceilingAmount) {
                $ceillingId = Ceiling::where('budget_class_id', $key)->value('id');
                $sectionid = DB::table('sections')->whereNull('parent_id')->value('id');
                $reallocation = DB::table('budget_reallocations')->where('id', $request->reallocationId)->select('reallocation_desc', 'financial_year_id')->get();
                $adminId = (int) explode('-', $reallocation[0]->reallocation_desc)[1];
                $finencial_year =  $reallocation[0]->financial_year_id;

                $adminHierarchyCeilings = $item = new  AdminHierarchyCeiling();
                $adminHierarchyCeilings->ceiling_id = $ceillingId;
                $adminHierarchyCeilings->section_id = $sectionid;
                $adminHierarchyCeilings->admin_hierarchy_id = $adminId;
                $adminHierarchyCeilings->financial_year_id = $finencial_year;
                $adminHierarchyCeilings->amount = $ceilingAmount;
                $adminHierarchyCeilings->created_at = Carbon::now();
                $adminHierarchyCeilings->created_by = UserServices::getUser()->id;
                $adminHierarchyCeilings->save();
            }
        } else {
            if ($request->status) {
                try {

                    foreach ($items as &$item) {
                        //insert response
                        $obj = new BudgetReallocationResponse();
                        $obj->updateOrCreate(
                            ['budget_reallocation_item_id' => $item['id'], 'created_by' => UserServices::getUser()->id],
                            ['response' => isset($request->comments) ? $request->comments : 'Rejected', 'is_approved' => false]
                        );

                        //get user
                        BudgetReallocationItem::find($item['id'])->update(['is_rejected' => true, 'comments' => isset($request->comments) ? $request->comments : 'Rejected']);
                        $user_id = DB::table('budget_reallocation_items')
                            ->where('id', $item['id'])
                            ->select('created_by')
                            ->first()->created_by;

                        $users['id'] = array(UserServices::getUser()->id, $user_id);
                        $message = "Budget reallocation has been rejected. Message: " . isset($request->comments) ? $request->comments : 'Rejected';
                        $url = url('/notifications#!/');
                        $object_id = 0;
                        $job = new sendNotifications($users, $object_id, $url, $message, "fa fa-book");
                        $this::dispatch($job->delay(10));
                        return ['successMessage' => 'Reallocations Disapproved'];
                    }
                } catch (Exception $e) {
                    return response()->json(['errorMessage' => 'Error Disapprove', 'error' => $e]);
                }
            }
        }

        return response()->json(['reallocationItems' => $request], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    private function tryUnlockAccounts($reallocation)
    {
        $otherFromExists = DB::table('budget_export_accounts as acc')->join('budget_reallocation_items as al', 'acc.id', 'al.budget_export_account_id')->where('acc.id', $reallocation['budget_export_account_id'])->where('al.id', '<>', $reallocation['id'])->first();
        if ($otherFromExists == null) {
            DB::table('budget_export_accounts')->where('id', $reallocation['budget_export_account_id'])->update(array('is_locked' => false));
        }
    }
}
