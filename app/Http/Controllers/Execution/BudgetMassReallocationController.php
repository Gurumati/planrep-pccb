<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Models\Execution\BudgetMassReallocation;
use Illuminate\Support\Facades\DB;
use App\DTOs\SetupDTOs\SectionDTO;
use Illuminate\Support\Facades\Log;
use App\Models\Setup\Sector;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\FundSource;


class BudgetMassReallocationController extends Controller
{

    public function reallocate($budgetType, $financialYearId, $adminHierarchyId, $sectorId,$exportTransactionsToEpicor, $exportTransactionsToFfars)
    {
        set_time_limit(3000);
        if ($sectorId == 1) {
            $fromBudgetClassId = ConfigurationSetting::getValueByKey('HS_FROM_BUDGET_CLASS');
            $fromBudgetClass = BudgetClass::where('id', $fromBudgetClassId)->first();

            $toBudgetClassId = ConfigurationSetting::getValueByKey('HS_TO_BUDGET_CLASS');
            $toBudgetClass = BudgetClass::where('id', $toBudgetClassId)->first();

            $fundSourceIds = ConfigurationSetting::getValueByKey('HS_FUND_SOURCES');

        } else if ($sectorId == 6) {
            $fromBudgetClassId = ConfigurationSetting::getValueByKey('WS_FROM_BUDGET_CLASS');
            $fromBudgetClass = BudgetClass::where('id', $fromBudgetClassId)->first();

            $toBudgetClassId = ConfigurationSetting::getValueByKey('WS_TO_BUDGET_CLASS');
            $toBudgetClass = BudgetClass::where('id', $toBudgetClassId)->first();

            $fundSourceIds = ConfigurationSetting::getValueByKey('WS_FUND_SOURCES');
        } else if ($sectorId == 4) {
            $fromBudgetClassId = ConfigurationSetting::getValueByKey('ES_FROM_BUDGET_CLASS');
            $fromBudgetClass = BudgetClass::where('id', $fromBudgetClassId)->first();

            $toBudgetClassId = ConfigurationSetting::getValueByKey('ES_TO_BUDGET_CLASS');
            $toBudgetClass = BudgetClass::where('id', $toBudgetClassId)->first();

            $fundSourceIds = ConfigurationSetting::getValueByKey('ES_FUND_SOURCES');
        } else if ($sectorId == 2) {
            $fromBudgetClassId = ConfigurationSetting::getValueByKey('AS_FROM_BUDGET_CLASS');
            $fromBudgetClass = BudgetClass::where('id', $fromBudgetClassId)->first();

            $toBudgetClassId = ConfigurationSetting::getValueByKey('AS_TO_BUDGET_CLASS');
            $toBudgetClass = BudgetClass::where('id', $toBudgetClassId)->first();

            $fundSourceIds = ConfigurationSetting::getValueByKey('AS_FUND_SOURCES');
        } else {
            return response()->json(['errorMessage' => 'invalid sector'], 400);
        }
        if ($fromBudgetClass == null) {
            return response()->json(['errorMessage' => 'invalid from budget class'], 400);
        }
        if ($toBudgetClass == null) {
            return response()->json(['errorMessage' => 'invalid to budget class'], 400);
        }
        if ($fundSourceIds == null || sizeof($fundSourceIds) == 0) {
            return response()->json(['errorMessage' => 'No valid fund source selected'], 400);
        }

        try {
            DB::beginTransaction();
            $budgetReallocated = BudgetMassReallocation::reallocateBudget($budgetType, $financialYearId, $adminHierarchyId, $sectorId, $fromBudgetClass, $toBudgetClass, $fundSourceIds, $exportTransactionsToEpicor, $exportTransactionsToFfars);
            $ceilingReallocated = BudgetMassReallocation::reallocateCeiling($budgetType, $financialYearId, $adminHierarchyId, $sectorId, $fromBudgetClass, $toBudgetClass, $fundSourceIds, $exportTransactionsToEpicor, $exportTransactionsToFfars);
            DB::commit();

            $fromBudget = BudgetMassReallocation::getBudget($budgetType, $financialYearId, $adminHierarchyId, $sectorId, $fromBudgetClass->id, $fundSourceIds);
            $toBudget = BudgetMassReallocation::getBudget($budgetType, $financialYearId, $adminHierarchyId, $sectorId, $toBudgetClass->id, $fundSourceIds);
            $fromCeiling = BudgetMassReallocation::getCeiling($budgetType, $financialYearId, $adminHierarchyId, $sectorId, $fromBudgetClass->id, $fundSourceIds);
            $toCeiling = BudgetMassReallocation::getCeiling($budgetType, $financialYearId, $adminHierarchyId, $sectorId, $toBudgetClass->id, $fundSourceIds);

            return response()->json(
                [
                    'successMessage' => 'SUCCESSFULLY_REALLOCATE',
                    'budgetReallocated' => $budgetReallocated,
                    'ceilingReallocated' => $ceilingReallocated,
                    'fromBudget' => $fromBudget,
                    'toBudget' => $toBudget,
                    'fromCeiling' => $fromCeiling,
                    'toCeiling' => $toCeiling
                ],
                200
            );
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollback();
            return response()->json(['errorMessage' => $e], 500);
        }
    }

    public function getReallocations($budgetType, $financialYearId, $adminHierarchyId, $sectorId, $level)
    {

        if ($sectorId == 1) {
            $fromBudgetClassId = ConfigurationSetting::getValueByKey('HS_FROM_BUDGET_CLASS');
            $fromBudgetClass = BudgetClass::where('id', $fromBudgetClassId)->first();

            $toBudgetClassId = ConfigurationSetting::getValueByKey('HS_TO_BUDGET_CLASS');
            $toBudgetClass = BudgetClass::where('id', $toBudgetClassId)->first();

            $fundSourceIds = ConfigurationSetting::getValueByKey('HS_FUND_SOURCES');

        } else if ($sectorId == 6) {
            $fromBudgetClassId = ConfigurationSetting::getValueByKey('WS_FROM_BUDGET_CLASS');
            $fromBudgetClass = BudgetClass::where('id', $fromBudgetClassId)->first();

            $toBudgetClassId = ConfigurationSetting::getValueByKey('WS_TO_BUDGET_CLASS');
            $toBudgetClass = BudgetClass::where('id', $toBudgetClassId)->first();

            $fundSourceIds = ConfigurationSetting::getValueByKey('WS_FUND_SOURCES');
        } else if ($sectorId == 4) {
            $fromBudgetClassId = ConfigurationSetting::getValueByKey('ES_FROM_BUDGET_CLASS');
            $fromBudgetClass = BudgetClass::where('id', $fromBudgetClassId)->first();

            $toBudgetClassId = ConfigurationSetting::getValueByKey('ES_TO_BUDGET_CLASS');
            $toBudgetClass = BudgetClass::where('id', $toBudgetClassId)->first();

            $fundSourceIds = ConfigurationSetting::getValueByKey('ES_FUND_SOURCES');
        } else if ($sectorId == 2) {
            $fromBudgetClassId = ConfigurationSetting::getValueByKey('AS_FROM_BUDGET_CLASS');
            $fromBudgetClass = BudgetClass::where('id', $fromBudgetClassId)->first();

            $toBudgetClassId = ConfigurationSetting::getValueByKey('AS_TO_BUDGET_CLASS');
            $toBudgetClass = BudgetClass::where('id', $toBudgetClassId)->first();

            $fundSourceIds = ConfigurationSetting::getValueByKey('AS_FUND_SOURCES');
        }else {
            return response()->json(['errorMessage' => 'invalid sector'], 400);
        }
        if ($fromBudgetClass == null) {
            return response()->json(['errorMessage' => 'invalid from budget class'], 400);
        }
        if ($toBudgetClass == null) {
            return response()->json(['errorMessage' => 'invalid to budget class'], 400);
        }
        if ($fundSourceIds == null || sizeof($fundSourceIds) == 0) {
            return response()->json(['errorMessage' => 'invalid to fund sources'], 400);
        }
        $adminAreas = [];
        $data = array();
        if ($level == 2) {
            $adminAreas = AdminHierarchy::where('parent_id', $adminHierarchyId)->orderBy('name')->get();
        }
        if ($level == 3) {
            $adminAreas = AdminHierarchy::where('id', $adminHierarchyId)->get();
        }

        foreach ($adminAreas as $adm) {
            $a = new \StdClass();
            $fromBudget = BudgetMassReallocation::getBudget($budgetType, $financialYearId, $adm->id, $sectorId, $fromBudgetClass->id, $fundSourceIds);
            $toBudget = BudgetMassReallocation::getBudget($budgetType, $financialYearId, $adm->id, $sectorId, $toBudgetClass->id, $fundSourceIds);

            $fromCeiling = BudgetMassReallocation::getCeiling($budgetType, $financialYearId, $adm->id, $sectorId, $fromBudgetClass->id, $fundSourceIds);
            $toCeiling = BudgetMassReallocation::getCeiling($budgetType, $financialYearId, $adm->id, $sectorId, $toBudgetClass->id, $fundSourceIds);

            $a->id = $adm->id;
            $a->name = $adm->name;
            $a->fromBudget = $fromBudget;
            $a->toBudget = $toBudget;
            $a->fromCeiling = $fromCeiling;
            $a->toCeiling = $toCeiling;
            array_push($data, $a);
        }
        $fundSourcesToMove = FundSource::whereIn('id',$fundSourceIds)->pluck('name');

        return ['reallocations' => $data, 'fromBudgetClass' => $fromBudgetClass->name, 'toBudgetClass' => $toBudgetClass->name,'fundSourcesToMove'=>$fundSourcesToMove];

    }

}
