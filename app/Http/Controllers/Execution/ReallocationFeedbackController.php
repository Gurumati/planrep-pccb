<?php

namespace App\Http\Controllers\Execution;

use App\Http\Services\CustomPager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class ReallocationFeedbackController extends Controller
{
    public function index(Request $request)
    {
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $financial_year_id  = $request->financial_year_id;
        $system_code = $request->system_code;
        $perPage = isset($request->perPage) ? $request->perPage : 25;
        $sql = "select br.id, i.from_chart_of_accounts, i.to_chart_of_accounts, e.created_at, e.amount, i.is_approved,
        e.queue_name, ai.budget_type,
        r.is_delivered, r.response 
        from budget_reallocations br 
        join budget_reallocation_items i on i.budget_reallocation_id = br.id
        join budget_export_transaction_items it on it.budget_reallocation_item_id = i.id
        join budget_export_to_financial_systems e on e.budget_export_transaction_item_id = it.id
        join budget_export_accounts b on b.id = i.budget_export_account_id
        join activity_facility_fund_source_inputs ai on ai.id = b.activity_facility_fund_source_input_id
        left join budget_export_responses r on r.budget_export_to_financial_system_id = e.id
        where br.admin_hierarchy_id = $admin_hierarchy_id and br.financial_year_id = $financial_year_id 
        and b.financial_system_code = '$system_code'";
        $items = DB::select($sql);
        $data = CustomPager::paginate($items,$perPage);
        return response()->json($data);
    }
}
