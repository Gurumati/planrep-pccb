<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Execution\ImportMethod;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class ImportMethodController extends Controller
{
    public function index() {
        $all = ImportMethod::all();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return ImportMethod::orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new ImportMethod();
            $obj->name = $data->name;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "IMPORT_METHOD_CREATED_SUCCESSFULLY", "importMethods" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = ImportMethod::find($data->id);
            $obj->name = $data->name;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "IMPORT_METHOD_UPDATED_SUCCESSFULLY", "importMethods" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = ImportMethod::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "IMPORT_METHOD_DELETED_SUCCESSFULLY", "importMethods" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = ImportMethod::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        ImportMethod::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "IMPORT_METHOD_RESTORED", "trashedImportMethods" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        ImportMethod::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "IMPORT_METHOD_DELETED_PERMANENTLY", "trashedImportMethods" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = ImportMethod::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "IMPORT_METHOD_DELETED_PERMANENTLY", "trashedImportMethods" => $all];
        return response()->json($message, 200);
    }
}
