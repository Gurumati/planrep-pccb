<?php

namespace App\Http\Controllers\Execution;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use Illuminate\Support\Facades\Log;

class NestIntegrationController extends Controller
{
    //Receive all requests from NeST
    public function getDataFromNest(Request $request){


        // $privateKey = openssl_pkey_get_private("file://" .base_path() . "/config/keys/private.pem");

        // $publicKey  = openssl_pkey_get_public("file://" .base_path() . "/config/keys/public.pem");

        $signedData = $request->all();
        //$data = json_encode($signedData["data"],JSON_UNESCAPED_SLASHES);
        //$data = $signedData["data"];
        //log::info($data);
        // openssl_sign($data, $signature, 
        // $privateKey, "sha256WithRSAEncryption");  
        // return response()->json(["data"=>$data, "signature"=>base64_encode($signature)]);
       // $signature = $signedData["signature"];
        // //$b64signature = base64_encode($signature);
        // log::info(base64_encode($signature));
        // //log::info($b64signature);
        //  //$publicKey  = openssl_pkey_get_public("file://" .base_path() . "/config/keys/public.pem");
        // //log::info($publicKey);
        // // Verify signature obtained for your data
        // $result = openssl_verify($data, base64_encode($signature), 
        //     $publicKey, "sha256WithRSAEncryption");
        
       // if ($result == 1) {
            $trNumber = $signedData["data"]["trNumber"];
            $costCentreCode = $signedData["data"]["peDelegate"];
            $procurementType = $signedData["data"]["procurementType"];
            $financialYear = $signedData["data"]["financialYear"];

            $admin_hierarchy = DB::table('admin_hierarchies as ah')
            ->join('admin_hierarchies as ah2', 'ah.parent_id', 'ah2.id')
            ->where('ah.code', $trNumber)
            ->select('ah.id as admin_hierarchy_id', 'ah2.name as institution_name')
            ->first();
            if(!$admin_hierarchy){
                $errorMessage  = "WRONG trNumber PROVIDED";

                $requestErrorToNest = array(
                    'data' => $errorMessage,
                    'signature'=>app('App\Http\Controllers\Execution\DigitalSignature')->signingData(json_encode($errorMessage))
                );   
                return response()->json($requestErrorToNest,400);

            }
            else
            $admin_hierarchy_id = $admin_hierarchy->admin_hierarchy_id;

            $financial_year_query = "select * from financial_years where name = '".$financialYear."'";
            $get_financial_year = DB::select(DB::raw($financial_year_query));
            $financial_year_id  = isset($get_financial_year[0]->id) ? $get_financial_year[0]->id: null;
            

            try {
                if(count($signedData) > 0 && $costCentreCode != NULL && $admin_hierarchy != NULL && $financial_year_id != NULL ) {

                    if($costCentreCode == 0000){
                        $procurableItems = DB::select("select ah.code trnumber,s1.code depcode,s.code costcentrecode,s.name costcentrename,affsi.budget_type budgetstatus,a.description activitydescription,
                        case when length(a.code)=9 then CONCAT(substring(a.code from 1 for 1),substring(a.code from 5)) else CONCAT(substring(a.code from 1 for 1),substring(a.code from 4)) end as activitycode,
                        fs.code fundsourcecode,fs.name fundsourcedescription,gc.description gfscodedescription,gc.code gfscode,pt.name procurementtype,u.name unitofmeasure,affsi.quantity quantity,affsi.frequency frequency,affsi.unit_price unitprice from mtefs m
                            join admin_hierarchies ah on m.admin_hierarchy_id = ah.id
                            join mtef_sections ms on m.id = ms.mtef_id
                            join sections s on ms.section_id = s.id
                            join sections s1 on s.parent_id = s1.id
                            join activities a on ms.id = a.mtef_section_id
                            join activity_facilities af on a.id = af.activity_id
                            join activity_facility_fund_sources affs on af.id = affs.activity_facility_id
                            join fund_sources fs on affs.fund_source_id = fs.id
                            join activity_facility_fund_source_inputs affsi on affs.id = affsi.activity_facility_fund_source_id
                            join procurement_types pt on affsi.procurement_type_id = pt.id
                            join gfs_codes gc on affsi.gfs_code_id = gc.id
                            join units u on affsi.unit_id = u.id
                        where ah.id = $admin_hierarchy_id and m.financial_year_id = $financial_year_id 
                        and frequency*quantity*unit_price > 0 and a.code != '00000000' 
                        and affsi.procurement_type_id is not null");
                    }elseif($costCentreCode == 0001){
                        $procurableItems = DB::select("select ah.code trnumber,s1.code depcode,s.code costcentrecode,s.name costcentrename,affsi.budget_type budgetstatus,a.description activitydescription,
                        case when length(a.code)=9 then CONCAT(substring(a.code from 1 for 1),substring(a.code from 5)) else CONCAT(substring(a.code from 1 for 1),substring(a.code from 4)) end as activitycode,
                        fs.code fundsourcecode,fs.name fundsourcedescription,gc.description gfscodedescription,gc.code gfscode,pt.name procurementtype,u.name unitofmeasure,affsi.quantity quantity,affsi.frequency frequency,affsi.unit_price unitprice from mtefs m
                            join admin_hierarchies ah on m.admin_hierarchy_id = ah.id
                            join mtef_sections ms on m.id = ms.mtef_id
                            join sections s on ms.section_id = s.id
                            join sections s1 on s.parent_id = s1.id
                            join activities a on ms.id = a.mtef_section_id
                            join activity_facilities af on a.id = af.activity_id
                            join facilities f on af.facility_id = f.id
                            join activity_facility_fund_sources affs on af.id = affs.activity_facility_id
                            join fund_sources fs on affs.fund_source_id = fs.id
                            join activity_facility_fund_source_inputs affsi on affs.id = affsi.activity_facility_fund_source_id
                            join procurement_types pt on affsi.procurement_type_id = pt.id
                            join gfs_codes gc on affsi.gfs_code_id = gc.id
                            join units u on affsi.unit_id = u.id
                        where ah.id = $admin_hierarchy_id and f.facility_code = '00000000' and m.financial_year_id = $financial_year_id 
                        and frequency*quantity*unit_price > 0 and a.code != '00000000' 
                        and affsi.procurement_type_id is not null");
                    }elseif(strlen($costCentreCode)== 3){
                        $procurableItems = DB::select("select ah.code trnumber,s1.code depcode,s.code costcentrecode,s.name costcentrename,affsi.budget_type budgetstatus,a.description activitydescription,
                        case when length(a.code)=9 then CONCAT(substring(a.code from 1 for 1),substring(a.code from 5)) else CONCAT(substring(a.code from 1 for 1),substring(a.code from 4)) end as activitycode,
                        fs.code fundsourcecode,fs.name fundsourcedescription,gc.description gfscodedescription,gc.code gfscode,pt.name procurementtype,u.name unitofmeasure,affsi.quantity quantity,affsi.frequency frequency,affsi.unit_price unitprice from mtefs m
                            join admin_hierarchies ah on m.admin_hierarchy_id = ah.id
                            join mtef_sections ms on m.id = ms.mtef_id
                            join sections s on ms.section_id = s.id
                            join sections s1 on s.parent_id = s1.id
                            join activities a on ms.id = a.mtef_section_id
                            join activity_facilities af on a.id = af.activity_id
                            join activity_facility_fund_sources affs on af.id = affs.activity_facility_id
                            join fund_sources fs on affs.fund_source_id = fs.id
                            join activity_facility_fund_source_inputs affsi on affs.id = affsi.activity_facility_fund_source_id
                            join procurement_types pt on affsi.procurement_type_id = pt.id
                            join gfs_codes gc on affsi.gfs_code_id = gc.id
                            join units u on affsi.unit_id = u.id
                        where ah.id = $admin_hierarchy_id and s1.code ='$costCentreCode' and m.financial_year_id = $financial_year_id 
                        and frequency*quantity*unit_price > 0 and a.code != '00000000' 
                        and affsi.procurement_type_id is not null");
                    }else{
                        $procurableItems = DB::select("select ah.code trnumber,s1.code depcode,s.code costcentrecode,s.name costcentrename,affsi.budget_type budgetstatus,a.description activitydescription,
                        case when length(a.code)=9 then CONCAT(substring(a.code from 1 for 1),substring(a.code from 5)) else CONCAT(substring(a.code from 1 for 1),substring(a.code from 4)) end as activitycode,
                        fs.code fundsourcecode,fs.name fundsourcedescription,gc.description gfscodedescription,gc.code gfscode,pt.name procurementtype,u.name unitofmeasure,affsi.quantity quantity,affsi.frequency frequency,affsi.unit_price unitprice from mtefs m
                            join admin_hierarchies ah on m.admin_hierarchy_id = ah.id
                            join mtef_sections ms on m.id = ms.mtef_id
                            join sections s on ms.section_id = s.id
                            join sections s1 on s.parent_id = s1.id
                            join activities a on ms.id = a.mtef_section_id
                            join activity_facilities af on a.id = af.activity_id
                            join activity_facility_fund_sources affs on af.id = affs.activity_facility_id
                            join fund_sources fs on affs.fund_source_id = fs.id
                            join activity_facility_fund_source_inputs affsi on affs.id = affsi.activity_facility_fund_source_id
                            join procurement_types pt on affsi.procurement_type_id = pt.id
                            join gfs_codes gc on affsi.gfs_code_id = gc.id
                            join units u on affsi.unit_id = u.id
                        where ah.id = $admin_hierarchy_id and s.code ='$costCentreCode' and m.financial_year_id = $financial_year_id 
                        and frequency*quantity*unit_price > 0 and a.code != '00000000' 
                        and affsi.procurement_type_id is not null");
                    }
                    

                    //$procurableItemsArray =  array($procurableItems);  
                // openssl_sign($procurableItems, $signature, 
                // $privateKey, "sha256WithRSAEncryption");                            
                //     $procurableItemsToNest = array(
                //         'data' => $procurableItems,
                //         'signature'=>base64_encode($signature)
                //     );  
                
                if($procurableItems == NULL){
                    $procurableItems  = "Something went wrong, Either budget is not approved or wrong request made";
                    $procurableItemsToNest = array(
                        'data' => $procurableItems,
                        'signature'=>app('App\Http\Controllers\Execution\DigitalSignature')->signingData(json_encode($procurableItems))
                    );                                
                    
                    return response()->json($procurableItemsToNest);
                }else{
                    $procurableItemsToNest = array(
                        'data' => $procurableItems,
                        'signature'=>app('App\Http\Controllers\Execution\DigitalSignature')->signingData(json_encode($procurableItems))
                    );                                
                    
                    return response()->json($procurableItemsToNest);
                }


                } else {
                    $errorMessage  = "Request is Empty or Not Clear";

                    $requestErrorToNest = array(
                        'data' => $errorMessage,
                        'signature'=>app('App\Http\Controllers\Execution\DigitalSignature')->signingData(json_encode($errorMessage))
                    );   
                    return response()->json($requestErrorToNest,400);
                }
            } catch (\Throwable $th) {
                return 'Something went wrong communicate with PlanRep Team';
            }
        // } elseif ($result == 0) {
            
        //     $errorMessage  = "Authentication Failed";
        //      openssl_sign($errorMessage, $signature, 
        //     $privateKey, "sha256WithRSAEncryption"); 
        //     $verificationErrorToNest = array(
        //         'data' => $errorMessage,
        //         'signature'=> base64_encode($signature)
        //     );   
        //     // $verificationErrorToNest = array(
        //     //     'data' => $errorMessage,
        //     //     'signature'=>app('App\Http\Controllers\Execution\DigitalSignature')->signingData(json_encode($errorMessage))
        //     // );   
        //     return response()->json($verificationErrorToNest,400);
        // } else {
        //     return "error: ".openssl_error_string();
        // }

     
        
    }

    public function sendRequestToNest($data){

        $temurl ="10.1.67.188:8000/api/v1/request";
        Log::info("send Request to NeST");
        Log::info(json_encode($data));
        
        $client = new GuzzleHttpClient();

        $response = $client->request("POST", "http://" . $temurl,
            [
                "headers" => [
                    "Accept" => "application/json",
                    "Content-type" => "application/json",
                    "service-code" => "SRVC033"
                ],
                "json" => $data,
            
            ]);    
    
    }

}
