<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Services\Execution\BudgetExportToFinancialSystemService;
use App\Http\Services\Execution\RevenueAccountServices;
use App\Http\Services\Execution\RevenueExportTransactionItemService;
use App\Http\Services\UserServices;
use App\Models\Execution\RevenueExportAccount;
use App\Models\Execution\RevenueExportTransaction;
use App\Models\Execution\BudgetExportToFinancialSystem;
use App\Models\Planning\AdminHierarchySectMappings;
use App\Models\Setup\ConfigurationSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use SendDataToFinancialSystems;

class RevenueExportAccountController extends Controller
{

   public function getApprovedRevenueAccounts(Request $request){
       $adminHierarchyId = $request->adminHierarchyId;
       $financialYearId  = $request->financialYearId;
       $financialSystem  = $request->financialSystem;
       $budgetType  = $request->budgetType;
       $perPage =isset($request->perPage)?$request->perPage:2000;
       $data = RevenueExportAccount::getAllPaginated($adminHierarchyId,$financialYearId,$financialSystem,$budgetType,$perPage);
       $exported = RevenueExportAccount::getAllSent($adminHierarchyId,$financialYearId,$financialSystem);

       return response()->json(['revenue'=>$data, 'exported'=>$exported]);
    }

   public function getRevenueAccounts(Request $request,$adminHierarchyId,$financialYearId)
    {
        $perPage            = isset($request->perPage)?(int)$request->perPage:25;
        $currentPage        = isset($request->currentPage)?(int)$request->currentPage:1;
        $searchString       = isset($request->searchString)?$request->searchString:'%';
        $revenueType        = isset($request->revenueType)?$request->revenueType:"ALL";
        $budgetType         = $request->budgetType;

        switch ($revenueType){
            case "ALL":
                $own_source_revenue = $this->getOwnSourceRevenue($adminHierarchyId,$financialYearId,$budgetType,$searchString );
                $other_revenues     = $this->getRevenue($adminHierarchyId,$financialYearId,$searchString);
                $data               = $own_source_revenue->merge($other_revenues);
                break;
            case "LGRCIS":
                $own_source_revenue = $this->getOwnSourceRevenue($adminHierarchyId,$financialYearId,$budgetType, $searchString );
                $data               = $own_source_revenue;
                break;
            case "MUSE":
                $other_revenues     = $this->getRevenue($adminHierarchyId,$financialYearId,$searchString);
                $data               = $other_revenues;
                break;
            case "FFARS":
                $other_revenues     = $this->getFacilityRevenue($adminHierarchyId,$financialYearId, $budgetType, $searchString);
                $data               = $other_revenues;
                break;
        }
        $data= paginate_this($data,$perPage,$currentPage);
        return json_encode(['revenue'=>$data]);
    }

    /**
     * @param $admin_hierarchy_id
     * @param $financial_year_id
     * @return mixed
     */
    function getRevenue($admin_hierarchy_id, $financial_year_id,$searchQuery )
    {

        $searchQuery = isset($searchQuery)?$searchQuery:"";
        $budgetingSectionLevelsIds =  AdminHierarchySectMappings::getBudgetingSectionLevelIds();
        $revenue = RevenueAccountServices::getRevenue($admin_hierarchy_id,$financial_year_id,$budgetingSectionLevelsIds,$searchQuery);
        return $revenue;
    }

    /**
     * @param $admin_hierarchy_id
     * @param $financial_year_id
     * @return mixed
     */
    function getOwnSourceRevenue($admin_hierarchy_id, $financial_year_id,$budgetType,$searchQuery )
    {

        $own_source = RevenueAccountServices::getOwnSourceRevenue($admin_hierarchy_id, $financial_year_id,$budgetType,$searchQuery);
        return $own_source;
    }
    /**
     * @param $admin_hierarchy_id
     * @param $financial_year_id
     * @return mixed
     */
    function getFacilityRevenue($admin_hierarchy_id, $financial_year_id, $budgetType, $searchQuery )
    {

        $searchQuery = isset($searchQuery)?$searchQuery:"";
        $revenue = RevenueAccountServices::getFacilityRevenue($admin_hierarchy_id,$financial_year_id,$budgetType, $searchQuery);
        return $revenue;
    }

    public function approveRevenueBudget(Request $request, $bySelection, $adminHierarchyId, $financialYearId, $budgetType){

        try {
            if ($bySelection === 'true') {
                $validator = Validator::make($request->all(), ["selectedRevenueIds" => "required"]);
                if ($validator->fails()) {
                    return $validator->errors()->all();
                }
                $selectedRevenueIds = $request->all()['selectedRevenueIds'];
                RevenueAccountServices::approveSelectedRevenue($selectedRevenueIds);
            } else {
                RevenueAccountServices::approveAllRevenue($adminHierarchyId,$financialYearId,$budgetType);
            }
            $data = ["successMessage" => "SUCCESSFUL_UPDATE_REVENUE"];
            return response()->json($data);
        }catch (\Exception $e){
             $data = ["errorMessage" => "ERROR_APPROVING_REVENUE"];
            return response()->json($data,500);
        }
    }

    public function revenueFeedback(Request $request)
    {

        $validator = Validator::make($request->all(), ['adminHierarchyId'=>'required','financialYearId'=>'required','financialSystem'=>'required']);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(),"errorMessage"=>"FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        $adminHierarchyId = $request->adminHierarchyId;
        $financialYearId  = $request->financialYearId;
        $financialSystem  = $request->financialSystem;
        $accounts = DB::table('revenue_export_accounts as acc')
            ->join('revenue_export_transaction_items as reti','acc.id','reti.revenue_export_account_id')
            ->join('revenue_export_transactions as ret','reti.revenue_export_transaction_id','ret.id')
            ->join('gfs_codes as gfs','acc.gfs_code_id','gfs.id')
            ->join('sections as s','acc.section_id','s.id')
            ->where('admin_hierarchy_id',$adminHierarchyId)
            ->where('financial_year_id',$financialYearId)
            ->where('financial_system_code',$financialSystem)
            //->where('acc.is_sent', true)
            ->select('acc.id','acc.admin_hierarchy_id','ret.id as transaction_id',
                'acc.section_id',
                's.name as section',
                'acc.financial_year_id',
                'acc.gfs_code_id',
                'acc.chart_of_accounts',
                'acc.financial_system_code',
                'acc.is_sent',
                'acc.is_delivered',
                'acc.amount','gfs.code as gfs_code',
                'gfs.description as description');
        return response()->json(['feedback' => $accounts->get()],200);
    }

    public function exportRevenue(Request $request)
    {
        
        $validator = Validator::make($request->all(), ['adminHierarchyId'=>'required','financialYearId'=>'required','financialSystem'=>'required']);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(),"errorMessage"=>"FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        $adminHierarchyId = $request->adminHierarchyId;
        $financialYearId  = $request->financialYearId;
        $financialSystem  = $request->financialSystem;
        $comments         = $request->comments;
        $transaction_id   = null;
        if($financialSystem == 'MUSE')
        {
            //$financialSystem = ['MUSE','LGRCIS'];
            $financialSystem = ['MUSE'];
            //check if all gl accounts has been delivered to MUSE
            if( $this->glAccountExist($adminHierarchyId, $financialYearId) == false){
                $message = ["errorMessage" => 'Some of the GL Accounts has not been delivered to MUSE. Please Wait...'];
                return response()->json($message, 200);
            }
        }
        else {
            $financialSystem = [$financialSystem];
        }
        //check if there is revenue budget to be sent to MUSE

       
        $resultArray = DB::transaction(function () use($comments,$adminHierarchyId,$financialYearId,$financialSystem, &$transaction_id){
            $userId =UserServices::getUser()->id;
            $accounts = DB::table('revenue_export_accounts as acc')
                ->join('gfs_codes as gfs','acc.gfs_code_id','gfs.id')
                ->where('admin_hierarchy_id',$adminHierarchyId)
                ->where('financial_year_id',$financialYearId)
                ->whereIn('financial_system_code',$financialSystem)
                ->where(function($q) {
                    $q->where('acc.is_sent', false)->orWhereNull('acc.is_sent');
                })
                ->select('acc.*','gfs.description as description');
               
             $accountArray = array();
            if($accounts->count() > 0) {
                $transaction = new RevenueExportTransaction();
                $transaction->description = isset($comments)?$comments:'';
                $transactionTypeConfig = ConfigurationSetting::getValueByKey('BUDGET_APPROVAL_TRANSACTION_TYPE');
                $budgetGroupName       = ConfigurationSetting::getValueByKey('APPROVED_BUDGET_GROUP_NAME');
                $transaction->budget_transaction_type_id = $transactionTypeConfig;
                $transaction->save();
                $control_number = 'REVENUE'.$transaction->id;
                $transaction->control_number = $control_number;
                $transaction->save();
                $transaction_id = $transaction->id;
                
                foreach ($accounts->get() as $account){

                     $check = BudgetExportToFinancialSystem::where('chart_of_accounts',$account->chart_of_accounts)->where('admin_hierarchy_id',$adminHierarchyId)->where('financial_year_id',$financialYearId)->get();
                     if($check->count() == 0){
                        $item = RevenueExportTransactionItemService::create($transaction->id,$account->id,$account->amount,$userId);

                        BudgetExportToFinancialSystemService::addRevenue($adminHierarchyId,$item->id,$account->chart_of_accounts,$budgetGroupName,
                                                            $account->financial_year_id,$account->amount,$account->financial_system_code,$userId);

                     }else{
                         array_push($accountArray,$account->chart_of_accounts);
                     } 
                }
            }
           // if(count($accountArray)>0){
                return $accountArray;
           // }
        });
        
        
        if(count($resultArray) > 0){
           $accounts = DB::table('revenue_export_transactions as ret')
           ->join('revenue_export_transaction_items as reti','ret.id','reti.revenue_export_transaction_id')
           ->join('budget_export_to_financial_systems as betfs','reti.id','betfs.revenue_export_transaction_item_id')
           ->where('betfs.chart_of_accounts',$resultArray[0])
           ->where('betfs.financial_year_id',$financialYearId)
           ->where('betfs.admin_hierarchy_id',$adminHierarchyId)
           ->orderBy('ret.id', 'DESC')
           ->distinct()->get(['ret.id']);
            $obj     = new SendDataToFinancialSystems();
            $message = $obj->sendRevenue($adminHierarchyId,$financialYearId, $accounts[0]->id);

         // $transaction_id = $accounts[0]->id;
        }

        $obj     = new SendDataToFinancialSystems();
        if(is_array($financialSystem)) //lgrcis and MUSE
        {
             if($transaction_id !== null){
                $message = $obj->sendRevenue($adminHierarchyId,$financialYearId, $transaction_id);
            }else {
                $message = ["errorMessage" => 'Revenue budget has been sent to MUSE.. you can not send it again'];
                return response()->json($message, 200);
            }

        } else if($financialSystem == 'FFARS')
        {
          $message = $obj->sendRevenueToFFARS($adminHierarchyId,$financialYearId);
        }
        //end
        $message = ["successMessage" => $message];
        return response()->json($message, 200);
    }


      /** send transactions to MUSE */
      public function sendTransaction(Request $request){

        $transaction_id = $request->transaction_id;
        $system_code    = $request->system_code;
        $selectedItems  = $request->selectedItems;
        $obj = new SendDataToFinancialSystems();
        if($selectedItems != ''){
            $transaction_id = $this->createExportTransaction($selectedItems);
        }
        //undo transaction
        DB::statement("update budget_export_to_financial_systems set is_sent = false where revenue_export_transaction_item_id in
        (select id from revenue_export_transaction_items where revenue_export_transaction_id = $transaction_id)");
        //check if gl has been sent
        if($this->GLAccountSent($transaction_id)){
            //get params
            $params =   DB::table('revenue_export_transactions as b')
                ->join('revenue_export_transaction_items as i','b.id','i.revenue_export_transaction_id')
                ->join('revenue_export_accounts as a','a.id','i.revenue_export_account_id')
                ->where('b.id', $transaction_id)
                ->select('a.admin_hierarchy_id','a.financial_year_id')
                ->groupBy('a.admin_hierarchy_id','a.financial_year_id')
                ->first();

            if($system_code ==  'MUSE'){
                $message = $obj->sendRevenue($params->admin_hierarchy_id, $params->financial_year_id,$transaction_id);
            }else if($system_code == 'NAVISION') {
                $message = $obj->sendFacilityBudget($params->admin_hierarchy_id, $params->financial_year_id,$system_code,$transaction_id, $params->budget_type);
            }
        }else {
            $message = "Please wait for GL Accounts to be delivered to MUSE..";
        }

        $message = ["successMessage" => $message];
        return response()->json($message, 200);
    }

        /** gl is sent */
    public function GLAccountSent($id){
            $count = DB::table('company_gl_accounts as c')
                ->join('revenue_export_accounts as a', 'a.id', 'c.revenue_export_account_id')
                ->join('revenue_export_transaction_items as i', 'i.revenue_export_account_id', 'a.id')
                ->where('i.revenue_export_transaction_id', $id)
                ->where('c.is_delivered', false)
                ->select('c.id')
                ->count();
            if($count > 0){
                return false;
            }else {
                return true;
            }
        }

    /** create transaction */
    public function createExportTransaction($selectedItems){
        $userId        = UserServices::getUser()->id;
        //create transaction if does not exist
        $transaction = new RevenueExportTransaction();
        $transaction->description = 'SELECTED REVENUE BUDGET';
        $transactionTypeConfig = ConfigurationSetting::getValueByKey('BUDGET_APPROVAL_TRANSACTION_TYPE');
        $budgetGroupName   = 'REVENUE';
        $transaction->budget_transaction_type_id = isset($transactionTypeConfig) ? $transactionTypeConfig : null;
        $transaction->save();
        //get id to update control number
        $control_number = $budgetGroupName . $transaction->id;
        $transaction->control_number = $control_number;
        $transaction->save();

        //get accounts
        $accounts = DB::select("select a.*, gfs.description as description  from revenue_export_accounts a
                                join gfs_codes as gfs on gfs.id = a.gfs_code_id
                                where a.id in
                                ($selectedItems) and a.amount > 0");
                                
        foreach ($accounts as $account) {
            $item = RevenueExportTransactionItemService::create($transaction->id, $account->id, $account->amount, $userId);

            $export = new BudgetExportToFinancialSystem();

            $export->revenue_export_transaction_item_id = $item->id;
            $export->admin_hierarchy_id = $account->admin_hierarchy_id;
            $export->chart_of_accounts = $account->chart_of_accounts;
            $export->description = $budgetGroupName;
            $export->is_sent = false;
            $export->financial_year_id = $account->financial_year_id;
            $export->amount = $account->amount;
            $systemQueueKey = $account->financial_system_code.'_BUDGET_EXPORT_QUEUE_NAME';
            $queue = ConfigurationSetting::where('key',$systemQueueKey)->first();
            $export->queue_name =($queue != null)?$queue->value:'';
            $export->created_by = $userId;
            $export->save();

        }
        return $transaction->id;
    }

    //check if all gl has been sent to MUSE
    public function glAccountExist($admin_hierarchy_id, $financial_year_id){
        $maxGL = ConfigurationSetting::getValueByKey('MAX_UNDELIVERED_GL_TO_RESTRICT_EXPORT');
        $maxGL = (isset($maxGL) && gettype(intVal($maxGL,10)) == "integer") ? $maxGL : 0;

        $GLAccounts = DB::table('revenue_export_accounts  as gl')
            ->join('company_gl_accounts as l', 'l.revenue_export_account_id', 'gl.id')
            ->where('gl.admin_hierarchy_id',$admin_hierarchy_id)
            ->where('gl.financial_year_id',$financial_year_id)
            ->where('gl.financial_system_code','MUSE')
            ->Where('l.is_delivered', false)
            ->select('gl.chart_of_accounts');

        if($GLAccounts->count() > $maxGL){
            return false;
        }else {
            return true;
        }
    }

    //send GL Accounts segments for revenue
    public function sendAccountSegment(Request $request){
       $admin_hierarchy_id = $request->adminHierarchyId;
       $financial_year_id  = $request->financialYearId;
       $system_code        = $request->financial_system_code;
       $obj     = new SendDataToFinancialSystems();
       if($system_code == 'MUSE' || $system_code == 'LGRCIS'){
        $obj->sendRevenueSegments($admin_hierarchy_id, $financial_year_id);
       }
    }

    //send GL Accounts for revenue
    public function sendGLAccount(Request $request){
       $admin_hierarchy_id = $request->adminHierarchyId;
       $financial_year_id  = $request->financialYearId;
       $system_code        = $request->financial_system_code;
       $obj     = new SendDataToFinancialSystems();
       if($system_code == 'MUSE' || $system_code == 'LGRCIS'){
        $obj->sendGLAccount($admin_hierarchy_id, $financial_year_id, 'REVENUE');
       }
       $message = ["successMessage" => 'SUCCESS'];
       return response()->json($message, 200);

    }

    public function downloadExcel(Request $request)
    {

        $adminHierarchyId      = $request->adminHierarchyId;
        $financialYearId       = $request->financialYear;
        $financialSystem = $request->financial_system_code;
        $budgetType           = $request->budget_type;
        $account_type          = $request->account_type;
        $perPage               = Input::get('perPage',10000);
        $items = RevenueExportAccount::getAllPaginated($adminHierarchyId,$financialYearId,$financialSystem,$budgetType,$perPage);

        $array = array();
        $i = 1;
        foreach ($items as $value) {
            $newGlAccount =  str_replace('-', '|',$value->chart_of_accounts );
            $data['GlAccount'] = $newGlAccount;
            $data['Amount'] = $value->amount;
            array_push($array, $data);
            $i++;
        }

        Excel::create('REVENUE_ACCOUNTS' . date("M d, Y H:i:s"), function ($excel) use ($array) {
            $excel->sheet('REVENUE_ACCOUNTS', function ($sheet) use ($array) {
                $sheet->freezeFirstRow();
                $sheet->setColumnFormat(array(
                    'B' => '@'
                ));
                $sheet->setAutoSize(true);
                $sheet->setAllBorders('thin');
                $sheet->fromArray($array);
            });
        })->export('xls');
    }
}
