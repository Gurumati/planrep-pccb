<?php

namespace App\Http\Controllers\Execution;
use App\Http\Controllers\Controller;
use App\Models\Execution\BudgetExportResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Log;



class BudgetExportResponseController extends Controller
{
    public function paginated(Request $request) {
        $perPage            = isset($request->per_page) ? $request->per_page: 15;
        $financial_year_id  = $request->financial_year_id;
        $admin_hierarchy_id  = $request->admin_hierarchy_id;
        
        $response = DB::table('budget_export_responses as ber')
            ->join('budget_export_to_financial_systems as befs', 'ber.budget_export_to_financial_system_id', '=','befs.id')
               ->join('admin_hierarchies as a', 'a.id','=', 'befs.admin_hierarchy_id')
            ->join('financial_years as f','f.id', '=', 'befs.financial_year_id')
            ->join('budget_export_transaction_items as beti', 'beti.id', '=', 'befs.budget_export_transaction_item_id')
            ->join('budget_export_accounts as bea', 'bea.id', '=', 'beti.budget_export_account_id')
            //->where('bea.has_issues', true)
            ->where('f.id', $financial_year_id)
            ->where('a.id',$admin_hierarchy_id)
            ->select('ber.response as response','a.code as company','befs.chart_of_accounts')
            ->paginate($perPage);
        return response()->json(["items"=>$response],200);
    }

    /** budget feedback response -FFARS */
    public function ffarsFeedback(Request $request){
        $all = DB::table('facility_budget_export_responses as e')
                ->join('facilities as f','e.facility_id','f.id')
                ->join('facility_types as ft','ft.id','f.facility_type_id')
                ->join('admin_hierarchies as w','f.admin_hierarchy_id', 'w.id')
                ->join('admin_hierarchies as c','w.parent_id','c.id')
                ->where('e.is_delivered',false)
                ->select('c.name as council','f.name as facility','f.facility_code','ft.name as facility_type','e.is_sent','e.is_delivered','e.response')
                ->paginate($request->per_page);
        return response()->json(["failed_budgets"=>$all],200);        
    }

    public function clearExportIssue(Request $request){
        $e = BudgetExportResponse::find($request->id);
        $e->is_delivered = true;
        $e->save();
        $all = $this->getAllPaginated($request->per_page);
        return response()->json(["items"=>$all,"successMessage" => "Budget Export Issue Cleared!"],200);
    }
}
