<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Execution\BudgetExportTransaction;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class BudgetExportTransactionController extends Controller
{
    public function index() {
        $all = BudgetExportTransaction::with('budget_transaction_type')->orderBy('created_at','asc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return BudgetExportTransaction::with('budget_transaction_type')->orderBy('created_at','asc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new BudgetExportTransaction();
            $obj->description = $data->description;
            $obj->budget_transaction_type_id = $data->budget_transaction_type_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "budgetExportTransactions" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = BudgetExportTransaction::find($data->id);
            $obj->description = $data->description;
            $obj->budget_transaction_type_id = $data->budget_transaction_type_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "budgetExportTransactions" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = BudgetExportTransaction::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "budgetExportTransactions" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = BudgetExportTransaction::with('budget_transaction_type')->orderBy('created_at', 'asc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        BudgetExportTransaction::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedBudgetExportTransactions" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        BudgetExportTransaction::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedBudgetExportTransactions" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = BudgetExportTransaction::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedBudgetExportTransactions" => $all];
        return response()->json($message, 200);
    }
}
