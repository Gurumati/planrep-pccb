<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Execution\BudgetExportToFinancialSystem;
use App\Models\Setup\FinancialYear;
use Illuminate\Support\Facades\DB;

class BudgetExportToFinancialSystemController extends Controller
{
    public function paginated() {
        $section_id = UserServices::getUser()->section_id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
        $all = BudgetExportToFinancialSystem::with(
            'budget_export_transaction_item','financial_year','system',
            'budget_export_transaction_item.budget_export_transaction',
            'budget_export_transaction_item.budget_export_account',
            'budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input',
            'budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input.activity_facility_fund_source',
            'budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input.gfs_code',
            'budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input.unit',
            'budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input.activity_facility_fund_source.activity_facility',
            'budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input.activity_facility_fund_source.fund_source',
            'budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input.activity_facility_fund_source.activity_facility.activity',
            'budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input.activity_facility_fund_source.activity_facility.facility',
            'budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input.activity_facility_fund_source.activity_facility.activity.budget_class',
            'budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input.activity_facility_fund_source.activity_facility.activity.mtef_section',
            'budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input.activity_facility_fund_source.activity_facility.activity.mtef_section.mtef',
            'budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input.activity_facility_fund_source.activity_facility.activity.mtef_section.mtef.admin_hierarchy',
            'budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input.activity_facility_fund_source.activity_facility.activity.mtef_section.mtef.financial_year',
            'budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input.activity_facility_fund_source.activity_facility.activity.mtef_section.section')
            ->whereHas('budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input.activity_facility_fund_source.activity_facility.activity.mtef_section.mtef',
                function ($query) use ($admin_hierarchy_id, $financial_year_id) {
                    $query->where('admin_hierarchy_id', $admin_hierarchy_id)->where('financial_year_id', $financial_year_id)->where('is_final', true);
                })
            ->whereHas('budget_export_transaction_item.budget_export_account.activity_facility_fund_source_input.activity_facility_fund_source.activity_facility.activity.mtef_section',
                function ($query) use ($section_id) {
                    $query->where('section_id', $section_id);
                })->paginate(10);
        return response()->json($all);
    }

    public function send($id){
        $item = BudgetExportToFinancialSystem::find($id);
        $item->is_sent = true;
        $item->response = 'RESPONSE';
        $item->save();
        return response()->json(['successMessage' => "ITEMS_SENT"],200);
    }

    public static function storeRevenue($admin_hierarchy_id, $financial_year_id)
    {
        $data = DB::table('revenue_export_transaction_items as i')
                ->join('revenue_export_accounts as a','a.id','i.revenue_export_account_id')
                ->where('a.admin_hierarchy_id',$admin_hierarchy_id)
                ->where('a.financial_year_id', $financial_year_id)
                ->where('a.deleted_at', null)
                ->select('i.id','a.chart_of_accounts','i.amount')
                ->get();

        $data_items = array();
        foreach($data as $item)
        {
            $items = array('chart_of_accounts'=>$item->chart_of_accounts,'description'=>'REVENUE','financial_year_id'=>$financial_year_id, 'is_sent'=>false,
                           'queue_name'=>'Revenue','amount'=>$item->amount,'admin_hierarchy_id'=>$admin_hierarchy_id,'revenue_export_transaction_item_id'=>$item->id);
            array_push($data_items,$items);
        }
        DB::table('budget_export_to_financial_systems')
            ->insert($data_items);
    }
}
