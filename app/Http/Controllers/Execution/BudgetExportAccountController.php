<?php

namespace App\Http\Controllers\Execution;

use App\Http\Controllers\Controller;
use App\Http\Services\Execution\BudgetExportToFinancialSystemService;
use App\Http\Services\Execution\BudgetExportTransactionItemService;
use App\Http\Services\UserServices;
use App\Models\Execution\BudgetExportAccount;
use App\Models\Execution\BudgetExportAccountGLAccountExportIssue;
use App\Models\Execution\BudgetExportTransaction;
use App\Models\Execution\BudgetExportTransactionItem;
use App\Models\Execution\RevenueExportTransactionItem;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\FinancialYear;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use SendDataToFinancialSystems;
use App\Models\Planning\ActivityFacilityFundSourceInput;
use App\Http\Services\Budgeting\BudgetExportAccountService;
use App\Models\Planning\Mtef;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;


/**
 * @resource BudgetExportAccountController
 *
 * This controller manages budget exports (chart of accounts COA) to financial system e.g MUSE, ffars
 */
class BudgetExportAccountController extends Controller
{
    public function index()
    {
        $section_id = UserServices::getUser()->section_id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
        $all = DB::table('budget_export_accounts')
            ->join('systems', 'systems.code', '=', 'budget_export_accounts.financial_system_code')
            ->where('budget_export_accounts.financial_year_id', $financial_year_id)
            ->where('budget_export_accounts.admin_hierarchy_id', $admin_hierarchy_id)
            ->where('budget_export_accounts.section_id', $section_id)
            ->select('budget_export_accounts.reference_number', 'systems.name')->distinct()->get();
        return $all;
    }

    public function paginated(Request $request)
    {
        $adminHierarchyId      = $request->adminHierarchyId;
        $financialYearId       = $request->financialYear;
        $financial_system_code = $request->financial_system_code;
        $budget_type           = $request->budget_type;
        $account_type          = $request->account_type;
        $perPage               = Input::get('perPage',100);
        

        if(!UserServices::hasPermission(['execution.export.'.$budget_type])){
            return response()->json(null,403);
        }
        //aggregate facility budget data
        if($account_type == 'facility'){
            BudgetExportAccount::getAggregateBudget($adminHierarchyId, $financialYearId, $budget_type);
        }
        $all = BudgetExportAccount::getAllPaginated($adminHierarchyId, $financialYearId, $perPage,$financial_system_code, $budget_type, $account_type);

        $is_facility_account   = $account_type == 'council'?false:true;

        //get summary of each sections
        if($account_type == 'facility'){
            $budget_summary = DB::table('aggregate_facility_budgets as a')
                ->join('aggregate_facility_budget_export_accounts as ai','a.id','ai.aggregate_facility_budget_id')
                ->join('budget_export_accounts as ac', 'ac.id', 'ai.budget_export_account_id')
                ->join('activities as act', 'act.id', 'ac.activity_id')
                ->join('activity_facility_fund_source_inputs as input', 'input.id', 'ac.activity_facility_fund_source_input_id')
                ->where('a.admin_hierarchy_id', $adminHierarchyId)
                ->where('a.financial_year_id', $financialYearId)
                ->where('input.budget_type', $budget_type)
                ->where('a.is_delivered', false)
                ->select('ac.section_id', DB::raw('SUM(input.frequency * input.unit_price * input.quantity) as amount'))
                ->groupBy('ac.section_id')
                ->get();
        }else if($financial_system_code == 'MUSE') {
            $budget_summary = DB::table('budget_export_accounts as a')
                ->join('activity_facility_fund_source_inputs as ai','ai.id','a.activity_facility_fund_source_input_id')
                ->join('activities as ac', 'ac.id', 'a.activity_id')
                ->where('admin_hierarchy_id', $adminHierarchyId)
                ->where('financial_year_id', $financialYearId)
                //->where('financial_system_code', $financial_system_code)
                ->where('ai.budget_type', $budget_type)
                //->where('ac.is_facility_account', $is_facility_account)
                ->select('section_id', DB::raw('SUM(amount) as amount'))
                ->groupBy('section_id')
                ->get();
        }else{

            $budget_summary = DB::table('activities as a')
                    ->join('mtef_sections as ms', 'a.mtef_section_id','ms.id')
                    ->join('mtefs as m', 'ms.mtef_id', 'm.id')
                    ->join('projects as p', 'p.id', 'a.project_id')
                    ->join('activity_facilities as af', 'a.id', 'af.activity_id')
                    ->join('activity_facility_fund_sources as aff', 'af.id','aff.activity_facility_id')
                    ->join('fund_sources as fs', 'aff.fund_source_id', 'fs.id')
                    ->join('activity_facility_fund_source_inputs as affsi', 'aff.id', 'affsi.activity_facility_fund_source_id')
                    ->where('m.admin_hierarchy_id', $adminHierarchyId)
                    ->where('m.financial_year_id', $financialYearId)
                    ->where('p.code','<>', '0000')
                    ->where('affsi.unit_price','>', 0)
            ->select('project_id',DB::raw('sum(affsi.quantity*affsi.frequency*affsi.unit_price) as amount'))
            ->groupBy('project_id')
            ->get();
        }


        //get aggregate project budget
        return response()->json(["items" => $all, "summary" => $budget_summary], 200);
    }

    public function getTotals(Request $request)
    {
        $financialYearId = $request->financialYearId;
        $adminHierarchyId = $request->adminHierarchyId;
        $system_code      = $request->system_code;
        $budget_type      = $request->budget_type;
        $account_type     = $request->account_type;

        if($account_type == 'facility'){

            $total = DB::table('aggregate_facility_budgets as af')
                ->join('aggregate_facility_budget_export_accounts as afc','af.id','afc.aggregate_facility_budget_id')
                ->join('budget_export_accounts as ba','ba.id','afc.budget_export_account_id')
                ->join('activity_facility_fund_source_inputs as affi', 'affi.id', 'ba.activity_facility_fund_source_input_id')
                ->join('activities as a','a.id','ba.activity_id')
                ->where('af.admin_hierarchy_id',$adminHierarchyId)
                ->where('af.financial_year_id',$financialYearId)
                ->where('affi.budget_type',$budget_type)
                ->distinct('af.id')
                ->count('af.id');

            $exported = DB::table('aggregate_facility_budgets as af')
                ->join('aggregate_facility_budget_export_accounts as afc','af.id','afc.aggregate_facility_budget_id')
                ->join('budget_export_accounts as ba','ba.id','afc.budget_export_account_id')
                ->join('activity_facility_fund_source_inputs as affi', 'affi.id', 'ba.activity_facility_fund_source_input_id')
                ->join('activities as a','a.id','ba.activity_id')
                ->where('af.admin_hierarchy_id',$adminHierarchyId)
                ->where('af.financial_year_id',$financialYearId)
                ->where('affi.budget_type',$budget_type)
                ->where('af.is_sent',true)
                ->distinct('af.id')
                ->count('af.id');

            $not_delivered = DB::table('aggregate_facility_budgets as af')
                ->join('aggregate_facility_budget_export_accounts as afc','af.id','afc.aggregate_facility_budget_id')
                ->join('budget_export_accounts as ba','ba.id','afc.budget_export_account_id')
                ->join('activity_facility_fund_source_inputs as affi', 'affi.id', 'ba.activity_facility_fund_source_input_id')
                ->join('activities as a','a.id','ba.activity_id')
                ->where('af.admin_hierarchy_id',$adminHierarchyId)
                ->where('af.financial_year_id',$financialYearId)
                ->where('affi.budget_type',$budget_type)
                ->where('af.is_sent',true)
                ->where('af.is_delivered',false)
                ->distinct('af.id')
                ->count('af.id');
            $realloc       = 0;
        }else {
            $total = DB::table('budget_export_accounts as a')
                ->join('activity_facility_fund_source_inputs as affi', 'affi.id', 'a.activity_facility_fund_source_input_id')
                ->join('activities as ai','a.activity_id','ai.id')
                ->where('a.financial_year_id', $financialYearId)
                ->where('a.admin_hierarchy_id', $adminHierarchyId)
               // ->where('a.financial_system_code',$system_code)
                ->where('affi.budget_type',$budget_type)
                ->where('a.amount', '>',0)
                ->count();

            //exported to financial_systems
            $exported = DB::table('budget_export_to_financial_systems as e')
                ->join('budget_export_transaction_items as itm', 'itm.id', 'e.budget_export_transaction_item_id')
                ->join('budget_export_accounts as ac', 'ac.id', 'itm.budget_export_account_id')
                ->join('activity_facility_fund_source_inputs as affi', 'affi.id', 'ac.activity_facility_fund_source_input_id')
                ->join('activities as ai','ac.activity_id','ai.id')
                ->where('e.admin_hierarchy_id', $adminHierarchyId)
                ->where('e.financial_year_id', $financialYearId)
               // ->where('ac.financial_system_code',$system_code)
                ->where('affi.budget_type',$budget_type)
                ->where('e.is_sent', true)
                ->where('ac.amount', '>', 0)
                ->select('e.id')->distinct()->count();

            //not deliverd to financial systems
            $not_delivered = DB::table('budget_export_to_financial_systems as e')
                ->join('budget_export_transaction_items as itm', 'itm.id', 'e.budget_export_transaction_item_id')
                ->join('budget_export_accounts as ac', 'ac.id', 'itm.budget_export_account_id')
                ->join('activity_facility_fund_source_inputs as affi', 'affi.id', 'ac.activity_facility_fund_source_input_id')
                ->join('activities as ai','ac.activity_id','ai.id')
                ->join('budget_export_responses as r', 'r.budget_export_to_financial_system_id','e.id')
                ->where('e.admin_hierarchy_id', $adminHierarchyId)
                ->where('e.financial_year_id', $financialYearId)
                ->where('ac.financial_system_code',$system_code)
                ->where('e.is_sent', true)
                ->where('r.is_delivered', false)
                ->where('affi.budget_type',$budget_type)
                ->where('ac.amount', '>', 0)
                ->select('e.id')->distinct()->count();

        }


        $data = ['total' => $total, 'exported' => $exported, 'not_delivered'=> $not_delivered, 'realloc' => null];

        return response()->json($data, 200);
    }

    public function budget_export_transaction_item($budget_export_transaction_id, $budget_export_account_id, $amount)
    {
        $item = new BudgetExportTransactionItem();
        $item->budget_export_transaction_id = $budget_export_transaction_id;
        $item->budget_export_account_id = $budget_export_account_id;
        $item->amount = $amount;
        $item->save();
    }

    /**
     *  exportAccounts()
     *
     * This function export chart of accounts to financial systems
     *
     * @param $adminHierarchyId , $financialYearId
     * This function depends on  configuration setting with key
     *   {financial_system_code}_BUDGET_EXPORT_QUEUE_NAME e.g MUSE_BUDGET_EXPORT_QUEUE_NAME
     *   BUDGET_APPROVAL_TRANSACTION_TYPE
     *
     * @return accounts
     */
    public function exportAccounts(Request $request)
    {
        $adminHierarchyId      = $request->adminHierarchyId;
        $financialYearId       = $request->financialYearId;
        $financial_system_code = $request->financial_system_code;
        $budget_type           = $request->budget_type;
        $account_type          = $request->account_type;
        $message               = null;
        $obj = new SendDataToFinancialSystems();
        $maxGL = ConfigurationSetting::getValueByKey('MAX_UNDELIVERED_GL_TO_RESTRICT_EXPORT');
        $maxGL = (isset($maxGL) && gettype(intVal($maxGL, 10)) == "integer") ? $maxGL : 0;

        if($financial_system_code == 'MUSE')
        {
            //check if all gl accounts has been sent to MUSE
            if($account_type == 'facility'){
                $gl = DB::table('aggregate_facility_budgets')
                    ->where('admin_hierarchy_id', $adminHierarchyId)
                    ->where('financial_year_id', $financialYearId)
                    ->where('gl_is_delivered', false)
                    ->get();
            }else {
                $gl = $obj->glAccountExist($adminHierarchyId, $financialYearId);
            }
            if(count($gl) >  $maxGL){
                $message = ["errorMessage" => 'Some of the GL Accounts have not been delivered to MUSE. Please Wait...'];
                return response()->json($message, 200);
            }
        }
        if($account_type !== 'facility'){
            $transaction_id = null; //initialize transaction to null
            $userId = UserServices::getUser()->id;
            $result = DB::select("  select count(a.*) as count from budget_export_accounts a
                                    join activities ac on ac.id = a.activity_id
                                    join activity_facility_fund_source_inputs as ai on ai.id = a.activity_facility_fund_source_input_id
                                    where a.id not in
                                    (select budget_export_account_id from budget_export_transaction_items i
                                    join budget_export_to_financial_systems e on e.budget_export_transaction_item_id = i.id
                                    where e.admin_hierarchy_id = $adminHierarchyId
                                    and e.is_sent = true) and
                                    a.admin_hierarchy_id = $adminHierarchyId and
                                    a.financial_system_code = '$financial_system_code'
                                    and ai.budget_type = '$budget_type' and a.financial_year_id = $financialYearId");

            if ($result[0]->count > 0) {
                //create transaction if does not exist
                $transaction = new BudgetExportTransaction();
                $transaction->description = $budget_type.' BUDGET';
                $transactionTypeConfig = ConfigurationSetting::getValueByKey('BUDGET_APPROVAL_TRANSACTION_TYPE');
                //$budgetGroupName = ConfigurationSetting::getValueByKey('APPROVED_BUDGET_GROUP_NAME');
                $budgetGroupName   = $budget_type;
                $transaction->budget_transaction_type_id = isset($transactionTypeConfig) ? $transactionTypeConfig : null;
                $transaction->save();
                //get id to update control number
                $control_number = $budgetGroupName . '_' . $transaction->id;
                $transaction->control_number = $control_number;
                $transaction->save();

                $transaction_id  = $transaction->id;
                //get accounts
                $accounts = DB::select("select a.*, gfs.description as description  from budget_export_accounts a
                                    join activities ac on ac.id = a.activity_id
                                    join activity_facility_fund_source_inputs as ai on ai.id = a.activity_facility_fund_source_input_id
                                    join gfs_codes as gfs on gfs.id = a.gfs_code_id
                                    where a.id not in
                                    (select budget_export_account_id from budget_export_transaction_items i
                                    join budget_export_to_financial_systems e on e.budget_export_transaction_item_id = i.id
                                    where e.admin_hierarchy_id = $adminHierarchyId
                                    and e.is_sent = true) and
                                    a.admin_hierarchy_id = $adminHierarchyId  and a.financial_system_code = '$financial_system_code'
                                    and ai.budget_type = '$budget_type' and a.financial_year_id = $financialYearId and a.amount > 0");

                foreach ($accounts as $account) {

                    // $count = DB::table("budget_export_transaction_items")->where("budget_export_account_id",$account->id)->count();
                    // if($count == 0){
                    $item = BudgetExportTransactionItemService::create($transaction->id, $account->id, $account->amount, null, true, $userId);

                    BudgetExportToFinancialSystemService::create($adminHierarchyId, $item->id, $account->chart_of_accounts, $budgetGroupName,
                        $account->financial_year_id, $account->amount, $account->financial_system_code, $userId);
                   // }
                    

                }
            }
        }

        //dispatch budget
        if($financial_system_code == 'MUSE')
        {
            if($account_type == 'council'){
                $message = $obj->sendBudget($adminHierarchyId, $financialYearId,$financial_system_code,$transaction_id, $budget_type);
            }else {
                //check if all gl has been received
                if($this->aggregateBudgetGLDelivered($adminHierarchyId, $financialYearId, $budget_type)){
                    $transaction = $this->aggregateBudgetExist($adminHierarchyId, $financialYearId, $budget_type);
                    if($transaction){
                        $message = $obj->sendAggregateBudget($adminHierarchyId, $financialYearId, $transaction, $budget_type);
                    }
                }else {
                    $message = ["errorMessage" => 'Some of the GL Accounts has not been delivered to MUSE. Please Wait...'];
                    return response()->json($message, 200);
                }

            }
        }else if($financial_system_code == 'NPMIS')
        {
            $message = $obj->sendProjectBudget($adminHierarchyId, $financialYearId, $financial_system_code, $transaction_id, $budget_type);

        }else if($financial_system_code == 'OTRMIS')//OTRMIS
        {
            $message = $obj->sendFacilityBudget($adminHierarchyId, $financialYearId, $financial_system_code, $transaction_id, $budget_type);
        }
        //end
        $message = ["successMessage" => $message];
        return response()->json($message, 200);
    }

    public function returnSentAccounts($adminHierarchyId, $financialYearId, $budgetType, $financialSystem)
    {

        $inputIds = ActivityFacilityFundSourceInput::getInputIds($adminHierarchyId, $financialYearId, $financialSystem);

        $budgetExportQuery = BudgetExportAccount::whereIn('activity_facility_fund_source_input_id', $inputIds);
        $revenueExportAccountIds =  DB::table('revenue_export_accounts as rea')
        ->where('rea.admin_hierarchy_id', $adminHierarchyId)
        ->where('rea.financial_year_id', $financialYearId)
        ->where('rea.financial_system_code', $financial_system_code)
        ->pluck('rea.id')
        ->toArray();

        try {
            DB::beginTransaction();
            $budgetExportAccountIds = $budgetExportQuery->pluck('id')->toArray();
            $budgetExportTransactionItemIds = BudgetExportTransactionItem::whereIn('budget_export_account_id', $budgetExportAccountIds)->pluck('id')->toArray();
            $revenueExportTransactionItemIds = RevenueExportTransactionItem::whereIn('revenue_export_account_id', $revenueExportAccountIds)->pluck('id')->toArray();

            foreach ($budgetExportTransactionItemIds as $id) {
                BudgetExportToFinancialSystem::where('id', $id)->update(['is_sent' => false]);
            }

            foreach ($revenueExportTransactionItemIds as $id) {
                BudgetExportToFinancialSystem::where('id', $id)->update(['is_sent' => false]);
            }

            DB::commit();
            $transactions = 0;
            return response()->json(['successMessage' => 'Budget Returned Successfully', 'transactions' => $transactions], 200);
        } catch (\Exception $e) {
            DB::rollback();
            $message = ["errorMessage" => "Error Returning Budget", 'message' => $e->getMessage()];
            //return response()->json($message, 400);
            return $e;
        }
    }

    //transaction exist but for item which has been previousely sent to MUSE (failed transaction items)
    public function transactionExist($adminHierarchyId, $financialYearId, $system_code, $budget_type){
        $result = DB::table('budget_export_to_financial_systems as e')
            ->join('budget_export_transaction_items as itm', 'itm.id', 'e.budget_export_transaction_item_id')
            ->join('budget_export_accounts as ac', 'ac.id', 'itm.budget_export_account_id')
            ->join('activity_facility_fund_source_inputs as ai','ai.id','ac.activity_facility_fund_source_input_id')
            ->join('budget_export_responses as r', 'r.budget_export_to_financial_system_id','e.id')
            ->join('budget_export_transactions as t','t.id','itm.budget_export_transaction_id')
            ->where('e.admin_hierarchy_id', $adminHierarchyId)
            ->where('e.financial_year_id', $financialYearId)
            ->where('ac.financial_system_code',$system_code)
            ->where('ai.budget_type', $budget_type)
            ->where('e.is_sent', true)
            ->where('r.is_delivered', false)
            ->select('t.id')->distinct()->first();
        return isset($result->id)?$result->id:null;
    }

    //resend budget to ffars
    public function resendFFARSBudget(Request $request){
        $obj = new SendDataToFinancialSystems();
        $obj->resendFFARSBudget();
    }

    public function getGLAccountsWithIssues(Request $request)
    {
        $perPage            = isset($request->per_page) ? $request->per_page : 15;
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $financial_year_id  = $request->financial_year_id;

        $all = DB::table('budget_export_account_glaccount_export_issues as beai')
            ->leftJoin('budget_export_accounts as bea', 'beai.budget_export_account_id', '=', 'bea.id')
            ->leftJoin('revenue_export_accounts as rea', 'beai.budget_export_account_id', '=', 'rea.id')
            ->where('beai.is_cleared', false)
            ->where('bea.financial_year_id', $financial_year_id)
            ->where('bea.admin_hierarchy_id', $admin_hierarchy_id)
            ->whereNotNull("beai.response_text")
            ->select('beai.id', 'beai.response_text')
            ->paginate($perPage);
        $data = ["items" => $all];
        return response()->json($data, 200);
    }

    public function clearGLAccountIssues($id)
    {
        $issue = BudgetExportAccountGLAccountExportIssue::find($id);
        $issue->is_cleared = true;
        $issue->delete();
        $all = DB::table('budget_export_account_glaccount_export_issues as beai')
            ->leftJoin('budget_export_accounts as bea', 'beai.budget_export_account_id', '=', 'bea.id')
            ->leftJoin('revenue_export_accounts as rea', 'beai.budget_export_account_id', '=', 'rea.id')
            ->where('beai.is_cleared', false)->whereNotNull("beai.response_text")
            ->select('beai.id', 'beai.response_text')
            ->paginate(10);
        $data = ["items" => $all,"successMessage"=>"GL Account Issue Cleared!"];
        return response()->json($data, 200);
    }

    public function coaSegmentExist(Request $request)
    {
        $admin_hierarchy_id    = $request->adminHierarchyId;
        $financial_year_id     = $request->financialYearId;
        $financial_system_code = $request->financial_system_code;
        $obj                   = new SendDataToFinancialSystems();
        if($financial_system_code == 'MUSE')
        {
            $data = $obj->coaSegmentExist($admin_hierarchy_id, $financial_year_id);
        }
        if($financial_system_code == 'FFARS')
        {
            $data = $obj->setupSegmentExist($admin_hierarchy_id,$financial_year_id);
        }
        return response()->json($data, 200);
    }

    public function glAccountExist(Request $request)
    {
        $admin_hierarchy_id    = $request->adminHierarchyId;
        $financial_year_id     = $request->financialYearId;
        $financial_system_code = $request->financial_system_code;
        $budget_type           = $request->budget_type;
        $account_type          = $request->account_type;

        $obj = new SendDataToFinancialSystems();
        if($financial_system_code == 'MUSE')
        {
            //check if all segments has been delivered to MUSE
            if($obj->segmentDeliveryStatus($admin_hierarchy_id)){
                $data = $obj->sendGLAccount($admin_hierarchy_id, $financial_year_id,'EXPENSE', $account_type);
            }else {
                //there are undeliverd messages
                //check if there is any unresolved issue with segment
                if($obj->segmentDeliveryError($admin_hierarchy_id)){
                    $data = ['errorMessage'=>'An error occured when sending segments.. Please Resolve that error first..'];
                    return response()->json($data, 200);
                }else {
                    $data = ['errorMessage'=>'Please wait for the segments responses from MUSE...'];
                    return response()->json($data, 200);
                }
            }
        }else if($financial_system_code == 'FFARS')
        {
            //check whether all activities has been sent to ffars
            $count = $this->sendFFARSActivity($admin_hierarchy_id, $financial_year_id, $budget_type);
            if($count > 0){
                $data = ['errorMessage'=> $count.' Activities were not sent to FFARS. Please check'];
                return response()->json($data, 200);
            }
        }
        $data = ['success'=>'success'];
        return response()->json($data, 200);
    }

    public function generateSegments(Request $request)
    {
        $admin_hierarchy_id    = $request->adminHierarchyId;
        $financial_year_id     = $request->financialYearId;
        $financial_system_code = $request->financial_system_code;
        $budget_type           = $request->budget_type;
        $account_type          = $request->account_type;

        $obj = new SendDataToFinancialSystems();
        if($financial_system_code == 'MUSE')
        {
            try {
                //check if there are segments which have not been sent to MUSE
                $result = $obj->coaSegmentExist($admin_hierarchy_id, $financial_year_id, $budget_type, $account_type);
                if(count($result['error']) == 0){
                    //if all segments has been sent
                    if($obj->allSegmenstsHasBeenSent($admin_hierarchy_id, $financial_year_id, $budget_type) == false){
                        $obj->generateSegments($admin_hierarchy_id, $financial_year_id, $budget_type);
                    }

                    $message = 'Success';
                    return response()->json($message, 200);
                }else {
                    return response()->json( $result, 500);
                }
            } catch (Exception $e) {
                $message = 'Error';
                return response()->json($message, 500);
            }
        }
    }

    public function sendGLAccount(Request $request)
    {
        $admin_hierarchy_id = $request->adminHierarchyId;
        $financial_year_id = $request->financialYearId;
        $obj = new SendDataToFinancialSystems();
        $data = $obj->sendGLAccount($admin_hierarchy_id, $financial_year_id,'EXPENSE');
        return response()->json(['message' => $data], 200);
    }

    //get PE  Budget
    public function getPEBudget(Request $request){
        $result = DB::select("select affi.id from
                            activities ac
                            join activity_facilities af on af.activity_id = ac.id
                            join activity_facility_fund_sources aff on aff.activity_facility_id = af.id
                            join activity_facility_fund_source_inputs affi on affi.activity_facility_fund_source_id = aff.id
                            where budget_class_id in (17,20)
                            and ac.code = '00000000'");
        $financial_year_id = FinancialYear::where('status', 1)->first()->id;
        foreach($result as $item){
            //check if it has been registred to export accounts
            if($this->existinExportAccount($item->id) == false){
                array_push($id, $item->id);
            }
        }
        $Ids = implode(",", $id);

        if(count($id) > 0){
            //get all councils
            $councils = DB::select("select id from admin_hierarchies where admin_hierarchy_level_id = 9 ");
            foreach($councils as $item){
                BudgetExportAccountService::createBudgetAccounts($item->id,$financial_year_id,$Ids, $budget_type = null);
            }
        }
        //get chart of accounts
        $data = DB::select("SELECT ac.chart_of_accounts as coa, ah.name as council, ac.amount  FROM
                           budget_export_accounts ac join activities a on a.id = ac.activity_id
                           join admin_hierarchies ah on ah.id = ac.admin_hierarchy_id
                           where a.code = '00000000' and ac.amount > 0
                           ORDER BY ah.name");
        return response()->json(['budget' => $data], 200);
    }

    //exist in export account
    public function existinExportAccount($input){
        $result = BudgetExportAccount::where('activity_facility_fund_source_input_id', $input)->select('id')->count();
        if($result > 0){
            return true;
        }else {
            return false;
        }
    }
    //send PE budget for all councils
    public function sendPEBudget(){
        $obj  = new SendDataToFinancialSystems();
        $financial_year_id = FinancialYear::where('status', 1)->first()->id;
        $budget_class = [17,20];
        $userId = UserServices::getUser()->id;
        $budget_type = 'APPROVED';
        $failed_councils = array();
        //get all councils
        $councils = DB::select("select id, name from admin_hierarchies where admin_hierarchy_level_id = 9 ");
        foreach($councils as $item){
            $admin_hierarchy_id = $item->id;
            //check if pe budget has been sent
            if($this->peBudgetExist($admin_hierarchy_id, $financial_year_id, 'MUSE', $budget_class, $budget_type) == false){
                //check if all gl accounts has been sent to MUSE
                $gl = $obj->glAccountExist($admin_hierarchy_id, $financial_year_id);
                $maxGL = ConfigurationSetting::getValueByKey('MAX_UNDELIVERED_GL_TO_RESTRICT_EXPORT');
                $maxGL = (isset($maxGL) && gettype(intVal($maxGL,10)) == "integer") ? $maxGL : 0;
                if(count($gl) > $maxGL){
                    $message = ["errorMessage" => 'Some of the GL Accounts has not been delivered to MUSE. Please Wait...'];
                    array_push($failed_councils, $item->name);
                }else {
                    //send budget.. create transaction
                    $transaction_id = $this->createTransaction($admin_hierarchy_id, $financial_year_id, 'MUSE', $budget_class, $userId, $budget_type);
                    $message = $obj->sendBudget($admin_hierarchy_id, $financial_year_id,'MUSE',$transaction_id, $budget_type);
                }
            }
        }
        if(count($failed_councils) > 0){
            return response()->json($message, 200);
        }else {
            $message = ["successMessage" => $message];
            return response()->json($message, 200);
        }
    }

    //send  Account Segments
    public function sendPESegments(){
        $obj = new SendDataToFinancialSystems();
        $financial_year_id = FinancialYear::where('status', 1)->first()->id;

        $budget_type = 'APPROVED';
        //get all councils
        $councils = DB::select("select id from admin_hierarchies where admin_hierarchy_level_id = 9 ");
        foreach($councils as $item){
            $admin_hierarchy_id = $item->id;
            //send COA segments
            $obj->generateSegments($admin_hierarchy_id, $financial_year_id, $budget_type);
        }
        $message = ["successMessage" => 'SUCCESS'];
        return response()->json($message, 200);
    }

    //send GL
    public function sendPEGL(){
        $obj                   = new SendDataToFinancialSystems();
        $financial_year_id = FinancialYear::where('status', 1)->first()->id;

        //get all councils
        $councils = DB::select("select id from admin_hierarchies where admin_hierarchy_level_id = 9 ");
        foreach($councils as $item){
            $admin_hierarchy_id = $item->id;
            //send GL Account
            $obj->sendGLAccount($admin_hierarchy_id, $financial_year_id,'EXPENSE');
        }
        $message = ["successMessage" => 'success'];
        return response()->json($message, 200);
    }

    //create transaction
    public function createTransaction($adminHierarchyId, $financialYearId, $financial_system_code, $budget_class, $userId, $budget_type){
        //create transaction if does not exist
        $transaction = new BudgetExportTransaction();
        $transaction->description = $budget_type.' PE BUDGET';
        $transactionTypeConfig = ConfigurationSetting::getValueByKey('BUDGET_APPROVAL_TRANSACTION_TYPE');
        //$budgetGroupName = ConfigurationSetting::getValueByKey('APPROVED_BUDGET_GROUP_NAME');
        $budgetGroupName   = $budget_type;
        $transaction->budget_transaction_type_id = isset($transactionTypeConfig) ? $transactionTypeConfig : null;
        $transaction->save();
        //get id to update control number
        $control_number = $budgetGroupName . '_' . $transaction->id;
        $transaction->control_number = $control_number;
        $transaction->save();

        $transaction_id  = $transaction->id;
        //get accounts
        $budget_class_flat = implode(",", $budget_class);

        $accounts = DB::select("select a.*, gfs.description as description  from budget_export_accounts a
                                join activities ac on ac.id = a.activity_id
                                join activity_facility_fund_source_inputs as ai on ai.id = a.activity_facility_fund_source_input_id
                                join gfs_codes as gfs on gfs.id = a.gfs_code_id
                                where a.id not in
                                (select budget_export_account_id from budget_export_transaction_items i
                                join budget_export_to_financial_systems e on e.budget_export_transaction_item_id = i.id
                                where e.admin_hierarchy_id = $adminHierarchyId) and
                                a.admin_hierarchy_id = $adminHierarchyId and
                                ac.budget_class_id in ($budget_class_flat) and a.financial_system_code = '$financial_system_code'
                                and ai.budget_type = '$budget_type' and a.financial_year_id = $financialYearId and a.amount > 0");

        foreach ($accounts as $account) {
            $item = BudgetExportTransactionItemService::create($transaction->id, $account->id, $account->amount, null, true, $userId);

            BudgetExportToFinancialSystemService::create($adminHierarchyId, $item->id, $account->chart_of_accounts, $budgetGroupName,
                $account->financial_year_id, $account->amount, $account->financial_system_code, $userId);
        }
        return $transaction_id;
    }

    //pe budget has been sent to MUSE
    public function peBudgetExist($adminHierarchyId, $financialYearId, $financial_system_code, $budget_class, $budget_type){
        $budget_class = implode(",", $budget_class);
        $accounts = DB::select("select count(a.*) as count from budget_export_accounts a
                                join activities ac on ac.id = a.activity_id
                                join activity_facility_fund_source_inputs as ai on ai.id = a.activity_facility_fund_source_input_id
                                where a.id not in
                                (select budget_export_account_id from budget_export_transaction_items i
                                join budget_export_to_financial_systems e on e.budget_export_transaction_item_id = i.id
                                where e.admin_hierarchy_id = $adminHierarchyId) and
                                a.admin_hierarchy_id = $adminHierarchyId and
                                ac.budget_class_id in ($budget_class) and a.financial_system_code = '$financial_system_code'
                                and a.amount > 0 and
                                and ai.budget_type = '$budget_type' and a.financial_year_id = $financialYearId");

        if($accounts[0]->count > 0){
            return false;
        }else {
            return true;
        }
    }

    /**return all transaction from MUSE */
    public function getTransactions(Request $request){
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $financial_year_id = $request->financial_year_id;
        $budget_type        = $request->budget_type;
        $system_code        = $request->system_code;
        $status             = $request->status;

        $transactions = DB::table('budget_export_transactions as b')
            ->join('budget_export_transaction_items as i','b.id','i.budget_export_transaction_id')
            ->join('budget_export_accounts as a','a.id','i.budget_export_account_id')
            ->join('activities as act', 'act.id','a.activity_id')
            ->join('admin_hierarchies as ac','ac.id', 'a.admin_hierarchy_id')
            ->where('a.financial_system_code',$system_code)
            ->where('act.budget_type', $budget_type)
            ->where('a.admin_hierarchy_id', $admin_hierarchy_id)
            ->where('a.financial_year_id', $financial_year_id)
            ->where('i.budget_reallocation_item_id', '=', null)
            ->where('b.is_sent', $status)
            ->select('b.id','b.description','ac.name as council','act.budget_type','b.is_sent','b.control_number', DB::raw('count(a.*) as transactions'))
            ->groupBy('b.id','b.description','ac.name','act.budget_type','b.is_sent','b.control_number')
            ->orderBy('b.id')
            ->get();
        return response()->json(['transactions'=>$transactions], 200);
    }

    /** get budget export accounts */
    public function getExportAccounts(Request $request){
        $transaction_id = $request->transaction_id;
        $transactions = DB::table('budget_export_transactions as b')
            ->join('budget_export_transaction_items as i','b.id','i.budget_export_transaction_id')
            ->leftjoin('budget_export_to_financial_systems as f', 'f.budget_export_transaction_item_id', 'i.id')
            ->leftjoin('budget_export_responses as r', 'r.budget_export_to_financial_system_id', 'f.id')
            ->join('budget_export_accounts as a','a.id','i.budget_export_account_id')
            ->where('b.id', $transaction_id)
            ->select('b.id','a.id as export_account_id','b.description','a.chart_of_accounts','a.amount', 'r.is_delivered', 'r.response')
            ->orderBy('r.is_delivered')
            ->get();
        return response()->json(['transactions'=>$transactions], 200);
    }

    /** send Segments */
    public function sendTransactionSegment(Request $request){
        $transaction_id = $request->transaction_id;
        $obj = new SendDataToFinancialSystems();
        //truncate gl accounts which are not sent to MUSE
        DB::statement("delete from company_gl_accounts where budget_export_account_id in
                        (select id from budget_export_accounts where chart_of_accounts
                        not in (select chart_of_accounts from budget_export_to_financial_systems e
                                join budget_export_transaction_items i on i.id = e.budget_export_transaction_item_id
                                where i.budget_export_transaction_id = $transaction_id))");

        $councils = DB::table('budget_export_transactions as b')
            ->join('budget_export_transaction_items as i','b.id','i.budget_export_transaction_id')
            ->join('budget_export_accounts as a','a.id','i.budget_export_account_id')
            ->join('activities as ac', 'ac.id', 'a.activity_id')
            ->where('b.id', $transaction_id)
            ->select('a.admin_hierarchy_id','a.financial_year_id','ac.budget_type','a.financial_system_code')
            ->groupBy('a.admin_hierarchy_id','a.financial_year_id', 'ac.budget_type', 'a.financial_system_code')
            ->get();
        foreach($councils as $item){
            $admin_hierarchy_id = $item->admin_hierarchy_id;
            $financial_year_id =  $item->financial_year_id;
            $budget_type       =  $item->budget_type;
            $financial_system_code = $item->financial_system_code;
            if($financial_system_code == 'MUSE'){
                //check if segment exists
                $result = $obj->coaSegmentExist($admin_hierarchy_id, $financial_year_id, $budget_type);
                if(count($result['error']) == 0){
                    //if all segments has been sent
                    if($obj->allSegmenstsHasBeenSent($admin_hierarchy_id, $financial_year_id, $budget_type) == false){
                        $obj->generateSegments($admin_hierarchy_id, $financial_year_id, $budget_type);
                    }
                    $message = ["successMessage" => 'success'];
                    return response()->json($message, 200);
                }else {
                    return response()->json( $result, 500);
                }
            }
        }
        //return succes if system code is not MUSE
        $message = ["successMessage" => 'success'];
        return response()->json($message, 200);
    }

    /** send GL as per transactions */
    public function sendTransactionGL(Request $request){
        $transaction_id = $request->transaction_id;
        $obj = new SendDataToFinancialSystems();
        //get admin_hierarchy ids
        $councils = DB::table('budget_export_transactions as b')
            ->join('budget_export_transaction_items as i','b.id','i.budget_export_transaction_id')
            ->join('budget_export_accounts as a','a.id','i.budget_export_account_id')
            ->where('b.id', $transaction_id)
            ->select('a.admin_hierarchy_id','a.financial_year_id', 'a.financial_system_code')
            ->groupBy('a.admin_hierarchy_id','a.financial_year_id', 'a.financial_system_code')
            ->get();
        foreach($councils as $item){
            $admin_hierarchy_id = $item->admin_hierarchy_id;
            $financial_year_id =  $item->financial_year_id;
            $financial_system_code = $item->financial_system_code;
            if($financial_system_code == 'MUSE'){
                //send GL Account
                $obj->sendGLAccount($admin_hierarchy_id, $financial_year_id,'EXPENSE');
            }
        }
        $message = ["successMessage" => 'success'];
        return response()->json($message, 200);
    }

    /** send transactions to MUSE */
    public function sendTransaction(Request $request){
        $transaction_id = $request->transaction_id;
        $system_code    = $request->system_code;
        $selectedItems  = $request->selectedItems;
        $obj = new SendDataToFinancialSystems();
        if($selectedItems != ''){
            $transaction_id = $this->createExportTransaction($selectedItems);
        }
        //undo transaction
        DB::statement("update budget_export_to_financial_systems set is_sent = false where id in
       (select e.id from budget_export_to_financial_systems as e
       join budget_export_transaction_items as i on i.id = e.budget_export_transaction_item_id
       where i.budget_export_transaction_id = $transaction_id)");
        //check if gl has been sent
        if($this->GLAccountSent($transaction_id)){
            //get params
            $params =   DB::table('budget_export_transactions as b')
                ->join('budget_export_transaction_items as i','b.id','i.budget_export_transaction_id')
                ->join('budget_export_accounts as a','a.id','i.budget_export_account_id')
                ->join('activities as ac','a.activity_id','ac.id')
                ->where('b.id', $transaction_id)
                ->select('a.admin_hierarchy_id','a.financial_year_id','ac.budget_type')
                ->groupBy('a.admin_hierarchy_id','a.financial_year_id', 'ac.budget_type')
                ->first();

            if($system_code ==  'MUSE'){
                $message = $obj->sendBudget($params->admin_hierarchy_id, $params->financial_year_id,$system_code,$transaction_id, $params->budget_type);
            }else if($system_code == 'FFARS') {
                $message = $obj->sendFacilityBudget($params->admin_hierarchy_id, $params->financial_year_id,$system_code,$transaction_id, $params->budget_type);
            }
        }else {
            $message = "Please wait for GL Accounts to be delivered to MUSE..";
        }

        $message = ["successMessage" => $message];
        return response()->json($message, 200);
    }

    /** gl is sent */
    public function GLAccountSent($id){
        $count = DB::table('company_gl_accounts as c')
            ->join('budget_export_accounts as a', 'a.id', 'c.budget_export_account_id')
            ->join('budget_export_transaction_items as i', 'i.budget_export_account_id', 'a.id')
            ->where('i.budget_export_transaction_id', $id)
            ->where('c.is_delivered', false)
            ->select('c.id')
            ->count();
        if($count > 0){
            return false;
        }else {
            return true;
        }
    }

    /** create transaction */
    public function createExportTransaction($selectedItems){
        $userId        = UserServices::getUser()->id;
        //create transaction if does not exist
        $transaction = new BudgetExportTransaction();
        $transaction->description = 'SELECTED BUDGET';
        $transactionTypeConfig = ConfigurationSetting::getValueByKey('BUDGET_APPROVAL_TRANSACTION_TYPE');
        $budgetGroupName   = 'SELECTED_BUDGET';
        $transaction->budget_transaction_type_id = isset($transactionTypeConfig) ? $transactionTypeConfig : null;
        $transaction->save();
        //get id to update control number
        $control_number = $budgetGroupName . '_' . $transaction->id;
        $transaction->control_number = $control_number;
        $transaction->save();
        //get accounts
        $accounts = DB::select("select a.*, gfs.description as description  from budget_export_accounts a
                                join activities ac on ac.id = a.activity_id
                                join activity_facility_fund_source_inputs as ai on ai.id = a.activity_facility_fund_source_input_id
                                join gfs_codes as gfs on gfs.id = a.gfs_code_id
                                where a.id in
                                ($selectedItems) and a.amount > 0");

        foreach ($accounts as $account) {
            $item = BudgetExportTransactionItemService::create($transaction->id, $account->id, $account->amount, null, true, $userId);

            BudgetExportToFinancialSystemService::create($account->admin_hierarchy_id, $item->id, $account->chart_of_accounts, $budgetGroupName,
                $account->financial_year_id, $account->amount, $account->financial_system_code, $userId);
        }
        return $transaction->id;
    }


    //check if aggregate facility budget exists
    public function aggregateBudgetExist($adminHierarchyId, $financialYearId, $budget_type){
        $result = DB::table('aggregate_facility_budgets as afb')
            ->where('admin_hierarchy_id', $adminHierarchyId)
            ->where('financial_year_id', $financialYearId)
            ->where('is_sent', false)
            ->select('timestamp')
            ->groupBy('timestamp')
            ->first();
        if (!$result){
            return null;
        } else {
            return $result->timestamp;
        }
    }

    //check if aggregate budget gl has been delivered to MUSE
    public function aggregateBudgetGLDelivered($adminHierarchyId, $financialYearId, $budget_type){
        $maxGL = ConfigurationSetting::getValueByKey('MAX_UNDELIVERED_GL_TO_RESTRICT_EXPORT');
        $maxGL = (isset($maxGL) && gettype(intVal($maxGL,10)) == "integer") ? $maxGL : 0;

        $number = DB::table('aggregate_facility_budgets')
            ->where('admin_hierarchy_id', $adminHierarchyId)
            ->where('financial_year_id', $financialYearId)
            ->where('gl_is_delivered', false)
            ->count();
        return $number > $maxGL ? false : true;
    }

    //send activities to FFARS
    public function sendFFARSActivity($admin_hierarchy_id, $financial_year_id, $budget_type){
        //get Mtef
        $mtef = Mtef::where('admin_hierarchy_id', $admin_hierarchy_id)
            ->where('financial_year_id', $financial_year_id)
            ->first();
        BudgetExportAccountService::createActivities($mtef, $budget_type);
        //check whether all ffars activities has been sent to ffars
        $exist = DB::select("SELECT count(id) as count FROM ffars_activities where
                            admin_hierarchy_id = $admin_hierarchy_id
                            and financial_year_id::integer = $financial_year_id
                            and budget_type = '$budget_type'
                            and is_delivered_to_ffars = false");
        return $exist[0]->count;
    }


    public function downloadExcel(Request $request)
    {

        $adminHierarchyId      = $request->adminHierarchyId;
        $financialYearId       = $request->financialYear;
        $financial_system_code = $request->financial_system_code;
        $budget_type           = $request->budget_type;
        $account_type          = $request->account_type;
        $perPage               = Input::get('perPage',10000);
        $items = BudgetExportAccount::getAllPaginated($adminHierarchyId, $financialYearId, $perPage,$financial_system_code, $budget_type, $account_type);

        $array = array();
        $i = 1;
        foreach ($items as $value) {
            $newGlAccount =  str_replace('-', '|',$value->chart_of_accounts );
            $data['GlAccount'] = $newGlAccount;
            $data['Amount'] = $value->amount;
            array_push($array, $data);
            $i++;
        }

        Excel::create('BUDGET_ACCOUNTS' . date("M d, Y H:i:s"), function ($excel) use ($array) {
            $excel->sheet('BUDGET_ACCOUNTS', function ($sheet) use ($array) {
                $sheet->freezeFirstRow();
                $sheet->setColumnFormat(array(
                    'B' => '@'
                ));
                $sheet->setAutoSize(true);
                $sheet->setAllBorders('thin');
                $sheet->fromArray($array);
            });
        })->export('xls');
    }
}
