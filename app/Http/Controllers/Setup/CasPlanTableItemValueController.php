<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\CasPlanTableItemValue;
use App\Models\Setup\ConfigurationSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class CasPlanTableItemValueController extends Controller
{
    private $adminHierarchyIds = array();
    public function index(Request $request)
    {
        
        $table_id  = $request->table_id;
        $facility  = $request->facility;
        $period_id = $request->period_id;
        $financial_year_id =$request->financial_year_id;
        $admin_hierarchy   = UserServices::getUser()->admin_hierarchy_id;
        //get cas_plan financial_year type
        $cas_plan = DB::table('cas_plan_tables as t')
                    ->join('cas_plan_contents as c', 'c.id', 't.cas_plan_content_id')
                    ->join('cas_plans as p','p.id', 'c.cas_plan_id')
                    ->where('t.id', $table_id)
                    ->select('p.financial_year_type')
                    ->first();
//        if($cas_plan->financial_year_type == 'planning'){
//            $financialYear = FinancialYearServices::getPlanningFinancialYear();
//            $financial_year_id = $financialYear->id;
//        }else {
//            $financial_year_id = ConfigurationSetting::getValueByKey('AUTO_FINANCIAL_YEAR_TO_REPORT');
//        }
        //check if plan is for facility
        if($facility > 0)
        {
            $dataItemValues = DB::table('cas_plan_table_item_values as v')
                ->join('cas_plan_table_columns as c','v.cas_plan_table_column_id','c.id')
                ->leftJoin('cas_plan_table_items as i','v.cas_plan_table_item_id','i.id')
                ->where('c.cas_plan_table_id',$table_id)
                ->where('v.financial_year_id',$financial_year_id)
                ->where('v.admin_hierarchy_id',$admin_hierarchy)
                ->where('v.facility_id',$facility)
               ->where('v.period_id', $period_id)
                ->select('v.*','c.sort_order as column_order','i.sort_order as row_order')
                ->orderBy('c.sort_order')
                ->get();
               
        }else
        {
            $dataItemValues = DB::table('cas_plan_table_item_values as v')
                ->join('cas_plan_table_columns as c','v.cas_plan_table_column_id','c.id')
                ->leftJoin('cas_plan_table_items as i','v.cas_plan_table_item_id','i.id')
                ->where('c.cas_plan_table_id',$table_id)
                ->where('v.financial_year_id',$financial_year_id)
                ->where('v.admin_hierarchy_id',$admin_hierarchy)
                ->where('v.period_id', $period_id)
                ->select('v.*','i.sort_order as row_order')
                ->orderBy('c.sort_order')
                ->get();
//            $dataItemValues = DB::table('cas_plan_table_details as v')
//                ->join('cas_plan_tables as c','v.cas_plan_table_id','c.id')
//                ->leftJoin('cas_plan_table_columns as cp','cp.cas_plan_table_id','c.id')
//                ->leftJoin('cas_plan_table_item_values as cv','cv.cas_plan_table_column_id','cp.id')
//                ->leftJoin('cas_plan_table_items as i','i.cas_plan_table_id','c.id')
//                ->where('v.cas_plan_table_id',$table_id)
//                ->where('v.financial_year_id',$financial_year_id)
//                ->where('v.admin_hierarchy_id',$admin_hierarchy)
//                ->select('v.*','i.id as cas_plan_table_item_id','cp.id as cas_plan_table_column_id','cp.sort_order','cv.value')
//                ->get();
        }

        //get admin hierarchies
        $this->adminHierarchyWithParents($admin_hierarchy);

        //get constant values
        $dataConstants  = DB::table('cas_plan_table_item_constants as c')
                          ->join('cas_plan_table_columns as l','l.id','c.cas_plan_table_column_id')
                          ->where('l.cas_plan_table_id',$table_id)
                          ->whereIn('c.admin_hierarchy_id',$this->adminHierarchyIds)
                          ->select('c.id','c.cas_plan_table_column_id','c.cas_plan_table_item_id','c.admin_hierarchy_id','c.value')
                          ->get();
        //get admin hierarchy level
        $admin_level    = DB::table('admin_hierarchies as a')
                          ->where('id',$admin_hierarchy)
                          ->first()->admin_hierarchy_level_id;

        //flattern object
        $flatternResult = CasPlanTableItemValue::flatternData($dataItemValues);
        $dataItemValues = $flatternResult['data_array'];
        $facilities     = $flatternResult['facilities'];
        $all = ['casPlanItemValues'=>$dataItemValues,
                'facilities'=>$facilities,
                'dataConstants'=>$dataConstants,
                'admin_hierarchy_level'=>$admin_level];
        return response()->json($all);
    }

    public function store(Request $request)
    {
        $inputData       = json_decode($request->getContent());
        $admin_hierarchy = UserServices::getUser()->admin_hierarchy_id;
        $table_id        = $inputData->table_id;
        $period_id       = isset($inputData->period_id)?$inputData->period_id:null;
        $fy_id       = $inputData->financial_year_id;
        $data_array      = array();
        $data            = $inputData->inputFields;

        //get cas_plan financial_year type
        $cas_plan = DB::table('cas_plan_tables as t')
                    ->join('cas_plan_contents as c', 'c.id', 't.cas_plan_content_id')
                    ->join('cas_plans as p','p.id', 'c.cas_plan_id')
                    ->where('t.id', $table_id)
                    ->select('p.financial_year_type')
                    ->first();
//        if($cas_plan->financial_year_type == 'planning'){
//            $financialYear     = FinancialYearServices::getPlanningFinancialYear();
//            $financial_year_id = $financialYear->id;
//        }else {
//            $financial_year_id = ConfigurationSetting::getValueByKey('AUTO_FINANCIAL_YEAR_TO_REPORT');
//        }

        /** store data **/
        try {
            if($inputData->is_facility_list)
            {
                foreach ($data as $key=>$value)
                {
                    $row_id = $value->row_id;
                    $col_id = $value->column_id;
                    $field_value = $value->value;

                    if($inputData->facility > 0)
                    {
                        $facility_id = $inputData->facility;
                    }else if(isset($inputData->facilities))
                    {
                        $facility_id = null;
                        $array = (array)$inputData->facilities;
                        if(is_array($array)){
                            foreach ($array as $key=>$value){
                                if($row_id == $key)
                                {
                                    $facility_id = $value;
                                    break;
                                }
                            }

                        }

                    }else
                    {
                        $facility_id = null;
                    }
                    //check if it does not exist
                    if ($row_id !== ""){
                        $items = CasPlanTableItemValue::where('financial_year_id',$fy_id)
                            ->where('admin_hierarchy_id',$admin_hierarchy)
                            ->where('cas_plan_table_column_id',$col_id)
                            ->where('facility_id',$facility_id)
                            ->where('period_id', $period_id)
                            ->where('sort_order',$row_id)
                            ->pluck('id');
                        if(count($items) > 0){
                            //update row
                            $new_data = array('value'=>$field_value);
                            CasPlanTableItemValue::whereIn('id', $items)->update($new_data);
                        }else {
                            //insert new row
                            $new_data = array('cas_plan_table_column_id'=>$col_id,
                                              'facility_id'=>$facility_id,
                                              'sort_order'=>$row_id,
                                              'value'=>$field_value,
                                              'financial_year_id'=>$fy_id,
                                              'admin_hierarchy_id'=>$admin_hierarchy,
                                              'period_id' => $period_id );
                            array_push($data_array, $new_data);
                        }
                    }
                }
            }else
            {
                if($inputData->facility > 0)
                {
                    $facility_id = $inputData->facility;
                }else
                {
                    $facility_id = null;
                }

                foreach ($data as $key=>$value)
                {
                    $row_id = $value->row_id;
                    $col_id = $value->column_id;
                    $field_value = $value->value;
                    //check if it does not exist
                    $items = CasPlanTableItemValue::where('financial_year_id',$fy_id)
                              ->where('admin_hierarchy_id',$admin_hierarchy)
                              ->where('cas_plan_table_column_id', $col_id)
                              ->where('facility_id',$facility_id)
                              ->where('period_id', $period_id)
                              ->where('cas_plan_table_item_id',$row_id)
                              ->pluck('id');
                    if(count($items) > 0){
                        $new_data = array('value'=>$field_value);
                        CasPlanTableItemValue::whereIn('id', $items)->update($new_data);
                    } else {
                        //insert row
                        $new_data = array('cas_plan_table_column_id'=>$col_id,
                                      'cas_plan_table_item_id'=>$row_id,
                                      'value'=>$field_value,
                                      'facility_id'=>$facility_id,
                                      'financial_year_id'=>$fy_id,
                                      'admin_hierarchy_id'=>$admin_hierarchy,
                                      'period_id' => $period_id);
                        array_push($data_array, $new_data);
                    }
                }
            }
            if(count($data_array) > 0){
                CasPlanTableItemValue::insert($data_array);
            }
            $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_VALUE_UPDATED"];
            return response()->json($message, 200);
        }catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function uploadFile()
    {
        $admin_hierarchy = UserServices::getUser()->admin_hierarchy_id;
        //$financialYearId = ConfigurationSetting::getValueByKey('AUTO_FINANCIAL_YEAR_TO_REPORT');
        $cas_plan_table_id = Input::get('plan_table_id');
        $description = Input::get('description')?Input::get('description'):null;
        $facility_id = Input::get('facility');
        $financialYearId = Input::get('financial_year_id');
        $period_id  = (integer)Input::get('period_id')?Input::get('period_id'):null;

        if (Input::hasFile('file')) {
            $destinationPath = 'uploads/comprehensive_plans'; // upload path
            $extension = Input::file('file')->getClientOriginalExtension(); // getting image extension
            $fileName = md5(time()).'.'.$extension; // renaming image
            Input::file('file')->storeAs($destinationPath, $fileName); // uploading file to given path

            if($facility_id == 0)
            {
                $facility_id = null;
            }

            //save data
            $dataExist = DB::table('cas_plan_table_details')
                          ->where('admin_hierarchy_id',$admin_hierarchy)
                          ->where('cas_plan_table_id',$cas_plan_table_id)
                          ->where('facility_id',$facility_id)
                          ->where('period_id', $period_id)
                          ->where('financial_year_id',$financialYearId)
                          ->first();

            if(isset($dataExist->id))
            {
                DB::table('cas_plan_table_details')
                    ->where('id',$dataExist->id)
                    ->update(['url'=>$fileName,
                        'description'=>$description]);

            }else
            {
                DB::table('cas_plan_table_details')
                    ->insert(['admin_hierarchy_id'=>$admin_hierarchy,
                        'cas_plan_table_id'=>$cas_plan_table_id,
                        'financial_year_id'=>$financialYearId,
                        'facility_id'=>$facility_id,
                        'period_id'=>$period_id,
                        'url'=>$fileName,
                        'description'=>$description]);
            }

            $message = ["successMessage" => 'SUCCESSFUL_DOCUMENT_UPLOADED'];
            return response()->json($message, 200);
        } else {
            $message = ["errorMessage" => 'PLEASE_SELECT_FILE'];
            return response()->json($message, 500);
        }
    }

    //function for report
    public function  getItemValues(Request $request)
    {
        //get financial year
        $financial_year_id = $request->financial_year_id;
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $table_id           = $request->table_id;
        $constraints = [];
        //get data
        $dataItemValues = DB::table('cas_plan_table_item_values as v')
            ->join('cas_plan_table_columns as c','v.cas_plan_table_column_id','c.id')
            ->where('c.cas_plan_table_id',$table_id)
            ->where('v.financial_year_id',$financial_year_id)
            ->where('v.admin_hierarchy_id',$admin_hierarchy_id)
            ->select('v.*')
            ->get();
        //get report conditions
        $cond = DB::table('cas_plan_table_report_constraints')
                ->where('cas_plan_table_id', $table_id)
                ->first();
        if(!empty($cond))
        {
            $limit = $cond->limit;
            $order_id = rtrim($cond->order_by,']');
            $order_id = ltrim($order_id,'[');
            $order_id = explode(',',$order_id);
            $constraints['limit'] = $limit;
            $constraints['order_by'] = $order_id;
            $constraints['with_value'] = $cond->with_value;
        }
        //get admin name
        $council = AdminHierarchy::where('id',$admin_hierarchy_id)->first()->name;
        $data = ['itemValues'=>$dataItemValues, 'constraint'=>$constraints,'council'=>$council];
        return response()->json($data);
    }

    //update constant
    public function updateConstant(Request $request)
    {
        $inputData = json_decode($request->getContent());
        $admin_hierarchy = UserServices::getUser()->admin_hierarchy_id;
        $data_array = array();
        $data = $inputData->inputFields;

        try {

                foreach ($data as $key=>$value)
                {
                        $row_id = $value->row_id;
                        $col_id = $value->column_id;
                        $field_value = $value->value;

                        //update if exists
                        $items = DB::table('cas_plan_table_item_constants')
                            ->where('cas_plan_table_column_id',$col_id)
                            ->where('cas_plan_table_item_id',$row_id)
                            ->where('admin_hierarchy_id',$admin_hierarchy)
                            ->pluck('id');
                        $new_data = array('cas_plan_table_column_id'=>$col_id,
                            'cas_plan_table_item_id'=>$row_id,
                            'value'=>$field_value,
                            'admin_hierarchy_id'=>$admin_hierarchy);
                        if(count($items) > 0){
                          //update
                          DB::table('cas_plan_table_item_constants')
                              ->whereIn('id',$items)
                              ->update($new_data);
                          $this->updateConstants($col_id, $row_id,$admin_hierarchy,$field_value);
                        }else {
                          //insert new row
                          DB::table('cas_plan_table_item_constants')->insert($new_data);
                        }
                }

            $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_CONSTANT_VALUE_UPDATED"];
            return response()->json($message, 200);
        }catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function adminHierarchyWithParents($admin_id)
    {
        $id = DB::table('admin_hierarchies')->where('id',$admin_id)->select('id','parent_id')->first();
        $this->adminHierarchyIds[] = $id->id;
        if($id->parent_id != null)
        {
            $this->adminHierarchyWithParents($id->parent_id);
        }
    }

    /**
     * update constant
     */
   public function updateConstants($column_id, $row_id, $admin_hierarchy_id, $value){
       $ids = DB::table('admin_hierarchies')
              ->where('parent_id', $admin_hierarchy_id)
              ->pluck('id');

       DB::table('cas_plan_table_item_values')
              ->where('cas_plan_table_column_id', $column_id)
              ->where('cas_plan_table_item_id', $row_id)
              ->whereIn('admin_hierarchy_id', $ids)
              ->update(array('value'=>$value));
   }
}
