<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\ReferenceDocument;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class DocumentController extends Controller {

    public function download($id) {
        try{
            //PDF file is stored under project/public/download/info.pdf
            $doc = ReferenceDocument::find($id);
            $document_url = $doc->document_url;
            $path = "uploads/reference_documents/".$document_url;
            $file = public_path() . $path;
            //echo $file;exit();
            $headers = array(
                'Content-Type: application/pdf',
            );
            return response()->download($file, time() . "_ReferenceDocument.pdf", $headers);
        } catch (FileNotFoundException $e){
            echo $e->getMessage();
        }
    }
}
