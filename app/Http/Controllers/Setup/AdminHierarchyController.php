<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\UserServices;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\AdminHierarchyLevel;
use App\Models\Setup\AdminHierarchyProject;
use App\Models\Setup\FacilityType;
use App\Models\Setup\Section;
use GuzzleHttp\Client;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

/**
 * @resource AdminHierarchy
 *
 * AdminHierarchyController: This Controller manage admin hierarchies e.g. national, region, district etc
 */
class AdminHierarchyController extends Controller {

    private $adminHierarchies = [];

    public function adminHierarchyLevel($id) {
        array_push($this->adminHierarchies, $id);
        $adminHierarchyId = AdminHierarchy::where('parent_id', $id)->get();
        foreach ($adminHierarchyId as $key => $value) {
            array_push($this->adminHierarchies, $value->id);
            //check if has children
            $count = AdminHierarchy::where('parent_id', $value->id)->count();
            if ($count > 0) {
                $this->adminHierarchyLevel($value->id);
            }
        }
    }

    /**** Pisc types ***/
    public function getPiscType()
    {
        $data = DB::table('pisc_types')
            ->orderBy('name')
            ->get();
        return response()->json(["pisc_type" => $data], 200);
    }
//Load AdminiHierrchies BySector
    public function getBysectorId(Request $request)
    {
        $SectorId = $request->sectorId;
        $adminHierarchy = Section::join('admin_hierarchy_sections as ahs','ahs.section_id','sections.id')
        ->join('admin_hierarchies as ah','ah.id','ahs.admin_hierarchy_id')
        ->where('sections.sector_id',$SectorId)
        ->select('ah.*')->distinct()->get();
        return response()->json(["psc" => $adminHierarchy], 200);
    }

    public function getAllParentCofog () {
        $cofog = DB::table('cofogs as c2')
            ->join('cofogs as c1','c1.id','=','c2.parent_id')
//            ->join('admin_hierarchies as a','a.cofog_id','=','c2.id')
            ->whereNull('c1.parent_id')
            ->select('c2.*')
            ->get();
        return response()->json(["cofog" => $cofog], 200);
    }

    public function getPicsNameFromSp(Request $request) {
       $id = $request->pisc_id;
        $data = AdminHierarchy::where('id', $id)->get();
        return response()->json(["ServiceProviderParent" => $data]);
    }

    /**
     * userHierarchies()
     * This method return user hierarchy and child hierarchies up to hierarchy  level 3
     */
    public function userHierarchies() {
        $flatten = new Flatten();
        $userHiearchies = $flatten->getUserHierarchyTree();
        return response()->json($userHiearchies);

    }

    /**
     * getAdminHierarchyChildren()
     * This method return child admin hierarchies of an admin hierarchy. e.g districts of a specific region
     */
    public function getAdminHierarchyChildren($adminHierarchyId) {

        $hierarchy_position = DB::table('admin_hierarchies as a')->
        join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
        ->select('l.*')->where('a.id',$adminHierarchyId)->first()->hierarchy_position;

        

        $data = DB::table('admin_hierarchies as a')
        ->when($hierarchy_position ==1 , function ($q) use($adminHierarchyId) {
            $query = $q->join('admin_hierarchies as a1','a1.parent_id','a.id')
            ->join('admin_hierarchies as a2','a2.parent_id','a1.id')
            ->join('admin_hierarchy_levels as l', 'a2.admin_hierarchy_level_id', 'l.id')
            ->select('a1.*','l.hierarchy_position','l.name as level')
            ->where('a1.is_active', true)->whereNull('a1.deleted_at')
            ->where('a2.is_active', true)->whereNull('a2.deleted_at')->orderBy('a2.name');
            return $query;
        })
        ->when($hierarchy_position ==2 , function ($q) use($adminHierarchyId) {
            $query = $q->join('admin_hierarchies as a1','a1.parent_id','a.id')
            ->join('admin_hierarchy_levels as l', 'a1.admin_hierarchy_level_id', 'l.id')
            ->select('a1.*','l.hierarchy_position','l.name as level')
            ->where('a1.is_active', true)->whereNull('a1.deleted_at')->orderBy('a1.name');
            return $query;
        })
        ->when($hierarchy_position ==3 , function ($q) use($adminHierarchyId) {
            $query = $q->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
            ->select('a.*','l.hierarchy_position','l.name as level')->orderBy('a.name');
            return $query;
        })->where('a.id', $adminHierarchyId)->where('a.is_active', true)
        ->whereNull('a.deleted_at')->get();
        
        return response()->json(["adminHierarchies" => $data]);
    }

    /**
     * userAdminHierarchy()
     * This method return admin hierarchy of a login user
     */
    public function userAdminHierarchy(Request $request) {
        $data = DB::table('admin_hierarchies as a')->
        join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
            ->where('a.id', UserServices::getUser()->admin_hierarchy_id)
            ->where('a.is_active', true)
            ->select('a.*', 'l.hierarchy_position')->first();
        return response()->json($data);
    }

    public function loadParentById (Request $request) {
    if($request ->parentId) {
        $childAdminHierarchies = DB::table('admin_hierarchies')
        ->where('parent_id','=',$request->parentId)
        ->whereNull('deleted_at')
        ->get();
        return response()->json(["childAdminHierarchies" => $childAdminHierarchies]);
    }

      $data = array();
      if ( $request->childId) {
        $data = DB::table('admin_hierarchies as a')
        ->join('admin_hierarchies as s','s.parent_id','=','a.id')
        ->join('admin_hierarchies as v','a.parent_id','=','v.id')
        ->where('s.parent_id','=',$request->childId)
        ->select('v.*')
        ->orderBy('v.name')
        ->get();
      } 
    
        $adminHierarchies = DB::table('admin_hierarchies as a')
        ->join('admin_hierarchy_levels as l','l.id','=','a.admin_hierarchy_level_id')
        ->where('l.hierarchy_position','=',2)
        ->whereNull('a.deleted_at')
        ->select('a.*')
        ->orderBy('a.name')
        ->get();
        return response()->json(["adminHierarchies" => $adminHierarchies,"Selected" => $data]);
      
    }

    /**
     * getBudgetingChildLevels()
     * This method return user admin hierarchy level and child admin hierarchy levels which have been configured to budget
     */
    public function getBudgetingChildLevels(Request $request) {

        $userPosition = DB::table('admin_hierarchies as a')->
        join('admin_hierarchy_levels as al', 'al.id', 'a.admin_hierarchy_level_id')->
        where('a.id', UserServices::getUser()->admin_hierarchy_id)->first();
        $all = DB::table('admin_hierarchies as a')->
        join('admin_hierarchy_levels as al', 'al.id', 'a.admin_hierarchy_level_id')->
        join('admin_hierarchy_lev_sec_mappings as am', 'am.admin_hierarchy_level_id', 'al.id')->
        where('al.hierarchy_position', '>=', $userPosition->hierarchy_position)->
        where('am.can_budget', true)->
        distinct()->
        select('al.*')->
        get();
        $data = ['adminLevels' => $all];
        return response()->json($data);
    }

    /**
     * globalAdminHierarchies()
     *
     */
    public function globalAdminHierarchies(Request $request) {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $this->adminHierarchyLevel($admin_hierarchy_id);
        $all = AdminHierarchy::whereIn('id', $this->adminHierarchies)->get();
        $data = ["globalAdminHierarchies" => $all];
        return response()->json($data);
    }

    public function piscs() {
        $all = DB::table('admin_hierarchies')
            ->join('admin_hierarchy_levels', 'admin_hierarchy_levels.id', '=', 'admin_hierarchies.admin_hierarchy_level_id')
            ->where('admin_hierarchy_levels.hierarchy_position', 2)
            ->whereNull('admin_hierarchies.deleted_at')
            ->select('admin_hierarchies.id', 'admin_hierarchies.name','admin_hierarchies.code')->distinct()
            ->orderBy('admin_hierarchies.name')
            ->orderBy('admin_hierarchies.code')
            ->get();
        $data = ["piscs" => $all];
        return response()->json($data);
    }

    public function serviceProviderType (Request $request) {
        $type_id = $request->sptype;
        $all = DB::table('facility_types')
        ->where('id',$type_id)
        ->select('name','code')
        ->distinct()->get();
        $data = ["type" => $all];
        return response()->json($data);

    }

    /**
     * councils()
     * This method return Councils(Tanzania specific)i.e level 3 admin hierarchies for Local Government administration hierarchy
     */
    public function councils() {
        $all = DB::table('admin_hierarchies')
            ->join('admin_hierarchy_levels', 'admin_hierarchy_levels.id', '=', 'admin_hierarchies.admin_hierarchy_level_id')
            ->where('admin_hierarchy_levels.hierarchy_position', 3)
            ->whereNull('admin_hierarchies.deleted_at')
            ->select('admin_hierarchies.id', 'admin_hierarchies.name')->distinct()->get();
        $data = ["councils" => $all];
        return response()->json($data);
    }

    /**
     * wards()
     * This method return Wards(Tanzania specific)i.e level 4 admin hierarchies for Local Government administration hierarchy
     */
    public function wards() {
        $all = DB::table('admin_hierarchies')
            ->join('admin_hierarchy_levels', 'admin_hierarchy_levels.id', '=', 'admin_hierarchies.admin_hierarchy_level_id')
            ->where('admin_hierarchy_levels.hierarchy_position', 4)
            ->whereNull('admin_hierarchies.deleted_at')
            ->select('admin_hierarchies.id', 'admin_hierarchies.name')->distinct()->get();
        $data = ["wards" => $all];
        return response()->json($data);
    }

    /**
     * wards()
     * This method return Wards(Tanzania specific)i.e level 5 admin hierarchies for Local Government administration hierarchy
     */
    public function villages() {
        $all = DB::table('admin_hierarchies')
            ->join('admin_hierarchy_levels', 'admin_hierarchy_levels.id', '=', 'admin_hierarchies.admin_hierarchy_level_id')
            ->where('admin_hierarchy_levels.hierarchy_position', 5)
            ->whereNull('admin_hierarchies.deleted_at')
            ->select('admin_hierarchies.id', 'admin_hierarchies.name')->distinct()->get();
        $data = ["villages" => $all];
        return response()->json($data);
    }

    /**
     * index()
     * This method return to most admin hierarchy and its children admin hierarchy
     */
    public function index() {
        $adminHierarchy = AdminHierarchy::with('children')
            ->whereNull('parent_id')->where('is_active', true)->get();
        return response()->json($adminHierarchy);
    }

    /**
     * store()
     * This method create a new admin hierarchy
     */
    public function store(Request $request) {
        $data = json_decode($request->getContent());
        if (isset($data->parent_id))
            $adminHierarchy = AdminHierarchy::where('name', $data->name)->where('parent_id', $data->parent_id)->get()->count();
        else
            $adminHierarchy = AdminHierarchy::where('name', $data->name)->get()->count();

        if ($adminHierarchy == 0) {

            try {
                $adminHierarchy = new AdminHierarchy();
                $adminHierarchy->name = $data->name;
                $adminHierarchy->code = $data->code;
                $adminHierarchy->pisc_type_id = $data->pisc_type_id;
                $adminHierarchy->cofog_id = $data->cofog_id;
                $adminHierarchy->admin_hierarchy_level_id = $data->admin_hierarchy_level_id;
                $adminHierarchy->parent_id = (isset($data->parent_id)) ? $data->parent_id : null;
                $adminHierarchy->sort_order = (isset($data->sort_order)) ? $data->sort_order : null;
//                $adminHierarchy->ward_code = (isset($data->ward_code)) ? $data->ward_code : null;
                $adminHierarchy->is_active = True;
                $adminHierarchy->save();
                //Return language success key and all data
                $adminHierarchies = AdminHierarchy::with('children')->whereNull('parent_id')->get();
                $message = ["successMessage" => "SUCCESSFUL_ADMIN_HIERARCHY_CREATED", "adminHierarchies" => $adminHierarchies];
                return response()->json($message, 200);
            } catch (QueryException $exception) {
                $error_code = $exception->errorInfo[1];
                Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
                $message = ["errorMessage" => "DATABASE_ERROR"];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => "ERROR_ADMIN_HIERARCHY_EXISTS"];
            return response()->json($message, 400);
        }
    }

    /**
     * update()
     * This method update an existing admin hierarchy
     */
    public function update(Request $request) {
        $data = json_decode($request->getContent());
        if (isset($data->parent_id))
            $adminHierarchy = AdminHierarchy::where('name', $data->name)->where('parent_id', $data->parent_id)->where('id', '!=', $data->id)->get()->count();
        else
            $adminHierarchy = AdminHierarchy::where('name', $data->name)->where('id', '!=', $data->id)->get()->count();

        if ($adminHierarchy == 0) {
            try {
                $adminHierarchy = AdminHierarchy::find($data->id);
                $adminHierarchy->name = $data->name;
                $adminHierarchy->code = $data->code;
                $adminHierarchy->pisc_type_id = $data->pisc_type_id;
                $adminHierarchy->cofog_id = $data->cofog_id;
                $adminHierarchy->admin_hierarchy_level_id = $data->admin_hierarchy_level_id;
                $adminHierarchy->parent_id = (isset($data->parent_id)) ? $data->parent_id : null;
                $adminHierarchy->sort_order = (isset($data->sort_order)) ? $data->sort_order : null;
//                $adminHierarchy->ward_code = (isset($data->ward_code)) ? $data->ward_code : null;
                $adminHierarchy->is_active = True;
                $adminHierarchy->save();
                //Return language success key and all data
                $adminHierarchies = AdminHierarchy::with('children')->whereNull('parent_id')->get();
                $message = ["successMessage" => "SUCCESSFUL_ADMIN_HIERARCHY_UPDATED", "adminHierarchies" => $adminHierarchies];
                return response()->json($message, 200);

            } catch (QueryException $exception) {
                $error_code = $exception->errorInfo[1];
                Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
                $message = ["errorMessage" => "DATABASE_ERROR"];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => "ERROR_ADMIN_HIERARCHY_EXISTS"];
            return response()->json($message, 400);
        }
    }

    /**
     * delete()
     * This method delete  an existing admin hierarchy return error message if admin hierarchy to be deleted already in use
     */
    public function delete($id) {
        try {

            
            AdminHierarchy::where('id', $id)->delete();
            $message = ["successMessage" => "SUCCESSFUL_ADMIN_HIERARCHY_DELETED"];
            return response()->json($message, 200);
        } catch (QueryException $e) {
            $message = ["errorMessage" => "ERROR_DELETING_ADMIN_HIERARCHY_DELETED"];
            return response()->json($message, 400);
        }

    }

    /**
     * loadParentAdminHierarchy()
     * This method return parent admin hierarchy level of an admin hierarchy level
     */
    public function loadParentAdminHierarchy($childAdminHierarchyLevelId) {
        $childAdminHierarchyLevel = AdminHierarchyLevel::find($childAdminHierarchyLevelId);
        
        $parentAdminHierarchies = DB::table('admin_hierarchies')
            ->join('admin_hierarchy_levels', 'admin_hierarchy_levels.id', '=', 'admin_hierarchies.admin_hierarchy_level_id')
            ->select('admin_hierarchies.*')
            ->where('hierarchy_position', '=', $childAdminHierarchyLevel->hierarchy_position - 1)
            ->whereNull('admin_hierarchies.deleted_at')
            ->get();
        return response()->json($parentAdminHierarchies, 200);
    }
    
    public function onMinistryloadParentAdminHierarchy ($ministryId) {
        $parentAdminHierarchies = DB::table('admin_hierarchies')
        ->select('admin_hierarchies.*')
        ->where('parent_id', '=', $ministryId)
        ->whereNull('deleted_at')
        ->get();
    return response()->json($parentAdminHierarchies, 200);
    }

    /**
     * getByLevel()
     * This method return admin hierarchies of a specific admin hierarchy level
     */
    public function getByLevel($levelId) {
        $flatten = new Flatten();
        $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        if ($adminHierarchyId != null) {
            $userAdminHierarchies = AdminHierarchy::where('id', $adminHierarchyId)->get();
        } else {
            $userAdminHierarchies = AdminHierarchy::whereNull('parent_id')->get();
        }

        $Ids = $flatten->flattenAdminHierarchyWithChild($userAdminHierarchies);
        $adminHierarchies = DB::table('admin_hierarchies as a')
            ->join('admin_hierarchy_levels as l', 'l.id', '=', 'a.admin_hierarchy_level_id')
            ->select('a.*')
            ->where('a.admin_hierarchy_level_id', '=', $levelId)
            ->whereNull('a.deleted_at')
            ->whereIn('a.id', $Ids)
            ->get();
        return response()->json($adminHierarchies, 200);
    }

    /**
     * allAdminHierarchies()
     *
     */
    public function allAdminHierarchies() {
        $adminHierarchy = AdminHierarchy::with('children')->get();
        return response()->json($adminHierarchy);
    }

    /**
     * upload()
     * This saves uploaded admin hierarchies
     *
     */
    public function upload() {
        ini_set('max_execution_time', 3600);
        $admin_hierarchy_level_id = Input::get('admin_hierarchy_level_id');
        if (Input::hasFile('file')) {
            $path = Input::file('file')->getRealPath();
            $data = Excel::selectSheets('ADMIN_HIERARCHIES')->load($path, function ($reader) {
            })->get();
            if (!empty($data) && $data->count()) {
                $error = array();
                $error_row = array();
                $row = 2;
                foreach ($data as $key => $value) {
                    $name = $value->name;
                    $code = $value->code;
                    $pisc_type = $value->pisc_type;
                    $parent_admin_hierarchy_code = $value->parent_admin_hierarchy_code;
                    if (is_null($name) || is_null($code) || is_null($parent_admin_hierarchy_code)) {
                        array_push($error, $value);
                        array_push($error_row, $row);
                    }
                    $row++;
                }
                $duplicates = array();
                $parentErrors = array();
                foreach ($data as $key => $value) {
                    $exists = AdminHierarchy::where('code', $value->code)->count();
                    if ($exists == 1) {
                        array_push($duplicates, $value->code);
                    } else {
                        $query = AdminHierarchy::where('code', $value->parent_admin_hierarchy_code);
                        if ($query->count() > 0) {
                            $parent = $query->first();
                            $parent_id = $parent->id;
                            $a = new AdminHierarchy();
                            $a->name = $value->name;
                            $pisc_type = $value->pisc_type;
                            $a->code = $value->code;
                            $a->admin_hierarchy_level_id = $admin_hierarchy_level_id;
                            $a->parent_id = $parent_id;
                            $a->ward_code = $value->code;
                            $a->is_active = true;
                            $a->save();
                        } else {
                            array_push($parentErrors, $value->parent_admin_hierarchy_code);
                        }
                    }
                }
                $feedback = ["successMessage" => "DATA_UPLOADED_SUCCESSFULLY", "duplicates" => $duplicates, "parentErrors" => $parentErrors];
                return response()->json($feedback, 200);
            } else {
                $message = ["errorMessage" => 'FILE_SPECIFIED_HAS_NO_DATA'];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => 'PLEASE_SELECT_A_FILE'];
            return response()->json($message, 500);
        }
    }

    /**
     * allAdminHierarchyProjects()
     * This method return admin hierarchies with projects
     */
    public function allAdminHierarchyProjects($id) {
        $all = AdminHierarchyProject::where('admin_hierarchy_id', $id)->with('project')->get();
        $message = ["admin_hierarchy_projects" => $all];
        return response()->json($message);
    }

    /**
     * addProject()
     *
     */
    public function addProject(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $admin_hierarchy_id = $data->admin_hierarchy_id;
            $project_id = $data->project_id;
            $obj = new AdminHierarchyProject();
            $obj->admin_hierarchy_id = $admin_hierarchy_id;
            $obj->project_id = $project_id;
            $obj->created_by = UserServices::getUser()->id;
            $obj->save();
            $all = AdminHierarchyProject::where('admin_hierarchy_id', $data->admin_hierarchy_id)->with('project')->get();
            $message = ["admin_hierarchy_projects" => $all, "successMessage" => "PROJECT_ADDED_SUCCESSFULLY"];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $all = AdminHierarchyProject::where('admin_hierarchy_id', $data->admin_hierarchy_id)->with('project')->get();
            $message = ["errorMessage" => "DATABASE_ERROR", "admin_hierarchy_projects" => $all, "admin_hierarchy_id" => $data->admin_hierarchy_id];
            return response()->json($message, 500);
        }
    }

    /**
     * delete_admin_hierarchy_project()
     *
     */
    public function delete_admin_hierarchy_project($admin_hierarchy_project_id, $admin_hierarchy_id) {
        $object = AdminHierarchyProject::find($admin_hierarchy_project_id);
        $object->delete();
        $all = AdminHierarchyProject::where('admin_hierarchy_id', $admin_hierarchy_id)->with('project')->get();
        $message = ["admin_hierarchy_projects" => $all, "successMessage" => "PROJECT_REMOVED_SUCCESSFULLY"];
        return response()->json($message);
    }

    /**
     * import()
     *
     */
    public function import() {
        ini_set('max_execution_time', 0);
        $admin_hierarchy_link = config('app.admin_hierarchies_api_link');
        try {

            $client = new Client();
            $levels = $client->request('GET', $admin_hierarchy_link . '/levels');
            $levels = $levels->getBody();
            $level_results = json_decode($levels, true);

            foreach ($level_results as $data) {
                $found_level = AdminHierarchyLevel::find($data['id']);
                if (!$found_level) {
                    $level = new AdminHierarchyLevel();
                    $level->id = $data['id'];
                    $level->name = $data['name'];
                    $level->hierarchy_position = $data['position'];
                    $level->sort_order = $data['position'];
                    $level->is_active = true;
                    $level->code = $data['slug'];
                    $level->save();
                } else {
                    continue;
                }
            }
        } catch (\Exception $exception) {
            return response()->json($exception->getMessage());
        }
        //start working on admin areas.
        $perpage = 1000;
        $page = 0;
        $admin_area_count = $client->request('GET', $admin_hierarchy_link . '/admin-area-count');
        $admin_area_count = $admin_area_count->getBody();
        $admin_area_count = json_decode($admin_area_count, true);
        while ($admin_area_count > $perpage * $page) {
            $res = $client->request('GET', $admin_hierarchy_link . "/admin-areas-local?page=$page");
            $page++;
            $results = $res->getBody();
            $results = json_decode($results, true);
            foreach ($results as $data) {
                $admin_area = AdminHierarchy::find($data['id']);
                if (!$admin_area) {
                    $admin = new AdminHierarchy();
                    $admin->id = $data['id'];
                    $admin->name = $data['name'];
                    $admin->code = $data['vote_no'];
                    $admin->admin_hierarchy_level_id = $data['level_id'];
                    $admin->sort_order = $data['level_id'];
                    $admin->is_active = true;
                    $admin->parent_id = (isset($data['admin_area_parent_id'])) ? $data['admin_area_parent_id'] : null;
                    $admin->save();
                } else {
                    continue;
                }
            }
        }
        //end of chunking admin areas.
        $all = AdminHierarchy::with('children')->whereNull('parent_id')->get();
        $message = ["errorMessage" => "DATABASE_ERROR", "adminHierarchies" => $all];
        return response()->json($message, 500);
    }

    /**
     * getAdminHierarchiesByFacilityTypeID()
     * This method return admin hierarchies by facility type
     */
    public function getAdminHierarchiesByFacilityTypeID($facilityTypeID) {
        $flatten = new Flatten();
        $userAdminHierarchies = AdminHierarchy::where('id', UserServices::getUser()->admin_hierarchy_id)->get();
        $Ids = $flatten->flattenAdminHierarchyWithChild($userAdminHierarchies);
        $facilityTypeAdmHierarchyLevelID = FacilityType::where('id', $facilityTypeID)->first()->admin_hierarchy_level_id;
        $adminHierarchies = DB::table('admin_hierarchies')
            ->where('admin_hierarchy_level_id', $facilityTypeAdmHierarchyLevelID)
            ->whereIn('id', $Ids)->get();
        return response()->json($adminHierarchies);

    }

    /**
     * regions()
     * This method return region (Tanzania specific)
     */
    public function regions() {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $admin_hierarchy = AdminHierarchy::find($admin_hierarchy_id);
        $level_id = $admin_hierarchy->admin_hierarchy_level_id;
        $level = AdminHierarchyLevel::find($level_id);
        $position = 1;
        $all = null;
        //PORALG
        if ($position == 1) {
            $all = AdminHierarchy::whereNull('parent_id')->get();
        }
        //REGION
        if ($position == 2) {
            $all = AdminHierarchy::where('admin_hierarchy_level_id', 2)->get();
        }
        //COUNCIL
        if ($position == 3) {
            $all = AdminHierarchy::where('id', $admin_hierarchy_id)->get();
        }
        $data = ["regions" => $all];
        return response()->json($data);
    }

    /**
     * lgas()
     * This method return council of a specific region (Tanzania specific)
     */
    public function lgas($region_id) {
        $all = AdminHierarchy::where('parent_id', $region_id)->get();
        $data = ["lgas" => $all];
        return response()->json($data);
    }

    /**
     * admin_hierarchies()
     *
     */
    public function admin_hierarchies() {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $admin_hierarchy = AdminHierarchy::find($admin_hierarchy_id);
        $level_id = $admin_hierarchy->admin_hierarchy_level_id;
        $parent_id = $admin_hierarchy->parent_id;
        $level = AdminHierarchyLevel::find($level_id);
        $position = $level->hierarchy_position;
        $all = null;
        //PORALG
        if ($position == 1) {
            $all = AdminHierarchy::where('admin_hierarchy_level_id', 2)->where('is_active', true)->get();
        }
        //REGION
        if ($position == 3) {
            $all = AdminHierarchy::where('parent_id', $parent_id)->where('is_active', true)->get();
        }
        //COUNCIL
        if ($position == 4) {
            $all = AdminHierarchy::find($admin_hierarchy_id);
        }
        $data = ["admin_hierarchies" => $all, "admin_hierarchy" => $admin_hierarchy->name];
        return response()->json($data);
    }

    /**
     * userRegions()
     * This method return regions i.e level 2 admin hierarchies for Tanzania
     */
    public function userRegions() {
        $all = DB::table('admin_hierarchies as a')
            ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
            ->where('l.hierarchy_position', 4)
            ->select('a.id', 'a.name')
            ->orderBy('a.name')
            ->get();
        $data = ["regions" => $all];
        return response()->json($data);
    }

    /**
     * userCouncils()
     * This method return councils i.e level 3 admin hierarchies for Tanzania
     */
    public function userCouncils() {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $all = DB::table('admin_hierarchies as a')
            ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
            ->where('l.hierarchy_position', 3)
            ->where('a.parent_id', $admin_hierarchy_id)
            ->select('a.id', 'a.name')
            ->get();
        $data = ["councils" => $all];
        return response()->json($data);
    }


    public function councilWards(Request $request) {
        $admin_hierarchy_id = $request->council_id;
        $all = AdminHierarchy::where('parent_id', $admin_hierarchy_id)->where('is_active', true)->get();
        $data = ["wards" => $all];
        return response()->json($data);
    }

    public function wardVillages(Request $request) {
        $admin_hierarchy_id = $request->ward_id;
        $all = AdminHierarchy::where('parent_id', $admin_hierarchy_id)
            ->where('admin_hierarchy_level_id', 11)
            ->where('is_active', true)
            ->distinct()
            ->orderBy("name", "asc")
            ->get();
        $data = ["villages" => $all];
        return response()->json($data);
    }

    public function parentAdminHierarchy(Request $request) {
        $admin_hierarchy = AdminHierarchy::find($request->parent_id);
        return response()->json(['parent' => $admin_hierarchy]);
    }

    public function levelCouncils() {
        return response()->json(['councils' => $this->onlyCouncils()], 200);
    }

    private function onlyCouncils() {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $data_array = array();
        $admin_position = DB::table('admin_hierarchies as a')
            ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
            ->where('a.id', $admin_hierarchy_id)
            ->select('l.hierarchy_position')
            ->first();

        //regional level
        if ($admin_position->hierarchy_position == 2) {
            $admin_hierarchies = DB::table('admin_hierarchies as a')
                ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
                ->where('a.parent_id', $admin_hierarchy_id)
                ->whereNotNull('a.parent_id')
                ->select('a.id', 'a.name', 'a.code', 'a.admin_hierarchy_level_id', 'a.parent_id', 'l.hierarchy_position')
                ->distinct()
                ->orderBy('a.name', 'ASC')
                ->get();
        } else {
            //cost center and national
            $admin_hierarchies = DB::table('admin_hierarchies as a')
                ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
                ->where('a.parent_id', $admin_hierarchy_id)
                ->orWhere('a.id', $admin_hierarchy_id)
                ->whereNotNull('a.parent_id')
                ->select('a.id', 'a.name', 'a.code', 'a.admin_hierarchy_level_id', 'a.parent_id', 'l.hierarchy_position')
                ->distinct()
                ->orderBy('a.name', 'ASC')
                ->get();
        }

        foreach ($admin_hierarchies as $key => $value) {
            if ($value->hierarchy_position == 4) {
                array_push($data_array, $value);
            } else if ($value->hierarchy_position == 3) {
                //get lga's
                $admin_hierarchy = DB::table('admin_hierarchies as a')
                    ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
                    ->where('a.parent_id', $value->id)
                    ->select('a.id', 'a.name', 'a.code','a.pisc_type_id', 'a.admin_hierarchy_level_id', 'a.parent_id')
                    ->orderBy('a.name', 'ASC')
                    ->get();
                foreach ($admin_hierarchy as $key2 => $value2) {
                    array_push($data_array, $value2);
                }
            }
        }

        return $data_array;
    }

    public function search(Request $request) {
        $searchString = $request->searchText;
        $items = AdminHierarchy::where('name', 'ILIKE', '%' . $searchString . '%')
            ->orWhere('code', 'ILIKE', '%' . $searchString . '%')
            ->orderBy('name', 'asc')->get();
        return response()->json(["items" => $items], Response::HTTP_OK);
    }

    public function otrPisc(Request $request){
        $items =   AdminHierarchy::where('admin_hierarchy_level_id',4)
            ->where('deleted_at', null)
            ->where('is_active', true)
            ->orderBy('name', 'asc')->get(); 
            return response()->json(["councils" => $items], Response::HTTP_OK);
    }
}
