<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\ExpenditureCentre;
use App\Models\Setup\ExpenditureCentreGroup;
use App\Models\Setup\Section;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class ExpenditureCentreGroupController extends Controller {
    public function index() {
        $all = ExpenditureCentreGroup::with('fund_source', 'section', 'reference_document')->where('active',true)->orderBy('created_at', 'asc')->get();
        return response()->json($all);
    }

    public function fetchAll() {
        $all = ExpenditureCentreGroup::with('fund_source', 'section', 'reference_document')->where('active',true)->orderBy('created_at', 'asc')->get();
        return response()->json(["expenditureCentreGroups" => $all]);
    }

    public function getAllPaginated($perPage) {
        return ExpenditureCentreGroup::with('fund_source', 'section', 'reference_document')->orderBy('created_at', 'asc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $obj = new ExpenditureCentreGroup();
            $obj->name = $data->name;
            $obj->code = $data->code;
            $obj->type = (isset($data->type)) ? $data->type : null;
            $obj->fund_source_id = (isset($data->fund_source_id)) ? $data->fund_source_id : null;
            $obj->reference_document_id = (isset($data->reference_document_id)) ? $data->reference_document_id : null;
            $obj->section_id = (isset($data->section_id)) ? $data->section_id : null;
            $obj->active = true;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_EXPENDITURE_CENTRE_GROUP_CREATED", "expenditureCentreGroups" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try {
            $obj = ExpenditureCentreGroup::find($data->id);
            $obj->name = $data->name;
            $obj->code = $data->code;
            $obj->type = (isset($data->type)) ? $data->type : null;
            $obj->fund_source_id = (isset($data->fund_source_id)) ? $data->fund_source_id : null;
            $obj->reference_document_id = (isset($data->reference_document_id)) ? $data->reference_document_id : null;
            $obj->section_id = (isset($data->section_id)) ? $data->section_id : null;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_EXPENDITURE_CENTRE_GROUP_UPDATED", "expenditureCentreGroups" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = ExpenditureCentreGroup::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_EXPENDITURE_CENTRE_GROUP_DELETED", "expenditureCentreGroups" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = ExpenditureCentreGroup::with('fund_source', 'section', 'reference_document')->orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        ExpenditureCentreGroup::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "EXPENDITURE_CENTRE_GROUP_RESTORED", "trashedExpenditureCentreGroups" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        ExpenditureCentreGroup::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "EXPENDITURE_CENTRE_GROUP_DELETED_PERMANENTLY", "trashedExpenditureCentreGroups" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = ExpenditureCentreGroup::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EXPENDITURE_CENTRE_GROUP_DELETED_PERMANENTLY", "trashedExpenditureCentreGroups" => $all];
        return response()->json($message, 200);
    }

    public function toggleActive(Request $request) {
        $data = json_decode($request->getContent());
        $object = ExpenditureCentreGroup::find($data->id);
        $object->active = $data->active;
        $object->save();
        $all = ExpenditureCentreGroup::orderby('created_at')->paginate($request->perPage);
        if ($data->active == false) {
            $feedback = ["action" => "DEACTIVATED", "alertType" => "warning", "expenditureCentreGroups" => $all];
        } else {
            $feedback = ["action" => "ACTIVATED", "alertType" => "success", "expenditureCentreGroups" => $all];
        }
        return response()->json($feedback, 200);
    }

    public function groupExpenditureCentres(Request $request) {
        $group_id = $request->group_id;
        $all = ExpenditureCentre::with('link_spec', 'expenditure_centre_group', 'section', 'parent','activity_category','lga_level')->where('expenditure_centre_group_id',$group_id)->orderBy('created_at', 'desc')->get();
        return response()->json(["expenditureCentres" => $all], 200);
    }
    public function groupCostCentres(Request $request) {
        $section_id = $request->section_id;
        $all = Section::where('parent_id',$section_id)->orderBy('created_at', 'desc')->get();
        return response()->json(["sections" => $all], 200);
    }
    public function loadExpenditureGroupCostCentres(Request $request) {
        $group_id = $request->group_id;
        $group = ExpenditureCentreGroup::find($group_id);
        $section_id = $group->section_id;
        $all = Section::where('parent_id',$section_id)->orderBy('created_at', 'desc')->get();
        return response()->json(["sections" => $all], 200);
    }
}
