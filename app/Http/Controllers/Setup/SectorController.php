<?php

namespace App\Http\Controllers\Setup;

use App\DTOs\SetupDTOs\SectionDTO;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\UserServices;
use App\Models\Setup\Section;
use App\Models\Setup\Sector;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

/**
 * @resource Sector
 *
 * SectorController: This controller manage sectors
 */
class SectorController extends Controller
{

    /**
     * index()
     *
     * This method return all active sectors as array
     * @response {
     * data[]
     * }
     *
     */
    public function index()
    {
        $sectors = Sector::where('is_active', true)->get();
        return response()->json($sectors);
    }

    /**
     * fetchAll()
     * return all sectors as object with sector arrays
     * @response {
     * data[]
     * }
     */
    public function fetchAll()
    {
        $sectors = Sector::where('is_active', true)->get();
        return response()->json(["sectors" => $sectors], 200);
    }

    public function sections(Request $request)
    {
        $section_id = UserServices::getUser()->section_id;
        $section = Section::find($section_id);
        $section_level_id = $section->section_level_id;
        $sector_id = $request->sector_id;
        $all = Section::where('sector_id', $sector_id)
            ->where('section_level_id', $section_level_id)
            ->where('is_active', true)->get();
        return response()->json(["sections" => $all], 200);
    }


    public function userSectors()
    {
        $user_id = UserServices::getUser()->id;
        $user = DB::table("users")
            ->where("id",$user_id)
            ->select("users.section_id")
            ->first();
        if(!is_null($user->section_id)){
            $section_id = UserServices::getUser()->section_id;
            $section = SectionDTO::find($section_id);
            $sector_id = $section->sector_id;
            $sectors = Sector::where('id',$sector_id)->where('is_active', true)->get();
            return response()->json(["sectors" => $sectors], 200);
        } else {
            $sectors = Sector::where('is_active', true)->get();
            return response()->json(["sectors" => $sectors], 200);
        }
    }

    /**
     * indexFilter()
     * This function return user sectors
     */
    public function indexFilter()
    {
        try {
            $section_id = UserServices::getUser();
            $sectors = null;
            if ($section_id != null) {
                $section = Section::where('id', $section_id)->get();
                $flatten = new Flatten();
                $sectorIds = $flatten->flattenSectionWithChildrenGetSectors($section);
                $sectors = Sector::whereIn('id', $sectorIds)->where('is_active', true)->get();
            } else {
                $sectors = Sector::where('is_active', true)->get();
            }
            return response()->json($sectors);
        } catch (QueryException $exception) {
            $message = ["errorMessage" => 'FAILED_TO_LOAD_SECTORS_FOR_CURRENT_USER'];
            return response()->json($message, 400);

        }
    }

    /**
     * indexPagination()
     * This method return sectors paginated
     */
    public function indexPagination(Request $request)
    {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function getAllPaginated($perPage)
    {
        return Sector::paginate($perPage);
    }

    /**
     * store()
     * This method create new sector
     */
    public function store(Request $request)
    {
        try {
            $data = json_decode($request->getContent());
            $sector = new Sector();
            $sector->name = $data->name;
            $sector->code = $data->code;
            $sector->description = $data->description;
            $sector->created_by = UserServices::getUser()->id;
            $sector->created_at = date('Y-m-d H:i:s');
            $sector->is_active = true;
            $sector->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_SECTOR_CREATED", "sectors" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_FINANCIAL_YEAR_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

    /**
     * update()
     * This method update existing sector
     */
    public function update(Request $request)
    {
        try {
            $data = json_decode($request->getContent());
            $sector = Sector::find($data->id);
            $sector->name = $data->name;
            $sector->code = $data->code;
            $sector->description = $data->description;
            $sector->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_SECTOR_UPDATED", "sectors" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_SECTOR_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

    /**
     * delete()
     * This method delete existing sector
     */
    public function delete($id)
    {
        $sector = Sector::find($id);
        $sector->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFULLY_SECTOR_DELETED", "sectors" => $all];
        return response()->json($message, 200);
    }

    /**
     * planning_units()
     * This method return sections by sector
     */
    public function planning_units($sector_id)
    {
        $planning_units = Section::where('sector_id', $sector_id)->distinct()->get();
        $data = ['planning_units' => $planning_units];
        return response()->json($data, 200);
    }

    public function departments(Request $request)
    {
        $sector_id = $request->sector_id;
        $departments = SectionDTO::where('sector_id', $sector_id)
            ->where('section_level_id', 3)
            ->get();
        $data = ['departments' => $departments];
        return response()->json($data, 200);
    }

    public function departmentCostCentres(Request $request)
    {
        $department_id = $request->department_id;
        $all = SectionDTO::where("parent_id", $department_id)->where("section_level_id", 4)->get();
        $data = ['sections' => $all];
        return response()->json($data, 200);
    }

    public function sectorCostCentres(Request $request)
    {
        $sectorId = $request->sector_id;
        $all = DB::select("select id,name,code from sections where sector_id = $sectorId and is_active = true and section_level_id = 4");
        $data = ['sections' => $all];
        return response()->json($data, 200);
    }
}
