<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\CasPlanTablesSelectOption;
use Illuminate\Support\Facades\DB;

class CasPlanTablesSelectOptionController extends Controller
{
    public function index(){
        $options = CasPlanTablesSelectOption::whereNull('parent_id')->get();
        return response()->json($options);
    }

    public function getOptions($select_group)
    {

        $options = explode(',',$select_group);

        $all = CasPlanTablesSelectOption::whereIn('parent_id',$options)->select('id','name','code','parent_id')->get();
        return response()->json($all);
    }

    //get facilities
    public function  getFacilityList()
    {
        //get sector
        $section_id = UserServices::getUser()->section_id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;

        //get sector
        $sector = DB::table('sections')
            ->where('id',$section_id)
            ->select('sector_id')
            ->first()->sector_id;
        //get facilities
        $facilities = DB::table('facilities as f')
            ->join('facility_types as ft','f.facility_type_id','ft.id')
            ->join('sections as s','ft.section_id','s.id')
            ->join('admin_hierarchies as a','f.admin_hierarchy_id','a.id')
            ->join('admin_hierarchy_levels as al','a.admin_hierarchy_level_id','al.id')
            ->where('s.sector_id',$sector)
            ->where('f.is_active', true)
            ->whereIn('f.admin_hierarchy_id',function ($query) use ($admin_hierarchy_id){
                $query->select('id')->from('admin_hierarchies')->where('parent_id',$admin_hierarchy_id);
            })->select('f.id','f.name','f.facility_code as code','ft.code as facility_type','al.code as admin_hierarchy_code','a.name as admin_hierarchy_name')
            ->get();
        return response()->json($facilities);
    }

    //get Facility Count
    public function getFacilityCount(){
        //get sector
        $section_id = UserServices::getUser()->section_id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;

        //get sector
        $sector = DB::table('sections')
            ->where('id',$section_id)
            ->select('sector_id')
            ->first()->sector_id;
        //get facilities
        $facilities = DB::table('facilities as f')
            ->join('facility_types as ft','f.facility_type_id','ft.id')
            ->join('sections as s','ft.section_id','s.id')
            ->join('admin_hierarchies as a','f.admin_hierarchy_id','a.id')
            ->join('admin_hierarchy_levels as al','a.admin_hierarchy_level_id','al.id')
            ->where('s.sector_id',$sector)
            ->where('f.is_active', true)
            ->whereIn('f.admin_hierarchy_id',function ($query) use ($admin_hierarchy_id){
                $query->select('id')->from('admin_hierarchies')->where('parent_id',$admin_hierarchy_id);
            })->count();
        return response()->json(['count'=>$facilities],200);    
    }

    //facility by council
    public function  getFacilityByCouncil($admin_hierarchy_id)
    {
        //get sector
        $section_id = UserServices::getUser()->section_id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        //get sector
        $sector = DB::table('sections')
          ->where('id',$section_id)
           ->select('sector_id')
           ->first()->sector_id;

        //get facilities
        $facilities = DB::table('facilities as f')
            ->join('facility_types as ft','f.facility_type_id','ft.id')
            ->join('sections as s','ft.section_id','s.id')
            ->join('admin_hierarchies as a','f.admin_hierarchy_id','a.id')
            ->join('admin_hierarchy_levels as al','a.admin_hierarchy_level_id','al.id')
            ->where('s.sector_id',$sector)
            ->where('f.admin_hierarchy_id',$admin_hierarchy_id)
            ->select('f.id','f.name','f.facility_code as code','ft.code as facility_type','al.code as admin_hierarchy_code','a.name as admin_hierarchy_name')
            ->get();
        return response()->json($facilities);
    }
}
