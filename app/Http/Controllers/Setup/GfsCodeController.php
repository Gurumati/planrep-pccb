<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Planning\ActivityFacilityFundSourceInput;
use App\Models\Setup\AccountType;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\GfsCode;
use App\Models\Setup\GfsCodeCategory;
use App\Models\Setup\GfsCodeSection;
use App\Models\Setup\ProcurementType;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class GfsCodeController extends Controller
{

    public function index()
    {
        $all = GfsCode::with('gfs_code_sub_category', 'accountType', 'fund_source') // add procurement_type relationship here too
        ->where('is_active', true)->get();
        return response()->json($all);
    }

    public function indexExpenditure()
    {
        $all = GfsCode::with('gfs_code_sub_category', 'accountType', 'fund_source')
            ->where('is_active', true)
            ->where('account_type_id', 2)
            ->orderBy("code", "asc")
            ->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage)
    {
        return GfsCode::with('gfs_code_sub_category', 'gfs_code_sub_category.gfs_code_category', 'accountType', 'fund_source')
            ->where('is_active', true)->orderBy('created_at', 'desc')->paginate($perPage);

    }

    public function paginateIndex(Request $request)
    {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function ifrsGfsMapping(Request $request)
    {
        $perpage = $request->perPage ? $request->perPage : 10;
        $data = DB::table('ifrsgfs_mapping as im')
            ->join('gfs_codes as gc', 'gc.id', 'im.gfs_code_id')
            ->join('gfs_code_sub_categories as gsc', 'gsc.id', 'im.gfs_code_sub_category_id')
            ->select('gsc.name as sub_cat', 'gsc.id as sub_category_id', 'gc.id as gfs_code_id', 'gc.name as gfs_code_name', 'im.id')
            ->paginate($perpage);
        return response()->json(['data' => $data], 200);
    }

    public function storeIfrs(Request $request)
    {
        foreach ($request->gfsCodeId as $item) {
            DB::table('ifrsgfs_mapping')->insert([
                'gfs_code_sub_category_id' => $request->gfs_code_sub_category_id,
                'gfs_code_id' => $item,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
        return response()->json(['items' => $item, "successMessage" => "Data Saved successfully"], 201);
    }

//    public function updateifrs(Request $request)
    //    {
    //        foreach ($request->gfsCodeId as $item){
    //            DB::table('ifrsgfs_mapping')->update([
    //                'gfs_code_sub_category_id' => $request->gfs_code_sub_category_id,
    //                'gfs_code_id' => $item,
    //                'created_at' => Carbon::now(),
    //                'updated_at' => Carbon::now()
    //            ]);
    //        }
    //        return response()->json(['successMessage' =>'Data updated successfully'],201);
    //    }

    public function deletemapping(Request $request)
    {
        $gfscodesubcateId = $request->gfs_code_sub_category_id;
        $id = $request->id;
        $row = IfrsGfsCodeMapping::find($id);
        $row->delete();
        $item = IfrsGfsCodeMapping::with('gfs_code')->where('gfs_code_sub_category_id', $gfscodesubcateId)->get();
        return response()->json(['items' => $item, "successMessage" => "Data Deleted successfully"]);
    }

    public function expenditureGfsCodes()
    {
        $all = GfsCode::with('gfs_code_sub_category', 'accountType', 'fund_source')->
        where(function ($q) {
            $q->where('code', 'like', '020%')->
            orWhere('code', 'like', '2%')->
            orWhere('code', 'like', '3%')->
            orWhere('code', 'like', '030%')->
            orWhere('code', 'like', '040%')->
            orWhere('code', 'like', '4%');
        })->where('is_active', true)
            ->select('id', 'name', 'code', 'description', 'gfs_code_sub_category_id')
            ->orderBy("code", "asc")
            ->get();

        return response()->json($all);
    }

    public function revenueGfsCodes()
    {
        $revenueGfsAccountType = ConfigurationSetting::where('key', 'REVENUE_GFS_ACCOUNT_TYPE')->first();
        return response()->json(GfsCode::revenueGfsCodes($revenueGfsAccountType));
    }

    public function revenueByFundSourceVersion($fundSourceVersionId, $financialYearId)
    {
        $revenueGfsAccountType = ConfigurationSetting::where('key', 'REVENUE_GFS_ACCOUNT_TYPE')->first();
        return response()->json(['gfsCodes' => GfsCode::revenueByFundSourceVersion($fundSourceVersionId, $financialYearId, $revenueGfsAccountType)]);
    }

    public function onlyRevenueOnes()
    {
        $revenueGfsAccountType = ConfigurationSetting::where('key', 'REVENUE_GFS_ACCOUNT_TYPE')->first();
        if ($revenueGfsAccountType != null) {
            $all = GfsCode::with('gfs_code_sub_category', 'accountType', 'fund_source')->
            whereHas('accountType', function ($q) use ($revenueGfsAccountType) {
                $q->where('id', (integer)$revenueGfsAccountType->value);
            })->
            whereNull('fund_source_id')->where('is_active', true)->select('id', 'name', 'code', 'description')->get();
        } else {
            $all = GfsCode::with('gfs_code_sub_category', 'accountType', 'fund_source')->
            where('code', 'like', '010%')->whereNull('fund_source_id')->where('is_active', true)->select('id', 'name', 'code', 'description')->get();
        }

        return response()->json(["gfs_codes" => $all], 200);
    }

    public function store(Request $request)
    {
        $userID = UserServices::getUser()->id;
        $data = json_decode($request->getContent());
        try {
            $gfsCode = new GfsCode();
            $gfsCode->name = $data->name;
            $gfsCode->code = $data->code;
            $gfsCode->account_type_id = $data->account_type_id;
            $gfsCode->gfs_code_sub_category_id = $data->gfs_code_sub_category_id;
            //$gfsCode->fund_source_id = (isset($data->fund_source_id)) ? $data->fund_source_id : null;
            $gfsCode->is_procurement = isset($data->is_procurement) ? $data->is_procurement : false;
            $gfsCode->is_active = true;
            $gfsCode->created_by = $userID;
            $gfsCode->sort_order = $data->sort_order;
            $gfsCode->procurement_type_id = isset($data->procurement_type_id) ? $data->procurement_type_id : NULL; // Govella
            $gfsCode->description = isset($data->description) ? $data->description : '';
            $gfsCode->save();
            $id = $gfsCode->id;

            $g = GfsCode::with("account_type")->where("id", $id)->first();

            $item['id'] = $id;
            $item['code'] = $g->code;
            $item['description'] = $g->name;
            switch ($g->account_type->code) {
                case '01':
                    $item['category'] = 'REV';
                    break;
                    break;
                case '03':
                    $item['category'] = 'EXP';
                    break;
                default:
                    $item['category'] = 'REV';
            }
            $item['deletedAt'] = null;

            try {
                $this->send($item, "GFSCODES");
            } catch (\Exception $exception) {
                Log::error($exception);
            }

            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_GFS_CODE_CREATED", "gfsCodes" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request)
    {
        $userID = UserServices::getUser()->id;
        $data = json_decode($request->getContent());
        try {
            $gfsCode = GfsCode::find($data->id);
            $gfsCode->name = $data->name;
            $gfsCode->code = $data->code;
            $gfsCode->description = isset($data->description) ? $data->description : NULL;
            $gfsCode->account_type_id = $data->account_type_id;
            $gfsCode->gfs_code_sub_category_id = $data->gfs_code_sub_category_id;
//           $gfsCode->fund_source_id = (isset($data->fund_source_id)) ? $data->fund_source_id : null;
            $gfsCode->is_procurement = isset($data->is_procurement) ? $data->is_procurement : false;
            $gfsCode->procurement_type_id = isset($data->procurement_type_id) ? $data->procurement_type_id : null;
            $gfsCode->created_by = $userID;
            $gfsCode->sort_order = $data->sort_order;
            $gfsCode->save();

            $g = GfsCode::with("account_type")->where("id", $data->id)->first();

            $item['id'] = $data->id;
            $item['code'] = $g->code;
            $item['description'] = $g->name;
            switch ($g->account_type->code) {
                case '01':
                    $item['category'] = 'REV';
                    break;
                    break;
                case '03':
                    $item['category'] = 'EXP';
                    break;
                default:
                    $item['category'] = 'REV';
            }
            $item['deletedAt'] = null;
            try {
                $this->send($item, "GFSCODES");
            } catch (\Exception $exception) {
                Log::error($exception);
            }
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_GFS_CODE_UPDATED", "gfsCodes" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            //return response()->json($message, 500);
            return $exception;
        }
    }

    public function delete($id)
    {
        $gfsCode = GfsCode::find($id);
        $gfsCode->delete();
        $g = GfsCode::with("account_type")->where("id", $id)->first();
        $item['id'] = $g->id;
        $item['code'] = $g->code;
        $item['description'] = $g->name;
        $item['deleted_at'] = $g->deleted_at;
        switch ($g->account_type->code) {
            case '01':
                $item['category'] = 'REV';
                break;
                break;
            case '03':
                $item['category'] = 'EXP';
                break;
            default:
                $item['category'] = 'REV';
        }
        $item['deletedAt'] = $gfsCode->deleted_at;
        try {
            $this->send($item, "GFSCODES");
        } catch (\Exception $exception) {
            Log::error($exception);
        }
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFULLY_GFS_CODE_DELETED", "gfsCodes" => $all];
        return response()->json($message, 200);
    }

    public function loadAccountTypes()
    {
        $accountTypes = AccountType::all();
        return response()->json($accountTypes);
    }

    public function loadGfsCodeCategories()
    {
        $gfsCodeCategories = GfsCodeCategory::all();
        return response()->json($gfsCodeCategories);
    }

    // Govella
    public function loadProcurementTypes()
    {
        $procurement_types = ProcurementType::where('is_active', true)->get();
        return response()->json($procurement_types);
    }

    public function activateGfsCode(Request $request)
    {
        $data = json_decode($request->getContent());
        $gfsCode = GfsCode::find($data->id);
        $gfsCode->is_active = $data->is_active;
        $gfsCode->save();
        $all = $this->getAllPaginated($request->perPage);
        if ($gfsCode->is_active) {
            $message = ["activateMessage" => "SUCCESSFUL_GFS_CODE_ACTIVATED", "gfsCodes" => $all];
        } else {
            $message = ["activateMessage" => "SUCCESSFUL_GFS_CODE_DEACTIVATED", "gfsCodes" => $all];
        }
        return response()->json($message, 200);
    }

    public function activateIsProcurement(Request $request)
    {
        $data = json_decode($request->getContent());
        $gfsCode = GfsCode::find($data->id);
//        $gfsCodeSubCategory = GfsCodeSubCategory::find($gfsCode->gfs_code_sub_category_id);  // Govella: I did this because when updating is_procurement in gfs_codes we wanted to effect the change in is_procurement in gfs_code_sub_categories too.
        $gfsCode->is_procurement = $data->is_procurement;
//        $gfsCodeSubCategory->is_procurement = $data->is_procurement;
        $gfsCode->save();
//        $gfsCodeSubCategory->save(); // Govella   NOW WE DON'T NEED TO CHANGE gfs_code_sub_category when we change gfs_code
        $all = $this->getAllPaginated($request->perPage);
        if ($gfsCode->is_procurement) {
            $message = ["activateMessageProcurement" => "SUCCESSFUL_GFS_CODE_SET_PROCUREMENT", "gfsCodes" => $all];
        } else {
            $message = ["activateMessageProcurement" => "SUCCESSFUL_GFS_CODE_UNSET_PROCUREMENT", "gfsCodes" => $all];
        }
        return response()->json($message, 200);
    }

    public function upload()
    {
        $account_type_id = Input::get('account_type_id');
        $gfs_code_sub_category_id = Input::get('gfs_code_sub_category_id');
        //$fund_source_id = Input::get('fund_source_id') ? Input::get('fund_source_id') : null;
        if (Input::hasFile('file')) {
            $path = Input::file('file')->getRealPath();
            $data = Excel::selectSheets('GFS_CODES')->load($path, function ($reader) {
            })->get();
            if (!empty($data) && $data->count()) {
                $error = array();
                $error_row = array();
                $row = 2;
                foreach ($data as $key => $value) {
                    $name = $value->name;
                    $code = $value->code;
                    if (is_null($name) || is_null($code)) {
                        array_push($error, $value);
                        array_push($error_row, $row);
                    }
                    $row++;
                }
                if (count($error) > 0) {
                    $all = $this->getAllPaginated(Input::get('perPage'));
                    $feedback = ["errorMessage" => 'DIRTY DATA', "errorData" => $error, "errorRows" => $error_row, "gfsCodes" => $all];
                    return response()->json($feedback, 500);
                } else {
                    foreach ($data as $key => $value) {
                        $exists = GfsCode::where('code', $value->code)->count();
                        if ($exists == 1) {
                            continue;
                        }
                        $f = new GfsCode();
                        $f->name = $value->name;
                        $f->code = $value->code;
                        $f->description = $value->name;
                        $f->account_type_id = $account_type_id;
                        $f->gfs_code_sub_category_id = $gfs_code_sub_category_id;
                        //$f->fund_source_id = $fund_source_id;
                        $f->is_procurement = false;
                        $f->is_active = true;
                        $f->save();

                        $id = $f->id;
                        $g = GfsCode::with("account_type")->where("id", $id)->first();
                        $item['id'] = $id;
                        $item['code'] = $value->code;
                        $item['description'] = $value->name;
                        switch ($g->account_type->code) {
                            case '01':
                                $item['category'] = 'REV';
                                break;
                                break;
                            case '03':
                                $item['category'] = 'EXP';
                                break;
                            default:
                                $item['category'] = 'REV';
                        }
                        $item['deletedAt'] = null;
                        try {
                            $this->send($item, "GFSCODES");
                        } catch (\Exception $exception) {
                            Log::error($exception);
                        }
                    }
                    $all = $this->getAllPaginated(Input::get('perPage'));
                    $message = ["successMessage" => "GFS_CODES_UPLOADED_SUCCESSFULLY", "gfsCodes" => $all];
                    return response()->json($message, 200);
                }
            } else {
                $message = ["errorMessage" => 'FILE_SPECIFIED_HAS_NO_DATA'];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => 'PLEASE_SELECT_A_FILE'];
            return response()->json($message, 500);
        }
    }

    public function search(Request $request)
    {
        $searchText = $request->searchText;
        $perPage = $request->perPage;
        $all = GfsCode::with('gfs_code_sub_category', 'accountType', 'fund_source')
            ->where('name', 'LIKE', '%' . $searchText . '%')
            ->orWhere('code', 'LIKE', '%' . $searchText . '%')
            ->orWhere('description', 'LIKE', '%' . $searchText . '%')
            ->paginate($perPage);
        return response()->json($all);
    }

    public function single($id)
    {
        $all = GfsCode::where('id', $id)->get();
        return response()->json($all, 200);
    }

    public function expenditure_gfs_codes()
    {
        $all = GfsCode::where(function ($q) {
            $q->where('code', 'like', '020%')->
            orWhere('code', 'like', '2%')->
            orWhere('code', 'like', '3%')->
            orWhere('code', 'like', '030%')->
            orWhere('code', 'like', '040%')->
            orWhere('code', 'like', '4%');
        })->where('is_active', true)
            ->orderBy("code", "asc")
            ->select('id', 'name', 'code')
            ->get();
        return response()->json($all);
    }

    public function revenue_gfs_codes()
    {
        $revenueGfsAccountType = ConfigurationSetting::where('key', 'REVENUE_GFS_ACCOUNT_TYPE')->first();
        $all = [];
        if ($revenueGfsAccountType != null) {
            $all = GfsCode::with('gfs_code_sub_category', 'accountType', 'fund_source')->
            whereHas('accountType', function ($q) use ($revenueGfsAccountType) {
                $q->where('id', (integer)$revenueGfsAccountType->value);
            })->
            whereNull('fund_source_id')->where('is_active', true)->select('id', 'name', 'code', 'description')->get();
        } else {
            $all = GfsCode::with('gfs_code_sub_category', 'accountType', 'fund_source')->
            where('code', 'like', '010%')->whereNull('fund_source_id')->where('is_active', true)->select('id', 'name', 'code', 'description')->get();
        }

        return response()->json($all);
    }

    public function searchExpenditureGfsCodes(Request $request)
    {
        $queryString = $request->searchQuery;
        $idsToExclude = json_decode($request->getContent())->gfsCodesIdsToExclude;
        $queryString = "'%" . strtolower($queryString) . "%'";
        $activityFacilityFundSourceId = Input::get('activityFacilityFundSourceId', 0);
        $gfsExists = ActivityFacilityFundSourceInput::where('activity_facility_fund_source_id', $activityFacilityFundSourceId)->pluck('gfs_code_id');
        $all = GfsCode::with('gfs_code_sub_category', 'accountType', 'fund_source', 'procurement_type')->
        where(function ($q) {
            $q->where('code', 'like', '020%')->
            orWhere('code', 'like', '2%')->
            orWhere('code', 'like', '3%')->
            orWhere('code', 'like', '030%')->
            orWhere('code', 'like', '040%')->
            orWhere('code', 'like', '4%');
        })->where(function ($q) use ($queryString) {
            $q->whereRaw('LOWER(description) like ' . $queryString)->
            orWhereRaw('LOWER(code) like ' . $queryString);
        })->whereNotIn('id', $gfsExists)->where('is_active', true)->select('id', 'name', 'code', 'description', 'gfs_code_sub_category_id', 'procurement_type_id', 'is_procurement')->limit(5)->get(); // Govella

        $data = ['gfsCodes' => $all];
        return response()->json($data);
    }

    public function searchExpdGfsCodes(Request $request)
    {
        $queryString = $request->searchQuery;
        $queryString = "'%" . strtolower($queryString) . "%'";
        $all = GfsCode::with('gfs_code_sub_category')->where(function ($q) {
            $q->where('code', 'like', '020%')->
            orWhere('code', 'like', '2%')->
            orWhere('code', 'like', '3%')->
            orWhere('code', 'like', '030%')->
            orWhere('code', 'like', '040%')->
            orWhere('code', 'like', '4%');
        })->where(function ($q) use ($queryString) {
            $q->whereRaw('LOWER(description) like ' . $queryString)->
            orWhereRaw('LOWER(code) like ' . $queryString);
        })->where('is_active', true)->limit(5)->get();

        $data = ['gfsCodes' => $all];
        return response()->json($data);
    }

    public function downloadExcel()
    {
        $sql = "select g.code as gfs_code,g.name as gfs_code_name,s.code as cost_centre_code,
        s.name as cost_centre,act.name as account_type
        from gfs_codes g
        join account_types act on g.account_type_id = act.id
        left join gfs_code_sections gcs on g.id = gcs.gfs_code_id
        left join sections s on gcs.section_id = s.id";
        $items = DB::select($sql);

        $array = array();
        foreach ($items as $value) {
            $data = array();
            $data['GFS_CODE'] = $value->gfs_code;
            $data['GFS_CODE_NAME'] = $value->gfs_code_name;
            $data['COST_CENTRE_CODE'] = $value->cost_centre_code;
            $data['COST_CENTRE'] = $value->cost_centre;
            $data['ACCOUNT_TYPE'] = $value->account_type;
            array_push($array, $data);
        }

        Excel::create('GFS_CODES_' . time(), function ($excel) use ($array) {
            $excel->sheet('DATA', function ($sheet) use ($array) {
                $sheet->fromArray($array);
            });
        })->export('csv');
    }

    public function fetchAll()
    {
        $all = GfsCode::with('account_type')->where('is_active', true)->get();
        return response()->json(["gfsCodes" => $all], 200);
    }

    public function costCentres(Request $request)
    {
        $gfs_code_id = $request->gfs_code_id;
        $all = GfsCodeSection::with('section')->where('gfs_code_id', $gfs_code_id)->get();
        return response()->json(["items" => $all], 200);
    }

    public function removeCostCentre(Request $request)
    {
        $gfs_code_id = $request->gfs_code_id;
        $id = $request->id;
        $obj = GfsCodeSection::find($id);
        $obj->delete();
        $all = GfsCodeSection::with('section')->where('gfs_code_id', $gfs_code_id)->get();
        return response()->json(["items" => $all, "successMessage" => "Cost Centre Removed Successfully!"], 200);
    }

    public function addCostCentres(Request $request)
    {
        $gfs_code_id = $request->gfs_code_id;
        if (isset($request->sections)) {
            $sections = $request->sections;
            foreach ($sections as $value) {
                $exists = GfsCodeSection::where("gfs_code_id", $gfs_code_id)
                    ->where("section_id", $value['id'])
                    ->count();
                if ($exists < 1) {
                    $obj = new GfsCodeSection();
                    $obj->section_id = $value['id'];
                    $obj->gfs_code_id = $gfs_code_id;
                    $obj->save();
                }
            }
        }
        $all = GfsCodeSection::with('section')->where('gfs_code_id', $gfs_code_id)->get();
        return response()->json(["items" => $all, "successMessage" => "Cost Centres Added Successfully!"], 200);
    }
}
