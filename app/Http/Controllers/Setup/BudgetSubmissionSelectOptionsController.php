<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\BudgetSubmissionSelectOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BudgetSubmissionSelectOptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_admin_hierarchy = UserServices::getUser()->admin_hierarchy_id;
        $query = "select json_agg(json_build_object('id',bs.id,'name',bs.name,'parent',bs.parent_id,'children',
                    (select json_agg(json_build_object('id',bs1.id,'name',bs1.name,'parent',bs1.parent_id,'admin_hierarchy',bs1.admin_hierarchy_id)) from budget_submission_select_option bs1
                    where bs1.parent_id = bs.id and bs1.admin_hierarchy_id = $user_admin_hierarchy)))from budget_submission_select_option bs where bs.parent_id is null";
        $all = DB::select(DB::raw($query));
        return response()->json(json_decode($all[0]->json_agg));
    }

    public function getChildren(Request $request)   
    {
        $user_admin_hierarchy = UserServices::getUser()->admin_hierarchy_id;
        $all = BudgetSubmissionSelectOption::where('parent_id',$request->id)
            ->where('admin_hierarchy_id',$user_admin_hierarchy)
            ->get();
        return response()->json(['children'=>$all]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $obj            = new BudgetSubmissionSelectOption();
        $obj->name      = $request->name;
        $obj->parent_id = isset($request->parent_id)?$request->parent_id:null;
        $obj->admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $obj->active    = true;
        $obj->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $obj = BudgetSubmissionSelectOption::where('id',$id)->get();
        return response()->json($obj);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $obj            = BudgetSubmissionSelectOption::find($id);
        $obj->name      = $request->name;
        $obj->parent_id = $request->parent_id;
        $obj->save();

        $all     = BudgetSubmissionSelectOption::with('children')->whereNull('parent_id')->get();
        $message = ['successMessage'=>'Selection item has been updated successfully',
                    'options' =>$all ];
        return response()->json($message);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $obj            = BudgetSubmissionSelectOption::find($id);
        $obj->delete();
        $all     = BudgetSubmissionSelectOption::with('children')->whereNull('parent_id')->get();
        $message = ['successMessage'=>'Selection item has been updated successfully',
            'options' =>$all ];
        return response()->json($message);

    }
}
