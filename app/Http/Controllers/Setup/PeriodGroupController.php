<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\PeriodGroup;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class PeriodGroupController extends Controller {
    public function index() {
        $all = PeriodGroup::all();
        return response()->json($all);
    }

    public function fetchAll() {
        $all = PeriodGroup::orderBy('created_at', 'asc')->get();
        return response()->json(["periodGroups" => $all], 200);
    }

    public function getAllPaginated($perPage) {
        return PeriodGroup::orderBy('created_at', 'desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $periodGroup = new PeriodGroup();
            $periodGroup->name = $data->name;
            $periodGroup->number = $data->number;
            $periodGroup->created_by = UserServices::getUser()->id;
            $periodGroup->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "PERIOD_GROUP_CREATED_SUCCESSFULLY", "periodGroups" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try {
            $periodGroup = PeriodGroup::find($data->id);
            $periodGroup->name = $data->name;
            $periodGroup->number = $data->number;
            $periodGroup->created_by = UserServices::getUser()->id;
            $periodGroup->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "PERIOD_GROUP_UPDATED_SUCCESSFULLY", "periodGroups" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = PeriodGroup::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "PERIOD_GROUP_DELETED_SUCCESSFULLY", "periodGroups" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = PeriodGroup::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        PeriodGroup::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERIOD_GROUP_RESTORED", "trashedPeriodGroups" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        PeriodGroup::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERIOD_GROUP_DELETED_PERMANENTLY", "trashedPeriodGroups" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = PeriodGroup::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERIOD_GROUP_DELETED_PERMANENTLY", "trashedPeriodGroups" => $all];
        return response()->json($message, 200);
    }
}
