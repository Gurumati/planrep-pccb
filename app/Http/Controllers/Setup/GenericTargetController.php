<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\CustomPager;
use App\Http\Services\UserServices;
use App\Http\Services\VersionServices;
use App\Models\Setup\GenericSectorProblem;
use App\Models\Setup\GenericTarget;
use App\Models\Setup\GenericTargetCostCentre;
use App\Models\Setup\GenericTargetNationalReference;
use App\Models\Setup\GenericTargetPerformanceIndicator;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use phpDocumentor\Reflection\Types\Null_;
use Illuminate\Support\Facades\Validator;

class GenericTargetController extends Controller
{

    public function index()
    {
        return response()->json(GenericTarget::get());
    }

    public function byPlanningMatrix($planningMatrixId){
        return response()->json(['genericTargets'=>GenericTarget::where('planning_matrix_id', $planningMatrixId)->get()]);
    }

    public function paginated($planningMatrixId)
    {
        return response()->json(GenericTarget::with('performance_indicator','plan_chain')
            ->where('planning_matrix_id',$planningMatrixId)
            ->orderBy('id','DESC')
            ->paginate(Input::get('perPage', 10)));
    }

    public function createOrUpdate(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, GenericTarget::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        if (isset($data['id'])) {
            GenericTarget::find($data['id'])->update($data);
            return response()->json(["successMessage" => "UPDATE_SUCCESS"], 200);
        }
        GenericTarget::create($data);
        return response()->json(["successMessage" => "CREATE_SUCCESS"], 200);
    }

    public function delete($id)
    {
        try {
            GenericTarget::find($id)->delete();
            return response()->json(["successMessage" => "DELETE_SUCCESS"], 200);
        } catch (\Execption $e) {
            Log::error($e);
            return response()->json(["errorMessage" => "ERROR_DELETING"], 500);
        }

    }

    public function downloadExcel($planningMatrixId)
    {

        $sql = "SELECT gt.description as genericTargetDescription,pc.description as serviceOutput,pm.name as planningMatrix,pi.description as performanceIndicator,gt.created_at as createdAt from generic_targets gt
        join planning_matrices pm on gt.planning_matrix_id = pm.id
        join plan_chains pc on gt.plan_chain_id = pc.id
        left join performance_indicators pi on gt.performance_indicator_id = pi.id
        where pm.id = $planningMatrixId and gt.deleted_at is null;";

        $items = DB::select($sql);

        $array = array();
        foreach ($items as $value) {
            $data = array();
            $data['GENERIC_TARGET'] = $value->generictargetdescription;
            $data['GENERIC_TARGET'] = $value->serviceoutput;
            $data['PLANNING_MATRIX'] = $value->planningmatrix;
            $data['DATE_CREATED'] = $value->createdat;
            array_push($array, $data);
        }

        Excel::create('GENERIC_TARGETS' . time(), function ($excel) use ($array) {
            $excel->sheet('DATA', function ($sheet) use ($array) {
                $sheet->fromArray($array);
            });
        })->export('csv');
    }
}
