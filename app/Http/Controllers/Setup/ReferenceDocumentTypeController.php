<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\UserServices;
use App\Models\Setup\ReferenceDocumentType;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;


class ReferenceDocumentTypeController extends Controller {

    public function index() {
        $referenceDocumentType = ReferenceDocumentType::all();
        return response()->json($referenceDocumentType);
    }

    public function nationGuidelineTypes() {
        $referenceDocumentType = ReferenceDocumentType::where('is_national_guideline', true)->get();
        return response()->json($referenceDocumentType);
    }

    public function fetchAll() {
        $all = ReferenceDocumentType::where('is_national_guideline', true)->get();
        return response()->json(["referenceDocumentTypes" => $all]);
    }

    public function unUsed($isNationalGuideline) {
        if ($isNationalGuideline == 0) {
            $flatten = new Flatten();
            $existingIds = $flatten->getUserExistingReferenceDocumentTypesIds();
            $referenceDocumentType = ReferenceDocumentType::whereNotIn('id', $existingIds)->where('is_national_guideline', false)->get();
        } else {
            $referenceDocumentType = ReferenceDocumentType::where('is_national_guideline', true)->get();
        }
        return response()->json($referenceDocumentType);
    }

    public function paginateIndex(Request $request) {
        $referenceDocumentType = $this->getAllPaginated($request->perPage);
        return response()->json($referenceDocumentType);
    }

    public function getAllPaginated($perPage) {
        return ReferenceDocumentType::paginate($perPage);
    }

    public function store(Request $request) {
        $data = array_merge($request->all(),['created_by'=>UserServices::getUser()->id]);
        $validator = Validator::make($data, ReferenceDocumentType::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        try{
            ReferenceDocumentType::create($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "REFERENCE_DOCUMENT_TYPE_CREATED_SUCCESSFULLY", "referenceDocumentTypes" => $all];
            return response()->json($message, 200);
        }
        catch (\Exception $e){
            Log::error($e);
            $message = ["errorMessage" => "ERROR_CREATING_REFERENCE_DOCUMENT_TYPE"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = array_merge($request->all(),['updated_by'=>UserServices::getUser()->id]);
        $validator = Validator::make($data, ReferenceDocumentType::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        try{
            $referenceDocumentType = ReferenceDocumentType::find($request->id);
            $referenceDocumentType->update($data);

            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "REFERENCE_DOCUMENT_TYPE_UPDATED_SUCCESSFULLY", "referenceDocumentTypes" => $all];
            return response()->json($message, 200);
        }
        catch (\Exception $e){
            Log::error($e);
            $message = ["errorMessage" => "ERROR_UPDATING_REFERENCE_DOCUMENT_TYPE"];
            return response()->json($message, 500);
        }

    }

    public function delete($id) {
        try{
            $referenceDocumentType = ReferenceDocumentType::find($id);
            $referenceDocumentType->delete();
            $all = $this->getAllPaginated(Input::get('perPage'));
            $message = ["successMessage" => "REFERENCE_DOCUMENT_TYPE_DELETED_SUCCESSFULLY", "referenceDocumentTypes" => $all];
            return response()->json($message, 200);
        }
        catch(QueryException $e){
            Log::error($e);
            $message = ["errorMessage" => "ERROR_DELETING_REFERENCE_DOCUMENT_TYPE"];
            return response()->json($message, 500);
        }


    }
}
