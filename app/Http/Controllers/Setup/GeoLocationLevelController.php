<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\GeoLocationLevel;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

/**
 * @resource GeoLocationLevel
 *
 * GeoLocationLevelController: This controller manages geographical locations levels e.g National,Region, District etc.
 * The highest level in the location start with geo_location_position 1
*/
class GeoLocationLevelController extends Controller {

    public function getAllPaginated($perPage) {
         return GeoLocationLevel::orderBy('sort_order')->paginate($perPage);
    }

    /**
     * index()
     *
     * This method return paginated geo location levels by calling getAllPaginated() method
     * @response {
     *  data[]
     * }
    */
    public function index(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    /**
     * all()
     * This method return all geographical location levels
    */
    public function all() {
        $geoLocationLevels = GeoLocationLevel::orderBy('id','asc')->get();
        return response()->json($geoLocationLevels);
    }

    /**
     * store()
     * This method create new geographical location level.
    */
    public function store(Request $request) {
        try {
            $data = json_decode($request->getContent());
            GeoLocationLevel::create([
                'name'=>$data->name,
                'geo_level_position'=>$data->geo_level_position,
                'sort_order'=>$data->sort_order,
                'code'=>$data->code,
                //'created_by'=>UserServices::getUser()->id,
                'is_active'=>$data->is_active = True,
            ]);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "Geographical location level created successfully ", "geoLocationLevels" => $all];
            return response()->json($message, 200);

        } catch (QueryException $exception) {
            $message = ["errorMessage" => $exception];
            return response()->json($message, 500);
        }
    }
    /**
     * update()
     * This method update existing geographical location level.
     */
    public function update(Request $request) {
        try {

            $data = json_decode($request->getContent());
            $obj = GeoLocationLevel::find($data->id);
            $obj->update([
                'name'=>$data->name,
                'geo_level_position'=>$data->geo_level_position,
                'sort_order'=>$data->sort_order,
                'code'=>$data->code,
                //'created_by'=>UserServices::getUser()->id,
                'is_active'=>$data->is_active = True,
            ]);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "Geographical location level updated successfully", "geoLocationLevels" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $message = ["errorMessage" => $exception];
            return response()->json($message, 500);
        }
    }

    /**
     * delete()
     * This method delete existing geographical location level.
     */
    public function delete($id) {
        GeoLocationLevel::find($id)->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "Geographical location level deleted successfully", "geoLocationLevels" => $all];
        return response()->json($message, 200);
    }

    /**
     * toggleGeoLocationLevel()
     * This method toggle location level active and inactive.
     */
    public function toggleGeoLocationLevel(Request $request) {
        $data = json_decode($request->getContent());
        $obj = GeoLocationLevel::find($data->id);
        $obj->is_active = $data->is_active;
        $obj->save();
        $all = $this->getAllPaginated($request->perPage);
        if ($data->is_active == false) {
            //Return language success key and all data
            $feedback = ["action" => "Geographical level deactivated", "alertType" => "warning", "geoLocationLevels" => $all];
        } else {
            //Return language success key and all data
            $feedback = ["action" => "Geographical level activated", "alertType" => "success", "geoLocationLevels" => $all];
        }
        return response()->json($feedback);
    }

    private function allTrashed() {
        $all = GeoLocationLevel::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    /**
     * trashed()
     * This method return all trashed geographical location levels by calling allTrashed() method
     */
    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    /**
     * restore()
     * This method restore trashed geographical location level
     */
    public function restore($id) {
        GeoLocationLevel::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "geographical location levels restored successfully", "trashedLevels" => $all];
        return response()->json($message, 200);
    }

    /**
     * permanentDelete()
     * This method permanently delete geographical location level
     */
    public function permanentDelete($id) {
        GeoLocationLevel::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "Geographical location level deleted permanently", "deletedLevels" => $all];
        return response()->json($message, 200);
    }

    /**
     * emptyTrash()
     * This method delete trashed geographical location levels
     */
    public function emptyTrash() {
        $trashes = GeoLocationLevel::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "Trash emptied", "trashedEmptied" => $all];
        return response()->json($message, 200);
    }

    public function fetchAll() {
        $geoLocationLevels = GeoLocationLevel::orderBy('id','asc')->get();
        return response()->json(["geoLocLevels"=>$geoLocationLevels],200);
    }
}
