<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\AdminHierarchyLevel;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class UploadController extends Controller
{

    public function uploadTemplates()
    {
        $file = Input::file('file');
        if($file->move('templates', $file->getClientOriginalName())){
            return response()->json(['success'=>true],200);
        } else {
            return response()->json(['success'=>false],500);
        }
    }

}
