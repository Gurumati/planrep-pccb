<?php
/**
 * This controller manage all crud operation related to reference documents
 * e.g of reference document include Strategic Plan that is used to setup long term targets.
 * For Strategic plan there should exist only one entry per admin hierarchy
 */

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\ReferenceDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ReferenceDocumentController extends Controller {

    public function index() {
        $userAdminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $all = ReferenceDocument::getUserValidActiveDocuments($userAdminHierarchyId);
        return response()->json($all, 200);
    }

    public function fetchAll() {
        $userAdminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $all = ReferenceDocument::getUserValidActiveDocuments($userAdminHierarchyId);
        return response()->json(["referenceDocs"=>$all], 200);
    }


    public function allByUser() {
        $userAdminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $all = ReferenceDocument::getUserValidActiveDocuments($userAdminHierarchyId);
        return response()->json(["referenceDocs" => $all], 200);
    }

    public function nationalGuideLineRefDocs() {
        $userAdminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $all = ReferenceDocument::getNationalGuidelines($userAdminHierarchyId);
        return response()->json(["referenceDocs" => $all], 200);
    }

    private function _paginated($perPage, $isNationalGuideline) {
        $userAdminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $referenceDocuments = ReferenceDocument::paginated($userAdminHierarchyId,$perPage,$isNationalGuideline);
        return $referenceDocuments;
    }

    public function paginated(Request $request, $isNationalGuideline) {
        return $this->_paginated($request->perPage, $isNationalGuideline);
    }

    public function getReferenceDocFinancialYears(){
        
    }

    public function store(Request $request, $isNationalGuideline) {
        $data = array_merge($request->all(),['admin_hierarchy_id'=>UserServices::getUser()->admin_hierarchy_id,'created_by'=>UserServices::getUser()->id]) ;
        $messages = array(
            'name.required'=>'Name is required',
            'reference_document_type_id.required'=>'Document Type is required',
            'file_to_upload.required'=>'Select document to upload',
            'file_to_upload.max'=>'Minimum upload file size is 50MB',
            'start_financial_year.required'=>'Start financial year is required',
            'end_financial_year.required'=>'End financial year is required'
        );
        /** To enable update without re upload document this rule is define as option*/
        $documentUrlRule = ["file_to_upload" => "required|max:51200"];
        $validator = Validator::make($data, ReferenceDocument::rules(0,$documentUrlRule),$messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "Form has errors"];
            return response()->json($message, 400);
        }
        try{
            $file = Input::file('file_to_upload');
            $dir = 'uploads/reference_documents/';
            $fileName = time()."_".$file->getClientOriginalName();
            $document_url = '/' . $dir . $fileName;
            $file->move($dir, $fileName);
            $data = array_merge($data,['document_url'=>$document_url]);
            ReferenceDocument::create($data);
            $all = $this->_paginated($request->perPage, $isNationalGuideline);
            $message = ["successMessage" => "CREATE_SUCCESSFUL", "referenceDocuments" => $all];
            return response()->json($message, 200);
        }
        catch(\Exception $e){
            Log::error($e);
            $message = ["errorMessage" => "ERROR_CREATING"];
            return response()->json($message, 500);
        }

    }

    public function update(Request $request, $isNationalGuideline,$id) {
        $data = array_merge($request->all(),['updated_by'=>UserServices::getUser()->id]);
        $messages = array(
            'name.required'=>'Name is required',
            'reference_document_type_id.required'=>'Document Type is required',
            'file_to_upload.required'=>'Select document to upload',
            'file_to_upload.max'=>'Minimum upload file size is 10MB',
            'start_financial_year.required'=>'Start financial year is required',
            'end_financial_year.required'=>'End financial year is required'
        );
        if(isset($request->hasNewDocument) && $request->hasNewDocument === 'true'){
            /** add optional document_url validation rule*/
            $validationRules = ReferenceDocument::rules(0,["file_to_upload" => "required|max:10240"]);
        }
        else{
            $validationRules = ReferenceDocument::rules();
        }
        $validator = Validator::make($data,$validationRules,$messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "Form has errors"];
            return response()->json($message, 400);
        }
        try{
            $referenceDocument = ReferenceDocument::find($id);
            if ($request->hasNewDocument === 'true') {
                $existingFileUri =  ltrim($referenceDocument->document_url, '/');
                Storage::delete($existingFileUri);
                $newFile = Input::file('file_to_upload');
                $dir = 'uploads/reference_documents/';
                $newFileName = time()."_".$newFile->getClientOriginalName();
                $document_url = '/' . $dir . $newFileName;
                $newFile->move($dir, $newFileName);
                $data = array_merge($data,['document_url'=>$document_url]);
            }
            $referenceDocument->update($data);
            $all = $this->_paginated($request->perPage, $isNationalGuideline);
            $message = ["successMessage" => "UPDATE_SUCCESSFUL", "referenceDocuments" => $all];
            return response()->json($message, 200);
        }
        catch (\Exception $e){
            Log::error($e);
            $message = ["errorMessage" => "ERROR_UPDATING"];
            return response()->json($message, 500);
        }

    }


    public function delete(Request $request, $isNationalGuideline,$id) {
        try{
            $document = ReferenceDocument::find($id);
            $fileUri =  ltrim($document->document_url, '/');
            Storage::delete($fileUri);
            DB::table('reference_documents')->where('id', $id)->delete();
            $message = ["successMessage" => "DELETE_SUCCESSFUL"];
            return response()->json($message, 200);
        }
        catch (\Exception $e){
            Log::error($e);
            $message = ["errorMessage" => "ERROR_DELETING"];
            return response()->json($message, 400);
        }

    }
}