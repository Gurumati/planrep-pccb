<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\BodList;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class BodListController extends Controller {
    public function index() {
        $all = BodList::all();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return BodList::with('sector')->orderBy('created_at', 'desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, BodList::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {

            $v = new BodList();
            $v->name = $request->name;
            $v->sort_order = $request->sort_order;
            /*$v->sector_id = $sector_id;*/
            $v->sector_id =$request->sector_id;
            $v->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "BOD_LIST_CREATED_SUCCESSFULLY", "bodLists" => $all];
            return response()->json($message, 200);
        }
    }

    public function update(Request $request) {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, BodList::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            $obj = BodList::find($request->id);
            $obj->update($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "BOD_LIST_UPDATED_SUCCESSFULLY", "bodLists" => $all];
            return response()->json($message, 200);
        }
    }

    public function delete($id) {
        try {
            $obj = BodList::find($id);
            $obj->delete();
            $all = $this->getAllPaginated(Input::get('perPage'));
            $message = ["successMessage" => "BOD_LIST_DELETED_SUCCESSFULLY", "bodLists" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $message = ["errorMessage" => "DATA_IN_USE"];
            return response()->json($message, 500);
        }
    }

    private function allTrashed() {
        $all = BodList::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        BodList::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "BOD_LIST_RESTORED", "trashedBodLists" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        BodList::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "BOD_LIST_DELETED_PERMANENTLY", "trashedBodLists" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = BodList::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "BOD_LIST_DELETED_PERMANENTLY", "trashedBodLists" => $all];
        return response()->json($message, 200);
    }



    public function xxx(){
            $all = BodList::orderBy('created_at')->get();
            return response()->json($all);

    }
}
