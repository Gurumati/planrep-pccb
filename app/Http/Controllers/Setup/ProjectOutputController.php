<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\ProjectOutput;
use App\Models\Setup\Section;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ProjectOutputController extends Controller
{
    public function index()
    {
        try {
            $user = UserServices::getUser();
            $section_id = $user->section_id;
            $section = Section::find($section_id);
            $sector_id = $section->sector_id;
            $all = ProjectOutput::with('sector', 'expenditure_category', 'expenditure_category.activity_task_nature')->where("sector_id", $sector_id)->orderBy('created_at', 'desc')->get();
            return response()->json(["items" => $all], 200);
        } catch (\Exception $exception) {
            Log::error("ERROR: " . $exception->getMessage());
        }
    }

    public function getByExpenditureCategory(Request $request){
        $expenditureCategoryId = $request->expenditureCategoryId;
        $user = UserServices::getUser();
        $section_id = $user->section_id;
        $section = Section::find($section_id);
        $sector_id = $section->sector_id;
        if((int)$sector_id > 0){
            $all = ProjectOutput::with('sector','expenditure_category','expenditure_category.activity_task_nature')->where("sector_id",$sector_id)->where("expenditure_category_id",$expenditureCategoryId)->orderBy('name','asc')->get();
        } else{
            $all =  ProjectOutput::with('sector','expenditure_category','expenditure_category.activity_task_nature')->where("expenditure_category_id",$expenditureCategoryId)->orderBy('name','asc')->get();
        }
        return response()->json(["items" => $all], 200);
    }

    public function getAllPaginated($perPage)
    {
        try {
            $user = UserServices::getUser();
            $section_id = $user->section_id;
            $section = Section::find($section_id);
            $sector_id = $section->sector_id;
            if ((int)$sector_id > 0) {
                return ProjectOutput::with('sector', 'expenditure_category', 'expenditure_category.activity_task_nature')->where("sector_id", $sector_id)->orderBy('name', 'asc')->paginate($perPage);
            } else {
                return ProjectOutput::with('sector', 'expenditure_category', 'expenditure_category.activity_task_nature')->orderBy('name', 'asc')->paginate($perPage);
            }
        } catch (\Exception $exception) {
            Log::error("ERROR: " . $exception->getMessage());
        }
    }

    public function paginated(Request $request)
    {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), ProjectOutput::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        if(count($request['sectors']) > 0){
            foreach ($request['sectors'] as $item) {
                $output = new ProjectOutput();
                $output->name = $request->name;
                $output->sector_id = $item['id'];
                $output->expenditure_category_id = $request->expenditure_category_id;
                $output->save();
            }
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESSFUL", "items" => $all];
            return response()->json($message, 200);
        } else{
            $message = ["errorMessage" => "Please select at least a single sector"];
            return response()->json($message, 200);
        }

    }

    public function update(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, ProjectOutput::rules($request->id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        $expCategory = ProjectOutput::find($request->id);
        $expCategory->update($data);
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "UPDATE_SUCCESSFUL", "items" => $all];
        return response()->json($message, 200);
    }

    public function delete($id)
    {
        $obj = ProjectOutput::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }
}
