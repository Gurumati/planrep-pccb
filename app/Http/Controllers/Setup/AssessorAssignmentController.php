<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\AdminHierarchyLevel;
use App\Models\Setup\AssessorAssignment;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class AssessorAssignmentController extends Controller {

    public function index() {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $admin_hierarchy = AdminHierarchy::find($admin_hierarchy_id);
        $level_id = $admin_hierarchy->admin_hierarchy_level_id;
        $level = AdminHierarchyLevel::find($level_id);
        $position = $level->hierarchy_position;
        if ($position == 1) {
            //PORALG - LOAD REGIONS
            $all = AssessorAssignment::with('user', 'admin_hierarchy', 'period', 'period.financial_year',
                'cas_assessment_round', 'admin_hierarchy_level',
                'cas_assessment_category_version',
                'cas_assessment_category_version.financial_year',
                'cas_assessment_category_version.cas_assessment_category',
                'cas_assessment_category_version.reference_document',
                'cas_assessment_category_version.cas_assessment_state')
                ->whereHas('admin_hierarchy_level',
                    function ($query) use ($position) {
                        $query->where('hierarchy_position', 2);
                    })
                ->orderBy('created_at', 'desc')->get();
        }
        if ($position == 2) {
            //LGA - LOAD LGAs
            $all = AssessorAssignment::with('user', 'admin_hierarchy', 'period', 'period.financial_year',
                'cas_assessment_round', 'admin_hierarchy_level',
                'cas_assessment_category_version',
                'cas_assessment_category_version.financial_year',
                'cas_assessment_category_version.cas_assessment_category',
                'cas_assessment_category_version.reference_document',
                'cas_assessment_category_version.cas_assessment_state')
                ->whereHas('admin_hierarchy_level',
                    function ($query) use ($position) {
                        $query->where('hierarchy_position', 5);
                    })
                ->whereHas('admin_hierarchy',
                    function ($query) use ($position,$admin_hierarchy_id) {
                        $query->where('parent_id', $admin_hierarchy_id);
                    })
                ->orderBy('created_at', 'desc')->get();
        }
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $admin_hierarchy = AdminHierarchy::find($admin_hierarchy_id);
        $level_id = $admin_hierarchy->admin_hierarchy_level_id;
        $level = AdminHierarchyLevel::find($level_id);
        $position = $level->hierarchy_position;
        $all = AssessorAssignment::with('user', 'admin_hierarchy', 'period', 'period.financial_year',
            'cas_assessment_round', 'admin_hierarchy_level',
            'cas_assessment_category_version',
            'cas_assessment_category_version.financial_year',
            'cas_assessment_category_version.cas_assessment_category',
            'cas_assessment_category_version.reference_document',
            'cas_assessment_category_version.cas_assessment_state')
            ->whereHas('admin_hierarchy_level',
                function ($query) use ($position) {
                    $query->where('hierarchy_position', $position);
                })
            ->whereHas('admin_hierarchy',
                function ($query) use ($admin_hierarchy_id) {
                    $query->where('parent_id', $admin_hierarchy_id);
                })
            ->orderBy('created_at', 'desc')->paginate($perPage);
        return $all;
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $admin_hierarchy = AdminHierarchy::find($admin_hierarchy_id);
        $level_id = $admin_hierarchy->admin_hierarchy_level_id;
        $level = AdminHierarchyLevel::find($level_id);
        $position = $level->hierarchy_position;
        $data = json_decode($request->getContent());
        $admin_hierarchy_array = $data->admin_hierarchy_ids;
        try {
            if ($position == 1) {
                for ($x = 0; $x < count($admin_hierarchy_array); $x++) {
                    $obj = new AssessorAssignment();
                    $obj->user_id = $data->user_id;
                    $obj->admin_hierarchy_id = $admin_hierarchy_array[$x];
                    $obj->period_id = $data->period_id;
                    $obj->cas_assessment_round_id = $data->cas_assessment_round_id;
                    $obj->admin_hierarchy_level_id = $level_id;
                    $obj->cas_assessment_category_version_id = $data->cas_assessment_category_version_id;
                    $obj->active = true;
                    $obj->save();
                }
            }
            //REGION
            if ($position == 2) {
                for ($x = 0; $x < count($admin_hierarchy_array); $x++) {
                    $obj = new AssessorAssignment();
                    $obj->user_id = $data->user_id;
                    $obj->admin_hierarchy_id = $admin_hierarchy_array[$x];
                    $obj->period_id = $data->period_id;
                    $obj->cas_assessment_round_id = $data->cas_assessment_round_id;
                    $obj->admin_hierarchy_level_id = $level_id;
                    $obj->cas_assessment_category_version_id = $data->cas_assessment_category_version_id;
                    $obj->active = true;
                    $obj->save();
                }
            }
            //COUNCIL
            if ($position == 3) {
                $obj = new AssessorAssignment();
                $obj->user_id = $data->user_id;
                $obj->admin_hierarchy_id = $admin_hierarchy_id;
                $obj->period_id = $request->period_id;
                $obj->cas_assessment_round_id = $request->cas_assessment_round_id;
                $obj->admin_hierarchy_level_id = $level_id;
                $obj->cas_assessment_category_version_id = $request->cas_assessment_category_version_id;
                $obj->active = true;
                $obj->save();
            }

            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "ASSIGNMENT_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try {
            $obj = AssessorAssignment::find($data->id);
            $obj->user_id = $data->user_id;
            $obj->admin_hierarchy_id = $data->admin_hierarchy_id;
            $obj->period_id = $data->period_id;
            $obj->cas_assessment_round_id = $data->cas_assessment_round_id;
            $obj->admin_hierarchy_level_id = $data->admin_hierarchy_level_id;
            $obj->cas_assessment_category_version_id = $data->cas_assessment_category_version_id;
            $obj->active = true;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }


    public function delete($id) {
        $obj = AssessorAssignment::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = AssessorAssignment::with('user', 'admin_hierarchy', 'period', 'period.financial_year',
            'cas_assessment_round', 'admin_hierarchy_level',
            'cas_assessment_category_version',
            'cas_assessment_category_version.financial_year',
            'cas_assessment_category_version.cas_assessment_category',
            'cas_assessment_category_version.reference_document',
            'cas_assessment_category_version.cas_assessment_state')->orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        $message = ["trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function restore($id) {
        AssessorAssignment::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        AssessorAssignment::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = AssessorAssignment::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function periods(Request $request) {
        $cas_assessment_category_version_id = $request->cas_assessment_category_version_id;
        $sql = "SELECT periods.id,periods.name as period,financial_years.name as financial_year from periods join financial_years on periods.financial_year_id = financial_years.id join period_groups on periods.period_group_id = period_groups.id join cas_assessment_categories on cas_assessment_categories.period_group_id = period_groups.id join cas_assessment_category_versions on cas_assessment_category_versions.cas_assessment_category_id = cas_assessment_categories.id WHERE cas_assessment_category_versions.id = '$cas_assessment_category_version_id'";
        $all = DB::select($sql);
        $data = ["periods" => $all];
        return response()->json($data, 200);
    }

    public function toggleActive(Request $request) {
        $data = json_decode($request->getContent());
        $object = AssessorAssignment::find($data->id);
        $object->active = $data->active;
        $object->save();
        $all = $this->getAllPaginated($request->perPage);
        if ($data->active == false) {
            $feedback = ["action" => "DEACTIVATED", "alertType" => "warning", "items" => $all];
        } else {
            $feedback = ["action" => "ACTIVATED", "alertType" => "success", "items" => $all];
        }
        return response()->json($feedback, 200);
    }
}
