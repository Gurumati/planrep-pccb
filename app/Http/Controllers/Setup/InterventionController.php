<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\CustomPager;
use App\Http\Services\UserServices;
use App\Models\Setup\Intervention;
use App\Models\Setup\InterventionVersion;
use App\Models\Setup\PriorityArea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class InterventionController extends Controller
{
    public function index()
    {
        $all = Intervention::with('intervention_category', 'priority_area', 'link_level')->where('is_active', true)->get();
        return response()->json($all);
    }

    public function fetchAll()
    {
        $all = Intervention::with('intervention_category', 'priority_area', 'link_level')->where('is_active', true)->get();
        return response()->json(["interventions" => $all], 200);
    }

    public function getAllPaginated($financialYearId, $versionId, $perPage)
    {
        $sql = "select i.id,i.id as intervention_id,i.description,i.number,i.is_active,i.is_primary,ic.id as intervention_category_id,ic.name as intervention_category,p.id as priority_area_id,p.description as priority_area,
                p.number as priority_area_number,l.id as link_level_id,l.name as link_level_name
                from interventions i
                join intervention_categories ic on ic.id = i.intervention_category_id
                join priority_areas p on p.id = i.priority_area_id
                join link_levels l on l.id = i.link_level_id
                join intervention_versions iv on iv.intervention_id = i.id
                join versions v on iv.version_id = v.id
                join financial_year_versions fyv on v.id = fyv.version_id
                where fyv.financial_year_id = $financialYearId 
                and v.id = $versionId and i.deleted_at is null order by p.number";
        return CustomPager::paginate(DB::select($sql), $perPage);
    }

    public function paginated(Request $request)
    {
        $financialYearId = $request->financialYearId;
        $versionId = $request->versionId;
        $all = $this->getAllPaginated($financialYearId, $versionId, $request->perPage);
        return response()->json($all);
    }

    public function store(Request $request)
    {
        if (isset($request->perPage)) {
            $perPage = $request->perPage;
        } else {
            $perPage = 10;
        }
        DB::transaction(function () use ($request) {
            $data = json_decode($request->getContent());

            $count = Intervention::with('priority_area')->where('priority_area_id', $data->priority_area_id)->count();
            $last_auto_id = $count + 1;
            $pa = PriorityArea::find($data->priority_area_id);
            $priority_area_number = $pa->number;
            $standard_number = str_pad(($last_auto_id), 2, 0, STR_PAD_LEFT);

            $obj = new Intervention();
            $obj->description = $data->description;
            $obj->intervention_category_id = $data->intervention_category_id;
            $obj->priority_area_id = $data->priority_area_id;
            $obj->link_level_id = $data->link_level_id;
            $obj->number = $priority_area_number . "" . $standard_number;
            $obj->is_primary = (isset($data->is_primary)) ? $data->is_primary : false;
            $obj->is_active = true;
            $obj->created_by = UserServices::getUser()->id;
            $obj->save();

            $intervention_id = $obj->id;
            $iv = new InterventionVersion();
            $iv->version_id = $request->versionId;
            $iv->intervention_id = $intervention_id;
            $iv->save();
        });
        $all = $this->getAllPaginated($request->financialYearId, $request->versionId, $perPage);
        $message = ["successMessage" => "INTERVENTION_CREATED_SUCCESSFULLY", "interventions" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request)
    {
        $data = json_decode($request->getContent());
        $obj = Intervention::find($data->id);
        $obj->description = $data->description;
        $obj->intervention_category_id = $data->intervention_category_id;
        $obj->priority_area_id = $data->priority_area_id;
        $obj->link_level_id = $data->link_level_id;
        $obj->is_primary = (isset($data->is_primary)) ? $data->is_primary : false;
        $obj->updated_by = UserServices::getUser()->id;
        $obj->save();
        $all = $this->getAllPaginated($request->financialYearId, $request->versionId, $request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_INTERVENTION_UPDATED", "interventions" => $all];
        return response()->json($message, 200);
    }

    public function copy(Request $request)
    {
        $sourceFinancialYearId = $request->sourceFinancialYearId;
        $sourceVersionId = $request->sourceVersionId;
        $destinationVersionId = $request->destinationVersionId;
        $sql = "select i.* from interventions i
                join intervention_versions iv on iv.intervention_id = i.id
                join versions v on iv.version_id = v.id
                join financial_year_versions fyv on v.id = fyv.version_id
                where iv.version_id = $sourceVersionId and fyv.financial_year_id = $sourceFinancialYearId 
                and i.deleted_at is null";
        $sourceInterventions = DB::select($sql);
        foreach ($sourceInterventions as $data) {
            DB::transaction(function () use ($data, $destinationVersionId) {
                $obj = new Intervention();
                $obj->description = $data->description;
                $obj->intervention_category_id = $data->intervention_category_id;
                $obj->priority_area_id = $data->priority_area_id;
                $obj->link_level_id = $data->link_level_id;
                $obj->number = $data->number;
                $obj->is_primary = $data->is_primary;
                $obj->is_active = $data->is_active;;
                $obj->created_by = UserServices::getUser()->id;
                $obj->save();
                $intervention_id = $obj->id;

                $inv = new InterventionVersion();
                $inv->intervention_id = $intervention_id;
                $inv->version_id = $destinationVersionId;
                $inv->save();
            });
        }
        return apiResponse(201,"Successfully copied Interventions",[],true,[]);
    }

    public function delete($id)
    {
        $perPage = Input::get('perPage');
        $financialYearId = Input::get('financialYearId');
        $versionId = Input::get('versionId');
        $obj = Intervention::find($id);
        $obj->delete();
        $all = $this->getAllPaginated($financialYearId, $versionId, $perPage);
        $message = ["successMessage" => "SUCCESSFUL_INTERVENTION_DELETED", "interventions" => $all];
        return response()->json($message, 200);
    }

    public function active(Request $request)
    {
        $id = $request->id;
        $active = $request->active;
        $obj = Intervention::find($id);
        $obj->is_active = $active;
        $obj->save();
        return apiResponse(200, "Success", [], true, []);
    }

    public function primary(Request $request)
    {
        $id = $request->id;
        $primary = $request->primary;
        $obj = Intervention::find($id);
        $obj->is_primary = $primary;
        $obj->save();
        return apiResponse(200, "Success", [], true, []);
    }


    public function generateCodes()
    {
        $interventions = Intervention::with('priority_area')->orderBy('id', 'asc')->get();
        foreach ($interventions as $intervention) {
            $priority_area_code = $intervention->priority_area->number;
            $number = $priority_area_code . "" . str_pad(($intervention->id), 2, 0, STR_PAD_LEFT);
            DB::table('interventions')->where('id', $intervention->id)->update(['number' => $number]);
        }
    }
}
