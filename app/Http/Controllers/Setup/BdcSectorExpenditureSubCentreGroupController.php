<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\BdcSectorExpenditureSubCentreGroup;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;

class BdcSectorExpenditureSubCentreGroupController extends Controller {
    public function index() {
        $all = BdcSectorExpenditureSubCentreGroup::with('bdc_group', 'bdc_sector_expenditure_sub_centre')->orderBy('created_at', 'desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return BdcSectorExpenditureSubCentreGroup::with('bdc_group', 'bdc_sector_expenditure_sub_centre')->orderBy('created_at', 'desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, BdcSectorExpenditureSubCentreGroup::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            $count = BdcSectorExpenditureSubCentreGroup::where('bdc_group_id',$request->bdc_group_id)->where('bdc_sector_expenditure_sub_centre_id',$request->bdc_sector_expenditure_sub_centre_id)->count();
            if($count == 1){
                $message = ["errorMessage" => "DUPLICATE_ENTRY"];
                return response()->json($message, 500);
            } else {
                $v = new BdcSectorExpenditureSubCentreGroup();
                $v->bdc_group_id = $request->bdc_group_id;
                $v->bdc_sector_expenditure_sub_centre_id = $request->bdc_sector_expenditure_sub_centre_id;
                $v->save();
                $all = $this->getAllPaginated($request->perPage);
                $message = ["successMessage" => "CREATE_SUCCESS", "sectorExpenditureSubCentreGroups" => $all];
                return response()->json($message, 200);
            }
        }
    }

    public function update(Request $request) {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, BdcSectorExpenditureSubCentreGroup::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            $obj = BdcSectorExpenditureSubCentreGroup::find($request->id);
            $obj->update($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "sectorExpenditureSubCentreGroups" => $all];
            return response()->json($message, 200);
        }
    }

    public function delete($id) {
        try {
            $obj = BdcSectorExpenditureSubCentreGroup::find($id);
            $obj->delete();
            $all = $this->getAllPaginated(Input::get('perPage'));
            $message = ["successMessage" => "DELETE_SUCCESS", "sectorExpenditureSubCentreGroups" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $message = ["errorMessage" => "DATA_IN_USE"];
            return response()->json($message, 500);
        }
    }

    private function allTrashed() {
        $all = BdcSectorExpenditureSubCentreGroup::with('bdc_group', 'bdc_sector_expenditure_sub_centre')
            ->orderBy('created_at', 'desc')
            ->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        BdcSectorExpenditureSubCentreGroup::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedSectorExpenditureSubCentreGroups" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        BdcSectorExpenditureSubCentreGroup::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedSectorExpenditureSubCentreGroups" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = BdcSectorExpenditureSubCentreGroup::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedSectorExpenditureSubCentreGroups" => $all];
        return response()->json($message, 200);
    }
}
