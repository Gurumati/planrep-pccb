<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\PlanChainType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PlanChainTypeController extends Controller {
    public function index() {
        $planChainTypes = PlanChainType::where('is_active', true)->orderBy('hierarchy_level')->get();
        return response()->json($planChainTypes);
    }

    public function getAllPaginated($perPage) {
        return PlanChainType::orderby('id')->orderBy('hierarchy_level')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        $planChainType = new PlanChainType();
        $planChainType->name = $data->name;
        $planChainType->is_sectoral = $data->is_sectoral;
        $planChainType->is_incremental = $data->is_incremental;
        $planChainType->hierarchy_level = $data->hierarchy_level;
        $planChainType->is_active = TRUE;
        $planChainType->save();
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_PLAN_CHAIN_TYPE_CREATED", "planChainTypes" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        $planChainType = PlanChainType::find($data->id);
        $planChainType->name = $data->name;
        $planChainType->is_sectoral = $data->is_sectoral;
        $planChainType->is_incremental = $data->is_incremental;
        $planChainType->hierarchy_level = $data->hierarchy_level;
        $planChainType->is_active = $data->is_active;
        $planChainType->save();
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_PLAN_CHAIN_TYPE_UPDATED", "planChainTypes" => $all];
        return response()->json($message, 200);
    }

    public function delete($id) {
        $planChainType = PlanChainType::find($id);
        $planChainType->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_PLAN_CHAIN_TYPE_DELETED", "planChainTypes" => $all];
        return response()->json($message, 200);
    }

    /** activate/ deactivate plan chain type */
    public function activate(Request $request) {
        $data = json_decode($request->getContent());
        $planChainType = PlanChainType::find($data->id);
        $planChainType->is_active = $data->is_active;
        $planChainType->save();
        $all = $this->getAllPaginated($request->perPage);
        if ($planChainType->is_active) {
            $message = ["activateMessage" => "SUCCESSFUL_PLAN_CHAIN_TYPE_ACTIVATED", "planChainTypes" => $all];
        } else {
            $message = ["activateMessage" => "SUCCESSFUL_PLAN_CHAIN_TYPE_DEACTIVATED", "planChainTypes" => $all];
        }
        return response()->json($message, 200);
    }
}
