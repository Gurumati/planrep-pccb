<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\AssetUse;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class AssetUseController extends Controller {
    public function index() {
        $all = AssetUse::orderBy('created_at', 'desc')->get();
        return response()->json(["assetUses" => $all], 200);
    }

    public function fetchAll() {
        $all = AssetUse::orderBy('created_at', 'desc')->get();
        return response()->json(["assetUses" => $all], 200);
    }

    public function getAllPaginated($perPage) {
        return AssetUse::orderBy('id')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, AssetUse::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "Form Errors"];
            return response()->json($message, 500);
        } else {
            try {
                AssetUse::create($data);
                $all = $this->getAllPaginated($request->perPage);
                $message = ["successMessage" => "CREATE_SUCCESS", "items" => $all];
                return response()->json($message, 200);
            } catch (QueryException $exception) {
                $error = $exception->getMessage();
                $message = ["errorMessage" => $error];
                return response()->json($message, 500);
            }
        }
    }

    public function update(Request $request) {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, AssetUse::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "Form Errors"];
            return response()->json($message, 500);
        } else {
            try {
                $obj = AssetUse::find($request->id);
                $obj->update($data);
                $all = $this->getAllPaginated($request->perPage);
                $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
                return response()->json($message, 200);
            } catch (QueryException $exception) {
                $error = $exception->getMessage();
                $message = ["errorMessage" => $error];
                return response()->json($message, 500);
            }
        }
    }

    public function delete($id) {
        $obj = AssetUse::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = AssetUse::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json(["trashedItems" => $all], 200);
    }

    public function restore($id) {
        AssetUse::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        AssetUse::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = AssetUse::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }
}
