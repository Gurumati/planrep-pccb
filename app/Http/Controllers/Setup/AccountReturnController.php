<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\AccountReturn;
use App\Models\Setup\AccountReturnGfsCode;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class AccountReturnController extends Controller
{
    public function index()
    {
        $items = AccountReturn::with('section','sector.sector')->get();
        return response()->json($items);
    }

    public function getAllPaginated($perPage)
    {
        return AccountReturn::with('section','section.sector')->orderBy('created_at')->paginate($perPage);
    }

    public function paginated(Request $request)
    {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function gfsCodes(Request $request)
    {
        $accountReturnId = $request->account_return_id;
        $items = AccountReturnGfsCode::with('gfs_code')->where('account_return_id', $accountReturnId)->get();
        return response()->json(['items' => $items]);
    }

    public function addGfsCodes(Request $request)
    {
        $accountReturnId = $request->account_return_id;
        $gfsCodes = $request->gfs;
        foreach ($gfsCodes as $gfsCode) {
            $gfs_code_id = $gfsCode['id'];
            $count = AccountReturnGfsCode::where('account_return_id', $accountReturnId)
                ->where('gfs_code_id', $gfs_code_id)->count();
            if ($count < 1) {
                $new = new AccountReturnGfsCode();
                $new->gfs_code_id = $gfs_code_id;
                $new->account_return_id = $accountReturnId;
                $new->save();
            }
        }

        $items = AccountReturnGfsCode::with('gfs_code')->where('account_return_id', $accountReturnId)->get();
        return response()->json(['items' => $items, "successMessage" => "ADD_SUCCESS"]);
    }

    public function removeGfsCode(Request $request)
{
    $accountReturnId = $request->account_return_id;
    $id = $request->id;
    $row = AccountReturnGfsCode::find($id);
    $row->delete();
    $items = AccountReturnGfsCode::with('gfs_code')->where('account_return_id', $accountReturnId)->get();
    return response()->json(['items' => $items, "successMessage" => "REMOVE_SUCCESS"]);
}

    public function store(Request $request)
    {
        if (!isset($request->gfs)) {
            $message = ["errorMessage" => "Select at least one gfs code"];
            return response()->json($message, 500);
        } else {
            $validator = Validator::make($request->all(), [
                'code' => 'required|unique:account_returns',
                'name' => 'required',
                'section_id' => 'required|integer',
            ]);

            if ($validator->fails()) {
                $errors = $validator->errors();
                $message = ["errors" => $errors->all()];
                return response()->json($message, 500);
            } else {
                $name = $request->name;
                $code = $request->code;
                $section_id = $request->section_id;
                $ns = new AccountReturn();
                $ns->name = $name;
                $ns->code = $code;
                $ns->section_id = $section_id;
                $ns->save();
                $account_return_id = $ns->id;
                $gfsCodes = $request->gfs;
                foreach ($gfsCodes as $gfsCode) {
                    $gfs_code_id = $gfsCode['id'];
                    $new = new AccountReturnGfsCode();
                    $new->gfs_code_id = $gfs_code_id;
                    $new->account_return_id = $account_return_id;
                    $new->save();
                }

                $all = $this->getAllPaginated($request->perPage);
                $message = ["successMessage" => "CREATE_SUCCESS", "items" => $all];
                return response()->json($message, 200);
            }
        }
    }


    public function update(Request $request)
    {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, AccountReturn::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            $obj = AccountReturn::find($request->id);
            $obj->update($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        }
    }

    public function delete($id)
    {
        try {
            $accountType = AccountReturn::find($id);
            $accountType->delete();
            $all = $this->getAllPaginated(Input::get('perPage'));
            $message = ["successMessage" => "DELETED_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $message = ["errorMessage" => "DATA_IN_USE"];
            return response()->json($message, 500);
        }
    }
}
