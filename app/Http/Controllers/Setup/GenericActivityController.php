<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\FinancialYearVersion;
use App\Models\Setup\GenericActivity;
use App\Models\Setup\GenericActivityCostCentre;
use App\Models\Setup\GenericTarget;
use App\Models\Setup\Section;
use App\Models\Setup\Sector;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class GenericActivityController extends Controller
{

    public function index()
    {
        return response()->json(GenericActivity::get());
    }

    public function paginate($planningMatrixId)
    {
        return response()->json(GenericActivity::with('generic_target', 'intervention', 'generic_sector_problem')
            ->whereHas('generic_target', function ($gt) use ($planningMatrixId) {
                $gt->where('planning_matrix_id', $planningMatrixId);
            })
            ->orderBy('id', 'DESC')
            ->paginate(Input::get('perPage', 10)));
    }

    public function createOrUpdate(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, GenericActivity::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        if (isset($data['id'])) {
            GenericActivity::find($data['id'])->update($data);
            return response()->json(["successMessage" => "UPDATE_SUCCESS"], 200);
        }
        GenericActivity::create($data);
        return response()->json(["successMessage" => "CREATE_SUCCESS"], 200);
    }

    public function delete($id)
    {
        try {
            GenericActivity::find($id)->delete();
            return response()->json(["successMessage" => "DELETE_SUCCESS"], 200);
        } catch (\Execption $e) {
            Log::error($e);
            return response()->json(["errorMessage" => "ERROR_DELETING"], 500);
        }

    }

    public function downloadExcel($planningMatrixId)
    {

        $sql = "SELECT ga.description as genericActivityDescription, ga.code as genericActivityCode,gt.description as genericTargetDescription,
        i.description as interventionDescription,gsp.description as genericSectorProblem,ga.params as parameters,ga.created_at,pm.name as planningMatrix from generic_activities ga
        join generic_targets gt on ga.generic_target_id = gt.id
        join interventions i on ga.intervention_id = i.id
        join generic_sector_problems gsp on ga.generic_sector_problem_id = gsp.id
        join planning_matrices pm on gt.planning_matrix_id = pm.id where pm.id = $planningMatrixId and ga.deleted_at is null";

        $items = DB::select($sql);

        $array = array();
        foreach ($items as $value) {
            $data = array();
            $data['GENERIC_ACTIVITY'] = $value->genericactivitydescription;
            $data['GENERIC_TARGET'] = $value->generictargetdescription;
            $data['INTERVENTION'] = $value->interventiondescription;
            $data['GENERIC_SECTOR_PROBLEM'] = $value->genericsectorproblem;
            $data['PARAMETERS'] = $value->parameters;
            $data['PLANNING_MATRIX'] = $value->planningmatrix;
            $data['DATE_CREATED'] = $value->created_at;
            array_push($array, $data);
        }

        Excel::create('GENERIC_ACTIVITIES' . time(), function ($excel) use ($array) {
            $excel->sheet('DATA', function ($sheet) use ($array) {
                $sheet->fromArray($array);
            });
        })->export('csv');
    }

}
