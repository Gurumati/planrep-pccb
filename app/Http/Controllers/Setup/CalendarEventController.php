<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\CalendarEvent;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

/**
 * @resource CalendarEvent
 * CalendarEventController: This controller manage calender events
*/
class CalendarEventController extends Controller {

    /**
     * index()
     * This method return all calendar events
    */
    public function index() {
        $all = CalendarEvent::orderBy('number')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return CalendarEvent::orderBy('number','asc')->paginate($perPage);
    }
    /**
     * paginated()
     * This method return calendar event by page
     */
    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }
    /**
     * getByYearAndLevel()
     * This method return Calendar Event by financial year and hierarchy position
    */
    public function getByYearAndLevel($financialYearId,$hierarchyPosition){


        $Ids= DB::table('calendars as c')->
                  join('calendar_events as e','e.id','c.calendar_event_id')->
                  where('c.hierarchy_position',$hierarchyPosition)->
                  where('e.is_system_event',true)->
                  where('c.financial_year_id',$financialYearId)->pluck('c.id');

        $all= CalendarEvent::whereNotIn('id',$Ids)->orderBy('number','asc')->get();
        $message=['events'=>$all];
        return response()->json($message);
    }
    /**
     * store()
     * This method create new Calendar event
    */
    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new CalendarEvent();
            $obj->name = $data->name;
            $obj->number = $data->number;
            $obj->before_start_reminder_sms = $data->before_start_reminder_sms;
            $obj->before_end_reminder_sms = $data->before_end_reminder_sms;
            $obj->before_start_reminder_days = $data->before_start_reminder_days;
            $obj->before_end_reminder_days = $data->before_end_reminder_days;
            $obj->url = $data->url;
            $obj->expected_value_query = $data->expected_value_query;
            $obj->actual_value_query = $data->actual_value_query;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "calendarEvents" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }
    /**
     * update()
     * This method update existing Calendar Event
    */
    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = CalendarEvent::find($data->id);
            $obj->name = $data->name;
            $obj->number = $data->number;
            $obj->before_start_reminder_sms = $data->before_start_reminder_sms;
            $obj->before_end_reminder_sms = $data->before_end_reminder_sms;
            $obj->before_start_reminder_days = $data->before_start_reminder_days;
            $obj->before_end_reminder_days = $data->before_end_reminder_days;
            $obj->expected_value_query = $data->expected_value_query;
            $obj->actual_value_query = $data->actual_value_query;
            $obj->url = $data->url;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "calendarEvents" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }
    /**
     * delete()
     * This method delete existing Calendar Event
    */
    public function delete($id) {
        $obj = CalendarEvent::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "calendarEvents" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = CalendarEvent::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }
    /**
     * allTrashed()
     * This method get all trashed Calendar Event
     */
    public function trashed() {
        $all = $this->allTrashed();
        $message = ["trashedCalendarEvents" => $all];
        return response()->json($message,200);
    }
    /**
     * restore()
     * This method restore trashed Calendar Event
    */
    public function restore($id) {
        CalendarEvent::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedCalendarEvents" => $all];
        return response()->json($message, 200);
    }
    /**
     * permanentDelete()
     * This method soft delete Calendar Event
    */
    public function permanentDelete($id) {
        CalendarEvent::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedCalendarEvents" => $all];
        return response()->json($message, 200);
    }
    /**
     * emptyTrash()
     * This method permanent delete Calendar Event
    */
    public function emptyTrash() {
        $trashes = CalendarEvent::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedCalendarEvents" => $all];
        return response()->json($message, 200);
    }
}
