<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\Version;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class VersionController extends Controller
{
    public function index()
    {
        $items = Version::with('type')->orderBy('date', 'desc')->get();
        return apiResponse(200, "Success", $items, true, []);
    }

    public function getAllPaginated($perPage)
    {
        return Version::with('type')->orderBy('date', 'desc')
            ->orderBy("name","ASC")
            ->paginate($perPage);
    }

    public function paginated(Request $request)
    {
        $perPage = $request->perPage;
        $items = $this->getAllPaginated($perPage);
        return apiResponse(200, "Success", $items, true, []);
    }

    public function toggleCurrent(Request $request)
    {
        $id = $request->id;
        $versionTypeId = $request->versionTypeId;
        if($request->is_current == true){
            $vs = Version::where("version_type_id",$versionTypeId)->get();
            foreach ($vs as $v) {
                $v->is_current = false;
                $v->save();
            }
        }
        $v = Version::find($id);
        $v->is_current = $request->is_current;
        $v->save();
        if(isset($request->perPage)){
            $perPage = $request->perPage;
        } else{
            $perPage = 10;
        }
        $items = $this->getAllPaginated($perPage);
        return apiResponse(201, "Success", $items, true, []);
    }


    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, Version::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $items = $this->getAllPaginated($request->perPage);
            return apiResponse(500, "Failed", $items, false, $errors->all());
        } else {
            Version::create($data);
            $items = $this->getAllPaginated($request->perPage);
            return apiResponse(201, "Success", $items, true, []);
        }
    }


    public function update(Request $request)
    {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, Version::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $items = $this->getAllPaginated($request->perPage);
            return apiResponse(500, "Failed", $items, false, $errors->all());
        } else {
            $obj = Version::find($request->id);
            $obj->update($data);
            $items = $this->getAllPaginated($request->perPage);
            return apiResponse(201, "Success", $items, true, []);
        }
    }

    public function delete($id)
    {
        $item = Version::find($id);
        $item->delete();
        $items = $this->getAllPaginated(Input::get('perPage'));
        return apiResponse(200, "Success", $items, true, []);
    }

    public function show($id)
    {
        $item = Version::find($id);
        return apiResponse(200, "Success", $item, true, []);
    }

    public function getNextFinancialYearVersion($versionType, $currentFinancialYearId, $itemId){
        return Version::getNextFinancialYearVersion($versionType, $currentFinancialYearId, $itemId);
    }

    public function addToVersion($versionType, $itemId, $versionId){
        return Version::addToVersion($versionType, $itemId, $versionId);
    }
}
