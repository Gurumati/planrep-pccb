<?php

namespace App\Http\Controllers\Setup;

use App\DTOs\SetupDTOs\InterventionCategoryDTO;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\Intervention;
use App\Models\Setup\InterventionCategory;
use App\Models\Setup\InterventionCategoryWithParent;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class InterventionCategoryController extends Controller
{
    public function index()
    {
        $all = InterventionCategory::with('childInterventionCategories')->whereNull('intervention_category_id')->get();
        return response()->json($all);
    }

    public function filterInterventionByCategory(Request $request)
    {
        $intervention_category_id = $request->intervention_category_id;
        $all = Intervention::where('intervention_category_id', $intervention_category_id)
            ->where('is_active', true)
            ->select('id','number','description')->get();
        return response()->json(["interventions" => $all], Response::HTTP_OK);
    }

    public function entireTree($id = null)
    {
        if ($id == null) {
            $all = InterventionCategory::with('childInterventionCategories')->get();
        } else {
            $all = InterventionCategory::with('childInterventionCategories')->where('id', '<>', $id)->get();
        }
        return response()->json($all);
    }

    public function wholeTree()
    {
        $all = InterventionCategory::with('childInterventionCategories')->get();
        return response()->json($all);
    }

    public function store(Request $request)
    {
        try {
            $data = json_decode($request->getContent());
            $obj = new InterventionCategory();
            $obj->name = $data->name;
            $obj->description = $data->description;
            $obj->footer_text = (isset($data->footer_text)) ? $data->footer_text : null;
            $obj->sort_order = $data->sort_order;
            $obj->intervention_category_id = (isset($data->intervention_category_id)) ? $data->intervention_category_id : null;
            $obj->is_active = true;
            $obj->created_by = UserServices::getUser()->id;
            $obj->save();
            $all = $this->hierarchyTree();
            $message = ["successMessage" => "INTERVENTION_CATEGORY_CREATED_SUCCESSFULLY", "interventionCategories" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request)
    {
        $data = json_decode($request->getContent());
        try {
            $obj = InterventionCategory::find($data->id);
            $obj->name = $data->name;
            $obj->description = $data->description;
            $obj->footer_text = (isset($data->footer_text)) ? $data->footer_text : null;
            $obj->sort_order = $data->sort_order;
            $obj->intervention_category_id = (isset($data->intervention_category_id)) ? $data->intervention_category_id : null;
            $obj->updated_by = UserServices::getUser()->id;
            $obj->save();
            $all = $this->hierarchyTree();
            $message = ["successMessage" => "SUCCESSFUL_INTERVENTION_CATEGORY_UPDATED", "interventionCategories" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id)
    {
        $obj = InterventionCategory::find($id);
        $obj->delete();
        $all = $this->hierarchyTree();
        $message = ["successMessage" => "SUCCESSFUL_INTERVENTION_CATEGORY_DELETED", "interventionCategories" => $all];
        return response()->json($message, 200);
    }

    private function hierarchyTree()
    {
        $all = InterventionCategory::with('childInterventionCategories')->whereNull('intervention_category_id')->get();
        return $all;
    }

    private function allTrashed()
    {
        $all = InterventionCategory::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed()
    {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id)
    {
        InterventionCategory::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "INTERVENTION_CATEGORY_RESTORED", "trashedInterventionCategories" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id)
    {
        InterventionCategory::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "INTERVENTION_CATEGORY_DELETED_PERMANENTLY", "trashedInterventionCategories" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash()
    {
        $trashes = InterventionCategory::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "INTERVENTION_CATEGORY_DELETED_PERMANENTLY", "trashedInterventionCategories" => $all];
        return response()->json($message, 200);
    }

    public function parents()
    {
        $all = InterventionCategoryDTO::whereNull('intervention_category_id')->get();
        return response()->json(["items"=>$all],Response::HTTP_OK);
    }

    public function children(Request $request)
    {
        $parent_id = $request->parent_id;
        $all = InterventionCategoryDTO::where('intervention_category_id',$parent_id)->get();
        return response()->json(["items"=>$all],Response::HTTP_OK);
    }
}
