<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\FacilityCustomDetailMapping;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class FacilityCustomDetailMappingController extends Controller {
    public function index() {
        $all = FacilityCustomDetailMapping::with('facility_custom_detail', 'facility_type', 'facility_ownership')->orderBy('created_at', 'asc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return FacilityCustomDetailMapping::with('facility_custom_detail', 'facility_type', 'facility_ownership')->orderBy('created_at', 'asc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $facility_types_array = $data->facility_type_id;
            for ($i = 0;$i < count($facility_types_array); $i++){
                $obj = new FacilityCustomDetailMapping();
                $obj->facility_custom_detail_id = $data->facility_custom_detail_id;
                $obj->facility_type_id = $facility_types_array[$i];
                $obj->facility_ownership_id = $data->facility_ownership_id;
                $obj->save();
            }
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_number = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_number . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try {
            $obj = FacilityCustomDetailMapping::find($data->id);
            $obj->facility_custom_detail_id = $data->facility_custom_detail_id;
            $obj->facility_type_id = $data->facility_type_id;
            $obj->facility_ownership_id = $data->facility_ownership_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_number = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_number . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = FacilityCustomDetailMapping::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = FacilityCustomDetailMapping::with('facility_custom_detail', 'facility_type', 'facility_ownership')->orderBy('created_at', 'asc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        $message = ["trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function restore($id) {
        FacilityCustomDetailMapping::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        FacilityCustomDetailMapping::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = FacilityCustomDetailMapping::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function loadFacilityCustomDetails(Request $request) {
        $facility_type_id = $request->facility_type_id;
        $facility_ownership_id = $request->facility_ownership_id;
        $all = FacilityCustomDetailMapping::with('facility_custom_detail')
            ->where('facility_type_id', $facility_type_id)
            ->where('facility_ownership_id', $facility_ownership_id)
            ->get();
        $data = ['facilityCustomDetails' => $all];
        return response()->json($data, 200);
    }
}
