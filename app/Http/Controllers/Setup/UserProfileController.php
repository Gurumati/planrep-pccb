<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;


class UserProfileController extends Controller
{

    public function get(){
        $user = User::where('id',UserServices::getUser()->id)->select('id','user_name','first_name','last_name','email','cheque_number','profile_picture_url')->first();
        $user->key_values=DB::table('user_profile_keys as upk')->
                              get();
        foreach ($user->key_values as &$keyObj){
            $valueObj=DB::table('user_profiles')->where('user_id',$user->id)->where('user_profile_key_id',$keyObj->id)->first();
            if($valueObj == null){
                DB::table('user_profiles')->insert(array(
                    "user_id"=>$user->id,
                    "user_profile_key_id"=>$keyObj->id
                ));
                $keyObj->value=null;
            }
            else{
                $keyObj->value= $valueObj->value;
            }
        }
        return response()->json($user);
    }

    public function update(Request $request){

        $userData = json_decode($request->getContent());
        $user = User::find(UserServices::getUser()->id);

        $user->user_name=$userData->user_name;
        $user->first_name=$userData->first_name;
        $user->last_name=$userData->last_name;
        $user->user_name=$userData->user_name;
        $user->cheque_number=$userData->cheque_number;

        $user->save();
        $message=['successMessage'=>"SUCCESSFUL_UPDATE_PROFILE"];
        return response()->json($message);

    }

    public function uploadProfilePicture(Request $request) {
        $user = User::find(UserServices::getUser()->id);

        $file = Input::file('file_to_upload');
        $extension='.'.$file->getClientOriginalExtension();
        $dir = 'uploads/profile_pictures/';
        $picture_url = '/' . $dir . $user->user_name.$extension;
        $user->profile_picture_url = $picture_url;
        $file->move($dir, $user->user_name.$extension);
        $user->save();
        $message=['successMessage'=>"SUCCESSFUL_UPDATE_PROFILE_PICTURE",'profile_picture_url'=>$picture_url];
        return response()->json($message);
    }
}
