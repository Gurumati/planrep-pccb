<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\GfsCodeSubCategory;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class GfsCodeSubCategoryController extends Controller
{
    public function index() {
        $all = GfsCodeSubCategory::with('gfs_code_category')->orderBy('created_at','desc')->get();
        return apiResponse(200,"Success",$all,true,[]);
    }

    public function getAllPaginated($perPage) {
        return GfsCodeSubCategory::with('gfs_code_category')->orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $request->perPage = 300;
        $all = $this->getAllPaginated($request->perPage);
        return apiResponse(200,"Success",$all,true,[]);
    }

    public function store(Request $request) {

        $data = $request->all();
        $validator = Validator::make($data, GfsCodeSubCategory::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $all = $this->getAllPaginated($request->perPage);
            return apiResponse(500, "Failed", $all, false, $errors->all());
        } else {
            GfsCodeSubCategory::create($data);
            $all = $this->getAllPaginated($request->perPage);
            return apiResponse(201, "Success", $all, true, []);
        }
    }

    public function update(Request $request) {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, GfsCodeSubCategory::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $all = $this->getAllPaginated($request->perPage);
            return apiResponse(500, "Failed", $all, false, $errors->all());
        } else {
            $obj = GfsCodeSubCategory::find($request->id);
            $obj->update($data);
            $all = $this->getAllPaginated($request->perPage);
            return apiResponse(201, "Success", $all, true, []);
        }
    }

    public function delete($id) {
        $obj = GfsCodeSubCategory::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        return apiResponse(200, "Success", $all, true, []);
    }

    public function filter($id){
        $all = GfsCodeSubCategory::where('gfs_code_category_id',$id)->get();
        return apiResponse(200, "Success", $all, true, []);
    }

    public function upload() {
        $gfs_code_category_id = Input::get('gfs_code_category_id');
        if (Input::hasFile('file')) {
            $path = Input::file('file')->getRealPath();
            $data = Excel::selectSheets('GFS_CODE_SUB_CATEGORIES')->load($path, function ($reader) {})->get();
            if (!empty($data) && $data->count() > 0) {
                foreach ($data as $key => $value) {
                    $name = $value->name;
                    $count = GfsCodeSubCategory::where('name', $name)->where('gfs_code_category_id',$gfs_code_category_id)->count();
                    if ($count == 1) {
                        continue;
                    }
                    $insert[] = [
                        'name' => $value->name,
                        'gfs_code_category_id' => $gfs_code_category_id,
                    ];
                }
                if (!empty($insert)) {
                    DB::table('gfs_code_sub_categories')->insert($insert);
                    $all = $this->getAllPaginated(Input::get('perPage'));
                    return apiResponse(201, "Success", $all, true, []);
                } else {
                    $all = $this->getAllPaginated(Input::get('perPage'));
                    return apiResponse(500, "Failed", $all, false, []);
                }
            } else {
                $all = $this->getAllPaginated(Input::get('perPage'));
                return apiResponse(500, "Specified file has no data", $all, false, []);
            }
        } else {
            $all = $this->getAllPaginated(Input::get('perPage'));
            return apiResponse(500, "Please select file", $all, false, []);
        }
    }

    public function toggleProcurable(Request $request)
    {
        $object = GfsCodeSubCategory::find($request->id);
        $object->is_procurement = $request->is_procurement;
        $object->save();
        if ($request->is_procurement == false) {
            return apiResponse(201, "Non-Procurable", [], true, []);
        } else {
            return apiResponse(201, "Procurable", [], true, []);
        }
    }
}
