<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\Period;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PeriodController extends Controller
{


    public function getAllPaginated($perPage, $financialYearId)
    {

        if (is_null($financialYearId)) {
            $period = Period::with('financial_year', 'period_group')->
            whereHas('financial_year', function ($q) {
                $q->whereIn('status', [1, 2, 3])->orderBy('status');
            })->
            orderBy('start_date', 'asc')->paginate($perPage);
        } else {
            $period = Period::with('financial_year', 'period_group')->
            whereHas('financial_year', function ($q) use ($financialYearId) {
                $q->where('id', $financialYearId)->orderBy('status');
            })->
            orderBy('start_date', 'asc')->paginate($perPage);
        }

        return $period;
    }

    public function index()
    {
        $financial_year = FinancialYearServices::getPlanningFinancialYear();
        if ($financial_year == null) {
            $financial_year = FinancialYearServices::getExecutionFinancialYear();
        }
        $financial_year_id = $financial_year->id;
        $all = Period::with('financial_year')
            ->where('financial_year_id', $financial_year_id)
            ->orderBy('start_date', 'asc')->get();
        return response()->json($all);
    }

    public function periodsByFinancialYear($financial_year_id){
        $periods = Period::where('financial_year_id','=',$financial_year_id)
                        // ->where('code','<>','ANNUAL')
                         ->orderBy('id','asc')
                         ->get();
        return customApiResponse($periods,'',200,'SUCCESS');
    }

    public function executionPeriods(Request $request)
    {
        $financial_year_id = $request->financialYearId;
        //$financial_year_id = ConfigurationSetting::getValueByKey('AUTO_FINANCIAL_YEAR_TO_REPORT');;
        $all = Period::where('financial_year_id', $financial_year_id)
            ->where('id','<>',5)
            ->orderBy('start_date', 'asc')->get();
        return response()->json(['periods' => $all]);
    }

    public function paginated(Request $request)
    {
        $all = $this->getAllPaginated($request->perPage, $request->financialYearId);
        return response()->json($all);
    }

    public function store(Request $request)
    {
        $data = json_decode($request->getContent());
        $period = new Period();
        $period->name = $data->name;
        $period->start_date = $data->start_date;
        $period->end_date = $data->end_date;
        $period->financial_year_id = $data->financial_year_id;
        $period->sort_order = $data->sort_order;
        $period->is_active = true;
        $period->code = $data->code;
        $period->period_group_id = $data->period_group_id;
        $period->created_by = UserServices::getUser()->id;
        $period->created_at = date('Y-m-d H:i:s');
        $period->save();
        $all = $this->getAllPaginated($request->perPage, $request->financialYearId);
        $message = ["successMessage" => "SUCCESSFUL_PERIOD_CREATED", "periods" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request)
    {
        $data = json_decode($request->getContent());
        $period = Period::find($data->id);
        $period->name = $data->name;
        $period->start_date = $data->start_date;
        $period->end_date = $data->end_date;
        $period->code = $data->code;
        $period->financial_year_id = $data->financial_year_id;
        $period->sort_order = $data->sort_order;
        $period->period_group_id = $data->period_group_id;
        $period->save();
        $all = $this->getAllPaginated($request->perPage, $request->financialYearId);
        $message = ["successMessage" => "SUCCESSFUL_PERIOD_UPDATED", "periods" => $all];
        return response()->json($message, 200);
    }

    public function togglePeriod(Request $request)
    {
        //
        $data = json_decode($request->getContent());
        $period = Period::find($data->id);
        $period->is_active = $data->is_active;
        $period->save();
        $all = $this->getAllPaginated($request->perPage, $request->financialYearId);
        if ($data->is_active == false) {
            //Return language success key and all data
            $feedback = ["action" => "PERIOD_DEACTIVATED", "alertType" => "warning", "periods" => $all];
        } else {
            //Return language success key and all data
            $feedback = ["action" => "PERIOD_ACTIVATED", "alertType" => "success", "periods" => $all];
        }
        return response()->json($feedback, 200);
    }

    public function delete(Request $request, $id)
    {
        try {
            DB::table('periods')->where('id', $id)->delete();
            $all = $this->getAllPaginated($request->perPage, $request->financialYearId);
            $message = ["successMessage" => "SUCCESSFULLY_PERIOD_DELETED", "periods" => $all];
            return response()->json($message, 200);
        } catch (QueryException $e) {
            $message = ["errorMessage" => "ERROR_ON_DELETE"];
            return response()->json($message, 400);
        }

    }

    public function allPeriods()
    {
        $financial_year = FinancialYearServices::getPlanningFinancialYear();
        $all = Period::find($financial_year->id)->periods;
        return response()->json($all);
    }

    private function allTrashed()
    {
        $all = Period::with('financial_year')->orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed()
    {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id)
    {
        Period::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERIOD_RESTORED", "trashedPeriods" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id)
    {
        Period::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERIOD_DELETED_PERMANENTLY", "trashedPeriods" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash()
    {
        $trashes = Period::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERIOD_DELETED_PERMANENTLY", "trashedPeriods" => $all];
        return response()->json($message, 200);
    }

    public function getByActivity($activityId)
    {
        return ['periods' => Period::getByActivity($activityId)];
    }

    public function getByFinancialYearId(Request $request)
    {
        $financial_year_id = $request->financialYearId;
        $all = Period::where('financial_year_id', $financial_year_id)
            ->orderBy('start_date', 'asc')->get();
        return response()->json(["items" => $all], 200);
    }
}
