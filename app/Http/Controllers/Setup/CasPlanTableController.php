<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\CasPlan;
use App\Models\Setup\CasPlanContent;
use App\Models\Setup\CasPlanTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Psr\Log\NullLogger;


class CasPlanTableController extends Controller
{
    public function index(){
        $all = CasPlanTable::all();
        return response()->json($all);
    }

    public function indexPaginated(Request $request){
	    $id = $request->cas_plan_id;
	    $query_string = "select cas_plan_tables.id, cas_plan_tables.cas_plan_content_id, cas_plan_tables.description,cas_plan_tables.name,
                         cas_plan_tables.type, cas_plan_tables.report_url, cas_plan_tables.parameters, cas_plan_tables.is_html,
                         cas_plan_tables.default_values::json FROM cas_plan_tables
                         WHERE cas_plan_content_id in (select id from cas_plan_contents where cas_plan_id = ".$id.") order by name";
	    $all = DB::select(DB::raw($query_string));
	    $all = paginate_this($all, $request->perPage);
	    return response()->json($all);
    }

    public function getAllPaginated($perPage, $cas_plan_id) {
        $plan_content = $this->planContents($cas_plan_id);
        $all = CasPlanTable::whereIn('cas_plan_content_id',$plan_content)->orderBy('name')->paginate($perPage);
        return $all;
    }

    public function getAllCasPlanTables(){
        $cas_plan = CasPlan::get();
        return response()->json($cas_plan);
    }


    public function store(Request $request){
        try{
            $data = json_decode($request->getContent());
            $cas_plan_table = new CasPlanTable();
            $cas_plan_table->cas_plan_content_id = $data->cas_plan_content_id;
            $cas_plan_table->description = $data->name;
            $cas_plan_table->report_url = isset($data->report_url)?$data->report_url:null;
            $cas_plan_table->parent_id = isset($data->parent_id)?$data->parent_id:null;
            $cas_plan_table->name = $data->name;
            $cas_plan_table->type = $data->type;
            $cas_plan_table->parameters = isset($data->parameters)?$data->parameters:null;
            $cas_plan_table->is_html = isset($data->is_html)?$data->is_html:false;
            $cas_plan_table->default_values = isset($data->default_values)?$data->default_values:null;
            $cas_plan_table->created_by = UserServices::getUser()->id;
            $cas_plan_table->is_periodic = isset($data->is_periodic)?$data->is_periodic : false;
            $cas_plan_table->save();
            $plan_content = $this->planContents($data->cas_plan_id);
            $all = CasPlanTable::whereIn('cas_plan_content_id',$plan_content)->paginate($request->perPage);

            $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_CREATED", "planTables" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request){
        $data = json_decode($request->getContent());
        try{
            $cas_plan_table = CasPlanTable::find($data->id);
            $cas_plan_table->cas_plan_content_id = $data->cas_plan_content_id;
            $cas_plan_table->description = $data->name;
            $cas_plan_table->name = $data->name;
            $cas_plan_table->report_url = isset($data->report_url)?$data->report_url:null;
            $cas_plan_table->parent_id = isset($data->parent_id)?$data->parent_id:null;
            $cas_plan_table->type = $data->type;
            $cas_plan_table->parameters = isset($data->parameters)?$data->parameters:null;
            $cas_plan_table->default_values = isset($data->default_values)?$data->default_values:null;
            $cas_plan_table->is_html = $data->is_html;
            $cas_plan_table->updated_by = UserServices::getUser()->id;
            $cas_plan_table->is_periodic = isset($data->is_periodic)?$data->is_periodic:false;
            $cas_plan_table->save();
            $plan_content = $this->planContents($data->cas_plan_id);
            $all = CasPlanTable::whereIn('cas_plan_content_id',$plan_content)->orderBy('name')->paginate($request->perPage);

            $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_UPDATED", "planTables" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function planContents($cas_plan_id)
    {

        $plan_content = DB::table('cas_plan_contents')->where('cas_plan_id',$cas_plan_id)->select('id')->get();
        $array = array();
        foreach ($plan_content as $key=>$value)
        {
            $array[] = $value->id;
        }
        return $array;
    }

    public function getPlanTables($cas_plan_table_content_id)
    {
        $cas_plan_table = CasPlanTable::where('cas_plan_content_id',$cas_plan_table_content_id)
                ->where('deleted_at',null)
                ->get();
        //check if table has parent

        if($cas_plan_table[0]->parent_id !== null){
            $cas_plan_table = CasPlanTable::where('id',$cas_plan_table[0]->parent_id)->get();
        }
        return response()->json(['cas_plan_table'=>$cas_plan_table], 200);
        //return response()->json($cas_plan_table);
    }

    public function delete($id, $cas_plan_table_id){
        DB::table('cas_plan_tables')->where([
            'id' => $id
        ])->delete();

        $all = $this->getAllPaginated(Input::get('perPage'), $cas_plan_table_id);
        $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_DELETED", "planTables" => $all];
        return response()->json($message, 200);
    }

    public function searchByName(Request $request)
    {
        $key = $request->searchQuery;
        $cas_plan_id = $request->cas_plan_id;
        $key = isset($key)?"'%".strtolower($key)."%'":"'%'";

        $query = "select cas_plan_tables.id, cas_plan_tables.cas_plan_content_id, cas_plan_tables.description,cas_plan_tables.name,
                    cas_plan_tables.type, cas_plan_tables.report_url, cas_plan_tables.parameters, cas_plan_tables.is_html,
                    cas_plan_tables.default_values::json FROM cas_plan_tables
                    WHERE id in (select id from cas_plan_contents where
                    cas_plan_id = ".$cas_plan_id." and LOWER(name) like ".$key.")";
        $all = DB::select(DB::raw($query));
        $all = paginate_this($all, 10);
        return response()->json($all);
    }
}
