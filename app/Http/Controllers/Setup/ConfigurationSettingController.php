<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\ConfigurationSetting;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @resource ConfigurationSettingController
 *
 * This controller manages configuration settings: i.e key:value
*/
class ConfigurationSettingController extends Controller
{
    /**
     * index()
     *
     * This method return all configuration settings by calling getAll() method
     */
    public function index()
    {
        return response()->json($this->getAll());
    }
    /**
     * getAll()
     *
     * This method query all configuration settings and prepare multi select values
    */
    public function getAll(){
        $configurations = ConfigurationSetting::where('is_configurable',true)->get();
        foreach ($configurations as &$config){
            if($config->value_options_query != null) {
                $q = strToLower($config->value_options_query);
                if (strpos($q, 'delete') !== false || strpos($q, 'drop') !== false || strpos($q, 'trancate') !== false) {
                    $config->query_options = [];
                }
                else{
                    try {
                        $config->query_options = DB::select($config->value_options_query);
                    }catch (QueryException $exception){

                    }
                }
            }
            if($config->value_type=="MULT_SELECT")
            {
                $config->value= explode(',', $config->value);
            }
        }
        return $configurations;
    }

    /**
     * store()
     *
     * This method store configuration settings
     */
    public function store(Request $request)
    {
        try {
            $data = json_decode($request->getContent());
            $config = new ConfigurationSetting();
            $config->key=$data->key;
            $config->value=(isset($data->value) && !empty($data->value))?$data->value:null;
            $config->group_name=$data->group_name;
            $config->name=$data->name;
            $config->description=$data->description;
            $config->sort_order=$data->sort_order;
            $config->value_type=$data->value_type;
            $config->value_options=isset($data->value_options)?$data->value_options:null;
            $config->value_options_query=isset($data->value_options_query)?$data->value_options_query:null;
            $config->is_cofigurable=isset($data->is_cofigurable)?$data->is_cofigurable:true;
            $config->save();
        }catch (QueryException $exception){
            Log::error($exception);

        }
    }

    /**
     * update()
     *
     * This method update configuration settings
     */
    public function update(Request $request)
    {
        $data = json_decode($request->getContent());

        foreach ($data as $c) {
            $value="";
            if($c->value_type =='MULT_SELECT')
            {
                foreach ($c->value as $k=>$v){
                    $value.=$v;
                    if($k != sizeof($c->value)-1){
                        $value.=",";
                    }
                }
            }
            else{
                $value=$c->value;
            }
            $config = ConfigurationSetting::find($c->id);
            $config->value=(isset($value) && !empty( $value))? $value:null;
            $config->save();
        }
        $message = ["successMessage" => "SUCCESSFUL_UPDATE_CONFIG", "configs" => $this->getAll()];
        return response()->json($message, 200);

    }

    public function refreshMaterializedView($viewName) {
        return ['status'=>ConfigurationSetting::refreshMaterialView($viewName)];
    }

}
