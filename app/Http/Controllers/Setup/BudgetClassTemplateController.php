<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\BudgetClassTemplate;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BudgetClassTemplateController extends Controller {
    public function index($budget_class_id) {
        $budgetTemplate = BudgetClassTemplate::where('budget_class_id', $budget_class_id)->get();
        $message = ['budgetTemplates' => $budgetTemplate];
        return response()->json($message);
    }

    public function storeTemplate(Request $request) {
        $data = json_decode($request->getContent());
        try {
            if (!isset($data->id)) {
                $budgetTemplate = new BudgetClassTemplate(); //create new object for insert
            } else {
                $budgetTemplate = BudgetClassTemplate::find($data->id); //find object to update
            }
            $budgetTemplate->code = $data->code;
            $budgetTemplate->type = $data->type;
            $budgetTemplate->description = $data->description;
            $budgetTemplate->budget_class_id = $data->budget_class_id;
            $budgetTemplate->save();
            //Return language success key and all data
            $budgetTemplate = BudgetClassTemplate::where('budget_class_id', $data->budget_class_id)->get();
            $message = ["successMessage" => "SUCCESSFUL_BUDGET_TEMPLATE_UPDATED", 'budgetTemplates' => $budgetTemplate];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function loadTemplate($type, $budget_class_id) {
        $budgetTemplate = BudgetClassTemplate::where('type', $type)->where('budget_class_id', $budget_class_id)->get();
        return response()->json($budgetTemplate);
    }

    public function deleteTemplate($id) {
        try {
            $budgetTemplate = BudgetClassTemplate::find($id); //find object to update
            $budget_class_id = $budgetTemplate->budget_class_id;
            $budgetTemplate->delete();

            $budgetTemplate = BudgetClassTemplate::where('budget_class_id', $budget_class_id)->get();
            $message = ["successMessage" => "SUCCESSFUL_BUDGET_TEMPLATE_UPDATED", 'budgetTemplates' => $budgetTemplate];
            return response()->json($message, 200);

        } catch (QueryException $e) {
            $message = ["errorMessage" => 'FAILED_TO_DELETE'];
            return response()->json($message, 400);
        }
    }
}
