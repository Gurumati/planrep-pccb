<?php
namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\FeatureNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class FeatureNotificationController extends Controller {

    /**
     * Paginate feature notifications
     */
    public function paginate() {
      $perPage = Input::get('perPage', 10);
      return FeatureNotification::orderBy('id','desc')->paginate($perPage);
    }

    /**
     * Get feature notification which has not experied
     */
    public function getActive() {
        $now = date('Y-m-d');
        return ['notifications'=> FeatureNotification::whereNull('expiration_date')->orWhere('expiration_date', '>', $now)->orderBy('id','desc')->get()];
    }

    /**
     * Create or update notification
     */
    public function update(Request $request) {
        $data = $request->all();
        $notF = null;
        if (isset($data['id'])) {
            $notF = FeatureNotification::find($data['id']);
            $notF->name = $data['name'];
            $notF->url = $data['url'];
            $notF->expiration_date = $data['expiration_date'];
        } else {
            $notF = new FeatureNotification($data);
        }
        $notF->save();
        return ['successMessage'=>'Notification upadated successfully'];
    }

    /**
     * Delete notifcation
     */
    public function delete($id) {
        FeatureNotification::find($id)->delete();
    }
}

