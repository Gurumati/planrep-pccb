<?php

namespace App\Http\Controllers\Setup;

use App\Models\Setup\Project;
use App\Models\Setup\ProjectDataForm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;


class ProjectDataFormController extends Controller
{
    public function index(){
        $projectDataForms = ProjectDataForm::with('sectors')->with('projects')->get();
        return response()->json($projectDataForms);
    }

    public function store(Request $request){
        try {
            $data = json_decode($request->getContent());
            $form = new ProjectDataForm();
            $form->name = $request->name;
            $form->sector_id = isset($request->sector_id) ? $request->sector_id : null;
            $form->project_id = isset($request->project_id) ? $request->project_id : null;
            $form->save();

            $all = ProjectDataForm::with('sectors')->with('projects')->get();
            $message = ["successMessage" => "SUCCESSFUL_PROJECT_CREATED", "projectDataForms" => $all];
            return response()->json($message, 200);
        }

        catch (QueryException $e){
                $message = ["errorMessage" =>$e->getMessage()];
                return response()->json($message);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        $form = ProjectDataForm::find($request->id);
        $form->name = $request->name;
        $form->sector_id = isset($request->sector_id) ? $request->sector_id : null;
        $form->project_id = isset($request->project_id) ? $request->project_id : null;
        $form->save();
        $all = ProjectDataForm::with('sectors')->with('projects')->get();
        $message = ["successMessage" => "SUCCESSFUL_PROJECT_UPDATED", "projectDataForms" => $all];
        return response()->json($message, 200);
    }

    public function delete($id) {
        $form = ProjectDataForm::find($id);
        $form->delete();
        $all = ProjectDataForm::with('sectors')->with('projects')->get();
        $message = ["successMessage" => "SUCCESSFULLY_FORM_DELETED", "projectDataForms" => $all];
        return response()->json($message, 200);
    }
}
