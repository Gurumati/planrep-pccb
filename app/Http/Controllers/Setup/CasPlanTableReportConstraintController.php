<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\CasPlanTableReportConstraint;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CasPlanTableReportConstraintController extends Controller
{
    public function getCasPlanTableConstraints(Request $request){
        $all = CasPlanTableReportConstraint::where('cas_plan_table_id',$request->id)->first();
        return response()->json($all);
    }

    public function store(Request $request){
        //Read the JSON object/array in use
        $data = json_decode($request->getContent());
        $limit = $data->limit;
        $cas_plan_table_id = $data->cas_plan_table_id;
        $order_by = $data->order_by;
        $with_value = $data->with_value;

       try {
            //Check if the constraints of the table exist
            $check = CasPlanTableReportConstraint::where('cas_plan_table_id',$cas_plan_table_id)->first();


            if ($check == null){
                //Insert the data
                DB::table('cas_plan_table_report_constraints')->insert(
                    ['limit' => $limit, 'order_by' => $order_by, 'with_value' => $with_value, 'cas_plan_table_id'=> $cas_plan_table_id, 'created_by'=> UserServices::getUser()->id ]
                );

                //Send response to the user
                $constraints = CasPlanTableReportConstraint::where('cas_plan_table_id',$cas_plan_table_id);
                $message = ["successMessage" => "DATA_SAVED_SUCCESSFULLY","constraints"=>$constraints];
                return response()->json($message);

            } else {
                //update the data
                DB::table('cas_plan_table_report_constraints')
                    ->where('cas_plan_table_id', $cas_plan_table_id)
                    ->update(['limit' => $limit, 'order_by' => $order_by, 'with_value' => $with_value, 'update_by'=> UserServices::getUser()->id]);

                //Send response to the user
                $constraints = CasPlanTableReportConstraint::where('cas_plan_table_id',$cas_plan_table_id)->with('casPlanTable')->get();
                $message = ["successMessage" => "DATA_UPDATED_SUCCESSFULLY","constraints"=>$constraints];
                return response()->json($message);
            }

        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_DATA_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

}
