<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Objective;
use Illuminate\Http\Request;

class ObjectiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $objectives = Objective::with('section')->get();
        return response()->json($objectives);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $data = json_decode($request->getContent());
            $objective = new Objective();
            $objective->name = $data->name;
            $objective->code = $data->code;
            $objective->description = $data->description;
            $objective->section_id = $data->section_id;
            $objective->is_active = TRUE;
            $objective->sort_order = $data->sort_order;
            $objective->save();
            //Return language success key and all data
            $message = ["successMessage" => "SUCCESSFUL_OBJECTIVE_CREATED", "objectives" => Objective::with('section')->get()];
            return response()->json($message, 200);

        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_OBJECTIVE_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        //
        try{
            $data = json_decode($request->getContent());
            $objective = Objective::find($data->id);
            $objective->name = $data->name;
            $objective->code = $data->code;
            $objective->description = $data->description;
            $objective->section_id = $data->section_id;
            $objective->is_active = TRUE;
            $objective->sort_order = $data->sort_order;
            $objective->save();
            //Return language success key and all data
            $message = ["successMessage" => "SUCCESSFUL_OBJECTIVE_UPDATED", "objectives" => Objective::with('section')->get()];
            return response()->json($message, 200);
        } catch (QueryException $exception){
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR__EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        //
        $objective = Objective::find($id);
        $objective->delete();
        //Return language success key and all data
        $message = ["successMessage" => "SUCCESSFUL_OBJECTIVE_DELETED", "objectives" => Objective::with('section')->get()];
        return response()->json($message, 200);

    }

    public function activate(Request $request)
    {
        try{
            $data = json_decode($request->getContent());
            $objective = Objective::find($data->id);
            $objective->is_active = $data->is_active;
            $objective->save();
            //Return language success key and all data
            if($objective->is_active)
            {
                $message = ["activateMessage" => "SUCCESSFUL_OBJECTIVE_ACTIVATED", "objectives" => Objective::with('section')->get()];
            }else
            {
                $message = ["activateMessage" => "SUCCESSFUL_OBJECTIVE_DEACTIVATED", "objectives" => Objective::with('section')->get()];
            }
            return response()->json($message, 200);
        } catch (QueryException $exception){
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_OBJECTIVE_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

}
