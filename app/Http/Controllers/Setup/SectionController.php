<?php

namespace App\Http\Controllers\Setup;

use AdminHierarchySection;
use App\Models\Setup\AdminHierarchyLevel;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\UserServices;
use App\Models\Setup\Section;
use App\Models\Setup\SectionLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\Environment\Console;

/**
 * @resource Section
 *
 * SectionController: This controller manages sections(Planning Units) i.e Division within admin hierarchy
 *
 */
class SectionController extends Controller
{

    private $limit = 2;

    /**
     * index()
     * This method return sections with children
     */
    public function index()
    {
        $query = "select json_agg(json_build_object('id',s.id,'code',s.code,'name',s.name,'parent_id',s.parent_id,'section_level_id',s.section_level_id,'sector_id',s.sector_id,'sort_order',s.sort_order,'child_sections',
                (select json_agg(json_build_object('id',s1.id,'name',s1.name,'code',s1.code,'parent_id',s1.parent_id,'section_level_id',s1.section_level_id,'sector_id',s1.sector_id,'sort_order',s1.sort_order,'child_sections',
                (select json_agg( jsonb_build_object('id',id,'code',code,'name',name,'parent_id',parent_id,'sector_id', sector_id,'child_sections',
                (select json_agg(json_build_object('id',s2.id,'code',s2.code,'name',s2.name,'parent_id',s2.parent_id,'section_level_id',s2.section_level_id,'admin_id',t.id,'sector_id',s2.sector_id,'sort_order',s2.sort_order,'child_sections',
                (select json_agg(json_build_object('id',s3.id,'code',s3.code,'name',s3.name,'parent_id',s3.parent_id,'section_level_id',s3.section_level_id,'admin_id',t.id,'sector_id',s3.sector_id,'sort_order',s3.sort_order)) from sections s3
                where s3.parent_id = s2.id)))from sections s2
                inner join admin_hierarchy_sections ahs on ahs.section_id = s2.id
                inner join section_levels sl on s2.section_level_id = sl.id
                where sl.hierarchy_position = 3 and ahs.admin_hierarchy_id = t.id ))) from
                (select DISTINCT ah.*,s4.sector_id from admin_hierarchies ah
                inner join admin_hierarchy_sections ahs on ahs.admin_hierarchy_id = ah.id
                join sections s4 on s4.id = ahs.section_id where s4.sector_id = s1.sector_id) as t)))
                from sections s1 where s1.parent_id = s.id))) from sections s
                join section_levels l on s.section_level_id = l.id
                where l.hierarchy_position = (select MIN(hierarchy_position) from section_levels)";
        $sections = DB::select(DB::raw($query));
        return response()->json(json_decode($sections[0]->json_agg));
    }

    /**
     * sectionsToActivate()
     * This method return sections to be activated as object
     */
    public function sectionsToActivate()
    {
        $query = "select json_agg(json_build_object('id',s.id,'code',s.code,'name',s.name,'parent_id',s.parent_id,'section_level_id',s.section_level_id,'sector_id',s.sector_id,'sort_order',s.sort_order,'child_sections',
                    (select json_agg( jsonb_build_object('id',id,'code',code,'name',name,'parent_id',parent_id,'sector_id', sector_id,'child_sections',
                        (select json_agg(json_build_object('id',s3.id,'code',s3.code,'name',s3.name,'pisc',t.id,'pisc_name',t.name)) from sections s3
                            inner join admin_hierarchy_sections ahs on ahs.section_id = s3.id
                            inner join section_levels sl on s3.section_level_id = sl.id
                            where sl.hierarchy_position = 4 and ahs.admin_hierarchy_id = t.id
                            and s3.id not in (select section_id from mtef_sections)))) from
                    (select DISTINCT ah.*,s4.sector_id from admin_hierarchies ah
                    inner join admin_hierarchy_sections ahs on ahs.admin_hierarchy_id = ah.id
                    join sections s1 on s1.parent_id = 1
                    join sections s4 on s4.id = ahs.section_id where s4.sector_id = s1.sector_id and ah.id in
                    (select distinct ahs.admin_hierarchy_id from sections s
                    join admin_hierarchy_sections ahs on s.id = ahs.section_id
                    where s.id not in (select section_id from mtef_sections) and s.section_level_id = 4))as t))) from sections s
                    join section_levels l on s.section_level_id = l.id
                    where l.hierarchy_position = (select MIN(hierarchy_position) from section_levels)";
        $sections = DB::select(DB::raw($query));
        return response()->json(json_decode($sections[0]->json_agg));
    }

    public function activateSections(Request $request)
    {
        $mtef_id = null;
        $financial_year = DB::table('financial_years')->where('status', 1)->select('id')->get();
        
        $pisc = DB::table('mtefs')
            ->where('admin_hierarchy_id', $request->admin_hierarchy_id)
            ->get();

        $new_mtef_id = DB::table('mtefs')
            ->where('admin_hierarchy_id', $request->admin_hierarchy_id)
            ->where('financial_year_id', $financial_year[0]->id)
            ->select('id')
            ->get();
    
        if (sizeof($pisc) > 0) {
            $mtef_id = $new_mtef_id[0]->id;
        } else {
            if (sizeof($financial_year) > 0) {
                $mtef = DB::table('mtefs')->insert([
                    'admin_hierarchy_id' => $request->admin_hierarchy_id,
                    'financial_year_id' => $financial_year[0]->id,
                    'decision_level_id' => 4,
                    'locked' => false,
                    'is_final' => false
                ]);
                $mtef_id = $mtef->id;
            } else {
                return response()->json(["errorMessage" =>"no active financial year","sections"=>null]);
            }
        }
        $adminLevel = DB::table('admin_hierarchies as ah')
            ->join('admin_hierarchy_levels as ahl','ah.admin_hierarchy_level_id','ahl.id')
            ->where('ah.id',$request->admin_hierarchy_id)
            ->select('ahl.*')->first();
            
        foreach ($request->cost_center as $section) {
            $costcenter = DB::table('mtef_sections')->where('section_id', $section['id'])->count();
            $sec = Section::find($section['id']);


            $defaultMtefSectionDecisionLevel = DB::table('decision_levels')
            ->where('admin_hierarchy_level_position', $adminLevel->hierarchy_position)
            ->where('section_level_id', $sec->section_level_id)
            ->where('is_default', true)->first();
            if ($costcenter == 0) {
                DB::table('mtef_sections')->insert([
                    'mtef_id' => $mtef_id,
                    'section_id' => $section['id'],
                    "decision_level_id" =>  $defaultMtefSectionDecisionLevel->id
                ]);
        
            }

            for($admin_hierarchy_level_id=1;$admin_hierarchy_level_id<=4;$admin_hierarchy_level_id++){

                if($admin_hierarchy_level_id==4){
                    DB::table('admin_hierarchy_lev_sec_mappings')->insert([
                        'admin_hierarchy_level_id' => $admin_hierarchy_level_id,
                        'section_id' => $section['id'],
                        'can_budget' => true
                    ]);
                }else{
                    DB::table('admin_hierarchy_lev_sec_mappings')->insert([
                        'admin_hierarchy_level_id' => $admin_hierarchy_level_id,
                        'section_id' => $section['id'],
                        'can_budget' => false
                    ]);
                }
            }
        }
        $all = $this->sectionsToActivate();
        $message = ["successMessage" => "Section activated successfully", "sections" => $all];
        return response()->json($message);
    }

    /**
     * fetchAll()
     * This method return sections as object
     */
    public
    function fetchAll()
    {
        $section = Section::orderBy("created_at", "asc")->get();
        return response()->json(["costCentres" => $section], 200);
    }

    /**
     * planningUnits()
     * This method return all sections by section levels
     */
    public
    function planningUnits(Request $request)
    {
        $section_level_id = $request->section_level_id;
        $section = Section::where('section_level_id', $section_level_id)->orderBy("created_at", "asc")->get();
        return response()->json(["sections" => $section], 200);
    }

    /**
     * paginateSection()
     * This method return paginated sections
     */
    public
    function paginateSection()
    {
        $section = Section::with('childSections')->whereNull('parent_id')->paginate($this->limit);
        return response()->json($section);
    }

    /**
     * allSections()
     * This method return all active sections
     */
    public
    function allSections()
    {
        $all = Section::where('is_active', true)->orderBy('name')->get();
        return response()->json($all);
    }

    /**
     * userTargetingSections()
     * This method return section that can target
     */
    public
    function userTargetingSections()
    {
        $flatten = new Flatten();
        return response()->json($flatten->userTargetingSections());
    }

    /**
     * userCanBudgetSections()
     * This method return user section and children that can budget
     */
    public
    function userCanBudgetSections()
    {
        $data = ['sections' => Flatten::userBudgetingSections()];
        return response()->json($data);
    }

    /**
     * store()
     * This method create new section
     */
    public
    function store(Request $request)
    {
        // $data = json_decode($request->getContent());

        if (isset($request->parent_id) && $request->parent_id != null && $request->parent_id != '') {
            $section = Section::where('name', $request->name)->where('parent_id', $request->parent_id)->count();
        } else {
            $section = Section::where('name', $request->name)->count();
        }


        if ($section == 0) {
            try {
                $section = new Section();
                $section->name = $request->name;
                $section->code = $request->code;
                $section->section_level_id = $request->section_level_id;
                $section->parent_id = (isset($request->parent_id)) ? $request->parent_id : null;
                $section->sector_id = isset($request->sector_id) ? $request->sector_id : null;
                $section->sort_order = $request->sort_order;
                $section->is_active = True;
                $section->save();

                $sectionLevelPosition =DB::table('section_levels')->select('hierarchy_position')->where('id',$request->section_level_id)->first();
                if(in_array($sectionLevelPosition->hierarchy_position, [3,4], true)){
                    DB::table('admin_hierarchy_sections')->insert([
                        'admin_hierarchy_id' => $request->admin_id,
                        'section_id' => $section->id
                    ]);
                }

                //Return language success key and all data
                $sections = $this->sectionsTree();
                $message = ["successMessage" => "SUCCESSFUL_SECTION_CREATED", "sections" => json_decode($sections[0]->json_agg)];
                return response()->json($message, 200);
            } catch (\Exception $exception) {
                $error = $exception->getMessage();
                $message = ["errorMessage" => $error];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => "ERROR_SECTION_EXISTS"];
            return response()->json($message, 400);
        }
    }

    /**
     * update()
     * This method update an existing section
     */
    public
    function update(Request $request)
    {
        if (isset($request->parent_id))
            $section = Section::where('name', $request->name)->where('parent_id', $request->parent_id)->where('id', '!=', $request->id)->count();
        else
            $section = Section::where('name', $request->name)->where('id', '!=', $request->id)->count();

        if ($section == 0) {
            try {
                $section = Section::find($request->id);
                $section->name = $request->name;
                $section->code = $request->code;
                $section->section_level_id = $request->section_level_id;
                $section->parent_id = (isset($request->parent_id)) ? $request->parent_id : null;
                $section->sort_order = $request->sort_order;
                $section->sector_id = isset($request->sector_id) ? $request->sector_id : null;
                $section->is_active = True;
                $section->save();

                $sectionLevelPosition =DB::table('section_levels')->select('hierarchy_position')->where('id',$request->section_level_id)->first();
                if(in_array($sectionLevelPosition->hierarchy_position, [3,4], true)){
                    DB::table('admin_hierarchy_sections')
                    ->where('section_id', $section->id)
                    ->update([
                        'admin_hierarchy_id' => $request->admin_id,
                        'section_id' => $section->id
                    ]);
                }

                //Return language success key and all data
                $sections = $this->sectionsTree();
                $message = ["successMessage" => "SUCCESSFUL_SECTION_UPDATED", "sections" => json_decode($sections[0]->json_agg)];
                return response()->json($message, 200);
            } catch (\Exception $exception) {
                $error = $exception->getMessage();
                $message = ["errorMessage" => $error];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => "ERROR_SECTION_EXISTS"];
            return response()->json($message, 400);
        }
    }

    /**
     * delete()
     * This method delete existing section
     */
    public
    function delete($id)
    {
        $section = Section::find($id);
        $section->delete();
        //Return language success key and all data
        $sections = $this->sectionsTree();
        $message = ["successMessage" => "SUCCESSFUL_SECTION_DELETED", "sections" => json_decode($sections[0]->json_agg)];
        return response()->json($message, 200);
    }

    public function sectionsTree(){
        $query = "select json_agg(json_build_object('id',s.id,'code',s.code,'name',s.name,'parent_id',s.parent_id,'section_level_id',s.section_level_id,'sector_id',s.sector_id,'sort_order',s.sort_order,'child_sections',
        (select json_agg(json_build_object('id',s1.id,'name',s1.name,'code',s1.code,'parent_id',s1.parent_id,'section_level_id',s1.section_level_id,'sector_id',s1.sector_id,'sort_order',s1.sort_order,'child_sections',
         (select json_agg( jsonb_build_object('id',id,'code',code,'name',name,'parent_id',parent_id,'sector_id', sector_id,'child_sections',
        (select json_agg(json_build_object('id',s2.id,'code',s2.code,'name',s2.name,'parent_id',s2.parent_id,'section_level_id',s2.section_level_id,'admin_id',t.id,'sector_id',s2.sector_id,'sort_order',s2.sort_order,'child_sections',
        (select json_agg(json_build_object('id',s3.id,'code',s3.code,'name',s3.name,'parent_id',s3.parent_id,'section_level_id',s3.section_level_id,'admin_id',t.id,'sector_id',s3.sector_id,'sort_order',s3.sort_order)) from sections s3
        where s3.parent_id = s2.id and s3.deleted_at isnull and s2.deleted_at isnull)))from sections s2
        inner join admin_hierarchy_sections ahs on ahs.section_id = s2.id
        inner join section_levels sl on s2.section_level_id = sl.id
        where sl.hierarchy_position = 3 and ahs.admin_hierarchy_id = t.id ))) from
        (select DISTINCT ah.*,s4.sector_id from admin_hierarchies ah
        inner join admin_hierarchy_sections ahs on ahs.admin_hierarchy_id = ah.id
        join sections s4 on s4.id = ahs.section_id where s4.sector_id = s1.sector_id and ah.deleted_at isnull) as t)))
        from sections s1 where s1.parent_id = s.id and s1.deleted_at isnull and s.deleted_at isnull ))) from sections s
        join section_levels l on s.section_level_id = l.id
        where l.hierarchy_position = (select MIN(hierarchy_position) from section_levels)";

        $sections = DB::select(DB::raw($query));
        return $sections;
    }

    /**
     * loadParentSections()
     * This method return parent section of a section
     */
    public
    function loadParentSections($childSectionLevelId)
    {
        $childSectionLevel = SectionLevel::find($childSectionLevelId);

        $parentSections = DB::table('sections')
            ->join('section_levels', 'section_levels.id', '=', 'sections.section_level_id')
            ->select('sections.*')
            ->where('hierarchy_position', '=', $childSectionLevel->hierarchy_position - 1)
            ->get();
        return response()->json($parentSections, 200);
    }

    /**
     * getByLevel()
     * This method return sections by levels
     */
    public
    function getPiscBySector($sectorId)
    {
        $query = "select json_agg( jsonb_build_object('id',id,'code',code,'name',name,'parent_id',parent_id,'sector_id', sector_id)) from
        (select DISTINCT ah.*,s4.sector_id from admin_hierarchies ah
        inner join admin_hierarchy_sections ahs on ahs.admin_hierarchy_id = ah.id
        join sections s4 on s4.id = ahs.section_id where s4.sector_id = $sectorId) as t";
        $piscs = DB::select(DB::raw($query));
        return response()->json(json_decode($piscs[0]->json_agg));
    }

    /**
     * getByLevel()
     * This method return sections by levels
     */
    public
    function getByLevel($levelId, $adminHierarchyId)
    {
        $flatten = new Flatten();

        $userSectionId = UserServices::getUser()->section_id;
        if ($userSectionId != null) {
            $userSections = Section::where('id', $userSectionId)->get();
        } else {
            $userSections = Section::whereNull('parent_id')->get();
        }
        $Ids = $flatten->flattenSectionWithChild($userSections);

        if (UserServices::getUser()->decision_level_id == 4 || UserServices::getUser()->decision_level_id == 3) {
            $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        }

        $sectionLevels = DB::table('section_levels')->where('id',$levelId)->get();

        $position = sizeof($sectionLevels)===1?$sectionLevels[0]->hierarchy_position:0;

        $sections = DB::table('sections as s');

        if(in_array($position,[3,4])){
            $sections->join('admin_hierarchy_sections as ahs', 's.id', '=', 'ahs.section_id');
        }

        $sections->join('section_levels as l', 'l.id', '=', 's.section_level_id')
            ->select('s.*')
            ->where('s.section_level_id', '=', $levelId)
            ->whereIn('s.id', $Ids)
            ->whereNull('s.deleted_at')
            ->where('s.is_active', true);

        if(in_array($position,[3,4])){
            $sections->where('ahs.admin_hierarchy_id', $adminHierarchyId);
        }
        $result=$sections->orderBy('s.name')->get();

        if (sizeof($result) <= 0) {
            $result = DB::table('sections')->where('id', 1)->get();
        }

        return response()->json($result, 200);
    }

    /**
     * getByLevelFilter
     * This method return sections By admin Hierarchy
     */
    public
    function getByLevelFilter($levelId)
    {

        $flatten = new Flatten();
        $userSectionId = UserServices::getUser()->section_id;
        if ($userSectionId != null) {
            $userSections = Section::where('id', $userSectionId)->get();
        } else {
            $userSections = Section::whereNull('parent_id')->get();
        }
        $Ids = $flatten->flattenSectionWithChild($userSections);

        $adminPosition = DB::Table('admin_hierarchies as ah')
            ->join('admin_hierarchy_levels as ahl', 'ahl.id', '=', 'ah.admin_hierarchy_level_id')
            ->where('ah.id', UserServices::getUser()->admin_hierarchy_id)->first()->hierarchy_position;
        $query = DB::table('sections as s')
            ->join('admin_hierarchy_sections as ahs', 's.id', '=', 'ahs.section_id')
            ->join('section_levels as l', 'l.id', '=', 's.section_level_id')
            ->select('s.*')
            ->where('s.section_level_id', '=', $levelId)
            ->whereIn('s.id', $Ids)
            ->whereNull('s.deleted_at')
            ->where('s.is_active', true);
        if ($adminPosition > 3) {
            $query = $query->where('ahs.admin_hierarchy_id', UserServices::getUser()->admin_hierarchy_id);
        }

        $sections = $query->orderBy('s.name')->get();

        if (sizeof($sections) <= 0) {
            $sections = DB::table('sections')->where('id', 1)->get();
        }
        return response()->json($sections, 200);
    }


    /**
     * filter()
     * This method return section by sector
     */
    public
    function filter($id, $sector_id = null)
    {
        if ((int)$sector_id > 0) {
            $sections = Section::where('section_level_id', $id)->where('sector_id', $sector_id)->orderBy('name')->get();
        } else {
            $sections = Section::where('section_level_id', $id)->orderBy('name')->get();
        }
        return response()->json($sections);
    }

     /**
     * getSectionsByAdmin()
     * This method return section by admin Hierarchies
     */
    public
    function getSectionsByAdmin($id,$admin_id)
    {
        $sections = DB::table('sections as s')
        ->join('admin_hierarchy_sections as ahs','s.id','ahs.section_id')
        ->where('s.section_level_id', $id)
        ->where('ahs.admin_hierarchy_id', $admin_id)
        ->select('s.*')
        ->get();
        return response()->json($sections);
    }

    public
    function byFacility($sectionLevelId, $facilityId, $fundSourceId)
    {
        return ['sections' => Section::byFacility($sectionLevelId, $facilityId, $fundSourceId)];
    }

    public
    function sectorCostCentres()
    {
        $userSectionId = UserServices::getUser()->section_id;
        $section = Section::find($userSectionId);
        $sector_id = $section->sector_id;
        $all = Section::where("sector_id", $sector_id)->where("section_level_id", 4)->orderBy('name')->get();
        $data = ['costCentres' => $all];
        return response()->json($data, 200);
    }

    public
    function getBySector($sectorId)
    {
        $sections = DB::table('sections as s')->join('section_levels as sl', 'sl.id', 's.section_level_id')->where('s.sector_id', $sectorId)->orderBy('sl.id', 'asc')->orderBy('s.name', 'asc')->select('s.id', 's.name', 'sl.name as level')->get();

        $data = ['sections' => $sections];
        return response()->json($data, 200);
    }
}
