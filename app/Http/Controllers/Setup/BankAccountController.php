<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\VersionServices;
use App\Models\Setup\BankAccount;
use App\Models\Setup\BankAccountVersion;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Budgeting\Ceiling;

class BankAccountController extends Controller
{
    public function all(){
        return ['bankAccounts'=>BankAccount::select('id','name')->get()];
    }
    public function index(Request $request)
    {
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'BA');
        $items = BankAccount::whereHas("bank_account_versions", function ($version) use ($versionId) {
            $version->where('version_id', $versionId);
        })->orderBy('code', 'asc')
            ->get();
        return apiResponse(200, "Bank Accounts", $items, true, []);
    }

    public function getAllPaginated($versionId, $perPage)
    {
        $items = BankAccount::whereHas("bank_account_versions", function ($version) use ($versionId) {
            $version->where('version_id', $versionId);
        })->orderBy('code', 'asc')
            ->paginate($perPage);
        return $items;
    }

    public function paginated(Request $request)
    {

        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'BA');
        if (isset($request->perPage)) {
            $perPage = $request->perPage;
        } else {
            $perPage = 10;
        }
        $all = $this->getAllPaginated($versionId, $perPage);
        return apiResponse(200, "Bank Accounts", $all, true, []);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, BankAccount::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            return apiResponse(400, "Failed", [], false, $errors->all());
        } else {
            DB::transaction(function () use ($data, $request) {
                $ba = BankAccount::create($data);
                $bav = new BankAccountVersion();
                $bav->bank_account_id = $ba->id;
                $bav->version_id = $request->versionId;
                $bav->save();
            });
            $financialYearId = $this->getFinancialYearId($request);
            $versionId = $this->getVersionId($request, $financialYearId, 'BA');
            if (isset($request->perPage)) {
                $perPage = $request->perPage;
            } else {
                $perPage = 10;
            }
            $all = $this->getAllPaginated($versionId, $perPage);
            return apiResponse(201, "Success", $all, true, []);
        }
    }


    public function update(Request $request)
    {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, BankAccount::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            return apiResponse(400, "Success", [], true, $errors->all());
        } else {
            $obj = BankAccount::find($request->id);
            $obj->update($data);
            $financialYearId = $this->getFinancialYearId($request);
            $versionId = $this->getVersionId($request, $financialYearId, 'BA');
            if (isset($request->perPage)) {
                $perPage = $request->perPage;
            } else {
                $perPage = 10;
            }
            $all = $this->getAllPaginated($versionId, $perPage);
            return apiResponse(201, "Success", $all, true, []);
        }
    }

    public function delete($id, Request $request)
    {
        try {
            DB::transaction(function () use ($id) {
                DB::table('bank_account_versions')->where('bank_account_id', $id)->delete();
                $item = BankAccount::find($id);
                $item->delete();
            });

            if (isset($request->financialYearId)) {
                $financialYearId = $request->financialYearId;
            } else {
                $financialYear = FinancialYearServices::getPlanningFinancialYear();
                if (is_null($financialYear)) {
                    $financialYear = FinancialYearServices::getExecutionFinancialYear();
                }
                $financialYearId = $financialYear->id;
            }

            if (isset($request->versionId)) {
                $versionId = $request->versionId;
            } else {
                $versionId = VersionServices::getVersionId($financialYearId, 'BA');
            }

            if (isset($request->perPage)) {
                $perPage = $request->perPage;
            } else {
                $perPage = 10;
            }
            $all = $this->getAllPaginated($versionId, $perPage);
            return apiResponse(200, "Success", $all, true, []);
        } catch (QueryException $exception) {
            $message = ["errorMessage" => "DATA_IN_USE"];
            return response()->json($message, 500);
        }
    }

    public function copy(Request $request)
    {
        $sourceVersionId = $request->sourceVersionId;
        $destinationVersionId = $request->destinationVersionId;
        $items = BankAccount::whereHas("bank_account_versions", function ($version) use ($sourceVersionId) {
            $version->where('version_id', $sourceVersionId);
        })->orderBy('code', 'asc')
            ->get();
        foreach ($items as $data) {
            DB::transaction(function () use ($data, $destinationVersionId) {
                $obj = new BankAccount();
                $obj->name = $data->name;
                $obj->code = $data->code;
                $obj->save();
                $bank_account_id = $obj->id;

                $inv = new BankAccountVersion();
                $inv->bank_account_id = $bank_account_id;
                $inv->version_id = $destinationVersionId;
                $inv->save();
            });
        }
        return apiResponse(201,"Successfully Copied Bank Accounts",[],true,[]);
    }

    public function balaceAndCeiling($id, $financialYearId, $adminHierarchyId, $sectionId){
        $balance = DB::table('bank_account_balances')
            ->where('bank_account_id',$id)
            ->where('financial_year_id',$financialYearId)
            ->where('admin_hierarchy_id',$adminHierarchyId)
            ->sum('amount');
        
        $query = DB::table('fund_source_budget_classes')->where('bank_account_id',$id);
        $fundIds = $query->pluck('fund_source_id');
        $budgetClassIds = $query->pluck('budget_class_id');

        $ceilings = Ceiling::getExpenditureCeilingByFundAndBudgetClassIds($fundIds, $budgetClassIds);
        foreach($ceilings as &$ceiling){
            $ceiling->amount = DB::table('admin_hierarchy_ceilings')
                ->where('ceiling_id', $ceiling->id)
                ->where('financial_year_id', $financialYearId)
                ->where('admin_hierarchy_id', $adminHierarchyId)
                ->where('section_id', $sectionId)
                ->where('budget_type','CARRYOVER')
                ->sum('amount');
        }
        return ['balance' => $balance, 'ceilings' =>$ceilings];
    }

    public function updateAccountBalance($id, $financialYearId, $adminHierarchyId, $amount){
        
        $exist = DB::table('bank_account_balances')
            ->where('bank_account_id',$id)
            ->where('financial_year_id',$financialYearId)
            ->where('admin_hierarchy_id',$adminHierarchyId);
        if($exist->count() > 0){
          $exist->update(['amount'=>$amount]);
        }
        else{
            DB::table('bank_account_balances')->create([
                'bank_account_id'=> $id,
                'financialYearId'=> $financialYearId,
                'adminHierarchyId'=> $adminHierarchyId,
                'amount'=>$amount
            ]);
        }

        return $exist->first();
    }
}
