<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\ExpenditureCentreLink;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class ExpenditureCentreGfsCodeController extends Controller {
    public function index() {
        $all = ExpenditureCentreLink::with('gfs_code', 'expenditure_centre')->orderBy('created_at', 'desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return ExpenditureCentreLink::with('gfs_code', 'expenditure_centre')->orderBy('created_at', 'desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $obj = new ExpenditureCentreLink();
            $obj->gfs_code_id = $data->gfs_code_id;
            $obj->expenditure_centre_id = $data->expenditure_centre_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_EXPENDITURE_CENTRE_GFS_CODE_CREATED", "expenditureCentreGfsCodes" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try {
            $obj = ExpenditureCentreLink::find($data->id);
            $obj->expenditure_centre_id = $data->expenditure_centre_id;
            $obj->gfs_code_id = $data->gfs_code_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_EXPENDITURE_CENTRE_GFS_CODE_UPDATED", "expenditureCentreGfsCodes" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = ExpenditureCentreLink::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_EXPENDITURE_CENTRE_GFS_CODE_DELETED", "expenditureCentreGfsCodes" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = ExpenditureCentreLink::with('gfs_code', 'expenditure_centre')
            ->orderBy('created_at', 'desc')
            ->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        ExpenditureCentreLink::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "EXPENDITURE_CENTRE_GFS_CODE_RESTORED", "trashedExpenditureCentreGfsCodes" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        ExpenditureCentreLink::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "EXPENDITURE_CENTRE_GFS_CODE_DELETED_PERMANENTLY", "trashedExpenditureCentreGfsCodes" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = ExpenditureCentreLink::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EXPENDITURE_CENTRE_GFS_CODE_DELETED_PERMANENTLY", "trashedExpenditureCentreGfsCodes" => $all];
        return response()->json($message, 200);
    }

    public function gfs_codes($id) {
        $all = ExpenditureCentreLink::where('expenditure_centre_id', $id)->with('gfs_code')->get();
        $message = ["expenditure_centre_gfs_codes" => $all];
        return response()->json($message);
    }

    public function add_gfs_code(Request $request) {
        $data = json_decode($request->getContent());
        $expenditure_centre_id = $data->expenditure_centre_id;
        $gfs_code_id = $data->gfs_code_id;
        try{
            $obj = new ExpenditureCentreLink();
            $obj->gfs_code_id = $gfs_code_id;
            $obj->expenditure_centre_id = $expenditure_centre_id;
            $obj->created_by = UserServices::getUser()->id;
            $obj->save();
            $all = ExpenditureCentreLink::where('expenditure_centre_id', $expenditure_centre_id)->with('gfs_code')->get();
            $message = ["expenditure_centre_gfs_codes" => $all, "successMessage" => "GFS_CODE_ADDED"];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete_gfs_code($expenditure_centre_gfs_code_id, $expenditure_centre_id) {
        $object = ExpenditureCentreLink::find($expenditure_centre_gfs_code_id);
        $object->delete();
        $all = ExpenditureCentreLink::where('expenditure_centre_id', $expenditure_centre_id)->with('gfs_code')->get();
        $message = ["expenditure_centre_gfs_codes" => $all, "successMessage" => "GFS_CODE_REMOVED"];
        return response()->json($message);
    }
}
