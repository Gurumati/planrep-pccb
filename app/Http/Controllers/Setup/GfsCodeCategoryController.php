<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\GfsCodeCategory;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

class GfsCodeCategoryController extends Controller {

    public function index() {
        return response()->json(GfsCodeCategory::orderBy('sort_order','asc')->get());
    }

    public function getAllPaginated($perPage) {
        return GfsCodeCategory::orderBy('sort_order','asc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, GfsCodeCategory::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            GfsCodeCategory::create($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "gfsCodeCategories" => $all];
            return response()->json($message, 200);
        }
    }

    public function update(Request $request) {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, GfsCodeCategory::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            $obj = GfsCodeCategory::find($request->id);
            $obj->update($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "gfsCodeCategories" => $all];
            return response()->json($message, 200);
        }
    }

    public function delete($id) {
        try {
            $object = GfsCodeCategory::find($id);
            $object->delete();
            $all = $this->getAllPaginated(Input::get('perPage'));
            $message = ["successMessage" => "SUCCESSFULLY_GFS_CODE_CATEGORY_DELETED", "gfsCodeCategories" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $message = ["errorMessage" => "DATA_IN_USE"];
            return response()->json($message, 500);
        }
    }

    public function upload() {
        if (Input::hasFile('file')) {
            $path = Input::file('file')->getRealPath();
            $data = Excel::selectSheets('GFS_CODE_CATEGORIES')->load($path, function ($reader) { })->get();
            if (!empty($data) && $data->count() > 0) {
                foreach ($data as $key => $value) {
                    $name = $value->name;
                    $count = GfsCodeCategory::where('name', $name)->count();
                    if ($count == 1) {
                        continue;
                    }
                    $insert[] = [
                        'name' => $value->name,
                        'description' => $value->description,
                    ];
                }
                if (!empty($insert)) {
                    try {
                        DB::table('gfs_code_categories')->insert($insert);
                        $all = $this->getAllPaginated(Input::get('perPage'));
                        $message = ["successMessage" => "DATA_UPLOAD_SUCCESS", "gfsCodeCategories" => $all];
                        return response()->json($message, 200);
                    } catch (QueryException $exception) {
                        $error_code = $exception->errorInfo[1];
                        Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
                        $message = ["errorMessage" => "DATABASE_ERROR"];
                        return response()->json($message, 500);
                    }
                } else {
                    $message = ["successMessage" => 'DATA_UPLOAD_SUCCESS'];
                    return response()->json($message, 500);
                }
            } else {
                $message = ["errorMessage" => 'FILE_SPECIFIED_HAS_NO_DATA'];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => 'PLEASE_SELECT_FILE'];
            return response()->json($message, 500);
        }
    }
}
