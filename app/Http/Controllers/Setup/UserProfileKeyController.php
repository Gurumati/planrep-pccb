<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\UserProfileKey;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class UserProfileKeyController extends Controller {
    public function index() {
        $all = UserProfileKey::orderBy('created_at','desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return UserProfileKey::orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new UserProfileKey();
            $obj->key = $data->key;
            $obj->default_value = $data->default_value;
            $obj->group = $data->group;
            $obj->options = $data->options;
            $obj->option_query = $data->option_query;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "userProfileKeys" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = UserProfileKey::find($data->id);
            $obj->key = $data->key;
            $obj->default_value = $data->default_value;
            $obj->group = $data->group;
            $obj->options = $data->options;
            $obj->option_query = $data->option_query;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "userProfileKeys" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }



    public function delete($id) {
        $obj = UserProfileKey::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "userProfileKeys" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = UserProfileKey::orderBy('created_at','desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        $message = ["trashedUserProfileKeys" => $all];
        return response()->json($message);
    }

    public function restore($id) {
        UserProfileKey::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedUserProfileKeys" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        UserProfileKey::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedUserProfileKeys" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = UserProfileKey::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedUserProfileKeys" => $all];
        return response()->json($message, 200);
    }
}
