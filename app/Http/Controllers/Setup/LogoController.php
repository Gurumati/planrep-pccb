<?php

namespace App\Http\Controllers\Setup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use Illuminate\Support\Facades\File;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\logo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class LogoController extends Controller
{

    public function currentUserAdminHeirachyId () {
     return  $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
    }
    public function currentUserAdminHeirachyPosition () {
        $admin_hierarchy_id = $this->currentUserAdminHeirachyId();
        $admin_position = DB::table('admin_hierarchies as a')
        ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
        ->where('a.id', $admin_hierarchy_id)
        ->select('l.hierarchy_position')
        ->first();
        return $admin_position;
    }

    //defalt called function
    public function getLogoByPisc (Request $request) {
        $perPage = $request->perPage;
        if ($perPage == null or $perPage == "") {
            $perPage = 10;
        }

        $piscs = $this->getLogoByUserLevel($perPage);
        return response()->json(["piscs" => $piscs], 200);
    }


    public function getLogoByUserLevel ($perPage) {

        $admin_position = $this->currentUserAdminHeirachyPosition();
        $admin_hierarchy_id = $this->currentUserAdminHeirachyId();

        // //otr Level
        // if ($admin_position->hierarchy_position == 1) {
        //     $piscs = AdminHierarchy::select('admin_hierarchies.*','log.id as logo_id','log.path as path','log.description as description')
        //     ->join('admin_hierarchies as a1','a1.id','=','admin_hierarchies.parent_id')
        //     ->join('admin_hierarchies as a2','a2.id','=','a1.parent_id')
        //     ->join('admin_hierarchies as a3','a3.id','=','a2.parent_id')
        //     ->join('admin_hierarchy_levels as l', 'a3.admin_hierarchy_level_id', 'l.id')
        //     ->leftJoin('logos as log', 'admin_hierarchies.id', 'log.admin_hierarchy_id')
        //     ->where('l.hierarchy_position', $admin_position->hierarchy_position)
        //     ->orderBy('admin_hierarchies.name','ASC')
        //     ->paginate($perPage);
        //     //return response()->json(["piscs" => $piscs], 200);
        //     return $piscs;


        // } elseif ($admin_position->hierarchy_position == 4) { 

        //     $piscs = AdminHierarchy::select('admin_hierarchies.*','log.id as logo_id','log.path as path','log.description as description')
        //     ->join('admin_hierarchies as a1','a1.id','=','admin_hierarchies.parent_id')
        //     ->join('admin_hierarchies as a2','a2.id','=','a1.parent_id')
        //     ->join('admin_hierarchy_levels as l', 'a2.admin_hierarchy_level_id', 'l.id')
        //     ->leftJoin('logos as log', 'admin_hierarchies.id', 'log.admin_hierarchy_id')
        //     ->where('l.hierarchy_position', $admin_position->hierarchy_position)
        //     ->where('a2.id', $admin_hierarchy_id)
        //     ->orderBy('admin_hierarchies.name','ASC')
        //     ->paginate($perPage);
        //     //return response()->json(["piscs" => $piscs], 200);
        //     return $piscs;

        // }elseif ($admin_position->hierarchy_position == 3) {

        //     $piscs = AdminHierarchy::select('admin_hierarchies.*','log.id as logo_id','log.path as path','log.description as description')
        //     ->join('admin_hierarchies as a1','a1.id','=','admin_hierarchies.parent_id')
        //     ->join('admin_hierarchy_levels as l', 'a1.admin_hierarchy_level_id', 'l.id')
        //     ->leftJoin('logos as log', 'admin_hierarchies.id', 'log.admin_hierarchy_id')
        //     ->where('l.hierarchy_position', $admin_position->hierarchy_position)
        //     ->where('a1.id', $admin_hierarchy_id)
        //     ->orderBy('admin_hierarchies.name','ASC')
        //     ->paginate($perPage);
        //     //return response()->json(["piscs" => $piscs], 200);
        //     return $piscs;

        // } else {
            $piscs = AdminHierarchy::select('admin_hierarchies.*','log.id as logo_id','log.path as path','log.description as description')
            ->join('admin_hierarchy_levels as l', 'admin_hierarchies.admin_hierarchy_level_id', 'l.id')
            ->leftJoin('logos as log', 'admin_hierarchies.id', 'log.admin_hierarchy_id')
            ->where('l.hierarchy_position', $admin_position->hierarchy_position)
            ->where('admin_hierarchies.id', $admin_hierarchy_id)
            ->orderBy('admin_hierarchies.name','ASC')
            ->paginate($perPage);
            return $piscs;
           // return response()->json(["piscs" => $piscs], 200);

        // }


    }

    public function uploadLogo (Request $request) {

        $file_path = public_path("uploads/logos/");
        if (!File::isDirectory($file_path)) {
             File::makeDirectory($file_path, 0777, true, true);
        }

        $validation_rule = ["file" => "required|mimes:jpeg,jpg,png"];



        if ($request->hasFile('file')) {
            if ($request->file('file')->isValid()) {
                $validator = Validator::make($request->all(), $validation_rule);
                if (!$validator->fails()) {
                    $file = $request->file;
                    $path = $file->store('uploads/logos');
                    return $path;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }


    public function saveLogo (Request $request) {
        $data = $request->all();
        $description = $request->description;
        $realImageSize = $request->realImageSize;

        if ($realImageSize > 102400) {
            $piscs = $this->getLogoByUserLevel($request->perPage);
            $message = ["errorMessage" => "Image size must be less than 100KB", "piscs" => $piscs];
            return response()->json($message, 200);
        }
        if (strlen($description) < 5 OR strlen($description) > 20) {
            $piscs = $this->getLogoByUserLevel($request->perPage);
            $message = ["errorMessage" => "Description is between 5 to 20 charaactors", "piscs" => $piscs];
            return response()->json($message, 200);
        }

        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $created_by = UserServices::getUser()->id;
        $urls = $request['urls'];
        if ($urls != null) {
            for($i=0;$i<sizeof($urls);$i++){
                logo::create([
                    'description' =>$description,
                    'path' => $urls[$i]['ad_url'],
                    'user_id' => $created_by,
                    'admin_hierarchy_id' => $admin_hierarchy_id,
                ]);
            }
        } else {

            $message = ["errorMessage" => "Please select logo again"];
            return response()->json($message, 500);

        }
        $piscs = $this->getLogoByUserLevel($request->perPage);
        $message = ["successMessage" => "LOGO_UPDATED", "piscs" => $piscs];
        return response()->json($message, 200);

    }

    public function removeLogo (Request $request) {
        $file_url = $request->file_url;
        File::delete($file_url);
        return $file_url;
    }

    public function deleteLogo ($id) {
        logo::where('id',$id)->delete();
         $path = Input::get('path');
         $file_path = public_path()."/".$path;
        if (File::exists($file_path)) {
            unlink($file_path);
        }

        $piscs = $this->getLogoByUserLevel(Input::get('perPage'));
       // return response()->json(["piscs" => $piscs], 200);
        $message = ["successMessage" => "DELETE_SUCCESS", "piscs" => $piscs];
        return response()->json($message, 200);
    }




}
