<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\SectorProject;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SectorProjectController extends Controller
{
    public function index() {
        $all = SectorProject::with('sector')->with('project')->orderBy('created_at')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return SectorProject::with('sector')->with('project')->orderBy('created_at')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function sectors($id) {
        $all = SectorProject::with('sector')->where('project_id', $id)->get();
        $message = ["sector_projects" => $all];
        return response()->json($message);
    }

    public function add_sector(Request $request) {
        $data = json_decode($request->getContent());
        $project_id = $data->project_id;
        $sector_array = $data->sector_id;
        try{
            for ($i=0;$i<count($sector_array);$i++){
                $sector_id = $sector_array[$i];
                $exist = SectorProject::where('sector_id',$sector_id)->where('project_id',$project_id)->count();
                if($exist == 1) continue;
                $sector_project = new SectorProject();
                $sector_project->sector_id = $sector_id;
                $sector_project->project_id = $project_id;
                $sector_project->save();
            }
            $all = SectorProject::with('sector')->where('project_id', $project_id)->get();
            $message = ["sector_projects" => $all,"successMessage" => "SECTOR_ADDED_SUCCESSFULLY"];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete_sector($sector_project_id,$project_id) {
        $object = SectorProject::find($sector_project_id);
        $object->delete();
        $all = SectorProject::with('sector')->where('project_id', $project_id)->get();
        $message = ["sector_projects" => $all,"successMessage" => "SECTOR_REMOVED_SUCCESSFULLY"];
        return response()->json($message);
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        $project_id = $data->project_id;
        $sectorArray = $data->sector_id;
        $count = count($sectorArray);
        $user_id = UserServices::getUser()->id;
        try {
            for ($i = 0; $i < $count; $i++) {
                $sector_id = $sectorArray[$i];
                $obj = new SectorProject();
                $obj->sector_id = $sector_id;
                $obj->project_id = $project_id;
                $obj->created_by = $user_id;
                $obj->save();
            }
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_SECTOR_PROJECT_CREATED", "sectorProjects" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try {
            $obj = SectorProject::find($data->id);
            $obj->sector_id = $data->sector_id;
            $obj->project_id = $data->project_id;
            $obj->updated_by = UserServices::getUser()->id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_SECTOR_PROJECT_UPDATED", "sectorProjects" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = SectorProject::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_SECTOR_PROJECT_DELETED", "sectorProjects" => $all];
        return response()->json($message, 200);
    }
}
