<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\Planning\PlanningServices;
use App\Http\Services\UserServices;
use App\Models\Setup\GenericSectorProblem;
use App\Models\Setup\Intervention;
use App\Models\Setup\NationalTarget;
use App\Models\Setup\PriorityArea;
use App\Models\Setup\PriorityAreaSector;
use App\Models\Setup\PriorityAreaVersion;
use App\Models\Setup\Section;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class PriorityAreaController extends Controller
{
    public function index(Request $request)
    {
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'PA');
        $sql = "select p.id,p.description,p.number,p.is_active,l.id as link_level_id,l.name as link_level from priority_areas p
                join priority_area_versions pv on pv.priority_area_id = p.id
                join versions v on v.id = pv.version_id
                left join financial_year_versions fv on fv.version_id = v.id
                join link_levels l on l.id = p.link_level
                where v.id = $versionId and fv.financial_year_id = $financialYearId order by p.number asc";
        $items = DB::select($sql);
        return apiResponse(200, "Success", $items, true, []);
    }

    public function fetchAll(Request $request)
    {
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'PA');
        $sql = "select p.id,p.description,p.number,p.is_active,l.id as link_level_id,l.name as link_level from priority_areas p
                join priority_area_versions pv on pv.priority_area_id = p.id
                join versions v on v.id = pv.version_id
                left join financial_year_versions fv on fv.version_id = v.id
                join link_levels l on l.id = p.link_level
                where v.id = '$versionId' and fv.financial_year_id = '$financialYearId' order by p.number asc";
        $items = DB::select($sql);
        return apiResponse(200, "Success", $items, true, []);
    }

    public function getForCurrentVersion(){
        return ['priorityAreas'=>PriorityArea::getForCurrentVersion()];
    }

    public function getAllPaginated($versionId, $financialYearId)
    {
        $sql = "select p.id,p.description,p.number,p.is_active,l.id as link_level_id,l.name as link_level from priority_areas p
                join priority_area_versions pv on pv.priority_area_id = p.id
                join versions v on v.id = pv.version_id
                left join financial_year_versions fv on fv.version_id = v.id
                join link_levels l on l.id = p.link_level
                where v.id = $versionId and fv.financial_year_id = $financialYearId order by p.number asc";
        $items = DB::select($sql);
        return $items;
    }

    public function priorityWithInterventionAndProblems($linkLevel,$planChainId,$financialYearId,$adminHierarchyId,$sectionId)
    {
       
        return response()->json(['priorityWithInterventionAndProblems'=>PriorityArea::getPriorityWithInterventionAndProblems($linkLevel,$planChainId, $financialYearId, $adminHierarchyId,$sectionId)]);
    }

    public function paginated(Request $request)
    {
        $versionId = $request->versionId;
        $financialYearId = $request->financialYearId;
        $all = $this->getAllPaginated($versionId, $financialYearId);
        return apiResponse(200, "Success", $all, true, []);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, PriorityArea::rules());
        if (isset($request->perPage)) {
            $perPage = $request->perPage;
        } else {
            $perPage = 10;
        }
        if ($validator->fails()) {
            $errors = $validator->errors();
            $items = $this->getAllPaginated($request->versionId,$request->financialYearId);
            return apiResponse(500, "Failed", $items, false, $errors->all());
        } else {
            DB::transaction(function () use ($request) {
                $data = $request->all();
                $newPriorityArea = PriorityArea::create($data);
                $priority_area_id = $newPriorityArea->id;

                $newPV = new PriorityAreaVersion();
                $newPV->priority_area_id = $priority_area_id;
                $newPV->version_id = $request->versionId;
                $newPV->save();

                foreach ($request->sectors as $sector) {
                    $pas = new PriorityAreaSector();
                    $pas->priority_area_id = $priority_area_id;
                    $pas->sector_id = $sector['id'];
                    $pas->save();
                }
            });
            if (isset($request->perPage)) {
                $perPage = $request->perPage;
            } else {
                $perPage = 10;
            }
            $items = $this->getAllPaginated($request->versionId,$request->financialYearId);
            return apiResponse(201, "Success", $items, true, []);
        }
    }

    public function copyPriorityAreas(Request $request)
    {
        DB::transaction(function () use ($request) {
            $sourceFinancialYearId = $request->sourceFinancialYearId;
            $sourceVersionId = $request->sourceVersionId;
            $destinationVersionId = $request->destinationVersionId;
            $items = DB::select("select p.* from priority_areas p 
            join priority_area_versions v on p.id = v.priority_area_id
            join versions v2 on v.version_id = v2.id
            join financial_year_versions fyv on v2.id = fyv.version_id 
            where v2.id = $sourceVersionId and fyv.financial_year_id = $sourceFinancialYearId");
            foreach ($items as $item) {
                $pa = new PriorityArea();
                $pa->description = $item->description;
                $pa->number = $item->number;
                $pa->is_active = $item->is_active;
                $pa->link_level = $item->link_level;
                $pa->save();
                $priority_area_id = $pa->id;

                $npa = new PriorityAreaVersion();
                $npa->version_id = $destinationVersionId;
                $npa->priority_area_id = $priority_area_id;
                $npa->save();

                $sectors = DB::select("select sector_id as id from priority_area_sectors where priority_area_id = $item->id");
                foreach ($sectors as $sector) {
                    $s = new PriorityAreaSector();
                    $s->sector_id = $sector->id;
                    $s->priority_area_id = $priority_area_id;
                    $s->save();
                }
            }

        });
        return apiResponse(201, "Success", [], true, []);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, PriorityArea::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $items = $this->getAllPaginated($request->versionId,$request->financialYearId);
            return apiResponse(500, "Failed", $items, false, $errors->all());
        } else {
            $obj = PriorityArea::find($request->id);
            $obj->update($data);
            $items = $this->getAllPaginated($request->versionId, $request->financialYearId);
            return apiResponse(201, "Success", $items, true, []);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $v = PriorityAreaVersion::where("priority_area_id",$id)
            ->where("version_id",Input::get('versionId'))
            ->first();
        $v->delete();
        $ps = PriorityAreaSector::where("priority_area_id",$id)->get();
        foreach ($ps as $p) {
            $p->delete();
        }
        $obj = PriorityArea::find($id);
        $obj->delete();
        $all = $this->getAllPaginated($request->versionId,$request->financialYearId);
        return apiResponse(200, "Success", $all, true, []);
    }

    public function toggleActive(Request $request)
    {
        $object = PriorityArea::find($request->id);
        $object->is_active = $request->is_active;
        $object->save();
        if ($request->is_active == false) {
            return apiResponse(200, "Priority Area Deactivated", [], true, []);
        } else {
            return apiResponse(200, "Priority Area Activated", [], true, []);
        }
    }

    public function sectors(Request $request)
    {
        $id = $request->priorityAreaId;
        $items = $this->priorityAreaSectors($id);
        return apiResponse(200, "Success", $items, true, []);
    }

    public function removeSector(Request $request)
    {
        $priorityAreaId = $request->priorityAreaId;
        $id = $request->id;
        $b = PriorityAreaSector::find($id);
        $b->delete();
        $items = $this->priorityAreaSectors($priorityAreaId);
        return apiResponse(200, "Sector Removed Successfully", $items, true, []);
    }

    public function addSector(Request $request)
    {
        $priorityAreaId = $request->priorityAreaId;
        foreach ($request->sectors as $sector) {
            $count = PriorityAreaSector::where("priority_area_id", $priorityAreaId)
                ->where("sector_id", $sector['id'])
                ->count();
            if ($count == 0) {
                $x = new PriorityAreaSector();
                $x->priority_area_id = $priorityAreaId;
                $x->sector_id = $sector['id'];
                $x->save();
            }
        }
        $items = $this->priorityAreaSectors($priorityAreaId);
        return apiResponse(200, "Sector Added Successfully", $items, true, []);
    }

    public function interventions(Request $request)
    {
        $priority_area_id = $request->priority_area_id;
        $interventions = Intervention::with('intervention_category')->where('priority_area_id', $priority_area_id)->orderBy('number', 'asc')->get();
        $genericSectorProblems = GenericSectorProblem::where('priority_area_id', $priority_area_id)->orderBy('code', 'asc')->get();
        $nationalTargets = NationalTarget::where('priority_area_id', $priority_area_id)->orderBy('code', 'asc')->get();
        $response = ["interventions" => $interventions, "genericSectorProblems" => $genericSectorProblems, "nationalTargets" => $nationalTargets];
        return response()->json($response, 200);
    }

    public function genericSectorProblems(Request $request)
    {
        $priority_area_id = $request->priority_area_id;
        $all = GenericSectorProblem::where('priority_area_id', $priority_area_id)->orderBy('code', 'asc')->get();
        $response = ["genericSectorProblems" => $all];
        return response()->json($response, 200);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function priorityAreaSectors($id)
    {
        $sql = "select s.id,s.name,p.id as priority_area_sector_id from sectors s
                join priority_area_sectors p on p.sector_id = s.id
                where p.priority_area_id = $id";
        $items = DB::select($sql);
        return $items;
    }
}
