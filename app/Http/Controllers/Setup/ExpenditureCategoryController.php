<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;

use App\Models\Setup\ExpenditureCategory;
use Illuminate\Contracts\Logging\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ExpenditureCategoryController extends Controller {
    public function index() {
        $all = ExpenditureCategory::with('activity_task_nature','project_type')->orderBy('created_at','desc')->get();
        return response()->json(["items"=>$all],200);
    }

    public function expenditureCategoryByTaskNature(Request $request) {
        $task_nature_id = $request->activity_task_nature_id;
        $project_type_id = $request->project_type_id;
        $all = ExpenditureCategory::where('project_type_id',$project_type_id)->where('is_active', true)->orderBy('created_at','desc')->get();
        return response()->json(["items"=>$all],200);
    }

    public function getAllPaginated($perPage) {
        return ExpenditureCategory::with('activity_task_nature','project_type')->orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), ExpenditureCategory::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        ExpenditureCategory::create($request->all());
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "CREATE_SUCCESSFUL", "items" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, ExpenditureCategory::rules($request->id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        $expCategory = ExpenditureCategory::find($request->id);
        $expCategory->update($data);
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "UPDATE_SUCCESSFUL", "items" => $all];
        return response()->json($message, 200);
    }

    public function toggleActive($id)
    {
        try {
            $obj = ExpenditureCategory::find($id);
            $obj->is_active = ($obj->is_active) ? false : true;
            $obj->save();
            $successMessage = (!$obj->is_active) ? 'Expenditure category deactivated' : 'Expenditure category activated';
            return ['successMessage' => $successMessage];
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['errorMessage' => 'ERROR_CHANGING_CEILING'], 500);
        }
    }

    public function delete($id) {
        $obj = ExpenditureCategory::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }
}
