<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\System;

class SystemController extends Controller
{
    public function index() {
        $data = System::all();
        return response()->json($data,200);
    }
}
