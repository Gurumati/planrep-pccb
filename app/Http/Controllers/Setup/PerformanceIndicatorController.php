<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\CustomPager;
use App\Http\Services\UserServices;
use App\Models\Setup\PerformanceIndicator;
use App\Models\Setup\PerformanceIndicatorVersion;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Services\FinancialYearServices;
use App\Models\Setup\Version;

class PerformanceIndicatorController extends Controller
{
    public function index(Request $request)
    {
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request,$financialYearId,'PI');
        $section_id = UserServices::getUser()->section_id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $sql = "select distinct p.id,p.id as performance_indicator_id,p.number,p.description,pc.code as plan_chain_code,pc.description as plan_chain
                from performance_indicators p
                join sections s on s.id = p.section_id
                join plan_chains pc on p.plan_chain_id = pc.id
                join performance_indicator_versions piv on p.id = piv.performance_indicator_id
                where p.deleted_at is null and pc.admin_hierarchy_id = $admin_hierarchy_id
                  and piv.version_id = $versionId and s.id = $section_id order by p.number asc";
        return apiResponse(200, "Success", DB::select($sql), true, []);
    }

    public function getByPlanChainId(Request $request)
    {
        $planChainId = $request->planChainId;
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request,$financialYearId,'PI');
        $section_id = UserServices::getUser()->section_id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $sql = "select distinct p.id,p.id as performance_indicator_id,p.number,p.description,pc.code as plan_chain_code,pc.description as plan_chain
                from performance_indicators p
                join sections s on s.id = p.section_id
                join plan_chains pc on p.plan_chain_id = pc.id
                join performance_indicator_versions piv on p.id = piv.performance_indicator_id
                where p.deleted_at is null
                  and pc.id = $planChainId
                  and piv.version_id = $versionId and pc.admin_hierarchy_id = $admin_hierarchy_id
                  and s.id = $section_id order by p.number asc";
        return apiResponse(200, "Success", DB::select($sql), true, []);
    }

    public function getAllPaginated($financialYearId, $versionId, $perPage, $searchQuery)
    {
        $section_id = UserServices::getUser()->section_id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $sql = "select p.id,p.id as performance_indicator_id,p.description,p.number,p.is_qualitative,p.less_is_good,
                s.id as section_id,
                s.name as section,s.code as section_code,pc.id as plan_chain_id,pc.description as plan_chain,
                pc.code as plan_chain_code,pcv.version_id as plan_chain_version_id
                from performance_indicators p
                join sections s on s.id = p.section_id
                join plan_chains pc on p.plan_chain_id = pc.id
                join plan_chain_versions pcv on pcv.plan_chain_id = pc.id
                join performance_indicator_versions piv on p.id = piv.performance_indicator_id
                join versions v on piv.version_id = v.id
                join financial_year_versions fyv on v.id = fyv.version_id
                where p.deleted_at is null and fyv.financial_year_id = $financialYearId
                and v.id = $versionId and pc.admin_hierarchy_id = $admin_hierarchy_id and s.id = $section_id";
        if (isset($searchQuery)) {
            $q = "'%" . $searchQuery . "%'";
            $sql .= " AND (p.description ILIKE $q
                    OR p.number ILIKE $q
                    OR pc.code ILIKE $q
                    OR pc.description ILIKE $q)";
        }
        $sql .= " order by p.id,p.number asc";
        return CustomPager::paginate(DB::select($sql), $perPage);
    }

    public function paginated(Request $request)
    {
        if (isset($request->perPage)) {
            $perPage = $request->perPage;
        } else {
            $perPage = 10;
        }
        $all = $this->getAllPaginated($request->financialYearId, $request->versionId, $perPage, $request->searchQuery);
        return apiResponse(200, "Success", $all, true, []);
    }

    public function search(Request $request)
    {
        if (isset($request->perPage)) {
            $perPage = $request->perPage;
        } else {
            $perPage = 10;
        }
        $all = $this->getAllPaginated($request->financialYearId, $request->versionId, $perPage, $request->searchQuery);
        return apiResponse(200, "Success", $all, true, []);
    }

    public function store(Request $request)
    {
        $data = array_merge($request->all(), ["section_id" => UserServices::getUser()->section_id]);
        $validator = Validator::make($data, PerformanceIndicator::rules());
        if (isset($request->perPage)) {
            $perPage = $request->perPage;
        } else {
            $perPage = 10;
        }
        if ($validator->fails()) {
            $errors = $validator->errors();
            $all = $this->getAllPaginated($request->financialYearId, $request->versionId, $perPage, $request->searchQuery);
            return apiResponse(500, "Failed", $all, false, $errors->all());
        } else {
            DB::transaction(function () use ($request, $data) {
                $pi = PerformanceIndicator::create($data);
                $pv = new PerformanceIndicatorVersion();
                $pv->performance_indicator_id = $pi->id;
                $pv->version_id = $request->versionId;
                $pv->save();
            });
            $all = $this->getAllPaginated($request->financialYearId, $request->versionId, $perPage, $request->searchQuery);
            return apiResponse(201, "Success", $all, true, []);
        }
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $id = $request->id;
        if (isset($request->perPage)) {
            $perPage = $request->perPage;
        } else {
            $perPage = 10;
        }
        $validator = Validator::make($data, PerformanceIndicator::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $all = $this->getAllPaginated($request->financialYearId, $request->versionId, $perPage, $request->searchQuery);
            return apiResponse(500, "Failed", $all, false, $errors->all());
        } else {
            $obj = PerformanceIndicator::find($request->id);
            $obj->update($data);
            $all = $this->getAllPaginated($request->financialYearId, $request->versionId, $perPage, $request->searchQuery);
            return apiResponse(201, "Success", $all, true, []);
        }
    }

    public function delete($id)
    {
        DB::transaction(function () use ($id) {
            $pvs = PerformanceIndicatorVersion::where("performance_indicator_id", $id)->get();
            foreach ($pvs as $pv) {
                $pv->delete();
            }
            DB::table('performance_indicators')->where('id', $id)->delete();
        });
        $all = $this->getAllPaginated(Input::get('financialYearId'), Input::get('versionId'), Input::get('perPage'), Input::get('searchQuery'));
        return apiResponse(200, "Success", $all, true, []);
    }

    public function toggleQualitative(Request $request)
    {
        $data = json_decode($request->getContent());
        $object = PerformanceIndicator::find($data->id);
        $object->is_qualitative = $data->is_qualitative;
        $object->save();
        $all = $this->getAllPaginated($request->financialYearId, $request->versionId, $request->perPage, $request->searchQuery);
        return apiResponse(200, "Success", $all, true, []);
    }

    public function toggleLessIsGood(Request $request)
    {
        $data = json_decode($request->getContent());
        $object = PerformanceIndicator::find($data->id);
        $object->less_is_good = $data->less_is_good;
        $object->save();
        $all = $this->getAllPaginated($request->financialYearId, $request->versionId, $request->perPage, $request->searchQuery);
        return apiResponse(200, "Success", $all, true, []);
    }

    public function upload()
    {
        $plan_chain_id = Input::get('plan_chain_id');
        $financialYearId = Input::get('financialYearId');
        $versionId = Input::get('versionId');
        $perPage = Input::get('perPage');
        $searchQuery = Input::get('searchQuery');
        if (Input::hasFile('file')) {
            $path = Input::file('file')->getRealPath();
            $data = Excel::selectSheets('PERFORMANCE_INDICATORS')->load($path, function ($reader) {
            })->get();
            if (!empty($data) && $data->count() > 0) {
                foreach ($data as $key => $value) {
                    DB::transaction(function () use ($value, $versionId, $plan_chain_id) {
                        $p = new PerformanceIndicator();
                        $p->description = $value->description;
                        $p->number = $value->number;
                        $p->plan_chain_id = $plan_chain_id;
                        $p->section_id = UserServices::getUser()->section_id;
                        $p->save();
                        $performance_indicator_id = $p->id;
                        $pv = new PerformanceIndicatorVersion();
                        $pv->performance_indicator_id = $performance_indicator_id;
                        $pv->version_id = $versionId;
                        $pv->save();
                    });
                }
                $all = $this->getAllPaginated($financialYearId, $versionId, $perPage, $searchQuery);
                return apiResponse(200, "Success", $all, true, []);
            } else {
                $all = $this->getAllPaginated($financialYearId, $versionId, $perPage, $searchQuery);
                return apiResponse(500, "File specified has no data", $all, false, []);
            }
        } else {
            $all = $this->getAllPaginated($financialYearId, $versionId, $perPage, $searchQuery);
            return apiResponse(500, "Please select excel file", $all, false, []);
        }
    }

    public function getByTarget($targetId)
    {
        $indicators = DB::table('performance_indicators as pi')
        ->join('target_performance_indicators as tpi', 'pi.id', 'tpi.performance_indicator_id')
        ->where('tpi.target_id', $targetId)
        ->select('pi.*')
        ->get();
        $message = ["indicators" => $indicators];
        return response()->json($message, 200);
    }

    public function getByPlanChain($planChainId)
    {
        $versionId = Version::getCurrentVersionId('PI');
        $itemIds = Version::getVesionItemIds($versionId, 'PI');
        $indicators = DB::table('performance_indicators as pi')
            ->whereIn('id', $itemIds)
            ->where('pi.plan_chain_id', $planChainId)
            ->select('pi.*')
            ->get();
        $message = ["indicators" => $indicators];
        return response()->json($message, 200);
    }

    public function exportToCsv()
    {
        $financialYearId = Input::get('financialYearId');
        $versionId = Input::get('versionId');
        $items = DB::select("select x.number,x.description,x.planning_unit,x.service_output_number,x.service_output,string_agg(x.sector,',') as sectors
from (select distinct p.number,p.description,s.name as planning_unit,pc.code as service_output_number,pc.description as service_output,s2.name as sector
from performance_indicators p
       join sections s on s.id = p.section_id
       join plan_chains pc on p.plan_chain_id = pc.id
       join performance_indicator_versions piv on p.id = piv.performance_indicator_id
       join versions v on piv.version_id = v.id
       join financial_year_versions fyv on v.id = fyv.version_id
       left join plan_chain_sectors pcs on pcs.plan_chain_id = pc.id
      left join sectors s2 on pcs.sector_id = s2.id
where p.deleted_at is null and fyv.financial_year_id = $financialYearId
  and v.id = $versionId and p.description is not null and p.deleted_at is null) x
group by x.number,x.description,x.planning_unit,x.service_output_number,x.service_output");

        $array = array();
        foreach ($items as $value) {
            $data = array();
            $data['PERFORMANCE_INDICATOR_NUMBER'] = $value->number;
            $data['DESCRIPTION'] = $value->description;
            $data['SERVICE_OUTPUT_NUMBER'] = (isset($value->service_output_number)) ? $value->service_output_number : "";
            $data['SERVICE_OUTPUT'] = (isset($value->service_output)) ? $value->service_output : "";
            $data['PLANNING_UNIT'] = $value->planning_unit;
            $data['SECTOR'] = $value->sectors;
            array_push($array, $data);
        }

        Excel::create('PERFORMANCE_INDICATORS_' . time(), function ($excel) use ($array) {
            $excel->sheet('DATA', function ($sheet) use ($array) {
                $sheet->fromArray($array);
            });
        })->export('csv');
    }

    public function copy(Request $request)
    {
        $sourceFinancialYearId = $request->sourceFinancialYearId;
        $sourceVersionId = $request->sourceVersionId;
        $destinationVersionId = $request->destinationVersionId;
        $section_id = UserServices::getUser()->section_id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $sql = "select p.* from performance_indicators p
                       join sections s on s.id = p.section_id
                       join plan_chains pc on p.plan_chain_id = pc.id
                       join performance_indicator_versions piv on p.id = piv.performance_indicator_id
                       join versions v on piv.version_id = v.id
                       join financial_year_versions fyv on v.id = fyv.version_id
                where p.deleted_at is null and pc.admin_hierarchy_id = $admin_hierarchy_id and fyv.financial_year_id = $sourceFinancialYearId and v.id = $sourceVersionId
                and s.id = $section_id and p.description is not null";
        $items = DB::select($sql);
        DB::transaction(function () use ($items, $destinationVersionId) {
            foreach ($items as $item) {
                $np = new PerformanceIndicator();
                $np->description = $item->description;
                $np->number = $item->number;
                $np->plan_chain_id = $item->plan_chain_id;
                $np->is_qualitative = $item->is_qualitative;
                $np->less_is_good = $item->less_is_good;
                $np->section_id = $item->section_id;
                $np->save();
                $performance_indicator_id = $np->id;
                $npv = new PerformanceIndicatorVersion();
                $npv->performance_indicator_id = $performance_indicator_id;
                $npv->version_id = $destinationVersionId;
                $npv->save();
            }
        });
        return apiResponse(201, "Success", [], true, []);
    }
}
