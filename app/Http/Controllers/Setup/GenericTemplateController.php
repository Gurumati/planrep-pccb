<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\GenericTemplate;
use App\Models\Setup\PlanChain;
use Illuminate\Http\Request;

class GenericTemplateController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function plan_chains() {
        $planChain = PlanChain::all();
        return response()->json($planChain);
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        $obj = new GenericTemplate();
        $obj->name = $data->name;
        $obj->plan_chain_id = $data->plan_chain_id;
        $obj->description = (isset($data->description) ? $data->description : null);
        $obj->template_type = $data->template_type;
        $obj->generic_template_id = (isset($data->generic_template_id) ? $data->generic_template_id : null);
        $obj->parameters = $data->parameters;
        $obj->is_active = True;
        $obj->created_by = UserServices::getUser()->id;
        $obj->save();
        $all = GenericTemplate::with('childTemplates')->where('plan_chain_id', $data->plan_chain_id)->whereNull('generic_template_id')->get();
        $message = ["successMessage" => "SUCCESSFUL_GENERIC_TEMPLATE_CREATED", "genericTemplates" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        $obj = GenericTemplate::find($data->id);
        $obj->name = $data->name;
        $obj->plan_chain_id = $data->plan_chain_id;
        $obj->description = (isset($data->description) ? $data->description : null);
        $obj->template_type = $data->template_type;
        $obj->generic_template_id = (isset($data->generic_template_id) ? $data->generic_template_id : null);
        $obj->parameters = $data->parameters;
        $obj->updated_by = UserServices::getUser()->id;
        $obj->save();
        //Return language success key and all data
        $all = GenericTemplate::with('childTemplates')->where('generic_template_id', $data->generic_template_id)->whereNull('generic_template_id')->get();
        $message = ["successMessage" => "SUCCESSFUL_GENERIC_TEMPLATE_UPDATED", "genericTemplates" => $all];
        return response()->json($message, 200);
    }

    public function getByPlanChainId($generic_template_id) {
        $genericTemplates = GenericTemplate::with('childTemplates')->where('plan_chain_id', $generic_template_id)->whereNull('generic_template_id')->get();
        return response()->json($genericTemplates);
    }

    public function getParentByPlanChainId($generic_template_id, $childTemplateId) {
        $genericTemplates = GenericTemplate::with('parent')->where('plan_chain_id', $generic_template_id)->where('id', "!=", $childTemplateId)->orderBy('name')->get();
        return response()->json($genericTemplates);
    }
}
