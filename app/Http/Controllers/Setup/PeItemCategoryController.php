<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\PeItemCategory;
use Illuminate\Http\Request;

class PeItemCategoryController extends Controller
{
    private $limit = 10;

    public function index() {
        $all = PeItemCategory::all();
        return response()->json($all);
    }

    public function paginated() {
        $all = PeItemCategory::orderby('created_at')->paginate($this->limit);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        $obj = new PeItemCategory();
        $obj->name = $data->name;
        $obj->description = $data->description;
        $obj->created_by = UserServices::getUser()->id;
        $obj->save();
        $all = PeItemCategory::orderby('created_at')->paginate($this->limit);
        $message = ["successMessage" => "SUCCESSFUL_PE_ITEM_CATEGORY_CREATED", "peItemCategories" => $all];
        return response()->json($message, 200);
    }


    public function update(Request $request) {
        $data = json_decode($request->getContent());
        $obj = PeItemCategory::find($data->id);
        $obj->name = $data->name;
        $obj->description = $data->description;
        $obj->updated_by = UserServices::getUser()->id;
        $obj->save();
        $all = PeItemCategory::orderby('created_at')->paginate($this->limit);
        $message = ["successMessage" => "SUCCESSFUL_PE_ITEM_CATEGORY_UPDATED", "peItemCategories" => $all];
        return response()->json($message, 200);
    }

    public function delete($id) {
        $obj = PeItemCategory::find($id);
        $obj->delete();
        $all = PeItemCategory::orderby('created_at')->paginate($this->limit);
        $message = ["successMessage" => "SUCCESSFUL_PE_ITEM_CATEGORY_DELETED", "peItemCategories" => $all];
        return response()->json($message, 200);
    }
}
