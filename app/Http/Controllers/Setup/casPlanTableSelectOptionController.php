<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\casPlanTableSelectOption;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;

class casPlanTableSelectOptionController extends Controller
{

    public function index()
    {
       $all = casPlanTableSelectOption::whereNull('parent_id')->get();
       $data = ['all'=>$all];
       return response()->json($data);
    }

    public function store(Request $request)
    {
        try{
            $data = json_decode($request->getContent());
            $casPlanTableOption = new casPlanTableSelectOption();
            $casPlanTableOption->name      = $data->name;
            $casPlanTableOption->code      = $data->code;
            $casPlanTableOption->parent_id = isset($data->parent_id)?$data->parent_id:null;
            $casPlanTableOption->active    = isset($data->active)?$data->active:false;
            $casPlanTableOption->save();
            $all = casPlanTableSelectOption::whereNull('parent_id')->get();
            $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_SELECT_OPTION_CREATED", "planTableOptions" => $all];
            return response()->json($message, 200);
        }catch (QueryException $exception)
        {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 400);
        }

    }

    public function update(Request $request)
    {
        try{
            $data = json_decode($request->getContent());
            $casPlanTableOption = casPlanTableSelectOption::find($request->id);
            $casPlanTableOption->name      = $data->name;
            $casPlanTableOption->code      = $data->code;
            $casPlanTableOption->parent_id = isset($data->parent_id)?$data->parent_id:null;
            $casPlanTableOption->active    = isset($data->active)?$data->active:false;
            $casPlanTableOption->save();
            $all = casPlanTableSelectOption::whereNull('parent_id')->get();
            $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_SELECT_OPTION_UPDATED", "planTableOptions" => $all];
            return response()->json($message, 200);
        }catch (QueryException $exception)
        {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 400);
        }

    }

    public function delete($id)
    {
        try{
            $casPlanTableOption = casPlanTableSelectOption::find($id);
            $casPlanTableOption->delete();
            $all = casPlanTableSelectOption::whereNull('parent_id')->get();
            $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_SELECT_OPTION_DELETED", "planTableOptions" => $all];
            return response()->json($message, 200);
        }catch (QueryException $exception)
        {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 400);
        }

    }
}
