<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Setup\Project;
use App\Models\Setup\ProjectFundSource;
use App\Models\Setup\ProjectVersion;
use App\Models\Setup\SectorProject;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class ProjectController extends Controller
{
    public function index(Request $request)
    {
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'PR');
        $projects = Project::whereHas("project_versions", function ($version) use ($versionId) {
            $version->where("version_id", $versionId);
        })->orderBy('code', 'desc')->get();
        return response()->json(["projects"=>$projects]);
    }

    public function fetchAll(Request $request)
    {
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'PR');
        $projects = Project::whereHas("project_versions", function ($version) use ($versionId) {
            $version->where("version_id", $versionId);
        })->orderBy('code', 'asc')->get();
        return response()->json(["projects" => $projects], 200);
    }

    public function getBySector($sectorId){
        $projects = DB::table('projects as p')
            ->join ('sector_projects as sp','sp.project_id','=','p.id')
            ->where('sp.sector_id',$sectorId)
            ->get();
        return response()->json(['projects'=>$projects]);
    }

    public function getAllPaginated($versionId, $perPage)
    {
        // $items = DB::table('projects as p')
        //     ->join('project_versions as pv','p.id','pv.project_id')
        //     ->where('pv.version_id',$versionId)
        //     ->select('p.*')
        //     ->orderBy('p.id','asc')
        //     ->paginate($perPage);
        // return response()->json(["projects"=>$items]);

        $items = Project::whereHas("project_versions", function ($query) use ($versionId) {
                $query->where('version_id', $versionId);
            })->select('*')->get();

        return response()->json(["projects"=>$items]);
    }

    public function search(Request $request)
    {
        $searchText = $request->searchText;
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'PR');
        $all = Project::whereHas("project_versions", function ($version) use ($versionId) {
            $version->where("version_id", $versionId);
        })->where('name', 'ILIKE', '%' . $searchText . '%')
            ->orWhere('code', 'ILIKE', '%' . $searchText . '%')
            ->orWhere('description', 'ILIKE', '%' . $searchText . '%')
            ->orderBy('code', 'asc')->take(20)->paginate(10);
        return response()->json(['projects'=>$all, Response::HTTP_OK]);
    }

    public function paginated(Request $request)
    {
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'PR');
        $all = $this->getAllPaginated($versionId, $request->perPage);
        return response()->json($all);
    }

    public function getByBudgetClassAndSection($budgetClassId, $sectionId)
    {
        return ["projects" => Project::getByBudgetClassAndSection($budgetClassId, $sectionId)];
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, Project::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }

        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'PR');

        $count = DB::table('projects as p')
            ->join('project_versions as pv', 'p.id', 'pv.project_id')
            ->where('pv.version_id', $versionId)
            ->where('p.code', $request->code)
            ->count();

        if ($count > 0) {
            $message = ["errorMessage" => "Project code exists"];
            return response()->json($message, 400);
        }

        DB::transaction(function () use ($request, $versionId) {
            $data = $request->all();
            $project = Project::create($data);
            $project_id = $project->id;

            $pv = new ProjectVersion();
            $pv->project_id = $project_id;
            $pv->version_id = $versionId;
            $pv->save();

            if (isset($request->fund_sources)) {
                foreach ($request->fund_sources as $value) {
                    $exists = ProjectFundSource::where('project_id', $project_id)->where("fund_source_id", $value['id'])->count();
                    if ($exists < 1) {
                        $pf = new ProjectFundSource();
                        $pf->fund_source_id = $value['id'];
                        $pf->project_id = $project_id;
                        $pf->save();
                    }
                }
            }

            if (isset($request->sectors)) {
                foreach ($request->sectors as $sector) {
                    $sector_project = new SectorProject();
                    $sector_project->sector_id = $sector['id'];
                    $sector_project->project_id = $project_id;
                    $sector_project->save();
                }
            }
        });

        $all = $this->getAllPaginated($versionId, $request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_PROJECT_CREATED", "projects" => $all];

        return ($message);
    }

    public function update(Request $request)
    {

        $data = $request->all();
        $validator = Validator::make($data, Project::rules($request->id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }

        $project = Project::find($request->id);
        $project->update($data);
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'PR');
        $all = $this->getAllPaginated($versionId, $request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_PROJECT_UPDATED", "projects" => $all];
        return response()->json($message, 200);
    }

    public function delete(Request $request, $id)
    {
        $object = Project::find($id);
        $object->delete();

        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'PR');
        $all = $this->getAllPaginated($versionId, $request->perPage);
        $message = ["successMessage" => "SUCCESSFULLY_PROJECT_DELETED", "projects" => $all];
        return response()->json($message, 200);
    }


    public function getUsedProjects($request)
    {
            $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
            $financialYearId = FinancialYearServices::getPlanningFinancialYear()->id;
            // $versionId = $this->getVersionId($request, $financialYearId, 'PR');

            $versionId = ProjectVersion::distinct()->
            join('financial_year_versions as fyv','fyv.version_id','project_versions.version_id')
            ->where('fyv.financial_year_id',$financialYearId)
            ->first()->version_id;

            $projects = DB::table('projects')
                ->join('project_versions', 'projects.id', '=', 'project_versions.project_id')
                ->join('activities', 'projects.id', '=', 'activities.project_id')
                ->join('mtef_sections', 'activities.mtef_section_id', '=', 'mtef_sections.id')
                ->join('mtefs', 'mtef_sections.mtef_id', '=', 'mtefs.id')
                ->select('projects.id', 'projects.name')
                ->select('projects.id','projects.name','projects.description','projects.code')
                ->where('mtefs.financial_year_id', '=', $financialYearId)
                ->where('mtefs.admin_hierarchy_id', '=', $admin_hierarchy_id)
                ->where('project_versions.version_id', '=', $versionId)
                ->distinct()
                ->get();
            // $projects = ['data' => $projects];
            // return response()->json($projects);
            return response()->json(['projects'=>$projects]);
        return response()->json(['projects'=>$projects]);
    }

    public function exportToCsv(Request $request)
    {
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'PR');
        $items = Project::whereHas("project_versions", function ($version) use ($versionId) {
            $version->where("version_id", $versionId);
        })->orderBy('code', 'asc')->get();
        $array = array();
        foreach ($items as $value) {
            $data = array();
            $code = $value->code;
            $name = $value->name;
            $data['CODE'] = $code;
            $data['NAME'] = $name;
            array_push($array, $data);
        }

        Excel::create('PROJECTS' . time(), function ($excel) use ($array) {
            $excel->sheet('DATA', function ($sheet) use ($array) {
                $sheet->fromArray($array);
            });
        })->export('csv');
    }

    public function upload(Request $request)
    {
        $versionId = Input::get('versionId');
        if (Input::hasFile('file')) {
            $path = Input::file('file')->getRealPath();
            $data = Excel::selectSheets('PROJECTS')->load($path, function ($reader) {
            })->get();
            if (!empty($data) && $data->count()) {
                $error = array();
                $error_row = array();
                $row = 2;
                foreach ($data as $key => $value) {
                    $name = $value->name;
                    $code = $value->code;
                    if (is_null($name) || is_null($code)) {
                        array_push($error, $value);
                        array_push($error_row, $row);
                    }
                    $row++;
                }
                if (count($error) > 0) {
                    $all = $this->getAllPaginated($versionId, Input::get('perPage'));
                    $feedback = ["errorMessage" => 'DIRTY DATA', "errorData" => $error, "errorRows" => $error_row, "projects" => $all];
                    return response()->json($feedback, 500);
                } else {
                    $errors = array();
                    foreach ($data as $key => $value) {
                        if (strlen($value->code) == 4) {
                            $f = new Project();
                            $f->name = $value->name;
                            $f->code = $value->code;
                            $f->description = $value->name;
                            $f->save();

                            $v = new ProjectVersion();
                            $v->version_id = $versionId;
                            $v->project_id = $f->id;
                            $v->save();
                        } else {
                            array_push($errors, $value->code);
                        }
                    }
                    $all = $this->getAllPaginated($versionId, Input::get('perPage'));
                    $message = ["successMessage" => "Projects Uploaded Successfully", "projects" => $all, "errors" => $error];
                    return response()->json($message, 200);
                }
            } else {
                $message = ["errorMessage" => 'FILE_SPECIFIED_HAS_NO_DATA'];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => 'PLEASE_SELECT_A_FILE'];
            return response()->json($message, 500);
        }
    }

    public function addFundSources(Request $request)
    {
        $project_id = $request->project_id;
        if (isset($request->fund_sources)) {
            foreach ($request->fund_sources as $value) {
                $exists = ProjectFundSource::where('project_id', $project_id)->where("fund_source_id", $value['id'])->count();
                if ($exists < 1) {
                    $pf = new ProjectFundSource();
                    $pf->fund_source_id = $value['id'];
                    $pf->project_id = $project_id;
                    $pf->save();
                }
            }
        }
        $all = ProjectFundSource::where('project_id', $project_id)->get();
        $message = ["successMessage" => "Fund Source Added Successfully!", "items" => $all];
        return response()->json($message, 200);
    }

    public function removeFundSource(Request $request)
    {
        $project_id = $request->project_id;
        $id = $request->id;
        $obj = ProjectFundSource::find($id);
        $obj->delete();
        $all = ProjectFundSource::where('project_id', $project_id)->get();
        $message = ["successMessage" => "Fund Source Removed Successfully!", "items" => $all];
        return response()->json($message, 200);
    }

    public function fundSources(Request $request)
    {
        $project_id = $request->project_id;
        $all = ProjectFundSource::where('project_id', $project_id)->get();
        $message = ["items" => $all];
        return response()->json($message, 200);
    }

    public function copy(Request $request)
    {
        $sourceVersionId = $request->sourceVersionId;
        $destinationVersionId = $request->destinationVersionId;
        $items = Project::with("fund_sources", "sector_projects")
            ->whereHas("project_versions", function ($version) use ($sourceVersionId) {
            $version->where("version_id", $sourceVersionId);
        })->orderBy('code', 'asc')->get();
        foreach ($items as $data) {
            DB::transaction(function () use ($data, $destinationVersionId) {
                $obj = new Project();
                $obj->name = $data->name;
                $obj->description = $data->description;
                $obj->code = $data->code;
                $obj->save();

                $project_id = $obj->id;

                $pv = new ProjectVersion();
                $pv->project_id = $project_id;
                $pv->version_id = $destinationVersionId;
                $pv->save();

                $fund_sources = $data->fund_sources;
                foreach ($fund_sources as $value) {
                    $exists = ProjectFundSource::where('project_id', $project_id)
                        ->where("fund_source_id", $value->fund_source_id)->count();
                    if ($exists == 0) {
                        $pf = new ProjectFundSource();
                        $pf->fund_source_id = $value->fund_source_id;
                        $pf->project_id = $project_id;
                        $pf->save();
                    }
                }

                $sectors = $data->sector_projects;
                foreach ($sectors as $sector) {
                    $exists = SectorProject::where('project_id', $project_id)
                        ->where("sector_id", $sector->sector_id)->count();
                    if ($exists == 0) {
                        $sector_project = new SectorProject();
                        $sector_project->sector_id = $sector->sector_id;
                        $sector_project->project_id = $project_id;
                        $sector_project->save();
                    }
                }
            });
        }
        return apiResponse(201, "Successfully Copied Projects", [], true, []);
    }
}
