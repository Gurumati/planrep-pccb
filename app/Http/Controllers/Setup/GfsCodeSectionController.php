<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\GfsCodeSection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class GfsCodeSectionController extends Controller {
    function gfs_codes() {
        $section_id = UserServices::getUser()->section_id;
        $gfs_codes = DB::table('gfs_codes')
            ->join('gfs_code_sections', 'gfs_codes.id', '=', 'gfs_code_sections.gfs_code_id')
            ->join('sections', 'sections.id', '=', 'gfs_code_sections.section_id')
            ->select('gfs_codes.*')->where('sections.id', $section_id)->get();
        return response()->json($gfs_codes, 200);
    }

    public function index() {
        $section_id = UserServices::getUser()->section_id;
        $gfs_codes = DB::table('gfs_codes')
            ->join('gfs_code_sections', 'gfs_codes.id', '=', 'gfs_code_sections.gfs_code_id')
            ->join('sections', 'sections.id', '=', 'gfs_code_sections.section_id')
            ->join('account_types', 'account_types.id', '=', 'gfs_codes.account_type_id')
            ->join('gfs_code_categories', 'gfs_code_categories.id', '=', 'gfs_codes.gfs_code_category_id')
            ->leftJoin('fund_sources', 'fund_sources.id', '=', 'gfs_codes.fund_source_id')
            ->select('gfs_code_sections.id', 'gfs_codes.code', 'gfs_codes.description', 'account_types.name as account_type', 'gfs_code_categories.name as gfs_code_category','fund_sources.name as fund_source')
            ->where('sections.id', $section_id)
            ->whereNull('gfs_code_sections.deleted_at')
            ->get();
        return response()->json($gfs_codes, 200);
    }

    public function paginated(Request $request) {
        $section_id = UserServices::getUser()->section_id;
        $all = $this->getAllPaginated($request->perPage, $section_id);
        return response()->json($all, 200);
    }

    public function getAllPaginated($perPage, $section_id) {
        $all = DB::table('gfs_codes')
            ->join('gfs_code_sections', 'gfs_codes.id', '=', 'gfs_code_sections.gfs_code_id')
            ->join('sections', 'sections.id', '=', 'gfs_code_sections.section_id')
            ->join('account_types', 'account_types.id', '=', 'gfs_codes.account_type_id')
            ->join('gfs_code_categories', 'gfs_code_categories.id', '=', 'gfs_codes.gfs_code_category_id')
            ->leftJoin('fund_sources', 'fund_sources.id', '=', 'gfs_codes.fund_source_id')
            ->select('gfs_code_sections.id', 'gfs_codes.code', 'gfs_codes.description', 'account_types.name as account_type', 'gfs_code_categories.name as gfs_code_category','fund_sources.name as fund_source')
            ->where('sections.id', $section_id)
            ->whereNull('gfs_code_sections.deleted_at')
            ->paginate($perPage);
        return $all;
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        $section_id = UserServices::getUser()->section_id;
        $gfs_code_array = $data->gfs_code_id;
        $perPage = $request->perPage;
        for ($i = 0; $i < count($gfs_code_array); $i++) {
            $gfs_code_id = $gfs_code_array[$i];
            $exists = GfsCodeSection::where('section_id', $section_id)
                ->where('gfs_code_id', $gfs_code_id)->count();
            if ($exists == 1) {
                continue;
            }
            $obj = new GfsCodeSection();
            $obj->gfs_code_id = $gfs_code_id;
            $obj->section_id = $section_id;
            $obj->created_by = UserServices::getUser()->id;
            $obj->save();
        }
        $all = $this->getAllPaginated($perPage ? $perPage : 10, $section_id);
        $message = ["successMessage" => "SECTION_GFS_CODE_CREATED_SUCCESSFULLY", "sectionGfsCodes" => $all];
        return response()->json($message, 200);
    }

    public function delete($id) {
        $obj = GfsCodeSection::find($id);
        $obj->delete();
        $section_id = UserServices::getUser()->section_id;
        $all = $this->getAllPaginated(Input::get('perPage'), $section_id);
        $message = ["successMessage" => "SUCCESSFUL_SECTION_GFS_CODE_DELETED", "sectionGfsCodes" => $all];
        return response()->json($message, 200);
    }
}
