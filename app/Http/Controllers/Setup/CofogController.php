<?php

namespace App\Http\Controllers\Setup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use Illuminate\Support\Facades\File;
use App\Models\Setup\AdminHierarchy;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

class CofogController extends Controller
{

    public function getParentCofog () {
        $cofog = DB::table('cofogs as c2')
        ->join('cofogs as c1','c1.id','=','c2.parent_id')
        ->join('admin_hierarchies as a','a.cofog_id','=','c1.id')
        ->where('a.id','=',UserServices::getUser()->admin_hierarchy_id)
        ->whereNull('c2.deleted_at')
        ->select('c2.*')
        ->get();
        return response()->json(["cofog" => $cofog], 200);
    }

    public function getChildCofog (Request $request) {
        $parent_id = $request->parent_id;
        $cofog = DB::table('cofogs')
        ->where('parent',$parent_id)
        ->whereNull('deleted_at')
        ->get();
        return response()->json(["cofog" => $cofog], 200);
    }

    public function getBytargetId ($long_term_target_id) {
       $cofog_id =DB::table("long_term_target_references")->where("long_term_target_id",$long_term_target_id)->get();
       return response()->json(["cofog_id" => $cofog_id], 200);
    }
}
