<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\BdcGroup;
use App\Models\Setup\FinancialYear;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Validator;

class BdcGroupController extends Controller {
    public function index() {
        $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
        $user_id = UserServices::getUser()->id;
        $sector_id = DB::table('sectors')
            ->join('sections','sectors.id','=','sections.sector_id')
            ->join('users','sections.id','=','users.section_id')
            ->select('sectors.id')
            ->where('users.id',$user_id)
            ->first()->id;
        $all = BdcGroup::with(
            'bdc_main_group_fund_source',
            'bdc_main_group_fund_source.bdc_main_group',
            'bdc_main_group_fund_source.fund_source',
            'bdc_main_group_fund_source.bdc_main_group.sector')
            ->whereHas('bdc_main_group_fund_source.bdc_main_group.sector',
                function ($query) use ($sector_id) {
                    $query->where('id', $sector_id);
                })
            ->where('financial_year_id', $financial_year_id)
            ->orderBy('created_at', 'desc')
            ->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
        $user_id = UserServices::getUser()->id;
        $sector_id = DB::table('sectors')
            ->join('sections','sectors.id','=','sections.sector_id')
            ->join('users','sections.id','=','users.section_id')
            ->select('sectors.id')
            ->where('users.id',$user_id)
            ->first()->id;
        $all = BdcGroup::with(
            'bdc_main_group_fund_source',
            'bdc_main_group_fund_source.bdc_main_group',
            'bdc_main_group_fund_source.fund_source',
            'bdc_main_group_fund_source.bdc_main_group.sector')
            ->whereHas('bdc_main_group_fund_source.bdc_main_group.sector',
                function ($query) use ($sector_id) {
                    $query->where('id', $sector_id);
                })
            ->where('financial_year_id', $financial_year_id)
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
        return $all;
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, BdcGroup::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
            $obj = new BdcGroup();
            $obj->name = $request->name;
            $obj->financial_year_id = $financial_year_id;
            $obj->bdc_main_group_fund_source_id = $request->bdc_main_group_fund_source_id;
            $obj->value = $request->value;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "bdcGroups" => $all];
            return response()->json($message, 200);
        }
    }

    public function update(Request $request) {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, BdcGroup::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            $obj = BdcGroup::find($request->id);
            $obj->update($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "bdcGroups" => $all];
            return response()->json($message, 200);
        }
    }

    public function delete($id) {
        try {
            $obj = BdcGroup::find($id);
            $obj->delete();
            $all = $this->getAllPaginated(Input::get('perPage'));
            $message = ["successMessage" => "SUCCESSFUL_BDC_GROUP_DELETED", "bdcGroups" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $message = ["errorMessage" => "DATA_IN_USE"];
            return response()->json($message, 500);
        }
    }

    private function allTrashed() {
        $all = BdcGroup::with('bdc_main_group_fund_source', 'bdc_main_group_fund_source.bdc_main_group', 'bdc_main_group_fund_source.fund_source', 'bdc_main_group_fund_source.bdc_main_group.sector')->orderBy('created_at', 'desc')
            ->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        BdcGroup::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "BDC_GROUP_RESTORED", "trashedBdcGroups" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        BdcGroup::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "BDC_GROUP_DELETED_PERMANENTLY", "trashedBdcGroups" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = BdcGroup::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "BDC_GROUP_DELETED_PERMANENTLY", "trashedBdcGroups" => $all];
        return response()->json($message, 200);
    }
}
