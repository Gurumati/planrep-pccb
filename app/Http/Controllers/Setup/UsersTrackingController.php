<?php

namespace App\Http\Controllers\Setup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UsersTrackingController extends Controller
{
    //

    public function getUsersDetails(){

        $registeredUsers = DB::table('users')->select('*')->distinct()->count();

        $activeUsers = DB::table('activations')->select('*')->distinct()->count();

        $loggedUsers = DB::table('sessions')->where('is_active', true)->select('*')->distinct()->count();


           
        $userDetails = array(
            'registered_users' => $registeredUsers,
            'active_users' => $activeUsers,
            'logged_users'=> $loggedUsers
        );

      return  response()->json(['data' => $userDetails]);

    }
}
