<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Setup\ProjectDataFormQuestion;
use App\Models\Setup\ProjectDataFormQuestionItemValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProjectDataFormQuestionItemValueController extends Controller
{
    public function index($id){
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $financial_year_id = FinancialYearServices::getPlanningFinancialYear()->id;
        $question = ProjectDataFormQuestionItemValue::with('projectDataFormQuestion')
                                                     ->where('project_data_form_question_id',$id)
                                                     ->where('financial_year_id',$financial_year_id)
                                                     ->where('admin_hierarchy_id',$admin_hierarchy_id)
                                                     ->get();
        return response()->json($question);
    }

    public function getQuestionValues($project_id, $content_id){
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $financial_year_id  = FinancialYearServices::getPlanningFinancialYear()->id;

        $req_questions = ProjectDataFormQuestion::where('project_data_form_content_id',$content_id)->pluck('id');
        $values =           ProjectDataFormQuestionItemValue::with('projectDataFormQuestion')
                            ->where('financial_year_id',$financial_year_id)
                            ->where('admin_hierarchy_id',$admin_hierarchy_id)
                            ->where('project_id',$project_id)
                            ->whereIn('project_data_form_question_id', $req_questions)
                            ->get();
        $values = $values ? $values : 0;
        //Initialize the values
        if (sizeof((array)$values)){
            //Get the ids of the questions
            $questions = ProjectDataFormQuestion::where('project_data_form_content_id',$content_id)->get();
            for ($i=0; $i<sizeof($questions);$i++){
                $check = ProjectDataFormQuestionItemValue::where('financial_year_id',$financial_year_id)
                                                          ->where('admin_hierarchy_id',$admin_hierarchy_id)
                                                          ->where('project_data_form_question_id',$questions[$i]->id)
                                                          ->where('project_id',$project_id)
                                                          ->first();
                if($check == null){
                    DB::table('project_data_form_question_item_values')->insert([
                        'value'=>null,
                        'admin_hierarchy_id'=>$admin_hierarchy_id,
                        'financial_year_id' =>$financial_year_id,
                        'project_id'        =>$project_id,
                        'project_data_form_question_id' =>$questions[$i]->id
                                                                                ]);
                }

            }
            $values =           ProjectDataFormQuestionItemValue::with('projectDataFormQuestion')
                ->where('financial_year_id',$financial_year_id)
                ->where('admin_hierarchy_id',$admin_hierarchy_id)
                ->where('project_id',$project_id)
                ->whereIn('project_data_form_question_id', $req_questions)
                ->get();
        }
         return response()->json($values);
    }

    public function store(Request $request){
        $data = json_decode($request->getContent());
         $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $financial_year_id = FinancialYearServices::getPlanningFinancialYear()->id;
        $user_id = UserServices::getUser()->id;
        $project_id = $data[0]->project_id;

        //try {
        //Check if the values of the question has been saved before. If no then insert the data
            $array_size = count($data);
            for ($i = 0; $i < $array_size; $i++){
                $exists = ProjectDataFormQuestionItemValue::where('project_data_form_question_id',$data[$i]->project_data_form_question_id)
                    ->where('financial_year_id',$financial_year_id)
                    ->where('admin_hierarchy_id',$admin_hierarchy_id)
                    ->where('project_id',$project_id)
                    ->exists();

                if ($exists == false){
                    //do an insert
                    $value = new ProjectDataFormQuestionItemValue();
                    $value->financial_year_id = $financial_year_id;
                    $value->admin_hierarchy_id = $admin_hierarchy_id;
                    $value->value = isset($data[$i]->value)?$data[$i]->value:null;
                    $value->project_data_form_question_id = $data[$i]->project_data_form_question_id;
                    $value->created_by = $user_id;
                    $value->project_id = $data[$i]->project_id;
                    $value->save();
                } else {
                    //do an update
                    $values = DB::table('project_data_form_question_item_values')
                        ->where('project_data_form_question_id', $data[$i]->project_data_form_question_id)
                        ->where('financial_year_id', $financial_year_id)
                        ->where('admin_hierarchy_id', $admin_hierarchy_id)
                        ->where('project_id',$data[$i]->project_id)
                        ->update(
                            array(
                                'value' => isset($data[$i]->value)?$data[$i]->value:null
                            ));
                }
            }
            $message = ["successMessage" => "SAVED_SUCCESSFULLY",$values];
            return response()->json($message, 200);
//            } catch (QueryException $exception) {
//        $error_code = $exception->errorInfo[1];
//            //ERROR CODE 7 CATCH Duplicate
//        if ($error_code == 7) {
//        $message = ["errorMessage" => "ERROR"];
//        return response()->json($message, 400);
//        } else {
//            $message = ["errorMessage" => 'ERROR_OTHERS'];
//            return response()->json($message, 400);
//        }
//        }
            }
}
