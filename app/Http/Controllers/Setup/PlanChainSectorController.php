<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\PlanChainSector;

class PlanChainSectorController extends Controller
{
    public function indexNoRelation() {
        $all = PlanChainSector::all();
        return response()->json($all);
    }
}
