<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Http\Services\VersionServices;
use App\Models\Planning\Activity;
use App\Models\Planning\AdminHierarchySectMappings;
use App\Models\Planning\AnnualTarget;
use App\Models\Planning\LongTermTarget;
use App\Models\Planning\Mtef;
use App\Models\Planning\MtefSection;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\DecisionLevel;
use App\Models\Setup\PerformanceIndicator;
use App\Models\Setup\PlanChain;
use App\Models\Setup\PlanChainSector;
use App\Models\Setup\PlanChainType;
use App\Models\Setup\PlanChainVersion;
use App\Models\Setup\PriorityAreaPlanChain;
use App\Models\Setup\Section;
use App\Models\Setup\SectionLevel;
use App\Models\Setup\Sector;
use App\Models\Setup\Version;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class PlanChainController extends Controller
{

    private $refDoc;

    public function getByTypeAndSection($planChainTypeId, $mtefSectionId)
    {
        return ['planChains' => PlanChain::getByTypeAndSection($planChainTypeId, $mtefSectionId)];
    }

    public function index(Request $request)
    {
        $flatten = new Flatten();
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'PC');
        $section_id = UserServices::getUser()->section_id;
        $section = Section::where('id', $section_id)->get();
        $sectorIds = $flatten->flattenSectionWithChildrenGetSectors($section);
        $codes = $flatten->getAutoBudgetTemplatesCodes();
        $items = PlanChain::where(function ($query) use ($sectorIds) {
            $query->whereHas('plan_chain_type', function ($query1) {
                $query1->where('is_sectoral', false);
            })->orWhereHas('plan_chain_sectors', function ($query2) use ($sectorIds) {
                $query2->whereIn('sector_id', $sectorIds);
            });
        })->whereHas('plan_chain_versions', function ($query3) use ($versionId) {
            $query3->where('version_id', $versionId);
        })->whereNotIn('code', $codes)
           ->whereNull('deleted_at')
            ->orderBy('code', 'asc')
            ->get();
        return response()->json($items);
    }


    public function validateObjCode($objCode)
    {
        $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
       $existing = DB::table('plan_chains')
           ->where('admin_hierarchy_id',$adminHierarchyId)
           ->whereRaw("LOWER(code) = '" . strtolower($objCode)."'")
           ->whereNull('deleted_at')
           ->count();
        if($existing >0){
            $message = ["exist" => true];
            return response()->json($message, 200);
        }
        else{
            $message = ["exist" => false];
            return response()->json($message, 200);
        }
    }

    public function priorityAreas(Request $request)
    {
        $items = $this->planChainPriorityAreas($request->id);
        return apiResponse(200, "Success", $items, true, []);
    }

    public function addPriorityAreas(Request $request)
    {
        $planChainId = $request->planChainId;
        foreach ($request->priorityAreas as $item) {
            $count = PriorityAreaPlanChain::where('plan_chain_id', $planChainId)
                ->where('priority_area_id', $item['id'])
                ->count();
            if ($count == 0) {
                $pc = new PriorityAreaPlanChain();
                $pc->plan_chain_id = $planChainId;
                $pc->priority_area_id = $item['id'];
                $pc->save();
            }
        }
        $items = $this->planChainPriorityAreas($planChainId);
        return apiResponse(200, "Priority Areas Assigned Successfully!", $items, true, []);
    }

    public function removePriorityArea(Request $request)
    {
        $id = $request->id;
        $pp = PriorityAreaPlanChain::find($id);
        $pp->delete();
        $planChainId = $request->planChainId;
        $items = $this->planChainPriorityAreas($planChainId);
        return apiResponse(200, "Priority Areas Removed Successfully!", $items, true, []);
    }

    public function getNextFinancialYearVersion($currentFinancialYearId, $planChainId)
    {
        return PlanChain::getNextFinancialYearVersion($currentFinancialYearId, $planChainId);
    }

    public function addToVersion($planChainId, $versionId)
    {
        return PlanChain::addToVersion($planChainId, $versionId);
    }

    public function fetchAll(Request $request)
    {
        $flatten = new Flatten();
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'PC');
        $section_id = UserServices::getUser()->section_id;
        $section = Section::where('id', $section_id)->get();
        $sectorIds = $flatten->flattenSectionWithChildrenGetSectors($section);
        $codes = $flatten->getAutoBudgetTemplatesCodes();
        $items = PlanChain::where(function ($query) use ($sectorIds) {
            $query->whereHas('plan_chain_type', function ($query1) {
                $query1->where('is_sectoral', false);
            })->orWhereHas('plan_chain_sectors', function ($query2) use ($sectorIds) {
                $query2->whereIn('sector_id', $sectorIds);
            });
        })->whereHas('plan_chain_versions', function ($query3) use ($versionId) {
            $query3->where('version_id', $versionId);
        })->whereNotIn('code', $codes)
            ->orderBy('plan_chain_type_id', 'asc')
            ->orderBy('code', 'asc')
            ->get();
        return response()->json(['planChains' => $items], 200);
    }

    public function getTopLevel()
    {
        return ['planChains' => PlanChain::getTopLevel()];
    }

    public function getChildren($parentId)
    {
        return ['planChains' => PlanChain::getChildren($parentId)];
    }

    public function performanceIndicators(Request $request)
    {
        $plan_chain_id = $request->plan_chain_id;
        $all = PerformanceIndicator::where('plan_chain_id', $plan_chain_id)
            ->orderBy('created_at', 'desc')->get();
        $data = ["performanceIndicators" => $all];
        return response()->json($data, 200);
    }

    public function sectors(Request $request)
    {
        $plan_chain_id = $request->plan_chain_id;
        $all = DB::table("sectors as s")
            ->join("plan_chain_sectors as pcs", "s.id", "pcs.sector_id")
            ->join("plan_chains as pc", "pc.id", "pcs.plan_chain_id")
            ->where("pc.id", $plan_chain_id)->select("s.id", "s.name")->get();
        $data = ["sectors" => $all];
        return response()->json($data, 200);
    }

    public function copy(Request $request)
    {
        $versionId = $request->sourceVersionId;
        $sourceFinancialYearId = $request->sourceFinancialYearId;
        $destinationVersionId = $request->destinationVersionId;

        $sql = "select distinct pc.*
            from plan_chains pc join plan_chain_types pt on pt.id = pc.plan_chain_type_id
            join plan_chain_versions pv on pv.plan_chain_id = pc.id
            join financial_year_versions fv on fv.version_id = pv.version_id
            left join plan_chain_sectors ps on pc.id = ps.plan_chain_id
            left join sectors s on s.id = ps.sector_id
            where pc.deleted_at is null
              and fv.financial_year_id = $sourceFinancialYearId and fv.version_id = $versionId
              and s.id is null and pc.parent_id is null and pt.is_sectoral = false
                ORDER BY pc.code asc";
        $items = DB::select($sql);
        DB::transaction(function () use ($request, $items, $versionId, $sourceFinancialYearId, $destinationVersionId) {
            foreach ($items as $item) {

                $pcv = new PlanChainVersion();
                $pcv->plan_chain_id = $item->id;
                $pcv->version_id = $destinationVersionId;
                $pcv->save();

                $pas = DB::select("SELECT priority_area_id from priority_area_plan_chains where plan_chain_id = $item->id");
                foreach ($pas as $pa) {
                    $npa = new PriorityAreaPlanChain();
                    $npa->priority_area_id = $pa->priority_area_id;
                    $npa->plan_chain_id = $pa->plan_chain_id;
                    $npa->save();
                }

                $children = DB::select("select distinct pc.*,string_agg(s.id::text,',') as sectors
                from plan_chains pc join plan_chain_types pt on pt.id = pc.plan_chain_type_id
                join plan_chain_versions pv on pv.plan_chain_id = pc.id
                join financial_year_versions fv on fv.version_id = pv.version_id
                left join plan_chain_sectors ps on pc.id = ps.plan_chain_id
                left join sectors s on s.id = ps.sector_id
                where pc.deleted_at is null
                  and fv.financial_year_id = $sourceFinancialYearId and fv.version_id = $versionId
                  and s.id is not null and pc.parent_id = $item->id and pt.is_sectoral = true
                  GROUP BY pc.id ORDER BY pc.code asc");

                foreach ($children as $child) {
                    $pc = new PlanChain();
                    $pc->description = $child->description;
                    $pc->code = $child->code;
                    $pc->is_active = $child->is_active;
                    $pc->parent_id = $plan_chain_id;
                    $pc->plan_chain_type_id = $child->plan_chain_type_id;
                    $pc->save();

                    $new_plan_chain_id = $pc->id;

                    $pcv = new PlanChainVersion();
                    $pcv->plan_chain_id = $new_plan_chain_id;
                    $pcv->version_id = $destinationVersionId;
                    $pcv->save();

                    $pasc = DB::select("SELECT priority_area_id from priority_area_plan_chains where plan_chain_id = $child->id");
                    foreach ($pasc as $pa) {
                        $npa = new PriorityAreaPlanChain();
                        $npa->priority_area_id = $pa->priority_area_id;
                        $npa->plan_chain_id = $pa->$new_plan_chain_id;
                        $npa->save();
                    }

                    $sectors = explode(',', $child->sectors);

                    foreach ($sectors as $sector) {
                        $s = new PlanChainSector();
                        $s->plan_chain_id = $new_plan_chain_id;
                        $s->sector_id = $sector;
                        $s->save();
                    }
                }
            }
        });
        return apiResponse(201, "Planning sequence copied successfully", [], true, []);
    }

    public function getAllPaginated($financialYearId, $versionId, $planChainTypeId, $perPage)
    {
        //$perpage = $request->perPage?$request->perPage:10;
        $flatten = new Flatten();
        $section_id = UserServices::getUser()->section_id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section = Section::where('id', $section_id)->get();
        $sectorIds = $flatten->flattenSectionWithChildrenGetSectors($section);
        $codes = $flatten->getAutoBudgetTemplatesCodes();
        $searchText = Input::get('searchText');
        $searchText = isset($searchText) ? "'%" . strtolower($searchText) . "%'" : "'%'";

        if ($planChainTypeId == 1){
            $sql = "select DISTINCT pc.*,pct.name component,count(pi.*) count from plan_chains pc
                     join plan_chain_types pct  on pc.plan_chain_type_id = pct.id
                     join plan_chain_versions pcv on pc.id = pcv.plan_chain_id
                     join financial_year_versions fyv on fyv.version_id = pcv.version_id
                     left join performance_indicators pi on pc.id = pi.plan_chain_id
                        where fyv.version_id = $versionId
                        and financial_year_id =$financialYearId and pc.deleted_at is null
                        and (admin_hierarchy_id is null
                        or admin_hierarchy_id = $admin_hierarchy_id)
                        and parent_id is null
                        and pct.is_sectoral = false
                        and pc.code !='0' group by pc.id,pi.id,pct.name";
        }else{
            $sql = "select DISTINCT pc.*,pct.name component,count(pi.*) count from plan_chains pc
                     join plan_chain_types pct  on pc.plan_chain_type_id = pct.id
                     join plan_chain_versions pcv on pc.id = pcv.plan_chain_id
                     join financial_year_versions fyv on fyv.version_id = pcv.version_id
                     left join performance_indicators pi on pc.id = pi.plan_chain_id
                      where fyv.version_id = $versionId
                      and financial_year_id =$financialYearId
                      and pc.deleted_at is null
                      and admin_hierarchy_id = $admin_hierarchy_id
                      and parent_id is not null
                      and pct.is_sectoral = true  group by pc.id,pi.id,pct.name";
        }
        $planChains = DB::select(DB::raw($sql));
        $total =sizeof($planChains);

        foreach($planChains as $planChain){
            $planChain->plan_chain_type = DB::table('plan_chain_types')->select('id','name')->where('id',$planChain->plan_chain_type_id)->first();
            $planChain->parent = DB::table('plan_chains')->select('id','description','code','parent_id','plan_chain_type_id')->where('id',$planChain->parent_id)->first();
            $planChain->performance_indicators = DB::table('performance_indicators')->select('id','description','plan_chain_id')->where('plan_chain_id',$planChain->id)->get();
            $planChain->nextVersion = DB::table('financial_year_versions as fyv')->join('financial_years as fy','fyv.financial_year_id','=','fy.previous' )->select('name as financialYear')->where('fyv.financial_year_id',$financialYearId)->where('fyv.version_id',$versionId)->first();
        }

        $response = ['data' => $planChains,'total'=>$total,'current_page'=>1];
        return $response;
    }

    public function returnCanTargetSection($sectionId, $adminHierarchyId)
    {
        $sections = Section::find($sectionId);
        $sectionLevel = SectionLevel::find($sections->section_level_id);
        $adminHierarchy = AdminHierarchy::find($adminHierarchyId);
        $mapping = AdminHierarchySectMappings::where('admin_hierarchy_level_id', $adminHierarchy->admin_hierarchy_level_id)->where('section_id', $sectionId)->first();
        if ($mapping->can_target) {
            return $sectionId;
        } else {
            $count = Section::where('id', $sections->parent_id)->count();
            if ($count > 0) {
                $sectionParent = Section::find($sections->parent_id);
                $this->returnCanTargetSection($sectionParent->id, $adminHierarchyId);
            } else {

                return 0;
            }
        }


    }

    public function getFullChainIndex($adminHierarchyId, $section_id, $plan_type)
    {

        $this->setRefDocId($adminHierarchyId);
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $userHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $userSectionId = UserServices::getUser()->section_id;
        $userSection = Section::find($userSectionId);

        // $targetSection = $this->returnCanTargetSection($section_id,$adminHierarchyId);

        $mtef = Mtef::where('financial_year_id', $financialYear->id)
            ->where('admin_hierarchy_id', $adminHierarchyId)
            ->where('plan_type', $plan_type)
            ->first();
        $refDoc = $this->refDoc;
        $mtef_id = $mtef->id;
        $mtefSection = MtefSection::where('mtef_id', $mtef_id)->where('section_id', $section_id)->first();
        $mtef_section_id = $mtefSection->id;

        $activities = Activity::where('mtef_section_id', $mtef_section_id)->get();
        $activityIds = [];
        $annualTargetIds = [];
        $longTermTargetIds = [];
        $planChainIds = [];
        foreach ($activities as $activity) {
            $activityIds[] = $activity->id;
            if (!in_array($activity->mtef_annual_target_id, $annualTargetIds)) {
                $annualTargetIds[] = $activity->mtef_annual_target_id;
            }
        }
        $annualTargets = AnnualTarget::whereIn('id', $annualTargetIds)->get();
        foreach ($annualTargets as $annualTarget) {
            $longTermTargetIds[] = $annualTarget->long_term_target_id;
        }
        $longTermTargets = LongTermTarget::whereIn('id', $longTermTargetIds)->get();
        foreach ($longTermTargets as $longTermTarget) {
            $planChainIds[] = $longTermTarget->plan_chain_id;
        }

        $all = PlanChain::with(
            [
                'longTermTargets' => function ($query) use ($refDoc, $longTermTargetIds) {
                    $query
                        ->where('reference_document_id', $refDoc)
                        ->whereIn('id', $longTermTargetIds);
                },
                'longTermTargets.annualTargets' => function ($query) use ($annualTargetIds) {
                    $query->whereIn('id', $annualTargetIds);
                }, 'longTermTargets.annualTargets.activities' => function ($query) use ($mtef_section_id) {
                    $query
                        ->where('mtef_section_id', $mtef_section_id);
                }
            ]
        )
            ->where('is_active', true)
            ->whereIn('id', $planChainIds)
            ->get();


        $inMtefSectionComments = null;
        $mtefDecisionLevel = DecisionLevel::find($mtef->decision_level_id);
        if (isset($mtefDecisionLevel) && $mtefDecisionLevel->is_default && $userHierarchyId == $adminHierarchyId) {
            $toMeComments = DB::table('mtef_section_comments')->where('mtef_section_id', $mtefSection->id)->where('to_section_level', $userSection->section_level_id)->orderBy('created_at', 'desc')->first();

            $topMost = DB::table('mtef_section_comments')->where('mtef_section_id', $mtefSection->id)->orderBy('created_at', 'desc')->first();
            if ($topMost->id == $toMeComments->id) {
                $inMtefSectionComments = $toMeComments;
            } else {
                $inMtefSectionComments = null;
            }

        }

        $message = ["budget" => $all, "inMtefSectionComments" => $inMtefSectionComments];

        return response()->json($message, 200);
    }

    public function setRefDocId($adminHierarchyId)
    {
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $refDocument = DB::table('reference_documents as r')
            ->join('financial_years as sF', 'sF.id', '=', 'r.start_financial_year')
            ->join('financial_years as eF', 'eF.id', '=', 'r.end_financial_year')
            ->where('r.admin_hierarchy_id', '=', $adminHierarchyId)
            ->where('sF.start_date', '<=', $financialYear->start_date)
            ->where('eF.end_date', '>=', $financialYear->end_date)->select('r.*')->first();
        $this->refDoc = $refDocument->id;
    }

    public function paginateIndex(Request $request)
    {
        $all = $this->getAllPaginated($request->financialYearId, $request->versionId, $request->planChainTypeId, $request->perPage);
        return response()->json(['planChains' => $all]);
    }

    public function store(Request $request)
    {
        set_time_limit(3000);
        $data = json_decode($request->getContent());
        DB::transaction(function () use ($data) {
            $planChainType = (new \App\Models\Setup\PlanChainType)->find($data->plan_chain_type->id);
            $code = null;
            if ($planChainType && $planChainType->is_incremental) {
                if ($data->parent_id) {
                    $parent = PlanChain::find($data->parent_id);
                    $codeString = trim($parent->code) . "%";
                    $lastPlanChainCode = DB::table('plan_chains')->where('code', 'like', $codeString)
                        ->where('parent_id', $data->parent_id)
                        ->where('plan_chain_type_id', $data->plan_chain_type->id)->max('code');
                    if ($lastPlanChainCode != null) {
                        $codeIndex = str_replace($parent->code, "", $lastPlanChainCode);
                        $newIndex = intval($codeIndex) + 1;
                        $newIndex = ($newIndex < 10) ? "0" . $newIndex : $newIndex;
                        $code = $parent->code . $newIndex;
                    } else {
                        $code = $parent->code . "01";
                    }
                } else {
                    $lastPlanChainCode = DB::table('plan_chains')
                        ->where('plan_chain_type_id', $data->plan_chain_type->id)->max('code');
                    if ($lastPlanChainCode != null) {
                        $newIndex = intval($lastPlanChainCode) + 1;
                        $newIndex = ($newIndex < 10) ? "0" . $newIndex : $newIndex;
                        $code = $newIndex;
                    } else {
                        $code = "01";
                    }
                }
            } else {
                $code = $data->code;
            }
            $id = DB::table('plan_chains')->insertGetId(
                [
                    'code' => $code,
                    'plan_chain_type_id' => $data->plan_chain_type->id,
                    'parent_id' => isset($data->parent_id) ? $data->parent_id : null,
                    'description' => $data->description,
                    'is_active' => true,
                    'created_by' => UserServices::getUser()->id,
                    'admin_hierarchy_id' => UserServices::getUser()->admin_hierarchy_id,
                    'created_at' => date('Y-m-d H:i:s')
                ]
            );

            $pv = new PlanChainVersion();
            $pv->plan_chain_id = $id;
            $pv->version_id = $data->versionId;
            $pv->save();

            $planChainSectors = PlanChainSector::where('plan_chain_id', $id)->get();

            foreach ($planChainSectors as $planChainSector) {
                $planChainSector = PlanChainSector::find($planChainSector->id);
                $planChainSector->delete();
            }
            $sectionId = UserServices::getUser()->section_id;
             $sec = DB::table('sections as s')
                ->join('sections as s2','s2.id','s.parent_id')
                ->join('sections as s3','s3.id','s2.parent_id')
                ->join('sections as s4','s4.id','s3.parent_id')
                ->join('mtef_sections as ms','s.id','ms.section_id')
                ->join('mtefs as m','m.id','ms.mtef_id')
                ->select('s.sector_id')
                ->where('s4.id',$sectionId)
                ->where('m.admin_hierarchy_id',UserServices::getUser()->admin_hierarchy_id)
                ->first();
            $obj = new PlanChainSector();
            $obj->sector_id = $sec->sector_id;
            $obj->plan_chain_id = $id;
            $obj->created_by = UserServices::getUser()->id;
            $obj->save();
        });
        $planChains = $this->getAllPaginated($data->financialYearId, $data->versionId, $data->plan_chain_type->id, $data->perPage);
        $message = ["successMessage" => "SUCCESSFUL_PLAN_CHAIN_CREATED", "planChains" => $planChains];
        return response()->json($message, 200);
    }

    public function update(Request $request)
    {
        $data = json_decode($request->getContent());
        DB::transaction(function () use ($data) {
            $planChain = PlanChain::find($data->id);
            $planChain->code = $data->code;
            $planChain->plan_chain_type_id = $data->plan_chain_type_id;
            $planChain->parent_id = (isset($data->parent_id)) ? $data->parent_id : null;
            $planChain->description = $data->description;
            $planChain->save();

            $planChainSectors = PlanChainSector::where('plan_chain_id', $data->id)->get();

            foreach ($planChainSectors as $planChainSector) {
                $planChainSector = PlanChainSector::find($planChainSector->id);
                $planChainSector->delete();
            }

            foreach ($data->sectors as $sector) {

                $obj = new PlanChainSector();
                $obj->sector_id = $sector;
                $obj->plan_chain_id = $data->id;
                $obj->created_by = UserServices::getUser()->id;
                $obj->save();
            }
        });
        $all = $this->getAllPaginated($request->financialYearId, $request->versionId, $request->planChainTypeId, $request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_PLAN_CHAIN_UPDATED", "planChains" => $all];
        return response()->json($message, 200);
    }


    public function delete($id)
    {
        $planChain = PlanChain::find($id);
        $planChain->delete();
        $all = $this->getAllPaginated(Input::get('financialYearId'), Input::get('versionId'), Input::get('planChainTypeId'), Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_PLAN_CHAIN_DELETED", "planChains" => $all];
        return response()->json($message, 200);
    }

    public function activate(Request $request)
    {
        try {
            $data = json_decode($request->getContent());
            $planChain = PlanChain::find($data->id);
            $planChain->is_active = $data->is_active;
            $planChain->save();
            $all = $this->getAllPaginated($request->financialYearId, $request->versionId, $request->planChainTypeId, $request->perPage);
            if ($planChain->is_active) {
                $message = ["activateMessage" => "SUCCESSFUL_PLAN_CHAIN_ACTIVATED", "planChains" => $all];
            } else {
                $message = ["activateMessage" => "SUCCESSFUL_PLAN_CHAIN_DEACTIVATED", "planChains" => $all];
            }
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_PLAN_CHAIN_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

    public function loadParentPlanChains(Request $request)
    {
        $id = $request->childId;
        $financialYearId = $request->financialYearId;
        $versionId = $request->versionId;

        $childChainLevel = PlanChainType::find($id);
        $flatten = new Flatten();
        $section_id = UserServices::getUser()->section_id;

        if ($section_id != null) {
            $section = Section::where('id', $section_id)->get();
            $sectorIds = $flatten->flattenSectionWithChildrenGetSectors($section);
        } else {
            $sectors = Sector::all();
            $sectorIds = $flatten->flattenSectors($sectors);
        }

        $codes = $flatten->getAutoBudgetTemplatesCodes();
        $parentPlanChains = DB::table('plan_chains as pc')
            ->join('plan_chain_types as pct', 'pct.id', '=', 'pc.plan_chain_type_id')
            ->join('plan_chain_versions as pv', 'pc.id', 'pv.plan_chain_id')
            ->join('versions as v', 'v.id', 'pv.version_id')
            ->join('financial_year_versions as fv', 'fv.version_id', 'v.id')
            ->leftjoin('plan_chain_sectors as pcs', 'pc.id', '=', 'pcs.plan_chain_id')
            ->where(function ($query) use ($sectorIds) {
                $query->where('pct.is_sectoral', false)->orWhereIn('pcs.sector_id', $sectorIds);
            })->whereNull('pc.deleted_at')
            ->whereNotIn('pc.code', $codes)
            ->where('fv.financial_year_id', $financialYearId)
            ->where('fv.version_id', $versionId)
            ->where('pct.hierarchy_level', '=', $childChainLevel->hierarchy_level - 1)
            ->orderBy('pc.created_at', 'asc')
            ->select('pc.*')->get();
        return response()->json($parentPlanChains);
    }

    public function loadParentPlanChain($id, $financialYearId, $versionId)
    {
        $childChainLevel = PlanChainType::find($id);
        $flatten = new Flatten();
        $adminId = UserServices::getUser()->admin_hierarchy_id;
        $section_id = UserServices::getUser()->section_id;

        $sql = "select distinct pc.* from plan_chains pc
                join plan_chain_types pct  on pc.plan_chain_type_id = pct.id
                join plan_chain_versions pcv on pc.id = pcv.plan_chain_id
                join financial_year_versions fyv on fyv.version_id = pcv.version_id
                where fyv.version_id = $versionId
                and financial_year_id =$financialYearId
                and (pc.deleted_at is null and pc.code !='0')
                and (admin_hierarchy_id is null or admin_hierarchy_id = $adminId)
                and parent_id is null order by pc.code";
        $parentPlanChains = DB::select(DB::raw($sql));
        return response()->json($parentPlanChains);
    }

    public function loadPlanChainType($id)
    {
        $flatten = new Flatten();
        $section_id = UserServices::getUser()->section_id;
        $section = Section::where('id', $section_id)->get();
        $sectorIds = $flatten->flattenSectionWithChildrenGetSectors($section);
        $codes = $flatten->getAutoBudgetTemplatesCodes();

        $planChains = PlanChain::where('plan_chain_type_id', $id)->where(function ($query) use ($sectorIds) {
            $query->whereHas('plan_chain_type', function ($query1) {
                $query1->where('is_sectoral', false);
            })->orWhereHas('plan_chain_sectors', function ($query2) use ($sectorIds) {
                $query2->whereIn('sector_id', $sectorIds);
            });
        })->where('is_active', true)
            ->whereNotIn('code', $codes)->orderBy('plan_chain_type_id', 'asc')->orderBy('code', 'asc')->paginate(Input::get('perPage'));
        return response()->json($planChains);
    }

    public function getLowestPlanChains()
    {
        set_time_limit(3000);
        $flatten = new Flatten();
        $section_id = UserServices::getUser()->section_id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section = Section::where('id', $section_id)->get();
        $sectorIds = $flatten->flattenSectionWithChildrenGetSectors($section);
        $codes = $flatten->getAutoBudgetTemplatesCodes();
        

        $versionId = Version::getCurrentVersionId('PC');

        $lowerPlanChainType = PlanChainType::where('hierarchy_level', PlanChainType
        ::whereNull('deleted_at')->max('hierarchy_level'))->get();
        
        foreach ($lowerPlanChainType as &$plan) {
            $plan->plan_chains = DB::table('plan_chains as pch')
            ->join('plan_chain_versions as pcv','pcv.plan_chain_id','pch.id')
            ->join('plan_chain_sectors as pcs','pcs.plan_chain_id','pch.id')
            ->whereNotIn('pch.code',$codes)
            ->where('pch.admin_hierarchy_id',$admin_hierarchy_id)
            ->where('pch.plan_chain_type_id',2)
            ->whereNull('pch.deleted_at')
            ->where('pcv.version_id', $versionId)->select('pch.*')->orderBy('pch.code', 'asc')->distinct()->get();
            foreach ($plan->plan_chains as &$p) {
                $p->plan_chain_type = PlanChainType::where('id',$p->plan_chain_type_id)
                ->where('is_sectoral', false)
                ->select('id','name')->first();
                // $p->parent = PlanChain::where('id',$p->parent_id)
                // ->select('id','code','description','parent_id')->first();
            }
        }
        return response()->json($lowerPlanChainType);
    }

    public function lowestPlanChains(Request $request)
    {
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'PC');
        $flatten = new Flatten();
        $section_id = UserServices::getUser()->section_id;
        $section = Section::where('id', $section_id)->get();
        $sectorIds = $flatten->flattenSectionWithChildrenGetSectors($section);
        $codes = $flatten->getAutoBudgetTemplatesCodes();
        /* this code were not working */

//        $lowerPlanChainType = PlanChainType::with(
//            ['planChains' => function ($query) use ($codes, $sectorIds, $versionId) {
//            $query->whereNotIn('code', $codes)
//                ->where(function ($query0) use ($sectorIds, $versionId) {
//                $query0->whereHas('plan_chain_type', function ($query1) {
//                    $query1->where('is_sectoral', false);
//                })
//                    ->orWhereHas('plan_chain_sectors', function ($query2) use ($sectorIds) {
//                    $query2->whereIn('sector_id', $sectorIds);
//                })
//                    ->where('admin_hierarchy_id',UserServices::getUser()->admin_hierarchy_id);
//            })->whereHas('plan_chain_versions', function ($query3) use ($versionId) {
//                $query3->where('version_id', $versionId);
//            })->orderBy('code', 'asc');
//        }])
//            ->where('hierarchy_level', DB::table('plan_chain_types')
//            ->whereNull('deleted_at')->max('hierarchy_level'))->get();

        /* End this code were not working */
        $lowerPlanChainType = DB::table('plan_chains')
            ->where('admin_hierarchy_id',UserServices::getUser()->admin_hierarchy_id)
            ->whereNull('deleted_at')
            ->where('is_active',true)
            ->where('plan_chain_type_id',2)->get();
         return response()->json(["items" => $lowerPlanChainType], 200);
    }

    public function exportToCsv(Request $request)
    {
        $financialYearId = $request->financialYearId;
        $versionId = $request->versionId;
        $planChainTypeId = $request->planChainTypeId;
        $query = "select distinct pc.code,pc.description,s.name as sector,pt.name as planning_component
                    from plan_chains pc join plan_chain_types pt on pt.id = pc.plan_chain_type_id
                                        join plan_chain_versions pv on pv.plan_chain_id = pc.id
                                        join financial_year_versions fv on fv.version_id = pv.version_id
                                        left join plan_chain_sectors ps on pc.id = ps.plan_chain_id
                                        left join sectors s on s.id = ps.sector_id
                    where pc.deleted_at is null
                      and fv.financial_year_id = $financialYearId
                      and fv.version_id = $versionId
                      and pt.id = $planChainTypeId
                        ORDER BY pc.code asc";
        $items = DB::select($query);

        $array = array();
        foreach ($items as $value) {
            $data = array();
            $data['CODE'] = $value->code;
            $data['DESCRIPTION'] = $value->description;
            $data['SECTOR'] = $value->sector;
            $data['TYPE'] = $value->planning_component;
            array_push($array, $data);
        }

        Excel::create('PLAN_CHAINS' . time(), function ($excel) use ($array) {
            $excel->sheet('DATA', function ($sheet) use ($array) {
                $sheet->fromArray($array);
            });
        })->export('csv');
    }

    public function planChainPriorityAreas($planChainId)
    {
        return PriorityAreaPlanChain::with('priority_area')->where('plan_chain_id', $planChainId)->get();
    }
}
