<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\Planning\PlanningServices;
use App\Http\Services\UserServices;
use App\Models\Setup\Reference;
use App\Models\Setup\ReferenceType;
use App\Models\Setup\ReferenceTypeSector;
use App\Models\Setup\ReferenceTypeVersion;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use referenceTypes;

class ReferenceTypeController extends Controller
{

    public function index(Request $request)
    {
        if (isset($request->versionId)) {
            $items = ReferenceType::with('referenceTypeSectors')
                ->whereHas("referenceTypeVersions", function ($version) use ($request) {
                    $version->where("version_id", $request->versionId);
                })->get();
        } else {
            $items = ReferenceType::with('referenceTypeSectors')->get();
        }
        return response()->json($items);
    }

    public function typeWithReference($linkLevel)
    {
        $planningServices = new PlanningServices();
        $referenceType = $planningServices->getReferenceTypeWithReference($linkLevel,null);
        return response()->json($referenceType);
    }

    public function store(Request $request)
    {

        $data = $request->all();
        $validator = Validator::make($data, ReferenceType::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            DB::transaction(function () use ($data, $request) {
                $rt = ReferenceType::create($data);
                $id = $rt->id;

                if (isset($data['sectors'])) {
                    foreach ($data['sectors'] as $sectorId) {
                        DB::table("reference_type_sectors")->insert(array(
                            "reference_type_id" => $id,
                            "sector_id" => $sectorId
                        ));
                    }
                }

                $rtv = new ReferenceTypeVersion();
                $rtv->version_id = $request->versionId;
                $rtv->reference_type_id = $id;
                $rtv->save();
            });
            if (isset($request->versionId)) {
                $items = ReferenceType::with('referenceTypeSectors')
                    ->whereHas("referenceTypeVersions", function ($version) use ($request) {
                        $version->where("version_id", $request->versionId);
                    })->get();
            } else {
                $items = ReferenceType::with('referenceTypeSectors')->get();
            }
            $message = ["successMessage" => "SUCCESSFUL_REFERENCE_TYPE_CREATED", "referenceTypes" => $items];
            return response()->json($message, 200);
        }
    }

    public function copy(Request $request)
    {
        $sourceVersionId = $request->sourceVersionId;
        $destinationVersionId = $request->destinationVersionId;
        $referenceTypes = DB::select("select rt.*,string_agg(rts.sector_id::text,',') as sectors from reference_types rt
                            join reference_type_versions rtv on rt.id = rtv.reference_type_id
                            left join reference_type_sectors rts on rt.id = rts.reference_type_id
                            where rtv.version_id = $sourceVersionId group by rt.id");
        foreach ($referenceTypes as $referenceType) {
            DB::transaction(function () use ($referenceType,$destinationVersionId){
                $old_reference_type_id = $referenceType->id;
                $sectors = $referenceType->sectors;
                $rt = new ReferenceType();
                $rt->name = $referenceType->name;
                $rt->description = $referenceType->description;
                $rt->multiple_select = $referenceType->multiple_select;
                $rt->link_level = $referenceType->link_level;
                $rt->save();
                $new_reference_type_id = $rt->id;

                $rv = new ReferenceTypeVersion();
                $rv->version_id = $destinationVersionId;
                $rv->reference_type_id = $new_reference_type_id;
                $rv->save();

                $referenceDocs = Reference::where("reference_type_id",$old_reference_type_id)
                    ->whereNull('parent_id')
                    ->get();
                foreach ($referenceDocs as $referenceDoc) {
                    $rd = new Reference();
                    $rd->code = $referenceDoc->code;
                    $rd->name = $referenceDoc->name;
                    $rd->is_active = $referenceDoc->is_active;
                    $rd->description = $referenceDoc->description;
                    $rd->reference_type_id = $new_reference_type_id;
                    $rd->parent_id = null;
                    $rd->link_level = $referenceDoc->link_level;
                    $rd->save();
                }

                $sectorArray = explode(',', $sectors);
                foreach ($sectorArray as $sector_id) {
                    $count = ReferenceTypeSector::where("sector_id",$sector_id)
                        ->where("reference_type_id",$new_reference_type_id)
                        ->count();
                    if($count == 0){
                        $rts = new ReferenceTypeSector();
                        $rts->sector_id = $sector_id;
                        $rts->reference_type_id = $new_reference_type_id;
                        $rts->save();
                    }
                }
            });
        }
        return apiResponse(201,"National References copied successfully!",[],true,[]);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, ReferenceType::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            DB::transaction(function () use ($data, $request) {
                $obj = ReferenceType::find($request->id);
                $obj->update($data);
                if (isset($data['sectors'])) {
                    DB::table("reference_type_sectors")->where('reference_type_id', $request->id)->delete();
                    foreach ($data['sectors'] as $sectorId) {
                        DB::table("reference_type_sectors")->insert(array(
                            "reference_type_id" => $request->id,
                            "sector_id" => $sectorId
                        ));
                    }
                }
            });
            if (isset($request->versionId)) {
                $items = ReferenceType::with('referenceTypeSectors')
                    ->whereHas("referenceTypeVersions", function ($version) use ($request) {
                        $version->where("version_id", $request->versionId);
                    })->get();
            } else {
                $items = ReferenceType::with('referenceTypeSectors')->get();
            }
            $message = ["successMessage" => "SUCCESSFUL_REFERENCE_TYPE_UPDATED", "referenceTypes" => $items];
            return response()->json($message, 200);
        }
    }

    public function delete($id,$versionId)
    {
        DB::transaction(function () use($id,$versionId){
            DB::table("reference_type_sectors")->where('reference_type_id', $id)->delete();
            DB::table("reference_type_versions")
                ->where('version_id', $versionId)
                ->where('reference_type_id', $id)
                ->delete();
            $referenceType = ReferenceType::find($id);
            $referenceType->delete();
        });
        if (isset($versionId)) {
            $items = ReferenceType::with('referenceTypeSectors')
                ->whereHas("referenceTypeVersions", function ($version) use ($versionId) {
                    $version->where("version_id", $versionId);
                })->get();
        } else {
            $items = ReferenceType::with('referenceTypeSectors')->get();
        }
        $message = ["successMessage" => "SUCCESSFULLY_REFERENCE_TYPE_DELETED", "referenceTypes" => $items];
        return response()->json($message);
    }
}
