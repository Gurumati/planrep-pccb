<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\FacilityStarRating;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class FacilityStarRatingController extends Controller {
    public function index() {
        $all = FacilityStarRating::orderBy('created_at','asc')->get();
        $data = ["facilityStarRatings" => $all];
        return response()->json($data);
    }

    public function getAllPaginated($perPage) {
        return FacilityStarRating::orderBy('created_at','asc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new FacilityStarRating();
            $obj->name = $data->name;
            $obj->number = $data->number;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_number = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_number.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = FacilityStarRating::find($data->id);
            $obj->name = $data->name;
            $obj->number = $data->number;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_number = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_number.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = FacilityStarRating::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = FacilityStarRating::orderBy('created_at', 'asc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        $message = ["trashedItems" => $all];
        return response()->json($message,200);
    }

    public function restore($id) {
        FacilityStarRating::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        FacilityStarRating::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = FacilityStarRating::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }
}
