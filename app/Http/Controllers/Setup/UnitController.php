<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\Unit;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Sabberworm\CSS\Value\Size;

class UnitController extends Controller {
    public function index() {
        $units = Unit::where('is_active', true)->orderBy('name')->get();
        return response()->json($units);
    }

    public function getAllPaginated($perPage) {
        return Unit::orderBy('name')->paginate($perPage);
    }

    public function paginate(Request $request) {
        $units = $this->getAllPaginated($request->perPage);
        return response()->json($units);
    }

    public function store(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $unit = new Unit();
            $unit->name = $data->name;
            $unit->symbol = $data->symbol;
            $unit->sort_order = $data->sort_order;
            $unit->is_active = True;
            $unit->save();
            $units = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFULLY_UNIT_CREATED", "units" => $units];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_UNIT_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }


    }

    public function update(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $unit = Unit::find($data->id);
            $unit->name = $data->name;
            $unit->symbol = $data->symbol;
            $unit->sort_order = $data->sort_order;
            $unit->is_active = True;
            $unit->save();
            $units = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFULLY_UNIT_UPDATED", "units" => $units];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_UNIT_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

    public function toggleUnit(Request $request) {
        //
        $data = json_decode($request->getContent());
        $object = Unit::find($data->id);
        $object->is_active = $data->is_active;
        $object->save();
        $units = $this->getAllPaginated($request->perPage);
        if ($data->is_active == false) {
            $feedback = ["action" => "UNIT_DEACTIVATED", "alertType" => "warning", "facilities" => $units];
        } else {
            $feedback = ["action" => "UNIT_ACTIVATED", "alertType" => "success", "facilities" => $units];
        }
        return response()->json($feedback, 200);
    }

    public function delete($id) {
        $unit = Unit::find($id);
        $unit->delete();
        $units = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFULLY_UNIT_DELETED", "units" => $units];
        return response()->json($message, 200);
    }

    public function upload() {
        if (Input::hasFile('file')) {
            $path = Input::file('file')->getRealPath();
            $data = Excel::selectSheets('UNITS_OF_MEASURE')->load($path, function ($reader) {})->get();
            if (!empty($data) && $data->count() > 0) {
                foreach ($data as $key => $value) {
                    $name = $value->name;
                    $symbol = $value->symbol;
                    $count = Unit::where('name', $name)->whereNull('deleted_at')->where('symbol',$symbol)->count();
                    if ($count == 1) {
                        continue;
                    }
                    $insert[] = [
                        'name' => $value->name,
                        'symbol' => $value->symbol,
                        'sort_order' => 1,
                        'is_active' => true,
                    ];
                }
                if (!empty($insert)) {
                    try {
                        DB::table('units')->insert($insert);
                        $all = $this->getAllPaginated(Input::get('perPage'));
                        $message = ["successMessage" => "DATA_UPLOAD_SUCCESS", "units" => $all];
                        return response()->json($message, 200);
                    } catch (QueryException $exception) {
                        $error_code = $exception->errorInfo[1];
                        Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
                        $message = ["errorMessage" =>  $exception];
                        return response()->json($message, 500);
                    }
                } else {
                    $message = ["successMessage" => 'DATA_UPLOAD_SUCCESS'];
                    return response()->json($message, 500);
                }
            } else {
                $message = ["errorMessage" => 'FILE_SPECIFIED_HAS_NO_DATA'];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => 'PLEASE_SELECT_FILE'];
            return response()->json($message, 500);
        }
    }
}
