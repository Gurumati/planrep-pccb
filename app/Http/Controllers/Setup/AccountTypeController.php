<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\AccountType;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class AccountTypeController extends Controller {
    public function index() {
        $accountTypes = AccountType::all();
        return response()->json($accountTypes);
    }

    public function getAllPaginated($perPage) {
        return AccountType::orderby('created_at')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }


    public function store(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, AccountType::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            AccountType::create($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "accountTypes" => $all];
            return response()->json($message, 200);
        }
    }


    public function update(Request $request) {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, AccountType::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            $obj = AccountType::find($request->id);
            $obj->update($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "accountTypes" => $all];
            return response()->json($message, 200);
        }
    }

    public function delete($id) {
        try {
            $accountType = AccountType::find($id);
            $accountType->delete();
            $all = $this->getAllPaginated(Input::get('perPage'));
            $message = ["successMessage" => "SUCCESSFUL_ACCOUNT_TYPE_DELETED", "accountTypes" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $message = ["errorMessage" => "DATA_IN_USE"];
            return response()->json($message, 500);
        }
    }

    private function allTrashed() {
        $all = AccountType::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        AccountType::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "ACCOUNT_TYPE_RESTORED", "trashedAccountTypes" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        AccountType::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "ACCOUNT_TYPE_DELETED_PERMANENTLY", "trashedAccountTypes" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = AccountType::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "ACCOUNT_TYPE_DELETED_PERMANENTLY", "trashedAccountTypes" => $all];
        return response()->json($message, 200);
    }
}
