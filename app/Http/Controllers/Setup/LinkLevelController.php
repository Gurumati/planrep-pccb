<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\LinkLevel;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class LinkLevelController extends Controller
{
    public function index() {
        $all = LinkLevel::orderBy('created_at','asc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return LinkLevel::orderby('created_at','asc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = new LinkLevel();
            $obj->name = $data->name;
            $obj->code = $data->code;
            $obj->created_by = UserServices::getUser()->id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_LINK_LEVEL_CREATED", "linkLevels" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }


    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = LinkLevel::find($data->id);
            $obj->name = $data->name;
            $obj->code = $data->code;
            $obj->updated_by = UserServices::getUser()->id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_LEVEL_LEVEL_UPDATED", "linkLevels" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = LinkLevel::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_LEVEL_LEVEL_DELETED", "linkLevels" => $all];
        return response()->json($message, 200);
    }

    public function truncateData() {
        $run = LinkLevel::truncate();
        if($run){
            $message = ["successMessage" => "LINK_LEVEL_TRUNCATED_SUCCESSFULLY"];
            return response()->json($message, 200);
        } else {
            $message = ["errorMessage" => "LINK_LEVEL_COULD_NOT_BE_TRUNCATED"];
            return response()->json($message, 500);
        }
    }

    private function allTrashed() {
        $all = LinkLevel::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        LinkLevel::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "LEVEL_LEVEL_RESTORED", "trashedLinkLevels" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        LinkLevel::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "LEVEL_LEVEL_DELETED_PERMANENTLY", "trashedLinkLevels" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = LinkLevel::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "LEVEL_LEVEL_DELETED_PERMANENTLY", "trashedLinkLevels" => $all];
        return response()->json($message, 200);
    }
}
