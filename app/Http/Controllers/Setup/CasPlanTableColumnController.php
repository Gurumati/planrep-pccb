<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\CasGroupColumn;
use App\Models\Setup\CasPlanTableColumn;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class CasPlanTableColumnController extends Controller
{
    public function index(){
        $all = CasPlanTableColumn::with('casPlanTableColumns')->with('parent')->get();
        return response()->json($all);
    }

    public function getCasPlanTableColumnsByTableId($id){
        $all = CasPlanTableColumn::where('cas_plan_table_id', $id)->with('parent')->get();
        return response()->json($all);
    }

    public function paginateIndex(Request $request){
        $all = CasPlanTableColumn::with('parent')
                                   ->where('cas_plan_table_id',$request->table_id)
                                   ->paginate($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request){
        try{
            $data = json_decode($request->getContent());
            $cas_plan_table_column = new CasPlanTableColumn();
            $cas_plan_table_column->code = $data->code;
            $cas_plan_table_column->cas_plan_table_id = $data->cas_plan_table_id;
            $cas_plan_table_column->name = $data->name;
            $cas_plan_table_column->is_list = isset($data->is_list)?$data->is_list:null;
            $cas_plan_table_column->list_item = isset($data->list_item)?($data->list_item):null;
            $cas_plan_table_column->cas_plan_table_select_option_id = isset($data->cas_plan_table_select_option_id)?($data->cas_plan_table_select_option_id):null;
            $cas_plan_table_column->type = isset($data->type)?$data->type:null;
            $cas_plan_table_column->sort_order = $data->sort_order;
            $cas_plan_table_column->is_constant = isset($data->is_constant)?$data->is_constant:false;
            $cas_plan_table_column->admin_hierarchy_level_id = isset($data->admin_hierarchy_level_id)?$data->admin_hierarchy_level_id:null;
            $cas_plan_table_column->vertical_total = isset($data->vertical_total)?$data->vertical_total:false;;
            $cas_plan_table_column->is_lowest = isset($data->is_lowest)?$data->is_lowest:null;
            $cas_plan_table_column->created_by = UserServices::getUser()->id;
            //cas plan table column id is parent
            if(!isset($data->cas_plan_table_column_id))
            {
                $data->cas_plan_table_column_id = null;
            }

            //check for multiple entry

            if(isset($data->cas_group_column_id))
            {
                $baseData = array('type'=>$cas_plan_table_column->type,
                                   'is_lowest'=>$cas_plan_table_column->is_lowest,
                                   'created_by'=>$cas_plan_table_column->created_by,
                                   'cas_plan_table_id'=>$cas_plan_table_column->cas_plan_table_id);

                $inputData = $baseData;
                $allDataInputs = array();

                foreach($data->cas_group_column_id as $k=>$v)
                {
                    //get name, code, sort order from group column
                    $cas_group_column_parameters = CasGroupColumn::where('id',$v)->first();
                    $inputData['code'] = $cas_group_column_parameters->code;
                    $inputData['name'] = $cas_group_column_parameters->name;
                    $inputData['sort_order'] = $cas_group_column_parameters->sort_order;
                    $inputData['cas_group_column_id'] = $v;
                    //check for multiple parent
                    if( $data->cas_plan_table_column_id != null)
                    {
                        foreach ($data->cas_plan_table_column_id as $kp=>$vp)
                        {
                            $inputData['cas_plan_table_column_id'] = $vp;
                            array_push($allDataInputs, $inputData);

                        }
                    } else {
                        array_push($allDataInputs, $inputData);
                    }
                }

                CasPlanTableColumn::insert($allDataInputs);
            }else
            {
                $cas_plan_table_column->cas_plan_table_column_id = $data->cas_plan_table_column_id[0];
                $cas_plan_table_column->save();
            }

            $all = CasPlanTableColumn::orderby('created_at')
                   ->with('parent')
                   ->where('cas_plan_table_id',$cas_plan_table_column->cas_plan_table_id)
                   ->paginate($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_COLUMN_CREATED", "casPlanTableColumns" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request){
        $data = json_decode($request->getContent());
        try{
            $cas_plan_table_column = CasPlanTableColumn::find($data->id);
            $cas_plan_table_column->code = $data->code;
            $cas_plan_table_column->cas_plan_table_id = $data->cas_plan_table_id;
            $cas_plan_table_column->name = $data->name;
            $cas_plan_table_column->is_list = isset($data->is_list)?$data->is_list:null;
            $cas_plan_table_column->list_item = isset($data->list_item)?($data->list_item):null;
            $cas_plan_table_column->cas_plan_table_select_option_id = isset($data->cas_plan_table_select_option_id)?($data->cas_plan_table_select_option_id):null;
            $cas_plan_table_column->type = $data->type;
            $cas_plan_table_column->sort_order = $data->sort_order;
            $cas_plan_table_column->is_constant = isset($data->is_constant)?$data->is_constant:false;
            $cas_plan_table_column->admin_hierarchy_level_id = isset($data->admin_hierarchy_level_id)?$data->admin_hierarchy_level_id:null;
            $cas_plan_table_column->vertical_total = isset($data->vertical_total)?$data->vertical_total:false;
            $cas_plan_table_column->cas_plan_table_column_id = isset($data->cas_plan_table_column_id) ? $data->cas_plan_table_column_id:null;
            $cas_plan_table_column->is_lowest = isset($data->is_lowest)?$data->is_lowest:null;
            $cas_plan_table_column->updated_by = UserServices::getUser()->id;
            $cas_plan_table_column->save();
            $all = CasPlanTableColumn::orderby('created_at')
                                      ->with('parent')
                                      ->where('cas_plan_table_id',$cas_plan_table_column->cas_plan_table_id)
                                      ->paginate($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_COLUMN_UPDATED", "casPlanTableColumns" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public  function  updateFormula(Request $request)
    {
        $data = json_decode($request->getContent());

        try{
            $cas_plan_table_column = CasPlanTableColumn::find($data->id);
            $cas_plan_table_column->formula = $data->formula;
            $cas_plan_table_column->updated_by = UserServices::getUser()->id;
            $cas_plan_table_column->save();
            $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_COLUMN_UPDATED"];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function getAllPaginated($perPage,$cas_plan_table_id) {
        return CasPlanTableColumn::with('casPlanTableColumns')
                                   ->with('parent')
                                   ->where('cas_plan_table_id',$cas_plan_table_id)
                                   ->paginate($perPage);
    }

    public function delete($id){
        $cas_plan_table_column = CasPlanTableColumn::with('casPlanTableColumns')->find($id);
        $cas_plan_table_id = $cas_plan_table_column->cas_plan_table_id;
        $cas_plan_table_column->delete();
        $all = $this->getAllPaginated(Input::get('perPage'),$cas_plan_table_id);
        $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_COLUMN_DELETED", "casPlanTableColumns" => $all];
        return response()->json($message, 200);
    }

    public function getTableColumns($cas_plan_table_id)
    {
        $cas_plan_table_column = CasPlanTableColumn::
                        whereNull('cas_plan_table_column_id')
                        ->where('cas_plan_table_id',$cas_plan_table_id)
                        ->orderBy('sort_order')
                        ->get();
        return response()->json($cas_plan_table_column, 200);
    }
}
