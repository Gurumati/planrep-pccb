<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\ResponsiblePerson;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;


class ResponsiblePersonController extends Controller {


    public function byAdminAreaAndSector($adminHierarchyId, $sectionId) {

    $responsiblePersons = DB::table("budget_submission_select_option")
    ->where("parent_id","1")
    ->where("admin_hierarchy_id",$adminHierarchyId)
    ->select("id","name")
    ->get();
     return response()->json(['responsiblePersons'=>$responsiblePersons], 200);
    
    }

    public function paginated($adminHierarchyId,$sectionId) {
        return response()->json(['responsiblePersons' => ResponsiblePerson::paginateByAdminAreaAndSectionTree($adminHierarchyId,$sectionId)], 200);
    }


    public function store(Request $request) {
        $data = json_decode($request->getContent());
        try {
            $obj = new ResponsiblePerson();
            $obj->email = $data->email;
            $obj->password = bcrypt('12345');
            $obj->first_name = $data->first_name;
            $obj->last_name = $data->last_name;
            $obj->admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
            $obj->section_id = $data->section_id;
            $obj->user_name = strtolower($data->first_name . "" . $data->last_name);
            $obj->decision_level_id = (isset($data->decision_level_id)) ? $data->decision_level_id : null;
            $obj->cheque_number = isset($data->cheque_number)? $data->cheque_number:null;
            $obj->is_superuser = (isset($data->is_superuser)) ? $data->is_superuser : false;
            $obj->profile_picture_url = 'no-image.png';
            $obj->title = isset($data->title)?$data->title:null;
            $obj->mobile = isset($data->mobile)?$data->mobile:null;
            $obj->save();
            $message = ["successMessage" => "SUCCESSFUL_RESPONSIBLE_PERSON_CREATED"];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[2];
            $message = ["errorMessage" => "DATABASE_ERROR","errorInfo" => $error_code];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try {
            $obj = ResponsiblePerson::find($data->id);
            $obj->email = $data->email;
            $obj->first_name = $data->first_name;
            $obj->last_name = $data->last_name;
            $obj->cheque_number = isset($data->cheque_number)?$data->cheque_number:null;
            $obj->is_superuser = (isset($data->is_superuser)) ? $data->is_superuser : false;
            $obj->profile_picture_url = 'no-image.png';
            $obj->title = isset($data->title)?$data->title:null;
            $obj->mobile = isset($data->mobile)?$data->mobile:null;
            $obj->save();
            $all = $this->responsiblePersons();
            $message = ["successMessage" => "SUCCESSFUL_RESPONSIBLE_PERSON_UPDATED", "responsiblePersons" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = ResponsiblePerson::find($id);
        $obj->delete();
        $all = $this->responsiblePersons();
        $message = ["successMessage" => "SUCCESSFUL_RESPONSIBLE_PERSON_DELETED", "responsiblePersons" => $all];
        return response()->json($message, 200);
    }
}
