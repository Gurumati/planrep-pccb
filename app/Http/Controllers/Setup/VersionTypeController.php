<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\VersionType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class VersionTypeController extends Controller
{
    public function index()
    {
        $items = VersionType::orderBy('name', 'asc')->get();
        return apiResponse(200, "Success", $items, true, []);
    }

    public function getAllPaginated($perPage)
    {
        return VersionType::orderBy('name', 'asc')->paginate($perPage);
    }

    public function paginated(Request $request)
    {
        $perPage = $request->perPage;
        $items = $this->getAllPaginated($perPage);
        return apiResponse(200, "Success", $items, true, []);
    }


    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, VersionType::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $items = $this->getAllPaginated($request->perPage);
            return apiResponse(500, "Failed", $items, false, $errors->all());
        } else {
            VersionType::create($data);
            $items = $this->getAllPaginated($request->perPage);
            return apiResponse(201, "Success", $items, true, []);
        }
    }


    public function update(Request $request)
    {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, VersionType::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $items = $this->getAllPaginated($request->perPage);
            return apiResponse(500, "Failed", $items, false, $errors->all());
        } else {
            $obj = VersionType::find($request->id);
            $obj->update($data);
            $items = $this->getAllPaginated($request->perPage);
            return apiResponse(201, "Success", $items, true, []);
        }
    }

    public function delete($id)
    {
        $item = VersionType::find($id);
        $item->delete();
        $items = $this->getAllPaginated(Input::get('perPage'));
        return apiResponse(200, "Success", $items, true, []);
    }

    public function show($id)
    {
        $item = VersionType::find($id);
        return apiResponse(200, "Success", $item, true, []);
    }
}
