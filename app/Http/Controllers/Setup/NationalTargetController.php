<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\NationalTarget;
use App\Models\Setup\PriorityArea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Maatwebsite\Excel\Facades\Excel;

class NationalTargetController extends Controller
{
    public function index()
    {
        $all = NationalTarget::with('priority_area')->orderBy('created_at', 'asc')->get();
        $data = ["nationalTargets" => $all];
        return response()->json($data);
    }

    public function getAllPaginated($perPage)
    {
        return NationalTarget::with('priority_area')->orderBy('created_at', 'asc')->paginate($perPage);
    }

    public function paginated(Request $request)
    {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, NationalTarget::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "Form Errors"];
            return response()->json($message, 500);
        } else {
            $query = NationalTarget::with('priority_area')->where('priority_area_id', $request->priority_area_id);
            if($query->count() == 0){
                $last_auto_id = 0;
                $pa = PriorityArea::find($data->priority_area_id);
                $priority_area_number = $pa->number;
                $standard_number = str_pad(($last_auto_id), 2, 0, STR_PAD_LEFT);
            } else {
                $nt = $query->first();
                $last_auto_id = $query->count();
                $priority_area_number = $nt->priority_area->number;
                $standard_number = str_pad(($last_auto_id + 1), 2, 0, STR_PAD_LEFT);
            }
            $obj = new NationalTarget();
            $obj->code = $priority_area_number."".$standard_number;
            $obj->description = $request->description;
            $obj->priority_area_id = $request->priority_area_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        }
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, NationalTarget::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "Form Errors"];
            return response()->json($message, 500);
        } else {
            $obj = NationalTarget::find($request->id);
            $obj->update($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        }
    }

    public function delete($id)
    {
        $obj = NationalTarget::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed()
    {
        $all = NationalTarget::with('priority_area')->orderBy('created_at', 'asc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed()
    {
        $all = $this->allTrashed();
        $message = ["trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function restore($id)
    {
        NationalTarget::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id)
    {
        NationalTarget::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash()
    {
        $trashes = NationalTarget::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function upload()
    {
        $priority_area_id = Input::get('priority_area_id');
        if (Input::hasFile('file')) {
            $path = Input::file('file')->getRealPath();
            $data = Excel::selectSheets('NATIONAL_TARGETS')->load($path, function ($reader) {
            })->get();
            if (!empty($data) && $data->count()) {
                $error = array();
                $error_row = array();
                $row = 2;
                foreach ($data as $key => $value) {
                    $name = $value->description;
                    if (is_null($name)) {
                        array_push($error, $value);
                        array_push($error_row, $row);
                    }
                    $row++;
                }
                if (count($error) > 0) {
                    $all = $this->getAllPaginated(Input::get('perPage'));
                    $feedback = ["errorMessage" => 'DIRTY_DATA', "errorData" => $error, "errorRows" => $error_row, "items" => $all];
                    return response()->json($feedback, 500);
                } else {
                    $count = NationalTarget::with('priority_area')->where('priority_area_id', $priority_area_id)->count();
                    $x = $count + 1;
                    $i = $x;
                    foreach ($data as $key => $value) {
                        $pa = PriorityArea::find($priority_area_id);
                        $priority_area_number = $pa->number;
                        $new_auto_number = $i;
                        $nt_number = str_pad($new_auto_number, 2, 0, STR_PAD_LEFT);
                        $f = new NationalTarget();
                        $f->code = $priority_area_number . "" . $nt_number;
                        $f->description = $value->description;
                        $f->priority_area_id = $priority_area_id;
                        $f->save();
                        $i++;
                    }
                    $all = $this->getAllPaginated(Input::get('perPage'));
                    $message = ["successMessage" => "NATIONAL_TARGETS_UPLOADED_SUCCESSFULLY", "items" => $all];
                    return response()->json($message, 200);
                }
            } else {
                $message = ["errorMessage" => 'FILE_SPECIFIED_HAS_NO_DATA'];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => 'PLEASE_SELECT_A_FILE'];
            return response()->json($message, 500);
        }
    }
}
