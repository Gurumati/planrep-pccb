<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\CasPlanContent;
use Illuminate\Http\Request;

class CasPlanContentController extends Controller
{

    public function index($cas_plan_id){
        $all = CasPlanContent::whereNull('cas_plan_content_id')
               ->where('cas_plan_id',$cas_plan_id)
               ->where('deleted_at',null)
               ->orderBy('sort_order')
               ->get();
        //check if all project out data has been updated
        return response()->json($all);
    }

    public function getCasContentById($id){
        $CasContent = CasPlanContent::where('id',$id)->first();
        $parent_id = $CasContent->cas_plan_content_id;

        if ($parent_id){
            //The table has a parent therefore lets find it
            $CasContent = CasPlanContent::where('id',$parent_id)->first();
            return response()->json($CasContent);
        } else {
            //The table doesn't have a parent
            return response()->json($CasContent);
        }
    }

    public function store(Request $request){
        try {
            $userID = UserServices::getUser()->id;
            $data = json_decode($request->getContent());

            $casPlanContent = new CasPlanContent();
            $casPlanContent->cas_plan_id =$data->cas_plan_id;
            $casPlanContent->description =$data->description;
            $casPlanContent->sort_order =$data->sort_order;
            $casPlanContent->cas_plan_content_id = isset($data->cas_plan_content_id)?$data->cas_plan_content_id:null;
            $casPlanContent->is_lowest = isset($data->is_lowest)?$data->is_lowest:false;
            $casPlanContent->created_by = $userID;
            $casPlanContent->save();
            $all = CasPlanContent::whereNull('cas_plan_content_id')
                ->where('cas_plan_id',$data->cas_plan_id)
                ->with('children')
                ->orderBy('sort_order')->get();
            //Return language success key and all data
            $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_CREATED_CONTENT_CREATED", "casPlanContents" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request){
        try{
            $userID = UserServices::getUser()->id;
            $data = json_decode($request->getContent());
            $casPlanContent = CasPlanContent::find($data->id);
            $casPlanContent->cas_plan_id = $data->cas_plan_id;
            $casPlanContent->description = $data->description;
            $casPlanContent->sort_order =$data->sort_order;
            $casPlanContent->cas_plan_content_id = $data->cas_plan_content_id;
            $casPlanContent->is_lowest =$data->is_lowest;
            $casPlanContent->updated_by = $userID;
            $casPlanContent->save();
            $all = CasPlanContent::whereNull('cas_plan_content_id')->where('cas_plan_id',$data->cas_plan_id)->with('children')->orderBy('sort_order')->get();
            //Return language success key and all data
            $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_CREATED_CONTENT_UPDATED", "casPlanContents" => $all ];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id){
        $userID = UserServices::getUser()->id;
        $casPlanContent = CasPlanContent::find($id);
        $cas_plan_id = $casPlanContent->cas_plan_id;
        $casPlanContent->deleted_by=$userID;
        $casPlanContent->delete();
        $all = CasPlanContent::whereNull('cas_plan_content_id')->where('cas_plan_id',$cas_plan_id)->with('children')->orderBy('sort_order')->get();
        $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_CREATED_CONTENT_DELETED", "casPlanContents" => $all ];
        return response()->json($message, 200);
    }
}
