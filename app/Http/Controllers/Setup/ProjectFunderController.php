<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\ProjectFunder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ProjectFunderController extends Controller {

    public function index() {
        $projectFunder = ProjectFunder::with('funder')->with('project')->where('is_active', true)->get();
        return response()->json($projectFunder);
    }

    public function getAllPaginated($perPage) {
        return ProjectFunder::with('funder')->with('project')->paginate($perPage);
    }

    public function paginateIndex(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        $projectFunder = new ProjectFunder();
        $projectFunder->project_id = $data->project_id;
        $projectFunder->funder_id = $data->funder_id;
        $projectFunder->is_loan = (isset($data->is_loan)) ? $data->is_loan : false;
        $projectFunder->is_active = TRUE;
        $projectFunder->save();
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_PROJECT_FUNDER_CREATED", "projectFunders" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        $projectFunder = ProjectFunder::find($data->id);
        $projectFunder->project_id = $data->project_id;
        $projectFunder->funder_id = $data->funder_id;
        $projectFunder->is_loan = (isset($data->is_loan)) ? $data->is_loan : false;
        $projectFunder->is_active = TRUE;
        $projectFunder->save();
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_PROJECT_FUNDER_UPDATED", "projectFunders" => $all];
        return response()->json($message, 200);
    }

    public function delete($id) {
        $projectFunder = ProjectFunder::find($id);
        $projectFunder->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_PROJECT_FUNDER_DELETED", "projectFunders" => ProjectFunder::with('funder')->with('project')->paginate($this->limit)];
        return response()->json($message, 200);
    }

    public function activate(Request $request) {
        $data = json_decode($request->getContent());
        $projectFunder = ProjectFunder::find($data->id);
        $projectFunder->is_active = $data->is_active;
        $projectFunder->save();
        $all = $this->getAllPaginated($request->perPage);
        if ($projectFunder->is_active) {
            $message = ["activateMessage" => "SUCCESSFUL_PROJECT_FUNDER_ACTIVATED", "projectFunders" => $all];
        } else {
            $message = ["activateMessage" => "SUCCESSFUL_PROJECT_FUNDER_DEACTIVATED", "projectFunders" => $all];
        }
        return response()->json($message, 200);
    }
}
