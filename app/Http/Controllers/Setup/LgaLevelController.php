<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\LgaLevel;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class LgaLevelController extends Controller {
    public function index() {
        $all = LgaLevel::orderBy('created_at','asc')->get();
        return response()->json($all);
    }

    public function fetchAll() {
        $all = LgaLevel::orderBy('created_at','asc')->get();
        return response()->json(["lgaLevels"=>$all],200);
    }

    public function getAllPaginated($perPage) {
        return LgaLevel::orderBy('id')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new LgaLevel();
            $obj->name = $data->name;
            $obj->description = $data->description;
            $obj->created_by = UserServices::getUser()->id;
            $obj->save();
            $all = LgaLevel::orderBy('created_at')->paginate($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_PISC_LEVEL_CREATED", "lgaLevels" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = LgaLevel::find($data->id);
            $obj->name = $data->name;
            $obj->description = $data->description;
            $obj->updated_by = UserServices::getUser()->id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_PISC_LEVEL_UPDATED", "lgaLevels" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = LgaLevel::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_PISC_LEVEL_DELETED", "lgaLevels" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = LgaLevel::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        LgaLevel::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PISC_LEVEL_RESTORED", "trashedLgaLevels" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        LgaLevel::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PISC_LEVEL_DELETED_PERMANENTLY", "trashedLgaLevels" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = LgaLevel::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "PISC_LEVEL_DELETED_PERMANENTLY", "trashedLgaLevels" => $all];
        return response()->json($message, 200);
    }
}
