<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\FundType;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class FundTypeController extends Controller
{
    public function index()
    {
        $all = FundType::orderBy('created_at', 'desc')->get();
        return response()->json($all);
    }

    public function fetchAll()
    {
        $all = FundType::orderBy('created_at', 'desc')->get();
        return response()->json(["fundTypes" => $all], 200);
    }

    public function getAllPaginated($perPage)
    {
        return FundType::orderBy('created_at', 'desc')->paginate($perPage);
    }

    public function paginated(Request $request)
    {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request)
    {
        try {
            $data = json_decode($request->getContent());
            $obj = new FundType();
            $obj->name = $data->name;
            $obj->current_budget_code = $data->current_budget_code;
            $obj->carried_over_budget_code = $data->carried_over_budget_code;
            $obj->save();
            $id = $obj->id;

            try {
                $item['id'] = $id;
                $item['code'] = $data->current_budget_code;
                $item['description'] = $data->name;
                $item['deletedAt'] = null;
                $this->send($item, "FUNDTYPES");
            } catch (\Exception $e) {
                Log::error($e);
            }
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "fundTypes" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request)
    {
        $data = json_decode($request->getContent());
        try {
            $obj = FundType::find($data->id);
            $obj->name = $data->name;
            $obj->current_budget_code = $data->current_budget_code;
            $obj->carried_over_budget_code = $data->carried_over_budget_code;
            $obj->save();
            $id = $data->id;

            try{
                $item['id'] = $id;
                $item['code'] = $data->current_budget_code;
                $item['description'] = $data->name;
                $item['deletedAt'] = null;
                $this->send($item, "FUNDTYPES");
            } catch (\Exception $e) {
                Log::error($e);
            }

            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "fundTypes" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }


    public function delete($id)
    {
        $obj = FundType::find($id);
        $obj->delete();
        $data = FundType::find($id);
        try{
            $item['id'] = $id;
            $item['code'] = $data->current_budget_code;
            $item['description'] = $data->name;
            $item['deletedAt'] = $data->deleted_at;
            $this->send($item, "FUNDTYPES");
        } catch (\Exception $e) {
            Log::error($e);
        }

        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "fundTypes" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed()
    {
        $all = FundType::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed()
    {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id)
    {
        FundType::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedFundTypes" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id)
    {
        FundType::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedFundTypes" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash()
    {
        $trashes = FundType::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedFundTypes" => $all];
        return response()->json($message, 200);
    }
}
