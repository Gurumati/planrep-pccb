<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\PriorityArea;
use App\Models\Setup\Section;
use App\Models\Setup\SectorProblem;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use App\Models\Setup\Version;

class SectorProblemController extends Controller
{
    public function index()
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section_id = UserServices::getUser()->section_id;
        $section = Section::find($section_id);
        $sector_id = $section->sector_id;
        $all = SectorProblem::with('priority_area', 'priority_area.sectors', 'admin_hierarchy')
            ->whereHas('priority_area.sectors', function ($query) use ($sector_id) {
                $query->where('id', $sector_id);
            })
            ->where('is_active', true)
            ->where('admin_hierarchy_id', $admin_hierarchy_id)
            ->get();
        return response()->json($all);
    }

    public function fetchAll()
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section_id = UserServices::getUser()->section_id;
        $section = Section::find($section_id);
        $sector_id = $section->sector_id;
        $all = SectorProblem::with('priority_area', 'priority_area.sectors', 'admin_hierarchy')
            ->whereHas('priority_area.sectors', function ($query) use ($sector_id) {
                $query->where('sector_id', $sector_id);
            })
            ->where('is_active', true)
            ->where('admin_hierarchy_id', $admin_hierarchy_id)
            ->get();
        return response()->json(['sectorProblems' => $all], 200);
    }

    public function getAllPaginated($perPage)
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section_id = UserServices::getUser()->section_id;
        $section = Section::find($section_id);
        $sector_id = $section->sector_id;

        $versionId = Version::getCurrentVersionId('PA');
        $currentVersionItemIds = Version::getVesionItemIds($versionId,'PA');
        $all = SectorProblem::with('priority_area', 'priority_area.sectors', 'admin_hierarchy')
            ->whereHas('priority_area.sectors', function ($query) use ($sector_id) {
                $query->where('sector_id', $sector_id);
            })
            ->whereIn('priority_area_id',$currentVersionItemIds)
            ->where('is_active', true)
            ->where('admin_hierarchy_id', $admin_hierarchy_id)
            ->orderBy('priority_area_id')->paginate($perPage);
        return $all;
    }

    public function paginated(Request $request)
    {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    private function has_duplicates($number, $priority_area_id, $admin_hierarchy_id, $id = null)
    {
        if (is_null($id)) {
            $count = SectorProblem::where('number', $number)
                ->where('admin_hierarchy_id', $admin_hierarchy_id)
                ->where('priority_area_id', $priority_area_id)
                ->whereNull('deleted_at')
                ->count();
        } else {
            $count = SectorProblem::whereNotIn('id', [$id])
                ->where('number', $number)
                ->where('admin_hierarchy_id', $admin_hierarchy_id)
                ->where('priority_area_id', $priority_area_id)
                ->whereNull('deleted_at')
                ->count();
        }
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function store(Request $request)
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $data = json_decode($request->getContent());
        $number = $this->spnumber($data->priority_area_id);
        try {
            if ($this->has_duplicates($number, $data->priority_area_id, $admin_hierarchy_id)) {
                $all = $this->getAllPaginated($request->perPage);
                $message = ["errorMessage" => "SECTOR_PROBLEM_ALREADY_EXISTS", "sectorProblems" => $all];
                return response()->json($message, 400);
            } else {
                $obj = new SectorProblem();
                $obj->description = $data->description;
                $obj->number = $number;
                $obj->priority_area_id = $data->priority_area_id;
                $obj->admin_hierarchy_id = $admin_hierarchy_id;
                $obj->is_active = true;
                $obj->created_by = UserServices::getUser()->id;
                $obj->save();
                $all = $this->getAllPaginated($request->perPage);
                $message = ["successMessage" => "SECTOR_PROBLEM_CREATED_SUCCESSFULLY", "sectorProblems" => $all];
                return response()->json($message, 200);
            }
        } catch (QueryException $exception) {
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request)
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $data = json_decode($request->getContent());
        $number = $this->spnumber($data->priority_area_id);
        try {
            if ($this->has_duplicates($number, $data->priority_area_id, $admin_hierarchy_id, $data->id)) {
                $all = $this->getAllPaginated($request->perPage);
                $message = ["errorMessage" => "SECTOR_PROBLEM_ALREADY_EXISTS", "sectorProblems" => $all];
                return response()->json($message, 200);
            } else {
                $obj = SectorProblem::find($data->id);
                $obj->description = $data->description;
                $obj->number = $data->number;
                $obj->priority_area_id = $data->priority_area_id;
                $obj->updated_by = UserServices::getUser()->id;
                $obj->save();
                $all = $this->getAllPaginated($request->perPage);
                $message = ["successMessage" => "SUCCESSFUL_SECTOR_PROBLEM_UPDATED", "sectorProblems" => $all];
                return response()->json($message, 200);
            }
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id)
    {
        $obj = SectorProblem::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_SECTOR_PROBLEM_DELETED", "sectorProblems" => $all];
        return response()->json($message, 200);
    }

    public function toggleActive(Request $request)
    {
        $data = json_decode($request->getContent());
        $object = SectorProblem::find($data->id);
        $object->is_active = $data->is_active;
        $object->save();
        $all = $this->getAllPaginated($request->perPage);
        if ($data->is_active == false) {
            $feedback = ["action" => "SECTOR_PROBLEM_DEACTIVATED", "alertType" => "warning", "sectorProblems" => $all];
        } else {
            $feedback = ["action" => "SECTOR_PROBLEM_ACTIVATED", "alertType" => "success", "sectorProblems" => $all];
        }
        return response()->json($feedback, 200);
    }

    private function allTrashed()
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section_id = UserServices::getUser()->section_id;
        $section = Section::find($section_id);
        $sector_id = $section->sector_id;
        $all = SectorProblem::with('priority_area', 'priority_area.sectors', 'admin_hierarchy')
            ->whereHas('priority_area.sectors', function ($query) use ($sector_id) {
                $query->where('sector_id', $sector_id);
            })->where('is_active', true)->onlyTrashed()
            ->where('admin_hierarchy_id', $admin_hierarchy_id)
            ->get();
        return $all;
    }

    public function trashed()
    {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id)
    {
        SectorProblem::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "SECTOR_PROBLEM_RESTORED", "trashedSectorProblems" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id)
    {
        SectorProblem::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "SECTOR_PROBLEM_DELETED_PERMANENTLY", "trashedSectorProblems" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash()
    {
        $trashes = SectorProblem::with('priority_area')->orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "SECTOR_PROBLEM_DELETED_PERMANENTLY", "trashedSectorProblems" => $all];
        return response()->json($message, 200);
    }

    public function spnumber($priority_area_id)
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $pa = PriorityArea::find($priority_area_id);
        $panumber = $pa->number;

        $spnumber = SectorProblem::where('priority_area_id', $priority_area_id)
            ->where('admin_hierarchy_id', $admin_hierarchy_id)
            ->orderBy('id', 'desc')
            ->get()->count();
        $n = $panumber . "." . ($spnumber + 1);
        return $n;
        /*return response()->json(['spnumber' => $n], 200);*/
    }

    private function new_spnumber($new_spnumber)
    {
        $digits = $this->countEndingDigits($new_spnumber);
        if ($digits > 0) {
            $base_portion = substr($new_spnumber, 0, -$digits);
            $digits_portion = abs(substr($new_spnumber, -$digits));
        } else {
            $base_portion = $new_spnumber;
            $digits_portion = '';
        }
        $original_digits_count = $this->countDigits($new_spnumber);
        $x = $base_portion . intval($digits_portion + 1);
        return str_pad($x, $original_digits_count, '0', STR_PAD_LEFT);
    }

    private function countDigits($str)
    {
        return preg_match_all("/[0-9]/", $str);
    }

    private function countEndingDigits($string)
    {
        $tailing_number_digits = 0;
        $i = 0;
        $from_end = -1;
        while ($i < strlen($string)) :
            if (is_numeric(substr($string, $from_end - $i, 1))) :
                $tailing_number_digits++;
            else:
                // End our while if we don't find a number anymore
                break;
            endif;
            $i++;
        endwhile;
        return $tailing_number_digits;
    }


}
