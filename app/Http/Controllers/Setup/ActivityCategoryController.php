<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\ActivityCategory;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ActivityCategoryController extends Controller
{

    public function index()
    {
        $activityCategory = ActivityCategory::where('is_active', true)->orderBy('created_at', 'desc')->get();
        return response()->json($activityCategory);
    }

    private function getPaginated($perPage)
    {
        return ActivityCategory::orderBy('is_active', 'desc')->orderBy('sort_order')->paginate($perPage);
    }

    public function paginated(Request $request)
    {
        $all = $this->getPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request)
    {
        $data = array_merge($request->all(), ["created_by" => UserServices::getUser()->id]);
        $validator = Validator::make($data, ActivityCategory::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        try {
            ActivityCategory::create($data);
            $all = ActivityCategory::orderby('created_at')->paginate($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESSFUL", "activityCategories" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            Log::error($exception);
            $message = ["errorMessage" => "ERROR_CREATING"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, ActivityCategory::rules($request->id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        try {
            $activityCategory = ActivityCategory::find($request->id);
            $activityCategory->update($data);
            $all = $this->getPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESSFUL", "activityCategories" => $all];
            return response()->json($message, 200);
        } catch (\Exception $exception) {
            Log::error($exception);
            $message = ["errorMessage" => "ERROR_UPDATING"];
            return response()->json($message, 500);
        }
    }

    public function delete($id)
    {
        try {
            $activityCategory = ActivityCategory::find($id);
            $activityCategory->delete();
            $all = $this->getPaginated(Input::get('perPage'));
            $message = ["successMessage" => "DELETE_SUCCESSFUL", "activityCategories" => $all];
            return response()->json($message, 200);
        } catch (QueryException $e) {
            Log::error($e);
            $message = ["errorMessage" => "ERROR_DELETING"];
            return response()->json($message, 500);
        }
    }


    public function toggleActive(Request $request)
    {
        try {
            $object = ActivityCategory::find($request->id);
            $object->is_active = $request->is_active;
            $object->save();
            $all = $this->getPaginated($request->perPage);
            if ($request->is_active == false) {
                $feedback = ["action" => "DEACTIVATE_SUCCESSFUL", "alertClass" => "alert-warning", "activityCategories" => $all];
            } else {
                $feedback = ["action" => "ACTIVATE_SUCCESSFUL", "alertClass" => "alert-success", "activityCategories" => $all];
            }
            return response()->json($feedback, 200);
        } catch (\Exception $e) {
            Log::error($e);
            $message = ["errorMessage" => "ERROR_TOGGLE_ACTIVE"];
            return response()->json($message, 500);
        }

    }

    public function fetchAll()
    {
        $all = ActivityCategory::where('is_active', true)->orderBy('created_at', 'desc')->get();
        return response()->json(['items' => $all]);
    }
}
