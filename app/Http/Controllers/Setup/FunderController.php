<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\Funder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class FunderController extends Controller {


    public function index() {
        $all = Funder::where('is_active', true)->orderby('id')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return Funder::orderby('id')->paginate($perPage);
    }

    public function paginateIndex(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        $funder = new Funder();
        $funder->name = $data->name;
        $funder->code = $data->code;
        $funder->is_local = (isset($data->is_local)) ? $data->is_local : false;
        $funder->is_active = true;
        $funder->sort_order = $data->sort_order;
        $funder->save();
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_FUNDER_CREATED", "funders" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        $funder = Funder::find($data->id);
        $funder->name = $data->name;
        $funder->code = $data->code;
        $funder->is_local = (isset($data->is_local)) ? $data->is_local : false;
        $funder->is_active = TRUE;
        $funder->sort_order = $data->sort_order;
        $funder->save();
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_FUNDER_UPDATED", "funders" => $all];
        return response()->json($message, 200);
    }


    public function delete($id) {
        $funder = Funder::find($id);
        $funder->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_FUNDER_DELETED", "funders" => $all];
        return response()->json($message, 200);
    }

    /** activate/ deactivate funder */
    public function activate(Request $request) {
        $data = json_decode($request->getContent());
        $funder = Funder::find($data->id);
        $funder->is_active = $data->is_active;
        $funder->save();
        $all = $this->getAllPaginated($request->perPage);
        if ($funder->is_active) {
            $message = ["activateMessage" => "SUCCESSFUL_FUNDER_ACTIVATED", "funders" => $all];
        } else {
            $message = ["activateMessage" => "SUCCESSFUL_FUNDER_DEACTIVATED", "funders" => $all];
        }
        return response()->json($message, 200);
    }
}
