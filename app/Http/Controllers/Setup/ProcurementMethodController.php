<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Execution\ActivityProjectOutput;
use App\Models\Setup\ProcurementMethod;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ProcurementMethodController extends Controller
{
    public function index()
    {
        $items = ProcurementMethod::orderBy('code', 'asc')->get();
        return response()->json(['items' => $items], 200);
    }

    public function getAllPaginated($perPage)
    {
        return ProcurementMethod::orderBy('code', 'asc')->paginate($perPage);
    }

    public function paginated(Request $request)
    {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json(['items' => $all], 200);
    }


    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, ProcurementMethod::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 400);
        } else {
            ProcurementMethod::create($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        }
    }


    public function update(Request $request)
    {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, ProcurementMethod::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            $obj = ProcurementMethod::find($request->id);
            $obj->update($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        }
    }

    public function delete($id)
    {
        $item = ProcurementMethod::find($id);
        $item->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }
}
