<?php

namespace App\Http\Controllers\Setup;

use App\Models\Setup\MetadataSource;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class MetadataSourcesController extends Controller {
    public function index() {
        $data = MetadataSource::orderBy("source_name")->get();
        return customApiResponse($data, "Success", 200, "");
    }

    private function paginate() {
        if (Input::get('perPage') && Input::get('perPage') > 0) {
            $perPage = Input::get('perPage');
        } else {
            $perPage = 10;
        }
        return MetadataSource::orderBy("source_name", "asc")->paginate($perPage);
    }

    public function paged() {
        $items = $this->paginate();
        return customApiResponse($items, "Success", 200);
    }

    public function store(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, MetadataSource::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            return customApiResponse("", "Failed", 400, $errors->all());
        } else {
            MetadataSource::create($data);
            $items = $this->paginate();
            return customApiResponse($items, "Metadata Source Created Successfully!", 201);
        }
    }

    public function update(Request $request) {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, MetadataSource::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            return customApiResponse("", "Failed", 400, $errors->all());
        } else {
            $obj = MetadataSource::find($request->id);
            $obj->update($data);
            $items = $this->paginate();
            return customApiResponse($items, "Metadata source updated", 201);
        }
    }

    public function delete($id) {
        $item = MetadataSource::find($id);
        if ($item->types()->count() > 0) {
            return customApiResponse("", "Metadata source already in use!", 400);
        } else {
            $item->delete();
            $items = $this->paginate();
            return customApiResponse($items, "Metadata source deleted Successfully", 200);
        }
    }
}
