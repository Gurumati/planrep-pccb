<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\SectorExpenditureSubCentreGfsCode;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SectorExpenditureSubCentreGfsCodeController extends Controller {
    public function gfs_codes($id) {
        $all = SectorExpenditureSubCentreGfsCode::where('bdc_sector_expenditure_sub_centre_id', $id)->with('gfs_code')->get();
        $message = ["expenditureSubCentreGfsCodes" => $all];
        return response()->json($message);
    }

    public function add_gfs_code(Request $request) {
        $data = json_decode($request->getContent());
        $bdc_sector_expenditure_sub_centre_id = $data->bdc_sector_expenditure_sub_centre_id;
        $gfs_code_id = $data->gfs_code_id;
        try {
            $obj = new SectorExpenditureSubCentreGfsCode();
            $obj->gfs_code_id = $gfs_code_id;
            $obj->bdc_sector_expenditure_sub_centre_id = $bdc_sector_expenditure_sub_centre_id;
            $obj->save();
            $all = SectorExpenditureSubCentreGfsCode::where('bdc_sector_expenditure_sub_centre_id', $bdc_sector_expenditure_sub_centre_id)->with('gfs_code')->get();
            $message = ["expenditureSubCentreGfsCodes" => $all, "successMessage" => "GFS_CODE_ADDED"];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete_gfs_code($expenditure_centre_gfs_code_id, $bdc_sector_expenditure_sub_centre_id) {
        $object = SectorExpenditureSubCentreGfsCode::find($expenditure_centre_gfs_code_id);
        $object->delete();
        $all = SectorExpenditureSubCentreGfsCode::where('bdc_sector_expenditure_sub_centre_id', $bdc_sector_expenditure_sub_centre_id)->with('gfs_code')->get();
        $message = ["expenditureSubCentreGfsCodes" => $all, "successMessage" => "GFS_CODE_REMOVED"];
        return response()->json($message);
    }
}
