<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\BodIntervention;
use App\Models\Setup\BodVersion;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class BodVersionController extends Controller {

    public function index() {
        $bodVersions = BodVersion::with('bod_list', 'start_financial_year', 'end_financial_year', 'reference_document')->where('active', true)->get();
        return response()->json($bodVersions);
    }

    public function getAllPaginated($perPage) {
        return BodVersion::with('bod_list', 'start_financial_year', 'end_financial_year', 'reference_document')->orderBy('created_at', 'desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $bodVersion = new BodVersion();
            $bodVersion->start_financial_year_id = $data->start_financial_year_id;
            $bodVersion->end_financial_year_id = $data->end_financial_year_id;
            $bodVersion->reference_doc = $data->reference_doc;
            $bodVersion->bod_value = $data->bod_value;
            $bodVersion->bod_list_id = $data->bod_list_id;
            $bodVersion->active = true;
            $bodVersion->created_by = UserServices::getUser()->id;
            $bodVersion->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "BOD_VERSION_CREATED_SUCCESSFULLY", "bodVersions" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try {
            $bodVersion = BodVersion::find($data->id);
            $bodVersion->start_financial_year_id = $data->start_financial_year_id;
            $bodVersion->end_financial_year_id = $data->end_financial_year_id;
            $bodVersion->reference_doc = $data->reference_doc;
            $bodVersion->bod_value = $data->bod_value;
            $bodVersion->bod_list_id = $data->bod_list_id;
            $bodVersion->created_by = UserServices::getUser()->id;
            $bodVersion->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "BOD_VERSION_UPDATED_SUCCESSFULLY", "bodVersions" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function toggleBodVersion(Request $request) {
        $data = json_decode($request->getContent());
        $object = BodVersion::find($data->id);
        $object->active = $data->active;
        $object->save();
        $all = $this->getAllPaginated($request->perPage);
        if ($data->active == false) {
            $feedback = ["action" => "BOD_VERSION_DEACTIVATED", "alertType" => "warning", "bodVersions" => $all];
        } else {
            $feedback = ["action" => "BOD_VERSION_ACTIVATED", "alertType" => "success", "bodVersions" => $all];
        }
        return response()->json($feedback, 200);
    }

    public function delete($id) {
        $obj = BodVersion::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "BOD_VERSION_DELETED_SUCCESSFULLY", "bodVersions" => $all];
        return response()->json($message, 200);
    }


    private function allTrashed() {
        $all = BodVersion::with('bod_list', 'start_financial_year', 'end_financial_year')->orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        BodVersion::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "BOD_VERSION_RESTORED", "trashedBodVersions" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        BodVersion::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "BOD_VERSIONS_DELETED_PERMANENTLY", "trashedBodVersions" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = BodVersion::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "BOD_VERSION_DELETED_PERMANENTLY", "trashedBodVersions" => $all];
        return response()->json($message, 200);
    }

    public function bodInterventions(Request $request) {
        $bod_version_id = $request->bod_version_id;
        $all = BodIntervention::with('intervention', 'fund_source', 'bod_list')->where('bod_version_id', $bod_version_id)->get();
        $data = ["bodInterventions" => $all];
        return response()->json($data, 200);
    }
}
