<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\CasGroupType;
use Illuminate\Http\Request;


class CasGroupTypeController extends Controller
{
    public function index(){
        $all = CasGroupType::all();
        return response()->json($all);

    }

    public function paginateIndex(Request $request){
        $all = CasGroupType::paginate($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request){
        try {
            $userID = UserServices::getUser()->id;
            $data = json_decode($request->getContent());
            $casGroupType = new CasGroupType();
            $casGroupType->name =$data->name;
            $casGroupType->created_by = $userID;
            $casGroupType->save();
            //Return language success key and all data
            $message = ["successMessage" => "SUCCESSFUL_CAS_GROUP_TYPE_CREATED", "casGroupTypes" => CasGroupType::paginate($request->perPage)];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request){
        try{
            $userID = UserServices::getUser()->id;
            $data = json_decode($request->getContent());
            $casGroupType = CasGroupType::find($data->id);
            $casGroupType->name = $data->name;
            $casGroupType->update_by = $userID;
            $casGroupType->save();
            //Return language success key and all data
            $message = ["successMessage" => "SUCCESSFUL_CAS_GROUP_TYPE_UPDATED", "casGroupTypes" => CasGroupType::paginate($request->perPage)];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete(Request $request, $id){
        $userID = UserServices::getUser()->id;
        $casGroupType = CasGroupType::find($id);
        $casGroupType->deleted_by=$userID;
        $casGroupType->delete();
        $message = ["successMessage" => "SUCCESSFULLY_CAS_GROUP_TYPE_DELETED", "casGroupTypes" => CasGroupType::paginate($request->perPage)];
        return response()->json($message, 200);
    }
}
