<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\UserServices;
use App\Models\Setup\AdminHierarchyLevel;
use App\Models\Setup\SectionLevel;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

/**
 * @resource AdminHierarchyLevel
 *
 * AdminHierarchyLevelController: This controller manages admin hierarchy levels e.g National,Region, District etc.
 * The highest level in the hierarchy start with hierarchy_position 1
*/
class AdminHierarchyLevelController extends Controller {

    public function getAllPaginated($perPage) {
        return AdminHierarchyLevel::orderBy('sort_order')->paginate($perPage);
    }

    /**
     * index()
     *
     * This method return paginated admin hierarchy levels by calling getAllPaginated() method
     * @response {
     *  data[]
     * }
    */
    public function index(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    /**
     * admin_hierarchies()
     */
    public function admin_hierarchies() {

        
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section_id = UserServices::getUser()->section_id;
        $section_name = DB::table('sections')->where('id',$section_id)->select('name')->first();

        $data_array = array();

        $admin_position = DB::table('admin_hierarchies as a')
            ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
            ->where('a.id', $admin_hierarchy_id)
            ->select('l.hierarchy_position')
            ->first();

        //regional level
        if($admin_position->hierarchy_position == 1)
        {
            $admin_hierarchies = DB::table('admin_hierarchies as a')
            ->join('admin_hierarchies as a1', 'a.id', 'a1.parent_id')
            ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
            ->where('a.parent_id', $admin_hierarchy_id)
            ->whereNotNull('a.parent_id')
            ->select('a1.id', 'a1.name', 'a1.code', 'a1.admin_hierarchy_level_id', 'a1.parent_id', 'l.hierarchy_position')
            ->distinct()
            ->orderBy('a1.name','ASC')
            ->get();
        }elseif($admin_position->hierarchy_position == 2){
            $admin_hierarchies = DB::table('admin_hierarchies as a')
            ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
            ->where('a.id', $admin_hierarchy_id)
            ->whereNotNull('a.parent_id')
            ->select('a.id', 'a.name', 'a.code', 'a.admin_hierarchy_level_id', 'a.parent_id', 'l.hierarchy_position')
            ->distinct()
            ->orderBy('a.name','ASC')
            ->get();
        }else {
            //cost center and national
            $admin_hierarchies = DB::table('admin_hierarchies as a')
            ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
            ->where('a.parent_id', $admin_hierarchy_id)
            ->orWhere('a.id', $admin_hierarchy_id)
            ->whereNotNull('a.parent_id')
            ->select('a.id', 'a.name', 'a.code', 'a.admin_hierarchy_level_id', 'a.parent_id', 'l.hierarchy_position')
            ->distinct()
            ->orderBy('a.name','ASC')
            ->get();
        }

        foreach ($admin_hierarchies as $key => $value) {
            if ($value->hierarchy_position == 2) {
                array_push($data_array, $value);
            } elseif ($value->hierarchy_position == 3  || $value->hierarchy_position == 1){
                //get lga's
                $admin_hierarchy = DB::table('admin_hierarchies as a')
                    ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
                    ->where('a.parent_id', $value->id)
                    ->select('a.id', 'a.name', 'a.code', 'a.admin_hierarchy_level_id', 'a.parent_id')
                    ->orderBy('a.name','ASC')
                    ->get();
                foreach ($admin_hierarchy as $key2 => $value2) {
                    array_push($data_array, $value2);
                }
            }
        }

        foreach ($data_array as $item) {
            $item->section_id = $section_id;
            $item->section_name = $section_name->name;
        }

        LOG::INFO($data_array);
        return response()->json($data_array, 200);
    }

    public function region_by_levels(){
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $data_array = array();
        $admin_hierarchies = DB::table('admin_hierarchies as a')
            ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
            ->where('l.hierarchy_position','=',2)
            ->where(function ($query) use($admin_hierarchy_id){
                $query->where('a.parent_id', $admin_hierarchy_id)
                    ->orWhere('a.id', $admin_hierarchy_id);
            })
            ->select('a.id', 'a.name', 'a.code', 'a.admin_hierarchy_level_id', 'a.parent_id', 'l.hierarchy_position')
            ->distinct()
            ->get();
        foreach ($admin_hierarchies as $key => $value) {
                array_push($data_array, $value);
        }
        return response()->json($data_array, 200);
    }

    /**
     * all()
     * This method return all admin hierarchy levels
    */
    public function all() {
        $adminHierarchyLevels = AdminHierarchyLevel::orderBy('id','asc')->get();
        return response()->json($adminHierarchyLevels);
    }

    /**
     * getByUser()
     *
     * This method return all user admin hierarchy levels i.e user hierarchy level + child hierarchy levels
     * @response {
     *  data[]
     * }
     */
    public function getByUser() {
        $flatten = new Flatten();

        $hierarchies = $flatten->getUserHierarchyTree();
        $levelIds = $flatten->flattenAdminHierarchyWithChildGelLevels($hierarchies);
         $adminHierarchyLevels = AdminHierarchyLevel::whereIn('id', $levelIds)->orderBy('sort_order')->get();

        return response()->json($adminHierarchyLevels);
    }

    public function getUserLevelAndChildren(Request $request){
        $adminLevels = AdminHierarchyLevel::getUserLevelAndChildren();
        $sectionLevels = SectionLevel::getUserSectionLevelAndChildren();
        $data =['adminLevels'=>$adminLevels,'sectionLevels'=>$sectionLevels];
        return response()->json($data);
    }

    /**
     * store()
     * This method create new admin hierarchy level.
    */
    public function store(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $adminHierarchy = new AdminHierarchyLevel();
            $adminHierarchy->name = $data->name;
            $adminHierarchy->hierarchy_position = $data->hierarchy_position;
            $adminHierarchy->sort_order = $data->sort_order;
            $adminHierarchy->created_by = UserServices::getUser()->id;
            $adminHierarchy->is_active = True;
            $adminHierarchy->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_ADMIN_HIERARCHY_LEVEL_CREATED", "adminHierarchyLevels" => $all];
            return response()->json($message, 200);

        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }
    /**
     * update()
     * This method update existing admin hierarchy level.
     */
    public function update(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $adminHierarchy = AdminHierarchyLevel::find($data->id);
            $adminHierarchy->name = $data->name;
            $adminHierarchy->hierarchy_position = $data->hierarchy_position;
            $adminHierarchy->sort_order = $data->sort_order;
            $adminHierarchy->updated_by = UserServices::getUser()->id;
            $adminHierarchy->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_ADMIN_HIERARCHY_LEVEL_CREATED", "adminHierarchyLevels" => $all];
            return response()->json($message, 200);

        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    /**
     * delete()
     * This method delete existing admin hierarchy level.
     */
    public function delete($id) {
        $adminHierarchy = AdminHierarchyLevel::find($id);
        $adminHierarchy->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_ADMIN_HIERARCHY_LEVEL_DELETED", "adminHierarchyLevels" => $all];
        return response()->json($message, 200);
    }

    /**
     * toggleAdminHierarchyLevel()
     * This method toggle hierarchy level active and inactive.
     */
    public function toggleAdminHierarchyLevel(Request $request) {
        $data = json_decode($request->getContent());
        $obj = AdminHierarchyLevel::find($data->id);
        $obj->is_active = $data->is_active;
        $obj->save();
        $all = $this->getAllPaginated($request->perPage);
        if ($data->is_active == false) {
            //Return language success key and all data
            $feedback = ["action" => "ADMIN_HIERARCHY_LEVEL_DEACTIVATED", "alertType" => "warning", "adminHierarchyLevels" => $all];
        } else {
            //Return language success key and all data
            $feedback = ["action" => "ADMIN_HIERARCHY_LEVEL_ACTIVATED", "alertType" => "success", "adminHierarchyLevels" => $all];
        }
        return response()->json($feedback);
    }

    private function allTrashed() {
        $all = AdminHierarchyLevel::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    /**
     * trashed()
     * This method return all trashed admin hierarchy levels by calling allTrashed() method
     */
    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    /**
     * restore()
     * This method restore trashed admin hierarchy level
     */
    public function restore($id) {
        AdminHierarchyLevel::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "ADMIN_HIERARCHY_LEVEL_RESTORED", "trashedAdminHierarchyLevels" => $all];
        return response()->json($message, 200);
    }

    /**
     * permanentDelete()
     * This method permanently delete admin hierarchy level
     */
    public function permanentDelete($id) {
        AdminHierarchyLevel::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "ADMIN_HIERARCHY_LEVEL_DELETED_PERMANENTLY", "trashedAdminHierarchyLevels" => $all];
        return response()->json($message, 200);
    }

    /**
     * emptyTrash()
     * This method delete trashed admin hierarchy levels
     */
    public function emptyTrash() {
        $trashes = AdminHierarchyLevel::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "ADMIN_HIERARCHY_LEVEL_DELETED_PERMANENTLY", "trashedAdminHierarchyLevels" => $all];
        return response()->json($message, 200);
    }

    public function fetchAll() {
        $adminHierarchyLevels = AdminHierarchyLevel::orderBy('id','asc')->get();
        return response()->json(["ahLevels"=>$adminHierarchyLevels],200);
    }
}
