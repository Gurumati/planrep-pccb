<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\TaskNature;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class TaskNatureController extends Controller {

    public function index() {
        $all = TaskNature::all();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return TaskNature::orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new TaskNature();
            $obj->name = $data->name;
            $obj->is_active = isset($data->is_active)?$data->is_active: true;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_TASK_NATURE_CREATED", "taskNatures" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = TaskNature::find($data->id);
            $obj->name = $data->name;
            $obj->is_active = isset($data->is_active)?$data->is_active: true;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_TASK_NATURE_UPDATED", "taskNatures" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }



    public function delete($id) {
        $obj = TaskNature::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_TASK_NATURE_DELETED", "taskNatures" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = TaskNature::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        TaskNature::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "TASK_NATURE_RESTORED", "trashedTaskNatures" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        TaskNature::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "TASK_NATURE_DELETED_PERMANENTLY", "trashedTaskNatures" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = TaskNature::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "TASK_NATURE_DELETED_PERMANENTLY", "trashedTaskNatures" => $all];
        return response()->json($message, 200);
    }
}
