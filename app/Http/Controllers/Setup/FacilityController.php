<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\UserServices;
use App\Models\Planning\ActivityFacility;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\Facility;
use App\Models\Setup\GeoLocation;
use App\Models\Setup\FacilityBankAccount;
use App\Models\Setup\FacilityCustomDetailMapping;
use App\Models\Setup\FacilityCustomDetailValue;
use App\Models\Setup\FacilityType;
use App\Models\Setup\Section;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class FacilityController extends Controller
{

    private $sectionIds;
    private $adminHierarchyIds;
    private $adminHierarchies = [];

    public function search(Request $request)
    {
        $searchText = $request->searchText;
        $onlyActive = $request->onlyActive;
        $perPage = $request->perPage;
        $all = null;
        if ($onlyActive === true) {
            $all = Facility::with('facility_type', 'admin_hierarchy', 'bank_accounts', 'physical_state', 'star_rating', 'ownership', 'facility_custom_details')
                ->where('name', 'ILIKE', '%' . $searchText . '%')
                ->orWhere('facility_code', 'ILIKE', '%' . $searchText . '%')
                ->orWhere('description', 'ILIKE', '%' . $searchText . '%')
                ->where('is_active', true)
                ->paginate($perPage);
        } else {
            $all = Facility::with('facility_type', 'admin_hierarchy', 'bank_accounts', 'physical_state', 'star_rating', 'ownership', 'facility_custom_details')
                ->where('name', 'ILIKE', '%' . $searchText . '%')
                ->orWhere('facility_code', 'ILIKE', '%' . $searchText . '%')
                ->orWhere('description', 'ILIKE', '%' . $searchText . '%')
                ->paginate($perPage);
        }

        return response()->json(["facilities" => $all], 200);
    }

    public function index()
    {

        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $this->adminHierarchyLevel($admin_hierarchy_id);
        $all = Facility::with('facility_type', 'bank_accounts', 'admin_hierarchy', 'physical_state', 'star_rating', 'ownership', 'facility_custom_details','geographical_locations')->where('is_active', true)->orderBy("created_at", 'asc')
            ->whereIn('admin_hierarchy_id', $this->adminHierarchies)->get();
        return response()->json($all);
    }

    public function fetchAll()
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $this->adminHierarchyLevel($admin_hierarchy_id);
        $all = Facility::with('facility_type', 'bank_accounts', 'admin_hierarchy', 'physical_state', 'star_rating', 'ownership', 'facility_custom_details','geographical_locations')->where('is_active', true)->orderBy("created_at", 'asc')
            ->whereIn('admin_hierarchy_id', $this->adminHierarchies)->get();
        $data = ['facilities' => $all];
        return response()->json($data, 200);
    }

    public function getAllPaginated($perPage)
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $this->adminHierarchyLevel($admin_hierarchy_id);
        $fac =Facility::with(
            'facility_type',
            'bank_accounts',
            'admin_hierarchy',
            'physical_state',
            'star_rating',
            'ownership',
            'facility_custom_details',
            'geographical_locations'
        )
            ->where('is_active', true)
            ->orderBy("created_at", 'asc')
            ->whereIn('admin_hierarchy_id', $this->adminHierarchies)
            ->paginate($perPage);
        return $fac;
    }

    public function facilityHasFacility(Request $request)
    {
        $hasBudget = Facility::hasBudget($request->facilityId);
        return response()->json(['yesNo' => $hasBudget], 200);
    }

    public function paginated(Request $request)
    {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function sections()
    {
        return response()->json(Section::all());
    }

    public function facility_types()
    {
        return response()->json(FacilityType::all());
    }

    public function admin_hierarchies()
    {
        return response()->json(AdminHierarchy::all());
    }

    public function store(Request $request)
    {
        $data = json_decode($request->getContent());
        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $data->facility_code)) {
            $message = ["errorMessage" => "Service Provider Code Can not contain special characters"];
            return response()->json($message, 400);
        } else {
            if (strlen($data->facility_code) == 8) {

                if ($data->facility_code == '00000000') {
                    $object = new Facility();
                    $object->name = $data->name;
                    $object->facility_ownership_id = (isset($data->facility_ownership_id)) ? $data->facility_ownership_id : null;
                    $object->facility_code = (isset($data->facility_code)) ? $data->facility_code : null;;
                    $object->description = (isset($data->name)) ? $data->name : null;
                    $object->number_of_villages_served = (isset($data->number_of_villages_served)) ? $data->number_of_villages_served : 0;
                    $object->facility_type_id = $data->facility_type_id;
                    $object->admin_hierarchy_id = $data->admin_hierarchy_id;
                    $object->created_by = UserServices::getUser()->id;
                    $object->is_active = true;
                    $object->geo_location_id = $data->geo_location_id;
                    $object->save();
                    $facility_id = $object->id;

                    if (isset($data->account_number) || isset($data->account_name) || isset($data->bank)) {
                        $ac = new FacilityBankAccount();
                        $ac->account_number = $data->account_number;
                        $ac->account_name = $data->account_name;
                        $ac->bank = $data->bank;
                        $ac->facility_id = $facility_id;
                        $ac->is_primary = true;
                        $ac->save();
                    }

                    $facility = Facility::with("facility_type", "admin_hierarchy", "bank_accounts","geolocations")->where('id', $facility_id)->first();
                    $this->segmentData($facility, true);
                    $message = ["successMessage" => "SUCCESSFUL_SERVICE_PROVIDER_CREATED", "facility" => $facility];
                    return response()->json($message, 200);
                } else {
                    $count = Facility::where("facility_code", $data->facility_code)->count();
                    if ($count < 1) {
                        $object = new Facility();
                        $object->name = $data->name;
                        $object->facility_code = (isset($data->facility_code)) ? $data->facility_code : null;;
                        $object->description = (isset($data->name)) ? $data->name : null;
                        $object->facility_type_id = $data->facility_type_id;
                        $object->admin_hierarchy_id = $data->admin_hierarchy_id;
                        $object->facility_ownership_id = (isset($data->facility_ownership_id)) ? $data->facility_ownership_id : null;
                        $object->created_by = UserServices::getUser()->id;
                        $object->is_active = true;
                        $object->geo_location_id = $data->geo_location_id;
                        $object->save();
                        $facility_id = $object->id;

                        if (isset($data->account_number) || isset($data->account_name) || isset($data->bank)) {
                            $ac = new FacilityBankAccount();
                            $ac->account_number = $data->account_number;
                            $ac->account_name = $data->account_name;
                            $ac->bank = $data->bank;
                            $ac->facility_id = $facility_id;
                            $ac->is_primary = true;
                            $ac->save();
                        }

                        $facility = Facility::with("facility_type", "admin_hierarchy", "bank_accounts","geolocations")->where('id', $facility_id)->first();
                        $this->segmentData($facility, true);
                        $message = ["successMessage" => "SUCCESSFUL_SERVICE_PROVIDER_CREATED", "facility" => $facility];
                        return response()->json($message, 200);
                    } else {
                        $message = ["errorMessage" => "SERVICE_PROVIDER_CODE_EXISTS"];
                        return response()->json($message, 500);
                    }
                }
            } else {
                $message = ["errorMessage" => "INVALID_FACILITY_CODE_LENGTH"];
                return response()->json($message, 500);
            }
        }

    }

    public function update(Request $request)
    {
        $data = json_decode($request->getContent());

        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $data->facility_code)) {
            $message = ["errorMessage" => "INVALID_FACILITY_CODE_LENGTH"];
            return response()->json($message, 500);
        } else {
            $hasBudget = Facility::hasBudget($data->id);
            $hasCeiling = Facility::hasCeiling($data->id);

            if (strlen($data->facility_code) == 8) {
                if ($data->facility_code) {
                    try {
                        $object = Facility::find($data->id);
                        $hasBudget = Facility::hasBudget($data->id);
                        $hasCeiling = Facility::hasCeiling($data->id);
                        $object->name = $data->name;
                        $object->geo_location_id = $data->geo_location_id;
                        $object->facility_code = (isset($data->facility_code)) ? $data->facility_code : null;
                        $object->description = (isset($data->description)) ? $data->description : null;
                        if ($hasCeiling == false && $hasBudget == false) {
                            $object->facility_type_id = $data->facility_type_id;
                        }
                        $object->admin_hierarchy_id = $data->admin_hierarchy_id;
                        $object->phone_number = (isset($data->phone_number)) ? $data->phone_number : null;
                        $object->email = (isset($data->email)) ? $data->email : null;
                        $object->postal_address = (isset($data->postal_address)) ? $data->postal_address : null;
                        $object->updated_by = UserServices::getUser()->id;
                        $object->updated_at = date('Y-m-d H:i:s');
                        $object->facility_ownership_id = $data->facility_ownership_id;
                        $object->nearest_similar_facility_distance = (isset($data->nearest_similar_facility_distance)) ? $data->nearest_similar_facility_distance : 0;
                        $object->operation_year = (isset($data->operation_year)) ? $data->operation_year : null;
                        $object->save();

                        $facility = Facility::with("facility_type", "admin_hierarchy", "bank_accounts","geolocations")->where('id', $object->id)->first();
                        $this->segmentData($facility, true);

                        $message = ["successMessage" => "SUCCESSFUL_SERVICE_PROVIDER_UPDATED", "facility" => $facility];
                        return response()->json($message, 200);
                    } catch (QueryException $exception) {
                        $message = ["errorMessage" => "Form Error"];
                        return response()->json($message, 500);
                    }
                } else {
                    $validator = Validator::make($request->all(), Facility::rules($request->id));
                    if ($validator->fails()) {
                        $errors = $validator->errors();
                        $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
                        return response()->json($message, 400);
                    }
                    try {
                        $object = Facility::find($request->id);
                        $object->name = $data->name;
                        $object->geo_location_id = $data->geo_location_id;
                        $object->facility_code = (isset($data->facility_code)) ? $data->facility_code : null;
                        if ($hasCeiling == false && $hasBudget == false) {
                            $object->facility_type_id = $data->facility_type_id;
                        }
                        $object->admin_hierarchy_id = $data->admin_hierarchy_id;
                        $object->phone_number = (isset($data->phone_number)) ? $data->phone_number : null;
                        $object->email = (isset($data->email)) ? $data->email : null;
                        $object->postal_address = (isset($data->postal_address)) ? $data->postal_address : null;
                        $object->updated_by = UserServices::getUser()->id;
                        $object->updated_at = date('Y-m-d H:i:s');
                        $object->facility_ownership_id = $data->facility_ownership_id;
                        $object->nearest_similar_facility_distance = (isset($data->nearest_similar_facility_distance)) ? $data->nearest_similar_facility_distance : 0;
                        $object->operation_year = (isset($data->operation_year)) ? $data->operation_year : null;
                        $object->save();

                        $facility = Facility::with("facility_type", "admin_hierarchy", "bank_accounts")->where('id', $object->id)->first();
                        $this->segmentData($facility, true);
                        $message = ["successMessage" => "SUCCESSFUL_SERVICE_PROVIDER_UPDATED", "facility" => $facility];
                        return response()->json($message, 200);
                    } catch (QueryException $exception) {
                        $message = ["errorMessage" => "Form Error"];
                        return response()->json($message, 500);
                    }
                }
            } else {
                $message = ["errorMessage" => "Service Code must be 8 digits"];
                return response()->json($message, 500);
            }
        }
    }

    public function toggleFacility(Request $request)
    {
        $data = json_decode($request->getContent());
        $fa = Facility::find($data->id);
        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $fa->facility_code)) {
            $message = ["errorMessage" => "Service Provider Code Can not contain special characters"];
            return response()->json($message, 400);
        } else {
            $hasBudget = Facility::hasBudget($data->id);
            $hasCeiling = Facility::hasCeiling($data->id);
            if (($data->is_active == false) && ($hasCeiling || $hasBudget)) {
                $feedback = ["errorMessage" => "Service Provider can not be deactivated because it has been assigned ceilings and budget"];
                return response()->json($feedback, 200);
            } else {
                $object = Facility::find($data->id);
                $object->is_active = $data->is_active;
                $object->save();
                $facility = Facility::with("facility_type", "admin_hierarchy", "bank_accounts")->where('id', $object->id)->first();
                $this->segmentData($facility, true);
                if ($data->is_active == false) {
                    $feedback = ["action" => "Service Provider Deactivated", "alertType" => "warning"];
                } else {
                    $feedback = ["action" => "Service Provider activated", "alertType" => "success"];
                }
                return response()->json($feedback, 200);
            }
        }
    }

    public function delete($id)
    {
        $facility = Facility::find($id);
        if (is_null($facility)) {
            $message = ["errorMessage" => "Facility Does not exist!"];
            return response()->json($message, 500);
        }
        try {

            $hasBudget = Facility::hasBudget($id);
            $hasCeiling = Facility::hasCeiling($id);

            if ($hasBudget || $hasCeiling) {
                $message = ["errorMessage" => "Cant delete Service Provider, it has ceilings & budget"];
                return response()->json($message, 500);
            } else {
                $ob = Facility::find($id);
                $ob->delete();
                $fac = Facility::with("facility_type", "admin_hierarchy", "bank_accounts","geolocations")->where('id', $id)->first();
                // $this->segmentData($fac, true);
                $message = ["successMessage" => "DELETED", "facilities" => $fac];
               /// $message = ["successMessage" => "DELETE_SUCCESS"];

                return response()->json($message, 200);
            }
        } catch (\Exception $exception) {
            $message = ["errorMessage" => $exception->getMessage()];
            return response()->json($message, 500);
        }
    }

    public function getByHierarchyAndSection()
    {
        $user = UserServices::getUser();
        $section_id = $user->section_id;
        $admin_hierarchy_id = $user->admin_hierarchy_id;
        $sections = Section::with('childSections')->where('id', $section_id)->get();
        $hierarchies = AdminHierarchy::with('children')->where('id', $admin_hierarchy_id)->get();
        $sec = (array)$sections;
        $this->sectionIds = array();
        $this->flattenSection($sec);
        $admin = (array)$hierarchies;
        $this->adminHierarchyIds = array();
        $this->flattenAdminHierarchies($admin);
        $facilities = Facility::with('facility_type', 'bank_accounts')->whereIn('admin_hierarchy_id', $this->adminHierarchyIds)->select('id', 'name', 'facility_type_id')->get();
        return response()->json($facilities);
    }

    private function flattenSection($array)
    {
        foreach ($array as $value) {
            $this->sectionIds[] = $value[0]->id;
            if (sizeof($value[0]->child_sections) > 0) {
                $this->flatten((array)$value[0]->child_sections);
            }
        }
        return $this->sectionIds;
    }

    private function flattenAdminHierarchies($array)
    {
        foreach ($array as $value) {
            $this->adminHierarchyIds[] = $value[0]->id;
            if (sizeof($value[0]->child_admin_hierarchies) > 0) {
                $this->flatten((array)$value[0]->child_admin_hierarchies);
            }
        }
        return $this->adminHierarchyIds;
    }

    public function upload()
    {
        $facility_type_id = Input::get('facility_type_id');
        $admin_hierarchy_id = Input::get('admin_hierarchy_id');
        $facility_ownership_id = Input::get('facility_ownership_id');
        if (Input::hasFile('file')) {
            $path = Input::file('file')->getRealPath();
            $data = Excel::selectSheets('FACILITIES')->load($path, function ($reader) {

            })->get();
            if (!empty($data) && $data->count()) {
                $error = array();
                $error_row = array();
                $row = 2;
                foreach ($data as $key => $value) {
                    $name = $value->name;
                    $facility_code = $value->facility_code;
                    if (is_null($name) || is_null($facility_code)) {
                        array_push($error, $value);
                        array_push($error_row, $row);
                    }
                    if (strlen($facility_code) != 8) {
                        array_push($error, $value);
                        array_push($error_row, $row);
                    }
                    $row++;
                }
                if (count($error) > 0) {
                    $feedback = ["errorData" => $error, "errorRows" => $error_row];
                    return response()->json($feedback, 200);
                } else {
                    $duplicates = array();
                    $accountErrors = array();
                    foreach ($data as $key => $value) {
                        $exists = Facility::where('facility_code', $value->facility_code)->count();
                        if ($exists == 1) {
                            array_push($duplicates, $value->facility_code);
                            continue;
                        }

                        $f = new Facility();
                        $f->name = $value->name;
                        $f->facility_code = $value->facility_code;
                        $f->description = $value->name;
                        $f->facility_type_id = $facility_type_id;
                        $f->admin_hierarchy_id = $admin_hierarchy_id;
                        $f->facility_ownership_id = $facility_ownership_id;
                        $f->is_active = true;
                        $f->save();

                        $facility_id = $f->id;

                        try {
                            if (isset($value->account_number) && isset($value->account_name) && isset($value->bank)) {
                                $ac = new FacilityBankAccount();
                                $ac->account_number = $value->account_number;
                                $ac->account_name = $value->account_name;
                                $ac->bank = $value->bank;
                                $ac->facility_id = $facility_id;
                                $ac->is_primary = false;
                                $ac->save();
                            }
                        } catch (\Exception $e) {
                            array_push($accountErrors, $e->getMessage());
                            $feedback = ["successMessage" => "Bank Account Not Provided, Facility Added"];
                            return response()->json($feedback, 200);
                        }

                        $facility = Facility::with("facility_type", "admin_hierarchy", "bank_accounts")->where('id', $facility_id)->first();
                        $this->segmentData($facility, false);
                    }
                    if (count($duplicates) > 0) {
                        $feedback = ["successMessage" => "DATA_UPLOADED_SUCCESSFULLY", "duplicates" => $duplicates, "accountErrors" => $accountErrors];
                        return response()->json($feedback, 200);
                    } else {
                        $feedback = ["successMessage" => "DATA_UPLOADED_SUCCESSFULLY"];
                        return response()->json($feedback, 200);
                    }
                }
            } else {
                $message = ["errorMessage" => 'FILE_SPECIFIED_HAS_NO_DATA'];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => 'PLEASE_SELECT_A_FILE'];
            return response()->json($message, 500);
        }
    }

    public function similarFacilities(Request $request)
    {
        $facility_type_id = $request->facility_type_id;
        $council_id = $request->council_id;
        $wards = DB::select("select id from admin_hierarchies where parent_id = '$council_id'");
        $ward_ids = [];
        foreach ($wards as $ward) {
            $id = $ward->id;
            array_push($ward_ids, $id);
        }
        array_push($ward_ids, $council_id);
        $all = Facility::with('facility_type', 'admin_hierarchy')
            ->whereIn('admin_hierarchy_id', $ward_ids)
            ->where('facility_type_id', $facility_type_id)
            ->where('is_active', true)
            ->orderBy("created_at", 'asc')->get();
        return response()->json(["similarFacilities" => $all], 200);
    }

    public function allFacilities()
    {
        $all = Facility::where('is_active', true)->get();
        $data = ['allFacilities' => $all];
        return response()->json($data, 200);
    }

    public function myFacilityCustomDetails(Request $request)
    {
        $facility_type_id = $request->facility_type_id;
        $facility_ownership_id = $request->facility_ownership_id;
        $all = FacilityCustomDetailMapping::with('facility_custom_detail')
            ->where('facility_type_id', $facility_type_id)
            ->where('facility_ownership_id', $facility_ownership_id)
            ->get();
        $data = ['facilityCustomDetails' => $all];
        return response()->json($data, 200);
    }

    public function setCustomDetails(Request $request)
    {
        $facility_id = $request->facility_id;
        $data = $request->all();
        $details = $data['details'];
        foreach ($details as $detail) {
            $facility_custom_detail_id = $detail['facility_custom_detail_id'];
            $detail_value = $detail['detail_value'];
            $v = FacilityCustomDetailValue::where("facility_id", $facility_id)->where("facility_custom_detail_id", $facility_custom_detail_id)->first();
            if ($v == null) {
                $v = new FacilityCustomDetailValue();
                $v->facility_id = $facility_id;
                $v->facility_custom_detail_id = $facility_custom_detail_id;
            }
            $v->detail_value = $detail_value;
            $v->save();
        }
        $data = ["successMessage" => "Custom Detail Added Successfully!"];
        return response()->json($data, 200);
    }

    public function deleteCustomDetail(Request $request)
    {
        $id = $request->id;
        $facility_id = $request->facility_id;
        $obj = FacilityCustomDetailValue::find($id);
        $obj->delete();
        $facility_custom_details = FacilityCustomDetailValue::where('facility_id', $facility_id)->get();
        $data = ['facility_custom_details' => $facility_custom_details, "successMessage" => "Custom Detail Removed Successfully!"];
        return response()->json($data, 200);
    }

    public function adminHierarchyLevel($id)
    {
        array_push($this->adminHierarchies, $id);
        $adminHierarchyId = AdminHierarchy::where('parent_id', $id)->get();
        foreach ($adminHierarchyId as $key => $value) {
            array_push($this->adminHierarchies, $value->id);
            //check if has children
            $count = AdminHierarchy::where('parent_id', $value->id)->count();
            if ($count > 0) {
                $this->adminHierarchyLevel($value->id);
            }
        }
    }

    public function facilityTypeAdminHierarchies(Request $request)
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $facility_type = FacilityType::find($request->facility_type_id);

        $query = DB::table('sectors')
            ->join('sections', 'sectors.id', '=', 'sections.sector_id')
            ->join('facility_types', 'facility_types.section_id', '=', 'sections.id')
            ->where('facility_types.id', $request->facility_type_id)
            ->select('sectors.*');

        $count = $query->count();
        if ($count > 0) {
            $sector = $query->first();
            $code = $sector->code;
        } else {
            $code = 0;
        }

        $admin_hierarchy_level_id = $facility_type->admin_hierarchy_level_id;
        $this->adminHierarchyLevel($admin_hierarchy_id);
        $all = DB::table('admin_hierarchies as a')->join('admin_hierarchies as p', 'p.id', 'a.parent_id')->where('a.admin_hierarchy_level_id', $admin_hierarchy_level_id)->whereIn('a.id', $this->adminHierarchies)->select('a.id', 'a.name', 'p.name as parent')->get();
        $data = ["admin_hierarchies" => $all, "code" => $code];
        return response()->json($data);
    }

    public function facilitiesByTypeAndHierarchy(Request $request)
    {
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $facility_type_id = $request->facility_type_id;
        $validator = Validator::make($request->all(), [
            "admin_hierarchy_id" => "required",
            "facility_type_id" => "required"
        ]);

        if ($validator->fails()) {
            return $validator->errors()->all();
        }

        //sio
        $all = Facility::with('facility_type', 'admin_hierarchy', 'admin_hierarchy.parent', 'bank_accounts', 'physical_state', 'star_rating', 'ownership', 'facility_custom_details', 'village', 'furthest_ward', 'furthest_village','geolocations','geolocations.wardParent')
            ->where('facility_type_id', $facility_type_id)
            ->where('admin_hierarchy_id', $admin_hierarchy_id)
            ->where('is_active', true)
            ->orderBy("created_at", 'asc')->get();
        return response()->json(["facilities" => $all], 200);
    }

    public function facilitiesByTypeCouncilSetup(Request $request)
    {
        $council_id = $request->admin_hierarchy_id;
        $facility_type_id = $request->facility_type_id;
        $perPage = $request->perPage;

        $wards = DB::select("select id from admin_hierarchies where parent_id = '$council_id'");
        $ward_ids = [];
        foreach ($wards as $ward) {
            $id = $ward->id;
            array_push($ward_ids, $id);
        }
        array_push($ward_ids, $council_id);
        //hapa
        $all = Facility::with('facility_type', 'admin_hierarchy', 'admin_hierarchy.parent', 'bank_accounts', 'physical_state', 'star_rating', 'ownership', 'facility_custom_details', 'village', 'furthest_ward', 'furthest_village','geolocations','geolocations.wardParent')
            ->whereIn('admin_hierarchy_id', $ward_ids)
            ->where('facility_type_id', $facility_type_id)
            ->orderBy("created_at", 'asc')->paginate($perPage);
        return response()->json(["facilities" => $all], 200);
    }

    public function facilitiesByTypeCouncilUser(Request $request)
    {
        $council_id = $request->admin_hierarchy_id;
        $facility_type_id = $request->facility_type_id;
        $perPage = $request->perPage;

        $wards = DB::select("select id from admin_hierarchies where parent_id = '$council_id'");
        $ward_ids = [];
        foreach ($wards as $ward) {
            $id = $ward->id;
            array_push($ward_ids, $id);
        }
        array_push($ward_ids, $council_id);
        $all = Facility::with('facility_type', 'admin_hierarchy', 'admin_hierarchy.parent', 'bank_accounts', 'physical_state', 'star_rating', 'ownership', 'facility_custom_details', 'village', 'furthest_ward', 'furthest_village','geolocations','geolocations.wardParent')
            ->whereIn('admin_hierarchy_id', $ward_ids)
            ->where('facility_type_id', $facility_type_id)
            ->where('is_active', true)
            ->orderBy("created_at", 'asc')->paginate($perPage);
        return response()->json(["facilities" => $all], 200);
    }

    public function getUserHierarchyTree()
    {
        return AdminHierarchy::getAdminHierarchyTree(UserServices::getUser()->admin_hierarchy_id);

    }

    private function onlyCouncils()
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $data_array = array();
        $admin_position = DB::table('admin_hierarchies as a')
            ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
            ->where('a.id', $admin_hierarchy_id)
            ->select('l.hierarchy_position')
            ->first();

        //regional level
        if ($admin_position->hierarchy_position == 2) { //subvote Users
            $admin_hierarchies = DB::table('admin_hierarchies as a')
                ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
                ->where('a.id', $admin_hierarchy_id)
                ->whereNotNull('a.parent_id')
                ->select('a.id', 'a.name', 'a.code', 'a.admin_hierarchy_level_id', 'a.parent_id', 'l.hierarchy_position')
                ->distinct()
                ->orderBy('a.name', 'ASC')
                ->get();
        } else {
            //cost center and national
            $admin_hierarchies = DB::table('admin_hierarchies as a')
                ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
                ->where('a.parent_id', $admin_hierarchy_id)
                ->orWhere('a.id', $admin_hierarchy_id)
                ->whereNotNull('a.parent_id')
                ->select('a.id', 'a.name', 'a.code', 'a.admin_hierarchy_level_id', 'a.parent_id', 'l.hierarchy_position')
                ->distinct()
                ->orderBy('a.name', 'ASC')
                ->get();
        }

        foreach ($admin_hierarchies as $key => $value) {
            if ($value->hierarchy_position == 3) {  
                array_push($data_array, $value);
            } elseif($value->hierarchy_position == 2){
                array_push($data_array, $value);
            }
            else if ($value->hierarchy_position == 4) {
                //get 
                $admin_hierarchy = DB::table('admin_hierarchies as a')
                    ->join('admin_hierarchy_levels as l', 'a.admin_hierarchy_level_id', 'l.id')
                    ->where('a.parent_id', $value->id)
                    ->select('a.id', 'a.name', 'a.code', 'a.admin_hierarchy_level_id', 'a.parent_id')
                    ->orderBy('a.name', 'ASC')
                    ->get();
                foreach ($admin_hierarchy as $key2 => $value2) {
                    array_push($data_array, $value2);
                }
            }
        }

        return $data_array;
    }

    public function searchFacility(Request $request)
    {

        $searchText = $request->searchText;
        $onlyActive = $request->onlyActive;
        $perPage = $request->perPage;
        $all = null;

        $Ids = array();

        $councils = $this->onlyCouncils();

        foreach ($councils as $item) {
            $council_id = $item->id;
            array_push($Ids, $council_id);
            $wards = DB::select("select id from admin_hierarchies where parent_id = '$council_id'");
            foreach ($wards as $ward) {
                $ward_id = $ward->id;
                array_push($Ids, $ward_id);
            }
        }

        if ($onlyActive === true) {
            $all = Facility::with(
                'facility_type',
                'admin_hierarchy',
                'bank_accounts',
                'physical_state',
                'star_rating',
                'ownership',
                'facility_custom_details',
                'geolocations'
            )->where('name', 'ILIKE', '%' . $searchText . '%')
                ->where(function ($query) use ($searchText) {
                    $query->where('facility_code', 'ILIKE', '%' . $searchText . '%')
                        ->orWhere('description', 'ILIKE', '%' . $searchText . '%');
                })->where('is_active', true)
                ->whereIn('admin_hierarchy_id', $Ids)
                ->paginate($perPage);
        } else {
            $all = Facility::with(
                'facility_type',
                'admin_hierarchy',
                'bank_accounts',
                'physical_state',
                'star_rating',
                'ownership',
                'facility_custom_details',
                'geolocations'
            )->where('name', 'ILIKE', '%' . $searchText . '%')
                ->where(function ($query) use ($searchText) {
                    $query->where('facility_code', 'ILIKE', '%' . $searchText . '%')
                        ->orWhere('description', 'ILIKE', '%' . $searchText . '%');
                })->whereIn('admin_hierarchy_id', $Ids)
                ->paginate($perPage);
        }
        return response()->json(["facilities" => $all], 200);
    }

    //get facilities of the section
    public function facilityBySector($sector_id, $council)
    {
        //get admin hierarchy
        $admin_hierarchy_id = $council;
        //get list of wards in the council

        $facilities = DB::table('facilities')
            ->join('facility_types', 'facilities.facility_type_id', 'facility_types.id')
            ->join('sections', 'sections.id', 'facility_types.section_id')
            ->whereIn('facilities.admin_hierarchy_id', function ($query) use ($admin_hierarchy_id) {
                $query->select('id')->from('admin_hierarchies')->where('parent_id', $admin_hierarchy_id);
            })->where('sections.sector_id', $sector_id)
            ->where('facilities.is_active', true)
            ->select('facilities.id', 'facilities.name', 'facility_types.id as facility_type_id', 'facility_types.name as facility_type')
            ->orderBy('facilities.name', 'ASC')
            ->get();

        return response()->json($facilities);
    }

    public function facilityByUserSection()
    {
        $sectionId = UserServices::getUser()->section_id;
        $facilities = DB::table('facilities as f')->join('facility_types as ft', 'ft.id', 'f.facility_type_id')->where('ft.section_id', $sectionId)->where('f.is_active', true)->select('f.id', 'f.name', 'ft.name as type')->get();

        return response()->json($facilities);
    }

    public function getByUser($userId)
    {

        return response()->json(['userFacilities' => Facility::getByUser($userId)]);
    }


    public function getAllByPlanningSection(Request $request, $mtefSectionId, $isFacilityAccount)
    {

        $idsToExclude = json_decode($request->getContent())->facilityIdsToExclude;
        $idsToExcludeString = "(0,";
        foreach ($idsToExclude as $teId) {
            $idsToExcludeString = $idsToExcludeString . $teId . ",";
        }
        $idsToExcludeString = rtrim($idsToExcludeString, ',') . ")";


        $data = ['facilities' => Facility::getAllByPlanningSection($mtefSectionId, $isFacilityAccount, $idsToExcludeString)];
        return response()->json($data);
    }

    public function searchByPlanningSection(Request $request, $mtefSectionId, $isFacilityAccount)
    {
        $queryString = $request->searchQuery;
        $queryString = "'%" . strtolower($queryString) . "%'";
        $idsToExcludeBody = json_decode($request->getContent())->facilityIdsToExclude;
        $activityId = Input::get('activityId');
        $idsToExclude = isset($activityId) ? ActivityFacility::where('activity_id', $activityId)->pluck('facility_id') : $idsToExcludeBody;
        $idsToExcludeString = "(0,";
        foreach ($idsToExclude as $teId) {
            $idsToExcludeString = $idsToExcludeString . $teId . ",";
        }
        $idsToExcludeString = rtrim($idsToExcludeString, ',') . ")";
        $data = ['facilities' => Facility::searchFacilityBySection($mtefSectionId, $isFacilityAccount, $idsToExcludeString, $queryString)];
        return response()->json($data);
    }

    public function searchByMtefSectionAndBudgetType(Request $request, $mtefSectionId, $budgetType)
    {
        $queryString = $request->searchQuery;
        $queryString = "'%" . strtolower($queryString) . "%'";
        $data = ['facilities' => Facility::searchFacilityBySection($mtefSectionId, $isFacilityAccount, $idsToExcludeString, $queryString)];
        return response()->json($data);
    }

    public function searchByAdmiAreaAndSection(Request $request, $adminHierarchyId, $sectionId, $isFacilityUser)
    {
        // $queryString = $request->searchQuery;
        // $queryString = "'%" . strtolower($queryString) . "%'";
        // $idsToExclude = ($isFacilityUser == 0) ? [] : Facility::getAllByAdminAreaAndSection($adminHierarchyId, $sectionId);
        // $idsToExcludeString = "(0,";
        // foreach ($idsToExclude as $teId) {
        //     $idsToExcludeString = $idsToExcludeString . $teId . ",";
        // }
        // $idsToExcludeString = rtrim($idsToExcludeString, ',') . ")";
        // $data = ['facilities' => Facility::searchFacilityByAdmiAreaAndSection($adminHierarchyId, $sectionId, $idsToExcludeString, $queryString)];
        $result = DB::table('facilities as f')
        ->join('facility_types as ft','ft.id','=','f.facility_type_id')
        ->where('f.name', 'LIKE', "%{$request->searchQuery}%") 
        ->where('f.admin_hierarchy_id',$adminHierarchyId)
       // ->where('f.facility_code','<>','00000000')
        ->select('f.name as name','f.id as id','ft.name as type')
        ->get();
        $data = ['facilities' => $result];

        return response()->json($data);
    }

    public function getHomeFacilityAndSuperviedFacilityTypes($adminHierarchyId, $sectionId)
    {
        return Facility::getHomeFacilityAndSuperviedFacilityTypes($adminHierarchyId, $sectionId);
    }

    public function setBankAccount(Request $request)
    {
        $account_number = $request->account_number;
        $account_name = $request->account_name;
        $bank = $request->bank;
        $branch = $request->branch;
        $facility_id = Input::get('facility_id');
        $exists = FacilityBankAccount::where('facility_id', $facility_id)
            ->where('account_number', $account_number)
            ->where('bank', $bank)
            ->count();
        if ($exists != 1) {
            $gacs = new FacilityBankAccount();
            $gacs->facility_id = $facility_id;
            $gacs->account_number = $account_number;
            $gacs->account_name = $account_name;
            $gacs->bank = $bank;
            $gacs->branch = $branch;
            $gacs->save();
        }

        $facility = Facility::with("facility_type", "admin_hierarchy", "bank_accounts")->where('id', $facility_id)->first();
        $this->segmentData($facility, true);
        $all = FacilityBankAccount::where('facility_id', $facility_id)->get();
        $message = ["successMessage" => "Successfully added Bank Account", "bankAccounts" => $all];
        return response()->json($message, 200);
    }

    public function deleteBankAccount(Request $request)
    {
        $id = $request->id;
        $obj = FacilityBankAccount::find($id);
        $obj->delete();
        $facility_id = $request->facility_id;
        $facility = Facility::with("facility_type", "admin_hierarchy", "bank_accounts")->where('id', $facility_id)->first();
        $this->segmentData($facility, true);
        $bankAccounts = FacilityBankAccount::where('facility_id', $facility_id)->get();
        $message = ["successMessage" => "Successfully removed Bank Account", "bankAccounts" => $bankAccounts];
        return response()->json($message, 200);
    }

    public function excel()
    {

        $items = Facility::with("facility_type", "admin_hierarchy", "admin_hierarchy.parent","geolocations","geolocations.wardParent")->orderBy('name', 'asc')->get();

        $array = array();
        $i = 1;
        foreach ($items as $value) {
            $active = $value->is_active;
            $data['SNO'] = $i;
            $data['SERVICE PROVIDERS'] = $value->name;
            $data['SP CODE'] = $value->facility_code;
            $data['TYPE'] = $value->facility_type->name;
            $data['PISCs'] = $value->admin_hierarchy->name;
            $data['COUNCIL'] = $value->geolocations->name;
            $data['REGION'] = $value->geolocations->wardParent->name;
            $data['STATUS'] = $active ? "ACTIVE" : "INACTIVE";
            array_push($array, $data);
            $i++;
        }

        Excel::create('SERVICE PROVIDERS' . date("M d, Y H:i:s"), function ($excel) use ($array) {
            $excel->sheet('SERVICE PROVIDERS', function ($sheet) use ($array) {
                $sheet->freezeFirstRow();
                $sheet->setColumnFormat(array(
                    'B' => '@'
                ));
                $sheet->setAutoSize(true);
                $sheet->setAllBorders('thin');
                $sheet->fromArray($array);
            });
        })->export('csv');
    }

    public function facilitiesAllByTypeCouncilUser(Request $request)
    {
        $council_id = $request->admin_hierarchy_id;
        $facility_type_id = $request->facility_type_id;
        $wards = DB::select("select id from admin_hierarchies where parent_id = '$council_id'");
        $ward_ids = [];
        foreach ($wards as $ward) {
            $id = $ward->id;
            array_push($ward_ids, $id);
        }
        array_push($ward_ids, $council_id);
        $all = Facility::with('facility_type', 'admin_hierarchy', 'admin_hierarchy.parent', 'bank_accounts', 'physical_state', 'star_rating', 'ownership', 'facility_custom_details', 'village', 'furthest_ward', 'furthest_village','geographical_locations')
            ->whereIn('admin_hierarchy_id', $ward_ids)
            ->where('facility_type_id', $facility_type_id)
            ->where('is_active', true)
            ->orderBy("created_at", 'asc')->get();
        return response()->json(["facilities" => $all], 200);
    }

}
