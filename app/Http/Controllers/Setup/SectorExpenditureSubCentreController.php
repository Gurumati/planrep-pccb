<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\SectorExpenditureSubCentre;
use App\Models\Setup\SectorExpenditureSubCentreGfsCode;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class SectorExpenditureSubCentreController extends Controller {
    public function index() {
        $all = SectorExpenditureSubCentre::with('expenditure_centre', 'bdc_group', 'sector')->orderBy('created_at', 'desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return SectorExpenditureSubCentre::with('expenditure_centre', 'bdc_group', 'sector')->orderBy('created_at', 'desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $obj = new SectorExpenditureSubCentre();
            $obj->name = $data->name;
            $obj->number = $data->number;
            $obj->expenditure_centre_id = $data->expenditure_centre_id;
            $obj->sector_id = $data->sector_id;
            $obj->save();
            $bdc_sector_expenditure_sub_centre_id = $obj->id;
            $gfs_code_array = $data->gfs_code_id;
            for ($i = 0; $i < count($gfs_code_array); $i++) {
                $gfs_code_id = $gfs_code_array[$i];
                $v = new SectorExpenditureSubCentreGfsCode();
                $v->bdc_sector_expenditure_sub_centre_id = $bdc_sector_expenditure_sub_centre_id;
                $v->gfs_code_id = $gfs_code_id;
                $v->save();
            }
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_SECTOR_EXPENDITURE_SUB_CENTRE_CREATED", "sectorExpenditureSubCentres" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_number = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_number . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try {
            $obj = SectorExpenditureSubCentre::find($data->id);
            $obj->name = $data->name;
            $obj->number = $data->number;
            $obj->expenditure_centre_id = $data->expenditure_centre_id;
            $obj->sector_id = $data->sector_id;
            $obj->gfs_code_id = $data->gfs_code_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_SECTOR_EXPENDITURE_SUB_CENTRE_UPDATED", "sectorExpenditureSubCentres" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_number = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_number . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = SectorExpenditureSubCentre::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_SECTOR_EXPENDITURE_SUB_CENTRE_DELETED", "sectorExpenditureSubCentres" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = SectorExpenditureSubCentre::with('expenditure_centre', 'bdc_group', 'sector')
            ->orderBy('created_at', 'desc')
            ->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        SectorExpenditureSubCentre::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "SECTOR_EXPENDITURE_SUB_CENTRE_RESTORED", "trashedSectorExpenditureSubCentres" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        SectorExpenditureSubCentre::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "SECTOR_EXPENDITURE_SUB_CENTRE_DELETED_PERMANENTLY", "trashedSectorExpenditureSubCentres" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = SectorExpenditureSubCentre::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "SECTOR_EXPENDITURE_SUB_CENTRE_DELETED_PERMANENTLY", "trashedSectorExpenditureSubCentres" => $all];
        return response()->json($message, 200);
    }
}
