<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\PlanningMatrix;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class PlanningMatrixController extends Controller {

    public function index() {
        $all = PlanningMatrix::with('reference_document','reference_document.sFinancialYear','reference_document.eFinancialYear')->orderBy('created_at','asc')->where('is_active',true)->get();
        $data = ["planningMatrices" => $all];
        return response()->json($data);
    }

    public function getAllPaginated($perPage) {
        return PlanningMatrix::with('sectors','reference_document','reference_document.sFinancialYear','reference_document.eFinancialYear')
            // ->join('planning_matrix_adminhierarchies as pma','pma.planning_matrix_id','planning_matrices.id')
            // ->join('admin_hierarchies as ah','ah.id','planning_matrices.')
            ->orderBy('created_at','asc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new PlanningMatrix();
            $obj->name = $data->name;
            $obj->reference_document_id = $data->reference_document_id;
            $obj->is_active = true;
            $obj->save();
            if(isset($data->sectors)){
                 DB::table('planning_matrix_sectors')->insert(['planning_matrix_id'=>$obj->id,'sector_id'=>$data->sectors->id]);
            }
            if(isset($data->adminhierarchies)){
                foreach($data->adminhierarchies as $a){
                     DB::table('planning_matrix_adminhierarchies')->insert(['planning_matrix_id'=>$obj->id,'admin_hierarchy_id'=>$a->id]);
                }
            }
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_number = $exception->errorInfo[1];
            Log::error(UserServices::getUser()->email.'[ERROR_CODE:'.$error_number.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = PlanningMatrix::find($data->id);
            $obj->name = $data->name;
            $obj->reference_document_id = $data->reference_document_id;
            $obj->save();
            DB::table('planning_matrix_sectors')->where('planning_matrix_id',$obj->id)->delete();
            if(isset($data->sectors)){
                foreach($data->sectors as $s){
                    DB::table('planning_matrix_sectors')->insert(['planning_matrix_id'=>$obj->id,'sector_id'=>$s->id]);
                }
            }
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_number = $exception->errorInfo[1];
            Log::error(UserServices::getUser()->email.'[ERROR_CODE:'.$error_number.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = PlanningMatrix::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = PlanningMatrix::with('reference_document','reference_document.sFinancialYear','reference_document.eFinancialYear')->orderBy('created_at', 'asc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        $message = ["trashedItems" => $all];
        return response()->json($message,200);
    }

    public function restore($id) {
        PlanningMatrix::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        PlanningMatrix::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = PlanningMatrix::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function toggleActive(Request $request) {
        $data = json_decode($request->getContent());
        $object = PlanningMatrix::find($data->id);
        $object->is_active = $data->is_active;
        $object->save();
        $all = $this->getAllPaginated($request->perPage);
        if ($data->is_active == false) {
            $feedback = ["action" => "DEACTIVATED", "alertType" => "warning", "items" => $all];
        } else {
            $feedback = ["action" => "ACTIVATED", "alertType" => "success", "items" => $all];
        }
        return response()->json($feedback, 200);
    }
}
