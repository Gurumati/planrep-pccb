<?php

namespace App\Http\Controllers\setup;

use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Setup\FundSourceBudgetClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class FundSourceBudgetClassController extends Controller
{
    public function index(Request $request)
    {
        $financialYearId = $this->getFinancialYearId($request);
        $fundSourceVersionId = $this->getVersionId($request, $financialYearId, 'FS');
        $budgetClassVersionId = $this->getVersionId($request, $financialYearId, 'BC');
        $all = FundSourceBudgetClass::with('fund_source', 'budget_class', 'fund_type')
            ->whereHas('fund_source',function ($fundSource) use($fundSourceVersionId){
                $fundSource->whereHas('fund_source_versions',function ($version) use($fundSourceVersionId){
                    $version->where('version_id',$fundSourceVersionId);
                });
            })->whereHas('budget_class',function ($budgetClass) use($budgetClassVersionId){
                $budgetClass->whereHas('budget_class_versions',function ($version) use($budgetClassVersionId){
                    $version->where('version_id',$budgetClassVersionId);
                });
            })->orderBy('created_at', 'desc')->get();
        return response()->json(["items" => $all], 200);
    }

    public function getAllPaginated($fundSourceVersionId, $budgetClassVersionId, $perPage)
    {
        return FundSourceBudgetClass::with('fund_source', 'budget_class', 'fund_type','bank_account')
            ->whereHas('fund_source',function ($fundSource) use($fundSourceVersionId){
                $fundSource->whereHas('fund_source_versions',function ($version) use($fundSourceVersionId){
                    $version->where('version_id',$fundSourceVersionId);
                })->orderBy('code','asc');
            })->whereHas('budget_class',function ($budgetClass) use($budgetClassVersionId){
                $budgetClass->whereHas('budget_class_versions',function ($version) use($budgetClassVersionId){
                    $version->where('version_id',$budgetClassVersionId);
                });
            })->paginate($perPage);
    }

    public function paginated(Request $request)
    {
        $financialYearId = $request->financialYearId;
        $fundSourceVersionId = $this->getVersionId($request, $financialYearId, 'FS');
        $budgetClassVersionId = $this->getVersionId($request, $financialYearId, 'BC');
        $all = $this->getAllPaginated($fundSourceVersionId, $budgetClassVersionId, $request->perPage);
        return response()->json($all);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), FundSourceBudgetClass::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        FundSourceBudgetClass::create($request->all());
        $financialYearId = $this->getFinancialYearId($request);
        $fundSourceVersionId = $this->getVersionId($request, $financialYearId, 'FS');
        $budgetClassVersionId = $this->getVersionId($request, $financialYearId, 'BC');
        $all = $this->getAllPaginated($fundSourceVersionId, $budgetClassVersionId,$request->perPage);
        $message = ["successMessage" => "CREATE_SUCCESSFUL", "items" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, FundSourceBudgetClass::rules($request->id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        $expCategory = FundSourceBudgetClass::find($request->id);
        $expCategory->update($data);
        $financialYearId = $this->getFinancialYearId($request);
        $fundSourceVersionId = $this->getVersionId($request, $financialYearId, 'FS');
        $budgetClassVersionId = $this->getVersionId($request, $financialYearId, 'BC');
        $all = $this->getAllPaginated($fundSourceVersionId, $budgetClassVersionId,$request->perPage);
        $message = ["successMessage" => "UPDATE_SUCCESSFUL", "items" => $all];
        return response()->json($message, 200);
    }

    public function delete(Request $request,$id)
    {
        $obj = FundSourceBudgetClass::find($id);
        $obj->delete();
        $financialYearId = $this->getFinancialYearId($request);
        $fundSourceVersionId = $this->getVersionId($request, $financialYearId, 'FS');
        $budgetClassVersionId = $this->getVersionId($request, $financialYearId, 'BC');
        $all = $this->getAllPaginated($fundSourceVersionId, $budgetClassVersionId,$request->perPage);
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }

    public function exportToCsv(Request $request)
    {
        $financialYearId = $request->financialYearId;
        $fundSourceVersionId = $this->getVersionId($request, $financialYearId, 'FS');
        $budgetClassVersionId = $this->getVersionId($request, $financialYearId, 'BC');

        $items = FundSourceBudgetClass::with('fund_source', 'budget_class', 'fund_type','bank_account')
            ->whereHas('fund_source',function ($fundSource) use($fundSourceVersionId){
                $fundSource->whereHas('fund_source_versions',function ($version) use($fundSourceVersionId){
                    $version->where('version_id',$fundSourceVersionId);
                })->orderBy('code','asc');
            })->whereHas('budget_class',function ($budgetClass) use($budgetClassVersionId){
                $budgetClass->whereHas('budget_class_versions',function ($version) use($budgetClassVersionId){
                    $version->where('version_id',$budgetClassVersionId);
                });
            })->get();

        $array = array();
        foreach ($items as $value) {
            $data = array();
            $data['FUND_SOURCE_CODE'] = $value->fund_source->code;
            $data['FUND_SOURCE'] = $value->fund_source->name;
            $data['BUDGET_CLASS_CODE'] = $value->budget_class->code;
            $data['BUDGET_CLASS'] = $value->budget_class->name;
            array_push($array, $data);
        }

        Excel::create('FUND_SOURCE_BUDGET_CLASS_MAPPING_' . time(), function ($excel) use ($array) {
            $excel->sheet('DATA', function ($sheet) use ($array) {
                $sheet->fromArray($array);
            });
        })->export('csv');
    }
}
