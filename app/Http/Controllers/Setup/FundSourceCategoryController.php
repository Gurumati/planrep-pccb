<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\FundSourceCategory;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class FundSourceCategoryController extends Controller {
    public function index() {
        $obj = FundSourceCategory::with('fund_type')->orderBy('created_at','asc')->get();
        return response()->json($obj);
    }

    public function getAllPaginated($perPage) {
        return FundSourceCategory::with('fund_type')->orderBy('created_at','asc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new FundSourceCategory();
            $obj->code = $data->code;
            $obj->name = $data->name;
            $obj->fund_type_id = $data->fund_type_id;
            $obj->created_by = UserServices::getUser()->id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "fundSourceCategories" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = FundSourceCategory::find($data->id);
            $obj->code = $data->code;
            $obj->name = $data->name;
            $obj->fund_type_id = $data->fund_type_id;
            $obj->receipt_code = isset($data->receipt_code)? $data->receipt_code : null;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCESS", "fundSourceCategories" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        } 
    }

    public function delete($id) {
        $obj = FundSourceCategory::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "fundSourceCategories" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = FundSourceCategory::with('fund_type')->orderBy('created_at', 'asc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        $message = ["trashedFundSourceCategories" => $all];
        return response()->json($message);
    }

    public function restore($id) {
        FundSourceCategory::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedFundSourceCategories" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        FundSourceCategory::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedFundSourceCategories" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = FundSourceCategory::orderBy('created_at', 'asc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedFundSourceCategories" => $all];
        return response()->json($message, 200);
    }

    public function upload() {
        $fund_type_id = Input::get('fund_type_id');
        if (Input::hasFile('file')) {
            $path = Input::file('file')->getRealPath();
            $data = Excel::selectSheets('FUND_SOURCE_CATEGORIES')->load($path, function ($reader) {})->get();
            if (!empty($data) && $data->count() > 0) {
                foreach ($data as $key => $value) {
                    $code = $value->code;
                    $count = FundSourceCategory::where('code', $code)->count();
                    if ($count == 1) {
                        continue;
                    }
                    $insert[] = [
                        'name' => $value->name,
                        'code' => $value->code,
                        'fund_type_id' => $fund_type_id
                    ];
                }
                if (!empty($insert)) {
                    try {
                        DB::table('fund_source_categories')->insert($insert);
                        $all = $this->getAllPaginated(Input::get('perPage'));
                        $message = ["successMessage" => "DATA_UPLOAD_SUCCESS", "fundSourceCategories" => $all];
                        return response()->json($message, 200);
                    } catch (QueryException $exception) {
                        $error_code = $exception->errorInfo[1];
                        Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
                        $message = ["errorMessage" => "DATABASE_ERROR"];
                        return response()->json($message, 500);
                    }
                } else {
                    $message = ["successMessage" => 'DATA_UPLOAD_SUCCESS'];
                    return response()->json($message, 500);
                }
            } else {
                $message = ["errorMessage" => 'FILE_SPECIFIED_HAS_NO_DATA'];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => 'PLEASE_SELECT_FILE'];
            return response()->json($message, 500);
        }
    }
}
