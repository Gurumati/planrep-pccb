<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\PeSubmissionFormReport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;


class PeSubmissionFormReportController extends Controller
{
    public function index(Request $request){
        $units = $this->getAllPaginated($request->perPage);
        return response()->json($units);
    }

    public function getAllPaginated($perPage) {
        return PESubmissionFormReport::with('parent')->paginate($perPage);
    }

    public function allReports(){
        $reports =  PeSubmissionFormReport::select('name','title','id')->get();
        return response()->json($reports);
    }

    public function store(Request $request){
        try {
            $data = json_decode($request->getContent());
            $report = new PESubmissionFormReport();
            $report->name = $data->name;
            $report->code = $data->code;
            $report->title = $data->title;
            $report->params = implode(',',$data->params);
            $report->parent_id = (isset($data->parent_id)) ? $data->parent_id : null;
            $report->active = true;
            $report->sql_query = $data->sql_query;
            $report->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_REPORT_CREATED", "reports" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request){
        try {
            $data = json_decode($request->getContent());
            $report = PESubmissionFormReport::find($data->id);
            $report->name = $data->name;
            $report->code = $data->code;
            $report->title = $data->title;
            $report->params = implode(',',$data->params);
            $report->active = true;
            $report->parent_id = (isset($data->parent_id)) ? $data->parent_id : null;
            $report->sql_query = $data->sql_query;
            $report->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_REPORT_UPDATED", "reports" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id){
        $report = PESubmissionFormReport::find($id);
        $report->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFULLY_REPORT_DELETED", "reports" => $all];
        return response()->json($message, 200);
    }

    public function toggleReport(Request $request) {
        $data = json_decode($request->getContent());
        $object = PESubmissionFormReport::find($data->id);
        $object->active = $data->active;
        $object->save();
        $report = $this->getAllPaginated($request->perPage);
        if ($data->active == false) {
            $feedback = ["action" => "REPORT_DEACTIVATED", "alertType" => "warning", "reports" => $report];
        } else {
            $feedback = ["action" => "REPORT_ACTIVATED", "alertType" => "success", "reports" => $report];
        }
        return response()->json($feedback, 200);
    }
}
