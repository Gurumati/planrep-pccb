<?php

/**
 * FinancialYear Status
 * 0-Created default state
 * 1-Opened for planning and budgeting
 * 2-Execution stage
 * 3-Next to be opened
 * -1- Closed
 */

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Controllers\SharedService;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Budgeting\BudgetType;
use App\Models\Planning\Activity;
use App\Models\Planning\AnnualTarget;
use App\Models\Planning\LongTermTarget;
use App\Models\Planning\Mtef;
use App\Models\Planning\MtefAnnualTarget;
use App\Models\Planning\MtefSection;
use App\Models\Planning\PlanChain;
use App\Models\Planning\ReferenceDocument;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\AdminHierarchyLevel;
use App\Models\Setup\BudgetClassTemplate;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\FinancialYear;
use App\Models\Setup\FinancialYearVersion;
use App\Models\Setup\Period;
use App\Models\Setup\PeriodGroup;
use App\Models\Setup\Section;
use App\Models\Setup\SectionLevel;
use App\Models\Setup\SectionParent;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class FinancialYearController extends Controller
{

    public function index()
    {
        $financialYears = FinancialYear::with('previousYear')->where('is_active', true)->select('id', 'name', 'start_date', 'end_date', 'status')->orderBy('start_date', 'asc')->get();
        return response()->json($financialYears);
    }
    public function periodsByYear($financialYearId)
    {
        $all = Period::byYear($financialYearId);
        return response()->json(['periods' => $all], 200);
    }


    public function all(){
        $financialYears = FinancialYear::with('previousYear')->where('is_active', true)->select('id', 'name', 'start_date', 'end_date', 'status')->orderBy('start_date', 'asc')->get();
        return response()->json(['financialYears'=>$financialYears]);
    }

    public function versions(Request $request)
    {
        $financialYearId = $request->id;
        if (isset($request->type)) {
            $items = $this->financialYearVersions($financialYearId, $request->type);
        } else {
            $items = $this->financialYearVersions($financialYearId);
        }
        return apiResponse(200, "success", $items, true, []);
    }

    public function addVersion(Request $request)
    {
        $financialYearId = $request->financial_year_id;
        if (isset($request->versions)) {
            foreach ($request->versions as $version) {
                $count = FinancialYearVersion::where("financial_year_id", $financialYearId)
                    ->where("version_id", $version['id'])
                    ->count();
                if ($count == 0) {
                    $fv = new FinancialYearVersion();
                    $fv->financial_year_id = $financialYearId;
                    $fv->version_id = $version['id'];
                    $fv->save();
                }
            }
        }

        $items = $this->financialYearVersions($financialYearId);
        return apiResponse(201, "Success", $items, true, []);
    }

    public function otherFinancialYearVersions(Request $request)
    {
        $financialYearId = $request->financialYearId;
        $currentVersionId = $request->currentVersionId;
        $currentFinancialYearId = $request->currentFinancialYearId;
        if (isset($request->type)) {
            $items = $this->otherVersions($financialYearId, $currentVersionId, $currentFinancialYearId, $request->type);
        } else {
            $items = $this->otherVersions($financialYearId, $currentVersionId, $currentFinancialYearId);
        }

        return apiResponse(200, "success", $items, true, []);
    }

    public function removeVersion(Request $request)
    {
        $id = $request->id;
        $x = FinancialYearVersion::find($id);
        $x->delete();
        $items = $this->financialYearVersions($request->financialYearId);
        return apiResponse(200, "Version removed successfully!", $items, true, []);
    }

    public function getByBudgetType($budgetType)
    {

        if (!in_array($budgetType, BudgetType::getAll())) {
            return response()->json(['errorMessage' => 'Invalid budget type'], 400);
        }
        $financialYear = FinancialYear::getByBudgetType($budgetType);
        $previousFinancialYear = (isset($financialYear)) ? FinancialYear::getPreviousYear($financialYear->id) : null;
        return ['financialYear' => $financialYear, 'previousFinancialYear' => $previousFinancialYear];
    }

    public function previousFinancialYears()
    {
        $financialYears = FinancialYear::where('status', -1)
            ->select('id', 'name', 'start_date', 'end_date')
            ->orderBy('start_date', 'asc')->get();
        return response()->json(["financialYears" => $financialYears], 200);
    }

    public function fetchAll()
    {
        $financialYears = FinancialYear::with('previousYear')->where('is_active', true)
            ->select('id', 'name', 'start_date', 'end_date')
            ->orderBy('start_date', 'asc')->get();
            return response()->json(["financialYears" => $financialYears], 200);
    }

    private function setNextToOpen()
    {
        $today = date("Y-m-d");
        $nextToOpen = FinancialYearServices::getNextFinancialYear();
        if (!isset($nextToOpen)) {
            $financialYear = FinancialYear::whereDate('end_date', '>', $today)->where('status', 0)->orderBy('start_date', 'asc')->first();
            if (!is_null($financialYear)){
                $financialYear->status = 3;
                $financialYear->save();
            }
        }
    }


    public function getAllPaginated($perPage)
    {
        $today = date("Y-m-d");
        $this->setNextToOpen();
        return FinancialYear::with('previousYear')->orderBy('start_date', 'asc')->paginate($perPage);
    }

    public function usableFinancialYears()
    {
        $financialYears = FinancialYear::with('previousYear')->whereIn('status', [1, 2])->get();
        $data = ['financialYears' => $financialYears];
        return response()->json($data);
    }

    public function forwardFinancialYears($financialYearId)
    {
        $currentYears = FinancialYear::find($financialYearId);
        $forwardFinancialYears = FinancialYear::with('previousYear')->where('start_date', '>', $currentYears->end_date)->where('is_active', true)->orderBy('start_date', 'asc')->limit(4)->select('id', 'name')->get();
        return response()->json($forwardFinancialYears);
    }

    public function lastFinancialYear()
    {
        $financialYear = FinancialYear::with('previousYear')->where('end_date', (DB::table('financial_years')->max('end_date')))->first();
        return response()->json($financialYear);
    }

    public function paginateIndex(Request $request)
    {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function getCalenderFinancialYears()
    {
        $financialYears = FinancialYear::with('previousYear')->whereIn('status', [1, 2, 3])->where('is_active', true)->orderBy('status', 'desc')->get();
        return response()->json($financialYears);
    }

    public function forIndicatorProjection()
    {
        //TODO considers previousYear years
        try {
            $financialYears = FinancialYearServices::getPlanningFinancialYear();
            $refDoc = SharedService::getUserStrategicPlan($financialYears);
            $startF = FinancialYear::find($refDoc->start_financial_year);
            $endF = FinancialYear::find($refDoc->end_financial_year);

            $financialYears = FinancialYear::where('start_date', '>=', $startF->start_date)->where('end_date', '<=', $endF->end_date)->orderBy('start_date', 'asc')->get();
            $executionFinancialYear = FinancialYearServices::getExecutionFinancialYear();
            $message = ["financialYears" => $financialYears, 'executionFinancialYear' => $executionFinancialYear];
            return response()->json($message);
        } catch (QueryException $e) {
            $message = ["errorMessage" => $e->getMessage()];
            return response()->json($message);
        }

    }


    public function initialiseTargets($financialYear)
    {

        $financialYearId = $financialYear->id;
        $councilLevels = AdminHierarchyLevel::where('hierarchy_position', 3)->get();
        foreach ($councilLevels as $councilLevel) {
            $admin_hierarchies = AdminHierarchy::where('admin_hierarchy_level_id', $councilLevel->id)->get();
            foreach ($admin_hierarchies as $admin_hierarchy) {
                $admin_hierarchy_id = $admin_hierarchy->id;
                $councilDocId = ReferenceDocument::councilRefDoc($admin_hierarchy_id, $financialYear);
                if (isset($councilDocId)) {
                    $LongTermTargets = LongTermTarget::where('reference_document_id', $councilDocId)->get();
                    foreach ($LongTermTargets as $longTermTarget) {
                        $mtef = Mtef::where('admin_hierarchy_id', $admin_hierarchy_id)->where('financial_year_id', $financialYearId)->first();
                        $count = AnnualTarget::where('long_term_target_id', $longTermTarget->id)
                            ->where('mtef_id', $mtef->id)->where('section_id', $longTermTarget->section_id)->count();
                        if ($count < 1 && isset($longTermTarget->section_id)) {
                            $annualTarget = new AnnualTarget();
                            $annualTarget->description = $longTermTarget->description;
                            $annualTarget->code = $longTermTarget->code;
                            $annualTarget->long_term_target_id = $longTermTarget->id;
                            $annualTarget->mtef_id = $mtef->id;
                            $annualTarget->is_final = false;
                            $annualTarget->section_id = $longTermTarget->section_id;
                            $annualTarget->created_by = UserServices::getUser()->id;
                            $annualTarget->created_at = date('Y-m-d H:i:s');
                            $annualTarget->save();
                        }
                    }
                }
            }
        }

    }


    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, FinancialYear::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "Form Errors"];
            return response()->json($message, 500);
        } else {
            FinancialYear::create($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "financialYears" => $all];
            return response()->json($message, 200);
        }
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, FinancialYear::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "Form Errors"];
            return response()->json($message, 500);
        } else {
            $obj = FinancialYear::find($request->id);
            $obj->update($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "financialYears" => $all];
            return response()->json($message, 200);
        }
    }

    public function toggleFinancialYear(Request $request)
    {
        $data = json_decode($request->getContent());
        $object = FinancialYear::find($data->id);
        $object->is_active = $data->is_active;
        $object->save();
        $all = $this->getAllPaginated($request->perPage);
        if ($data->is_active == false) {
            $feedback = ["action" => "ACTIVITY_FINANCIAL_YEAR_DEACTIVATED", "alertType" => "warning", "financialYears" => $all];
        } else {
            $feedback = ["action" => "ACTIVITY_FINANCIAL_YEAR_ACTIVATED", "alertType" => "success", "financialYears" => $all];
        }
        return response()->json($feedback, 200);
    }

    public function delete($id)
    {
        $financialYear = FinancialYear::find($id);
        $financialYear->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFULLY_FINANCIAL_YEAR_DELETED", "financialYears" => $all];
        return response()->json($message, 200);
    }

    public function execute($id)
    {
        try {
            FinancialYear::where('id', $id)->update(['status' => 2]);
            $message = ["successMessage" => "SUCCESSFUL_EXECUTE_FINANCIAL_YEAR"];
            return response()->json($message, 200);
        } catch (\Exception $exception) {
            $message = ["errorMessage" => "ERROR_EXECUTE_FINANCIAL_YEAR"];
            return response()->json($message, 500);
        }

    }

    public function reExecute($id)
    {
        try {
            FinancialYear::where('id', $id)->update(['status' => 1]);
            $message = ["successMessage" => "SUCCESSFUL_RE_EXECUTE_FINANCIAL_YEAR"];
            return response()->json($message, 200);
        } catch (\Exception $exception) {
            $message = ["errorMessage" => "ERROR_RE_EXECUTE_FINANCIAL_YEAR"];
            return response()->json($message, 500);
        }

    }

    public function close($id)
    {
        //TODO carry activities
        FinancialYear::find($id)->update(['status' => -1]);
        return ['successMessage'=>'Financial year closed successfull'];
    }

    public function openPreRequest($id)
    {
        try {
            $messages = [];
            $passed = true;
            $existing = FinancialYearServices::getPlanningFinancialYear();
            if ($existing != null) {
                $passed = false;
                $messages['EXISTING_OPEN_YEAR'] = $existing->name;
            }
            //Check if mapping is configured
            $levelsIds = DB::table('admin_hierarchy_lev_sec_mappings')
                ->distinct('admin_hierarchy_level_id')->where('can_budget', true)
                ->select('admin_hierarchy_level_id')->get();
            if (count($levelsIds) == 0) {
                $messages['NO_BUDGETING_SECTION_CONFIGURED'] = true;
                $passed = false;
            }
            //Check first if all levels configure to budget has default decision level before proceed
            foreach ($levelsIds as $value) {

                $adminHierarchyLevel = AdminHierarchyLevel::find($value->admin_hierarchy_level_id);
//                $section = Section::find($value->section_id);

                $decisionLevel = DB::table('decision_levels')->where('admin_hierarchy_level_position', $adminHierarchyLevel->hierarchy_position)->
//                                                    where('section_level_id', $section->section_level_id)->
                where('is_default', true)->count();
                if ($decisionLevel === 0) {
                    $messages['NO_DEFAULT_DECISION_LEVEL'][] = $adminHierarchyLevel->name;
                    $passed = false;
                } else {
                    $sectionsLevels = DB::table('admin_hierarchy_lev_sec_mappings as m')
                        ->leftJoin('sections as s', 's.id', 'm.section_id')
                        ->leftJoin('section_levels as sl', 'sl.id', 's.section_level_id')
                        ->distinct()->where('can_budget', true)
                        ->where('admin_hierarchy_level_id', $value->admin_hierarchy_level_id)
                        ->select('sl.name')
                        ->get();
                    $level = ["name" => $adminHierarchyLevel->name, "sections" => $sectionsLevels];
                    $messages['BUDGETING_LEVELS'][] = $level;
                }
            }

            $message = ["messages" => $messages, "passed" => $passed];
            return response()->json($message, 200);
        } catch (QueryException $e) {
            $message = ["errorMessage" => "FINANCIAL_YEAR_PRE_INITIALIZATION_FAILED", "exception" => $e];
            return response()->json($message, 400);
        } catch (\Exception $e) {
            $message = ["errorMessage" => "FINANCIAL_YEAR_PRE_INITIALIZATION_FAILED", "exception" => $e];
            return response()->json($message, 400);
        }

    }

    public function openBySection($sectionId)
    {
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $mtefs = DB::table('mtefs')->where('financial_year_id', $financialYear->id)->get();
        $total = 0;
        foreach ($mtefs as $mtef) {
            $exist = DB::table('mtef_sections')->where('mtef_id', $mtef->id)->where('section_id', $sectionId)->count();
            if ($exist === 0) {
                DB::table('mtef_sections')->insert(
                    array(
                        'mtef_id' => $mtef->id,
                        'section_id' => $sectionId,
                        'decision_level_id' => $mtef->decision_level_id,
                        'is_locked' => false
                    )
                );
                $total = $total + 1;
            }
        }
        $message = ["total" => $total];
        return response()->json($message, 200);
    }

    public function open(Request $request, $id)
    {
        set_time_limit(10000);
        $check = FinancialYearServices::getPlanningFinancialYear();

        if (isset($check)) {
            $message = ['errorMessage' => 'OPEN_YEAR_EXIST'];
            return response()->json($message, 400);
        } else {

            try {
                $financialYear = FinancialYear::find($id);
                $financialYearId = $financialYear->id;
                $levelsIds = DB::table('admin_hierarchy_lev_sec_mappings')
                    ->distinct()->where('can_budget', true)
                    ->select('admin_hierarchy_level_id')->get();
                $status = DB::transaction(function () use ($financialYear, $levelsIds) {

                    foreach ($levelsIds as $value) {
                        $adminLevel = AdminHierarchyLevel::find($value->admin_hierarchy_level_id);

                        $adminHierarchies = DB::table('admin_hierarchies')
                            ->where('admin_hierarchy_level_id', $value->admin_hierarchy_level_id)
                            ->get();
                        $sectionIds = DB::table('admin_hierarchy_lev_sec_mappings')->distinct('section_id')
                            ->where('admin_hierarchy_level_id', $value->admin_hierarchy_level_id)
                            ->where('can_budget', true)->get();


                        foreach ($adminHierarchies as $adminHierarchy) {

                            $defaultMtefDecisionLevel = DB::table('decision_levels')
                                ->where('admin_hierarchy_level_position', $adminLevel->hierarchy_position)->where('is_default', true)->first();

                            $existingMtef = Mtef::where('financial_year_id', $financialYear->id)->where('admin_hierarchy_id', $adminHierarchy->id)->count();

                            if ($existingMtef < 1) {
                                $mtefId = DB::table('mtefs')->insertGetId(
                                    array(
                                        "financial_year_id" => $financialYear->id,
                                        "admin_hierarchy_id" => $adminHierarchy->id,
                                        "locked" => false,
                                        "decision_level_id" => $defaultMtefDecisionLevel->id,
                                        "plan_type" => "CURRENT",
                                        "is_final" => false
                                    )
                                );
 
                                foreach ($sectionIds as $section) {

                                     $admin =  DB::table('admin_hierarchy_sections')
                                     ->where('section_id',$section->section_id)
                                     ->where('admin_hierarchy_id',$adminHierarchy->id)->first();
                                     
                                     if($admin){

                                        $sec = Section::find($section->section_id);
                                        $defaultMtefSectionDecisionLevel = DB::table('decision_levels')
                                            ->where('admin_hierarchy_level_position', $adminLevel->hierarchy_position)
                                            ->where('section_level_id', 4)
                                            ->where('is_default', true)->first();
                                         $existing = MtefSection::where('mtef_id', $mtefId)
                                            ->where('section_id', $section->section_id)
                                            ->count();

                                        if ($existing < 1) {
                                            if($defaultMtefSectionDecisionLevel){
                                                $mtef_section_id = DB::table('mtef_sections')->insertGetId(
                                                    array(
                                                        "mtef_id" => $mtefId,
                                                        "section_id" => $section->section_id,
                                                        "decision_level_id" => $defaultMtefSectionDecisionLevel->id
                                                    )
                                                );
                                            }

                                        }
                                    }
                                }
                            }


                        }
                    }

                    $this->initialiseTargets($financialYear);
                    //Link to current Versions
                   //TODO FinancialYear::linkCurrentVersions($financialYear->id);
                    DB::table('financial_years')->where("id", $financialYear->id)->update(["status" => 1]);
                    $this->setNextToOpen();


                });
                $message = ["successMessage" => "FINANCIAL_YEAR_INITIALIZATION_SUCCESSFUL", "financialYears" => $this->getAllPaginated($request->perPage)];
                return response()->json($message, 200);


            } catch (QueryException $e) {
                $message = ["errorMessage" => "FINANCIAL_YEAR_INITIALIZATION_FAILED", "exception" => $e];
                //return response()->json($message, 400);
                return $e;
            } catch (\Exception $e) {
                $message = ["errorMessage" => "FINANCIAL_YEAR_INITIALIZATION_FAILED", "exception" => $e];
                //return response()->json($message, 400);
                return $e;
            }
        }

    }

    /**
     * @param $financialYearId
     * @return mixed
     */
    public function financialYearVersions($financialYearId, $type = null)
    {
        $sql = "select v.id,v.name,vt.name as type,v.date,fv.id as financial_year_version_id from versions v
                join financial_year_versions fv on v.id = fv.version_id
                join version_types vt on vt.id = v.version_type_id
                where v.deleted_at is null and fv.financial_year_id = $financialYearId";
        if (isset($type)) {
            $sql .= " and vt.name = '$type'";
        }
        $items = DB::select($sql);
        return $items;
    }

    public function otherVersions($financialYearId, $currentVersionId, $currentFinancialYearId, $type = null)
    {
        if ($financialYearId == $currentFinancialYearId) {
            $sql = "select v.id,v.name,fv.id as financial_year_version_id,vt.name as type,v.date from versions v
                join financial_year_versions fv on v.id = fv.version_id
                join version_types vt on vt.id = v.version_type_id
                where v.deleted_at is null and fv.financial_year_id = $financialYearId
                and v.id not in ($currentVersionId)";
            if (isset($type)) {
                $sql .= " and vt.name = '$type'";
            }
        } else {
            $sql = "select v.id,v.name,fv.id as financial_year_version_id,vt.name as type,v.date from versions v
                join financial_year_versions fv on v.id = fv.version_id
                join version_types vt on vt.id = v.version_type_id
                where v.deleted_at is null and fv.financial_year_id = $financialYearId";
            if (isset($type)) {
                $sql .= " and vt.name = '$type'";
            }
        }
        $items = DB::select($sql);
        return $items;
    }

    private function getTargetSectionId($sectionId)
    {
        $sections = SectionParent::where('id', $sectionId)->get();
        $flatten = new Flatten();
        $sectionIds = $flatten->flattenSectionWithParent($sections);
        $targetSection = DB::table('admin_hierarchy_lev_sec_mappings')->whereIn('section_id', $sectionIds)->where('can_target', true)->first();
        return $targetSection->section_id;
    }

    public function initiateAutoBudgetPlanChain($budgetClassId)
    {
        $maxLevel = DB::table('plan_chain_types')->max('hierarchy_level');
        $lowerPlanChainLevel = DB::table('plan_chain_types')->where('hierarchy_level', $maxLevel)->first();
        $planChainBudgetClassTemplate = BudgetClassTemplate::where('type', '1')->where('budget_class_id', $budgetClassId)->first();
        $existing = PlanChain::where('plan_chain_type_id', $lowerPlanChainLevel->id)->where('code', $planChainBudgetClassTemplate->code)->first();
        if ($existing == null) {
            $planChainId = DB::table('plan_chains')->insertGetId(
                array(
                    "description" => $planChainBudgetClassTemplate->description,
                    "code" => $planChainBudgetClassTemplate->code,
                    "is_active" => true,
                    "plan_chain_type_id" => $lowerPlanChainLevel->id
                )
            );
            return $planChainId;
        } else {
            return $existing->id;
        }
    }

    public function initiateAutoBudgetLongTermTarget($budgetClassId, $adminHierarchyId, $financialYearId)
    {
        $maxLevel = DB::table('plan_chain_types')->max('hierarchy_level');
        $lowerPlanChainLevel = DB::table('plan_chain_types')->where('hierarchy_level', $maxLevel)->first();
        $planChainBudgetClassTemplate = BudgetClassTemplate::where('type', '1')->where('budget_class_id', $budgetClassId)->first();
        $existingPlanChain = PlanChain::where('plan_chain_type_id', $lowerPlanChainLevel->id)->where('code', $planChainBudgetClassTemplate->code)->first();
        $longTermTemplate = BudgetClassTemplate::where('type', '2')->where('budget_class_id', $budgetClassId)->first();
        $refDocId = $this->getRefDocId($adminHierarchyId, $financialYearId);
        $existing = LongTermTarget::where('reference_document_id', $refDocId)->where('code', $longTermTemplate->code)->first();

        if ($existing == null) {
            $longTermId = DB::table('long_term_targets')->insertGetId(array(
                "plan_chain_id" => $existingPlanChain->id,
                "description" => $longTermTemplate->description,
                "is_active" => true,
                "code" => $longTermTemplate->code,
                "reference_document_id" => $refDocId,
            ));
            return $longTermId;
        } else {
            return $existing->id;
        }
    }

    function getTopSections()
    {
        $sectionLevels = SectionLevel::where('hierarchy_position', 1)->first();
        $levelArray = [];
        foreach ($sectionLevels as $sectionLevel) {
            $levelArray[] = $sectionLevels->id;
        }
        $sections = Section::whereIn('section_level_id', $levelArray)->get();
        $sectionArray = [];
        foreach ($sections as $section) {
            $sectionArray[] = $section->id;
        }

        return $sectionArray;
    }

    public function initiateAutoBudgetAnnualTarget($mtefId, $sectionId, $longTermId)
    {
        $target = LongTermTarget::where('id', $longTermId)->first();
        $existing = MtefAnnualTarget::where('mtef_id', $mtefId)->where('long_term_target_id', $longTermId)->where('section_id', $sectionId)
            ->where('code', $target->code)->first();
        if ($existing == null) {
            $annualTargetId = DB::table('mtef_annual_targets')->insertGetId(array(
                "description" => $target->description,
                "code" => $target->code,
                "mtef_id" => $mtefId,
                "long_term_target_id" => $longTermId,
                "section_id" => $sectionId
            ));
            return $annualTargetId;
        } else {
            return $existing->id;
        }
    }

    public function initiateAutoBudgetActivities($budgetClassId, $annualTargetId, $mtef_section_id)
    {
        $activityTemplate = BudgetClassTemplate::where('type', '3')->where('budget_class_id', $budgetClassId)->first();
        $existing = Activity::where('budget_class_id', $budgetClassId)->where('mtef_annual_target_id', $annualTargetId)
            ->where('mtef_section_id', $mtef_section_id)->first();
        if ($existing == null) {
            $activityId = DB::table('activities')->insertGetId(array(
                "description" => $activityTemplate->description,
                "code" => $activityTemplate->code,
                "budget_class_id" => $budgetClassId,
                "progress_status" => "OPEN",
                "mtef_annual_target_id" => $annualTargetId,
                "mtef_section_id" => $mtef_section_id,
                "locked" => false,
            ));
            return $activityId;
        } else {
            return $existing->id;
        }

    }

    public function getRefDocId($adminHierarchyId, $financialYearId)
    {
        $financialYear = FinancialYear::find($financialYearId);
        $refDocument = DB::table("reference_documents as r")
            ->where("r.admin_hierarchy_id", $adminHierarchyId)
            ->join("financial_years as sF", "sF.id", "=", "r.start_financial_year")
            ->join("financial_years as eF", "eF.id", "=", "r.end_financial_year")
            ->where("sF.start_date", "<=", "'" . $financialYear->start_date . "'")
            ->where("eF.end_date", ">=", "'" . $financialYear->end_date . "'")
            ->select("r.*")->orderBy('r.id', 'asc')->first();
        if ($refDocument == null) {
            return null;
        } else {
            return $refDocument->id;
        }
    }

    public function setRefDoc($adminHierarchy, $financialYearId)
    {
        $minYears = ConfigurationSetting::where('key', 'MINIMUM_YEARS_FOR_A_REFERENCE_DOCUMENT')->first();
        $startFinancialYear = FinancialYear::find($financialYearId);
        $startTime = strtotime($startFinancialYear->start_date);
        $plus = "+" . $minYears->value . " years";
        $endTime = strtotime($plus, $startTime);
        $endDate = date('Y-m-d', $endTime);

        $endFinancialYear = FinancialYear::where('end_date', '<=', $endDate)->orderBy('start_date', 'desc')->first();
        $refDoCId = DB::table("reference_documents")->insertGetId(array(
            "name" => $adminHierarchy->name . '___',
            "reference_document_type_id" => null,
            "document_url" => null,
            "admin_hierarchy_id" => $adminHierarchy->id,
            "start_financial_year" => $financialYearId,
            "end_financial_year" => $endFinancialYear->id,
            "is_active" => true
        ));
        return $refDoCId;

    }

    public function allFinancialYearPeriods($id)
    {
        $all = Period::where('financial_year_id', $id)->with('financial_year')->get();
        $message = ["financial_year_periods" => $all];
        return response()->json($message);
    }

    public function addFinancialYearPeriod(Request $request)
    {
        $data = json_decode($request->getContent());
        $max = Period::max('sort_order');
        $financial_year_id = $data->financial_year_id;
        $name = $data->name;
        $start_date = $data->start_date;
        $end_date = $data->end_date;
        $sort_order = $max + 1;
        $is_active = true;
        try {
            $obj = new Period();
            $obj->name = $name;
            $obj->start_date = $start_date;
            $obj->end_date = $end_date;
            $obj->financial_year_id = $financial_year_id;
            $obj->sort_order = $sort_order;
            $obj->is_active = $is_active;
            $obj->created_by = UserServices::getUser()->id;
            $obj->save();
            $all = Period::where('financial_year_id', $financial_year_id)->with('financial_year')->get();
            $message = ["financial_year_periods" => $all, "successMessage" => "PERIOD_ADDED_SUCCESSFULLY"];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete_financial_year_period($period_id, $financial_year_id)
    {
        $object = Period::find($period_id);
        $object->delete();
        $all = Period::where('financial_year_id', $financial_year_id)->with('financial_year')->get();
        $message = ["financial_year_periods" => $all, "successMessage" => "PERIOD_REMOVED_SUCCESSFULLY"];
        return response()->json($message);
    }

    private function allTrashed()
    {
        $all = FinancialYear::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed()
    {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id)
    {
        FinancialYear::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "FINANCIAL_YEAR_RESTORED", "trashedFinancialYears" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id)
    {
        FinancialYear::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "FINANCIAL_YEAR_DELETED_PERMANENTLY", "trashedFinancialYears" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash()
    {
        $trashes = FinancialYear::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "FINANCIAL_YEAR_DELETED_PERMANENTLY", "trashedFinancialYears" => $all];
        return response()->json($message, 200);
    }

    public function periods()
    {
        $financial_year = FinancialYearServices::getPlanningFinancialYear();
        $financial_year_id = $financial_year->id;
        $all = Period::where('financial_year_id', $financial_year_id)->orderBy('start_date', 'asc')->get();
        $count = Period::where('financial_year_id', $financial_year_id)->count();
        $message = ["total" => $count, "periods" => $all];
        return response()->json($message, 200);
    }

    public function currentPeriods($financialYearId)
    {

        $configuration = ConfigurationSetting::where('key', 'PLANNING_PERIODS_GROUPS')->first();
        $periodGroup = null;
        if ($configuration != null) {
            $periodGroup = PeriodGroup::find(intval($configuration->value));
            if ($periodGroup == null) {
                $periodGroup = PeriodGroup::where('number', 3)->first();
            }
        } else {
            $periodGroup = PeriodGroup::where('number', 3)->first();
        }
        $all = Period::where('financial_year_id', $financialYearId)
            ->where('period_group_id', $periodGroup->id)
            ->orderBy('start_date', 'asc')
            ->select('id', 'name')
            ->get();

        return response()->json($all, 200);
    }

    public function planningPeriods()
    {
        $configuration = ConfigurationSetting::where('key', 'PLANNING_PERIODS_GROUPS')->first();
        $periodGroup = null;
        if ($configuration != null) {
            $periodGroup = PeriodGroup::find(intval($configuration->value));
            if ($periodGroup == null) {
                $periodGroup = PeriodGroup::where('number', 3)->first();
            }
        } else {
            $periodGroup = PeriodGroup::where('number', 3)->first();
        }
        $financial_year = FinancialYearServices::getPlanningFinancialYear();
        if($financial_year == null){
            return [];
        }
        $all = Period::where('financial_year_id', $financial_year->id)->where('period_group_id', $periodGroup->id)->orderBy('start_date', 'asc')->get();
        return response()->json($all, 200);
    }

    public function nextFinancialYears(Request $request)
    {
        $start_financial_year_id = $request->id;
        $all = FinancialYear::where('id', '>=', $start_financial_year_id)->orderBy('id', 'ASC')->get();
        $data = ["years" => $all];
        return response()->json($data, 200);
    }

    public function projectionFinancialYears($start_financial_year_id, $end_financial_year_id)
    {
        $all = FinancialYear::where('id', '>=', $start_financial_year_id)->where('id', '<=', $end_financial_year_id)->orderBy('id', 'ASC')->take(5)->get();
        $data = ["years" => $all];
        return response()->json($data, 200);
    }
}
