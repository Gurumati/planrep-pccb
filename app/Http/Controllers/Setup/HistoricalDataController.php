<?php

namespace App\Http\Controllers\Setup;

use App\DTOs\SetupDTOs\AdminHierarchyDTO;
use App\DTOs\SetupDTOs\SectionDTO;
use App\Http\Services\FinancialYearServices;
use App\Models\Report\HistoricalDataItem;
use App\Models\Setup\FinancialYear;
use App\Models\Setup\GfsCode;
use App\Models\Setup\Sector;
use App\Models\Setup\Section;
use App\Models\Setup\HistoricalDataColumn;
use App\Models\Setup\HistoricalDataRow;
use App\Models\Setup\HistoricalDataTable;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Services\CustomPager;
use Illuminate\Support\Facades\Validator;

class HistoricalDataController extends Controller
{
    public function fetchAll()
    {
        $items = HistoricalDataTable::orderBy('name', 'asc')->get();
        return response()->json(['items' => $items], Response::HTTP_OK);
    }

    private function getPaginated($perPage)
    {
        return HistoricalDataTable::orderBy('name', 'asc')->paginate($perPage);
    }

    public function paginated(Request $request)
    {
        $all = $this->getPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, HistoricalDataTable::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        HistoricalDataTable::create($data);
        $all = HistoricalDataTable::orderby('name', 'desc')->paginate($request->perPage);
        $message = ["successMessage" => "CREATE_SUCCESSFUL", "items" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, HistoricalDataTable::rules($request->id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        $item = HistoricalDataTable::find($request->id);
        $item->update($data);
        $all = $this->getPaginated($request->perPage);
        $message = ["successMessage" => "UPDATE_SUCCESSFUL", "items" => $all];
        return response()->json($message, 200);
    }

    public function delete($id)
    {
        try {
            $item = HistoricalDataTable::find($id);
            $item->delete();
            $all = $this->getPaginated(Input::get('perPage'));
            $message = ["successMessage" => "DELETE_SUCCESSFUL", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $e) {
            $message = ["errorMessage" => "ERROR_DELETING"];
            return response()->json($message, 500);
        }
    }

    public function columns(Request $request)
    {
        $all = HistoricalDataColumn::where('historical_data_table_id', $request->historical_data_table_id)->get();
        return response()->json(['columns' => $all], Response::HTTP_OK);
    }

    public function addColumn(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, HistoricalDataColumn::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        HistoricalDataColumn::create($data);
        $all = HistoricalDataColumn::where('historical_data_table_id', $request->historical_data_table_id)->get();
        return response()->json(['columns' => $all, "successMessage" => "Column Added Successfully!"], Response::HTTP_OK);
    }

    public function removeColumn(Request $request)
    {
        $item = HistoricalDataColumn::find($request->id);
        $item->delete();
        $all = HistoricalDataColumn::where('historical_data_table_id', $request->historical_data_table_id)->get();
        return response()->json(['columns' => $all, "successMessage" => "Column Removed Successfully!"], Response::HTTP_OK);
    }

    public function updateDataColumn(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, HistoricalDataColumn::rules($request->id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        $item = HistoricalDataColumn::find($request->id);
        $item->update($data);
        $all = HistoricalDataColumn::where('historical_data_table_id', $request->historical_data_table_id)->get();
        return response()->json(['columns' => $all, "successMessage" => "Column Updated Successfully!"], Response::HTTP_OK);
    }

    public function dataRows(Request $request)
    {
        $financial_year = FinancialYearServices::getPlanningFinancialYear();
        $financial_year_id = $financial_year->id;
        $financial_year = FinancialYear::find($financial_year_id);
        $financial_year_name = $financial_year->name;
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $fund_source_id = $request->fund_source_id;
        $council = AdminHierarchyDTO::find($admin_hierarchy_id);
        $council_name = $council->name;
        $historical_data_table_id = $request->historical_data_table_id;
        $state = $request->state;
        $type = $request->historical_data_table_type;

        $amountArray = array();
        $all = null;

        $section_id = $request->section_id;
        //init gfs codes
        if($state == 'LOAD'){
            if($type == "REVENUE"){
                $gfs_codes = GfsCode::where("fund_source_id", $fund_source_id)->get();
            } else {
                $gfs_codes = $this->expGfsCodes($fund_source_id);
            }
            foreach ($gfs_codes as $item) {
                $exists = DB::table('historical_data_rows')
                    ->join('historical_data_items', 'historical_data_rows.id', '=', 'historical_data_items.row_id')
                    ->where('historical_data_rows.code',$item->code)
                    ->where('historical_data_rows.historical_data_table_id',$historical_data_table_id)
                    ->where('historical_data_items.financial_year_id',$financial_year_id)
                    ->where('historical_data_items.admin_hierarchy_id',$admin_hierarchy_id)
                    ->where('historical_data_items.section_id',$section_id)
                    ->where('historical_data_items.fund_source_id',$fund_source_id)
                    ->select('historical_data_rows.*')
                    ->count();
                $r = new HistoricalDataRow();
                if($exists < 1){
                    $r->name = $item->name;
                    $r->code = $item->code;
                    $r->historical_data_table_id = $historical_data_table_id;
                    $r->save();
                }
                $row_id = $r->id;
                if((int)$row_id > 0){
                    $columns = HistoricalDataColumn::where('historical_data_table_id', $historical_data_table_id)->get();
                    foreach ($columns as $column) {
                        $count = DB::table('historical_data_rows')
                            ->join('historical_data_items', 'historical_data_rows.id', '=', 'historical_data_items.row_id')
                            ->where('historical_data_rows.code',$item->code)
                            ->where('historical_data_rows.historical_data_table_id',$historical_data_table_id)
                            ->where('historical_data_items.financial_year_id',$financial_year_id)
                            ->where('historical_data_items.admin_hierarchy_id',$admin_hierarchy_id)
                            ->where('historical_data_items.section_id',$section_id)
                            ->where('historical_data_items.fund_source_id',$fund_source_id)
                            ->select('historical_data_rows.*')
                            ->count();
                        if ($count < 1) {
                            $i = new HistoricalDataItem();
                            $i->row_id = $r->id;
                            $i->column_id = $column->id;
                            $i->value = 0.00;
                            $i->financial_year_id = $financial_year_id;
                            $i->admin_hierarchy_id = $admin_hierarchy_id;
                            $i->fund_source_id = $fund_source_id;
                            $i->section_id = $section_id;
                            $i->save();
                        }
                    }
                }
            }
        }

        $department = SectionDTO::find($section_id);
        $department_name = $department->name;
        $dataItems['section_id'] = $section_id;
        $dataItems['department_name'] = $department_name;

        $sector_id = $request->sector_id;
        $sector = Sector::find($sector_id);
        $sector_name = $sector->name;
        $dataItems['sector_id'] = $sector_id;
        $dataItems['sector_name'] = $sector_name;

        $sql = "select r.id as row_id,c.id as column_id,i.value from historical_data_rows r
                      join historical_data_items i on r.id = i.row_id
                      join historical_data_columns c on c.id = i.column_id
                      where i.financial_year_id = '$financial_year_id' 
                      and i.admin_hierarchy_id = '$admin_hierarchy_id'
                      and c.historical_data_table_id = '$historical_data_table_id'
                      and i.section_id = '$section_id'
                      and i.column_id is not null and i.row_id is not null and i.fund_source_id = '$fund_source_id' ORDER BY r.id,c.id";
        $itms = DB::select($sql);
        foreach ($itms as $itm) {
            $row_id = $itm->row_id;
            $column_id = $itm->column_id;
            $amount = $itm->value ? $itm->value : '0.00';
            $amountArray[$row_id][$column_id] = $amount;
        }


        $searchString = null;
        if (isset($request->searchString)) {
            $searchString = $request->searchString;
        }

        if ($searchString != null) {
            /*$all = DB::table('historical_data_rows as r')
                ->join('historical_data_tables as t', 't.id', '=', 'r.historical_data_table_id')
                ->join('historical_data_items as i', 'i.row_id', '=', 'r.id')
                ->select('r.id', 'r.code', 'r.name')
                ->where('i.fund_source_id', $fund_source_id)
                ->where('i.section_id', $section_id)
                ->where('i.admin_hierarchy_id', $admin_hierarchy_id)
                ->where('t.id', $historical_data_table_id)
                ->where('r.code', 'ILIKE', '%' . $searchString . '%')
                ->orWhere('r.name', 'ILIKE', '%' . $searchString . '%')
                ->distinct()
                ->orderByRaw('r.code ASC')
                ->paginate($request->perPage);*/
            $sql = "select distinct r.id,r.code,r.name from historical_data_rows r
                    join historical_data_tables t on t.id = r.historical_data_table_id
                    join historical_data_items i on i.row_id = r.id
                    where i.fund_source_id = '$fund_source_id' 
                    and i.section_id = '$section_id' 
                    and i.admin_hierarchy_id = '$admin_hierarchy_id' 
                    and t.id = '$historical_data_table_id' and (r.code ilike '%$searchString%' or r.name ilike '%$searchString%')
                    order by r.code asc";
            $result = DB::select($sql);
            $all = CustomPager::paginate($result, $request->perPage);
        } else {
            /*$all = DB::table('historical_data_rows as r')
                ->join('historical_data_tables as t', 't.id', '=', 'r.historical_data_table_id')
                ->join('historical_data_items as i', 'i.row_id', '=', 'r.id')
                ->select('r.id', 'r.code', 'r.name')
                ->where('i.fund_source_id', $fund_source_id)
                ->where('i.section_id', $section_id)
                ->where('i.admin_hierarchy_id', $admin_hierarchy_id)
                ->where('t.id', $historical_data_table_id)
                ->distinct()
                ->orderByRaw('r.code ASC')
                ->paginate($request->perPage);*/

            $sql = "select distinct r.id,r.code,r.name from historical_data_rows r
                    join historical_data_tables t on t.id = r.historical_data_table_id
                    join historical_data_items i on i.row_id = r.id
                    where i.fund_source_id = '$fund_source_id' 
                    and i.section_id = '$section_id' 
                    and i.admin_hierarchy_id = '$admin_hierarchy_id' 
                    and t.id = '$historical_data_table_id'
                    order by r.code asc";
            $result = DB::select($sql);
            $all = CustomPager::paginate($result, $request->perPage);
        }

        $dataItems['financial_year_id'] = $financial_year_id;
        $dataItems['admin_hierarchy_id'] = $admin_hierarchy_id;
        $dataItems['council'] = $council_name;
        $dataItems['financial_year_name'] = $financial_year_name;
        $dataItems['values'] = $amountArray;

        $columns = HistoricalDataColumn::where('historical_data_table_id', $historical_data_table_id)->get();
        return response()->json(['items' => $all, 'columns' => $columns, 'dataItems' => $dataItems], Response::HTTP_OK);
    }

    public function saveRowValues(Request $request)
    {
        $historical_data_table_id = $request->historical_data_table_id;
        $type = $request->historical_data_table_type;
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $fund_source_id = $request->fund_source_id;
        $council = AdminHierarchyDTO::find($admin_hierarchy_id);
        $council_name = $council->name;
        $financial_year = FinancialYearServices::getPlanningFinancialYear();
        $financial_year_id = $financial_year->id;
        $financial_year_name = $financial_year->name;

        $section_id = $request->section_id;
        $department = Section::find($section_id);
        $department_name = $department->name;
        $dataItems['section_id'] = $section_id;
        $dataItems['department_name'] = $department_name;

        $sector_id = $request->sector_id;
        $sector = Sector::find($sector_id);
        $sector_name = $sector->name;
        $dataItems['sector_id'] = $sector_id;
        $dataItems['sector_name'] = $sector_name;

        $values = $request->values;
        foreach ($values as $row => $columns) {
            $row_id = $row;
            foreach ($columns as $column => $value) {
                $column_id = $column;
                $amount = $value;
                $query = HistoricalDataItem::where('row_id', $row_id)
                    ->where('column_id', $column_id)
                    ->where('financial_year_id', $financial_year_id)
                    ->where('admin_hierarchy_id', $admin_hierarchy_id);
                if ($query->count() == 1) {
                    $itm = $query->first();
                } else {
                    $itm = new HistoricalDataItem();
                }
                $itm->row_id = $row_id;
                $itm->column_id = $column_id;
                $itm->value = $amount;
                $itm->financial_year_id = $financial_year_id;
                $itm->admin_hierarchy_id = $admin_hierarchy_id;
                $itm->fund_source_id = (isset($fund_source_id)) ? $fund_source_id : null;
                $itm->section_id = (isset($request->section_id)) ? $request->section_id : null;
                $itm->save();
            }
        }

        $searchString = null;
        if (isset($request->searchString)) {
            $searchString = $request->searchString;
        }

        if ($searchString != null) {
            /*$all = DB::table('historical_data_rows as r')
                ->join('historical_data_tables as t', 't.id', '=', 'r.historical_data_table_id')
                ->join('historical_data_items as i', 'i.row_id', '=', 'r.id')
                ->select('r.id', 'r.code', 'r.name')
                ->where('i.fund_source_id', $fund_source_id)
                ->where('i.section_id', $section_id)
                ->where('i.admin_hierarchy_id', $admin_hierarchy_id)
                ->where('t.id', $historical_data_table_id)
                ->where('r.code', 'ILIKE', '%' . $searchString . '%')
                ->orWhere('r.name', 'ILIKE', '%' . $searchString . '%')
                ->distinct()
                ->orderByRaw('r.code ASC')
                ->paginate($request->perPage);*/
            $sql = "select distinct r.id,r.code,r.name from historical_data_rows r
                    join historical_data_tables t on t.id = r.historical_data_table_id
                    join historical_data_items i on i.row_id = r.id
                    where i.fund_source_id = '$fund_source_id' 
                    and i.section_id = '$section_id' 
                    and i.admin_hierarchy_id = '$admin_hierarchy_id' 
                    and t.id = '$historical_data_table_id' and (r.code ilike '%$searchString%' or r.name ilike '%$searchString%')
                    order by r.code asc";
            $result = DB::select($sql);
            $all = CustomPager::paginate($result, $request->perPage);
        } else {
            /*$all = DB::table('historical_data_rows as r')
                ->join('historical_data_tables as t', 't.id', '=', 'r.historical_data_table_id')
                ->join('historical_data_items as i', 'i.row_id', '=', 'r.id')
                ->select('r.id', 'r.code', 'r.name')
                ->where('i.fund_source_id', $fund_source_id)
                ->where('i.admin_hierarchy_id', $admin_hierarchy_id)
                ->where('t.id', $historical_data_table_id)
                ->distinct()
                ->orderByRaw('r.code ASC')
                ->paginate($request->perPage);*/

            $sql = "select distinct r.id,r.code,r.name from historical_data_rows r
                    join historical_data_tables t on t.id = r.historical_data_table_id
                    join historical_data_items i on i.row_id = r.id
                    where i.fund_source_id = '$fund_source_id' 
                    and i.section_id = '$section_id' 
                    and i.admin_hierarchy_id = '$admin_hierarchy_id' 
                    and t.id = '$historical_data_table_id'
                    order by r.code asc";
            $result = DB::select($sql);
            $all = CustomPager::paginate($result, $request->perPage);
        }

        $dataItems['financial_year_id'] = $financial_year_id;
        $dataItems['council'] = $council_name;
        $dataItems['financial_year_name'] = $financial_year_name;

        $sql = "select r.id as row_id,c.id as column_id,i.value from historical_data_rows r
                      left join historical_data_items i on r.id = i.row_id
                      left join historical_data_columns c on c.id = i.column_id
                      where i.financial_year_id = '$financial_year_id' 
                      and i.admin_hierarchy_id = '$admin_hierarchy_id'
                      and c.historical_data_table_id = '$historical_data_table_id'
                      and i.section_id = '$section_id'
                      and i.column_id is not null and i.row_id is not null and i.fund_source_id = '$fund_source_id'";
        $itms = DB::select($sql);

        $amountArray = array();
        foreach ($itms as $itm) {
            $row_id = $itm->row_id;
            $column_id = $itm->column_id;
            $amount = $itm->value;
            $amountArray[$row_id][$column_id] = $amount;
        }
        $dataItems['values'] = $amountArray;
        $columns = HistoricalDataColumn::where('historical_data_table_id', $historical_data_table_id)->get();
        return response()->json(['items' => $all, 'columns' => $columns, 'dataItems' => $dataItems, 'successMessage' => 'Data saved successfully!'], Response::HTTP_OK);
    }

    private function expGfsCodes($fund_source_id)
    {
        $financial_year = FinancialYearServices::getPlanningFinancialYear();
        $financial_year_id = $financial_year->id;
        $sql = "select DISTINCT g.name,g.code from gfs_codes g
                join activity_facility_fund_source_inputs a ON g.id = a.gfs_code_id
                join activity_facility_fund_sources af on af.id = a.activity_facility_fund_source_id
                join fund_sources f on f.id = af.fund_source_id
                join activity_facilities acf on acf.id = af.activity_facility_id
                join activities ac on ac.id = acf.activity_id
                join mtef_sections ms on ms.id = ac.mtef_section_id
                join mtefs m on m.id = ms.mtef_id
                where f.id = '$fund_source_id' and m.financial_year_id = '$financial_year_id'";
        $all = DB::select($sql);
        return $all;
    }
}
