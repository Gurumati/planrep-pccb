<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Budgeting\ExpenditureCentreValue;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\ExpenditureCentre;
use App\Models\Setup\ExpenditureCentreLink;
use App\Models\Setup\ExpenditureCentreSection;
use App\Models\Setup\ExpenditureCentreTaskNature;
use App\Models\Setup\FinancialYear;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ExpenditureCentreController extends Controller
{
    public function index()
    {
        $all = ExpenditureCentre::with('link_spec', 'expenditure_centre_group', 'section', 'sections', 'parent', 'activity_category', 'lga_level')->orderBy('created_at', 'desc')->get();
        return response()->json($all);
    }

    public function fetchAll()
    {
        $all = ExpenditureCentre::with('link_spec', 'expenditure_centre_group', 'section', 'sections', 'parent', 'activity_category', 'lga_level')->orderBy('created_at', 'desc')->get();
        return response()->json(["expenditureCentres" => $all], 200);
    }

    public function getAllPaginated()
    {
        return ExpenditureCentre::with('link_spec', 'expenditure_centre_group', 'section', 'sections', 'parent', 'activity_category', 'lga_level')->orderBy('created_at', 'desc')->get();
    }

    public function paginated()
    {
        $all = $this->getAllPaginated();
        return response()->json($all);
    }

    public function store(Request $request)
    {
        $expenditure_centre_group_id = $request->group_id;
        $data = json_decode($request->getContent());
        $obj = new ExpenditureCentre();
        $obj->name = $data->name;
        $obj->code = $data->code;
        $obj->expenditure_centre_group_id = $expenditure_centre_group_id;
        $spec = $data->link_spec;
        $obj->link_spec_id = $spec->id;
        $obj->parent_id = (isset($data->parent_id)) ? $data->parent_id : null;
        $obj->activity_category_id = (isset($data->activity_category_id)) ? $data->activity_category_id : null;
        $obj->lga_level_id = (isset($data->lga_level_id)) ? $data->lga_level_id : null;

        $obj->save();

        //COST CENTRES
        if (isset($data->sections)) {
            foreach ($data->sections as $s) {
                DB::table('bdc_expenditure_centre_sections')->insert(
                    array(
                        "bdc_expenditure_centre_id" => $obj->id,
                        "section_id" => $s->id
                    )
                );
            }
        }
        //TASK NATURES
        if (isset($data->task_natures)) {
            foreach ($data->task_natures as $s) {
                DB::table('bdc_expenditure_centre_task_natures')->insert(
                    array(
                        "expenditure_centre_id" => $obj->id,
                        "task_nature_id" => $s->id
                    )
                );
            }
        }

        $expenditure_centre_id = $obj->id;
        if(isset($value->min_value) && isset($data->max_value)){
            $financial_year = FinancialYearServices::getPlanningFinancialYear();
            $financial_year_id = $financial_year->id;
            $v = new ExpenditureCentreValue();
            $v->min_value = $data->min_value;
            $v->max_value = $data->max_value;
            $v->financial_year_id = $financial_year_id;
            $v->expenditure_centre_id = $expenditure_centre_id;
            $v->save();
        }

        //LINK WITH GFS CODES
        if (isset($data->gfs_code_id)) {
            $gfs_code_array = $data->gfs_code_id;
            for ($i = 0; $i < count($gfs_code_array); $i++) {
                $g = new ExpenditureCentreLink();
                $g->gfs_code_id = $gfs_code_array[$i];
                $g->expenditure_centre_id = $expenditure_centre_id;
                $g->save();
            }
        }

        $all = $this->groupExpenditureCentres($expenditure_centre_group_id);
        $message = ["successMessage" => "SUCCESSFUL_EXPENDITURE_CENTRE_CREATED", "expenditureCentres" => $all];
        return response()->json($message, 200);
    }

    public function groupExpenditureCentres($group_id)
    {
        $all = ExpenditureCentre::with('link_spec', 'expenditure_centre_group', 'section', 'parent', 'activity_category', 'lga_level')->where('expenditure_centre_group_id', $group_id)->orderBy('created_at', 'desc')->get();
        return $all;
    }

    public function update(Request $request)
    {
        $data = json_decode($request->getContent());

        $obj = ExpenditureCentre::find($data->id);
        $obj->name = $data->name;
        $obj->code = $data->code;
        $obj->expenditure_centre_group_id = $data->expenditure_centre_group_id;
        $obj->link_spec_id = $data->link_spec_id;
        $obj->parent_id = (isset($data->parent_id)) ? $data->parent_id : null;
        $obj->activity_category_id = (isset($data->activity_category_id)) ? $data->activity_category_id : null;
        $obj->lga_level_id = (isset($data->lga_level_id)) ? $data->lga_level_id : null;
        $obj->save();

        $all = $this->getAllPaginated();
        $message = ["successMessage" => "SUCCESSFUL_EXPENDITURE_CENTRE_UPDATED", "expenditureCentres" => $all];
        return response()->json($message, 200);
    }

    public function delete($id)
    {
        $obj = ExpenditureCentre::find($id);
        $obj->delete();
        $all = $this->getAllPaginated();
        $message = ["successMessage" => "SUCCESSFUL_EXPENDITURE_CENTRE_DELETED", "expenditureCentres" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed()
    {
        $all = ExpenditureCentre::with('link_spec', 'expenditure_centre_group', 'section', 'parent', 'activity_category', 'lga_level')
            ->orderBy('created_at', 'desc')
            ->onlyTrashed()->get();
        return $all;
    }

    public function trashed()
    {
        $all = $this->allTrashed();
        return response()->json(["trashedExpenditureCentres" => $all], 200);
    }

    public function restore($id)
    {
        ExpenditureCentre::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "EXPENDITURE_CENTRE_RESTORED", "trashedExpenditureCentres" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id)
    {
        ExpenditureCentre::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "EXPENDITURE_CENTRE_DELETED_PERMANENTLY", "trashedExpenditureCentres" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash()
    {
        $trashes = ExpenditureCentre::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EXPENDITURE_CENTRE_DELETED_PERMANENTLY", "trashedExpenditureCentres" => $all];
        return response()->json($message, 200);
    }

    public function links(Request $request)
    {
        $expenditure_centre_id = $request->id;
        $all = $this->getAllLinks($expenditure_centre_id);
        $data = ["links" => $all];
        return response()->json($data, 200);
    }

    public function getAllLinks($id)
    {
        $all = ExpenditureCentreLink::with('gfs_code')->where('expenditure_centre_id', $id)->get();
        return $all;
    }

    public function addLink(Request $request)
    {
        $data = json_decode($request->getContent());
        try {
            $count = ExpenditureCentreLink::where('gfs_code_id',$data->gfs_code_id)
                ->where('expenditure_centre_id',$data->expenditure_centre_id)->count();
            if($count < 1){
                $obj = new ExpenditureCentreLink();
                $obj->gfs_code_id = $data->gfs_code_id;
                $obj->expenditure_centre_id = $data->expenditure_centre_id;
                $obj->save();
            }
            $all = $this->getAllLinks($data->expenditure_centre_id);
            $message = ["links" => $all, "successMessage" => "CREATE_SUCCESS"];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function removeLink(Request $request)
    {
        $id = $request->id;
        $expenditure_centre_id = $request->expenditure_centre_id;
        $object = ExpenditureCentreLink::find($id);
        $object->delete();
        $all = $this->getAllLinks($expenditure_centre_id);
        $message = ["links" => $all, "successMessage" => "REMOVE_SUCCESS"];
        return response()->json($message);
    }

    public function values(Request $request)
    {
        $expenditure_centre_id = $request->id;
        $all = $this->getValues($expenditure_centre_id);
        $data = ["values" => $all];
        return response()->json($data, 200);
    }

    public function getValues($id)
    {
        $all = ExpenditureCentreValue::with('financial_year')->where('expenditure_centre_id', $id)->get();
        return $all;
    }

    public function addValue(Request $request)
    {
        $data = json_decode($request->getContent());
        $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
        try {
            $obj = new ExpenditureCentreValue();
            $obj->min_value = $data->min_value;
            $obj->max_value = $data->max_value;
            $obj->financial_year_id = $financial_year_id;
            $obj->expenditure_centre_id = $data->expenditure_centre_id;
            $obj->save();
            $all = $this->getValues($data->expenditure_centre_id);
            $message = ["values" => $all, "successMessage" => "CREATE_SUCCESS"];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function removeValue(Request $request)
    {
        $id = $request->id;
        $expenditure_centre_id = $request->expenditure_centre_id;
        $object = ExpenditureCentreValue::find($id);
        $object->delete();
        $all = $this->getValues($expenditure_centre_id);
        $message = ["links" => $all, "successMessage" => "REMOVE_SUCCESS"];
        return response()->json($message);
    }

    public function setUserExpenditureCentreActualValue()
    {
        $userExpenditureCentres = ExpenditureCentre::getUserExpenditureCentres(1);
        foreach ($userExpenditureCentres as $exp) {
            if ($exp->link_spec_id === ConfigurationSetting::getValueByKey('TASK_NATURE_LINK_SPEC')) {

            } elseif ($exp->link_spec_id === ConfigurationSetting::getValueByKey('ACTIVITY_CATEGORY_LINK_SPEC')) {

            } elseif ($exp->link_spec_id === ConfigurationSetting::getValueByKey('LGA_LEVEL_LINK_SPEC')) {

            } elseif ($exp->link_spec_id === ConfigurationSetting::getValueByKey('SECTION_LINK_SPEC')) {

            }
        }
    }

    public function taskNatures(Request $request)
    {
        $all = ExpenditureCentreTaskNature::with('task_nature')->where('expenditure_centre_id', $request->expenditure_centre_id)->get();
        $message = ["taskNatures" => $all];
        return response()->json($message);
    }

    public function addTaskNature(Request $request)
    {
        $expenditure_centre_id = $request->expenditure_centre_id;
        if (isset($request->task_natures)) {
            foreach ($request->task_natures as $s) {
                $task_nature_id = $s['id'];
                $exists = ExpenditureCentreTaskNature::where('expenditure_centre_id',$expenditure_centre_id)
                    ->where('task_nature_id',$task_nature_id)->count();
                if($exists < 1){
                    DB::table('bdc_expenditure_centre_task_natures')->insert(
                        array(
                            "expenditure_centre_id" => $expenditure_centre_id,
                            "task_nature_id" => $task_nature_id
                        )
                    );
                }
            }
            $all = ExpenditureCentreTaskNature::with('task_nature')->where('expenditure_centre_id', $expenditure_centre_id)->get();
            $message = ["taskNatures" => $all, "successMessage" => "CREATE_SUCCESS"];
            return response()->json($message, 200);
        } else {
            $all = ExpenditureCentreTaskNature::with('task_nature')->where('expenditure_centre_id', $expenditure_centre_id)->get();
            $message = ["taskNatures" => $all, "errorMessage" => "Select Task Nature please"];
            return response()->json($message, 200);
        }
    }

    public function removeTaskNature(Request $request)
    {
        $id = $request->id;
        $expenditure_centre_id = $request->expenditure_centre_id;
        $object = ExpenditureCentreTaskNature::find($id);
        $object->delete();
        $all = ExpenditureCentreTaskNature::with('task_nature')->where('expenditure_centre_id', $expenditure_centre_id)->get();
        $message = ["taskNatures" => $all, "successMessage" => "REMOVE_SUCCESS"];
        return response()->json($message);
    }

    public function costCentres(Request $request)
    {
        $expenditure_centre_id = $request->expenditure_centre_id;
        $all = ExpenditureCentreSection::with('cost_centre')->where('bdc_expenditure_centre_id', $expenditure_centre_id)->get();
        $message = ["costCentres" => $all];
        return response()->json($message);
    }

    public function addCostCentre(Request $request)
    {
        $expenditure_centre_id = $request->expenditure_centre_id;
        if (isset($request->sections)) {
            foreach ($request->sections as $s) {
                $section_id = $s['id'];
                $exists = ExpenditureCentreSection::where('bdc_expenditure_centre_id',$expenditure_centre_id)
                    ->where('section_id',$section_id)->count();
                if($exists < 1){
                    DB::table('bdc_expenditure_centre_sections')->insert(
                        array(
                            "bdc_expenditure_centre_id" => $expenditure_centre_id,
                            "section_id" => $section_id
                        )
                    );
                }
            }
            $all = ExpenditureCentreSection::with('cost_centre')->where('bdc_expenditure_centre_id', $expenditure_centre_id)->get();
            $message = ["costCentres" => $all, "successMessage" => "CREATE_SUCCESS"];
            return response()->json($message, 200);
        } else {
            $all = ExpenditureCentreSection::with('cost_centre')->where('bdc_expenditure_centre_id', $expenditure_centre_id)->get();
            $message = ["costCentres" => $all, "errorMessage" => "Select Cost Centre please"];
            return response()->json($message, 200);
        }
    }

    public function removeCostCentre(Request $request)
    {
        $id = $request->id;
        $expenditure_centre_id = $request->expenditure_centre_id;
        $object = ExpenditureCentreSection::find($id);
        $object->delete();
        $all = ExpenditureCentreSection::with('cost_centre')->where('bdc_expenditure_centre_id', $expenditure_centre_id)->get();
        $message = ["costCentres" => $all,"successMessage" => "DELETE_SUCCESS"];
        return response()->json($message);
    }

}
