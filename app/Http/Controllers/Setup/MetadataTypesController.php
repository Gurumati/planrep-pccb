<?php

namespace App\Http\Controllers\Setup;

use App\Models\Setup\MetadataType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class MetadataTypesController extends Controller {

    public function index() {
        $items = MetadataType::with("source")
            ->orderBy("metadata_type_name", "asc")
            ->get();
        return customApiResponse($items, "Success", 200);
    }

    private function paginate() {
        if (Input::get('perPage') && Input::get('perPage') > 0) {
            $perPage = Input::get('perPage');
        } else {
            $perPage = 10;
        }
        return MetadataType::with("source")
            ->orderBy("metadata_type_name", "asc")
            ->paginate($perPage);
    }

    public function paged() {
        $items = $this->paginate();
        return customApiResponse($items, "Success", 200);
    }

    public function store(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, MetadataType::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            return customApiResponse("", "Failed", 400, $errors->all());
        } else {
            MetadataType::create($data);
            $items = $this->paginate();
            return customApiResponse($items, "Metadata Type Created Successfully!", 201);
        }
    }

    public function update(Request $request) {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, MetadataType::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            return customApiResponse("", "Failed", 400, $errors->all());
        } else {
            $obj = MetadataType::find($request->id);
            $obj->update($data);
            $items = $this->paginate();
            return customApiResponse($items, "Metadata Type Updated", 201);
        }
    }

    public function delete($id) {
        $item = MetadataType::find($id);
        if ($item->items()->count() > 0) {
            return customApiResponse("", "Metadata Type already in use!", 400);
        } else {
            $item->delete();
            $items = $this->paginate();
            return customApiResponse($items, "Metadata Type Deleted Successfully", 200);
        }
    }
}
