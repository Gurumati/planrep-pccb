<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\BdcMainGroup;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Validator;

class BdcMainGroupController extends Controller {

    public function index() {
        $all = BdcMainGroup::with('sector')->orderBy('created_at', 'desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return BdcMainGroup::with('sector')->orderBy('created_at', 'desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, BdcMainGroup::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            $user_id = UserServices::getUser()->id;
            $sector_id = DB::table('sectors')
                ->join('sections', 'sectors.id', '=', 'sections.sector_id')
                ->join('users', 'sections.id', '=', 'users.section_id')
                ->select('sectors.id')
                ->where('users.id', $user_id)
                ->first()->id;
            $v = new BdcMainGroup();
            $v->name = $request->name;
            $v->sector_id = $sector_id;
            $v->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "bdcMainGroups" => $all];
            return response()->json($message, 200);
        }
    }

    public function update(Request $request) {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, BdcMainGroup::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all()];
            return response()->json($message, 500);
        } else {
            $obj = BdcMainGroup::find($request->id);
            $obj->update($data);
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "bdcMainGroups" => $all];
            return response()->json($message, 200);
        }
    }

    public function delete($id) {
        try {
            $obj = BdcMainGroup::find($id);
            $obj->delete();
            $all = $this->getAllPaginated(Input::get('perPage'));
            $message = ["successMessage" => "SUCCESSFUL_BDC_MAIN_GROUP_DELETED", "bdcMainGroups" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $message = ["errorMessage" => "DATA_IN_USE"];
            return response()->json($message, 500);
        }
    }

    private function allTrashed() {
        $all = BdcMainGroup::with('sector')
            ->orderBy('created_at', 'desc')
            ->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        BdcMainGroup::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "BDC_MAIN_GROUP_RESTORED", "trashedBdcMainGroups" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        BdcMainGroup::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "BDC_MAIN_GROUP_DELETED_PERMANENTLY", "trashedBdcMainGroups" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = BdcMainGroup::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "BDC_MAIN_GROUP_DELETED_PERMANENTLY", "trashedBdcMainGroups" => $all];
        return response()->json($message, 200);
    }
}
