<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\ActivityStatus;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;


class ActivityStatusController extends Controller
{
    public function index() {
        $all = ActivityStatus::all();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return ActivityStatus::orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $activityStatus = new ActivityStatus();
            $activityStatus->name = $data->name;
            $activityStatus->position = $data->position;
            $activityStatus->is_default = true;
            $activityStatus->created_by = UserServices::getUser()->id;
            $activityStatus->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "ACTIVITY_STATUS_CREATED_SUCCESSFULLY", "activityStatuses" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $activityStatus = ActivityStatus::find($data->id);
            $activityStatus->name = $data->name;
            $activityStatus->position = $data->position;
            $activityStatus->is_default = true;
            $activityStatus->created_by = UserServices::getUser()->id;
            $activityStatus->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "ACTIVITY_STATUS_UPDATED_SUCCESSFULLY", "activityStatuses" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function toggleActivityStatus(Request $request) {
        //
        $data = json_decode($request->getContent());
        $activityStatus = ActivityStatus::find($data->id);
        $activityStatus->is_default = $data->is_default;
        $activityStatus->save();
        $activity = $this->getAllPaginated($request->perPage);
        if ($data->is_default == false) {
            $feedback = ["action" => "ACTIVITY_STATUS_DEACTIVATED", "alertType" => "warning", "facilities" => $activity];
        } else {
            $feedback = ["action" => "ACTIVITY_STATUS_ACTIVATED", "alertType" => "success", "facilities" => $activity];
        }
        return response()->json($feedback, 200);
    }

    public function delete($id) {
        $obj = ActivityStatus::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "ACTIVITY_STATUS_DELETED_SUCCESSFULLY", "activityStatuses" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = ActivityStatus::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        ActivityStatus::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "ACTIVITY_STATUS_RESTORED", "trashedActivityStatuses" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        ActivityStatus::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "ACTIVITY_STATUS_DELETED_PERMANENTLY", "trashedActivityStatuses" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = ActivityStatus::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "ACTIVITY_STATUS_DELETED_PERMANENTLY", "trashedActivityStatuses" => $all];
        return response()->json($message, 200);
    }
}
