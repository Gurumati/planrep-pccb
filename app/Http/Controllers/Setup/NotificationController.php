<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\Notification;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class NotificationController extends Controller
{
    public function index() {
        $all = Notification::all();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return Notification::orderby('created_at')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }


    public function store(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = new Notification();
            $obj->message = $data->message;
            $obj->type = $data->type;
            $obj->email = $data->email;
            $obj->status = $data->status;
            $obj->sent_at = Carbon::now();
            $obj->last_trial = Carbon::now();
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "Notifications" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }


    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = Notification::find($data->id);
            $obj->message = $data->message;
            $obj->type = $data->type;
            $obj->email = $data->email;
            $obj->status = $data->status;
            $obj->sent_at = Carbon::now();
            $obj->last_trial = Carbon::now();
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "Notifications" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = Notification::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "Notifications" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = Notification::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        $message = ["trashedNotifications" => $all];
        return response()->json($message);
    }

    public function restore($id) {
        Notification::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedNotifications" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        Notification::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedNotifications" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = Notification::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedNotifications" => $all];
        return response()->json($message, 200);
    }
}
