<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\TargetType;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TargetTypeController extends Controller {
    public function index() {
        $targetTypes = TargetType::where('is_active', true)->get();
        return response()->json($targetTypes);
    }
    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $target_type = new TargetType();
            $target_type->name = $data->name;
            $target_type->code = $data->code;
            $target_type->description = $data->description;
            $target_type->is_active = true;
            $target_type->sort_order = $data->sort_order;
            $target_type->created_by = UserServices::getUser()->id;
            $target_type->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_TARGET_TYPE_CREATED", "targetTypes" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $target_type = TargetType::find($data->id);
            $target_type->name = $data->name;
            $target_type->code = $data->code;
            $target_type->description = $data->description;
            $target_type->sort_order = $data->sort_order;
            $target_type->update_by = UserServices::getUser()->id;
            $target_type->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_TARGET_TYPE_UPDATED", "targetTypes" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function getAllPaginated($perPage) {
        return TargetType::orderBy('created_at','desc')->paginate($perPage);
    }

    public function delete($id) {
        $target_type = TargetType::find($id);
        $target_type->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_TARGET_TYPE_DELETED", "targetTypes" => $all];
        return response()->json($message, 200);
    }

    public function toggleTargetType(Request $request) {
        $data = json_decode($request->getContent());
        $object = TargetType::find($data->id);
        $object->is_active = $data->is_active;
        $object->save();
        $all = $this->getAllPaginated($request->perPage);
        if ($data->is_active == false) {
            $feedback = ["action" => "TARGET_TYPE_DEACTIVATED", "alertType" => "warning", "targetTypes" => $all];
        } else {
            $feedback = ["action" => "TARGET_TYPE_ACTIVATED", "alertType" => "success", "targetTypes" => $all];
        }
        return response()->json($feedback, 200);
    }
}
