<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\GenericSectorProblem;
use App\Models\Setup\GenericTarget;
use App\Models\Setup\PriorityArea;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class GenericSectorProblemController extends Controller
{
    public function index()
    {
        $all = GenericSectorProblem::with('priority_area', 'planning_matrix')->orderBy('created_at', 'asc')->get();
        $data = ["genericSectorProblems" => $all];
        return response()->json($data);
    }

    public function getAllPaginated($perPage)
    {
        return GenericSectorProblem::with('priority_area', 'planning_matrix')->orderBy('created_at', 'asc')->paginate($perPage);
    }

    public function paginated(Request $request)
    {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function getByPriorityArea($priorityAreaId)
    {
        $genericSectorProblems = GenericSectorProblem::where('priority_area_id', $priorityAreaId)->get();
        $data = ["genericSectorProblems" => $genericSectorProblems];
        return response()->json($data);
    }

    public function store(Request $request)
    {
        try {
            $data = json_decode($request->getContent());

            $count = GenericSectorProblem::with('priority_area')->where('priority_area_id', $data->priority_area_id)->count();
            $last_auto_id = $count + 1;
            $pa = PriorityArea::find($data->priority_area_id);
            $priority_area_number = $pa->number;
            $standard_number = str_pad(($last_auto_id), 2, 0, STR_PAD_LEFT);

            $obj = new GenericSectorProblem();
            $obj->code = $priority_area_number . "" . $standard_number;
            $obj->description = (isset($data->description)) ? $data->description : null;
            $obj->priority_area_id = $data->priority_area_id;
            $obj->params = $data->params;
            $obj->planning_matrix_id = $data->planning_matrix_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_number = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_number . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request)
    {
        $data = json_decode($request->getContent());
        try {
            $obj = GenericSectorProblem::find($data->id);
            $obj->description = (isset($data->description)) ? $data->description : null;
            $obj->priority_area_id = $data->priority_area_id;
            $obj->params = $data->params;
            $obj->planning_matrix_id = $data->planning_matrix_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_number = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_number . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id)
    {
        $obj = GenericSectorProblem::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed()
    {
        $all = GenericSectorProblem::with('priority_area', 'planning_matrix')->orderBy('created_at', 'asc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed()
    {
        $all = $this->allTrashed();
        $message = ["trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function restore($id)
    {
        GenericSectorProblem::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id)
    {
        GenericSectorProblem::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash()
    {
        $trashes = GenericSectorProblem::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function toggleActive(Request $request)
    {
        $data = json_decode($request->getContent());
        $object = GenericSectorProblem::find($data->id);
        $object->is_active = $data->is_active;
        $object->save();
        $all = $this->getAllPaginated($request->perPage);
        if ($data->is_active == false) {
            $feedback = ["action" => "DEACTIVATED", "alertType" => "warning", "items" => $all];
        } else {
            $feedback = ["action" => "ACTIVATED", "alertType" => "success", "items" => $all];
        }
        return response()->json($feedback, 200);
    }

    public function genericTargets(Request $request)
    {
        $generic_sector_problem_id = $request->generic_sector_problem_id;
        $all = GenericTarget::where('generic_sector_problem_id', $generic_sector_problem_id)->orderBy('code', 'asc')->get();
        $response = ["genericTargets" => $all];
        return response()->json($response, 200);
    }

    public function generateCodes()
    {
        $items = GenericSectorProblem::with('priority_area')->orderBy('id', 'asc')->get();
        foreach ($items as $item) {
            $priority_area_code = $item->priority_area->number;
            $number = $priority_area_code . "" . str_pad(($item->id), 2, 0, STR_PAD_LEFT);
            DB::table('generic_sector_problems')->where('id', $item->id)->update(['code' => $number]);
        }
    }
}
