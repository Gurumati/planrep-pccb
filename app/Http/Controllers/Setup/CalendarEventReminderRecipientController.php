<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\CalendarEventReminderRecipient;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

/**
 * @resource CalendarEventReminderRecipient
 * CalendarEventReminderRecipientController: This controller manage Calendar event receipt
*/
class CalendarEventReminderRecipientController extends Controller {

    /**
     * index()
     * This method return all Calendar Remainder receipt
    */
    public function index() {
        $all = CalendarEventReminderRecipient::with('calendar_event')->orderBy('created_at','desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return CalendarEventReminderRecipient::with('calendar_event')->orderBy('created_at','desc')->paginate($perPage);
    }
    /**
     * paginate()
     * This method return  Calendar Remainder in pages
     */
    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }
    /**
     * store()
     * This method create new  Calendar Remainder Receipt
    */
    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new CalendarEventReminderRecipient();
            $obj->calendar_event_id = $data->calendar_event_id;
            $obj->email = $data->email;
            $obj->mobile_number = $data->mobile_number;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "eventRecipients" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }
    /**
     * update()
     * This method update existing  Calendar Remainder Receipt
    */
    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = CalendarEventReminderRecipient::find($data->id);
            $obj->calendar_event_id = $data->calendar_event_id;
            $obj->email = $data->email;
            $obj->mobile_number = $data->mobile_number;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "eventRecipients" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }
    /**
     * delete()
     * This method soft delete existing  Calendar Remainder Receipt
    */
    public function delete($id) {
        $obj = CalendarEventReminderRecipient::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "eventRecipients" => $all];
        return response()->json($message, 200);
    }
    /**
     * allTrashed()
     * This method return all trashed Calendar Remainder Receipt
    */
    private function allTrashed() {
        $all = CalendarEventReminderRecipient::with('calendar_event')->orderBy('created_at','desc')->onlyTrashed()->get();
        return $all;
    }
    /**
     * trashed()
     * This method return all trashed Calendar Remainder Receipt
     *
    */
    public function trashed() {
        $all = $this->allTrashed();
        $message = ["trashedEventRecipients" => $all];
        return response()->json($message,200);
    }
    /**
     * restore()
     * This method restore Calendar Remainder Receipt from trash
    */
    public function restore($id) {
        CalendarEventReminderRecipient::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedEventRecipients" => $all];
        return response()->json($message, 200);
    }
    /**
     * permanentDelete()
     * This method permanent delete Calendar Remainder Receipt
    */
    public function permanentDelete($id) {
        CalendarEventReminderRecipient::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedEventRecipients" => $all];
        return response()->json($message, 200);
    }
    /**
     * emptyTrash()
     * This method permanent delete all Calendar Remainder Receipt from trash
    */
    public function emptyTrash() {
        $trashes = CalendarEventReminderRecipient::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedEventRecipients" => $all];
        return response()->json($message, 200);
    }
}
