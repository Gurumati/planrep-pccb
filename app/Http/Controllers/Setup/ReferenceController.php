<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\Reference;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;


class ReferenceController extends Controller
{

    public function index(Request $request)
    {
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'NR');
        $all = Reference::whereHas('reference_type.reference_type_versions',function ($version) use($versionId){
            $version->where('version_id',$versionId);
        })->where('is_active', true)
            ->orderBy('created_at', 'asc')->get();
        return response()->json($all);
    }

    public function fetchAll(Request $request)
    {
        $financialYearId = $this->getFinancialYearId($request);
        $versionId = $this->getVersionId($request, $financialYearId, 'NR');
        $all = Reference::whereHas('reference_type.reference_type_versions',function ($version) use($versionId){
            $version->where('version_id',$versionId);
        })->where('is_active', true)
            ->orderBy('created_at', 'asc')->get();
        $data = ["nationalReferences" => $all];
        return response()->json($data);
    }

    public function store(Request $request)
    {
        $data = array_merge($request->all(), ['is_active' => true]);
        $validator = Validator::make($data, Reference::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $response = ["errorMessage" => "Failed", "errors" => $errors->all()];
            return response()->json($response, 500);
        } else {
            Reference::create($data);
            $references = Reference::with('children')
                ->where('reference_type_id', $request->reference_type_id)
                ->whereNull('parent_id')->get();
            $response = ["successMessage" => "SUCCESSFUL_REFERENCE_CREATED", "references" => $references];
            return response()->json($response, 200);
        }
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, Reference::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $response = ["errorMessage" => "Failed", "errors" => $errors->all()];
            return response()->json($response, 500);
        } else {
            $obj = Reference::find($request->id);
            $obj->update($data);
            $references = Reference::with('children')->where('reference_type_id', $request->reference_type_id)->whereNull('parent_id')->get();
            $response = ["successMessage" => "SUCCESSFUL_REFERENCE_UPDATED", "references" => $references];
            return response()->json($response, 200);
        }
    }

    public function getByReferenceTypeId($referenceTypeId)
    {
        $references = Reference::with('children')->where('reference_type_id', $referenceTypeId)->whereNull('parent_id')->get();
        return response()->json($references);
    }

    public function getParentByReferenceTypeId($referenceTypeId, $childReferenceId)
    {
        $references = Reference::with('parent')->where('reference_type_id', $referenceTypeId)->where('id', "!=", $childReferenceId)->orderBy('name')->get();
        return response()->json($references);
    }
}
