<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Setup\Calendar;
use App\Models\Setup\CalendarEvent;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

/**
 * @resource Calendar
 * CalendarController: This Controller manage Calendars
*/
class CalendarController extends Controller {

    /**
     * index()
     * This method return all Calendars
    */
    public function index() {
        $all = Calendar::with('calendar_event','financial_year','hierarchy_level','sector')->orderBy('created_at','desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage,$financialYearId) {
        if(is_null($financialYearId))
         $calender=Calendar::with('calendar_event','financial_year','hierarchy_level','sector')->WhereHas('financial_year',function ($q){
            $q->whereIn('status',[1,2,3])->orderBy('status','desc');
        })->paginate($perPage);
        else{
            $calender=Calendar::with('calendar_event','financial_year','hierarchy_level','sector')->WhereHas('financial_year',function ($q) use($financialYearId){
                $q->where('id',$financialYearId)->orderBy('status','desc');
            })->paginate($perPage);
        }
        return $calender;
    }
    /**
     * paginated()
     * This method return Calendars by page
     */
    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage,$request->financialYearId);
        return response()->json($all);
    }

    /**
     * store()
     * This method create new Calendar
    */
    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new Calendar();
            $obj->description = isset($data->description)?$data->description:null;
            $obj->financial_year_id = $data->financial_year_id;
            $obj->start_date = $data->start_date;
            $obj->end_date = $data->end_date;
            $obj->calendar_event_id = $data->calendar_event->id;
            $obj->section_level_id = isset($data->section_level_id)?$data->section_level_id:null;
            $obj->hierarchy_position = $data->hierarchy_position;
            $obj->sector_id =isset($data->sector_id)?$data->sector_id:null;
            $obj->before_start_reminder_sms = isset($data->before_start_reminder_sms)?$data->before_start_reminder_sms:null;
            $obj->before_end_reminder_sms = isset($data->before_end_reminder_sms)?$data->before_end_reminder_sms:null;
            $obj->before_start_reminder_days = isset($data->before_start_reminder_days)?$data->before_start_reminder_days:null;
            $obj->before_end_reminder_days = isset($data->before_end_reminder_days)?$data->before_end_reminder_days:null;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage,$request->financialYearId);
            $message = ["successMessage" => "CREATE_CALENDER_SUCCESS", "calendars" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 400);
        }
    }

    /**
     * update()
     * This method update existing Calendar
    */
    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = Calendar::find($data->id);
            $obj->description = isset($data->description)?$data->description:null;
            $obj->financial_year_id = $data->financial_year_id;
            $obj->start_date = $data->start_date;
            $obj->end_date = $data->end_date;
            $obj->calendar_event_id = $data->calendar_event->id;
            $obj->section_level_id = isset($data->section_level_id)?$data->section_level_id:null;
            $obj->hierarchy_position = $data->hierarchy_position;
            $obj->sector_id =isset($data->sector_id)?$data->sector_id:null;
            $obj->before_start_reminder_sms = isset($data->before_start_reminder_sms)?$data->before_start_reminder_sms:null;
            $obj->before_end_reminder_sms = isset($data->before_end_reminder_sms)?$data->before_end_reminder_sms:null;
            $obj->before_start_reminder_days = isset($data->before_start_reminder_days)?$data->before_start_reminder_days:null;
            $obj->before_end_reminder_days = isset($data->before_end_reminder_days)?$data->before_end_reminder_days:null;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage,$request->financialYearId);
            $message = ["successMessage" => "UPDATE_CALENDER_SUCCESS", "calendars" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 400);
        }
    }

    public function getRespectiveCalendar($event_number, $financial_year, $hierarchy_position) {
        $event_true = false;
        $calendarEvent = CalendarEvent::where('number',$event_number)->first();
        $calendarEventId = $calendarEvent->id;

        $calendar_count = Calendar::where('calendar_event_id',$calendarEventId)
            ->where('financial_year_id',$financial_year)
            ->where('hierarchy_position',$hierarchy_position)
            ->where('start_date','>=',date('Y-m-d H:i:s'))
            ->where('end_date','<=',date('Y-m-d H:i:s'))
            ->count();
        if($calendar_count > 0)
            $event_true = true;

        return $event_true;
    }

    public static function getRespectiveSectorCalendar($event_number, $financial_year, $hierarchy_position, $sector) {
        $event_true = false;
        $calendarEvent = CalendarEvent::where('number',$event_number)->first();
        $calendarEventId = $calendarEvent->id;

        $calendar_count = Calendar::where('calendar_event_id',$calendarEventId)
            ->where('financial_year_id',$financial_year)
            ->where('hierarchy_position',$hierarchy_position)
            ->where('sector_id',$sector)
            ->where('start_date','<=',date('Y-m-d H:i:s'))
            ->where('end_date','>=',date('Y-m-d H:i:s'))
            ->count();

        if($calendar_count > 0)
            $event_true = true;

        return $event_true;

    }

    public static function getIsCalendar()
    {
        $financialYear =  FinancialYearServices::getPlanningFinancialYear();

        $fy_id = $financialYear->id;

        $user_id = UserServices::getUser()->id;
        $user = User::find($user_id);
        $admin_hierarchy = AdminHierarchy::find($user->admin_hierarchy_id);
        $admin_hierarchy_level = AdminHierarchyLevel::find($admin_hierarchy->admin_hierarchy_level_id);
        $hierarchy_position = $admin_hierarchy_level->hierarchy_position;

        return CalendarController::getRespectiveCalendar('01',$fy_id,$hierarchy_position);
    }
    /**
     * delete()
     * This method delete existing Calendar
     */
    public function delete(Request $request,$id) {
        $obj = Calendar::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'),$request->financialYearId);
        $message = ["successMessage" => "DELETE_SUCCESS", "calendars" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = Calendar::with('calendar_event','financial_year','hierarchy_level','sector')->orderBy('created_at','desc')->onlyTrashed()->get();
        return $all;
    }
    /**
     * trashed()
     * This method return all trashed Calendars
     */
    public function trashed() {
        $all = $this->allTrashed();
        $message = ["trashedCalendars" => $all];
        return response()->json($message);
    }
    /**
     * restore()
     * This method restore Calendar from a trash
    */
    public function restore($id) {
        Calendar::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedCalendars" => $all];
        return response()->json($message, 200);
    }
    /**
     * permanentDelete()
     * This method soft delete an existing Calendar
    */
    public function permanentDelete($id) {
        Calendar::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedCalendars" => $all];
        return response()->json($message, 200);
    }
    /**
     * emptyTrash()
     * This method permanent delete Calendar from trash
    */
    public function emptyTrash() {
        $trashes = Calendar::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedCalendars" => $all];
        return response()->json($message, 200);
    }

    public function getUserTodos(Request $request){
        $todos = Calendar::getUserTodos(3);
        return response()->json(['todos'=>$todos], 200);
    }
}
