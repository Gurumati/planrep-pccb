<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\Planning\PlanningServices;
use App\Http\Services\UserServices;
use App\Http\Services\VersionServices;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\BudgetClassVersion;
use App\Models\Setup\Section;
use App\Models\Setup\Sector;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class BudgetClassController extends Controller
{
    public function index(Request $request)
    {
        $versionId = $request->versionId;
        if(!isset($request->versionId)){
            $financialYear = FinancialYearServices::getPlanningFinancialYear();
            $financialYearId = $financialYear->id;
            $versionId = VersionServices::getVersionId($financialYearId,'BC');
        }
        $budgetClasses = $this->parentBudgetClasses($versionId);
        return response()->json($budgetClasses);
    }

    public function copy(Request $request)
    {
        $sourceVersionId = $request->sourceVersionId;
        $destinationVersionId = $request->destinationVersionId;
        $budgetClasses = $this->parentBudgetClasses($sourceVersionId);
        foreach ($budgetClasses as $data) {
            DB::transaction(function () use ($data, $sourceVersionId, $destinationVersionId) {
                $budgetClass = new BudgetClass();
                $budgetClass->name = $data->name;
                $budgetClass->description = $data->description;
                $budgetClass->sort_order = $data->sort_order;
                $budgetClass->code = $data->code;
                $budgetClass->is_active = isset($data->is_active) ? $data->is_active : false;
                $budgetClass->is_automatic = isset($data->is_active) ? $data->is_active : false;
                $budgetClass->parent_id = null;
                $budgetClass->save();
                $id = $budgetClass->id;

                $bv = new BudgetClassVersion();
                $bv->version_id = $destinationVersionId;
                $bv->budget_class_id = $id;
                $bv->save();

                $item['id'] = $id;
                $item['code'] = $bv->code;
                $item['description'] = $bv->name;
                $item['deletedAt'] = null;
                try {
                    $this->send($item, "SUBBUDGETCLASSES");
                } catch (\Exception $exception) {
                    Log::error($exception);
                }

                if (isset($data->is_automatic)) {
                    foreach ($data->budgetClassTemplates as $t) {
                        DB::table('budget_class_templates')->insert(array(
                            'code' => $t->code,
                            'type' => $t->type,
                            'description' => $t->description,
                            'budget_class_id' => $id,
                        ));
                    }
                }

                foreach ($data->children as $child) {
                    $budgetClass = new BudgetClass();
                    $budgetClass->name = $child->name;
                    $budgetClass->description = $child->description;
                    $budgetClass->sort_order = $child->sort_order;
                    $budgetClass->code = $child->code;
                    $budgetClass->is_active = isset($child->is_active) ? $child->is_active : false;
                    $budgetClass->is_automatic = isset($child->is_automatic) ? $child->is_automatic : false;
                    $budgetClass->parent_id = $id;
                    $budgetClass->save();
                    $budget_class_id = $budgetClass->id;

                    $bv = new BudgetClassVersion();
                    $bv->version_id = $destinationVersionId;
                    $bv->budget_class_id = $budget_class_id;
                    $bv->save();

                    $item['id'] = $budget_class_id;
                    $item['code'] = $bv->code;
                    $item['description'] = $bv->name;
                    $item['deletedAt'] = null;
                    try {
                        $this->send($item, "SUBBUDGETCLASSES");
                    } catch (\Exception $exception) {
                        Log::error($exception);
                    }

                    if (isset($child->is_automatic)) {
                        foreach ($child->budgetClassTemplates as $t) {
                            DB::table('budget_class_templates')->insert(array(
                                'code' => $t->code,
                                'type' => $t->type,
                                'description' => $t->description,
                                'budget_class_id' => $budget_class_id,
                            ));
                        }
                    }
                }
            });
        }
        return apiResponse(201, "Budget classes copied successfully", [], 'true', []);
    }

    public function parentBudget($childId, $financialYearId, $versionId)
    {
        $budgetClasses = BudgetClass::whereNull('parent_id')
            ->where('id', '<>', $childId)
            ->whereHas("budget_class_versions", function ($query) use ($versionId) {
                $query->where('version_id', $versionId);
            })
            ->select('id', 'name', 'parent_id')->get();
        return response()->json($budgetClasses);
    }

    public function fetchAll(Request $request)
    {
        $budgetClasses = BudgetClass::where('is_active', true)
            ->whereHas("budget_class_versions", function ($query) use ($request) {
                $query->where('version_id', $request->versionId);
            })->get();
        return response()->json(["budgetClasses" => $budgetClasses], 200);
    }

    public function subBudgetClasses()
    {
        $budgetClasses = BudgetClass::where('parent_id', '!=', null)->get();
        return response()->json($budgetClasses);
    }

    public function childrenBudgetClasses(Request $request)
    {
        try {
            $financialYear = FinancialYearServices::getPlanningFinancialYear();
            if(!isset($financialYear)){
                $financialYear = FinancialYearServices::getExecutionFinancialYear();
            }
            $financialYearId = $financialYear->id;
            $versionId = $this->getVersionId($request, $financialYearId, 'BC');

            $budgetClasses = DB::select("select bc.*,fyv.version_id,fyv.financial_year_id from budget_classes bc
            join budget_class_versions v on bc.id = v.budget_class_id join versions v2 on v.version_id = v2.id
            join financial_year_versions fyv on v2.id = fyv.version_id
            where v2.id = $versionId and fyv.financial_year_id = $financialYearId and bc.parent_id is not null");
            return apiResponse(200, "Success", $budgetClasses, true, []);
        } catch (\Exception $exception) {
            return apiResponse(500, "error", $exception, true,[]);
        }
    }

    public function planningBudgetClasses()
    {
      //  try{
            $planningService = new PlanningServices();
            $section_id = UserServices::getUser()->section_id;
            $section = Section::find($section_id);
            $sector = Sector::find($section->sector_id);
            $sector_id = isset($sector) ? $sector->id : null;
            $budgetClasses = $planningService->getPlanningBudgetClasses($sector_id);
            return response()->json($budgetClasses);
        // }catch (\Exception $exception){
        //     return response()->json(['errorMessage'=>$exception->getMessage()],500);
        // }
    }

    public function store(Request $request)
    {
        $data = json_decode($request->getContent());
        DB::transaction(function () use ($data) {
            $userID = UserServices::getUser()->id;
            $budgetClass = new BudgetClass();
            $budgetClass->name = $data->name;
            $budgetClass->description = is_null($data)?'':$data->description;
            $budgetClass->sort_order = $data->sort_order;
            $budgetClass->code = $data->code;
            $budgetClass->is_active = True;
            $budgetClass->is_automatic = isset($data->is_automatic) ? $data->is_automatic : false;
            $budgetClass->created_by = $userID;
            $budgetClass->parent_id = (isset($data->parent_id)) ? $data->parent_id : null;
            $budgetClass->save();
            $id = $budgetClass->id;

            $bv = new BudgetClassVersion();
            $bv->version_id = $data->versionId;
            $bv->budget_class_id = $id;
            $bv->save();

            $item['id'] = $id;
            $item['code'] = $data->code;
            $item['description'] = $data->name;
            $item['deletedAt'] = null;
            try {
                $this->send($item, "SUBBUDGETCLASSES");
            } catch (\Exception $exception) {
                Log::error($exception);
            }

            if (isset($data->is_automatic)) {
                foreach ($data->budget_class_templates as $t) {
                    DB::table('budget_class_templates')->insert(array(
                        'code' => $t->code,
                        'type' => $t->type,
                        'description' => $t->description,
                        'budget_class_id' => $budgetClass->id,
                    ));
                }
            }
        });
        $message = ["successMessage" => "SUCCESSFUL_BUDGET_CLASS_CREATED", "budgetClasses" => $this->parentBudgetClasses($request->versionId)];
        return response()->json($message, 200);
    }

    public function update(Request $request)
    {
        $data = json_decode($request->getContent());
        try {
            DB::transaction(function () use ($data) {
                $userID = UserServices::getUser()->id;
                $budgetClass = BudgetClass::find($data->id);
                $budgetClass->name = $data->name;
                $budgetClass->description = $data->description;
                $budgetClass->code = isset($data->code) ? $data->code : null;
                $budgetClass->is_automatic = isset($data->is_automatic) ? $data->is_automatic : false;
                $budgetClass->update_by = $userID;
                $budgetClass->parent_id = (isset($data->parent_id)) ? $data->parent_id : null;
                $budgetClass->save();

                $id = $data->id;
                $item['id'] = $id;
                $item['code'] = $data->code;
                $item['description'] = $data->name;
                $item['deletedAt'] = null;
                try {
                    $this->send($item, "SUBBUDGETCLASSES");
                } catch (\Exception $exception) {
                    Log::error($exception);
                }

                DB::table('budget_class_templates')->where('budget_class_id', $budgetClass->id)->delete();
                if (isset($data->is_automatic)) {
                    foreach ($data->budget_class_templates as $t) {
                        if (isset($t->code) and isset($t->type)) {
                            DB::table('budget_class_templates')->insert(array(
                                'code' => $t->code,
                                'type' => $t->type,
                                'description' => $t->description,
                                'budget_class_id' => $budgetClass->id,
                            ));
                        }
                    }
                }
            });
            //Return language success key and all data
            $message = ["successMessage" => "SUCCESSFUL_BUDGET_CLASS_UPDATED", "budgetClasses" => $this->parentBudgetClasses($request->versionId)];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id)
    {
        $versionId = Input::get('versionId');
        DB::table('budget_class_templates')->where('budget_class_id', $id)->delete();
        $budgetClass = BudgetClass::find($id);
        $budgetClass->delete();

        $b = BudgetClass::withTrashed()
            ->where('id', $id)
            ->first();
        $item['id'] = $id;
        $item['code'] = $b->code;
        $item['description'] = $b->name;
        $item['deletedAt'] = $b->deleted_at;
        try {
            $this->send($item, "SUBBUDGETCLASSES");
        } catch (\Exception $exception) {
            Log::error($exception);
        }

        $message = ["successMessage" => "SUCCESSFULLY_BUDGET_CLASS_DELETED", "budgetClasses" => $this->parentBudgetClasses($versionId)];
        return response()->json($message, 200);
    }

    public function allBudgetClasses()
    {
        $budgetClasses = BudgetClass::all();
        return response()->json($budgetClasses);
    }

    public function parents()
    {
        $budgetClasses = BudgetClass::whereNull('parent_id')->get();
        return response()->json($budgetClasses);
    }

    public function subs($id)
    {
        $budgetClasses = BudgetClass::where('parent_id', $id)->get();
        $data = ['subBudgetClasses' => $budgetClasses];
        return response()->json($data, 200);
    }

    /**
     * @param Request $request
     * @return BudgetClass[]|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function parentBudgetClasses($versionId)
    {
        return BudgetClass::with('children', 'budgetClassTemplates')
            ->whereHas("budget_class_versions", function ($query) use ($versionId) {
                $query->where('version_id', $versionId);
            })->whereNull('parent_id')
            ->get();
    }
}
