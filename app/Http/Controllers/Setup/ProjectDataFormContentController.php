<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Models\Setup\ProjectDataFormContent;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ProjectDataFormContentController extends Controller
{
    public function index($form_id){
        $contents = ProjectDataFormContent::with('projectDataForm')
            ->with('children.children')
            ->where('project_data_form_id',$form_id)
            ->whereNull('parent_id')
            ->get();
        return response()->json($contents);
    }

    public function listFormContents($form_id){
        $contents = ProjectDataFormContent::where('project_data_form_id',(int)$form_id)
                  ->where('is_filled',true)->get();
        return response()->json($contents);
    }

    public function indexAll($form_id){
        $contents = ProjectDataFormContent::with('projectDataForm')
            ->with('children')
            ->where('project_data_form_id',$form_id)
            ->where('is_filled',true)
            ->get();
        return response()->json($contents);
    }

    public function store(Request $request){
        try {
            $data = json_decode($request->getContent());
            $contents = new ProjectDataFormContent();
            $contents->project_data_form_id = $data->project_data_form_id;
            $contents->description = $data->description;
            $contents->is_filled = isset($data->is_filled) ? $data->is_filled : null;
            $contents->is_lowest = isset($data->is_lowest) ? $data->is_lowest : null;
            $contents->sort_order = isset($data->sort_order) ? $data->sort_order : null;
            $contents->parent_id = isset($data->parent_id) ? $data->parent_id : null;
            $contents->created_by = isset($data->created_by) ? $data->created_by : null;
            $contents->save();

            $all = ProjectDataFormContent::with('projectDataForm')->with('children')->where('project_data_form_id',$contents->project_data_form_id)->get();
            $message = ["successMessage" => "SUCCESSFULLY_QUESTION_CREATED", "questions" => $all];
            return response()->json($message, 200);
        }   catch (QueryException $e){
            $message = ["errorMessage" =>$e->getMessage()];
            return response()->json($message);
        }

    }

    public function update(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $contents = ProjectDataFormContent::find($data->id);
            $contents->project_data_form_id = $data->project_data_form_id;
            $contents->description = $data->description;
            $contents->is_lowest = isset($data->is_lowest) ? $data->is_lowest : null;
            $contents->is_filled = isset($data->is_filled) ? $data->is_filled : null;
            $contents->sort_order = isset($data->sort_order) ? $data->sort_order : null;
            $contents->parent_id = isset($data->parent_id) ? $data->parent_id : null;
            $contents->created_by = isset($data->created_by) ? $data->created_by : null;;
            $contents->save();

            $contents = ProjectDataFormContent::with('projectDataForm')->with('children')->where('project_data_form_id',$contents->project_data_form_id)->get();
            $message = ["successMessage" => "SUCCESSFULLY_CONTENT_UPDATED", "projectDataFormContents" => $contents];
            return response()->json($message, 200);
        }
        catch (QueryException $e){
            $message = ["errorMessage" =>$e->getMessage()];
            return response()->json($message);
        }
    }

    public function delete($id) {
        try {
            $contents = ProjectDataFormContent::find($id);
            $contents->delete();
            $all = ProjectDataFormContent::with('projectDataForm')->with('children')->where('project_data_form_id',$contents->project_data_form_id)->get();
            $message = ["successMessage" => "SUCCESSFULLY_CONTENT_DELETED", "projectDataFormContents" => $all];
            return response()->json($message, 200);
        }
        catch (QueryException $e){
            $message = ["errorMessage" =>$e->getMessage()];
            return response()->json($message);
        }
    }
}
