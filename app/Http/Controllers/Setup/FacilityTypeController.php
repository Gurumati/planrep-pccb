<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Models\Setup\FacilityType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Validator;

class FacilityTypeController extends Controller {
    public function index() {
        $facilityTypes = FacilityType::with('sections')
            ->with('lga_level')
            ->with('admin_hierarchy_level')
            ->orderBy('created_at', 'asc')
            ->get();
        return response()->json($facilityTypes);
    }

    public function fetchAll() {
        $all = FacilityType::with('sections')
            ->with('lga_level')
            ->with('admin_hierarchy_level')
            ->orderBy('created_at', 'asc')
            ->get();
        $data = ["facilityTypes" => $all];
        return response()->json($data);
    }

    public function getAllPaginated($perPage) {
        return FacilityType::with('sections','lga_level','admin_hierarchy_level')->orderBy('created_at', 'asc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, FacilityType::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(),"errorMessage" => "Form Errors"];
            return response()->json($message, 500);
        } else {
            DB::transaction(function () use ($data){
                $type = new FacilityType($data);
                $type->save();
                foreach ($data['sections'] as $section){
                    DB::table('facility_type_sections')->insert(
                        array(
                            'facility_type_id'=>$type->id,
                            'section_id'=>$section['id']
                        )
                    );
                }
            });
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "facilityTypes" => $all];
            return response()->json($message, 200);
        }
    }

    public function update(Request $request) {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, FacilityType::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(),"errorMessage" => "Form Errors"];
            return response()->json($message, 500);
        } else {


            $sectionsIds = DB::table('admin_hierarchy_sections')
            ->where('admin_hierarchy_id',$request->admin_id)->pluck('section_id')->toArray();

            DB::transaction(function () use ($data,$request,$sectionsIds) {
                $obj = FacilityType::find($request->id);
                $obj->update($data);
                
                DB::table('facility_type_sections')
                ->where('facility_type_id', $obj->id)
                ->whereIn('section_id',$sectionsIds)
                ->delete();

                $maxId = DB::table('facility_type_sections')->max('id'); 
                DB::select( DB::raw("SELECT setval(pg_get_serial_sequence('facility_type_sections' , 'id'), coalesce((max(id) + 1),1), false) FROM  facility_type_sections"));
                foreach ($data['sections'] as $section) {
                    DB::table('facility_type_sections')->insert(
                        array(
                            'facility_type_id' => $obj->id,
                            'section_id' => $section['id']
                        )
                    );
                }
            });
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "facilityTypes" => $all];
            return response()->json($message, 200);
        }
    }

    public function delete($id) {
        $facilityType = FacilityType::find($id);
        $facilityType->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFULLY_FACILITY_TYPE_DELETED", "facilityTypes" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = FacilityType::with('lga_level')
            ->with('section')
            ->orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        FacilityType::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "FACILITY_TYPE_RESTORED", "trashedFacilityTypes" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        FacilityType::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "FACILITY_TYPE_DELETED_PERMANENTLY", "trashedFacilityTypes" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = FacilityType::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "FACILITY_TYPE_DELETED_PERMANENTLY", "trashedFacilityTypes" => $all];
        return response()->json($message, 200);
    }

    public function code(Request $request) {
        if(isset($request->facility_type_id)){
            $facility_type_id = $request->facility_type_id;
            $obj = FacilityType::find($facility_type_id);
            $code = $obj->code;
            $message = ["code" => $code];
            return response()->json($message, 200);
        }
    }

    public function sector(Request $request) {
        $sector = DB::table('sectors')
            ->join('sections','sectors.id','=','sections.sector_id')
            ->join('facility_types','facility_types.section_id','=','sections.id')
            ->where('facility_types.id',$request->id)
            ->select('sectors.*')
            ->get()
            ->first();
        $code = $sector->code;
        return response()->json(['code'=>$code], 200);
    }
}
