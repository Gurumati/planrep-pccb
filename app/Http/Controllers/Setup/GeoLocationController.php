<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\GeoLocation;
use App\Models\Setup\GeoLocationLevel;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;

/**
 * @resource GeoLocation
 *
 * GeoLocationController: This Controller manage geo location e.g. national, region, district etc
 */
class GeoLocationController extends Controller
{

    /**
     * getGeoLocationChildren()
     * This method return child geo locations of an geo location. e.g districts of a specific region
     */
    public function getGeoLocationChildren($geoLocationId)
    {
        $data = DB::table('geographical_locations as g')->join('geographical_location_levels as gl', 'g.geo_location_level_id', 'gl.id')->where('g.parent_id', $geoLocationId)->where('g.is_active', true)->select('g.*', 'gl.geo_level_position')->orderBy('g.name')->get();
        return response()->json(["geographicalLocations" => $data]);
    }


    /**
     * councils()
     * This method return Councils(Tanzania specific)i.e level 3 geolocation locations for Local Government geolocation location
     */
    public function fetchAllRegions() {
        $all = DB::table('geographical_locations')
            ->join('geographical_location_levels', 'geographical_location_levels.id', '=', 'geographical_locations.geo_location_level_id')
            ->where('geographical_location_levels.geo_level_position', 2)
            ->whereNull('geographical_locations.deleted_at')
            ->select('geographical_locations.id', 'geographical_locations.name')->distinct()->orderBy("geographical_locations.name","ASC")->get();
        $data = ["regions" => $all];
        return response()->json($data);

    }

    /**
     * wards()
     * This method return Wards(Tanzania specific)i.e level 4 geolocation locations for Local Government geolocation locations
     */
    public function getCouncilFromRegion(Request $request) {
        $geo_location_id = $request->region_id;
        $all = GeoLocation::where('parent_id', $geo_location_id)->where('is_active', true)->orderBy("name","ASC")->get();
        $data = ["councils" => $all];
        return response()->json($data);
    }

    /**
     * index()
     * This method return to most geolocation location and its children geolocation location
     */
    public function index()
    {
        $geoLocation = json_decode(GeoLocation::with('children')
            ->whereNull('parent_id')->where('is_active', true)->get());
        return response(["locations" => $geoLocation]);
    }

    /**
     * store()
     * This method create a new geolocation location
     */
    public function store(Request $request)
    {
        $data = json_decode($request->getContent());
        if (isset($data->parent_id))
            $geoLocation = GeoLocation::where('name', $data->name)->where('parent_id', $data->parent_id)->get()->count();
        else
            $geoLocation = GeoLocation::where('name', $data->name)->get()->count();

        if ($geoLocation == 0) {
            try {

                $geoLocation = new GeoLocation();
                $geoLocation->name = $data->name;
                $geoLocation->code = $data->code;
                $geoLocation->geo_location_level_id = $data->geo_location_level_id;
                $geoLocation->parent_id = (isset($data->parent_id)) ? $data->parent_id : null;
                $geoLocation->sort_order = (isset($data->sort_order)) ? $data->sort_order : null;
                $geoLocation->is_active = True;
                $geoLocation->save();
                //Return language success key and all data
                $geoLocations = GeoLocation::with('children')->whereNull('parent_id')->get();
                $message = ["successMessage" => "Geographical location created successfully", "geoLocations" => $geoLocations];
                return response()->json($message, 200);
            } catch (QueryException $exception) {
                $message = ["errorMessage" => $exception];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => "Geographical location already exists"];
            return response()->json($message, 400);
        }
    }

    /**
     * update()
     * This method update an existing geographical Location
     */
    public function update(Request $request)
    {
        $data = json_decode($request->getContent());
        if (isset($data->parent_id))
            $geoLocation = GeoLocation::where('name', $data->name)->where('parent_id', $data->parent_id)->where('id', '!=', $data->id)->get()->count();
        else
            $geoLocation = GeoLocation::where('name', $data->name)->where('id', '!=', $data->id)->get()->count();

        if ($geoLocation == 0) {
            try {
                $geoLocation = GeoLocation::find($data->id);
                $geoLocation->name = $data->name;
                $geoLocation->code = $data->code;
                $geoLocation->geo_location_level_id = $data->geo_location_level_id;
                $geoLocation->parent_id = (isset($data->parent_id)) ? $data->parent_id : null;
                $geoLocation->sort_order = (isset($data->sort_order)) ? $data->sort_order : null;
                $geoLocation->is_active = True;
                $geoLocation->save();
                //Return language success key and all data
                $geoLocations = GeoLocation::with('children')->whereNull('parent_id')->get();
                $message = ["successMessage" => "Geographical location updated successfully", "geoLocations" => $geoLocations];
                return response()->json($message, 200);
            } catch (QueryException $exception) {
                $message = ["errorMessage" => $exception];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => "Geographical location exists"];
            return response()->json($message, 400);
        }
    }

    /**
     * delete()
     * This method delete  an existing geographical location return error message if geographical location to be deleted already in use
     */
    public function delete($id)
    {
        try {

            GeoLocation::find($id)->delete();
            $message = ["successMessage" => "Geographical location deleted successfully"];
            return response()->json($message, 200);
        } catch (QueryException $e) {
            $message = ["errorMessage" => "Error in deleting the geographical location"];
            return response()->json($message, 400);
        }
    }

    /**
     * loadParentGeoLocation()
     * This method return parent geolocation location level of an geolocation locations level
     */
    public function loadParentGeoLocation($childGeoLocationLevelId)
    {
        $childGeoLocationLevel = GeoLocationLevel::find($childGeoLocationLevelId);

        $parentGeoLocations = DB::table('geographical_locations')
            ->join('geographical_location_levels', 'geographical_location_levels.id', '=', 'geographical_locations.geo_location_level_id')
            ->select('geographical_locations.*')
            ->where('geo_level_position', '=', $childGeoLocationLevel->geo_level_position - 1)
            ->get();
        return response()->json(["geoLocationParents" => $parentGeoLocations], 200);
    }

    /**
     * upload()
     * This saves uploaded geolocation locations
     *
     */
    public function upload()
    {
        ini_set('max_execution_time', 3600);
        $geo_location_level_id = Input::get('get_location_level_id');
        if (Input::hasFile('file')) {
            $path = Input::file('file')->getRealPath();
            $data = Excel::selectSheets('GEOGRAPHICAL_LOCATIONS')->load($path, function ($reader) {
            })->get();
            if (!empty($data) && $data->count()) {
                $error = array();
                $error_row = array();
                $row = 2;
                foreach ($data as $key => $value) {
                    $name = $value->name;
                    $code = $value->code;
                    $parent_geo_location_code = $value->parent_geo_location_code;
                    if (is_null($name) || is_null($code) || is_null($parent_geo_location_code)) {
                        array_push($error, $value);
                        array_push($error_row, $row);
                    }
                    $row++;
                }
                $duplicates = array();
                $parentErrors = array();
                foreach ($data as $key => $value) {
                    $exists = GeoLocation::where('code', $value->code)->count();
                    if ($exists == 1) {
                        array_push($duplicates, $value->code);
                    } else {
                        $query = GeoLocation::where('code', $value->parent_geo_location_code);
                        if ($query->count() > 0) {
                            $parent = $query->first();
                            $parent_id = $parent->id;
                            $a = new GeoLocation();
                            $a->name = $value->name;
                            $a->code = $value->code;
                            $a->geo_location_level_id = $geo_location_level_id;
                            $a->parent_id = $parent_id;
                            $a->is_active = true;
                            $a->save();
                        } else {
                            array_push($parentErrors, $value->parent_geo_location_code);
                        }
                    }
                }
                $feedback = ["successMessage" => "Data uploaded successfully", "duplicates" => $duplicates, "parentErrors" => $parentErrors];
                return response()->json($feedback, 200);
            } else {
                $message = ["errorMessage" => 'selected file has not data'];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => 'Please select a file'];
            return response()->json($message, 500);
        }
    }

    public function search(Request $request)
    {
        // $searchString = $request->searchText;
        // $items = AdminHierarchy::where('name', 'ILIKE', '%' . $searchString . '%')
        //     ->orWhere('code', 'ILIKE', '%' . $searchString . '%')
        //     ->orderBy('name', 'asc')->get();
        // return response()->json(["items" => $items], Response::HTTP_OK);
    }
}
