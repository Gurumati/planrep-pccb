<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\Planning\PlanningServices;
use App\Http\Services\UserServices;
use App\Http\Services\VersionServices;
use App\Models\Budgeting\AdminHierarchyCeiling;
use App\Models\Budgeting\AdminHierarchyCeilingForward;
use App\Models\Budgeting\AdminHierarchyCeilingPeriod;
use App\Models\Budgeting\Ceiling;
use App\Models\Budgeting\CeilingSector;
use App\Models\Planning\Mtef;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\BudgetClassVersion;
use App\Models\Setup\CeilingGfsCode;
use App\Models\Setup\FundSource;
use App\Models\Setup\FundSourceVersion;
use App\Models\Setup\GfsCode;
use App\Models\Setup\Section;
use App\Models\Setup\SectionLevel;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Setup\FinancialYear;
use App\Models\Setup\FundSourceBudgetClass;
use App\Models\Setup\Version;


class FundSourceController extends Controller {
    public $fund_source_id;
    private $newCeilings = [];
    private $newAllocationCeilings = [];


    public function index() {
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $financialYearId = isset($financialYear->id) ? $financialYear->id : FinancialYearServices::getExecutionFinancialYear()->id;

        $versionId = VersionServices::getVersionId($financialYearId, 'FS');
        $fundSourceIds = FundSource::getIdsByVersion($versionId);
        $FundSources = FundSource::with(
            'childFundSources',
            'childFundSources.childFundSources',
            'fund_source_category',
            'fund_source_budget_classes',
            'funder'
        )->whereIn('fund_sources.id', $fundSourceIds)
            ->whereNull('fund_source_id')->orderBy('name')
            ->get();
        return response()->json($FundSources);
    }

    public function getAllPaginated($financialYearId, $versionId, $perPage) {
        $searchText = Input::get('searchText');
        $searchText = isset($searchText) ? "'%" . strtolower($searchText) . "%'" : "'%'";


        $fundSources = FundSource::with(
            'childFundSources',
            'childFundSources.childFundSources',
            'fund_source_category',
            'fund_source_budget_classes',
            'funder'
        )->whereHas('fund_source_versions', function ($query) use ($versionId) {
            $query->where('version_id', $versionId);
        })->where(function ($filter) use ($searchText) {
            $filter->whereRaw('LOWER(code) like ' . $searchText);
            $filter->orWhereRaw('LOWER(name) like ' . $searchText);
            $filter->orWhereRaw('LOWER(description) like ' . $searchText);
        })->orderBy('name', 'asc')->paginate($perPage);

        foreach ($fundSources as &$fundsource) {
            $gfsCodes = DB::Table('gfscode_fundsources as gfsf')->join('gfs_codes as gfs', 'gfs.id', 'gfsf.gfs_code_id')
                ->where('gfsf.fund_source_id', $fundsource->id)->get();
            $fundsource->gfs_codes = $gfsCodes;
        }

        return $fundSources;
    }

    public function paginated(Request $request) {
        $financialYearId = $request->financialYearId;
        $versionId = $request->versionId;
        $all = $this->getAllPaginated($financialYearId, $versionId, $request->perPage);
        return response()->json($all);
    }

    public function fetchAll() {
        $all = FundSource::with('fund_source_category.fund_type')->orderBy('created_at', 'asc')->get();
        return response()->json(["fundSources" => $all]);
    }

    public function returnCeilings() {
        $FundSources = FundSource::with('childFundSources', 'fund_source_budget_classes', 'fund_source_category', 'funder')->whereNull('fund_source_id')->orderBy('name')->get();
        return response()->json($FundSources);
    }

    public function allFundSources(Request $request) {

        $financialYearId = $this->getFinancialYearId($request, 'PLANNING');
        $versionId = $this->getVersionId($request, $financialYearId, 'FS');
        $FundSources = FundSource::with('childFundSources', 'fund_source_budget_classes')
            ->whereHas("fund_source_versions", function ($version) use ($versionId) {
                $version->where("version_id", $versionId);
            })->orderBy('code', "asc")->get();
        return response()->json($FundSources);
    }

    public function getByBudgetClassAndSector($financialYearId, $budgetClassId, $sectionId) {

        return ['fundSources' => FundSource::getByBudgetClassAndSector($financialYearId, $budgetClassId, $sectionId)];
    }

    public function getByMainBudgetClassAndSector($financialYearId, $fundSourceId, $sectionId) {

        return ['fundSources' => FundSource::getByMainBudgetClassAndSector($financialYearId, $fundSourceId, $sectionId)];
    }

    public function getPlanningFundSources() {
        $planningService = new PlanningServices();
        $fundSources = $planningService->getPlanningFundSources();
        return response()->json($fundSources);
    }

    public function getUserPlanningFundSources($mtefSectionId,$budgetType) {
        return response()->json(PlanningServices::getUserPlanningFundSources($mtefSectionId,$budgetType));
    }

    public function fundSourceBudgetClasses($fund_source_id) {
        $fundSourceBudgetClasses = FundSourceBudgetClass::with('budget_class')->where('fund_source_id', $fund_source_id)->get();
        return response()->json($fundSourceBudgetClasses);
    }

    public function fundSourceCeilingSectors($fund_source_id) {
        $all = DB::table('ceiling_sectors')
            ->join('ceilings', 'ceilings.id', '=', 'ceiling_sectors.ceiling_id')
            ->join('gfs_codes', 'gfs_codes.id', '=', 'ceilings.gfs_code_id')
            ->where('gfs_codes.fund_source_id', $fund_source_id)
            ->select('ceiling_sectors.*')
            ->get();
        return response()->json($all);
    }

    public function fundSourceCeilingGfs($fund_source_id) {
        $all = DB::table('ceiling_gfs_codes')
            ->join('ceilings', 'ceilings.id', '=', 'ceiling_gfs_codes.ceiling_id')
            ->join('gfs_codes', 'gfs_codes.id', '=', 'ceilings.gfs_code_id')
            ->where('gfs_codes.fund_source_id', $fund_source_id)
            ->select('ceiling_gfs_codes.*')
            ->get();
        return response()->json($all);
    }

    public function fundSourceRevenueCodes($fund_source_id) {
        $gfsCodes = GfsCode::where('fund_source_id', $fund_source_id)->get();
        return response()->json($gfsCodes);
    }

    public function store_revenue_codes(Request $request) {

        $data = json_decode($request->getContent());
        $fund_source_id = $data->fund_source_id;


        $gfsCodes = GfsCode::where('fund_source_id', $fund_source_id)->get();
        return response()->json($gfsCodes);
    }

    public function store(Request $request) {

        $data = $request->all();
        $validator = Validator::make($data, FundSource::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 500);
        }
        $count = DB::table('fund_sources as fs')
            ->join('fund_source_versions as fv', 'fs.id', 'fv.fund_source_id')
            ->where('fv.version_id', $request->versionId)
            ->where('fs.code', $request->code)
            ->count();

        if ($count > 0) {
            $message = ["errorMessage" => "Fund source code exists"];
            return response()->json($message, 400);
        }
        try {
            $userId = UserServices::getUser()->id;
            $data = json_decode($request->getContent());
            DB::transaction(function () use ($data, $userId, $request) {
                $id = DB::table('fund_sources')->insertGetId(
                    [
                        'name' => $data->name,
                        'description' => isset($data->description) ? $data->description : $data->name,
                        'fund_source_category_id' => $data->fund_source_category_id,
                        'code' => isset($data->code) ? $data->code : $data->code,
                        'fund_source_id' => (isset($data->fund_source_id)) ? $data->fund_source_id : null,
                        'is_conditional' => true,
                        'created_at' => date('Y-m-d H:i:s'),
                        'created_by' => $userId,
                        'can_project' => (isset($data->can_project)) ? $data->can_project : false,
                        'is_foreign' => (isset($data->is_foreign)) ? $data->is_foreign : false,
                    ]
                );

                $fv = new FundSourceVersion();
                $fv->fund_source_id = $id;
                $fv->version_id = $request->versionId;
                $fv->save();

                $this->fund_source_id = $id;
                foreach ($data->subBudget_classes as $subBudgetClass) {

                    $count = FundSourceBudgetClass::where('fund_source_id', $id)
                        ->where('budget_class_id', $subBudgetClass)
                        ->count();
                    if ($count < 1) {
                        $fundSourceBudgetClass = new FundSourceBudgetClass();
                        $fundSourceBudgetClass->fund_source_id = $id;
                        $fundSourceBudgetClass->budget_class_id = $subBudgetClass;
                        $fundSourceBudgetClass->created_at = date('Y-m-d H:i:s');
                        $fundSourceBudgetClass->created_by = $userId;
                        $fundSourceBudgetClass->save();
                    }
                }


                foreach ($data->gfsCodes as $gfsCode) {

                    DB::Table('gfscode_fundsources')->insert([
                        'fund_source_id' => $id,
                        'gfs_code_id' => $gfsCode->id
                    ]);

                    $fundBgts = DB::Table('fund_source_budget_classes as fsbc')
                        ->join('budget_class_versions as bcv', 'bcv.budget_class_id', 'fsbc.budget_class_id')
                        ->where('bcv.version_id', $request->bcVersion)
                        ->where('fsbc.fund_source_id', $id)
                        ->get();

                    foreach ($fundBgts as $fundBgt) {
                        $count = Ceiling::where('gfs_code_id', $gfsCode->id)
                            ->where('budget_class_id', $fundBgt->budget_class_id)
                            ->count();

                        if ($count < 1) {

                            $obj = new Ceiling();
                            $obj->name = $gfsCode->description;
                            $obj->gfs_code_id = $gfsCode->id;
                            $obj->budget_class_id = $fundBgt->budget_class_id;
                            $obj->created_by = UserServices::getUser()->id;
                            $obj->save();
                        }
                    }
                }
                if (isset($data->can_project) && $data->can_project && isset($data->allocation_subBudget_classes)) {
                    foreach ($data->allocation_subBudget_classes as $bclass) {
                        $budgetClass = BudgetClass::find($bclass);

                        $count = Ceiling::whereNull('gfs_code_id')
                            ->where('budget_class_id', $budgetClass->id)
                            ->count();

                        if ($count < 1) {

                            $obj = new Ceiling();
                            $obj->name = $budgetClass->description;
                            $obj->budget_class_id = $budgetClass->id;
                            $obj->is_aggregate = true;
                            $obj->is_active = true;
                            $obj->aggregate_fund_source_id = $id;
                            $obj->created_by = UserServices::getUser()->id;
                            $obj->save();
                        }

                        $count = FundSourceBudgetClass::where('fund_source_id', $id)
                            ->where('budget_class_id', $bclass)
                            ->count();
                        if ($count < 1) {
                            $fundSourceBudgetClass = new FundSourceBudgetClass();
                            $fundSourceBudgetClass->fund_source_id = $id;
                            $fundSourceBudgetClass->budget_class_id = $bclass;
                            $fundSourceBudgetClass->created_at = date('Y-m-d H:i:s');
                            $fundSourceBudgetClass->created_by = UserServices::getUser()->id;;
                            $fundSourceBudgetClass->save();
                        }
                    }
                }
                $this->fund_source_id = $id;
            });
            $fundSources = null;
            $ceilings = null;
            if (isset($data->can_project) && $data->can_project) {
                //
                $ceilings = DB::table('ceilings as c')
                    ->join('budget_classes as b', 'b.id', '=', 'c.budget_class_id')
                    ->where('c.is_aggregate', true)->where('aggregate_fund_source_id', $this->fund_source_id)
                    ->select('c.*', 'b.name as budget_class')
                    ->get();
                if (sizeof($ceilings) == 0) {
                    $fundSources = $this->getAllPaginated($request->financialYearId, $request->versionId, $request->perPage);
                }
            } else {
                $ceilings = $this->fundSourceVersionCeilings($this->fund_source_id, $request->bcVersion);
            }

            try {
                $fs = FundSource::with('fund_source_category.fund_type')->where('id', $this->fund_source_id)->first();
                $item['id'] = $this->fund_source_id;
                $item['code'] = $fs->code;
                $item['name'] = $fs->name;
                $item['description'] = $fs->description;
                $item['fundTypeCode'] = $fs->fund_source_category->fund_type->current_budget_code;
                $this->send($item, "FUNDSOURCES");
            } catch (\Exception $e) {
                Log::error($e);
            }

            $message = [
                "successMessage" => "SUCCESSFULLY_FUND_SOURCE_CREATED", "fundSources" => $fundSources,
                "fundSourceId" => $this->fund_source_id,
                "fundSourceCeilings" => $ceilings
            ];

            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $exception->getMessage() . ']');
            $message = ["errorMessage" =>  $exception];
            return response()->json($message, 500);
        }
    }


    private function fundSourceVersionCeilings($fundSourceId, $versionId)
    {
        $fundSourceCeilings = DB::table('ceilings as c')
            ->join('gfs_codes as g', 'g.id', '=', 'c.gfs_code_id')
            ->join('gfscode_fundsources as gfsf', 'gfsf.gfs_code_id', 'g.id')
            ->join('fund_sources as f', 'f.id', '=', 'gfsf.fund_source_id')
            ->join('fund_source_versions as fsv', 'fsv.fund_source_id', 'gfsf.fund_source_id')
            ->join('financial_year_versions as fyv', 'fyv.version_id', 'fsv.version_id')
            ->join('budget_classes as b', 'b.id', '=', 'c.budget_class_id')
            ->join('budget_class_versions as bcv', 'bcv.budget_class_id', 'b.id')
            ->where('f.id', $fundSourceId)
            ->where('bcv.version_id', $versionId)
            ->select('c.*', 'b.name as budget_class', 'bcv.*')
            ->get();
        return $fundSourceCeilings;
    }

    public function update(Request $request) {
        $data = $request->all();
        $bcVersionId = $request->bcVersion;
        $validator = Validator::make($data, FundSource::rules($request->id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }

        try {

            $userId = UserServices::getUser()->id;
            $financialYearId = $request->financialYearId;
            $data = json_decode($request->getContent());
            DB::transaction(function () use ($data, $userId, $bcVersionId, $financialYearId) {
                $id = $data->id;
                $existingFundSource = FundSource::find($id);
                DB::table('fund_sources')->where('id', $id)->update(
                    [
                        'name' => $data->name,
                        'description' => isset($data->description) ? $data->description : $data->name,
                        'fund_source_category_id' => $data->fund_source_category_id,
                        'code' => isset($data->code) ? $data->code : $data->name,
                        'is_conditional' => true,
                        'updated_at' => date('Y-m-d H:i:s'),
                        'update_by' => $userId,
                        'can_project' => (isset($data->can_project)) ? $data->can_project : false,
                        'is_foreign' => (isset($data->is_foreign)) ? $data->is_foreign : false,
                    ]
                );

                $this->fund_source_id = $id;

                $existingBudgetClasses = FundSourceBudgetClass::where('fund_source_id', $id)->get();

                if (isset($data->can_project) && $data->can_project) {
                    $toDeleteBudgetClasses = DB::table('fund_source_budget_classes')
                        ->where('fund_source_id', $id)->whereNotIn('budget_class_id', $data->allocation_subBudget_classes)->whereNotIn('budget_class_id', $data->subBudget_classes);
                    $idsTo = $toDeleteBudgetClasses->pluck('id')->toArray();
                    $deleted = DB::table('fund_source_budget_classes')->whereIN('id',  $idsTo)->delete();
                } else {
                    $toDeleteBudgetClasses = DB::table('fund_source_budget_classes')
                        ->where('fund_source_id', $id)->whereNotIn('budget_class_id', $data->subBudget_classes);
                    $idsTo = $toDeleteBudgetClasses->pluck('id')->toArray();
                    $deleted = DB::table('fund_source_budget_classes')->whereIN('id',  $idsTo)->delete();
                }

                foreach ($data->subBudget_classes as $subBudgetClass) {
                    $count = FundSourceBudgetClass::where('fund_source_id', $id)
                        ->where('budget_class_id', $subBudgetClass)
                        ->count();
                    if ($count < 1) {
                        $fundSourceBudgetClass = new FundSourceBudgetClass();
                        $fundSourceBudgetClass->fund_source_id = $id;
                        $fundSourceBudgetClass->budget_class_id = $subBudgetClass;
                        $fundSourceBudgetClass->created_at = date('Y-m-d H:i:s');
                        $fundSourceBudgetClass->created_by = $userId;
                        $fundSourceBudgetClass->save();
                    }
                }
                $newCodeIds = [];
                $existingGfs = [];
                foreach ($data->gfs_codes as $gfsCode) {
                    $newCodeIds[] = $gfsCode->id;
                    $existingGfs = DB::Table('gfscode_fundsources')->where('fund_source_id', $id)
                        ->where('gfs_code_id', $gfsCode->id)->get();
                    if (sizeof($existingGfs) <= 0) {
                        DB::Table('gfscode_fundsources')->insert([
                            'fund_source_id' => $id,
                            'gfs_code_id' => $gfsCode->id
                        ]);
                    }

                    $fundBgts = DB::Table('fund_source_budget_classes as fsbc')
                        ->join('budget_class_versions as bcv', 'bcv.budget_class_id', 'fsbc.budget_class_id')
                        ->where('bcv.version_id', $bcVersionId)
                        ->where('fsbc.fund_source_id', $id)
                        ->get();

                    foreach ($fundBgts as $fundBgt) {
                        $count = Ceiling::where('gfs_code_id', $gfsCode->id)
                            ->where('budget_class_id', $fundBgt->budget_class_id)
                            ->count();

                        $budgetClass = BudgetClass::find($fundBgt->budget_class_id);
                        if ($count < 1) {
                            $obj = new Ceiling();
                            $obj->name = $gfsCode->description;
                            $obj->gfs_code_id = $gfsCode->id;
                            $obj->budget_class_id = $fundBgt->budget_class_id;
                            $obj->created_by = UserServices::getUser()->id;
                            $obj->save();
                            $obj->budget_class = $budgetClass->name;
                            $this->newCeilings[] = $obj;
                        }
                    }
                }


                $toDeleteGfsCode = DB::table('gfs_codes as gfs')
                    ->join('gfscode_fundsources as gfsf', 'gfsf.gfs_code_id', 'gfs.id')
                    ->join('fund_source_versions as fsv', 'fsv.fund_source_id', 'gfsf.fund_source_id')
                    ->join('financial_year_versions as fyv', 'fyv.version_id', 'fsv.version_id')
                    ->where('gfsf.fund_source_id', $id)
                    ->where('fyv.financial_year_id', $financialYearId)
                    ->whereNotIn('gfs.id', $newCodeIds)->select('gfs.*');


                DB::table('gfscode_fundsources')->where('fund_source_id', $id)
                    ->whereIn('gfs_code_id', $toDeleteGfsCode->pluck('id')->toArray())
                    ->delete();

                $toDelete = $toDeleteBudgetClasses->get();

                if (sizeof($toDelete) > 0)
                    $this->cleanCeilingByBudgetClass($existingGfs,  $toDelete);
                if (sizeof($toDeleteGfsCode->get()) > 0)
                    $this->cleanCeilingByGfsCode($existingBudgetClasses, $toDeleteGfsCode->get());

                if ($existingFundSource->can_project && !$data->can_project) {
                }
                if (isset($data->can_project) && $data->can_project && isset($data->allocation_subBudget_classes)) {

                    foreach ($data->allocation_subBudget_classes as $bclass) {
                        $budgetClass = BudgetClass::find($bclass);

                        $count = Ceiling::whereNull('gfs_code_id')
                            ->where('budget_class_id', $budgetClass->id)
                            ->count();



                        if ($count < 1) {

                            $obj = new Ceiling();
                            $obj->name = $budgetClass->description;
                            $obj->budget_class_id = $budgetClass->id;
                            $obj->is_aggregate = true;
                            $obj->is_active = true;
                            $obj->aggregate_fund_source_id = $id;
                            $obj->created_by = UserServices::getUser()->id;
                            $obj->save();
                            $obj->budget_class = $budgetClass->name;
                            $this->newAllocationCeilings[] = $obj;
                        }

                        $count = FundSourceBudgetClass::where('fund_source_id', $id)
                            ->where('budget_class_id', $bclass)
                            ->count();
                        if ($count < 1) {
                            $fundSourceBudgetClass = new FundSourceBudgetClass();
                            $fundSourceBudgetClass->fund_source_id = $id;
                            $fundSourceBudgetClass->budget_class_id = $bclass;
                            $fundSourceBudgetClass->created_at = date('Y-m-d H:i:s');
                            $fundSourceBudgetClass->created_by = UserServices::getUser()->id;;
                            $fundSourceBudgetClass->save();
                        }
                    }
                    if (sizeof($toDelete) > 0) {
                        $this->cleanAggregateCeilingByBudgetClass($id, $toDelete);
                    }
                }
                $this->fund_source_id = $id;
            });
            $fundSources = null;

            if (isset($data->can_project) && $data->can_project) {
                $ceilings = $this->newAllocationCeilings;
            } else {

                $ceilings = $this->newCeilings;
            }
            if (sizeof($ceilings) == 0) {
                $fundSources = $this->getAllPaginated($request->financialYearId, $request->versionId, $request->perPage);
            }
            try {
                $fs = FundSource::with('fund_source_category.fund_type')->where('id', $this->fund_source_id)->first();
                $exportData = array();
                $item['id'] = $this->fund_source_id;
                $item['code'] = $fs->code;
                $item['name'] = $fs->name;
                $item['description'] = $fs->description;
                $item['fundTypeCode'] = $fs->fund_source_category->fund_type->current_budget_code;
                $this->send($item, "FUNDSOURCES");
            } catch (\Exception $e) {
                Log::error($e);
            }

            $message = [
                "successMessage" => "SUCCESSFULLY_FUND_SOURCE_UPDATED", "fundSources" => $fundSources,
                "fundSourceId" => $this->fund_source_id,
                "fundSourceCeilings" => $ceilings
            ];

            return response()->json($message, 200);
        } catch (QueryException $exception) {
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $exception->getMessage() . ']');
            $message = ["errorMessage" => $exception->getMessage()];
            return response()->json($message, 500);
        }

    }

    private function cleanCeilingByBudgetClass($existingGfsCode, $toDeleteBudgetClasses) {
        foreach ($existingGfsCode as $gfs) {
            foreach ($toDeleteBudgetClasses as $bc) {
                try {
                    $ceiling = DB::table('ceilings')->where('gfs_code_id', $gfs->id)
                        ->where('budget_class_id', $bc->budget_class_id)
                        ->first();

                    if ($ceiling) {
                        $usage = DB::table('admin_hierarchy_ceilings')->where('ceiling_id',$ceiling->id)->where('amount', '>', 0)->count();
                        if($usage == 0){
                            DB::table('ceiling_sectors')->where('ceiling_id', $ceiling->id)->delete();
                            DB::table('ceiling_gfs_codes')->where('ceiling_id', $ceiling->id)->delete();
                            DB::table('ceilings')->where('gfs_code_id', $gfs->id)
                            ->where('budget_class_id', $bc->budget_class_id)
                            ->delete();
                        }
                    }

                } catch (QueryException $exception) {

                }
            }
        }
    }

    private function cleanAggregateCeilingByBudgetClass($fundSourceId, $toDeleteBudgetClasses) {
        foreach ($toDeleteBudgetClasses as $bc) {
            try {
                $ceiling = DB::table('ceilings')->where('aggregate_fund_source_id', $fundSourceId)
                    ->where('budget_class_id', $bc->budget_class_id)
                    ->first();
                if ($ceiling) {
                    $usage = DB::table('admin_hierarchy_ceilings')->where('ceiling_id',$ceiling->id)->where('amount', '>', 0)->count();
                    if($usage == 0){
                        DB::table('ceiling_sectors')->where('ceiling_id', $ceiling->id)->delete();
                        DB::table('ceiling_gfs_codes')->where('ceiling_id', $ceiling->id)->delete();
                        DB::table('ceilings')->where('aggregate_fund_source_id', $fundSourceId)
                        ->where('budget_class_id', $bc->budget_class_id)
                        ->delete();
                    }
                }

            } catch (QueryException $exception) {

            }
        }
    }

    private function cleanCeilingByGfsCode($existingBudgetClasses, $toDeleteGfsCode) {
        foreach ($existingBudgetClasses as $bc) {
            foreach ($toDeleteGfsCode as $gfs) {
                try {
                    $ceiling = DB::table('ceilings')->where('gfs_code_id', $gfs->id)
                        ->where('budget_class_id', $bc->budget_class_id)
                        ->first();
                    if ($ceiling) {
                        $usage = DB::table('admin_hierarchy_ceilings')->where('ceiling_id',$ceiling->id)->where('amount', '>', 0)->count();
                        if($usage == 0){
                            DB::table('ceiling_sectors')->where('ceiling_id', $ceiling->id)->delete();
                            DB::table('ceiling_gfs_codes')->where('ceiling_id', $ceiling->id)->delete();
                            DB::table('ceilings')->where('gfs_code_id', $gfs->id)
                            ->where('budget_class_id', $bc->budget_class_id)
                            ->delete();
                        }
                    }

                } catch (QueryException $exception) {

                }
            }
        }
    }

    private function fundSourceCeilings($fundSourceId) {
        $fundSourceCeilings = DB::table('ceilings as c')
            ->join('gfs_codes as g', 'g.id', '=', 'c.gfs_code_id')
            ->join('fund_sources as f', 'f.id', '=', 'g.fund_source_id')
            ->join('budget_classes as b', 'b.id', '=', 'c.budget_class_id')
            ->where('f.id', $fundSourceId)
            ->select('c.*', 'b.name as budget_class')
            ->get();
        return $fundSourceCeilings;
    }

    public function edit_sectors(Request $request) {
        $data = json_decode($request->getContent());
        $userId = UserServices::getUser()->id;
        $fundSourceId = 0;
        $status = DB::transaction(function () use ($data, $userId) {
            foreach ($data->sectors as $sector) {

                $this->fund_source_id = $sector->fund_source_id;
            }

            $gfsList = GfsCode::where('fund_source_id', $this->fund_source_id)->get();
            foreach ($gfsList as $gfs) {
                $ceilings = Ceiling::with('gfs_code_id', $gfs->id)->get();
                foreach ($ceilings as $ceiling) {
                    CeilingSector::where('ceiling_id', $ceiling->id)->delete();
                    CeilingGfsCode::where('ceiling_id', $ceiling->id)->delete();
                }
            }

            foreach ($data->sectors as $sector) {

                $count = CeilingSector::where('ceiling_id', $sector->ceiling_id)
                    ->where('sector_id', $sector->sector_id)
                    ->count();
                if ($count < 1) {

                    $this->fund_source_id = $sector->fund_source_id;
                    $ceilingSector = new CeilingSector();
                    $ceilingSector->sector_id = $sector->sector_id;
                    $ceilingSector->ceiling_id = $sector->ceiling_id;
                    $ceilingSector->created_at = date('Y-m-d H:i:s');
                    $ceilingSector->created_by = $userId;
                    $ceilingSector->save();
                }
            }

            foreach ($data->gfs as $gfs) {
                $count = CeilingGfsCode::where('ceiling_id', $gfs->ceiling_id)
                    ->where('gfs_code_id', $gfs->gfs_id)
                    ->count();
                if ($count < 1) {
                    $ceilingGfs = new CeilingGfsCode();
                    $ceilingGfs->gfs_code_id = $gfs->gfs_id;
                    $ceilingGfs->ceiling_id = $gfs->ceiling_id;
                    $ceilingGfs->is_inclusive = $gfs->is_inclusive;
                    $ceilingGfs->created_at = date('Y-m-d H:i:s');
                    $ceilingGfs->created_by = $userId;
                    $ceilingGfs->save();
                }
            }
        });


        $FundSources = FundSource::with('childFundSources')->whereNull('fund_source_id')->get();
        $message = ["successMessage" => "SUCCESSFUL_FUND_SOURCE_UPDATED", "fundSources" => $FundSources];
        return response()->json($message, 200);
    }

    public function store_sectors(Request $request) {
        $data = json_decode($request->getContent());
        $userId = UserServices::getUser()->id;
        $this->fund_source_id = $data->fund_source_id;

        $status = DB::transaction(function () use ($data, $userId) {
            foreach ($data->ceilings as $ceiling) {

                foreach ($ceiling->sectors as $sector) {
                    $ceilingSector = new CeilingSector();
                    $ceilingSector->sector_id = $sector;
                    $ceilingSector->ceiling_id = $ceiling->id;
                    $ceilingSector->created_at = date('Y-m-d H:i:s');
                    $ceilingSector->created_by = $userId;
                    $ceilingSector->save();
                }
                foreach ($ceiling->gfs_codes as $gfs) {
                    $ceilingGfs = new CeilingGfsCode();
                    $ceilingGfs->gfs_code_id = $gfs->gfs_code->id;
                    $ceilingGfs->ceiling_id = $ceiling->id;
                    $ceilingGfs->is_inclusive = isset($gfs->is_inclusive) ? $gfs->is_inclusive : false;
                    $ceilingGfs->created_at = date('Y-m-d H:i:s');
                    $ceilingGfs->created_by = $userId;
                    $ceilingGfs->save();
                }
            }
        });

        $FundSources = FundSource::with('childFundSources')->whereNull('fund_source_id')->orderBy('name')->get();
        $message = ["successMessage" => "SUCCESSFUL_FUND_SOURCE_UPDATED", "fundSources" => $FundSources];
        return response()->json($message, 200);
    }

    public function delete($id, $financialYearId, $versionId) {

        try {
            $object = FundSource::find($id);
            $object->delete();
            $fs = FundSource::with('fund_source_category.fund_type')->where('id', $id)->first();
            $item['id'] = $id;
            $item['code'] = $fs->code;
            $item['name'] = $fs->name;
            $item['description'] = $fs->description;
            $item['fundTypeCode'] = $fs->fund_source_category->fund_type->current_budget_code;
            $item['deleted_at'] = $fs->deleted_at;
            $this->send($item, "FUNDSOURCES");
        } catch (\Exception $e) {
            Log::error($e);
            $fundSources = $this->getAllPaginated($financialYearId, $versionId, Input::get('perPage'));
            $message = ["errorMessage" => "FAILLED_TO_DELETE_FUND_SOURCE", "fundSources" => $fundSources];
            return response()->json($message, 400);
        }


        $fundSources = $this->getAllPaginated($financialYearId, $versionId, Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_FUND_SOURCE_DELEATED", "fundSources" => $fundSources];
        return response()->json($message, 200);
    }

    public function loadParentFundSources($fundSourceCategoryId) {

        $fundSources = DB::table('fund_sources')
            ->select('fund_sources.*')
            ->where('fund_source_category_id', $fundSourceCategoryId)
            ->whereNull('fund_source_id')
            ->get();
        return response()->json($fundSources, 200);
    }

    public function gfs_codes($budgetType, $financialYearId, $adminHierarchyId, $sectionId, $fundSourceId) {
        if (!Section::canProject($sectionId)) {
            return response()->json(['errorMessage' => 'This level cannot project'], 400);
        }

        $versionId = Version::getVersionByFinancialYear($financialYearId, 'BC');
        $fVersionId = Version::getVersionByFinancialYear($financialYearId, 'FS');
        $budgetClassIds = BudgetClassVersion::where('version_id', $versionId)->pluck('budget_class_id')->toArray();
        $fundSourceIds = FundSourceVersion::where('version_id', $fVersionId)->pluck('fund_source_id')->toArray();


        $revenuesSBC = DB::Table('budget_classes as bc')
            ->join('budget_class_versions as bcv', 'bcv.budget_class_id', 'bc.id')
            ->join('financial_year_versions as fyv', 'fyv.version_id', 'bcv.version_id')
            ->where('bc.code', '501')
            ->where('fyv.financial_year_id', $financialYearId)->select('bc.*')
            ->pluck('bc.id')->toArray();
        $all = null;
        $totalRevenue = 0;

        $existingGfsIds = DB::table('admin_hierarchy_ceilings as ac')
            ->join('ceilings as c', 'c.id', 'ac.ceiling_id')
            ->join('gfs_codes as g', 'g.id', 'c.gfs_code_id')
            ->whereIn('c.budget_class_id', $revenuesSBC)
            ->where('ac.admin_hierarchy_id', $adminHierarchyId)
            ->where('ac.financial_year_id', $financialYearId)
            ->where('ac.section_id', $sectionId)
            ->where('ac.budget_type', $budgetType)
            ->whereNull('ac.deleted_at')
            ->pluck('g.id')
            ->toArray();

        $existingGfsIds_ = array_filter($existingGfsIds, static function ($var) {
            return $var != null;
        });

        $gfsCodes = DB::table('gfs_codes as g')
            ->join('ceilings as c', 'c.gfs_code_id', 'g.id')
            ->join('gfscode_fundsources as gfs', 'gfs.gfs_code_id', 'g.id')
            ->whereIn('c.budget_class_id', $revenuesSBC)
            ->where('gfs.fund_source_id', $fundSourceId)
            ->whereIn('gfs.fund_source_id',$fundSourceIds)
            ->whereIn('c.budget_class_id',$budgetClassIds)
            ->where('c.is_active', true)
            ->where('g.is_active', true)
            ->where('g.code', 'like', '1%')
            ->whereNotIn('g.id', $existingGfsIds_)
            ->select('g.*', 'c.id as ceiling_id')
            ->orderBy('g.description')
            ->get();

        return response()->json(['gfs_codes' => $gfsCodes, 'existingGfsIds' => $existingGfsIds_], 200);
    }

    public function can_project() {
        $sectionId = UserServices::getUser()->section_id;
        $userSection = Section::find($sectionId);
        $minSectionPosition = SectionLevel::min('hierarchy_position');
        $topSectionLevels = SectionLevel::where('hierarchy_position', $minSectionPosition)->get();
        $userAtTopSection = false;
        $all = [];
        $count = 0;

        $fy = FinancialYearServices::getPlanningFinancialYear();
        $fyear = isset($fy) ? $fy : FinancialYearServices::getExecutionFinancialYear();
        $versionId = Version::getVersionByFinancialYear($fyear->id, 'FS');

        foreach ($topSectionLevels as $s) {
            if ($s->id == $userSection->section_level_id)
                $userAtTopSection = true;
        }
        if ($userAtTopSection) {
            $all = FundSource::where('can_project', true)
                ->join('fund_source_versions as fsv', 'fsv.fund_source_id', 'fund_sources.id')
                ->where('fsv.version_id', $versionId)
                ->select('fund_sources.*')
                ->get();
            $count = sizeof($all);
        }
        $message = ["total" => $count, "fund_sources" => $all];
        return response()->json($message, 200);
    }

    public function for_report() {
        $fund_sources = DB::table('fund_sources as fs')
        ->join('gfscode_fundsources as gfsf', 'fs.id', 'gfsf.fund_source_id')
        ->join('gfs_codes as gfs', 'gfs.id', '=', 'gfsf.gfs_code_id')
        ->select('fund_sources.*')
        ->distinct()
        ->where('fs.can_project', true)
        ->whereNull('fs.deleted_at')->get();
    $message = ["fund_sources" => $fund_sources];
    return response()->json($message, 200);
    }

    public function assignGfsCodes(Request $request) {
        $data = json_decode($request->getContent());
        $fund_source_id = $request->fund_source_id;
        $gfs_code_array = $data->gfs_code_id;
        for ($i = 0; $i < count($gfs_code_array); $i++) {
            $gfs_code_id = $gfs_code_array[$i];
            $gfs_code = GfsCode::find($gfs_code_id);
            $gfs_code->fund_source_id = $fund_source_id;
            $gfs_code->save();
        }
        $message = ["successMessage" => "GFS_CODES_ASSIGNED_SUCCESSFULLY"];
        return response()->json($message, 200);
    }

    public function canProjectGfsCodes($id) {
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $adminHierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section_id = UserServices::getUser()->section_id;

        $all = DB::table('gfs_codes')
            ->join('ceilings', 'gfs_codes.id', '=', 'ceilings.gfs_code_id')
            ->where('gfs_codes.fund_source_id', $id)
            ->where('gfs_codes.is_active', true)
            ->select('gfs_codes.*')
            ->get();
        $message = ["gfsCodes" => $all];
        return response()->json($message, 200);
    }

    public function getBudgetedByFacility($budgetType, $financialYearId, $adminHiearchyId, $sectionId, $facilityId) {
        try {
            $fundSources = FundSource::getBudgetedByFacility($budgetType, $financialYearId, $adminHiearchyId, $sectionId, $facilityId);
            $data = ['fundSources' => $fundSources];
            return response()->json($data, 200);
        } catch (\Exception $e) {
            $message = ["errorMessage" => "ERROR_FETCHING_DATA"];
            return response()->json($message, 400);
        }

    }

    public function exportToCsv(Request $request) {
        $versionId = $request->versionId;
        $sql = "select x.fund_source_code,x.fund_source, x.budget_class_code,x.budget_class, string_agg(x.sectors,'|') as sectors
            from (
            SELECT distinct f.code as fund_source_code,f.name as fund_source,c2.code as budget_class_code,
            c2.name as budget_class, se.name as sectors from fund_sources f
            join fund_source_versions fv on fv.fund_source_id = f.id
            left join fund_source_budget_classes f2 on f.id = f2.fund_source_id
            left join budget_classes c2 on f2.budget_class_id = c2.id
            left join ceilings ce on ce.budget_class_id = c2.id
            left join ceiling_sectors cs on cs.ceiling_id = ce.id
            left join sectors se on se.id = cs.sector_id
            where fv.version_id = $versionId
              and f.deleted_at is null
              and c2.deleted_at is null) x
            group by x.fund_source_code,x.fund_source, x.budget_class_code,x.budget_class";
        $items = DB::select($sql);

        $array = array();
        foreach ($items as $value) {
            $data = array();
            $data['FUND_SOURCE_CODE'] = $value->fund_source_code;
            $data['FUND_SOURCE'] = $value->fund_source;
            $data['BUDGET_CLASS_CODE'] = $value->budget_class_code;
            $data['BUDGET_CLASS'] = $value->budget_class;
            $data['SECTORS'] = $value->sectors;
            array_push($array, $data);
        }

        Excel::create('FUND_SOURCES' . time(), function ($excel) use ($array) {
            $excel->sheet('DATA', function ($sheet) use ($array) {
                $sheet->fromArray($array);
            });
        })->export('csv');
    }

    public function copy(Request $request) {
        $sourceFinancialYearId = $request->sourceFinancialYearId;
        $sourceVersionId = $request->sourceVersionId;
        $destinationVersionId = $request->destinationVersionId;
        $items = FundSource::with(
            'gfs_codes',
            'fund_source_category',
            'fund_source_budget_classes'
        )->whereHas('fund_source_versions', function ($query) use ($sourceVersionId) {
            $query->where('version_id', $sourceVersionId);
        })->get();
        DB::transaction(function () use ($items, $destinationVersionId) {
            foreach ($items as $item) {
                $f = new FundSource();
                $f->name = $item->name;
                $f->description = $item->description;
                $f->fund_source_category_id = $item->fund_source_category_id;
                $f->is_conditional = $item->is_conditional;
                $f->is_treasurer = $item->is_treasurer;
                $f->funder_id = $item->funder_id;
                $f->can_project = $item->can_project;
                $f->code = $item->code;
                $f->is_foreign = $item->is_foreign;
                $f->save();
                $fund_source_id = $f->id;

                $this->fund_source_id = $fund_source_id;

                $fv = new FundSourceVersion();
                $fv->fund_source_id = $fund_source_id;
                $fv->version_id = $destinationVersionId;
                $fv->save();

                foreach ($item->fund_source_budget_classes as $subBudgetClass) {
                    $fundSourceBudgetClass = new FundSourceBudgetClass();
                    $fundSourceBudgetClass->fund_source_id = $fund_source_id;
                    $fundSourceBudgetClass->budget_class_id = $subBudgetClass->budget_class_id;
                    $fundSourceBudgetClass->fund_type_id = $subBudgetClass->fund_type_id;
                    $fundSourceBudgetClass->save();
                }

                foreach ($item->gfs_codes as $gfsCode) {

                    $existingGFCode = GfsCode::where('id', $gfsCode->id)->first();
                    $existingGFCode->fund_source_id = $fund_source_id;
                    $existingGFCode->save();

                    $fundBgts = FundSourceBudgetClass::where('fund_source_id', $fund_source_id)->get();
                    foreach ($fundBgts as $fundBgt) {
                        $count = Ceiling::where('gfs_code_id', $existingGFCode->id)
                            ->where('budget_class_id', $fundBgt->budget_class_id)
                            ->count();

                        if ($count < 1) {

                            $obj = new Ceiling();
                            $obj->name = $gfsCode->description;
                            $obj->gfs_code_id = $existingGFCode->id;
                            $obj->budget_class_id = $fundBgt->budget_class_id;
                            $obj->created_by = UserServices::getUser()->id;
                            $obj->save();
                        }
                    }
                }

                if (isset($item->can_project) && $item->can_project && isset($item->fund_source_budget_classes)) {
                    foreach ($item->fund_source_budget_classes as $bclass) {
                        $budgetClass = BudgetClass::find($bclass->budget_class_id);

                        $count = Ceiling::whereNull('gfs_code_id')
                            ->where('budget_class_id', $budgetClass->id)
                            ->count();

                        if ($count < 1) {

                            $obj = new Ceiling();
                            $obj->name = $budgetClass->description;
                            $obj->budget_class_id = $budgetClass->id;
                            $obj->is_aggregate = true;
                            $obj->is_active = true;
                            $obj->aggregate_fund_source_id = $fund_source_id;
                            $obj->created_by = UserServices::getUser()->id;
                            $obj->save();
                        }

                        $count = FundSourceBudgetClass::where('fund_source_id', $fund_source_id)
                            ->where('budget_class_id', $bclass->budget_class_id)
                            ->count();
                        if ($count < 1) {
                            $fundSourceBudgetClass = new FundSourceBudgetClass();
                            $fundSourceBudgetClass->fund_source_id = $fund_source_id;
                            $fundSourceBudgetClass->budget_class_id = $bclass;
                            $fundSourceBudgetClass->created_at = date('Y-m-d H:i:s');
                            $fundSourceBudgetClass->created_by = UserServices::getUser()->id;;
                            $fundSourceBudgetClass->save();
                        }
                    }
                }
            }
        });
        return apiResponse(201, "Fund Sources Copied Successfully", [], true, []);
    }

    public function upload() {
        $fund_source_category_id = Input::get('fund_source_category_id');
        $financialYearId = Input::get('versionId');
        $versionId = Input::get('financialYearId');
        if (Input::hasFile('file')) {
            $path = Input::file('file')->getRealPath();
            $data = Excel::selectSheets('FUND_SOURCES')->load($path, function ($reader) {
            })->get();
            if (!empty($data) && $data->count()) {
                $error = array();
                $error_row = array();
                $row = 2;
                foreach ($data as $key => $value) {
                    $name = $value->name;
                    $code = $value->code;
                    if (is_null($name) || is_null($code)) {
                        array_push($error, $value);
                        array_push($error_row, $row);
                    }
                    $row++;
                }
                if (count($error) > 0) {
                    $all = $this->getAllPaginated($financialYearId, $versionId, 10);
                    $feedback = ["errorMessage" => 'DIRTY DATA', "errorData" => $error, "errorRows" => $error_row, "fundSources" => $all];
                    return response()->json($feedback, 500);
                } else {
                    foreach ($data as $key => $value) {
                        DB::transaction(function () use ($value, $fund_source_category_id, $versionId) {
                            $f = new FundSource();
                            $f->name = $value->name;
                            $f->code = $value->code;
                            $f->description = $value->name;
                            $f->fund_source_category_id = $fund_source_category_id;
                            $f->save();

                            $fv = new FundSourceVersion();
                            $fv->fund_source_id = $f->id;
                            $fv->version_id = $versionId;
                            $fv->save();

                            $fs = FundSource::with('fund_source_category.fund_type')->where('id', $f->id)->first();
                            $item['id'] = $fs->id;
                            $item['code'] = $fs->code;
                            $item['name'] = $fs->name;
                            $item['description'] = $fs->description;
                            $item['fundTypeCode'] = $fs->fund_source_category->fund_type->current_budget_code;
                            $this->send($item, "FUNDSOURCES");
                        });
                    }
                    $all = $this->getAllPaginated($financialYearId, $versionId, 10);
                    $message = ["successMessage" => "FUND_SOURCES_UPLOADED_SUCCESSFULLY", "fundSources" => $all];
                    return response()->json($message, 200);
                }
            } else {
                $message = ["errorMessage" => 'FILE_SPECIFIED_HAS_NO_DATA'];
                return response()->json($message, 500);
            }
        } else {
            $message = ["errorMessage" => 'PLEASE_SELECT_A_FILE'];
            return response()->json($message, 500);
        }
    }

    public function searchFundSource(Request $request) {
        $searchText = $request->searchText;
        $financialYearId = $request->financialYearId;
        $versionId = $request->versionId;

        $all = FundSource::with(
            'childFundSources',
            'childFundSources.childFundSources',
            'gfs_codes',
            'fund_source_category',
            'fund_source_budget_classes',
            'funder'
        )->whereHas('fund_source_versions', function ($query) use ($versionId) {
            $query->where('version_id', $versionId);
        })->where('name', 'LIKE', '%' . $searchText . '%')
            ->orWhere('code', 'LIKE', '%' . $searchText . '%')
            ->orWhere('description', 'LIKE', '%' . $searchText . '%')
            ->orderBy('name', 'asc')->take(10)
            ->paginate(10);

        return response()->json($all, Response::HTTP_OK);
    }

    public function sectorDepartmentFundSources(Request $request) {
        $sector_id = $request->sector_id;
        $section_id = $request->section_id;
        $sql = "select DISTINCT f.id,f.name from fund_sources f
                join activity_facility_fund_sources af on af.fund_source_id = f.id
                join activity_facility_fund_source_inputs a ON af.id = a.activity_facility_fund_source_id
                join activity_facilities afa on af.activity_facility_id = afa.id
                  join activities av on av.id = afa.activity_id
                  join mtef_sections ms on ms.id = av.mtef_section_id
                  join sections s on s.id = ms.section_id
                  join sectors se on se.id = s.sector_id
                  where se.id = '$sector_id' and s.id = '$section_id' and f.deleted_at is null
                  ORDER BY name asc";
        $items = DB::select($sql);
        return response()->json(["fundSources" => $items], Response::HTTP_OK);
    }

    public function revenueFundSources() {
        $sql = "select DISTINCT f.id,f.name from fund_sources f join gfs_codes g on g.fund_source_id = f.id ORDER BY f.name asc";
        $items = DB::select($sql);
        return response()->json(["fundSources" => $items], Response::HTTP_OK);
    }

    
    /**
     * Delete projection
     */
    public function deleteProjection($id)
    {
        try {
            DB::beginTransaction();
           AdminHierarchyCeilingPeriod::where('admin_hierarchy_ceiling_id', $id)->forceDelete();
           AdminHierarchyCeilingForward::where('admin_hierarchy_ceiling_id', $id)->forceDelete();
            AdminHierarchyCeiling::find($id)->forceDelete();
            DB::commit();
            return response()->json(['successMessage' => 'Projection deleted']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['errorMessage' => $e->getMessage()], 500);
        }
    }

        /**
     * Get revenue projections by admin hierarchy nad financial year
     */
    public function getProjection($budgetType, $financialYearId, $adminHierarchyId, $sectionId, $fundSourceId)
    {

        if (!Section::canProject($sectionId)) {
            return response()->json(['errorMessage' => 'This level cannot project'], 400);
        }

        $perPage = Input::get('perPage', 10);
        $searchQuery = Input::get('searchQuery', "%");

        $revenueSbc =  DB::Table('budget_classes as bc')
            ->join('budget_class_versions as bcv', 'bcv.budget_class_id', 'bc.id')
            ->join('financial_year_versions as fyv', 'fyv.version_id', 'bcv.version_id')
            ->where('bc.code', '501')
            ->where('fyv.financial_year_id', $financialYearId)->select('bc.*')
            ->pluck('bc.id')->toArray();

        // Get revenue gfs codes by fund source to filter revenues
        $gfsCodeIds = DB::table('gfs_codes as g')
            ->join('ceilings as c', 'c.gfs_code_id', 'g.id')
            ->join('gfscode_fundsources as gfsf', 'g.id', 'gfsf.gfs_code_id')
            ->join('fund_source_versions as fsv', 'fsv.fund_source_id', 'gfsf.fund_source_id')
            ->join('financial_year_versions as fyv', 'fsv.version_id', 'fyv.version_id')
            ->whereIn('c.budget_class_id', $revenueSbc)
            ->where('gfsf.fund_source_id', $fundSourceId)
            ->where('fyv.financial_year_id', $financialYearId)
            ->where('c.is_active', true)
            ->where('g.is_active', true)
            ->where('g.description', 'ILIKE', '%' . $searchQuery . '%')
            ->where('g.code', 'like', '1%')
            ->pluck('g.id')
            ->toArray();

        // Get projections with periods and forward projections
        $projection = AdminHierarchyCeiling::with('admin_hierarchy_ceiling_periods', 'admin_hierarchy_ceiling_forwards', 'ceiling.gfs_code')
            ->whereHas('ceiling', function ($c) use ($gfsCodeIds, $revenueSbc) {
                $c->whereIn('gfs_code_id', $gfsCodeIds)->whereIn('budget_class_id', $revenueSbc);
            })
            ->where('admin_hierarchy_id', $adminHierarchyId)
            ->where('financial_year_id', $financialYearId)
            ->where('section_id', $sectionId)
            ->where('budget_type', $budgetType)
            ->whereNull('admin_hierarchy_ceilings.deleted_at')
            ->orderBy('id', 'desc')
            ->paginate($perPage);

        $mtef =Mtef::where('admin_hierarchy_id', $adminHierarchyId)->where('financial_year_id', $financialYearId)->select('locked', 'is_final', 'id')->first();
        $fund_source = FundSource::find($fundSourceId);

        $totalRevenue = AdminHierarchyCeiling::getTotalProjectionRevenue($budgetType, $adminHierarchyId, $financialYearId, $sectionId, $fundSourceId);
        $message = ["totalRevenue" => $totalRevenue, "fundSourceTitle" => $fund_source->name, "projections" => $projection, "mtef" => $mtef];
        return response()->json($message, 200);
    }
}
