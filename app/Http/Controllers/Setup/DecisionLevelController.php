<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\AdminHierarchyLevel;
use App\Models\Setup\DecisionLevel;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

/**
 * @resource DecisionLevel
 *
 * DecisionLevelController: This controller manage Budget Decision levels for scrutinization purpose
 */
class DecisionLevelController extends Controller
{
    /**
     * index()
     * This method return all decision levels with their next decision level
     * @response{
     * data[]
     * }
    */
    public function index(){
        $decisionLevel = DecisionLevel::with('adminHierarchyLevels','next_decisions','sectionLevel')->orderBy('sort_order','asc')->get();
        return response()->json($decisionLevel);
    }

    /**
     * decisionLevelByAdminHierarchyLevel()
     * This method return decision levels by specified admin hierarchy
     */
    public function decisionLevelByAdminHierarchyLevel($adminHierarchyLevelId){
        $level=AdminHierarchyLevel::find($adminHierarchyLevelId);
        $decisionLevel = DB::table('decision_levels as dl')->where('admin_hierarchy_level_position',$level->hierarchy_position)->get();
        return response()->json($decisionLevel);
    }

    public function getAllPaginated($perPage) {
        return DecisionLevel::with('adminHierarchyLevels','next_decisions','sectionLevel')->orderBy('sort_order','desc')->paginate($perPage);
    }

    /**
     * getAllPaginated()
     * This method return paginated decision levels with its next decision level
     */
    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    /**
     * store()
     * This method create new decision level
     */
    public function store(Request $request){
        try{
            $userID = UserServices::getUser()->id;
            $data = json_decode($request->getContent());
            $decisionLevel = new DecisionLevel();
            $decisionLevel->admin_hierarchy_level_position = $data->admin_hierarchy_level_position;
            $decisionLevel->section_level_id = $data->section_level_id;
            $decisionLevel->name = $data->name;
            $decisionLevel->description = $data->description;
            $decisionLevel->sort_order = isset($data->sort_order)?$data->sort_order:null;
            $decisionLevel->is_active = True;
            $decisionLevel->created_by = $userID;
            $decisionLevel->save();
            if(isset($data->next_decisions)) {
                foreach ($data->next_decisions as $level) {
                    DB::table('decision_level_next_decisions')->insert(
                        array(
                            'decision_level_id' => $decisionLevel->id,
                            'next_level_id' => $level->id,
                        ));
                }
            }
            //Return language success key and all data
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_DECISION_LEVEL_CREATED", "decisionLevels" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }
    /**
     * update()
     * This method update existing decision level
     */
    public function update(Request $request){
        $userID = UserServices::getUser()->id;
        $data = json_decode($request->getContent());
        try{
            $decisionLevel = DecisionLevel::find($data->id);
            $decisionLevel->admin_hierarchy_level_position = $data->admin_hierarchy_level_position;
            $decisionLevel->section_level_id = $data->section_level_id;
            $decisionLevel->name = $data->name;
            $decisionLevel->description = $data->description;
            $decisionLevel->sort_order = isset($data->sort_order)?$data->sort_order:null;
            $decisionLevel->save();
            DB::table('decision_level_next_decisions')->where('decision_level_id',$decisionLevel->id)
                ->delete();
            if(isset($data->next_decisions)) {
                foreach ($data->next_decisions as $level) {
                    DB::table('decision_level_next_decisions')->insert(
                        array(
                            'decision_level_id' => $decisionLevel->id,
                            'next_level_id' => $level->id,
                        ));
                }
            }
            //Return language success key and all data
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_DECISION_LEVEL_UPDATED", "decisionLevels" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    /**
     * delete()
     * This method delete decision level permanently and return error message if decision level is already in use
     */
    public function delete($id){
        try{
            DB::transaction(function () use ($id){
                DB::table('decision_level_next_decisions')->where('decision_level_id',$id)->delete();
                DB::table('decision_levels')->where('id',$id)->delete();
            });
            $all = $this->getAllPaginated(Input::get('perPage'));

            $message = ["successMessage" => "SUCCESSFULLY_DECISION_LEVEL_DELETED", "decisionLevels" => $all];
            return response()->json($message, 200);
        }
        catch(\Illuminate\Database\QueryException $q){
            $message = ["errorMessage" => "ERROR_DELETING"];
            return response()->json($message, 400);
        }

    }

    /**
     * defaultDecisionLevel()
     * This method set default decision level at each admin hierarchy level. Only one default decision level at each admin hierarchy level is allowed
     */
    public function defaultDecisionLevel(Request $request) {
        $data = json_decode($request->getContent());
        $object = DecisionLevel::find($data->id);

        $existing=DB::table('decision_levels')->
            where('admin_hierarchy_level_position',$object->admin_hierarchy_level_position)->
            where('section_level_id',$object->section_level_id)->
            where('is_default',true)->count();
        if($existing >0 && $data->is_default == true){
            $feedback = ["errorMessage" => "DEFAULT_DECISION_LEVEL_EXIST"];
            return response()->json($feedback, 400);
        }else{
            $object->is_default = $data->is_default;
            $object->save();
            $all = $this->getAllPaginated($request->perPage);
            $feedback = ["action" => "ACTIVITY_DECISION_LEVEL_ACTIVATED", "alertType" => "success", "decisionLevels" => $all];
            return response()->json($feedback, 200);
        }

    }
}
