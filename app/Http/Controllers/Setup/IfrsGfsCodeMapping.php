<?php


namespace App\Http\Controllers\Setup;


use Illuminate\Database\Eloquent\Model;

class IfrsGfsCodeMapping extends Model
{
    public $timestamps=true;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";
    protected $table = "ifrsgfs_mapping";

    protected $fillable = ['gfs_code_id','gfs_code_sub_category_id'];

    public function gfs_code(){
        return $this->belongsTo('App\Models\Setup\GfsCode','gfs_code_id','id');
    }

    public function gfs_code_sub_category(){
        return $this->belongsTo('App\Models\Setup\GfsCodeSubCategory','gfs_code_sub_category_id','id');
    }
}
