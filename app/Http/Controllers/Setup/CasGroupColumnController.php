<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\CasGroupColumn;
use Illuminate\Http\Request;


class CasGroupColumnController extends Controller
{
    public function index(){
        $casGroupColumn = CasGroupColumn::all();
        return response()->json($casGroupColumn);
    }

    public function getCasGroupColumnById($id){
        $casGroupColumn = CasGroupColumn::where('cas_group_type_id',$id)->get();
        return response()->json($casGroupColumn);
    }

    public function getCasGroupColumnParameters($id)
    {
        $casGroupColumn = CasGroupColumn::where('id',$id)->get();
        return response()->json($casGroupColumn);
    }

    public function paginateIndex(Request $request){
        $all = CasGroupColumn::with('groupTypes')->paginate($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request){
        try {
            $userID = UserServices::getUser()->id;
            $data = json_decode($request->getContent());
            $casGroupColumn = new CasGroupColumn();
            $casGroupColumn->name =$data->name;
            $casGroupColumn->cas_group_type_id =$data->cas_group_type_id;
            $casGroupColumn->code =$data->code;
            $casGroupColumn->sort_order =$data->sort_order;
            $casGroupColumn->created_by = $userID;
            $casGroupColumn->save();
            //Return language success key and all data
            $message = ["successMessage" => "SUCCESSFUL_CAS_GROUP_COLUMN_CREATED", "casGroupColumns" => CasGroupColumn::paginate($request->perPage)];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }

    }

    public function update(Request $request){
        try{
            $userID = UserServices::getUser()->id;
            $data = json_decode($request->getContent());
            $casGroupColumn = CasGroupColumn::find($data->id);
            $casGroupColumn->name =$data->name;
            $casGroupColumn->cas_group_type_id =$data->cas_group_type_id;
            $casGroupColumn->code =$data->code;
            $casGroupColumn->sort_order =$data->sort_order;
            $casGroupColumn->update_by = $userID;
            $casGroupColumn->save();

            //Return language success key and all data
            $message = ["successMessage" => "SUCCESSFUL_CAS_GROUP_COLUMN_UPDATED", "casGroupColumns" => CasGroupColumn::paginate($request->perPage)];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete(Request $request, $id){
        $userID = UserServices::getUser()->id;
        $casGroupColumn = CasGroupColumn::find($id);
        $casGroupColumn->deleted_by=$userID;
        $casGroupColumn->delete();
        $message = ["successMessage" => "SUCCESSFULLY_CAS_GROUP_COLUMN_DELETED", "casGroupColumns" => CasGroupColumn::paginate($request->perPage)];
        return response()->json($message, 200);
    }
}
