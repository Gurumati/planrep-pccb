<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\AdminHierarchyGfsCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class AdminHierarchyGfsCodeController extends Controller {
    public function index() {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $gfs_codes = DB::table('gfs_codes')
            ->join('admin_hierarchy_gfs_codes', 'gfs_codes.id', '=', 'admin_hierarchy_gfs_codes.gfs_code_id')
            ->join('admin_hierarchies', 'admin_hierarchies.id', '=', 'admin_hierarchy_gfs_codes.admin_hierarchy_id')
            ->join('account_types', 'account_types.id', '=', 'gfs_codes.account_type_id')
            ->join('gfs_code_categories', 'gfs_code_categories.id', '=', 'gfs_codes.gfs_code_category_id')
            ->leftJoin('fund_sources', 'fund_sources.id', '=', 'gfs_codes.fund_source_id')
            ->select('admin_hierarchy_gfs_codes.id','gfs_codes.id as gfs_code_id','gfs_codes.code','gfs_codes.description','account_types.name as account_type','gfs_code_categories.name as gfs_code_category','fund_sources.name as fund_source')
            ->where('admin_hierarchies.id',$admin_hierarchy_id)
            ->whereNull('admin_hierarchy_gfs_codes.deleted_at')
            ->get();
        return response()->json($gfs_codes,200);
    }

    public function paginated(Request $request) {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $all = $this->getAllPaginated($request->perPage,$admin_hierarchy_id);
        return response()->json($all);
    }

    public function getAllPaginated($perPage,$admin_hierarchy_id) {
        $all = DB::table('gfs_codes')
            ->join('admin_hierarchy_gfs_codes', 'gfs_codes.id', '=', 'admin_hierarchy_gfs_codes.gfs_code_id')
            ->join('admin_hierarchies', 'admin_hierarchies.id', '=', 'admin_hierarchy_gfs_codes.admin_hierarchy_id')
            ->join('account_types', 'account_types.id', '=', 'gfs_codes.account_type_id')
            ->join('gfs_code_categories', 'gfs_code_categories.id', '=', 'gfs_codes.gfs_code_category_id')
            ->leftJoin('fund_sources', 'fund_sources.id', '=', 'gfs_codes.fund_source_id')
            ->select('admin_hierarchy_gfs_codes.id','gfs_codes.code','gfs_codes.description','account_types.name as account_type','gfs_code_categories.name as gfs_code_category','fund_sources.name as fund_source')
            ->where('admin_hierarchies.id',$admin_hierarchy_id)
            ->whereNull('admin_hierarchy_gfs_codes.deleted_at')
            ->paginate($perPage);
        return $all;
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $gfs_code_array = $data->gfs_code_id;
        $perPage = $request->perPage;
        for ($i = 0; $i < count($gfs_code_array); $i++) {
            $gfs_code_id = $gfs_code_array[$i];
            $exists = AdminHierarchyGfsCode::where('admin_hierarchy_id', $admin_hierarchy_id)
                ->where('gfs_code_id', $gfs_code_id)->count();
            if ($exists == 1) {
                continue;
            }
            $obj = new AdminHierarchyGfsCode();
            $obj->gfs_code_id = $gfs_code_id;
            $obj->admin_hierarchy_id = $admin_hierarchy_id;
            $obj->created_by = UserServices::getUser()->id;
            $obj->save();
        }
        $all = $this->getAllPaginated($perPage?$perPage:10,$admin_hierarchy_id);
        $message = ["successMessage" => "ADMIN_HIERARCHY_GFS_CODE_CREATED_SUCCESSFULLY", "adminHierarchyGfsCodes" => $all];
        return response()->json($message, 200);
    }

    public function delete($id) {
        $obj = AdminHierarchyGfsCode::find($id);
        $obj->delete();
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $all = $this->getAllPaginated(Input::get('perPage'),$admin_hierarchy_id);
        $message = ["successMessage" => "SUCCESSFUL_ADMIN_HIERARCHY_GFS_CODE_DELETED", "adminHierarchyGfsCodes" => $all];
        return response()->json($message, 200);
    }
}
