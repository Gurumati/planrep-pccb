<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\casPlanTableItem;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class CasPlanTableItemController extends Controller
{
    public function index()
    {

    }


    public function paginateIndex(Request $request){
        $all = casPlanTableItem::where('cas_plan_table_id',$request->cas_plan_table_id)->with('parent')->paginate($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request)
    {
      try
      {
          $data = json_decode($request->getContent());
          $casPlanTableItem = new casPlanTableItem();
          $casPlanTableItem->code = $data->code;
          $casPlanTableItem->description = $data->description;
          $casPlanTableItem->cas_plan_table_id = $data->cas_plan_table_id;
          $casPlanTableItem->cas_plan_table_item_id = isset($data->cas_plan_table_item_id)?$data->cas_plan_table_item_id:null;
          $casPlanTableItem->sort_order = $data->sort_order;
          $casPlanTableItem->min_value = isset($data->min_value)?$data->min_value:null;
          $casPlanTableItem->max_value = isset($data->max_value)?$data->max_value:null;
          //set default values
          //$casPlanTableItem->value_type_id = 1;
          //$casPlanTableItem->best_value_id = 1;
          //$casPlanTableItem->good_nature_trend_id = 1;
          //$casPlanTableItem->aggregation_operation_id = 1;
          //$casPlanTableItem->statistics_id = 1;

          $casPlanTableItem->save();
          $all = casPlanTableItem::where('cas_plan_table_id',$data->cas_plan_table_id)->paginate($request->perPage);
          $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_ROW_CREATED", "planTableItems" => $all];
          return response()->json($message, 200);

      }catch (QueryException $exception)
      {
        $error_code = $exception->errorInfo[1];
        Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
        $message = ["errorMessage" => "DATABASE_ERROR"];
        return response()->json($message, 400);
      }
    }

    public function update(Request $request)
    {
       try
       {
           $data = json_decode($request->getContent());
           $casPlanTableItem = casPlanTableItem::find($data->id);
           $casPlanTableItem->code = $data->code;
           $casPlanTableItem->description = $data->description;
           $casPlanTableItem->cas_plan_table_id = $data->cas_plan_table_id;
           $casPlanTableItem->cas_plan_table_item_id = isset($data->cas_plan_table_item_id)?$data->cas_plan_table_item_id:null;
           $casPlanTableItem->sort_order = $data->sort_order;
           $casPlanTableItem->min_value = isset($data->min_value)?$data->min_value:null;
           $casPlanTableItem->max_value = isset($data->max_value)?$data->max_value:null;
           $casPlanTableItem->save();
           $all = casPlanTableItem::where('cas_plan_table_id',$data->cas_plan_table_id)->paginate($request->perPage);
           $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_ROW_UPDATED", "planTableItems" => $all];
           return response()->json($message, 200);

       }catch (QueryException $exception)
       {
           $error_code = $exception->errorInfo[1];
           Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
           $message = ["errorMessage" => "DATABASE_ERROR"];
           return response()->json($message, 400);
       }
    }

    public function delete($id)
    {
       try
       {
           $casPlanTableItem = casPlanTableItem::find($id);
           $cas_plan_table_id = $casPlanTableItem->cas_plan_table_id;
           $casPlanTableItem->delete();
           $all = casPlanTableItem::where('cas_plan_table_id',$cas_plan_table_id)->paginate(Input::get('perPage'));
           $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_ROW_DELETED", "planTableItems" => $all];
           return response()->json($message, 200);
       }catch (QueryException $exception)
       {
           $error_code = $exception->errorInfo[1];
           Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
           $message = ["errorMessage" => "DATABASE_ERROR"];
           return response()->json($message, 400);
       }
    }

    public function getTableRows($cas_plan_table_id)
    {
        $casPlanTableItem = casPlanTableItem::whereNull('cas_plan_table_item_id')->where('cas_plan_table_id',$cas_plan_table_id)->get();
        return response()->json($casPlanTableItem, 200);
    }
}
