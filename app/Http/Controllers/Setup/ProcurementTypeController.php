<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\ProcurementType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ProcurementTypeController extends Controller {

    public function index() {
        $all = ProcurementType::where('is_active', true)->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return ProcurementType::orderBy('id')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        $procurementType = new ProcurementType();
        $procurementType->name = $data->name;
        $procurementType->description = $data->description;
        $procurementType->is_active = true;
        $procurementType->sort_order = $data->sort_order;
        $procurementType->save();
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_PROCUREMENT_TYPE_CREATED", "procurementTypes" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        $procurementType = ProcurementType::find($data->id);
        $procurementType->name = $data->name;
        $procurementType->description = $data->description;
        $procurementType->sort_order = $data->sort_order;
        $procurementType->save();
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_PROCUREMENT_TYPE_UPDATED", "procurementTypes" => $all];
        return response()->json($message, 200);
    }

    public function delete($id) {
        $procurementType = ProcurementType::find($id);
        $procurementType->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_PROCUREMENT_TYPE_DELETED", "procurementTypes" => $all];
        return response()->json($message, 200);
    }

    public function toggleProcurementType(Request $request) {
        $data = json_decode($request->getContent());
        $object = ProcurementType::find($data->id);
        $object->is_active = $data->is_active;
        $object->save();
        $all = $this->getAllPaginated($request->perPage);
        if ($data->is_active == false) {
            $feedback = ["action" => "PROCUREMENT_TYPE_DEACTIVATED", "alertType" => "warning", "procurementTypes" => $all];
        } else {
            $feedback = ["action" => "PROCUREMENT_TYPE_ACTIVATED", "alertType" => "success", "procurementTypes" => $all];
        }
        return response()->json($feedback, 200);
    }
}
