<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\CeilingChain;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class CeilingChainController extends Controller
{
    public function index() {
        $all = CeilingChain::with('next_stage','for_admin_hierarchy_level','admin_hierarchy_level','section_level')->orderBy('created_at','asc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return CeilingChain::with('next_stage','next_stage.admin_hierarchy_level','next_stage.section_level','for_admin_hierarchy_level','admin_hierarchy_level','section_level')->orderBy('created_at','asc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new CeilingChain();
            $obj->for_admin_hierarchy_level_position = $data->for_admin_hierarchy_level_position;
            $obj->admin_hierarchy_level_position = $data->admin_hierarchy_level_position;
            $obj->section_level_position = $data->section_level_position;
            $obj->next_id = (isset($data->next_id)) ? $data->next_id : null;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "ceilingChains" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = CeilingChain::find($data->id);
            $obj->for_admin_hierarchy_level_position = $data->for_admin_hierarchy_level_position;
            $obj->admin_hierarchy_level_position = $data->admin_hierarchy_level_position;
            $obj->section_level_position = $data->section_level_position;
            $obj->next_id = (isset($data->next_id)) ? $data->next_id : null;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "ceilingChains" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = CeilingChain::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "ceilingChains" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = CeilingChain::with('next_stage','for_admin_hierarchy_level','admin_hierarchy_level','section_level')->orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        $message = ["trashedCeilingChains" => $all];
        return response()->json($message, 200);
    }

    public function restore($id) {
        CeilingChain::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedCeilingChains" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        CeilingChain::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedCeilingChains" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = CeilingChain::onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS"];
        return response()->json($message, 200);
    }
}
