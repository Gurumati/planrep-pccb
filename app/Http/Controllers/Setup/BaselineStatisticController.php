<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\BaselineStatistic;
use App\Models\Setup\BaselineStatisticFinancialYear;
use App\Models\Setup\BaselineStatisticVersion;
use App\Models\Setup\ConfigurationSetting;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Services\UserServices;
use App\Http\Services\FinancialYearServices;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Validator;


class BaselineStatisticController extends Controller
{
    public function index(Request $request){
        $financialYearId = $request->financialYearId;
        $sql = "select bs.id,bs.hmis_uid,bsfy.financial_year_id,
                   bs.description,bs.code,bs.default_value
            from baseline_statistics bs
                join baseline_statistic_financial_years bsfy on bs.id = bsfy.baseline_statistic_id
            where bsfy.financial_year_id = $financialYearId and bs.deleted_at is null";
        $baseline_statistics = DB::select($sql);
        return response()->json(['data'=>$baseline_statistics]);
    }

    public function indexSingle(){
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $financial_year_id = Input::get('financialYearId');
        /**  $financial_year = FinancialYearServices::getPlanningFinancialYear();
          if ($financial_year){
            $financial_year_id = $financial_year->id;
        }
        else {
            $financial_year = FinancialYearServices::getExecutionFinancialYear();
            $financial_year_id = $financial_year->id;
        }

         $baseline_statistics = DB::table('baseline_statistics as b')
                               ->leftjoin('baseline_statistic_values as v','b.id','v.baseline_statistic_id')
                               ->where('v.admin_hierarchy_id',$admin_hierarchy_id)
                               ->where('v.financial_year_id',$financial_year_id)
                               ->select('b.id','b.description','b.hmis_uid','b.is_common','b.default_value','v.value')
                               ->toSql();*/
        $sql = "select b.id, b.description, b.is_common,b.hmis_uid, b.default_value,bsv.value
from
    baseline_statistics as b
        join baseline_statistic_financial_years bsfy on b.id = bsfy.baseline_statistic_id
        left join (select * from baseline_statistic_values v where v.admin_hierarchy_id=$admin_hierarchy_id ) bsv on b.id = bsv.baseline_statistic_id
        where bsfy.financial_year_id = $financial_year_id  and b.deleted_at is null";
        $baseline_statistics = DB::select($sql);
        return response()->json($baseline_statistics);
    }

    public function store(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, BaselineStatistic::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "Form Errors"];
            return response()->json($message, 500);
        } else {
            DB::transaction(function () use ($request) {
                $data = $request->all();
                $newBaseline = BaselineStatistic::create($data);
                $baseline_statistic_id = $newBaseline->id;

                $newPV = new BaselineStatisticFinancialYear();
                $newPV->baseline_statistic_id = $baseline_statistic_id;
                $newPV->financial_year_id = $request->financialYearId;
                $newPV->save();
            });
        }
        return apiResponse(201, "Success", [], true, []);
    }
    public function copyBaselineStatistics(Request $request)
    {
        DB::transaction(function () use ($request) {
            $sourceFinancialYearId = $request->sourceFinancialYearId;
            $sourceVersionId = $request->sourceVersionId;
            $destinationFinancialYearId = $request->destinationFinancialYearId;
            $items = DB::select("select p.* from baseline_statistics p 
            join baseline_statistic_versions v on p.id = v.baseline_statistic_id
            join versions v2 on v.version_id = v2.id
            join financial_year_versions fyv on v2.id = fyv.version_id 
            where v2.id = $sourceVersionId and fyv.financial_year_id = $sourceFinancialYearId");
            foreach ($items as $item) {
                $pa = new BaselineStatistic();
                $pa->description = $item->description;
                $pa->code = $item->code;
                $pa->save();
                $baseline_statistic_id = $pa->id;

                $npa = new BaselineStatisticFinancialYear();
                $npa->financial_year_id = $destinationFinancialYearId;
                $npa->baseline_statistic_id = $baseline_statistic_id;
                $npa->save();
            }

        });
        return apiResponse(201, "Success", [], true, []);
    }

    public function update(Request $request) {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, BaselineStatistic::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "Form Errors"];
            return response()->json($message, 500);
        } else {
            try {
                $obj = BaselineStatistic::find($request->id);
                $obj->update($data);
                $all = $this->getAllPaginated($request->perPage);
                $message = ["successMessage" => "UPDATE_SUCCESS", "baselineStatistics" => $all];
                return response()->json($message, 200);
            } catch (QueryException $exception) {
                $error = $exception->getMessage();
                $message = ["errorMessage" => $error];
                return response()->json($message, 500);
            }
        }
    }

    public function delete($id) {
        $baseline_statistics = BaselineStatistic::find($id);
        $baseline_statistics->deleted_by = UserServices::getUser()->id;
        $baseline_statistics->delete();
        $baseline_statistics = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFULLY_BASIC_STATISTIC_DELETED", "baselineStatistics" => $baseline_statistics];
        return response()->json($message, 200);
    }

    public function getAllPaginated($perPage) {
        $all = BaselineStatistic::paginate($perPage);
        return $all;
    }

    public function paginate(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }
}
