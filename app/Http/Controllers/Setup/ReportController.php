<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\Report;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ReportController extends Controller {
    private function fetchAll(){
         return Report::with('childReports')->whereNull('parent_id')->orderBy('created_at','asc')->get();
    }
    public function index() {
        $all = $this->fetchAll();
        return response()->json($all);
    }

    public function tree() {
        $all = Report::with('childReports')->get();
        return response()->json($all);
    }
    public function store(Request $request) {
        $data = json_decode($request->getContent());
        try {
            $obj = new Report();
            $obj->name = $data->name;
            $obj->description = (isset($data->description)) ? $data->description : null;
            $obj->parent_id = (isset($data->parent_id)) ? $data->parent_id : null;
            $obj->is_html = $data->is_html;
            $obj->level = (isset($data->level)) ? $data->level : null;
            $obj->position = (isset($data->position)) ? $data->position : null;
            $obj->active = true;
            $obj->save();
            $all = $this->fetchAll();
            $message = ["successMessage" => "CREATE_SUCCESS", "reports" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try {
            $obj = Report::find($data->id);
            $obj->name = $data->name;
            $obj->description = (isset($data->description)) ? $data->description : null;
            $obj->parent_id = (isset($data->parent_id)) ? $data->parent_id : null;
            $obj->is_html = $data->is_html;
            $obj->level = (isset($data->level)) ? $data->level : null;
            $obj->position = (isset($data->position)) ? $data->position : null;
            $obj->active = true;
            $obj->save();
            $all = $this->fetchAll();
            $message = ["successMessage" => "UPDATE_SUCCESS", "reports" => $all];
            return response()->json($message, 200);

        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = Report::find($id);
        $obj->delete();
        $all = $this->fetchAll();
        $message = ["successMessage" => "DELETE_SUCCESS", "reports" => $all];
        return response()->json($message, 200);
    }
}
