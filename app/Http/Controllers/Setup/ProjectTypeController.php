<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;

use App\Models\Setup\ProjectType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ProjectTypeController extends Controller {
    public function index() {
        $all = ProjectType::orderBy('name','desc')->get();
        return response()->json(["items"=>$all],200);
    }

    public function getAllPaginated($perPage) {
        return ProjectType::orderBy('name','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), ProjectType::rules());
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        ProjectType::create($request->all());
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "CREATE_SUCCESSFUL", "items" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, ProjectType::rules($request->id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "FORM_HAS_ERRORS"];
            return response()->json($message, 400);
        }
        $entity = ProjectType::find($request->id);
        $entity->update($data);
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "UPDATE_SUCCESSFUL", "items" => $all];
        return response()->json($message, 200);
    }

    public function delete($id) {
        $obj = ProjectType::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }
}
