<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\BodIntervention;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class BodInterventionController extends Controller
{
    public function index() {
        $all = BodIntervention::orderBy('created_at','desc')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return BodIntervention::with('fund_source','bod_version.start_financial_year','intervention_category')
            ->orderBy('created_at','desc')
            ->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        if(isset($data->intervention_id)){
            $array = $data->intervention_id;
            for ($i = 0;$i < count($array);$i++){
                $count = BodIntervention::where('bod_version_id',$request->bod_version_id)
                    ->where('intervention_id',$array[$i])->count();
                if($count < 1){
                    $bodIntervention = new BodIntervention();
                    $bodIntervention->bod_version_id = $request->bod_version_id;
                    $bodIntervention->intervention_id = $array[$i];
                    $bodIntervention->created_by = UserServices::getUser()->id;
                    $bodIntervention->save();
                }
            }
        }

        $bod_version_id = $request->bod_version_id;
        $all = BodIntervention::with('intervention','bod_list')->where('bod_version_id',$bod_version_id)->get();
        $message = ["successMessage" => "BOD_INTERVENTION_CREATED_SUCCESSFULLY", "bodInterventions" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $bodIntervention = BodIntervention::find($data->id);
            $bodIntervention->bod_version_id = $data->bod_version_id;
            $bodIntervention->intervention_id = $data->intervention_id;
            $bodIntervention->fund_source_id = $data->fund_source_id;
            $bodIntervention->arithmetic_operator = (isset($data->arithmetic_operator)) ? $data->arithmetic_operator : null;
            $bodIntervention->formula = (isset($data->formula)) ? $data->formula : null;
            $bodIntervention->formula_type = (isset($data->formula_type)) ? $data->formula_type : null;
            $bodIntervention->created_by = UserServices::getUser()->id;
            $bodIntervention->save();
            $all = BodIntervention::with('intervention_category','fund_source','bod_list')->get();
            $message = ["successMessage" => "BOD_INTERVENTION_UPDATED_SUCCESSFULLY", "bodInterventions" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete(Request $request,$id) {
        $obj = BodIntervention::find($id);
        $obj->delete();
        $bod_version_id = $request->bod_version_id;
        $all = BodIntervention::with('intervention','fund_source','bod_list')->where('bod_version_id',$bod_version_id)->get();
        $message = ["successMessage" => "BOD_INTERVENTION_DELETED_SUCCESSFULLY", "bodInterventions" => $all];
        return response()->json($message, 200);
    }
    private function allTrashed() {
        $all = BodIntervention::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        BodIntervention::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "BOD_INTERVENTION_RESTORED", "trashedBodInterventions" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        BodIntervention::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "BOD_INTERVENTION_DELETED_PERMANENTLY", "trashedBodInterventions" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = BodIntervention::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "BOD_INTERVENTION_DELETED_PERMANENTLY", "trashedBodInterventions" => $all];
        return response()->json($message, 200);
    }
}
