<?php

namespace App\Http\Controllers\Setup;
use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Setup\ProjectDataFormQuestion;
use App\Models\Setup\ProjectDataFormSelectOption;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class ProjectDataFormQuestionController extends Controller
{
    public function index(){
        $questions = ProjectDataFormQuestion::with('content')
            ->with('questionValues')
            ->with('selectOptions')
            ->orderBy('sort_order')
            ->get();

        return response()->json($questions);
    }

    public function getQuestionByContent($content_id){
        $questions = ProjectDataFormQuestion::with('content')
                   ->orderBy('sort_order')
                   ->where('project_data_form_content_id',$content_id)
                   ->with('questionValues')
                   ->with('selectOptions')
                   ->get();
        return response()->json($questions);
    }

    public function store(Request $request){
        try {
            $data = json_decode($request->getContent());
            $question = new ProjectDataFormQuestion();
            $question->project_data_form_content_id = $data->project_data_form_content_id;
            $question->input_type = $data->input_type;
            $question->description = $data->description;
            $question->question_number = $data->question_number;
            $question->sort_order = $data->sort_order;
            $question->save();

            //Save the select options of the question
            if($data->input_type == "checkbox")
            {
                $select_options = $data->select_options;
                $array_size = count($select_options);
                for ($x = 0; $x<$array_size; $x++){
                    $option = new ProjectDataFormSelectOption();
                    $option -> project_data_form_question_id = $question->id;
                    $option ->label = $data->select_options[$x]->label;
                    $option ->option_value = $data->select_options[$x]->value;
                    $option ->created_by = UserServices::getUser()->id;
                    $option->save();
                }
            }

            //Return language success key and all data
            $questions = ProjectDataFormQuestion::with('content')->orderBy('sort_order','asc')->get();
            $message = ["successMessage" => "SUCCESSFULLY_QUESTION_CREATED", "questions" => $questions];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_QUESTION_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

    public function update(Request $request){
        try {
            $data = json_decode($request->getContent());
            dd($data);
            $question = ProjectDataFormQuestion::find($data->id);
            $question->project_data_form_content_id = $data->project_data_form_content_id;
            $question->input_type = $data->input_type;
            $question->description = $data->description;
            $question->question_number = $data->question_number;
            $question->sort_order = $data->sort_order;
            $question->save();
            //Return language success key and all data
            $questions = ProjectDataFormQuestion::with('content')->orderBy('sort_order','asc')->get();
            $message = ["successMessage" => "SUCCESSFULLY_QUESTION_UPDATED", "questions" => $questions];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_QUESTION_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

    public function delete($id){
        try {
            $contents = ProjectDataFormQuestion::find($id);
            $contents->delete();
            $questions = ProjectDataFormQuestion::with('content')->orderBy('sort_order','asc')->get();
            $message = ["successMessage" => "SUCCESSFULLY_QUESTION_CREATED", "questions" => $questions];
            return response()->json($message, 200);
        }
        catch (QueryException $e){
            $message = ["errorMessage" =>$e->getMessage()];
            return response()->json($message);
        }
    }

}
