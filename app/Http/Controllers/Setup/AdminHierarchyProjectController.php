<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\AdminHierarchyProject;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class AdminHierarchyProjectController extends Controller {
    public function index() {
        $all = AdminHierarchyProject::with('admin_hierarchy')->with('project')->orderBy('created_at')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return AdminHierarchyProject::with('admin_hierarchy')->with('project')->orderBy('created_at')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        $project_id = $data->project_id;
        $adminHierarchyArray = $data->admin_hierarchy_id;
        $count = count($adminHierarchyArray);
        $user_id = UserServices::getUser()->id;
        try {
            for ($i = 0; $i < $count; $i++) {
                $admin_hierarchy_id = $adminHierarchyArray[$i];
                $obj = new AdminHierarchyProject();
                $obj->admin_hierarchy_id = $admin_hierarchy_id;
                $obj->project_id = $project_id;
                $obj->created_by = $user_id;
                $obj->save();
            }
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_ADMIN_HIERARCHY_PROJECT_CREATED", "adminHierarchyProjects" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try {
            $obj = AdminHierarchyProject::find($data->id);
            $obj->admin_hierarchy_id = $data->admin_hierarchy_id;
            $obj->project_id = $data->project_id;
            $obj->updated_by = UserServices::getUser()->id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_ADMIN_HIERARCHY_PROJECT_UPDATED", "adminHierarchyProjects" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = AdminHierarchyProject::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_ADMIN_HIERARCHY_PROJECT_DELETED", "adminHierarchyProjects" => $all];
        return response()->json($message, 200);
    }
}
