<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\AdminHierarchyLevel;
use App\Models\Setup\CasPlan;
use App\Models\Setup\ConfigurationSetting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Models\Setup\Period;
use Illuminate\Support\Facades\Log;
use App\Models\Planning\CasPlanSubmission;

class CasPlanController extends Controller
{
    public function index()
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $admin_hierarchy_level_id = AdminHierarchy::where('id',$admin_hierarchy_id)->first()->admin_hierarchy_level_id;
        $current_position = AdminHierarchyLevel::where('id',$admin_hierarchy_level_id )->first()->hierarchy_position;
        $hierarchy_level_ids = AdminHierarchyLevel::where('hierarchy_position','>=',$current_position)->pluck('id')->toArray();

        $casPlans = CasPlan::with('parent')
                    ->with('administrativeLevel')
                    ->with('assessmentCategory')
                    ->whereIn('cas_plans.admin_hierarchy_level_id',$hierarchy_level_ids)
                    ->orWhereNull('cas_plans.admin_hierarchy_level_id')
                    ->orderBy('name','asc')
                    ->get();
        return response()->json($casPlans, 200);
    }

    public function parents()
    {
        $casPlans = CasPlan::with("children")->whereNull('parent_id')->get();
        return response()->json(['items' => $casPlans], Response::HTTP_OK);
    }

    public function children(Request $request)
    {
        $casPlans = CasPlan::where('parent_id', $request->parent_id)->get();
        return response()->json(['items' => $casPlans], Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        try {
            $data = json_decode($request->getContent());
            $casPlan = new CasPlan();
            $casPlan->name = $data->name;
            $casPlan->sector_id = isset($data->sector_id) ? $data->sector_id : null;
            $casPlan->parent_id = isset($data->parent_id) ? $data->parent_id : null;
            $casPlan->admin_hierarchy_level_id = isset($data->admin_hierarchy_level_id) ? $data->admin_hierarchy_level_id : null;
            $casPlan->is_facility = isset($data->is_facility) ? $data->is_facility : false;
            $casPlan->is_assessed = isset($data->is_assessed) ? $data->is_assessed : false;
            $casPlan->financial_year_type = $data->financial_year_type;
            $casPlan->save();
            $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_CREATED", "casPlans" => CasPlan::with('parent')
                ->with('administrativeLevel')->get()];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 400);
        }
    }

    public function update(Request $request)
    {
        try {
            $data = json_decode($request->getContent());
            $casPlan = CasPlan::find($data->id);
            $casPlan->name = $data->name;
            $casPlan->sector_id = isset($data->sector_id) ? $data->sector_id : null;
            $casPlan->parent_id = isset($data->parent_id) ? $data->parent_id : null;
            $casPlan->admin_hierarchy_level_id = $data->admin_hierarchy_level_id;
            $casPlan->is_facility = isset($data->is_facility) ? $data->is_facility : false;
            $casPlan->is_assessed = isset($data->is_assessed) ? $data->is_assessed : false;
            $casPlan->financial_year_type = $data->financial_year_type;
            $casPlan->save();
            $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_UPDATED", "casPlans" => CasPlan::with('parent')
                ->with('administrativeLevel')
                ->get()];
            return response()->json($message, 200);

        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 400);
        }
    }

    public function delete($id)
    {
        try {
            $casPlan = CasPlan::find($id);
            $casPlan->delete();
            $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_DELETED", "casPlans" => CasPlan::with('parent')
                ->with('administrativeLevel')
                ->get()];
            return response()->json($message, 200);

        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 400);
        }

    }

//check if the plan has been submitted i.e not in edit mode
    public function planIsSubmitted($id){
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        if(!isset($financialYear)){
            $financialYear = FinancialYearServices::getExecutionFinancialYear();
        }
        $financial_year_id = $financialYear->id;
        $editable = true; //initialize to true
        //check if plan has been submitted for assessment
        $is_submitted = CasPlanSubmission::where('cas_plan_id', $id)
                        ->where('admin_hierarchy_id', $admin_hierarchy_id)
                        ->where('financial_year_id', $financial_year_id)
                        ->count();
        if($is_submitted > 0){
            $editable = false;
        }

        //check if plan has been returned
        $cas_result = DB::table('cas_assessment_results as cr')
            ->join('mtefs as m', 'm.id', 'cr.mtef_id')
            ->join('cas_assessment_category_versions as cv', 'cv.id', 'cr.cas_assessment_category_version_id')
            ->join('cas_assessment_categories as cc', 'cc.id', 'cv.cas_assessment_category_id')
            ->where('m.admin_hierarchy_id', $admin_hierarchy_id)
            ->where('m.financial_year_id', $financial_year_id)
            ->where('cc.cas_plan_id', $id)
            ->orderBy('cr.id', 'DESC')
            ->first();
        if(isset($cas_result->is_confirmed)){
            if($cas_result->is_confirmed == false && $cas_result->is_selected == false){
                $editable = true;
                //unlock mtef section
                CasPlanSubmission::lockMtef($admin_hierarchy_id, $financial_year_id, false);
            }
        }

        $data = ['status' => $editable];
        return response()->json($data, 200);
    }

    //get comprehensive plans periods
    function getPlanPeriods(Request $request){
        $table_id = $request->table_id;
        $cas_plan = DB::table('cas_plan_tables as t')
                    ->join('cas_plan_contents as c', 'c.id', 't.cas_plan_content_id')
                    ->join('cas_plans as p','p.id', 'c.cas_plan_id')
                    ->where('t.id', $table_id)
                    ->select('p.financial_year_type')
                    ->first();

        if($cas_plan->financial_year_type === 'planning'){
            $financialYear = FinancialYearServices::getPlanningFinancialYear();
            $financial_year_id = $financialYear->id;
           }else {

         $financialYear = FinancialYearServices::getExecutionFinancialYear();
            //$financial_year_id = ConfigurationSetting::getValueByKey('AUTO_FINANCIAL_YEAR_TO_REPORT');
//            $financial_year_id = $financialYear->id;
        }
       $financial_year_id = $financialYear->id;
        $all = Period::with('financial_year')
            ->where('financial_year_id',$financial_year_id)
            ->orderBy('start_date','asc')->get();
        return response()->json($all);
    }

}
