<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\UserServices;
use App\Models\Setup\Section;
use App\Models\Setup\SectionLevel;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

/**
 * @resource SectionLevel
 *
 * SectionLevelController: This method manage section level (Planning Units Levels); Section level is division types within admin hierarchies
*/
class SectionLevelController extends Controller {

    /**
     * index()
     * This method return all Section levels as array
     * @response {
     * data[]
     * }
     *
    */
    public function index() {
        $sectionLevels = SectionLevel::with('nextBudget')->orderBy('hierarchy_position', 'desc')->get();
        return response()->json($sectionLevels);
    }
    /**
     * getByUser()
     * This method return user section level and its children levels
    */
    public function getByUser() {
        $flatten = new Flatten();
        $userSectionId = UserServices::getUser()->section_id;
        if ($userSectionId != null) {
            $userSections = Section::where('id', $userSectionId)->get();
        } else {
            $userSections = Section::whereNull('parent_id')->get();
        }
        $levelIds = $flatten->flattenSectionWithChildGetLevels($userSections);
        $sectionLevels = SectionLevel::whereIn('id', $levelIds)->orderBy('hierarchy_position', 'asc')->get();
        return response()->json($sectionLevels);
    }
    /**
     * getLevelsByUser()
     * This method return user section level and its children levels as object
     */
    public function getLevelsByUser() {
        $flatten = new Flatten();
        $userSectionId = UserServices::getUser()->section_id;
        if ($userSectionId != null) {
            $userSections = Section::where('id', $userSectionId)->get();
        } else {
            $userSections = Section::whereNull('parent_id')->get();
        }
        $levelIds = $flatten->flattenSectionWithChildGetLevels($userSections);
        $sectionLevels = SectionLevel::whereIn('id', $levelIds)->orderBy('hierarchy_position', 'asc')->get();
        return response()->json(['sectionLevels'=>$sectionLevels],200);
    }

    public function getAllPaginated($perPage) {
        return SectionLevel::with('nextBudget')->orderby('created_at', 'desc')->paginate($perPage);
    }

    /**
     * getSections()
     *
    */
    public function getSections(Request $request) {
        $section_id = UserServices::getUser()->section_id;
        $section = Section::find($section_id);
        $sector_id = $section->sector_id;
        $section_level_id = $request->section_level_id;

        $all = Section::where('section_level_id',$section_level_id)
            ->where('sector_id',$sector_id)
            ->orderby('created_at', 'desc')->get();


        return response()->json(['sections'=>$all],200);
    }
    /**
     * paginated()
     * This method return all Section level by page
    */
    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }
    /**
     * store()
     * This method create new section level
     */
    public function store(Request $request) {
        $data = json_decode($request->getContent());
        $sectionLevel = new SectionLevel();
        $sectionLevel->name = $data->name;
        $sectionLevel->code = $data->code;
        $sectionLevel->description = $data->description;
        $sectionLevel->hierarchy_position = $data->hierarchy_position;
        $sectionLevel->next_budget_level = isset($data->next_budget_level) ? $data->next_budget_level : null;
        $sectionLevel->sort_order = $data->sort_order;
        $sectionLevel->is_active = True;
        $sectionLevel->save();
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_SECTION_LEVEL_CREATED", "sectionLevels" => $all];
        return response()->json($message, 200);
    }
    /**
     * update()
     * This method update existing section level
    */
    public function update(Request $request) {
        $data = json_decode($request->getContent());
        $sectionLevel = SectionLevel::find($data->id);
        $sectionLevel->name = $data->name;
        $sectionLevel->code = $data->code;
        $sectionLevel->description = $data->description;
        $sectionLevel->hierarchy_position = $data->hierarchy_position;
        $sectionLevel->sort_order = $data->sort_order;
        $sectionLevel->next_budget_level = isset($data->next_budget_level) ? $data->next_budget_level : null;
        $sectionLevel->save();
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_SECTION_LEVEL_UPDATED", "sectionLevels" => $all];
        return response()->json($message, 200);
    }
    /**
     * delete()
     * This method delete existing section level
    */
    public function delete($id) {
       try{
           DB::table('section_levels')->where('id',$id)->delete();
           $all = $this->getAllPaginated(Input::get('perPage'));
           $message = ["successMessage" => "SUCCESSFUL_SECTION_LEVEL_DELETED", "sectionLevels" => $all];
           return response()->json($message, 200);
       }catch (QueryException $e){
           $message =['errorMessage'=>'CANNOT_DELETE_SECTION_LEVEL'];
           return response()->json($message, 400);
       }

    }
    /**
     * parentSectionLevel()
     * This method return parent section level of a section level
    */
    public function parentSectionLevel($id) {
        if ($id != 0) {
            $childSectionLevel = SectionLevel::find($id);

            $parentSections = DB::table('section_levels')
                ->select('section_levels.*')
                ->where('id', '!=', $childSectionLevel->id)
                ->where('deleted_at', '=', null)
                ->get();
        } else {
            $parentSections = DB::table('section_levels')
                ->select('section_levels.*')
                ->where('deleted_at', '=', null)
                ->get();
        }
        return response()->json($parentSections, 200);
    }
    /**
     * toggleSectionLevel()
     * This method toggle section level active/inactive
    */
    public function toggleSectionLevel(Request $request) {
        $data = json_decode($request->getContent());
        $obj = SectionLevel::find($data->id);
        $obj->is_active = $data->is_active;
        $obj->save();
        $all = $this->getAllPaginated($request->perPage);
        if ($data->is_active == false) {
            $feedback = ["action" => "SECTION_LEVEL_DEACTIVATED", "alertType" => "warning", "sectionLevels" => $all];
        } else {
            $feedback = ["action" => "SECTION_LEVEL_ACTIVATED", "alertType" => "success", "sectionLevels" => $all];
        }
        return response()->json($feedback, 200);
    }


}
