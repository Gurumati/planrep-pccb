<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\ActivityTaskNature;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class ActivityTaskNatureController extends Controller {
    public function index() {
        $all = ActivityTaskNature::with('activity_category')->orderBy('created_at','desc')->get();
        return response()->json($all,200);
    }

    public function fetchAll() {
        $all = ActivityTaskNature::with('activity_category')->orderBy('created_at','desc')->get();
        return response()->json(["items"=>$all],200);
    }

    public function getAllPaginated($perPage) {
        return ActivityTaskNature::with('activity_category')->orderBy('created_at','desc')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new ActivityTaskNature();
            $obj->name = $data->name;
            $obj->activity_category_id = $data->activity_category_id;
            $obj->save();
            $all = ActivityTaskNature::orderBy('created_at')->paginate($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = ActivityTaskNature::find($data->id);
            $obj->name = $data->name;
            $obj->activity_category_id = $data->activity_category_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function toggleActive($id)
    {
        try {
            $obj = ActivityTaskNature::find($id);
            $obj->is_active = ($obj->is_active)? false : true;
            $obj->save();
            $successMessage = (!$obj->is_active) ? 'Task nature deactivated' : 'Task nature activated';
            return ['successMessage' => $successMessage];
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['errorMessage' => 'ERROR_CHANGING_CEILING'], 500);
        }
    }

    public function delete($id) {
        $obj = ActivityTaskNature::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = ActivityTaskNature::with('activity_category')->orderBy('created_at','desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        $message = ["trashedTaskNatures" => $all];
        return response()->json($message, 200);
    }

    public function restore($id) {
        ActivityTaskNature::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCES", "trashedTaskNatures" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        ActivityTaskNature::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedTaskNatures" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = ActivityTaskNature::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH", "trashedTaskNatures" => $all];
        return response()->json($message, 200);
    }
}
