<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\FinancialYear;
use App\Models\Setup\MetadataListItem;
use App\Models\Setup\MetadataSource;
use App\Models\Setup\MetadataType;
use GuzzleHttp\Client as GuzzleHttpClient;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use stdClass;

class MetadataListItemsController extends Controller
{
    /**
     * Basic algorithm to exchange data with iMES
     *  1. Read all organisation units and data elements from iMES
     *  2. Store their values into the database
     *  3. Write an SQL query to return the data required by a data element
     *  4. Execute the query
     *  5. Structure the result of the query in accordance to iMES's format
     *  6. Send the data to iMES
     */

    public function readApiData(Request $request)
    {
        $connection = Config::get('app');
        $username = $connection['IMES_USERNAME'];
        $password = $connection['IMES_PASSWORD'];
        $imes_host = $connection['IMES_HOST'];
        $imes_port = $connection['IMES_PORT'];

        $data = $request->all();
        $metadataType = $data['metadataType'];
        unset($data['metadataType']);
        $apiEndPoint = 'http://' . $imes_host . ':' . $imes_port . '/api/' . $metadataType . '.json';
        if ($metadataType == 'dataElements') {
            $apiEndPoint = $apiEndPoint . "?fields=id,name,categoryCombo%5BcategoryOptionCombos%5Bid,name%5D%5D&paging=false";
        }
        $queryString = '';

        if (count($data)) {
            foreach ($data as $key => $value) {
                $queryString .= $key . "=$value&";
            }
        }

        $apiEndPoint .= rtrim("?$queryString", '&');
        /**
         * Get authenticated and login to the server the request is sent to
         */
        $client = new GuzzleHttpClient();
        $response = $client->request("GET", $apiEndPoint, [
            "auth" => [$username, $password],
            "headers" => [
                "Accept" => "application/json",
                "Content-type" => "application/json",
            ]]);
        $decoded_data = json_decode($response->getBody()->getContents());

        switch ($metadataType) {
            case "dataElements":
                if ($this->saveDataElements($decoded_data->$metadataType)) {
                    return customApiResponse("", "Data Elements Synchronized Successfully", 200, "");
                };
                break;
            case "dataSets":
                if ($this->saveDatasets($decoded_data->$metadataType)) {
                    return customApiResponse("", "DataSets Synchronized Successfully", 200, "");
                };
                break;
            default:
                return;
        }
    }

    private function saveDataElements($data)
    {
        $check = MetadataType::where('code', 'DE')->first();
        $metadata_type_id = $check->id ? $check->id : null;

        foreach ($data as $datum) {
            /**
             * Check if the data exists
             */
            $data_exists = MetadataListItem::where([
                'uid' => $datum->id,
                'metadata_type_id' => $metadata_type_id,
            ])->count();

            if ($data_exists == 0) {
                /**
                 * Insert records
                 **/
                $de = new MetadataListItem();
                $de->name = $datum->name;
                $de->metadata_type_id = $metadata_type_id;
                $de->uid = $datum->id;
                $de->code = isset($datum->code) ? $datum->code : null;
                $de->category_option_combos = json_encode($datum->categoryCombo);
                $de->save();
            } else {
                //Update the data
                $de = MetadataListItem::where([
                    'uid' => $datum->id,
                    'metadata_type_id' => $metadata_type_id,
                ])->first();

                $de->name = $datum->name;
                $de->metadata_type_id = $metadata_type_id;
                $de->uid = $datum->id;
                $de->code = isset($datum->code) ? $datum->code : null;
                $de->category_option_combos = json_encode($datum->categoryCombo);
                $de->save();
            };
        }
        return true;
    }

    private function saveDatasets($data)
    {
        /**
         *  1. Store dataset
         *  2. Get the data elements
         */
        $check = MetadataType::where('code', 'DS')->first();
        $metadata_type_id = $check->id ? $check->id : null;
        foreach ($data as $datum) {
            /**
             * Check if the data exists
             */
            $count = MetadataListItem::where([
                'uid' => $datum->id,
                'metadata_type_id' => $metadata_type_id,
            ])->count();

            if ($count == 0) {
                $ds = new MetadataListItem();
                $ds->name = $datum->displayName;
                $ds->metadata_type_id = $metadata_type_id;
                $ds->uid = $datum->id;
                $ds->code = isset($datum->code) ? $datum->code : null;
                $ds->save();
                //Get data elements
            } else {
                //Update the data
                $ds = MetadataListItem::where([
                    'uid' => $datum->id,
                    'metadata_type_id' => $metadata_type_id,
                ])->first();

                $ds->name = $datum->displayName;
                $ds->metadata_type_id = $metadata_type_id;
                $ds->uid = $datum->id;
                $ds->code = isset($datum->code) ? $datum->code : null;
                $ds->save();
            };
        }
        return true;
    }

    public function updateDataElements()
    {
        //get metadataTypeId of data elements
        $metadataTypeId = MetadataType::where('code', 'DS')->first();
        $metadataTypeId = isset($metadataTypeId->id) ? $metadataTypeId->id : null;
        $dataSets = MetadataListItem::where('metadata_type_id', $metadataTypeId)->get();

        //Get the data elements for each dataSet
        $connection = Config::get('app');
        $username   = $connection['IMES_USERNAME'];
        $password   = $connection['IMES_PASSWORD'];
        $imes_host  = $connection['IMES_HOST'];
        $imes_port  = $connection['IMES_PORT'];

        foreach ($dataSets as $dataset) {
            $metadataType = MetadataType::where('code', 'DE')->first();
            $dataSetId = $dataset->uid;
            $endpoint = "http://" . $imes_host . ":" . $imes_port . "/api/dataSets/" . $dataSetId . ".json";

            $client = new GuzzleHttpClient();
            $response = $client->request("GET", $endpoint, [
                "auth" => [$username, $password],
                "headers" => [
                    "Accept" => "application/json",
                    "Content-type" => "application/json",
                ]]);

            $response = $response->getBody()->getContents();
            $decoded_data = json_decode($response, true);
            $period = isset($decoded_data['periodType']) ? $decoded_data['periodType'] : null;
            $orgUnit = isset($decoded_data['organisationUnits']) ? $decoded_data['organisationUnits'] : null;
            $parentId = $dataSetId;

            if ($period !== null and $orgUnit !== null) {
                $dataSetElements = $decoded_data['dataSetElements'];
                foreach ($dataSetElements as $value) {
                    $elementId = $value['dataElement']['id'];
                    $dataElement = MetadataListItem::where([
                        'uid' => $elementId,
                        'metadata_type_id' => $metadataType->id,
                    ])->first();

                    $dataElement->parent_id = $parentId;
                    $dataElement->period = $period;
                    $dataElement->save();
                }
            } else {
                //Throw error
            }
        }
        return customApiResponse("", "Metadata Updated Successfully", 200, "");
    }

    public function getDataValuesPeriodically()
    {

        /**
         *  The following is the algorithm
         *  1. Get the data elements which have an sql_query stored
         *  2. Get the data element values for the specified period. The end point receives an argument with the name "selectedPeriod"
         *  3. Structure the values in accordance to the iMES format
         *  4. Send data to iMES
         */

        //---To do --- Query the data elements by periods
        $dataElementUid = Input::get('uid');
        //Get the period of the data element
        // $period_type = MetadataListItem::where();
        $selectedPeriod = Input::get('selectedPeriod');
        $year = Input::get('Year');
        $quarterNumber = Input::get('quarterNumber');//e.g 01,02,03,04

        //Flip the quarter numbers to match the ones of the DHIS2 platform
        switch ($quarterNumber){
            case 'Q1':
                $flippedQuarterNumber = 'Q3';
                break;
            case 'Q2':
                $flippedQuarterNumber = 'Q4';
                break;
            case 'Q3':
                $flippedQuarterNumber = 'Q1';
                break;
            case 'Q4':
                $flippedQuarterNumber = 'Q2';
                break;
            default:
                break;
        }

        $dataSetUid = Input::get('dataSetUid');
        $dataElements = MetadataListItem::where('sql_query', '<>', null)
            ->where('period', $selectedPeriod)
            ->where('parent_id', $dataSetUid)
            ->where(function ($filter) use ($dataElementUid) {
                if (isset($dataElementUid)) {
                    $filter->where('uid', $dataElementUid);
                }
            })->get();
        //Iterate the results to get the result of each SQL query
        //---To do --- Get start and end date by periodTypes
        $valuesArray = array();
        foreach ($dataElements as $dataElement) {
            $dataset = $dataElement->parent_id;
            $dataElementUid = $dataElement->uid;
            $dataElementName = $dataElement->name;
            // ---To do -- Replace parameters in query by passing the sql_query
            $sql_query = $dataElement->sql_query;
            $query_params = getDateRange($dataElement->period, Input::get('relativePeriod', 0));

            $params = [
                'start_date'        => $query_params->start_date,
                'end_date'          => $query_params->end_date,
                'quarter'           => $quarterNumber,
                'financial_year_id' => (int)$year
            ];

            try {
                $sql_query = replaceParams($sql_query, $params);
                $query_results = DB::select(DB::raw($sql_query));

            } catch (\Exception $e) {
                Log::debug($e->getMessage());
                exit();
            }


            //Structure the query result
            foreach ($query_results as $query_result) {
                $result = new stdClass();
                $result->orgUnit = $query_result->orgUnit;
                $result->dataElement = $dataElementUid;
                $result->categoryOptionCombo = isset($query_result->categoryOptionCombo) ?
                    $query_result->categoryOptionCombo : null;
                $result->value = $query_result->value;
                $result->comment = $dataElementName;
                $result->dataSet = $dataset;
                array_push($valuesArray, $result);
            }
        }
        $response = new stdClass();
        $response->completeDate = date('Y-m-d');
        //Get financial year to be used as period for sending data to iMES
        $year_name = FinancialYear::where('id',(int)$year)->get();
        $year_name = $year_name[0]->start_date;
        $period_name = explode('-',$year_name);
        $period_name = $period_name[0];

        //Set the name of period for Quarterly data
        //The quarter number is read from the api
        switch ($selectedPeriod) {
            case "Quarterly":
                $period_name = $period_name.$flippedQuarterNumber;
                break;
            case "FinancialJuly":
                $period_name = $period_name."July";
                break;
            default:
                return "Undefined Period";
        }


        if ($selectedPeriod = 'Quarterly'){
            $period_name = $period_name;
        } elseif ($selectedPeriod = 'FinancialJuly'){
            $period_name = $period_name."FinancialJuly";
        }

        $response->period = $period_name;
        $response->dataValues = $valuesArray;
        $data = array_values($response->dataValues);
        $response->dataValues = $data;
        return response()->json($response);
        $response = ($this->sendData(json_decode(json_encode($response))));
        return customApiResponse($response);
    }

    private function sendData($data)
    {
        $connection = Config::get('app');
        $username = $connection['IMES_USERNAME'];
        $password = $connection['IMES_PASSWORD'];
        $imes_host = $connection['IMES_HOST'];
        $imes_port = $connection['IMES_PORT'];

        $client = new GuzzleHttpClient();
        $response = $client->request("POST", "http://" . $imes_host . ":" . $imes_port . "/api/31/dataValueSets?orgUnitIdScheme=CODE",
            ["auth" => [$username, $password],
                "headers" => [
                    "Accept" => "application/json",
                    "Content-type" => "application/json",
                ],
                "json" => $data,
            ]);

        return json_decode($response->getBody()->getContents());
    }

    public function dataSets()
    {
        $items = $this->loadDataSets();
        return customApiResponse($items, "Success", 200);
    }

    public function dataSourceDataSets(Request $request)
    {
        $data = $request->all();
        $dataSourceId = $data['dataSourceId'];
        $dataSource   = MetadataSource::where('id',$dataSourceId)->first()->source_name;

        $sourceId = $request->dataSourceId;
        $items = MetadataListItem::whereHas('type', function ($type) use($sourceId,$dataSource){
            $type->where("code", $dataSource.'_DS')->where("metadata_source_id",$sourceId);
        })->orderBy("name", "asc")->get();
        return customApiResponse($items, "Success", 200);
    }

    private function loadDataSets()
    {
        return MetadataListItem::whereHas('type', function ($type) {
            $type->where("code", 'DS');
        })->orderBy("name", "asc")->get();
    }

    public function dataElements(Request $request)
    {
        $data = $request->all();
        $dataSourceId = $data['sourceID'];
        $dataSetUID = Input::get("dataSetUID");
        $items = $this->pagedDataElements($dataSetUID,$dataSourceId);
        return customApiResponse($items, "Success", 200);
    }

    private function pagedDataElements($dataSetUID,$dataSourceId)
    {
        /**
         * Get the name of the data source from ID
         */
        $dataSource = MetadataSource::where('id',$dataSourceId)->first()->source_name;
        if (Input::get('perPage') && Input::get('perPage') > 0) {
            $perPage = Input::get('perPage');
        } else {
            $perPage = 10;
        }
        if (Input::get("searchText")) {
            $searchText = '%' . Input::get("searchText") . '%';
            $items = MetadataListItem::where("parent_id", $dataSetUID)
                ->whereHas('type', function ($type) use ($dataSource) {
                    $type->where("code", $dataSource.'_DE');
                })->where(function ($item) use ($searchText) {
                    $item->where('name', 'ILIKE', $searchText)
                        ->orWhere('period', 'ILIKE', $searchText)
                        ->orWhere('code', 'ILIKE', $searchText)
                        ->orWhere('uid', 'ILIKE', $searchText);
                })->orderBy("name", "asc")->paginate($perPage);
        } else {
            $items = MetadataListItem::where("parent_id", $dataSetUID)
                ->whereHas('type', function ($type) use ($dataSource) {
                    $type->where("code", $dataSource.'_DE');
                })->orderBy("name", "asc")
                ->paginate($perPage);
        }
        return $items;
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, MetadataListItem::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            return customApiResponse("", "Failed", 400, $errors->all());
        } else {
            $obj = MetadataListItem::find($request->id);
            $obj->update($data);
            $dataSetUID = Input::get("dataSetUID");
            $items = $this->pagedDataElements($dataSetUID,2);
            return customApiResponse($items, "Metadata Item Updated Successfully!", 201);
        }
    }

    public function search(Request $request)
    {
        $searchString = $request->searchText;
        $items = MetadataListItem::whereNull('deleted_at')
            ->where('name', 'ILIKE', '%' . $searchString . '%')
            ->orWhere('code', 'ILIKE', '%' . $searchString . '%')
            ->select('id', 'name', 'code', 'uid')
            ->orderBy('name', 'asc')->get();
        return response()->json(["items" => $items], Response::HTTP_OK);
    }

    public function sync(Request $request)
    {
        $data = $request->all();
        $dataSourceId = $data['dataSourceId'];
        try{
            $this->processDataSets($dataSourceId);
            $this->processDataElements($dataSourceId);
            $this->mapDataSetElements($dataSourceId);
            $items = MetadataSource::all();
            return customApiResponse($items, "Metadata Item Synced Successfully!", 200);
        } catch (\Exception $exception){
            return customApiResponse('', $exception->getMessage(), 500);
        }
    }

    private function mapDataSetElements($dataSourceId)
    {
        /**
         * Get the name of the system by its id
         */
        $metadataSource = MetadataSource::where('id',$dataSourceId)->first();
        $metadataSource = isset($metadataSource->source_name) ? $metadataSource->source_name : null;

        set_time_limit(600);
        $metadataTypeId = MetadataType::where('code', $metadataSource.'_DS')->first();
        $metadataTypeId = isset($metadataTypeId->id) ? $metadataTypeId->id : null;
        $dataSets = MetadataListItem::where('metadata_type_id', $metadataTypeId)->get();

        $connection = Config::get('app');
        $username = $connection[$metadataSource.'_USERNAME'];
        $password = $connection[$metadataSource.'_PASSWORD'];
        $host = $connection[$metadataSource.'_HOST'];
        $port = $connection[$metadataSource.'_PORT'];

        foreach ($dataSets as $dataset) {
            $metadataType = MetadataType::where('code', $metadataSource.'_DE')->first();
            $dataSetId = isset($dataset->uid) ? $dataset->uid : null;
            if ($port != null){
                $endpoint = $host . ":" . $port . "/api/dataSets/" . $dataSetId . ".json";
            } else {
                $endpoint = $host . "/api/dataSets/" . $dataSetId . ".json";
            }

            $client = new GuzzleHttpClient();
            $response = $client->request("GET", $endpoint, [
                "auth" => [$username, $password],
                "headers" => [
                    "Accept" => "application/json",
                    "Content-type" => "application/json",
                ]]);

            $response = $response->getBody()->getContents();
            $decoded_data = json_decode($response, true);
            $period = isset($decoded_data['periodType']) ? $decoded_data['periodType'] : null;
            $dataSetElements = $decoded_data['dataSetElements'];
            foreach ($dataSetElements as $value) {
                $elementId = $value['dataElement']['id'];
                $dataElement = MetadataListItem::where([
                    'uid' => $elementId,
                    'metadata_type_id' => $metadataType->id,
                ])->first();
                $dataElement->parent_id = $dataSetId;
                $dataElement->period = $period;
                $dataElement->save();
            }
        }
        return customApiResponse("", "Metadata Items Synced Successfully!", 200);
    }

    private function processDataSets($dataSourceId)
    {
        /**
         * Get the name of the system by its id
         */
        $metadataSource = MetadataSource::where('id',$dataSourceId)->first();
        $metadataSource = isset($metadataSource->source_name) ? $metadataSource->source_name : null;

        $connection = Config::get('app');
        $username = $connection[$metadataSource.'_USERNAME'];
        $password = $connection[$metadataSource.'_PASSWORD'];
        $host = $connection[$metadataSource.'_HOST'];
        $port = $connection[$metadataSource.'_PORT'];
        if ($port != null){
            $apiEndPoint = $host . ':' . $port . '/api/dataSets?format=json';
        } else {
            $apiEndPoint = $host . '/api/dataSets?format=json';
        }
        $client = new GuzzleHttpClient();
        $response = $client->request("GET", $apiEndPoint, [
            "auth" => [$username, $password],
            "headers" => [
                "Accept" => "application/json",
                "Content-type" => "application/json",
            ]]);
        $data = json_decode($response->getBody()->getContents());
        $check = MetadataType::where('code', $metadataSource.'_DS')->first();
        $metadata_type_id = isset($check->id) ? $check->id : null;

        foreach ($data->dataSets as $datum) {
            $count = MetadataListItem::where([
                'uid' => $datum->id,
                'metadata_type_id' => $metadata_type_id,
            ])->count();

            if ($count == 0) {
                $ds = new MetadataListItem();
                $ds->name = $datum->displayName;
                $ds->metadata_type_id = $metadata_type_id;
                $ds->uid = $datum->id;
                $ds->code = isset($datum->code) ? $datum->code : null;
                $ds->save();
                //Get data elements
            } else {
                //Update the data
                $ds = MetadataListItem::where([
                    'uid' => $datum->id,
                    'metadata_type_id' => $metadata_type_id,
                ])->first();

                $ds->name = $datum->displayName;
                $ds->metadata_type_id = $metadata_type_id;
                $ds->uid = $datum->id;
                $ds->code = isset($datum->code) ? $datum->code : null;
                $ds->save();
            };
        }
    }

    private function processDataElements($dataSourceId)
    {
        /**
         * Get the name of the system by its id
         */
        $metadataSource = MetadataSource::where('id',$dataSourceId)->first();
        $metadataSource = isset($metadataSource->source_name) ? $metadataSource->source_name : null;

        $connection = Config::get('app');
        $username = $connection[$metadataSource.'_USERNAME'];
        $password = $connection[$metadataSource.'_PASSWORD'];
        $host = $connection[$metadataSource.'_HOST'];
        $port = $connection[$metadataSource.'_PORT'];
        if ($port != null){
            $apiEndPoint = $host . ':' . $port . '/api/dataElements?format=json';

        } else {
            $apiEndPoint = $host . '/api/dataElements?format=json';
        }
        $apiEndPoint .= "&fields=id,name,categoryCombo%5BcategoryOptionCombos%5Bid,name%5D%5D&paging=false";
        $client = new GuzzleHttpClient();
        $response = $client->request("GET", $apiEndPoint, [
            "auth" => [$username, $password],
            "headers" => [
                "Accept" => "application/json",
                "Content-type" => "application/json",
            ]]);
        $data = json_decode($response->getBody()->getContents());
        $check = MetadataType::where('code', $metadataSource.'_DE')->first();
        $metadata_type_id = $check->id ? $check->id : null;

        foreach ($data->dataElements as $datum) {
            $data_exists = MetadataListItem::where([
                'uid' => $datum->id,
                'metadata_type_id' => $metadata_type_id,
            ])->count();

            if ($data_exists == 0) {
                $de = new MetadataListItem();
                $de->name = $datum->name;
                $de->metadata_type_id = $metadata_type_id;
                $de->uid = $datum->id;
                $de->code = isset($datum->code) ? $datum->code : null;
                $de->category_option_combos = json_encode($datum->categoryCombo);
                $de->save();
            } else {
                //Update the data
                $de = MetadataListItem::where([
                    'uid' => $datum->id,
                    'metadata_type_id' => $metadata_type_id,
                ])->first();

                $de->name = $datum->name;
                $de->metadata_type_id = $metadata_type_id;
                $de->uid = $datum->id;
                $de->code = isset($datum->code) ? $datum->code : null;
                $de->category_option_combos = json_encode($datum->categoryCombo);
                $de->save();
            };
        }
    }
}
