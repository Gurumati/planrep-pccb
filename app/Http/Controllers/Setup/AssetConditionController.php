<?php

namespace App\Http\Controllers\Setup;

use App\Http\Controllers\Controller;
use App\Models\Setup\AssetCondition;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class AssetConditionController extends Controller {
    public function index() {
        $all = AssetCondition::orderBy('created_at', 'asc')->get();
        return response()->json(["assetConditions" => $all], 200);
    }

    public function fetchAll() {
        $all = AssetCondition::orderBy('created_at', 'asc')->get();
        return response()->json(["assetConditions" => $all], 200);
    }

    public function getAllPaginated($perPage) {
        return AssetCondition::orderBy('id')->paginate($perPage);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = $request->all();
        $validator = Validator::make($data, AssetCondition::rules());

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "Form Errors"];
            return response()->json($message, 500);
        } else {
            try {
                AssetCondition::create($data);
                $all = $this->getAllPaginated($request->perPage);
                $message = ["successMessage" => "CREATE_SUCCESS", "items" => $all];
                return response()->json($message, 200);
            } catch (QueryException $exception) {
                $error = $exception->getMessage();
                $message = ["errorMessage" => $error];
                return response()->json($message, 500);
            }
        }
    }

    public function update(Request $request) {
        $data = $request->all();
        $id = $request->id;
        $validator = Validator::make($data, AssetCondition::rules($id));
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "Form Errors"];
            return response()->json($message, 500);
        } else {
            try {
                $obj = AssetCondition::find($request->id);
                $obj->update($data);
                $all = $this->getAllPaginated($request->perPage);
                $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
                return response()->json($message, 200);
            } catch (QueryException $exception) {
                $error = $exception->getMessage();
                $message = ["errorMessage" => $error];
                return response()->json($message, 500);
            }
        }
    }

    public function delete($id) {
        $obj = AssetCondition::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = AssetCondition::orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json(["trashedItems" => $all], 200);
    }

    public function restore($id) {
        AssetCondition::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        AssetCondition::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = AssetCondition::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }
}
