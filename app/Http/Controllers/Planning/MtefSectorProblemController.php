<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Planning\MtefSectorProblem;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class MtefSectorProblemController extends Controller
{
    public function index() {
        $all = MtefSectorProblem::with('mtef','sector_problem')->get();
        return response()->json($all);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try{
            $data = json_decode($request->getContent());
            $obj = new MtefSectorProblem();
            $obj->mtef_id = $data->mtef_id;
            $obj->sector_problem_id = $data->sector_problem_id;
            $obj->created_by = UserServices::getUser()->id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "MTEF_SECTOR_PROBLEM_CREATED_SUCCESSFULLY", "mtefSectorProblems" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try{
            $obj = MtefSectorProblem::find($data->id);
            $obj->mtef_id = $data->mtef_id;
            $obj->sector_problem_id = $data->sector_problem_id;
            $obj->updated_by = UserServices::getUser()->id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "SUCCESSFUL_MTEF_SECTOR_PROBLEM_UPDATED", "mtefSectorProblems" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function getAllPaginated($perPage) {
        return MtefSectorProblem::with('mtef','sector_problem')->orderBy('created_at','desc')->paginate($perPage);
    }

    public function delete($id) {
        $obj = MtefSectorProblem::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFUL_MTEF_SECTOR_PROBLEM_DELETED", "mtefSectorProblems" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $all = MtefSectorProblem::with('mtef','sector_problem')->orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        $all = $this->allTrashed();
        return response()->json($all);
    }

    public function restore($id) {
        MtefSectorProblem::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "MTEF_SECTOR_PROBLEM_RESTORED", "trashedMtefSectorProblems" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        MtefSectorProblem::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "MTEF_SECTOR_PROBLEM_DELETED_PERMANENTLY", "trashedMtefSectorProblems" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = MtefSectorProblem::with('mtef','sector_problem')->orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "MTEF_SECTOR_PROBLEM_DELETED_PERMANENTLY", "trashedMtefSectorProblems" => $all];
        return response()->json($message, 200);
    }
}
