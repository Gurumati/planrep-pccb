<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Execution\BudgetExportAccount;
use App\Models\Planning\MtefSection;
use Illuminate\Support\Facades\DB;

class MtefSectionController extends Controller
{
    public function index() {
        $all = MtefSection::with('mtef')->with('section')->with('section_level')->get();
        return response()->json($all);
    }

    public function allMTEFSections() {
        $mtef_sections = DB::table('financial_years')
            ->join('mtefs', 'financial_years.id', '=', 'mtefs.financial_year_id')
            ->join('admin_hierarchies', 'admin_hierarchies.id', '=', 'mtefs.admin_hierarchy_id')
            ->join('mtef_sections', 'mtefs.id', '=', 'mtef_sections.mtef_id')
            ->join('sections', 'mtef_sections.section_id', '=', 'sections.id')
            ->join('section_levels', 'section_levels.id', '=', 'mtef_sections.section_level_id')
            ->join('sectors', 'sectors.id', '=', 'sections.sector_id')
            ->select('financial_years.name as financial_year', 'admin_hierarchies.name as admin_hierarchy','sections.id as section_id','sections.name as section','sectors.id as sector_id', 'sectors.name as sector','mtef_sections.id')
            ->where('financial_years.status',1)
            ->whereNull('mtef_sections.deleted_at')
            ->get();
        return response()->json($mtef_sections);
    }

    public function export($mtef_id){
        $financial_year_id = FinancialYearServices::getPlanningFinancialYear()->id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $section_id = UserServices::getUser()->section_id;

        $sql = "SELECT activity_facility_fund_source_inputs.id,activity_facility_fund_source_inputs.gfs_code_id,activities.id as activity_id 
                from activity_facility_fund_source_inputs 
                join activity_facility_fund_sources on activity_facility_fund_sources.id = activity_facility_fund_source_inputs.activity_facility_fund_source_id 
                join activity_facilities on activity_facilities.id = activity_facility_fund_sources.activity_facility_id 
                join activities on activities.id = activity_facilities.activity_id 
                join mtef_sections on mtef_sections.id = activities.mtef_section_id 
                join mtefs on mtefs.id = mtef_sections.mtef_id 
                where mtef_sections.mtef_id = '$mtef_id'";
        $inputs = DB::select($sql);
        if(count($inputs) > 1){
            foreach ($inputs as $input) {
                $budget_export_account = new BudgetExportAccount();
                $budget_export_account->admin_hierarchy_id = $admin_hierarchy_id;
                $budget_export_account->section_id = $section_id;
                $budget_export_account->financial_year_id = $financial_year_id;
                $budget_export_account->activity_facility_fund_source_input_id = $input->id;
                $budget_export_account->activity_id = $input->activity_id;
                $budget_export_account->gfs_code_id = $input->gfs_code_id;
                $budget_export_account->save();
            }
        }
    }
}
