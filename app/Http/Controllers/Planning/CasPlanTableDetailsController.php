<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Planning\CasPlanTableDetail;
use App\Models\Setup\ConfigurationSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CasPlanTableDetailsController extends Controller
{
    public function getCasPlanTableDetails(Request $request){
        $id       = $request->cas_plan_table_id;
        $facility = $request->facility;
        $admin_hierarchy_id = $request->admin_hierarchy_id;
        $period_id = isset($request->period_id) ? $request->period_id : null;
        //get financial year contents
        $financial_year_id = $request->financial_year_id;
        //Get the current financial year
        //$financial_year_id = $this->getFinancialYear($id);

        //Get the details of the cas plan table
        if($facility != 0)
        {
            $cas_plan_table_details = CasPlanTableDetail::where('admin_hierarchy_id', $admin_hierarchy_id)
                ->where('cas_plan_table_id', $id)
                ->where('facility_id',$facility)
                ->where('period_id', $period_id)
                ->where('financial_year_id', $financial_year_id)
                ->orderBy('id','DESC')
                ->get();
        }else{
            $cas_plan_table_details = CasPlanTableDetail::where('admin_hierarchy_id', $admin_hierarchy_id)
                ->where('cas_plan_table_id', $id)
                ->where('period_id', $period_id)
                ->where('financial_year_id', $financial_year_id)
                ->orderBy('id','DESC')
                ->get();
        }
        return response()->json($cas_plan_table_details);
    }

    public function store(Request $request){
        try {
            //Get the admin hierarchy id
            $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;

            //Get the current financial year
            $data = json_decode($request->getContent());
            $financial_year_id = $this->getFinancialYear($data->cas_plan_table_id);
            $period_id = isset($data->period_id)?$request->period_id:null;

            //check if data exists
            $facility_id = ($data->facility !== 0)?$data->facility:null;
            $casDetails = DB::table('cas_plan_table_details')
                ->where('admin_hierarchy_id', $adminHierarchyId)
                ->where('cas_plan_table_id', $data->cas_plan_table_id)
                ->where('financial_year_id', $financial_year_id)
                ->where('facility_id', $facility_id)
                ->where('period_id', $period_id)
                ->count();
            if($casDetails > 0)
            {
                if($data->facility == 0)
                {
                    DB::table('cas_plan_table_details')
                        ->where('admin_hierarchy_id', $adminHierarchyId)
                        ->where('cas_plan_table_id', $data->cas_plan_table_id)
                        ->where('financial_year_id', $financial_year_id)
                        ->where('period_id', $period_id)
                        ->update(array('comment' => isset($data->comment)?$data->comment:null,
                                       'key' => isset($data->key )?$data->key:null));

                }else
                {
                    DB::table('cas_plan_table_details')
                        ->where('admin_hierarchy_id', $adminHierarchyId)
                        ->where('cas_plan_table_id', $data->cas_plan_table_id)
                        ->where('facility_id',$facility_id)
                        ->where('financial_year_id', $financial_year_id)
                        ->where('period_id', $period_id)
                        ->update(array('comment' => isset($data->comment)?$data->comment:null,
                                       'key' => isset($data->key )?$data->key:null));

                }

            }else
            {
                $casPlanTableDetails = new CasPlanTableDetail();
                $casPlanTableDetails->admin_hierarchy_id = $adminHierarchyId;
                $casPlanTableDetails->cas_plan_table_id = $data->cas_plan_table_id;
                $casPlanTableDetails->financial_year_id = $financial_year_id;
                $casPlanTableDetails->comment = $data->comment;
                $casPlanTableDetails->key = (isset($data->key))?$data->key:null;
                $casPlanTableDetails->url = (isset($data->url))?$data->url:null;
                $casPlanTableDetails->facility_id = ($data->facility !== 0)?$data->facility:null;
                $casPlanTableDetails->description = (isset($data->description))?$data->description:null;
                $casPlanTableDetails->created_by = UserServices::getUser()->id;
                $casPlanTableDetails->period_id = $period_id;
                $casPlanTableDetails->save();
            }
            //end

            $message = ["successMessage" => "SUCCESSFULLY_DETAIL_UPDATED"];
            return response()->json($message,200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_DETAIL_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
    }}

    public function update(Request $request, $id) {
        try {
            //Get the admin hierarchy id
            $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
            $data = json_decode($request->getContent());
            //get financial year to report
            $financial_year_id = $data->financial_year_id;
            //Get the current financial year
           // $financial_year_id = $this->getFinancialYear($id);
            $period_id = isset($data->period_id)?$data->period_id:null;
            if($data->facility == 0)
            {
                $casPlanTableDetails = DB::table('cas_plan_table_details')
                    ->where('admin_hierarchy_id', $adminHierarchyId)
                    ->where('cas_plan_table_id', $id)
                    ->where('financial_year_id', $financial_year_id)
                    ->where('period_id', $period_id)
                    ->update(array('comment' => isset($data->comment)?$data->comment:null,'key' => isset($data->key )?$data->key:null));

            }else
            {
                $casPlanTableDetails = DB::table('cas_plan_table_details')
                    ->where('admin_hierarchy_id', $adminHierarchyId)
                    ->where('cas_plan_table_id', $id)
                    ->where('facility_id',$data->facility)
                    ->where('financial_year_id', $financial_year_id)
                    ->where('period_id', $period_id)
                    ->update(array('comment' => isset($data->comment)?$data->comment:null,'key' => isset($data->key )?$data->key:null));

            }

            $message = ["successMessage" => "SAVED_SUCCESSFULLY"];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

    public function getFinancialYear($cas_plan_table_id){
        $cas_plan = DB::table('cas_plan_tables as t')
                    ->join('cas_plan_contents as c', 'c.id', 't.cas_plan_content_id')
                    ->join('cas_plans as p','p.id', 'c.cas_plan_id')
                    ->where('t.id', $cas_plan_table_id)
                    ->select('p.financial_year_type')
                    ->first();
        if($cas_plan->financial_year_type == 'planning'){
            $financialYear = FinancialYearServices::getPlanningFinancialYear();
            $financialYearId = $financialYear->id;
            //$financialYearId = ConfigurationSetting::getValueByKey('AUTO_FINANCIAL_YEAR_TO_REPORT');
           }else {
            $financialYear = FinancialYearServices::getExecutionFinancialYear();
            $financialYearId = $financialYear->id;
           // $financialYearId = ConfigurationSetting::getValueByKey('AUTO_FINANCIAL_YEAR_TO_REPORT');
        }
        return $financialYearId;
    }
}
