<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;


class MainController extends Controller {
    public function __construct() {
    }

    public function index() {
        $title = "Planning|" . $this->app_initial;
        return view('planning.index', ['js_scripts' => $this->form_scripts, 'title' => $title]);
    }

    public function jsonPlans() {
        return response()->json([
            'lga' => 'Kwimba',
            'year' => '2017'
        ]);
    }
}
