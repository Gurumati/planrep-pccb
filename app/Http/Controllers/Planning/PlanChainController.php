<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Models\Planning\PlanChain;
use Illuminate\Http\Request;


class PlanChainController extends Controller
{


    public function index()
    {
        $planChains = PlanChain::all()->where('is_active',true)->get();
        return response()->json($planChains);
    }

    public function fetchAll(Request $request){

        $financial_year_id = $request->financialYearId;
        $admin_hierarchy_id = $request->adminHierarchyId;
        $query = PlanChain::join('plan_chains as pc','plan_chains.parent_id','pc.id')
                    ->join('plan_chain_types as pct','plan_chains.plan_chain_type_id','pct.id')
                    ->join('long_term_targets as ltt','plan_chains.id','ltt.plan_chain_id')
                    ->join('mtef_annual_targets as mat','ltt.id','mat.long_term_target_id')
                    ->join('mtefs as m','mat.mtef_id','m.id')
                    ->join('sections as s','ltt.section_id','s.id')
                    ->whereNull('plan_chains.deleted_at')
                    ->where('m.admin_hierarchy_id',$admin_hierarchy_id)
                    ->where('m.financial_year_id',$financial_year_id)
                    ->where('pc.is_active', true)
                    ->select('pc.code as objcode','pc.description as objdescription','ltt.code as targetcode','ltt.description as targetdescription')
                    ->orderBy("pc.code", 'asc')
                    ->get();

        return response()->json(["objectives" => $query], 200);
    }
}
