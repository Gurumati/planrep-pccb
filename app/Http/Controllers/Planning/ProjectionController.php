<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Budgeting\AdminHierarchyCeiling;
use App\Models\Budgeting\AdminHierarchyCeilingPeriod;
use App\Models\Budgeting\Ceiling;
use App\Models\Planning\Projection;
use App\Models\Planning\ProjectionBudgetClass;
use App\Models\Planning\ProjectionPeriod;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Services\Budgeting\BudgetingServices;
use App\Models\Budgeting\AdminHierarchyCeilingForward;
use App\Models\Planning\MtefSection;
use App\Models\Setup\FinancialYear;
use App\Models\Setup\Period;
use App\Models\Setup\Version;
use Illuminate\Support\Facades\Log;

class ProjectionController extends Controller
{
    private $limit = 10;

    public function index()
    {
        $all = Projection::with('financial_year', 'admin_hierarchy', 'fund_source')->get();
        return response()->json($all);
    }

    public function paginated()
    {
        $all = Projection::with('financial_year', 'admin_hierarchy', 'fund_source')
            ->paginate($this->limit);
        return response()->json($all);
    }

      /**
     * Initiate projection by gfs code and ceiling
     */
    public function initProjection($budgetType, $financialYearId, $adminHierarchyId, $sectionId, $fundSourceId, $gfsCodeId)
    {

        $revenueSbc =  DB::Table('budget_classes as bc')
            ->join('budget_class_versions as bcv', 'bcv.budget_class_id', 'bc.id')
            ->join('financial_year_versions as fyv', 'fyv.version_id', 'bcv.version_id')
            ->where('bc.code', '501')
            ->where('fyv.financial_year_id', $financialYearId)->select('bc.*')
            ->pluck('bc.id')->toArray();

        $ceiling = DB::table('ceilings as c')
            ->join('gfs_codes as gfs', 'gfs.id', 'c.gfs_code_id')
            ->join('gfscode_fundsources as gfsf', 'gfsf.gfs_code_id', 'gfs.id')
            ->join('fund_source_versions as fsv', 'fsv.fund_source_id', 'gfsf.fund_source_id')
            ->join('financial_year_versions as fyv', 'fyv.version_id', 'fsv.version_id')
            ->where('fyv.financial_year_id', $financialYearId)
            ->whereIn('c.budget_class_id', $revenueSbc)
            ->where('c.is_active', true)
            ->where('gfs.is_active', true)
            ->where('gfsf.fund_source_id', $fundSourceId)
            ->where('gfs.id', $gfsCodeId)
            ->select('c.*')
            ->first();


        if (!isset($ceiling)) {
            return response()->json("Ivalid gfs code", 400);
        }

        $exist = AdminHierarchyCeiling::where('ceiling_id', $ceiling->id)
            ->where('admin_hierarchy_id', $adminHierarchyId)
            ->where('financial_year_id', $financialYearId)
            ->where('budget_type', $budgetType)
            ->where('section_id', $sectionId)
            ->count('id');
        if ($exist > 0) {
            return [];
        }

        $projection = new AdminHierarchyCeiling();
        $projection->ceiling_id = $ceiling->id;
        $projection->financial_year_id = $financialYearId;
        $projection->admin_hierarchy_id = $adminHierarchyId;
        $projection->section_id = $sectionId;
        $projection->budget_type = $budgetType;
        $projection->dissemination_status = 0;
        $projection->use_status = 0;
        $projection->amount = 0;
        $projection->save();

        $periods = Period::planningPeriods();
        foreach ($periods as $period) {
            $periodProjection = new AdminHierarchyCeilingPeriod();
            $periodProjection->period_id = $period->id;
            $periodProjection->admin_hierarchy_ceiling_id = $projection->id;
            $periodProjection->amount = 0;
            $periodProjection->save();
        }
        $forwards = FinancialYear::getForwards($financialYearId);
        $order = 1;
        foreach ($forwards as $year) {
            $yearProjection = new AdminHierarchyCeilingForward();
            $yearProjection->financial_year_id = $year->id;
            $yearProjection->admin_hierarchy_ceiling_id = $projection->id;
            $yearProjection->amount = 0;
            $yearProjection->financial_year_order = $order;
            $yearProjection->save();
            $order++;
        }
        return response()->json($projection, 200);
    }

    public function saveAllocationCeilings(Request $request)
    {
        $data = json_decode($request->getContent());

        foreach ($data->ceilings as $c) {
            $adminCeiling = AdminHierarchyCeiling::find($c->id);
            if($adminCeiling->budget_type == 'APPROVED'){
                return response()->json(500, ['errorMessage'=>'budget is already approved']);
            }
            $adminCeiling->amount = $c->amount;
            $adminCeiling->updated_by = UserServices::getUser()->id;
            $adminCeiling->save();
        }
        $message = ["successMessage" => "ALLOCATION_SAVE_SUCCESSFUL"];
        return response()->json($message);
    }

    public function getAllocationCeilings($budgetType, $financialYearId, $adminHierarchyId, $sectionId, $fundSourceId)
    {
          //  $sectionId = UserServices::getUser()->section_id;
          $topSections = Section::whereNull('parent_id')->get();
          $userIsAtTopSection = false;
          $all = null;
          $totalRevenue = 0;
  
          $versionId = Version::getVersionByFinancialYear($financialYearId, 'FS');
  
  
          $budgetClassIds = DB::Table('budget_classes as bc')
              ->join('budget_class_versions as bcv', 'bcv.budget_class_id', 'bc.id')
              ->join('financial_year_versions as fyv', 'fyv.version_id', 'bcv.version_id')
              ->where('fyv.financial_year_id', $financialYearId)->select('bc.*')
              ->pluck('bc.id')->toArray();
  
          foreach ($topSections as $s) {
              if ($sectionId == $s->id) {
                  $userIsAtTopSection = true;
              }
          }
          if (!$userIsAtTopSection) {
              return response()->json(['errorMessage' => 'This level cannot project'], 500);
          }
          if ($userIsAtTopSection) {
              $financialYear = FinancialYearServices::getPlanningFinancialYear();
  
              $ceilings = Ceiling::where('is_aggregate', true)->where('aggregate_fund_source_id', $fundSourceId)->where('is_active', true)->get();
  
  
  
              $existingCeiling = AdminHierarchyCeiling::with('ceiling')
                  ->whereHas('ceiling', function ($query) use ($fundSourceId, $versionId, $budgetClassIds) {
                      $query->where('is_aggregate', true)
                          ->join('fund_source_versions as fsv', 'fsv.fund_source_id', 'aggregate_fund_source_id')
                          ->where('is_active', true)
                          ->whereIn('budget_class_id', $budgetClassIds)
                          ->where('fsv.version_id', $versionId)
                          ->where('aggregate_fund_source_id', $fundSourceId);
                  })
                  ->where('admin_hierarchy_id', $adminHierarchyId)
                  ->where('financial_year_id', $financialYearId)
                  ->where('section_id', $sectionId)
                  ->where('budget_type', $budgetType)
                  ->get();
  
              if (sizeof($ceilings) > sizeof($existingCeiling)) {
                  $idsToExclude = [];
                  foreach ($existingCeiling as $c) {
                      $idsToExclude[] = $c->ceiling_id;
                  }
                  $newCeilings = Ceiling::where('is_aggregate', true)
                      ->join('fund_source_versions as fsv', 'fsv.fund_source_id', 'aggregate_fund_source_id')
                      ->where('aggregate_fund_source_id', $fundSourceId)
                      ->whereIn('budget_class_id', $budgetClassIds)
                      ->where('fsv.version_id', $versionId)
                      ->where('is_active', true)
                      ->whereNotIn('ceilings.id', $idsToExclude)
                      ->select('ceilings.*')
                      ->get();
  
                  foreach ($newCeilings as $c) {
                      $newCeiling = new AdminHierarchyCeiling();
                      $newCeiling->ceiling_id = $c->id;
                      $newCeiling->amount = 0;
                      $newCeiling->dissemination_status = 0;
                      $newCeiling->use_status = 0;
                      $newCeiling->admin_hierarchy_id = $adminHierarchyId;
                      $newCeiling->financial_year_id = $financialYearId;
                      $newCeiling->section_id = $sectionId;
                      $newCeiling->budget_type = $budgetType;
                      $newCeiling->created_by = UserServices::getUser()->id;
                      $newCeiling->save();
                  }
                  $all = AdminHierarchyCeiling::with('ceiling')
                      ->whereHas('ceiling', function ($query) use ($fundSourceId, $versionId, $budgetClassIds) {
                          $query->where('is_aggregate', true)
                              ->join('fund_source_versions as fsv', 'fsv.fund_source_id', 'aggregate_fund_source_id')
                              ->where('is_active', true)
                              ->whereIn('budget_class_id', $budgetClassIds)
                              ->where('aggregate_fund_source_id', $fundSourceId)
                              ->where('fsv.version_id', $versionId);
                      })
                      ->where('admin_hierarchy_id', $adminHierarchyId)
                      ->where('financial_year_id', $financialYearId)
                      ->where('section_id', $sectionId)
                      ->where('budget_type', $budgetType)
                      ->get();
              } else {
                  $all = $existingCeiling;
              }
  
              $totalRevenue = AdminHierarchyCeiling::getTotalProjectionRevenue($budgetType, $adminHierarchyId, $financialYearId, $sectionId, $fundSourceId);
          }
  
          $message = ['ceilings' => $all, 'totalRevenue' => $totalRevenue];
          return response()->json($message);
    }

    public function store(Request $request, $budgetType, $financialYearId, $adminHierarchyId, $sectionId,$fundSourceId)
    {
        if ($budgetType == 'APPROVED') {
            return response()->json(500, ['errorMessage' => 'Budget is already approved']);
        }
        if (!Section::canProject($sectionId)) {
            return response()->json([], 400);
        }
 

        $data = json_decode($request->getContent());

        DB::transaction(function () use ($data, $budgetType, $financialYearId, $adminHierarchyId, $sectionId, $fundSourceId) {

            $projection = $data;
            $admCeiling = null;

            if (isset($projection->id)) {
                $admCeiling = AdminHierarchyCeiling::find($projection->id);
                $admCeiling->amount = $projection->amount;
            } else {
                $admCeiling = new AdminHierarchyCeiling();
                $admCeiling->dissemination_status = 0;
                $admCeiling->use_status = 0;
                $admCeiling->ceiling_id = $projection->ceiling_id;
                $admCeiling->amount = $projection->amount;
                $admCeiling->admin_hierarchy_id = $adminHierarchyId;
                $admCeiling->financial_year_id = $financialYearId;
                $admCeiling->budget_type = $budgetType;
                $admCeiling->section_id = $sectionId;
            }
            $admCeiling->save();

            foreach ($projection->admin_hierarchy_ceiling_periods as $perProj) {
                $peiodCeiling = null;
                if (isset($perProj->id)) {
                    $peiodCeiling = AdminHierarchyCeilingPeriod::find($perProj->id);
                    $peiodCeiling->amount = $perProj->amount;
                } else {
                    $peiodCeiling = new AdminHierarchyCeilingPeriod();
                    $peiodCeiling->admin_hierarchy_ceiling_id = $admCeiling->id;
                    $peiodCeiling->period_id = $perProj->period_id;
                    $peiodCeiling->amount = $perProj->amount;
                }
                $peiodCeiling->save();
            }
            $order = 1;
            foreach ($projection->admin_hierarchy_ceiling_forwards as $yearProj) {
                $forwardCeiling = null;
                if (isset($yearProj->id)) {
                    $forwardCeiling = AdminHierarchyCeilingForward::find($yearProj->id);
                    $forwardCeiling->amount = $yearProj->amount;
                } else {
                    $forwardCeiling = new AdminHierarchyCeilingForward();
                    $forwardCeiling->admin_hierarchy_ceiling_id = $admCeiling->id;
                    $forwardCeiling->financial_year_id = $yearProj->financial_year_id;
                    $forwardCeiling->amount = $yearProj->amount;
                    $forwardCeiling->financial_year_order = $order;
                }
                $forwardCeiling->save();
            }
        });
        $totalRevenue = AdminHierarchyCeiling::getTotalProjectionRevenue($budgetType, $adminHierarchyId, $financialYearId, $sectionId, $fundSourceId);
        $message = ["successMessage" => "PROJECTION_TRANSACTION_SUCCESS", "totalRevenue" => $totalRevenue];
        return response()->json($message, 200);
    }

    public function update(Request $request)
    {
        $data = json_decode($request->getContent());
        $obj = Projection::find($data->id);
        $obj->financial_year_id = $data->financial_year_id;
        $obj->fund_source_id = $data->fund_source_id;
        $obj->save();
        $all = Projection::with('financial_year')
            ->with('admin_hierarchy')
            ->with('fund_source')
            ->paginate($this->limit);
        $message = ["successMessage" => "SUCCESSFUL_PROJECTION_UPDATED", "projections" => $all];
        return response()->json($message, 200);
    }

    public function delete($id)
    {
        $object = Projection::find($id);
        $object->delete();
        $all = Projection::with('financial_year')
            ->with('admin_hierarchy')
            ->with('fund_source')
            ->paginate($this->limit);
        $message = ["successMessage" => "SUCCESSFULLY_PROJECTION_DELETED", "projections" => $all];
        return response()->json($message, 200);
    }

    public function allProjectionBudgetClasses($id)
    {
        $all = ProjectionBudgetClass::where('projection_id', $id)->with('budget_class')->get();
        $message = ["projectionBudgetClasses" => $all];
        return response()->json($message);
    }

    public function updateAmounts(Request $request)
    {
        $data = json_decode($request->getContent());
        $dataArray = $data->dataArray;
        $projection_id = $data->projection_id;
        $total = 0;
        foreach ($dataArray as $item) {
            $budget_class_id = $item->budget_class_id;
            $amount = $item->amount;
            ProjectionBudgetClass::where('projection_id', $projection_id)
                ->where('budget_class_id', $budget_class_id)
                ->update(['amount' => $amount]);
            $total = $total + $amount;
        }
        $obj = Projection::find($projection_id);
        $obj->total_amount = $total;
        $obj->save();
        $all = Projection::with('financial_year')
            ->with('admin_hierarchy')
            ->with('fund_source')
            ->paginate($this->limit);
        $message = ["successMessage" => "SUCCESSFULLY_PROJECTION_AMOUNT_UPDATED", "projections" => $all];
        return response()->json($message, 200);
    }

    public function allProjectionPeriods($id)
    {
        $all = ProjectionPeriod::where('projection_id', $id)->with('period')->get();
        $message = ["projectionPeriods" => $all];
        return response()->json($message);
    }

    public function updateProjectionPeriodPercentage(Request $request)
    {
        $data = json_decode($request->getContent());
        $dataArray = $data->dataArray;
        $projection_id = $data->projection_id;
        $total = 0;
        foreach ($dataArray as $item) {
            $period_id = $item->period_id;
            $percentage = $item->percentage;
            ProjectionPeriod::where('projection_id', $projection_id)
                ->where('period_id', $period_id)
                ->update(['percentage' => $percentage]);
            $total = $total + $percentage;
        }
        $obj = Projection::find($projection_id);
        $obj->total_percentage = $total;
        $obj->save();
        $all = Projection::with('financial_year')
            ->with('admin_hierarchy')
            ->with('fund_source')
            ->paginate($this->limit);
        $message = ["successMessage" => "SUCCESSFULLY_PROJECTION_PERCENTAGE_UPDATED", "projections" => $all];
        return response()->json($message, 200);
    }
}
