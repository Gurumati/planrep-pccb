<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Http\Controllers\SharedService;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\Planning\PlanningServices;
use App\Http\Services\UserServices;
use App\Models\Planning\AnnualTarget;
use App\Models\Planning\LongTermTarget;
use App\Models\Planning\Mtef;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AnnualTargetController extends Controller
{
    private $refdoc;
    private $mtef;
    private $limit = 10;

    public function setRefDocId()
    {
        $adminHierarchyId=UserServices::getUser()->admin_hierarchy_id;
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $refDocument=DB::table('reference_documents as r')
            ->join('financial_years as sF','sF.id','=','r.start_financial_year')
            ->join('financial_years as eF','eF.id','=','r.end_financial_year')
            ->where('r.admin_hierarchy_id','=',$adminHierarchyId)
            ->where('sF.start_date','<=',$financialYear->start_date)
            ->where('eF.end_date','>=',$financialYear->end_date)->select('r.*')->first();

        $mtef=Mtef::where('admin_hierarchy_id',$adminHierarchyId)->where('financial_year_id',$financialYear->id)->first();
        $this->mtef = $mtef;
        $this->refdoc=$refDocument->id;
    }

    /**
     * @param $mtefId
     * @param $mtefSectionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($mtefId,$mtefSectionId)
    {
        return response()->json(AnnualTarget::getByMtefSection($mtefId,$mtefSectionId,null));
    }
    /**
     * get activity categories to be used with targets
     * @return \Illuminate\Http\JsonResponse
     */
    public function getActivityCategories()
    {
        try {
            $planningService = new PlanningServices();
            $data = $planningService->getActivityCategories();
            return response()->json($data);
        } catch (QueryException $exception){
            $message = ['errorMessage' => 'ERROR_LOADING_DATA'];
            return response()->json($message, 500);
        }
    }
    /**
     * @param $mtefId
     * @param $mtefSectionId
     * @param $budgetType
     * @return \Illuminate\Http\JsonResponse
     */
    public function byMtefSectionAndPlanChain($mtefId,$mtefSectionId,$budgetType, $planChainId)
    {
        return response()->json(['annualTargets'=>AnnualTarget::getByMtefSectionAndPlanChain($mtefId,$mtefSectionId,$budgetType,$planChainId)]);
    }


    public function byMtefSectionPaginated($mtefId,$mtefSectionId,$budgetType)
    {
        return AnnualTarget::getByMtefSectionPaginated($mtefId,$mtefSectionId,$budgetType);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $financialYear = FinancialYearServices::getPlanningFinancialYear();
            $financialYearId = $financialYear->id;
            $section_id = UserServices::getUser()->section_id;
            $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
            $mtef=Mtef::where('admin_hierarchy_id',$admin_hierarchy_id)->where('financial_year_id',$financialYearId)->get();

            $data = json_decode($request->getContent());
            $annualTarget = new AnnualTarget();
            $annualTarget->description = $data->target_description;
            $annualTarget->code = $data->code;
            $annualTarget->long_term_target_id = $data->long_term_target_id;
            $annualTarget->mtef_id = $mtef[0]->id;
            $annualTarget->section_id = $section_id;
            $annualTarget->created_by = UserServices::getUser()->id;
            $annualTarget->created_at = date('Y-m-d H:i:s');
            $annualTarget->save();
            //Return language success key and all data
           // $annualTargets=$this->getTargets();
            $message = ["successMessage" => "SUCCESSFUL_ANNUAL_TARGET_CREATED"];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_ANNUAL_TARGET_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $financialYear = FinancialYearServices::getPlanningFinancialYear();
            $financialYearId = $financialYear->id;
            $section_id = UserServices::getUser()->section_id;
            $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
            $mtef=Mtef::where('admin_hierarchy_id',$admin_hierarchy_id)->where('financial_year_id',$financialYearId)->get();

            $data = json_decode($request->getContent());
            $annualTarget = AnnualTarget::find($data->id);
            $annualTarget->description = $data->target_description;
            $annualTarget->code = $data->code;
            $annualTarget->long_term_target_id = $data->long_term_target_id;
            $annualTarget->mtef_id = $mtef[0]->id;
            $annualTarget->section_id = $section_id;
            $annualTarget->created_by = UserServices::getUser()->id;
            $annualTarget->created_at = date('Y-m-d H:i:s');
            $annualTarget->save();
            //Return language success key and all data
            $reference_document_id = $this->refdoc;
            $annualTargets=$this->getTargets();

            $message = ["successMessage" => "SUCCESSFUL_ANNUAL_TARGET_UPDATED", "annualTargets" => $annualTargets];
            return response()->json($message, 200);
        } catch (QueryException $exception){
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_ANNUAL_TARGET_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }


    public function delete($id)
    {
        $annualTarget = AnnualTarget::find($id);
        $annualTarget->delete();
        $annualTargets=$this->getTargets();
        $message = ["successMessage" => "SUCCESSFULLY_ANNUAL_TARGET_DELETED", "annualTargets" => $annualTargets];
        return response()->json($message, 200);
    }

    public function confirm(Request $request, $id){
        $data = $request->all();
        AnnualTarget::confirm($id, $data);
        $message = ["successMessage" => "SUCCESSFUL_CONFIRM_TARGET"];
        return response()->json($message, 200);
    }

    public function initiate($financialYearId, $longTermTargetId){
        $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $longTermTarget = LongTermTarget::find($longTermTargetId);
        $mtef = Mtef::where('admin_hierarchy_id', $adminHierarchyId)->where('financial_year_id', $financialYearId)->first();
        $exist = AnnualTarget::where('long_term_target_id', $longTermTarget->id)->
                 where('section_id',$longTermTarget->section_id)->
                 where('mtef_id', $mtef->id)->first();
        if(isset($exist)){
            return $exist;
        }
        $target = AnnualTarget::create([
            'description' => $longTermTarget->description,
            'code' => $longTermTarget->code,
            'mtef_id' => $mtef->id,
            'long_term_target_id' => $longTermTarget->id,
            'section_id' => $longTermTarget->section_id,
            'is_final' =>false
        ]);
        return response()->json($target, 200);
    }
}
