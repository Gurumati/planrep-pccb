<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Models\Planning\ActivityFacility;

class ActivityFacilityController extends Controller {

    public function getByActivity($activityId){

        return ActivityFacility::paginateByActivityWithInput($activityId);
    }
}
