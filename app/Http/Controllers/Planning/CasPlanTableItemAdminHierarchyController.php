<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Planning\CasPlanTableItemAdminHierarchy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CasPlanTableItemAdminHierarchyController extends Controller
{
   public function index($cas_plan_table_id)
   {
       $all = CasPlanTableItemAdminHierarchy::where('cas_plan_table_id',$cas_plan_table_id)
           ->with('casPlanTableItem')
           ->get();
       return response()->json($all);
   }

   public function store(Request $request)
   {
       $data = json_decode($request->getContent());
       try{
           $cas_plan_table_items = new CasPlanTableItemAdminHierarchy();
           $cas_plan_table_items->cas_plan_table_id = $data->cas_plan_table_id;
           $admin_hierarchy = UserServices::getUser()->admin_hierarchy_id;
           $data_input = array(); $i = 1;
           foreach ($data->items as $key=>$value)
           {
               //check exists
              $itemExists = CasPlanTableItemAdminHierarchy::where('cas_plan_table_id',$cas_plan_table_items->cas_plan_table_id)
                                               ->where('admin_hierarchy_id',$admin_hierarchy)
                                               ->where('cas_plan_table_item_id',$value)
                                               ->first();

               if(empty($itemExists))
               {
                   $new_data = array('cas_plan_table_id'=>$cas_plan_table_items->cas_plan_table_id,
                       'admin_hierarchy_id'=>$admin_hierarchy,
                       'cas_plan_table_item_id'=>$value,
                       'sort_order'=>$i);

                   array_push($data_input,$new_data);
                   $i++;
               }
           }
           if(!empty($data_input))
           {
               CasPlanTableItemAdminHierarchy::insert($data_input);
           }
           $all = CasPlanTableItemAdminHierarchy::where('cas_plan_table_id',$cas_plan_table_items->cas_plan_table_id)
                                                  ->with('casPlanTableItem')
                                                  ->get();
           $message = ["successMessage" => "SUCCESSFUL_CAS_PLAN_TABLE_ITEMS_CREATED",'casPlanItemValues'=>$all];
           return response()->json($message, 200);
       }catch (QueryException $exception) {
           $error_code = $exception->errorInfo[1];
           Log::alert(UserServices::getUser()->email.'[ERROR_CODE:'.$error_code.']');
           $message = ["errorMessage" => "DATABASE_ERROR"];
           return response()->json($message, 500);
       }

   }

   public function update(Request $request)
   {

   }

   public function delete($id)
   {

   }

    public function planContents($cas_plan_id)
    {

        $plan_content = DB::table('cas_plan_contents')->where('cas_plan_id',$cas_plan_id)->select('id')->get();
        $array = array();
        foreach ($plan_content as $key=>$value)
        {
            $array[] = $value->id;
        }
        return $array;
    }

    public function getTabularTable($cas_plan_id)
   {
       $plan_content   = $this->planContents($cas_plan_id);
       $cas_plan_table = DB::table('cas_plan_tables')
                         ->where('type','2')
                         ->whereIn('cas_plan_content_id',$plan_content)
                         ->orderBy('cas_plan_content_id')
                         ->get();
       return response()->json($cas_plan_table);
   }
}
