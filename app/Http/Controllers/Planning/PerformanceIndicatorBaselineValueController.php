<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Controllers\SharedService;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Planning\PerformanceIndicatorBaselineValue;
use App\Models\Planning\PerformanceIndicatorFinancialYearValue;
use App\Models\Setup\PerformanceIndicator;
use App\Models\Setup\PlanChain;
use App\Models\Setup\Section;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use App\Models\Setup\Version;

class PerformanceIndicatorBaselineValueController extends Controller
{

    public function index()
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $all = PerformanceIndicatorBaselineValue::with('start_financial_year', 'end_financial_year', 'admin_hierarchy', 'performance_indicator')->where('admin_hierarchy_id', $admin_hierarchy_id)->orderBy('created_at', 'desc')->get();
        $data = ["baseline_values" => $all];
        return response()->json($data, 200);
    }

    public function getAllPaginated($perPage, $queryString)
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $sectionId = UserServices::getUser()->section_id;
        $sections = Section::where('id', $sectionId)->get();
        $flatten = new Flatten();
        $useCanTarget = SharedService::userCanTarget();
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $refDoc = SharedService::getUserStrategicPlan($financialYear);

        $versionId = Version::getCurrentVersionId('PI');
        $Ids = Version::getVesionItemIds($versionId, 'PI');

        if ($refDoc != null) {

            if ($useCanTarget) {
                $performanceIndicatorIds = DB::table('long_term_targets')
                ->where('reference_document_id',$refDoc->id)
                ->where('section_id',$sectionId)
                ->get()
                ->pluck('performance_indicator_id');
            
                if (is_null($queryString)) {
                    $queryString = "'%" .'%'. "%'";
                } else {
                    $queryString = "'%" . strtolower($queryString) . "%'";
                }
                $indicators = PerformanceIndicator::
                    with([
                        'baseline_values' => function ($query) use ($admin_hierarchy_id, $sectionId, $refDoc) {
                            $query->where('admin_hierarchy_id', $admin_hierarchy_id)
                                    ->where('section_id', $sectionId)
                                    ->where('reference_document_id', $refDoc->id);
                        }, 
                        'baseline_values.financial_year_values'])
                ->whereRaw('LOWER(description) like ' . $queryString)
                ->whereIn('id', $performanceIndicatorIds)
                ->paginate($perPage);
            } else {
                $indicators = new \stdClass();
                $indicators->data = array();
                $indicators->userCanTarget = false;
                $indicators->errorMessage = 'CURRENT_PLANNING_UNIT_CANNOT_SET_INDICATOR_VALUES';
            }
        } else {
            $indicators = new \stdClass();
            $indicators->data = array();
            $indicators->referenceDocExist = false;
            $indicators->errorMessage = 'NO_STRATEGIC_PLAN_SET_FOR_CURRENT_PERIOD';
        }
        return $indicators;
    }

    public function paginated(Request $request)
    {
        try {
            $queryString = $request->searchQuery;
            $all = $this->getAllPaginated($request->perPage, $queryString);
            if ((isset($all->userCanTarget) && $all->userCanTarget == false) || (isset($all->referenceDocExist) && $all->referenceDocExist == false)) {
                return response()->json($all, 400);
            } else {
                return response()->json($all);
            }
        } catch (\Exception $exception) {
            Log::error($exception);
            return response()->json(['errorMessage' => 'INTERNAL_SERVER_ERROR', 'errorInfo' => $exception->getMessage()], 500);
        }

    }

    public function store(Request $request)
    {

        $data = json_decode($request->getContent());
        DB::transaction(function () use ($data) {
            $sectionId = UserServices::getUser()->section_id;
            $financialYear = FinancialYearServices::getPlanningFinancialYear();
            $refDoc = SharedService::getUserStrategicPlan($financialYear);

            $obj = new PerformanceIndicatorBaselineValue();
            $obj->baseline_value = $data->baseline_value;
            $obj->targeted_value = $data->targeted_value;
            $obj->admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
            $obj->reference_document_id = $refDoc->id;
            $obj->section_id = $sectionId;
            $obj->performance_indicator_id = $data->performance_indicator_id;
            $obj->save();

            $performance_indicator_baseline_value_id = $obj->id;

            $years_array = $data->years;
            foreach ($years_array as $year) {
                $v = new PerformanceIndicatorFinancialYearValue();
                $v->performance_indicator_baseline_value_id = $performance_indicator_baseline_value_id;
                $v->financial_year_id = $year->financial_year_id;
                $v->planned_value = $year->planned_value;
                $v->actual_value = 0;
                $v->data_source_id = $year->data_source_id;
                $v->save();
            }
        });
        $all = $this->getAllPaginated($request->perPage, null);
        $message = ["successMessage" => "CREATE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request)
    {
        $data = json_decode($request->getContent());
        try {

            $obj = PerformanceIndicatorBaselineValue::find($data->id);
            $obj->baseline_value = $data->baseline_value;
            $obj->targeted_value = $data->targeted_value;

            $obj->performance_indicator_id = $data->performance_indicator_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage, null);
            $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id)
    {
        $obj = PerformanceIndicatorBaselineValue::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'), null);
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed()
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $all = PerformanceIndicatorBaselineValue::with('start_financial_year', 'end_financial_year', 'admin_hierarchy', 'performance_indicator')->where('admin_hierarchy_id', $admin_hierarchy_id)->orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed()
    {
        $all = $this->allTrashed();
        $message = ["trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function restore($id)
    {
        PerformanceIndicatorBaselineValue::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCES", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id)
    {
        PerformanceIndicatorBaselineValue::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash()
    {
        $trashes = PerformanceIndicatorBaselineValue::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function financialYearBaselineValues(Request $request)
    {
        $performance_indicator_baseline_value_id = $request->performance_indicator_baseline_value_id;
        $all = PerformanceIndicatorFinancialYearValue::with('financial_year', 'data_source')->where('performance_indicator_baseline_value_id', $performance_indicator_baseline_value_id)->get();
        $message = ["baseline_values" => $all];
        return response()->json($message, 200);
    }

    public function addProjection(Request $request)
    {
        $data = json_decode($request->getContent());
        $performance_indicator_baseline_value_id = $data->performance_indicator_baseline_value_id;
        try {
            $obj = new PerformanceIndicatorFinancialYearValue();
            $obj->performance_indicator_baseline_value_id = $data->performance_indicator_baseline_value_id;
            $obj->financial_year_id = $data->financial_year_id;
            $obj->data_source_id = $data->data_source_id;
            $obj->planned_value = $data->planned_value;
            $obj->actual_value = 0;
            $obj->save();
            $all = PerformanceIndicatorFinancialYearValue::with('financial_year', 'data_source')->where('performance_indicator_baseline_value_id', $performance_indicator_baseline_value_id)->get();
            $message = ["baseline_values" => $all, "successMessage" => "PROJECTION_CREATED_SUCCESS"];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_code . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function deleteProjection(Request $request)
    {
        $projection_id = $request->projection_id;
        $performance_indicator_baseline_value_id = $request->performance_indicator_baseline_value_id;
        $object = PerformanceIndicatorFinancialYearValue::find($projection_id);
        $object->delete();
        $all = PerformanceIndicatorFinancialYearValue::with('financial_year', 'data_source')->where('performance_indicator_baseline_value_id', $performance_indicator_baseline_value_id)->get();
        $message = ["baseline_values" => $all, "successMessage" => "PROJECTION_REMOVED_SUCCESSFULLY"];
        return response()->json($message);
    }

    public function saveValues(Request $request)
    {
        $data = json_decode($request->getContent());
        DB::transaction(function () use ($data) {
            foreach ($data->indicators as $indicator) {
                if (isset($indicator->baseline_values[0])) {
                    $this->saveBaseLine($indicator->baseline_values[0], $indicator->id);
                }
            }
        });
        $message = ["successMessage" => "BASE_LINE_VALUES_SAVED_SUCCESSFUL"];
        return response()->json($message);

    }
    private function saveBaseLine($baseLine, $indicatorId)
    {
        $sectionId = UserServices::getUser()->section_id;
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $refDoc = SharedService::getUserStrategicPlan($financialYear);

        if (isset($baseLine->id)) {
            $baseLineValues = PerformanceIndicatorBaselineValue::find($baseLine->id);
        } else {
            $baseLineValues = new PerformanceIndicatorBaselineValue();
            $baseLineValues->performance_indicator_id = $indicatorId;
            $baseLineValues->admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
            $baseLineValues->section_id = $sectionId;
            $baseLineValues->reference_document_id = $refDoc->id;

        }
        $baseLineValues->baseline_value = isset($baseLine->baseline_value) ? $baseLine->baseline_value : '';
        $baseLineValues->targeted_value = isset($baseLine->targeted_value) ? $baseLine->targeted_value : '';
        $baseLineValues->save();
        if (isset($baseLine->fy_values)) {
            $dataSourceId = (isset($baseLine->dataSourceId) ? $baseLine->dataSourceId : null);
            $this->saveFinancialYearValues($baseLine->fy_values, $baseLineValues->id, $dataSourceId);
        }
    }
    private function saveFinancialYearValues($fy_values, $baseLineId, $dataSourceId)
    {
        foreach ($fy_values as $value) {
            if (isset($value->id)) {
                $fyValue = PerformanceIndicatorFinancialYearValue::find($value->id);
            } else {
                $fyValue = new PerformanceIndicatorFinancialYearValue();
                $fyValue->performance_indicator_baseline_value_id = $baseLineId;
            }
            $fyValue->financial_year_id = $value->financial_year_id;
            $fyValue->planned_value = isset($value->planned_value) ? $value->planned_value : '';
            $fyValue->data_source_id = $dataSourceId;

            $fyValue->save();

        }
    }
    public function saveActualValues(Request $request)
    {
        $data = json_decode($request->getContent());
        DB::transaction(function () use ($data) {
            foreach ($data->indicators as $indicator) {
                if (isset($indicator->baseline_values[0]->id))
                    $this->saveFinancialYearActualValues($indicator->baseline_values[0]->fy_values, $indicator->baseline_values[0]->id);
            }
        });
        $message = ["successMessage" => "BASE_LINE_VALUES_SAVED_SUCCESSFUL"];
        return response()->json($message);

    }
    private function saveFinancialYearActualValues($fy_values, $baseLineId)
    {

        foreach ($fy_values as $value) {
            if (isset($value->actual_value)) {
                if (isset($value->id)) {
                    $fyValue = PerformanceIndicatorFinancialYearValue::find($value->id);
                } else {
                    $fyValue = new PerformanceIndicatorFinancialYearValue();
                    $fyValue->performance_indicator_baseline_value_id = $baseLineId;
                }
                $fyValue->financial_year_id = $value->financial_year_id;
                $fyValue->actual_value = isset($value->actual_value) ? $value->actual_value : null;
                $fyValue->data_source_id = isset($value->data_source_id) ? $value->data_source_id : null;
                $fyValue->save();
            }

        }
    }

}
