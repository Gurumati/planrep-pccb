<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Http\Controllers\SharedService;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Planning\AdminHierarchySectMappings;
use App\Models\Planning\AnnualTarget;
use App\Models\Planning\LongTermTarget;
use App\Models\Planning\Mtef;
use App\Models\Planning\PlanChain;
use App\Models\Setup\ActivityCategory;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\GenericTarget;
use App\Models\Setup\Section;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;


class LongTermTargetController extends Controller
{

    private $refdoc;

    public function getLongTermTargetByRefDocId()
    {
        $this->setRefDocId();
        $longTermTarget = LongTermTarget::where('is_active', true)
            ->where('reference_document_id', $this->refdoc)
            ->get();
        return response()->json($longTermTarget);
    }

    public function setRefDocId()
    {
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $refDocument = SharedService::getUserStrategicPlan($financialYear);
        $this->refdoc = isset($refDocument) ? $refDocument->id : 0;
    }

    public function genericTargetsByPlanChain($planChainId)
    {
        $userSectorId = Section::find(UserServices::getUser()->section_id)->sector_id;
        $genericTargets = GenericTarget::where('plan_chain_id', $planChainId)->get();
        $genericTargets = DB::table('generic_targets as gt')
            ->join('planning_matrix_sectors as pms','gt.planning_matrix_id','pms.planning_matrix_id')
            ->where('gt.plan_chain_id', $planChainId)
            ->where('pms.sector_id',$userSectorId)
            ->select('gt.*')
            ->get();

        $data = ["genericTargets" => $genericTargets];
        return response()->json($data);
    }

    public function index()
    {
        $this->setRefDocId();
        $longTermTargets = LongTermTarget::with('planChain', 'performanceIndicators')->with('referenceDocument')->where('is_active', true)->where('reference_document_id', $this->refdoc)->get();
        return response()->json($longTermTargets);
    }

    public function indexNoAnnual()
    {
        $this->setRefDocId();
        $longTermTargets = DB::table('long_term_targets')
            ->where('reference_document_id', $this->refdoc)
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('mtef_annual_targets')
                    ->whereRaw('mtef_annual_targets.long_term_target_id = long_term_targets.id');
            })
            ->get();

        return response()->json($longTermTargets);
    }

    public function paginateByPlanChain($planChainId,$perPage)
    {
        $this->setRefDocId();
         /** @var  $perPage */
         $perPage =isset($perPage)?$perPage:Input::get("perPage", 1000);

        $targets = LongTermTarget::with('references','performanceIndicators')->where('is_active', true)
            ->where('reference_document_id', $this->refdoc)
            ->where('section_id', UserServices::getUser()->section_id)
            ->where('plan_chain_id', $planChainId)
            ->orderBy('code')
            ->select('id', 'code', 'description','plan_chain_id','performance_indicator_id','section_id')
            ->paginate($perPage);

        foreach ($targets as &$l) {

            $l->annualTargets = DB::table('mtef_annual_targets as mt')
                ->join('mtefs as m', 'm.id', 'mt.mtef_id')
                ->where('long_term_target_id', $l->id)
                ->select('mt.*', 'm.financial_year_id')
                ->get();

                $l->cofog_id = DB::table("long_term_target_references")
               ->where("long_term_target_id", $l->id)
               ->where("cofog_id","!=",null)
               ->first()->cofog_id;
        }
        return response()->json($targets);
    }


//    public function store(Request $request)
//    {
//        try {
//            $this->setRefDocId();
//           $data = json_decode($request->getContent(),true);
//
//             DB::transaction(function () use ($data) {
//                $cofog_id = $data["cofog_id"];
//                $code = null;
//                $planChain = PlanChain::find($data["plan_chain_id"]);
//                $targetType = ActivityCategory::find($data["target_type_id"]);
//               // $parent = PlanChain::where('id',$planChain->parent_id)->first();
//               $parent = DB::table("plan_chains")->where("id",$planChain->parent_id)->first();
//                $codeString = trim($parent->code) . "%";
//                $lastTargetCode = DB::table('long_term_targets')->where('code', 'like', $codeString)
//                    ->where('reference_document_id', $this->refdoc)->max('code');
//                if ($lastTargetCode != null) {
//                     $codeIndex = preg_replace("/[^0-9]/","",$lastTargetCode);
//                     $newIndex = intval($codeIndex) + 1;
//                     $newIndex = ($newIndex < 10) ? "0" . $newIndex : $newIndex;
//                     $code = $parent->code . $newIndex . $targetType->code;
//                } else {
//                    $code = $parent->code ."01".$targetType->code;
//                }
//                $sectionId = isset($data["section_id"]) ? $data["section_id"] : UserServices::getUser()->section_id;
//
//                $longTermTarget = new LongTermTarget();
//                $longTermTarget->description = $data["description"];
//                $longTermTarget->target_type_id = isset($data["target_type_id"]) ? $data["target_type_id"] : null;
//                $longTermTarget->code = $code;
//                $longTermTarget->plan_chain_id = $data["plan_chain_id"];
//                $longTermTarget->reference_document_id = $this->refdoc;
//                $longTermTarget->section_id = $sectionId;
//                $longTermTarget->performance_indicator_id = isset($data["performance_indicator_id"]) ? $data["performance_indicator_id"] : null;
//                $longTermTarget->intervention_id = isset($data["intervention"]) ? $data["intervention"]["id"] : null;
//                $longTermTarget->sector_problem_id = isset($data["sector_problem"]) ? $data["sector_problem"]["id"] : null;
//                $longTermTarget->generic_target_id = isset($data["generic_target_id"]) ? $data["generic_target_id"] : null;
//                $longTermTarget->created_by = UserServices::getUser()->section_id;
//                $longTermTarget->created_at = date('Y-m-d H:i:s');
//                $longTermTarget->is_active = true;
//                $longTermTarget->save();
//
//                if (isset($data["references"])) {
//                    $this->saveTargetReferences($longTermTarget->id,$cofog_id, $data);
//                }
//                //TODO implement check inf system configured to use annual instead of long as annual;
//
//                /**
//                 * Assumption is the system use long term target as annul target
//                 */
//                $this->saveAnnualTarget($longTermTarget);
//
//
//            });
//            $longTermTargets = LongTermTarget::with('planChain', 'performanceIndicators')->with('referenceDocument')->where('is_active', true)->where('reference_document_id', $this->refdoc)->orderBy('code')->get();
//            $message = ["successMessage" => "SUCCESSFUL_LONG_TERM_TARGET_CREATED", "longTermTargets" => $longTermTargets];
//            return response()->json($message, 200);
//        } catch (QueryException $exception) {
//            $error_code = $exception->errorInfo[1];
//            //ERROR CODE 7 CATCH Duplicate
//            if ($error_code == 7) {
//                $message = ["errorMessage" => "ERROR_LONG_TERM_TARGET_EXISTS"];
//                return response()->json($message, 400);
//            } else {
//                $message = ["errorMessage" => 'ERROR_OTHERS'];
//                return response()->json($message, 400);
//            }
//        }
//    }

    public function store(Request $request)
    {
        try {
            set_time_limit(3000);
            $this->setRefDocId();
            $data = json_decode($request->getContent(),true);

            DB::transaction(function () use ($data) {
                $cofog_id = $data["cofog_id"];
                $code = null;
                $planChain = PlanChain::find($data["plan_chain_id"]);
                $codeString = trim($planChain->code) . "%";
                $lastTargetCode = DB::table('long_term_targets')->where('code', 'like', $codeString)
                    ->where('reference_document_id', $this->refdoc)->max('code');
                if ($lastTargetCode != null) {
                    $codeIndex = str_replace($planChain->code, "", $lastTargetCode);
                    $newIndex = intval($codeIndex) + 1;
                    $newIndex = ($newIndex < 10) ? "0" . $newIndex : $newIndex;
                    $code = $planChain->code . $newIndex;
                } else {
                    $code = $planChain->code . "01";
                }
                $sectionId = isset($data->section_id) ? $data->section_id : UserServices::getUser()->section_id;
                

                $longTermTarget = new LongTermTarget();
                $longTermTarget->description = $data["description"];
                $longTermTarget->target_type_id = isset($data["target_type_id"]) ? $data["target_type_id"] : null;
                $longTermTarget->code = $code;
                $longTermTarget->plan_chain_id = $data["plan_chain_id"];
                $longTermTarget->reference_document_id = $this->refdoc;
                $longTermTarget->section_id = $sectionId;
                $longTermTarget->performance_indicator_id = isset($data["performance_indicator_id"]) ? $data["performance_indicator_id"] : null;
                $longTermTarget->intervention_id = isset($data["intervention"]) ? $data["intervention"]["id"] : null;
                $longTermTarget->sector_problem_id = isset($data["sector_problem"]) ? $data["sector_problem"]["id"] : null;
                $longTermTarget->generic_target_id = isset($data["generic_target_id"]) ? $data["generic_target_id"] : null;
                $longTermTarget->created_by = UserServices::getUser()->section_id;
                $longTermTarget->created_at = date('Y-m-d H:i:s');
                $longTermTarget->is_active = true;

                $longTermTarget->save();

                if (isset($data["references"])) {
                    $this->saveTargetReferences($longTermTarget->id,$cofog_id, $data);
                }
                //TODO implement check inf system configured to use annual instead of long as annual;

                /**
                 * Assumption is the system use long term target as annul target
                 */
                $this->saveAnnualTarget($longTermTarget);


            });
            $longTermTargets = null; // LongTermTarget::with('planChain', 'performanceIndicators')->with('referenceDocument')->where('is_active', true)->where('reference_document_id', $this->refdoc)->orderBy('code')->get();
            $message = ["successMessage" => "SUCCESSFUL_LONG_TERM_TARGET_CREATED", "longTermTargets" => $longTermTargets];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_LONG_TERM_TARGET_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }
    public function update(Request $request)
    {
        try {
            $this->setRefDocId();
            $data = json_decode($request->getContent());
            DB::transaction(function () use ($data) {
            $cofog_id = $data->cofog_id;

                $sectionId = isset($data->section_id) ? $data->section_id : UserServices::getUser()->id;

                $longTermTarget = LongTermTarget::find($data->id);
                $longTermTarget->description = $data->description;
                $longTermTarget->code = $data->code;
                $longTermTarget->plan_chain_id = $data->plan_chain_id;
                $longTermTarget->reference_document_id = $this->refdoc;
                $longTermTarget->section_id = $sectionId;
                $longTermTarget->performance_indicator_id = isset($data->performance_indicator_id) ? $data->performance_indicator_id : null;
                $longTermTarget->generic_target_id = isset($data->generic_target_id) ? $data->generic_target_id : null;
                $longTermTarget->intervention_id = isset($data->intervention) ? $data->intervention->id : null;
                $longTermTarget->sector_problem_id = isset($data->sector_problem) ? $data->sector_problem->id : null;
                $longTermTarget->created_by = UserServices::getUser()->id;
                $longTermTarget->created_at = date('Y-m-d H:i:s');
                $longTermTarget->is_active = true;
                $longTermTarget->save();

                if (isset($data->references)) {
                    $this->saveTargetReferences($longTermTarget->id, $cofog_id, $data);
                }

                /**
                 * Assumption is system is configured to use long term as annual target
                 */
                //TODO to add configuration to set use annual target instead of long and prevent long to update annual target
                $this->updateAnnualTarget($longTermTarget);


            });
            //Return language success key and all data
            $longTermTargets = LongTermTarget::with('planChain', 'performanceIndicators')->with('referenceDocument')->where('is_active', true)
                ->where('reference_document_id', $this->refdoc)->orderBy('code')->get();
            $message = ["successMessage" => "SUCCESSFUL_LONG_TERM_TARGET_UPDATED", "longTermTargets" => $longTermTargets];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_LONG_TERM_TARGET_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

    public function saveAnnualTarget($longTermTarget)
    {
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        if (isset($financialYear)) {
            $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
            $financialYearId = $financialYear->id;
            $mtef = Mtef::where('admin_hierarchy_id', $admin_hierarchy_id)
                ->where('financial_year_id', $financialYearId)
                ->first();
            $count = AnnualTarget::where('long_term_target_id', $longTermTarget->id)
                ->where('mtef_id', $mtef->id)->where('section_id', UserServices::getUser()->section_id)->count();
            if ($count < 1) {
                $annualTarget = new AnnualTarget();
                $annualTarget->description = $longTermTarget->description;
                $annualTarget->code = $longTermTarget->code;
                $annualTarget->long_term_target_id = $longTermTarget->id;
                $annualTarget->mtef_id = $mtef->id;
                $annualTarget->is_final = false;
                $annualTarget->section_id = $longTermTarget->section_id;
                $annualTarget->created_by = UserServices::getUser()->id;
                $annualTarget->created_at = date('Y-m-d H:i:s');
                $annualTarget->save();
            }
        }
    }

    public function updateAnnualTarget($longTermTarget)
    {
        $check = AnnualTarget::where('long_term_target_id', $longTermTarget->id)->count();
        if ($check > 0) {
            $annualTargets = AnnualTarget::where('long_term_target_id', $longTermTarget->id)->get();
            foreach ($annualTargets as $annualTarget) {
                $annualTarget->description = $longTermTarget->description;
                $annualTarget->code = $longTermTarget->code;
                $annualTarget->section_id = $longTermTarget->section_id;
                $annualTarget->update_by = UserServices::getUser()->id;
                $annualTarget->updated_at = date('Y-m-d H:i:s');
                $annualTarget->save();
            }
        } else {
            $this->saveAnnualTarget($longTermTarget);
        }
    }

    public function saveTargetReferences($target_id,$cofog_id, $target)
    {
        DB::table('long_term_target_references')->where('long_term_target_id', $target_id)->delete();
        if ($cofog_id) {
            DB::table('long_term_target_references')->insert(
                array(
                    "long_term_target_id" => $target_id,
                    "cofog_id" => $cofog_id
                )
            );
        }

        $reference = is_array($target)?$target["references"]:$target->references;

        foreach ($reference as $value) {
            if ($value != null)
                $valueId = is_array($value)?$value["id"]:$value->id;
                DB::table('long_term_target_references')->insert(
                array(
                    "long_term_target_id" => $target_id,
                    "reference_id" => $valueId,
                )
            );

        }
    }

    public function getSectorTargetSections($sectorId, $adminHierarchyId)
    {
        $adminHierarchy = AdminHierarchy::find($adminHierarchyId);
        $mappings = AdminHierarchySectMappings::where('admin_hierarchy_level_id', $adminHierarchy->admin_hierarchy_level_id)->where('can_target', true)->get();
        $sections = [];
        foreach ($mappings as $mapping) {
            $section = Section::where('id', $mapping->section_id)->where('sector_id', $sectorId)->first();
            if ($section) {
                $sections[] = $section->id;
            }
        }

        return $sections;
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
                DB::table('mtef_annual_targets')->where('long_term_target_id', $id)->delete();
                DB::table('long_term_target_references')->where('long_term_target_id', $id)->delete();
                DB::table('long_term_targets')->where('id', $id)->delete();
            DB::commit();
            $message = ["successMessage" => "SUCCESSFULLY_LONG_TERM_TARGET_DELETED"];
            return response()->json($message, 200);

        } catch (\Exception $exception) {
            DB::rollback();
            Log::error($exception);
            $message = ["errorMessage" => "ERROR_DELETING_TARGET_TARGET_IN_USE"];
            return response()->json($message, 500);
        }
    }

    public function refreshCode($id){
        try{
            DB::beginTransaction();
                $result = LongTermTarget::refreshCode($id);
            DB::commit();
            return $result;
        }
        catch (\Exception $exception) {
            DB::rollback();
            Log::error($exception);
            return response()->json($exception, 500);
        }
    }

}
