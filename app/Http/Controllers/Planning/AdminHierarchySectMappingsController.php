<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Models\Planning\AdminHierarchySectMappings;
use App\Models\Setup\Section;
use Illuminate\Http\Request;

class AdminHierarchySectMappingsController extends Controller
{

    public function index()
    {
        //
        $all = AdminHierarchySectMappings::with('section')
               ->get();
        return response()->json($all);
    }

    public function store(Request $request)
    {
        //
        $data = json_decode($request->getContent(), true);
        //truncate all data
        AdminHierarchySectMappings::truncate();
        //loop data object
        foreach($data as $key=>$value)
        {
            $admin_hierarchy = $value['adminHierarchyLevelId'];

            $section_levels = $value['sections'];
            foreach($section_levels as $key1=>$value1)
            {
                $section_level_id = $value1['sectionId'];

                foreach($value1['values'] as $key3=>$value3)
                {
                    if($key3 == 'can_budget')
                    {
                        $can_budget = $value3;
                    } else if($key3 == 'can_target')
                    {
                        $can_target = $value3;
                    } else if($key3 == 'in_use')
                    {
                        $in_use = $value3;
                    }
                }
                $in_use = isset($in_use)?$in_use:false;

                //try to load data
                if($in_use == 1 or $in_use != '')
                {
                    //load sections in section levels
                    $sections = $this->getAllSections($section_level_id);
                    foreach ($sections as $section)
                    {
                        $section_id = $section->id;
                        $adminMapping = new AdminHierarchySectMappings();
                        //create entry
                        $adminMapping->admin_hierarchy_level_id = $admin_hierarchy;
                        $adminMapping->section_id = $section_id;
                        $adminMapping->can_budget = isset($can_budget)?$can_budget:false;
                        $adminMapping->can_target = isset($can_target)?$can_target:false;
                        $adminMapping->save();
                    }

                }
                $in_use     = false;
                $can_target = false;
                $can_budget = false;
            }
        }

        $message = ["successMessage" => 'ADMIN_HIERARCHY_SECTION_MAPPING_UPDATED_SUCCESSFULLY','adminSectMapping'=>AdminHierarchySectMappings::all()];
        return response()->json($message, 200);
    }

    public function getAllSections($section_level_id) {
        $all = Section::where('is_active',true)
               ->where('section_level_id',$section_level_id)
               ->select('id')
               ->get();
        return $all;
    }
}
