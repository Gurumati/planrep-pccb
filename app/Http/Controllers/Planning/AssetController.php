<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Planning\Asset;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\FinancialYear;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class AssetController extends Controller {

    private $adminHierarchies = [];

    public function adminHierarchyLevel($id) {
        array_push($this->adminHierarchies, $id);
        $adminHierarchyId = AdminHierarchy::where('parent_id', $id)->get();
        foreach ($adminHierarchyId as $key => $value) {
            array_push($this->adminHierarchies, $value->id);
            //check if has children
            $count = AdminHierarchy::where('parent_id', $value->id)->count();
            if ($count > 0) {
                $this->adminHierarchyLevel($value->id);
            }
        }
    }

    public function index() {
        ini_set('max_execution_time', 300);
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $this->adminHierarchyLevel($admin_hierarchy_id);
        $all = Asset::with('transport_facility_type', 'asset_condition', 'asset_use', 'admin_hierarchy')
            ->whereIn('admin_hierarchy_id', $this->adminHierarchies)
            ->orderBy('created_at', 'desc')->get();
        $data = ["transportFacilities" => $all];
        return response()->json($data);
    }

    public function fetchAll() {
        ini_set('max_execution_time', 300);
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $this->adminHierarchyLevel($admin_hierarchy_id);
        $all = Asset::with('transport_facility_type', 'asset_condition', 'asset_use', 'admin_hierarchy')
            ->whereIn('admin_hierarchy_id', $this->adminHierarchies)
            ->orderBy('created_at', 'desc')->get();
        $data = ["transportFacilities" => $all];
        return response()->json($data);
    }

    public function getAllPaginated($perPage) {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $this->adminHierarchyLevel($admin_hierarchy_id);
        return Asset::with('transport_facility_type', 'asset_condition', 'asset_use', 'admin_hierarchy')
            ->whereIn('admin_hierarchy_id', $this->adminHierarchies)
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

    public function paginated(Request $request) {
        ini_set('max_execution_time', 300);
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        try {
            $data = json_decode($request->getContent());
            $obj = new Asset();
            $financial_year_id = FinancialYear::whereIn('status', [1, 2])->first()->id;
            $obj->financial_year_id = $financial_year_id;
            $obj->asset_name = $data->asset_name;
            $obj->registration_number = isset($data->registration_number)?($data->registration_number):null;
            $obj->parent_id = isset($data->parent_id)?($data->parent_id):null;
            $obj->date_of_acquisition = isset($data->date_of_acquisition)?($data->date_of_acquisition):null;
            $obj->used_for = isset($data->used_for)?($data->used_for):null;
            $obj->condition = isset($data->condition)?($data->condition):null;
            $obj->station = isset($data->station)?($data->station):null;
            $obj->mileage = isset($data->mileage)?($data->mileage):null;
            $obj->type = isset($data->type)?($data->type):null;
            $obj->comments = isset($data->comments)?($data->comments):null;
            $obj->ownership = isset($data->ownership)?($data->ownership):null;
            $obj->insurance_type = isset($data->insurance_type)?($data->insurance_type):null;
            $obj->admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "CREATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_number = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_number . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        try {
            $obj = Asset::find($data->id);
            $obj->asset_name = $data->asset_name;
            $obj->registration_number = isset($data->registration_number)?($data->registration_number):null;
            $obj->parent_id = isset($data->parent_id)?($data->parent_id):null;
            $obj->date_of_acquisition = isset($data->date_of_acquisition)?($data->date_of_acquisition):null;
            $obj->used_for = isset($data->used_for)?($data->used_for):null;
            $obj->condition = isset($data->condition)?($data->condition):null;
            $obj->station = isset($data->station)?($data->station):null;
            $obj->mileage = isset($data->mileage)?($data->mileage):null;
            $obj->type = isset($data->type)?($data->type):null;
            $obj->comments = isset($data->comments)?($data->comments):null;
            $obj->ownership = isset($data->ownership)?($data->ownership):null;
            $obj->insurance_type = isset($data->insurance_type)?($data->insurance_type):null;
            $obj->admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
            $obj->save();
            $all = $this->getAllPaginated($request->perPage);
            $message = ["successMessage" => "UPDATE_SUCCESS", "items" => $all];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_number = $exception->errorInfo[1];
            Log::alert(UserServices::getUser()->email . '[ERROR_CODE:' . $error_number . ']');
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }
    }

    public function delete($id) {
        $obj = Asset::find($id);
        $obj->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "DELETE_SUCCESS", "items" => $all];
        return response()->json($message, 200);
    }

    private function allTrashed() {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $this->adminHierarchyLevel($admin_hierarchy_id);
        $all = Asset::with('transport_facility_type', 'asset_condition', 'asset_use', 'admin_hierarchy')
            ->whereIn('admin_hierarchy_id', $this->adminHierarchies)
            ->orderBy('created_at', 'desc')->onlyTrashed()->get();
        return $all;
    }

    public function trashed() {
        ini_set('max_execution_time', 300);
        $all = $this->allTrashed();
        $message = ["trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function restore($id) {
        Asset::withTrashed()->where('id', $id)->restore();
        $all = $this->allTrashed();
        $message = ["successMessage" => "RESTORE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function permanentDelete($id) {
        Asset::withTrashed()->where('id', $id)->forceDelete();
        $all = $this->allTrashed();
        $message = ["successMessage" => "PERMANENT_DELETE_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }

    public function emptyTrash() {
        $trashes = Asset::orderBy('created_at', 'desc')->onlyTrashed()->get();
        foreach ($trashes as $trash) {
            $trash->forceDelete();
        }
        $all = $this->allTrashed();
        $message = ["successMessage" => "EMPTY_TRASH_SUCCESS", "trashedItems" => $all];
        return response()->json($message, 200);
    }
}
