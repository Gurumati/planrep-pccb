<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\Budgeting\BudgetingServices;
use App\Http\Services\Planning\PlanningServices;
use App\Http\Services\UserServices;
use App\Models\Budgeting\AdminHierarchyCeiling;
use App\Models\Budgeting\BudgetSubmissionLineValue;
use App\Models\Budgeting\BudgetType;
use App\Models\Planning\Activity;
use App\Models\Planning\ActivityFacility;
use App\Models\Planning\ActivityFacilityFundSource;
use App\Models\Planning\ActivityFacilityFundSourceInput;
use App\Models\Planning\ActivityFacilityFundSourceInputBreakdown;
use App\Models\Planning\ActivityFacilityFundSourceInputForward;
use App\Models\Planning\MtefAnnualTarget;
use App\Models\Planning\MtefSection;
use App\Models\Planning\Mtef;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\Facility;
use App\Models\Setup\GenericActivity;
use App\Models\Setup\Section;
use App\Models\Setup\SectionParent;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Http\Services\FinancialYearServices;
use App\Models\Execution\ActivityProjectOutput;
use App\Models\Execution\RevenueExportAccount;
use App\Models\Execution\RevenueExportTransactionItem;
use App\Models\Execution\BudgetExportTransactionItem;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Execution\BudgetExportTransaction;
use App\Models\Execution\BudgetExportAccount;
use App\Models\Execution\BudgetExportToFinancialSystem;

class ActivityController extends Controller
{
    private $sectionIds;
    private $activity;

    // get all activities excluding pe 
    public function fetchAll(Request $request)
    {
        $financial_year_id = $request->financialYearId;
        $budget_type = $request->budgetType;
        $admin_hierarchy_id = $request->adminHierarchyId;

        $activities = Activity::join('mtef_sections as ms','activities.mtef_section_id','ms.id')
        ->join('mtefs as m','ms.mtef_id','m.id')
        ->where('activities.deleted_at', null)->where('code','!=','00000000')->where('budget_type',$budget_type)
        ->where('m.financial_year_id',$financial_year_id)->where('m.admin_hierarchy_id',$admin_hierarchy_id)
        ->select('*')
        ->orderBy("activities.created_at", 'asc')
        ->get();
        return response()->json(["activities" => $activities], 200);
    }

    /**
     * @return array
     * Fix  activities with invalid codes
     */
    public function fixActivityWithInvalidCode($limit)
    {
        set_time_limit(3000);
        $activities = Activity::getActivityWithInvalidCode($limit);
        $totalProcessed = 0;
        $processed = false;
        foreach ($activities as $a) {
            if ($a->is_facility_account == false) {
                $code6Char = substr($a->code, 0, 6);
                $processed = Activity::fixCouncilAccountCode($a->id, $code6Char, $a->admin_hierarchy_id, $a->financial_year_id);
            } else if ($a->is_facility_account == true) {
                $code6Char = substr($a->code, 0, 6);
                $processed = Activity::fixFacilityCode($a->id, $code6Char, $a->admin_hierarchy_id, $a->financial_year_id);
            }
            if ($processed) {
                $totalProcessed = $totalProcessed + 1;
            }

        }

        return ["processed" => $totalProcessed, "total" => Activity::getTotalActivityWithInvalidCode(), 'act' => $activities];
    }

    public function refreshCode($activityId)
    {
        return Activity::refreshActivityCode($activityId);
    }


    public function getTotalActivityWithInvalidCode()
    {

        return ["total" => Activity::getTotalActivityWithInvalidCode()];
    }

    /**
     * Get Paginated Activities
     *
     * @param $mtefSectionId
     * @param $targetId
     * @param $budgetType
     * @return array
     */
    public function paginated(Request $request, $mtefSectionId, $targetId, $budgetType)
    {
        $facilityIds = $request->input('selectedFacilityIds');
        if (!UserServices::hasPermission(['planning.activity.' . $budgetType])) {
            return response()->json(null, 403);
        }
        if (!in_array($budgetType, BudgetType::getAll())) {
            return response()->json(['errorMessage' => 'Invalid budget type'], 400);
        }
        return ['activities' => Activity::paginated($mtefSectionId, $targetId, $budgetType,$facilityIds)];
    }

    /**
     * @param Request $request
     * @param $financialYearId
     * @param $budgetType
     * @param $adminHierarchyId
     * @param $sectionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function createOrUpdate(Request $request, $financialYearId, $budgetType, $adminHierarchyId, $sectionId)
    {
        $data = $request->all();
        $validator = Validator::make($data, Activity::rules(), []);

         /**
         * Validate if locked
         */
        $validator->after(function ($validator) use ($data) {
            if (isset($data['id']) && $data['id'] !== null) {
                if (Activity::where('id', $data['id'])->where('locked', true)->count() > 0) {
                    $validator->errors()->add('id', 'This Activity is Locked');
                }
            }
            if ($data['budget_type'] === 'CURRENT' && MtefSection::where('id', $data['mtef_section_id'])->where('is_locked', true)->count() > 0) {
                $validator->errors()->add('id', 'This Plan is Locked');
            }
        });

        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = ["errors" => $errors->all(), "errorMessage" => "ERROR_ON_CREATE_OR_UPDATE_ACTIVITY"];
            return response()->json($message, 400);
        }

        DB::beginTransaction();
        try {

            $activity = Activity::createOrUpdate($data, $financialYearId, $budgetType, $adminHierarchyId, $sectionId);
            track_activity($activity, UserServices::getUser(), 'create_activity');

            DB::commit();
            $message = ["successMessage" => "SUCCESSFUL_SAVE_ACTIVITY", 'activity' => $activity];
            return response()->json($message);
        } catch (\Exception $exception) {
            DB::rollback();
            $message = ["errorMessage" => $exception];
            return response()->json($message, 500);
        }

    }

    /**
     * @param $activityId
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse
     */
    public function getPaginatedActivityFacilities($activityId)
    {
        try {

            return ActivityFacility::paginateByActivity($activityId);
        } catch (\Exception $exception) {
            $data = ['errorMessage' => 'ERROR_GETTING_DATA'];
            return response()->json($data, 500);
        }
    }

    /**
     * Delete Activity
     * @param $id
     * @param $mtefSectionId
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id, $mtefSectionId)
    {

        try {
            DB::transaction(function () use ($id) {
                $toDeletes = ActivityFacility::where('activity_id', $id)->get();
                foreach ($toDeletes as $toDelete) {
                    $this->deleteActivityFacility($toDelete->id);
                }
                DB::table('activity_indicators')->where('activity_id', $id)->delete();
                DB::table('activity_references')->where('activity_id', $id)->delete();
                DB::table('activity_periods')->where('activity_id', $id)->delete();
                DB::table('activity_project_outputs')->where('activity_id', $id)->delete();
                $activity = Activity::find($id);
                DB::table('activities')->where('id', $id)->delete();
                track_activity($activity, UserServices::getUser(), 'delete_activity');
            }, 1);

            return response()->json(['successMessage' => 'Activity Delete Successfully'], 200);
        } catch (QueryException $e) {
            $message = ["errorMessage" => "CANT_DELETE_ACTIVITY_PROBABLY_HAS_INPUTS"];
            return response()->json($message, 400);
        }

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function disApprove($id)
    {

        $transactionTypeId = ConfigurationSetting::getValueByKey('BUDGET_DISAPPROVAL_TRANSACTION_TYPE');
        if (!isset($transactionTypeId)) {
            return response()->json(['errorMessage' => 'No budget disapproval transaction type in configuration settings'], 400);
        }

        try {
            DB::beginTransaction();
            $transactions = Activity::disApprove($id, $transactionTypeId);
            DB::commit();
            return response()->json(['successMessage' => 'Activity Disapproved Successfully', 'transactions' => $transactions], 200);

        } catch (\Exception $e) {
            DB::rollback();
            $message = ["errorMessage" => "Error Disapproving activity" ,'message'=>$e->getMessage()];
            return response()->json($message, 400);

        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function approve($id)
    {

        $transactionTypeId = ConfigurationSetting::getValueByKey('BUDGET_APPROVAL_TRANSACTION_TYPE');

        if (!isset($transactionTypeId)) {
            return response()->json(['errorMessage' => 'No budget approval transaction type in configuration settings'], 400);
        }
        try {
            DB::beginTransaction();
            $transactions = Activity::approve($id, $transactionTypeId);
            DB::commit();
            return response()->json(['successMessage' => 'Activity Approved Successfully', 'accounts' => $transactions], 200);
        } catch (\Exception $e) {
            DB::rollback();
            $message = ["errorMessage" => "Error Approving activity"];
            return response()->json($message, 400);

        }
    }

    public function toggleFacilityAccount($id, $isFacilityAccount){
        try{
            Activity::where('id',$id)->update(['is_facility_account'=>$isFacilityAccount]);
            return response()->json(['successMessage' => 'Activity Approved Successfully'], 200);
        }
        catch(\Exception $e){
            return response()->json(['errorMessage'=>'Error Updating Activity'], 500);

        }
    }


    private function getPaginatedActivitiesWithInputs($mtefSectionId, $budgetClassId, $budgetType, $facility, $fundSourceId, $activityIds, $perPage)
    {
        $budgetClassIds = [];
        if ($budgetClassId == "all") {
            $budgetCLasses = DB::table('budget_classes')->get();
            foreach ($budgetCLasses as $b) {
                $budgetClassIds[] = $b->id;
            }
        } else {
            $budgetCLass = BudgetClass::where('id', $budgetClassId)->get();
            $flatten = new Flatten();
            $budgetClassIds = $flatten->flattenBudgetClassGetIds($budgetCLass);
        }
        if ($budgetType == 'REALLOCATION') {
            $planTypes = ['REALLOCATION', 'CURRENT'];
        } else {
            $planTypes[] = $budgetType;
        }
        $perPage = isset($perPage) ? $perPage : 10;

        $withAmount = Input::get('withAmount');
        $withNoAmount = (isset($withAmount) && $withAmount == '0') ? true : false;

        $activities = ActivityFacilityFundSource::with([
            'inputs' => function ($iq) use ($budgetType) {
                $iq->where('budget_type', $budgetType);
                if ($budgetType == 'REALLOCATION')
                    $iq->doesntHave('account');
            },
            'inputs.input_comments', 'activity_facility',
            'activity_facility.activity', 'activity_facility.activity.activity_comments',
            'activity_facility.activity.annualTarget'
        ])->whereHas('activity_facility.activity', function ($b) use ($budgetClassIds, $planTypes, $mtefSectionId, $activityIds) {
            $b->where('mtef_section_id', $mtefSectionId)->whereIn('budget_class_id', $budgetClassIds)->whereIn('budget_type', $planTypes);
            if (isset($activityIds)) {
                $b->whereIn('id', $activityIds);
            }
        })->whereHas('activity_facility', function ($f) use ($facility) {
            if ($facility != 'all')
                $f->where('facility_id', $facility)->orderBy('facility_id');
        })->whereHas('activity_facility.facility', function ($fa) {
            $fa->where('is_active', true);
        })->where(function ($q) use ($fundSourceId, $withNoAmount) {
            if (isset($fundSourceId) && $fundSourceId != 'all') {
                $q->where('fund_source_id', $fundSourceId);
            }
            if ($withNoAmount) {
                $q->doesnthave('inputs');
            }
        })->orderBy('id', 'desc')->paginate($perPage);
        foreach ($activities as &$a) {
            $ceiling = 0;
            $totalUsed = 0;
            $theCeiling = BudgetingServices::getTheCeiling($a->activity_facility->activity->budget_class_id, $a->fund_source_id);
            if ($theCeiling != null) {
                if (!$theCeiling->extends_to_facility && $a->activity_facility->activity->is_facility_account) {
                    $a->wrongAccount = true;
                }
            }

            if ($a->activity_facility->activity->is_facility_account && isset($theCeiling) && $theCeiling->extends_to_facility) {
                $ceiling = BudgetingServices::getFacilityCeiling($mtefSectionId, $a->activity_facility->activity->budget_class_id, $a->fund_source_id, $a->activity_facility->facility_id);
                $totalUsed = BudgetingServices::getTotalUsedByFacilityCeiling($mtefSectionId, $budgetType, $a->activity_facility->activity->budget_class_id, $a->fund_source_id, $a->activity_facility->facility_id);
            } else {
                $ceiling = BudgetingServices::getCeiling($mtefSectionId, $a->activity_facility->activity->budget_class_id, $a->fund_source_id);
                $totalUsed = BudgetingServices::getTotalUsedByCeiling($mtefSectionId, $budgetType, $a->activity_facility->activity->budget_class_id, $a->fund_source_id);
            }

            if ($ceiling < $totalUsed) {
                $a->excededCeiling = true;
            }
        }

        return $activities;
    }

    public function getByMtefSectionAndBudgetType($mtefSectionId, $budgetType)
    {
        $code = Input::get('code');
        $perPage = Input::get('perPage',10);
        $facilityId = Input::get('facilityId');
        $targetId = Input::get('targetId');

        $activities = Activity::with([ 'activity_comments',
        'annualTarget',
        'annualTarget.longTermTarget.planChain',
        'annualTarget.longTermTarget.planChain.parent',
        'budgetClass',
        'activityCategory'])->where('mtef_section_id',$mtefSectionId)->where('budget_type',$budgetType)->where(function($filter) use($facilityId){
            if(isset($facilityId)){
                $filter->whereHas('activity_facilities', function($af) use($facilityId){
                    $af->where('facilityId',$facilityId);
                });
            }
        })->paginate($perPage);
        $data = ['activities' => $activities];
        return response()->json($data);
    }

    public function saveActivityFacilities($activity_id, $act, $plaType)
    {
        $userId = UserServices::getUser()->id;
        /**Clean facilities*/
        $Ids = [];
        foreach ($act->activity_facilities as $actfac) {
            if (isset($actfac->id)) {
                $Ids[] = $actfac->id;
            }
        }
        $toDeletes = ActivityFacility::where('activity_id', $activity_id)->whereNotIn('id', $Ids)->get();
        foreach ($toDeletes as $toDelete) {
            $this->deleteActivityFacility($toDelete->id);
        }
        foreach ($act->activity_facilities as $actfac) {
            $activityFacility = new ActivityFacility();
            $id = (isset($actfac->id) ? $actfac->id : null);
            if ($id != null) {
                $activityFacility = ActivityFacility::find($id);
            }
            $activityFacility->activity_id = $activity_id;
            $activityFacility->facility_id = $actfac->facility->id;
            $activityFacility->created_by = $userId;
            $activityFacility->updated_by = $userId;
            $activityFacility->save();
            if ($id != null) {
                track_activity($activityFacility, UserServices::getUser(), 'update_activity_facility');
            } else {
                track_activity($activityFacility, UserServices::getUser(), 'create_activity_facility');
            }

            $this->saveActivityFundSources($activityFacility->id, $actfac, $plaType);
        }
    }

    public function saveActivityFundSources($activityFacilityId, $actfac, $planType)
    {
        foreach ($actfac->activity_facility_fund_sources as $f) {
            $facilityFundSource = ActivityFacilityFundSource::where('activity_facility_id', $activityFacilityId)->where('fund_source_id', $f->id)->first();
            if ($facilityFundSource == null) {
                $facilityFundSource = new ActivityFacilityFundSource();
            }
            $facilityFundSource->activity_facility_id = $activityFacilityId;
            $facilityFundSource->fund_source_id = $f->id;
            $facilityFundSource->save();
            track_activity($facilityFundSource, UserServices::getUser(), 'create_activity_facility_fund_source');

            if (isset($f->inputs)) {
                $this->saveActivityInputs($facilityFundSource->id, $f, $planType);
            }
        }
    }

    public function addFacilityForActivity(Request $request, $activityId)
    {
        $data = $request->all();
        $facilityId = $data['facility']['id'];
        $projectOutputId = isset($data['projectOutputId'])?$data['projectOutputId']:null;
        $outputValue = isset($data['outputValue'])?$data['outputValue']:null;
        $activityFacilityToAdd = [];
        /** if all facilities */
        if ($facilityId === '%') {
            $activity = Activity::find($activityId);
            $idsToExclude = ActivityFacility::where('activity_id', $activityId)->pluck('facility_id');
            $idsToExcludeString = "(0,";
            foreach ($idsToExclude as $teId) {
                $idsToExcludeString = $idsToExcludeString . $teId . ",";
            }
            $idsToExcludeString = rtrim($idsToExcludeString, ',') . ")";

            $activityFacilityToAdd = Facility::getAllByPlanningSection($activity->mtef_section_id, $activity->is_facility_account, $idsToExcludeString);
        } else {

            array_push($activityFacilityToAdd, $data['facility']);
        }

        foreach ($activityFacilityToAdd as $fac) {
            $facId = 0;
            if (isset($fac->id)) {
                $facId = $fac->id;
            }
            if (!is_object($fac) && isset($fac['id'])) {
                $facId = $fac['id'];
            }
            $facilityData = ['activity_id' => $activityId, 'facility_id' => $facId,'project_output_id'=>$projectOutputId,'output_value'=>$outputValue];
            $actFacId = ActivityFacility::addFacility($facilityData);
            foreach ($data['activity_facility_fund_sources'] as $f) {
                $fundSourceData = array_merge(['activity_facility_id' => $actFacId, 'fund_source_id' => $f['id']]);
                ActivityFacilityFundSource::addFundSource($fundSourceData);
            }
        }
        $data = ["successMessage" => "FACILITIES_ADDED_SUCCESSFUL"];
        response()->json($data);

    }

    public function deleteFacilityFromActivity(Request $request, $activityFacilityId)
    {
        try {
            DB::beginTransaction();
            ActivityFacility::permanentDelete($activityFacilityId);
            DB::commit();
            $data = ["successMessage" => "DELETE_SUCCESSFUL"];
            response()->json($data);
        } catch (\Exception $exception) {
            DB::rollback();
            $data = ["errorMessage" => "ERROR_DELETING"];
            response()->json($data, 500);
        }
    }

    public function deleteAllFacilityFromActivity(Request $request, $activityId)
    {

        DB::beginTransaction();
        try {
            $toggleAccountType = Input::get("toggleAccountType");
            if (isset($toggleAccountType) && $toggleAccountType == 1) {
                $activity = Activity::find($activityId);
                if ($activity->is_facility_account) {
                    $activity->is_facility_account = false;
                } else {
                    $activity->is_facility_account = true;
                }
                $activity->save();
                track_activity($activity, UserServices::getUser(), action_name());
            }

            $activityFacilities = ActivityFacility::where('activity_id', $activityId)->get();
            foreach ($activityFacilities as $actf) {
                ActivityFacility::permanentDelete($actf->id);
            }
            DB::commit();
            $data = ["successMessage" => "DELETE_SUCCESSFUL"];
            response()->json($data);
        } catch (\Exception $exception) {
            DB::rollback();
            $data = ["errorMessage" => "ERROR_DELETING"];
            response()->json($data, 500);
        }
    }

    public function getActivitiesWithInput($mtefSectionId, $budgetClassId)
    {
        $planType = Input::get('planType');
        $perPage = Input::get('perPage');
        $facility = Input::get('facility');
        $fundSourceId = Input::get('fundSourceId');
        $activityIds = json_decode(Input::get('activityIds'));
        $theFacility = new \stdClass();
        if ($facility != null && $facility != 'all') {
            $theFacility = DB::table('facilities')->where('id', $facility)->first();
        } else {
            $theFacility->id = 'all';
            $theFacility->name = 'All Facilities';
        }

        $activities = $this->getPaginatedActivitiesWithInputs($mtefSectionId, $budgetClassId, $planType, $facility, $fundSourceId, $activityIds, $perPage);
        $data = ['activities' => $activities, 'facility' => $theFacility];
        return response()->json($data);
    }

    private function getMtefSection($mtefSectionId)
    {
        $mtseSection = null;
        $mtseSection = MtefSection::with([
            'mtefSectionComments',
            'mtef',
            'mtef.admin_hierarchy',
            'mtef.financial_year',
            'decision_level',
            'decision_level.next_decisions',
            'section'
        ])->where('id', $mtefSectionId)->select('id', 'decision_level_id', 'is_locked', 'mtef_id', 'section_id')->first();

        return $mtseSection;
    }

    public function getPlanningData(Request $request,$mtefSectionId,$targetId)
    {
        try {
            $planningService = new PlanningServices();
            $data = $planningService->getPlanningData($mtefSectionId,$targetId);
            return response()->json($data);
        } catch (QueryException $exception) {
            $message = ['errorMessage' => 'ERROR_LOADING_DATA'];
            return response()->json($message, 500);
        }
    }

    public function getGenericActivitiesByTarget($targetId)
    {
        $target = DB::table('long_term_targets as lt')
        ->join('mtef_annual_targets as at', 'lt.id', 'at.long_term_target_id')
        ->where('at.id', $targetId)
        ->select('lt.*')->first();
        $genericActivities = [];
        if ($target != null) {
            $genericActivities = GenericActivity::with('generic_sector_problem', 'budget_class', 'intervention')
            ->where('generic_target_id', $target->generic_target_id)->get();

        }
        $data = ["genericActivities" => $genericActivities];
        return response()->json($data);
    }

    public function getCeilings($mtefSectionId)
    {
        $flatten = new Flatten();

        $mtefSection = DB::table('mtef_sections')->find($mtefSectionId);
        $mtef = DB::table('mtefs')->find($mtefSection->mtef_id);

        $adminHierarchyId = $mtef->admin_hierarchy_id;
        $sectionId = $mtefSection->section_id;

        $userSection = Section::where('id', $sectionId)->get();
        $sectorIds = $flatten->flattenSectionWithChildrenGetSectors($userSection);

        $ceilings = AdminHierarchyCeiling::with('ceiling', 'ceiling.gfs_code')
            ->whereHas('ceiling.sectors', function ($query) use ($sectorIds) {
                $query->whereIn('sectors.id', $sectorIds);
            })
            ->whereHas('ceiling', function ($q) {
                $q->where('is_active', true);
            })
            ->where('admin_hierarchy_id', $adminHierarchyId)
            ->where('section_id', $sectionId)
            ->where('is_facility', false)
            ->where('financial_year_id', $mtef->financial_year_id)->select('admin_hierarchy_id', 'amount', 'ceiling_id')->get();

        return $ceilings;
    }

    public function getByMtefSectionId($mtefSectionId)
    {
        $withCeiling = Input::get('withCeiling');
        $mtseSection = $this->getMtefSection($mtefSectionId);
        $ceilings = [];
        if (isset($withCeiling) && $withCeiling)
            $ceilings = $this->getCeilings($mtefSectionId);
        $data = ["plan" => $mtseSection, "ceilings" => $ceilings];
        return response()->json($data);
    }

    public function getAnnualTargets($mtefId)
    {

        $section_id = UserServices::getUser()->section_id;
        $section = SectionParent::with('parent')->where('id', $section_id)->select('id', 'parent_id')->get();
        $x = (array)$section;
        $this->sectionIds = array();
        $this->flatten($x);

        $targets = MtefAnnualTarget::with('longTermTarget', 'longTermTarget.planChain')->whereIn('section_id', $this->sectionIds)->where('mtef_id', $mtefId)->get();

        return response()->json($targets);
    }

    private function flatten($array)
    {

        foreach ($array as $value) {
            $this->sectionIds[] = $value[0]->id;
            if (sizeof($value[0]->parent) > 0) {
                $this->flatten((array)$value[0]->parent);
            }
        }
        return $this->sectionIds;
    }

    private function deleteActivityFacility($id)
    {

        DB::transaction(function () use ($id) {
            $activityFacilitiesFundSource = DB::table('activity_facility_fund_sources')->where('activity_facility_id', $id)->get();
            foreach ($activityFacilitiesFundSource as $aff) {
                $this->deleteInputByFundSource($aff->id);
            }
            DB::table('activity_facility_fund_sources')->where('activity_facility_id', $id)->delete();
            DB::table('activity_facilities')->where('id', $id)->delete();
        }, 1);

    }

    private function deleteInputByFundSource($affId)
    {
        $inputs = DB::table('activity_facility_fund_source_inputs')->where('activity_facility_fund_source_id', $affId)->get();
        foreach ($inputs as $in) {
            $this->deleteInputImpl($in->id);
        }
    }

    public function deleteInput($id, $mtefSectionId)
    {
        try {
            $this->deleteInputImpl($id);
            $mtseSection = $this->getMtefSection($mtefSectionId);
            $message = ["successMessage" => "SUCCESSFUL_DELETE_ACTIVITIES", 'mtefSection' => $mtseSection];
            return response()->json($message, 200);
        } catch (QueryException $e) {
            $message = ["errorMessage" => "CANT_DELETE_ACTIVITY_PROBABLY_HAS_INPUTS"];
            return response()->json($message, 400);
        }
    }

    public function deleteInputImpl($id)
    {
        DB::transaction(function () use ($id) {
            $breakDowns = ActivityFacilityFundSourceInputBreakdown::where('activity_facility_fund_source_input_id', $id)->get();
            ActivityFacilityFundSourceInputBreakdown::deleteAndTrack($breakDowns);

            $forwards = ActivityFacilityFundSourceInputForward::where('activity_facility_fund_source_input_id', $id)->get();
            ActivityFacilityFundSourceInputForward::deleteAndTrack($forwards);

            $submissionLines = BudgetSubmissionLineValue::where('activity_facility_fund_source_input_id', $id)->get();
            BudgetSubmissionLineValue::deleteAndTrack($submissionLines);

            $inputs = ActivityFacilityFundSourceInput::where('id', $id)->get();
            ActivityFacilityFundSourceInput::deleteAndTrack($inputs);

        });
    }

    public function search(Request $request)
    {
        $query = $request->get('query');
        $activities = Activity::searchByName($query);
        return $activities;
    }

    /** get activity facility project */
    public function getActivityFacilityProject(Request $request){
        $activity_id = $request->activity_id;
        $facility_id = $request->facility_id;

        $result = Activity::with(['activityProjectOutputs'=>function($q) use($activity_id, $facility_id){
                          $q->where('activity_id', $activity_id)
                            ->where('facility_id', $facility_id);
                  }])->where('id', $activity_id)->get();
        return  response()->json(['items'=>$result]);
    }

    /** get activities with no project output */
    public function projectOutputFilled(Request $request){
         //get admin_hierachy
         $admin_hierarchy_id = isset($request->admin_hierarchy_id)?$request->admin_hierarchy_id:null;
         //get financial year
 //        $financial_year_id = $request->financial_year_id;
         $budget_type = $request->budget_type;
         $financial_year = FinancialYearServices::getPlanningFinancialYear();
 //        $financial_year = ConfigurationSetting::getValueByKey('AUTO_FINANCIAL_YEAR_TO_REPORT');
         $data = null;
         if($admin_hierarchy_id != null){
             $data = ActivityProjectOutput::projectOutputFilled($admin_hierarchy_id, $financial_year->id,$budget_type);
 //          $data = ActivityProjectOutput::projectOutputFilled($admin_hierarchy_id, $financial_year_id,$budget_type);
         }
         return response()->json(['items'=>$data]);
    }

    /** download excel */
    public function downloadProjectOutputFilled(Request $request){
        $admin_hierarchy_id = isset($request->admin_hierarchy_id)?$request->admin_hierarchy_id:null;
        //$financial_year = FinancialYearServices::getPlanningFinancialYear();
      $financial_year = $request->financial_year_id;
      $budget_type = $request->budget_type;

            $result = DB::select("SELECT s.code as section_code, bc.name as budget_class,
                                        s.name as section, ac.code, ac.description,ac.budget_type
                                    FROM
                                        mtefs m
                                        join mtef_sections ms on ms.mtef_id = m.id
                                        join activities ac on ac.mtef_section_id = ms.id
                                        join activity_facilities af on af.activity_id = ac.id
                                        join facilities f on f.id = af.facility_id
                                        join budget_classes bc on bc.id = ac.budget_class_id
                                        join budget_classes bcc on bcc.id = bc.parent_id
                                        join sections s on s.id = ms.section_id
                                        left join activity_project_outputs ao on ao.activity_id = ac.id
                                        where
                                        bcc.code = '200' and m.admin_hierarchy_id = $admin_hierarchy_id
                                        and m.financial_year_id = $financial_year  and f.is_active = true
                                        and ao.id is null and ac.deleted_at is null and ac.budget_type='$budget_type'
                                   ORDER BY s.code");
        $array = array();
        foreach($result as $item){
            $data = array();
            $data['SECTION_CODE'] = $item->section_code;
            $data['SECTION'] =  $item->section;
            $data['SUB_BUDGET_CLASS'] = $item->budget_class;
            $data['ACTIVITY_CODE'] =  $item->code;
            $data['NAME'] = $item->description;
            $data['BUDGET_TYPE'] = $item->budget_type;
            array_push($array, $data);
        }
        Excel::create('BUDGET_REALLOCATION' . time(), function ($excel) use ($array) {
            $excel->sheet('DATA', function ($sheet) use ($array) {
                $sheet->fromArray($array);
            });
        })->export('csv');
    }

        /** Disapprove budget by system
     *this function disapproves council budget based on selected financial year and financial system code
     */
    public function disApproveBySystem($adminHierarchyId, $financialYearId, $budgetType, $financialSystem)
    {
        $transactionTypeId = ConfigurationSetting::getValueByKey('BUDGET_DISAPPROVAL_TRANSACTION_TYPE');
        if (!isset($transactionTypeId)) {
            return response()->json(['errorMessage' => 'No budget disapproval transaction type in configuration settings'], 400);
        }

        try {
            DB::beginTransaction();
            $this->disApproveAll($adminHierarchyId, $financialYearId, $budgetType, $financialSystem, $transactionTypeId);
            DB::commit();
            $transactions = 0;
            return response()->json(['successMessage' => 'Budget Disapproved Successfully', 'transactions' => $transactions], 200);
        } catch (\Exception $e) {
            DB::rollback();
            $message = ["errorMessage" => "Error Disapproving Budget", 'message' => $e->getMessage()];
            //return response()->json($message, 400);
            return $e;
        }
    }
    /** Re-approve budget by system
     *this function re-approves disapproved council budget based on selected financial year and financial system code
     */
    public function approveBySystem($adminHierarchyId, $financialYearId, $budgetType, $financialSystem)
    {
    }

    public function disApproveAll($adminHierarchyId, $financialYearId, $budgetType, $financialSystem, $transactionTypeId)
    {
        $inputIds = ActivityFacilityFundSourceInput::getInputIds($adminHierarchyId, $financialYearId, $financialSystem);

        $activityIds = DB::table('mtefs as m')->join('mtef_sections as ms', 'm.id', 'ms.mtef_id')->join('activities as a', 'ms.id', 'a.mtef_section_id')->join('activity_facilities as af', 'a.id', 'af.activity_id')->join('activity_facility_fund_sources as affs', 'af.id', 'affs.activity_facility_id')->join('activity_facility_fund_source_inputs as affsi', 'affs.id', 'affsi.activity_facility_fund_source_id')
            ->join('budget_export_accounts as bea', 'affsi.id', 'bea.activity_facility_fund_source_input_id')
            ->where('m.admin_hierarchy_id', $adminHierarchyId)
            ->where('m.financial_year_id', $financialYearId)
            ->where('a.budget_type', 'APPROVED')
            ->where('bea.financial_system_code', $financialSystem)
            ->pluck('a.id')
            ->toArray();
        
        $ceilingIds = DB::table('admin_hierarchy_ceilings as ahc')
            ->join('ceilings as c','c.id','ahc.ceiling_id')
            ->where('ahc.admin_hierarchy_id', $adminHierarchyId)
            ->where('ahc.financial_year_id', $financialYearId)
            ->where('ahc.budget_type', 'APPROVED')
            ->where('c.is_active', true)
            ->where('ahc.amount','>', 0)
            ->where('ahc.is_approved',true)
            ->pluck('ahc.id')
            ->toArray();

        $mtefId = DB::table('mtefs as m')
            ->where('m.admin_hierarchy_id', $adminHierarchyId)
            ->where('m.financial_year_id', $financialYearId)
            ->where('m.locked',true)
            ->where('m.is_final',true)
            ->pluck('m.id')
            ->toArray();

        $budgetExportQuery = BudgetExportAccount::whereIn('activity_facility_fund_source_input_id', $inputIds);
        $revenueExportAccountIds =  DB::table('revenue_export_accounts as rea')
        ->where('rea.admin_hierarchy_id', $adminHierarchyId)
        ->where('rea.financial_year_id', $financialYearId)
        ->pluck('rea.id')
        ->toArray();

        try {
            DB::beginTransaction();
            $budgetExportAccountIds = $budgetExportQuery->pluck('id')->toArray();
            $budgetExportTransactionItemIds = BudgetExportTransactionItem::whereIn('budget_export_account_id', $budgetExportAccountIds)->pluck('id')->toArray();
            $revenueExportTransactionItemIds = RevenueExportTransactionItem::whereIn('revenue_export_account_id', $revenueExportAccountIds)->pluck('id')->toArray();
            BudgetExportToFinancialSystem::whereIn('budget_export_transaction_item_id', $budgetExportTransactionItemIds)->forceDelete();
            BudgetExportToFinancialSystem::whereIn('revenue_export_transaction_item_id', $revenueExportTransactionItemIds)->forceDelete();
            BudgetExportTransactionItem::whereIn('id', $budgetExportTransactionItemIds)->forceDelete();
            RevenueExportTransactionItem::whereIn('id', $revenueExportTransactionItemIds)->forceDelete();
            DB::table('aggregate_facility_budget_export_accounts')->whereIn('budget_export_account_id', $budgetExportAccountIds)->delete();
            BudgetExportAccount::whereIn('id', $budgetExportAccountIds)->forceDelete();
            RevenueExportAccount::whereIn('id', $revenueExportAccountIds)->forceDelete();

            foreach ($ceilingIds as $id) {

                AdminHierarchyCeiling::where('id', $id)->update(['is_approved' => false, 'budget_type' => 'CURRENT']);
            }

            foreach ($activityIds as $id) {

                Activity::where('id', $id)->update(['is_final' => false, 'budget_type' => 'CURRENT']);
            }

            foreach ($inputIds as $id) {
                ActivityFacilityFundSourceInput::where('id', $id)->update(['budget_type' => 'CURRENT']);
            }

            Mtef::where('id', $mtefId)->update(['is_final' => false, 'locked' => false]);


            DB::commit();
            return ['budgetExportAccounts' => 'Deleted'];
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollback();
            $budgetExportAccounts = $budgetExportQuery->get();
            $transaction = BudgetExportTransaction::create([
                'description' => 'DIS APPROVE ACTIVITY',
                'control_number' => time(),
                'created_by' => UserServices::getUser()->id,
                'budget_transaction_type_id' => $transactionTypeId
            ]);

            foreach ($budgetExportAccounts as $a) {
               BudgetExportTransactionItem::create([
                    'budget_export_transaction_id' => $transaction->id,
                    'budget_export_account_id' => $a->id,
                    'amount' => $a->amount,
                    'is_credit' => false
                ]);
                $a->amount = 0;
                $a->save();
            }

            foreach ($activityIds as $id) {

                Activity::where('id', $id)->update(['is_final' => false, 'budget_type' => 'CURRENT']);
            }            
            return ['budgetExportAccounts' => $budgetExportAccounts, 'exception' => $e];
        }
    }

}
