<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\Budgeting\BudgetExportAccountService;
use App\Http\Services\Budgeting\ScrutinizationService;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Budgeting\Scrutinization;
use App\Models\Planning\Activity;
use App\Models\Planning\ActivityFacility;
use App\Models\Planning\ActivityFacilityFundSource;
use App\Models\Planning\ActivityFacilityFundSourceInput;
use App\Models\Planning\AnnualTarget;
use App\Models\Planning\Mtef;
use App\Models\Planning\MtefComment;
use App\Models\Planning\MtefSection;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\DecisionLevel;
use App\Models\Setup\DecisionLevelDTO;
use App\Models\Setup\FundSource;
use App\Models\Setup\Section;
use App\Models\Setup\SectionLevelWithNextBudgetLevelDTO;
use Doctrine\DBAL\Query\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Input\Input as SymfonyInput;

class MtefController extends Controller
{

    protected $user;
    private $sectionIds;
    private $response;

    public function __construct()
    {

    }

    /**
     * Get plans (MtefSections) filtered by param bellow
     *
     * @param $budgetType
     * @param $financialYearId
     * @param $adminHierarchyId
     * @param $sectionId
     * @return array(MtefSection)
     */
    public function filter($budgetType, $financialYearId, $adminHierarchyId, $sectionId)
    {
        try {
            // $sectionIds = Cache::rememberForever('SECTION_IDS_WITH_CHILDREN_' . $sectionId, function () use ($sectionId) {
                $sections = Section::with('childSections')
                ->where('id', $sectionId)->get();
                $flatten = new Flatten();
                $sectionIds = $flatten->flattenSectionWithChild($sections);
            //  });

             $ids =  DB::table('admin_hierarchy_sections as ahs')
            ->join('sections as s','s.id','=','ahs.section_id')
            ->join('section_levels as sl','sl.id','=','s.section_level_id')
            ->where('sl.hierarchy_position',4)
            ->where('ahs.admin_hierarchy_id',$adminHierarchyId)
            ->whereIn('s.id',$sectionIds)
            ->pluck('s.id');

             return response()->json(['plans' => MtefSection::filter($budgetType, $financialYearId, $adminHierarchyId, $ids)]);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['errorMessage' => 'ERROR_GETTING_PLANS']);
        }

    }

    public function getCurrentFinancialYearPlans()
    {

        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $perPage = Input::get('perPage');
        $perPage = isset($perPage) ? $perPage : 10;
        $mtefs = DB::table('mtefs as m')->join('admin_hierarchies as a', 'a.id', 'm.admin_hierarchy_id')
            ->join('decision_levels as l', 'l.id', 'm.decision_level_id')
            ->where('financial_year_id', $financialYear->id)
            ->select('m.id as mtef_id', 'a.id as admin_hierarchy_id', 'm.financial_year_id', 'a.name as admin_hierarchy', 'l.name as decision_level', 'm.is_final')
            ->orderBy('a.name')
            ->paginate($perPage);
        foreach ($mtefs as &$mtef) {
            $mtef->ceilingStatus = DB::select("Select CASE WHEN fu.can_project THEN 'OWN_SOURCE' ELSE 'OTHER' END as fundtype, CASE WHEN adc.is_approved = true THEN 'APPROVED' ELSE 'NOT_APPROVED' END as status, COALESCE(count(*),0) as total
                                              from admin_hierarchy_ceilings as adc
                                              join sections as sec on sec.id = adc.section_id
                                              join section_levels as secl on secl.id = sec.section_level_id
                                              join ceilings as ce on ce.id = adc.ceiling_id
                                              join gfs_codes as gfs on gfs.id = ce.gfs_code_id
                                              join gfscode_fundsources gfcf ON gfcf.gfs_code_id = gfs.id
                                              join fund_sources as fu on fu.id = gfcf.fund_source_id
                                              where secl.hierarchy_position = 1
                                              and ce.is_active = true
                                              and adc.amount > 0
                                              and adc.financial_year_id=" . $financialYear->id . "
                                              and adc.admin_hierarchy_id=" . $mtef->admin_hierarchy_id . "
                                              group by fundtype, status
                                              order by fundtype, status");
        }
        $message = ["plans" => $mtefs];
        return response()->json($message);
    }

    public function getSectionBudget(Request $request, $adminHierarchyId, $sectionId, $planType)
    {
        $status = $request->input('scrutinizationStatus');
        if ($status == 0) {
            DB::table('scrutinizations')->where('id', $request->input('scrutinizationId'))->update(array('status' => 1));
        }
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $inputs = ActivityFacilityFundSourceInput::with(
            'activityFacilityFundSource',
            'activityFacilityFundSource.activityFacility',
            'activityFacilityFundSource.activityFacility.activity',
            'activityFacilityFundSource.activityFacility.activity.mtefSection',
            'activityFacilityFundSource.activityFacility.activity.mtefSection.mtef',
            'activityFacilityFundSource.activityFacility.activity.annualTarget'
        )
            ->whereHas('activityFacilityFundSource.activityFacility.activity.mtefSection', function ($query) use ($sectionId) {
                $query->where('section_id', $sectionId);
            })
            ->whereHas('activityFacilityFundSource.activityFacility.activity.mtefSection.mtef', function ($query) use ($adminHierarchyId, $planType, $financialYear) {
                $query->where('admin_hierarchy_id', $adminHierarchyId)
                    ->where('financial_year_id', $financialYear->id)->where('plan_type', $planType);
            })->get();

        $message = ["budget" => $inputs];
        return response()->json($message);
    }

    public function index()
    {
        $all = Mtef::with('financial_year')->with('admin_hierarchy')->with('decision_level')->get();
        return response()->json($all);
    }

    public function adminHierarchyMtefs()
    {
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;
        $all = Mtef::with('financial_year', 'admin_hierarchy', 'decision_level')
            ->where('admin_hierarchy_id', $admin_hierarchy_id)->get();
        return response()->json($all);
    }

    public function sectionBudgetDecisionLevel()
    {
        $sections = Section::with('childSections')->where('id', UserServices::getUser()->section_id)->get();
        $this->sectionIds = array();
        $flatten = new Flatten();
        $sectionLevelIds = $flatten->flattenSectionGetLevels($sections);
        $sectionDecisionLevels = SectionLevelWithNextBudgetLevelDTO::whereIn('id', $sectionLevelIds)->where('hierarchy_position', DB::table('section_levels')->whereNull('deleted_at')->max('hierarchy_position'))->select('id', 'name', 'next_budget_level')->first();
        return response()->json($sectionDecisionLevels);
    }

    public function MainBudgetDecisionLevels()
    {
        $adminHierarchy = AdminHierarchy::find(UserServices::getUser()->admin_hierarchy_id);
        $decisionLevels = DecisionLevelDTO::whereHas('adminHierarchyLevels', function ($query) use ($adminHierarchy) {
            $query->where('admin_hierarchy_level_id', $adminHierarchy->admin_hierarchy_level_id);
        })->where('is_default', true)->first();
        return response()->json($decisionLevels);
    }

    public function forwardMtefByUser(Request $request)
    {
        try {

            DB::transaction(function () use ($request) {
                $mtefComment = json_decode($request->getContent());
                $scrutinizationService = new ScrutinizationService();
                $submitedMtef = $scrutinizationService->getSubmittedHierarchies();
                $decisionLevel = DecisionLevel::with('next_decision_level')->where('id', UserServices::getUser()->decision_level_id)->first();
                $comments = isset($mtefComment->comments) ? $mtefComment->comments : '';
                foreach ($submitedMtef as $mtef) {
                    if (isset($decisionLevel) && $decisionLevel->next_decision_level != null) {
                        $scrutinData = $scrutinizationService->getAllScrutinizationDataByAdmin($mtef->admin_hierarchy_id);
                        foreach ($scrutinData as $data) {
                            $object = Scrutinization::find($data->id);
                            $object->status = 2;
                            $object->save();
                        }

                        $this->moveMtef($mtef->id, $decisionLevel->next_decision_level->id, $decisionLevel->id, $comments, $mtefComment->direction);
                    }
                }
            });

            $message = ["successMessage" => "SUCCESSFUL_FORWARD_BUDGET"];
            return response()->json($message);
        } catch (QueryException $e) {
            $message = ["errorMessage" => "ERROR_FORWARD_BUDGET", "exception" => $e];
            return response()->json($message, 400);
        }


    }

    public function returnMtefToDecisionLevel(Request $request)
    {
        try {

            $data = json_decode($request->getContent());
            DB::transaction(function () use ($data) {
                $scrutinService = new ScrutinizationService();
                $financialYear = FinancialYearServices::getPlanningFinancialYear();

                $lastMovement = DB::table('mtef_comments as mc')->join('mtefs as m', 'm.id', 'mc.mtef_id')->where('m.admin_hierarchy_id', $data->adminHierarchyId)->where('m.financial_year_id', $financialYear->id)->where('mc.to_decision_level', UserServices::getUser()->decision_level_id)->where('mc.direction', 1)->orderBy('mc.created_at', 'desc')->select('mc.*')->first();
                if ($lastMovement) {

                    $scrutinData = $scrutinService->getAllScrutinizationData($data->adminHierarchyId);

                    $mtefCommentId = $this->moveMtef(
                        $lastMovement->mtef_id,
                        $lastMovement->from_decision_level,
                        $lastMovement->to_decision_level,
                        $data->comments,
                        $data->direction
                    );

                    foreach ($scrutinData as $c) {
                        $mtefSection = DB::table('mtef_sections')->where('mtef_id', $lastMovement->mtef_id)->where('section_id', $c->section_id)->first();
                        if ($mtefSection) {
                            DB::table('mtef_section_comments')->insert(array(
                                "mtef_section_id" => $mtefSection->id,
                                "mtef_comment_id" => $mtefCommentId,
                                "from_section_level" => $mtefSection->section_level_id,
                                "to_section_level" => $mtefSection->section_level_id,
                                "comments" => $c->comments,
                                "direction" => $data->direction
                            ));
                        }
                        DB::table('scrutinizations')->where('id', $c->id)->update(array('status' => 2));
                    }

                    $decisionLevel = DecisionLevel::find($lastMovement->from_decision_level);
                    $message = ["successMessage" => "SUCCESSFUL_RETURNED_BUDGET", "to" => $decisionLevel];
                    $this->response = response()->json($message);
                } else {
                    $message = ["errorMessage" => "ERROR_FORWARD_BUDGET", "exception" => "NO_FROM"];
                    $this->response = response()->json($message, 400);
                }
            });

            return $this->response;



        } catch (QueryException $e) {
            $message = ["errorMessage" => "ERROR_FORWARD_BUDGET", "exception" => $e];
            return response()->json($message, 400);
        }
    }

    public function moveMtef($mtefId, $toDecisionLevelId, $fromDecisionLevelId, $comments, $direction)
    {
        $mtefComment = new MtefComment();
        DB::transaction(function () use ($mtefComment, $mtefId, $toDecisionLevelId, $fromDecisionLevelId, $comments, $direction) {
            DB::table('mtefs')->where('id', $mtefId)->update(array(
                'decision_level_id' => $toDecisionLevelId
            ));

            $mtefComment->mtef_id = $mtefId;
            $mtefComment->from_decision_level = $fromDecisionLevelId;
            $mtefComment->to_decision_level = $toDecisionLevelId;
            $mtefComment->comments = $comments;
            $mtefComment->direction = $direction;
            $mtefComment->save();

        });
        return $mtefComment->id;
    }

    public function approveMtef(Request $request)
    {
        $mtefId = $request->input('mtefId');
        $budget_type = $request->input('budget_type');

        try {

            DB::transaction(function () use ($mtefId, $budget_type) {

                $mtef = Mtef::where('id', $mtefId)->first();
                $mtefSections = MtefSection::where('mtef_id', $mtef->id)->get();
                foreach ($mtefSections as $mtefSection) {
                    $mtefSection->is_locked = true;
                    $mtefSection->save();
                }
                $mtef->is_final = true;
                $mtef->locked = true;
                $mtef->save();
                $financialYearId = $mtef->financial_year_id;
                //lock activities
                $this->lockActivities($mtefId, $budget_type, true);
                BudgetExportAccountService::createBudgetAccounts($mtef->admin_hierarchy_id, $mtef->financial_year_id, null, $budget_type);

                if ($budget_type == 'CURRENT') {
                    $finalType = 'APPROVED';
                    Activity::changeBudgetType($mtefId, $budget_type, 'APPROVED');
                }
                else{
                    $finalType = $budget_type;
                    Activity::changeBudgetType($mtefId, $budget_type, $budget_type);
                }
                BudgetExportAccountService::createActivities($mtef,  $finalType);
            });

            //track activity
            $mtef = Mtef::find($mtefId);
            track_activity($mtef, UserServices::getUser(), 'approve_budget');
            $message = ["successMessage" => "SUCCESSFUL_APPROVE_BUDGET"];
            return response()->json($message);
        } catch (QueryException $exception) {
            $error = ["errorMessage" => "ERROR_APPROVING_BUDGET"];
            return response()->json($error, 500);
        }

    }

    public function createFfarsActivities($adminHierarchyCode, $financialYearId, $budgetType){
        $adminHierarchyId = AdminHierarchy::where('code','=',$adminHierarchyCode)->first()->id;
        try{
            $mtef = Mtef::where('admin_hierarchy_id', $adminHierarchyId)->where('financial_year_id',$financialYearId)->first();
            BudgetExportAccountService::createActivities($mtef, $budgetType);
            return response()->json(['successMessage'=>'Ffars Activity created']);
        }catch(\Exception $e){
            Log::error($e->errorMessage);
            return response()->json($e, 500);

        }
    }

    public function disapproveMtef(Request $request)
    {
        $mtefId = $request->input('mtefId');
        $budget_type = $request->input('budget_type');
        try {
            DB::transaction(function () use ($mtefId, $budget_type) {
                $mtef = Mtef::where('id', $mtefId)->first();
                /** unlock activities */
                $this->lockActivities($mtefId, $budget_type, false);
                $accountRemoved = BudgetExportAccountService::removeBudgetAccounts($mtef->admin_hierarchy_id, $mtef->financial_year_id, $budget_type);
                /** rollback export to ffars activities */
                BudgetExportAccountService::removeFFARSactivities($mtef->admin_hierarchy_id, $mtef->financial_year_id);
            });
            /** unlock current budget */
            if ($budget_type == 'CURRENT') {
                MtefSection::where('mtef_id', $mtefId)->update(['is_locked' => false]);
                Mtef::where('id', $mtefId)->update(['locked' => false]);
            }
            $message = ["successMessage" => "SUCCESSFUL_DISAPPROVE_BUDGET"];
            return response()->json($message);
        } catch (QueryException $exception) {
            $error = ["errorMessage" => "ERROR_DISAPPROVING_BUDGET"];
            return response()->json($error, 500);

        }

    }

    public function getMtefs()
    {
        $financialYearId = Input::get('financialYearId',0);
        return DB::table('admin_hierarchies as a')
            ->join('mtefs as m', 'a.id', 'm.admin_hierarchy_id')
            ->where('m.financial_year_id', $financialYearId)
            ->select('m.id as mtefId', 'a.name as admin')
            ->orderBy('a.name')
            ->get();
    }
    public function getMtefSections($mtefId)
    {
        $financialYearId = Input::get('financialYearId',0);
        return DB::table('sections as s')
            ->join('mtef_sections as ms', 's.id', 'ms.section_id')
            ->join('mtefs as m', 'm.id', 'ms.mtef_id')
            ->where('m.financial_year_id', $financialYearId)
            ->where('m.id', $mtefId)
            ->select('ms.id as mtefSectionId', 's.name as section')
            ->orderBy('s.name')
            ->get();
    }

    public function getToMoveActivities($budgetType, $mtefId, $mtefSectionId)
    {

        $fundSourceToMove = ConfigurationSetting::getValueByKey('FUND_SOURCE_TO_MOVE');
        $budgetClassToMove = ConfigurationSetting::getValueByKey('BUDGET_CLASS_TO_MOVE');

        $activities = Activity::with(
            'mtefSection',
            'mtefSection.section',
            'activity_facilities',
            'annualTarget',
            'activity_facilities.facility',
            'budget_class',
            'activity_facilities.activity_facility_fund_sources2',
            'activity_facilities.activity_facility_fund_sources2.fund_source'
        )->whereHas('mtefSection', function ($ms) use ($mtefSectionId) {
            $ms->where('id', $mtefSectionId);
        })->whereHas('mtefSection.mtef', function ($m) use ($mtefId) {
            $m->where('id', $mtefId);
        })->whereHas('activity_facilities.activity_facility_fund_sources2', function ($f) use ($fundSourceToMove) {
            $f->whereIn('fund_source_id', $fundSourceToMove);
        })
        ->whereIn('budget_class_id', $budgetClassToMove)
        ->where('budget_type', $budgetType)->get();
        return $activities;
    }

    public function moveFacilityToFundSource($activityFacilityFundSourceId, $fundSourceId)
    {
        $one = DB::table('activity_facility_fund_sources')->where('id', $activityFacilityFundSourceId)->count();
        if ($one === 1) {
            DB::table('activity_facility_fund_sources')->where('id', $activityFacilityFundSourceId)->update(['fund_source_id' => $fundSourceId]);
            $newFundSource = ActivityFacilityFundSource::with('fund_source')->where('id', $activityFacilityFundSourceId)->first();
            $message = ["successMessage" => "SUCCESSFUL_MOVED", "newFundSource" => $newFundSource];
            return response()->json($message);
        } else {
            $message = ["errorMessage" => "ERROR_MULTIPLE_FOUND"];
            return response()->json($message, 400);
        }
    }
    public function moveActivityFacility($activityFacilityId, $facilityId)
    {
        $one = DB::table('activity_facilities')->where('id', $activityFacilityId)->count();
        if ($one === 1) {
            DB::table('activity_facilities')->where('id', $activityFacilityId)->update(['facility_id' => $facilityId]);
            $newFacility = ActivityFacility::with('facility')->where('id', $activityFacilityId)->first();
            $message = ["successMessage" => "SUCCESSFUL_MOVED", "newFacility" => $newFacility];
            return response()->json($message);
        } else {
            $message = ["errorMessage" => "ERROR_MULTIPLE_FOUND"];
            return response()->json($message, 400);
        }
    }

    public function moveActivityCostCentre($activityId, $mtefSectionId)
    {
        $one = DB::table('activities')->where('id', $activityId)->count();
        if ($one === 1) {

            DB::table('activities')->where('id', $activityId)->update(['mtef_section_id' => $mtefSectionId]);
            $newActivity = Activity::with(
                'mtefSection',
                'mtefSection.section',
                'activity_facilities',
                'activity_facilities.facility',
                'budget_class',
                'annualTarget.longTermTarget',
                'activity_facilities.activity_facility_fund_sources2',
                'activity_facilities.activity_facility_fund_sources2.fund_source'
            )->where('id', $activityId)->first();
            $mtefSection = DB::table('mtef_sections')->where('id', $mtefSectionId)->first();
            $targets = AnnualTarget::getByMtefSectionAndPlanChain($mtefSection->mtef_id, $mtefSectionId,$newActivity->budget_type, $newActivity->annualTarget->longTermTarget->plan_chain_id);
            $message = ["successMessage" => "SUCCESSFUL_MOVED", "newActivity" => $newActivity, "targets" => $targets];
            return response()->json($message);
        } else {
            $message = ["errorMessage" => "ERROR_MULTIPLE_FOUND"];
            return response()->json($message, 400);
        }
    }

    public function moveActivityTarget($activityId, $targetId)
    {
        $one = Activity::with(['mtefSection', 'mtefSection.mtef', ])->where('id', $activityId)->first();
        if ($one != null) {
            $code = Activity::getFacilityAccountNextCode($one->mtefSection->mtef->financial_year_id,$one->budget_type, $one->mtefSection->mtef->admin_hierarchy_id,$targetId,$one->activity_category_id);
            DB::table('activities')->where('id', $activityId)->update(['mtef_annual_target_id' => $targetId, 'code' => $code]);
            $newActivity = Activity::with('annualTarget')->where('id', $activityId)->first();
            $message = ["successMessage" => "SUCCESSFUL_MOVED", "newActivity" => $newActivity];
            return response()->json($message);
        } else {
            $message = ["errorMessage" => "ERROR_MULTIPLE_FOUND"];
            return response()->json($message, 400);
        }
    }



    public function moveActivityBudgetClass($activityId, $budgetClassId)
    {
        $one = DB::table('activities')->where('id', $activityId)->count();
        if ($one === 1) {

            DB::table('activities')->where('id', $activityId)->update(['budget_class_id' => $budgetClassId]);
            $newActivity = Activity::with(
                'mtefSection',
                'mtefSection.section',
                'activity_facilities',
                'activity_facilities.facility',
                'budget_class',
                'activity_facilities.activity_facility_fund_sources2',
                'activity_facilities.activity_facility_fund_sources2.fund_source'
            )->where('id', $activityId)->first();
            $message = ["successMessage" => "SUCCESSFUL_MOVED", "newActivity" => $newActivity];
            return response()->json($message);
        } else {
            $message = ["errorMessage" => "ERROR_MULTIPLE_FOUND"];
            return response()->json($message, 400);
        }
    }
    public function getToMoveData()
    {
        $budgetClasses = BudgetClass::whereIn('id', ConfigurationSetting::getValueByKey('BUDGET_CLASS_TO_MOVE'))->get();
        $fundSource = FundSource::whereIn('id', ConfigurationSetting::getValueByKey('FUND_SOURCE_TO_MOVE'))->get();

        $message = ["budgetClasses" => $budgetClasses, "fundSources" => $fundSource];
        return response()->json($message);
    }

    /** regenerate segments */
    public function resetMtef(Request $request)
    {
        set_time_limit(10000);
        $mtef_id = $request->mtefId;
        $budget_type = $request->budget_type;
        try {
            $accountUpdated = [];
            DB::transaction(function () use ($mtef_id, $budget_type, &$accountUpdated) {
                $mtef = Mtef::where('id', $mtef_id)->first();
                $financialYearId = $mtef->financial_year_id;
                $accountUpdated = BudgetExportAccountService::resetBudget($mtef->admin_hierarchy_id, $mtef->financial_year_id, $budget_type);
                BudgetExportAccountService::createActivities($mtef, $budget_type);
            });
            $count = sizeof($accountUpdated);
            $message = ["successMessage" => $count . " Segments has been updated"];
            return response()->json($message);
        } catch (QueryException $exception) {
            $error = ["errorMessage" => "ERROR_UPDATE_SEGMENT"];
            return response()->json($error, 500);
        }
    }

    /** get budget*/
    public function getBudget($admin_hierarchy_id, $hierarchy_position, $financialYearId, $budget_type)
    {
        if(!UserServices::hasPermission(['execution.export.'.$budget_type])){
            return response()->json(null,403);
        }
        if ($hierarchy_position == 1) {
            $admin_hierarchy_ids = AdminHierarchy::where('parent_id', $admin_hierarchy_id)
                ->select('id')
                ->get()
                ->toArray();
        }
        else if ($hierarchy_position == 2) {
            $admin_hierarchy_ids = array(array('id' => $admin_hierarchy_id));
        }
        else {
            return response()->json('Not set', 404);
        }

        $message = array();
        $mtefId = Mtef::where('admin_hierarchy_id', $admin_hierarchy_id)
        ->where('financial_year_id', $financialYearId)
        ->select('id')
        ->first()
        ->id;
        $sectionAtThisLevel = MtefSection::where('decision_level_id',2)->where('mtef_id',$mtefId)->count();
        $sectionWithBudget = $this->getSectionWithBudget($mtefId);
        if($sectionAtThisLevel === $sectionWithBudget){
        //get budget
        foreach ($admin_hierarchy_ids as $id) {
            $id = $id['id'];
        //get budget
            $budget = Mtef::getBudget($budget_type, $id, $financialYearId);
            $peBudget = Mtef::getPEBudget($budget_type, $id, $financialYearId);
            $ceiling = Mtef::getCeiling($budget_type, $id, $financialYearId);
            $peCeiling = Mtef::getPECeiling($budget_type, $id, $financialYearId);
            $sentNotification = Mtef::getSentNotification($budget_type,$id);
            $council = AdminHierarchy::find($id)->name;
            $mtef_id = Mtef::where('admin_hierarchy_id', $id)
                ->where('financial_year_id', $financialYearId)
                ->select('id')
                ->first()
                ->id;
            $ceiling_status = $this->getCeilingStatus($id, $financialYearId, $budget_type);
            $number_of_activities = $this->budgetApproved($mtef_id, $budget_type);
            $is_final = $number_of_activities > 0 ? false : true;
            $data = [
                'admin_hierarchy' => $council,
                'ceiling' => $ceiling,
                'peCeiling' => $peCeiling,
                'budget' => $budget,
                'peBudget' => $peBudget,
                'admin_hierarchy_id' => $id, 'ceiling_final' => $ceiling_status, 'mtef_id' => $mtef_id,
                'is_final' => $is_final, 'activities_number' => $number_of_activities, 'notification'=> $sentNotification
            ];
            array_push($message, $data);
        }
        $data = ['plans' => $message];
        return response()->json($data);

        }else{
            return response(['errorMessage'=>'Budget is not full submitted'],500); 
        }

    
    }
    /* get sections with budget */
    public function getSectionWithBudget($mtefId){
        $sectionWithBudget = Mtef::join('mtef_sections as ms','ms.mtef_id','mtefs.id')
                                ->join('activities as a','a.mtef_section_id','ms.id')
                                ->join('activity_facilities as af','af.activity_id','a.id')
                                ->join('activity_facility_fund_sources as affs','affs.activity_facility_id','af.id')
                                ->join('activity_facility_fund_source_inputs as affsi','affsi.activity_facility_fund_source_id','affs.id')
                                ->distinct('ms.section_id')
                                ->where('mtefs.id',$mtefId)
                                ->where('affsi.unit_price','>',0)
                                ->where('a.code','<>','00000000')
                                ->count('ms.section_id');
        return $sectionWithBudget;

    }

    /** get ceiling status */
    public function getCeilingStatus($admin_hierarchy_id, $financial_year_id, $budget_type)
    {
        if($budget_type == 'CARRYOVER' || $budget_type == 'SUPPLEMENTARY'){
            $results = DB::select("Select
                        count(adc.id) as not_approved
                        from admin_hierarchy_ceilings as adc
                        join sections as sec on sec.id = adc.section_id
                        join section_levels as secl on secl.id = sec.section_level_id
                        join ceilings as ce on ce.id = adc.ceiling_id
                        join gfs_codes as gfs on gfs.id = ce.gfs_code_id
                        join gfscode_fundsources gfcf ON gfcf.gfs_code_id = gfs.id
                        join fund_sources as fu on fu.id = gfcf.fund_source_id
                        where
                        ce.is_active = true
                        and adc.amount > 0
                        and adc.financial_year_id = $financial_year_id
                        and adc.admin_hierarchy_id = $admin_hierarchy_id
                        and adc.budget_type = '$budget_type'
                        and adc.is_approved = false
                        and adc.facility_id is not null
                        ");
        }else{
            $results = DB::select("Select
                        count(adc.id) as not_approved
                        from admin_hierarchy_ceilings as adc
                        join sections as sec on sec.id = adc.section_id
                        join section_levels as secl on secl.id = sec.section_level_id
                        join ceilings as ce on ce.id = adc.ceiling_id
                        join gfs_codes as gfs on gfs.id = ce.gfs_code_id
                        join gfscode_fundsources gfcf ON gfcf.gfs_code_id = gfs.id
                        join fund_sources as fu on fu.id = gfcf.fund_source_id
                        where
                        ce.is_active = true
                        and adc.amount > 0
                        and adc.financial_year_id = $financial_year_id
                        and adc.admin_hierarchy_id = $admin_hierarchy_id
                        and adc.budget_type = '$budget_type'
                        and adc.is_approved = false
                        ");
        }
        
        $ceilingStatus = $results[0]->not_approved > 0 ? false : true;
        return $ceilingStatus;
    }
    /** check if budget has been approved */
    public function budgetApproved($mtef_id, $budget_type)
    {
        $result = DB::table('activities as a')
            ->join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')
            ->join('activity_facilities as af', 'a.id', 'af.activity_id')
            ->join('activity_facility_fund_sources as aff', 'aff.activity_facility_id', 'af.id')
            ->join('activity_facility_fund_source_inputs as ai', 'ai.activity_facility_fund_source_id', 'aff.id')
            ->where('ms.mtef_id', $mtef_id)
            ->where('a.is_final', false)
            ->where('ai.budget_type', $budget_type)
            ->select('a.id')
            ->count();
        return $result;
    }
    /** lock activities */
    public function lockActivities($mtef_id, $budget_type, $lock)
    {
        $activities = DB::table('activities as a')
            ->join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')
            ->join('activity_facilities as af','af.activity_id','a.id')
            ->join('activity_facility_fund_sources as aff','aff.activity_facility_id','af.id')
            ->join('activity_facility_fund_source_inputs as affi','affi.activity_facility_fund_source_id','aff.id')
            ->where('ms.mtef_id', $mtef_id)
            ->where('a.is_final', '!=', $lock)
            ->where('a.budget_type', $budget_type)
            ->pluck('a.id');
      //update
        DB::table('activities')->whereIn('id', $activities)->update(['is_final' => $lock]);
    }
}
