<?php

namespace App\Http\Controllers\Planning;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Planning\CasPlanSubmission;
use Illuminate\Support\Facades\Log;

class CasPlanSubmissionController extends Controller
{
    public function submitPlan(Request $request){
        $financial_year_type = $request->financial_year_type;
        $cas_plan = $request->cas_plan;
        if($financial_year_type == 'planning'){
            $financialYear = FinancialYearServices::getPlanningFinancialYear();
        }else {
            $financialYear = FinancialYearServices::getExecutionFinancialYear();
        }
        $user = UserServices::getUser();
        $admin_hierarchy   = $user->admin_hierarchy_id;
        $data = array('admin_hierarchy_id'=>$admin_hierarchy, 'financial_year_id'=>$financialYear->id,
                 'cas_plan_id'=>$cas_plan, 'user_id'=>$user->id);
        try{
            CasPlanSubmission::create($data);
            //lock budget for health
            if($user->section_id == 3){
               CasPlanSubmission::lockMtef($admin_hierarchy, $financialYear->id, true);
            }
            $message = ["successMessage" => "CAS_PLAN_SUBMITTED_SUCCESSFULLY"];
            return response()->json($message, 200);
        }catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            $message = ["errorMessage" => "DATABASE_ERROR"];
            return response()->json($message, 500);
        }

    }

    
}
