<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Flatten;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Planning\MtefAnnualTarget;
use App\Models\Setup\SectionParent;
use Illuminate\Http\Request;

class MtefAnnualTargetController extends Controller
{

    private $limit = 10;
    public function index()
    {
        $mtefAnnualTargets=MtefAnnualTarget::with('mtef')->with('longTermTargets')->get();
        return response()->json($mtefAnnualTargets);
    }
    public function indexPagination()
    {
        $mtefAnnualTargets=MtefAnnualTarget::paginate($this->limit);
        return response()->json($mtefAnnualTargets);
    }
    public function annualTargetByUser()
    {
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        $financialYearId = $financialYear->id;
        $section_id = UserServices::getUser()->section_id;
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;

        $sections=SectionParent::where('id',$section_id)->get();
        $flatten=new Flatten();
        $sectionIds=$flatten->flattenSectionWithParent($sections);
        $code=$flatten->getAutoBudgetTemplatesCodes();
        $mtefAnnualTargets=MtefAnnualTarget::with('mtef')->with('longTermTarget')->
            whereHas('mtef', function ($query) use ($admin_hierarchy_id, $financialYearId) {
                $query->where('admin_hierarchy_id', $admin_hierarchy_id)->where('financial_year_id', $financialYearId);
            })->whereHas('longTermTarget', function ($query) use ($code) {
                $query->whereNotIn('code', $code);
            })
            ->whereIn('section_id',$sectionIds)->get();
        return response()->json($mtefAnnualTargets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = json_decode($request->getContent());
            $mtefAnnualTarget = new MtefAnnualTarget();
            $mtefAnnualTarget->target_description = $data->target_description;
            $mtefAnnualTarget->code = $data->code;
            $mtefAnnualTarget->save();
            //Return language success key and all data
            $message = ["successMessage" => "SUCCESSFUL_ANNUAL_TARGET_CREATED", "mtefAnnualTargets" => MtefAnnualTarget::paginate($this->limit)];
            return response()->json($message, 200);
        } catch (QueryException $exception) {
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_LONG_TERM_TARGET_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $data = json_decode($request->getContent());
            $mtefAnnualTarget = MtefAnnualTarget::find($data->id);
            $mtefAnnualTarget->target_description = $data->target_description;
            $mtefAnnualTarget->code = $data->code;
            $mtefAnnualTarget->long_term_target_id = $data->long_term_target_id;
            $mtefAnnualTarget->mtef_id = $data->mtef_id;
            $mtefAnnualTarget->save();
            //Return language success key and all data
            $message = ["successMessage" => "SUCCESSFUL_ANNUAL_TARGET_UPDATED", "annualTargets" => MtefAnnualTarget::paginate($this->limit)];
            return response()->json($message, 200);
        } catch (QueryException $exception){
            $error_code = $exception->errorInfo[1];
            //ERROR CODE 7 CATCH Duplicate
            if ($error_code == 7) {
                $message = ["errorMessage" => "ERROR_LONG_TERM_TARGET_EXISTS"];
                return response()->json($message, 400);
            } else {
                $message = ["errorMessage" => 'ERROR_OTHERS'];
                return response()->json($message, 400);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {
        $mtefAnnualTarget = MtefAnnualTarget::find($id);
        $mtefAnnualTarget->delete();
        $message = ["successMessage" => "SUCCESSFULLY_ANNUAL_TARGET_DELETED", "annualTargets" => MtefAnnualTarget::paginate($this->limit)];
        return response()->json($message, 200);
    }
}
