<?php

namespace App\Http\Controllers\Planning;
use App\Http\Controllers\Controller;
use App\Models\Planning\PeContribution;
use Illuminate\Http\Request;

class PeContributionController extends Controller {

    private $limit = 2;

    public function index() {
        $all = PeContribution::all();
        return response()->json($all);
    }

    public function paginated() {
        $all = PeContribution::orderby('created_at')->paginate($this->limit);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        $pe = new PeContribution();
        $pe->name = $data->name;
        $pe->code = $data->code;
        $pe->percentage = $data->percentage;
        $pe->is_active = true;
        $pe->save();
        $all = PeContribution::orderby('created_at')->paginate($this->limit);
        $message = ["successMessage" => "SUCCESSFUL_PE_CONTRIBUTION_CREATED", "pe_contributions" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        $pe = PeContribution::find($data->id);
        $pe->name = $data->name;
        $pe->code = $data->code;
        $pe->percentage = $data->percentage;
        $pe->is_active = true;
        $pe->save();
        $all = PeContribution::orderby('created_at')->paginate($this->limit);
        $message = ["successMessage" => "SUCCESSFUL_PE_CONTRIBUTION_UPDATED", "pe_contributions" => $all];
        return response()->json($message, 200);
    }

    public function delete($id) {
        $pe = PeContribution::find($id);
        $pe->delete();
        $all = PeContribution::orderby('created_at')->paginate($this->limit);
        $message = ["successMessage" => "SUCCESSFUL_PE_CONTRIBUTION_DELETED", "pe_contributions" => $all];
        return response()->json($message, 200);
    }

    public function togglePeContribution(Request $request) {
        $data = json_decode($request->getContent());
        $object = PeContribution::find($data->id);
        $object->is_active = $data->is_active;
        $object->save();
        $all = PeContribution::orderby('created_at')->paginate($this->limit);
        if ($data->is_active == false) {
            $feedback = ["action" => "PE_CONTRIBUTION_DEACTIVATED", "alertType" => "warning", "pe_contributions" => $all];
        } else {
            $feedback = ["action" => "PE_CONTRIBUTION_ACTIVATED", "alertType" => "success", "pe_contributions" => $all];
        }
        return response()->json($feedback, 200);
    }
}
