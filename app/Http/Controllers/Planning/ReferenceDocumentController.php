<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Setup\ReferenceDocument;

class ReferenceDocumentController extends Controller
{
    public function index()
    {
        $referenceDocuments = ReferenceDocument::where('is_active',true)->where('admin_hierarchy_id', UserServices::getUser()->admin_hierarchy_id)->get();
        return response()->json($referenceDocuments);
    }
}
