<?php

namespace App\Http\Controllers\Planning;

use App\Http\Controllers\Controller;
use App\Http\Services\UserServices;
use App\Models\Planning\LongTermTarget;
use App\Models\Planning\SectorTarget;
use App\Models\Setup\Sector;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class SectorTargetController extends Controller {
    private $limit = 10;

    public function index() {
        $all = SectorTarget::with('sector','long_term_target')->get();
        return response()->json($all);
    }

    public function getAllPaginated($perPage) {
        return SectorTarget::with('sector','long_term_target')->orderBy('id')->paginate($perPage);
    }

    public function indexNoRelation() {
        $all = SectorTarget::all();
        return response()->json($all);
    }

    public function paginated(Request $request) {
        $all = $this->getAllPaginated($request->perPage);
        return response()->json($all);
    }

    public function store(Request $request) {
        $data = json_decode($request->getContent());
        $obj = new SectorTarget();
        $obj->sector_id = $data->sector_id;
        $obj->long_term_target_id = $data->long_term_target_id;
        $obj->created_by = UserServices::getUser()->id;
        $obj->save();
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_SECTOR_TARGET_CREATED", "sectorTargets" => $all];
        return response()->json($message, 200);
    }

    public function update(Request $request) {
        $data = json_decode($request->getContent());
        $obj = SectorTarget::find($data->id);
        $obj->sector_id = $data->sector_id;
        $obj->long_term_target_id = $data->long_term_target_id;
        $obj->updated_by = UserServices::getUser()->id;
        $obj->save();
        $all = $this->getAllPaginated($request->perPage);
        $message = ["successMessage" => "SUCCESSFUL_SECTOR_TARGET_UPDATED", "sectorTargets" => $all];
        return response()->json($message, 200);
    }

    public function delete($id) {
        $object = SectorTarget::find($id);
        $object->delete();
        $all = $this->getAllPaginated(Input::get('perPage'));
        $message = ["successMessage" => "SUCCESSFULLY_SECTOR_TARGET_DELETED", "sectorTargets" => $all];
        return response()->json($message, 200);
    }

    public function allSectors() {
        $all = Sector::all();
        return response()->json($all);
    }

    public function allLongTermTargets() {
        $all = LongTermTarget::all();
        return response()->json($all);
    }
}
