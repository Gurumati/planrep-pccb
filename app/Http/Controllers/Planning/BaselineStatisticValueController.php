<?php

namespace App\Http\Controllers\Planning;
use App\Http\Controllers\Controller;
use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use App\Models\Planning\BaselineStatisticValue;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;


class BaselineStatisticValueController extends Controller
{
    public function index(){
        //Capture the financial year to be used
        $financialYear = FinancialYearServices::getPlanningFinancialYear();
        if(!isset($financialYear)){
            $financialYear = FinancialYearServices::getExecutionFinancialYear();
        }
//        $financial_year_id = $financialYear->id;
        $financial_year_id = Input::get('financialYearId');

        //Capture the user's admin hierarchy
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;

        $values = BaselineStatisticValue::where('admin_hierarchy_id',$admin_hierarchy_id)
                                          ->where('financial_year_id', $financial_year_id)
                                          ->select('id','baseline_statistic_id','value')
                                          ->get();
        return response()->json($values);
    }

    public function store(Request $request){
        //Read the JSON object/array in use
        $data = json_decode($request->getContent());

        //Capture the financial year to be used
        $financial_year = FinancialYearServices::getPlanningFinancialYear();
        $financial_year_id = $financial_year->id;

        //Capture the user's admin hierarchy
        $admin_hierarchy_id = UserServices::getUser()->admin_hierarchy_id;

        try {
          foreach($data as $verileri){
            $check_data_exists = BaselineStatisticValue::where('baseline_statistic_id', $verileri->id)
                                                         ->where('admin_hierarchy_id',$admin_hierarchy_id)
                                                         ->where('financial_year_id', $financial_year_id)
                                                         ->first();

            //check if the data exists
            if (!isset($check_data_exists->id)){
                // insert the data
                    //Create the object to save
                    $value = new BaselineStatisticValue();

                    //Loop and insert all the elements
                    $user_id = UserServices::getUser()->id;
                    $data_array = array('financial_year_id'=>$financial_year_id,
                            'admin_hierarchy_id'=>$admin_hierarchy_id,
                            'value'=>isset($verileri->value)?$verileri->value:null,
                            'baseline_statistic_id'=>$verileri->id,
                            'created_by'=>$user_id);

                    $value->insert($data_array);

            } else {
                //update the data
                $obj = BaselineStatisticValue::find($check_data_exists->id);
                $obj->value = $verileri->value;
                $obj->save();

            }
          }

                //Send response to the user
                $values = DB::table('baseline_statistics as b')
                          ->join('baseline_statistic_values as v','b.id','v.baseline_statistic_id')
                          ->where('v.admin_hierarchy_id',$admin_hierarchy_id)
                          ->where('v.financial_year_id',$financial_year_id)
                          ->select('b.id','b.description','b.is_common','b.default_value','v.value')
                          ->get();
                $message = ["successMessage" => "DATA_SAVED_SUCCESSFULLY","baselineData"=>$values];
                return response()->json($message);

            } catch (QueryException $exception) {
                    $error_code = $exception->errorInfo[1];
                //ERROR CODE 7 CATCH Duplicate
                if ($error_code == 7) {
                    $message = ["errorMessage" => "ERROR_DATA_EXISTS"];
                    return response()->json($message, 500);
                } else {
                    $message = ["errorMessage" => 'ERROR_OTHERS'];
                    return response()->json($message, 500);
                }
            }
    }
}
