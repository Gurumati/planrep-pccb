<?php
namespace App\Custom;

use Illuminate\Support\Facades\Config;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Wire\AMQPTable;
use Illuminate\Support\Facades\Log;


/**
 * Created by PhpStorm.
 * User: bamsi
 * Date: 11/14/2017
 * Time: 9:23 AM
 */
class Connection
{
  /*
   * options defined by users
   * an array of variables
   * allowed options (host,username, password, vhost,exchange_type,port,content_type,consumer_tag)
   */
  protected $options;
  /*
   * connections holds default connections parameters
   * read from config/queue.php
   */
  protected $connections;
  /*
   * store name of the queue
   */
  protected $queue_name;
  /*
   * store name of the exchange
   */
  protected $exchange;
  /*
   * store name of the route
   */
  protected $route_name;
  /*
   * hold current channel
   */
  protected $channel;
  /*
   * hold current connection
   */
  protected $AMQPConnection;

  /*
   * constructor function
   * overwrite options when passed
   * initializes connections
   * @options - array of allowed options, its default value is null
   */
  public function __construct(array $options = null)
  {
    //set default exchange to empty
    $this->exchange = '';
    $this->options = $options;
    $this->setConnection();
  }




  /*
   * prepare connections variables
   * read from config/queue.php file
   * overwrite some of the default properties when define by user in options
   */
  public function setConnection()
  {
    $this->connections = Config::get("queue.connections.rabbitmq");

    if(isset($this->options['host']))
        $this->connections['host'] = $this->options['host'];

    if(isset($this->options['username']))
        $this->connections['username'] = $this->options['username'];

    if(isset($this->options['password']))
        $this->connections['password'] = $this->options['password'];

    if(isset($this->options['host']))
        $this->connections['port'] = $this->options['port'];

    if(isset($this->options['vhost']))
        $this->connections['vhost'] = $this->options['vhost'];

    if(isset($this->options['exchange_type']))
        $this->connections['exchange_type'] = $this->options['exchange_type'];

    if(isset($this->options['content_type']))
        $this->connections['content_type'] = $this->options['content_type'];

    if(isset($this->options['exchange']))
        $this->exchange = $this->options['exchange'];

  }

  /*
   * initialize connection object
   * initialize channel
   * return null
   */
  public function open()
  {
      $this->AMQPConnection = new AMQPStreamConnection( $this->connections['host'],
      	                                                $this->connections['port'],
      	                                                $this->connections['username'],
      	                                                $this->connections['password'],
      	                                                $this->connections['vhost']);

      $this->channel = $this->AMQPConnection->channel();
  }

  /*
   * use current channel to declare queue and exchange
   * bind queue and exchange
   * @args - pass arguments defined by a user (normally when using headers exchange)
   */
  public function buildConnection(array $args = null)
  {
     if($args)
     {
          $args = new AMQPTable($args);
     }
  	$this->open();
    /*
     * @queue_name    - string
     * @auto_delete   - boolean
     * @durable       - boolean (message persist)
     * @exclusive     - boolean (true if is an exclusive consumer)
     * @nowait        - boolean
     * @passive       - boolean
     * @argument      - object
     */
    $this->channel->queue_declare($this->queue_name,false,true,false,false,false,$args);
    /*
     * @exchange      - string
     * @exchange_type - string (e.g headers,direct, fanout, topic)
     * @auto_delete   - boolean
     * @durable       - boolean
     */
    $this->channel->exchange_declare($this->exchange,$this->connections['exchange_type'],false,true,false,false,false,$args);
    /*
     * @queue_name    - string
     * @exchange       - string
     * @route         - string (optional)
     * @nowait        - boolean
     * @argument      - object
     */
    $this->channel->queue_bind($this->queue_name,$this->exchange,$this->route_name, false, $args);
  }

  /*
   * close channel
   * close connection
   */
  public function close()
  {
      if(isset($this->channel))
          $this->channel->close();

      if(isset($this->AMQPConnection))
          $this->AMQPConnection->close();
  }

}