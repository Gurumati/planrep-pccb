<?php

namespace App\Custom;

use Closure;
use Exception;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

/**
 * Created by PhpStorm.
 * User: bamsi
 * Date: 11/14/2017
 * Time: 9:18 AM
 */
class Tail extends Connection
{
    /*
     * this function send message to rabbit
     * @message      - message body, accepts different format based on content-type defined
     * @queue_name   - name of the queue
     * @exchange_name- name of exchange (optional)
     * @options      - define connection properties which overwrite the default properties such as host, username etc
     * @args         - message arguments (especially for headers exchange type)
     */
    public function add($queue_name, $message, $options)
    {
        //get arguments from options
        if (isset($options['headers'])) {
            $args = $options['headers'];
        }else {
            $args = null;
        }
        $connection = new Connection($options);
        $connection->queue_name = $queue_name;
        $connection->buildConnection($args);

        $msg = new AMQPMessage($message, array('content_type' => $this->connections['content_type'], 'delivery_mode' => 2));
        if ($args) {
            $args = new AMQPTable($args);
            $msg->set('application_headers', $args);
        }
        $connection->channel->basic_publish($msg, $connection->exchange, '');
        $connection->close();
    }

    public function listenWithOptions($queue_name, array $options = null, Closure $closure)
    {
        $connection = new Connection($options);
        $connection->queue_name = $queue_name;
        $connection->buildConnection();
        $listenerObject = $this;
        $connection->channel->basic_consume($this->queue_name, $connection->connections['consumer_tag'],
            false, false, false, false, function ($msg) use ($closure, $listenerObject) {

                try {
                    //return message as an object
                    $closure($msg);
                } catch (Exception $e) {
                    throw $e;
                }

                $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);

            });

        while (count($connection->channel->callbacks)) {
            $connection->channel->wait();
        }

    }


    public static function getMessages()
    {
        $queue_name = "GLAccounts_Queue_FeedBack";
        $options = array('exchange_type' => 'headers', 'exchange' => 'GLAccountsExchange');
        $obj = new Tail();
        $obj->listenWithOptions($queue_name, $options, function ($msg) {
            echo $msg->body;
        });
    }

}