<?php
//This is data transfer object fro admin hierarchy when reference from other objects

namespace App\DTOs\SetupDTOs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class AdminHierarchyDTO extends Model {
    protected $table = 'admin_hierarchies';


    protected $with = [
        'admin_hierarchy_level','parent'
    ];

    public function admin_hierarchy_level() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchyLevel')->select('id','name','hierarchy_position');
    }

    public function parent() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchy','parent_id','id')->select('id','name');
    }
}
