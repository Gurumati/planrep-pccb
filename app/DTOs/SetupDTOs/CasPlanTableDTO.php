<?php

namespace App\DTOs\SetupDTOs;

use Illuminate\Database\Eloquent\Model;

class CasPlanTableDTO extends Model {

    protected $table = 'cas_plan_tables';

}
