<?php
//This is data transfer object fro admin hierarchy when reference from other objects

namespace App\DTOs\SetupDTOs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class geoLocationDTO extends Model {
    protected $table = 'geographical_locations';


    protected $with = [
        'geographical_location_level','wardParent'
    ];

    public function geographical_location_level() {
        return $this->belongsTo('App\Models\Setup\GeoLocationLevel')->select('id','name','geo_level_position');
    }

    public function wardParent() {
        return $this->belongsTo('App\Models\Setup\GeoLocation','parent_id','id')->select('id','name');
    }
}
