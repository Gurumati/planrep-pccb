<?php

namespace App\DTOs\SetupDTOs;

use Illuminate\Database\Eloquent\Model;

class InterventionCategoryDTO extends Model {

    protected $table = "intervention_categories";
}
