<?php

namespace App\DTOs\SetupDTOs;

use Illuminate\Database\Eloquent\Model;

class SectionDTO extends Model {

    protected $table = 'sections';

    public function section_level() {
        return $this->belongsTo('App\Models\Setup\SectionLevel','section_level_id','id');
    }

    public function sector() {
        return $this->belongsTo('App\Models\Setup\Sector','sector_id','id');
    }
}
