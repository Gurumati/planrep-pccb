<?php

use App\Http\Controllers\Flatten;
use App\Jobs\QueuePublisher;
use App\Models\Auth\Trackable;
use App\Models\CacheKeys;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\Section;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Contracts\Routing\ResponseFactory;

/**
 * Created by PhpStorm.
 * User: mrisho
 * Date: 10/14/17
 * Time: 11:20 AM
 */

/**
 * Generates pagination of items for an array or an eloquent collection.
 * by default, you can't call paginate on an array, only works on collections
 * this function solves that.
 * url: https://laravel.com/docs/5.4/pagination
 *
 * @param array|Collection $items
 * @param int $perPage
 * @param int $page
 * @param array $options
 *
 * @return LengthAwarePaginator
 */
function paginate_this($items, $perPage = 15, $page = null, $options = [])
{
    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
    $items = $items instanceof Collection ? $items : Collection::make($items);
    return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

}

if (!function_exists('sendToQueue')) {
    function sendToQueue($data, $queue_name, $exchange, $exchange_type, $routing_key, $headers, $queue_bindings)
    {
        dispatch(new QueuePublisher($data, $queue_name, $exchange, $exchange_type, $routing_key, $headers, $queue_bindings));
    }
}

/**
 * Tracks user's activities on models
 *
 * @param Model $trackable the model object we're tracking
 * @param Model $user the user performing the action on the model
 * @param String $action the action being performed on the model
 */
function track_activity($trackable, $user, $action)
{
    $deletedObject = (strpos($action, 'delete') !== false) ? json_encode($trackable) : null;
    Trackable::create(array(
        'user_id' => $user->id,
        'action' => $action,
        'trackable_id' => $trackable->id,
        'trackable_type' => get_class($trackable),
        'deleted_object' => $deletedObject
    ));
}

function get_section_ids_string($sectionId)
{

    // return Cache::rememberForever(CacheKeys::SECTION_WITH_CHILDREN_IDS_AS_STRING . $sectionId, function () use ($sectionId) {
        $flatten = new Flatten();
        $sectionTree = Section::where('id', $sectionId)->get();
        return $flatten->flattenSectionWithChildIdsAsString($sectionTree);
    // });
}

function get_sector_ids_string($sectionId)
{

    return Cache::rememberForever(CacheKeys::SECTION_WITH_CHILDREN_IDS_AS_STRING . $sectionId, function () use ($sectionId) {
        $flatten = new Flatten();
        $userSection = Section::where('id', $sectionId)->get();
        $this->sectorIdsString = $flatten->flattenSectionWithChildrenGetSectorsIdsString($userSection);
    });
}

function get_admin_areas_ids_string($childAdminAreaId)
{
    // return Cache::rememberForever(CacheKeys::ADMIN_AREA_WITH_CHILD_IDS_STRING . $childAdminAreaId, function () use ($childAdminAreaId) {
        $flatten = new Flatten();
        $childAdminHierarchyTree = AdminHierarchy::where('id', $childAdminAreaId)->get();
        return $flatten->flattenAdminHierarchyWithChildGetStringIds($childAdminHierarchyTree);
    // });
}

function apiResponse($status, $message, $data, $success, $errors)
{
    return response()->json(['status' => $status, 'message' => $message, 'data' => $data, 'success' => $success, 'errors' => $errors]);
}

if (!function_exists('customApiResponse')) {
    /**
     * @param string $data
     * @param string $message
     * @param int $status
     * @param array $errors
     * @param array $headers
     * @return \Illuminate\Foundation\Application|mixed
     */
    function customApiResponse($data = '', $message = '', $status = 200, $errors = [], array $headers = ['Content-Type' => 'application/json']) {
        /**
         * make sure that we always return errors as an array of errors
         */
        $errorsArray = [];
        $_status = $status;
        if(!is_array($errors)) {
            array_push($errorsArray, $errors);
            $errors = $errorsArray;
        }

        $responseData =  [
            'data'    => $data,
            'message' => $message,
            'errors'  => $errors,
            'status'  => $status
        ];

        $request_headers = getallheaders();
        if(isset($request_headers['http_standard_response']) && $request_headers['http_standard_response']){
            $_status = 200;
        }

        $factory = app(ResponseFactory::class);
        if (func_num_args() === 0) {
            return $factory;
        }
        return $factory->make(json_encode($responseData), $_status, $headers);
    }
}

if (!function_exists('parseUrl')) {
    /**
     * Generate a querystring url for the application.
     *
     * Assumes that you want a URL with a querystring rather than route params
     * (which is what the default url() helper does)
     *
     * @param  string  $path
     * @param  mixed   $qs
     * @param  bool    $secure
     * @return string
     */
    function parseUrl($qs = array(), $secure = null)
    {
        $url = app('url')->to($path, $secure);
        if (count($qs)){

            foreach($qs as $key => $value){
                $qs[$key] = sprintf('%s=%s',$key, urlencode($value));
            }
            $url = sprintf('%s?%s', $url, implode('&', $qs));
        }
        return $url;
    }
}
