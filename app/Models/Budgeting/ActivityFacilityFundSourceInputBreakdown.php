<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;

class ActivityFacilityFundSourceInputBreakdown extends Model
{

    public $timestamps = true;
    //use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'activity_facility_fund_source_input_breakdowns';

    protected $with = [
        'unit'
    ];

    public function unit(){
        return $this->belongsTo('App\Models\Setup\Unit')->select(['id','name','symbol']);
    }
}
