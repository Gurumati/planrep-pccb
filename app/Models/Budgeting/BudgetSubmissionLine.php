<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;

class BudgetSubmissionLine extends Model
{
    //
    public $timestamps = true;
    //use SoftDeletes;
    protected $table = "budget_submission_lines";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function budgetSubmissionSubForm()
    {
        return $this->belongsTo('App\Models\Budgeting\BudgetSubmissionSubForm');
    }

    public function activity()
    {
        return $this->belongsTo('App\Models\Planning\Activity');
    }

    public function budgetSubmissionLineValues()
    {
        return $this->hasMany('App\Models\Budgeting\BudgetSubmissionLineValue');
    }

}
