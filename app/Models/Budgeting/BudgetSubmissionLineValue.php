<?php

namespace App\Models\Budgeting;

use App\Http\Services\UserServices;
use Illuminate\Database\Eloquent\Model;

class BudgetSubmissionLineValue extends Model
{
    //
    public $timestamps = true;
    protected $table = "budget_submission_line_values";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public static function deleteAndTrack($lineValues){
        foreach ($lineValues as $l) {
            $l->delete();
            track_activity($l, UserServices::getUser(), 'delete_input_submission_lines');
        }
    }

    public function budgetSubmissionLine()
    {
        $this->belongsTo('App\Models\Budgeting\BudgetSubmissionLine');
    }

    public function budgetSubmissionDefinition()
    {
        $this->belongsTo('App\Models\Budgeting\BudgetSubmissionDefinition');
    }
}
