<?php

namespace App\Models\Budgeting;

use App\Models\Setup\FinancialYear;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use App\Http\Services\FinancialYearServices;

class BudgetType extends Model
{
    private static  $BUDGET_TYPES = ['CURRENT', 'APPROVED','CARRYOVER','SUPPLEMENTARY'];

    public static function getAll(){

        // $financialYearStatus = FinancialYear::where('status',2)->first();
        // if($financialYearStatus != null){
        //     $financialYearId = FinancialYearServices::getExecutionFinancialYear()->id;
        // }else{
            $financialYearId = Input::get('financialYearId');
        //}
        if(isset($financialYearId)){
            return self::getByFinancialYear($financialYearId);
        }
        return self::$BUDGET_TYPES;
    }

    public static function getDefault()
    {
        return self::$BUDGET_TYPES[0];
    }

    public static function getWithFinancialYears()
    {
        $budgetTypes = BudgetType::getAll();
        $types = [];
        foreach ($budgetTypes as $type){
            $t = new \StdClass();
            $t->budgetType = $type;
            $t->financialYear = FinancialYear::getByBudgetType($type);
            $t->previousFinancialYear =($t->financialYear != null)?FinancialYear::getPreviousYear($t->financialYear->id):null;
            array_push($types,$t);
        }
        return $types;
    }

    public static function getByFinancialYear($financialYearId)
    {
        $financialYear = FinancialYear::find($financialYearId);
        switch ($financialYear->status){
            case 1:
                return ['CURRENT'];
                break;
            case 2:
                return ['APPROVED','CARRYOVER','SUPPLEMENTARY'];
                break;
            default:
                return [];
        }
    }

}
