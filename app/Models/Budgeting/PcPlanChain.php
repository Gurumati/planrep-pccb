<?php

namespace App\models\budgeting;

use Illuminate\Database\Eloquent\Model;

class PcPlanChain extends Model
{
    public $timestamps=true;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";
    protected $table = 'plan_chains';

    public function children() {
        return $this->hasMany('App\Models\Budgeting\PcLongTarget', 'plan_chain_id');
    }

}
