<?php

namespace App\models\budgeting;

use Illuminate\Database\Eloquent\Model;

class PcTarget extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'mtef_annual_targets';

    public function children(){
        return $this->hasMany('App\Models\Budgeting\PcActivity');
    }

    public function longTermTarget(){
        return $this->belongsTo('App\Models\Budgeting\PcLongTarget');
    }

}
