<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;

class CeilingSector extends Model
{
    public function sector(){
        return $this->belongsTo('App\Models\Setup\Sector');
    }

    public function ceiling(){
        return $this->belongsTo('App\Models\Budgeting\Ceiling');
    }
}
