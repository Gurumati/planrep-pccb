<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;

class ScrutinizationRound extends Model
{
   public $timestamps=false;
    public function mtef_section() {
        return $this->belongsTo('App\Models\Planning\MtefSection');
    }
}
