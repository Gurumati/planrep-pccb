<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminHierarchyCeilingPeriod extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'admin_hierarchy_ceiling_periods';

    public function admin_hierarchy_ceiling() {
        return $this->belongsTo('App\Models\Budgeting\AdminHierarchyCeiling');
    }

    public function ceiling() {
        return $this->belongsTo('App\Models\Setup\Period');
    }
}
