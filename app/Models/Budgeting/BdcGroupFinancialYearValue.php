<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BdcGroupFinancialYearValue extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'bdc_group_financial_year_values';


    public function financial_year() {
        return $this->belongsTo('App\Models\Setup\FinancialYear','financial_year_id','id');
    }

    public function bdc_group() {
        return $this->belongsTo('App\Models\Setup\BdcGroup','group_id','id');
    }

}
