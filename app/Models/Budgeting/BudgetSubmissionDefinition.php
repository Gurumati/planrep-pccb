<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetSubmissionDefinition extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $table = "budget_submission_definitions";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public $with = ['parent'];

    public function budgetSubmissionForm(){
        return $this->belongsTo('App\Models\Budgeting\BudgetSubmissionForm');
    }

    public function gfsCode(){
        return $this->belongsTo('App\Models\Setup\GfsCode');
    }

    public function unit(){
        return $this->belongsTo('App\Models\Setup\Unit');
    }

    public function childForm(){
        return $this->hasMany('App\Models\Budgeting\BudgetSubmissionDefinition', 'parent_id');
    }

    public function parent(){
        return $this->belongsTo('App\Models\Budgeting\BudgetSubmissionDefinition');
    }

    public function budgetSubmissionLineValues()
    {
        return $this->hasMany('App\Models\Budgeting\BudgetSubmissionLineValue');
    }
}
