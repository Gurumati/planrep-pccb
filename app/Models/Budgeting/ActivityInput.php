<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ActivityInput extends Model
{

    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $with = [
        'activityInputBreakdowns','periods','gfsCode','procurementType','unit','activityInputForwards'
    ];

    public function activity() {
        return $this->belongsTo('App\Models\Planning\Activity');
    }

    public function activityInputBreakdowns() {
        return $this->hasMany('App\Models\Budgeting\ActivityInputBreakdown')
                    ->select(['activity_input_breakdowns.id','item','activity_input_id','unit_id','unit_price','quantity','frequency']);
    }
    public function periods() {
       return $this->hasMany('App\Models\Budgeting\ActivityInputPeriod');
    }
    public function gfsCode() {
       return $this->belongsTo('App\Models\Setup\GfsCode')
                    ->select(['id','code','description','name']);
    }
    public function unit() {
       return $this->belongsTo('App\Models\Setup\Unit')
                    ->select(['id','name','symbol']);
    }
    public function procurementType() {
       return $this->belongsTo('App\Models\Setup\ProcurementType')
                    ->select(['id','name','description']);
    }

    public function activityInputForwards() {
        return $this->hasMany('App\Models\Budgeting\ActivityInputForward');
    }

}
