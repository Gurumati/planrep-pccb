<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PeBudget extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $table = "pe_budgets";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function mtef(){
        return $this->belongsTo('App\Models\Planning\Mtef');
    }

    public function section(){
        return $this->belongsTo('App\Models\Setup\Section');
    }
}
