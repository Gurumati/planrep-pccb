<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class BudgetSubmissionForm extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $table = "budget_submission_forms";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";


    public function budgetSubmissionDefinition(){
            return $this->hasMany('App\Models\Budgeting\BudgetSubmissionDefinition');
    }

    public function budgetSubmissionSubForm()
    {
        return $this->hasMany('App\Models\Budgeting\BudgetSubmissionSubForm');
    }

    public function budgetClass()
    {
        return $this->belongsTo('App\Models\Setup\BudgetClass');
    }

}
