<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Scrutinization extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public static function getAdmin($adminHierarchyId,$decision_level_id,$sectionIds,$canAssignUser,$userId,$p1AdminId){
        $admin = DB::table('vw_scrutinizations')->
                    groupBy('admin',"admin_id")->
                    where('parent1_id',$p1AdminId)->
                    where(function ($q) use($canAssignUser,$userId){
                        if(!$canAssignUser){
                            $q->where('user_id',$userId);
                        }
                    })->
                    where('scru_admin_id',$adminHierarchyId)->
                    whereIn('scru_section_id',$sectionIds)->
                    whereIn('status',[0,1,2])->
                    where('scru_dec_level_id',$decision_level_id)->
                    select("admin as name","admin_id as id")->
                    get();
        return $admin;
    }

    public static function getSectors($adminHierarchyId,$decision_level_id,$sectionIds,$canAssignUser,$userId,$p1AdminId,$adminIds){
        $admin = DB::table('vw_scrutinizations')->
                    groupBy('sector',"sector_id")->
                    where('parent1_id',$p1AdminId)->
                    whereIn('admin_id',$adminIds)->
                    where(function ($q) use($canAssignUser,$userId){
                        if(!$canAssignUser){
                            $q->where('user_id',$userId);
                        }
                    })->
                    where('scru_admin_id',$adminHierarchyId)->
                    whereIn('scru_section_id',$sectionIds)->
                    whereIn('status',[0,1,2])->
                    where('scru_dec_level_id',$decision_level_id)->
                    select("sector as name","sector_id as id")->
                    get();
        return $admin;
    }

    public static function getSubmittedAdminCountByUser($adminHierarchyId,$decision_level_id,$sectionIds,$canAssignUser,$userId){
        $admin = DB::table('vw_scrutinizations')->

                    where(function ($q) use($canAssignUser,$userId){
                        if(!$canAssignUser){
                            $q->where('user_id',$userId);
                        }
                    })->
                    where('scru_admin_id',$adminHierarchyId)->
                    whereIn('scru_section_id',$sectionIds)->
                    whereIn('status',[0,1,2])->
                    where('scru_dec_level_id',$decision_level_id)->
                    distinct('admin_id')->
                    count('admin_id');
        return $admin;
    }
    public static function getSubmittedDeptCountByUser($adminHierarchyId,$decision_level_id,$sectionIds,$canAssignUser,$userId){
        $admin = DB::table('vw_scrutinizations')->
                where(function ($q) use($canAssignUser,$userId){
                    if(!$canAssignUser){
                        $q->where('user_id',$userId);
                    }
                })->
                where('scru_admin_id',$adminHierarchyId)->
                whereIn('scru_section_id',$sectionIds)->
                whereIn('status',[0,1,2])->
                where('scru_dec_level_id',$decision_level_id)->
                distinct('dpt_id')->
                count('dpt_id');
        return $admin;
    }
    public static function getSubmittedCostCentreCountByUser($adminHierarchyId,$decision_level_id,$sectionIds,$canAssignUser,$userId){
        $admin = DB::table('vw_scrutinizations')->
        where(function ($q) use($canAssignUser,$userId){
            if(!$canAssignUser){
                $q->where('user_id',$userId);
            }
        })->
        where('scru_admin_id',$adminHierarchyId)->
        whereIn('scru_section_id',$sectionIds)->
        whereIn('status',[0,1,2])->
        where('scru_dec_level_id',$decision_level_id)->
        distinct('cost_centre')->
        count('cost_centre');
        return $admin;
    }


    public static function getScrutins($adminHierarchyId,$decision_level_id,$sectionIds,$canAssignUser,$userId,$p1AdminId,$adminIds,$sectorIds,$statusIds,$isAssigned){
        $scrutins = DB::table('vw_scrutinizations')->
                    where('parent1_id',$p1AdminId)->
                    whereIn('admin_id',$adminIds)->
                    whereIn('sector_id',$sectorIds)->
                    whereIn('is_assigned',$isAssigned)->
                    where(function ($q) use($canAssignUser,$userId){
                        if(!$canAssignUser){
                            $q->where('user_id',$userId);
                        }
                    })->
                    where('scru_admin_id',$adminHierarchyId)->
                    whereIn('scru_section_id',$sectionIds)->
                    whereIn('status',$statusIds)->
                    where('scru_dec_level_id',$decision_level_id)->
                    distinct('cost_centre_id')->
                    orderBy('id','desc')->
                    get();
        return $scrutins;
    }

    public static function getP1Admin($adminHierarchyId,$decision_level_id,$sectionIds,$canAssignUser,$userId){
        $p1Admin = DB::table('vw_scrutinizations')->
                    groupBy('parent1',"parent1_id")->
                    where(function ($q) use($canAssignUser,$userId){
                        if(!$canAssignUser){
                            $q->where('user_id',$userId);
                        }
                    })->
                    where('scru_admin_id',$adminHierarchyId)->
                    whereIn('scru_section_id',$sectionIds)->
                    whereIn('status',[0,1,2])->
                    where('scru_dec_level_id',$decision_level_id)->
                    select("parent1 as name","parent1_id as id")->
                    get();
        return $p1Admin;
    }

    public static function getStatus($adminHierarchyId,$decisionLevelId,$sectionIdString,$canAssignUser,$userId,$extraFilter=""){
        if($canAssignUser){
            $whereUser ="";
        }else{
            $whereUser =" and user_id=".$userId." ";
        }
       $query ="Select
                  case when is_assigned then 'Assigned' else 'Not Assigned' END as status, count(*)
                FROM vw_scrutinizations
                WHERE 
                  status in (0,1,2) and
                  scru_admin_id=".$adminHierarchyId." and 
                  scru_dec_level_id=".$decisionLevelId." and 
                  scru_section_id in (".$sectionIdString.") ".$whereUser.$extraFilter."
                GROUP BY is_assigned
                
                UNION ALL
                
                Select
                  CASE when status = 0 then 'Not Stated'
                   when status=1 then 'In Progress'
                   when status =2 then 'Finished'
                  END as status, count(*)
                FROM vw_scrutinizations
                WHERE status in (0,1,2) and
                      scru_admin_id=".$adminHierarchyId." and
                      scru_dec_level_id=".$decisionLevelId." and
                      scru_section_id in (".$sectionIdString.") ".$whereUser.$extraFilter."
                GROUP BY status
                
                UNION ALL
                
                Select
                  CASE when recommendation = 1 then 'Passed'
                  when recommendation = -1 then 'To return'
                  END as status, count(*)
                FROM vw_scrutinizations
                WHERE status=2 AND
                      scru_admin_id=".$adminHierarchyId." and
                      scru_dec_level_id=".$decisionLevelId." and
                      scru_section_id in (".$sectionIdString.") ".$whereUser.$extraFilter."
                GROUP BY recommendation";

        return DB::select($query);
    }

    public static function getById($id){
        $scutins = DB::table('scrutinizations as scru')->
                       join('scrutinization_rounds as so','so.id','scru.scrutinization_round_id')->
                       join('mtef_sections as ms','ms.id','so.mtef_section_id')->
                       join('sections as s','s.id','ms.section_id')->
                       join('mtefs as m','m.id','ms.mtef_id')->
                       join('admin_hierarchies as a','a.id','m.admin_hierarchy_id')->
                       leftJoin('users as u','u.id','scru.user_id')->
                       leftjoin('mtef_section_comments as msc','scru.id','msc.scrutinization_id')->
                       where('scru.id',$id)->
                       select('a.name as adminHierarchy','s.name as section','scru.*','a.id as admin_hierarchy_id','so.round as round','so.mtef_section_id',DB::raw("CONCAT(u.first_name || ' '|| u.last_name) as user"),'msc.id as mtefSectionCommentId')->
                       first();

        return $scutins;
    }

    public function scrutinization_round() {
        return $this->belongsTo('App\Models\Budgeting\ScrutinizationRound');
    }

    public function mtef_section_comments() {
        return $this->hasMany('App\Models\Planning\MtefSectionComment');
    }


}
