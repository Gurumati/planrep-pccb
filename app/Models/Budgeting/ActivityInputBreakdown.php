<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;

class ActivityInputBreakdown extends Model
{
    protected $with = [
        'unit'
    ];

    public function unit(){
        return $this->belongsTo('App\Models\Setup\Unit')->select(['id','name','symbol']);
    }
}
