<?php

namespace App\Models\Budgeting;

use App\Http\Services\FinancialYearServices;
use App\Models\Planning\MtefSection;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\DecisionLevel;
use App\Models\Setup\Section;
use App\Models\Setup\Sector;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Http\Services\UserServices;
use App\Models\Auth\User;
use App\Models\Setup\ConfigurationSetting;
use Illuminate\Support\Facades\Log;

class Scrutiny extends Model
{

    public static function getDistinctSubmittedHierarchy($parentAdminHierarchyId, $parentSectionId)
    {
        $decisionLevel = DecisionLevel::getByAdminAndSection($parentAdminHierarchyId,$parentSectionId);

        $financialYear = FinancialYearServices::getPlanningFinancialYear();

        $adminIds = AdminHierarchy::getAdminIdsWithChildren($parentAdminHierarchyId);
        $sectionIds = Section::getSectionIdsWithChildren($parentSectionId);

        if($decisionLevel == null || $financialYear == null || sizeof($adminIds) ==0 || sizeof($sectionIds) == 0){
            return ['budgets'=>[], 'nextDecisionLevels'=>[]];
        }

         $returnDecisionLevels = DecisionLevel::getReturnDecisionLevels($decisionLevel->id);

          if ($decisionLevel->section_level_id > 2){
             $adminHierarchies = DB::table('mtefs as m')->
             join('mtef_sections as ms', 'm.id','ms.mtef_id')->
             join('admin_hierarchies as region', 'region.id','m.admin_hierarchy_id')->
             //leftJoin('admin_hierarchies as region', 'region.id','council.parent_id')->
             where('m.financial_year_id',$financialYear->id)->
             whereIn('m.admin_hierarchy_id',$adminIds)
                 ->whereIn('ms.section_id',$sectionIds);
         }else {
             $adminHierarchies = DB::table('mtefs as m')->
             join('mtef_sections as ms', 'm.id','ms.mtef_id')->
             join('sections as s', 's.id','ms.section_id')->
             join('sections as s2', 's2.id','s.parent_id')->
             join('sections as s3', 's3.id','s2.parent_id')->
             join('sections as s4', 's4.id','s3.parent_id')->
             join('admin_hierarchies as region', 'region.id','m.admin_hierarchy_id')->
             //leftJoin('admin_hierarchies as region', 'region.id','council.parent_id')->
             where('m.financial_year_id',$financialYear->id)->
             whereIn('m.admin_hierarchy_id',$adminIds)
                 ->whereIn('s4.id',$sectionIds);
         }

         $total = $adminHierarchies->where('ms.decision_level_id',$decisionLevel->id)->count();

         if( UserServices::hasPermission(['budgeting.scrutinization.assign_users'])){
            $adminHierarchies =  $adminHierarchies->where('ms.decision_level_id',$decisionLevel->id);
         } else{
            $adminHierarchies =  $adminHierarchies->where('ms.user_id',UserServices::getUser()->id);
        }
        $adminHierarchies = $adminHierarchies->groupBy('region.name','region.id','m.id')->
            select('region.name as region','region.id as admin','m.id as mtefId')->orderBy('region.name')->
            get();
         $allChildren = DB::table('admin_hierarchies as a')
            ->join('admin_hierarchy_levels as al','al.id','a.admin_hierarchy_level_id')
            ->where('parent_id', $parentAdminHierarchyId)
            ->where('al.hierarchy_position', '<=',4)
            ->select('a.id','a.name')->orderBy('a.name')->get();
        $allSectors = Sector::select('id','name')->get();

        return ['adminHierarchies'=>$adminHierarchies, 'decisionLevel'=>$decisionLevel, 'returnDecisionLevels'=>$returnDecisionLevels,
                 'allChildren'=>$allChildren, 'allSectors'=>$allSectors, 'total'=>$total];
    }

    public static function getDistinctSubmittedSectors($parentAdminHierarchyId, $parentSectionId,$mtefId){

        $decisionLevel = DecisionLevel::getByAdminAndSection($parentAdminHierarchyId,$parentSectionId);

        $sectionIds = Section::getSectionIdsWithChildren($parentSectionId);

        if($decisionLevel == null ||  sizeof($sectionIds) == 0){
            return [];
        }
         if ($decisionLevel->section_level_id > 2){
            return DB::table('mtef_sections as ms')->
            join('sections as sec', 'sec.id','ms.section_id')->
            join('sectors as sect', 'sect.id','sec.sector_id')->
            where('ms.decision_level_id',$decisionLevel->id)->
            where('ms.mtef_id',$mtefId)->
            whereIn('ms.section_id',$sectionIds)->
            distinct('sect.id')->
            select('sect.name as sector','sect.id as sectorId')->
            get();
        }
        else {
            return DB::table('mtef_sections as ms')->
            join('sections as sec', 'sec.id','ms.section_id')->
            join('sectors as sect', 'sect.id','sec.sector_id')->
            join('sections as s', 's.id','ms.section_id')->
            join('sections as s2', 's2.id','s.parent_id')->
            join('sections as s3', 's3.id','s2.parent_id')->
            join('sections as s4', 's4.id','s3.parent_id')->
            where('ms.decision_level_id',$decisionLevel->id)->
            where('ms.mtef_id',$mtefId)->
            whereIn('s4.id',$sectionIds)->
            distinct('sect.id')->
            select('sect.name as sector','sect.id as sectorId')->
            get();
        }

    }

    public static function getDistinctSubmittedCostCentres($parentAdminHierarchyId,$parentSectionId,$mtefId,$sectorId){

        $decisionLevel = DecisionLevel::getByAdminAndSection($parentAdminHierarchyId,$parentSectionId);

        $sectionIds = Section::getSectionIdsWithChildren($parentSectionId);

        if($decisionLevel == null ||  sizeof($sectionIds) == 0){
            return [];
        }
        if ($decisionLevel->section_level_id > 2){
            return DB::table('mtef_sections as ms')->
            join('mtef_section_comments as msc','msc.mtef_section_id','ms.id')->
            join('sections as sec', 'sec.id','ms.section_id')->
            where('ms.mtef_id',$mtefId)->
            where('ms.decision_level_id',$decisionLevel->id)->
            // where('sec.sector_id',$sectorId)->
            whereIn('ms.section_id',$sectionIds)->
            select('msc.forward_message','sec.name as cost_centre','sec.id as cost_centre_id','ms.id as mtef_section_id','ms.user_id')->
            orderByDesc('msc.id')->
           // distinct('sec.id')->
            get();
        }else {
            return DB::table('mtef_sections as ms')->
            join('mtef_section_comments as msc','msc.mtef_section_id','ms.id')->
            join('sections as sec', 'sec.id','ms.section_id')->
            join('sections as s', 's.id','ms.section_id')->
            join('sections as s2', 's2.id','s.parent_id')->
            join('sections as s3', 's3.id','s2.parent_id')->
            join('sections as s4', 's4.id','s3.parent_id')->
            where('ms.mtef_id',$mtefId)->
            where('ms.decision_level_id',$decisionLevel->id)->
            // where('sec.sector_id',$sectorId)->
            whereIn('s4.id',$sectionIds)->
            select('msc.forward_message','sec.name as cost_centre','sec.id as cost_centre_id','ms.id as mtef_section_id','ms.user_id')->
            orderByDesc('msc.id')->
            //distinct('sec.name')->
            get();
        }
    }
    public static function getDistinctSubmittedDepartments($parentAdminHierarchyId,$parentSectionId,$mtefId,$sectorId){

        $decisionLevel = DecisionLevel::getByAdminAndSection($parentAdminHierarchyId,$parentSectionId);
         $sectionIds = Section::getSectionIdsWithChildren($parentSectionId);

        if($decisionLevel == null ||  sizeof($sectionIds) == 0){
            return [];
        }
        if ($decisionLevel->section_level_id > 2){
            return DB::table('mtef_sections as ms')->
            join('sections as sec', 'sec.id','ms.section_id')->
            join('sections as dept', 'dept.id','sec.parent_id')->
            where('ms.mtef_id',$mtefId)->
            where('ms.decision_level_id',$decisionLevel->id)->
            // where('sec.sector_id',$sectorId)->
            whereIn('ms.section_id',$sectionIds)->
            select('dept.name as department','dept.id as department_id','ms.user_id')->
            distinct()->
            orderBy('dept.name')->get();
        }else {
            return DB::table('mtef_sections as ms')->
            join('sections as sec', 'sec.id','ms.section_id')->
            join('sections as dept', 'dept.id','sec.parent_id')->
            join('sections as s3', 's3.id','dept.parent_id')->
            join('sections as s4', 's4.id','s3.parent_id')->
            where('ms.mtef_id',$mtefId)->
            where('ms.decision_level_id',$decisionLevel->id)->
            // where('sec.sector_id',$sectorId)->
            whereIn('s4.id',$sectionIds)->
            select('dept.name as department','dept.id as department_id','ms.user_id')->
            distinct()->
            orderBy('dept.name')->get();
        }

    }

    public static function moveByMtef($mtefId, $fromDecisionLevelId, $toDecisionLevelId)
    {
        $toDecisionLevel = DecisionLevel::find($toDecisionLevelId);
        if($toDecisionLevel == null){
            throw(\Exception);
        }

        $isLocked = ($toDecisionLevel->id == 8)? false : true;

        MtefSection::where('mtef_id',$mtefId)->
                     where('decision_level_id',$fromDecisionLevelId)->
                     update(['decision_level_id' => $toDecisionLevelId, 'is_locked'=>$isLocked, 'user_id'=>UserServices::getUser()->id]);
    }

    public static function moveBySector($mtefId,$sectorId, $fromDecisionLevelId, $toDecisionLevelId)
    {
        $toDecisionLevel = DecisionLevel::find($toDecisionLevelId);
        if($toDecisionLevel == null){
            throw(\Exception);
        }

        $sectionIds = DB::table('mtef_sections as ms')->
                        join('sections as s','s.id','ms.section_id')->
                        join('sections as s2','s2.id','s.parent_id')->
                        where('mtef_id',$mtefId)->
                        where('ms.decision_level_id',$fromDecisionLevelId)->
                        where('s2.id',$sectorId)
                        ->pluck('ms.id');

        $isLocked = ($toDecisionLevel->id == 4)? false : true;

        MtefSection::where('mtef_id',$mtefId)->
                    where('decision_level_id',$fromDecisionLevelId)->
                    whereIn('id',$sectionIds)->
                    update(['decision_level_id' => $toDecisionLevelId, 'is_locked'=>$isLocked, 'user_id'=>UserServices::getUser()->id]);
    }

    public static function moveByMtefSection($mtefSectionId, $fromDecisionLevelId, $toDecisionLevelId)
    {
        $toDecisionLevel = DecisionLevel::find($toDecisionLevelId);
        if($toDecisionLevel == null){
            throw(\Exception);
        }

        $isLocked = ($toDecisionLevel->id == 4)? false : true;

        MtefSection::where('id',$mtefSectionId)->
                    where('decision_level_id',$fromDecisionLevelId)->
                    update(['decision_level_id' => $toDecisionLevelId, 'is_locked'=>$isLocked, 'user_id'=>UserServices::getUser()->id]);
    }

    public static function assignUser($mtefSectionId, $userId){
        MtefSection::find($mtefSectionId)->update(['user_id'=>$userId]);
    }

}
