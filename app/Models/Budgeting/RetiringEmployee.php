<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RetiringEmployee extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $table = "retiring_employees";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function section(){
        return $this->belongsTo('App\Models\Setup\Section');
    }

    public function admin_hierarchy(){
        return $this->belongsTo('App\Models\Setup\AdminHierarchy');
    }
}
