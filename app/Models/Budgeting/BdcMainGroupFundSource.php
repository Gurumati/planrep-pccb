<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BdcMainGroupFundSource extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'bdc_main_group_fund_sources';


    public function financial_year() {
        return $this->belongsTo('App\Models\Setup\FinancialYear','financial_year_id','id');
    }

    public function reference_document() {
        return $this->belongsTo('App\Models\Setup\ReferenceDocument','reference_document_id','id');
    }

    public function bdc_main_group() {
        return $this->belongsTo('App\Models\Setup\BdcMainGroup','main_group_id','id');
    }

    public function fund_source() {
        return $this->belongsTo('App\Models\Setup\FundSource','fund_source_id','id');
    }
}
