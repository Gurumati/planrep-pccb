<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminHierarchyCeilingForward extends Model
{
    protected $table = 'admin_hierarchy_ceiling_forwards';
    public $timestamps = false;


    public function admin_hierarchy_ceiling() {
        return $this->belongsTo('App\Models\Budgeting\AdminHierarchyCeiling');
    }
}
