<?php

namespace App\models\budgeting;

use Illuminate\Database\Eloquent\Model;

class PcInput extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'activity_inputs';

    protected $with=['children'];

    public function children(){
        return $this->hasMany('App\Models\Budgeting\PcInput');
    }

    public function planChain(){
        return $this->belongsTo('App\Models\Budgeting\PcTarget');
    }

}
