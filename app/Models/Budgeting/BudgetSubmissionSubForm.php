<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetSubmissionSubForm extends Model
{
    //
    public $timestamps = true;
    //use SoftDeletes;
    protected $table = "budget_submission_sub_forms";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $with = ['budgetSubmissionForm'];

    public function budgetSubmissionForm()
    {
        return $this->belongsTo('App\Models\Budgeting\BudgetSubmissionForm')->select('id','name','description','budget_classes','fund_sources');
    }

    public function child_budgetForm()
    {
        return $this->hasMany('App\Models\Budgeting\BudgetSubmissionSubForm', 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Budgeting\BudgetSubmissionSubForm');
    }

    public function budgetSubmissionLines()
    {
        return $this->hasMany('App\Models\Budgeting\BudgetSubmissionLine');

    }
}