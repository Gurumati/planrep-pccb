<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;

class AdminHierarchyCeilingDoc extends Model
{
    public $timestamps = true;
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'admin_hierarchy_ceiling_docs';
    protected $fillables = ['admin_hierarchy_id', 'financial_year_id', 'budget_type', 'document_url'];

}
