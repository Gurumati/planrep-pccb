<?php

namespace App\models\budgeting;

use Illuminate\Database\Eloquent\Model;

class PcLongTarget extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'long_term_targets';

    public function children(){
        return $this->hasMany('App\Models\Budgeting\PcTarget');
    }

    public function planChain(){
        return $this->belongsTo('App\Models\Budgeting\PcPlanChain');
    }



}
