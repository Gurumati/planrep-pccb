<?php

namespace App\Models\Budgeting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetSubmissionSelectOption extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $table = "budget_submission_select_option";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $with = ['children'];

    public function children(){
        return $this->hasMany('App\Models\Budgeting\BudgetSubmissionSelectOption', 'parent_id');
    }

    public function parent(){
        return $this->belongsTo('App\Models\Budgeting\BudgetSubmissionSelectOption');
    }
}
