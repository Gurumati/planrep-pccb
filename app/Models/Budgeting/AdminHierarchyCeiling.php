<?php

namespace App\Models\Budgeting;

use App\Http\Services\UserServices;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\CeilingChain;
use App\Models\Setup\Section;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use App\Models\Setup\Facility;
use App\Models\Setup\SectionParent;
use App\Http\Controllers\Flatten;
use App\Http\Services\Execution\RevenueAccountServices;
use App\Models\Setup\BudgetClassVersion;
use App\Models\Setup\FundSourceVersion;
use App\Models\Setup\Version;

class AdminHierarchyCeiling extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'admin_hierarchy_ceilings';

    protected $fillable = ['ceiling_id', 'amount', 'admin_hierarchy_id', 'budget_type', 'financial_year_id', 'section_id', 'facility_id', 'is_facility', 'is_approved', 'is_locked', 'created_by', 'updated_by'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'amount' => ['required', 'numeric', 'min:0']
            ],
            $merge
        );
    }


    /**
     * Get paginated Admin hierarchy ceilings;
     *
     * @param $budgetType
     * @param $financialYearId
     * @param $adminHierarchyId
     * @param $sectionId
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function paginated($budgetType, $financialYearId, $adminHierarchyId, $sectionId)
    {

        if ($budgetType == 'APPROVED') {
            $budgetType2 = ['CURRENT', 'APPROVED'];
        } else {
            $budgetType2 = [$budgetType];
        }

        $adminHierarchyId = 3;
        $versionBcId = Version::getVersionByFinancialYear($financialYearId, 'BC');
        $budgetClassIds = BudgetClassVersion::select('budget_class_id')->where('version_id', $versionBcId)->pluck('budget_class_id')->toArray();

        $versionId = Version::getVersionByFinancialYear($financialYearId, 'FS');
        $fundSourceIds = FundSourceVersion::select('fund_source_id')->where('version_id', $versionId)->pluck('fund_source_id')->toArray();

        $perPage = Input::get("perPage", 10);
        $searchQuery = Input::get("searchQuery");
        $searchQuery = isset($searchQuery) ? "'%" . strtolower($searchQuery) . "%'" : "'%'";


        $all = AdminHierarchyCeiling::with('ceiling', 'ceiling.budget_class', 'ceiling.gfs_code.fund_source_gfs.fund_source', 'ceiling.aggregate_fund_source')
            ->whereHas('ceiling', function ($query) use ($fundSourceIds) {
                $query->where(function ($query2) use ($fundSourceIds) {
                    $query2->whereHas('gfs_code.fund_source_gfs.fund_source', function ($query3) use ($fundSourceIds) {
                        $query3->where('can_project', false);
                        $query3->whereIn('gfscode_fundsources.fund_source_id', $fundSourceIds);
                    })->orWhereNull('gfs_code_id');
                })->where('is_active', true);
            })
            ->whereHas('ceiling', function ($query) use ($budgetClassIds) {
                $query->where(function ($query2) use ($budgetClassIds) {
                    $query2->whereHas('budget_class', function ($query3) use ($budgetClassIds) {
                    })->WhereIn('budget_class_id', $budgetClassIds);
                });
            })
            ->where(function ($q) use ($searchQuery, $fundSourceIds, $budgetClassIds) {
                $q->whereHas('ceiling', function ($cq) use ($searchQuery) {
                    $cq->whereRaw('LOWER(name) like ' . $searchQuery);
                })->orWhereHas('ceiling.budget_class', function ($bq) use ($searchQuery, $budgetClassIds) {
                    $bq->whereRaw('LOWER(name) like ' . $searchQuery);
                    $bq->whereIn('budget_class_id', $budgetClassIds);
                })->orWhereHas('ceiling.gfs_code.fund_source_gfs.fund_source', function ($fq) use ($searchQuery, $fundSourceIds) {
                    $fq->whereRaw('LOWER(name) like ' . $searchQuery);
                    $fq->whereIn('id', $fundSourceIds);
                })->orWhereHas('ceiling.aggregate_fund_source', function ($fq) use ($searchQuery) {
                    $fq->whereRaw('LOWER(name) like ' . $searchQuery);
                });
            })
            ->whereIn('budget_type', $budgetType2)
            ->where('financial_year_id', $financialYearId)
            ->where('admin_hierarchy_id', $adminHierarchyId)
            ->where('is_facility', false)
            ->where('section_id', $sectionId)
            ->orderBy('admin_hierarchy_ceilings.id')
            ->paginate($perPage);
        foreach ($all as &$c) {
            $index = 0;
            $fundSourceGfs = isset($c->ceiling->gfs_code->fund_source_gfs) ? $c->ceiling->gfs_code->fund_source_gfs : [];
            $fundSources = sizeof($fundSourceGfs) > 0 ? $fundSourceGfs : [];
            if (isset($c->ceiling->gfs_code)) {
                foreach ($fundSources as &$fund) {
                    if (in_array($fund->fund_source_id, $fundSourceIds)) {
                        $c->ceiling->gfs_code->fund_source = $fund->fund_source;
                    }
                }
            }

            $fundSourceId = isset($c->ceiling->gfs_code->fund_source) ? $c->ceiling->gfs_code->fund_source->id : $c->ceiling->aggregate_fund_source_id;
            $c->bank_account = DB::table('bank_accounts as ba')
                ->join('fund_source_budget_classes as fbc', 'fbc.bank_account_id', 'ba.id')
                ->where('fbc.fund_source_id', $fundSourceId)
                ->where('fbc.budget_class_id', $c->ceiling->budget_class_id)
                ->select('ba.*')
                ->first();
        }
        return $all;
    }

    public static function ceilingToExport($budgetType, $financialYearId, $adminHierarchyId, $sectionId){
        if ($budgetType == 'APPROVED') {
            $budgetType2 = ['CURRENT', 'APPROVED'];
        } else {
            $budgetType2 = [$budgetType];
        }

        $versionBcId = Version::getVersionByFinancialYear($financialYearId, 'BC');
        $budgetClassIds = BudgetClassVersion::select('budget_class_id')->where('version_id', $versionBcId)->pluck('budget_class_id')->toArray();

        $versionId = Version::getVersionByFinancialYear($financialYearId, 'FS');
        $fundSourceIds = FundSourceVersion::select('fund_source_id')->where('version_id', $versionId)->pluck('fund_source_id')->toArray();

        $perPage = Input::get("perPage", 10);
        $searchQuery = Input::get("searchQuery");
        $searchQuery = isset($searchQuery) ? "'%" . strtolower($searchQuery) . "%'" : "'%'";


        $all = AdminHierarchyCeiling::with('ceiling', 'ceiling.budget_class', 'ceiling.gfs_code.fund_source_gfs.fund_source', 'ceiling.aggregate_fund_source')
            ->whereHas('ceiling', function ($query) use ($fundSourceIds) {
                $query->where(function ($query2) use ($fundSourceIds) {
                    $query2->whereHas('gfs_code.fund_source_gfs.fund_source', function ($query3) use ($fundSourceIds) {
                        $query3->where('can_project', false);
                        $query3->whereIn('gfscode_fundsources.fund_source_id', $fundSourceIds);
                    })->orWhereNull('gfs_code_id');
                })->where('is_active', true);
            })
            ->whereHas('ceiling', function ($query) use ($budgetClassIds) {
                $query->where(function ($query2) use ($budgetClassIds) {
                    $query2->whereHas('budget_class', function ($query3) use ($budgetClassIds) {
                    })->WhereIn('budget_class_id', $budgetClassIds);
                });
            })

            ->where(function ($q) use ($searchQuery, $fundSourceIds, $budgetClassIds) {
                $q->whereHas('ceiling', function ($cq) use ($searchQuery) {
                    $cq->whereRaw('LOWER(name) like ' . $searchQuery);
                })->orWhereHas('ceiling.budget_class', function ($bq) use ($searchQuery, $budgetClassIds) {
                    $bq->whereRaw('LOWER(name) like ' . $searchQuery);
                    $bq->whereIn('budget_class_id', $budgetClassIds);
                })->orWhereHas('ceiling.gfs_code.fund_source_gfs.fund_source', function ($fq) use ($searchQuery, $fundSourceIds) {
                    $fq->whereRaw('LOWER(name) like ' . $searchQuery);
                    $fq->whereIn('id', $fundSourceIds);
                })->orWhereHas('ceiling.aggregate_fund_source', function ($fq) use ($searchQuery) {
                    $fq->whereRaw('LOWER(name) like ' . $searchQuery);
                });
            })
            ->whereIn('budget_type', $budgetType2)
            ->where('financial_year_id', $financialYearId)
            ->where('admin_hierarchy_id', $adminHierarchyId)
            ->where('is_facility', false)
            ->where('section_id', $sectionId)
            ->orderBy('admin_hierarchy_ceilings.id')
            ->get();
        foreach ($all as &$c) {
            $index = 0;
            $fundSourceGfs = isset($c->ceiling->gfs_code->fund_source_gfs) ? $c->ceiling->gfs_code->fund_source_gfs : [];
            $fundSources = sizeof($fundSourceGfs) > 0 ? $fundSourceGfs : [];
            if (isset($c->ceiling->gfs_code) ) {
                foreach ($fundSources as &$fund) {
                    if (in_array($fund->fund_source_id, $fundSourceIds)) {
                        $c->ceiling->gfs_code->fund_source = $fund->fund_source;
                    }
                }
            }

            $fundSourceId = isset($c->ceiling->gfs_code->fund_source) ? $c->ceiling->gfs_code->fund_source->id : $c->ceiling->aggregate_fund_source_id;
            $c->bank_account = DB::table('bank_accounts as ba')
                ->join('fund_source_budget_classes as fbc', 'fbc.bank_account_id', 'ba.id')
                ->where('fbc.fund_source_id', $fundSourceId)
                ->where('fbc.budget_class_id', $c->ceiling->budget_class_id)
                ->select('ba.*')
                ->first();
        }
       // return $all;
        $dataToDisplay = array();
        foreach ($all as $filtered){
            if($filtered->ceiling->aggregate_fund_source_id != 58){
                array_push($dataToDisplay,$filtered);
            }
        }
        return $dataToDisplay;
    }

    public static function getByCeiling($ceilingId){
        return AdminHierarchyCeiling::with('admin_hierarchy','section','financial_year')
            ->where('ceiling_id',$ceilingId)
            ->where('amount','>',0)
            ->orderBy('amount', 'ASC')
            ->paginate(Input::get('perPage',10));
    }

    public static function firstLowerLevel($ceilingId, $budgetType, $financialYearId, $parentAdminHierarchyId, $parentSectionId, $isSum)
    {

        /** @var  $parentCeilingChain {Object} = parent/current ceiling chain intends to allocate ceiling to its lower chain */
        $parentCeilingChain = CeilingChain::getCurrentCeilingChain($parentAdminHierarchyId, $parentSectionId);

        /** @var  $ceilingSectorIds [Array of sector Ids] to determine lower ceiling chain section based on the ceiling sector */
        $ceilingSectorIds = Ceiling::getSectorIds($ceilingId);


        $all = [];

        /** Parent ceiling chain not found return invalid request */
        if ($parentCeilingChain === null) {
//            throw ("INVALID_CEILING_CHAIN");
        }

        /**
         * Get next ceiling chain (i.e Which admin level and section to be allocated) based on the parent ceiling chain (i.e Which admin level and section doing allocation)
         */
        $nextLowerCeilingChain = CeilingChain::getNextCeilingChain($parentCeilingChain->next_id);

        /**
         * Next Ceiling chain is found proceed to check if next is not or on the same  admin hierarchy level  of parent to determine which admin hierarchies and or sections to be allocated next
         */
        if ($nextLowerCeilingChain !== null) {
            log::info('nimooo 1');
            /** @var  $lowerCeilingChainAdminHierarchyIds initialize lower ceiling chain admin hierarchies ids*/
            $lowerCeilingChainAdminHierarchyIds = [];

            /** @var  $lowerCeilingChainSectionIds array(section id) */
            $lowerCeilingChainSectionIds = CeilingChain::getNextLowerCeilingChainSection($ceilingSectorIds, $parentSectionId, $nextLowerCeilingChain->section_level_position);
            /**
             *  ParentAdminHierarchy  =  NextAdminHierarchy
             */
            if ($nextLowerCeilingChain->admin_hierarchy_level_position === $parentCeilingChain->admin_hierarchy_level_position) {

                /** push parent admin hierarchy id as lower ceiling chain admin hierarchy  */
                $lowerCeilingChainAdminHierarchyIds[] = $parentAdminHierarchyId;

            }
            /** ParentAdminHierarchy  !=  NextAdminHierarchy */
            else {

                $lowerCeilingChainAdminHierarchyIds = CeilingChain::getNextLowerCeilingChainAdminHierarchies($parentAdminHierarchyId, $nextLowerCeilingChain->admin_hierarchy_level_position);

            }
            if (!$isSum) {
                AdminHierarchyCeiling::initiateLowerChain($ceilingId, $budgetType, $financialYearId, $lowerCeilingChainAdminHierarchyIds, $lowerCeilingChainSectionIds, null);
            }
            $all = DB::table('admin_hierarchy_ceilings as ac')
                ->join('ceilings as c', 'ac.ceiling_id', 'c.id')
                ->Join('admin_hierarchy_sections as ahsc', function($join){
                    $join->on('ahsc.admin_hierarchy_id', '=', 'ac.admin_hierarchy_id');
                    $join->on('ahsc.section_id', '=', 'ac.section_id');
                })
                ->leftJoin('admin_hierarchies as a', 'a.id', 'ac.admin_hierarchy_id')
                ->leftJoin('sections as s', 's.id', 'ac.section_id')
                ->leftJoin('section_levels as sl', 'sl.id', 's.section_level_id')
                ->leftJoin('admin_hierarchy_levels as al', 'al.id', 'a.admin_hierarchy_level_id')

                //->where('ahsc.section_id', 'ac.section_id')
                ->where('ac.financial_year_id', $financialYearId)
                ->where('c.id', $ceilingId)
                ->where('c.is_active', true)
                ->where('ac.is_facility', false)
                ->where('ac.budget_type', $budgetType)
                ->whereIn('ac.admin_hierarchy_id', $lowerCeilingChainAdminHierarchyIds)
                ->whereIn('ac.section_id', $lowerCeilingChainSectionIds)
                ->whereNull('c.deleted_at');
            if ($isSum) {
                $all = $all->sum('ac.amount');
            } else {
                $all = $all->select(
                    'ac.*',
                    'c.id as ceilingId',
                    'a.id as adminHierarchyId',
                    's.id as sectionId',
                    's.name as section',
                    'sl.id as sectionLevelId',
                    'sl.name as sectionLevel',
                    'al.name as adminLevel',
                    'al.id as adminLevelId',
                    'c.name as ceiling',
                    'a.name as admin_hierarchy'
                )->orderBy('s.name')->distinct('ac.section_id')->get();
            }

        } else {

            log::info('nimooo 2');
            $ceiling = Ceiling::find($ceilingId);

            $mtefSection = DB::table('mtef_sections as ms')
                ->join('mtefs as m', 'm.id', 'ms.mtef_id')
                ->where('ms.section_id', $parentSectionId)
                ->where('m.financial_year_id', $financialYearId)
                ->where('m.admin_hierarchy_id', $parentAdminHierarchyId)
                ->select('ms.*')
                ->first();

            $facilities = [];
            
            $pq = DB::table('admin_hierarchy_ceilings as ac')
                ->join('ceilings as c', 'ac.ceiling_id', 'c.id')
                ->join('admin_hierarchies as a', 'a.id', 'ac.admin_hierarchy_id')
                ->join('sections as s', 's.id', 'ac.section_id')
                ->join('section_levels as sl', 'sl.id', 's.section_level_id')
                ->join('admin_hierarchy_levels as al', 'al.id', 'a.admin_hierarchy_level_id')
                ->join('facilities as f', 'f.id', 'ac.facility_id')
                ->join('facility_types as ft', 'ft.id', 'f.facility_type_id')
                ->where('ac.financial_year_id', $financialYearId)
                ->where('c.id', $ceilingId)
                ->whereNotNull('ac.facility_id')
                ->where('f.is_active', true)
                ->where('c.is_active', true)
                ->where('ac.admin_hierarchy_id', $parentAdminHierarchyId)
                ->where('ac.section_id', $parentSectionId)
                ->where('ac.budget_type', $budgetType)
                ->whereNull('c.deleted_at');



            $existing = $pq->pluck('facility_id')->toArray();
            $existing = array_merge($existing, [0]);
            $exclude = '(' . implode(',', $existing) . ')';

            
            $allFacilities = [];
            if ($mtefSection !== null) {
                $allFacilities = Facility::getAllByPlanningSection($mtefSection->id, true, $exclude);
            }
            $fIds = $existing;
            foreach ($allFacilities as $facility) {
                $fIds[] = $facility->id;
                if (!$isSum) {
                    $existingCeiling = AdminHierarchyCeiling::where('ceiling_id', $ceilingId)
                        ->where('admin_hierarchy_id', $parentAdminHierarchyId)
                        ->where('financial_year_id', $financialYearId)
                        ->where('section_id', $parentSectionId)
                        ->where('budget_type', $budgetType)
                        ->where('facility_id', $facility->id)
                        ->where('is_facility', true)
                        ->first();

                    if ($existingCeiling == null) {
                        AdminHierarchyCeiling::create([
                            'ceiling_id' => $ceilingId,
                            'amount' => 0,
                            'dissemination_status' => 0,
                            'use_status' => 0,
                            'admin_hierarchy_id' => $parentAdminHierarchyId,
                            'financial_year_id' => $financialYearId,
                            'section_id' => $parentSectionId,
                            'budget_type' => $budgetType,
                            'facility_id' => $facility->id,
                            'is_facility' => true,
                            'created_by' => UserServices::getUser()->id
                        ]);
                    }
                }
            }
            $all = $pq->whereIn('ac.facility_id', $fIds);
            if ($isSum) {
                $all = $all->sum('ac.amount');
            } else {
                $all = $all->select(
                    'ac.*',
                    'c.id as ceilingId',
                    'a.id as adminHierarchyId',
                    's.id as sectionId',
                    's.name as section',
                    'f.name as facility',
                    'ft.name as type',
                    'sl.id as sectionLevelId',
                    'sl.name as sectionLevel',
                    'al.name as adminLevel',
                    'al.id as adminLevelId',
                    'c.name as ceiling',
                    'a.name as admin_hierarchy'
                )->orderBy('type', 'asc')->distinct('ac.facility_id')->get();
            }

        }

        return $all;
    }

    /**
     * Get Ceiling to initiate to an admin hierarchy and section base on the starting point of ceiling chain
     *
     * @param $budgetType
     * @param $adminHierarchyId
     * @param $sectionId
     * @param $financialYearId
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|null
     */
    public static function paginateCeilingToInitiate($budgetType, $financialYearId, $adminHierarchyId, $sectionId)
    {
        $ceilingToInitiate = ['data' => []];


        $versionBcId = Version::getVersionByFinancialYear($financialYearId, 'BC');
        $budgetClassIds = BudgetClassVersion::select('budget_class_id')->where('version_id', $versionBcId)->pluck('budget_class_id')->toArray();

        $versionId = Version::getVersionByFinancialYear($financialYearId, 'FS');
        $fundSourceIds = FundSourceVersion::select('fund_source_id')->where('version_id', $versionId)->pluck('fund_source_id')->toArray();

        if (self::isAtStartCeilingChain($adminHierarchyId, $sectionId)) {

            $ceilings = Ceiling::getForBudget();

            $existingAdminCeilings = AdminHierarchyCeiling::with('ceiling')->whereHas('ceiling', function ($filter1) {
                $filter1->where(function ($filter2) {
                    $filter2->whereHas('gfs_code.fund_source_gfs.fund_source', function ($filter3) {
                        $filter3->where('can_project', false);
                    })->orWhereNull('gfs_code_id');
                })->where('is_active', true)->whereNull('deleted_at');
            })->where('admin_hierarchy_id', $adminHierarchyId)->where('financial_year_id', $financialYearId)->where('section_id', $sectionId)->where('budget_type', $budgetType)->get();

            if (sizeof($ceilings) > sizeof($existingAdminCeilings)) {
                $idsToExclude = [];
                foreach ($existingAdminCeilings as $c) {
                    $idsToExclude[] = $c->ceiling_id;
                }



                $perPage = Input::get("perPage", 10);
                $searchQueryFund = Input::get("searchQueryFund");
                $searchQueryBc = Input::get("searchQueryBc");
                $perPage = isset($perPage) ? $perPage : 10;
                $searchQueryFund = isset($searchQueryFund) ? "'%" . strtolower($searchQueryFund) . "%'" : "'%'";
                $searchQueryBc = isset($searchQueryBc) ? "'%" . strtolower($searchQueryBc) . "%'" : "'%'";


                $ceilingToInitiate = Ceiling::with('gfs_code', 'budget_class', 'gfs_code.fund_source_gfs.fund_source', 'aggregate_fund_source')->where(function ($filter1) use ($fundSourceIds) {
                    $filter1->whereHas(
                        'gfs_code.fund_source_gfs.fund_source',
                        function ($filter2) use ($fundSourceIds) {
                            $filter2->whereIn('gfscode_fundsources.fund_source_id', $fundSourceIds);
                            $filter2->where('can_project', false);
                        }
                    )->orWhereNull('gfs_code_id');
                })->where('is_active', true)
                    ->whereIn('budget_class_id', $budgetClassIds)
                    ->where(function ($search) use ($searchQueryFund, $searchQueryBc, $fundSourceIds, $budgetClassIds) {
                        $search->whereHas('budget_class', function ($bq) use ($searchQueryBc, $budgetClassIds) {
                            $bq->whereIn('id', $budgetClassIds);
                            $bq->whereRaw('LOWER(name) like ' . $searchQueryBc);
                        })->where(function ($fund) use ($searchQueryFund, $fundSourceIds) {
                            $fund->whereHas('gfs_code.fund_source_gfs.fund_source', function ($fq) use ($searchQueryFund, $fundSourceIds) {
                                $fq->whereIn('id', $fundSourceIds);
                                $fq->whereRaw('LOWER(name) like ' . $searchQueryFund);
                            })->orWhereHas('aggregate_fund_source', function ($fq) use ($searchQueryFund) {
                                $fq->whereRaw('LOWER(name) like ' . $searchQueryFund);
                            });
                        });
                    })->whereNotIn('ceilings.id', $idsToExclude)->paginate($perPage);
            }
        }
        foreach ($ceilingToInitiate as &$gfs) {
            $fundSourceGfs = isset($gfs->gfs_code->fund_source_gfs) ? ($gfs->gfs_code->fund_source_gfs) : [];
            $fundSources = sizeof($fundSourceGfs) > 0 ? $fundSourceGfs : [];
            if (isset($gfs->gfs_code)) {
                foreach ($fundSources as &$fund) {
                    if (in_array($fund->fund_source_id, $fundSourceIds)) {
                        $gfs->gfs_code->fund_source = $fund->fund_source;
                    }
                }
            }
            unset($gfs->fund_source_gfs);
        }
        return $ceilingToInitiate;
    }

    public static function isAtStartCeilingChain($adminHierarchyId, $sectionId)
    {

        $startChain = CeilingChain::getStartingPoint();

        if($adminHierarchyId==1)
            $adminAtCeilingStart = AdminHierarchy::isAtCeilingStartingPoint(1, $adminHierarchyId);
        else
            $adminAtCeilingStart = AdminHierarchy::isAtCeilingStartingPoint($startChain->admin_hierarchy_level_position, $adminHierarchyId);


        $sectionAtCeilingStart = Section::adminAtCeilingStart($startChain->section_level_position, $sectionId);

        return $adminAtCeilingStart && $sectionAtCeilingStart;
    }

    /**
     * Initiate Admin hierarchy ceiling by budget type
     *
     * @param $ceilingId
     * @param $budgetType
     * @param $financialYearId
     * @param $adminHierarchyId
     * @param $sectionId
     * @param $facilityId
     *
     */
    public static function initiateAdminHierarchyCeiling($ceilingId, $budgetType, $financialYearId, $adminHierarchyId, $sectionId, $facilityId)
    {

        $adminHierarchyId=3;
        $exist = AdminHierarchyCeiling::where('ceiling_id', $ceilingId)->where('budget_type', $budgetType)->where('financial_year_id', $financialYearId)->where('admin_hierarchy_id', $adminHierarchyId)->where('section_id', $sectionId)->
        where(function ($filter) use ($facilityId) {
            if ($facilityId == null) {
                $filter->where('is_facility', false);
                $filter->whereNull('facility_id');
            } else {
                $filter->where('facility_id', $facilityId);
            }
        });
        if ($exist->count() > 0) {
            return $exist->first();
        }

        $userId = isset(UserServices::getUser()->id) ? UserServices::getUser()->id : null;

        $created = AdminHierarchyCeiling::create([
            'ceiling_id' => $ceilingId,
            'amount' => 0,
            'dissemination_status' => 0,
            'use_status' => 0,
            'budget_type' => $budgetType,
            'admin_hierarchy_id' => $adminHierarchyId,
            'financial_year_id' => $financialYearId,
            'section_id' => $sectionId,
            'is_facility' => ($facilityId == null) ? false : true,
            'facility_id' => ($facilityId == null) ? null : $facilityId,
            'created_by' => $userId
        ]);
        if (!$userId){
            //pass
        } else {
            track_activity($created, UserServices::getUser(), 'initiate_ceiling');
        }
        return $created;
    }

    /**
     * Toggle all admin hierarchy locked or unlocked
     *
     * @param $adminHierarchyId
     * @param $financialYearId
     * @param $isLocked
     */
    public static function toggleLockAll($adminHierarchyId, $financialYearId, $isLocked)
    {
        AdminHierarchyCeiling::where('admin_hierarchy_id', $adminHierarchyId)->where('financial_year_id', $financialYearId)->update(['is_locked' => $isLocked]);
    }


    public static function getTotalProjectionRevenue($budgetType, $adminHierarchyId, $financialYearId, $sectionId, $fundSourceId)
    {
        $versionId =Version::getVersionByFinancialYear($financialYearId, 'BC');
        $fVersionId = Version::getVersionByFinancialYear($financialYearId, 'FS');
        $budgetClassIds =BudgetClassVersion::where('version_id',$versionId)->pluck('budget_class_id')->toArray();
        $fundSourceIds =FundSourceVersion::where('version_id',$fVersionId)->pluck('fund_source_id')->toArray();

        $revenuesSBC = DB::Table('budget_classes as bc')
            ->where('bc.code', '501')
            ->whereIN('bc.id',$budgetClassIds)->select('bc.*')
            ->pluck('bc.id')->toArray();

        $totalRevenue = AdminHierarchyCeiling::with('ceiling', 'ceiling.gfs_code')
            ->where('admin_hierarchy_id', $adminHierarchyId)
            ->where('financial_year_id', $financialYearId)
            ->where('section_id', $sectionId)
            ->where('budget_type', $budgetType)
            ->whereHas('ceiling', function ($ce) use ($revenuesSBC,$budgetClassIds) {
                $ce->whereIn('budget_class_id', $revenuesSBC)
                ->whereIn('budget_class_id',$budgetClassIds);
            })
            ->whereHas('ceiling.gfs_code', function ($query) use ($fundSourceId,$budgetClassIds,$fundSourceIds, $financialYearId) {
                $query->join('gfscode_fundsources as gfsf', 'gfsf.gfs_code_id', 'gfs_code_id')
                    ->join('fund_source_budget_classes as fsbc','fsbc.fund_source_id','gfsf.fund_source_id')
                    ->where('gfsf.fund_source_id', $fundSourceId)
                    ->whereIn('gfsf.fund_source_id',$fundSourceIds)
                    ->whereIn('fsbc.budget_class_id',$budgetClassIds);
                $query->where('gfsf.fund_source_id', $fundSourceId);
            })
            ->whereNull('admin_hierarchy_ceilings.deleted_at');
            $totalRevenue = $totalRevenue->sum('amount');

        return $totalRevenue;
    }



    public static function getForwards($gfsCodeId, $budgetType, $financialYearId, $adminHierarchyId, $sectionId)
    {
        $forwards = DB::table('admin_hierarchy_ceiling_forwards as admcf')
            ->join('admin_hierarchy_ceilings as admc', 'admc.id', 'admcf.admin_hierarchy_ceiling_id')
            ->join('ceilings as c', 'c.id', 'admc.ceiling_id')
            ->where('c.gfs_code_id', $gfsCodeId)
            ->where('admc.admin_hierarchy_id', $adminHierarchyId)
            ->where('admc.financial_year_id', $financialYearId)
            ->where('admc.section_id', $sectionId)
            ->where('admc.budget_type', $budgetType)
            ->select('admcf.*')
            ->get();

        return $forwards;
    }

    /**
     * Toggle specific Admin hierarchy ceiling at a section locked or unlocked
     *
     * @param $adminHierarchyCeilingId
     * @param $isLocked
     */
    public static function toggleLocked($adminHierarchyCeilingId, $isLocked)
    {
        $adminHierarchyCeiling = AdminHierarchyCeiling::find($adminHierarchyCeilingId);
        $adminHierarchyCeiling->is_locked = $isLocked;
        $adminHierarchyCeiling->save();
        track_activity($adminHierarchyCeiling, UserServices::getUser(), 'toggle_admin_hierarchy_ceiling_lock');
    }


    public static function toggleAllLocked($isLocked)
    {
        return DB::table('admin_hierarchy_ceilings')->update(['is_locked' => $isLocked]);
    }

    public static function updateStatusCodeIfExists($id)
    {
        $useStatusCode = Input::get('useStatusCode');
        $disseminationStatusCode = Input::get('disseminationStatusCode');
        if (isset($useStatusCode) || isset($disseminationStatusCode)) {
            $adminCeiling = AdminHierarchyCeiling::find($id);
            if (isset($useStatusCode)) {
                $adminCeiling->use_status = $useStatusCode;
            }
            if (isset($disseminationStatusCode)) {
                $adminCeiling->dissemination_status = $disseminationStatusCode;
            }
            $adminCeiling->save();
        }

    }

    public static function initiateLowerChain($ceilingId, $budgetType, $financialYearId, $lowerCeilingChainAdminHierarchyIds, $lowerCeilingChainSectionIds, $facilityIds)
    {

        foreach ($lowerCeilingChainAdminHierarchyIds as $adminHierarchyId) {
            foreach ($lowerCeilingChainSectionIds as $sectionId) {
                if ($facilityIds == null) {
                    self::initiateAdminHierarchyCeiling($ceilingId, $budgetType, $financialYearId, $adminHierarchyId, $sectionId, null);
                } else {
                    foreach ($facilityIds as $facilityId) {
                        self::initiateAdminHierarchyCeiling($ceilingId, $budgetType, $financialYearId, $adminHierarchyId, $sectionId, $facilityId);
                    }
                }
            }
        }

    }

    /**
     * Bottom - Up ceiling import from facility fund balances
    */
    public static function importCarryOverCeiling($financialYearId, $adminHierarchyId, $sectionId, $facilityId, $fundSourceId, $budgetClassId, $amount){
        $amount = (float)$amount;
        $budgetType = 'CARRYOVER';
        $section = Section::find($sectionId);
        if(!isset($section->sector_id)){
            return;
        }
        //1. Get the ceiling.
           $ceiling = Ceiling::getExpenditureCeilingByFundSource($fundSourceId, $section->sector_id);
        //2. Save my ceiling
            if($ceiling == null){
               return;
            }
            try {
                DB::beginTransaction();
                $facilityCeiling = AdminHierarchyCeiling::initiateAdminHierarchyCeiling($ceiling->id, $budgetType, $financialYearId, $adminHierarchyId, $sectionId, $facilityId);
                $facilityCeiling->amount = $amount;
                $facilityCeiling->is_locked = true;
                $facilityCeiling->save();
            //3. Get Next parents
                $costCentreCeiling = AdminHierarchyCeiling::initiateAdminHierarchyCeiling($ceiling->id, $budgetType, $financialYearId, $adminHierarchyId, $sectionId, null);
                $costCentreCeiling->amount = $costCentreCeiling->amount + $amount;
                $costCentreCeiling->save();

            //4.For each parent create/update ceiling
                $childSection = Section::find($sectionId);
                $parentSectionId = $childSection->parent_id;

                $forAdminLevel = DB::table('admin_hierarchies as a')->
                                    join('admin_hierarchy_levels as al','al.id','a.admin_hierarchy_level_id')->
                                    where('a.id', $adminHierarchyId)->
                                    select('al.*')->
                                    first();
                while($parentSectionId != null){

                    $parentSection = DB::table('sections as s')->
                                        join('section_levels as sl','sl.id', 's.section_level_id')->
                                        where('s.id', $parentSectionId)->
                                        select('s.*','sl.hierarchy_position')->
                                        first();

                    $existInCeiling = DB::table('ceiling_chains as cc')->
                                        where('for_admin_hierarchy_level_position', $forAdminLevel->hierarchy_position)->
                                        where('section_level_position', $parentSection->hierarchy_position)->
                                        count('cc.id');

                    if($existInCeiling > 0){
                        $parentCeiling = AdminHierarchyCeiling::initiateAdminHierarchyCeiling($ceiling->id, $budgetType, $financialYearId, $adminHierarchyId, $parentSectionId, null);
                        $parentCeiling->amount = $parentCeiling->amount + $amount;
                        $parentCeiling->save();
                    }
                    $parentSectionId = $parentSection->parent_id;
                }
                DB::commit();
              return $facilityCeiling->id;
          }catch(\Exception $e) {
            Log::error($e);
            DB::rollback();
            return null;
          }
    }

    public function section()
    {
        return $this->belongsTo('App\Models\Setup\Section');
    }

    public function financial_year()
    {
        return $this->belongsTo('App\Models\Setup\FinancialYear');
    }


    public function ceiling()
    {
        return $this->belongsTo('App\Models\Budgeting\Ceiling')->select('aggregate_fund_source_id', 'budget_class_id', 'id', 'name', 'gfs_code_id', 'is_aggregate', 'extends_to_facility');
    }

    public function admin_hierarchy()
    {
        return $this->belongsTo('App\Models\Setup\AdminHierarchy');
    }

    public function admin_hierarchy_ceiling_periods()
    {
        return $this->hasMany('App\Models\Budgeting\AdminHierarchyCeilingPeriod');
    }
    public function admin_hierarchy_ceiling_forwards()
    {
        return $this->hasMany('App\Models\Budgeting\AdminHierarchyCeilingForward');
    }
    public static function approveById($revenueIds)
    {

    }
    public static function approveByAdminHierarchy($financial_year_id, $adminHierarchyId, $budgetType)
    {
        RevenueAccountServices::approveAllRevenue($adminHierarchyId, $financial_year_id, $budgetType);
    }

    public static function disApproveByAdminHierarchy($financial_year_id, $adminHierarchyId, $budgetType)
    {
        return DB::table('admin_hierarchy_ceilings')->where('financial_year_id', $financial_year_id)->where('admin_hierarchy_id', $adminHierarchyId)->where('budget_type', $budgetType)->update(['is_approved' => false]);
    }

    public function facility()
    {
        return $this->belongsTo('App\Models\Setup\Facility','facility_id','id');
    }

}
