<?php

namespace App\Models\Budgeting;

use App\Models\CacheKeys;
use App\Models\Setup\Section;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class Ceiling extends Model
{
    public $timestamps = true;
//    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'ceilings';

    public static function getSectorIds($ceilingId){
       $ids = DB::table('ceiling_sectors')->where('ceiling_id', $ceilingId)->pluck('sector_id')->toArray();
        return $ids;
    }

    public static function getSimilarCeiling($ceilingId){
        $c = Ceiling::find($ceilingId);
        return Ceiling::where('budget_class_id',$c->budget_class_id)
                        ->where('gfs_code_id',$c->gfs_code_id)
                        ->where('id','<>',$ceilingId)
                        ->where('created_at','<',$c->created_at)
                        ->count();
    }

    public static function getForBudget()
    {
        return 
        $ceiling = Cache::rememberForever(CacheKeys::CEILINGS_FOR_BUDGET,function (){
                return Ceiling::where(function($query){
                        $query->whereHas('gfs_code',function ($query){
                            $query->join('gfscode_fundsources as gfsf','gfsf.gfs_code_id','gfs_codes.id');
                            $query->join('fund_sources as fund','fund.id', 'gfsf.fund_source_id');
                            $query->where('can_project',false);
                        })->orWhereNull('gfs_code_id');
                      })->where('is_active',true)->whereNull('deleted_at')->get();
        });

        foreach ($ceiling as &$value) {
            $value = $value * 2;
        }
    }

    public static function getExpenditureCeiling($fundSourceId, $budgetClassId){
            $ceiling = DB::table('ceilings as c')->
                            join('gfs_codes as gfs','gfs.id', 'c.gfs_code_id')->
                            join('fund_sources as fund','fund.id', 'gfs.fund_source_id')->
                            where('c.budget_class_id', $budgetClassId)->
                            where('fund.id', $fundSourceId)->
                            where('fund.can_project', false)->
                            where('c.is_active', true)->
                            select('c.*')->
                            first();
        if($ceiling != null){
            return $ceiling;
        } else {

            return $ceiling = DB::table('ceilings as c')->
                            join('fund_sources as fund','fund.id', 'c.aggregate_fund_source_id')->
                            where('c.budget_class_id', $budgetClassId)->
                            where('fund.id', $fundSourceId)->
                            where('fund.can_project', true)->
                            where('c.is_active', true)->
                            select('c.*')->
                            first();
        }
    }

    public static function getExpenditureCeilingByFundAndBudgetClassIds($fundSourceIds, $budgetClassIds){
            $ceiling1 = DB::table('ceilings as c')->
                            join('gfs_codes as gfs','gfs.id', 'c.gfs_code_id')->
                            join('fund_sources as fund','fund.id', 'gfs.fund_source_id')->
                            join('budget_classes as bc','bc.id', 'c.budget_class_id')->
                            whereIn('c.budget_class_id', $budgetClassIds)->
                            whereIn('fund.id', $fundSourceIds)->
                            where('fund.can_project', false)->
                            where('c.is_active', true)->
                            select('c.*','fund.name as fund_source','bc.name as budget_class')->
                            get();

            $ceiling2 = DB::table('ceilings as c')->
                            join('fund_sources as fund','fund.id', 'c.aggregate_fund_source_id')->
                            join('budget_classes as bc','bc.id', 'c.budget_class_id')->
                            whereIn('c.budget_class_id', $budgetClassIds)->
                            whereIn('fund.id', $fundSourceIds)->
                            where('fund.can_project', true)->
                            where('c.is_active', true)->
                            select('c.*','fund.name as fund_source','bc.name as budget_class')->
                            get();
            return $ceiling1->merge($ceiling2);
    }

    public static function getExpenditureCeilingByFundSource($fundSourceId, $sectorId){
        

        $ceiling = DB::table('ceilings as c')->
                    join('ceiling_sectors as cs','c.id','cs.ceiling_id')->
                    join('gfs_codes as gfs','gfs.id', 'c.gfs_code_id')->
                    join('fund_sources as fund','fund.id', 'gfs.fund_source_id')->
                    where('fund.id', $fundSourceId)->
                    where('cs.sector_id', $sectorId)->
                    where('fund.can_project', false)->
                    where('c.is_active', true)->
                    orderBy('c.id','desc')->
                    select('c.*')->
                    first();
        if($ceiling != null){
            return $ceiling;
        } else {

            return $ceiling = DB::table('ceilings as c')->
                    join('ceiling_sectors as cs','c.id','cs.ceiling_id')->
                    join('fund_sources as fund','fund.id', 'c.aggregate_fund_source_id')->
                    where('fund.id', $fundSourceId)->
                    where('cs.sector_id', $sectorId)->
                    where('fund.can_project', true)->
                    where('c.is_active', true)->
                    orderBy('c.id','desc')->
                    select('c.*')->
                    first();
        }
    }

    public function budget_class() {
        return $this->belongsTo('App\Models\Setup\BudgetClass');
    }

    public function gfs_code() {
        return $this->belongsTo('App\Models\Setup\GfsCode')->
                     select('code','id','description','name');
    }

    public function ceiling_sectors() {
        return $this->hasMany('App\Models\Budgeting\CeilingSector');
    }
    public function sectors() {
        return $this->belongsToMany('App\Models\Setup\Sector','ceiling_sectors');
    }
    public function aggregate_fund_source() {
        return $this->belongsTo('App\Models\Setup\FundSource','aggregate_fund_source_id');
    }

    public function admin_hierarchy_ceilings() {
        return $this->hasMany('App\Models\Budgeting\AdminHierarchyCeiling');
    }
}
