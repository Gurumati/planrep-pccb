<?php

namespace App\models\budgeting;

use Illuminate\Database\Eloquent\Model;

class PcActivity extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'activities';

    protected $with=['children'];

    public function children(){
        return $this->hasMany('App\Models\Budgeting\PcInput');
    }

    public function annualTarget(){
        return $this->belongsTo('App\Models\Budgeting\PcTarget');
    }

}
