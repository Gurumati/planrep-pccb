<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    public $timestamps=true;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";
    protected $guarded = ['id'];


    public function role(){
        return $this->belongsTo('App\Models\Auth\Role');
    }
    public function user(){
        return $this->belongsTo('App\Models\Auth\User');
    }
}
