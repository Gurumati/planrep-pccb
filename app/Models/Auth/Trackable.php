<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class Trackable extends Model {
    
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $guarded = ['id'];

}
