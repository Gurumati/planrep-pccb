<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class FailedLoginAttempt extends Model
{
    protected $guarded = [];
//    protected $table = 'failed_login_attempts';
}
