<?php

namespace App\Models\Auth;

use App\Models\Security\Audit;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Models\Setup\UserFacility;
use App\Models\Setup\Section;
use App\Http\Services\UserServices;
use App\Http\Controllers\Flatten;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;

class User extends EloquentUser implements AuditableContract, UserResolver{
    use Notifiable, Auditable;
    protected $table = 'users';
    protected $fillable = [
        'email',
        'password',
        'user_name',
        'first_name',
        'last_name',
        'title',
        'admin_hierarchy_id',
        'section_id',
        'decision_level_id',
        'permissions',
        'cheque_number',
        'is_superuser',
        'is_facility_user',
    ];
    protected $hidden = ['password'];

    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $loginNames = ['user_name'];

    /**
     * {@inheritdoc}
     */
    public static function resolveId()
    {
        $user = UserServices::getUser();
        return $user ? $user->getUserId() : null;
    }


    public  static function levelUsers($Ids,$sectionIds,$searchString){
        $users = User::whereIn('admin_hierarchy_id', $Ids)
            ->whereIn('section_id', $sectionIds)
            ->orWhere('user_name','ILIKE','%'.$searchString.'%')
            ->orWhere('email','ILIKE','%'.$searchString.'%')
            ->orWhere('cheque_number','ILIKE','%'.$searchString.'%')
            ->orWhere('first_name','ILIKE','%'.$searchString.'%')
            ->orWhere('last_name','ILIKE','%'.$searchString.'%')
            ->select('id', 'user_name', 'first_name', 'last_name','cheque_number', 'email')
            ->orderBy('admin_hierarchy_id')
            ->orderBy('user_name')->take(10)->get();
        return $users;
    }

    public static function addFacility($userId, $facilityId){
        $count =UserFacility::where('user_id',$userId)->where('facility_id',$facilityId)->count();
        if($count > 0){
            return;
        }
        UserFacility::insert(['user_id'=>$userId, 'facility_id'=>$facilityId, 'is_home'=>false]);
    }

    public static function removeFacility($userId, $facilityId){
        UserFacility::where('user_id',$userId)->where('facility_id',$facilityId)->forceDelete();
    }
    public static function removeAllUserFacility($userId){
        UserFacility::where('user_id',$userId)->forceDelete();
    }

    public static function getPaginatedEvents($userId){
        $perPage = Input::get('perPage',10);
        $actionQuery = Input::get('actionQuery');
        $actionQuery = isset($actionQuery)?"'%".strtolower($actionQuery)."%'":"'%'";
        $dateQuery = Input::get('dateQuery');
        $dateQuery = isset($dateQuery)?"'".substr($dateQuery,0,10)."'":null;
        return Trackable::where('user_id',$userId)->
                where(function ($query) use($actionQuery,$dateQuery){
                    $query->whereRaw('LOWER(action) like ' . $actionQuery);
                    if(isset($dateQuery))
                    $query->whereRaw('created_at::date ='.$dateQuery.'::date');
                 })->
                orderBy('id','desc')->paginate($perPage);
    }


    public  static function paginated($Ids,$sectionIds){

        $firstNameQuery = Input::get('firstNameQuery');
        $lastNameQuery = Input::get('lastNameQuery');
        $userNameQuery = Input::get('userNameQuery');
        $adminQuery = Input::get('adminQuery');
        $sectionQuery = Input::get('sectionQuery');
        $perPage = Input::get('perPage');

        $firstNameQuery = isset($firstNameQuery)?"'%".strtolower($firstNameQuery)."%'":"'%'";
        $lastNameQuery = isset($lastNameQuery)?"'%".strtolower($lastNameQuery)."%'":"'%'";
        $userNameQuery = isset($userNameQuery)?"'%".strtolower($userNameQuery)."%'":"'%'";
        $adminQuery = isset($adminQuery)?"'%".strtolower($adminQuery)."%'":"'%'";
        $sectionQuery = isset($sectionQuery)?"'%".strtolower($sectionQuery)."%'":"'%'";
        $perPage = isset($perPage)?$perPage:10;

        $users = User::with('admin_hierarchy', 'section', 'roles','activations')->
                    whereIn('admin_hierarchy_id', $Ids)->whereIn('section_id', $sectionIds)->
                    where(function ($query) use ($firstNameQuery,$lastNameQuery,$userNameQuery,$adminQuery,$sectionQuery) {
                        $query->whereRaw('LOWER(user_name) like ' . $userNameQuery);
                        $query->whereRaw('LOWER(first_name) like ' . $firstNameQuery);
                        $query->whereRaw('LOWER(last_name) like ' . $lastNameQuery);
                        $query->whereHas('admin_hierarchy',function($query2) use($adminQuery){
                            $query2->whereRaw('LOWER(name) like ' . $adminQuery);
                        });
                        $query->whereHas('section',function($query2) use($sectionQuery){
                            $query2->whereRaw('LOWER(name) like ' . $sectionQuery);
                        });
                    })->
                    select('id', 'user_name', 'first_name', 'last_name','title', 'admin_hierarchy_id', 'section_id', 'cheque_number', 'email', 'decision_level_id', 'is_superuser','is_facility_user')->
                    orderBy('admin_hierarchy_id')->orderBy('user_name')->paginate($perPage);
            foreach($users as &$u){
                if($u->is_facility_user){
                    $u->homeFacility = DB::table('facilities as f')
                    ->join('user_facilities as uf','uf.facility_id','f.id')
                    ->join('facility_types as ft','ft.id','f.facility_type_id')
                    ->where('uf.user_id',$u->id)->select('f.id','f.name','ft.name as type')->first();
                }
            }

        return $users;
    }

    public static function sameLevelUsers(){
        $user = UserServices::getUser();
        $sections = Section::where('id',$user->section_id)->get();

        $flatten = new Flatten();
        $ids = $flatten->flattenSectionWithChild($sections);

        $users = DB::table('users as u')->
                   join('sections as s','s.id','u.section_id')->
                   leftJoin('sectors as sec','sec.id','s.sector_id')->
                   where('u.admin_hierarchy_id',$user->admin_hierarchy_id)->
                   where('u.decision_level_id',$user->decision_level_id)->
                   whereIn('u.section_id',$ids)->select('u.id','u.first_name','u.last_name','u.title','sec.name as sector')->get();

        return $users;
    }
    public static function sameLevelUsersBySector($sectorId){
        $user = UserServices::getUser();
        $sections = Section::where('id',$user->section_id)->get();

        $flatten = new Flatten();
        $ids = $flatten->flattenSectionWithChild($sections);

        $users = DB::table('users as u')->
                   join('sections as s','s.id','u.section_id')->
                   join('sectors as sec','sec.id','s.sector_id')->
                   where('u.admin_hierarchy_id',$user->admin_hierarchy_id)->
                   where('u.decision_level_id',$user->decision_level_id)->
                   where('sec.id',$sectorId)->
                   whereIn('u.section_id',$ids)->select('u.id','u.first_name','u.last_name','u.title','sec.name as sector')->get();

        return $users;
    }

    public static function byEmail($email) {
        return static::whereEmail($email)->first();
    }

    public function events() {
        return $this->hasMany('App\Models\Auth\Trackable', 'user_id');
    }

    public function admin_hierarchy() {
        return $this->belongsTo('App\DTOs\SetupDTOs\AdminHierarchyDTO')->select('id','name','admin_hierarchy_level_id');
    }

    public function section() {
        return $this->belongsTo('App\DTOs\SetupDTOs\SectionDTO','section_id','id')->select('id','name','section_level_id');
    }

    public function role_users() {
        return $this->hasMany('App\Models\Authentication\RoleUser');
    }
    public function activations() {
        return $this->hasMany('App\Models\Auth\Activation')->where('completed',true)->select('id','user_id','completed');
    }

    public function audits(): HasMany
    {
        return $this->hasMany(Audit::class);
    }
}
