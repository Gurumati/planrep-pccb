<?php

namespace App\Models\Ads;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Advertisment extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'advertisments';
    protected $fillable = ['description','status','start_date','ad_url','end_date','created_by','updated_by','unit_price'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'start_date' => 'required|string',
                'end_date' => 'required|string',
                'ad_url' => 'required|string',
            ],
            $merge);
    }
}
