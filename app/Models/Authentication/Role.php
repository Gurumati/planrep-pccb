<?php

namespace App\Models\Authentication;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function role_users() {
        return $this->hasMany('App\RoleUser');
    }

    public function admin_hierarchy_level(){
        return $this->belongsTo('App\AdminHierarchyLevel');
    }

}
