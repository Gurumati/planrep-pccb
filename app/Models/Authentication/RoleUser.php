<?php

namespace App\Models\Authentication;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoleUser extends Model
{
    public $timestamps=true;
    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";

    public function role(){
        return $this->belongsTo('App\Role');
    }
    public function user(){
        return $this->belongsTo('App\Models\Auth\User');
    }
}
