<?php

namespace App\Models\Misc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DemoFormField extends Model
{
    public $timestamps=false;
    protected $with = [
        'form_fields'
    ];

    public function form_fields() {
        return $this->hasMany(DemoFormField::class);
    }
}
