<?php

namespace App\Models\Misc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\DemoFormField;

class DemoForm extends Model
{
    public $timestamps=false;
    protected $with = [
        'form_fields'
    ];

    public function form_fields() {
        return $this->hasMany(DemoFormField::class)->whereNull('demo_form_field_id');
    }
}
