<?php

namespace App\Models\Misc;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DemoSectionInformation extends Model
{
    public $timestamps=false;
    //
    protected $with = [
        'demoSectionInformation'
    ];

    public function demoSectionInformation() {
        return $this->hasMany(DemoSectionInformation::class);
    }

//    public function childs() { return $this->hasMany('App\DemoSectionInformation', 'parent_id', 'id'); }
}
