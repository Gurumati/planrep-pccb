<?php

namespace App\models\assessment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class CasAssessmentResult extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'cas_assessment_results';

    public function cas_assessment_result_details(){
        return $this->hasMany('App\Models\Assessment\CasAssessmentResultDetail');
    }

    public function period(){
        return $this->belongsTo('App\Models\Setup\Period');
    }

    public function round(){
        return $this->belongsTo('App\Models\Assessment\CasAssessmentRound','cas_assessment_round_id');
    }

    public function mtef(){
        return $this->belongsTo('App\Models\Planning\Mtef');
    }

    public function admin_hierarchy_level(){
        return $this->belongsTo('App\Models\Setup\AdminHierarchyLevel');
    }

    public function user(){
        return $this->belongsTo('App\Models\Auth\User','user_id');
    }
}
