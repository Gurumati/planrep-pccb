<?php

namespace App\Models\Assessment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CasAssessmentCategoryVersion extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'cas_assessment_category_versions';

    public function financial_year(){
        return $this->belongsTo('App\Models\Setup\FinancialYear','financial_year_id','id');
    }

    public function reference_document(){
        return $this->belongsTo('App\Models\Setup\ReferenceDocument','reference_document_id','id');
    }

    public function cas_assessment_category(){
        return $this->belongsTo('App\Models\Assessment\CasAssessmentCategory','cas_assessment_category_id','id');
    }

    public function cas_assessment_state(){
        return $this->belongsTo('App\Models\Assessment\CasAssessmentState','cas_assessment_state_id','id');
    }

}
