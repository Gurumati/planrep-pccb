<?php

namespace App\Models\Assessment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CasAssessmentSubCriteriaReportSet extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'cas_assessment_sub_criteria_report_sets';

    public function casAssessmentSubCriteria(){
        return $this->belongsTo('App\Models\Assessment\CasAssessmentSubCriteria');
    }

    public function casPlanContent(){
        return $this->belongsTo('App\Models\Setup\CasPlanContent')->select('id','description');
    }
}
