<?php

namespace App\Models\Assessment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CasAssessmentRound extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'cas_assessment_rounds';

    public function assessment_results() {
        return $this->hasMany('App\Models\Assessment\CasAssessmentResult','cas_assessment_round_id');
    }
}
