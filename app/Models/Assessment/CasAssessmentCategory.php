<?php

namespace App\Models\Assessment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CasAssessmentCategory extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'cas_assessment_categories';

    public function cas_plan(){
        return $this->belongsTo('App\Models\Setup\CasPlan','cas_plan_id','id');
    }

    public function period_group(){
        return $this->belongsTo('App\Models\Setup\PeriodGroup','period_group_id','id');
    }
}
