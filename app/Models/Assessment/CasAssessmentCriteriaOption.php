<?php

namespace App\Models\Assessment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CasAssessmentCriteriaOption extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'cas_assessment_criteria_options';

    public function cas_assessment_category_version(){
        return $this->belongsTo('App\Models\Assessment\CasAssessmentCategoryVersion','cas_assessment_category_version_id','id');
    }

    public function cas_assessment_result_details(){
        return $this->hasMany('App\Models\Assessment\CasAssessmentResultDetails');
    }
}
