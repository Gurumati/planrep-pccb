<?php

namespace App\Models\Assessment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CasAssessmentCategoryVersionState extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'cas_assessment_category_version_states';

    public function cas_assessment_category_version(){
        return $this->belongsTo('App\Models\Assessment\CasAssessmentCategoryVersion','cas_assessment_category_version_id','id');
    }

    public function cas_assessment_state(){
        return $this->belongsTo('App\Models\Assessment\CasAssessmentState','cas_assessment_state_id','id');
    }
}
