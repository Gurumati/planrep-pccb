<?php

namespace App\Models\Assessment;

use Illuminate\Database\Eloquent\Model;

class CasAssessmentResultReply extends Model
{
    public function cas_assessment_result(){
        return $this->belongsTo('App\Models\Assessment\CasAssessmentResult','cas_assessment_result_id');
    }

    public function user(){
        return $this->belongsTo('App\Models\Auth\User','updated_by');
    }
}
