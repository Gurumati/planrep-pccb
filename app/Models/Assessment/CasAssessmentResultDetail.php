<?php

namespace App\models\assessment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CasAssessmentResultDetail extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'cas_assessment_result_details';


    public function cas_assessment_result(){
        return $this->belongsTo('App\Models\Assessment\CasAssessmentResult');
    }
    public function cas_assessment_sub_criteria(){
        return $this->belongsTo('App\Models\Assessment\CasAssessmentSubCriteriaOption','cas_assessment_sub_criteria_option_id');
    }

    public function cas_assessment_result_detail_replies(){
        return $this->hasMany('App\Models\Assessment\CasAssessmentResultDetailReply','cas_assessment_result_id');
    }
}
