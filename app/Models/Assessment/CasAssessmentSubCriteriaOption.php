<?php

namespace App\Models\Assessment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CasAssessmentSubCriteriaOption extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'cas_assessment_sub_criteria_options';

    public function cas_assessment_criteria_option(){
        return $this->belongsTo('App\Models\Assessment\CasAssessmentCriteriaOption','cas_assessment_criteria_option_id','id');
    }
}
