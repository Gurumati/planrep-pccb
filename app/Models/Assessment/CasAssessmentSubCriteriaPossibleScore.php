<?php

namespace App\Models\Assessment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CasAssessmentSubCriteriaPossibleScore extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'cas_assessment_sub_criteria_possible_scores';

    public function casAssessmentSubCriteria(){
        return $this->belongsTo('App\Models\Assessment\CasAssessmentSubCriteria');
    }
}
