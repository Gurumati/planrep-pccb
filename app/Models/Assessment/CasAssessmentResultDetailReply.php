<?php

namespace App\Models\Assessment;

use Illuminate\Database\Eloquent\Model;

class CasAssessmentResultDetailReply extends Model
{
    public function cas_assessment_result_detail(){
        return $this->belongsTo('App\Models\Assessment\CasAssessmentResultDetail','cas_assessment_result_detail_id');
    }

    public function user(){
        return $this->belongsTo('App\Models\Auth\User','updated_by');
    }
}
