<?php

namespace App\Models\Dashboard;

use App\Http\Controllers\Flatten;
use App\Models\Setup\BudgetClassVersion;
use App\Models\Setup\FundSourceVersion;
use App\Models\Setup\Version;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class Dashboard extends Model
{
  public $timestamps = true;
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  protected $dateFormat = "Y-m-d H:i:s";


  public static function getBudgetAndCeiling($budgetType, $financialYearId, $adminHierarchyId, $sectionId, $sectionIdsString)
  {
    $peBudgetClassIdString = Flatten::getPeBudgetClassIdString();
    $versionId = Version::getVersionByFinancialYear($financialYearId, 'BC');
    $budgetClassIds =BudgetClassVersion::where('version_id',$versionId)->pluck('budget_class_id')->toArray();
    $fVersionId = Version::getVersionByFinancialYear($financialYearId, 'FS');
    $fundSourceIds =FundSourceVersion::where('version_id',$fVersionId)->pluck('fund_source_id')->toArray();
    $budgetClassIdString = implode(",",$budgetClassIds);
    $fundSourceIdsString =implode(",",$fundSourceIds);
    $budgetType = ($budgetType == 'APPROVED')? "('CURRENT','APPROVED')":"('".$budgetType."')";

    $query = "WITH C AS(
                      SELECT
                        bc.id as bid,
                        CASE WHEN c.is_aggregate THEN c.aggregate_fund_source_id
                          ELSE
                            f.id
                            END as fid,
                        MAX(c.name) as name,
                        SUM(adc.amount) as ceiling FROM admin_hierarchy_ceilings AS adc
                        JOIN ceilings AS c ON c.id = adc.ceiling_id
                        LEFT JOIN gfs_codes as gfs ON gfs.id=c.gfs_code_id
                        LEFT JOIN gfscode_fundsources as gfsf ON  gfsf.gfs_code_id = gfs.id
                        LEFT JOIN fund_sources as f on f.id = gfsf.fund_source_id
                        LEFT JOIN fund_sources as fp on fp.id = c.aggregate_fund_source_id
                        LEFT JOIN budget_classes as bc on bc.id = c.budget_class_id
                      WHERE adc.budget_type in " . $budgetType . " 
                            and (f.can_project = false or fp.can_project = true)
                            and adc.admin_hierarchy_id = " . $adminHierarchyId . " 
                            and adc.section_id =" . $sectionId . " 
                            and adc.financial_year_id = " . $financialYearId . "
                            and adc.is_facility = false  
                            and  c.is_active=true 
                            and c.budget_class_id NOT IN (" . $peBudgetClassIdString . ")
                            and c.budget_class_id  IN (" . $budgetClassIdString . ")
                            and ( f.id IN (". $fundSourceIdsString .") OR c.aggregate_fund_source_id IN (". $fundSourceIdsString ."))
                     GROUP BY  bid,fid
                )
            SELECT C.name as label, C.ceiling as ceiling, coalesce(B.budget,0) as budget,
                 CASE WHEN ceiling = 0 THEN 0 ELSE (coalesce(budget,0)/ceiling)*100 END as completion FROM C
             LEFT JOIN
            (
              SELECT
                 bc.id  AS bid,
                aff.fund_source_id  AS fid,
                SUM(i.unit_price * i.quantity * i.frequency) AS budget
              FROM activity_facility_fund_source_inputs AS i
                INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
                INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
                INNER JOIN facilities AS fac ON fac.id = af.facility_id
                INNER JOIN activities AS a ON a.id = af.activity_id
                INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
                INNER JOIN mtefs AS m ON m.id = ms.mtef_id
                INNER JOIN sections AS s ON s.id = ms.section_id
                INNER JOIN budget_classes AS bc ON bc.id = a.budget_class_id
              WHERE i.budget_type in " . $budgetType . " and a.budget_class_id NOT IN (" . $peBudgetClassIdString . ") and i.deleted_at is null and fac.is_active = true and
              m.admin_hierarchy_id = " . $adminHierarchyId . " AND m.financial_year_id = " . $financialYearId . " and  s.id in (" . $sectionIdsString . ")
              GROUP BY bid,fid
            )
           AS B on C.bid=B.bid and C.fid=B.fid WHERE C.ceiling > 0 OR (C.ceiling =0 AND B.budget >0)";

    return DB::select($query);
  }

  public static function getTotalBudgetByClass($budgetType, $financialYearId, $adminHierarchyId, $sectionId, $sectionIdsString)
  {
    $budgetType = ($budgetType == 'APPROVED')? "('CURRENT','APPROVED')":"('".$budgetType."')";

    $query = "SELECT
                'PE' AS name,
                CASE WHEN SUM(i.unit_price * i.quantity * i.frequency) is null THEN 0 ELSE SUM(i.unit_price * i.quantity * i.frequency) END AS budget
              FROM activity_facility_fund_source_inputs AS i
              INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
              INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
              INNER JOIN activities AS a ON a.id = af.activity_id
              INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
              INNER JOIN mtefs AS m ON m.id = ms.mtef_id
              INNER JOIN sections AS s ON s.id = ms.section_id
              INNER JOIN budget_classes AS bc ON bc.id = a.budget_class_id
              WHERE i.budget_type in " . $budgetType . "
                and i.deleted_at is null
                and m.admin_hierarchy_id = " . $adminHierarchyId . "
                and m.financial_year_id = " . $financialYearId . "
                and  s.id in (" . $sectionIdsString . ")
                and bc.parent_id = 1
                and bc.name LIKE '%Personal%'
              
              UNION
              
              SELECT
                'OC' AS name,
                CASE WHEN SUM(i.unit_price * i.quantity * i.frequency) is null THEN 0 ELSE SUM(i.unit_price * i.quantity * i.frequency) END AS budget
              FROM activity_facility_fund_source_inputs AS i
              INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
              INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
              INNER JOIN activities AS a ON a.id = af.activity_id
              INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
              INNER JOIN mtefs AS m ON m.id = ms.mtef_id
              INNER JOIN sections AS s ON s.id = ms.section_id
              INNER JOIN budget_classes AS bc ON bc.id = a.budget_class_id
              WHERE i.budget_type in " . $budgetType . "
                and i.deleted_at is null
                and m.admin_hierarchy_id = " . $adminHierarchyId . "
                and m.financial_year_id = " . $financialYearId . "
                and  s.id in (" . $sectionIdsString . ")
                and bc.parent_id = 1
                and bc.name LIKE '%Other Charges%'
                
              UNION
              
              SELECT
                'DEV' AS name,
                CASE WHEN SUM(i.unit_price * i.quantity * i.frequency) is null THEN 0 ELSE SUM(i.unit_price * i.quantity * i.frequency) END AS budget
              FROM activity_facility_fund_source_inputs AS i
              INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
              INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
              INNER JOIN activities AS a ON a.id = af.activity_id
              INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
              INNER JOIN mtefs AS m ON m.id = ms.mtef_id
              INNER JOIN sections AS s ON s.id = ms.section_id
              INNER JOIN budget_classes AS bc ON bc.id = a.budget_class_id
              WHERE i.budget_type in " . $budgetType . "
                and i.deleted_at is null
                and m.admin_hierarchy_id = " . $adminHierarchyId . "
                and m.financial_year_id = " . $financialYearId . "
                and  s.id in (" . $sectionIdsString . ")
                and bc.parent_id = 2";

    return DB::select($query);
  }

  public static function getPEBudgetAndPECeiling($budgetType, $financialYearId, $adminHierarchyId, $sectionId, $sectionIdsString)
  {

    $peBudgetClassIdString = Flatten::getPeBudgetClassIdString();
    $budgetType = ($budgetType == 'APPROVED')? "('CURRENT','APPROVED')":"('".$budgetType."')";
    $versionId = Version::getVersionByFinancialYear($financialYearId, 'BC');
    $budgetClassIds =BudgetClassVersion::where('version_id',$versionId)->pluck('budget_class_id')->toArray();
    $fVersionId = Version::getVersionByFinancialYear($financialYearId, 'FS');
    $fundSourceIds =FundSourceVersion::where('version_id',$fVersionId)->pluck('fund_source_id')->toArray();
    $budgetClassIdString = implode(",",$budgetClassIds);
    $fundSourceIdsString =implode(",",$fundSourceIds);



    $query = "WITH C AS(
                      SELECT
                        bc.id as bid,
                        CASE WHEN c.is_aggregate THEN c.aggregate_fund_source_id
                          ELSE
                            f.id
                            END as fid,
                        MAX(c.name) as name,
                        SUM(adc.amount) as ceiling FROM admin_hierarchy_ceilings AS adc
                        JOIN ceilings AS c ON c.id = adc.ceiling_id
                        LEFT JOIN gfs_codes as gfs ON gfs.id=c.gfs_code_id
                        LEFT JOIN gfscode_fundsources as gfsf ON gfsf.gfs_code_id = gfs.id
                        LEFT JOIN fund_sources as f on f.id = gfsf.fund_source_id
                        LEFT JOIN fund_sources as fp on fp.id = c.aggregate_fund_source_id
                        LEFT JOIN budget_classes as bc on bc.id = c.budget_class_id
                      WHERE adc.budget_type in " . $budgetType . " and (f.can_project = false or fp.can_project = true)
                            and adc.admin_hierarchy_id = " . $adminHierarchyId . " and adc.section_id =" . $sectionId . " and adc.financial_year_id = " . $financialYearId . "
                            and adc.is_facility = false  and  c.is_active=true and c.budget_class_id IN (" . $peBudgetClassIdString . ")
                            and c.budget_class_id  IN (" . $budgetClassIdString . ")
                            and ( f.id IN (". $fundSourceIdsString .") OR c.aggregate_fund_source_id IN (". $fundSourceIdsString ."))
                     GROUP BY  bid,fid
                )
            SELECT C.name as label, C.ceiling as ceiling, coalesce(B.budget,0) as budget,
                 CASE WHEN ceiling = 0 THEN 0 ELSE (coalesce(budget,0)/ceiling)*100 END as completion FROM C
             LEFT JOIN
            (
              SELECT
                 bc.id  AS bid,
                aff.fund_source_id  AS fid,
                SUM(i.unit_price * i.quantity * i.frequency) AS budget
              FROM activity_facility_fund_source_inputs AS i
                INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
                INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
                INNER JOIN facilities AS fac ON fac.id = af.facility_id
                INNER JOIN activities AS a ON a.id = af.activity_id
                INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
                INNER JOIN mtefs AS m ON m.id = ms.mtef_id
                INNER JOIN sections AS s ON s.id = ms.section_id
                INNER JOIN budget_classes AS bc ON bc.id = a.budget_class_id
                INNER JOIN fund_sources fs on aff.fund_source_id = fs.id
              WHERE a.budget_type in " . $budgetType . " and a.budget_class_id IN (" . $peBudgetClassIdString . ") and i.deleted_at is null and fac.is_active = true and
              m.admin_hierarchy_id = " . $adminHierarchyId . " AND m.financial_year_id = " . $financialYearId . " and  s.id in (" . $sectionIdsString . ")
              and i.id IN (select DISTINCT activity_facility_fund_source_input_id  FROM activity_facility_fund_source_input_breakdowns  affsib
                        join budget_submission_definitions bsd on affsib.budget_submission_definition_id = bsd.id
                         where ((fs.code = '10A' AND bsd.id = bsd.id ) OR (fs.code = '0GT' AND bsd.field_name IN ('Basic Salary','Annual Increment','Promotion')) ))
              GROUP BY bid,fid
            )
           AS B on C.bid=B.bid and C.fid=B.fid WHERE C.ceiling > 0 OR (C.ceiling =0 AND B.budget >0)";

    return DB::select($query);
  }

  public static function getProcurableAgainstTotalBudget($budgetType, $financialYearId, $adminHierarchyId, $sectionId, $sectionIdsString)
  {
    $budgetType = ($budgetType == 'APPROVED')? "('CURRENT','APPROVED')":"('".$budgetType."')";
    $versionId = Version::getVersionByFinancialYear($financialYearId, 'BC');
    $budgetClassIds =BudgetClassVersion::where('version_id',$versionId)->pluck('budget_class_id')->toArray();
    $fVersionId = Version::getVersionByFinancialYear($financialYearId, 'FS');
    $fundSourceIds =FundSourceVersion::where('version_id',$fVersionId)->pluck('fund_source_id')->toArray();
    $budgetClassIdString = implode(",",$budgetClassIds);
    $fundSourceIdsString =implode(",",$fundSourceIds);

    $query = "WITH B AS (SELECT
                    sec.name AS name1,
                    SUM(i.unit_price * i.quantity * i.frequency) AS procurable_budget
                  FROM activity_facility_fund_source_inputs AS i
                    INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
                    INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
                    INNER JOIN facilities AS fa ON fa.id = af.facility_id
                    INNER JOIN activities AS a ON a.id = af.activity_id
                    INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
                    INNER JOIN mtefs AS m ON m.id = ms.mtef_id
                    INNER JOIN sections AS s ON s.id = ms.section_id
                    INNER JOIN sectors AS sec ON sec.id = s.sector_id
                    INNER JOIN gfs_codes AS gfs ON gfs.id = i.gfs_code_id
                  WHERE a.budget_type in " . $budgetType . " 
                        and i.deleted_at is null
                        and m.admin_hierarchy_id  IN (" . $adminHierarchyId . ")
                        and m.financial_year_id = " . $financialYearId . "
                        and  s.id in (" . $sectionIdsString . ")
                        and fa.is_active =true
                        and gfs.is_procurement = 'TRUE'
                  GROUP BY name1)
                  SELECT B.name1 as name, B.procurable_budget as procurable_budget, coalesce(C.total_budget,0) as total_budget,
                  CASE WHEN total_budget = 0 THEN 0 ELSE (coalesce(procurable_budget,0)/total_budget)*100 END as ratio FROM B

                  LEFT JOIN

                  (SELECT
                    sec.name AS name2,
                    SUM(i.unit_price * i.quantity * i.frequency) AS total_budget
                  FROM activity_facility_fund_source_inputs AS i
                    INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
                    INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
                    INNER JOIN facilities AS fa ON fa.id = af.facility_id
                    INNER JOIN activities AS a ON a.id = af.activity_id
                    INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
                    INNER JOIN mtefs AS m ON m.id = ms.mtef_id
                    INNER JOIN sections AS s ON s.id = ms.section_id
                    INNER JOIN sectors AS sec ON sec.id = s.sector_id
                  WHERE a.budget_type in " . $budgetType . "
                        and i.deleted_at is null
                        and m.admin_hierarchy_id  IN (" . $adminHierarchyId . ")
                        and m.financial_year_id = " . $financialYearId . "
                        and s.id in (" . $sectionIdsString . ")
                        and fa.is_active =true 
                        and i.deleted_at is null
                  GROUP BY name2) C
                  ON B.name1 = C.name2";

    return DB::select($query);
  }

  public static function getProblemCeiling($budgetType, $financialYearId, $adminIdsString, $total)
  {
    return [];
  }

  public static function getProblemAllocation($budgetType, $adminIdsString, $financialYearId, $parentSectionId, $childSectionIdString)
  {

    return [];// DB::select($query);
  }

  public static function getProblemByFacilityAllocation($budgetType, $adminIdsString, $financialYearId, $parentSectionId, $facilityIdString)
  {
    return []; // DB::select($query);
  }
  public static function getIncompleteCeilingAndBudget($budgetType, $financialYearId, $adminIds,  $sectionIds, $isTotal){
    $query = DB::table('view_incomplete_ceiling_and_budget')->
         where('budget_type', $budgetType)->
         where('financial_year_id',$financialYearId)->
         whereIn('adminid',explode(',',$adminIds))->
         whereIn('secid', explode(',',$sectionIds));
         if($isTotal == 'true'){
           return ['total' =>$query->count()];
         }
         return $query->paginate(Input::get('perPage',10));
  }

  public static function getAggregateBudget($budgetType, $financialYearId, $adminIdsString, $sectionIdsString, $peBudgetClassIdString, $peFundSourceIdString)
  {

    $query = "SELECT
              SUM(i.unit_price * i.quantity * i.frequency) AS budget
            FROM activity_facility_fund_source_inputs AS i
              INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
              INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
              INNER JOIN activities AS a ON a.id = af.activity_id
              INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
              INNER JOIN mtefs AS m ON m.id = ms.mtef_id
              INNER JOIN facilities AS f ON f.id = af.facility_id
            WHERE a.budget_type='" . $budgetType . "' AND f.is_active = true AND a.budget_class_id NOT IN (" . $peBudgetClassIdString . ") AND aff.fund_source_id NOT IN (" . $peFundSourceIdString . ")
            AND m.admin_hierarchy_id IN (" . $adminIdsString . ")
            AND m.financial_year_id = " . $financialYearId . " and i.deleted_at is null AND
            ms.section_id in (" . $sectionIdsString . ")";
    return DB::select($query)[0]->budget;

  }

  public static function getAggregateCeiling($budgetType, $financialYearId, $adminIdsString, $sectionId, $peBudgetClassIdString)
  {

    $query = "SELECT
                  COALESCE (SUM(adc.amount),0) as ceiling FROM admin_hierarchy_ceilings AS adc
                  INNER JOIN ceilings AS c ON c.id = adc.ceiling_id
                  INNER JOIN admin_hierarchies AS a ON a.id = adc.admin_hierarchy_id
                  INNER JOIN gfs_codes as gfs ON gfs.id=c.gfs_code_id
                  join gfscode_fundsources gf on gfs.id = gf.gfs_code_id
                  INNER JOIN fund_sources as f on f.id = gf.fund_source_id
                  WHERE adc.budget_type='" . $budgetType . "' AND f.can_project=false and
                  adc.admin_hierarchy_id IN (" . $adminIdsString . ") and adc.section_id IN (" . Flatten::userBudgetingSectionIdString() . ")  and adc.financial_year_id = " . $financialYearId . "
                  and adc.is_facility = false and c.is_active=true and c.budget_class_id NOT IN (" . $peBudgetClassIdString . ")";

    $ownquery = "SELECT
                  COALESCE (SUM(adc.amount),0) as ceiling FROM admin_hierarchy_ceilings AS adc
                  INNER JOIN ceilings AS c ON c.id = adc.ceiling_id
                  INNER JOIN admin_hierarchies AS a ON a.id = adc.admin_hierarchy_id
                  WHERE adc.budget_type='" . $budgetType . "' AND c.is_aggregate = true and
                  adc.admin_hierarchy_id IN (" . $adminIdsString . ") and adc.section_id IN (" . Flatten::userBudgetingSectionIdString() . ")  and adc.financial_year_id = " . $financialYearId . "
                  and adc.is_facility = false and c.is_active=true and c.budget_class_id NOT IN (" . $peBudgetClassIdString . ")";


    return (DB::select($query)[0]->ceiling + DB::select($ownquery)[0]->ceiling);
  }

  public static function getTotalCeilingAgainstTotalBudget($adminIdsString, $financialYearId, $sectionId, $sectionIdsString)
  {
    $query = "WITH C AS(
            SELECT
              adc.admin_hierarchy_id as admin_id,
              MAX(a.name) as name,
              SUM(adc.amount) as ceiling FROM admin_hierarchy_ceilings AS adc
              JOIN ceilings AS c ON c.id = adc.ceiling_id
              JOIN admin_hierarchies AS a ON a.id = adc.admin_hierarchy_id
              LEFT JOIN gfs_codes as gfs ON gfs.id=c.gfs_code_id
              JOIN gfscode_fundsources as gfsf ON gfs.id = gfsf.gfs_code_id
              JOIN fund_sources as f on f.id = gfsf.fund_source_id
              LEFT JOIN fund_source_versions fv on f.id = fv.fund_source_id
              LEFT JOIN versions v on fv.version_id = v.id
              left join  financial_year_versions fyv on v.id = fyv.version_id
            WHERE (f.can_project = false or c.is_aggregate = true)
                  and adc.admin_hierarchy_id IN (" . $adminIdsString . ") and adc.section_id=" . $sectionId . "  and adc.financial_year_id = " . $financialYearId . "
                  and adc.is_facility = false
                  and fyv.financial_year_id = ".$financialYearId."
            GROUP BY admin_id
        )
        SELECT C.name as label,
               C.ceiling as ceiling,
               coalesce(B.budget,0) as budget,
               (budget/ceiling) as percentage
               FROM C
          LEFT JOIN
          (
            SELECT
              m.admin_hierarchy_id as admin_id,
              SUM(i.unit_price * i.quantity * i.frequency) AS budget
            FROM activity_facility_fund_source_inputs AS i
              INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
              INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
              INNER JOIN facilities AS fac ON fac.id = af.facility_id
              INNER JOIN activities AS a ON a.id = af.activity_id
              INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
              INNER JOIN mtefs AS m ON m.id = ms.mtef_id
            WHERE a.code <> '00000000' and fac.is_active= true and i.deleted_at is null and m.admin_hierarchy_id IN (" . $adminIdsString . ") AND m.financial_year_id = " . $financialYearId . " and  ms.section_id in (" . $sectionIdsString . ")
            GROUP BY admin_id
          )
          AS B on C.admin_id=B.admin_id
        Where C.ceiling > 0";

    return DB::select($query);
  }

  public static function getIncomplete($budgetType, $financialYearId, $adminHierarchyId) {
    $query = " WITH c AS (
      SELECT adc.financial_year_id,
         adc.budget_type,
         adc.admin_hierarchy_id AS adminid,
         adc.section_id AS secid,
         adc.facility_id AS facid,
         bc.id AS bid,
         f.id AS fid,
         max(c.name::text) AS name,
         COALESCE(sum(adc.amount), 0::numeric) AS ceiling
        FROM admin_hierarchy_ceilings adc
          JOIN ceilings c ON c.id = adc.ceiling_id
          JOIN gfs_codes gfs ON gfs.id = c.gfs_code_id
          JOIN gfscode_fundsources as gfsf ON gfs.id = gfsf.gfs_code_id
          JOIN fund_sources as f on f.id = gfsf.fund_source_id
          LEFT JOIN fund_source_versions fv on f.id = fv.fund_source_id
          LEFT JOIN versions v on fv.version_id = v.id
          left join  financial_year_versions fyv on v.id = fyv.version_id
          JOIN budget_classes bc ON bc.id = c.budget_class_id
          JOIN facilities fac ON fac.id = adc.facility_id
          JOIN sections s ON s.id = adc.section_id
          JOIN section_levels sl ON sl.id = s.section_level_id
       WHERE f.can_project = false and fyv.financial_year_id = ".$financialYearId." AND adc.financial_year_id = ".$financialYearId." and adc.admin_hierarchy_id =".$adminHierarchyId." AND c.is_active = true AND adc.budget_type::text = '".$budgetType."' AND sl.hierarchy_position = 4
       GROUP BY adc.financial_year_id, adc.budget_type, adc.admin_hierarchy_id, adc.section_id, adc.facility_id, bc.id, f.id
     UNION
      SELECT adc.financial_year_id,
         adc.budget_type,
         adc.admin_hierarchy_id AS adminid,
         adc.section_id AS secid,
         adc.facility_id AS facid,
         bc.id AS bid,
         f.id AS fid,
         max(c.name::text) AS name,
         COALESCE(sum(adc.amount), 0::numeric) AS ceiling
        FROM admin_hierarchy_ceilings adc
          JOIN ceilings c ON c.id = adc.ceiling_id
          JOIN fund_sources f ON f.id = c.aggregate_fund_source_id
          JOIN budget_classes bc ON bc.id = c.budget_class_id
          JOIN facilities fac ON fac.id = adc.facility_id
          JOIN sections s ON s.id = adc.section_id
          JOIN section_levels sl ON sl.id = s.section_level_id
       WHERE f.can_project = true AND adc.financial_year_id = ".$financialYearId."  and adc.admin_hierarchy_id =".$adminHierarchyId." AND c.is_active = true AND adc.budget_type::text = '".$budgetType."' AND sl.hierarchy_position = 4
       GROUP BY adc.financial_year_id, adc.budget_type, adc.admin_hierarchy_id, adc.section_id, adc.facility_id, bc.id, f.id
     )
SELECT b.financial_year_id,
 b.budget_type,
 b.adminid,
 b.adminarea,
 b.secid,
 b.section,
 b.facility,
 b.facid,
 b.fund_source,
 b.budget_class,
 COALESCE(c.ceiling, 0::numeric) AS ceiling,
 COALESCE(b.budget, 0::numeric) AS budget,
     CASE
         WHEN c.ceiling = 0::numeric THEN 0
         ELSE (COALESCE(b.budget, 0::numeric) / c.ceiling * 100::numeric)::integer
     END AS completion
FROM c
  RIGHT JOIN ( SELECT m.financial_year_id,
         a.budget_type,
         adm.name AS adminarea,
         adm.id AS adminid,
         s.name AS section,
         s.id AS secid,
         fac.id AS facid,
         fac.name AS facility,
         bc.id AS bid,
         bc.name AS budget_class,
         fu.id AS fid,
         fu.name AS fund_source,
         sum(i.unit_price * i.quantity * i.frequency) AS budget
        FROM activity_facility_fund_source_inputs i
          JOIN activity_facility_fund_sources aff ON aff.id = i.activity_facility_fund_source_id
          JOIN activity_facilities af ON af.id = aff.activity_facility_id
          JOIN facilities fac ON fac.id = af.facility_id
          JOIN activities a ON a.id = af.activity_id
          JOIN mtef_sections ms ON ms.id = a.mtef_section_id
          JOIN mtefs m ON m.id = ms.mtef_id
          JOIN admin_hierarchies adm ON adm.id = m.admin_hierarchy_id
          JOIN sections s ON s.id = ms.section_id
          JOIN budget_classes bc ON bc.id = a.budget_class_id
          JOIN fund_sources fu ON fu.id = aff.fund_source_id
       WHERE CASE WHEN fu.code='0GT' AND bc.code='101' THEN (i.deleted_at IS NULL AND m.financial_year_id = ".$financialYearId." and m.admin_hierarchy_id =".$adminHierarchyId." AND fac.is_active = true AND
       a.budget_type::text = '".$budgetType."' AND i.id IN (select DISTINCT activity_facility_fund_source_input_id  FROM
   activity_facility_fund_source_input_breakdowns  affsib join budget_submission_definitions bsd on affsib.budget_submission_definition_id = bsd.id
   where (fu.code = '0GT' AND bsd.field_name IN ('Basic Salary','Annual Increment','Promotion')) )) ELSE i.deleted_at IS NULL AND m.financial_year_id = ".$financialYearId."
   and m.admin_hierarchy_id =".$adminHierarchyId." AND fac.is_active = true AND a.budget_type::text = '".$budgetType."' END
       GROUP BY m.financial_year_id, a.budget_type, adm.id, s.id, fac.id, bc.id, fu.id) b ON c.financial_year_id = b.financial_year_id AND c.budget_type::text = b.budget_type::text AND c.adminid = b.adminid AND c.secid = b.secid AND c.facid = b.facid AND c.bid = b.bid AND c.fid = b.fid
WHERE (COALESCE(c.ceiling, 0::numeric) >= 0::numeric OR COALESCE(b.budget, 0::numeric) >= 0::numeric) AND abs(COALESCE(c.ceiling, 0::numeric) - COALESCE(b.budget, 0::numeric)) != 0::numeric
UNION
SELECT l.financial_year_id,
 l.budget_type,
 l.adminid,
 l.adminarea,
 l.secid,
 l.section,
 l.facility,
 l.facid,
 l.fund_source,
 l.budget_class,
 l.ceiling,
 l.budget,
 l.completion
FROM ( WITH c AS (
              SELECT fac.name AS facility,
                 fac.id AS facid,
                 adc.financial_year_id,
                 adc.budget_type,
                 adm.name AS adminarea,
                 s.name AS section,
                 adm.id AS adminid,
                 s.id AS secid,
                 bc.id AS bid,
                 bc.name AS budget_class,
                 f.id AS fid,
                 f.name AS fund_source,
                 max(c_1.name::text) AS name,
                 COALESCE(sum(adc.amount), 0::numeric) AS ceiling
                FROM admin_hierarchy_ceilings adc
                  JOIN ceilings c_1 ON c_1.id = adc.ceiling_id
                  JOIN gfs_codes gfs ON gfs.id = c_1.gfs_code_id
                  JOIN gfscode_fundsources as gfsf ON gfs.id = gfsf.gfs_code_id
                  JOIN fund_sources as f on f.id = gfsf.fund_source_id
                  LEFT JOIN fund_source_versions fv on f.id = fv.fund_source_id
                  LEFT JOIN versions v on fv.version_id = v.id
                  left join  financial_year_versions fyv on v.id = fyv.version_id
                  JOIN budget_classes bc ON bc.id = c_1.budget_class_id
                  JOIN admin_hierarchies adm ON adm.id = adc.admin_hierarchy_id
                  JOIN facilities fac ON fac.id = adc.facility_id
                  JOIN sections s ON s.id = adc.section_id
                  JOIN section_levels sl ON sl.id = s.section_level_id
               WHERE f.can_project = false and fyv.financial_year_id = ".$financialYearId." AND adc.financial_year_id = ".$financialYearId."  and adc.admin_hierarchy_id = ".$adminHierarchyId." AND c_1.is_active = true AND adc.budget_type::text = '".$budgetType."' AND sl.hierarchy_position = 4
               GROUP BY adc.financial_year_id, adc.budget_type, adm.id, s.id, fac.id, bc.id, f.id
             UNION
              SELECT fac.name AS facility,
                 fac.id AS facid,
                 adc.financial_year_id,
                 adc.budget_type,
                 adm.name AS adminarea,
                 s.name AS section,
                 adm.id AS adminid,
                 s.id AS secid,
                 bc.id AS bid,
                 bc.name AS budget_class,
                 f.id AS fid,
                 f.name AS fund_source,
                 max(c_1.name::text) AS name,
                 COALESCE(sum(adc.amount), 0::numeric) AS ceiling
                FROM admin_hierarchy_ceilings adc
                  JOIN ceilings c_1 ON c_1.id = adc.ceiling_id
                  JOIN fund_sources f ON f.id = c_1.aggregate_fund_source_id
                  JOIN budget_classes bc ON bc.id = c_1.budget_class_id
                  JOIN admin_hierarchies adm ON adm.id = adc.admin_hierarchy_id
                  JOIN facilities fac ON fac.id = adc.facility_id
                  JOIN sections s ON s.id = adc.section_id
                  JOIN section_levels sl ON sl.id = s.section_level_id
               WHERE f.can_project = true AND adc.financial_year_id = ".$financialYearId."  and adc.admin_hierarchy_id =".$adminHierarchyId." AND c_1.is_active = true AND adc.budget_type::text = '".$budgetType."'  AND sl.hierarchy_position = 4
               GROUP BY adc.financial_year_id, adc.budget_type, adm.id, s.id, fac.id, bc.id, f.id
             )
      SELECT c.financial_year_id,
         c.budget_type,
         c.adminid,
         c.adminarea,
         c.secid,
         c.section,
         c.facility,
         c.facid,
         c.fund_source,
         c.budget_class,
         COALESCE(c.ceiling, 0::numeric) AS ceiling,
         COALESCE(b.budget, 0::numeric) AS budget,
             CASE
                 WHEN c.ceiling = 0::numeric THEN 0
                 ELSE (COALESCE(b.budget, 0::numeric) / c.ceiling * 100::numeric)::integer
             END AS completion
        FROM c
          LEFT JOIN ( SELECT m.financial_year_id,
                 a.budget_type,
                 adm.name AS adminarea,
                 adm.id AS adminid,
                 s.name AS section,
                 s.id AS secid,
                 fac.id AS facid,
                 fac.name AS facility,
                 bc.id AS bid,
                 bc.name AS budget_class,
                 fu.id AS fid,
                 fu.name AS fund_source,
                 sum(i.unit_price * i.quantity * i.frequency) AS budget
                FROM activity_facility_fund_source_inputs i
                  JOIN activity_facility_fund_sources aff ON aff.id = i.activity_facility_fund_source_id
                  JOIN activity_facilities af ON af.id = aff.activity_facility_id
                  JOIN facilities fac ON fac.id = af.facility_id
                  JOIN activities a ON a.id = af.activity_id
                  JOIN mtef_sections ms ON ms.id = a.mtef_section_id
                  JOIN mtefs m ON m.id = ms.mtef_id
                  JOIN admin_hierarchies adm ON adm.id = m.admin_hierarchy_id
                  JOIN sections s ON s.id = ms.section_id
                  JOIN budget_classes bc ON bc.id = a.budget_class_id
                  JOIN fund_sources fu ON fu.id = aff.fund_source_id
               WHERE CASE WHEN fu.code='0GT' AND bc.code='101' THEN (i.deleted_at IS NULL AND m.financial_year_id = ".$financialYearId." and m.admin_hierarchy_id =".$adminHierarchyId." AND fac.is_active = true AND
               a.budget_type::text = '".$budgetType."' AND i.id IN (select DISTINCT activity_facility_fund_source_input_id  FROM
           activity_facility_fund_source_input_breakdowns  affsib join budget_submission_definitions bsd on affsib.budget_submission_definition_id = bsd.id
           where (fu.code = '0GT' AND bsd.field_name IN ('Basic Salary','Annual Increment','Promotion')) )) ELSE i.deleted_at IS NULL AND m.financial_year_id = ".$financialYearId."
           and m.admin_hierarchy_id =".$adminHierarchyId." AND fac.is_active = true AND a.budget_type::text = '".$budgetType."' END
               GROUP BY m.financial_year_id, a.budget_type, adm.id, s.id, fac.id, bc.id, fu.id) b ON c.budget_type::text = b.budget_type::text AND c.adminid = b.adminid AND c.secid = b.secid AND c.facid = b.facid AND c.bid = b.bid AND c.fid = b.fid
       WHERE (COALESCE(c.ceiling, 0::numeric) >= 0::numeric OR COALESCE(b.budget, 0::numeric) >= 0::numeric) AND abs(COALESCE(c.ceiling, 0::numeric) - COALESCE(b.budget, 0::numeric)) != 0::numeric) l
       ORDER BY 4";

       return DB::select($query);
  }

  public static function getAggregatePEBudget($budgetType, $financialYearId, $adminIdsString, $sectionIdsString, $peBudgetClassIdString, $peFundSourceIdString)
    {
        $query = "SELECT
              SUM(i.unit_price * i.quantity * i.frequency) AS budget
            FROM activity_facility_fund_source_inputs AS i
              INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
              INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
              INNER JOIN activities AS a ON a.id = af.activity_id
              INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
              INNER JOIN mtefs AS m ON m.id = ms.mtef_id
              INNER JOIN facilities AS f ON f.id = af.facility_id
            WHERE a.budget_type='" . $budgetType . "' AND f.is_active = true AND a.budget_class_id IN (" . $peBudgetClassIdString . ") AND aff.fund_source_id IN (" . $peFundSourceIdString . ")
            AND m.admin_hierarchy_id IN (" . $adminIdsString . ")
            AND m.financial_year_id = " . $financialYearId . " and i.deleted_at is null AND
            ms.section_id in (" . $sectionIdsString . ")";
        return DB::select($query)[0]->budget;
    }

    public static function getAggregatePECeiling($budgetType, $financialYearId, $adminIdsString, $sectionId, $peBudgetClassIdString)
    {
        $query = "SELECT
                  COALESCE (SUM(adc.amount),0) as ceiling FROM admin_hierarchy_ceilings AS adc
                  INNER JOIN ceilings AS c ON c.id = adc.ceiling_id
                  INNER JOIN admin_hierarchies AS a ON a.id = adc.admin_hierarchy_id
                  INNER JOIN gfs_codes as gfs ON gfs.id=c.gfs_code_id
                  JOIN gfscode_fundsources as gfsf ON gfs.id = gfsf.gfs_code_id
                  JOIN fund_sources as f on f.id = gfsf.fund_source_id
                  LEFT JOIN fund_source_versions fv on f.id = fv.fund_source_id
                  LEFT JOIN versions v on fv.version_id = v.id
                  left join  financial_year_versions fyv on v.id = fyv.version_id
                  WHERE adc.budget_type='" . $budgetType . "' and fyv.financial_year_id = '".$financialYearId."' AND  f.can_project=false and
                  adc.admin_hierarchy_id IN (" . $adminIdsString . ") and adc.section_id IN (" . Flatten::userBudgetingSectionIdString() . ")  and adc.financial_year_id = " . $financialYearId . "
                  and adc.is_facility = false and c.is_active=true and c.budget_class_id IN (" . $peBudgetClassIdString . ")";

        $ownquery = "SELECT
                  COALESCE (SUM(adc.amount),0) as ceiling FROM admin_hierarchy_ceilings AS adc
                  INNER JOIN ceilings AS c ON c.id = adc.ceiling_id
                  INNER JOIN admin_hierarchies AS a ON a.id = adc.admin_hierarchy_id
                  WHERE adc.budget_type='" . $budgetType . "' AND c.is_aggregate = true and
                  adc.admin_hierarchy_id IN (" . $adminIdsString . ") and adc.section_id IN (" . Flatten::userBudgetingSectionIdString() . ")  and adc.financial_year_id = " . $financialYearId . "
                  and adc.is_facility = false and c.is_active=true and c.budget_class_id IN (" . $peBudgetClassIdString . ")";


        return (DB::select($query)[0]->ceiling + DB::select($ownquery)[0]->ceiling);
    }

}
