<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Project extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "projects";
    protected $fillable = ['name', 'code', 'description'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'name' => 'required|string',
                'description' => 'required|string',
                'code' => 'required|min:4|max:4',
            ],
            $merge);
    }

    public static function getByBudgetClassAndSection($budgetClassId, $sectionId)
    {
        $naProject = DB::table('projects')->where('code','0000')->get();
        $projectBc = ConfigurationSetting::getValueByKey('BUDGET_CLASSES_WITH_PROJECTS');
        if($projectBc != null && !in_array($budgetClassId, $projectBc)){
            return $naProject;
        }
        $fundSourcesIds = FundSourceBudgetClass::where('budget_class_id',$budgetClassId)->pluck('fund_source_id');
        $sectorIds = Section::where('id',$sectionId)->pluck('sector_id');
        $projects = DB::table('projects as p')->
                        join('project_fund_sources as pf', 'p.id', 'pf.project_id')->
                        join('sector_projects as ps', 'p.id', 'ps.project_id')->
                       // where('p.code', '!=','0000')->
                        whereIn('ps.sector_id', $sectorIds)->
                        whereIn('pf.fund_source_id', $fundSourcesIds)->
                        distinct()->
                        orderBy('p.code')->
                        select('p.*')->
                        get();
        return $projects;

    }

    public function admin_hierarchy_projects()
    {
        return $this->hasMany('App\Models\Setup\AdminHierarchyProject');
    }

    public function fund_sources()
    {
        return $this->hasMany('App\Models\Setup\ProjectFundSource', 'project_id', 'id');
    }

    public function sector_projects()
    {
        return $this->hasMany('App\Models\Setup\SectorProject',"project_id","id");
    }

    public function ceilings()
    {
        return $this->hasMany('App\Models\Budget\Ceiling');
    }

    public function projectDataForms()
    {
        return $this->hasMany('App\Models\Setup\ProjectDataForm');
    }

    public function project_versions()
    {
        return $this->hasMany('App\Models\Setup\ProjectVersion', 'project_id', 'id');
    }

    public function activity(){
        return $this->hasMany('App\Models\Planning\Activity',"project_id","id");
    }
}
