<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FacilityType extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "facility_types";

    protected $fillable = ['name', 'code', 'description', 'lga_level_id', 'admin_hierarchy_level_id', 'parent_id'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'name' => 'required|string|max:128',
                'code' => 'required|unique:facility_types,code' . ($id ? ",$id" : ''),
                'description' => 'string|nullable|max:255',
                'sections' => 'required',
                'lga_level_id' => 'required|integer',
                'admin_hierarchy_level_id' => 'required|integer',
                'parent_id' => 'integer|nullable',
            ], $merge);
    }

    public function facilities()
    {
        return $this->hasMany('App\Models\Setup\Facility');
    }


    public function lga_level()
    {
        return $this->belongsTo('App\Models\Setup\LgaLevel', 'lga_level_id', 'id');
    }

    public function admin_hierarchy_level()
    {
        return $this->belongsTo('App\Models\Setup\AdminHierarchyLevel', 'admin_hierarchy_level_id', 'id');
    }

    public function sections(){
        return $this->belongsToMany('App\DTOs\SetupDTOs\SectionDTO','facility_type_sections','facility_type_id','section_id');
    }
}
