<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReferenceTypeVersion extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'reference_type_versions';
    protected $fillable = ['version_id','reference_type_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'reference_type_id' => 'required|integer',
                'version_id' => 'required|unique_with:reference_type_versions,reference_type_id'. ($id ? ",$id" : '')
            ],
            $merge);
    }

    public function version(){
        return $this->belongsTo("App\Models\Setup\Version","version_id",'id');
    }

    public function referenceType(){
        return $this->belongsTo("App\Models\Setup\ReferenceType","reference_type_id",'id');
    }
}
