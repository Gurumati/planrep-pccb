<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class BdcGroup extends Model
{
    public $timestamps=true;
    protected $table = 'bdc_groups';
    //use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";

    protected $fillable = ['name','bdc_main_group_fund_source_id','value'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string|max:64',
                'bdc_main_group_fund_source_id' => 'required|integer',
                'value' => 'required|integer',
            ],
            $merge);
    }

    public function bdc_main_group_fund_source(){
        return $this->belongsTo('App\Models\Budgeting\BdcMainGroupFundSource','bdc_main_group_fund_source_id','id');
    }
}
