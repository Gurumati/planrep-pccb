<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FundSourceCategory extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "fund_source_categories";

    public function fund_type() {
        return $this->belongsTo('App\Models\Setup\FundType', 'fund_type_id','id');
    }
}

