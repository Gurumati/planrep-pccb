<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class BudgetClassTemplate extends Model
{
    protected $table = "budget_class_templates";
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function budgetClass()
    {
        return $this->belongsTo('App\Models\Setup\BudgetClass')->select('id','name');
    }
}
