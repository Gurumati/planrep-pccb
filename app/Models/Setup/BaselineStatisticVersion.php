<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class BaselineStatisticVersion extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'baseline_statistic_versions';
    protected $fillable = ['baseline_statistic_id','version_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'baseline_statistic_id' => 'required|integer',
                'version_id' => 'required|integer|unique_with:baseline_statistic_versions,baseline_statistic_id',
            ],
            $merge);
    }

    public function baseline_statistic() {
        return $this->belongsTo('App\Models\Setup\BaselineStatisticVersion','baseline_statistic_id','id');
    }

    public function version() {
        return $this->belongsTo('App\Models\Setup\Version','version_id','id');
    }
}
