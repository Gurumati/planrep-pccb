<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NationalTarget extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'national_targets';

    protected $fillable = ['code', 'description', 'priority_area_id'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                /*'code' => 'required|unique:national_targets,code' . ($id ? ",$id" : ''),*/
                'description' => 'required|string|max:500',
                'priority_area_id' => 'required|integer',
            ], $merge);
    }


    public function priority_area() {
        return $this->belongsTo('App\Models\Setup\PriorityArea','priority_area_id','id');
    }
}
