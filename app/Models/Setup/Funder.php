<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Funder extends Model
{
    public $timestamps=true;
    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";

    public function projectFunder()
    {
        return $this->hasMany('App\Models\Setup\ProjectFunder');
    }

    public function fund_sources() {
        return $this->hasMany('App\Models\Setup\FundSource');
    }
}
