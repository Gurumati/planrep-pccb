<?php

namespace App\Models\Setup;

use App\Http\Services\UserServices;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AdminHierarchyLevel extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected  $table = "admin_hierarchy_levels";

    public function admin_hierarchies(){
        return $this->hasMany('App\Models\Setup\AdminHierarchy');
    }

    public function AdminHierarchySectMappings(){
        return $this->hasMany('App\Models\Planning\AdminHierarchySectMappings');
    }

    public function decision_levels(){
        return $this->hasMany('App\Models\Setup\DecisionLevel');
    }

    public function facilityTypes(){
        return $this->hasMany('App\Models\Setup\FacilityType');
    }

    public function casPlans(){
        return $this->hasMany('App\Models\Setup\CasPlan');
    }

    public static function getUserLevelAndChildren(){
        $userPosition= DB::table('admin_hierarchies as a')->
                           join('admin_hierarchy_levels as al','al.id','a.admin_hierarchy_level_id')->
                           where('a.id',UserServices::getUser()->admin_hierarchy_id)->first();
        $all = DB::table('admin_hierarchy_levels')->
                   where('hierarchy_position', '>=',$userPosition->hierarchy_position)->
                   where('hierarchy_position', '<=',2)->
                   distinct()->
                   orderBy('hierarchy_position','asc')->
                   get();
        return $all;
    }

    public static function getByAdminHierarchyId($adminHierarchyId){
        return DB::table('admin_hierarchy_levels as l')->
                    join('admin_hierarchies as a','l.id','a.admin_hierarchy_level_id')->
                    where('a.id',$adminHierarchyId)->
                    select('l.*')->first();
    }
}
