<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CasPlanTable extends Model
{
    public $timestamps = true;
    protected $dates      = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table      = 'cas_plan_tables';
    protected $with       = ['casPlanContent'];
    use SoftDeletes;
    protected $casts = [
        'default_values' => 'array',
    ];

    public function casPlanContent()
    {
        return $this->belongsTo('App\Models\Setup\CasPlanContent')->select('id', 'description', 'cas_plan_id');
    }

    public function casPlanTableColumns()
    {
        return $this->hasMany('App\Models\Setup\CasPlanTableColumn');
    }

    public function casPlanTableItemAdminHierarchies()
    {
        return $this->hasMany('App\Models\Planning\CasPlanTableItemAdminHierarchy');
    }

    public function casPlanTableReportConstraint()
    {
        return $this->hasMany('App\Models\Setup\CasPlanTableReportConstraint');
    }

    public function casPlanTableDetails()
    {
        return $this->hasMany('App\Models\Planning\CasPlanTableDetail');
    }

    public function casPlan(){
        return $this->belongsTo('App\Models\Setup\CasPlan', 'cas_plan_content_id');
//        return $this->hasManyThrough('App\Models\Setup\CasPlanContent','App\Models\Setup\CasPlanTable');

    }
}
