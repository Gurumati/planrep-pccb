<?php

namespace App\Models\setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FundSourceBudgetClass extends Model
{
    public $timestamps=true;
    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";
    protected $table = 'fund_source_budget_classes';
    protected $fillable = ['fund_source_id','budget_class_id','fund_type_id','bank_account_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'fund_source_id' => 'required|integer',
                'budget_class_id' => 'required|integer',
                'fund_type_id' => 'required|integer',
            ],
            $merge);
    }

    public function fund_source(){
        return $this->belongsTo('App\Models\Setup\FundSource','fund_source_id','id');
    }

    public function budget_class(){
        return $this->belongsTo('App\Models\Setup\BudgetClass','budget_class_id','id');
    }

    public function fund_type(){
        return $this->belongsTo('App\Models\Setup\FundType','fund_type_id','id');
    }

    public function bank_account(){
        return $this->belongsTo('App\Models\Setup\BankAccount','bank_account_id','id');
    }
}
