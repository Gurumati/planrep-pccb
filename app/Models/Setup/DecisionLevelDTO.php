<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class DecisionLevelDTO extends Model
{

    protected $table = "decision_levels";

    protected $with = [
       'nextDecisionLevel'
    ];

    public function nextDecisionLevel() {
       return $this->belongsTo('App\Models\Setup\DecisionLevelDTO', 'parent_id');
    }

    public function adminHierarchyLevels(){
        return $this->hasMany('App\Models\Setup\DecisionLevelAdminHierarchy','decision_level_id');
    }
}
