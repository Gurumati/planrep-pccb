<?php

namespace App\Models\Setup;

use App\Http\Controllers\Flatten;
use App\Http\Services\UserServices;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Models\CacheKeys;
use Illuminate\Support\Facades\Log;

class Section extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'sections';


    protected $with = [
        'childSections'
    ];

    public static function getParentSectionIds($sectionId)
    {

        $sectionIds = Cache::rememberForever(CacheKeys::SECTION_WITH_PARENT_IDS . $sectionId, function () use ($sectionId) {
            $flatten = new Flatten();
            $sectionWithParentTree = SectionParent::where('id', $sectionId)->get();
            return $flatten->flattenSectionWithParent($sectionWithParentTree);

        });

        return $sectionIds;
    }

    public static function canProject($sectionId) {
        $sectionCanProject = false;
        $topSections = Section::whereNull('parent_id')->get();
        foreach ($topSections as $s) {
            if ($sectionId == $s->id) {
                $sectionCanProject = true;
            }
        }
        return $sectionCanProject;
    }

    public static function adminAtCeilingStart($sectionLevelPosition, $sectionId)
    {
        $startSectionLevel = SectionLevel::where('hierarchy_position', $sectionLevelPosition)->first();

        $section = self::find($sectionId);

        return ($startSectionLevel->id == $section->section_level_id) ? true : false;
    }
    public static function getSectionIdsWithChildren($sectionId)
    {
        // return Cache::rememberForever('SECTION_IDS_WITH_CHILDREN_' . $sectionId, function () use ($sectionId) {
            $sections = Section::with('childSections')->where('id', $sectionId)->get();
            $flatten = new Flatten();
            return $flatten->flattenSectionWithChild($sections);
        // });
    }

    public static function byFacility($sectionLevelId, $facilityId, $fundSourceId)
    {
        //If user has permission to reallocate accross cost centre(e.g Dispenary to Admin and general)
        if(UserServices::hasPermission(['execution.reallocation.accross-costcentre'])){
            return Section::get();
        }

        //Get Subbudget Classes Ids by fund sources to determine ceiling
        $subBudgetClassIds = DB::table('fund_source_budget_classes')
            ->where('fund_source_id', $fundSourceId)
            ->pluck('budget_class_id')
            ->toArray();

        //Get Sectors Ids by ceiling from seleted budget classes
        $sectorIds = DB::table('ceiling_sectors as cs')
            ->join('ceilings as c', 'c.id', 'cs.ceiling_id')
            ->whereIn('c.budget_class_id', $subBudgetClassIds)
            ->pluck('cs.sector_id')
            ->toArray();

        //Select
        $sectionIds = DB::table('facilities as f')
            ->join('facility_types as ft', 'ft.id', 'f.facility_type_id')
            ->join('facility_type_sections as fs', 'fs.facility_type_id', 'ft.id')
            ->where('f.id', $facilityId)
            ->select('fs.*')
            ->pluck('fs.section_id')
            ->toArray();

        return Section::whereIn('id', $sectionIds)
            ->whereIn('sector_id', $sectorIds)
            ->where('section_level_id', $sectionLevelId)
            ->get();
    }

    public function childSections()
    {
        return $this->hasMany('App\Models\Setup\Section', 'parent_id');
    }

    public function users()
    {
        return $this->hasMany('App\Models\Auth\User');
    }

    public function projects()
    {
        return $this->hasMany('App\Models\Setup\Project');
    }

    public function section_level()
    {
        return $this->belongsTo('App\Models\Setup\SectionLevel', 'section_level_id', 'id');
    }

    public function responsiblePerson()
    {
        return $this->hasOne('App\Models\Setup\ResponsiblePerson');
    }

    public function objective()
    {
        return $this->hasMany('App\Models\Setup\Objective');
    }

    public function facilities()
    {
        return $this->hasMany('App\Models\Setup\Facility');
    }

    public function sector()
    {
        return $this->belongsTo('App\Models\Setup\Sector', 'sector_id', 'id');
    }

    public function retiring_employees()
    {
        return $this->hasMany('App\Models\Budgeting\RetiringEmployee');
    }

    public function AdminHierarchySectMappings()
    {
        return $this->hasMany('App\Models\Planning\AdminHierarchySectMappings');
    }

    public function section_generic_types()
    {
        return $this->hasMany('App\Models\Setup\SectionGenericType');
    }

    public function pe_budgets()
    {
        return $this->hasMany('App\Models\Budgeting\PeBudget');
    }

    public function facility_types()
    {
        return $this->hasMany('App\Models\Setup\FacilityType');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Setup\Section');
    }
    public function mtefSections()
    {
        return $this->hasMany('App\Models\Planning\MtefSection');
    }

    public function gfs_code_projections()
    {
        return $this->hasMany('App\Models\Planning\GfsCodeProjection');
    }

    public function gfs_code_sections()
    {
        return $this->hasMany('App\Models\Setup\GfsCodeSection');
    }
}
