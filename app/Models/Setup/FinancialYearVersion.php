<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class FinancialYearVersion extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'financial_year_versions';
    protected $fillable = ['financial_year_id','version_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'financial_year_id' => 'required|integer',
                'version_id' => 'required|unique_with:financial_year_versions,financial_year_id'. ($id ? ",$id" : '')
            ],
            $merge);
    }

    public function financial_year() {
        return $this->belongsTo('App\Models\Setup\FinancialYear','financial_year_id','id');
    }

    public function version() {
        return $this->belongsTo('App\Models\Setup\Version','version_id','id');
    }
}
