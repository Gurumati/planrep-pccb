<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class ProjectFundSource extends Model
{
    public $timestamps = true;
    //use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "project_fund_sources";

    protected $with = [
        'fund_source','project'
    ];

    public function fund_source()
    {
        return $this->belongsTo('App\Models\Setup\FundSource','fund_source_id','id');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Setup\Project','project_id','id');
    }
}
