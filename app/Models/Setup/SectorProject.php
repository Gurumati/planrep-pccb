<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SectorProject extends Model {
    public $timestamps = true;
    //use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'sector_projects';

    public function sector(){
        return $this->belongsTo('\App\Models\Setup\Sector');
    }

    public function project(){
        return $this->belongsTo('\App\Models\Setup\Project');
    }
}
