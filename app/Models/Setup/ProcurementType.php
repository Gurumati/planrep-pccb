<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProcurementType extends Model
{
    public $timestamps = true;
    protected $table = 'procurement_types';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function gfs_codes()
    {
        return $this->hasMany('App\Models\Setup\GfsCode');
    }
}
