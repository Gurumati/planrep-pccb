<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Planning\ReferenceDocument;
use Illuminate\Support\Facades\DB;

class FinancialYear extends Model
{
    //
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "financial_years";

    protected $fillable = [
        'name',
        'description',
        'start_date',
        'end_date',
        'status',
        'sort_order',
        'is_active',
        'previous',
        'is_current',
    ];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'name' => 'required|string|max:128',
                'description' => 'string|nullable|max:255',
                'start_date' => 'required|date',
                'end_date' => 'required|date',
                'status' => 'integer|nullable',
                'sort_order' => 'integer|nullable',
                'is_active' => 'boolean|nullable',
                'previous' => 'integer|nullable',
                'is_current' => 'boolean|nullable',
            ], $merge);
    }

    public static function getByBudgetType($budgetType)
    {
        switch ($budgetType){
            case 'CURRENT':
                return FinancialYear::where('status',1)->first();
                break;
            case 'APPROVED':
                    $byStatus = FinancialYear::where('status',2)->first();
                    if($byStatus != null){
                        return $byStatus;
                    } else {
                        $byItems =  FinancialYear::where('status',1)->first();
                        if($byItems != null && FinancialYear::hasApprovedItems($byItems->id) > 0){
                            return $byItems;
                        } else {
                            return null;
                        }
                    }
                break;
            case 'CARRYOVER' || 'SUPPLEMENTARY' :
                 return FinancialYear::where('status',2)->first();
                break;
            default:
                return null;
        }
    }
    public static function hasApprovedItems($financialYearId){
        return DB::table('activities as a')->
                    join('mtef_sections as ms','ms.id','a.mtef_section_id')->
                    join('mtefs as m','m.id','ms.mtef_id')->
                    where('m.financial_year_id',$financialYearId)->
                    where('a.budget_type','APPROVED')->
                    count('m.id');

    }

    public static function getPreviousYear($financialYearId){
        $thisYear = self::find($financialYearId);
        return FinancialYear::where('end_date','<',$thisYear->start_date)->orderBy('start_date','desc')->select('id','name')->first();
    }

    public static function getForwards($financialYearId) {
        $currentYears = FinancialYear::find($financialYearId);
        return FinancialYear::with('previousYear')->where('start_date', '>', $currentYears->end_date)->where('is_active', true)->orderBy('start_date', 'asc')->limit(4)->select('id', 'name')->get();
    }

    public static function getNextYear($financialYearId){
            $thisYear = self::find($financialYearId);
            return FinancialYear::where('start_date','>',$thisYear->end_date)->orderBy('end_date','asc')->select('id','name')->first();
    }

    public static function getReferenceDocFinancialYears($referenceDocId){
        $refDoc = ReferenceDocument::find($referenceDocId);
        $financialYears = FinancialYear::where('id','>=',$refDoc->start_financial_year)->
                                where('id','<=',$refDoc->end_financial_year)->get();
        return $financialYears;
    }

    public static function linkCurrentVersions($financialYearId){
        $allCurrentVesions = Version::where('is_current', true)->distinct('version_type_id')->get();
        foreach ($allCurrentVesions as $version) {
            FinancialYearVersion::firstOrCreate(['financial_year_id'=>$financialYearId,'version_id'=>$version->id]);
        }
    }


    public function periods()
    {
        return $this->hasMany('App\Models\Setup\Period');
    }

    public function projections()
    {
        return $this->hasMany('App\Models\Planning\Projection');
    }

    public function referenceDocuments()
    {
        return $this->hasMany('App\Models\Setup\ReferenceDocument');
    }

    public function mtefs()
    {
        return $this->hasMany('App\Models\Planning\Mtef');
    }

    public function ceilings()
    {
        return $this->hasMany('App\Models\Planning\Ceiling');
    }

    public function mtef_sections()
    {
        return $this->hasManyThrough('App\Models\Planning\MtefSection', 'App\Models\Planning\Mtef');
    }

    public function casPlanTableItemValues()
    {
        return $this->hasMany('App\Models\Setup\CasPlanTableItemValue');
    }

    public function baselineStatisticValue()
    {
        return $this->hasMany('App\Models\Setup\BaselineStatisticValue');
    }

    public function previousYear()
    {
        return $this->belongsTo('App\Models\Setup\FinancialYear','previous','id');
    }

}
