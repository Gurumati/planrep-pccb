<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class ProjectDataForm extends Model
{
    protected $table = 'project_data_forms';

    public function sectors(){
        return $this->belongsTo('App\Models\Setup\Sector','sector_id');
    }

    public function projects(){
        return $this->belongsTo('App\Models\Setup\Project','project_id');
    }

    public function projectDataFormContents(){
        return $this->hasMany('App\Models\Setup\ProjectDataFormContent');
    }
}
