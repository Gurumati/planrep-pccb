<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class PriorityAreaPlanChain extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'priority_area_plan_chains';
    protected $fillable = ['priority_area_id','plan_chain_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'priority_area_id' => 'required|integer',
                'plan_chain_id' => 'required|integer|unique_with:priority_area_plan_chains,priority_area_id',
            ],
            $merge);
    }

    public function priority_area() {
        return $this->belongsTo('App\Models\Setup\PriorityArea','priority_area_id','id');
    }

    public function plan_chain() {
        return $this->belongsTo('App\Models\Setup\PlanChain','plan_chain_id','id');
    }
}
