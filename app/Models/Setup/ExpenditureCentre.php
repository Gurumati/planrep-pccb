<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpenditureCentre extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "bdc_expenditure_centres";

    protected $with = [
        'children'
    ];

    public function children() {
        return $this->hasMany('App\Models\Setup\ExpenditureCentre', 'parent_id');
    }

    public function parent(){
        return $this->belongsTo('App\Models\Setup\ExpenditureCentre','parent_id','id');
    }

    public function expenditure_centre_group(){
        return $this->belongsTo('App\Models\Setup\ExpenditureCentreGroup','expenditure_centre_group_id','id');
    }

    public function section(){
        return $this->belongsTo('App\Models\Setup\Section','section_id','id');
    }
    public function sections(){
        return$this->belongsToMany('App\Models\Setup\Section','bdc_expenditure_centre_sections','bdc_expenditure_centre_id');
    }

    public function activity_category(){
        return $this->belongsTo('App\Models\Setup\ActivityCategory','activity_category_id','id');
    }

    public function lga_level(){
        return $this->belongsTo('App\Models\Setup\LgaLevel','lga_level_id','id');
    }

    public function link_spec(){
        return $this->belongsTo('App\Models\Setup\LinkSpec','link_spec_id','id');
    }

    public static function  getUserExpenditureCentres($groupIds){
       return ExpenditureCentre::with('children','expenditure_centre_group')->whereNull('parent_id')->whereId('expenditure_centre_group_id',$groupIds);
    }



}
