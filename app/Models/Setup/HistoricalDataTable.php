<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class HistoricalDataTable extends Model
{
    //use SoftDeletes;

    public $timestamps = true;
    protected $table = 'historical_data_tables';
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $fillable = ['name', 'code','grouping','type'];

    protected $with = ['columns'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'name' => 'required|string',
                'code' => 'required|unique:historical_data_tables,code' . ($id ? ",$id" : ''),
                'grouping' => 'string|required',
                'type' => 'string|required',
            ],
            $merge);
    }
    public function columns()
    {
        return $this->hasMany("App\Models\Setup\HistoricalDataColumn", 'historical_data_table_id', 'id');
    }
}
