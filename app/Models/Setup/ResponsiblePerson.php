<?php

namespace App\Models\Setup;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Input;

class ResponsiblePerson extends Model {
    public $timestamps = true;
    protected $table = 'users';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    /**
     * get all responsible persion who can be assigned to sector specific activities
     * @param $adminHierarchyId
     * @param $sectionId
     * @return mixed
     */
    public static function byAdminAreaAndSector($adminHierarchyId ,$sectionId){
        $sectorId = Section::find($sectionId)->sector_id;
        $users = User::whereHas('section', function ($filter) use ($sectorId) {
                        $filter->where('sector_id', $sectorId);
                    })->
                    where('admin_hierarchy_id', $adminHierarchyId)->
                    orderBy('user_name')->get();
        return $users;
    }

    /**
     * Get all by specified admin hierarchy and section tree
     *
     * @param $adminHierarchyId
     * @param $sectionId
     * @return mixed
     */
    public static function paginateByAdminAreaAndSectionTree($adminHierarchyId,$sectionId){
        $perPage = Input::get('perPage',10);
        $sectorId = Section::find($sectionId)->sector_id;
        $users = User::whereHas('section', function ($filter) use ($sectorId) {
            $filter->where('sector_id', $sectorId);
        })->
        where('admin_hierarchy_id', $adminHierarchyId)->
        orderBy('user_name')->paginate();

        return $users;
    }

    public function admin_hierarchy() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchy', 'admin_hierarchy_id', 'id');
    }

    public function section() {
        return $this->belongsTo('App\Models\Setup\Section', 'section_id', 'id');
    }

    public function decision_level() {
        return $this->belongsTo('App\Models\Setup\DecisionLevel', 'decision_level_id', 'id');
    }

}
