<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class GfsFundSource extends Model
{
    public $timestamps = true;
    protected $table = 'gfscode_fundsources';
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $fillable = ['fund_source_id', 'gfs_code_id'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
            
                'fund_source_id' => 'integer|required',
                'gfs_code_id' => 'integer|required',
            ],
            $merge);
    }

    public function fund_source() {
        return $this->belongsTo('App\Models\Setup\FundSource','fund_source_id','id');
    }

    public function gfs_code() {
        return $this->belongsTo('App\Models\Setup\GfsCode','gfs_code_id','id');
    }
}
