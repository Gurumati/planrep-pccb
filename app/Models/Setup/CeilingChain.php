<?php

namespace App\Models\Setup;

use App\Http\Controllers\Flatten;
use App\Models\CacheKeys;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class CeilingChain extends Model
{
    public $timestamps=true;
    protected $table = 'ceiling_chains';
    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";

    public static function getNextCeilingChain($nextId){
        return DB::table('ceiling_chains')->where('id', $nextId)->first();
    }

    /**
     * @param $adminHierarchyLevelPosition
     * @param $userSectionLevelPosition
     * @return mixed
     */
    public static function getCurrentCeilingChain($parentAdminHierarchyId, $parentSectionId){

        $adminHierarchyLevelPosition = AdminHierarchy::find($parentAdminHierarchyId)->admin_hierarchy_level->hierarchy_position;
        $userSectionLevelPosition = Section::with('section_level')->find($parentSectionId)->section_level->hierarchy_position;

        $currentCeilingChain = DB::table('ceiling_chains')->
            where('for_admin_hierarchy_level_position', $adminHierarchyLevelPosition)->
            where('admin_hierarchy_level_position', $adminHierarchyLevelPosition)->
            where('section_level_position', $userSectionLevelPosition)->
            first();

        return $currentCeilingChain;
    }


    public static function getStartingPoint()
    {

        return Cache::rememberForever(CacheKeys::START_CEILING_CHAIN, function (){
            /** Get the minimum hierarchy position of the ceiling Chain Which is the highest admin hierarchy level */
            $minHierarchy = DB::table('ceiling_chains')->min('admin_hierarchy_level_position');

            /** Get the minimum section hierarchy position number which is the highest Section level*/
            $minSection = DB::table('ceiling_chains')->min('section_level_position');

            /** Get the starting of the Ceiling chain*/
            return DB::table('ceiling_chains')->where('admin_hierarchy_level_position',$minHierarchy)->where('section_level_position',$minSection)->first();
        });

    }

    /**
     * Get next lower ceiling section ids by parent section id and sectors and next ceiling chain section level position
     * @param $ceilingSectorIds
     * @param $parentSectionId
     * @param $nextLowerCeilingChainSectionPosition
     * @return mixed
     */
    public static function getNextLowerCeilingChainSection($ceilingSectorIds,$parentSectionId,$nextLowerCeilingChainSectionPosition)
    {
        $flatten = new Flatten();

        $nextLowerCeilingChainSectionLevel = SectionLevel::getByPosition($nextLowerCeilingChainSectionPosition);

        /** @var  $parentAndAllChildrenSectionTree @Section with 'childrenSection' @array of child @Section  (see @Section Model with definition and relationship)*/
        $parentAndAllChildrenSectionTree = Section::where('id', $parentSectionId)->get();

        return $flatten->flattenSectionWithChildOnly($parentAndAllChildrenSectionTree, $parentSectionId, $ceilingSectorIds, $nextLowerCeilingChainSectionLevel->id);
    }

    public static function getNextLowerCeilingChainAdminHierarchies($parentAdminHierarchyId, $nextLowerCeilingChainAdminHierarchyPosition)
    {
        $flatten = new Flatten();

        $nextLowerCeilingChainAdminHierarchyLevelIds = AdminHierarchyLevel::where('hierarchy_position',$nextLowerCeilingChainAdminHierarchyPosition)->
                                                                            pluck('id');

        /** @var  $parentAndAllChildrenAdminHierarchyTree @array (@AdminHierarchy) limited to three 3 levels only */
        $parentAndAllChildrenAdminHierarchyTree = AdminHierarchy::getAdminHierarchyTree($parentAdminHierarchyId);

        return $flatten->flattenAdminWithChildOnly($parentAndAllChildrenAdminHierarchyTree,$parentAdminHierarchyId,$nextLowerCeilingChainAdminHierarchyLevelIds);
    }

    public function next_stage(){
        return $this->belongsTo('App\Models\Setup\CeilingChain','next_id','id');
    }

    public function for_admin_hierarchy_level(){
        return $this->belongsTo('App\Models\Setup\AdminHierarchyLevel','for_admin_hierarchy_level_position','hierarchy_position');
    }

    public function admin_hierarchy_level(){
        return $this->belongsTo('App\Models\Setup\AdminHierarchyLevel','admin_hierarchy_level_position','hierarchy_position');
    }

    public function section_level(){
        return $this->belongsTo('App\Models\Setup\SectionLevel','section_level_position','hierarchy_position');
    }
}
