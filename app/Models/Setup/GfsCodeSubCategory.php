<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GfsCodeSubCategory extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'gfs_code_sub_categories';

    protected $fillable = ['name','gfs_code_category_id','is_procurement'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string',
                'gfs_code_category_id' => 'required|integer|unique_with:gfs_code_sub_categories,name'. ($id ? ",$id" : ''),
                'is_procurement' => 'boolean',
            ],
            $merge);
    }

    public function gfs_code_category() {
        return $this->belongsTo('App\Models\Setup\GfsCodeCategory','gfs_code_category_id','id');
    }

    public function gfs_codes() {
        return $this->hasMany('App\Models\Setup\GfsCode');
    }
}
