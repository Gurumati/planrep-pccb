<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GfsCodeCategory extends Model
{
    public $timestamps = true;
    protected $table = 'gfs_code_categories';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $fillable = ['name','description','sort_order'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string|max:64',
                'description' => 'max:255',
                'sort_order' => 'integer|nullable',
            ],
            $merge);
    }

    public function gfs_code_sub_categories() {
        return $this->hasMany('App\Models\Setup\GfsCodeSubCategory');
    }

    public function gfs_codes() {
        return $this->hasManyThrough('App\Models\Setup\GfsCode','App\Models\Setup\GfsCodeSubCategory');
    }
}
