<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GenericSectorProblem extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'generic_sector_problems';

    public function priority_area() {
        return $this->belongsTo('App\Models\Setup\PriorityArea','priority_area_id','id');
    }

    public function planning_matrix() {
        return $this->belongsTo('App\Models\Setup\PlanningMatrix','planning_matrix_id','id');
    }
}
