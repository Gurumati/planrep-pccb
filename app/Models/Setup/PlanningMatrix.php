<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanningMatrix extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'planning_matrices';

    public function reference_document() {
        return $this->belongsTo('App\Models\Setup\ReferenceDocument','reference_document_id','id');
    }

    public function sectors(){
        return $this->belongsToMany('App\Models\Setup\Sector','planning_matrix_sectors','planning_matrix_id');
    }
}
