<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class ExpenditureCentreLink extends Model
{
    public $timestamps = true;
    //use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "bdc_expenditure_centre_links";

    public function gfs_code(){
        return $this->belongsTo('App\Models\Setup\GfsCode','gfs_code_id','id');
    }

    public function expenditure_centre(){
        return $this->belongsTo('App\Models\Setup\ExpenditureCentre','expenditure_centre_id','id');
    }

    public function activity_category(){
        return $this->belongsTo('App\Models\Setup\ActivityCategory','activity_category_id','id');
    }

    public function lga_level(){
        return $this->belongsTo('App\Models\Setup\LgaLevel','lga_level_id','id');
    }
}
