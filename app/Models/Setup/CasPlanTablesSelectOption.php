<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class CasPlanTablesSelectOption extends Model
{
    protected $table = 'cas_plan_table_select_options';

    public function casPlanTableColumns(){
        return $this->belongsTo('App\Models\Setup\CasPlanTableColumn');
    }
}
