<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PlanChainSector extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'plan_chain_sectors';

    public function planChains() {
        return $this->belongsTo('App\Models\Setup\PlanChain');
    }

    public function sectors() {
        return $this->belongsTo('App\Models\Setup\Sector');
    }

}
