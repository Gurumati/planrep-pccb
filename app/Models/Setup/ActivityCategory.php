<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class ActivityCategory extends Model
{
    public $timestamps=true;
    protected $table = 'activity_categories';
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";

    protected $fillable = ['name','code','description','sort_order','is_active','created_by','updated_by'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string|max:64',
                'code' => 'required|unique:activity_categories,code' . ($id ? ",$id" : ''),
                'description' => 'sometimes|max:255',
                'sort_order' => 'sometimes|integer',
                'is_active' => 'sometimes|boolean',
            ],
            $merge);
    }
}
