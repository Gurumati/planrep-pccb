<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class Version extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'versions';
    protected $fillable = ['name','version_type_id','date'];
    private static $versionTables =[
        'PC'=>'plan_chain_versions',
        'PI'=>'performance_indicator_versions',
        'PA'=>'priority_area_versions',
        'NRT'=>'reference_type_versions',
        'FS'=>'fund_source_versions',
        'BC'=>'budget_class_versions',
        'IN'=>'intervention_versions',
        'BLD'=>'baseline_statistic_versions'
    ];
    private static $versionTableItemColumId =[
        'PC'=>'plan_chain_id',
        'PI'=>'performance_indicator_id',
        'PA'=>'priority_area_id',
        'NRT'=>'reference_type_id',
        'FS'=>'fund_source_id',
        'BC'=>'budget_class_id',
        'IN'=>'intervention_id',
        'BLD'=>'baseline_statistic_id'
    ];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string',
                'version_type_id' => 'required|integer',
                'date' => 'required|date'
            ],
            $merge);
    }

    public static function getNextFinancialYearVersion($versionType, $currentFinancialYearId, $itemId){
         $currentFinancialYear = FinancialYear::find($currentFinancialYearId);
        $nextFinancialYear = FinancialYear::where('start_date','>', $currentFinancialYear->end_date)->orderBy('start_date')->first();
        $nextVersion =[];
        if($nextFinancialYear == null){
            return $nextVersion;
        }
        $nextVersion['financialYear'] = $nextFinancialYear->name;
        $version = DB::table('financial_year_versions as fv')
            ->join('versions as v','v.id','fv.version_id')
            ->join('version_types as vt','vt.id','v.version_type_id')
            ->where('vt.code',$versionType)
            ->where('fv.financial_year_id',$nextFinancialYear->id)
            ->select('v.*')
            ->first();
        if($version == null){
            return $nextVersion;
        }

        $exist = DB::table(Version::$versionTables[$versionType])->where('version_id',$version->id)
                     ->where(Version::$versionTableItemColumId[$versionType],$itemId)->first();
        $nextVersion['version'] = $version->name;
        $nextVersion['id'] = $version->id;
        $nextVersion['itemVersionId'] = isset($exist)?$exist->id:null;

        return $nextVersion;
    }

    public static function addToVersion($versionType, $itemId, $versionId){
        $existing = DB::table(Version::$versionTables[$versionType])->where(Version::$versionTableItemColumId[$versionType],$itemId)->where('version_id',$versionId)->first();
        if(!is_null($existing)){
            return ['itemId'=>$existing->id];
        }
        DB::table(Version::$versionTables[$versionType])->insert([Version::$versionTableItemColumId[$versionType]=>$itemId,'version_id'=>$versionId]);
        $version = DB::table(Version::$versionTables[$versionType])->where(Version::$versionTableItemColumId[$versionType],$itemId)->where('version_id',$versionId)->first();

        return ['itemVersionId'=>$version->id];
    }

    public function type(){
        return $this->belongsTo("App\Models\Setup\VersionType","version_type_id",'id');
    }

    public function financial_year_versions(){
        return $this->hasMany("App\Models\Setup\FinancialYearVersion","version_id",'id');
    }

    public static function getCurrentVersionId($type){

        $version = DB::table('versions as v')
                       ->join('version_types as vt', 'vt.id', 'v.version_type_id')
                       ->where('vt.code', $type)
                       ->where('v.is_current', true)
                       ->whereNull('v.deleted_at')
                       ->select('v.id')
                       ->first();
        if($version == null){
            return 0;
        }
        return $version->id;
    }
    public static function getVersionByFinancialYear($financialYearId,$type){

        $version = DB::table('versions as v')
                       ->join('version_types as vt', 'vt.id', 'v.version_type_id')
                       ->join('financial_year_versions as fv', 'fv.version_id', 'v.id')
                       ->where('vt.code', $type)
                       ->where('fv.financial_year_id', $financialYearId)
                       ->whereNull('v.deleted_at')
                       ->select('v.id')
                       ->first();
        if($version == null){
            return 0;
        }
        return $version->id;
    }

    public static function getVesionItemIds($versionId,$type){
        try{
            $ids = DB::table(Version::$versionTables[$type])
            ->where('version_id',$versionId)
            ->pluck(Version::$versionTableItemColumId[$type]);
            return $ids;
        }
        catch(\Exception $e){
            Log::error($e);
            return [];
        }
    }
}
