<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class FeatureNotification extends Model
{
    protected $table = "feature_notifications";
    public $timestamps = false;
    protected $fillable = ['name','url','expiration_date'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string',
                'url' => 'required|string',
                'expiration_date' => 'date',
            ],
            $merge);
    }

}
