<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BdcMainGroup extends Model
{
    public $timestamps=true;
    protected $table = 'bdc_main_groups';
    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";

    protected $fillable = ['name'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string|max:255'
            ],
            $merge);
    }

    public function sector(){
        return $this->belongsTo('App\Models\Setup\Sector','sector_id','id');
    }
}
