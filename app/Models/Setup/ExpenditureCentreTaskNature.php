<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpenditureCentreTaskNature extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "bdc_expenditure_centre_task_natures";

    protected $fillable = [
        'task_nature_id',
        'expenditure_centre_id'
    ];


    public function task_nature(){
        return $this->belongsTo('App\Models\Setup\TaskNature','task_nature_id','id');
    }

    public function expenditure_centre(){
        return $this->belongsTo('App\Models\Setup\ExpenditureCentre','expenditure_centre_id','id');
    }
}
