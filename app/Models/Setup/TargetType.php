<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class TargetType extends Model {

    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'target_types';

    public function longTermTargets() {
        return $this->hasMany('App\Models\Planning\LongTermTarget');
    }
}
