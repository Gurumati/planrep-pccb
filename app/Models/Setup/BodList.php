<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BodList extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'bod_lists';

    protected $fillable = ['name', 'sort_order'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string|max:255',
                'sort_order' => 'required|integer',
            ],
            $merge);
    }

    public function sector(){
        return $this->belongsTo('App\Models\Setup\Sector');
    }

}
