<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class ActivitySurplusCategory extends Model
{
    protected $fillable = ['activity_id','surplus_id'];
}
