<?php

namespace App\Models\Setup;

use App\Http\Controllers\Flatten;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class GeoLocation extends Model
{


    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'geographical_locations';
    protected $guarded = [];
    protected $with = [
        'geo_location_level', 'parent'
    ];


    public static function getGeoLocationTree($geoLocationId)
    {

        $userLocationLevelPosition = DB::table('geographical_location_levels as gl')->join('geographical_locations as g', 'gl.id', 'g.geo_location_level_id')->where('g.id', $geoLocationId)->first()->geo_level_position;
        $withString = array();
        if ($userLocationLevelPosition == 1) {
            $withString = array();
            array_push($withString, 'children');
            array_push($withString, 'children.children');
        } else if ($userLocationLevelPosition == 2) {
            $withString = array();
            array_push($withString, 'children');
        }

        $geoLocation = GeoLocation::with($withString)->where('id', $geoLocationId)->select('id', 'name', 'parent_id', 'geo_location_level_id')->get();

        return $geoLocation;
    }

    public static function getLocationIdsWithChildren($geoLocationId)
    {
        return Cache::rememberForever(
            'Geographical locations ids with children' . $geoLocationId,
            function () use ($geoLocationId) {
                $flatten = new Flatten();
                $geoLocation = $flatten->getLocationTreeById($geoLocationId);
                return $flatten->flattenGeoLocationWithChild($geoLocation);
            }
        );
    }

    public function geo_location_level()
    {
        return $this->belongsTo('App\Models\Setup\GeoLocationLevel', 'geo_location_level_id', 'id')
            ->select('id', 'name', 'geo_level_position');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Setup\GeoLocation', 'parent_id', 'id')->select('id', 'name');
    }

    public function users()
    {
        return $this->hasMany('App\Models\Auth\User');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Setup\GeoLocation', 'parent_id');
    }

    public function parentArray()
    {
        return $this->hasMany('App\Models\Setup\GeoLocation', 'id', 'parent_id');
    }
}
