<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SurplusCategory extends Model
{
    use SoftDeletes;
    protected $fillable = ['name','code','is_active','created_by','updated_by','deleted_by'];
}
