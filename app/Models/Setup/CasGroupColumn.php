<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CasGroupColumn extends Model
{
    protected $table = "cas_group_columns";
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function groupTypes(){
        return $this->belongsTo('App\Models\Setup\CasGroupType');
    }
}
