<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Reference extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'reference_docs';

    protected $fillable = ['code', 'name', 'is_active', 'description', 'reference_type_id', 'parent_id', 'link_level'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'name' => 'required|string',
                'code' => 'required|string',
                'is_active' => 'boolean',
                'description' => 'required|string',
                'reference_type_id' => 'required|integer',
                'parent_id' => 'nullable',
                'link_level' => 'required|in:ACTIVITY,TARGET'
            ],
            $merge);
    }

    protected $with = ['children'];

    public function children()
    {
        return $this->hasMany('App\Models\Setup\Reference', 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Setup\Reference', 'parent_id');
    }

    public function referenceType()
    {
        return $this->belongsTo('App\Models\Setup\ReferenceType', 'reference_type_id', 'id');
    }

    public function reference_type()
    {
        return $this->belongsTo('App\Models\Setup\ReferenceType', 'reference_type_id', 'id');
    }
}
