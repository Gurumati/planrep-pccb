<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssessorAssignment extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'assessor_assignments';

    public function user() {
        return $this->belongsTo('App\Models\Auth\User','user_id','id');
    }

    public function admin_hierarchy() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchy','admin_hierarchy_id','id');
    }

    public function period() {
        return $this->belongsTo('App\Models\Setup\Period','period_id','id');
    }

    public function cas_assessment_round() {
        return $this->belongsTo('App\Models\Assessment\CasAssessmentRound','cas_assessment_round_id','id');
    }

    public function admin_hierarchy_level() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchyLevel','admin_hierarchy_level_id','id');
    }

    public function cas_assessment_category_version() {
        return $this->belongsTo('App\Models\Assessment\CasAssessmentCategoryVersion','cas_assessment_category_version_id','id');
    }
}
