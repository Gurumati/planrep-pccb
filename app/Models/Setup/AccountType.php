<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;


class AccountType extends Model
{
    public $timestamps=true;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";

    protected $fillable = ['name','code','description','balance_type'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string|max:64',
                'code' => 'required|unique:account_types,code' . ($id ? ",$id" : ''),
                'description' => 'max:255',
                'balance_type' => 'min:2|max:2',
            ],
            $merge);
    }

    public function gfs_codes(){
        return $this->hasMany('App\Models\Setup\GfsCode');
    }
}
