<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Services\UserServices;
use Illuminate\Support\Facades\Log;

class PriorityArea extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "priority_areas";
    protected $fillable = ['description','number','is_active','link_level'];

    public static function getPriorityWithInterventionAndProblems($linkLevel,$planChainId,$financialYearId, $adminHierarchyId,$sectionId) {
        
        $sectorId = Section::where('id',$sectionId)->first()->sector_id;
        $versionId = Version::getVersionByFinancialYear($financialYearId,'PA');
        $currentVersionItemIds = Version::getVesionItemIds($versionId,'PA');

        $interVersionId = Version::getVersionByFinancialYear($financialYearId,'IN');
        $interVersionItemIds = Version::getVesionItemIds($interVersionId,'IN');
        
        $priority = PriorityArea::with(['interventions'=>function($filter) use( $interVersionItemIds){
                        $filter->whereIn('id',  $interVersionItemIds);
                    },'sector_problems'=>function($query) use($adminHierarchyId){
                        $query->where('admin_hierarchy_id', $adminHierarchyId);
                    }])
                    ->where(function($filter) use($sectorId,$planChainId){
                        $filter->whereHas('sectors', function($s) use($sectorId){
                            $s->where('sector_id',$sectorId);
                        });
                        $filter->orWhereHas('plan_chains', function($s) use($planChainId){
                            $s->where('plan_chain_id',$planChainId);
                        });
                    })
                    ->whereIn('id',$currentVersionItemIds)
                    ->where('is_active',true)
                    ->where('link_level',$linkLevel)->get();
        return $priority;
    }

    public static function getForCurrentVersion() {
       
        $versionId = Version::getCurrentVersionId('PA');
        $currentVersionItemIds = Version::getVesionItemIds($versionId,'PA');
        $section_id = UserServices::getUser()->section_id;
        $section = Section::find($section_id);
        $sector_id = $section->sector_id;
        
        $priority = PriorityArea::whereIn('id',$currentVersionItemIds)
                    ->where('is_active',true)
                    ->whereHas('sectors', function ($query) use ($sector_id) {
                        if(isset($section_id))
                        $query->where('sector_id', $sector_id);
                    })
                    ->get();
        return $priority;
    }

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'description' => 'required|string',
                'number' => 'required|string',
                'is_active' => 'boolean',
                'link_level' => 'required|integer',
            ],
            $merge);
    }
    public function sectors() {
        return $this->belongsToMany('App\Models\Setup\Sector','priority_area_sectors');
    }
    public function plan_chains() {
        return $this->belongsToMany('App\Models\Setup\PlanChain','priority_area_plan_chains');
    }

    public function interventions() {
        return $this->hasMany('App\Models\Setup\Intervention');
    }

    public function sector_problems() {
        return $this->hasMany('App\Models\Setup\SectorProblem');
    }

    public function link_level() {
        return $this->belongsTo('App\Models\Setup\LinkLevel','link_level','id');
    }

    public function priority_area_versions()
    {
        return $this->hasMany('App\Models\Setup\PriorityAreaVersion', 'priority_area_id', 'id');
    }
}
