<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class BankAccountVersion extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "bank_account_versions";

    protected $fillable = ['bank_account_id', 'version_id'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'bank_account_id' => 'required|string',
                'version_id' => 'required|integer|unique_with:bank_account_versions,bank_account_id' . ($id ? ",$id" : ''),
            ],
            $merge);
    }

    public function version()
    {
        return $this->belongsTo('App\Models\Setup\Version','version_id','id');
    }

    public function bank_account()
    {
        return $this->belongsTo('App\Models\Setup\BankAccount','bank_account_id','id');
    }
}
