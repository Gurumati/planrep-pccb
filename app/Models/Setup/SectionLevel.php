<?php

namespace App\Models\Setup;

use App\Http\Services\UserServices;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;


class SectionLevel extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function sections() {
        return $this->hasMany('App\Models\Setup\Section');
    }

    /*protected $with = ['parentLevels'];

    public function parentLevels(){
        return $this->belongsTo('App\Models\Setup\SectionLevel','parent_id');
    }

    public function childSectionLevels(){
        return $this->hasMany('App\Models\Setup\SectionLevel');
    }*/

    public function nextBudgetLevel() {
        return $this->hasMany('App\Models\Setup\SectionLevel','next_budget_level');
    }

    public function nextBudget() {
        return $this->belongsTo('App\Models\Setup\SectionLevel', 'next_budget_level');
    }

    public function mtef_sections() {
        return $this->hasMany('App\Models\Planning\MtefSection');
    }
    public static function getUserSectionLevelAndChildren(){
        $userPosition= DB::table('sections as s')->
                           join('section_levels as sl','sl.id','s.section_level_id')->
                           where('s.id',UserServices::getUser()->section_id)->first();

        $all = DB::table('section_levels')->
                   where('hierarchy_position', '>=',$userPosition->hierarchy_position)->
                   where('hierarchy_position', '<=',4)->
                   distinct()->
                   orderBy('hierarchy_position','asc')->
                   get();
        return $all;
    }

    public static function getBySectionId($sectionId){
        return DB::table('section_levels as l')->
                   join('sections as s','l.id','s.section_level_id')->
                   where('s.id',$sectionId)->
                   select('l.*')->first();
    }

    public static function getByPosition($position){
        return DB::table('section_levels')->where('hierarchy_position', $position)->first();
    }
}
