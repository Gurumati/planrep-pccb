<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sector extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'sectors';

    public function sections() {
        return $this->hasMany('App\Models\Setup\Sections');
    }

    public function plan_chain_sectors() {
        return $this->hasMany('App\Models\Setup\PlanChainSector');
    }

    public function priority_areas() {
        return $this->hasMany('App\Models\Setup\PriorityArea');
    }

    public function sector_projects() {
        return $this->hasMany('App\Models\Setup\SectorProject');
    }

    public function sector_targets(){
        return $this->hasMany('App\Models\Planning\SectorTarget');
    }

    public function ceiling_sectors() {
        return $this->hasMany('App\Models\Budget\CeilingSector');
    }

    public function casPlan(){
        return $this->hasMany('App\Models\Planning\CasPlan');
    }

    public function projectDataForms(){
        return $this->hasMany('App\Models\Setup\ProjectDataForm');
    }
}
