<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanChainParent extends Model {
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'plan_chains';


    protected $with = [
        'parent'
    ];

    public function  parent()
    {
        return $this->hasMany('App\Models\Setup\PlanChainParent','id','parent_id');
    }

}
