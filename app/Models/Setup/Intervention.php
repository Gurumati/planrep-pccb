<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Intervention extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'interventions';
    protected $fillable = ['description','intervention_category_id','priority_area_id','number','is_primary','is_active','link_level_id'];


    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'description' => 'required|string',
                'intervention_category_id' => 'required|integer',
                'priority_area_id' => 'required|integer',
                'number' => 'required|string',
                'is_primary' => 'boolean',
                'is_active' => 'boolean',
                'link_level_id' => 'required|integer',
            ],
            $merge);
    }

    public function intervention_category() {
        return $this->belongsTo('App\Models\Setup\InterventionCategory');
    }

    public function priority_area() {
        return $this->belongsTo('App\Models\Setup\PriorityArea');
    }

    public function link_level() {
        return $this->belongsTo('App\Models\Setup\LinkLevel');
    }

    public function intervention_versions()
    {
        return $this->hasMany('App\Models\Setup\InterventionVersion', 'intervention_id', 'id');
    }
}
