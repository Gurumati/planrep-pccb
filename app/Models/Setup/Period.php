<?php

namespace App\Models\Setup;

use App\Http\Services\FinancialYearServices;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;


class Period extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public static function getByActivity($activityId){
        return DB::table('periods as p')
        ->join('activity_periods as ap','p.id','ap.period_id')
        ->where('ap.activity_id',$activityId)
        ->pluck('p.id');
    }
    public static function planningPeriods()
    {

        $financial_year = FinancialYearServices::getPlanningFinancialYear();
        if ($financial_year == null) {
            $financial_year_execution = FinancialYearServices::getExecutionFinancialYear();
            if($financial_year_execution == null){
                return [];
            }else{
                return Period::byYear($financial_year_execution->id);     

            }
            return [];
        }
        return Period::byYear($financial_year->id);
    }

    public static function byYear($financialYearId)
    {
        $configuration = ConfigurationSetting::where('key', 'PLANNING_PERIODS_GROUPS')->first();
        $periodGroup = null;
        if ($configuration != null) {
            $periodGroup = PeriodGroup::find(intval($configuration->value));
            if ($periodGroup == null) {
                $periodGroup = PeriodGroup::where('number', 3)->first();
            }
        } else {
            $periodGroup = PeriodGroup::where('number', 3)->first();
        }
        return Period::where('financial_year_id', $financialYearId)
            ->where('period_group_id', $periodGroup->id)
            ->orderBy('start_date', 'asc')
            ->get();
    }

    public function financial_year() {
        return $this->belongsTo('App\Models\Setup\FinancialYear');
    }
    public function period_group() {
        return $this->belongsTo('App\Models\Setup\PeriodGroup');
    }

    public function gfs_code_projections() {
        return $this->hasMany('App\Models\Planning\GfsCodeProjection');
    }

    public function admin_hierarchy_ceiling_periods() {
        return $this->hasMany('App\Models\Budgeting\AdminHierarchyCeilingPeriod');
    }

    public function assessment_results() {
        return $this->hasMany('App\Models\Assessment\CasAssessmentResult');
    }

}
