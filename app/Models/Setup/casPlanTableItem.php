<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class casPlanTableItem extends Model
{
    public $timestamps = true;
    use SoftDeletes;

    protected $table = "cas_plan_table_items";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $with  = ['children'];
    public function casPlanTableItemValues() {
        return $this->hasMany('App\Models\Setup\CasPlanTableItemValue');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Setup\casPlanTableItem','cas_plan_table_item_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Setup\casPlanTableItem');
    }

    public function casPlanTableItemAdminHierarchies()
    {
        return $this->hasMany('App\Models\Planning\CasPlanTableItemAdminHierarchy');
    }
}
