<?php

namespace App\Models\Setup;

use App\Http\Services\FinancialYearServices;
use App\Http\Services\UserServices;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Calendar extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "calendars";


    public function calendar_event(){
        return $this->belongsTo('App\Models\Setup\CalendarEvent','calendar_event_id','id')->
                      select('id','name','expected_value_query', 'actual_value_query','url');
    }

    public function financial_year(){
        return $this->belongsTo('App\Models\Setup\FinancialYear','financial_year_id','id');
    }

    public function hierarchy_level(){
        return $this->belongsTo('App\Models\Setup\AdminHierarchyLevel','hierarchy_position','hierarchy_position');
    }

    public function sector(){
        return $this->belongsTo('App\Models\Setup\Sector','sector_id','id');
    }

    public static function getUserTodos($perPage){
        $user = UserServices::getUser();
        $p = FinancialYearServices::getPlanningFinancialYear();
        $e = FinancialYearServices::getExecutionFinancialYear();
        $pId = $eId = 0;
        $financialYears = [];
        if(isset($p->id)) {
            $pId = $p->id;
            array_push($financialYears,$p->id);
        }
        if(isset($e->id)) {
            $eId = $e->id;
            array_push($financialYears,$e->id);
        }
        $params = ['#{adminHierarchyId}', '#{planningFinancialYearId}', '#{executionFinancialYearId}', '#{sectionId}'];
        $paramValues= [$user->admin_hierarchy_id,$pId,$eId,$user->section_id];

        $position = DB::table("admin_hierarchy_levels as l")->
                        join('admin_hierarchies as a','a.admin_hierarchy_level_id','l.id')->
                        where('a.id',$user->admin_hierarchy_id)->
                        pluck('l.hierarchy_position');

       $section = Section::where("id",$user->section_id)->first();
       $sectionLevelId = $section->section_level_id;
       $sectorId = isset($section->sector_id)?isset($section->sector_id):0;


        $calenders = Calendar::with("calendar_event")->
                               whereIn("hierarchy_position",$position)->
                               where(function ($q) use($sectionLevelId,$sectorId){
                                   $q->where(function ($q2) use($sectionLevelId){
                                       $q2->where("section_level_id",$sectionLevelId)->whereNull("sector_id");
                                    })->orWhere(function ($q3) use($sectionLevelId,$sectorId) {
                                       $q3->where("section_level_id",$sectionLevelId)->where("sector_id",$sectorId);
                                    });
                                })->
                               whereIn("financial_year_id",$financialYears)->
                               orderBy("start_date","asc")->
                               select('id','description','start_date','end_date','calendar_event_id')->
                               paginate($perPage);

        foreach ($calenders as &$cal){
            $value = $expected = null;
            if(isset($cal->calendar_event)){
                if(isset($cal->calendar_event->actual_value_query)) {
                    $aQuery = str_ireplace($params, $paramValues, $cal->calendar_event->actual_value_query);
                    try {
                        $value = DB::select($aQuery)[0]->count;
                    } catch (\Exception $e) {
                    }
                    $cal->calendar_event->actual_value_query = null;
                }
                if(isset($cal->calendar_event->expected_value_query)) {
                    $eQuery = str_ireplace($params, $paramValues, $cal->calendar_event->expected_value_query);
                    try {
                        $expected = DB::select($eQuery)[0]->count;
                    } catch (\Exception $e) {
                    }
                    $cal->calendar_event->expected_value_query = null;
                }
                $cal->calendar_event->expected_value_query=null;
            }
            $cal->actual_value = $value;
            $cal->expected_value = $expected;
        }
        return $calenders;

    }
}
