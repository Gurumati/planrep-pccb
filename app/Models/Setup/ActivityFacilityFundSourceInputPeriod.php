<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class ActivityFacilityFundSourceInputPeriod extends Model
{

    protected $fillable = ['activity_facility_fund_source_input_id','period_id','percentage'];

    public static function saveAll($nputId,$periods){
        ActivityFacilityFundSourceInputPeriod::where('activity_facility_fund_source_input_id',$nputId)->forceDelete();
        foreach($periods as $k=>$v){
            ActivityFacilityFundSourceInputPeriod::create([
                'activity_facility_fund_source_input_id'=>$nputId,
                'period_id'=>$k,
                'percentage'=>$v
            ]);
        }
    }
}
