<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class AccountReturn extends Model
{
    public $timestamps=true;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";
    protected $table = "account_returns";

    protected $fillable = ['name','code','section_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string|max:64',
                'code' => 'required|unique:account_returns,code' . ($id ? ",$id" : ''),
                'section_id' => 'required|integer',
            ],
            $merge);
    }

    public function section() {
        return $this->belongsTo('App\DTOs\SetupDTOs\SectionDTO','section_id','id');
    }
}
