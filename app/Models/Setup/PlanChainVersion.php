<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class PlanChainVersion extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'plan_chain_versions';
    protected $fillable = ['plan_chain_id','version_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'plan_chain_id' => 'required|integer',
                'version_id' => 'required|integer|unique_with:plan_chain_versions,plan_chain_id',
            ],
            $merge);
    }

    public function plan_chain() {
        return $this->belongsTo('App\Models\Setup\PlanChain','plan_chain_id','id');
    }

    public function version() {
        return $this->belongsTo('App\Models\Setup\Version','version_id','id');
    }
}
