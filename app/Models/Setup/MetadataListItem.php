<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class MetadataListItem extends Model
{
    public $timestamps = true;
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "metadata_list_items";
    protected $fillable = ['name','period','metadata_type_id','code','sql_query','uid','parent_id','orgUnit'];
    protected $guarded = ["id"];
    protected $dates = ['deleted_at'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string',
                'period' => 'string|nullable',
                'metadata_type_id' => 'required|integer',
                'sql_query' => 'required|string',
                'uid' => 'required|string',
                'parent_id' => 'string|nullable',
                'orgUnit' => 'string|nullable'
            ],
            $merge);
    }

    public function type(){
        return $this->belongsTo("App\Models\Setup\MetadataType","metadata_type_id","id");
    }

    public function parent(){
        return $this->belongsTo("App\Models\Setup\MetadataListItem","uid","uid");
    }
}
