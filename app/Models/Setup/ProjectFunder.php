<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectFunder extends Model
{
    //
    public $timestamps=true;
    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";

    public function funder()
    {
        return $this->belongsTo('App\Models\Setup\Funder');
    }
    public function project()
    {
        return $this->belongsTo('App\Models\Setup\Project');
    }
}
