<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class ExpenditureCategory extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "expenditure_categories";
    protected $fillable = ['name','activity_task_nature_id','project_type_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string',
                'activity_task_nature_id' => 'required|integer',
                'project_type_id' => 'required|integer'
            ],
            $merge);
    }

    public function activity_task_nature(){
        return $this->belongsTo('App\Models\Setup\ActivityTaskNature','activity_task_nature_id','id');
    }

    public function project_type(){
        return $this->belongsTo('App\Models\Setup\ProjectType','project_type_id','id');
    }
}
