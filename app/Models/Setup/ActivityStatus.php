<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityStatus extends Model
{
    public $timestamps=true;
    protected $table = 'activity_statuses';
    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";
}
