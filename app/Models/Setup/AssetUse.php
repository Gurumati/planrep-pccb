<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssetUse extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'asset_uses';

    protected $fillable = ['name', 'code'];

    /**
     * @param int $id
     * @param array $merge
     * @return array
     */
    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'name' => 'required|string|max:255',
                'code' => 'required|unique:asset_uses,code' . ($id ? ",$id" : ''),
            ],
            $merge);
    }
}
