<?php

namespace App\Models\Setup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CasPlan extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'cas_plans';

    protected $with = ['sector'];

    public function casPlan() {
        return $this->hasMany('App\Models\Setup\CasPlanContent');
    }

    public function parent() {
        return $this->belongsTo('App\Models\Setup\CasPlan');
    }

    public function children() {
        return $this->hasMany('App\Models\Setup\CasPlan','parent_id','id');
    }

    public function sector(){
        return $this->belongsTo('App\Models\Setup\Sector');
    }

    public function administrativeLevel(){
        return $this->belongsTo('App\Models\Setup\AdminHierarchyLevel','admin_hierarchy_level_id');
    }

    public function casPlanTables(){
        return $this->hasManyThrough('App\Models\Setup\CasPlanTable','App\Models\Setup\CasPlanContent');
    }

    public function assessmentCategory(){
        return $this->hasMany('App\Models\Assessment\CasAssessmentCategory');
    }

}
