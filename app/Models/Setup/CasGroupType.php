<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CasGroupType extends Model
{
    protected $table = "cas_group_types";
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function groupColumns(){
        return $this->hasMany('App\Models\Setup\CasGroupColumn');
    }

}
