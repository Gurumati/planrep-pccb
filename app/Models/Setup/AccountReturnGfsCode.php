<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class AccountReturnGfsCode extends Model
{
    public $timestamps=true;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";
    protected $table = "account_return_gfs_codes";

    protected $fillable = ['gfs_code_id','account_return_id'];

    public function gfs_code(){
        return $this->belongsTo('App\Models\Setup\GfsCode','gfs_code_id','id');
    }

    public function account_return(){
        return $this->belongsTo('App\Models\Setup\AccountReturn','account_return_id','id');
    }
}
