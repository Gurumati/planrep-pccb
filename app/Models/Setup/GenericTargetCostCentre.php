<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GenericTargetCostCentre extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'generic_target_cost_centres';

    public function generic_target() {
        return $this->belongsTo('App\Models\Setup\GenericTarget','generic_target_id','id');
    }

    public function section() {
        return $this->belongsTo('App\Models\Setup\Section','section_id','id');
    }

    public function cost_centre() {
        return $this->belongsTo('App\Models\Setup\Section','section_id','id');
    }
}
