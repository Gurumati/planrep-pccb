<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class BudgetSubmissionSelectOption extends Model
{
    protected $table = "budget_submission_select_option";

    public function parent()
    {
        return $this->belongsTo('\App\Models\Setup\BudgetSubmissionSelectOption');
    }

    public function children()
    {
        return $this->hasMany('\App\Models\Setup\BudgetSubmissionSelectOption','parent_id');
    }
}
