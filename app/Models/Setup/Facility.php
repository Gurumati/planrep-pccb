<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Services\UserServices;
use App\Models\Auth\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Cache;
use App\Models\CacheKeys;

class Facility extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "facilities";
    //protected $with = [ownership];

    protected $fillable = [
        'name',
        'facility_code',
        'description',
        'facility_type_id',
        'admin_hierarchy_id',
        'is_active',
        'longitude',
        'latitude',
        'gnl',
        'catchment_population',
        'facility_ownership_id',
        'population_within_area',
        'nearest_similar_facility_id',
        'nearest_similar_facility_distance',
        'furthest_ward_served_id',
        'furthest_ward_served_distance',
        'furthest_village_served_id',
        'furthest_village_served_distance',
        'has_title_deed',
        'physical_state_id',
        'star_rating_id',
        'has_custom_details',
        'operation_year',
        'exported_to_ffars',
        'ffars_response',
        'is_delivered_to_ffars',
        'postal_address',
        'email',
        'phone_number',
        'number_of_villages_served',
        'facility_village_id',
        'geo_location_id',
    ];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'name' => 'required|string|max:255',
                'facility_code' => 'required|min:8|max:8|unique:facilities,facility_code' . ($id ? ",$id" : ''),
                'description' => 'string|max:500|nullable',
                'facility_type_id' => 'required|integer',
                'admin_hierarchy_id' => 'required|integer',
                'geo_location_id' => 'required|integer',
                'is_active' => 'boolean|nullable',
                'longitude' => 'numeric|nullable',
                'latitude' => 'numeric|nullable',
                'gnl' => 'numeric|nullable',
                'catchment_population' => 'integer|nullable',
                'facility_ownership_id' => 'integer|nullable',
                'population_within_area' => 'integer|nullable',
                'nearest_similar_facility_id' => 'integer|nullable',
                'nearest_similar_facility_distance' => 'integer|nullable',
                'furthest_ward_served_id' => 'integer|nullable',
                'furthest_ward_served_distance' => 'integer|nullable',
                'furthest_village_served_id' => 'integer|nullable',
                'furthest_village_served_distance' => 'integer|nullable',
                'has_title_deed' => 'boolean|nullable',
                'physical_state_id' => 'integer|nullable',
                'star_rating_id' => 'integer|nullable',
                'has_custom_details' => 'boolean|nullable',
                'operation_year' => 'integer|nullable',
                'exported_to_ffars' => 'boolean|nullable',
                'ffars_response' => 'string|nullable',
                'is_delivered_to_ffars' => 'boolean|nullable',
                'postal_address' => 'string|nullable',
                'email' => 'email|nullable',
                'phone_number' => 'string|nullable',
                'number_of_villages_served' => 'integer|nullable',
                'facility_village_id' => 'integer|nullable',
            ],
            $merge
        );
    }

    protected $with = [
        'facility_type'
    ];

    public static function getByUser($userId)
    {
        $f = DB::table('facilities as f')
            ->join('user_facilities as uf', 'uf.facility_id', 'f.id')
            ->join('facility_types as ft', 'ft.id', 'f.facility_type_id')
            ->where('uf.user_id', $userId)
            ->where('f.is_active', true)
            ->where('uf.is_home', false)
            ->select('f.id', 'f.name', 'ft.name as type')
            ->paginate(10);
        return $f;
    }

    public static function getAllByAdminAreaAndSection($adminHierarchyId, $sectionId)
    {
        $userIds = User::where('admin_hierarchy_id', $adminHierarchyId)
            ->where('section_id', $sectionId)->pluck('id');

        $f = DB::table('user_facilities')
            ->whereIn('user_id', $userIds)
            ->pluck('facility_id')->toArray();

        return $f;
    }


    public function bank_accounts()
    {
        return $this->hasMany("App\Models\Setup\FacilityBankAccount", "facility_id", "id");
    }

    public function facility_type()
    {
        return $this->belongsTo('App\Models\Setup\FacilityType')->select('id', 'name', 'code');
    }
    public function geolocations()
    {
        return $this->belongsTo('App\DTOs\SetupDTOs\geoLocationDTO','geo_location_id','id');
    }

    // public function geolocations()
    // {
    //     return $this->belongsTo(GeoLocation::class,'geo_location_id');
    // }

    public function admin_hierarchy()
    {
        return $this->belongsTo('App\DTOs\SetupDTOs\AdminHierarchyDTO', 'admin_hierarchy_id', 'id');
    }

    public function ward()
    {
        return $this->belongsTo('App\DTOs\SetupDTOs\AdminHierarchyDTO', 'admin_hierarchy_id', 'id');
    }

    public function village()
    {
        return $this->belongsTo('App\DTOs\SetupDTOs\AdminHierarchyDTO', 'facility_village_id', 'id');
    }

    public function furthest_ward()
    {
        return $this->belongsTo('App\DTOs\SetupDTOs\AdminHierarchyDTO', 'furthest_ward_served_id', 'id');
    }

    public function furthest_village()
    {
        return $this->belongsTo('App\DTOs\SetupDTOs\AdminHierarchyDTO', 'furthest_village_served_id', 'id');
    }

    public function ownership()
    {
        return $this->belongsTo('App\Models\Setup\FacilityOwnership', 'facility_ownership_id', 'id');
    }

    public function nearest_similar_facility()
    {
        return $this->belongsTo('App\Models\Setup\Facility', 'nearest_similar_facility_id', 'id');
    }

    public function furthest_ward_served()
    {
        return $this->belongsTo('App\Models\Setup\AdminHierarchy', 'furthest_ward_served_id', 'id');
    }

    public function furthest_village_served()
    {
        return $this->belongsTo('App\Models\Setup\AdminHierarchy', 'furthest_village_served_id', 'id');
    }

    public function physical_state()
    {
        return $this->belongsTo('App\Models\Setup\FacilityPhysicalState', 'physical_state_id', 'id');
    }

    public function star_rating()
    {
        return $this->belongsTo('App\Models\Setup\FacilityStarRating', 'star_rating_id', 'id');
    }

    public function facility_custom_details()
    {
        return $this->hasMany('App\Models\Setup\FacilityCustomDetailValue', 'facility_id', 'id');
    }

    public static function getHomeFacilityAndSuperviedFacilityTypes($adminHierarchyId, $sectionId)
    {

        $user = UserServices::getUser();
        $homeFacility = null;
        if ($user->is_facility_user) {
            $homeFacility = DB::table('facilities as f')
                ->join('user_facilities as uf', 'uf.facility_id', 'f.id')
                ->join('facility_types as ft', 'ft.id', 'f.facility_type_id')
                ->where('uf.user_id', $user->id)
                ->where('uf.is_home', true)
                ->where('f.is_active', true)
                ->select('f.id', 'f.name', 'ft.name as type')
                ->first();
            $homeFacility->isFacilityAccount = true;

            $supervisedFacilityTypes = [];
        } else {

            $facilityTypeIds = DB::table('facility_type_sections')->where('section_id', $sectionId)->pluck('facility_type_id')->toArray();

            $hqFacilityType = ConfigurationSetting::where('key', 'HQ_FACILITY_TYPE')->first();
            $facilityType = DB::table('facility_types')->where('id', (int)$hqFacilityType->value)->first();

            if (in_array($facilityType->id, $facilityTypeIds)) {
                $homeFacility = DB::table('facilities as f')
                    ->join('facility_types as ft', 'ft.id', 'f.facility_type_id')
                    ->where('f.admin_hierarchy_id', $adminHierarchyId)
                    ->where('ft.id', $facilityType->id)
                    ->where('f.is_active', true)
                    ->select('f.id', 'f.name', 'ft.name as type')
                    ->first();
                $key = array_search($facilityType->id, $facilityTypeIds);
                unset($facilityTypeIds[$key]);
                $homeFacility->isFacilityAccount = false;
            }

            $supervisedFacilityTypes = DB::table('facility_types')->whereIn('id', $facilityTypeIds)->select('id', 'name')->get();

        }

        return ['homeFacility' => $homeFacility, 'supervisedFacilityTypes' => $supervisedFacilityTypes];
    }

    public static function getAllByPlanningSection($mtefSectionId, $isFacilityAccount, $idsToExcludeString)
    {

        $mtefSection = DB::table('mtef_sections')->where('id', $mtefSectionId)->first();

        $adminHierarchyId = DB::table('mtefs')->find($mtefSection->mtef_id)->admin_hierarchy_id;

        $userAdminLevel = DB::table('admin_hierarchy_levels as al')
            ->join('admin_hierarchies as a', 'al.id', 'a.admin_hierarchy_level_id')
            ->where('a.id', $adminHierarchyId)
            ->select('al.*')
            ->first();

        $publicFacility = ConfigurationSetting::where('key', 'PUBLIC_FACILITY_OWNERSHIP')->first();
        $publicFacilityOwnershiId = (isset($publicFacility) && $publicFacility->value != '' && $publicFacility->value != null) ? (int)$publicFacility->value : 0;

        $facilityTypeIdsString = '';
        $facilityTypeIds = [];

        $facilityTypeIds = DB::table('facility_type_sections')
            ->where('section_id', $mtefSection->section_id)
            ->pluck('facility_type_id');

        foreach ($facilityTypeIds as $id) {
            $facilityTypeIdsString = $facilityTypeIdsString . $id . ',';
        }
        $facilityTypeIdsString = trim($facilityTypeIdsString, ',');

        if ($facilityTypeIdsString !== '') {

            $facilityTypes = FacilityType::whereIn('id', $facilityTypeIds)->get();

            $userIsParent = '';

            foreach ($facilityTypes as $facilityType) {

                $facilityAdminLevel = DB::table('admin_hierarchy_levels')
                    ->where('id', $facilityType->admin_hierarchy_level_id)
                    ->first();

                $dif = $facilityAdminLevel->hierarchy_position - $userAdminLevel->hierarchy_position;
                $userIsParent = $userIsParent . "p" . $dif . ".id =" . $adminHierarchyId . " or ";
            }
            $userIsParent = trim($userIsParent, "or ");

            $string = "Select f.id,f.name,ft.name as type,p0.id as admin_hierarchy_id
        FROM facilities as f
        JOIN facility_types as ft on ft.id = f.facility_type_id
        JOIN admin_hierarchies as p0 on p0.id = f.admin_hierarchy_id
        LEFT JOIN admin_hierarchies as p1 on p1.id = p0.parent_id
        LEFT JOIN admin_hierarchies as p2 on p2.id = p1.parent_id
        LEFT JOIN admin_hierarchies as p3 on p3.id = p2.parent_id
        LEFT JOIN admin_hierarchies as p4 on p4.id = p3.parent_id
        where f.is_active = true
        and f.facility_ownership_id  =" . $publicFacilityOwnershiId . "
        and f.facility_type_id IN ( " . $facilityTypeIdsString . ")
        and (" . $userIsParent . ")
        and f.id not in " . $idsToExcludeString;

            $facilities = DB::select($string);
        } else {
            $facilities = [];
        }

        return $facilities;

    }

    public static function getAllFacilityIdsStringByAdminAndSection($adminHierarchyId, $sectionId)
    {

        $userAdminLevel = DB::table('admin_hierarchy_levels as al')
            ->join('admin_hierarchies as a', 'al.id', 'a.admin_hierarchy_level_id')
            ->where('a.id', $adminHierarchyId)->select('al.*')
            ->first();

        $publicFacility = ConfigurationSetting::where('key', 'PUBLIC_FACILITY_OWNERSHIP')->first();
        $publicFacilityOwnershiId = (isset($publicFacility) && $publicFacility->value != '' && $publicFacility->value != null) ? (int)$publicFacility->value : 0;

        $facilityTypeIdsString = '';
        $facilityTypeIds = [];

        $facilityTypeIds = DB::table('facility_type_sections')
            ->where('section_id', $sectionId)
            ->pluck('facility_type_id');

        foreach ($facilityTypeIds as $id) {
            $facilityTypeIdsString = $facilityTypeIdsString . $id . ',';
        }
        $facilityTypeIdsString = trim($facilityTypeIdsString, ',');

        if ($facilityTypeIdsString !== '') {

            $facilityTypes = FacilityType::whereIn('id', $facilityTypeIds)->get();

            $userIsParent = '';
            foreach ($facilityTypes as $facilityType) {
                $facilityAdminLevel = DB::table('admin_hierarchy_levels')->where('id', $facilityType->admin_hierarchy_level_id)->first();
                $dif = $facilityAdminLevel->hierarchy_position - $userAdminLevel->hierarchy_position;
                $userIsParent = $userIsParent . "p" . $dif . ".id =" . $adminHierarchyId . " or ";
            }
            $userIsParent = trim($userIsParent, "or ");

            $string = "Select f.id,f.name,ft.name as type,p0.id as admin_hierarchy_id
                    FROM facilities as f
                    JOIN facility_types as ft on ft.id = f.facility_type_id
                    JOIN admin_hierarchies as p0 on p0.id = f.admin_hierarchy_id
                    LEFT JOIN admin_hierarchies as p1 on p1.id = p0.parent_id
                    LEFT JOIN admin_hierarchies as p2 on p2.id = p1.parent_id
                    LEFT JOIN admin_hierarchies as p3 on p3.id = p2.parent_id
                    LEFT JOIN admin_hierarchies as p4 on p4.id = p3.parent_id
                    where f.is_active = true
                    and f.facility_ownership_id  =" . $publicFacilityOwnershiId . "
                    and f.facility_type_id IN ( " . $facilityTypeIdsString . ")
                    and (" . $userIsParent . ")";

            $facilities = DB::select($string);
            $facilityIdString = '0,';
            foreach ($facilities as $fac) {
                $facilityIdString = $facilityIdString . $fac->id . ',';
            }
            $facilityIdString = trim($facilityIdString, ',');
        } else {
            $facilityIdString = '0';
        }
        return $facilityIdString;
    }

    public static function searchFacilityBySectionAndBudegtType($mtefSectionId, $budgetType, $queryString)
    {

        $publicFacility = ConfigurationSetting::where('key', 'PUBLIC_FACILITY_OWNERSHIP')->first();
        $publicFacilityOwnershiId = (isset($publicFacility) && $publicFacility->value != '' && $publicFacility->value != null) ? (int)$publicFacility->value : 0;

        $query = "SELECT f.id,f.name,ft.name as type
                    FROM facilities as f
                    JOIN facility_types as ft on ft.id = f.facility_type_id
                    JOIN activity_facilities as af on af.facility_id = f.id
                    JOIN activities as a on a.id = af.activity_id
                    WHERE a.mtef_section_id=" . $mtefSectionId . " and a.budget_type=" . $budgetType . "
                    and LOWER(f.name) like " . $queryString . " LIMIT 5
                ";
        return DB::select($query);
    }

    public static function searchFacilityBySection($mtefSectionId, $isFacilityAccount, $idsToExcludeString, $queryString)
    {

        $isReallocation = Input::get('realoc');
        $facilityId = Input::get('facilityId');
        if (isset($isReallocation) && $isReallocation == 'true' && isset($facilityId)) {
            return DB::table('facilities as f')
                ->join('facility_types as ft', 'f.facility_type_id', 'ft.id')
                ->where('f.id', $facilityId)
                ->select('f.id', 'f.name', 'ft.name as type')
                ->get();
        }
        $user = UserServices::getUser();
        $count = UserFacility::where('user_id', $user->id)->count();

        if ($count == 0) {

            $facilityTypeIdsString = '';
            $facilityTypeIds = [];

            $mtefSection = DB::table('mtef_sections')
                ->where('id', $mtefSectionId)
                ->first();

            $mtef = DB::table('mtefs')
                ->where('id', $mtefSection->mtef_id)
                ->first();

            $adminHierarchyId = $mtef->admin_hierarchy_id;

            $userAdminLevel = DB::table('admin_hierarchy_levels as al')
                ->join('admin_hierarchies as a', 'al.id', 'a.admin_hierarchy_level_id')
                ->where('a.id', $adminHierarchyId)
                ->select('al.*')
                ->first();

            $publicFacility = ConfigurationSetting::where('key', 'PUBLIC_FACILITY_OWNERSHIP')->first();
            $publicFacilityOwnershiId = (isset($publicFacility) && $publicFacility->value != '' && $publicFacility->value != null) ? (int)$publicFacility->value : 0;

            $facilityTypeIds = DB::table('facility_type_sections')->where('section_id', $mtefSection->section_id)->pluck('facility_type_id');

            foreach ($facilityTypeIds as $id) {
                $facilityTypeIdsString = $facilityTypeIdsString . $id . ',';
            }
            $facilityTypeIdsString = trim($facilityTypeIdsString, ',');

            if ($facilityTypeIdsString !== '') {

                $facilityTypes = FacilityType::whereIn('id', $facilityTypeIds)->get();

                $userIsParent = '';
                foreach ($facilityTypes as $facilityType) {
                    $facilityAdminLevel = DB::table('admin_hierarchy_levels')->where('id', $facilityType->admin_hierarchy_level_id)->first();
                    $dif = $facilityAdminLevel->hierarchy_position - $userAdminLevel->hierarchy_position;
                    $userIsParent = $userIsParent . "p" . $dif . ".id =" . $adminHierarchyId . " or ";
                }
                $userIsParent = trim($userIsParent, "or ");

                $string = "Select f.id,f.name,ft.name as type,p0.id as admin_hierarchy_id
                        FROM facilities as f
                        JOIN facility_types as ft on ft.id = f.facility_type_id
                        JOIN admin_hierarchies as p0 on p0.id = f.admin_hierarchy_id
                        LEFT JOIN admin_hierarchies as p1 on p1.id = p0.parent_id
                        LEFT JOIN admin_hierarchies as p2 on p2.id = p1.parent_id
                        LEFT JOIN admin_hierarchies as p3 on p3.id = p2.parent_id
                        LEFT JOIN admin_hierarchies as p4 on p4.id = p3.parent_id
                        where f.is_active = true
                        and f.facility_ownership_id  =" . $publicFacilityOwnershiId . "
                        and f.facility_type_id IN ( " . $facilityTypeIdsString . ")
                        and (" . $userIsParent . ")
                        and f.id not in " . $idsToExcludeString .
                    " and LOWER(f.name) like " . $queryString . " LIMIT 5";

                $facilities = DB::select($string);
            } else {
                $facilities = [];
            }

        } else {

            $string = "Select f.id,f.name,ft.name as type,p0.id as admin_hierarchy_id
                FROM facilities as f
                JOIN facility_types as ft on ft.id = f.facility_type_id
                JOIN admin_hierarchies as p0 on p0.id = f.admin_hierarchy_id
                JOIN user_facilities as uf on uf.facility_id = f.id
                where f.is_active = true
                and uf.user_id =" . $user->id . "
                and f.id not in " . $idsToExcludeString .
                " and LOWER(f.name) like " . $queryString . " LIMIT 5";

            $facilities = DB::select($string);
        }

        return $facilities;
    }


    public static function searchFacilityByAdmiAreaAndSection($adminHierarchyId, $sectionId, $idsToExcludeString, $queryString)
    {

        $facilityTypeIdsString = '';
        $userAdminLevel = DB::table('admin_hierarchy_levels as al')
            ->join('admin_hierarchies as a', 'al.id', 'a.admin_hierarchy_level_id')
            ->where('a.id', $adminHierarchyId)
            ->select('al.*')
            ->first();

        $publicFacility = ConfigurationSetting::where('key', 'PUBLIC_FACILITY_OWNERSHIP')->first();
        $publicFacilityOwnershiId = (isset($publicFacility) && $publicFacility->value != '' && $publicFacility->value != null) ? (int)$publicFacility->value : 0;

        $facilityTypeIds = DB::table('facility_type_sections')
            ->where('section_id', $sectionId)
            ->pluck('facility_type_id');

        foreach ($facilityTypeIds as $id) {
            $facilityTypeIdsString = $facilityTypeIdsString . $id . ',';
        }
        $facilityTypeIdsString = trim($facilityTypeIdsString, ',');

        if ($facilityTypeIdsString !== '') {

            $facilityTypes = FacilityType::whereIn('id', $facilityTypeIds)->get();

            $userIsParent = '';
            foreach ($facilityTypes as $facilityType) {
                $facilityAdminLevel = DB::table('admin_hierarchy_levels')->where('id', $facilityType->admin_hierarchy_level_id)->first();
                $dif = $facilityAdminLevel->hierarchy_position - $userAdminLevel->hierarchy_position;
                $userIsParent = $userIsParent . "p" . $dif . ".id =" . $adminHierarchyId . " or ";
            }
            $userIsParent = trim($userIsParent, "or ");

            $string = "Select f.id,f.name,ft.name as type,p0.id as admin_hierarchy_id
                    FROM facilities as f
                    JOIN facility_types as ft on ft.id = f.facility_type_id
                    JOIN admin_hierarchies as p0 on p0.id = f.admin_hierarchy_id
                    LEFT JOIN admin_hierarchies as p1 on p1.id = p0.parent_id
                    LEFT JOIN admin_hierarchies as p2 on p2.id = p1.parent_id
                    LEFT JOIN admin_hierarchies as p3 on p3.id = p2.parent_id
                    LEFT JOIN admin_hierarchies as p4 on p4.id = p3.parent_id
                    where f.is_active = true
                    and f.facility_ownership_id  =" . $publicFacilityOwnershiId . "
                    and f.facility_type_id IN ( " . $facilityTypeIdsString . ")
                    and (" . $userIsParent . ")
                    and f.id not in " . $idsToExcludeString .
                " and LOWER(f.name) like " . $queryString . " LIMIT 5";

            $facilities = DB::select($string);
        } else {
            $facilities = [];
        }

        return $facilities;
    }

    public static function hasCeiling($id)
    {
        $count = DB::table('admin_hierarchy_ceilings as ahc')
            ->where('ahc.facility_id', $id)->where('ahc.is_facility', true)
            ->groupBy('ahc.id')
            ->havingRaw('SUM(ahc.amount) > 0')
            ->count();
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function hasBudget($id)
    {
        $count = DB::table('activity_facility_fund_source_inputs as inp')
            ->join("activity_facility_fund_sources as affs", "inp.activity_facility_fund_source_id", "=", "affs.id")
            ->join("activity_facilities as af", "affs.activity_facility_id", "=", "af.id")
            ->where('af.facility_id', $id)
            ->groupBy('af.facility_id')
            ->havingRaw('SUM(inp.quantity*inp.unit_price*inp.frequency) > 0')
            ->count();
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }
}
