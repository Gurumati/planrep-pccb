<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityTaskNature extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'activity_task_natures';

    public function activity_category() {
        return $this->belongsTo('App\Models\Setup\ActivityCategory','activity_category_id','id');
    }
}
