<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class MetadataSource extends Model {
    public $timestamps = true;
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "metadata_sources";
    protected $fillable = ['source_name'];
    protected $guarded = ["id"];
    protected $dates = ['deleted_at'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'source_name' => 'required|string|unique:metadata_sources,source_name' . ($id ? ",$id" : ''),
            ],
            $merge);
    }

    public function types(){
        return $this->hasMany("App\Models\Setup\MetadataType","metadata_source_id","id");
    }
}
