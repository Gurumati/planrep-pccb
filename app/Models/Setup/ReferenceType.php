<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ReferenceType extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "reference_types";

    protected $fillable = ['name', 'description', 'multiple_select', 'link_level'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'name' => 'required|string',
                'description' => 'required|string',
                'multiple_select' => 'boolean',
                'link_level' => 'required|integer',
            ],
            $merge);
    }

    public function referenceTypeSectors()
    {
        return $this->belongsToMany('App\Models\Setup\Sector', 'reference_type_sectors');
    }

    public function references()
    {
        return $this->hasMany('App\Models\Setup\Reference', 'reference_type_id', 'id');
    }

    public function referenceTypeVersions()
    {
        return $this->hasMany('App\Models\Setup\ReferenceTypeVersion', 'reference_type_id', 'id');
    }

    public function reference_type_versions()
    {
        return $this->hasMany('App\Models\Setup\ReferenceTypeVersion', 'reference_type_id', 'id');
    }
}
