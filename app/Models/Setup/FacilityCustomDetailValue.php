<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FacilityCustomDetailValue extends Model
{
    public $timestamps = true;
    //use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "facility_custom_detail_values";

    protected $with = ['detail'];

    public function detail() {
        return $this->belongsTo('App\Models\Setup\FacilityCustomDetail','facility_custom_detail_id','id');
    }

    public function facility() {
        return $this->belongsTo('App\Models\Setup\Facility','facility_id','id');
    }
}
