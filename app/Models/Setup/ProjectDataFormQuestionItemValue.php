<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class ProjectDataFormQuestionItemValue extends Model
{
    public $table = 'project_data_form_question_item_values';

    public function projectDataFormQuestion(){
        return $this->belongsTo('App\Models\Setup\ProjectDataFormQuestion');
    }

}
