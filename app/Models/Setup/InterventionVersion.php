<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class InterventionVersion extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'intervention_versions';
    protected $fillable = ['intervention_id','version_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'intervention_id' => 'required|integer',
                'version_id' => 'required|integer|unique_with:intervention_versions,intervention_id',
            ],
            $merge);
    }

    public function intervention() {
        return $this->belongsTo('App\Models\Setup\Intervention','intervention_id','id');
    }

    public function version() {
        return $this->belongsTo('App\Models\Setup\Version','version_id','id');
    }
}
