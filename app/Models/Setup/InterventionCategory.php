<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InterventionCategory extends Model
{

    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "intervention_categories";

    protected $with = [
        'childInterventionCategories'
    ];

    public function childInterventionCategories() {
        return $this->hasMany('App\Models\Setup\InterventionCategory', 'intervention_category_id');
    }

    public function interventions() {
        return $this->hasMany('App\Models\Setup\Intervention');
    }
}
