<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class RawFacility extends Model
{
    protected $table = 'raw_facilities';
}
