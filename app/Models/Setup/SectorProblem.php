<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SectorProblem extends Model {
    public $timestamps = true;
    //use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "sector_problems";

    public function priority_area() {
        return $this->belongsTo('App\Models\Setup\PriorityArea');
    }

    public function admin_hierarchy() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchy','admin_hierarchy_id','id');
    }
}
