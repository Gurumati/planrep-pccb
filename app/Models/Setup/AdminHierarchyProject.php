<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminHierarchyProject extends Model {
    public $timestamps = true;
    //use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'admin_hierarchy_projects';

    public function admin_hierarchy() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchy');
    }

    public function project() {
        return $this->belongsTo('App\Models\Setup\Project');
    }
}
