<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FacilityCustomDetailMapping extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "facility_custom_detail_mappings";

    public function facility_custom_detail() {
        return $this->belongsTo('App\Models\Setup\FacilityCustomDetail','facility_custom_detail_id','id');
    }

    public function facility_type() {
        return $this->belongsTo('App\Models\Setup\FacilityType','facility_type_id','id');
    }

    public function facility_ownership() {
        return $this->belongsTo('App\Models\Setup\FacilityOwnership','facility_ownership_id','id');
    }
}
