<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class MetadataType extends Model {
    public $timestamps = true;
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "metadata_types";
    protected $fillable = ['metadata_type_name','metadata_source_id','code'];
    protected $guarded = ["id"];
    protected $dates = ['deleted_at'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'metadata_type_name' => 'required|string',
                'metadata_source_id' => 'required|integer',
                'code' => 'required|string|unique:metadata_types,code' . ($id ? ",$id" : ''),
            ],
            $merge);
    }

    public function source(){
        return $this->belongsTo("App\Models\Setup\MetadataSource","metadata_source_id","id");
    }

    public function items(){
        return $this->hasMany("App\Models\Setup\MetadataListItem","metadata_type_id","id");
    }
}
