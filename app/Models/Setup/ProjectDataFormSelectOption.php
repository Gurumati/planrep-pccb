<?php

namespace App\Models\Setup;
use Illuminate\Database\Eloquent\Model;


class ProjectDataFormSelectOption extends Model
{
    public $table = "project_data_form_select_options";

    public function question (){
        return $this->belongsTo('App\Models\Setup\ProjectDataFormQuestion', 'project_data_form_question_id');
    }
}
