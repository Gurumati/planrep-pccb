<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class ProjectDataFormQuestion extends Model
{
    public $table = 'project_data_form_questions';
    public $with  = ['selectOptions'];

    public function content(){
        return $this->belongsTo('App\Models\Setup\ProjectDataFormContent','project_data_form_content_id');
    }

    public function questionValues(){
        return $this->hasMany('App\Models\Setup\ProjectDataFormQuestionItemValue');
    }

    public function selectOptions(){
        return $this->hasMany('App\Models\Setup\ProjectDataFormSelectOption');

    }

}
