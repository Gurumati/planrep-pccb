<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectVersion extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'project_versions';
    protected $fillable = ['version_id','project_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'project_id' => 'required|integer',
                'version_id' => 'required|unique_with:project_versions,project_id'. ($id ? ",$id" : '')
            ],
            $merge);
    }

    public function version(){
        return $this->belongsTo("App\Models\Setup\Version","version_id",'id');
    }

    public function project(){
        return $this->belongsTo("App\Models\Setup\Project","project_id",'id');
    }
}
