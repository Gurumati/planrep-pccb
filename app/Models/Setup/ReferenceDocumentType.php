<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;


class ReferenceDocumentType extends Model
{
    public $timestamps = true;
    protected $table = 'reference_document_types';
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $fillable = ['name','description','allowed_types','is_national_guideline','root_path','created_by','updated_by'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string|max:255',
            ],
            $merge);
    }

    public function referenceDocument(){
        return $this->hasMany('App\Models\Setup\ReferenceDocument');
    }

    public function adminHierarchy(){
        return $this->hasMany('App\Models\Setup\AdminHierarchy');
    }

}
