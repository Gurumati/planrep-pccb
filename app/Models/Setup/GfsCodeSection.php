<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class GfsCodeSection extends Model
{
    public $timestamps = true;
    //use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'gfs_code_sections';

    public function gfs_code()
    {
        return $this->belongsTo('App\Models\Setup\GfsCode', 'gfs_code_id', 'id');
    }

    public function section()
    {
        return $this->belongsTo('App\DTOs\SetupDTOs\SectionDTO', 'section_id', 'id');
    }
}
