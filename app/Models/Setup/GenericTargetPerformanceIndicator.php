<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GenericTargetPerformanceIndicator extends Model
{
    public $timestamps = true;
    //use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'generic_target_performance_indicators';

    public function generic_target() {
        return $this->belongsTo('App\Models\Setup\GenericTarget','generic_target_id','id');
    }

    public function performance_indicator() {
        return $this->belongsTo('App\Models\Setup\PerformanceIndicator','performance_indicator_id','id');
    }
}
