<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;


class GfsCode extends Model implements AuditableContract
{

    public $timestamps = true;
    use SoftDeletes, Auditable;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'gfs_codes';

    protected $auditInclude = ['code', 'description', 'name', 'is_procurement', 'is_active', 'gfs_code_sub_category_id', 'procurement_type_id'];
    public static function revenueGfsCodes($revenueGfsAccountType)
    {

        $gfsCode = null;
        if ($revenueGfsAccountType != null) {
            $gfsCode = GfsCode::with('gfs_code_sub_category', 'accountType', 'fund_source_gfs.fund_source')->whereHas('accountType', function ($q) use ($revenueGfsAccountType) {
                $q->where('id', (int) $revenueGfsAccountType->value);
            })
                ->where('is_active', true)
                ->select('id', 'name', 'code', 'description')
                ->get();
        } else {
            $gfsCode = GfsCode::with('gfs_code_sub_category', 'accountType', 'fund_source_gfs.fund_source')
                ->where('code', 'like', '010%')
                ->where('is_active', true)
                ->select('id', 'name', 'code', 'description')
                ->get();
        }

        foreach ($gfsCode as &$gfs) {
            $fundSourceGfs = isset($gfs->fund_source_gfs) ? $gfs->fund_source_gfs : [];
            $fundSource = sizeof($fundSourceGfs) > 0 ? $fundSourceGfs[0]->fund_source : null;
            $gfs->fund_source = $fundSource;
            unset($gfs->fund_source_gfs);
        }
        return $gfsCode;
    }

    public static function revenueByFundSourceVersion($fundSourceVersionId, $financialYearId, $revenueGfsAccountType)
    {

        $fundSourceIds = FundSourceVersion::where('version_id', $fundSourceVersionId)->pluck('fund_source_id')->toArray();
        $gfsToexclude = DB::table('gfscode_fundsources')->whereIn('fund_source_id', $fundSourceIds)->distinct('gfs_code_id')->pluck('gfs_code_id')->toArray();
        $gfsCode = null;
        if ($revenueGfsAccountType != null) {
            $gfsCode = GfsCode::with('gfs_code_sub_category', 'accountType')->whereHas('accountType', function ($q) use ($revenueGfsAccountType) {
                $q->where('account_types.id', (int) $revenueGfsAccountType->value);
            })
                ->where('is_active', true)
                ->whereNotIn('gfs_codes.id', $gfsToexclude)
                ->select('gfs_codes.id', 'name', 'code', 'description')
                ->orderBy('name')
                ->get();
        } else {
            $gfsCode = GfsCode::with('gfs_code_sub_category', 'accountType')->where('code', 'like', '010%')
                ->where('fyv.financial_year_id', $financialYearId)
                ->where('is_active', true)
                ->whereNotIn('gfs_codes.id', $gfsToexclude)
                ->select('gfs_codes.id', 'name', 'code', 'description')
                ->orderBy('name')
                ->get();
        }
        return $gfsCode;
    }

    public function accountType()
    {
        return $this->belongsTo('App\Models\Setup\AccountType');
    }

    public function account_type()
    {
        return $this->belongsTo('App\Models\Setup\AccountType');
    }

    public function gfs_code_sub_category()
    {
        return $this->belongsTo('App\Models\Setup\GfsCodeSubCategory', "gfs_code_sub_category_id", 'id');
    }

    public function fund_source()
    {
        return $this->belongsTo('App\Models\Setup\FundSource');
    }

    public function budgetSubmissionDefinition()
    {
        return $this->hasMany('App\Models\Budgeting\BudgetSubmissionDefinition');
    }

    public function gfs_code_projections()
    {
        return $this->hasMany('App\Models\Planning\GfsCodeProjection');
    }

    public function gfs_code_sections()
    {
        return $this->hasMany('App\Models\Setup\GfsCodeSection');
    }

    public function admin_hierarchy_gfs_codes()
    {
        return $this->hasMany('App\Models\Setup\AdminHierarchyGfsCode');
    }

    public function ceilings()
    {
        return $this->hasMany('App\Models\Budgeting\Ceiling');
    }

    public function fund_source_gfs()
    {
        return $this->hasMany('App\Models\Setup\GfsFundSource');
    }

    public function procurement_type()
    {
        return $this->belongsTo('App\Models\Setup\ProcurementType', 'procurement_type_id', 'id');
    }
}
