<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GenericTargetNationalReference extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'generic_target_national_references';

    public function generic_target() {
        return $this->belongsTo('App\Models\Setup\GenericTarget','generic_target_id','id');
    }

    public function national_reference() {
        return $this->belongsTo('App\Models\Setup\Reference','national_reference_id','id');
    }
}
