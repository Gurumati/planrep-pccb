<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReferenceTypeSector extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'reference_type_sectors';
    protected $fillable = ['sector_id','reference_type_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'sector_id' => 'required|integer',
                'reference_type_id' => 'required|unique_with:reference_type_sectors,sector_id'. ($id ? ",$id" : '')
            ],
            $merge);
    }

    public function sector(){
        return $this->belongsTo("App\Models\Setup\Sector","sector_id",'id');
    }

    public function referenceType(){
        return $this->belongsTo("App\Models\Setup\ReferenceType","reference_type_id",'id');
    }
}
