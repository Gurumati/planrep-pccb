<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class BudgetClassVersion extends Model {
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "budget_class_versions";
    protected $fillable = ['budget_class_id', 'version_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'budget_class_id' => 'required|string|max:64',
                'version_id' => 'required|integer|unique_with:budget_class_versions,budget_class_id' . ($id ? ",$id" : ''),
            ],
            $merge);
    }

    public function budget_class() {
        return $this->belongsTo('App\Models\Setup\BudgetClass', 'budget_class_id', 'id');
    }

    public function version() {
        return $this->belongsTo('App\Models\Setup\Version', 'version_id', 'id');
    }
}
