<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class SectionLevelWithNextBudgetLevelDTO extends Model
{

    protected $with = ['nextDecisionLevel'];
    protected $table='section_levels';

    public function nextDecisionLevel() {
        return $this->belongsTo('App\Models\Setup\SectionLevelWithNextBudgetLevelDTO', 'next_budget_level')
                      ->select('id','name','next_budget_level');
    }

}
