<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class FundSourceVersion extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'fund_source_versions';
    protected $fillable = ['fund_source_id','version_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'fund_source_id' => 'required|integer',
                'version_id' => 'required|integer|unique_with:fund_source_versions,plan_chain_id',
            ],
            $merge);
    }

    public function fund_source() {
        return $this->belongsTo('App\Models\Setup\FundSource','fund_source_id','id');
    }

    public function version() {
        return $this->belongsTo('App\Models\Setup\Version','version_id','id');
    }
}
