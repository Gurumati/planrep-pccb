<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanningUnit extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function admin_hierarchy_level() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchyLevel');
    }
}
