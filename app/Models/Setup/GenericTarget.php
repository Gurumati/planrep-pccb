<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GenericTarget extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'generic_targets';
    protected $fillable = ['code','description','plan_chain_id','target_type_id','performance_indicator_id','planning_matrix_id','params'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'description' => 'string|required',
                'plan_chain_id' => 'required|integer',
                'performance_indicator_id' => 'required|integer',
                'is_active' => 'sometimes|boolean',
                'planning_matrix_id' => 'required|integer',
                'params' => 'string'
            ],
            $merge);
    }

    public function performance_indicator() {
        return $this->belongsTo('App\Models\Setup\PerformanceIndicator')
            ->select(['id','description','number']);
    }

    public function planning_matrix() {
        return $this->belongsTo('App\Models\Setup\PlanningMatrix','planning_matrix_id','id');
    }

    public function plan_chain() {
        return $this->belongsTo('App\Models\Setup\PlanChain','plan_chain_id','id');
    }
}
