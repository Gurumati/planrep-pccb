<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class CasPlanTableReportConstraint extends Model
{
    public $table = 'cas_plan_table_report_constraints';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function casPlanTable(){
        return $this->belongsTo('\App\Models\Setup\CasPlanTableReportConstraint');
    }
}
