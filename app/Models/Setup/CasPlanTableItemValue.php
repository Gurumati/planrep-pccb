<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class CasPlanTableItemValue extends Model
{
    public $timestamps = true;
    
    protected $table = "cas_plan_table_item_values";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function casPlanTableColumn(){
        return $this->belongsTo('App\Models\Setup\CasPlanTableColumn');
    }

    public function casPlanTableItem(){
        return $this->belongsTo('App\Models\Setup\CasPlanTableItem');

    }

    public function financialYear(){
        return $this->belongsTo('App\Models\Setup\FinancialYear')->select('id', 'name', 'status', 'start_date', 'end_date');

    }

    public function adminHierarchy(){
        return $this->belongsTo('App\Models\Setup\AdminHierarchy')->select('id', 'name','code','is_active');
    }

    public static function flatternData($data){
       $result_array = array();
       $facilities   = array();

       foreach($data as $item){
           if($item->cas_plan_table_item_id == null || $item->cas_plan_table_item_id == ''){
               $row_id = $item->sort_order;
               if($item->facility_id > 0){
                $facilities[$row_id] = $item->facility_id;
               }
           }else {
               $row_id = $item->cas_plan_table_item_id;
           }
           $result_array[$row_id][$item->cas_plan_table_column_id]  =  $item->value;
           
       }
       return  ['data_array'=>$result_array,'facilities'=>$facilities];
    }
}
