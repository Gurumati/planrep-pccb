<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class BaselineStatisticFinancialYear extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'baseline_statistic_financial_years';
    protected $fillable = ['baseline_statistic_id','financial_year_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'baseline_statistic_id' => 'required|integer',
                'financial_year_id' => 'required|integer|unique_with:baseline_statistic_financial_years,baseline_statistic_id',
            ],
            $merge);
    }

    public function baseline_statistic() {
        return $this->belongsTo('App\Models\Setup\BaselineStatisticFinancialYear','baseline_statistic_id','id');
    }

    public function financial_year() {
        return $this->belongsTo('App\Models\Setup\FinancialYear','financial_year_id','id');
    }
}
