<?php

namespace App\Models\Setup;

use App\DTOs\SetupDTOs\SectionDTO;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class FundSource extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'fund_sources';
    protected $fillable = [
        'name', 'description', 'fund_source_category_id', 'code',
        'fund_source_id', 'is_conditional', 'can_project', 'is_foreign'
    ];


    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'code' => 'required|string|min:3|max:3'
            ],
            $merge
        );
    }

    public static function getByBudgetClassAndSector($financialYearId,$budgetClassId, $sectionId)
    {
        $sectorId = SectionDTO::where('id', $sectionId)->pluck('sector_id');
        $fundSourceId = Input::get('fundSourceId');

        $versionId = Version::getVersionByFinancialYear($financialYearId,'FS');
        $itemIds = Version::getVesionItemIds($versionId,'FS');

        return FundSource
            ::whereIn('id',$itemIds)
            ->where(function ($f) use ($fundSourceId, $budgetClassId) {
                if (isset($fundSourceId)) {
                    $f->where('id', $fundSourceId);
                } else {
                    $f->whereHas('fund_source_budget_classes', function ($filter) use ($budgetClassId) {
                        $filter->where('budget_class_id', $budgetClassId);
                    });
                }
            })
            ->where(function ($filter1) use ($sectorId) {
                $filter1->whereHas('fund_source_gfs.gfs_code.ceilings.ceiling_sectors', function ($filter) use ($sectorId) {
                    $filter->whereIn('sector_id', $sectorId);
                })->orWhere('can_project', true);
            })->get();
    }
    public static function getIdsByVersion($versionId) {

        if ($versionId == null) {
            return [];
        }
//       return Cache::rememberForever(CacheKeys::FUND_SOURCE_IDS_BY_VERSION . $versionId, function () use ($versionId) {
        return FundSource::whereHas('fund_source_versions', function ($query) use ($versionId) {
            $query->where('version_id', $versionId);
        })->pluck('id');
//        });

    }

    public static function getByMainBudgetClassAndSector($financialYearId,$fundSourceId, $sectionId)
    {
        $sectorId = SectionDTO::where('id', $sectionId)->pluck('sector_id');
        $mainBudgetClassIds = DB::table('fund_source_budget_classes as fb')
                ->join('budget_classes as b', 'b.id', 'fb.budget_class_id')
                ->where('fb.fund_source_id', $fundSourceId)
                ->pluck('b.parent_id');

        $versionId = Version::getVersionByFinancialYear($financialYearId,'FS');
        $itemIds = Version::getVesionItemIds($versionId,'FS');

        return DB::table('fund_sources as fu')
                ->leftjoin('gfscode_fundsources as gfsf','gfsf.fund_source_id','fu.id')
                ->leftJoin('gfs_codes as gfs', 'gfs.id', 'gfsf.gfs_code_id')
                ->leftJoin('ceilings as c', 'c.gfs_code_id', 'gfs.id')
                ->leftJoin('ceiling_sectors as cs', 'cs.ceiling_id', 'c.id')
                ->join('fund_source_budget_classes as fb', 'fb.fund_source_id', 'fu.id')
                ->join('budget_classes as b', 'b.id', 'fb.budget_class_id')
                ->whereIn('fu.id', $itemIds)
                ->whereIn('b.parent_id', $mainBudgetClassIds)
                ->where(function($f) use($sectorId){
                    $f->whereIn('cs.sector_id', $sectorId)->orWhere('fu.can_project', true);
                })
                ->groupBy('fu.id')
                ->select('fu.id', 'fu.name')
                ->orderBy('fu.name')
                ->get();
    }

    public function fund_source_category()
    {
        return $this->belongsTo('App\Models\Setup\FundSourceCategory', 'fund_source_category_id', 'id');
    }

    public function childFundSources()
    {
        return $this->hasMany('App\Models\Setup\FundSource', 'fund_source_id');
    }

    public function fund_source_versions()
    {
        return $this->hasMany('App\Models\Setup\FundSourceVersion', 'fund_source_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Setup\FundSource', 'fund_source_id');
    }

    public function projections()
    {
        return $this->hasMany('App\Models\Planning\Projection');
    }

    public function gfs_codes()
    {
        return $this->hasMany('App\Models\Setup\GfsCode');
    }

    public function fund_source_budget_classes()
    {
        return $this->hasMany('App\Models\Setup\FundSourceBudgetClass');
    }

    public function ceilings()
    {
        return $this->hasMany('App\Models\Budgeting\Ceiling');
    }

    public function fund_source_gfs() {
        return $this->hasMany('App\Models\Setup\GfsFundSource');
    }

    public function funder()
    {
        return $this->belongsTo('App\Models\Setup\Funder');
    }

    public static function getBudgetedByFacility($budgetType, $financialYearId, $adminHiearchyId, $sectionId, $facilityId)
    {
        $fundSources = DB::table('fund_sources as fu')
            ->join('activity_facility_fund_sources as aff', 'fu.id', 'aff.fund_source_id')
            ->join('activity_facilities as af', 'af.id', 'aff.activity_facility_id')
            ->join('activities as a', 'a.id', 'af.activity_id')
            ->join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')
            ->join('mtefs as m', 'm.id', 'ms.mtef_id')
            ->where('af.facility_id', $facilityId)
            ->where('ms.section_id', $sectionId)
            ->where('m.financial_year_id', $financialYearId)
            ->where('m.admin_hierarchy_id', $adminHiearchyId)
            ->where('a.budget_type', $budgetType)
            ->distinct()
            ->select('fu.*')
            ->get();

        return $fundSources;
    }
}
