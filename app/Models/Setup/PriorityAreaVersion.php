<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class PriorityAreaVersion extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'priority_area_versions';
    protected $fillable = ['priority_area_id','version_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'priority_area_id' => 'required|integer',
                'version_id' => 'required|integer|unique_with:priority_area_versions,priority_area_id',
            ],
            $merge);
    }

    public function priority_area() {
        return $this->belongsTo('App\Models\Setup\PriorityArea','priority_area_id','id');
    }

    public function version() {
        return $this->belongsTo('App\Models\Setup\Version','version_id','id');
    }
}
