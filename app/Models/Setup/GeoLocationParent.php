<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GeoLocationParent extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'geographical_locations';

    protected $with = [
        'parent'
    ];
    public function parent() {
        return $this->belongsTo('App\Models\Setup\GeoLocationParent','parent_id');
    }

    public function geo_location_level() {
        return $this->belongsTo('App\Models\Setup\GeoLocationLevel','geo_location_level_id');
    }
}
