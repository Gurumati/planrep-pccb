<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class ReferenceDocument extends Model {

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'reference_documents';
    protected $fillable = ['name', 'description','start_financial_year','reference_document_type_id','end_financial_year','admin_hierarchy_id','document_url'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string|max:255',
                'reference_document_type_id' => 'required',
                'start_financial_year' => 'required',
                'end_financial_year' => 'required',
            ],
            $merge);
    }

    public function referenceDocumentType() {
        return $this->belongsTo('App\Models\Setup\ReferenceDocumentType','reference_document_type_id','id');
    }

    public function adminHierarchy() {
        return $this->belongsTo('App\DTOs\SetupDTOs\AdminHierarchyDTO','admin_hierarchy_id','id')->select('id', 'name');
    }

    public function sFinancialYear() {
        return $this->belongsTo('App\Models\Setup\FinancialYear', 'start_financial_year')->select('id', 'name');
    }

    public function eFinancialYear() {
        return $this->belongsTo('App\Models\Setup\FinancialYear', 'end_financial_year')->select('id', 'name');
    }

    public function longTermTargets() {
        return $this->hasMany('App\Models\Planning\LongTermTarget');
    }

    public static function paginated($userAdminHierarchyId,$perPage, $isNationalGuideline){
        $today = date("Y-m-d") . ' 00:00:00';
        if ($isNationalGuideline == 0) {
            $referenceDocuments = ReferenceDocument::with('referenceDocumentType')
                ->with(['adminHierarchy' => function ($q) {
                    $q->select('id', 'name');
                }])
                ->with('sFinancialYear')
                ->with('eFinancialYear')
                ->whereHas('eFinancialYear', function ($q) use ($today) {
                    $q->whereDate('end_date', '>=', $today);
                })
                ->where('is_active', true)->
                where(function ($query) use ($userAdminHierarchyId) {
                    $query->whereHas('referenceDocumentType', function ($q) {
                        $q->where('is_national_guideline', true);
                    })->
                    orWhere('admin_hierarchy_id', $userAdminHierarchyId);
                })
                ->orderBy('id','desc')
                ->paginate($perPage);
        } else {
            $referenceDocuments = ReferenceDocument::with('referenceDocumentType')
                ->with(['adminHierarchy' => function ($q) {
                    $q->select('id', 'name');
                }])
                ->with('sFinancialYear')
                ->with('eFinancialYear')
                ->whereHas('eFinancialYear', function ($q) use ($today) {
                    $q->whereDate('end_date', '>=', $today);
                })
                ->where('is_active', true)->
                whereHas('referenceDocumentType', function ($q) {
                    $q->where('is_national_guideline', true);
                })
                ->orderBy('id','desc')
                ->paginate($perPage);
        }
        return $referenceDocuments;
    }

    public static function getUserValidActiveDocuments($userAdminHierarchyId){
        $today = date("Y-m-d") . ' 00:00:00';
        $refDocs = ReferenceDocument::with('referenceDocumentType')->with('sFinancialYear')->with('eFinancialYear')
                    ->whereHas('eFinancialYear', function ($q) use ($today) {
                        $q->whereDate('end_date', '>=', $today);
                    })
                    ->where('is_active', true)
                     ->where(function ($query) use ($userAdminHierarchyId) {
                          $query->whereHas('referenceDocumentType', function ($q) {
                              $q->where('is_national_guideline', true);
                          })->
                          orWhere('admin_hierarchy_id', $userAdminHierarchyId);
                      })
                    ->orderBy('created_at','desc')
                    ->get();
        return $refDocs;
    }

    public static function getNationalGuidelines($userAdminHierarchyId){
        $all = ReferenceDocument::with('referenceDocumentType', 'sFinancialYear', 'eFinancialYear')->where('admin_hierarchy_id', $userAdminHierarchyId)->get();
        return $all;

    }
}
