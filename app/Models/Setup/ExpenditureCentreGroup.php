<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExpenditureCentreGroup extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "bdc_expenditure_centre_groups";

    public function fund_source(){
        return $this->belongsTo('App\Models\Setup\FundSource','fund_source_id','id');
    }

    public function reference_document(){
        return $this->belongsTo('App\Models\Setup\ReferenceDocument','reference_document_id','id');
    }

    public function section(){
        return $this->belongsTo('App\Models\Setup\Section','section_id','id');
    }

    public static function getUserExpenditureCentreGroups($sectionId){
        return ExpenditureCentreGroup::where('section_id',$sectionId)->get();
    }
}
