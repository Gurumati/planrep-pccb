<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerformanceIndicator extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "performance_indicators";
    protected $fillable = ['description','number','plan_chain_id','is_qualitative','less_is_good','section_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'description' => 'required|string',
                'number' => 'required|string',
                'plan_chain_id' => 'required|integer',
                'is_qualitative' => 'boolean',
                'less_is_good' => 'boolean',
                'section_id' => 'required|integer',
            ],
            $merge);
    }

    public function plan_chain() {
        return $this->belongsTo('App\Models\Setup\PlanChain','plan_chain_id','id');
    }

    public function section() {
        return $this->belongsTo('App\Models\Setup\Section','section_id','id');
    }

    public function baseline_values(){
        return $this->hasMany('App\Models\Planning\PerformanceIndicatorBaselineValue');
    }

    public function performance_indicator_versions()
    {
        return $this->hasMany('App\Models\Setup\PerformanceIndicatorVersion', 'performance_indicator_id', 'id');
    }
}
