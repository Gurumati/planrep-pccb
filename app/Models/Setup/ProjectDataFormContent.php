<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class ProjectDataFormContent extends Model
{
    protected $table = 'project_data_form_contents';

    public function projectDataForm(){
        return $this->belongsTo('App\Models\Setup\ProjectDataForm','project_data_form_id');
    }

    public function children() {
        return $this->hasMany('App\Models\Setup\ProjectDataFormContent', 'parent_id');
    }

    public function questions(){
        return $this->hasMany('App\Models\Setup\ProjectDataFormQuestion');
    }
}
