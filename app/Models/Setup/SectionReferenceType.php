<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SectionReferenceType extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'section_generic_types';

    public function section(){
        return $this->belongsTo('App\Models\Setup\Section');
    }

    public function generic_type(){
        return $this->belongsTo('App\Models\Setup\GenericType');
    }
}
