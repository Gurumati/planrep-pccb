<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class casPlanTableSelectOption extends Model
{
    public $timestamps = true;
    use SoftDeletes;

    protected $table = "cas_plan_table_select_options";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $with = ['children'];

    public function parent() {
        return $this->belongsTo('App\Models\Setup\casPlanTableSelectOption');
    }

    public function children() {
        return $this->hasMany('App\Models\Setup\casPlanTableSelectOption','parent_id')->select('id','name','parent_id');
    }
}
