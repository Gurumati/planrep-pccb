<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BodVersion extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'bod_versions';

    public function start_financial_year(){
        return $this->belongsTo('App\Models\Setup\FinancialYear','start_financial_year_id','id');
    }

    public function end_financial_year(){
        return $this->belongsTo('App\Models\Setup\FinancialYear','end_financial_year_id','id');
    }

    public function bod_list(){
        return $this->belongsTo('App\Models\Setup\BodList','bod_list_id','id');
    }

    public function reference_document(){
        return $this->belongsTo('App\Models\Setup\ReferenceDocument','reference_doc','id');
    }
}
