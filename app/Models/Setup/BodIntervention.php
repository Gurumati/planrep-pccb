<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BodIntervention extends Model {

    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'bod_interventions';


    public function bod_version() {
        return $this->belongsTo('App\Models\Setup\BodVersion');
    }

    public function bod_list() {
        return $this->belongsTo('App\Models\Setup\BodList');
    }

    public function intervention() {
        return $this->belongsTo('App\Models\Setup\Intervention', 'intervention_id');
    }

    public function fund_source() {
        return $this->belongsTo('App\Models\Setup\FundSource');
    }

}
