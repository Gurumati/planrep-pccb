<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GenericTemplate extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'generic_templates';

    public function childTemplates() {
        return $this->hasMany('App\Models\Setup\GenericTemplate','generic_template_id');
    }

    public function parent(){
        return $this->belongsTo('App\Models\Setup\GenericTemplate','generic_template_id');
    }
}
