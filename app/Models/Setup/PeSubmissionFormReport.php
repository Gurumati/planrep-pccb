<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class PeSubmissionFormReport extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    public $table = "pe_submission_forms_reports";

    public function parent(){
        return $this->belongsTo('App\Models\Setup\PeSubmissionFormReport');
    }
}
