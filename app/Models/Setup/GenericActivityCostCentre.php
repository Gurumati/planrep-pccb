<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GenericActivityCostCentre extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'generic_activity_cost_centres';

    protected $fillable = ["generic_activity_id","section_id"];

    protected $with = ["cost_centre"];

    public function generic_activity() {
        return $this->belongsTo('App\Models\Setup\GenericActivity','generic_activity_id','id');
    }

    public function cost_centre() {
        return $this->belongsTo('App\Models\Setup\Section','section_id','id');
    }
}
