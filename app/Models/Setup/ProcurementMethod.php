<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProcurementMethod extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "procurement_methods";
    protected $fillable = ['name', 'code', 'description'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'name' => 'required|string',
                'code' => 'required|unique:procurement_methods,code' . ($id ? ",$id" : ''),
                'description' => 'required|string',
            ],
            $merge);
    }
}
