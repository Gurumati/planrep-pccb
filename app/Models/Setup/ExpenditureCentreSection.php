<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class ExpenditureCentreSection extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "bdc_expenditure_centre_sections";

    protected $fillable = [
        'section_id',
        'bdc_expenditure_centre_id'
    ];


    public function cost_centre(){
        return $this->belongsTo('App\DTOs\SetupDTOs\SectionDTO','section_id','id');
    }

    public function expenditure_centre(){
        return $this->belongsTo('App\Models\Setup\ExpenditureCentre','bdc_expenditure_centre_id','id');
    }
}
