<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class DecisionLevel extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "decision_levels";

    public static function getByAdminAndSection($adminHierarchyId, $sectionId)
    {

        // return Cache::rememberForever('DECISION_LEVEL_BY_ADMIN_'.$adminHierarchyId.'_AND_SECTION_'.$sectionId,
            // function () use($adminHierarchyId, $sectionId){
                $sectionLevel = DB::table('sections')->where('id',$sectionId)->first();
                $adminHierarchyLevel = DB::table('admin_hierarchy_levels as al')->
                                        join('admin_hierarchies as a','a.admin_hierarchy_level_id','al.id')->
                                        where('a.id',$adminHierarchyId)->
                                        select('al.*')->
                                        first();
                return DecisionLevel::with('next_decisions')->
                            where('admin_hierarchy_level_position',$adminHierarchyLevel->hierarchy_position)->
                            where('section_level_id',$sectionLevel->section_level_id)->
                            first();
        // });

    }

    public static function getReturnDecisionLevels($id)
    {
        $levelIds = DB::table('decision_level_next_decisions')->where('next_level_id',$id)->pluck('decision_level_id');
        return DecisionLevel::whereIn('id',$levelIds)->get();
    }

    public function mtefs() {
        return $this->hasMany('App\Models\Planning\Mtef');
    }

    public function adminHierarchyLevels() {
        return $this->hasMany('App\Models\Setup\AdminHierarchyLevel','hierarchy_position','admin_hierarchy_level_position');
    }
    public function sectionLevel() {
        return $this->belongsTo('App\Models\Setup\SectionLevel');
    }

    public function next_decisions() {
        return $this->belongsToMany('App\Models\Setup\DecisionLevel', 'decision_level_next_decisions','decision_level_id','next_level_id');
    }
}
