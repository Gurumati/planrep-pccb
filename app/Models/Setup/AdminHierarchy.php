<?php

namespace App\Models\Setup;

use App\Http\Controllers\Flatten;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class AdminHierarchy extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'admin_hierarchies';

    protected $with = [
        'admin_hierarchy_level','parent'
    ];

    public static function isAtCeilingStartingPoint($startCeilingPosition, $adminHierarchyId)
    {
        $ceilingStartAdminLevel = AdminHierarchyLevel::where('hierarchy_position',$startCeilingPosition)->first();

        $adminHierarchy = AdminHierarchy::find($adminHierarchyId);

        return ($ceilingStartAdminLevel->id == $adminHierarchy->admin_hierarchy_level_id) ? true : false;
    }

    public static function getAdminHierarchyTree($adminHierarchyId)
    {

        $userAdminLevelPosition = DB::table('admin_hierarchy_levels as al')->
                                    join('admin_hierarchies as a', 'al.id', 'a.admin_hierarchy_level_id')->
                                    where('a.id', $adminHierarchyId)->
                                    first()->hierarchy_position;
        $withString = array();

        if ($userAdminLevelPosition == 1) {
            $withString = array();
            array_push($withString, 'children');
            array_push($withString, 'children.children');
            array_push($withString, 'children.children.children');
        } else if ($userAdminLevelPosition == 2) {
            $withString = array();
            array_push($withString, 'children');
            array_push($withString, 'children.children');
        } else if($userAdminLevelPosition == 3){
            $withString = array();
            array_push($withString, 'children');
        }

        $adminHierarchy = AdminHierarchy::with($withString)->where('id', $adminHierarchyId)->select('id', 'name', 'parent_id', 'admin_hierarchy_level_id')->get();
        return $adminHierarchy;
    }

    public static function getAdminIdsWithChildren($adminHierarchyId)
    {
        // return Cache::rememberForever('ADMIN_HIERARCHY_IDS_WITH_CHILDREN_'.$adminHierarchyId,
        //     function () use($adminHierarchyId){
                $flatten = new Flatten();
                $adminHierarchy = $flatten->getHierarchyTreeById($adminHierarchyId);
                return $flatten->flattenAdminHierarchyWithChild($adminHierarchy);
            // });
    }

    public function admin_hierarchy_level() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchyLevel','admin_hierarchy_level_id','id')
            ->select('id','name','hierarchy_position');
    }

    public function parent() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchy','parent_id','id')->select('id','name');
    }


    public function users() {
        return $this->hasMany('App\Models\Auth\User');
    }

    public function admin_hierarchy_projects() {
        return $this->hasMany('App\Models\Setup\AdminHierarchyProject');
    }

    public function facilities() {
        return $this->hasMany('App\Models\Setup\Facility');
    }


    public function projections() {
        return $this->hasMany('App\Models\Planning\Projection');
    }

    public function children() {
        return $this->hasMany('App\Models\Setup\AdminHierarchy', 'parent_id');
    }

    public function referenceDocuments() {
        return $this->hasMany('App\Models\Planning\ReferenceDocument');
    }

    public function financialYears() {
        return $this->hasMany('App\Models\Planning\ReferenceDocument');
    }

    public function retiring_employees() {
        return $this->hasMany('App\Models\Budgeting\RetiringEmployee');
    }

    public function mtefs() {
        return $this->hasMany('App\Models\Planning\Mtef');
    }

    public function ceilings() {
        return $this->hasMany('App\Models\Planning\Ceiling');
    }

    public function admin_hierarchy_gfs_codes() {
        return $this->hasMany('App\Models\Setup\AdminHierarchyGfsCode');
    }

    public function admin_hierarchy_admin_hierarchy_ceilings() {
        return $this->hasMany('App\Models\Setup\AdminHierarchyCeilings');
    }

    public function casPlanTableItemValues() {
        return $this->hasMany('App\Models\Setup\CasPlanTableItemValue');
    }

    public function casPlanTableItemAdminHierarchies()
    {
        return $this->hasMany('App\Models\Planning\CasPlanTableItemAdminHierarchy');
    }

    public function baselineStatistics(){
        return$this->hasMany('App\Models\Setup\BaselineStatisticValue');
    }

    public function assets() {
        return $this->hasMany('App\Models\Planning\AdminHierarchy');
    }
    public function parentArray() {
        return $this->hasMany('App\Models\Setup\AdminHierarchy','id','parent_id');
    }
}
