<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ConfigurationSetting extends Model
{
    public $timestamps = false;

    public static function getValueByKey($key){
        $config = ConfigurationSetting::where('key',$key)->first();
        if($config !== null) {
            if ($config->value_type === "MULT_SELECT" ) {
                $config->value =($config->value !== null)?explode(',', rtrim($config->value,',')):[];
            }
            return $config->value;
        }else{
            return null;
        }

    }

    public static function refreshMaterialView($viewName) {
        $query = "REFRESH MATERIALIZED VIEW ".$viewName;
        return DB::statement($query);
    }

}
