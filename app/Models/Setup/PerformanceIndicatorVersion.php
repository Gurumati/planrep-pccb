<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class PerformanceIndicatorVersion extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'performance_indicator_versions';
    protected $fillable = ['performance_indicator_id','version_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'performance_indicator_id' => 'required|integer',
                'version_id' => 'required|integer|unique_with:performance_indicator_versions,performance_indicator_id',
            ],
            $merge);
    }

    public function performance_indicator() {
        return $this->belongsTo('App\Models\Setup\PerformanceIndicator','performance_indicator_id','id');
    }

    public function version() {
        return $this->belongsTo('App\Models\Setup\Version','version_id','id');
    }
}
