<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class CasPlanContentVersion extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "cas_plan_content_versions";
    protected $fillable = ['cas_plan_content_id', 'version_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'cas_plan_content_id' => 'required|string|max:64',
                'version_id' => 'required|integer|unique_with:cas_plan_content_versions,cas_plan_content_id' . ($id ? ",$id" : ''),
            ],
            $merge);
    }

    public function budget_class() {
        return $this->belongsTo('App\Models\Setup\CasPlanContentVersion', 'cas_plan_content_id', 'id');
    }

    public function version() {
        return $this->belongsTo('App\Models\Setup\Version', 'version_id', 'id');
    }
}
