<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class BaselineStatistic extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'baseline_statistics';

    protected $fillable = ['code','description','default_value','is_common','hmis_uid'];

    /**
     * @param int $id
     * @param array $merge
     * @return array
     */
    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'code' => 'required|unique:baseline_statistics,code' . ($id ? ",$id" : ''),
                'description' => 'required|string|max:500',

//                'default_value' => 'required|string|max:64',
//                'is_common' => 'required|boolean',
            ],
            $merge);
    }

    public function baselineStatisticValue(){
        return $this->belongsTo('App\Models\Planning\BaselineStatisticValue','id');
    }
}
