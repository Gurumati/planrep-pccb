<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class AdminHierarchyGfsCode extends Model {
    public $timestamps = true;
    protected $table = 'admin_hierarchy_gfs_codes';
    /*use SoftDeletes;*/
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function admin_hierarchy() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchy');
    }

    public function gfs_code() {
        return $this->belongsTo('App\Models\Setup\GfsCode');
    }
}
