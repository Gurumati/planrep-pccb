<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "bank_accounts";

    protected $fillable = ['name', 'code'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'name' => 'required|string',
                'code' => 'required|string|unique:bank_accounts,code' . ($id ? ",$id" : ''),
            ],
            $merge);
    }

    public function bank_account_versions()
    {
        return $this->hasMany('App\Models\Setup\BankAccountVersion','bank_account_id','id');
    }
}
