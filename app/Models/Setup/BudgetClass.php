<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Models\setup\FundSourceBudgetClass;
use Illuminate\Support\Facades\Input;


class BudgetClass extends Model
{
    protected $table = "budget_classes";
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $fillable = [
        'name', 'description', 'sort_order',
        'is_active', 'is_automatic', 'parent_id',
        'relationship','code','control_code'
    ];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string',
                'code' => 'required|string',
                'control_code' => 'required|string',
                'description' => 'required|string',
                'sort_order' => 'integer',
                'is_active' => 'boolean',
                'is_automatic' => 'boolean',
                'parent_id' => 'integer',
                'relationship' => 'integer'
            ],
            $merge);
    }

    protected $with = ['budgetClassTemplates'];

    /**
     * Get budget classes id already have ceiling
     */
    public static function getIdsByCeiling($adminHierarchyId, $financialYearId, $budgetType){
       return DB::table('ceilings as c')
            ->join('admin_hierarchy_ceilings as adc','adc.ceiling_id','c.id')
            ->where('adc.admin_hiearchy_id', $adminHierarchyId)
            ->where('adc.financial_year_id', $financialYearId)
            ->where('adc.budget_type', $budgetType)
            ->where('adc.amount','>', 0.00)
            ->distinct('c.budget_class_id')
            ->pluck('c.budget_class_id');
    }

    public static function getPlanningBudgetClasses($idToExclude,$sectorId)
    {
        $fundSourceId = Input::get('fundSourceId');

        $budgetClassIds = DB::table('ceilings as c')
                ->join('ceiling_sectors as sc','sc.ceiling_id','c.id')
                ->where(function($sector) use ($sectorId){
                  if (isset($sectorId)) {
                    $sector->where('sc.sector_id',$sectorId);
                  }
                })
                ->pluck('c.budget_class_id')
                ->toArray();
        if(isset($fundSourceId)){
            $fbudgetClassIds = DB::table('fund_source_budget_classes')
                ->where('fund_source_id',$fundSourceId)
                ->pluck('budget_class_id')
                ->toArray();
            $budgetClassIds = array_intersect($budgetClassIds,$fbudgetClassIds);
        }

        $budgetClasses = BudgetClass::with(['children'=>function($child) use($budgetClassIds,$idToExclude){
            $child->whereIn('id',$budgetClassIds);
            $child->whereNotIn('id', $idToExclude);
            $child->where('is_active', true);
            $child->where('is_automatic', false);
        }])
        ->where('is_automatic', false)
        ->where('is_active', true)
        ->whereNotIn('id', $idToExclude)
        ->whereNull('parent_id')
        ->select('id', 'name', 'description', 'parent_id', 'code')
        ->get();

        return $budgetClasses;
    }

    public function projection_budget_classes()
    {
        return $this->hasMany('App\Models\Planning\ProjectionBudgetClass');
    }

    public function budget_class_versions()
    {
        return $this->hasMany('App\Models\Setup\BudgetClassVersion', 'budget_class_id', 'id');
    }

    public function ceilings()
    {
        return $this->hasMany('App\Models\Budget\Ceiling');
    }

    public function budgetSubmission()
    {
        return $this->hasMany('App\Models\Budget\Ceiling');
    }

    public function budgetClassTemplates()
    {
        return $this->hasMany('App\Models\Setup\BudgetClassTemplate');
    }

    public function interventions()
    {
        return $this->hasMany('App\Models\Setup\Intervention');
    }

    public function fund_source_budget_classes()
    {
        return $this->hasMany('App\Models\Setup\FundSourceBudgetClass');
    }

    public function budgetClass()
    {
        return $this->belongsTo('App\Models\Setup\BudgetClass');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Setup\BudgetClass', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Setup\BudgetClass', 'parent_id')->select('id', 'code', 'name', 'description', 'parent_id');
    }
}
