<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SectorExpenditureSubCentre extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "bdc_sector_expenditure_sub_centres";

    public function expenditure_centre(){
        return $this->belongsTo('App\Models\Setup\ExpenditureCentre','expenditure_centre_id','id');
    }

    public function sector(){
        return $this->belongsTo('App\Models\Setup\Sector','sector_id','id');
    }

    public function bdc_group(){
        return $this->belongsTo('App\Models\Setup\BdcGroup','group_id','id');
    }
}
