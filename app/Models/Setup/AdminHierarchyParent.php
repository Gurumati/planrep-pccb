<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminHierarchyParent extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'admin_hierarchies';

    protected $with = [
        'parent'
    ];
    public function parent() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchyParent','parent_id');
    }

    public function admin_hierarchy_level() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchyLevel','admin_hierarchy_level_id');
    }

    public function mtefs() {
        return $this->hasMany('App\Models\Planning\Mtef');
    }
}
