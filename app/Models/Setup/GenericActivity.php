<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GenericActivity extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'generic_activities';

    protected $fillable = ['code','description','generic_target_id','priority_area_id','intervention_id','generic_sector_problem_id','params'];


    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'description' => 'string|required',
                'generic_target_id' => 'required|integer',
                'params' => 'string'
            ],
            $merge);
    }

    public function generic_target() {
        return $this->belongsTo('App\Models\Setup\GenericTarget','generic_target_id','id');
    }

    public function intervention() {
        return $this->belongsTo('App\Models\Setup\Intervention','intervention_id','id');
    }

    public function priority_area() {
        return $this->belongsTo('App\Models\Setup\PriorityArea','priority_area_id','id');
    }

    public function budget_class() {
        return $this->belongsTo('App\Models\Setup\BudgetClass','budget_class_id','id');
    }

    public function generic_sector_problem() {
        return $this->belongsTo('App\Models\Setup\GenericSectorProblem','generic_sector_problem_id','id');
    }
}
