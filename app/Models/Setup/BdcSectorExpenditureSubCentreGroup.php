<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class BdcSectorExpenditureSubCentreGroup extends Model
{
    public $timestamps=true;
    protected $table = 'bdc_sector_expenditure_sub_center_groups';
    //use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";

    protected $fillable = ['name'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'bdc_group_id' => 'required|integer',
                'bdc_sector_expenditure_sub_centre_id'  => 'required|integer',
            ],
            $merge);
    }

    public function sector(){
        return $this->belongsTo('App\Models\Setup\Sector','sector_id','id');
    }

    public function bdc_group(){
        return $this->belongsTo('App\Models\Setup\BdcGroup','bdc_group_id','id');
    }

    public function bdc_sector_expenditure_sub_centre(){
        return $this->belongsTo('App\Models\Setup\SectorExpenditureSubCentre','bdc_sector_expenditure_sub_centre_id','id');
    }
}
