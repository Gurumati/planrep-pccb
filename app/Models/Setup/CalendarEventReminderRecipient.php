<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CalendarEventReminderRecipient extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "calendar_event_reminder_recipients";

    public function calendar_event(){
        return $this->belongsTo('App\Models\Setup\CalendarEvent','calendar_event_id','id');
    }
}
