<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VersionType extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'version_types';
    protected $fillable = ['name','code'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string',
                'code' => 'required|unique:version_types,code' . ($id ? ",$id" : ''),
            ],
            $merge);
    }
}
