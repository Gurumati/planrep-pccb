<?php

namespace App\Models\Setup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CasPlanTableColumn extends Model
{
    public $timestamps = true;
    // use SoftDeletes;
    protected $table = "cas_plan_table_columns";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $with = ['casPlanTableColumns','children'];

    public function casPlanTableColumns(){
        return $this->belongsTo('App\Models\Setup\CasPlanTable');
    }

    public function parent(){
        return $this->belongsTo('App\Models\Setup\CasPlanTableColumn','cas_plan_table_column_id');
    }

    public function children(){
        return $this->hasMany('App\Models\Setup\CasPlanTableColumn','cas_plan_table_column_id')->orderBy('sort_order','asc');
    }

    public function casPlanTableItemValues() {
        return $this->hasMany('App\Models\Setup\CasPlanTableItemValue');
    }

    public function columnOptions(){
        return $this->hasMany('App\Models\Setup\CasPlanTablesSelectOption');
    }

}
