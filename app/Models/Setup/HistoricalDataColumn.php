<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class HistoricalDataColumn extends Model
{
    //use SoftDeletes;

    public $timestamps = true;
    protected $table = 'historical_data_columns';
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $fillable = ['name', 'code', 'sort_order','historical_data_table_id'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'name' => 'required|string',
                'code' => 'required|unique:historical_data_columns,code' . ($id ? ",$id" : ''),
                'sort_order' => 'integer|nullable',
                'historical_data_table_id' => 'integer|required',
            ],
            $merge);
    }

    public function data_table() {
        return $this->belongsTo('App\Models\Setup\HistoricalDataTable','historical_data_table_id','id');
    }
}
