<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class ProjectOutput extends Model
{

    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "project_outputs";

    protected $fillable = ['name','sector_id','expenditure_category_id'];

    public static function rules($id = 0, $merge = []) {
        return array_merge(
            [
                'name' => 'required|string|max:255',
                'expenditure_category_id' => 'required|integer',
            ],
            $merge);
    }

    public function expenditure_category(){
        return $this->belongsTo('App\Models\Setup\ExpenditureCategory','expenditure_category_id','id');
    }

    public function sector(){
        return $this->belongsTo('App\Models\Setup\Sector','sector_id','id');
    }
}
