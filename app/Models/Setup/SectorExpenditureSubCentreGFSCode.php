<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;

class SectorExpenditureSubCentreGfsCode extends Model
{
    public $timestamps=true;
    protected $table = 'bdc_sector_expenditure_sub_centre_gfs_codes';
    //use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";

    public function gfs_code(){
        return $this->belongsTo('App\Models\Setup\GfsCode','gfs_code_id','id');
    }

    public function sector_expenditure_sub_centre(){
        return $this->belongsTo('App\Models\Setup\SectorExpenditureSubCentre','bdc_sector_expenditure_sub_centre_id','id');
    }
}
