<?php

namespace App\Models\Setup;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CasPlanContent extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'cas_plan_contents';
    protected $with = ['children','casPlanTableDTO','casPlan'];

    public function children() {
        return $this->hasMany('App\Models\Setup\CasPlanContent', 'cas_plan_content_id')->orderBy('sort_order','ASC');
    }

    public function casPlanContent() {
        return $this->belongsTo('App\Models\Setup\CasPlanContent');
    }

    public function casPlan() {
        return $this->belongsTo('App\Models\Setup\CasPlan');
    }

    public function casPlanTable() {
        return $this->hasMany('App\Models\Setup\CasPlanTable');
    }
    public function casPlanTableDTO() {
        return $this->hasMany('App\DTOs\SetupDTOs\CasPlanTableDTO');
    }

    public function casAssessmentSubCriteriaReportSets() {
        return $this->hasMany('App\Models\Assessment\CasAssessmentSubCriteriaReportSet');
    }
}
