<?php

namespace App\Models\Setup;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Flatten;
use App\Models\Planning\MtefSection;
use Illuminate\Support\Facades\Log;
use App\Http\Services\UserServices;


class PlanChain extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'plan_chains';

    protected $with = ['plan_chain_type', 'parent'];

    public static function getByTypeAndSection($planChainTypeId, $mtefSectionId)
    {

    }

    public static function getNextFinancialYearVersion($currentFinancialYearId, $planChainId){

        $currentFinancialYear = FinancialYear::find($currentFinancialYearId);
        $nextFinancialYear = FinancialYear::where('start_date','>', $currentFinancialYear->end_date)->orderBy('start_date')->first();
        $nextVersion =[];
        if($nextFinancialYear == null){
            return $nextVersion;
        }
        $nextVersion['financialYear'] = $nextFinancialYear->name;
        $version = DB::table('financial_year_versions as fv')
            ->join('versions as v','v.id','fv.version_id')
            ->join('version_types as vt','vt.id','v.version_type_id')
            ->where('vt.code','PC')
            ->where('fv.financial_year_id',$nextFinancialYear->id)
            ->select('v.*')
            ->first();
        if($version == null){
            return $nextVersion;
        }
        $exist = PlanChainVersion::where('version_id',$version->id)->where('plan_chain_id',$planChainId)->first();
        $nextVersion['version'] = $version->name;
        $nextVersion['id'] = $version->id;
        $nextVersion['planChainVersionId'] = isset($exist)?$exist->id:null;

        return $nextVersion;
    }

    public static function addToVersion($planChainId, $versionId){
        $existing = PlanChainVersion::where('plan_chain_id',$planChainId)->where('version_id',$versionId)->first();
        if(!is_null($existing)){
            return ['planChainVersionId'=>$existing->id];
        }
        $version = PlanChainVersion::create(['plan_chain_id'=>$planChainId,'version_id'=>$versionId]);
        return ['planChainVersionId'=>$version->id];
    }

    public static function getTopLevel()
    {

        $planChains = null;
        $reallocation = Input::get('reallocation');
        $mtefSectionId = Input::get('mtefSectionId');
        $adminhierarchyId = UserServices::getUser()->admin_hierarchy_id;

         if($reallocation === 'true'){
            $planChains = PlanChain::whereNull('parent_id')
            ->select('id', 'description', 'code')
            ->where('admin_hierarchy_id',$adminhierarchyId)
            ->orWhere('admin_hierarchy_id',null)
            ->orderBy('code')
            ->get();
        }else{
            if(isset($mtefSectionId)){
                $financialYearId = DB::table('mtef_sections as ms')
                    ->join('mtefs as m','m.id','ms.mtef_id')
                    ->where('ms.id',$mtefSectionId)
                    ->select('m.*')
                    ->first()->financial_year_id;

                $versionId = Version::getVersionByFinancialYear($financialYearId,'PC');
            }
            else{
                $versionId = Version::getCurrentVersionId('PC');
            }

            $itemIds = Version::getVesionItemIds($versionId, 'PC');
            $mtefSectionId = Input::get('mtefSectionId');


            $planChains = DB::table('plan_chains as pc')
                ->where(function ($p) use ($mtefSectionId,$adminhierarchyId) {
                    if (isset($mtefSectionId)) {
                        $p->where('admin_hierarchy_id',$adminhierarchyId)
                        ->orWhereNull('admin_hierarchy_id');
                    //$p->whereIn('pc.id', PlanChain::getPlanChainIdsByTarget($mtefSectionId));
                    }
                })
                ->whereNull('pc.parent_id')
                ->where('pc.is_active', true)
                ->where('pc.code', '!=', '0')
                ->whereIn('pc.id', $itemIds)
                ->orderBy('pc.code')
                ->select('pc.id', 'pc.description', 'pc.code')
                ->get();
        }
        return $planChains;

    }


    public static function getPlanChainIdsByTarget($mtefSectionId)
    {

        $mtefSection = MtefSection::find($mtefSectionId);
        $sectionIds = Section::getParentSectionIds($mtefSection->section_id);

        $planChainIds = DB::table('mtef_annual_targets as mat')
            ->join('long_term_targets as lt', 'lt.id', 'mat.long_term_target_id')
            ->where('mat.mtef_id', $mtefSection->mtef_id)
            ->whereIn('mat.section_id', $sectionIds)
            ->pluck('lt.plan_chain_id')->toArray();

        $flatten = new Flatten();
        $planChainWithParentTree = PlanChainParent::whereIn('id', $planChainIds)->get();
        return $flatten->flattenPlanChainWithParent($planChainWithParentTree);

    }


    public static function getChildren($parentId)
    {

        $adminHierarchyId = UserServices::getUser()->admin_hierarchy_id;
        $mtefSectionId = Input::get('mtefSectionId');
        if(isset($mtefSectionId)){
            $financialYearId = DB::table('mtefs as m')
            ->join('mtef_sections as ms','m.id','ms.mtef_id')
            ->where('ms.id',$mtefSectionId)
            ->select('m.*')->first()->financial_year_id;
            $versionId = Version::getVersionByFinancialYear($financialYearId,'PC');
        }
        else{
            $versionId = Version::getCurrentVersionId('PC');
        }
        $itemIds = Version::getVesionItemIds($versionId, 'PC');

        $planChains = DB::table('plan_chains as pc')
            ->where(function ($p) use ($mtefSectionId,$adminHierarchyId) {
                if (isset($mtefSectionId)) {
                    $p->where('pc.admin_hierarchy_id',$adminHierarchyId);
                    // $p->whereIn('pc.id', PlanChain::getPlanChainIdsByTarget($mtefSectionId));
                }
            })
            ->where('pc.parent_id', $parentId)
            ->where('pc.is_active', true)
            ->whereIn('pc.id', $itemIds)
            ->orderBy('pc.code')
            ->select('pc.id', 'pc.description', 'pc.code')
            ->get();
        return $planChains;

    }

    public function children()
    {
        return $this->hasMany('App\Models\Setup\PlanChain', 'parent_id');
    }

    public function plan_chain_sectors()
    {
        return $this->hasMany('App\Models\Setup\PlanChainSector');
    }

    public function plan_chain_versions()
    {
        return $this->hasMany('App\Models\Setup\PlanChainVersion', 'plan_chain_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Setup\PlanChain', 'parent_id')->select('id', 'code', 'description', 'parent_id');
    }

    public function longTermTargets()
    {
        return $this->hasMany('App\Models\Planning\LongTermTarget');
    }

    public function performance_indicators()
    {
        return $this->hasMany('App\Models\Setup\PerformanceIndicator')->select('id','description','plan_chain_id');
    }

    public function planChainType()
    {
        return $this->belongsTo('App\Models\Setup\PlanChainType');
    }

    public function plan_chain_type()
    {
        return $this->belongsTo('App\Models\Setup\PlanChainType')->select('id', 'name');
    }

}
