<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessageFile extends Model
{
    public $timestamps=true;
    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";

    public function message(){
        return $this->belongsTo('App\Models\User\Message');
    }
}
