<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Budget extends Model {

    public static function summary($financial_year_id, $groupByOption, $admin_hierarchy_id = null, $sector_id = null, $section_id = null) {
        //NATIONAL REPORTS
        //1. SUMMARY LGAS
        if ($groupByOption == 1) {
            $sql = "SELECT admin_hierarchies.name as council,
       sum((activity_facility_fund_source_inputs.quantity*activity_facility_fund_source_inputs.unit_price)*activity_facility_fund_source_inputs.frequency) as amount
        from budget_classes join activities on activities.budget_class_id = budget_classes.id
          join activity_facilities on activity_facilities.activity_id =  activities.id
          join activity_facility_fund_sources on activity_facilities.id = activity_facility_fund_sources.activity_facility_id
          join activity_facility_fund_source_inputs on activity_facility_fund_sources.id = activity_facility_fund_source_inputs.activity_facility_fund_source_id
          join mtef_sections on mtef_sections.id = activities.mtef_section_id
          join mtefs on mtefs.id = mtef_sections.mtef_id
          join sections on sections.id = mtef_sections.section_id
          join admin_hierarchies on admin_hierarchies.id = mtefs.admin_hierarchy_id
          join sectors on sectors.id = sections.sector_id
        where mtefs.financial_year_id = '$financial_year_id'";
            if($admin_hierarchy_id != null){
                $sql .= "AND admin_hierarchies.id IN($admin_hierarchy_id)";
            }
            $sql .= "GROUP BY admin_hierarchies.id";
            $all = DB::select($sql);
            return $all;
        }
        //2. GROUP BY ADMIN_HIERARCHIES
        if ($groupByOption == 2) {
            $sql = "SELECT admin_hierarchies.name as council,budget_classes.id,budget_classes.name as budget_class,
                         sum((activity_facility_fund_source_inputs.quantity*activity_facility_fund_source_inputs.unit_price)*activity_facility_fund_source_inputs.frequency) as amount
            from budget_classes join activities on activities.budget_class_id = budget_classes.id
              join activity_facilities on activity_facilities.activity_id =  activities.id
              join activity_facility_fund_sources on activity_facilities.id = activity_facility_fund_sources.activity_facility_id
              join activity_facility_fund_source_inputs on activity_facility_fund_sources.id = activity_facility_fund_source_inputs.activity_facility_fund_source_id
              join mtef_sections on mtef_sections.id = activities.mtef_section_id
              join mtefs on mtefs.id = mtef_sections.mtef_id
              join sections on sections.id = mtef_sections.section_id
              join admin_hierarchies on admin_hierarchies.id = mtefs.admin_hierarchy_id
              join sectors on sectors.id = sections.sector_id
            where mtefs.financial_year_id = '$financial_year_id'";
            if($admin_hierarchy_id != null){
                //$sql .= "AND admin_hierarchies.id = '$admin_hierarchy_id'";
                $sql .= "AND admin_hierarchies.id IN($admin_hierarchy_id)";
            }
            $sql .= "GROUP BY admin_hierarchies.id,budget_classes.id";
            $all = DB::select($sql);
            return $all;
        }
        //3. SECTOR - NATIONAL
        if ($groupByOption == 3) {
            $sql = "SELECT sectors.name as sector,
                         sum((activity_facility_fund_source_inputs.quantity*activity_facility_fund_source_inputs.unit_price)*activity_facility_fund_source_inputs.frequency) as amount
            from budget_classes join activities on activities.budget_class_id = budget_classes.id
              join activity_facilities on activity_facilities.activity_id =  activities.id
              join activity_facility_fund_sources on activity_facilities.id = activity_facility_fund_sources.activity_facility_id
              join activity_facility_fund_source_inputs on activity_facility_fund_sources.id = activity_facility_fund_source_inputs.activity_facility_fund_source_id
              join mtef_sections on mtef_sections.id = activities.mtef_section_id
              join mtefs on mtefs.id = mtef_sections.mtef_id
              join sections on sections.id = mtef_sections.section_id
              join admin_hierarchies on admin_hierarchies.id = mtefs.admin_hierarchy_id
              join sectors on sectors.id = sections.sector_id
            where mtefs.financial_year_id = '$financial_year_id'";
            if($admin_hierarchy_id != null){
                //$sql .= "AND admin_hierarchies.id = '$admin_hierarchy_id'";
                $sql .= "AND admin_hierarchies.id IN($admin_hierarchy_id)";
            }
            $sql .= "GROUP BY sectors.id";
            $all = DB::select($sql);
            return $all;
        }
        //4. SECTOR - LGA
        if ($groupByOption == 4) {
            $sql = "SELECT sectors.name as sector,admin_hierarchies.name as council,
                         sum((activity_facility_fund_source_inputs.quantity*activity_facility_fund_source_inputs.unit_price)*activity_facility_fund_source_inputs.frequency) as amount
            from budget_classes join activities on activities.budget_class_id = budget_classes.id
              join activity_facilities on activity_facilities.activity_id =  activities.id
              join activity_facility_fund_sources on activity_facilities.id = activity_facility_fund_sources.activity_facility_id
              join activity_facility_fund_source_inputs on activity_facility_fund_sources.id = activity_facility_fund_source_inputs.activity_facility_fund_source_id
              join mtef_sections on mtef_sections.id = activities.mtef_section_id
              join mtefs on mtefs.id = mtef_sections.mtef_id
              join sections on sections.id = mtef_sections.section_id
              join admin_hierarchies on admin_hierarchies.id = mtefs.admin_hierarchy_id
              join sectors on sectors.id = sections.sector_id
            where mtefs.financial_year_id = '$financial_year_id'";
            if($admin_hierarchy_id != null){
                //$sql .= "AND admin_hierarchies.id = '$admin_hierarchy_id'";
                $sql .= "AND admin_hierarchies.id IN($admin_hierarchy_id)";
            }
            $sql .= "GROUP BY sectors.id,admin_hierarchies.id";
            $all = DB::select($sql);
            return $all;
        }
        //5. SECTOR - LGA
        if ($groupByOption == 5) {
            $sql = "SELECT sectors.name as sector,admin_hierarchies.name as council,budget_classes.name as budget_class,
                         sum((activity_facility_fund_source_inputs.quantity*activity_facility_fund_source_inputs.unit_price)*activity_facility_fund_source_inputs.frequency) as amount
                from budget_classes join activities on activities.budget_class_id = budget_classes.id
                  join activity_facilities on activity_facilities.activity_id =  activities.id
                  join activity_facility_fund_sources on activity_facilities.id = activity_facility_fund_sources.activity_facility_id
                  join activity_facility_fund_source_inputs on activity_facility_fund_sources.id = activity_facility_fund_source_inputs.activity_facility_fund_source_id
                  join mtef_sections on mtef_sections.id = activities.mtef_section_id
                  join mtefs on mtefs.id = mtef_sections.mtef_id
                  join sections on sections.id = mtef_sections.section_id
                  join admin_hierarchies on admin_hierarchies.id = mtefs.admin_hierarchy_id
                  join sectors on sectors.id = sections.sector_id
                where mtefs.financial_year_id = '$financial_year_id'";
            if ($admin_hierarchy_id != null) {
                //$sql .= "AND admin_hierarchies.id = '$admin_hierarchy_id'";
                $sql .= "AND admin_hierarchies.id IN($admin_hierarchy_id)";
            }
            $sql .= "GROUP BY sectors.id,admin_hierarchies.id,budget_classes.id";
            $all = DB::select($sql);
            return $all;
        }
        //6. SECTOR - COST CENTRE
        if ($groupByOption == 6) {
            $sql = "SELECT sectors.name as sector,sections.name as cost_centre,
                   sum((activity_facility_fund_source_inputs.quantity*activity_facility_fund_source_inputs.unit_price)*activity_facility_fund_source_inputs.frequency) as amount
            from budget_classes join activities on activities.budget_class_id = budget_classes.id
              join activity_facilities on activity_facilities.activity_id =  activities.id
              join activity_facility_fund_sources on activity_facilities.id = activity_facility_fund_sources.activity_facility_id
              join activity_facility_fund_source_inputs on activity_facility_fund_sources.id = activity_facility_fund_source_inputs.activity_facility_fund_source_id
              join mtef_sections on mtef_sections.id = activities.mtef_section_id
              join mtefs on mtefs.id = mtef_sections.mtef_id
              join sections on sections.id = mtef_sections.section_id
              join admin_hierarchies on admin_hierarchies.id = mtefs.admin_hierarchy_id
              join sectors on sectors.id = sections.sector_id
            where mtefs.financial_year_id = '$financial_year_id'";
            if ($admin_hierarchy_id != null) {
                //$sql .= "AND admin_hierarchies.id = '$admin_hierarchy_id'";
                $sql .= "AND admin_hierarchies.id IN($admin_hierarchy_id)";
            }
            $sql .= "GROUP BY sections.id,sectors.id";
            $all = DB::select($sql);
            return $all;
        }
        //7. LGA - SECTOR - COST CENTRE
        if ($groupByOption == 7) {
            $sql = "SELECT admin_hierarchies.name as council,sectors.name as sector,sections.name as cost_centre,
                   sum((activity_facility_fund_source_inputs.quantity*activity_facility_fund_source_inputs.unit_price)*activity_facility_fund_source_inputs.frequency) as amount
            from budget_classes join activities on activities.budget_class_id = budget_classes.id
              join activity_facilities on activity_facilities.activity_id =  activities.id
              join activity_facility_fund_sources on activity_facilities.id = activity_facility_fund_sources.activity_facility_id
              join activity_facility_fund_source_inputs on activity_facility_fund_sources.id = activity_facility_fund_source_inputs.activity_facility_fund_source_id
              join mtef_sections on mtef_sections.id = activities.mtef_section_id
              join mtefs on mtefs.id = mtef_sections.mtef_id
              join sections on sections.id = mtef_sections.section_id
              join admin_hierarchies on admin_hierarchies.id = mtefs.admin_hierarchy_id
              join sectors on sectors.id = sections.sector_id
            where mtefs.financial_year_id = '$financial_year_id'";
            if ($admin_hierarchy_id != null) {
                //$sql .= "AND admin_hierarchies.id = '$admin_hierarchy_id'";
                $sql .= "AND admin_hierarchies.id IN($admin_hierarchy_id)";
            }
            $sql .= "GROUP BY sections.id,sectors.id,admin_hierarchies.id";
            $all = DB::select($sql);
            return $all;
        }
        //8. LGA - BUDGET CLASS - SECTOR - COST CENTRE
        if ($groupByOption == 8) {
            $sql = "SELECT admin_hierarchies.name as council,budget_classes.name as budget_class,sectors.name as sector,sections.name as cost_centre,
                   sum((activity_facility_fund_source_inputs.quantity*activity_facility_fund_source_inputs.unit_price)*activity_facility_fund_source_inputs.frequency) as amount
            from budget_classes join activities on activities.budget_class_id = budget_classes.id
              join activity_facilities on activity_facilities.activity_id =  activities.id
              join activity_facility_fund_sources on activity_facilities.id = activity_facility_fund_sources.activity_facility_id
              join activity_facility_fund_source_inputs on activity_facility_fund_sources.id = activity_facility_fund_source_inputs.activity_facility_fund_source_id
              join mtef_sections on mtef_sections.id = activities.mtef_section_id
              join mtefs on mtefs.id = mtef_sections.mtef_id
              join sections on sections.id = mtef_sections.section_id
              join admin_hierarchies on admin_hierarchies.id = mtefs.admin_hierarchy_id
              join sectors on sectors.id = sections.sector_id
            where mtefs.financial_year_id = '$financial_year_id'";
            if ($admin_hierarchy_id != null) {
                //$sql .= "AND admin_hierarchies.id = '$admin_hierarchy_id'";
                $sql .= "AND admin_hierarchies.id IN($admin_hierarchy_id)";
            }
            $sql .= "GROUP BY sections.id,sectors.id,admin_hierarchies.id,budget_classes.id";
            $all = DB::select($sql);
            return $all;
        }
    }

    public static function summaryV2($financial_year_id,$admin_hierarchy_id,$budget_class,$type){
        if($type == 1){
            //revenue
            switch ($budget_class){
                case 1:
                    $sql = "select sq.sector,max(sq.own_source) as own_source,max(sq.grants) as grants,
                 max(sq.own_source_actual) as own_source_actual,
                 max(sq.grants_actual) as grants_actual FROM
                  (select dept.name as sector,bc.name as budget_class,
                          case when bc.code = '101' then sum(inp.unit_price*inp.quantity*inp.frequency) else 0 END as own_source,
                          case when bc.code in('102','105','106','107','108','109','110','111') then sum(inp.unit_price*inp.quantity*inp.frequency) else 0 END as grants,
                          case when bc.code = '101' then sum(revenue.amount) else 0 END as own_source_actual,
                          case when bc.code in('102','105','106','107','108','109','110','111') then sum(revenue.amount) else 0 END as grants_actual
                   from activity_facility_fund_source_inputs inp
                     join activity_facility_fund_sources afs on inp.activity_facility_fund_source_id = afs.id
                     join fund_sources f on f.id = afs.fund_source_id
                     join fund_source_budget_classes fbc on f.id = fbc.fund_source_id
                     join budget_classes bc on bc.id = fbc.budget_class_id
                     join budget_classes bcp on bcp.id = bc.parent_id
                     join activity_facilities af ON afs.activity_facility_id = af.id
                     join activities a ON af.activity_id = a.id
                     join mtef_sections mse on mse.id = a.mtef_section_id
                     join sections s on s.id = mse.section_id
                     join sections sv on sv.id = s.parent_id
                     join sections dept on dept.id = sv.parent_id
                     join mtefs m on m.id = mse.mtef_id
                     join admin_hierarchies ah on ah.id = m.admin_hierarchy_id
                     left join (
                                 select ahc.admin_hierarchy_id,
                                   dept.id as department,
                                   sum(rfi.amount) as amount
                                 from received_fund_items rfi
                                   join admin_hierarchy_ceilings ahc on rfi.admin_hierarchy_ceiling_id = ahc.id
                                   join fund_sources fs on fs.id = rfi.fund_source_id
                                   join fund_source_budget_classes fbc on fbc.fund_source_id = fs.id
                                   join budget_classes bc on bc.id = fbc.budget_class_id
                                   join budget_classes bcp on bcp.id = bc.parent_id
                                   join sections dept on dept.id = ahc.section_id
                                   join section_levels sl on sl.id = dept.section_level_id
                                 where ahc.admin_hierarchy_id = '$admin_hierarchy_id'
                                       and ahc.financial_year_id = '$financial_year_id'
                                       and sl.code = 'D001'
                                 GROUP BY ahc.admin_hierarchy_id,dept.id
                               ) revenue on (revenue.admin_hierarchy_id = ah.id and revenue.department = dept.id)
                   where m.financial_year_id = '$financial_year_id' and m.admin_hierarchy_id = '$admin_hierarchy_id'
                         and bcp.code in ('100')
                   GROUP BY dept.id,bc.id) sq
                GROUP BY sq.sector";
                    return DB::select($sql);
                    /*other charges*/
                    break;
                case 2:
                    $sql = "SELECT
                      y.sector,
                      MAX(y.own_source) as own_source,
                      MAX(y.grants) as grants,
                      MAX(0) as own_source_actual,
                      MAX(0) as grants_actual
                    FROM
                    (SELECT
                      x.sector,
                      CASE WHEN x.fund_source_code = '10A'
                        THEN MAX(x.pe) + max(x.contribution)
                      ELSE 0 END AS own_source,
                    
                      CASE WHEN x.fund_source_code = '20V'
                      THEN MAX(x.pe)
                      ELSE 0 END AS grants
                    FROM
                      (SELECT
                         a.id,
                         dpt.name   AS sector,
                         f.name     AS fund_source,
                         f.code     AS fund_source_code,
                         CASE WHEN bf.column_number = 5
                           THEN sum(replace(bv.field_value :: TEXT, ',' :: TEXT, '' :: TEXT)
                                    :: NUMERIC :: DOUBLE PRECISION)
                         ELSE 0 END AS pe,
                         CASE WHEN bf.column_number = 14
                           THEN sum(replace(bv.field_value :: TEXT, ',' :: TEXT, '' :: TEXT)
                                    :: NUMERIC :: DOUBLE PRECISION)
                         ELSE 0 END AS contribution
                       FROM admin_hierarchies a
                         JOIN mtefs m ON a.id = m.admin_hierarchy_id
                         JOIN mtef_sections ms ON m.id = ms.mtef_id
                         JOIN activities ac ON ac.mtef_section_id = ms.id
                         JOIN budget_submission_lines bl ON bl.activity_id = ac.id
                         JOIN sections s ON bl.section_id = s.id
                         JOIN sections sv ON sv.id = s.parent_id
                         JOIN sections dpt ON dpt.id = s.parent_id
                         JOIN budget_submission_line_values bv ON bv.budget_submission_line_id = bl.id
                         JOIN budget_submission_sub_forms bs ON bs.id = bl.budget_submission_sub_form_id
                         JOIN budget_submission_forms bsf ON bsf.id = bs.budget_submission_form_id
                         JOIN budget_submission_definitions bf ON bf.id = bv.budget_submission_definition_id
                         JOIN fund_sources f ON f.id = bl.fund_source_id
                       WHERE (bl.budget_submission_sub_form_id = ANY (ARRAY [7, 8, 9])) AND
                             a.id = '$admin_hierarchy_id' AND
                             m.financial_year_id = '$financial_year_id' AND
                             (bf.column_number = ANY (ARRAY [5, 14]))
                       GROUP BY a.id,
                         f.id,
                         bf.id,
                         dpt.id
                       ORDER BY
                         f.id, dpt.name) x
                    GROUP BY
                      x.sector,
                      x.fund_source,
                      x.fund_source_code) y
                    GROUP BY  y.sector";
                    return DB::select($sql);
                    break;
                case 3:
                    $sql = "select sq.sector,max(sq.own_source) as own_source,max(sq.grants) as grants,
                          max(sq.own_source_actual) as own_source_actual,
                          max(sq.grants_actual) as grants_actual FROM
                        (select dept.name as sector,bc.name as budget_class,
                                case when bc.code = '201' then sum(inp.unit_price*inp.quantity*inp.frequency) else 0 END as own_source,
                                case when bc.code <> '201' then sum(inp.unit_price*inp.quantity*inp.frequency) else 0 END as grants,
                                case when bc.code = '201' then sum(revenue.amount) else 0 END as own_source_actual,
                                case when bc.code <> '201' then sum(revenue.amount) else 0 END as grants_actual
                         from activity_facility_fund_source_inputs inp
                           join activity_facility_fund_sources afs on inp.activity_facility_fund_source_id = afs.id
                           join fund_sources f on f.id = afs.fund_source_id
                           join fund_source_budget_classes fbc on f.id = fbc.fund_source_id
                           join budget_classes bc on bc.id = fbc.budget_class_id
                           join budget_classes bcp on bcp.id = bc.parent_id
                           join activity_facilities af ON afs.activity_facility_id = af.id
                           join activities a ON af.activity_id = a.id
                           join mtef_sections mse on mse.id = a.mtef_section_id
                           join sections s on s.id = mse.section_id
                           join sections sv on sv.id = s.parent_id
                           join sections dept on dept.id = sv.parent_id
                           join mtefs m on m.id = mse.mtef_id
                           join admin_hierarchies ah on ah.id = m.admin_hierarchy_id
                           left join (
                                     select ahc.admin_hierarchy_id,
                                            dept.id as department,
                                            sum(rfi.amount) as amount
                                     from received_fund_items rfi
                                       join admin_hierarchy_ceilings ahc on rfi.admin_hierarchy_ceiling_id = ahc.id
                                       join fund_sources fs on fs.id = rfi.fund_source_id
                                       join fund_source_budget_classes fbc on fbc.fund_source_id = fs.id
                                       join budget_classes bc on bc.id = fbc.budget_class_id
                                       join budget_classes bcp on bcp.id = bc.parent_id
                                       join sections dept on dept.id = ahc.section_id
                                       join section_levels sl on sl.id = dept.section_level_id
                                     where ahc.admin_hierarchy_id = '$admin_hierarchy_id'
                                           and ahc.financial_year_id = '$financial_year_id'
                                           and sl.code = 'D001'
                                     GROUP BY ahc.admin_hierarchy_id,dept.id
                                     ) revenue on (revenue.admin_hierarchy_id = ah.id and revenue.department = dept.id)
                         where m.financial_year_id = '$financial_year_id' and m.admin_hierarchy_id = '$admin_hierarchy_id'
                               and bcp.code in ('200')
                         GROUP BY dept.id,bc.id) sq
                        GROUP BY sq.sector";
                    return DB::select($sql);
                    break;
                default:
                    //other charges
            }
        }
        if($type == 2){
            //expenditure
            switch ($budget_class){
                case 1:
                    //other charges
                    break;
                case 2:
                    //personal emolument
                    break;
                case 3:
                    //development
                    break;
                default:
                    //other charges
            }
        }
    }
}
