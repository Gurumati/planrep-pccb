<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class HistoricalDataItem extends Model
{
    public $timestamps = true;
    protected $table = 'historical_data_items';
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $fillable = ['row_id', 'column_id','value','financial_year_id','admin_hierarchy_id','section_id','fund_source_id'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'row_id' => 'required|integer',
                'column_id' => 'required|integer',
                'value' => 'required|numeric',
                'financial_year_id' => 'integer|required',
                'admin_hierarchy_id' => 'integer|required',
                'section_id' => 'integer|nullable',
                'fund_source_id' => 'integer|nullable',
            ],
            $merge);
    }

    public function row() {
        return $this->belongsTo('App\Models\Setup\HistoricalDataRow','row_id','id');
    }

    public function column() {
        return $this->belongsTo('App\Models\Setup\HistoricalDataColumn','column_id','id');
    }

    public function financial_year() {
        return $this->belongsTo('App\Models\Setup\FinancialYear','financial_year_id','id');
    }

    public function admin_hierarchy() {
        return $this->belongsTo('App\DTOs\SetupDTOs\AdminHierarchyDTO','admin_hierarchy_id','id');
    }
}
