<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PeSubmissionFormsReport extends Model
{
    public $timestamps=true;
    use SoftDeletes;
    protected $dates=['deleted_at'];
    protected $dateFormat="Y-m-d H:i:s";
    protected $table = 'pe_submission_forms_reports';
}
