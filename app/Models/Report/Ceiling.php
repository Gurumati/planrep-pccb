<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Ceiling extends Model {
    public static function ceiling($admin_hierarchies, $financial_year_id) {
        $sql = "select h.name admin_hierarchy,sect.name sector, dep.name department,cs.name cost_centre,b.name budget_class, f.name fund_source,g.code gfs_code,g.name gfs_name,
        ac.amount amount
        from ceilings c
        inner join admin_hierarchy_ceilings ac  on c.id = ac.ceiling_id
        inner join gfs_codes g on g.id = c.gfs_code_id
        inner join fund_sources f on f.id = g.fund_source_id
        inner join admin_hierarchies h on h.id = ac.admin_hierarchy_id
        inner join budget_classes b on b.id = c.budget_class_id
        inner join (select s.* from  sections s inner join section_levels l on s.section_level_id = l.id where l.hierarchy_position = 4) cs
        on cs.id = ac.section_id
        inner join sections dep on dep.id = cs.parent_id
        inner join sections sect on sect.id = dep.parent_id
        where ac.amount > 0 AND ac.admin_hierarchy_id IN ($admin_hierarchies) and f.can_project = false AND ac.financial_year_id = '$financial_year_id'";
        $all = DB::select($sql);
        return $all;
    }
}
