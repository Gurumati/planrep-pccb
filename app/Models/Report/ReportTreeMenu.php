<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Model;

class ReportTreeMenu extends Model {
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'report_tree_menu';

    public function parent() {
        return $this->belongsToMany('App\Models\Report\ReportTreeMenu', 'ParentID');
    }

    public function children() {
        return $this->hasMany('App\Models\Report\ReportTreeMenu', 'ParentID');
    }
}
