<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DateUtility extends Model
{
    public function getDateRange($periodType, $steps){
        $steps++;
        switch($periodType){
        case "financialJuly":
            return $this->getPeriodPastFinancialYears($steps);
            break;
        case "monthly":
            return $this->getPeriodPastMonths($steps);
            break;
        case "weekly":
            return $this->getPeriodPastWeeks($steps);
            break;
        case "daily":
            return $this->getPeriodPastDays($steps);
            break;
        default:
            throw new \Exception('Failed to resolve date with the supplied period');
        }
    }

    /**
     * calculates date by using today and number of weeks to go back
     *
     * @param int $periods
     *
     * @return stdClass
     */
    public function getPeriodPastWeeks($periods = 1)
    {
        $today = date('Y-m-d');
        $date_obj = date_create($today);
        date_sub($date_obj, date_interval_create_from_date_string("$periods weeks"));
        $dates_array = $this->startAndEndOfWeek($date_obj->format('Y-m-d'));
        $result = $this->getObject($dates_array[0],$dates_array[1]);
        return $result;
    }

    /**
     * calculates date by using today and number of days to go back
     *
     * @param int $periods
     *
     * @return stdClass
     */
    public function getPeriodPastDays($periods = 1)
    {
        $today = date('Y-m-d');
        $date_obj = date_create($today);
        date_sub($date_obj, date_interval_create_from_date_string("$periods days"));
        $date = $date_obj->format('Y-m-d');
        $result = $this->getObject($date, $date);
        return $result;
    }

    public function getPeriodPastMonths($periods = 1)
    {
        $today = date('Y-m-d');
        $date_obj = date_create($today);
        date_sub($date_obj, date_interval_create_from_date_string("$periods months"));
        $date = $date_obj->format('Y-m-d');
        $dates_array = $this->startAndEndOfMonth($date);
        $result = $this->getObject($dates_array[0],$dates_array[1]);
        return $result;
    }

    /**
     * calculates date by using today and number of days to go back
     *
     * @param int $periods
     *
     * @return stdClass
     */
    public function getPeriodPastFinancialYears($periods = 0)
    {
        $today = date('Y-m-d');
        $date_obj = date_create($today);
        date_sub($date_obj, date_interval_create_from_date_string("$periods years"));
        $date = $date_obj->format('Y-m-d');
        $dates_array = $this->startAndEndOfFinancialYear($date);
        $result = $this->getObject($dates_array[0], $dates_array[1]);
        return $result;
    }

    /**
     * format the found dates as php std objec
     *  @param Date $start_date;
     * @param Date $end_date;
     * @return stdClass Ojbect
     *
     */
    public function getObject($startDate, $endDate)
    {
        $model = new \stdClass();
        $model->start_date = $startDate;
        $model->end_date = $endDate;
        return $model;
    }
    /**
     * Determine start and end of financial year.
     * @param string $date
     */
    public function startAndEndOfFinancialYear($date)
    {
        //determine the start and end of year
        //$date = $date->format('Y-m-d');
        $date_array = explode('-', $date);
        $datetime = strtotime($date);

        if($date_array[1] * 1  > 6){
            //between july and dec
            //use this year as start and next year as end
            $start_date =  date('Y-m-d',strtotime('this year july 1st',$datetime));
            $end_date = date('Y-m-d',strtotime('next year june 30th',$datetime));
        }else{
            //use this year as  end prev year as start.
            $start_date =  date('Y-m-d',strtotime('last year july 1st',$datetime));
            $end_date = date('Y-m-d',strtotime('this year june 30th',$datetime));
        }
        return array($start_date,$end_date);
    }

    public function startAndEndOfMonth($date)
    {
        //determine the start and end of thi month
        $datetime = strtotime($date);

        $start_date = date('01-m-Y', $datetime);
        $end_date = date('t-m-Y', $datetime);
        return array($start_date,$end_date);
    }

    public function startAndEndOfWeek($date)
    {
        //$date = $date->format('Y-m-d');
        $ts = strtotime($date);
        $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
        return array(date('Y-m-d', $start),
            date('Y-m-d', strtotime('next saturday', $start)));
    }

    public function startAndEndOfQuarter($date)
    {
        //determine the start and end of year
    }

    public function startAndEndOfSemiYear($date)
    {
        //determine the start and end of year
    }
}

