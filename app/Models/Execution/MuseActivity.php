<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class MuseActivity extends Model
{
    public $timestamps = true;
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'muse_activities';
    protected $fillable = ['activity_id','apply_date','exported_to_muse','delivered_to_muse','muse_response','admin_hierarchy_id','financial_year_id'];
}
