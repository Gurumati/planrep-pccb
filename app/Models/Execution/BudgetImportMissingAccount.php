<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class BudgetImportMissingAccount extends Model
{
    protected $table = 'budget_import_missing_accounts';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
}