<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class TransactionExchangeControl extends Model
{
    protected $guarded = [];
}
