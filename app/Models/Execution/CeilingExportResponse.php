<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class CeilingExportResponse extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'ceiling_export_responses';

    public function admin_hierarchy_ceiling(){
        return $this->belongsTo('App\Models\Budgeting\AdminHierarchyCeiling');
    }
}
