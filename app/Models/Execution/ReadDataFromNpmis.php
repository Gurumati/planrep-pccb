<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use App\Models\Setup\AdminHierarchy;
use Illuminate\Support\Facades\DB;
use App\Models\Setup\GfsCode;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\FundSource;
use App\Http\Controllers\Execution\NpmisIntegrationController;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ReadDataFromNpmis extends Model
{
    
    public static function processProjectBudgetData($data){
 
      $trNumber = $data["message"]["messageSummary"]["company"];
      $messageType = $data["message"]["messageHeader"]["messageType"];
      $financialYear = $data["message"]["messageSummary"]["financialYear"];

      if($trNumber != '00000'){
        $admin_hierarchy = DB::table('admin_hierarchies as ah')
                        ->join('admin_hierarchies as ah2', 'ah.parent_id', 'ah2.id')
                        ->where('ah.code', $trNumber)
                        ->select('ah.id as admin_hierarchy_id', 'ah2.name as institution_name')
                        ->first();
        
            if(!$admin_hierarchy){
                $errorMessage  = "WRONG trNumber PROVIDED";
    
                $requestErrorToNest = array(
                    'data' => $errorMessage,
                    'signature'=>app('App\Http\Controllers\Execution\DigitalSignature')->signingData(json_encode($errorMessage))
                );   
                return response()->json($requestErrorToNest,200);
            }
            else
                $admin_hierarchy_id = $admin_hierarchy->admin_hierarchy_id;
          }else{
              $admin_hierarchy_id = 00000;
          }

      $financial_year_query = "select * from financial_years where name = '".$financialYear."'";
      $get_financial_year = DB::select(DB::raw($financial_year_query));
      $financial_year_id  = isset($get_financial_year[0]->id) ? $get_financial_year[0]->id: null;

      if($messageType == "PROJECT_BUDGET"){

        if($admin_hierarchy_id == 00000){
            $admin_hierarchies = DB::table('admin_hierarchies as ah')
            ->where('ah.id','<>', 1)
            ->where('ah.admin_hierarchy_level_id', 4)
            ->select('ah.id as admin_hierarchy_id', 'ah.name as institution_name', 'ah.code as institution_code')
            ->get();

            foreach($admin_hierarchies  as $admin){
                $admin_hierarchy_id = $admin->admin_hierarchy_id;
                $institution_code = $admin->institution_code;
                $projectBudgetArray = array();
                $projects = DB::select("select p.id as id,p.code as code,p.name as description from activities a
                                            join mtef_sections s on a.mtef_section_id = s.id
                                            join mtefs m on s.mtef_id = m.id
                                            join projects p on  p.id = a.project_id
                                        where m.admin_hierarchy_id=$admin_hierarchy_id
                                        and m.financial_year_id = $financial_year_id
                                        and a.budget_type != 'CURRENT'
                                        and p.code != '0000'
                                        group by p.id");
        
                if (sizeof($projects) > 0 ){
                    foreach ($projects as $project){
                                    $project_id =  $project->id;
                                    $project_code =  $project->code;
                                    $project_name =  $project->description;
                                    /** Find target */
                        $activities = DB::select("select a2.id as actid,sec.code costcentrecode,sec2.code subvote,fs.code financiercode,CONCAT(substring(a2.code from 1 for 1),substring(a2.code from 4)) activitycode,a2.description activitydescription from activities a2
                                                    join mtef_sections s on a2.mtef_section_id = s.id
                                                    join sections sec on s.section_id = sec.id
                                                    join sections sec2 on sec.parent_id = sec2.id
                                                    join mtefs m on s.mtef_id = m.id
                                                    join projects p on  p.id = a2.project_id
                                                    join activity_facilities af on a2.id = af.activity_id
                                                    join activity_facility_fund_sources affs on af.id = affs.activity_facility_id
                                                    join activity_facility_fund_source_inputs affsi on affs.id = affsi.activity_facility_fund_source_id
                                                    join budget_export_accounts bea on affsi.id = bea.activity_facility_fund_source_input_id
                                                    join budget_export_transaction_items beti on bea.id = beti.budget_export_account_id
                                                    join fund_sources fs on affs.fund_source_id = fs.id
                                            where bea.financial_year_id = $financial_year_id
                                            and bea.admin_hierarchy_id = $admin_hierarchy_id
                                            and beti.budget_reallocation_item_id is null
                                            and p.id in ($project_id)
                                            group by project_id, p.code,p.name,a2.id,a2.code, a2.description,fs.code,sec.code,sec2.code");
        
                        $activityArray = array();
                        $projectAmount = 0;
                        foreach ($activities as $activity){
        
                            $activity_id = $activity->actid;
                            $inputs = DB::select("SELECT
                                                    i.id as uid,gc.code as gfscode,fs.code as financiercode,gc.description as gfsdescription,COALESCE (SUM(i.unit_price * i.quantity * i.frequency),0) AS amount
                                                    FROM  activity_facility_fund_source_inputs AS i
                                                    JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
                                                    JOIN fund_sources AS fs on aff.fund_source_id = fs.id
                                                    JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
                                                    JOIN activities AS a ON a.id = af.activity_id
                                                    JOIN gfs_codes AS gc on i.gfs_code_id = gc.id
                                                    WHERE a.id in ($activity_id)
                                                    group by gc.description, gc.code,fs.code,i.id
                                                ");
                                    
                                    $inputArray = array();
                                    foreach($inputs as $input){

                                        $inputObject = array(
                                            "uid"=> $input->uid,
                                            "financierCode"=> $input->financiercode,
                                            "gfsCode"=> $input->gfscode,
                                            "gfsDesc"=> $input->gfsdescription,
                                            "amount"=> number_format($input->amount,2,'.','')
                                        );
                                        array_push($inputArray,$inputObject);
                                        
                                    }

                                    $activityObject = array(
                                        "subVote"=> $activity->subvote,
                                        "activityCode"=> $activity->activitycode,
                                        "activityName"=> $activity->activitydescription,
                                        "inputs" => $inputArray
                                    );

                                    array_push($activityArray,$activityObject);
                        }
                                    /** objective Object */
                                    $projectActivityObject = array(
                                        "uid" => $project_id,
                                        "projectName" => $project_name,
                                        "projectCode" => $project_code,
                                        "activities" => $activityArray
                                    );
                                    array_push($projectBudgetArray,$projectActivityObject);
        
                    }
        
                $company = DB::table('admin_hierarchies')->where('id',$admin_hierarchy_id)->first()->code;
        
                $options = array(
                    'headers' => array('x-match' => 'all', 'Company' => $company, 'type' => 'Transaction'),
                    'exchange' => 'BudgetExchange'
                );
        
                // $queue_name = ConfigurationSetting::getValueByKey('BUDGET_QUEUE_NAME');
                $queue_name = 'PLANREPOTR_TO_OTRMIS_DATA_QUEUE_OBJECTIVE';
        
                $control_number = app('App\Http\Controllers\Execution\MuseIntegrationController')->transactionNumber("PROJECT_BUDGET",12);
        
                $messageHeader = array(
                    "sender" => "PLANREPOTR",
                    "receiver" => "NPMIS",
                    "msgId" => $control_number,
                    // "applyDate" => date('Y-m-d'),
                    "messageType" => "PROJECT_BUDGET",
                    "createdAt" => date('Y-m-d H:i:s')
                );
                $messageSummary = array(
                    "company" => $company,
                    "trxCtrlNum" => $control_number,
                    "applyDate"=> date('Y-m-d'),
                    "financialYear" => $financialYear,
                    "description"=>"PROJECTS, ACTIVITIES AND BUDGET"
                );
                $item = array(
                    "messageHeader"  => $messageHeader,
                    "messageSummary" => $messageSummary,
                    "messageDetails" => $projectBudgetArray
                );
        
                $payloadToNpmis = array(
                    'message' => $item,
                    'digitalSignature'=>app('App\Http\Controllers\Execution\NpimsIntegrationController')->signature(json_encode($item))
                );
        
                    $response =  app('App\Http\Controllers\Execution\NpimsIntegrationController')->sendRequestToNpmis($payloadToNpmis);
        
            } else {
                //nothing to export
                $feedback = "No project budget to Export of .$institution_code., Please contact for support";
              }        

            }

        }else{
            
        $projectBudgetArray = array();
        $projects = DB::select("select p.id as id,p.code as code,p.name as description from activities a
                                    join mtef_sections s on a.mtef_section_id = s.id
                                    join mtefs m on s.mtef_id = m.id
                                    join projects p on  p.id = a.project_id
                                where m.admin_hierarchy_id=$admin_hierarchy_id
                                and m.financial_year_id = $financial_year_id
                                and p.code != '0000'
                                group by p.id");

        if (sizeof($projects) > 0 ){
            foreach ($projects as $project){
                            $project_id =  $project->id;
                            $project_code =  $project->code;
                            $project_name =  $project->description;
                            /** Find target */
                $activities = DB::select("select a2.id as actid,sec.code costcentrecode,sec2.code as subvote,fs.code financiercode,CONCAT(substring(a2.code from 1 for 1),substring(a2.code from 4)) activitycode,a2.description activitydescription from activities a2
                                            join mtef_sections s on a2.mtef_section_id = s.id
                                            join sections sec on s.section_id = sec.id
                                            join sections sec2 on sec.parent_id = sec2.id
                                            join mtefs m on s.mtef_id = m.id
                                            join projects p on  p.id = a2.project_id
                                            join activity_facilities af on a2.id = af.activity_id
                                            join activity_facility_fund_sources affs on af.id = affs.activity_facility_id
                                            join activity_facility_fund_source_inputs affsi on affs.id = affsi.activity_facility_fund_source_id
                                            join budget_export_accounts bea on affsi.id = bea.activity_facility_fund_source_input_id
                                            join budget_export_transaction_items beti on bea.id = beti.budget_export_account_id
                                            join fund_sources fs on affs.fund_source_id = fs.id
                                    where bea.financial_year_id = $financial_year_id
                                    and bea.admin_hierarchy_id = $admin_hierarchy_id
                                    and beti.budget_reallocation_item_id is null
                                    and p.id in ($project_id)
                                    group by project_id, p.code,p.name,a2.id,a2.code, a2.description,fs.code,sec.code,sec2.code");

                $activityArray = array();
                $projectAmount = 0;
                foreach ($activities as $activity){

                    $activity_id = $activity->actid;
                    $inputs = DB::select("SELECT
                                            i.id as uid,gc.code as gfscode,fs.code as financiercode,gc.description as gfsdescription,COALESCE (SUM(i.unit_price * i.quantity * i.frequency),0) AS amount
                                            FROM  activity_facility_fund_source_inputs AS i
                                            JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
                                            JOIN fund_sources AS fs on aff.fund_source_id = fs.id
                                            JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
                                            JOIN activities AS a ON a.id = af.activity_id
                                            JOIN gfs_codes AS gc on i.gfs_code_id = gc.id
                                            WHERE a.id in ($activity_id)
                                            group by gc.description, gc.code,fs.code,i.id
                                        ");
                            
                            $inputArray = array();
                            foreach($inputs as $input){

                                $inputObject = array(
                                    "uid"=> $input->uid,
                                    "financierCode"=> $input->financiercode,
                                    "gfsCode"=> $input->gfscode,
                                    "gfsDesc"=> $input->gfsdescription,
                                    "amount"=> number_format($input->amount,2,'.','')
                                );
                                array_push($inputArray,$inputObject);
                                
                            }

                            $activityObject = array(
                                "subVote"=> $activity->subvote,
                                "activityCode"=> $activity->activitycode,
                                "activityName"=> $activity->activitydescription,
                                "inputs" => $inputArray
                            );

                            array_push($activityArray,$activityObject);
                }
                            /** objective Object */
                            $projectActivityObject = array(
                                "uid" => $project_id,
                                "projectName" => $project_name,
                                "projectCode" => $project_code,
                                "activities" => $activityArray
                            );
                            array_push($projectBudgetArray,$projectActivityObject);

            }

        $company = DB::table('admin_hierarchies')->where('id',$admin_hierarchy_id)->first()->code;

        $options = array(
            'headers' => array('x-match' => 'all', 'Company' => $company, 'type' => 'Transaction'),
            'exchange' => 'BudgetExchange'
        );

        // $queue_name = ConfigurationSetting::getValueByKey('BUDGET_QUEUE_NAME');
        //$queue_name = 'PLANREPOTR_TO_NPMIS_DATA_QUEUE_OBJECTIVE';

        $control_number = app('App\Http\Controllers\Execution\MuseIntegrationController')->transactionNumber("PROJECT_BUDGET",12);

        $messageHeader = array(
            "sender" => "PLANREPOTR",
            "receiver" => "NPMIS",
            "msgId" => $control_number,
            // "applyDate" => date('Y-m-d'),
            "messageType" => "PROJECT_BUDGET",
            "createdAt" => date('Y-m-d H:i:s')
        );
        $messageSummary = array(
            "company" => $company,
            "trxCtrlNum" => $control_number,
            "applyDate"=> date('Y-m-d'),
            "financialYear" => $financialYear,
            "description"=>"PROJECTS, ACTIVITIES AND BUDGET"
        );
        $item = array(
            "messageHeader"  => $messageHeader,
            "messageSummary" => $messageSummary,
            "messageDetails" => $projectBudgetArray
        );

        $payloadToNpmis = array(
            'message' => $item,
            'digitalSignature'=>app('App\Http\Controllers\Execution\NpimsIntegrationController')->signature(json_encode($item))
        );

        // $payloadToNpmis = json_encode($payloadToNpmis,JSON_UNESCAPED_UNICODE);

        $response =  app('App\Http\Controllers\Execution\NpimsIntegrationController')->sendRequestToNpmis($payloadToNpmis);

    } else {
        //nothing to export
        $feedback = "No project budget to Export, Please contact for support";
      }


      

        }
    }elseif($messageType == "PROJECT_REALLOCATION"){
        
        $projectReallocationArray = array();
        $projectReallocations = DB::select("select  split_part(bri.from_chart_of_accounts,'-',8) as fromprojectcode,
                                        split_part(bri.from_chart_of_accounts,'-',10) as fromactivitycode,
                                        split_part(bri.from_chart_of_accounts,'-',14) as fromgfscode,
                                        split_part(bri.to_chart_of_accounts,'-',8) as toprojectcode,
                                        (select name from projects where code = split_part(bri.to_chart_of_accounts,'-',8)) as toprojectname,
                                        split_part(bri.to_chart_of_accounts,'-',10) as toactivitycode,
                                        (select description from activities where id in (select activity_id from budget_export_accounts where chart_of_accounts = bri.to_chart_of_accounts)) as toactivitydesc,
                                        split_part(bri.to_chart_of_accounts,'-',14) as togfscode,
                                        (select description from gfs_codes where id in (select gfs_code_id from budget_export_accounts where chart_of_accounts = bri.to_chart_of_accounts)) as togfsdesc,
                                        bri.amount as amount,bri.id as uid
                                from budget_reallocation_items bri
                                    join budget_export_accounts bea on bri.budget_export_account_id = bea.id
                                    join activities a on bea.activity_id = a.id
                                    join projects p on a.project_id = p.id
                                where bea.admin_hierarchy_id = $admin_hierarchy_id and bea.financial_year_id = $financial_year_id and p.code != '0000' and bri.is_approved = true
                                order by fromactivitycode");

        if (sizeof($projectReallocations) > 0 ){
            foreach ($projectReallocations as $reallocation){

                            $uid =  $reallocation->uid;
                            $from_project_code =  $reallocation->fromprojectcode;
                            $from_activity_code =  $reallocation->fromactivitycode;
                            $from_gfs_code =  $reallocation->fromgfscode;
                            $to_project_code =  $reallocation->toprojectcode;
                            $to_project_name =  $reallocation->toprojectname;
                            $to_activity_code =  $reallocation->toactivitycode;
                            $to_activity_desc =  $reallocation->toactivitydesc;
                            $to_gfs_code =  $reallocation->togfscode;
                            $to_gfs_desc =  $reallocation->togfsdesc;
                            $amount =  $reallocation->amount;
                        
                            /** objective Object */
                            $projectReallocationObject = array(
                                "uid" => $uid,
                                "fromProjectCode" => $from_project_code,
                                "fromActivityCode" => $from_activity_code,
                                "fromGfsCode" => $from_gfs_code,
                                "toProjectCode" => $to_project_code,
                                "toProjectName" => $to_project_name,
                                "toActivityCode" => $to_activity_code,
                                "toActivityDesc" => $to_activity_desc,
                                "toGfsCode" => $to_gfs_code,
                                "toGfsDesc" => $to_gfs_desc,
                                "amount" => $amount
                            );

                            array_push($projectReallocationArray,$projectReallocationObject);

            }

        $company = DB::table('admin_hierarchies')->where('id',$admin_hierarchy_id)->first()->code;

        $options = array(
            'headers' => array('x-match' => 'all', 'Company' => $company, 'type' => 'Transaction'),
            'exchange' => 'BudgetExchange'
        );

        // $queue_name = ConfigurationSetting::getValueByKey('BUDGET_QUEUE_NAME');
        $queue_name = 'PLANREPOTR_TO_OTRMIS_DATA_QUEUE_OBJECTIVE';

        $control_number = app('App\Http\Controllers\Execution\MuseIntegrationController')->transactionNumber("PROJECT_REALLOCATION",12);

        $messageHeader = array(
            "sender" => "PLANREPOTR",
            "receiver" => "NPMIS",
            "msgId" => $control_number,
            // "applyDate" => date('Y-m-d'),
            "messageType" => "PROJECT_REALLOCATION",
            "createdAt" => date('Y-m-d H:i:s')
        );
        $messageSummary = array(
            "company" => $company,
            "trxCtrlNum" => $control_number,
            "applyDate"=> date('Y-m-d'),
            "financialYear" => $financialYear,
            "description"=>"PROJECTS REALLOCATIONS"
        );
        $item = array(
            "messageHeader"  => $messageHeader,
            "messageSummary" => $messageSummary,
            "messageDetails" => $projectReallocationArray
        );

        $payloadToNpmis = array(
            'message' => $item,
            'digitalSignature'=>app('App\Http\Controllers\Execution\NpimsIntegrationController')->signature(json_encode($item))
        );

        $response =  app('App\Http\Controllers\Execution\NpimsIntegrationController')->sendRequestToNpmis($payloadToNpmis);

        } else {
          //nothing to export
          $feedback = "No project reallocation(s), Please contact for support";
        }
      }else{
        log::info($data);
        return "payload format is incorrect, not as expected";     
       }
    

    }
    //Process Project Data Details
    public static function processProjectData($data){

        ini_set('max_execution_time', 6000);

        $responseToNpmisFail = array();
        $responseToNpmisSuccess = array();
        $sender = $data["message"]["messageHeader"]["sender"];
        $receiver = $data["message"]["messageHeader"]["receiver"];
        $orgMsgId = $data["message"]["messageHeader"]["msgId"];
        $messageType = $data["message"]["messageHeader"]["messageType"];

        $message = $data["message"]["messageDetails"];
        //$message = json_encode($projectDetail);
         
            foreach($message as $projectDetails){ 
              
                for( $i = 0; $i<count($projectDetails['financierCode']); $i++){

                    $project_query = "select * from npmis_projects where code = '".$projectDetails['projectCode']."'";
                    $get_project_id = DB::select(DB::raw($project_query));
                    $project_exist_id  = isset($get_project_id[0]->id) ? $get_project_id[0]->id: null;
    
                    if($project_exist_id != null){
                        //Projects exist
                        $projectExist = array(
                            "projectCode" => $projectDetails['projectCode'],
                            "status" => "Fail",
                            "description" => "PROJECT ALREADY EXIST IN PLANREP",
                         );
                         array_push($responseToNpmisFail,$projectExist);
                    }else{
                        $projectReceived = array(
                            "projectCode" => $projectDetails['projectCode'],
                            "status" => "Success",
                            "description" => "PROJECT RECEIVED ON PLANREP",
                         );
                         array_push($responseToNpmisSuccess,$projectReceived);
        
                        $projectData = array(
                            'name' => $projectDetails['projectName'],
                            'description' => $projectDetails['projectName'],
                            'code' => $projectDetails['projectCode'],
                            'created_at' => \Carbon\Carbon::now(),
                        );
    
                        DB::transaction(function() use ($projectData) {
                            DB::table('npmis_projects')->insert([$projectData]);
                        });  
                    }
                    $project_query = "select id from npmis_projects where code = '".$projectDetails['projectCode']."'";
                    $get_project_id = DB::select(DB::raw($project_query));
                    $project_id = isset($get_project_id[0]->id) ? $get_project_id[0]->id: null;
                    
                    $fcode = $projectDetails['financierCode'][$i];
                    $fund_source_query= "select id from fund_sources where code = '".$fcode."'";
                    $get_fund_source_id = DB::select(DB::raw($fund_source_query));
                    $fund_source_id = isset($get_fund_source_id[0]->id) ? $get_fund_source_id[0]->id: null;

                    if($fund_source_id > 0){
                        $projectFundSources = array(
                            'project_id' => $project_id,
                            'fund_source_id' => $fund_source_id,
                            'created_at' => \Carbon\Carbon::now(),
                        );

                        DB::transaction(function() use ($projectFundSources) {
                            DB::table('project_fund_sources')->insert([$projectFundSources]);
                        });  
                    }else { 
                        # code...
                        Log::info("No Fund Source supplied");
                    }

                
                  }

                  for( $i = 0; $i<count($projectDetails['sectorCode']); $i++){

                    $project_query = "select id from npmis_projects where code = '".$projectDetails['projectCode']."'";
                    $get_project_id = DB::select(DB::raw($project_query));
                    $project_id = isset($get_project_id[0]->id) ? $get_project_id[0]->id: null;
                    
                    $scode = $projectDetails['sectorCode'][$i];
                    $sector_query= "select id from sectors where code = '".$scode."'";
                    $get_sector_id = DB::select(DB::raw($sector_query));
                    $sector_id = isset($get_sector_id[0]->id) ? $get_sector_id[0]->id: null;

                    if($sector_id > 0){
                        $sectorProjects = array(
                            'project_id' => $project_id,
                            'sector_id' => $sector_id,
                            'created_at' => \Carbon\Carbon::now(),
                        );

                        DB::transaction(function() use ($sectorProjects) {
                            DB::table('sector_projects')->insert([$sectorProjects]);
                        });  
                    }else { 
                        # code...
                        Log::info("No Sector detail supplied");
                    }

                
                  }
                //}
          }

          if($responseToNpmisSuccess != null){
           $responseDataSuccess =  app('App\Http\Controllers\Execution\NpimsIntegrationController')->response($orgMsgId,"PROJECT","PROCESSED","Response from Planrep",$responseToNpmisSuccess);
           try {
               $response =  app('App\Http\Controllers\Execution\NpimsIntegrationController')->sendRequestToNpmis($responseDataSuccess);
               //code...
           } catch (\Throwable $th) {
               //Log::info("ERROR");
           }
         }elseif($responseToNpmisFail != null){
           $responseDataFail =  app('App\Http\Controllers\Execution\NpimsIntegrationController')->response($orgMsgId,"PROJECT","REJECTED","Response from Planrep",$responseToNpmisFail);
           try {
               $response =  app('App\Http\Controllers\Execution\NpimsIntegrationController')->sendRequestToNpmis($responseDataFail);
               //code...
           } catch (\Throwable $th) {
               //Log::info("ERROR");
           }
        }
         
    }

    public static function getObjectiveFeedback($data){

        $status = $data["message"]["messageSummary"]["responseStatus"];
 
         if($status == "PROCESSED" || $status == "RECEIVED"){
            
             $feedbackDetails = $data["message"]["messageDetails"];
             foreach($feedbackDetails as $detail){
                   $uid = $detail["uid"];
                   
                   DB::table('setup_segment_exports')
                       ->where('segment_detail_id',$uid)
                       ->update(['is_delivered' => true,'response'=>'RECEIVED']);
   
             }
        
         }elseif ($status == "REJECTED") {
 
             $feedbackDetails = $data["message"]["messageDetails"];
 
             foreach($feedbackDetails as $detail){
             $uid = $detail["uid"];
             $statusCode = $detail["statusCode"];
             $statusDesc = $detail["statusDesc"];
             $additionalInfo = $detail["additionalInfo"];
 
             $count = DB::table('budget_export_to_financial_systems')
             ->where('id',$uid)->get();
 
             if($count->count() > 0 && $statusCode =='Failed'){
                 $ber = BudgetExportResponse::where('budget_export_to_financial_system_id', $uid)->first();
                     $response_exists = isset($ber->id) ? $ber->id : null;
                     if ($response_exists == null) {
                         DB::table('budget_export_responses')
                         ->insert([
                             'budget_export_to_financial_system_id' => $uid,
                             'is_sent' => true,
                             'is_delivered' => true,
                             'response' => $statusDesc,
                             'financial_system_code'=>'MUSE',
                             'created_at' =>\Carbon\Carbon::now()
                         ]);
                     }else{
 
                         DB::table('budget_export_responses')
                             ->where('id', $response_exists)
                             ->update([
                                 'is_sent' => true,
                                 'is_delivered' => true,
                                 'response' => $statusDesc,
                                 'financial_system_code'=>'MUSE',
                                 'updated_at' =>\Carbon\Carbon::now()
                         ]);
 
                     }  
             } 
           
            }
         }
     }

     public static function getProjectBudgetFeedback($data){

        return "No Response Handling so far";

        $status = $data["message"]["messageSummary"]["responseStatus"];
 
         if($status == "PROCESSED" || $status == "RECEIVED"){
            
             $feedbackDetails = $data["message"]["messageDetails"];
             foreach($feedbackDetails as $detail){
                   $uid = $detail["uid"];
                   
                   DB::table('project_budget_exports')
                       ->where('project_id',$uid)
                       ->update(['is_delivered' => true,'response'=>'RECEIVED']);
   
             }
        
         }elseif ($status == "REJECTED") {
 
             $feedbackDetails = $data["message"]["messageDetails"];
 
             foreach($feedbackDetails as $detail){
             $uid = $detail["uid"];
             $statusCode = $detail["statusCode"];
             $statusDesc = $detail["statusDesc"];
             $additionalInfo = $detail["additionalInfo"];
 
             $count = DB::table('budget_export_to_financial_systems')
             ->where('id',$uid)->get();
 
             if($count->count() > 0 && $statusCode =='Failed'){
                 $ber = BudgetExportResponse::where('budget_export_to_financial_system_id', $uid)->first();
                     $response_exists = isset($ber->id) ? $ber->id : null;
                     if ($response_exists == null) {
                         DB::table('budget_export_responses')
                         ->insert([
                             'budget_export_to_financial_system_id' => $uid,
                             'is_sent' => true,
                             'is_delivered' => true,
                             'response' => $statusDesc,
                             'financial_system_code'=>'MUSE',
                             'created_at' =>\Carbon\Carbon::now()
                         ]);
                     }else{
 
                         DB::table('budget_export_responses')
                             ->where('id', $response_exists)
                             ->update([
                                 'is_sent' => true,
                                 'is_delivered' => true,
                                 'response' => $statusDesc,
                                 'financial_system_code'=>'MUSE',
                                 'updated_at' =>\Carbon\Carbon::now()
                         ]);
 
                     }  
             } 
           
            }
         }
     }
}
