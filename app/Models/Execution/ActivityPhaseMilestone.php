<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class ActivityPhaseMilestone extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'activity_phase_milestones';

    public function activity_phase() {
        return $this->belongsTo('App\Models\Execution\ActivityPhase','activity_phase_id','id');
    }

    public function period()
    {
        return $this->belongsTo('App\Models\Setup\Period', 'period_id', 'id');
    }

    public function facility_id()
    {
        return $this->belongsTo('App\Models\Setup\Facility', 'facility_id', 'id');
    }
}
