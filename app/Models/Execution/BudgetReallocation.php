<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Input;


class BudgetReallocation extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'budget_reallocations';
    protected $fillable = [
    'reference_code',
    'data_source_id',
    'import_method_id',
    'is_open',
    'comments',
    'decision_level_id',
    'admin_hierarchy_id',
    'financial_year_id',
    'created_by',
    'updated_by',
    'created_at',
    'update_at',
    'document_url',
    'document_url2',
    'document_url3',
    'reallocation_desc'
   ];

   public static function uploadDoc($num){
        $file = Input::file('file_to_upload'.$num);
        $dir = 'uploads/reference_documents/';
        $fileName = "realloc_ref_doc_".$num."_".time().'.'.$file->getClientOriginalExtension();
        $document_url = '/' . $dir . $fileName;
        $file->move($dir, $fileName);
        return  $document_url;
   }

    public function data_source(){
        return $this->belongsTo('App\Models\Execution\DataSource');
    }

    public function admin_hierarchy(){
        return $this->belongsTo('App\Models\Setup\AdminHierarchy');
    }

    public function financial_year(){
        return $this->belongsTo('App\Models\Setup\FinancialYear');
    }

    public function import_method(){
        return $this->belongsTo('App\Models\Execution\ImportMethod');
    }

    public function budget_reallocation_approve(){
        return $this->hasMany('App\Models\Execution\BudgetReallocationApprove');
    }
    public function items(){
        return $this->hasMany('App\Models\Execution\BudgetReallocationItem');
    }

    public function decision_level(){
        return $this->belongsTo('App\Models\Setup\DecisionLevel');
    }


}
