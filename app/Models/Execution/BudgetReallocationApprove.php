<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class BudgetReallocationApprove extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'budget_reallocation_approvals';

    public function budget_reallocation(){
        return $this->belongsTo('App\Models\Execution\BudgetReallocation','budget_reallocation_id','id');
    }
}
