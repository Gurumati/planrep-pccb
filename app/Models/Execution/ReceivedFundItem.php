<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReceivedFundItem extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'received_fund_items';

    public function admin_hierarchy_ceiling(){
        return $this->belongsTo('App\Models\Budgeting\AdminHierarchyCeiling','admin_hierarchy_ceiling_id','id');
    }

    public function period(){
        return $this->belongsTo('App\Models\Setup\Period','period_id','id');
    }

    public function fund_source(){
        return $this->belongsTo('App\Models\Setup\FundSource','fund_source_id','id');
    }

    public function gfs_code(){
        return $this->belongsTo('App\Models\Setup\GfsCode','gfs_code_id','id');
    }

    public function revenue_export_account(){
        return $this->belongsTo('App\Models\Setup\GfsCode','revenue_export_account_id','id');
    }

    public function cash_account(){
        return $this->belongsTo('App\Models\Setup\CashAccount','cash_account_id','id');
    }
}
