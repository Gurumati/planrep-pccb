<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class FfarsActualControlId extends Model
{
    protected $table = 'ffars_actual_control_ids';
    protected $guarded = ['id'];

}
