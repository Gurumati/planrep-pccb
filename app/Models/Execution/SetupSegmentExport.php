<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class SetupSegmentExport extends Model
{
    public $timestamps = true;
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'setup_segment_exports';
    protected $fillable = ['segment_detail_id','apply_date','is_sent','is_delivered','response','admin_hierarchy_id','financial_year_id','budget_type','segment_name','system_code'];
}
