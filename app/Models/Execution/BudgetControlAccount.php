<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class BudgetControlAccount extends Model
{
    public $table = 'budget_control_accounts';
}
