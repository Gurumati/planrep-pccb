<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class BudgetExportAccountGLAccountExportIssues extends Model
{
    protected $table = 'budget_export_account_glaccount_export_issues';

}
