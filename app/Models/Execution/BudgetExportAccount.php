<?php

namespace App\Models\Execution;

use App\Http\Services\UserServices;
use App\Models\Setup\ConfigurationSetting;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Http\Services\Execution\BudgetExportToFinancialSystemService;
use App\Http\Services\Execution\BudgetExportTransactionItemService;
use App\Models\Execution\BudgetExportTransaction;
use App\Http\Services\Budgeting\BudgetExportAccountService;
use Illuminate\Support\Facades\Log;

class BudgetExportAccount extends Model
{
    public $timestamps = true;
    //use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'budget_export_accounts';

    protected $fillable = [
        'admin_hierarchy_id', 'section_id', 'financial_year_id', 'activity_facility_fund_source_input_id',
        'activity_id', 'gfs_code_id', 'chart_of_accounts', 'financial_system_code', 'amount', 'reference_number',
        'fund_allocated_amount', 'expenditure_amount', 'has_issues', 'is_sent', 'is_delivered', 'is_locked', 'comments',
        'deleted_by', 'created_by', 'updated_by'
    ];

    public static function getByInputIds($inputIds)
    {
        return  DB::table('budget_export_accounts as bec')
            ->join('activities as a', 'a.id', 'bec.activity_id')
            ->join('sections as s', 's.id', 'bec.section_id')
            ->join('gfs_codes as gfs', 'gfs.id', 'bec.gfs_code_id')
            ->whereIn('bec.activity_facility_fund_source_input_id', $inputIds)
            ->select('bec.id as id','gfs.code as gfs_code', 'gfs.description as description', 'bec.chart_of_accounts', 'a.description as activity', 's.name as section')
            ->get();
    }

    public static function createTransactionAndExport($budgetExportAccount, $transactionId, $amount, $isCredit, $description)
    {

        $systemQueueKey = $budgetExportAccount->financial_system_code . '_BUDGET_EXPORT_QUEUE_NAME';
        $queue = ConfigurationSetting::where('key', $systemQueueKey)->first();

        $transactionItem = BudgetExportTransactionItem::create([
            'budget_export_transaction_id' => $transactionId,
            'budget_export_account_id' => $budgetExportAccount->id,
            'amount' => $amount,
            'is_credit' => $isCredit,
            'created_by' => UserServices::getUser()->id
        ]);

        $exported = BudgetExportToFinancialSystem::create([
            'budget_export_transaction_item_id' => $transactionItem->id,
            'chart_of_accounts' => $budgetExportAccount->chart_of_accounts,
            'description' => $description,
            'is_sent' => false,
            'financial_year_id' => $budgetExportAccount->financial_year_id,
            'amount' => $amount,
            'queue_name' => (isset($queue)) ? $queue->value : '',
            'admin_hierarchy_id' => $budgetExportAccount->admin_hierarchy_id
        ]);

        return $exported;

    }

    public function activity_facility_fund_source_input()
    {
        return $this->belongsTo('App\Models\Planning\ActivityFacilityFundSourceInput');
    }

    public function system()
    {
        return $this->belongsTo('App\Models\Setup\System', 'financial_system_code', 'code');
    }

    public static function getAllPaginated($adminHierarchyId, $financialYear, $perPage, $financial_system_code, $budget_type, $account_type)
    {
        if ($financial_system_code == 'NPMIS') {
            $page = Input::get('page', 1);
        $all = DB::table('activities as a')
                        ->join('mtef_sections as ms', 'a.mtef_section_id','ms.id')
                        ->join('mtefs as m', 'ms.mtef_id', 'm.id')
                        ->join('projects as p', 'p.id', 'a.project_id')
                        ->join('activity_facilities as af', 'a.id', 'af.activity_id')
                        ->join('activity_facility_fund_sources as aff', 'af.id','aff.activity_facility_id')
                        ->join('fund_sources as fs', 'aff.fund_source_id', 'fs.id')
                        ->join('activity_facility_fund_source_inputs as affsi', 'aff.id', 'affsi.activity_facility_fund_source_id')
                        ->where('m.admin_hierarchy_id', $adminHierarchyId)
                        ->where('m.financial_year_id', $financialYear)
                        ->where('p.code','<>', '0000')
                        ->where('affsi.unit_price','>', 0)
                ->select('a.id as id','p.code as pcode','p.name as pname','fs.code as fcode','fs.name as fname','a.code as acode','a.description as aname',DB::raw('sum(affsi.quantity*affsi.frequency*affsi.unit_price) as amount'))
                ->groupBy('a.id','p.code','p.name','fs.code','fs.name','a.code','a.description')
                ->get();
            $all = paginate_this($all, $perPage, $page);
            // foreach ($all as &$item) {
            //     $exported = DB::table('facility_budget_export_responses as resp')
            //         ->where('facility_id', $item->facility_id)
            //         ->where('financial_year_id', $financialYear)
            //         ->select('resp.facility_id as id', 'resp.is_sent', 'resp.is_delivered', 'resp.response')
            //         ->get();
            //     $item->exports = $exported;
            // }
        } else {
            if ($account_type == 'bureau') {
                $all = DB::table('budget_export_accounts as bacc')
                    ->join('admin_hierarchies as admin', 'admin.id', 'bacc.admin_hierarchy_id')
                    ->join('gfs_codes as gfs', 'gfs.id', 'bacc.gfs_code_id')
                    ->join('activities as a', 'a.id', 'bacc.activity_id')
                    ->join('activity_facility_fund_source_inputs as affi', 'affi.id', 'bacc.activity_facility_fund_source_input_id')
                    ->join('sections as s', 's.id', 'bacc.section_id')
                    ->where('bacc.admin_hierarchy_id', $adminHierarchyId)
                    ->where('bacc.financial_year_id', $financialYear)
                    //->where('bacc.is_delivered', false)
                   // ->where('bacc.financial_system_code', $financial_system_code)
                    ->where('affi.budget_type', $budget_type)
                    ->whereRaw('bacc.amount > 0')
                    //->where('a.is_facility_account', false)
                    ->select('bacc.*', 'admin.name as admin_hierarchy', 'gfs.code as gfs_code', 'gfs.description as gfs_code_description', 'a.description as activity', 's.name as section')
                    ->orderBy('bacc.section_id', 'a.id')
                    ->paginate($perPage);

                foreach ($all as &$item) {
                    $exported = DB::table('budget_export_transaction_items as be')
                        ->join('budget_export_to_financial_systems as bef', 'bef.budget_export_transaction_item_id', 'be.id')
                        ->leftJoin('budget_export_responses as resp', 'resp.budget_export_to_financial_system_id', 'bef.id')
                        ->where('be.budget_export_account_id', $item->id)
                        ->where('be.is_credit', true)
                        ->select('bef.id', DB::Raw("CASE WHEN be.budget_reallocation_item_id IS NOT NULL THEN '' ELSE 'A' END AS type"), 'bef.is_sent', 'resp.is_delivered', 'resp.financial_system_code')
                        ->get();
                    $item->exports = $exported;
                }
            } else {
                $sql = "SELECT bacc.id, bacc.chart_of_accounts, admin.name as admin_hierarchy,
                        'transfer' as gfs_code, 'transfer' as gfs_code_description, 'Transfer' as activity,
                        sub.id as section_id, sub.name as section, sum(bacc.amount) as amount FROM
                        aggregate_facility_budgets as bacc inner join
                        admin_hierarchies as admin on admin.id = bacc.admin_hierarchy_id inner join
                        ( select distinct aggregate_facility_budget_id, a.budget_type, s.id, s.name from
                        aggregate_facility_budget_export_accounts as ae inner join
                        budget_export_accounts as ac on ac.id = ae.budget_export_account_id inner join
                        activities as a on a.id = ac.activity_id inner join
                        sections as s on s.id = ac.section_id
                        where
                        a.is_facility_account = true
                        ) as sub on sub.aggregate_facility_budget_id = bacc.id
                        WHERE
                        bacc.admin_hierarchy_id = $adminHierarchyId and bacc.financial_year_id = $financialYear
                        and sub.budget_type = '$budget_type' and bacc.is_delivered = false
                        GROUP BY
                        bacc.id, bacc.chart_of_accounts, admin.name, sub.id, sub.name";
                $all = DB::select($sql);
                $all = paginate_this($all, $perPage);
            }

        }
        return $all;
    }

    public static function loadInputs($admin_hierarchy_id, $financial_year_id, $Ids, $budget_type)
    {
        $sql = "SELECT
         ah.id as admin_hierarchy_id,adm2.code as vote,adm1.code as sub_vote,ah.code as council,ah.name as council_name,
         s.code as cost_centre,
         b.code as sub_budget_class_code,
         fa.facility_code,fu.code as fund_source_code,
         CASE WHEN a.budget_type ='CARRYOVER' THEN ft.carried_over_budget_code ELSE ft.current_budget_code END as fund_type_code,
         pr.code as project_code,a.code as activity_code,pc.code service_output_code,
         a.is_facility_account, fat.code as ftypecode, g.code as gfs_code,s.id as section_id,fy.id as financial_year_id,
         affi.id as activity_facility_fund_source_input_id, a.id as activity_id, a.budget_type as budget_type,
         g.id as gfs_code_id,affi.unit_price,affi.quantity,affi.frequency,affi.unit_price*affi.quantity*affi.frequency as amount,
         concat(gl2.code,gl.code) location_code,'00000' cofog_code

     from
         mtefs m
             left join financial_years fy on fy.id = m.financial_year_id
             join mtef_sections ms on ms.mtef_id = m.id
             join sections s on s.id = ms.section_id
             join activities a on a.mtef_section_id = ms.id
             join activity_facilities af on af.activity_id = a.id
             join activity_facility_fund_sources aff on aff.activity_facility_id = af.id
             join activity_facility_fund_source_inputs affi on affi.activity_facility_fund_source_id = aff.id
             join gfs_codes g on g.id = affi.gfs_code_id
             INNER JOIN facilities fa on fa.id = af.facility_id
             INNER JOIN geographical_locations gl on fa.geo_location_id = gl.id
             INNER JOIN geographical_locations gl2 on gl2.id = gl.parent_id
             INNER JOIN facility_types fat on fat.id = fa.facility_type_id
             join fund_sources fu on fu.id = aff.fund_source_id
             INNER JOIN fund_source_categories fc on fc.id = fu.fund_source_category_id
             INNER JOIN mtef_annual_targets mat on a.mtef_annual_target_id = mat.id
             INNER JOIN long_term_targets ltt on mat.long_term_target_id = ltt.id
             INNER JOIN plan_chains pc on ltt.plan_chain_id = pc.id
             join budget_classes b on b.id = a.budget_class_id
             INNER JOIN fund_source_budget_classes fb on (fb.budget_class_id = b.id and fb.fund_source_id = fu.id)
             INNER JOIN fund_types ft on ft.id = fb.fund_type_id
             INNER JOIN projects pr on pr.id = a.project_id
             join admin_hierarchies ah on ah.id = m.admin_hierarchy_id
             INNER JOIN admin_hierarchies adm1 on ah.parent_id = adm1.id
             INNER JOIN admin_hierarchies adm2 on adm1.parent_id = adm2.id

     where
             ah.id =  '$admin_hierarchy_id'  and m.financial_year_id =  '$financial_year_id' and a.deleted_at is null and affi.deleted_at is null
             AND affi.id NOT IN (select DISTINCT activity_facility_fund_source_input_id  FROM activity_facility_fund_source_input_breakdowns  affsib
             join budget_submission_definitions bsd on affsib.budget_submission_definition_id = bsd.id
             where ((fu.code = '0GT' AND bsd.field_name IN ('ZSSF 15%','WCF 0.5%','PSSSF 15%','NHIF 3%','PSSSF 25% (Gratuity)')) ))";
        if (isset($Ids))
            $sql = $sql . " and affi.id IN (" . $Ids . ")";
        if (isset($budget_type))
            $sql = $sql . " and affi.budget_type = '" . $budget_type . "'";
        return DB::select($sql);
    }

    public static function exports($admin_hierarchy_id, $financial_year_id)
    {
        $sql = "SELECT e.id as budget_export_account_id,ah.id as admin_hierarchy_id,v.code as vote,
                ah.code as council,ah.name as council_name,se.code as cost_centre,b.code as sub_budget_class_code,
                b.name as budget_class, fu.code as fund_source_code,pr.code as project_code,a.code as activity_code,
                a.description as activity_description,g.code as gfs_code,g.description as gfs_code_description,
                se.id as section_id,f.id as financial_year_id, ai.id as activity_facility_fund_source_input_id,
                a.id as activity_id, g.id as gfs_code_id,
                ai.unit_price,ai.quantity,ai.frequency,ai.unit_price*ai.quantity*ai.frequency as amount,
                sy.name as system,e.chart_of_accounts,e.reference_number
                from budget_export_accounts e
                      INNER JOIN activity_facility_fund_source_inputs ai ON e.activity_facility_fund_source_input_id = ai.id
                      INNER JOIN activity_facility_fund_sources af on af.id = ai.activity_facility_fund_source_id
                      INNER JOIN fund_sources fu on fu.id = af.fund_source_id
                      INNER JOIN gfs_codes g on g.id = ai.gfs_code_id
                      INNER JOIN activity_facilities acf on acf.id = af.activity_facility_id
                      INNER JOIN activities a on a.id = acf.activity_id
                      INNER JOIN budget_classes b on b.id = a.budget_class_id
                      INNER JOIN projects pr on pr.id = a.project_id
                      INNER JOIN mtef_sections ms on ms.id = a.mtef_section_id
                      INNER JOIN mtefs m on m.id = ms.mtef_id
                      INNER JOIN sections se on se.id = ms.section_id
                      INNER JOIN financial_years f on f.id = m.financial_year_id
                      INNER JOIN admin_hierarchies ah on ah.id = m.admin_hierarchy_id
                      inner join admin_hierarchies sv on sv.id = ah.parent_id
                      inner join admin_hierarchies v on v.id = sv.parent_id
                      inner join systems sy on sy.code = e.financial_system_code
                where ah.id IN ($admin_hierarchy_id) and f.id = '$financial_year_id' ORDER BY a.id";
        return DB::select($sql);
    }

    public function issues()
    {
        return $this->hasMany('App\Models\Execution\BudgetExportAccountGLAccountExportIssue');
    }

    public static function getReallocationAccount($activityFundSourceId)
    {
        //for Actuals Expenditures to be used in select on second debit_amount return credit_amount
        $accountCanReallocate = DB::table('budget_export_accounts as a')
            ->join('activity_facility_fund_source_inputs as i', 'a.activity_facility_fund_source_input_id', 'i.id')
            ->join('activity_facility_fund_sources as aff', 'i.activity_facility_fund_source_id', 'aff.id')
            ->join('activities as act','a.activity_id','act.id')
            ->join('gfs_codes as gfs', 'gfs.id', 'a.gfs_code_id')
            ->leftJoin('budget_import_items as imp', 'imp.budget_export_account_id', 'a.id')
            ->where('aff.id', $activityFundSourceId)
            ->whereRaw('(a.amount - a.fund_allocated_amount) > 0')
            ->groupBy('aff.activity_facility_id','gfs.id','a.id','act.code','act.description')
            ->select(
                'a.id',
                'act.code as activityCode',
                'act.description as activityDescription',
                'aff.activity_facility_id',
                'a.chart_of_accounts',
                'gfs.description',
                'a.amount as budget',
                'a.fund_allocated_amount as allocated',
                 DB::raw("Coalesce(sum(imp.debit_amount)-sum(imp.debit_amount),0.00) as expenditure")
            )
            ->get();
        return $accountCanReallocate;
    }

    public static function getToReallocationAccount() {

        $budgetType = Input::get('budgetType');
        $financialYearId = Input::get('financialYearId', 0);
        $adminHiearchyId = Input::get('adminHierarchyId', 0);
        $sectionId = Input::get('sectionId', 0);
        $facilityId = Input::get('facilityId', 0);
        $fundSourceId = Input::get('fundSourceId', 0);

        //for Actuals Expenditures to be used in select on second debit_amount return credit_amount
        $accountCanReallocate = DB::table('budget_export_accounts as a')
        ->join('activity_facility_fund_source_inputs as i', 'a.activity_facility_fund_source_input_id', 'i.id')
        ->join('activity_facility_fund_sources as aff', 'i.activity_facility_fund_source_id', 'aff.id')
        ->join('activity_facilities as af', 'af.id', 'aff.activity_facility_id')
        ->join('activities as ac', 'ac.id', 'af.activity_id')
        ->join('gfs_codes as gfs', 'gfs.id', 'a.gfs_code_id')
        ->leftJoin('budget_import_items as imp', 'imp.budget_export_account_id', 'a.id')
        ->where('aff.fund_source_id', $fundSourceId)
        ->where('ac.budget_type', $budgetType)
        ->where('a.admin_hierarchy_id', $adminHiearchyId)
        ->where('a.financial_year_id', $financialYearId)
        ->where('a.section_id', $sectionId)
        ->where('af.facility_id', $facilityId)
        //->whereRaw('(a.amount - a.fund_allocated_amount) > 0')
        ->groupBy('ac.id','aff.activity_facility_id','gfs.id','a.id')
        ->select(
            'a.id',
            'ac.description as activity',
            'aff.activity_facility_id',
            'a.chart_of_accounts',
            'gfs.description',
            'a.amount as budget',
            'a.fund_allocated_amount as allocated',
             DB::raw("Coalesce(sum(imp.debit_amount)-sum(imp.debit_amount),0.00) as expenditure")
        )
        ->get();
      return $accountCanReallocate;
    }

    /** generate transactions
     * parameter $Ids budget export account ids
     */
    public static function generateTransaction($ids, $budget_type)
    {
        $userId = UserServices::getUser()->id;
        //create transaction if does not exist
        $transaction = new BudgetExportTransaction();
        $transaction->description = $budget_type . ' BUDGET';
        $transactionTypeConfig = ConfigurationSetting::getValueByKey('BUDGET_APPROVAL_TRANSACTION_TYPE');
        $budgetGroupName = $budget_type;
        $transaction->budget_transaction_type_id = isset($transactionTypeConfig) ? $transactionTypeConfig : null;
        $transaction->save();
        //get id to update control number
        $control_number = $budgetGroupName . '_' . $transaction->id;
        $transaction->control_number = $control_number;
        $transaction->save();

        $transaction_id = $transaction->id;
        $budget_export_account_ids = implode(',', $ids);
        //get accounts
        $accounts = DB::select("select a.*, gfs.description as description  from budget_export_accounts a
                            join activities ac on ac.id = a.activity_id
                            join activity_facility_fund_source_inputs as ai on ai.id = a.activity_facility_fund_source_input_id
                            join gfs_codes as gfs on gfs.id = a.gfs_code_id
                            where a.id in
                            ($budget_export_account_ids) and a.amount > 0");

        foreach ($accounts as $account) {
            $item = BudgetExportTransactionItemService::create($transaction->id, $account->id, $account->amount, null, true, $userId);

            BudgetExportToFinancialSystemService::create(
                $account->admin_hierarchy_id,
                $item->id,
                $account->chart_of_accounts,
                $budgetGroupName,
                $account->financial_year_id,
                $account->amount,
                $account->financial_system_code,
                $userId
            );
        }
    }

    //return journal code
    public static function getJournalCode($budget_type)
    {
        switch ($budget_type) {
            case 'APPROVED':
                return 'BA';
                break;
            case 'SUPPLEMENTARY':
                return 'BS';
                break;
            case 'CARRYOVER':
                return 'CF';
                break;
            default:
                return null;
        }
    }

    //return gfscodes for transfer to facilities
    public static function getGfsCode($cost_center)
    {
        //get sector
        $result = DB::select("select s.name from sections as sec
        join sectors s on s.id = sec.sector_id where sec.id = $cost_center");
        $sector = $result[0]->name;
        $key = strtoupper($sector) . '_TRANSFER';
        return ConfigurationSetting::getValueByKey($key);
    }

    //save aggreaged accounts for facility budget
    public static function saveAggreagetedAccounts($aggregate_facility_budget_id, $export_accounts)
    {
        $accounts = rtrim(ltrim($export_accounts, '{'), '}');
        $export_accounts = explode(',', $accounts);
        foreach ($export_accounts as $item) {
            DB::table('aggregate_facility_budget_export_accounts')
                ->insert([
                    'budget_export_account_id' => $item,
                    'aggregate_facility_budget_id' => $aggregate_facility_budget_id
                ]);
        }
    }

    //get aggreagate facility budget
    public static function getAggregateBudget($admin_hierarchy_id, $financial_year_id, $budget_type)
    {
        $sql = "select rg.code as region, ah.code as council, s.id, s.code as cost_center, bc.code as budget_class, fs.code as fund_source,
                ft.current_budget_code, ft.carried_over_budget_code, ac.budget_type,
                sum(affi.frequency * affi.unit_price * affi.quantity) as amount,
                array_agg(a.id) as export_accounts
                from
                mtefs m
                join mtef_sections ms on ms.mtef_id = m.id
                join activities ac on ac.mtef_section_id = ms.id
                join activity_facilities af on af.activity_id = ac.id
                join activity_facility_fund_sources aff on aff.activity_facility_id = af.id
                join activity_facility_fund_source_inputs affi on affi.activity_facility_fund_source_id = aff.id
                join budget_classes bc on bc.id = ac.budget_class_id
                join fund_sources fs on fs.id = aff.fund_source_id
                join fund_source_budget_classes fc on (fc.budget_class_id = bc.id and fc.fund_source_id = fs.id)
                join fund_types ft on ft.id = fc.fund_type_id
                join admin_hierarchies ah on ah.id = m.admin_hierarchy_id
                join admin_hierarchies rg on rg.id = ah.parent_id
                join sections s on s.id = ms.section_id
                join budget_export_accounts a on a.activity_facility_fund_source_input_id = affi.id
                where
                m.admin_hierarchy_id = $admin_hierarchy_id and ac.is_facility_account = true
                and m.financial_year_id = $financial_year_id
                and affi.budget_type = '$budget_type'
                and a.id not in (SELECT b.id FROM budget_export_accounts b
                join aggregate_facility_budget_export_accounts ag on ag.budget_export_account_id = b.id
                where admin_hierarchy_id =$admin_hierarchy_id and financial_year_id = $financial_year_id)
                group by
                rg.code, ah.code, s.id, s.code, bc.code, fs.code, ft.current_budget_code, ft.carried_over_budget_code, ac.budget_type";
        $result = DB::select($sql);
        $timestamps = microtime();
        $created_at = date("Y-m-d H:i:s");
        foreach ($result as $item) {
            //get gfscode for transfer
            $gfscode = BudgetExportAccount::getGfsCode($item->id);
            //assign the correct fund type based on budget_type...
            if ($item->budget_type == 'CARRYOVER') {
                $fund_type = $item->carried_over_budget_code;
            } else {
                $fund_type = $item->current_budget_code;
            }
            $chart_of_account = BudgetExportAccountService::getChartOfAccount(
                $item->region,
                $item->council,
                $item->cost_center,
                $item->budget_class,
                '00000000',
                '0000',
                '00000000',
                $fund_type,
                $item->fund_source,
                $gfscode
            );

            $id = DB::table('aggregate_facility_budgets')->insertGetId([
                'admin_hierarchy_id' => $admin_hierarchy_id,
                'financial_year_id' => $financial_year_id, 'chart_of_accounts' => $chart_of_account,
                'timestamp' => $timestamps,
                'created_at' => $created_at, 'amount' => $item->amount
            ]);
          //save aggregate budgets
            BudgetExportAccount::saveAggreagetedAccounts($id, $item->export_accounts);
        }
    }

}
