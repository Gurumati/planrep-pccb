<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class GlActualSummaryControlId extends Model
{
    public $timestamps = true;
    //use SoftDeletes;
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'gl_actual_summary_control_ids';
}
