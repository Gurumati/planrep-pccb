<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RevenueExportAccount extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'revenue_export_accounts';

    protected $fillable = ['admin_hierarchy_id','section_id','financial_year_id','gfs_code_id','chart_of_accounts','financial_system_code','admin_hierarchy_ceiling_id','amount'];

    public static function getAllPaginated ($adminHierarchyId,$financialYearId,$financialSystem,$budgetType,$perPage){
       if($financialSystem == 'MUSE')
       {
           $financialSystem = array('MUSE');
       }
       else {
         $financialSystem = array($financialSystem);
       }
       $all = DB::table('revenue_export_accounts as rv')->
                      join('sections as s','s.id','rv.section_id')->
                      join('admin_hierarchy_ceilings as admc','admc.id','rv.admin_hierarchy_ceiling_id')->
                      join('gfs_codes as gfs','gfs.id','rv.gfs_code_id')->
                      leftJoin('revenue_export_transaction_items as re','re.revenue_export_account_id','rv.id')->
                      leftJoin('budget_export_to_financial_systems as ref',function($ref) {
                        $ref->on('ref.revenue_export_transaction_item_id','re.id')->where('ref.is_sent', true);  // ise_sent=false to return only items that were not sent
                      })->
                      where('rv.admin_hierarchy_id',$adminHierarchyId)->
                      where('rv.financial_year_id',$financialYearId)->
                      whereIn('rv.financial_system_code',$financialSystem)->
                      where('rv.amount', '>', 0.00)->
                      where('admc.budget_type', $budgetType)->
                      select('rv.*','s.name as section','gfs.code as gfs_code','gfs.description as description','ref.is_sent')->
                      orderBy('rv.id','desc')->
                      paginate($perPage);
         return $all;
    }

    public static function getAllSent($adminHierarchyId,$financialYearId,$financialSystem) {
        if($financialSystem == 'MUSE')
        {
            $financialSystem = array('MUSE');
        }
        else {
          $financialSystem = array($financialSystem);
        }
        return  DB::table('revenue_export_accounts as rv')->
            join('sections as s','s.id','rv.section_id')->
            join('gfs_codes as gfs','gfs.id','rv.gfs_code_id')->
            join('revenue_export_transaction_items as re','re.revenue_export_account_id','rv.id')->
            join('budget_export_to_financial_systems as ref',function($ref) {
                $ref->on('ref.revenue_export_transaction_item_id','re.id')->where('ref.is_sent', true);
            })->
            where('rv.admin_hierarchy_id',$adminHierarchyId)->
            where('rv.financial_year_id',$financialYearId)->
            whereIn('rv.financial_system_code',$financialSystem)->
            count();
    }

    public function section(){
        return $this->belongsTo('App\Models\Setup\Section')->select('id','name');
    }

    public function gfsCode(){
        return $this->belongsTo('App\Models\Setup\GfsCode')->select('id','description','code');
    }

    public function admin_hierarchy_ceiling(){
        return $this->belongsTo('App\Models\Budgeting\AdminHierarchyCeiling','admin_hierarchy_ceiling_id','id');
    }
}
