<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class BudgetExportTransactionItem extends Model
{
    public $timestamps = true;
    //use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'budget_export_transaction_items';
    protected $fillable =['budget_export_transaction_id','budget_export_account_id','amount','created_by','deleted_by','updated_by','budget_reallocation_item_id','is_credit'];

    public function budget_export_account(){
        return $this->belongsTo('App\Models\Execution\BudgetExportAccount','budget_export_account_id','id');
    }

    public function budget_export_transaction(){
        return $this->belongsTo('App\Models\Execution\BudgetExportTransaction','budget_export_transaction_id','id');
    }

    public function budget_reallocation_id(){
        return $this->belongsTo('App\Models\Execution\BudgetReallocationItem','budget_reallocation_item_id','id');
    }
}
