<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ActivityImplementation extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'activity_implementations';

    protected $fillable = ['activity_id', 'description', 'status_id',
        'period_id', 'achievement', 'achievement_value', 'completion_percentage',
        'facility_id', 'remarks', 'latitude', 'longitude'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'activity_id' => 'required|integer',
                'description' => 'sometimes|string',
                'status_id' => 'required|integer',
                'facility_id' => 'required|integer',
                'achievement' => 'required|string',
                'achievement_value' => 'required|numeric',
                'completion_percentage' => 'required|numeric',
                'remarks' => 'required|string',
                'latitude' => 'sometimes|numeric|nullable',
                'longitude' => 'sometimes|numeric|nullable',
            ],
            $merge);
    }

    public function activity()
    {
        return $this->belongsTo('App\Models\Planning\Activity');
    }

    public function attachments()
    {
        return $this->hasMany('App\Models\Execution\ActivityImplementationAttachment', 'implementation_id', 'id');
    }

    public function status()
    {

        return $this->belongsTo('App\Models\Planning\ActivityStatus', 'status_id', 'id');
    }

    public function period()
    {

        return $this->belongsTo('App\Models\Setup\Period', 'period_id', 'id');
    }

    public function procurement_method()
    {

        return $this->belongsTo('App\Models\Setup\ProcurementMethod', 'procurement_method_id', 'id');
    }

    public function facility()
    {

        return $this->belongsTo('App\Models\Setup\Facility', 'facility_id', 'id');
    }

    public static function facilityBudget($activityId, $facilityId)
    {
        $sql = "select sum(i.quantity*i.unit_price*i.frequency) as budget
                from activity_facility_fund_source_inputs i
                       join activity_facility_fund_sources a on i.activity_facility_fund_source_id = a.id
                       join activity_facilities a2 on a.activity_facility_id = a2.id
                       where a2.activity_id = $activityId and a2.facility_id = $facilityId
                and i.deleted_at is null
                group by a2.activity_id,a2.facility_id";
        return DB::select($sql);
    }

    public static function activityFacilityBudget($activityId, $facilityId)
    {
        return DB::table("activity_facility_fund_source_inputs as i")
            ->join('activity_facility_fund_sources as a', 'i.activity_facility_fund_source_id', '=', 'a.id')
            ->join('activity_facilities as a2', 'a.activity_facility_id', '=', 'a2.id')
            ->select(DB::raw('SUM(i*unit_price*i.quantity*i.frequency) as budget'))
            ->where('a2.activity_id', $activityId)
            ->where('a2.facility_id', $facilityId)
            ->groupBy('a2.activity_id', 'a2.facility_id')
            ->first();
    }

    public static function activityFacilityBudgetExpenditure($activityId, $facilityId, $adminHierarchyId, $financialYearId, $periodId, $costCentres,$budgetClassId,$fundSourceId,$budgetType)
    {
        $sql = "select distinct sub.budget,case when sub.expenditure is null then 0 else sub.expenditure end as expenditure
from
    (select
         budget.*, expenditure.expenditure
     from
         (select a.id,a.code,a.description,a.indicator,a.indicator_value, p.code as period_code,
                 sum(i.frequency*i.unit_price*i.quantity) as budget,
                 a.budget_type,f.id as facility_id,f.name as facility,ft.name as facility_type,
                 af.completion_percentage,ah.id as admin_hierarchy_id,
                 ah.name as admin_hierarchy,pr.id as project_id,pr.code as project_code,
                 pr.name as project,sec.id as cost_center_id,
                 sec.name as cost_center,sec.code as cost_center_code,
                 fu.id as fund_source_id,fu.name as fund_source,
                 fu.code as fund_source_code,count(CASE WHEN i.is_procurement THEN 1 END) as procurable
          from activities a
                   join mtef_sections ms on ms.id = a.mtef_section_id
                   join budget_classes b on b.id = a.budget_class_id
                   join projects pr on pr.id = a.project_id
                   join mtefs m on m.id = ms.mtef_id
                   join admin_hierarchies ah on ah.id = m.admin_hierarchy_id
                   join sections sec on sec.id = ms.section_id
                   join activity_facilities af on af.activity_id = a.id
                   join activity_facility_fund_sources affus on affus.activity_facility_id = af.id
                   join fund_sources fu on fu.id = affus.fund_source_id
                   join activity_facility_fund_source_inputs i on i.activity_facility_fund_source_id = affus.id
                   join facilities f on f.id = af.facility_id
                   join facility_types ft on ft.id = f.facility_type_id
                   join activity_periods ap on a.id = ap.activity_id
                   join periods p on p.id = ap.period_id
          where ms.section_id in ($costCentres) and b.id = $budgetClassId
            and fu.id = $fundSourceId
            and m.financial_year_id = $financialYearId
            and m.admin_hierarchy_id = $adminHierarchyId and a.deleted_at is null
            and a.budget_type = '$budgetType' and ap.period_id = $periodId and a.is_facility_account = true
          group by ah.id,a.id, p.code, ft.id,f.id,af.id,sec.id,pr.id,fu.id) as budget
             left join (
             select  b.activity_id,
                     f.facility_id,
                     sum(i.debit_amount::numeric(20,2)) - sum(i.credit_amount::numeric(20,2)) as expenditure
             from
                 budget_import_items i
                     join budget_export_accounts b on b.id = i.budget_export_account_id
                     join activity_facility_fund_source_inputs afi on afi.id = b.activity_facility_fund_source_input_id
                     join activity_facility_fund_sources af on af.id = afi.activity_facility_fund_source_id
                     join activity_facilities f on f.id = af.activity_facility_id
             where
                 i.deleted_at is null and b.amount > 0
             group by b.activity_id, f.facility_id
         ) as expenditure on expenditure.activity_id = budget.id and expenditure.facility_id = budget.facility_id

     union

     select
         budget.*, expenditure.expenditure
     from
         (select a.id,a.code,a.description,a.indicator,a.indicator_value, p.code as period_code,
                 sum(i.frequency*i.unit_price*i.quantity) as budget,
                 a.budget_type,f.id as facility_id,f.name as facility,ft.name as facility_type,
                 af.completion_percentage,ah.id as admin_hierarchy_id,
                 ah.name as admin_hierarchy,pr.id as project_id,pr.code as project_code,
                 pr.name as project,sec.id as cost_center_id,
                 sec.name as cost_center,sec.code as cost_center_code,
                 fu.id as fund_source_id,fu.name as fund_source,
                 fu.code as fund_source_code,count(CASE WHEN i.is_procurement THEN 1 END) as procurable
          from activities a
                   join mtef_sections ms on ms.id = a.mtef_section_id
                   join budget_classes b on b.id = a.budget_class_id
                   join projects pr on pr.id = a.project_id
                   join mtefs m on m.id = ms.mtef_id
                   join admin_hierarchies ah on ah.id = m.admin_hierarchy_id
                   join sections sec on sec.id = ms.section_id
                   join activity_facilities af on af.activity_id = a.id
                   join activity_facility_fund_sources affus on affus.activity_facility_id = af.id
                   join fund_sources fu on fu.id = affus.fund_source_id
                   join activity_facility_fund_source_inputs i on i.activity_facility_fund_source_id = affus.id
                   join facilities f on f.id = af.facility_id
                   join facility_types ft on ft.id = f.facility_type_id
                   join activity_periods ap on a.id = ap.activity_id
                   join periods p on p.id = ap.period_id
          where ms.section_id in ($costCentres) and b.id = $budgetClassId
            and fu.id = $fundSourceId
            and m.financial_year_id = $financialYearId
            and m.admin_hierarchy_id = $adminHierarchyId and a.deleted_at is null
            and a.budget_type = '$budgetType'  and a.is_facility_account = true
          group by ah.id,a.id, p.code, ft.id,f.id,af.id,sec.id,pr.id,fu.id) as budget
             right join (
             select  b.activity_id,
                     f.facility_id,
                     sum(i.debit_amount::numeric(20,2)) - sum(i.credit_amount::numeric(20,2)) as expenditure
             from
                 budget_import_items i
                     join budget_export_accounts b on b.id = i.budget_export_account_id
                     join activity_facility_fund_source_inputs afi on afi.id = b.activity_facility_fund_source_input_id
                     join activity_facility_fund_sources af on af.id = afi.activity_facility_fund_source_id
                     join activity_facilities f on f.id = af.activity_facility_id
                     join activities a on a.id = f.activity_id
                     join mtef_sections ms on ms.id = a.mtef_section_id
             where
                 i.deleted_at is null and b.amount > 0 and i.period_id = $periodId
               and b.admin_hierarchy_id = $adminHierarchyId and b.financial_year_id = $financialYearId and
                     ms.section_id in ($costCentres) and a.budget_class_id = $budgetClassId
               and af.fund_source_id = $fundSourceId
             group by b.activity_id, f.facility_id
         ) as expenditure on (expenditure.activity_id = budget.id and expenditure.facility_id = budget.facility_id)
    ) as sub
        left join
    (
        select
            af.activity_id,
            max(acs.id) as activity_status_id,
            count(a3.id) as attachments
        from
            activities a
                join activity_facilities af on a.id = af.activity_id
                join facilities f on f.id = af.facility_id
                left join activity_statuses acs on acs.id = af.activity_status_id
                left join activity_implementations ai on ai.activity_id = a.id and ai.facility_id = f.id
                left join activity_implementation_attachments a3 on ai.id = a3.implementation_id
        group by af.activity_id
    ) as activity_imp on (activity_imp.activity_id = sub.id) where sub.facility_id = $facilityId and sub.id = $activityId;";
        return collect(DB::select($sql))->first();
    }

    public static function budgetClasses($adminHierarchyId, $financialYearId, $sectionId, $versionId)
    {
        $sql = "select distinct b.id,b.name,b.control_code from budget_classes b
          join budget_class_versions bv on bv.budget_class_id = b.id
          join activities a on b.id = a.budget_class_id
          join activity_facilities af on af.activity_id = a.id
          join activity_facility_fund_sources affs on affs.activity_facility_id = af.id
          join activity_facility_fund_source_inputs inp on inp.activity_facility_fund_source_id = affs.id
        join mtef_sections m2 on a.mtef_section_id = m2.id
        join sections s2 on m2.section_id = s2.id
        join mtefs m3 on m2.mtef_id = m3.id
        where m3.financial_year_id = '$financialYearId' 
          and m3.admin_hierarchy_id = '$adminHierarchyId'
          and bv.version_id = '$versionId'
        and s2.id in ($sectionId) and b.is_active = true
        group by b.id having(sum(inp.quantity*inp.unit_price*inp.frequency) > 0)";
        $items = DB::select($sql);
        return $items;
    }

    public static function costCentreFundSources($adminHierarchyId, $financialYearId, $sectionId,$facilityId,$versionId)
    {
        $sql = "select distinct fs.id,fs.name,fs.code from fund_sources fs
          join fund_source_versions fv on fv.fund_source_id = fs.id
          join activity_facility_fund_sources a on fs.id = a.fund_source_id
          join activity_facility_fund_source_inputs inp on inp.activity_facility_fund_source_id = a.id
          join activity_facilities a2 on a.activity_facility_id = a2.id
          join activities a3 on a2.activity_id = a3.id
          join mtef_sections m2 on a3.mtef_section_id = m2.id
          join sections s2 on m2.section_id = s2.id
          join mtefs m3 on m2.mtef_id = m3.id
        where s2.id in ($sectionId) 
          and m3.admin_hierarchy_id = '$adminHierarchyId' 
          and fv.version_id = '$versionId' 
          and m3.financial_year_id = '$financialYearId' and a2.facility_id = '$facilityId'
        group by fs.id having(sum(inp.quantity*inp.unit_price*inp.frequency) > 0) order by fs.code asc;";
        $items = DB::select($sql);
        return $items;
    }
}
