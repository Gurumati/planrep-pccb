<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetImport extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'budget_imports';

    public function data_source(){
        return $this->belongsTo('App\Models\Execution\DataSource');
    }

    public function import_method(){
        return $this->belongsTo('App\Models\Execution\ImportMethod');
    }
}
