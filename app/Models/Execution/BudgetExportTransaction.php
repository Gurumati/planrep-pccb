<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetExportTransaction extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'budget_export_transactions';

    protected $fillable = ['description','budget_transaction_type_id','created_by','updated_by','control_number'];

    public function budget_transaction_type(){
        return $this->belongsTo('App\Models\Execution\BudgetTransactionType');
    }
}
