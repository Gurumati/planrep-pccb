<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Models\Planning\Activity;
use App\Models\Setup\BudgetClass;
use App\Models\Report\Ceiling;
use App\Models\Budgeting\AdminHierarchyCeiling;
use App\Models\Setup\GfsCode;
use Illuminate\Support\Facades\Log;
use App\Models\Setup\ConfigurationSetting;

class BudgetMassReallocation extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'budget_reallocations';

    public function data_source()
    {
        return $this->belongsTo('App\Models\Execution\DataSource');
    }

    public function import_method()
    {
        return $this->belongsTo('App\Models\Execution\ImportMethod');
    }

    public static function reallocateBudget($budgetType, $financialYearId, $adminHierarchyId, $sectorId, $fromBudgetClass, $toBudgetClass,$fundSourceIds,$exportTransactionsToEpicor, $exportTransactionsToFfars)
    {

        $sectionIds = DB::table('sections as s')
            ->join('section_levels as sl', 'sl.id', 's.section_level_id')
            ->where('sector_id', $sectorId)
            ->where('sl.hierarchy_position', 4)
            ->pluck('s.id');

        $data = [];

        $chunkSize = 100;
        $activities = DB::table('activities as a')
            ->join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')
            ->join('mtefs as m', 'm.id', 'ms.mtef_id')
            ->join('activity_facilities as af', 'a.id', 'af.activity_id')
            ->join('activity_facility_fund_sources as aff', 'af.id', 'aff.activity_facility_id')
            ->where('m.financial_year_id', $financialYearId)
            ->where('m.admin_hierarchy_id', $adminHierarchyId)
            ->whereIn('ms.section_id', $sectionIds)
            ->whereIn('aff.fund_source_id', $fundSourceIds)
            ->where('a.budget_class_id', $fromBudgetClass->id)
            ->where('a.budget_type', $budgetType)
            ->groupBy('a.id')
            ->orderBy('a.id')
            ->select('a.*')
            ->get();
            $accounts = [0];

        $transactionTypeId = ConfigurationSetting::getValueByKey('CHANGE_OF_ACTIVITY_BUDGET_CLASS_TRANSACTION_TYPE');

        foreach ($activities as $a) {
            $processed = [];
            array_push($processed, 'fromActivity', $a);

            try{
                DB::beginTransaction();
                    $budgetExportAccounts = BudgetExportAccount::where('activity_id', $a->id)->get();
                    
                    foreach ($budgetExportAccounts as $bea) {
                        array_push($processed, 'fromCOA', $bea);
                        $fromBudgetClassCode = '-' . $fromBudgetClass->code . '-';
                        $toBudgetClassCode = '-' . $toBudgetClass->code . '-';
                        $toCOA = str_replace_first($fromBudgetClassCode, $toBudgetClassCode, $bea->chart_of_accounts);
                        $itemIds = BudgetExportTransactionItem::where('budget_export_account_id', $bea->id)->pluck('id');
                        
                        $befsUpdate = ['chart_of_accounts' => $toCOA];
                        $beaUpdate = ['chart_of_accounts' => $toCOA];

                        if((!$a->is_facility_account && $exportTransactionsToEpicor == 'true') || ($a->is_facility_account && $exportTransactionsToFfars == 'true') ){
                            $befsUpdate = ['chart_of_accounts' => $toCOA,  'is_sent' => false];
                            $beaUpdate = ['chart_of_accounts' => $toCOA, 'is_sent' => false, 'is_delivered' => false];

                            $transaction = BudgetExportTransaction::create(['budget_transaction_type_id' => $transactionTypeId, 'description' => 'CHANGE OF ACTIVITY BUDGET CLASS', 'is_sent' => false]);
                            BudgetExportTransactionItem::whereIn('id', $itemIds)->update(['budget_export_transaction_id' => $transaction->id]);
                        }
                        BudgetExportToFinancialSystem::whereIn('budget_export_transaction_item_id', $itemIds)
                            ->update($befsUpdate);

                        $bea->update($beaUpdate);
                        array_push($processed, 'toCOA', $bea);
                        array_push($accounts, $bea->id);
                        //unregister gl account
                        DB::table('company_gl_accounts')->where('budget_export_account_id',$bea->id)->delete();

                    }
                    $activity = Activity::find($a->id)->update(['budget_class_id' => $toBudgetClass->id]);
                    array_push($processed, 'toActivity', $activity);
                DB::commit();
            }
            catch(\Exception $e){
               Log::error($e);
               DB::rollback();
            }
            array_push($data, 'processed', $processed);
        }
        BudgetExportAccount::generateTransaction($accounts, $budgetType);

        return $data;
    }

    public static function reallocateCeiling($budgetType, $financialYearId, $adminHierarchyId, $sectorId, $fromBudgetClass, $toBudgetClass,$fundSourceIds)
    {
        
        $adminHierarchyCeilings = DB::table('admin_hierarchy_ceilings as adc')
            ->join('ceilings as c', 'c.id', 'adc.ceiling_id')
            ->join('gfs_codes as gfs', 'gfs.id', 'c.gfs_code_id')
            ->join('fund_sources as f', 'f.id', 'gfs.fund_source_id')
            ->join('ceiling_sectors as cs', 'cs.ceiling_id', 'c.id')
            ->where('cs.sector_id',$sectorId)
            ->where('f.can_project', false)
            ->where('adc.budget_type', $budgetType)
            ->where('adc.financial_year_id', $financialYearId)
            ->where('adc.admin_hierarchy_id', $adminHierarchyId)
            ->where('cs.sector_id', $sectorId)
            ->where('c.budget_class_id', $fromBudgetClass->id)
            ->whereIn('f.id', $fundSourceIds)
            ->select('adc.*','c.extends_to_facility','c.budget_class_id','f.id as fund_source_id')
            ->get();
        $ceilingIds = [];

        DB::transaction(function () use ($fromBudgetClass, $toBudgetClass, $adminHierarchyCeilings,$sectorId,$ceilingIds) {

            foreach ($adminHierarchyCeilings as $admc) {

            $fromCeiling = DB::table('ceilings as c')
                ->join('gfs_codes as gfs', 'gfs.id', 'c.gfs_code_id')
                ->join('ceiling_sectors as cs', 'cs.ceiling_id', 'c.id')
                ->where('cs.sector_id',$sectorId)
                ->where('c.budget_class_id', $fromBudgetClass->id)
                ->where('gfs.fund_source_id', $admc->fund_source_id)
                ->select('c.*')
                ->first();

            $toCeiling = DB::table('ceilings as c')
                ->join('gfs_codes as gfs', 'gfs.id', 'c.gfs_code_id')
                ->join('ceiling_sectors as cs', 'cs.ceiling_id', 'c.id')
                ->where('cs.sector_id',$sectorId)
                ->where('c.budget_class_id', $toBudgetClass->id)
                ->where('gfs.fund_source_id', $admc->fund_source_id)
                ->select('c.*')
                ->first();

            if (!isset($toCeiling)) {
                $toCeiling = new Ceiling();
                $toCeiling->name = $fromCeiling->name;
                $toCeiling->budget_class_id = $toBudgetClass->id;
                $toCeiling->is_active = true;
                $toCeiling->gfs_code_id = $fromCeiling->gfs_code_id;
                $toCeiling->is_aggregate = false;
                $toCeiling->aggregate_fund_source_id = null;
                $toCeiling->extends_to_facility = $fromCeiling->extends_to_facility;
                $toCeiling->save();
                
                DB::table('ceiling_sectors')
                ->insert(['sector_id'=>$sectorId,'ceiling_id'=>$toCeiling->id]);

                DB::table('fund_source_budget_classes')->insert(['fund_source_id'=>$admc->fund_source_id,'budget_class_id'=>$toBudgetClass->id]);
                
            }

                $revenueExportAccounts = RevenueExportAccount::where('admin_hierarchy_ceiling_id', $admc->id)->get();
                foreach ($revenueExportAccounts as $rea) {
                    $fromBudgetClassCode = '-' . $fromBudgetClass->code . '-';
                    $toBudgetClassCode = '-' . $toBudgetClass->code . '-';
                    $toCOA = str_replace_first($fromBudgetClassCode, $toBudgetClassCode, $rea->chart_of_accounts);
                    $itemIds = RevenueExportTransactionItem::where('revenue_export_account_id', $rea->id)->pluck('id');

                    $befsUpdate = ['chart_of_accounts' => $toCOA];
                    $beaUpdate = ['chart_of_accounts' => $toCOA];

                    if(!$admc->extends_to_facility || ($admc->extends_to_facility && !in_array($admc->section_id,[95,94,89]))){
                        $befsUpdate = ['chart_of_accounts' => $toCOA,  'is_sent' => false];
                        $beaUpdate = ['chart_of_accounts' => $toCOA, 'is_sent' => false, 'is_delivered' => false];
                    }

                    BudgetExportToFinancialSystem::whereIn('revenue_export_transaction_item_id', $itemIds)
                        ->update($befsUpdate);

                    $rea->update($beaUpdate);
                }
                if (!in_array($admc->ceiling_id, $ceilingIds)) {
                    array_push($ceilingIds, $admc->ceiling_id);
                }
               // try{
                    
                    AdminHierarchyCeiling::find($admc->id)->update(['ceiling_id' => $toCeiling->id]);
                // }
                // catch(\Exception $e){

                // }
            }
        });


    }

    public static function getBudget($budgetType, $financialYearId, $adminHierarchyId, $sectorId, $budgetClassId,$fundSourceIds)
    {

        $sectionIds = DB::table('sections as s')
            ->join('section_levels as sl', 'sl.id', 's.section_level_id')
            ->where('sector_id', $sectorId)
            ->where('sl.hierarchy_position', 4)
            ->pluck('s.id');

        return DB::table('activities as a')
            ->join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')
            ->join('mtefs as m', 'm.id', 'ms.mtef_id')
            ->join('activity_facilities as af', 'a.id', 'af.activity_id')
            ->join('activity_facility_fund_sources as aff', 'af.id', 'aff.activity_facility_id')
            ->join('activity_facility_fund_source_inputs as affi', 'aff.id', 'affi.activity_facility_fund_source_id')
            ->where('m.financial_year_id', $financialYearId)
            ->where('m.admin_hierarchy_id', $adminHierarchyId)
            ->whereIn('ms.section_id', $sectionIds)
            ->whereIn('aff.fund_source_id', $fundSourceIds)
            ->where('a.budget_class_id', $budgetClassId)
            ->where('a.budget_type', $budgetType)
            ->sum(DB::Raw('affi.quantity * affi.frequency * affi.unit_price'));
    }

    public static function getCeiling($budgetType, $financialYearId, $adminHierarchyId, $sectorId, $budgetClassId,$fundSourceIds)
    {
        
        $sectionIds = DB::table('sections as s')
            ->join('section_levels as sl', 'sl.id', 's.section_level_id')
            ->where('sector_id', $sectorId)
            ->where('sl.hierarchy_position', 4)
            ->pluck('s.id');

        return DB::table('admin_hierarchy_ceilings as adc')
            ->join('ceilings as c', 'c.id', 'adc.ceiling_id')
            ->join('gfs_codes as gfs', 'gfs.id', 'c.gfs_code_id')
            ->join('fund_sources as f', 'f.id', 'gfs.fund_source_id')
            ->where('f.can_project', false)
            ->where('adc.is_facility', false)
            ->where('adc.budget_type', $budgetType)
            ->where('adc.financial_year_id', $financialYearId)
            ->where('adc.admin_hierarchy_id', $adminHierarchyId)
            ->whereIn('adc.section_id', $sectionIds)
            ->where('c.budget_class_id', $budgetClassId)
            ->whereIn('f.id', $fundSourceIds)
            ->sum('adc.amount');

    }
}
