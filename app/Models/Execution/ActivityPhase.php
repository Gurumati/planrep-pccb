<?php

namespace App\Models\Execution;

use App\Http\Services\CustomPager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ActivityPhase extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'activity_phases';

    protected $fillable = ['activity_id', 'description'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'activity_id' => 'required|integer',
                'description' => 'required|max:255'
            ],
            $merge);
    }


    public function activity()
    {
        return $this->belongsTo('App\Models\Planning\Activity', 'activity_id', 'id');
    }

    public function milestones()
    {
        return $this->hasMany("App\Models\Execution\ActivityPhaseMilestone", "activity_phase_id", "id");
    }

    public static function activities($financialYearId, $adminHierarchyId, $sectionId,$budgetClassId, $fundSourceId, $perPage = 10, $page = 1)
    {
        $sql = "select ac.id,ac.code,ac.description,ac.activity_task_nature_id,(CASE WHEN ac.is_facility_account = true THEN 'FACILITY ACCOUNT' ELSE 'COUNCIL ACCOUNT' END) 
                AS account_type,f2.id as facility_id,f2.name as facility,ft.name as type,
                (select string_agg(p.name,',') from activity_periods ap join periods p on p.id = ap.period_id
                where ap.activity_id = ac.id) as periods,
                sum(inp.quantity*inp.unit_price*inp.frequency) as budget,
                (select count(acp.id) from activity_phases acp join activities a on a.id = acp.activity_id
                where acp.activity_id = ac.id group by a.id) as phases,
                (select sum(acpm.amount) from activity_phase_milestones acpm
                  join activity_phases acp on acp.id = acpm.activity_phase_id join activities a on a.id = acp.activity_id join facilities fac on fac.id = acpm.facility_id
                where acp.activity_id = ac.id and f2.id = acpm.facility_id group by a.id,fac.id) as allocated_amount,fs.name as fund_source,fs.code as fund_source_code,b.control_code as budget_class,at.name as task_nature
                from activity_facility_fund_source_inputs inp
                  join activity_facility_fund_sources afs on inp.activity_facility_fund_source_id = afs.id
                  join fund_sources fs on fs.id = afs.fund_source_id
                  join activity_facilities af on afs.activity_facility_id = af.id
                  join activities ac on ac.id = af.activity_id
                  join budget_classes b on b.id = ac.budget_class_id
                  join activity_task_natures at on at.id = ac.activity_task_nature_id
                  join mtef_sections ms on ms.id = ac.mtef_section_id
                  join sections s on s.id = ms.section_id
                  join mtefs m on m.id = ms.mtef_id
                  join financial_years f on f.id = m.financial_year_id
                  join admin_hierarchies ah on ah.id = m.admin_hierarchy_id
                  join facilities f2 on af.facility_id = f2.id
                  join facility_types ft on f2.facility_type_id = ft.id
                  where ah.id = '$adminHierarchyId' 
                  and f.id = '$financialYearId' 
                  and s.id = '$sectionId' and fs.id = '$fundSourceId' and b.id = '$budgetClassId' 
                  group by ac.id,ft.id,f2.id,fs.id,b.id,at.id 
                  having sum(inp.quantity*inp.unit_price*inp.frequency) > 0 order by ac.code asc";
        $items = DB::select($sql);
        return CustomPager::paginate($items, $perPage, $page);
    }

    public static function costCentreFundSources($adminHierarchyId, $financialYearId, $sectionId,$budgetClassId)
    {
        $sql = "select distinct fs.id,fs.name,fs.code from fund_sources fs
          join activity_facility_fund_sources a on fs.id = a.fund_source_id
          join activity_facility_fund_source_inputs inp on inp.activity_facility_fund_source_id = a.id
          join activity_facilities a2 on a.activity_facility_id = a2.id
          join activities a3 on a2.activity_id = a3.id
          join mtef_sections m2 on a3.mtef_section_id = m2.id
          join sections s2 on m2.section_id = s2.id
          join mtefs m3 on m2.mtef_id = m3.id
        where s2.id = '$sectionId' and m3.admin_hierarchy_id = '$adminHierarchyId' 
        and m3.financial_year_id = '$financialYearId' and a3.budget_class_id = '$budgetClassId'
        group by fs.id having(sum(inp.quantity*inp.unit_price*inp.frequency) > 0) order by fs.code asc;";
        $items = DB::select($sql);
        return $items;
    }

    public static function budgetClasses($adminHierarchyId, $financialYearId, $sectionId)
    {
        $sql = "select distinct b.id,b.name,b.control_code from budget_classes b
          join activities a on b.id = a.budget_class_id
          join activity_facilities af on af.activity_id = a.id
          join activity_facility_fund_sources affs on affs.activity_facility_id = af.id
          join activity_facility_fund_source_inputs inp on inp.activity_facility_fund_source_id = affs.id
        join mtef_sections m2 on a.mtef_section_id = m2.id
        join sections s2 on m2.section_id = s2.id
        join mtefs m3 on m2.mtef_id = m3.id
        where m3.financial_year_id = '$financialYearId' and m3.admin_hierarchy_id = '$adminHierarchyId' 
        and s2.id = '$sectionId' and b.is_active = true
        group by b.id having(sum(inp.quantity*inp.unit_price*inp.frequency) > 0)";
        $items = DB::select($sql);
        return $items;
    }

    public static function lostData($financialYearId, $adminHierarchyId, $sectionId,$budgetClassId, $fundSourceId)
    {
        $sql = "select ac.id,ac.code,ac.description,ac.activity_task_nature_id,(CASE WHEN ac.is_facility_account = true THEN 'FACILITY ACCOUNT' ELSE 'COUNCIL ACCOUNT' END) 
                AS account_type,f2.id as facility_id,f2.name as facility,ft.name as type,
                (select string_agg(p.name,',') from activity_periods ap join periods p on p.id = ap.period_id
                where ap.activity_id = ac.id) as periods,
                sum(inp.quantity*inp.unit_price*inp.frequency) as budget,
                (select count(acp.id) from activity_phases acp join activities a on a.id = acp.activity_id
                where acp.activity_id = ac.id group by a.id) as phases,
                (select sum(acpm.amount) from activity_phase_milestones acpm
                  join activity_phases acp on acp.id = acpm.activity_phase_id join activities a on a.id = acp.activity_id join facilities fac on fac.id = acpm.facility_id
                where acp.activity_id = ac.id and f2.id = acpm.facility_id group by a.id,fac.id) as allocated_amount,fs.name as fund_source,fs.code as fund_source_code,b.control_code as budget_class,at.name as task_nature
                from activity_facility_fund_source_inputs inp
                  join activity_facility_fund_sources afs on inp.activity_facility_fund_source_id = afs.id
                  join fund_sources fs on fs.id = afs.fund_source_id
                  join activity_facilities af on afs.activity_facility_id = af.id
                  join activities ac on ac.id = af.activity_id
                  join budget_classes b on b.id = ac.budget_class_id
                  join activity_task_natures at on at.id = ac.activity_task_nature_id
                  join mtef_sections ms on ms.id = ac.mtef_section_id
                  join sections s on s.id = ms.section_id
                  join mtefs m on m.id = ms.mtef_id
                  join financial_years f on f.id = m.financial_year_id
                  join admin_hierarchies ah on ah.id = m.admin_hierarchy_id
                  join facilities f2 on af.facility_id = f2.id
                  join facility_types ft on f2.facility_type_id = ft.id
                  where ah.id = '$adminHierarchyId' 
                  and f.id = '$financialYearId' 
                  and s.id = '$sectionId' and fs.id = '$fundSourceId' and b.id = '$budgetClassId' 
                  group by ac.id,ft.id,f2.id,fs.id,b.id,at.id 
                  having sum(inp.quantity*inp.unit_price*inp.frequency) > 0 order by ac.code asc";
        $items = DB::select($sql);
        return $items;
    }
}
