<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use App\Models\Setup\AdminHierarchy;
use Illuminate\Support\Facades\DB;
use App\Models\Setup\GfsCode;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\FundSource;
use App\Http\Controllers\Execution\MuseIntegrationController;//This for muse , add Controller for erms if not this
use App\Models\Execution\BudgetExportAccountGLAccountExportIssues;

use App\Models\Execution\BudgetExportResponse;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ReadDataFromErms extends Model
{
    //function that consume budget submitted by erms
    public static function processBudgetData($data){

        $budgetExist = array();
        $sender = $data["message"]["messageHeader"]["sender"];
        $receiver = $data["message"]["messageHeader"]["receiver"];
        $admin_hierarchy_code = $data["message"]["messageSummary"]["company"];
        $messageType = $data["message"]["messageHeader"]["messageType"];
        $orgMsgId = $data["message"]["messageHeader"]["msgId"];
        $c = AdminHierarchy::where('code', $admin_hierarchy_code)->first();
        $admin_hierarchy_id = isset($c->id) ? $c->id : null;

        if($admin_hierarchy_id > 0){

            $applyDate = $data["message"]["messageSummary"]["applyDate"];
            $je_date = isset($applyDate) ? $applyDate : null;

            $financial_year_query = "select * from financial_years where '".$je_date."'::date between start_date and end_date";
            $get_financial_year = DB::select(DB::raw($financial_year_query));
            $financial_year_id  = isset($get_financial_year[0]->id) ? $get_financial_year[0]->id: null;

            $admin_hierarchy_exist_query = "select * from plan_chains where admin_hierarchy_id = '".$admin_hierarchy_id."'";
            $get_admin_hierarchy_exist = DB::select(DB::raw($admin_hierarchy_exist_query));
            $admin_hierarchy_exist_id  = isset($get_admin_hierarchy_exist[0]->id) ? $get_admin_hierarchy_exist[0]->id: null;

            $message = $data["message"]["messageDetails"];

            foreach($message as $budgetDetails){

                if ($admin_hierarchy_exist_id){
                    $responseTomuse = array(
                       "sender" => $sender,
                       "receiver" => $receiver,
                       "messageType" => $messageType,
                       "trNumber" => $admin_hierarchy_code,
                       "status" => "Fail",
                       "description" => "BUDGET ALREADY EXIST IN PLANREP",
                    );
                    array_push($budgetExist,$responseTomuse);
                }else{
                    $responseTomuse = array(
                        "sender" => $sender,
                        "receiver" => $receiver,
                        "messageType" => $messageType,
                        "trNumber" => $admin_hierarchy_code,
                        "status" => "Success",
                        "description" => "BUDGET RECEIVED ON PLANREP",
                     );
                     array_push($budgetExist,$responseTomuse);

                    $planChainObjectiveData = array(
                        'description' => $budgetDetails['objectiveDescription'],
                        'code' => $budgetDetails['objectiveCode'],
                        'is_active' => true,
                        'created_at' => \Carbon\Carbon::now(),
                        'parent_id' => null,
                        'plan_chain_type_id' => 1,
                        'admin_hierarchy_id' =>$admin_hierarchy_id
                    );
    
                    DB::transaction(function() use ($planChainObjectiveData) {
                        DB::table('plan_chains')->insert([$planChainObjectiveData]);
                    });
    
                    $so_parent_query = "select * from plan_chains where admin_hierarchy_id = '".$admin_hierarchy_id."' and code = '".$budgetDetails['objectiveCode']."'";
                    $get_so_parent = DB::select(DB::raw($so_parent_query));
                    $so_parent_id  = isset($get_so_parent[0]->id) ? $get_so_parent[0]->id: null;
    
                    
                    $planChainServiceOutputData = array(
                        'description' => $budgetDetails['serviceOutputDescription'],
                        'code' => $budgetDetails['serviceOutputCode'],
                        'is_active' => true,
                        'created_at' => \Carbon\Carbon::now(),
                        'parent_id' => $so_parent_id,
                        'plan_chain_type_id' => 2,
                        'admin_hierarchy_id' =>$admin_hierarchy_id
                    );
    
                    DB::transaction(function() use ($planChainServiceOutputData) {
                        DB::table('plan_chains')->insert([$planChainServiceOutputData]);
                    });

                    $pi_parent_query = "select * from plan_chains where admin_hierarchy_id = '".$admin_hierarchy_id."' and parent_id = '".$so_parent_id."' and description = '".$budgetDetails['serviceOutputDescription']."'";
                    $get_pi_parent = DB::select(DB::raw($pi_parent_query));
                    $pi_parent_id  = isset($get_pi_parent[0]->id) ? $get_pi_parent[0]->id: null;
    
                    
                    $performanceIndicatorData = array(
                        'description' => $budgetDetails['indicatorDescription'],
                        'number' => $budgetDetails['indicatorValue'],
                        'plan_chain_id' => $pi_parent_id,
                        'is_qualitative' => $budgetDetails['is_qualitative'],
                        'less_is_good' => $budgetDetails['less_is_good'],
                        'created_at' => \Carbon\Carbon::now(),
                        'section_id' => 1
                    );
    
                    DB::transaction(function() use ($performanceIndicatorData) {
                        DB::table('performance_indicators')->insert([$performanceIndicatorData]);
                    });
                }
                
            }

            $responseData =  app('App\Http\Controllers\Execution\MuseIntegrationController')->response($orgMsgId,"BUDGET","PROCESSED","Response from Planrep",$budgetExist);
            try {
                $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($responseData);
                //code...
            } catch (\Throwable $th) {
                Log::info("ERROR");
            }



        }

    }


    public static function getCeilingFeedbck($data){
        $status = $data["message"]["messageSummary"]["responseStatus"];
       if($status == "PROCESSED" || $status == "SUCCESS" || $status == "RECEIVED"){
           $orgMsgId = $data["message"]["messageSummary"]["orgMsgId"];
           $company = $data["message"]["messageSummary"]["company"];
           $feedbackDetails = $data["message"]["messageDetails"];
           foreach($feedbackDetails as $detail){
               $uid = $detail["uid"];
               $statusCode = $detail["statusCode"];
               $statusDesc = $detail["statusDesc"];
               $additionalInfo = $detail["additionalInfo"];
               $status = $statusDesc=='success'?true:false;
               DB::table('admin_hierarchy_ceilings')->where('id',$uid)->update([
                   'export_response' => $status,
                   'is_delivered' =>true,
                   'updated_at' =>\Carbon\Carbon::now()
               ]);
           }

       }

   }


   public static function processCeilingDisData($data){

    $ceilingExist = array();
    $sender = $data["message"]["messageHeader"]["sender"];
    $receiver = $data["message"]["messageHeader"]["receiver"];
    $admin_hierarchy_code = $data["message"]["messageSummary"]["company"];
    $messageType = $data["message"]["messageHeader"]["messageType"];
    $orgMsgId = $data["message"]["messageHeader"]["msgId"];
    $c = AdminHierarchy::where('code', $admin_hierarchy_code)->first();
    $admin_hierarchy_id = isset($c->id) ? $c->id : null;

    if($admin_hierarchy_id > 0){

        $applyDate = $data["message"]["messageSummary"]["applyDate"];
        $je_date = isset($applyDate) ? $applyDate : null;

        $financial_year_query = "select * from financial_years where '".$je_date."'::date between start_date and end_date";
        $get_financial_year = DB::select(DB::raw($financial_year_query));
        $financial_year_id  = isset($get_financial_year[0]->id) ? $get_financial_year[0]->id: null;

        $admin_hierarchy_exist_query = "select * from plan_chains where admin_hierarchy_id = '".$admin_hierarchy_id."'";
        $get_admin_hierarchy_exist = DB::select(DB::raw($admin_hierarchy_exist_query));
        $admin_hierarchy_exist_id  = isset($get_admin_hierarchy_exist[0]->id) ? $get_admin_hierarchy_exist[0]->id: null;

        $message = $data["message"]["messageDetails"];

        foreach($message as $ceilingDetails){

            //$depCode = $ceilingDetails['departmentCode'];
            $costCentreCode = $ceilingDetails['costCentreCode'];
            $sbcCode = $ceilingDetails['subBudgetClassCode'];
            $gfsCode = $ceilingDetails['gfsCode'];

            $section_query = "select * from admin_hierarchy_sections ahc
            join sections s on ahc.section_id = s.id
            where s.code = '".$costCentreCode."' and ahc.admin_hierarchy_id = '".$admin_hierarchy_id."'";
            $get_section_id = DB::select(DB::raw($section_query));
            $section_id  = isset($get_section_id[0]->id) ? $get_section_id[0]->id: null;

            $ceiling_query = "select * from ceilings c
            join budget_classes bc on c.budget_class_id = bc.id
            join gfs_codes gc on c.gfs_code_id = gc.id
            where bc.code = '".$sbcCode."' and gc.code = '".$gfsCode."'";
            $get_ceiling_id = DB::select(DB::raw($ceiling_query));
            $ceiling_id  = isset($get_ceiling_id[0]->id) ? $get_ceiling_id[0]->id: null;

            $ceiling_exist_query = "select * from admin_hierarchy_ceilings where ceiling_id = '".$ceiling_id."'";
            $get_ceiling_exist = DB::select(DB::raw($ceiling_exist_query));
            $ceiling_exist_id  = isset($get_ceiling_exist[0]->id) ? $get_ceiling_exist[0]->id: null;

            if ($ceiling_exist_id){
                $responseToErms = array(
                   "sender" => $sender,
                   "receiver" => $receiver,
                   "messageType" => $messageType,
                   "trNumber" => $admin_hierarchy_code,
                   "status" => "Fail",
                   "description" => "CEILING ALREADY EXIST IN PLANREP",
                );
                array_push($ceilingExist,$responseToErms);
            }else{
                $responseToErms = array(
                    "sender" => $sender,
                    "receiver" => $receiver,
                    "messageType" => $messageType,
                    "trNumber" => $admin_hierarchy_code,
                    "status" => "Success",
                    "description" => "CEILING RECEIVED ON PLANREP",
                 );
                 array_push($ceilingExist,$responseToErms);

                $ceilingData = array(
                    'ceiling_id' => $ceiling_id,
                    'amount' => $ceilingDetails['amount'],
                    'created_at' => \Carbon\Carbon::now(),
                    'admin_hierarchy_id' =>$admin_hierarchy_id,
                    'financial_year_id' =>$financial_year_id,
                    'section_id' =>$section_id
                );

                DB::transaction(function() use ($ceilingData) {
                    DB::table('admin_hierarchy_ceilings')->insert([$ceilingData]);
                });
  
            }
            
        }

        $responseData =  app('App\Http\Controllers\Execution\MuseIntegrationController')->response($orgMsgId,"CEILINGDISSEMINATION","PROCESSED","Response from Planrep",$ceilingExist);
        try {
            $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($responseData);
            //code...
        } catch (\Throwable $th) {
            Log::info("ERROR");
        }



    }

}

}
