<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ActivityProjectOutput extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'activity_project_outputs';
    protected $with = ['project_output'];

    protected $fillable = ['activity_id', 'project_output_id','value','facility_id'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'activity_id' => 'required|integer',
                'project_output_id' => 'required|integer',
                'value' => 'required|string'
            ],
            $merge);
    }

    public function activity() {
        return $this->belongsTo('App\Models\Planning\Activity','activity_id','id');
    }

    public function project_output() {
        return $this->belongsTo('App\Models\Setup\ProjectOutput','project_output_id','id');
    }

    //check if all project output data has been updated
    public static function projectOutputFilled($admin_hierarchy_id, $financial_year_id,$budget_type){
        $sectionId = Input::get('sectionId');
        $query = "SELECT s.name, count(ac.*) not_filled from 
                    mtefs m
                    join mtef_sections ms on ms.mtef_id = m.id
                    join activities ac on ac.mtef_section_id = ms.id
                    join activity_facilities af on af.activity_id = ac.id
                    join facilities f on f.id = af.facility_id
                    join budget_classes bc on bc.id = ac.budget_class_id
                    join budget_classes bcc on bcc.id = bc.parent_id
                    join sections s on s.id = ms.section_id
                    left join activity_project_outputs ao on ao.activity_id = ac.id
                    where
                    bcc.code = '200' and m.admin_hierarchy_id = $admin_hierarchy_id 
                    and m.financial_year_id = $financial_year_id
                    and ao.id is null and f.is_active = true
                    and ac.deleted_at is null and ac.budget_type='$budget_type'
                ";

        if(isset($sectionId)){
            $sectionIds = get_section_ids_string($sectionId);
            $query = $query." and s.id in(".$sectionIds.") ";
        }
        $query = $query." group by s.name";
        $result = DB::select($query);
        $total = 0;
        foreach($result as $item){
            $total += $item->not_filled;
        }
        if($total > 0){
            return $result;
        }else {
            return null;
        }
        
    }
}
