<?php
/** get missing expenditure accounts */
namespace App\Models\Execution;

use App\Http\Services\CustomPager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ActivityExpenditure extends Model
{
    protected $table = 'budget_import_missing_accounts';

    public static function annualTargets($financialYearId,$objectiveId,$perPage=null)
    {
        $sql ="select distinct mat.id target_id,mat.description target,mat.code target_code,
                ob.code objective_code,ah.name council,ah.code council_code,
                fy.name financial_year_name
                from mtefs m
                join mtef_annual_targets mat on m.id = mat.mtef_id
                join admin_hierarchies ah on m.admin_hierarchy_id = ah.id
                join financial_years fy on m.financial_year_id = fy.id
                join long_term_targets ltt on mat.long_term_target_id = ltt.id
                join plan_chains pc on ltt.plan_chain_id = pc.id
                join plan_chains ob on ob.id = pc.parent_id
                where fy.id = $financialYearId and ob.id =$objectiveId and mat.deleted_at isnull   order by ah.code
                ";
        $items = DB::select($sql);
        if (!is_null($perPage)) {
            return CustomPager::paginate($items, $perPage);
        } else {
            return DB::select($sql);
        }
    }

    public static function activities($financialYearId,$targetId,$perPage=null)
    {
        $sql = "select distinct ah.name council,ah.code council_code,mat.code target_code,p.code period_code,p.start_date,p.end_date,fy.name financial_year_name,
                a.description activity,a.code activity_code,sum(affsi.unit_price*affsi.frequency*affsi.quantity) amount
                from mtefs m
                join mtef_annual_targets mat on m.id = mat.mtef_id
                join admin_hierarchies ah on m.admin_hierarchy_id = ah.id
                join financial_years fy on m.financial_year_id = fy.id
                join long_term_targets ltt on mat.long_term_target_id = ltt.id
                join plan_chains pc on ltt.plan_chain_id = pc.id
                join plan_chains ob on ob.id = pc.parent_id
                join activities a on mat.id = a.mtef_annual_target_id
                join activity_facilities af on a.id = af.activity_id
                join activity_facility_fund_sources affs on af.id = affs.activity_facility_id
                join activity_facility_fund_source_inputs affsi on affs.id = affsi.activity_facility_fund_source_id
                join activity_periods ap on a.id = ap.activity_id
                join periods p on ap.period_id = p.id
                where fy.id = $financialYearId and mat.id = $targetId and a.deleted_at isnull
                 group by ah.id,mat.id,a.id,p.id,fy.id
                order by ah.code";

        $items = DB::select($sql);
        if (!is_null($perPage)) {
            return CustomPager::paginate($items, $perPage);
        } else {
            return DB::select($sql);
        }
    }
}
