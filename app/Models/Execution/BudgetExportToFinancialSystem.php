<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class BudgetExportToFinancialSystem extends Model
{
    public $timestamps = true;
    //use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'budget_export_to_financial_systems';
    protected $fillable =['budget_export_transaction_item_id','chart_of_accounts','description','is_sent','financial_year_id','created_by','update_by','amount','queue_name','admin_hierarchy_id','revenue_export_transaction_item_id'];

    public function budget_export_transaction_item(){
        return $this->belongsTo('App\Models\Execution\BudgetExportTransactionItem','budget_export_transaction_item_id','id');
    }

    public function financial_year(){
        return $this->belongsTo('App\Models\Setup\FinancialYear','financial_id','id');
    }

    public function system(){
        return $this->belongsTo('App\Models\Setup\System','financial_system_code','code');
    }
}
