<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class FfarsActivity extends Model
{
    protected $table = 'ffars_activities';
}
