<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class AccountBalance extends Model
{
    public $timestamps = true;
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'account_balances';
}
