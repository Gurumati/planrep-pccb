<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class AggregateFacilityBudget extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'aggregate_facility_budgets';

}
