<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use App\Jobs\ConsumeDataFromQueueJob;
use App\Jobs\ReadDataFromQueueJob;
use App\Models\Setup\ConfigurationSetting;
use Illuminate\Support\Facades\Log;

class ListenToRabbitMQ extends Model
{
    public static function getCoaSegmentFeedBack()
    {
        $options = array(
            'time' => 60,
            'empty_queue_timeout' => 5,
            'connection_name' => 'default_connection',
            'exchange_type' =>'headers',
            'exchange' => 'COASegmentsExchange',
            'vhost' => '/',
            // 'headers' => array('Company'=> '000', 'Type' => 'FeedBack', 'x-match'=> 'all')
        );

        //Get the name of the queue name from the configuration settings
        $qn = ConfigurationSetting::where('key','COA_SEGMENT_FEEDBACK_QUEUE')->first();
        $queue_name = isset($qn->value)?$qn->value:null;
        //$queue_name = 'COASegments_Queue_FeedBack';
        $action     = 'getCoaSegmentFeedBack';
        dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
    }

    public static function getGLAccountsQueueFeedBack()
    {
        $options = array(
            'time' => 60,
            'empty_queue_timeout' => 5,
            'exchange_type'=>'headers',
            'exchange' => 'GLAccountsExchange'
        );

        //Get the name of the queue name from the configuration settings
        $qn = ConfigurationSetting::where('key','COA_SEGMENT_FEEDBACK_QUEUE')->first();
        $queue_name = isset($qn->value)?$qn->value:null;

        $queue_name = 'GLAccounts_Queue_FeedBack';
        $action     = 'getGLAccountsQueueFeedBack';
        dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
    }

    public static function getBudgetQueueFeedBack(){
        $options = array(
            'time' => 60,
            'empty_queue_timeout' => 5,
            'connection_name' => 'default_connection',
            'exchange' => 'BudgetExchange',
            'vhost' => '/',
            'headers' => array('Company'=> '000', 'Type' => 'FeedBack', 'x-match'=> 'all')
        );
        $queue_name = 'BUDGET_Queue_FeedBack';
        $action     = 'getBudgetQueueFeedBack';
        dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
    }

    public static function getGLActualSummary(){
        $options = array('exchange' => 'GLActualSummaryExchange');
        $queue_name = ConfigurationSetting::getValueByKey('EPICOR_GL_ACTUAL_SUMMARY_QUEUE_NAME');
        $action     = 'getGLActualSummary';
        dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
    }

    public static function getFacilityExpenditure(){
        $options = array('exchange'=>'ACTUALS_FROM_FFARS_EXCHANGE');
        $queue_name = 'ACTUAL_FROM_FFARS_QUEUE';
        $action     = 'getFacilityExpenditure';
        dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
    }


    public static function getFacilityBudgetFeedBack(){
        $options = array(
            'time' => 60,
            'connection_name' => 'default_connection',
            'exchange_type'=>'headers',
            'exchange' => 'BUDGET_TO_FFARS_EXCHANGE',
            'headers' => array('type' => 'BUDGET', 'x-match'=> 'all')
        );
        $queue_name = 'BUDGET_TO_FFARS_QUEUE_FEEDBACK';
        $action     = 'getFacilityBudgetFeedBack';
        dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
    }

    public static function getFfarsActualExpenditure(){
        $options = array(
            'time' => 60,
            'empty_queue_timeout' => 5,
            'connection_name' => 'default_connection',
            'exchange_type'=>'headers',
            'exchange' => 'EXPENDITURES_FROM_FFARS_EXCHANGE',
            'headers' => array('Type' => 'EXPENDITURE', 'x-match'=> 'all')
        );

        $queue_name = ConfigurationSetting::getValueByKey('FFARS_ACTUAL_EXPENDITURE_QUEUE_NAME');
        $action     = 'getFfarsActualExpenditure';
        dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
    }

    public static function getFfarsActualRevenue(){
        $options = array(
            'time' => 60,
            'empty_queue_timeout' => 5,
            'connection_name' => 'default_connection',
            'exchange_type'=>'headers',
            'exchange' => 'REVENUE_FROM_FFARS_EXCHANGE',
            'headers' => array('Type' => 'REVENUE', 'x-match'=> 'all')
        );

        $queue_name = ConfigurationSetting::getValueByKey('FFARS_ACTUAL_REVENUE_QUEUE_NAME');
        $action     = 'getFfarsActualRevenue';
        dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
    }

    public static function readMuseImplementations(){
       /// $options = array('exchange' => 'MUSE_TO_PLANREP_OTR_DATA_QUEUE_EXCHANGE');
       $options = array(
        'time' => 60,
        'empty_queue_timeout' => 5,
        'exchange_type'=>'headers',
        'exchange' => 'MUSE_TO_PLANREP_OTR_DATA_QUEUE_EXCHANGE'
    );
        $queue_name = 'MUSE_TO_PLANREP_OTR_DATA_QUEUE_2';
        $action     = 'readMuseImplementations';
        dispatch(new ReadDataFromQueueJob($queue_name, $options, $action));
    }

    public static function readMuseFeedback() {
        $options = array(
            'time' => 60,
            'empty_queue_timeout' => 5,
            'connection_name' => 'default_connection',
            'exchange_type'=>'headers',
            'exchange' => 'MUSE_TO_PLANREP_OTR_FEERBACK_EXCHANGE'
        );
        $queue_name = 'MUSE_TO_PLANREP_OTR_FEERBACK';
        $action     = 'readMuseFeedback';
        dispatch(new ReadDataFromQueueJob($queue_name, $options, $action));
    }
}
