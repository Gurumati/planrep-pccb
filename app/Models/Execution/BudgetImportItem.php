<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetImportItem extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'budget_import_items';

    public function budget_export_account(){
        return $this->belongsTo('App\Models\Execution\BudgetExportAccount','budget_export_account_id','id');
    }

    public function period(){
        return $this->belongsTo('App\Models\Setup\Period','period_id','id');
    }

    public function budget_control_account(){
        return $this->belongsTo('App\Models\Execution\BudgetControlAccount','budget_control_account_id','id');
    }

    public function gfs_code(){
        return $this->belongsTo('App\Models\Setup\GfsCode','gfs_code','id');
    }

    public function budget_class(){
        return $this->belongsTo('App\Models\Setup\BudgetClass','budget_class_id','id');
    }

    public function fund_source(){
        return $this->belongsTo('App\Models\Setup\FundSource','fund_source_id','id');
    }
}
