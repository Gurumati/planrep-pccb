<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use App\Models\Setup\AdminHierarchy;
use Illuminate\Support\Facades\DB;
use App\Models\Setup\GfsCode;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\FundSource;
use App\Http\Controllers\Execution\MuseIntegrationController;
use App\Http\Controllers\Execution\SocialSecurityIntegrationController;
use App\Models\Execution\BudgetExportAccountGLAccountExportIssues;

use App\Models\Execution\BudgetExportResponse;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ReadDataFromMuse extends Model
{
    public static function processExpenditureData($data){
        ini_set('max_execution_time', 6000);

         $failedChartOfAccount = array();
         $company = $data["message"]["messageSummary"]["company"];
         $messageType = $data["message"]["messageHeader"]["messageType"];
         $orgMsgId = $data["message"]["messageHeader"]["msgId"];
         $c = AdminHierarchy::where('code', $company)->first();
         $admin_hierarchy_id = isset($c->id) ? $c->id : null;

         if($admin_hierarchy_id > 0){
             $applyDate = $data["message"]["messageSummary"]["applyDate"];
             $je_date = isset($applyDate) ? $applyDate : null;

             //Check the current active period from the periods table. The period_group should not be annual
             $query = "select * from periods where '".$je_date."'::date between start_date and end_date and periods.period_group_id != 8";
             $get_period = DB::select(DB::raw($query));
             $period_id = isset($get_period[0]->id) ? $get_period[0]->id: null;

             //Check the current active period from the periods table. The period_group should not be annual
             $financial_year_query = "select * from financial_years where '".$je_date."'::date between start_date and end_date";
             $get_financial_year = DB::select(DB::raw($financial_year_query));
             $financial_year_id  = isset($get_financial_year[0]->id) ? $get_financial_year[0]->id: null;
             $message = $data["message"]["messageDetails"];
             foreach($message as $expenditureDetail){

                //Get the budget export account id
                 $glAccount = $expenditureDetail["glAccount"];

                 $raw_query = "select e.id
                 from budget_export_accounts e
                 inner join activities a on a.id = e.activity_id
                 where a.budget_type in ('APPROVED','CARRYOVER','SUPPLEMENTARY')
                 and e.chart_of_accounts ='".$glAccount."'";
                 $budget_export_account = DB::select(DB::raw($raw_query));
                 $budget_export_account_id = isset($budget_export_account[0]->id) ? $budget_export_account[0]->id : null;

                 $gl_account = explode('-', $glAccount);
                 $gfs_code_from_muse = $gl_account[13];
                 $gfs_code = GfsCode::where('code','=',$gfs_code_from_muse)->first();
                 $gfs_code_id = isset($gfs_code->id) ? $gfs_code->id : null;

                 $gl_account = explode('-', $glAccount);
                 $budget_class_from_muse = $gl_account[6];
                 $budget_class = BudgetClass::where('code','=',$budget_class_from_muse)->first();
                 $budget_class_id = isset($budget_class->id) ? $budget_class->id : null;

                 $gl_account = explode('-', $glAccount);
                 $fund_source_from_muse = $gl_account[12];
                 $fund_source = FundSource::where('code','=',$fund_source_from_muse)->first();
                 $fund_source_id = isset($fund_source->id) ? $fund_source->id : null;
 
                 $debit_amount = (double)$expenditureDetail["drAmount"];
                 $credit_amount = (double)$expenditureDetail["crAmount"];
                 $coa_for_muse =  str_replace('-', '|',$expenditureDetail["glAccount"]);
                 if($company == 'TR196'){$receiver = 'NAVISION';}else{$receiver = 'MUSE';}

                 if (!$budget_export_account_id){
                     $responseTomuse = array(
                        "uid" => $expenditureDetail["uid"],
                        "additionalInfo" => $coa_for_muse,
                        "glAccount" => $coa_for_muse,
                        "drAmount" => $expenditureDetail["drAmount"],
                        "crAmount" => $expenditureDetail["crAmount"],
                        "status" => "FAIL",
                        "statusCode"=> "fail",
                        "statusDesc" => "GL ACCOUNT NOT FOUND ON PLANREP",
                     );
                     array_push($failedChartOfAccount,$responseTomuse);
                     $missing_account_data = array(
                        'uid' => $expenditureDetail["uid"],
                        'chart_of_accounts' => $expenditureDetail["glAccount"],
                        'financial_year_id' => $financial_year_id,
                        'admin_hierarchy_id' => $admin_hierarchy_id,
                        'debit_amount' => $debit_amount,
                        'credit_amount' => $credit_amount,
                        'JEDate' => $applyDate,
                        'reason' => 'GL_ACCOUNT_NOT_FOUND_IN_PLANREPOTR',
                        'transaction_type' => 'EXPENDITURE',
                        'financial_system_code' => $receiver,
                     );
                     $count = DB::table('budget_import_missing_accounts')->where('chart_of_accounts',$expenditureDetail['glAccount'])->where('uid',$expenditureDetail['uid'])->count();
                        if($count == 0){
                          DB::table('budget_import_missing_accounts')->insert([$missing_account_data]);
                        }
                 }else{
                     //The account has been found.
                    //Insert and get the id of the fund_allocated_from_financial_systems
                    $budget_type_raw_query = "select a.budget_type
                    from budget_export_accounts e
                    inner join activities a on e.activity_id = a.id
                    where e.chart_of_accounts ='".$glAccount."' and a.budget_type in ('CARRYOVER','APPROVED','SUPPLEMENTARY')";
                    $budget_type = DB::select(DB::raw($budget_type_raw_query));
                    $budget_type = isset($budget_type[0]->budget_type) ? $budget_type[0]->budget_type : null;

                    $expenditure_data = array(
                        'budget_export_account_id' => $budget_export_account_id,
                        'period_id' => $period_id,
                        'cancelled' => false,
                        'created_at' => \Carbon\Carbon::now(),
                       // 'BookID' => $message->BookID,
                        'FiscalYear' => date("Y"),
                        'JEDate' => $applyDate,
                        'Account' => $glAccount,
                        'date_imported' => date("Y-m-d"),
                        'gfs_code_id' => $gfs_code_id,
                        'budget_class_id' => $budget_class_id,
                        'fund_source_id' => $fund_source_id,
                        'debit_amount' => $debit_amount,
                        'credit_amount' => $credit_amount,
                        'uid' => $expenditureDetail["uid"],
                        'msg_id' => $orgMsgId,
                        'budget_type' => $budget_type
                    );

                    $count = DB::table('budget_import_items')->where('Account',$glAccount)->where('uid',$expenditureDetail['uid'])->count();
                    if($count == 0){
                        DB::transaction(function() use ($expenditure_data) {
                            DB::table('budget_import_items')->insert([$expenditure_data]);
                        });
                        $status = "SUCCESS";
                        $description = "Data accepted on PlanRep";
                    }else{
                        $status = "FAIL";
                        $description = "Duplicate, Data exist in PlanRep";
                    }

                    $responseTomuse = array(
                        "uid" => $expenditureDetail["uid"],
                        "additionalInfo" => $coa_for_muse,
                        "glAccount" => $coa_for_muse,
                        "drAmount" => $expenditureDetail["drAmount"],
                        "crAmount" => $expenditureDetail["crAmount"],
                        "status" => $status,
                        "statusCode" => strtolower($status),
                        "statusDesc" => $description,
                     );
                     array_push($failedChartOfAccount,$responseTomuse);

                 }
                
            }

            $responseData =  app('App\Http\Controllers\Execution\MuseIntegrationController')->response($orgMsgId,"EXPENDITURE","PROCESSED","Response from Planrep",$failedChartOfAccount);
            try {
                if($company == 'TR196'){
                    $response =  app('App\Http\Controllers\Execution\SocialSecurityIntegrationController')->sendRequestToNavision($responseData);
    
                }else{
                $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($responseData);
                }//code...
            } catch (\Throwable $th) {
                Log::info("Failed to send response");
            }
            

         } else {

            $message = $data["message"]["messageDetails"];
                
                    foreach($message as $expenditureDetail){
                        $glAccount = $expenditureDetail["glAccount"];
                        $uid = $expenditureDetail["uid"];
                        $debit_amount = (double)$expenditureDetail["drAmount"];
                        $credit_amount = (double)$expenditureDetail["crAmount"]; 
                        
                        $responseTomuse = array(
                            "uid" => $uid,
                            "additionalInfo" => $glAccount,
                            "glAccount" => $glAccount,
                            "drAmount" => $debit_amount,
                            "crAmount" => $credit_amount,
                            "status" => "REJECT",
                            "statusCode" => "rejected",
                            "statusDesc" => "Institution Code not found in PlanRep",
                        );
                        array_push($failedChartOfAccount,$responseTomuse);
                        }

           $responseData =  app('App\Http\Controllers\Execution\MuseIntegrationController')->response($orgMsgId,"EXPENDITURE","REJECTED","Institution Code ".$company." Not found on Planrep",$failedChartOfAccount);
            try {
                if($company == 'TR196'){
                    $response =  app('App\Http\Controllers\Execution\SocialSecurityIntegrationController')->sendRequestToNavision($responseData);
    
                }else{
                    $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($responseData);
                }
            } catch (\Throwable $th) {
                Log::info("ERROR");
            }
            
         }
 
    }


    public static function processAllocation($data){
        
         $failedChartOfAccount = array();
         $company = $data["message"]["messageSummary"]["company"];
         $orgMsgId = $data["message"]["messageHeader"]["msgId"];
         $c = AdminHierarchy::where('code', $company)->first();
         $admin_hierarchy_id = isset($c->id) ? $c->id : 1;

        // if($admin_hierarchy_id > 0){
            $applyDate = $data["message"]["messageSummary"]["applyDate"];
            $je_date = isset($applyDate) ? $applyDate : null;

            //Check the current active period from the periods table. The period_group should not be annual
            $query = "select * from periods where '".$je_date."'::date between start_date and end_date and periods.period_group_id != 8";
            $get_period = DB::select(DB::raw($query));
            $period_id = isset($get_period[0]->id) ? $get_period[0]->id: null;

            //Check the current active period from the periods table. The period_group should not be annual
            $financial_year_query = "select * from financial_years where '".$je_date."'::date between start_date and end_date";
            $get_financial_year = DB::select(DB::raw($financial_year_query));
            $financial_year_id  = isset($get_financial_year[0]->id) ? $get_financial_year[0]->id: null;

            if ($je_date && $company){
                $message = $data["message"]["messageDetails"];
                
                foreach($message as $allocationDetail){
                    $glAccount = $allocationDetail["glAccount"];
                    $uid = $allocationDetail["uid"];
                    //Get the budget export account id
                     $raw_query = "select e.id from budget_export_accounts e inner join activities a on a.id = e.activity_id where a.budget_type in ('APPROVED','CARRYOVER','SUPPLEMENTARY') and e.chart_of_accounts ='".$glAccount."'";
                     $budget_export_account = DB::select(DB::raw($raw_query));
                     $budget_export_account_id = isset($budget_export_account[0]->id) ? $budget_export_account[0]->id : null;
                     
                     $debit_amount = (double)$allocationDetail["drAmount"];
                     $credit_amount = (double)$allocationDetail["crAmount"]; 
                     if($company == 'TR196'){$receiver = 'NAVISION';}else{$receiver = 'MUSE';}

                        if (!$budget_export_account_id){
                             /**
                             * Enter an entry into the missing accounts table
                             */

                            $responseTomuse = array(
                                "uid" => $uid,
                                "additionalInfo" => $glAccount,
                                "glAccount" => $glAccount,
                                "drAmount" => $debit_amount,
                                "crAmount" => $credit_amount,
                                "status" => "FAIL",
                                "statusCode"=> "fail",
                                "statusDesc" => "GL ACCOUNT NOT FOUND ON PLANREP",
                             );
                             array_push($failedChartOfAccount,$responseTomuse);

                            $missing_account_data = array(
                                'uid' => $uid,
                                'chart_of_accounts' => $glAccount,
                                'financial_year_id' => $financial_year_id,
                                'admin_hierarchy_id' => $admin_hierarchy_id,
                                'debit_amount' => $debit_amount,
                                'credit_amount' => $credit_amount,
                                'JEDate' => $applyDate,
                               // 'bookID' => $message->BookID,
                                'reason' => 'GL_ACCOUNT_NOT_FOUND_IN_PLANREP',
                                'transaction_type' => 'ALLOCATION',
                                'financial_system_code' => $receiver,
                                //'created_at' => \Carbon\Carbon::now()
                            );
                            $count = DB::table('budget_import_missing_accounts')->where('chart_of_accounts',$glAccount)->where('uid',$uid)->count();
                            if($count == 0){
                                DB::table('budget_import_missing_accounts')->insert([$missing_account_data]);
                            }


                         } else {

                                 //The account has been found.
                                //Insert and get the id of the fund_allocated_from_financial_systems
                                DB::transaction(function () use ($applyDate,$glAccount, $debit_amount, $credit_amount, $budget_export_account_id, $period_id) {
                                    $fund_allocated_from_financial_systems_id = DB::table('fund_allocated_from_financial_systems')
                                    ->insertGetId([
                                        'gl_account' => $glAccount,
                                        'description' => 'Allocation from financial system',
                                        'BookID' => 'AllocAccts',
                                        'JEDate' => $applyDate,
                                        'FiscalYear' => date("Y"),
                                        'debit_amount' => $debit_amount,
                                        'credit_amount' => $credit_amount,
                                        'created_at' =>\Carbon\Carbon::now()
                                    ]);
            
                                    //Insert an entry in the fund allocated transactions table
                                    $fund_allocated_transaction_id = DB::table('fund_allocated_transactions')
                                    ->insertGetId([
                                        'reference_code' => microtime(true) * 1000,
                                        'data_source_id' => null,
                                        'import_method_id' => null,
                                    ]);
            
                                    DB::table('fund_allocated_transaction_items')
                                    ->insertGetId([
                                        'fund_allocated_transaction_id' => $fund_allocated_transaction_id,
                                        'budget_export_account_id' => $budget_export_account_id,
                                        'amount' => null,
                                        'credit_amount' => $credit_amount,
                                        'debit_amount' => $debit_amount,
                                        'allocation_date' => $applyDate, ///to be revied
                                        'fund_allocated_from_financial_system_id' => $fund_allocated_from_financial_systems_id,
                                        'period_id' => $period_id,
                                        'created_at' =>\Carbon\Carbon::now()
                                    ]);
                                }, 2);

                                //Option
                                $responseTomuse = array(
                                    "uid" => $uid,
                                    "additionalInfo" => $glAccount,
                                    "glAccount" => $glAccount,
                                    "drAmount" => $debit_amount,
                                    "crAmount" => $credit_amount,
                                    "status" => "SUCCESS",
                                    "statusCode" => "success",
                                    "statusDesc" => "Data Accepted on PlanRep",
                                 );
                                 array_push($failedChartOfAccount,$responseTomuse);
                        }  
                        $sms = "Response from Planrep OTR";
                        $status = "PROCESSED";                 
                }
            }else {
                $message = $data["message"]["messageDetails"];
                
                    foreach($message as $allocationDetail){
                        $glAccount = $allocationDetail["glAccount"];
                        $uid = $allocationDetail["uid"];
                        $debit_amount = (double)$allocationDetail["drAmount"];
                        $credit_amount = (double)$allocationDetail["crAmount"]; 

                        $responseTomuse = array(
                            "uid" => $uid,
                            "additionalInfo" => $glAccount,
                            "glAccount" => $glAccount,
                            "drAmount" => $debit_amount,
                            "crAmount" => $credit_amount,
                            "status" => "REJECT",
                            "statusCode" => "rejected",
                            "statusDesc" => "Institution Code not found in PlanRep",
                        );
                        array_push($failedChartOfAccount,$responseTomuse);
                        }
                        $sms = "Institution code not found"; 
                        $status = "REJECTED";
            }

        
         $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->response($orgMsgId,"ALLOCATION",$status,$sms,$failedChartOfAccount);
         try {
            if($company == 'TR196'){
                $response =  app('App\Http\Controllers\Execution\SocialSecurityIntegrationController')->sendRequestToNavision($response);

            }else{
                $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($response);
            }
         } catch (\Throwable $th) {
                //throw $th;
         }
    }


    public static function processRevenue($data){

        $failedChartOfAccount = array();
        $company = $data["message"]["messageSummary"]["company"];
        $orgMsgId = $data["message"]["messageHeader"]["msgId"];
        $c = AdminHierarchy::where('code', $company)->first();
        $admin_hierarchy_id = isset($c->id) ? $c->id : null;

        if($admin_hierarchy_id > 0){
           $applyDate = $data["message"]["messageSummary"]["applyDate"];
           $je_date = isset($applyDate) ? $applyDate : null;

           //Check the current active period from the periods table. The period_group should not be annual
           $query = "select * from periods where '".$je_date."'::date between start_date and end_date and periods.period_group_id != 8";
           $get_period = DB::select(DB::raw($query));
           $period_id = isset($get_period[0]->id) ? $get_period[0]->id: null;

           //Check the current active period from the periods table. The period_group should not be annual
           $financial_year_query = "select * from financial_years where '".$je_date."'::date between start_date and end_date";
           $get_financial_year = DB::select(DB::raw($financial_year_query));
           $financial_year_id  = isset($get_financial_year[0]->id) ? $get_financial_year[0]->id: null;

            $message = $data["message"]["messageDetails"];
             foreach($message as $revenueDetail){

               $glAccount =  $revenueDetail["glAccount"];
               $uid =  $revenueDetail["uid"];
               $debit_amount = (double)$revenueDetail["drAmount"];
               $credit_amount = (double)$revenueDetail["crAmount"];

                //Get the budget export account id
                $raw_query = "select e.id, e.admin_hierarchy_id, e.section_id, e.financial_year_id, e.gfs_code_id,
                                e.admin_hierarchy_ceiling_id
                                from revenue_export_accounts e where chart_of_accounts ='".$glAccount."'
                                and e.financial_year_id = $financial_year_id";
                $revenue_export_account    = DB::select(DB::raw($raw_query));
                $revenue_export_account_id = isset($revenue_export_account[0]->id) ? $revenue_export_account[0]->id : null;
                $admin_hierarchy_ceiling_id = isset($revenue_export_account[0]->admin_hierarchy_ceiling_id) ?
                                                $revenue_export_account[0]->admin_hierarchy_ceiling_id : null;

                $section_id = isset($revenue_export_account[0]->section_id) ? $revenue_export_account[0]->section_id : null;
                
                $gl_account = explode('-', $glAccount);
                $gfs_code_from_muse = $gl_account[13];
                $gfs_code   = GfsCode::where('code','=',$gfs_code_from_muse)->first();
                $gfs_code_id = isset($gfs_code->id) ? $gfs_code->id : null;

                $gl_account = explode('-', $glAccount);
                $budget_class_from_muse = $gl_account[6];
                $budget_class = BudgetClass::where('code','=',$budget_class_from_muse)->first();
                $budget_class_id = isset($budget_class->id) ? $budget_class->id : null;

                $gl_account = explode('-', $glAccount);
                $fund_source_from_muse = $gl_account[12];
                $fund_source = FundSource::where('code','=',$fund_source_from_muse)->first();
                $fund_source_id = isset($fund_source->id) ? $fund_source->id : null;
                if($company == 'TR196'){$receiver = 'NAVISION';}else{$receiver = 'MUSE';}

                if (!$revenue_export_account_id){
                    /**
                     * Enter an entry into the missing accounts table
                     */
                    $responseTomuse = array(
                                "uid" => $uid,
                                "additionalInfo" => $glAccount,
                                "glAccount" => $glAccount,
                                "drAmount" => $debit_amount,
                                "crAmount" => $credit_amount,
                                "status" => "FAIL",
                                "statusCode"=> "fail",
                                "statusDesc" => "GL ACCOUNT NOT FOUND ON PLANREP",
                    );
                    array_push($failedChartOfAccount,$responseTomuse);

                    $missing_account_data = array(
                        'uid' => $uid,
                        'msg_id' => $orgMsgId,
                        'chart_of_accounts' => $glAccount,
                        'financial_year_id' => $financial_year_id,
                        'admin_hierarchy_id' => $admin_hierarchy_id,
                        'debit_amount' =>  $debit_amount,
                        'credit_amount' => $credit_amount,
                        'JEDate' => $je_date,
                        //'bookID' => $message->BookID,
                        'reason' => 'GL_ACCOUNT_NOT_FOUND_IN_PLANREP',
                        'transaction_type' => 'REVENUE',
                        'financial_system_code' => $receiver,
                        'created_at' => \Carbon\Carbon::now()
                    );
                    $count = DB::table('received_fund_missing_accounts')->where('chart_of_accounts',$glAccount)->where('uid',$uid)->count();
                    if($count == 0){
                        DB::table('received_fund_missing_accounts')->insert([$missing_account_data]);
                    }
                } else {
                     //The account has been found.
                      $revenue_data = array(
                        'admin_hierarchy_ceiling_id' => $admin_hierarchy_ceiling_id,
                        'date_received' => $je_date,
                        'reference_code' =>microtime(true) * 1000,
                        'period_id' => $period_id,
                        'fund_source_id' => $fund_source_id,
                        'gfs_code_id' => $gfs_code_id,
                        'revenue_export_account_id' => $revenue_export_account_id,
                        'debit_amount' => $debit_amount,
                        'credit_amount' => $credit_amount,
                        'fiscal_year' => date("Y"),
                        'account' => $glAccount,
                        'JEDate' => $je_date,
                        'uid' => $uid,
                        'msg_id' => $orgMsgId
                    );

                     $count = DB::table('received_fund_items')->where('account',$glAccount)->where('uid',$uid)->count();
                    if($count == 0){
                        DB::table('received_fund_items')->insert([$revenue_data]);
                        $status = "SUCCESS";
                        $description = "Data accepted on PlanRep";
                    }else{
                        $status = "FAIL";
                        $description = "Duplicate, Data exist in PlanRep";
                    }

                    $responseTomuse = array(
                                "uid" => $uid,
                                "additionalInfo" => $glAccount,
                                "glAccount" => $glAccount,
                                "drAmount" => $debit_amount,
                                "crAmount" => $credit_amount,
                                "status" => $status,
                                "statusCode" => strtolower($status),
                                "statusDesc" => $description,
                    );
                    array_push($failedChartOfAccount,$responseTomuse);
                }
            
             }
             

          //Send response to Muse
          $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->response($orgMsgId,"REVENUE","PROCESSED","Data proccessed on Planrep",$failedChartOfAccount);
            try {
                if($company == 'TR196'){
                    $response =  app('App\Http\Controllers\Execution\SocialSecurityIntegrationController')->sendRequestToNavision($response);
    
                }else{
                    $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($response);
                }
            } catch (\Throwable $th) {
                //throw $th;
            }

        } else {

            $message = $data["message"]["messageDetails"];
                
            foreach($message as $revenueDetail){
                $glAccount = $revenueDetail["glAccount"];
                $uid = $revenueDetail["uid"];
                $debit_amount = (double)$revenueDetail["drAmount"];
                $credit_amount = (double)$revenueDetail["crAmount"]; 
                
                $responseTomuse = array(
                    "uid" => $uid,
                    "additionalInfo" => $glAccount,
                    "glAccount" => $glAccount,
                    "drAmount" => $debit_amount,
                    "crAmount" => $credit_amount,
                    "status" => "REJECT",
                    "statusCode" => "rejected",
                    "statusDesc" => "Institution Code not found in PlanRep",
                );
                array_push($failedChartOfAccount,$responseTomuse);
                }

            // Error institution code not found
            $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->response($orgMsgId,"REVENUE","REJECTED","Institution code not found",$failedChartOfAccount);
            try {
                if($company == 'TR196'){
                    $response =  app('App\Http\Controllers\Execution\SocialSecurityIntegrationController')->sendRequestToNavision($response);
    
                }else{
                    $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($response);
                }
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
    }


    public static function getBudgetFeedbck($data){

        $orgMsgId = $data["message"]["messageHeader"]["msgId"];
        $messageType = $data["message"]["messageHeader"]["messageType"];
        $status = $data["message"]["messageSummary"]["responseStatus"];
        $trnumber = $data["message"]["messageSummary"]["company"];
        $trnumber == 'TR196'?$receiver = 'NAVISION':$receiver = 'MUSE';

       if(isset($data["digitalSignature"]) && isset($data["message"]["messageSummary"]) && isset($data["message"]["messageHeader"]) && isset($data["message"]["messageDetails"])){
        $feedbackDetails = $data["message"]["messageDetails"];

        if($status == "PROCESSED" || $status == "RECEIVED"){
           
            foreach($feedbackDetails as $detail){
                  $uid = $detail["uid"];
                  $statusDesc = $detail["statusDesc"];
                  $statusCode = $detail["statusCode"];

                  if($statusCode == 'success'){

                    DB::table('budget_export_accounts as bea')
                      ->join('budget_export_transaction_items as beti','bea.id','beti.budget_export_account_id')
                      ->join('budget_export_to_financial_systems as betfs','beti.id','betfs.budget_export_transaction_item_id')
                      ->where('betfs.id',$uid)
                      ->update(['is_delivered' => true]);
  
                    DB::table('budget_export_responses as ber')
                      ->join('budget_export_to_financial_systems as betfs','ber.budget_export_to_financial_system_id','betfs.id')
                      ->where('betfs.id',$uid)
                      ->update(['is_delivered' => true,'response'=>$statusDesc.' and processed successfully']);

                  }elseif($statusCode == 'failed'){

                    DB::table('budget_export_accounts as bea')
                      ->join('budget_export_transaction_items as beti','bea.id','beti.budget_export_account_id')
                      ->join('budget_export_to_financial_systems as betfs','beti.id','betfs.budget_export_transaction_item_id')
                      ->where('betfs.id',$uid)
                      ->update(['is_delivered' => false]);
  
                    DB::table('budget_export_responses as ber')
                      ->join('budget_export_to_financial_systems as betfs','ber.budget_export_to_financial_system_id','betfs.id')
                      ->where('betfs.id',$uid)
                      ->update(['is_delivered' => false,'response'=>$statusDesc]);

                  }
                  
  
            }
         }elseif ($status == "REJECTED") {
  
          foreach($feedbackDetails as $detail){
          $uid = $detail["uid"];
          $statusCode = $detail["statusCode"];
          $statusDesc = $detail["statusDesc"];
          $additionalInfo = $detail["additionalInfo"];
  
          $count = DB::table('budget_export_to_financial_systems')
          ->where('id',$uid)->get();
  
          if($count->count() > 0){
              $ber = BudgetExportResponse::where('budget_export_to_financial_system_id', $uid)->first();
                  $response_exists = isset($ber->id) ? $ber->id : null;
                  if ($response_exists == null) {
                      DB::table('budget_export_responses')
                      ->insert([
                          'budget_export_to_financial_system_id' => $uid,
                          'is_sent' => true,
                          'is_delivered' => false,
                          'response' => $statusDesc,
                          'financial_system_code'=> $receiver,
                          'created_at' =>\Carbon\Carbon::now()
                      ]);
                  }else{
  
                      DB::table('budget_export_responses')
                      ->where('id', $response_exists)
                      ->update([
                          'is_sent' => true,
                          'is_delivered' => false,
                          'response' => $statusDesc,
                          'financial_system_code'=>$receiver,
                          'updated_at' =>\Carbon\Carbon::now()
                      ]);
  
                  }  
          } 
        
         }
  
          }
       }else{
        return "Payload format is incorrect not as expected";
       }
       return  app('App\Http\Controllers\Execution\MuseIntegrationController')->acknowledgement($orgMsgId,$messageType);
       
    }

    public static function getRevenueProjectionFeedbabck($data){

       $orgMsgId = $data["message"]["messageHeader"]["msgId"];
       $messageType = $data["message"]["messageHeader"]["messageType"];
       $status = $data["message"]["messageSummary"]["responseStatus"];
       $trnumber = $data["message"]["messageSummary"]["company"];
       $trnumber == 'TR196'?$receiver = 'NAVISION':$receiver = 'MUSE';

        if($status == "PROCESSED" || $status == "RECEIVED"){
           
            $feedbackDetails = $data["message"]["messageDetails"];
            foreach($feedbackDetails as $detail){
                  $uid = $detail["uid"];
                  
                  DB::table('revenue_export_accounts as rea')
                      ->join('revenue_export_transaction_items as reti','rea.id','reti.revenue_export_account_id')
                      ->join('budget_export_to_financial_systems as betfs','reti.id','betfs.revenue_export_transaction_item_id')
                      ->where('betfs.id',$uid)
                      ->update(['is_delivered' => true]);
  
            }
       
        }elseif ($status == "REJECTED") {

            $feedbackDetails = $data["message"]["messageDetails"];

            foreach($feedbackDetails as $detail){
            $uid = $detail["uid"];
            $statusCode = $detail["statusCode"];
            $statusDesc = $detail["statusDesc"];
            $additionalInfo = $detail["additionalInfo"];

            $count = DB::table('budget_export_to_financial_systems')
            ->where('id',$uid)->get();

            if($count->count() > 0 && $statusCode =='Failed'){
                $ber = BudgetExportResponse::where('budget_export_to_financial_system_id', $uid)->first();
                    $response_exists = isset($ber->id) ? $ber->id : null;
                    if ($response_exists == null) {
                        DB::table('budget_export_responses')
                        ->insert([
                            'budget_export_to_financial_system_id' => $uid,
                            'is_sent' => true,
                            'is_delivered' => false,
                            'response' => $statusDesc,
                            'financial_system_code'=>$receiver,
                            'created_at' =>\Carbon\Carbon::now()
                        ]);
                    }else{

                        DB::table('budget_export_responses')
                            ->where('id', $response_exists)
                            ->update([
                                'is_sent' => true,
                                'is_delivered' => false,
                                'response' => $statusDesc,
                                'financial_system_code'=>$receiver,
                                'updated_at' =>\Carbon\Carbon::now()
                        ]);

                    }  
            } 
          
           }
        }
        return  app('App\Http\Controllers\Execution\MuseIntegrationController')->acknowledgement($orgMsgId,$messageType);
    }

    public static function getActivityFeedbabck($data){
        $status = $data["message"]["messageSummary"]["responseStatus"];
        if($status == "PROCESSED" || $status == "SUCCESS" || $status == "RECEIVED"){
            $orgMsgId = $data["message"]["messageSummary"]["orgMsgId"];
            $company = $data["message"]["messageSummary"]["company"];
            $feedbackDetails = $data["message"]["messageDetails"];
            foreach($feedbackDetails as $detail){
                $uid = $detail["uid"];
                $statusCode = $detail["statusCode"];
                $statusDesc = $detail["statusDesc"];
                $additionalInfo = $detail["additionalInfo"];

                $ic = DB::table("activities")->where("id",$uid)->first();
                $activityId = isset($ic->id) ? $ic->id : null;
                if($activityId > 0 && $statusCode ='success'){
                    DB::table("activities")->where("id",$uid)->ipdate([
                        "exported_to_muse" => true,
                        "delivered_to_muse" => true,
                        "muse_response " => $statusDesc,
                        "updated_at" =>\Carbon\Carbon::now()
                    ]);
                }
            }
        }

    }

    public static function getBalanceRequestFeedback($data){

           $status = $data["message"]["messageSummary"]["responseStatus"];
           $orgMsgId = $data["message"]["messageSummary"]["orgMsgId"];
           $feedbackDetails = $data["message"]["messageDetails"];
           foreach($feedbackDetails as $detail){
                 $uid = $detail["uid"];
                 $statusCode = $detail["statusCode"];
                 $statusDesc = $detail["statusDesc"];
 
                 $count = DB::table('budget_export_to_financial_systems')
                 ->where('id',$uid)->get();
 
               if($count->count() > 0 && $statusCode =='success'){
                    $ber = BudgetExportResponse::where('budget_export_to_financial_system_id', $uid)->first();
                     $response_exists = isset($ber->id) ? $ber->id : null;
                      if ($response_exists == null) {
                          DB::table('budget_export_responses')
                         ->insert([
                             'budget_export_to_financial_system_id' => $uid,
                             'is_sent' => true,
                             'is_delivered' => true,
                             'response' => $statusDesc,
                             'financial_system_code'=>'MUSE',
                             'created_at' =>\Carbon\Carbon::now()
                         ]);
                      }else{
 
                         DB::table('budget_export_responses')
                             ->where('id', $response_exists)
                             ->update([
                                 'is_sent' => true,
                                 'is_delivered' => true,
                                 'response' => $statusDesc,
                                  'financial_system_code'=>'MUSE',
                                 'updated_at' =>\Carbon\Carbon::now()
                         ]);
 
                      }  
               } 
           }


    }
    
    public static function getReallocationFeedbabck($data){
        $status = $data["message"]["messageSummary"]["responseStatus"];
        if($status == "PROCESSED" || $status == "SUCCESS" || $status == "RECEIVED"){
           $orgMsgId = $data["message"]["messageSummary"]["orgMsgId"];
           $feedbackDetails = $data["message"]["messageDetails"];
           foreach($feedbackDetails as $detail){
                 $uid = $detail["uid"];
                 $statusCode = $detail["statusCode"];
                 $statusDesc = $detail["statusDesc"];
 
                 $count = DB::table('budget_export_to_financial_systems')
                 ->where('id',$uid)->get();
 
               if($count->count() > 0 && $statusCode =='success'){
                    $ber = BudgetExportResponse::where('budget_export_to_financial_system_id', $uid)->first();
                     $response_exists = isset($ber->id) ? $ber->id : null;
                      if ($response_exists == null) {
                          DB::table('budget_export_responses')
                         ->insert([
                             'budget_export_to_financial_system_id' => $uid,
                             'is_sent' => true,
                             'is_delivered' => true,
                             'response' => $statusDesc,
                             'financial_system_code'=>'MUSE',
                             'created_at' =>\Carbon\Carbon::now()
                         ]);
                      }else{
 
                         DB::table('budget_export_responses')
                             ->where('id', $response_exists)
                             ->update([
                                 'is_sent' => true,
                                 'is_delivered' => true,
                                 'response' => $statusDesc,
                                  'financial_system_code'=>'MUSE',
                                 'updated_at' =>\Carbon\Carbon::now()
                         ]);
 
                      }  
               } 
           }
        }
    }

    public static function getProjectFeedbabck($data){
        $status = $data["message"]["messageSummary"]["responseStatus"];
        if($status == "PROCESSED" || $status == "SUCCESS" || $status == "RECEIVED"){
            $orgMsgId = $data["message"]["messageSummary"]["orgMsgId"];
            $company = $data["message"]["messageSummary"]["company"];
            $feedbackDetails = $data["message"]["messageDetails"];
            foreach($feedbackDetails as $detail){
                $uid = $detail["uid"];
                $statusCode = $detail["statusCode"];
                $statusDesc = $detail["statusDesc"];
                $additionalInfo = $detail["additionalInfo"];
                $status = $statusDesc=='success'?true:false;
                DB::table('projects')->where('id',$uid)->update([
                    'is_delivered_to_muse' =>$status,
                    'muse_response' => $statusDesc,
                    'exported_to_muse' =>true,
                    'updated_at' =>\Carbon\Carbon::now()
                ]);
            }
        }
    }


    public static function getFacilitybabck($data){
         $status = $data["message"]["messageSummary"]["responseStatus"];
        if($status == "PROCESSED" || $status == "SUCCESS" || $status == "RECEIVED"){
            $orgMsgId = $data["message"]["messageSummary"]["orgMsgId"];
            $company = $data["message"]["messageSummary"]["company"];
            $feedbackDetails = $data["message"]["messageDetails"];
            foreach($feedbackDetails as $detail){
                $uid = $detail["uid"];
                $statusCode = $detail["statusCode"];
                $statusDesc = $detail["statusDesc"];
                $additionalInfo = $detail["additionalInfo"];
                $status = $statusDesc=='success'?true:false;
                DB::table('facilities')->where('id',$uid)->update([
                    'is_delivered_to_muse' =>$status,
                    'muse_response' => $statusDesc,
                    'exported_to_muse' =>true,
                    'updated_at' =>\Carbon\Carbon::now()
                ]);
            }

        }

    }


    public static function updateGLAccount($gl_account_id, $status, $company, $type){
        if($type == 'revenue'){
            DB::table('company_gl_accounts')
                ->where('revenue_export_account_id', $gl_account_id)
                ->where('company',$company)
                ->update([
                    'is_delivered' => $status,
                    'updated_at' =>\Carbon\Carbon::now()
                    ]);
        }else {
            DB::table('company_gl_accounts')
                ->where('budget_export_account_id', $gl_account_id)
                ->where('company',$company)
                ->update([
                    'is_delivered' => $status,
                    'updated_at' =>\Carbon\Carbon::now()
                    ]);
        }

    }



}
