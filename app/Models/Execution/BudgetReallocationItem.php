<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetReallocationItem extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'budget_reallocation_items';
    protected $fillable = ['from_chart_of_accounts',
                            'to_chart_of_accounts',
                            'amount',
                            'budget_export_account_id',
                            'budget_reallocation_id',
                            'budget_export_to_account_id',
                            'is_approved',
                            'is_rejected',
                            'comments',
                            'created_by',
                            'updated_by',
                            'deleted_by',
                            'created_at',
                            'updated_at',
                            'deleted_at'
                        ];

    public function budget_export_account(){
        return $this->belongsTo('App\Models\Execution\BudgetExportAccount','budget_export_account_id','id');
    }

    public function budget_reallocation(){
        return $this->belongsTo('App\Models\Execution\BudgetReallocation','budget_reallocation_id','id');
    }

    public function to_export_account(){
        return $this->belongsTo('App\Models\Execution\BudgetExportAccount','budget_export_to_account_id','id');
    }

}
