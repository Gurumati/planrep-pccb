<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetReallocationResponse extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'budget_reallocation_responses';
    protected $fillable = ['budget_reallocation_item_id','is_approved','response','created_by'];
}
