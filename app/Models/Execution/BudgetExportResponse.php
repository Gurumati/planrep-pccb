<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BudgetExportResponse extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'budget_export_responses';

    public function budget_export_to_financial_system(){
        return $this->belongsTo('App\Models\Execution\BudgetExportToFinancialSystem','budget_export_to_financial_system_id','id');
    }

}
