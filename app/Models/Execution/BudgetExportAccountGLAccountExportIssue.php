<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class BudgetExportAccountGLAccountExportIssue extends Model
{
    public $table = 'budget_export_account_glaccount_export_issues';

    public function budget_export_accounts(){
        return $this->belongsTo('App\Models\Execution\BudgetExportAccount', 'budget_export_account_id');
    }
}
