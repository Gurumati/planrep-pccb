<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class CoaSegment extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'coa_segments';

    public function segmentCompany (){
        return $this->hasMany('App\Models\Execution\CoaSegmentCompany');
    }

}
