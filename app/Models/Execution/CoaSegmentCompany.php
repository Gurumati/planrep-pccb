<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class CoaSegmentCompany extends Model
{
    public $table = 'coa_segment_company';

    public function segments(){
        return $this->belongsTo('App\Models\Execution\CoaSegmentCompany');
    }

    public function coa_segment(){
        return $this->belongsTo('App\Models\Execution\CoaSegmentCompany','coa_segment_id','id');
    }
}
