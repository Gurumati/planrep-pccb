<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;

class ReceivedFundsMissingAccount extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'received_fund_missing_accounts';
}
