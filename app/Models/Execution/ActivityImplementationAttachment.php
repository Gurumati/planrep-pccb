<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityImplementationAttachment extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'activity_implementation_attachments';

    public function activity_implementation(){
        return $this->belongsTo('App\Models\Execution\ActivityImplementation','implementation_id','id');
    }
}
