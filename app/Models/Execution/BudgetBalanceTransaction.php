<?php

namespace App\Models\Execution;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Input;


class BudgetBalanceTransaction extends Model
{
    public $timestamps = false;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'budget_balance_transactions';
    protected $fillable = [
    'chart_of_accounts',
    'budget',
    'balance',
    'request_time',
    'response_time',
    'response_message',
    'transaction_id',
    'financial_year_id'
   ];


    public function financial_year(){
        return $this->belongsTo('App\Models\Setup\FinancialYear');
    }


}
