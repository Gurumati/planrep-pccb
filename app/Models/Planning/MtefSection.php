<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class MtefSection extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $table = "mtef_sections";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

   public function mtef(){
      return $this->belongsTo('App\Models\Planning\Mtef','mtef_id');
   }
   public function section(){
      return $this->belongsTo('App\DTOs\SetupDTOs\SectionDTO', 'section_id');
   }
   public function activities(){
      return $this->hasMany('App\Models\Planning\Activity')->
                    select('activity_category_id','activity_status_id','activity_task_nature_id','budget_class_id','code','description','generic_activity_id','project_id','plan_type',
                           'id','intervention_id','is_facility_account','locked','mtef_annual_target_id','indicator','indicator_value','mtef_section_id','sector_problem_id','responsible_person_id');
   }
   public function section_level(){
      return $this->belongsTo('App\Models\Setup\SectionLevel');
   }

   public function decision_level(){
      return $this->belongsTo('App\Models\Setup\DecisionLevel');
   }


    public function mtefSectionComments(){
        return $this->hasMany('App\Models\Planning\MtefSectionComment')
                    ->where('mtef_section_comments.addressed', false)
                    ->where('mtef_section_comments.direction', -1);
    }

    public static function filter($budgetType,$financialYearId,$adminHierarchyId,$sectionIds){
       $searchQuery = Input::get('searchQuery');

        $searchQuery = isset($searchQuery)?"'%".strtolower($searchQuery)."%'":"'%'";

        return MtefSection::with('section', 'decision_level','decision_level.next_decisions', 'mtef', 'mtef.admin_hierarchy', 'mtef.financial_year')
                        ->whereHas('mtef', function ($query) use ($adminHierarchyId, $financialYearId) {
                            $query->where('admin_hierarchy_id', $adminHierarchyId)->where('financial_year_id', $financialYearId);
                        })->whereIn('section_id', $sectionIds)
                        ->whereHas('section',function ($query) use($searchQuery){
                            $query->whereRaw('LOWER(name) like ' . $searchQuery);
                        })
                        ->get();
    }

}
