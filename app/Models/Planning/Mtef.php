<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Flatten;
use Illuminate\Support\Facades\Log;


class Mtef extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $table = "mtefs";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function pe_budgets() {
        return $this->hasMany('App\Models\Budgeting\PeBudget');
    }

    public function financial_year() {
        return $this->belongsTo('App\Models\Setup\FinancialYear');
    }

    public function admin_hierarchy() {
        return $this->belongsTo('App\DTOs\SetupDTOs\AdminHierarchyDTO');
    }
    public function admin_hierarchy_parent() {
            return $this->belongsTo('App\Models\Setup\AdminHierarchyParent','admin_hierarchy_id');
        }

    public function decision_level() {
        return $this->belongsTo('App\Models\Setup\DecisionLevel');
    }


    public function mtef_sections() {
        return $this->hasMany('App\Models\Planning\MtefSection');
    }

    public function assessment_results() {
        return $this->hasMany('App\Models\Assessment\CasAssessmentResult');
    }

    public function mtef_sector_problems() {
        return $this->belongsTo('App\Models\Planning\MtefSectorProblem');
    }

    /** return budget */
    public  static function getBudget($budgetType, $admin_hierarchy_id, $financialYearId){
        
        $peBudgetClassIdString = Flatten::getPeBudgetClassIdString();
        $bType = ($budgetType == 'CURRENT')? "('CURRENT','APPROVED')":"('".$budgetType."')";

        if($budgetType == 'SUPPLEMENTARY'){
        $query = "SELECT 
                  COALESCE (SUM(i.unit_price * i.quantity * i.frequency),0) AS budget
                 FROM  activity_facility_fund_source_inputs AS i
                INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
                INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
                INNER JOIN activities AS a ON a.id = af.activity_id
                INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
                INNER JOIN mtefs AS m ON m.id = ms.mtef_id
                INNER JOIN facilities AS f ON f.id = af.facility_id
                WHERE i.budget_type = 'SUPPLEMENTARY'
                AND f.is_active = true 
                AND m.admin_hierarchy_id =". $admin_hierarchy_id ."
                AND m.financial_year_id = ".$financialYearId." 
                AND a.budget_class_id NOT IN (" . $peBudgetClassIdString . ")
                AND i.deleted_at is null ";
            }else{
                $query = "SELECT 
                COALESCE (SUM(i.unit_price * i.quantity * i.frequency),0) AS budget
               FROM  activity_facility_fund_source_inputs AS i
              INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
              INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
              INNER JOIN activities AS a ON a.id = af.activity_id
              INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
              INNER JOIN mtefs AS m ON m.id = ms.mtef_id
              INNER JOIN facilities AS f ON f.id = af.facility_id
              WHERE i.budget_type in ".$bType."
              AND f.is_active = true 
              AND m.admin_hierarchy_id =". $admin_hierarchy_id ."
              AND m.financial_year_id = ".$financialYearId." 
              AND a.budget_class_id NOT IN (" . $peBudgetClassIdString . ")
              AND i.deleted_at is null ";
            }
        return DB::select($query)[0]->budget;        
    }

    /** return PE budget */
    public  static function getPEBudget($budgetType, $admin_hierarchy_id, $financialYearId){

        $peBudgetClassIdString = Flatten::getPeBudgetClassIdString();
        $bType = ($budgetType == 'CURRENT')? "('CURRENT','APPROVED')":"('".$budgetType."')";

        $query = "SELECT 
                  COALESCE (SUM(i.unit_price * i.quantity * i.frequency),0) AS budget
                 FROM  activity_facility_fund_source_inputs AS i
                INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
                INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
                INNER JOIN activities AS a ON a.id = af.activity_id
                INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
                INNER JOIN mtefs AS m ON m.id = ms.mtef_id
                INNER JOIN facilities AS f ON f.id = af.facility_id
                INNER JOIN sections AS s ON s.id = ms.section_id
                INNER JOIN budget_classes AS bc ON bc.id = a.budget_class_id
                INNER JOIN fund_sources fs on aff.fund_source_id = fs.id
                WHERE a.budget_type in ".$bType."
                AND f.is_active = true 
                AND m.admin_hierarchy_id =". $admin_hierarchy_id ."
                AND m.financial_year_id = ".$financialYearId." 
                AND a.budget_class_id IN (" . $peBudgetClassIdString . ")
                AND i.deleted_at is null 
                AND i.id IN (select DISTINCT activity_facility_fund_source_input_id  FROM activity_facility_fund_source_input_breakdowns  affsib
                        join budget_submission_definitions bsd on affsib.budget_submission_definition_id = bsd.id
                         where ((fs.code = '10A' AND bsd.id = bsd.id ) OR (fs.code = '0GT' AND bsd.field_name IN ('Basic Salary','Annual Increment','Promotion')) ))
                ";
        return DB::select($query)[0]->budget;        
    }
    /** return budget */
    public static function getCeiling($budgetType, $admin_hierarchy_id, $financialYearId){
       
        $peBudgetClassIdString = Flatten::getPeBudgetClassIdString();
        $bType = ($budgetType == 'CURRENT')? "('CURRENT','APPROVED')":"('".$budgetType."')";
        $query = "SELECT
                    COALESCE (SUM(adc.amount),0) as ceiling 
                    FROM admin_hierarchy_ceilings AS adc
                    INNER JOIN ceilings AS c ON c.id = adc.ceiling_id
                    INNER JOIN admin_hierarchies AS a ON a.id = adc.admin_hierarchy_id
                    INNER JOIN gfs_codes as gfs ON gfs.id=c.gfs_code_id
                    INNER JOIN gfscode_fundsources gfcf ON gfcf.gfs_code_id = gfs.id
                    INNER JOIN fund_sources as f on f.id = gfcf.fund_source_id
                    INNER JOIN facilities as fa on fa.id = adc.facility_id
                    WHERE adc.budget_type in ".$bType."
                    AND f.can_project=false 
                    AND fa.is_active = true
                    AND adc.admin_hierarchy_id =".$admin_hierarchy_id."  
                    AND adc.financial_year_id = ".$financialYearId."
                    AND adc.facility_id IS NOT NULL
                    AND c.budget_class_id NOT IN (" . $peBudgetClassIdString . ")
                    AND c.is_active=true  ";

        $ownquery = "SELECT
                    COALESCE (SUM(adc.amount),0) as ceiling 
                    FROM admin_hierarchy_ceilings AS adc
                    INNER JOIN ceilings AS c ON c.id = adc.ceiling_id
                    INNER JOIN admin_hierarchies AS a ON a.id = adc.admin_hierarchy_id
                    INNER JOIN facilities as fa on fa.id = adc.facility_id
                    WHERE adc.budget_type in ".$bType." 
                    AND c.is_aggregate = true 
                    AND fa.is_active = true
                    AND adc.admin_hierarchy_id =".$admin_hierarchy_id."
                    AND  adc.financial_year_id = ".$financialYearId."
                    AND adc.facility_id IS NOT NULL
                    AND c.budget_class_id NOT IN (" . $peBudgetClassIdString . ")
                    AND c.is_active=true ";

        return (DB::select($query)[0]->ceiling + DB::select($ownquery)[0]->ceiling) ;
    }

    /** return budget */
    public static function getPECeiling($budgetType, $admin_hierarchy_id, $financialYearId){
       
        $peBudgetClassIdString = Flatten::getPeBudgetClassIdString();
        $bType = ($budgetType == 'CURRENT')? "('CURRENT','APPROVED')":"('".$budgetType."')";

        // $query = "SELECT
        //             COALESCE (SUM(adc.amount),0) as ceiling 
        //             FROM admin_hierarchy_ceilings AS adc
        //             INNER JOIN ceilings AS c ON c.id = adc.ceiling_id
        //             INNER JOIN admin_hierarchies AS a ON a.id = adc.admin_hierarchy_id
        //             INNER JOIN gfs_codes as gfs ON gfs.id=c.gfs_code_id
        //             INNER JOIN gfscode_fundsources gfcf ON gfcf.gfs_code_id = gfs.id
        //             INNER JOIN fund_sources as f on f.id = gfcf.fund_source_id
        //             INNER JOIN facilities as fa on fa.id = adc.facility_id
        //             WHERE adc.budget_type in ".$bType."
        //             AND f.can_project=false 
        //             AND fa.is_active = true
        //             AND adc.admin_hierarchy_id =".$admin_hierarchy_id."  
        //             AND adc.financial_year_id = ".$financialYearId."
        //             AND adc.facility_id IS NOT NULL
        //             AND c.budget_class_id IN (" . $peBudgetClassIdString . ")
        //             AND c.is_active=true  ";
        $query = "select (subventionPE + ownsourcePE) ceiling from (select
                    id,name,
                    SUM(subventionPE) subventionPE
                From (
                        select ah.id,ah.name,
                        case when bc.code in ('101') then   sum(amount) else 0 end subventionPE
                
                        from
                            admin_hierarchies ah
                                INNER JOIN admin_hierarchy_levels ahl on ah.admin_hierarchy_level_id = ahl.id
                                LEFT JOIN admin_hierarchy_ceilings ahc on ah.id = ahc.admin_hierarchy_id
                                LEFT JOIN  ceilings c on ahc.ceiling_id = c.id
                                INNER JOIN  budget_classes bc on c.budget_class_id = bc.id
                                INNER JOIN gfs_codes gfs ON gfs.id = c.gfs_code_id
                                INNER JOIN gfscode_fundsources gfcf ON gfcf.gfs_code_id = gfs.id
                                INNER JOIN fund_sources fs on gfcf.fund_source_id = fs.id
                                INNER JOIN  fund_source_categories fsc on fs.fund_source_category_id = fsc.id
                                INNER JOIN  financial_years fy on ahc.financial_year_id = fy.id
                                INNER JOIN sections s on ahc.section_id = s.id
                                INNER JOIN section_levels sl on s.section_level_id = sl.id
                        where ahl.hierarchy_position = 2 and amount > 0  and ahc.financial_year_id = ".$financialYearId." and ahc.budget_type in ".$bType."
                        and sl.hierarchy_position = 1 and ahc.deleted_at is null and s.deleted_at is null and ah.id = ".$admin_hierarchy_id."
                        group by ah.id,fy.name,bc.code,fsc.code
                    ) income
                group by id,name)sub
                LEFT JOIN(
                    select id,
                        name,
                        SUM(ownsourcePE) ownsourcePE
                    From (
                            select ah.id,
                                    ah.name,
                                    case when bc.code = '104' then SUM(amount) else 0 end ownsourcePE
                
                            from admin_hierarchies ah
                                    INNER JOIN admin_hierarchy_levels ahl on ah.admin_hierarchy_level_id = ahl.id
                                    LEFT JOIN admin_hierarchy_ceilings ahc on ah.id = ahc.admin_hierarchy_id
                                    LEFT JOIN ceilings c on ahc.ceiling_id = c.id
                                    INNER JOIN budget_classes bc on c.budget_class_id = bc.id
                                    INNER JOIN fund_source_budget_classes fsbc on bc.id = fsbc.budget_class_id
                                    INNER JOIN fund_sources fs ON fs.id = fsbc.fund_source_id
                                    INNER JOIN fund_source_categories fsc on fs.fund_source_category_id = fsc.id
                                    INNER JOIN financial_years fy on ahc.financial_year_id = fy.id
                                    INNER JOIN sections s on ahc.section_id = s.id
                                    INNER JOIN section_levels sl on s.section_level_id = sl.id
                            where ahl.hierarchy_position = 2
                            and amount > 0
                            and ahc.financial_year_id = ".$financialYearId."
                            and ahc.budget_type in ".$bType."
                            and sl.hierarchy_position = 1
                            and ahc.deleted_at is null
                            and s.deleted_at is null
                            and ah.id = ".$admin_hierarchy_id."
                            group by ah.id, fy.name, bc.code, fsc.code, fs.code
                        ) income
                    group by id, name
                ) pe on sub.id = pe.id
                
                ";

        // $ownquery = "SELECT
        //             COALESCE (SUM(adc.amount),0) as ceiling 
        //             FROM admin_hierarchy_ceilings AS adc
        //             INNER JOIN ceilings AS c ON c.id = adc.ceiling_id
        //             INNER JOIN admin_hierarchies AS a ON a.id = adc.admin_hierarchy_id
        //             WHERE adc.budget_type in ".$bType."
        //             AND c.is_aggregate = true 
        //             AND adc.admin_hierarchy_id =".$admin_hierarchy_id."
        //             AND  adc.financial_year_id = ".$financialYearId."
        //             AND adc.facility_id IS NOT NULL
        //             AND c.budget_class_id IN (" . $peBudgetClassIdString . ")
        //             AND c.is_active=true ";

        return (DB::select($query)[0]->ceiling) ;
    }

     /** return Reallocation and Carryover Notification */
     public  static function getSentNotification($budget_type,$admin_hierarchy_id){

        if($budget_type == 'CARRYOVER'||$budget_type == 'SUPPLEMENTARY'){
            $query = "select comments from mtef_section_comments msc
            join scrutinizations s on msc.scrutinization_id = s.id
            where s.admin_hierarchy_id = $admin_hierarchy_id
                    AND msc.to_decision_level_id = 1
                    AND msc.from_decision_level_id = 6
            order by msc.id desc limit 1";

        return DB::select($query)[0]->comments;  

        }else{
            return "";
        }
 
    }
}
