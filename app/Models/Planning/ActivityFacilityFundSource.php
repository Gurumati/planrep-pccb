<?php

namespace App\Models\Planning;

use App\Http\Services\UserServices;
use App\Http\Services\FinancialYearServices;
use App\Models\Setup\FinancialYear;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Models\Setup\UserFacility;
use Illuminate\Support\Facades\Log;

class ActivityFacilityFundSource extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $with = ['fund_source'];

    protected $fillable = ['activity_facility_id', 'fund_source_id', 'created_by', 'updated_by'];

    public static function paginated($mtefSectionId, $budgetType)
    {

        if ($budgetType == 'SUPPLEMENTARY') {
            $perPage = Input::get('perPage', 200);
            $searchQuery = Input::get('searchQuery');
            $facilityId = Input::get('facilityId');
            $fundSourceId = Input::get('fundSourceId');
            $budgetClassId = Input::get('budgetClassId');
    
            $user = UserServices::getUser();
            $userFacilities = UserFacility::where('user_id', $user->id)->pluck('facility_id')->toArray();
    
           return DB::table('activity_facility_fund_sources as aff')
               ->join('activity_facilities as af', 'af.id','aff.activity_facility_id')
               ->join('activities as a', 'a.id', 'af.activity_id')
               ->join('facilities as f','f.id','af.facility_id')
               ->join('fund_sources as fu', 'fu.id', 'aff.fund_source_id')
               ->join('budget_classes as bc', 'bc.id','a.budget_class_id')
               ->leftJoin('activity_facility_fund_source_inputs as inp', 'inp.activity_facility_fund_source_id', 'aff.id')
               ->leftJoin('mtef_section_item_comments as mc',function($join){
                   $join->on('mc.item_id','inp.id');
                   $join->where('mc.item_type', 'INPUT');
                   $join->where('mc.addressed',false);
               })
               ->where('a.mtef_section_id', $mtefSectionId)
               //->whereIn('a.budget_type', $budgetType)
               ->where('a.code', '!=', '00000000')
               ->where(function($filter) use($facilityId, $fundSourceId, $budgetClassId, $searchQuery){
                   if (isset($facilityId) && $facilityId != 0) {
                       $filter->where('f.id', $facilityId);
                   }
                   if (isset($fundSourceId) && $fundSourceId != 0) {
                       $filter->where('fu.id', $fundSourceId);
                   }
                   if (isset($budgetClassId) && $budgetClassId != 0) {
                       $filter->where('bc.id', $budgetClassId);
                   }
                   if (isset($searchQuery) && $searchQuery != '' && $searchQuery != '%') {
                       $filter->where('a.code', $searchQuery);
                   }
               })
               ->groupBy('a.id', 'bc.id','f.id','fu.id','aff.id')
               ->select(
                   'a.code',
                   'a.description as activity',
                   'f.name as facility',
                   'fu.name as fund_source',
                   'bc.name as budget_class',
                   'aff.id',
                   DB::Raw('COALESCE(SUM(inp.quantity*inp.frequency*inp.unit_price),0) as totalBudget'),
                   DB::Raw('COUNT(mc.id) as totalComments')
                   )
               ->orderBy('a.code')
               ->paginate($perPage);
    
            return ActivityFacilityFundSource::with([
                'activity_facility',
                'activity_facility.activity' => function ($a) {
                    $a->select('id', 'code', 'description', 'budget_class_id', 'is_facility_account', 'is_final');
                },
                'activity_facility.activity.budgetClass' => function ($b) {
                    $b->select('id', 'name');
                },
                'activity_facility.activity.activity_comments'
            ])->whereHas('activity_facility.activity', function ($filter) use ($mtefSectionId, $budgetType, $budgetClassId) {
                $filter->where('mtef_section_id', $mtefSectionId);
                //$filter->whereIn('budget_type', $budgetType);
                if (isset($budgetClassId) && $budgetClassId !== '0') {
                    $filter->where('budget_class_id', $budgetClassId);
                }
            })->where(function ($search) use ($searchQuery, $facilityId, $fundSourceId, $userFacilities) {
                if (isset($searchQuery) && $searchQuery !== '%') {
                    $search->whereHas('activity_facility.activity', function ($activity) use ($searchQuery) {
                        $activity->whereRaw("LOWER(code) LIKE '%" . strtolower($searchQuery) . "%'");
                        $activity->orWhereRaw("LOWER(description) LIKE '%" . strtolower($searchQuery) . "%'");
                    });
                }
                if ((isset($facilityId) && $facilityId !== '0') || sizeof($userFacilities) >0) {
                    $search->whereHas('activity_facility', function ($activityFacility) use ($facilityId, $userFacilities) {
                        if((isset($facilityId) && $facilityId !== '0')){
                             $activityFacility->where('facility_id', $facilityId);
                        }
                        if(sizeof($userFacilities) >0){
                             $activityFacility->whereIn('facility_id', $userFacilities);
                        }
                    });
                }
                if (isset($fundSourceId) && $fundSourceId !== '0') {
                    $search->where('fund_source_id', $fundSourceId);
                }
            })->select(
                '*',
                DB::Raw('(select COALESCE(sum(quantity*frequency*unit_price),0) from activity_facility_fund_source_inputs where activity_facility_fund_source_id=activity_facility_fund_sources.id) as totalBudget'),
                DB::Raw("(select count(ic.id) from activity_facility_fund_source_inputs as i
                           join mtef_section_item_comments as ic on ic.item_id = i.id and ic.item_type ='INPUT' and ic.addressed =false
                            where activity_facility_fund_source_id=activity_facility_fund_sources.id) as totalComments")
            )->paginate($perPage);
        } elseif ($budgetType == 'CARRYOVER') {
            $perPage = Input::get('perPage', 100);
            $searchQuery = Input::get('searchQuery');
            $facilityId = Input::get('facilityId');
            $fundSourceId = Input::get('fundSourceId');
            $budgetClassId = Input::get('budgetClassId');

            //Uncomment All commented lines for retrieving previous year budget for carryover's another approach
            // $financialYear = FinancialYear::getByBudgetType($budgetType);
            // $financial_year_id = $financialYear->id;
            // $previousYearId = $financial_year_id - 1;

            $user = UserServices::getUser();
            $userSectionId = $user->section_id;
            $userFacilities = UserFacility::where('user_id', $user->id)->pluck('facility_id')->toArray();
    
           return DB::table('activity_facility_fund_sources as aff')
               ->join('activity_facilities as af', 'af.id','aff.activity_facility_id')
               ->join('activities as a', 'a.id', 'af.activity_id')
            //    ->join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')
            //    ->join('sections as s', 's.id', 'ms.section_id')
            //    ->join('mtefs as m', 'm.id', 'ms.mtef_id')
               ->join('facilities as f','f.id','af.facility_id')
               ->join('fund_sources as fu', 'fu.id', 'aff.fund_source_id')
               ->join('budget_classes as bc', 'bc.id','a.budget_class_id')
               ->leftJoin('activity_facility_fund_source_inputs as inp', 'inp.activity_facility_fund_source_id', 'aff.id')
               ->leftJoin('mtef_section_item_comments as mc',function($join){
                   $join->on('mc.item_id','inp.id');
                   $join->where('mc.item_type', 'INPUT');
                   $join->where('mc.addressed',false);
                })
            //    ->where('ms.section_id', $userSectionId)
            //    ->where('m.financial_year_id', $previousYearId)
               ->where('a.mtef_section_id', $mtefSectionId)
               ->where('a.budget_type', $budgetType)
               ->where('a.code', '!=', '00000000')
               ->where(function($filter) use($facilityId, $fundSourceId, $budgetClassId, $searchQuery){
                   if (isset($facilityId) && $facilityId != 0) {
                       $filter->where('f.id', $facilityId);
                   }
                   if (isset($fundSourceId) && $fundSourceId != 0) {
                       $filter->where('fu.id', $fundSourceId);
                   }
                   if (isset($budgetClassId) && $budgetClassId != 0) {
                       $filter->where('bc.id', $budgetClassId);
                   }
                   if (isset($searchQuery) && $searchQuery != '' && $searchQuery != '%') {
                       $filter->where('a.code', $searchQuery);
                   }
               })
               ->groupBy('a.id', 'bc.id','f.id','fu.id','aff.id')
               ->select(
                   'a.code',
                   'a.description as activity',
                   'f.name as facility',
                   'fu.name as fund_source',
                   'bc.name as budget_class',
                   'aff.id',
                   DB::Raw('COALESCE(SUM(inp.quantity*inp.frequency*inp.unit_price),0) as totalBudget'),
                   DB::Raw('COUNT(mc.id) as totalComments')
                   )
               ->orderBy('a.code')
               ->paginate($perPage);
    
            return ActivityFacilityFundSource::with([
                'activity_facility',
                'activity_facility.activity' => function ($a) {
                    $a->select('id', 'code', 'description', 'budget_class_id', 'is_facility_account', 'is_final');
                },
                'activity_facility.activity.budgetClass' => function ($b) {
                    $b->select('id', 'name');
                },
                'activity_facility.activity.activity_comments'
            ])->whereHas('activity_facility.activity', function ($filter) use ($mtefSectionId, $budgetType, $budgetClassId) {
                $filter->where('mtef_section_id', $mtefSectionId);
                //$filter->whereIn('budget_type', $budgetType);
                if (isset($budgetClassId) && $budgetClassId !== '0') {
                    $filter->where('budget_class_id', $budgetClassId);
                }
            })->where(function ($search) use ($searchQuery, $facilityId, $fundSourceId, $userFacilities) {
                if (isset($searchQuery) && $searchQuery !== '%') {
                    $search->whereHas('activity_facility.activity', function ($activity) use ($searchQuery) {
                        $activity->whereRaw("LOWER(code) LIKE '%" . strtolower($searchQuery) . "%'");
                        $activity->orWhereRaw("LOWER(description) LIKE '%" . strtolower($searchQuery) . "%'");
                    });
                }
                if ((isset($facilityId) && $facilityId !== '0') || sizeof($userFacilities) >0) {
                    $search->whereHas('activity_facility', function ($activityFacility) use ($facilityId, $userFacilities) {
                        if((isset($facilityId) && $facilityId !== '0')){
                             $activityFacility->where('facility_id', $facilityId);
                        }
                        if(sizeof($userFacilities) >0){
                             $activityFacility->whereIn('facility_id', $userFacilities);
                        }
                    });
                }
                if (isset($fundSourceId) && $fundSourceId !== '0') {
                    $search->where('fund_source_id', $fundSourceId);
                }
            })->select(
                '*',
                DB::Raw('(select COALESCE(sum(quantity*frequency*unit_price),0) from activity_facility_fund_source_inputs where activity_facility_fund_source_id=activity_facility_fund_sources.id) as totalBudget'),
                DB::Raw("(select count(ic.id) from activity_facility_fund_source_inputs as i
                           join mtef_section_item_comments as ic on ic.item_id = i.id and ic.item_type ='INPUT' and ic.addressed =false
                            where activity_facility_fund_source_id=activity_facility_fund_sources.id) as totalComments")
            )->paginate($perPage);
        
        } else{
            $perPage = Input::get('perPage', 200);
            $searchQuery = Input::get('searchQuery');
            $facilityId = Input::get('facilityId');
            $fundSourceId = Input::get('fundSourceId');
            $budgetClassId = Input::get('budgetClassId');
    
            $user = UserServices::getUser();
            $userFacilities = UserFacility::where('user_id', $user->id)->pluck('facility_id')->toArray();
    
           return DB::table('activity_facility_fund_sources as aff')
               ->join('activity_facilities as af', 'af.id','aff.activity_facility_id')
               ->join('activities as a', 'a.id', 'af.activity_id')
               ->join('facilities as f','f.id','af.facility_id')
               ->join('fund_sources as fu', 'fu.id', 'aff.fund_source_id')
               ->join('budget_classes as bc', 'bc.id','a.budget_class_id')
               ->leftJoin('activity_facility_fund_source_inputs as inp', 'inp.activity_facility_fund_source_id', 'aff.id')
               ->leftJoin('mtef_section_item_comments as mc',function($join){
                   $join->on('mc.item_id','inp.id');
                   $join->where('mc.item_type', 'INPUT');
                   $join->where('mc.addressed',false);
               })
               ->where('a.mtef_section_id', $mtefSectionId)
               ->where('a.budget_type', $budgetType)
               ->where('a.code', '!=', '00000000')
               ->where(function($filter) use($facilityId, $fundSourceId, $budgetClassId, $searchQuery){
                   if (isset($facilityId) && $facilityId != 0) {
                       $filter->where('f.id', $facilityId);
                   }
                   if (isset($fundSourceId) && $fundSourceId != 0) {
                       $filter->where('fu.id', $fundSourceId);
                   }
                   if (isset($budgetClassId) && $budgetClassId != 0) {
                       $filter->where('bc.id', $budgetClassId);
                   }
                   if (isset($searchQuery) && $searchQuery != '' && $searchQuery != '%') {
                       $filter->where('a.code', $searchQuery);
                   }
               })
               ->groupBy('a.id', 'bc.id','f.id','fu.id','aff.id')
               ->select(
                   'a.code',
                   'a.description as activity',
                   'f.name as facility',
                   'fu.name as fund_source',
                   'bc.name as budget_class',
                   'aff.id',
                   DB::Raw('COALESCE(SUM(inp.quantity*inp.frequency*inp.unit_price),0) as totalBudget'),
                   DB::Raw('COUNT(mc.id) as totalComments')
                   )
               ->orderBy('a.code')
               ->paginate($perPage);
    
            return ActivityFacilityFundSource::with([
                'activity_facility',
                'activity_facility.activity' => function ($a) {
                    $a->select('id', 'code', 'description', 'budget_class_id', 'is_facility_account', 'is_final');
                },
                'activity_facility.activity.budgetClass' => function ($b) {
                    $b->select('id', 'name');
                },
                'activity_facility.activity.activity_comments'
            ])->whereHas('activity_facility.activity', function ($filter) use ($mtefSectionId, $budgetType, $budgetClassId) {
                $filter->where('mtef_section_id', $mtefSectionId);
                $filter->where('budget_type', $budgetType);
                if (isset($budgetClassId) && $budgetClassId !== '0') {
                    $filter->where('budget_class_id', $budgetClassId);
                }
            })->where(function ($search) use ($searchQuery, $facilityId, $fundSourceId, $userFacilities) {
                if (isset($searchQuery) && $searchQuery !== '%') {
                    $search->whereHas('activity_facility.activity', function ($activity) use ($searchQuery) {
                        $activity->whereRaw("LOWER(code) LIKE '%" . strtolower($searchQuery) . "%'");
                        $activity->orWhereRaw("LOWER(description) LIKE '%" . strtolower($searchQuery) . "%'");
                    });
                }
                if ((isset($facilityId) && $facilityId !== '0') || sizeof($userFacilities) >0) {
                    $search->whereHas('activity_facility', function ($activityFacility) use ($facilityId, $userFacilities) {
                        if((isset($facilityId) && $facilityId !== '0')){
                             $activityFacility->where('facility_id', $facilityId);
                        }
                        if(sizeof($userFacilities) >0){
                             $activityFacility->whereIn('facility_id', $userFacilities);
                        }
                    });
                }
                if (isset($fundSourceId) && $fundSourceId !== '0') {
                    $search->where('fund_source_id', $fundSourceId);
                }
            })->select(
                '*',
                DB::Raw('(select COALESCE(sum(quantity*frequency*unit_price),0) from activity_facility_fund_source_inputs where activity_facility_fund_source_id=activity_facility_fund_sources.id) as totalBudget'),
                DB::Raw("(select count(ic.id) from activity_facility_fund_source_inputs as i
                           join mtef_section_item_comments as ic on ic.item_id = i.id and ic.item_type ='INPUT' and ic.addressed =false
                            where activity_facility_fund_source_id=activity_facility_fund_sources.id) as totalComments")
            )->paginate($perPage);
        }
        

    }

    public static function deleteAndTrack($afundSources)
    {
        foreach ($afundSources as $f) {
            $f->forceDelete();
            track_activity($f, UserServices::getUser(), 'delete_activity_facility_fund_source');
        }
    }

    public static function getOne($id)
    {
        return ActivityFacilityFundSource::with([
            'activity_facility',
            'activity_facility.activity' => function ($a) {
                $a->select('id', 'code', 'description', 'budget_class_id', 'is_facility_account', 'is_final');
            },
            'activity_facility.activity.budgetClass' => function ($b) {
                $b->select('id', 'name');
            },
        ])->where('id', $id)->first();
    }

    public function fund_source()
    {
        return $this->belongsTo('App\Models\Setup\FundSource')
            ->select(['id', 'name']);
    }

    public function inputs()
    {
        return $this->hasMany('App\Models\Planning\ActivityFacilityFundSourceInput')
            ->select([
                'id', 'activity_facility_fund_source_id', 'gfs_code_id', 'unit_id', 'budget_type',
                'quantity', 'frequency', 'unit_price', 'has_breakdown', 'is_in_kind', 'is_procurement', 'procurement_type_id'
            ]);
    }

    public function activityFacility()
    {
        return $this->belongsTo('App\Models\Planning\ActivityFacility');
    }

    public function activity_facility()
    {
        return $this->belongsTo('App\Models\Planning\ActivityFacility');
    }

    public static function addFundSource($data)
    {
        $existing = ActivityFacilityFundSource::where('activity_facility_id', $data['activity_facility_id'])->where('fund_source_id', $data['fund_source_id']);
        if ($existing->count() === 0) {
            $activityFacilityFundSource = new ActivityFacilityFundSource($data);
            $activityFacilityFundSource->save();
            track_activity($activityFacilityFundSource, UserServices::getUser(), 'create_activity_facility_fund_source');
            return $activityFacilityFundSource->id;
        } else {
            return $existing->first()->id;
        }
    }
}
