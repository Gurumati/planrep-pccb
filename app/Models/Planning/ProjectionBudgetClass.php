<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectionBudgetClass extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $table = "projection_budget_classes";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function projection(){
        return $this->belongsTo('App\Models\Planning\Projection');
    }

    public function budget_class(){
        return $this->belongsTo('App\Models\Setup\BudgetClass');
    }
}
