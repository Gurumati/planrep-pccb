<?php

namespace App\Models\Planning;

use App\Http\Services\UserServices;
use App\Models\Budgeting\BudgetType;
use  App\Models\Setup\ActivityFacilityFundSourceInputPeriod;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class ActivityFacilityFundSourceInput extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $fillable = ['activity_facility_fund_source_id','gfs_code_id','unit_id','quantity','frequency','unit_price',
        'has_breakdown','is_in_kind','is_procurement','procurement_type_id','budget_type','update_by','created_by'];

    protected $with = [
        'activityPeriods', 'input_breakdowns','gfs_code','procurement_type','unit','input_forwards','input_comments'
    ];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'activity_facility_fund_source_id' => 'required|integer',
                'gfs_code_id' => 'required|integer',
                'unit_id' => 'required|integer',
                'quantity' => 'required|numeric|min:0',
                'frequency' => 'required|numeric|min:0',
                'unit_price' => 'required|numeric|min:0',
                'has_breakdown' => 'required|boolean',
                'is_in_kind' => 'required|boolean',
                'is_procurement' => 'required|boolean',
                'budget_type' => ['required','string',Rule::in(BudgetType::getAll())]
            ],
            $merge);
    }


    /**
     * @param $input
     * @return Model
     */
    public static function createOrUpdate($input)
    {
        $id = (isset($input['id']))?$input['id'] : null;

        $updatedInput = ActivityFacilityFundSourceInput::updateOrCreate(['id'=>$id],$input);

        $action = ($id == null)?'create_input':'update_input';

        track_activity($updatedInput, UserServices::getUser(), $action);

        if (isset($input['has_breakdown']) && !$input['has_breakdown']) {
            DB::table('activity_facility_fund_source_input_breakdowns')->where('activity_facility_fund_source_input_id', $updatedInput->id)->delete();
        }
        if (isset($input['input_breakdowns']) && $input['has_breakdown']) {
            self::saveInputBreakDowns($updatedInput->id, $input['input_breakdowns']);
        }
        if (isset($input['input_forwards'])) {
            self::saveInputForwards($updatedInput->id, $input['input_forwards']);
        }

        return $updatedInput;

    }

    /**
     * @param $inputId
     * @param $breakDowns
     */
    public static function saveInputBreakDowns($inputId, $breakDowns)
    {
        $ids = [];
        foreach ($breakDowns as $brk) {
            $id = (isset($brk['id'])) ? $brk['id'] : null;
            if ($id != null) {
                $breakdown = ActivityFacilityFundSourceInputBreakdown::find($id);
                $breakdown->update($brk);
                track_activity($breakdown, UserServices::getUser(), 'update_input_break_down');
            }
            else{
                $breakdown = new ActivityFacilityFundSourceInputBreakdown(array_merge(['activity_facility_fund_source_input_id'=>$inputId],$brk));
                $breakdown->save();
                track_activity($breakdown, UserServices::getUser(), 'create_input_break_down');
            }
            array_push($ids,$breakdown->id);

        }
        ActivityFacilityFundSourceInputBreakdown::where('activity_facility_fund_source_input_id',$inputId)->whereNotIn('id',$ids)->forceDelete();

    }

    /**
     * @param $inputId
     * @param $forwards
     */
    public static function saveInputForwards($inputId, $forwards)
    {
        $order = 1;
        foreach ($forwards as $fwrd) {

            $id = (isset($fwrd['id'])) ? $fwrd['id'] : null;

            if($id != null){
                $forward = ActivityFacilityFundSourceInputForward::find($id);
                $forward->update(array_merge($fwrd,['financial_year_order'=>$order]));
                track_activity($forward, UserServices::getUser(), 'update_input_forward');
            }
            else if(isset($fwrd['quantity']) && isset($fwrd['frequency'])){
                $forward = new ActivityFacilityFundSourceInputForward(array_merge($fwrd,['activity_facility_fund_source_input_id'=>$inputId,'financial_year_order'=>$order]));
                $forward->save();
                track_activity($forward, UserServices::getUser(), 'create_input_forward');

            }
            $order++;
        }
    }

    public static function deleteAndTrack($inputs){
        foreach ($inputs as $i) {
            ActivityFacilityFundSourceInputPeriod::where('activity_facility_fund_source_input_id',$i->id)->forceDelete();
            $i->forceDelete();
            track_activity($i, UserServices::getUser(), 'delete_inputs');
        }
    }

    public static function getIdsByActivity($activityId)
    {
        return DB::table('activity_facility_fund_source_inputs as input')->
                    join('activity_facility_fund_sources as fun','fun.id','input.activity_facility_fund_source_id')->
                    join('activity_facilities as af','af.id','fun.activity_facility_id')->
                    where('af.activity_id',$activityId)->pluck('input.id')->toArray();


    }
    public static function getIdsByActivityIds($activityIds)
    {
        return DB::table('activity_facility_fund_source_inputs as input')->
                    join('activity_facility_fund_sources as fun','fun.id','input.activity_facility_fund_source_id')->
                    join('activity_facilities as af','af.id','fun.activity_facility_id')->
                    whereIn('af.activity_id',$activityIds)->pluck('input.id')->toArray();


    }

    public static function getByActivity($activityId)
    {
        return DB::table('activity_facility_fund_source_inputs as input')->
                    join('activity_facility_fund_sources as fun','fun.id','input.activity_facility_fund_source_id')->
                    join('activity_facilities as af','af.id','fun.activity_facility_id')->
                    where('af.activity_id',$activityId)->get();
    }

    public function input_breakdowns() {
        return $this->hasMany('App\Models\Planning\ActivityFacilityFundSourceInputBreakdown')
            ->select(['activity_facility_fund_source_input_breakdowns.id','item','activity_facility_fund_source_input_id','unit_id','unit_price','quantity','frequency']);
    }

    public function gfs_code() {
        return $this->belongsTo('App\Models\Setup\GfsCode')
            ->select(['gfs_codes.id','code','description','name']);
    }
    public function unit() {
        return $this->belongsTo('App\Models\Setup\Unit')
            ->select(['id','name','symbol']);
    }
    public function procurement_type() {
        return $this->belongsTo('App\Models\Setup\ProcurementType')
            ->select(['id','name','description']);
    }

    public function input_forwards() {
        return $this->hasMany('App\Models\Planning\ActivityFacilityFundSourceInputForward');
    }
    public function activityPeriods() {
        return $this->hasMany('App\Models\Setup\ActivityFacilityFundSourceInputPeriod');
    }

    public function activityFacilityFundSource(){
        return $this->belongsTo('App\Models\Planning\ActivityFacilityFundSource');
    }

    public function activity_facility_fund_source(){
        return $this->belongsTo('App\Models\Planning\ActivityFacilityFundSource');
    }

    public function input_comments(){
        return $this->hasMany('App\Models\Planning\MtefSectionItemComment','item_id')->
        where('mtef_section_item_comments.item_type','INPUT')->
        where('mtef_section_item_comments.addressed',false);
    }
    public function account() {
        return $this->hasOne('App\Models\Execution\BudgetExportAccount','activity_facility_fund_source_input_id');
    }
    public static function getInputIds($adminHierarchyId,$financialYearId,$financialSystem)
    {
        /**
         * implemented for approved budget only...
         * other budget type to be implemented when needed
         */

        return DB::table('mtefs as m')->
                    join('mtef_sections as ms','m.id','ms.mtef_id')->
                    join('activities as a','ms.id','a.mtef_section_id')->
                    join('activity_facilities as af','a.id','af.activity_id')->
                    join('activity_facility_fund_sources as affs','af.id','affs.activity_facility_id')->
                    join('activity_facility_fund_source_inputs as affsi','affs.id','affsi.activity_facility_fund_source_id')
                  ->  join('budget_export_accounts as bea','affsi.id','bea.activity_facility_fund_source_input_id')
                    ->where('m.admin_hierarchy_id',$adminHierarchyId)
                    ->where('m.financial_year_id',$financialYearId)
                    ->where('a.budget_type','APPROVED')
                    ->where('bea.financial_system_code',$financialSystem)
                    ->pluck('affsi.id')
                    ->toArray();
    }
}
