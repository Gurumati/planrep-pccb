<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminHierarchySectMappings extends Model
{
    //
    public $timestamps = true;
    protected $table = "admin_hierarchy_lev_sec_mappings";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function admin_hierarchy_level() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchyLevel');
    }

    public function section() {
        return $this->belongsTo('App\Models\Setup\Section')->select('id','section_level_id');
    }

    public static function getBudgetingSectionLevelIds(){
        $budgetingSectionLevelIds = DB::table('admin_hierarchy_lev_sec_mappings as m')
            ->join('sections as s','s.id','m.section_id')
            ->where('m.can_budget', true)
            ->distinct('s.section_level_id')
            ->select('s.section_level_id')
            ->pluck('s.section_level_id');
        return $budgetingSectionLevelIds;
    }
}
