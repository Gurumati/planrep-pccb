<?php

namespace App\Models\Planning;

use App\Http\Controllers\Flatten;
use App\Models\CacheKeys;
use App\Models\Setup\SectionParent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use App\Models\Setup\Section;
use App\Http\Services\UserServices;
use App\Models\Setup\UserFacility;

class AnnualTarget extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'mtef_annual_targets';

    protected $with = ['longTermTarget'];
    protected $fillable = ['description','created_at','updated_at','deleted_at','created_by','deleted_by','updated_by','code','mtef_id','long_term_target_id','section_id','is_final'];

    /**
     * @param $mtefId
     * @param $mtefSectionId
     * @param $budgetType
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function getByMtefSectionPaginated($mtefId, $mtefSectionId, $budgetType)
    {

        /** @var  $perPage */
        $perPage = Input::get("perPage", 10);

        /** @var  $searchQuery */
        $searchQuery = Input::get("searchQuery");
        $searchQuery = isset($searchQuery) ? "'%" . strtolower($searchQuery) . "%'" : "'%'";

        /** @var  $flatten */
        $flatten = new Flatten();
        $codes = $flatten->getAutoBudgetTemplatesCodes();
        /** @var  $sectionId */
        $sectionId = MtefSection::find($mtefSectionId)->section_id;

        /** @var  $sectionIds */
        $sectionIds = Section::getParentSectionIds($sectionId);

        /** @var  $targets */
        $targets = AnnualTarget::with('longTermTarget')->whereIn('section_id', $sectionIds)->where('mtef_id', $mtefId)->where('is_final', true)->whereRaw('LOWER(description) like ' . $searchQuery)->orderBy('code')->paginate($perPage);
        if ($budgetType == null) {
            return $targets;
        }

        $user = UserServices::getUser();
        $facilities = UserFacility::where('user_id', $user->id)->pluck('facility_id');

        foreach ($targets as &$target) {
            $totalActivities = Activity::where(function ($a) use ($facilities) {
                if (sizeof($facilities) > 0) {
                    $a->whereHas('activity_facilities', function ($f) use ($facilities) {
                        $f->whereIn('facility_id', $facilities);
                    });
                }
            })->where('mtef_section_id', $mtefSectionId)->where('budget_type', $budgetType)->where('mtef_annual_target_id', $target->id)->count();
            $target->totalActivities = $totalActivities;
        }

        //LOG::INFO($targets);

        return $targets;
    }

    public static function getByMtefSectionAndPlanChain($mtefId, $mtefSectionId, $budgetType, $planChainId)
    {


        /** @var  $flatten */
        $flatten = new Flatten();
        $codes = $flatten->getAutoBudgetTemplatesCodes();
        /** @var  $sectionId */
        $sectionId = MtefSection::find($mtefSectionId)->section_id;

        /** @var  $sectionIds */
        $sectionIds = Cache::rememberForever(CacheKeys::SECTION_WITH_PARENT_IDS . $sectionId, function () use ($sectionId, $flatten) {

            $sectionWithParentTree = SectionParent::where('id', $sectionId)->get();
            return $flatten->flattenSectionWithParent($sectionWithParentTree);

        });

        /** @var  $targets */
        $targets = AnnualTarget::with('longTermTarget')->whereIn('section_id', $sectionIds)
        ->where('mtef_id', $mtefId)->whereHas('longTermTarget', function ($lTarget) use ($planChainId) {
            $lTarget->where('plan_chain_id', $planChainId);
        })
            ->orderBy('code')->get();
        if ($budgetType == null) {
            return $targets;
        }

        $user = UserServices::getUser();
        $facilities = UserFacility::where('user_id', $user->id)->pluck('facility_id');

        foreach ($targets as &$target) {
            $totalActivities = Activity::where(function ($a) use ($facilities) {
                if (sizeof($facilities) > 0) {
                    $a->whereHas('activity_facilities', function ($f) use ($facilities) {
                        $f->whereIn('facility_id', $facilities);
                    });
                }
            })->where('mtef_section_id', $mtefSectionId)->where('budget_type', $budgetType)->where('mtef_annual_target_id', $target->id)->count();
            $target->totalActivities = $totalActivities;
        }

        return $targets;
    }

    public static function confirm($id, $data)
    {
        self::where('id', $id)->update(['is_final' => true, 'description' => $data['description']]);
    }


    public function children()
    {
        return $this->hasMany('App\Models\Planning\Activity', 'mtef_annual_target_id');
    }


    public function longTermTarget()
    {
        return $this->belongsTo('App\Models\Planning\LongTermTarget')->select(
            'id',
            'code',
            'description',
            'intervention_id',
            'is_active',
            'plan_chain_id',
            'reference_document_id',
            'section_id',
            'sector_problem_id'
        );
    }

    public function activities()
    {
        return $this->hasMany('App\Models\Planning\Activity', 'mtef_annual_target_id');
    }
}
