<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity_backup extends Model
{

    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $with = [
        'budgetClass','activityCategory','responsiblePerson','project','sector_problem','references',
        'facility','periods','indicator','funders','fundSources','activityInputs','intervention'
    ];

    public function annualTarget() {
        return $this->belongsTo('App\Models\Planning\AnnualTarget','mtef_annual_target_id');
    }

    public function budgetClass(){
        return $this->belongsTo('App\Models\Setup\BudgetClass')
                    ->select(['id','name','parent_id']);
    }

    public function activityCategory(){
        return $this->belongsTo('App\Models\Setup\ActivityCategory')
                    ->select(['id','name']);
    }
    public function responsiblePerson(){
        return $this->belongsTo('App\Models\Setup\ResponsiblePerson')
                    ->select(['id','name','mobile','phone_number','address','email']);
    }
    public function project(){
        return $this->belongsTo('App\Models\Setup\Project')
                    ->select(['id','name','code','description']);
    }
    public function facility(){
        return $this->belongsTo('App\Models\Setup\Facility')
                    ->select(['id','name','facility_code']);
    }
    public function periods(){
        return $this->hasMany('App\Models\Planning\ActivityPeriod');
    }
    public function indicator(){
        return $this->hasOne('App\Models\Planning\ActivityIndicator')
                    ->select(['id','indicator_value','description','activity_id']);
    }
    public function references(){
        return $this->belongsToMany('App\Models\Setup\Reference','activity_references')
                    ->select(['references.id','references.code','references.name']);
    }
    public function fundSources(){
        return $this->belongsToMany('App\Models\Setup\FundSource','activity_fund_sources')
                     ->select(['fund_sources.id','fund_sources.name','fund_sources.description']);
    }
    public function funders(){
        return $this->belongsToMany('App\Models\Setup\Funder','activity_funders')
                    ->select(['funders.id','funders.name','funders.code','funders.is_local']);
    }
    public function activityInputs(){
        return $this->hasMany('App\Models\Budgeting\ActivityInput');
    }


    public function budgetSubmissionLines()
    {
        return $this->hasMany('App\Models\Budgeting\BudgetSubmissionLine');
    }
    public function intervention(){
        return $this->belongsTo('App\Models\Setup\Intervention')
            ->select(['id','description','priority_area_id']);
    }
    public function sector_problem(){
        return $this->belongsTo('App\Models\Setup\SectorProblem')
            ->select(['id','description','priority_area_id']);
    }

}
