<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SectorTarget extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $table = "sector_targets";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function sector(){
        return $this->belongsTo('App\Models\Setup\Sector');
    }

    public function long_term_target(){
        return $this->belongsTo('App\Models\Planning\LongTermTarget');
    }
}
