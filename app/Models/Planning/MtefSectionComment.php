<?php

namespace App\Models\Planning;

use App\Models\Budgeting\Scrutinization;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Services\UserServices;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class MtefSectionComment extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $fillable = ['mtef_section_id','scrutinization_id','mtef_comment_id','comments','addressed','commented_by','address_comments','from_decision_level_id','to_decision_level_id','direction'];

    public function mtefSection(){
        return $this->belongsTo('App\Models\Planning\MtefSection');
    }

    public static function addComments($mtefSectionId, $data, $fromDecisionLevelId, $toDecisionLevelId){
        $user = UserServices::getUser();
        $round = DB::table('scrutinization_rounds')->where('mtef_section_id',$mtefSectionId)->where('status', 3)->count();
        $round = $round + 1;
        $Id = DB::table('scrutinization_rounds')->insertGetId(array(
            "round" => $round,
            "status" => 1,
            "start_decision_level_id" => $fromDecisionLevelId,
            "mtef_section_id" => $mtefSectionId
        ));
        $section = MtefSection::where('id',$mtefSectionId)->first();
        $scrutinizationId = DB::table('scrutinizations')->insertgetId(array(
            "admin_hierarchy_id" => UserServices::getUser()->admin_hierarchy_id,
            "section_id" => $section->section_id,
            "decision_level_id" => $toDecisionLevelId,
            "scrutinization_round_id" => $Id,
            "status" => 0,
            "created_at" => date('Y-m-d h:i:sa')
        ));

        MtefSectionComment::create([
            'mtef_section_id' => $mtefSectionId,
            'comments' => $data['comments'],
            'scrutinization_id' => $scrutinizationId,
            'direction' => $data['direction'],
            'from_decision_level_id' => $fromDecisionLevelId,
            'date_commented' => date('Y-M-d'),
            'commented_by' => $user->first_name . ' ' . $user->last_name,
            'to_decision_level_id' => $toDecisionLevelId
        ]);
    }
}
