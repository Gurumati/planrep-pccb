<?php

namespace App\Models\Planning;

use App\Http\Services\UserServices;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Input;
use App\Models\Execution\ActivityProjectOutput;
use App\Models\Setup\Project;
use Illuminate\Support\Facades\Log;

class ActivityFacility extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "activity_facilities";
    protected $with = ['facility'];
    protected $fillable = ['activity_id','facility_id','created_by','updated_by'];

    public static  function deleteAndTrack($afacilities){
        foreach ($afacilities as $afacility) {
            ActivityProjectOutput::where('activity_id',$afacility->activity_id)->where('facility_id',$afacility->facility_id)->forceDelete();
            $afacility->forceDelete();
            track_activity($afacility,UserServices::getUser(),'delete_activity_facility');
        }
    }
    public function facility(){
        return $this->belongsTo('App\Models\Setup\Facility','facility_id','id')->select('facilities.id','facilities.name','facilities.facility_code','facilities.facility_type_id');
    }

    public function activity_facility_fund_sources(){
        return $this->belongsToMany('App\Models\Setup\FundSource','activity_facility_fund_sources')->
            select('fund_sources.id','fund_sources.name');
    }
    public function activity_facility_fund_sources2(){
        return $this->hasMany('App\Models\Planning\ActivityFacilityFundSource')->select('id','activity_facility_id','fund_source_id');
    }

    public function activity(){
        return $this->belongsTo('App\Models\Planning\Activity',"activity_id","id");
    }

    public static function paginateByActivity($activityId){
        $perPage = Input::get('perPage');
        $perPage = isset($perPage)?$perPage:10;
        $searchQuery = Input::get('searchQuery');
        $searchQuery = isset($searchQuery)?"'%".strtolower($searchQuery)."%'":"'%'";
        $project = Project::whereHas('activity', function($act) use($activityId){
            $act->where('id',$activityId);
        })->first();

        $activityFacilities =  ActivityFacility::with('activity_facility_fund_sources')->
                                                 whereHas('facility',function ($query) use ($searchQuery){
                                                     $query->whereRaw('LOWER(name) like ' . $searchQuery);
                                                     $query->where('is_active',true);
                                                 })->
                                                 where('activity_id',$activityId)->
                                                 orderBy('id','desc')->
                                                 paginate($perPage);
      //  if(isset($project) && $project->code !== '0000'){
            foreach($activityFacilities as &$af){
                $af->project_output = ActivityProjectOutput::where('activity_id',$activityId)->where('facility_id',$af->facility_id)->first();
            }
    //    }
        
        return $activityFacilities;
    }

    public static function paginateByActivityWithInput($activityId){
        $activityFacilities =  ActivityFacility::with(
                                           'activity_facility_fund_sources2',
                                                   'activity_facility_fund_sources2.inputs')->
                                                 whereHas('facility',function ($query){
                                                      $query->where('is_active',true);
                                                  })->
                                                 where('activity_id',$activityId)->
                                                 orderBy('id','desc')->
                                                 paginate(10);
        return $activityFacilities;
    }

    public static function addFacility($data){
        $existing =ActivityFacility::where('activity_id',$data['activity_id'])->where('facility_id',$data['facility_id']);
        if( $existing->count() === 0) {
            $activityFacility = new ActivityFacility($data);
            $activityFacility->save();
            if(isset($data['project_output_id'])){
                ActivityProjectOutput::create([
                    'activity_id'=>$data['activity_id'],
                    'project_output_id'=>$data['project_output_id'],
                    'value'=>$data['output_value'],
                    'facility_id'=>$data['facility_id']
                    ]);
            }
            track_activity($activityFacility, UserServices::getUser(), 'create_activity_facility');
            return $activityFacility->id;
        }else{
            return $existing->first()->id;
        }
    }

    public  static  function permanentDelete($id){

                $fundSourceIds = ActivityFacilityFundSource::where('activity_facility_id',$id)->pluck('id');

                $inputIds = ActivityFacilityFundSourceInput::whereIn('activity_facility_fund_source_id',$fundSourceIds)->pluck('id');

                $inputForwards =ActivityFacilityFundSourceInputForward::whereIn('activity_facility_fund_source_input_id',$inputIds)->get();
                ActivityFacilityFundSourceInputForward::deleteAndTrack($inputForwards);

                $breakDowns = ActivityFacilityFundSourceInputBreakdown::whereIn('activity_facility_fund_source_input_id',$inputIds)->get();
                ActivityFacilityFundSourceInputBreakdown::deleteAndTrack($breakDowns);

                $inputs = ActivityFacilityFundSourceInput::whereIn('id',$inputIds)->get();
                ActivityFacilityFundSourceInput::deleteAndTrack($inputs);

                $afundSources = ActivityFacilityFundSource::where('activity_facility_id',$id)->get();
                ActivityFacilityFundSource::deleteAndTrack($afundSources);

                $afacilities= ActivityFacility::where('id',$id)->get();
                ActivityFacility::deleteAndTrack($afacilities);

    }
}
