<?php

namespace App\Models\Planning;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'assets';


    public function children() {
        return $this->hasMany('App\Models\Planning\Asset', 'parent_id');
    }

    public function adminHierarchies() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchy');
    }

    public function admin_hierarchy() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchy','admin_hierarchy_id','id');
    }

    public function transport_facility_type() {
        return $this->belongsTo('App\Models\Setup\TransportFacilityType','parent_id','id');
    }

    public function asset_condition() {
        return $this->belongsTo('App\Models\Setup\AssetCondition','condition','id');
    }

    public function asset_use() {
        return $this->belongsTo('App\Models\Setup\AssetUse','used_for','id');
    }

}
