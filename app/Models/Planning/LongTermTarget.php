<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LongTermTarget extends Model



{

    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'long_term_targets';

    protected $with=['planChain','intervention','sector_problem','references'];

    public static function refreshCode($id){
        $target = LongTermTarget::find($id);
        //Update code
        $newCode = LongTermTarget::getNextCode($target->plan_chain_id, $target->reference_document_id);
        $target->code = $newCode;
        $target->save();
        //Update annualTarget codes
        $annualTargets = AnnualTarget::where('long_term_target_id',$id)->get();
        foreach( $annualTargets as $aTarget){
            $aTarget->code=$newCode;
            $aTarget->save();

             //Update activities
            $actIds = Activity::where('mtef_annual_target_id',$aTarget->id)->pluck('id')->toArray();
            foreach($actIds as $actId){
               $a= Activity::refreshActivityCode($actId);
            }
        }
        return  $target;
    }

    public static function getNextCode($planChainId, $refCodeId){
        //Get Plan chain
        $planChain = PlanChain::find($planChainId);
        $codeString = trim($planChain->code) . "%";

        //Last target code per reference doc
        $targetByRef = DB::table('long_term_targets')
                            ->where('code', 'like', $codeString)
                            ->where('reference_document_id', $refCodeId);

        $lastTargetCode= $targetByRef->max('code');

        if ($lastTargetCode != null) {

            $code = LongTermTarget::reclaimCode($planChain->code,$targetByRef->pluck('code')->toArray());

            if($code == null){

                $codeIndex = str_replace($planChain->code, "", $lastTargetCode);

                $firstDigit = substr($codeIndex, 0, 1);
                $lastDigit = substr($codeIndex, 1, 1);

                $alpha = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
                $size = sizeof($alpha);
                $indexOfFirstDigit = array_search($firstDigit, $alpha);
                $indexOfLastDigit = array_search($lastDigit, $alpha);

                /**
                * First digit and last digit within bound increment last digit
                */
                if (($indexOfFirstDigit <= ($size - 1)) && ($indexOfLastDigit < ($size - 1))) {
                    $newFirstDigit = $alpha[$indexOfFirstDigit];
                    $newSecondDigit = $alpha[$indexOfLastDigit + 1];
                }
                /**
                * First digit within bound and last digit = last element ; Increment first digit and set last digit = first element
                */
                else if (($indexOfFirstDigit < ($size - 1)) && ($indexOfLastDigit == ($size - 1))) {
                        $newFirstDigit = $alpha[$indexOfFirstDigit + 1];
                        $newSecondDigit = $alpha[0];
                }
                $code = $planChain->code . $newFirstDigit . $newSecondDigit;
            }
        }
        else {
            $code = $planChain->code . "01";
        }

        return $code;
    }

    public static function reclaimCode($prefix, $existingCodes){
        $possibleCode = [];
        for($i =0; $i <= 9; $i++){
            for($j =1; $j <= 9; $j++){
                array_push($possibleCode,($prefix.$i.$j));
            } 
        }
       $available = array_diff($possibleCode,$existingCodes);
       if(sizeof($available) > 0){
         return min($available);
       }
       return null;
    }

    public function children() {
        return $this->hasMany('App\Models\Planning\AnnualTarget');
    }

    public function sectorTargets(){
        return $this->hasMany('App\Models\Planning\SectorTarget');
    }
    public function sectors(){
        return $this->belongsToMany('App\Models\Setup\Sector','sector_targets');
    }

    public function annualTargets(){
    return $this->hasMany('App\Models\Planning\AnnualTarget');
    }


    public function referenceDocument() {
        return $this->belongsTo('App\Models\Setup\ReferenceDocument');
    }

    public function planChain() {
        return $this->belongsTo('App\Models\Setup\PlanChain')->
                      select('id','code','description','parent_id');
    }

    public function targetType() {
        return $this->belongsTo('App\Models\Setup\TargetType');
    }

    public function intervention(){
        return $this->belongsTo('App\Models\Setup\Intervention')
            ->select(['id','description','priority_area_id']);
    }
    public function sector_problem(){
        return $this->belongsTo('App\Models\Setup\SectorProblem')
            ->select(['id','description','priority_area_id']);
    }
    public function references(){
        return $this->belongsToMany('App\Models\Setup\Reference','long_term_target_references')
            ->select(['reference_docs.id','reference_docs.code','reference_docs.name','reference_docs.reference_type_id']);
    }
    public function performanceIndicators(){
        return $this->belongsToMany('App\Models\Setup\PerformanceIndicator','target_performance_indicators','target_id')
            ->select(['performance_indicators.id','performance_indicators.description','performance_indicators.number']);
    }



}
