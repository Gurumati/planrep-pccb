<?php

namespace App\Models\Planning;

use App\Http\Services\Budgeting\BudgetExportAccountService;
use App\Http\Services\CustomPager;
use App\Http\Services\UserServices;
use App\Models\Budgeting\BudgetType;
use App\Models\Execution\BudgetExportAccount;
use App\Models\Execution\BudgetExportTransaction;
use App\Models\Setup\ActivityCategory;
use App\Models\Setup\ActivitySurplusCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use App\Models\Execution\BudgetExportTransactionItem;
use App\Models\Execution\BudgetExportToFinancialSystem;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\UserFacility;

class Activity extends Model
{

    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $fillable = [
        'description', 'budget_class_id', 'activity_category_id', 'mtef_annual_target_id', 'mtef_section_id', 'locked',
        'project_id', 'code', 'intervention_id', 'sector_problem_id', 'is_facility_account', 'activity_status_id',
        'activity_task_nature_id', 'generic_activity_id', 'indicator', 'indicator_value', 'completion_percentage',
        'responsible_person_id', 'budget_type', 'created_by', 'update_by'
    ];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'description' => 'required|max:255',
                'budget_class_id' => 'required|integer',
                'activity_category_id' => 'required|integer',
                'mtef_annual_target_id' => 'required|integer',
                'mtef_section_id' => 'required|integer',
                'project_id' => 'required|integer',
                'is_facility_account' => 'required|boolean',
                // 'responsible_person_id' => 'required|integer',
                //'responsible_person_id' => 'integer',
                'budget_type' => ['required', 'string', Rule::in(BudgetType::getAll())],
                'indicator' => 'required',
                'indicator_value' => 'required',
                'locked' => 'boolean:false',
            ],
            $merge
        );
    }

    /**
     * @param $mtefSectionId
     * @param $targetId
     * @param $budgetType
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public static function paginated($mtefSectionId, $targetId, $budgetType, $facilityIds)
    {
        $perPage = Input::get('perPage', 10);

        $user = UserServices::getUser();
        $userFacilities = UserFacility::where('user_id', $user->id)->pluck('facility_id')->toArray();
        $facilityIds = array_merge($facilityIds,$userFacilities);

        $searchQuery = Input::get("searchQuery");
        $searchQuery = isset($searchQuery) ? "'%" . strtolower($searchQuery) . "%'" : "'%'";

        $activities = Activity::with(
            'activity_comments',
            'annualTarget',
            'budgetClass',
            'activityCategory',
            'activityTaskNature',
            'responsiblePerson',
            'project',
            'sector_problem',
            'references',
            'periods',
            'intervention'
        )->where('mtef_section_id', $mtefSectionId)->where('mtef_annual_target_id', $targetId)->where(function ($filter) use ($searchQuery) {
            $filter->whereRaw('LOWER(description) like ' . $searchQuery);
        })
        ->where(function($filter) use($facilityIds){
            if(sizeof($facilityIds) >0){
                $filter->whereHas('activity_facilities', function($fac) use($facilityIds){
                    $fac->whereIn('facility_id',$facilityIds);
                });
                $filter->orDoesntHave('activity_facilities');
            }
        })
        ->where('budget_type', $budgetType)->orderBy('code', 'asc')
        ->paginate($perPage);

        foreach ($activities as $a) {
            $a->totalFacilities = ActivityFacility::where('activity_id', $a->id)->count();
            $a->totalBudget = Activity::getTotalBudget($a->id);
            $a->cofog_id = DB::table("activity_references")
            ->where("activity_id",$a->id);
            //->first()->cofog_id;
            foreach ($a->references as $r) {
                $r->type = DB::table("reference_docs as rd")
                    ->join("reference_types  as rt","rd.reference_type_id","rt.id")
                    ->where("rd.id",$r->id)->select("rt.id as reference_type_id")->first();
            }
        }
        return $activities;
    }

    /**
     * @param $activity
     * @param $financialYearId
     * @param $budgetType
     * @param $adminHierarchyId
     * @param $sectionId
     * @return Model
     */
    public static function createOrUpdate($activity, $financialYearId, $budgetType, $adminHierarchyId, $sectionId)
    {
         /** @var  $id =Activity Id */
        $id = (isset($activity['id'])) ? $activity['id'] : null;
        if ($id === null) {
            $activity['code'] =  self::getFacilityAccountNextCode($financialYearId, $budgetType, $adminHierarchyId, $activity['mtef_annual_target_id'], $activity['activity_category_id']);
            $defaultStatus = ActivityStatus::where('is_default', true)->first();
            if (is_null($defaultStatus)) {
                $defaultStatus = ActivityStatus::where('position', 1)->first();
            }
            $activity['activity_status_id'] = isset($defaultStatus) ? $defaultStatus->id : null;
        }

        $activityId = Activity::updateOrCreate(['id' => $id], $activity)->id;
        if (isset($activity['periods'])) {
            Activity::saveActivityPeriods($activityId, $activity['periods']);
        }
        if (isset($activity['references']) || isset($activity['cofog_id'])) {
            Activity::saveActivityReferences($activityId, $activity['references'],$activity['cofog_id']);
        }
        if (isset($activity['surplus_id'])){
            Activity::saveActivitySurplusCategories($activityId,$activity['surplus_id']);
        }


        return Activity::with('project')->where('id',$activityId)->first();
    }

    /**
     * @param $financialYearId
     * @param $budgetType
     * @param $adminHierarchyId
     * @param $targetId
     * @param $activityCategoryId
     * @return string
     */
    public static function getCouncilAccountCode($financialYearId, $budgetType, $adminHierarchyId, $targetId, $activityCategoryId)
    {

        /**
         * Get valid facility account activity code to be replaced
         */
        $target = AnnualTarget::find($targetId);
        /**
         * Activity code modified to accommodate code structure used in MUSE
         */
//        $activityCategory = ActivityCategory::find($activityCategoryId);
//        $code6Char = $target->code . $activityCategory->code;

        $code6Char = $target->code;

        $lastActivityCode = DB::table('activities as a')
            ->join('mtef_sections as ms', 'a.mtef_section_id', 'ms.id')
            ->join('mtefs as m', 'm.id', 'ms.mtef_id')
            ->where('m.admin_hierarchy_id', $adminHierarchyId)
            ->where('m.financial_year_id', $financialYearId)
            ->whereRaw("substr(a.code,1,6) = '" . $code6Char . "'")
            ->whereRaw("substr(a.code,7,2) >= '01'")
            ->whereRaw("substr(a.code,7,2) <= '98'")
            ->whereRaw("substr(a.code,8,1) <= '9'")
            ->whereRaw("substr(a.code,7,1) >= '0'")
            ->max('a.code');
        if ($lastActivityCode != null) {
            $codeIndex = str_replace($code6Char, "", $lastActivityCode);
            $newIndex = intval($codeIndex) + 1;
            $newIndex = ($newIndex < 10) ? "0" . $newIndex : $newIndex;
            $code = $code6Char . $newIndex;
        } else {

            $code = $code6Char . "01";
        }
        return $code;

    }

    /**
     * @param $financialYearId
     * @param $budgetType
     * @param $adminHierarchyId
     * @param $targetId
     * @param $activityCategoryId
     * @return string
     */
    public static function getFacilityAccountNextCode($financialYearId, $budgetType, $adminHierarchyId, $targetId, $activityCategoryId)
    {

        $target = AnnualTarget::find($targetId);

        $activityCategory = ActivityCategory::find($activityCategoryId);

        $code6Char = $target->code . $activityCategory->code;

        $query = "Select Max(a.code) as code from activities as a
                  JOIN mtef_sections as ms on ms.id = a.mtef_section_id
                  JOIN mtefs as m on m.id = ms.mtef_id
                  where admin_hierarchy_id=" . $adminHierarchyId . "
                  and substr(a.code,9,1) =''
                  and a.code like '" . $code6Char . "%'
                  and financial_year_id=" . $financialYearId;
        $result = DB::select($query);

        $firstDigit = substr($result[0]->code, 6, 1);
        $lastDigit = substr($result[0]->code, 7, 1);
        $newFirstDigit = null;
        $newSecondDigit = null;
        $withinBound = true;

        $alpha = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
        $size = sizeof($alpha);
        $indexOfFirstDigit = array_search($firstDigit, $alpha);
        $indexOfLastDigit = array_search($lastDigit, $alpha);

        /**
         * First digit and last digit within bound increment last digit
         */
        if ($indexOfLastDigit < ($size - 1)) {
            $newFirstDigit = $alpha[$indexOfFirstDigit];
            $newSecondDigit = $alpha[$indexOfLastDigit + 1];
        }
        /**
         * First digit within bound and last digit = last element ; Increment first digit and set last digit = first element
         */
        else if ($indexOfLastDigit == ($size - 1)) {
            $newFirstDigit = $alpha[$indexOfFirstDigit + 1];
            $newSecondDigit = $alpha[0];
        } else {
            $withinBound = false;
        }

        if ($withinBound) {
            $newCode = $code6Char . $newFirstDigit . $newSecondDigit;
            return $newCode;
        } else {
            return $code6Char . '100';
        }

    }


    /**
     * @param $activityId
     * @param $activityPeriods
     */
    public static function saveActivityPeriods($activityId, $activityPeriods)
    {
        DB::table('activity_periods')->where('activity_id', $activityId)->delete();

        foreach ($activityPeriods as $value) {

            DB::table('activity_periods')->insert(["activity_id" => $activityId, "period_id" => $value['period_id']]);
        }
    }

    /**
     * @param $activityId
     * @param $activityReferences
     */
    public static function saveActivityReferences($activityId, $activityReferences,$cofog_id)
    {
        DB::table('activity_references')->where('activity_id', $activityId)->delete();

        foreach ($activityReferences as $value) {
            if ($value != null){
                DB::table('activity_references')->insert(
                array(
                    "activity_id" => $activityId,
                    "reference_id" => $value['id'],
                    "cofog_id" => $cofog_id
                )
            );
        }

        }
    }

    public static function disApprove($id, $transactionTypeId)
    {

        $inputIds = ActivityFacilityFundSourceInput::getIdsByActivity($id);
        $budgetExportQuery = BudgetExportAccount::whereIn('activity_facility_fund_source_input_id', $inputIds);

        try{
            DB::beginTransaction();
            $budgetExportAccountIds = $budgetExportQuery->pluck('id')->toArray();
            $budgetExportTransactionItemIds = BudgetExportTransactionItem::whereIn('budget_export_account_id', $budgetExportAccountIds)->pluck('id')->toArray();
            BudgetExportToFinancialSystem::whereIn('budget_export_transaction_item_id',$budgetExportTransactionItemIds)->forceDelete();
            BudgetExportTransactionItem::whereIn('id', $budgetExportTransactionItemIds)->forceDelete();
            DB::table('aggregate_facility_budget_export_accounts')->whereIn('budget_export_account_id', $budgetExportAccountIds)->delete();
            BudgetExportAccount::whereIn('id',$budgetExportAccountIds)->forceDelete();
            Activity::where('id', $id)->update(['is_final' => false]);
            DB::commit();
            return ['budgetExportAccounts' => 'Deleted'];

        }
        catch(\Exception $e){
            Log::error($e);
            DB::rollback();
            $budgetExportAccounts = $budgetExportQuery->get();
            $transaction = BudgetExportTransaction::create([
                'description' => 'DIS APPROVE ACTIVITY',
                'control_number' => time(),
                'created_by' => UserServices::getUser()->id,
                'budget_transaction_type_id' => $transactionTypeId
             ]);

            foreach($budgetExportAccounts as $a){
                BudgetExportTransactionItem::create([
                    'budget_export_transaction_id' => $transaction->id,
                    'budget_export_account_id' => $a->id,
                    'amount' => $a->amount,
                    'is_credit' =>false
                ]);
                $a->amount = 0;
                $a->save();
            }

            Activity::where('id', $id)->update(['is_final' => false]);
            return ['budgetExportAccounts' => $budgetExportAccounts, 'exception'=>$e];
        }

    }


    public static function approve($id, $transactionTypeId)
    {
        $activity = DB::table('activities as a')->
        join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')->
        join('mtefs as m', 'm.id', 'ms.mtef_id')->where('a.id', $id)->
        select('a.id', 'm.admin_hierarchy_id', 'm.financial_year_id','a.budget_type')->first();
        $inputIds = ActivityFacilityFundSourceInput::getIdsByActivity($id);
        if(sizeof($inputIds) == 0){
            return;
        }
        $inputIdsStr = implode(',', $inputIds);
        /**
         *  generate Budget export account  if doest exist
         */
        $newCreatedAccounts = BudgetExportAccountService::createBudgetAccounts($activity->admin_hierarchy_id, $activity->financial_year_id, $inputIdsStr,$activity->budget_type);
        $newAccountIds = [];
        foreach ($newCreatedAccounts as $new) {
            array_push($newAccountIds, $new->id);
        }


        /**
         * Create transaction
         */
        $transaction = BudgetExportTransaction::create([
            'description' => 'RE APPROVE ACTIVITY',
            'control_number' => $id,
            'created_by' => UserServices::getUser()->id,
            'budget_transaction_type_id' => $transactionTypeId
        ]);

        $budgetExportAccounts = BudgetExportAccount::where('activity_id', $id)->whereIn('activity_facility_fund_source_input_id', $inputIds)->get();

        foreach ($budgetExportAccounts as $b) {

            $amount = ActivityFacilityFundSourceInput::where('id', $b->activity_facility_fund_source_input_id)->sum(DB::raw('quantity*frequency*unit_price'));
            /**
             * Account previously existed only amount changed after disapproved
             */
            if (!in_array($b->id, $newAccountIds)) {
                $b->amount = $b->amount + $amount;
                $b->save();
            }
            BudgetExportAccount::createTransactionAndExport($b, $transaction->id, $amount, true, 'APPR');
        }

        $update = ['is_final' => true];
        $currentBudgetType = Activity::find($id)->budget_type;
        if($currentBudgetType == 'DISAPPROVED' || $currentBudgetType == 'PENDING'){
            $update = ['budget_type' => 'APPROVED','is_final' => true];
        }
        Activity::where('id', $id)->update($update);

        return ['newAccounts' => $newCreatedAccounts, 'processedAccounts' => $budgetExportAccounts];
        //Invoke send function here

    }

    public static function getReallocationActivities($budgetType, $financialYearId, $adminHierarchyId, $sectionId, $facilityId, $fundSourceId) {
        return DB::table('activities as a')
          ->join('mtef_sections as ms','ms.id','a.mtef_section_id')
          ->join('mtefs as m', 'm.id', 'ms.mtef_id')
          ->join('activity_facilities as af','af.activity_id','a.id')
          ->join('activity_facility_fund_sources as aff', 'aff.activity_facility_id','af.id')
          ->where('m.admin_hierarchy_id', $adminHierarchyId)
          ->where('m.financial_year_id', $financialYearId)
          ->where('a.budget_type', $budgetType)
          ->where('aff.fund_source_id',$fundSourceId)
          ->where('af.facility_id', $facilityId)
          ->where('ms.section_id', $sectionId)
          ->select('a.code','a.description','a.id','aff.id as activity_facility_fund_source_id')
          ->get();
    }

    public static function changeBudgetType($mtefId, $currentBudgetType, $nextBudgetType)
    {
        if($currentBudgetType == 'CURRENT'){
            $activityIds = DB::table('activities as a')->
                join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')->
                where('ms.mtef_id', $mtefId)->
                where('a.budget_type', $currentBudgetType)->
                pluck('a.id');
        }else{
            $activityIds = DB::table('activities as a')->
                join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')->
                join('activity_facilities as af','af.activity_id','a.id')->
                join('activity_facility_fund_sources as aff','aff.activity_facility_id','af.id')->
                join('activity_facility_fund_source_inputs as affi','affi.activity_facility_fund_source_id','aff.id')->
                where('ms.mtef_id', $mtefId)->
                where('a.budget_type', $currentBudgetType)->
                pluck('a.id');
        }

        Activity::whereIn('id', $activityIds)->
                where('budget_type', $currentBudgetType)->
                update(['budget_type' => $nextBudgetType, 'is_final' => true]);

        $inputIds = ActivityFacilityFundSourceInput::getIdsByActivityIds($activityIds);

        ActivityFacilityFundSourceInput::whereIn('id', $inputIds)->
                            where('budget_type', $currentBudgetType)->
                            update(['budget_type' => $nextBudgetType]);
    }

    public static function refreshActivityCode($activityId)
    {
        try {
            DB::beginTransaction();

            $a = DB::table('activities as a')
                ->join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')
                ->join('mtefs as m', 'm.id', 'ms.mtef_id')
                ->select('a.*', 'm.admin_hierarchy_id', 'm.financial_year_id')
                ->where('a.id', $activityId)
                ->first();

            $sixDigitCode = substr($a->code, 0, 6);

            $unique = DB::table('activities as a')
                ->join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')
                ->join('mtefs as m', 'm.id', 'ms.mtef_id')
                ->select('a.*', 'm.admin_hierarchy_id', 'm.financial_year_id')
                ->where('a.id', '<>', $activityId)
                ->where('a.code', $a->code)
                ->where('m.financial_year_id', $a->financial_year_id)
                ->where('m.admin_hierarchy_id', $a->admin_hierarchy_id)
                ->count();

            if ($unique == 0) {
                return ['updated' => $a];
            } else {
                $code = Activity::getFacilityAccountNextCode($a->financial_year_id, $a->budget_type, $a->admin_hierarchy_id, $a->mtef_annual_target_id, $a->activity_category_id);
            }
            if ($a->code != $code) {

                $transactionTypeId = ConfigurationSetting::getValueByKey('CHANGE_OF_ACTIVITY_CODE_TRANSACTION_TYPE');
                if ($transactionTypeId == null) {
                    return;
                }

                $budgetExportAccounts = BudgetExportAccount::where('activity_id', $activityId)->get();

                foreach ($budgetExportAccounts as $bea) {
                    $fromActivityCode = '-' . $a->code . '-';
                    $toActivityCode = '-' . $code . '-';
                    $toCOA = str_replace_first($fromActivityCode, $toActivityCode, $bea->chart_of_accounts);
                    $itemIds = BudgetExportTransactionItem::where('budget_export_account_id', $bea->id)->pluck('id');

                    $befsUpdate = ['chart_of_accounts' => $toCOA];
                    $beaUpdate = ['chart_of_accounts' => $toCOA];

                    if (!$a->is_facility_account) {

                        $befsUpdate = ['chart_of_accounts' => $toCOA, 'is_sent' => false];
                        $beaUpdate = ['chart_of_accounts' => $toCOA, 'is_sent' => false, 'is_delivered' => false];

                        $transaction = BudgetExportTransaction::create(['budget_transaction_type_id' => $transactionTypeId, 'description' => 'CHANGE OF DUPLICATE ACTIVITY CODE', 'is_sent' => false]);
                        BudgetExportTransactionItem::whereIn('id', $itemIds)->update(['budget_export_transaction_id' => $transaction->id]);
                    }
                    BudgetExportToFinancialSystem::whereIn('budget_export_transaction_item_id', $itemIds)
                        ->update($befsUpdate);

                    $bea->update($beaUpdate);
                    //unregister gl account
                    DB::table('company_gl_accounts')->where('budget_export_account_id', $bea->id)->delete();

                }
                $updated = Activity::find($activityId);
                $updated->code = $code;
                $updated->save();
                DB::commit();
                return ['updated' => $updated];
            } else {
                return response()->json(['errorMessage' => 'Found same code please try again', 'lastCode' => $code], 500);
            }
        } catch (\Exception $e) {
            Log::error($e);
            DB::rollback();
            return response()->json(['errorMessage' => $e->getMessage()], 500);
        }
    }

    public static function saveActivitySurplusCategories($activityId, $surplus_id)
    {

        $obj = new ActivitySurplusCategory();
        $obj->activity_id = $activityId;
        $obj->surplus_category_id = $surplus_id;
        $obj->save();
    }


    public function annualTarget()
    {
        return $this->belongsTo('App\Models\Planning\AnnualTarget', 'mtef_annual_target_id')->select('id', 'code', 'description', 'long_term_target_id', 'mtef_id', 'section_id');
    }

    public function budgetClass()
    {
        return $this->belongsTo('App\Models\Setup\BudgetClass')
            ->select(['id', 'name', 'code', 'parent_id']);
    }
    public function budget_class()
    {
        return $this->belongsTo('App\Models\Setup\BudgetClass')
            ->select(['id', 'name', 'code', 'parent_id']);
    }
    public function mtefSection()
    {
        return $this->belongsTo('App\Models\Planning\MtefSection');
    }

    public function facility()
    {
        return $this->belongsTo('App\Models\Setup\Facility', 'facility_id', 'id');
    }

    public function activityCategory()
    {
        return $this->belongsTo('App\Models\Setup\ActivityCategory')
            ->select(['id', 'name']);
    }
    public function activityTaskNature()
    {
        return $this->belongsTo('App\Models\Setup\ActivityTaskNature')
            ->select(['id', 'name']);
    }
    public function responsiblePerson()
    {
        return $this->belongsTo('App\Models\Setup\ResponsiblePerson')
            ->select(['id', 'first_name', 'last_name', 'mobile', 'title', 'email']);
    }
    public function project()
    {
        return $this->belongsTo('App\Models\Setup\Project')
            ->select(['id', 'name', 'code', 'description']);
    }

    /** activity project output */
    public function activityProjectOutputs()
    {
        return $this->hasMany('App\Models\Execution\ActivityProjectOutput');
    }

    public function activity_facilities()
    {
        return $this->hasMany('App\Models\Planning\ActivityFacility')->select('activity_facilities.id', 'activity_facilities.activity_id', 'activity_facilities.facility_id');
    }

    public function periods()
    {
        return $this->hasMany('App\Models\Planning\ActivityPeriod');
    }

    public function references()
    {
        return $this->belongsToMany('App\Models\Setup\Reference', 'activity_references')
            ->select(['reference_docs.id', 'reference_docs.code', 'reference_docs.name']);
    }

    public function budgetSubmissionLines()
    {
        return $this->hasMany('App\Models\Budgeting\BudgetSubmissionLine');
    }
    public function intervention()
    {
        return $this->belongsTo('App\Models\Setup\Intervention')
            ->select(['id', 'description', 'priority_area_id']);
    }
    public function sector_problem()
    {
        return $this->belongsTo('App\Models\Setup\SectorProblem')
            ->select(['id', 'description', 'priority_area_id']);
    }

    public function mtef_section()
    {
        return $this->belongsTo('App\Models\Planning\MtefSection');
    }

    public function activity_comments()
    {
        return $this->hasMany('App\Models\Planning\MtefSectionItemComment', 'item_id')
                            ->where('mtef_section_item_comments.item_type', 'ACTIVITY');
    }

    public static function searchByName($query)
    {
        if ($query == "") {
            return Activity::get();
        }
        return DB::table("activities")->where("name", "ilike", "%" . $query . "%");
    }
    public function status()
    {
        return $this->belongsTo('App\Models\Planning\ActivityStatus', 'activity_status_id', 'id');
    }

    public static function getTotalBudget($activityId)
    {
        $facilityIds = ActivityFacility::where('activity_id', $activityId)->pluck('id');
        $fundSourceIds = ActivityFacilityFundSource::whereIn('activity_facility_id', $facilityIds)->pluck('id');
        $total = DB::table('activity_facility_fund_source_inputs')->whereIn('activity_facility_fund_source_id', $fundSourceIds)->select(DB::raw('coalesce(SUM(quantity*frequency*unit_price),0) as total'))->get();

        return $total[0]->total;
    }

    public static function getWithInValidChainCode($total = true, $limit = 100)
    {
        $q = DB::table('activities as a')->join('mtef_annual_targets as t', 't.id', 'a.mtef_annual_target_id')->join('activity_categories as ac', 'ac.id', 'a.activity_category_id')->whereRaw('substr(a.code,1,5) <> t.code')->orWhereRaw('substr(a.code,6,1) <> ac.code');
        if ($total == true) {
            return $q->count();
        } else {
            return $q->select('a.*', 't.code as tcode', 'ac.code as ccode')->get($limit);
        }
    }

    /**
     * @return mixed
     * get All activities with invalid code number;
     */
    public static function getActivityWithInvalidCode($limit)
    {
        $activities = DB::table("activities as a")->join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')->join('mtefs as m', 'm.id', 'ms.mtef_id')->whereRaw("substr(a.code,9,1) <> ''")->orWhereRaw("substr(a.code,7,1) = ''")->orWhere(function ($invalidCouncilCode) {
            $invalidCouncilCode->where('a.is_facility_account', false);
            $invalidCouncilCode->where(function ($invOr) {
                $invOr->whereRaw("substr(a.code,7,1) > '9'");
                $invOr->orWhereRaw("substr(a.code,8,1) > '9'");
            });
        })->select('a.*', 'm.admin_hierarchy_id', 'm.financial_year_id')->orderBy('a.code', 'asc')->limit($limit)->get();
        return $activities;
    }

    /**
     * @return int
     * get Total activities with invalid code number;
     */
    public static function getTotalActivityWithInvalidCode()
    {

        $totalInvalid = DB::table("activities as a")->join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')->join('mtefs as m', 'm.id', 'ms.mtef_id')->whereRaw("substr(a.code,9,1) <> ''")->orWhereRaw("substr(a.code,7,1) = ''")->orWhere(function ($invalidCouncilCode) {
            $invalidCouncilCode->where('a.is_facility_account', false);
            $invalidCouncilCode->where(function ($invOr) {
                $invOr->whereRaw("substr(a.code,7,1) > '9'");
                $invOr->orWhereRaw("substr(a.code,8,1) > '9'");
            });
        })->count();
        return $totalInvalid;
    }


    public static function getActivitiesWithDuplicatedCode($total = true, $limit = 100)
    {
        $query = "from(
                      select m.financial_year_id,m.admin_hierarchy_id, a.code,count(*) as count from activities a
                      JOIN mtef_sections as ms on ms.id=a.mtef_section_id
                      JOIn mtefs as m on m.id = ms.mtef_id
                      where   a.deleted_at is NULL and a.code <> '00000000' and substr(a.code,9,1) = ''
                     GROUP BY m.financial_year_id, m.admin_hierarchy_id,a.code) as dup where dup.count > 1";
        if ($total == true) {
            $query = "Select count(*) as total " . $query;
            return DB::select($query)[0]->total;
        } else {
            $query = "Select * " . $query . "  limit " . $limit;
            return DB::select($query);
        }
    }


    public static function fixCouncilAccountCode($id, $code6Char, $adminHierarchyId, $financialYearId)
    {

        /**
         * Get valid facility account activity code to be replaced
         */
        $query = "Select a.id, Max(a.code) as code from activities as a
                    JOIN mtef_sections as ms on ms.id = a.mtef_section_id
                    JOIN mtefs as m on m.id = ms.mtef_id
                    where substr(a.code,7,2) <= '99' and
                          substr(a.code,7,2) <> '00'  and
                          substr(a.code,8,1) < '9'  and
                          substr(a.code,1,6) = '" . $code6Char . "' and
                          a.is_facility_account = true and
                          m.admin_hierarchy_id=" . $adminHierarchyId . " and
                          m.financial_year_id=" . $financialYearId . " group by a.id order by a.id desc limit 1";

        $activityToReplace = DB::select($query);


        if (sizeof($activityToReplace) == 0 || (sizeof($activityToReplace) > 0 && $activityToReplace[0]->code == null)) {
            $processed = 0;
            for ($x = 1; $x <= 99; $x++) {
                $number = $x;
                if ($x <= 9) {
                    $number = '0' . $number;
                }

                $count = DB::table('activities as a')->join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')->join('mtefs as m', 'm.id', 'ms.mtef_id')->where('a.code', $code6Char . $number)->where('m.admin_hierarchy_id', $adminHierarchyId)->where('m.financial_year_id', $financialYearId)->count();
                if ($count == 0) {
                    DB::table('activities')->where('id', $id)->update(["code" => $code6Char . $number]);
                    $processed++;
                    break;
                }
            }
            return ($processed > 0) ? true : false;
        } else {

            /**
             * valid facility account activity code found
             */
            if ($activityToReplace != null) {
                try {
                    DB::beginTransaction();
                    /**
                     * Update council account code with new valid code
                     */
                    DB::table("activities")->where("id", $id)->update(
                        array("code" => $activityToReplace[0]->code)
                    );
                    /**
                     * Update the facility account activity code with invalid code 100 to be reprocessed
                     */
                  //  $query = "update activities set code= (substring('" . $activityToReplace[0]->code . "',1,6) || '100') where id=" . $activityToReplace[0]->id;
                    Activity::fixFacilityCode($activityToReplace[0]->id, $code6Char, $adminHierarchyId, $financialYearId);
                    DB::raw($query);
                    DB::commit();
                    return true;
                } catch (\Exception $exception) {
                    Log::error($exception);
                    DB::rollback();
                    return false;
                }
            } else {
                return false;
            }
        }

    }

    public static function fixFacilityCode($id, $code6Char, $adminHierarchyId, $financialYearId)
    {
        $processed = false;
        $newCode = self::getFacilityAccountNextCode($code6Char, $adminHierarchyId, $financialYearId);
        $exist = DB::table('activities as a')->join('mtef_sections as ms', 'ms.id', 'a.mtef_section_id')->join('mtefs as m', 'm.id', 'ms.mtef_id')->where('a.code', $newCode)->where('m.admin_hierarchy_id', $adminHierarchyId)->where('m.financial_year_id', $financialYearId)->first();
        if (!$exist || $newCode == ($code6Char . '100')) {
            DB::table('activities')->where('id', $id)->update(array('code' => $newCode));
            $processed = true;
        }
        return $processed;
    }

    public static function cdr($financialYearId, $adminHierarchyId, $perPage = null) {
        $sql = "select distinct
                ahp.name as region,
                ah.name as council,
                sct.name as department,
                s.name as cost_center,
                fs.name as fund_source,
                pr.name as project_name,
                ac.description as activity,
                f.name as facility,
                case when l.id = 1 then 'LLG' else 'HLG'  end as lga_level,
                ft.name as facility_type,
                pt.name as project_type,
                ec.name as expenditure_category,
                po.name as project_output,
                ao.value as planned_value,
                ap.period,
                ai.achievement as actual_implementation,
                ai.achievement_value,
                ai.remarks,
                ac.budget_type,
                bi.implemented_quarter,
                round(cast(sum(affi.frequency * affi.unit_price * affi.quantity) as numeric), 2) as budget,
                bi.amount as expenditure
            from
                mtefs m
                join mtef_sections ms on ms.mtef_id = m.id
                join activities ac on ac.mtef_section_id = ms.id
                join activity_facilities af on af.activity_id = ac.id
                join activity_facility_fund_sources aff on aff.activity_facility_id = af.id
                join activity_facility_fund_source_inputs affi on affi.activity_facility_fund_source_id = aff.id
                join facilities f on f.id = af.facility_id
                join facility_types ft on ft.id = f.facility_type_id
                join lga_levels l on l.id = ft.lga_level_id
                join budget_classes bc on bc.id = ac.budget_class_id
                join budget_classes bcp on bcp.id = bc.parent_id
                join projects pr on pr.id = ac.project_id
                join admin_hierarchies ah on ah.id = m.admin_hierarchy_id
                join admin_hierarchies ahp on ah.parent_id = ahp.id
                join sections s on s.id = ms.section_id
                join sections dpt on dpt.id = s.parent_id
                join sections sct on sct.id = dpt.parent_id
                join fund_sources fs on fs.id = aff.fund_source_id
                join ( select ap.activity_id, string_agg(distinct p.name, ' ,') as period
                from
                activity_periods ap
                join periods p on p.id = ap.period_id
                group by ap.activity_id) ap on ap.activity_id = ac.id

                left join activity_project_outputs ao on (ao.activity_id = ac.id and ao.facility_id = f.id)
                left join project_outputs po on po.id = ao.project_output_id
                left join expenditure_categories ec on ec.id = po.expenditure_category_id
                left join project_types pt on pt.id = ec.project_type_id
                left join
                (select * from activity_implementations) as ai on ai.activity_id = ac.id
                left join budget_export_accounts ba on ba.activity_facility_fund_source_input_id = affi.id
                left join
                (select
                budget_export_account_id,
                string_agg(distinct per.name, ' ,') as implemented_quarter,
                round(cast(sum(bi.debit_amount) - sum(bi.credit_amount) as numeric), 2) as amount
                from
                budget_import_items bi
                left join periods per on per.id = bi.period_id
                group by budget_export_account_id
                ) bi on bi.budget_export_account_id = ba.id

            where
                m.financial_year_id = $financialYearId and  bcp.code = '200' and ah.id = $adminHierarchyId and
                ac.budget_type in ('APPROVED', 'CARRYOVER', 'SUPPLEMENTARY','CURRENT')
            GROUP BY
                ahp.name,
                ah.name,
                sct.name,
                s.name,
                fs.name,
                pr.name,
                ac.description,
                f.name,
                l.id = 1,
                ft.name,
                pt.name,
                ec.name,
                po.name,
                ao.value,
                ap.period,
                ai.achievement,
                ai.achievement_value,
                ai.remarks,
                ac.budget_type,
                bi.implemented_quarter,
                bi.amount
            ORDER BY
            ac.budget_type";

        $items = DB::select($sql);
        if (!is_null($perPage)) {
            return CustomPager::paginate($items, $perPage);
        } else {
            return DB::select($sql);
        }
    }

    //consolidated cdr report

    public static function consolidatedCdr($financialYearId,$region_id, $perPage = null) {
        $sql = "select distinct
                ahp.name as region,
                ah.name as council,
                sct.name as department,
                s.name as cost_center,
                fs.name as fund_source,
                pr.name as project_name,
                ac.description as activity,
                f.name as facility,
                case when l.id = 1 then 'LLG' else 'HLG'  end as lga_level,
                ft.name as facility_type,
                pt.name as project_type,
                ec.name as expenditure_category,
                po.name as project_output,
                ao.value as planned_value,
                ap.period,
                ai.achievement as actual_implementation,
                ai.achievement_value,
                ai.remarks,
                ac.budget_type,
                bi.implemented_quarter,
                round(cast(sum(affi.frequency * affi.unit_price * affi.quantity) as numeric), 2) as budget,
                bi.amount as expenditure
            from
                mtefs m
                join mtef_sections ms on ms.mtef_id = m.id
                join activities ac on ac.mtef_section_id = ms.id
                join activity_facilities af on af.activity_id = ac.id
                join activity_facility_fund_sources aff on aff.activity_facility_id = af.id
                join activity_facility_fund_source_inputs affi on affi.activity_facility_fund_source_id = aff.id
                join facilities f on f.id = af.facility_id
                join facility_types ft on ft.id = f.facility_type_id
                join lga_levels l on l.id = ft.lga_level_id
                join budget_classes bc on bc.id = ac.budget_class_id
                join budget_classes bcp on bcp.id = bc.parent_id
                join projects pr on pr.id = ac.project_id
                join admin_hierarchies ah on ah.id = m.admin_hierarchy_id
                join admin_hierarchies ahp on ah.parent_id = ahp.id
                join sections s on s.id = ms.section_id
                join sections dpt on dpt.id = s.parent_id
                join sections sct on sct.id = dpt.parent_id
                join fund_sources fs on fs.id = aff.fund_source_id
                join ( select ap.activity_id, string_agg(distinct p.name, ' ,') as period
                from
                activity_periods ap
                join periods p on p.id = ap.period_id
                group by ap.activity_id) ap on ap.activity_id = ac.id

                left join activity_project_outputs ao on (ao.activity_id = ac.id and ao.facility_id = f.id)
                left join project_outputs po on po.id = ao.project_output_id
                left join expenditure_categories ec on ec.id = po.expenditure_category_id
                left join project_types pt on pt.id = ec.project_type_id
                left join
                (select * from activity_implementations) as ai on ai.activity_id = ac.id
                left join budget_export_accounts ba on ba.activity_facility_fund_source_input_id = affi.id
                left join
                (select
                budget_export_account_id,
                string_agg(distinct per.name, ' ,') as implemented_quarter,
                round(cast(sum(bi.debit_amount) - sum(bi.credit_amount) as numeric), 2) as amount
                from
                budget_import_items bi
                left join periods per on per.id = bi.period_id
                group by budget_export_account_id
                ) bi on bi.budget_export_account_id = ba.id

            where
                m.financial_year_id = $financialYearId and ahp.id = $region_id and bcp.code = '200' and
                ac.budget_type in ('APPROVED', 'CARRYOVER', 'SUPPLEMENTARY','CURRENT')
            GROUP BY
                ahp.name,
                ah.name,
                sct.name,
                s.name,
                fs.name,
                pr.name,
                ac.description,
                f.name,
                l.id = 1,
                ft.name,
                pt.name,
                ec.name,
                po.name,
                ao.value,
                ap.period,
                ai.achievement,
                ai.achievement_value,
                ai.remarks,
                ac.budget_type,
                bi.implemented_quarter,
                bi.amount
            ORDER BY
            ac.budget_type";

        $items = DB::select($sql);
        if (!is_null($perPage)) {
            return CustomPager::paginate($items, $perPage);
        } else {
            return DB::select($sql);
        }
    }

}
