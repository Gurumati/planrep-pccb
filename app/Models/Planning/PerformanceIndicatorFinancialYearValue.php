<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerformanceIndicatorFinancialYearValue extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "performance_indicator_financial_year_values";

    public function financial_year() {
        return $this->belongsTo('App\Models\Setup\FinancialYear','financial_year_id','id');
    }

    public function data_source() {
        return $this->belongsTo('App\Models\Execution\DataSource','data_source_id','id');
    }

    public function performance_indicator_baseline_value() {
        return $this->belongsTo('App\Models\Planning\PerformanceIndicatorBaselineValue','performance_indicator_baseline_value_id','id');
    }
}
