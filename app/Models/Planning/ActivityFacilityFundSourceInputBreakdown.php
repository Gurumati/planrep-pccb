<?php

namespace App\Models\Planning;

use App\Http\Services\UserServices;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityFacilityFundSourceInputBreakdown extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $fillable = ['activity_facility_fund_source_input_id','item','unit_id','quantity','frequency','unit_price','created_by','update_by','budget_submission_definition_id'];

    protected $with = [
        'unit'
    ];

    public function unit() {
        return $this->belongsTo('App\Models\Setup\Unit')
            ->select(['id','name','symbol']);
    }

    public static function deleteAndTrack($breakDowns){
        foreach ($breakDowns as $b) {
            $b->forceDelete();
            track_activity($b, UserServices::getUser(), 'delete_input_break_downs');
        }
    }
}
