<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Projection extends Model {
    public $timestamps = true;
    use SoftDeletes;
    protected $table = "projections";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function admin_hierarchy() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchy');
    }

    public function financial_year() {
        return $this->belongsTo('App\Models\Setup\FinancialYear');
    }

    public function fund_source() {
        return $this->belongsTo('App\Models\Setup\FundSource');
    }

    public function gfs_code_projections() {
        return $this->hasMany('App\Models\Planning\GfsCodeProjection');
    }

    public function projection_periods() {
        return $this->hasMany('App\Models\Planning\ProjectionPeriod');
    }
}
