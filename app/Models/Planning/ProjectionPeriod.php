<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectionPeriod extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $table = "projection_periods";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function projection(){
        return $this->belongsTo('App\Models\Planning\Projection');
    }

    public function period(){
        return $this->belongsTo('App\Models\Setup\Period');
    }
}
