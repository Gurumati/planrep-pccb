<?php

namespace App\Models\Planning;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class BaselineStatisticValue extends Model
{

    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'baseline_statistic_values';

    public function baselineStatistic(){
        return $this->hasMany('App\Models\Setup\BaselineStatistic','baseline_static_id');
    }

}
