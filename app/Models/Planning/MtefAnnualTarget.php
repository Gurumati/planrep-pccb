<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class MtefAnnualTarget extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    

    public function longTermTarget() {
        return $this->belongsTo('App\Models\Planning\LongTermTarget');
    }
    public function mtef(){
        return $this->belongsTo('App\Models\Planning\Mtef','mtef_id');
    }

}
