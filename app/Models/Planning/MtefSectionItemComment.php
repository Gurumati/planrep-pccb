<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;

class MtefSectionItemComment extends Model
{
    public $timestamps=false;
    protected $fillable = ['item_type','item_id','comments','addressed','commented_by','address_comments','date_addressed','date_commented'];

}
