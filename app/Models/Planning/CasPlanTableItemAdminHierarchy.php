<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CasPlanTableItemAdminHierarchy extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'cas_plan_table_item_admin_hierarchies';

    public function adminHierarchy()
    {
        return $this->belongsTo('App\Models\Setup\AdminHierarchy');
    }

    public function casPlanTable()
    {
        return $this->belongsTo('App\Models\Setup\CasPlanTable');
    }

    public function casPlanTableItem()
    {
        return $this->belongsTo('App\Models\Setup\CasPlanTableItem');
    }
}
