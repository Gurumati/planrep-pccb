<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityPeriod extends Model
{
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "activity_periods";
    protected $fillable = ['activity_id', 'period_id', 'overall_achievement'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                'activity_id' => 'required|integer',
                'period_id' => 'required|integer',
                'overall_achievement' => 'string',
            ],
            $merge);
    }

    public function period()
    {
        return $this->belongsTo('App\Models\Setup\Period', 'period_id', 'id');
    }

    public function activity()
    {
        return $this->belongsTo('App\Models\Planning\Activity', 'activity_id', 'id');
    }
}
