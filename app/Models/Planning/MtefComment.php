<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MtefComment extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function mtef(){
        return $this->belongsTo('App\Models\Planning\Mtef','mtef_id');
    }

    public function fromDecisionLevel(){
        return $this->belongsTo('App\Models\Setup\DecisionLevel','from_decision_level');
    }

    public function mtefSectionComments(){
        return $this->hasMany('App\Models\Planning\MtefSectionComment');
    }
}
