<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanChain extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'plan_chains';

    public function longTermTargets() {
        return $this->hasMany('App\Models\Planning\LongTermTarget');
    }

    public function children() {
        return $this->hasMany('App\Models\Planning\LongTermTarget', 'plan_chain_id');
    }

    public function parent() {
        return $this->belongsTo('App\Models\Planning\PlanChain', 'parent_id');
    }

    //
}
