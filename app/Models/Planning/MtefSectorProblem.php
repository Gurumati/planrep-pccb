<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MtefSectorProblem extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $table = "mtef_sector_problems";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function mtef() {
        return $this->belongsTo('App\Models\Planning\Mtef');
    }

    public function sector_problem() {
        return $this->belongsTo('App\Models\Setup\SectorProblem');
    }
}
