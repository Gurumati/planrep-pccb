<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PerformanceIndicatorBaselineValue extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = "performance_indicator_baseline_values";

    public function start_financial_year() {
        return $this->belongsTo('App\Models\Setup\FinancialYear','start_financial_year_id','id');
    }

    public function end_financial_year() {
        return $this->belongsTo('App\Models\Setup\FinancialYear','end_financial_year_id','id');
    }

    public function performance_indicator() {
        return $this->belongsTo('App\Models\Setup\PerformanceIndicator','performance_indicator_id','id');
    }

    public function admin_hierarchy() {
        return $this->belongsTo('App\Models\Setup\AdminHierarchy','admin_hierarchy_id','id');
    }

    public function financial_year_values() {
        return $this->hasMany('App\Models\Planning\PerformanceIndicatorFinancialYearValue');
    }
    public function reference_document() {
        return $this->belongsTo('App\Models\Setup\ReferenceDocument');
    }
}
