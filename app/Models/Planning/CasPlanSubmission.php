<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use App\Models\Planning\MtefSection;
use Illuminate\Support\Facades\DB;

class CasPlanSubmission extends Model
{
    public $timestamps = true;
    protected $table = "cas_plan_submissions";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $fillable = ['admin_hierarchy_id', 'financial_year_id', 'cas_plan_id', 'user_id'];

    public static function lockMtef($admin_hierarchy, $financial_year, $status){
        //get health cost centers
        $result = DB::select("SELECT s.id FROM sections s join sections sp on sp.id = s.parent_id
                              join sections dpt on dpt.id = sp.parent_id WHERE dpt.id = 3 
                              and s.deleted_at is null ");
        foreach($result as $item){
            //update mtef sections
            $mtef_section = DB::table('mtef_sections as ms')
                            ->join('mtefs as m', 'ms.mtef_id', 'm.id')
                            ->where('m.admin_hierarchy_id', $admin_hierarchy)
                            ->where('m.financial_year_id', $financial_year)
                            ->where('ms.section_id', $item->id)
                            ->select('ms.id')
                            ->first();
            MtefSection::where('id', $mtef_section->id)
                        ->update(['is_locked'=>$status]);
        }
    }
}
