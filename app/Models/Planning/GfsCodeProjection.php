<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GfsCodeProjection extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $table = "gfs_code_projections";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    public function projection() {
        return $this->belongsTo('App\Models\Planning\Projection');
    }

    public function gfs_code() {
        return $this->belongsTo('App\Models\Setup\GfsCode');
    }

    public function section() {
        return $this->belongsTo('App\Models\Setup\Section');
    }
}
