<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class ReferenceDocument extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
    protected $table = 'reference_documents';

    public function referenceDocumentType(){
        return $this->belongsTo('App\Models\Setup\ReferenceDocumentType');
    }

    public function adminHierarchy(){
        return $this->belongsTo('App\DTOs\SetupDTOs\AdminHierarchyDTO')->select('id','name');
    }

    public function sFinancialYear(){
        return $this->belongsTo('App\Models\Setup\FinancialYear','start_financial_year')->select('id','name');
    }

    public function eFinancialYear(){
        return $this->belongsTo('App\Models\Setup\FinancialYear','end_financial_year')->select('id','name');
    }

    public function longTermTargets(){
        return $this->hasMany('App\Models\Planning\LongTermTarget');
    }

    public static function councilRefDoc($adminHierarchyId, $financialYear)
    {
        $startDate = $financialYear->start_date;
        $endDate = $financialYear->end_date;

        $refDocument = DB::table('reference_documents as r')
            ->join('financial_years as eF', 'eF.id', '=', 'r.end_financial_year')
            ->join('financial_years as sF', 'sF.id', '=', 'r.start_financial_year')
            ->where('r.admin_hierarchy_id', '=', $adminHierarchyId)
            ->where('sF.start_date', '<=', $startDate)
            ->where('eF.end_date', '>=', $endDate)
            ->select('r.*')->first();
        $docId = isset($refDocument) ? $refDocument->id : null;
        return $docId;
    }

}
