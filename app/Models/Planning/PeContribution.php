<?php

namespace App\Models\Planning;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PeContribution extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $table = "pe_contributions";
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";
}
