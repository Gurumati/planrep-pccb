<?php

namespace App\Models\Planning;

use App\Http\Services\UserServices;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityFacilityFundSourceInputForward extends Model
{
    public $timestamps = true;
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $dateFormat = "Y-m-d H:i:s";

    protected $fillable = ['quantity','frequency','financial_year_id','financial_year_order','created_by','update_by','activity_facility_fund_source_input_id'];

    public static function deleteAndTrack($inputForwards){
        foreach ($inputForwards as $f) {
            $f->forceDelete();
            track_activity($f, UserServices::getUser(), 'delete_input_forward');
        }
    }

}
