<?php

namespace App\Jobs;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use JasperPHP\JasperPHP;
use App\Models\Setup\CasPlanTable;
use App\Http\Services\UserServices;
use App\Models\Auth\User;
use Illuminate\Support\Facades\Config;

class GenerateSingleReportFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     *
     */
    public $cas_plan_id;
    public $admin_hierarchy_id;
    public $financial_year_id;

    public function __construct($cas_plan_id, $financial_year_id, $admin_hierarchy_id)
    {
        $this->cas_plan_id = $cas_plan_id;
        $this->financial_year_id = $financial_year_id;
        $this->admin_hierarchy_id = $admin_hierarchy_id;

    }

    public static function getOptions($params)
    {
        $connection = Config::get('database.connections.pgsql');
        $options = [
            'format' => ['pdf'],
            'locale' => 'en',
            'params' => $params,
            'db_connection' => [
                'driver'   => 'postgres',
                'host'     => $connection['host'],
                'port'     => $connection['port'],
                'database' => $connection['database'],
                'username' => $connection['username'],
                'password' => $connection['password'],
            ]
        ];

        return $options;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            //Get the current user
            $user_id = UserServices::getUser()->id;
            $user = User::where('id',$user_id)->get();

            $output = public_path().'/reports/'.$this->admin_hierarchy_id.'/'.$this->cas_plan_id;
            //create directory if it doesn't exist
            if (!file_exists($output)) {
                mkdir($output, 0777, true);
            }

            //1. Get the content of the comprehensive plan
            $query_string = "select cas_plan_contents.id as cas_plan_content_id, cas_plan_contents.cas_plan_id, cas_plan_contents.description, 
                         cas_plan_contents.cas_plan_content_id as cas_plan_content_parent, cas_plans.name as cas_plan_name, 
                         cas_plan_tables.description as cas_plan_table_name, cas_plan_tables.report_url as jasper_url, 
                         cas_plan_tables.parameters, cas_plan_tables.default_values, cas_plan_table_details.url as uploaded_file_url 
                         from cas_plan_contents 
                         left join cas_plans on cas_plan_contents.cas_plan_id = cas_plans.id 
                         left join cas_plan_tables on cas_plan_tables.cas_plan_content_id = cas_plan_contents.id 
                         left join cas_plan_table_details on cas_plan_table_details.cas_plan_table_id = cas_plan_tables.id 
                         and cas_plan_table_details.admin_hierarchy_id = :admin_hierarchy_id 
                         and cas_plan_table_details.financial_year_id = :financial_year_id
                         where cas_plan_id = :cas_plan_id 
                         and cas_plan_tables.deleted_at is NULL 
                         and cas_plan_table_details.deleted_at is NULL 
                         order by cas_plan_contents.sort_order asc";
            $cas_plan_content = DB::select(DB::raw($query_string), array(
                'admin_hierarchy_id' => $this->admin_hierarchy_id,
                'financial_year_id' => $this->financial_year_id,
                'cas_plan_id' => $this->cas_plan_id
            ));

            $array_of_urls = array();
            $pdf_url = "";

            if ($cas_plan_content == null){
                //send notification to user
                $message = "A required report has not been uploaded. Please crosscheck and try again";
                $url = url('#');
                $object_id = 0;
                $job = new sendNotifications($user,$object_id,$url,$message,"fa fa-book");
                dispatch($job->delay(10));
                exit();
            }

            foreach ($cas_plan_content as $content) {
                $uploaded_file_url = isset($content->uploaded_file_url) ? $content->uploaded_file_url : null;
                $jasper_url = isset($content->jasper_url) ? $content->jasper_url : null;

                if ($jasper_url != null) {
                    $report_url = app_path().'/Http/Controllers/Report/'.$jasper_url.".jrxml";
                    $pdf_url = app_path().'/Http/Controllers/Report/'.$jasper_url.".pdf";

                    //Compile the .jrxml file
                    $input = $report_url;
                    $jasper = new JasperPHP;
                    $jasper->compile($input)->execute();

                    //Check for the required parameters
                    $jasperParameters = new JasperPHP;
                    $outputJasperParams = $jasperParameters->listParameters($input)->execute();
                    $logo = app_path().'/Http/Controllers/Report/reports/mtef/images/logo.png';
                    $params = [];

                    foreach ($outputJasperParams as $parameter_description) {
                        if (str_contains($parameter_description, 'footer_text')) {
                            $params['footer_text'] = 'Planrep Version 1.0';
                        }

                        if (str_contains($parameter_description, 'logo')) {
                            $params['logo'] = $logo;
                        }

                        if (str_contains($parameter_description, 'sub_report')) {
                            $params['sub_report'] = app_path().'/Http/Controllers/Report/' . '/' . $jasper_url . '_comments.jasper';
                        }

                        if (str_contains($parameter_description, 'checked')) {
                            $params['checked'] = app_path().'/Http/Controllers/Report/reports/mtef/images/ok.png';
                        }

                        if (str_contains($parameter_description, 'cancelled')) {
                            $params['cancelled'] = app_path().'/Http/Controllers/Report/reports/mtef/images/cancel.png';
                        }

                        if (str_contains($parameter_description, 'financial_year_id')) {
                            $params['financial_year_id'] = $this->financial_year_id;
                        }

                        if (str_contains($parameter_description, 'admin_hierarchy_id')) {
                            $params['admin_hierarchy_id'] = $this->admin_hierarchy_id;
                        }

                        if (str_contains($parameter_description, 'sector_dept_id')) {
                            $content_id = $content->cas_plan_content_id;
                            $sector_dept_id = CasPlanTable::where('cas_plan_content_id', $content_id)->first();
                            $sector_dept_id = $sector_dept_id->default_values;
                            $params['sector_dept_id'] = $sector_dept_id['sector_dept_id'];
                        }

                        if (str_contains($parameter_description, 'budget_class_id')) {
                            $content_id = $content->cas_plan_content_id;
                            $budget_class_id = CasPlanTable::where('cas_plan_content_id', $content_id)->first();
                            $budget_class_id = $budget_class_id->default_values;
                            $params['budget_class_id'] = $budget_class_id['budget_class_id'];
                        }

                        if (str_contains($parameter_description, 'fund_source_id')) {
                            $content_id = $content->cas_plan_content_id;
                            $fund_source_id = CasPlanTable::where('cas_plan_content_id', $content_id)->first();
                            $fund_source_id = $fund_source_id->default_values;
                            if (is_array($fund_source_id['fund_source_id'])) {
                                $default = implode(',', $fund_source_id['fund_source_id']);
                                $params['fund_source_id'] = $default;
                            }
                        }

                        if (str_contains($parameter_description, 'region_id')) {
                            $params['region_id'] = $params['admin_hierarchy_id'];
                            unset($params['admin_hierarchy_id']);
                        }

                        $jasper = new JasperPHP;
                        $jasper->process($input, $output, $this->getOptions($params))->execute();
                    }
                } else {
                    //The Jasper file URL is null. Abort with a notification to the user
                    //send notification to user
                    $message = "An internal error has occurred when processing the report. Please contact technical support.";
                    $url = url('/notifications#!/');
                    $object_id = 0;
                    $job = new sendNotifications($user,$object_id,$url,$message,"fa fa-book");
                    dispatch($job->delay(10));
                    exit();
                }

                if ($uploaded_file_url != null) {
                    $report_url = $uploaded_file_url;
                } else {
                    //The required file to be uploaded is missing. Abort with a notification to the user
                    $message = "A_REQUIRED_REPORT_HAS_NOT_BEEN_UPLOADED_PLEASE_CROSS_CHECK_AND_TRY_AGAIN";
                    $url = url('/notifications#!/');
                    $object_id = 0;
                    $job = new sendNotifications($user,$object_id,$url,$message,"fa fa-book");
                    dispatch($job->delay(10));
                    exit();
                }

                //push the url of the report to the $array_of_urls
                array_push($array_of_urls, $report_url);
            }

            $outputName = __DIR__ . '/../../../../public/reports/' . $this->admin_hierarchy_id . '/' . $this->cas_plan_id . '/' . "merged.pdf";
            $cmd = "gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$outputName";
            //Add each pdf file to the end of the command
            foreach ($array_of_urls as $file) {
                $cmd .= $file . " ";
            }
            shell_exec($cmd);

            //send notification to user
            $message = "THE_REPORT_HAS_BEEN_GENERATED_CLICK_TO_DOWNLOAD";
            $url = url($outputName);
            $object_id = 0;
            $job = new sendNotifications($user,$object_id,$url,$message,"fa fa-book");
            dispatch($job->delay(10));

//            return $outputName;
        } catch (Exception $e){
            $this->exception = $e->getMessage();
            $this->failed_jobs();
        }
    }
}
