<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Redis;
use App\Notifications\NotifyUsers;



class NotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $users;
    public $object_id;
    public $url;
    public $message;
    public $icon;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($users,$id,$url,$message,$icon)
    {
        $this->users = $users;
        $this->object_id = $id;
        $this->url = $url;
        $this->message = $message;
        $this->icon = $icon;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Notification::send($this->users, new NotifyUsers($this->object_id,$this->url,$this->message,$this->icon));

        foreach ($this->users as $user) {
            $theData = array(
                'event' => $user->id,
                'message' => $this->message,
                'url' => $this->url,
                'icon' => $this->icon,
                'data' => $user->unreadNotifications
            );
            Redis::publish("user_notifications", json_encode($theData));
        }

    }
}
