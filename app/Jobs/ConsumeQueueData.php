<?php
/**
 * Created by PhpStorm.
 * User: milton
 * Date: 11/7/17
 * Time: 9:32 AM
 */

namespace App\Jobs;
use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use PhpAmqpLib\Message\AMQPMessage;

class ConsumeQueueData extends Job implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $message;

    public function __construct($message) {
        $this->message = $message;
    }

    /**
     *
     * process the feedback data from RabbitMQ and persisit the extracted
     * information into the database
     * @param $message FeedbackMessage from RabbitMQ
     * @return null
     */
    private function processFeedbackData($message) {

        $data       =  json_decode($message, true);
        $logMessage =  $data['LogMessage'];
        $logMessage =  (json_decode($logMessage, true));

        if($data['StatusCode'] == 1) {
            $data = [
                'message'        => $logMessage['message'],
                'gl_ccount'      => $logMessage['GLAccount'],
                'transaction_id' => substr($logMessage['LineUID'], 2) ,
                'amount'         => $logMessage['Amount'],
                'UID'            => $logMessage['LineUID'],
                'TrxCtrlNum'     => $logMessage['LineUID'],
                'status'         => $data['StatusCode'],
                'apply_date'     => $data['ApplyDate']
            ];
            $result = EpicorTransaction::create($data);
        }
    }

    /**
     *
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        $this->processFeedbackData($this->message);
    }
}