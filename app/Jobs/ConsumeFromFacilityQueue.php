<?php

namespace App\Jobs;
use App\Models\Setup\Facility;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

class ConsumeFromFacilityQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $body;

    public function __construct($body)
    {
        $this->body = $body;
    }

    private function verify($body)
    {
        $facilities = $body->facilities;
        foreach ($facilities as &$facility) {
            $code = $facility->facilityCode;
            $query = DB::table("facilities as f")
                ->join("admin_hierarchies as w","w.id","f.admin_hierarchy_id")
                ->join("admin_hierarchies as c","c.id","w.parent_id")
                ->join("admin_hierarchy_levels as l","l.id","c.admin_hierarchy_level_id")
                ->select('f.name as facility','c.code as councilCode')
                ->where("f.facility_code",$code)
                ->where("l.hierarchy_position",3);
            if($query->count() == 1){
                $item = $query->first();
                $facility->exist = 1;
                $facility->message = "Facility Exists";
                $facility->name = $item->facility;
                $facility->councilCode = $item->councilCode;
            } else if($query->count() > 1){
                $facility->exist = 1;
                $facility->message = "Duplicate facility";
            } else {
                $facility->exist = 0;
                $facility->message = "Facility Doest Not Exist";
            }
        }
        $this->send($facilities);
    }

    public function send($data){
        $message = new AMQPMessage(json_encode($data));
        $queue_name = "FACILITY_MATCH_REQUEST_QUEUE_FEEDBACK";
        $exchange = "FACILITY_MATCH_REQUEST_EXCHANGE_FEEDBACK";
        $exchange_type = "headers";
        $routing_key = "";
        $headers = new AMQPTable([
            'x-match' => 'all',
            'type' => 'FACILITIES'
        ]);
        $queue_bindings = new AMQPTable([
            'x-match' => 'all',
            'type' => 'FACILITIES'
        ]);
        sendToQueue($message,$queue_name,$exchange,$exchange_type,$routing_key,$headers,$queue_bindings);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->verify(json_decode($this->body));
    }
}
