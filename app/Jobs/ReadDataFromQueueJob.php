<?php

namespace App\Jobs;
use App\Custom\Tail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\DB;
use App\Jobs\SendDataToRabbitMQJob;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Execution\TransactionExchangeControl;
use App\Models\Execution\BudgetExportAccount;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\GfsCode;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\FundSource;
use App\Http\Controllers\Execution\MuseIntegrationController;
use App\Models\Execution\BudgetExportAccountGLAccountExportIssues;
use App\Models\Execution\BudgetExportResponse;

class ReadDataFromQueueJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;



    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $queue_name;
    public $options;
    public $action;

    public function __construct($queue_name, $options, $action)
    {
        $this->queue_name   = $queue_name;
        $this->options = $options;
        $this->action  = $action;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            switch ($this->action) {
                case 'readMuseImplementations':
                    ReadDataFromQueueJob::readMuseImplementations();
                break;

                case 'readMuseFeedback':
                    ReadDataFromQueueJob::readMuseFeedback();
                break;
                   default:
                break;
            }

        } catch (\Throwable $e) {
             $this->exception = $e->getMessage();
            $this->failed_jobs();
        }
    }


    public function readMuseImplementations() {
        $obj = new Tail();
        $obj->listenWithOptions($this->queue_name, $this->options, function ($message) {
            if (is_array(json_decode($message->body, true))) {
                $data = json_decode($message->body, true);
                $digitalSignature = $data["digitalSignature"];
                switch ($data["message"]["messageHeader"]["messageType"]) {

                    case 'ALLOCATIONS':
                        ReadDataFromQueueJob::readMuseAllocations($data);
                    break;

                    case 'REVENUES':
                        ReadDataFromQueueJob::readMuseRevenue($data);
                    break;

                    case 'EXPENDITURE':
                        ReadDataFromQueueJob::readMuseExpenditure($data);
                    break;
                       default:
                    break;
                }
            } else {
                print($message->body);
            }
        });
    }

    public function readMuseAllocations ($data) {

       // $message = $msg;
        $feedback_message = array();
        $transactions = $msg["message"]["messageDetails"];
        $messageType = $msg["message"]["messageHeader"]["messageType"];
        $sender = $msg["message"]["messageHeader"]["sender"];
        $receiver = $msg["message"]["messageHeader"]["receiver"];

        $valid_transactions = array();
        $res_message = '';

        foreach ($transactions as $transaction) {
            $message = $transaction;
            //removing some key on response Message
            unset($message['account']);
            unset($message['debit']);
            unset($message['trxDate']);
            unset($message['credit']);
            unset($message['applyDate']);
            unset($message['transactionID']);
            // End
             $je_date = $transaction["trxDate"];
             $control_id = $transaction["transactionID"];
             $account = $transaction["account"];
             $uid = $transaction["id"];
             $apply_date = $transaction["applyDate"];
             $debit_amount = $transaction["debit"];
             $credit_amount = $transaction["credit"];


             $timestamp = strtotime($apply_date);
             $check = DB::table('transaction_exchange_controls')
             ->where('transaction_type','=',$messageType)
             ->select('transaction_type')
             ->where( 'sender','=',$sender)
             ->where('receiver','=',$receiver)
             ->where('control_id','=',$control_id)
          //  ->where('apply_date',date("Y-m-d h:i:s", $timestamp))
             ->count();

            if ($check == 0) {
                //Check the current active period from the periods table. The period_group should not be annual
                $query = "select * from periods where '" . $je_date . "'::date between start_date and end_date and periods.period_group_id != 1";
                $get_period = DB::select(DB::raw($query));
                $period_id = isset($get_period[0]->id) ? $get_period[0]->id : null;


                //Check the current active financial year from the financial_years table.
                $query = "select * from financial_years where '" . $je_date . "'::date between start_date and end_date";
                $get_period = DB::select(DB::raw($query));
                $financial_year_id = isset($get_period[0]->id) ? $get_period[0]->id : null;

                $budget_export_account = BudgetExportAccount::where([
                    'chart_of_accounts' => $account,
                    'financial_year_id' => $financial_year_id,
                    'financial_system_code' => 'MUSE'
                ])->count();

                if ($budget_export_account > 0) {

                    $budget_export_account = BudgetExportAccount::where([
                        'chart_of_accounts' => $account,
                        'financial_year_id' => $financial_year_id,
                        'financial_system_code' => 'MUSE'
                    ])->first();
                    $budget_export_account_id = $budget_export_account->id;
                    $success = true;
                    DB::beginTransaction();
                    try {
                       //Insert into transaction_exchange_controls
                       TransactionExchangeControl::create([
                        'sender' => $sender,
                        'receiver' => $receiver,
                        'control_id' => $control_id,
                        'transaction_type' => $messageType,
                        'apply_date' => $apply_date
                    ]);
                    /**
                 * Insert the transaction
                 **/
                     $fund_allocated_from_financial_systems_id = DB::table('fund_allocated_from_financial_systems')
                     ->insertGetId([
                        'gl_account' => $account,
                        'description' => 'MUSE-ALLOCATIONS',
                        'BookID' => 'MUSE-ALLOCATIONS',
                        'JEDate' => $je_date,
                        'uid' => $uid,
                        'FiscalYear' => date('Y'),
                        'debit_amount' => $debit_amount,
                        'credit_amount' => $credit_amount,
                        'created_at' => \Carbon\Carbon::now()
                        ]);



                     //Insert an entry in the fund allocated transactions table
                     $fund_allocated_transaction_id = DB::table('fund_allocated_transactions')
                     ->insertGetId([
                        'reference_code' => microtime(true) * 1000,
                        'data_source_id' => null,
                        'import_method_id' => null,
                     ]);

                     DB::table('fund_allocated_transaction_items')
                        ->insert([
                            'fund_allocated_transaction_id' => $fund_allocated_transaction_id,
                            'budget_export_account_id' => $budget_export_account_id,
                            'amount' => null,
                            'credit_amount' => $credit_amount,
                            'debit_amount' => $debit_amount,
                            'allocation_date' => $je_date,
                            'period_id' => $period_id,
                            'fund_allocated_from_financial_system_id' => $fund_allocated_from_financial_systems_id,
                            'created_at' => \Carbon\Carbon::now()
                        ]);

                    } catch (Exception $e) {
                        DB::rollback();
                        $msg = (array)$message;
                        $msg['description'] = 'Failed to save one or more entries in Planrep -- ';
                        $msg['status'] = 'Failed';
                       // $feedback_message = array();
                        array_push($feedback_message, $msg);
                        $success = false;
                        Log::debug($e->getMessage());
                    }
                    if ($success) {
                        DB::commit();
                        /**
                         * Create a feedback message for the valid transaction and queue it into RabbitMQ for FFARS
                         */
                        $msg = (array)$message;
                        $msg['description'] = "The transaction has been generated successfully in PlanRep";
                        $msg['status'] = 'Success';
                       // $feedback_message = array();
                        array_push($feedback_message, $msg);
                    }
                    //array_push($valid_transactions, $transaction);
                } else {
                    ///transaction failure
                   $msg = (array)$message;
                  // $msg["Message"]["messageDetails"]
                   $msg['description'] = "The GL Accounts is not found on PlanrepOtr ";
                   $msg['status'] = 'Failed';
                  // $feedback_message = array();
                   array_push($feedback_message, $msg);
                }

            } else {
                ///dublicate
                $msg = (array)$message;
                $msg['description'] = "This is a duplicate of an existing transaction. A new entry has not been generated in PlanRep-OTR";
                $msg['status'] = 'Failed';
               // $feedback_message = array();
                array_push($feedback_message, $msg);
            }
        }
        $this->returnFeedback($feedback_message);

    }

    private function returnFeedback($failedChartOfAccount){
       // Log::info($feedback_message);
        $messageFeedback = array();
        $options = array(
            'time' => 60,
            'empty_queue_timeout' => 5,
            'exchange_type'=>'headers',
            'exchange' => 'PLANREP_OTR_TO_MUSE_EXCHANGE_FEEDBACK'
        );
        $queue_name = 'PLANREP_OTR_TO_MUSE_FEEDBACK';
        $messageHeader = array(
            "sender" => "PLANREPOTR",
            "receiver" => "MUSE",
            "messageType" => "RESPONSE",
            "created_at" => date('Y-m-d H:i:s')
        );
        $messageDetails = $failedChartOfAccount;

        $item = array(
            "messageHeader" => $messageHeader,
            "messageDetails" => $messageDetails
        );

      // $privateKey = openssl_pkey_get_private("file://" . base_path() . "/config/keys/private.pem");
      // $publicKey = openssl_pkey_get_public("file://" . base_path() . "/config/keys/public.pem");

       // Make a signature
      // openssl_sign($plainText, $signature, $privateKey, OPENSSL_ALGO_SHA256);
       //$signature = base64_encode($signature);
        $signature = "OOO";

        $payloadToMuse = array(
            'massege' => $item,
            'digitalSignature'=>$signature
        );

        array_push($messageFeedback, $payloadToMuse);
        dispatch(new PublishDataToRabbitMQJob($queue_name, $messageFeedback, $options));

    }

       public function readMuseRevenue ($msg) {

       }

       public function readMuseExpenditure ($data) {
         //
         LOG::INFO($data);
         ini_set('max_execution_time', 6000);

         $failedChartOfAccount = array();
         $admin_hierarchy_code = $data["message"]["messageSummary"]["company"];
         $messageType = $data["message"]["messageHeader"]["messageType"];
         $orgMsgId = $data["message"]["messageHeader"]["msgId"];
         $c = AdminHierarchy::where('code', $admin_hierarchy_code)->first();
         $admin_hierarchy_id = isset($c->id) ? $c->id : null;

         if($admin_hierarchy_id > 0){
             $applyDate = $data["message"]["messageSummary"]["applyDate"];
             $je_date = isset($applyDate) ? $applyDate : null;

             //Check the current active period from the periods table. The period_group should not be annual
             $query = "select * from periods where '".$je_date."'::date between start_date and end_date and periods.period_group_id != 8";
             $get_period = DB::select(DB::raw($query));
             $period_id = isset($get_period[0]->id) ? $get_period[0]->id: null;

             //Check the current active period from the periods table. The period_group should not be annual
             $financial_year_query = "select * from financial_years where '".$je_date."'::date between start_date and end_date";
             $get_financial_year = DB::select(DB::raw($financial_year_query));
             $financial_year_id  = isset($get_financial_year[0]->id) ? $get_financial_year[0]->id: null;
             $message = $data["message"]["messageDetails"];
             foreach($message as $expenditureDetail){

                //Get the budget export account id
                 $glAccount = $expenditureDetail["glAccount"];
                 $raw_query = "select e.id
                 from budget_export_accounts e
                 inner join activities a on a.id = e.activity_id
                 where a.budget_type in ('APPROVED','CARRYOVER','SUPPLEMENTARY')
                 and e.chart_of_accounts ='".$glAccount."'";
                 $budget_export_account = DB::select(DB::raw($raw_query));
                 $budget_export_account_id = isset($budget_export_account[0]->id) ? $budget_export_account[0]->id : null;

                 $gl_account = explode('-', $glAccount);
                 $gfs_code_from_muse = $gl_account[13];
                 $gfs_code = GfsCode::where('code','=',$gfs_code_from_muse)->first();
                 $gfs_code_id = isset($gfs_code->id) ? $gfs_code->id : null;

                 $gl_account = explode('-', $glAccount);
                 $budget_class_from_muse = $gl_account[6];
                 $budget_class = BudgetClass::where('code','=',$budget_class_from_muse)->first();
                 $budget_class_id = isset($budget_class->id) ? $budget_class->id : null;

                 $gl_account = explode('-', $glAccount);
                 $fund_source_from_muse = $gl_account[12];
                 $fund_source = FundSource::where('code','=',$fund_source_from_muse)->first();
                 $fund_source_id = isset($fund_source->id) ? $fund_source->id : null;
 
                 $debit_amount = (double)$expenditureDetail["drAmount"];
                 $credit_amount = (double)$expenditureDetail["crAmount"];
                 if (!$budget_export_account_id){
                     $responseTomuse = array(
                        "uid" => $expenditureDetail["uid"],
                        "glAccount" => $expenditureDetail["glAccount"],
                        "drAmount" => $expenditureDetail["drAmount"],
                        "crAmount" => $expenditureDetail["crAmount"],
                        "status" => "Fail",
                        "description" => "GL ACCOUNT NOT FOUND ON PLANREP OTR",
                     );
                     array_push($failedChartOfAccount,$responseTomuse);
                     $missing_account_data = array(
                        'uid' => $expenditureDetail["uid"],
                        'chart_of_accounts' => $expenditureDetail["glAccount"],
                        'financial_year_id' => $financial_year_id,
                        'admin_hierarchy_id' => $admin_hierarchy_id,
                        'debit_amount' => $debit_amount,
                        'credit_amount' => $credit_amount,
                        'JEDate' => $applyDate,
                        'reason' => 'GL_ACCOUNT_NOT_FOUND_IN_PLANREPOTR',
                        'transaction_type' => 'EXPENDITURE',
                        'financial_system_code' => 'MUSE',
                     );
                     $count = DB::table('budget_import_missing_accounts')->where('chart_of_accounts',$expenditureDetail['glAccount'])->where('uid',$expenditureDetail['uid'])->count();
                        if($count == 0){
                          DB::table('budget_import_missing_accounts')->insert([$missing_account_data]);
                        }
                 }else{
                     //The account has been found.
                    //Insert and get the id of the fund_allocated_from_financial_systems
                    $budget_type_raw_query = "select a.budget_type
                    from budget_export_accounts e
                    inner join activities a on e.activity_id = a.id
                    where e.chart_of_accounts ='".$glAccount."' and a.budget_type in ('CARRYOVER','APPROVED','SUPPLEMENTARY')";
                    $budget_type = DB::select(DB::raw($budget_type_raw_query));
                    $budget_type = isset($budget_type[0]->budget_type) ? $budget_type[0]->budget_type : null;

                    $responseTomuse = array(
                        "uid" => $expenditureDetail["uid"],
                        "glAccount" => $expenditureDetail["glAccount"],
                        "drAmount" => $expenditureDetail["drAmount"],
                        "crAmount" => $expenditureDetail["crAmount"],
                        "status" => "success",
                        "description" => "Data Accepted on Planrep OTR",
                     );
                     array_push($failedChartOfAccount,$responseTomuse);

                    $expenditure_data = array(
                        'budget_export_account_id' => $budget_export_account_id,
                        'period_id' => $period_id,
                        'cancelled' => false,
                        'created_at' => \Carbon\Carbon::now(),
                       // 'BookID' => $message->BookID,
                        'FiscalYear' => date("Y"),
                        'JEDate' => $applyDate,
                        'Account' => $glAccount,
                        'date_imported' => date("Y-m-d"),
                        'gfs_code_id' => $gfs_code_id,
                        'budget_class_id' => $budget_class_id,
                        'fund_source_id' => $fund_source_id,
                        'debit_amount' => $debit_amount,
                        'credit_amount' => $credit_amount,
                        'uid' => $expenditureDetail["uid"],
                        'msg_id' => $orgMsgId,
                        'budget_type' => $budget_type
                    );
                    $count = DB::table('budget_import_items')->where('Account',$glAccount)->where('uid',$expenditureDetail['uid'])->count();
                    if($count == 0){
                        DB::transaction(function() use ($expenditure_data) {
                            DB::table('budget_import_items')->insert([$expenditure_data]);
                        });
                    }
                 }
                
            }

            // $responseData =  app('App\Http\Controllers\Execution\MuseIntegrationController')->response($orgMsgId,"EXPENDITURE","PROCESSED","Response from Planrep",$failedChartOfAccount);
            // try {
            //     $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($responseData);
            //     //code...
            // } catch (\Throwable $th) {
            //     Log::info("ERROR");
            // }
            $this->returnFeedback($failedChartOfAccount);


         } 
        //  else {
        //    $responseData =  app('App\Http\Controllers\Execution\MuseIntegrationController')->response($orgMsgId,"EXPENDITURE","REJECTED","Institution Code ".$admin_hierarchy_code." Not found on Planrep",array());
        //     try {
        //         $response =  app('App\Http\Controllers\Execution\MuseIntegrationController')->sendRequestTomuse($responseData);
        //         //code...
        //     } catch (\Throwable $th) {
        //         Log::info("ERROR");
        //     }
            
        //  }

 
       }


   /****** READ FEEDBACK FROM MUSE */
    public function readMuseFeedback() {
        $obj = new Tail();
        $obj->listenWithOptions($this->queue_name, $this->options, function ($message) {
            if (is_array(json_decode($message->body, true))) {
                $msg = json_decode($message->body, true);
                switch ($msg["Message"]["messageHeader"]["MessageType"]) {

                    case 'ALLOCATIONS':
                        ReadDataFromQueueJob::readMuseAllocationsFeedback($msg);
                    break;

                    case 'REVENUES':
                        ReadDataFromQueueJob::readMuseRevenueFeedback($msg);
                    break;

                    case 'EXPENDITURE':
                        ReadDataFromQueueJob::readMuseExpenditureFeedback($msg);
                    break;
                       default:
                    break;
                }

            }else {
                print($message->body);
            }
        });
    }


    public function readMuseAllocationsFeedback () {
        //
    }

    public function readMuseRevenueFeedback () {
        //
    }

    public function readMuseExpenditureFeedback () {
        //
    }


     // Called when the job is failing...
     public function failed_jobs(){
        //insert data into a failed_jobs table
        $failed_at = Carbon::now()->toDateTimeString();

        DB::table('failed_jobs')->insert(
            [
                'connection' => 'default', 'queue' => $this->queue_name,
                'payload' => 'default', 'exception' => $this->exception, 'failed_at' => $failed_at
            ]
        );
    }
}
