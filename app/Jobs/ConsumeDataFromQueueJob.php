<?php

namespace App\Jobs;

use App\Custom\Tail;
use App\Models\Budgeting\AdminHierarchyCeiling;
use App\Models\Execution\AccountBalance;
use App\Http\Services\FinancialYearServices;
use App\Models\Execution\BudgetControlAccount;
use App\Models\Execution\BudgetExportAccount;
use App\Models\Execution\FfarsActualControlId;
use App\Models\Execution\BudgetExportAccountGLAccountExportIssues;
use App\Models\Execution\BudgetExportResponse;
use App\Models\Execution\BudgetExportToFinancialSystem;
use App\Models\Execution\CoaSegmentCompany;
use App\Models\Execution\FfarsActivity;
use App\Models\Execution\GlActualSummaryControlId;
use App\Models\Execution\RevenueExportAccount;
use App\Models\Planning\Activity;
use App\Models\Setup\AdminHierarchy;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Setup\Facility;
use App\Models\Setup\FundSource;
use App\Models\Setup\Section;
use App\Models\Setup\CashAccount;
use App\Models\Setup\FundType;
use App\Models\Setup\GfsCode;
use App\Models\Setup\Project;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use App\Jobs\SendDataToRabbitMQJob;
use App\Http\Services\Budgeting\BudgetExportAccountService;


class ConsumeDataFromQueueJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $queue;
    public $options;
    public $action;

    public function __construct($queue, $options, $action)
    {
        $this->queue   = $queue;
        $this->options = $options;
        $this->action  = $action;

       // $this->handle();
    }



    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle(){
        try {
             switch ($this->action) {
                // case 'getCoaSegmentFeedBack':
                //     ConsumeDataFromQueueJob::getCoaSegmentFeedBack();
                //     break;
                // case 'getGLAccountsQueueFeedBack':
                //     ConsumeDataFromQueueJob::getGLAccountsQueueFeedBack();
                //     break;
                // case 'getBudgetQueueFeedBack':
                //     ConsumeDataFromQueueJob::getBudgetQueueFeedBack();
                //     break;
                // case 'getGLActualSummary':
                //     ConsumeDataFromQueueJob::getGLActualSummary();
                //     break;
                // case 'getFFARSSegmentFeedback':
                //     ConsumeDataFromQueueJob::getFFARSSegmentFeedback();
                //     break;
                // case 'getFacilityExpenditure':
                //     ConsumeDataFromQueueJob::getFacilityExpenditure();
                //     break;
                // case 'getFacilityBudgetFeedBack':
                //     ConsumeDataFromQueueJob::getFacilityBudgetFeedBack();
                //     break;
                // case 'getFfarsActualExpenditure':
                //     ConsumeDataFromQueueJob::getFfarsActualExpenditure();
                //     break;
                // case 'getFfarsActualRevenue':
                //     ConsumeDataFromQueueJob::getFfarsActualRevenue();
                //     break;
                // case 'getFfarsAccountBalances':
                //     ConsumeDataFromQueueJob::getFfarsAccountBalances();
                //     break;
                // case 'readFfarsAllocations':
                //     ConsumeDataFromQueueJob::readFfarsAllocations();
                //     break;
               // case 'readMuseImplementations':
                  //  ConsumeDataFromQueueJob::readMuseImplementations();
                   // break;
                default:
                    break;
            }
        } catch (Exception $e) {
            $this->exception = $e->getMessage();
            $this->failed_jobs();
        }
    }




    public function getCoaSegmentFeedBack(){
        $obj = new Tail();
        $obj->listenWithOptions($this->queue, $this->options, function ($message) {
            if (is_array(json_decode($message->body, true))) {
                $msg = json_decode($message->body, true);
                $segment_company = $msg['id'];
                $message_status = isset($msg['StatusCode']) ? $msg['StatusCode'] : null;
                if ($segment_company > 0  && $message_status !== null ) {

                    $response = $msg['LogMessage'];
                    $status = ($message_status == 1)? true: false;
                    //If the messages status is failed, then put the response text in the coa_segment_company table
                    DB::table('coa_segment_company')
                        ->where('id', $segment_company)
                        ->update([
                            'is_delivered' => $status,
                            'response'=> $response,
                            'updated_at' =>\Carbon\Carbon::now()
                            ]);
                }
            } else {
                print($message->body);
            }

        });
    }


    public function getGLAccountsQueueFeedBack(){
        $obj = new Tail();
        $obj->listenWithOptions($this->queue, $this->options, function ($message) {
            if (is_array(json_decode($message->body, true))) {
                $msg = json_decode($message->body, true);
                $gl_account_id = $msg['id'];
                $message_status = isset($msg['StatusCode']) ? $msg['StatusCode'] : null;
                $admin_hierarchy_code = $msg['Company'];
                $gl_account = isset($msg['GLAccount'])? $msg['GLAccount'] : null ;

                if ($gl_account_id > 0  && $message_status !== null && $gl_account !== null) {
                    $gfs = explode('|', $gl_account);
                    //get transfer code
                    $transfer =  array(ConfigurationSetting::getValueByKey('HEALTH_TRANSFER'), ConfigurationSetting::getValueByKey('EDUCATION_TRANSFER'));
                    if(in_array($gfs[0], $transfer)){
                       DB::table('aggregate_facility_budgets')
                           ->where('id', $gl_account_id)
                           ->update(['gl_is_delivered'=> true, 'updated_at' =>\Carbon\Carbon::now()]);
                    }else {
                        //check if revenue or expenditure
                        $account_type = ConsumeDataFromQueueJob::getAccountType($gfs[0]);

                        if ($account_type == 'revenue') {
                            ConsumeDataFromQueueJob::updateRevenueGLAccount($gl_account_id, $msg, $message_status, $admin_hierarchy_code);
                        } else if ($account_type == 'expense') {
                            ConsumeDataFromQueueJob::updateBudgetGLAccount($gl_account_id, $msg, $message_status, $admin_hierarchy_code);
                        } else {
                            //update control account response
                            ConsumeDataFromQueueJob::updateControlGLAccount($gl_account_id, $msg, $message_status);
                        }
                    }
                }
            } else {
                //insert data: flag as invalid json
                $msg = $message->body;
                DB::insert([
                    'response_text' => $msg,
                    'is_cleared' => false,
                    'updated_at' =>\Carbon\Carbon::now()
                    ]);
            }
        });
    }

    public static function updateBudgetGLAccount($gl_account_id, $msg, $message_status, $company){
        //Get the GL_accounts from the budget_export_accounts table
        $gl = isset(BudgetExportAccount::where('id', $gl_account_id)->first()->id) ?: null;
        if ($gl == null) {
            //Do nothing
        } else {
            //Do an insert or update to the database
            DB::transaction(function () use ($gl_account_id, $msg, $message_status, $company) {
                $status = $message_status == 1? true: false;
                DB::table('budget_export_accounts')
                    ->where('id', $gl_account_id)
                    ->update([
                        'is_delivered' => $status,
                        'created_at' =>\Carbon\Carbon::now()
                        ]);

                //update gl_account feedback
                ConsumeDataFromQueueJob::updateGLAccount($gl_account_id, $status, $company,'expense');

                if ($message_status == 0) {
                    //Check if the transaction if an update or an insert to the budget_export_glaccount_export_issues table
                    $issue_exists = isset(BudgetExportAccountGLAccountExportIssues::where('budget_export_account_id', $gl_account_id)->first()->id) ?: null;

                    if ($issue_exists == null) {
                        //Do an insert
                        DB::table('budget_export_account_glaccount_export_issues')->insert(
                            [
                                'budget_export_account_id' => $gl_account_id,
                                'response_text' => json_encode($msg),
                                'created_at' =>\Carbon\Carbon::now()
                            ]
                        );
                    } else {
                        //Do an update
                        DB::table('budget_export_account_glaccount_export_issues')
                            ->where('budget_export_account_id', $gl_account_id)
                            ->update([
                                'response_text' => json_encode($msg),
                                'updated_at' =>\Carbon\Carbon::now()
                                ]);
                    }
                }
            });
        }
    }

    public static function updateRevenueGLAccount($gl_account_id, $msg, $message_status, $company){
        //Get the GL_accounts from the budget_export_accounts table
        $gl = isset(RevenueExportAccount::where('id', $gl_account_id)->first()->id) ?: null;
        if ($gl == null) {
            //Do nothing
        } else {
            //Do an insert or update to the database
            DB::transaction(function () use ($gl_account_id, $msg, $message_status, $company) {
                $status = $message_status == 1? true: false;
                DB::table('revenue_export_accounts')
                    ->where('id', $gl_account_id)
                    ->update([
                        'is_delivered' => $status,
                        'updated_at' =>\Carbon\Carbon::now()
                        ]);
                //update gl_account feedback
                ConsumeDataFromQueueJob::updateGLAccount($gl_account_id, $status, $company, 'revenue');

                if ($message_status == 0) {

                    //Check if the transaction if an update or an insert to the budget_export_glaccount_export_issues tabl
                    $issue_exists = isset(BudgetExportAccountGLAccountExportIssues::where('revenue_export_account_id', $gl_account_id)->first()->id) ?: null;

                    if ($issue_exists == null) {
                        //Do an insert
                        DB::table('budget_export_account_glaccount_export_issues')->insert(
                            [
                                'revenue_export_account_id' => $gl_account_id,
                                'response_text' => json_encode($msg),
                                'created_at' =>\Carbon\Carbon::now()
                            ]
                        );
                    } else {
                        //Do an update
                        DB::table('budget_export_account_glaccount_export_issues')
                            ->where('revenue_export_account_id', $gl_account_id)
                            ->update([
                                'response_text' => json_encode($msg),
                                'updated_at' =>\Carbon\Carbon::now()
                                ]);
                    }
                }
            });
        }
    }

    /** update gl account */
    public static function updateGLAccount($gl_account_id, $status, $company, $type){
        if($type == 'revenue'){
            DB::table('company_gl_accounts')
                ->where('revenue_export_account_id', $gl_account_id)
                ->where('company',$company)
                ->update([
                    'is_delivered' => $status,
                    'updated_at' =>\Carbon\Carbon::now()
                    ]);
        }else {
            DB::table('company_gl_accounts')
                ->where('budget_export_account_id', $gl_account_id)
                ->where('company',$company)
                ->update([
                    'is_delivered' => $status,
                    'updated_at' =>\Carbon\Carbon::now()
                    ]);
        }

    }

    /** update control account **/
    public static function updateControlGLAccount($gl_account_id, $msg, $message_status){
        //Get the GL_accounts from the budget_control_accounts table
        $gl = isset(DB::table('budget_control_accounts')->where('id', $gl_account_id)->first()->id) ?: null;

        if ($gl == null) {
            //Do nothing
        } else {
            //Do an insert or update to the database
            DB::transaction(function () use ($gl_account_id, $msg, $message_status) {
                $status = $message_status == 1? true: false;
                DB::table('budget_control_accounts')
                    ->where('id', $gl_account_id)
                    ->update([
                        'is_delivered' => $status,
                        'updated_at' =>\Carbon\Carbon::now()
                        ]);
            });
        }
    }

    public function getBudgetQueueFeedBack(){
        $obj = new Tail();
        $obj->listenWithOptions($this->queue, $this->options, function ($message) {
            if (is_array(json_decode($message->body, true))) {
                $msg = json_decode($message->body, true);
                $budget_export_id = isset($msg['id'])?$msg['id']:null;
                $delivery_status  = isset($msg['StatusCode'])?$msg['StatusCode']:null ;
                if ($budget_export_id > 0 && $delivery_status !== null) {
                    if($msg['Description'] == 'Transfer'){
                            DB::table('aggregate_facility_budgets')
                            ->where('id', $budget_export_id)
                            ->update(['is_delivered'=> $delivery_status, 'updated_at' =>\Carbon\Carbon::now()]);
                        }else {

                        //Check if the budget_export_id exists in the budget_export_to_financial_systems table
                        $befs = BudgetExportToFinancialSystem::where('id', $budget_export_id)->first();
                        $budget_export_exists = isset($befs->id) ? $befs->id : null;

                        if ($budget_export_exists !== null && $delivery_status !== 0) {
                            /**
                             * The message has been delivered successfully. Update the budget_export_to_financial_systems table
                             **/
                            $ber = BudgetExportResponse::where('budget_export_to_financial_system_id', $budget_export_id)->first();
                            $response_exists = isset($ber->id) ? $ber->id : null;
                            $response = $msg['LogMessage'];

                            //If it is a new record do an insert
                            if ($response_exists == null) {
                                DB::table('budget_export_responses')
                                    ->where('id', $response_exists)
                                    ->insert([
                                        'is_sent' => true,
                                        'is_delivered' => true,
                                        'response' => $response,
                                        'created_at' =>\Carbon\Carbon::now()
                                    ]);
                            } else {
                                //It is not a new record then do an update
                                DB::table('budget_export_responses')
                                    ->where('id', $response_exists)
                                    ->update([
                                        'is_sent' => true,
                                        'is_delivered' => true,
                                        'response' => $response,
                                        'updated_at' =>\Carbon\Carbon::now()
                                    ]);
                            }
                        } else {
                            /**
                             * The message has not been delivered successfully. Put the response into the budget_export_responses table
                             **/
                            $ber = BudgetExportResponse::where('budget_export_to_financial_system_id', $budget_export_id)->first();
                            $response_exists = isset($ber->id) ? $ber->id : null;
                            $response = $msg['LogMessage'];

                            //If it is a new record do an insert
                            if ($response_exists == null) {
                                DB::table('budget_export_responses')
                                    ->where('id', $response_exists)
                                    ->insert([
                                        'is_sent' => true,
                                        'is_delivered' => false,
                                        'response' => $response,
                                        'created_at' =>\Carbon\Carbon::now()
                                    ]);
                            } else {
                                //It is not a new record then do an update
                                $response = $ber->response.' '.$response;
                                DB::table('budget_export_responses')
                                    ->where('id', $response_exists)
                                    ->update([
                                        'is_sent' => true,
                                        'is_delivered' => false,
                                        'response' => $response,
                                        'updated_at' =>\Carbon\Carbon::now()
                                    ]);
                            }
                        }
                   }
                }
            } else {
                print($message->body);
            }
        });
    }

    /**
     * Read budget & revenue from FFARS
     */
    public function getFacilityExpenditure(){
        $obj = new Tail();
        $obj->listenWithOptions($this->queue, $this->options, function ($message) {
            try{
                $properties          = $message->get_properties();
                $application_headers = $properties['application_headers'];
                $application_type    = $application_headers->getNativeData()['type'];

                $message              = json_decode($message->body, true);
                $admin_hierarchy_code = $message['Company'];

                $c = AdminHierarchy::where('code', $admin_hierarchy_code)->first();
                $admin_hierarchy_id = isset($c->id) ? $c->id : null;
                $date_today = "'" . date("Y-m-d") . "'";
                $query = "select * from periods where date($date_today) between start_date and end_date";
                $get_period = DB::select(DB::raw($query));
                $period_id = isset($get_period[0]->id) ?: null;

                $feedback_queue = ConfigurationSetting::getValueByKey('ACTUAL_FROM_FFARS_QUEUE_FEEDBACK');

                $options = array(
                    'exchange' =>'ACTUALS_FROM_FFARS_EXCHANGE_FEEDBACK',
                    'headers'  => array('x-match'=>'all',  'type'=>$application_type)
                );

                if ($admin_hierarchy_id !== null and $period_id !== null) {
                    //check if facility exitst
                    $facility_code = $message['FacilityCode'];
                    $facility      = Facility::where('facility_code',$facility_code);

                    if($facility->count() > 0)
                    {
                        //process budget
                        foreach($message['Debit'] as $data){
                            //convert to object
                            $data = (object)$data;
                            $data->JEDate       = $message['JEDate'];
                            $data->BookID       = $message['BookID'];
                            $data->FacilityCode = $message['FacilityCode'];
                            $data->id           = $message['id'];

                            try{
                                $result = ConsumeDataFromQueueJob::process_as_main_book($data, $period_id,$admin_hierarchy_id);
                                if($result !== 'success')
                                {
                                    //return error
                                    $data->LogMessage = 'Transaction Failed, '.$result;
                                    $data->Status     = 0;
                                }else {
                                    $data->LogMessage = 'Transaction has been added successfully!';
                                    $data->Status     = 1;

                                }
                                //back to array
                                $data = (array)$data;
                                $data = array($data);
                                //send feedback message
                                dispatch(new SendDataToRabbitMQJob($feedback_queue,$data, $options,'budget'));

                            }catch(Exception $e){
                                print($e->getMessage());
                            }
                        }

                    }else {
                        $log_message = "Facility does not exist";
                        $message['LogMessage'] = $log_message;
                        $message['Status']     = 0;

                        //send feedback message
                        dispatch(new SendDataToRabbitMQJob($feedback_queue,$message, $options,'budget'));
                    }
                }else {
                    $log_message = "Company not registered in PlanRep";
                }
            }catch (Exception $e){
                print($e->getMessage());
            }
        });
    }

    /**read budget feedback */
    public  function getFacilityBudgetFeedBack(){
        $obj = new Tail();
        $obj->listenWithOptions($this->queue, $this->options, function ($message) {
            if (json_decode($message->body)) {
                $msg  = json_decode($message->body);
                $data = $msg->data;
                $journal_code  = isset($data->JournalCode) ? $data->JournalCode : null;
                $DebitAcc = isset($data->DebitAcc)? $data->DebitAcc: null;
                $msg_status = isset($msg->status)?$msg->status:null;
                if($msg_status == 1){
                    $delivery_status = true;
                }else {
                    $delivery_status = false;
                }
                $response = $msg->message;
                //get current financial year

                $apply_date = $data->ApplyDate;
                $apply_date = date("Y-m-d", strtotime($apply_date));
                $financial_year_query = "select * from financial_years where '".$apply_date."'::date between start_date and end_date";

                $get_financial_year = DB::select(DB::raw($financial_year_query));
                $financial_year_id  = isset($get_financial_year[0]->id) ? $get_financial_year[0]->id: null;

                if($DebitAcc != null){
                    foreach ($DebitAcc as $key => $value) {
                        $uid = isset($value->UID)?$value->UID:null;
                        try{
                            DB::table('budget_export_responses')
                            ->where('budget_export_to_financial_system_id', $uid)
                            ->update(['is_delivered'=>$delivery_status, 'response'=>$response]);
                        }catch(Exception $e){
                            Log::debug($e->getMessage());
                        }
                    }
                }

                //return responses for budget disapproval
                if($journal_code == 'BD'){
                    $credit_accounts = $data->CreditAcc;
                    $this->getActivities($credit_accounts, $delivery_status);
                }
            }
        });
    }

    /** get activities */
    public static function getActivities($credit_accounts, $delivery_status){
        foreach($credit_accounts as $item){
            $transaction = $item->UID;
            //get activity_id
            $result = DB::select("SELECT ba.activity_id from budget_export_to_financial_systems be
                                inner join budget_export_transaction_items bi on be.budget_export_transaction_item_id = bi.id
                                inner join budget_export_accounts ba on ba.id = bi.budget_export_account_id
                                where
                                be.id = $transaction");
            $activity_id = $result[0]->activity_id;
            Activity::confirmDisapproved($activity_id, $delivery_status);
        }
    }

    public function getGLActualSummary(){
        $obj = new Tail();
        $obj->listenWithOptions($this->queue, $this->options, function ($message) {
            $message = json_decode($message->body);
            $admin_hierarchy_code = $message->Company;

            $c = AdminHierarchy::where('code', $admin_hierarchy_code)->first();
            $admin_hierarchy_id = isset($c->id) ? $c->id : null;

        if($admin_hierarchy_id > 0){

            $je_date = isset($message->JEDate) ? $message->JEDate : null;

            //Check the current active period from the periods table. The period_group should not be annual
            $query = "select * from periods where '".$je_date."'::date between start_date and end_date and periods.period_group_id != 8";
            $get_period = DB::select(DB::raw($query));
            $period_id = isset($get_period[0]->id) ? $get_period[0]->id: null;

            //Check the current active period from the periods table. The period_group should not be annual
            $financial_year_query = "select * from financial_years where '".$je_date."'::date between start_date and end_date";
            $get_financial_year = DB::select(DB::raw($financial_year_query));
            $financial_year_id  = isset($get_financial_year[0]->id) ? $get_financial_year[0]->id: null;
            //check if gl actual summary exists
            $is_consumed = GlActualSummaryControlId::where('message_id', '=', $message->id)
                            ->where('company', '=', $message->Company)->first();
            $is_consumed = isset($is_consumed->id) ? true : false;
            //Check to make sure that the message are valid
            if (!$je_date || !$admin_hierarchy_code || $is_consumed){
                //Ignore the message
             } else {
            //Contains valid data. Process
            //Check the nature of the message by reading the transaction type
                try {

                    switch($message->TransactionType){
                        case 'AA':
                            //Process as allocation
                            ConsumeDataFromQueueJob::process_as_allocation($message, $period_id, $financial_year_id, $admin_hierarchy_id);
                        break;
                        case 'AAW':
                            //Process as withdrawal
                            ConsumeDataFromQueueJob::process_as_withdrawal($message, $period_id, $financial_year_id, $admin_hierarchy_id);

                            break;
                        case 'AAR':
                            //Process as reallocation
                            ConsumeDataFromQueueJob::process_as_reallocation($message, $period_id, $financial_year_id, $admin_hierarchy_id);
                        break;
                        case 'Revenue':
                            //Process as revenue
                            ConsumeDataFromQueueJob::process_as_revenue($message, $period_id, $financial_year_id, $admin_hierarchy_id);
                        break;
                        case 'Expenditure':
                            //Process as expenditure
                            ConsumeDataFromQueueJob::process_as_expenditure($message, $period_id,$financial_year_id,$admin_hierarchy_id);
                        break;
                        default:
                            Log::debug("Undefined book has been received ".$message->TransactionType);

                    }
                    //record as read
                    $this->createGLActualSummaryControl(intval($message->id), $message->Company);
                } catch (Exception $e){
                    Log::debug($e->getMessage());
                }
            }
            }else {
             }
        });
    }

    public static function process_as_allocation($message, $period_id, $financial_year_id, $admin_hierarchy_id){
        /***
         * Check if the message has been consumed previously to avoid duplicates
         */
        $message_id = isset($message->id) ? $message->id :null;
        $message_id = intval($message_id);

        $array = ['0'=>$message->DrAccounts, '1'=>$message->CrAccounts];
        foreach($array as $key=>$value){
            foreach($value as $account){
                //Get the budget export account id
                $budget_import_itm_coa = $account->Account;
                $raw_query = "select e.id from budget_export_accounts e inner join activities a on a.id = e.activity_id where a.budget_type in ('APPROVED','CARRYOVER','SUPPLEMENTARY') and e.chart_of_accounts ='".$budget_import_itm_coa."'";
                $budget_export_account = DB::select(DB::raw($raw_query));
                $budget_export_account_id = isset($budget_export_account[0]->id) ? $budget_export_account[0]->id : null;
                if($key == 0){
                    $debit_amount = (double)$account->Amount;
                    $credit_amount = 0.00;
                }else {
                    $credit_amount = (double)$account->Amount;
                    $debit_amount  = 0.00;
                }

                /**
                 * Check if the budget_export_account_id is found. If not found, enter an entry in the
                 * missing accounts table
                 */
                if (!$budget_export_account_id){
                    /**
                     * Enter an entry into the missing accounts table
                     */
                    $missing_account_data = array(
                        'uid' => $message->id,
                        'chart_of_accounts' => $account->Account,
                        'financial_year_id' => $financial_year_id,
                        'admin_hierarchy_id' => $admin_hierarchy_id,
                        'debit_amount' => $debit_amount,
                        'credit_amount' => $credit_amount,
                        'JEDate' => $message->JEDate,
                        'bookID' => $message->BookID,
                        'reason' => 'GL_ACCOUNT_NOT_FOUND_IN_PLANREP',
                        'transaction_type' => 'ALLOCATION',
                        'financial_system_code' => 'EPICOR',
                        'created_at' => \Carbon\Carbon::now()
                    );
                    DB::table('budget_import_missing_accounts')->insert([$missing_account_data]);
                } else {
                    //The account has been found.
                    //Insert and get the id of the fund_allocated_from_financial_systems

                    DB::transaction(function () use ($message, $account, $debit_amount, $credit_amount, $budget_export_account_id, $period_id) {
                        $fund_allocated_from_financial_systems_id = DB::table('fund_allocated_from_financial_systems')
                        ->insertGetId([
                            'gl_account' => $account->Account,
                            'description' => 'AllocAccts',
                            'BookID' => $message->BookID,
                            'JEDate' => $message->JEDate,
                            'FiscalYear' => $message->FiscalYear,
                            'debit_amount' => $debit_amount,
                            'credit_amount' => $credit_amount,
                            'created_at' =>\Carbon\Carbon::now()
                        ]);

                        //Insert an entry in the fund allocated transactions table
                        $fund_allocated_transaction_id = DB::table('fund_allocated_transactions')
                        ->insertGetId([
                            'reference_code' => microtime(true) * 1000,
                            'data_source_id' => null,
                            'import_method_id' => null,
                        ]);

                        DB::table('fund_allocated_transaction_items')
                        ->insertGetId([
                            'fund_allocated_transaction_id' => $fund_allocated_transaction_id,
                            'budget_export_account_id' => $budget_export_account_id,
                            'amount' => null,
                            'credit_amount' => $credit_amount,
                            'debit_amount' => $debit_amount,
                            'allocation_date' => $message->JEDate,
                            'fund_allocated_from_financial_system_id' => $fund_allocated_from_financial_systems_id,
                            'period_id' => $period_id,
                            'created_at' =>\Carbon\Carbon::now()
                        ]);
                    }, 2);
                }
            }
        }
    }

    public static function process_as_withdrawal($message, $period_id, $financial_year_id, $admin_hierarchy_id){
        /***
         * Check if the message has been consumed previously to avoid duplicates
         */
        $message_id = isset($message->id) ? $message->id :null;
        $message_id = intval($message_id);

        $array = ['0'=>$message->DrAccounts, '1'=>$message->CrAccounts];
        foreach($array as $key=>$value){
            foreach($value as $account){
                //Get the budget export account id
                $budget_import_itm_coa = $account->Account;
                $raw_query = "select e.id from budget_export_accounts e inner join activities a on a.id = e.activity_id where a.budget_type in ('APPROVED','CARRYOVER','SUPPLEMENTARY') and e.chart_of_accounts ='".$budget_import_itm_coa."'";
                $budget_export_account = DB::select(DB::raw($raw_query));
                $budget_export_account_id = isset($budget_export_account[0]->id) ? $budget_export_account[0]->id : null;

                if($key == 0){
                    $debit_amount = (double)$account->Amount;
                    $credit_amount  = 0.00;
                }else {
                    $credit_amount = (double)$account->Amount;
                    $debit_amount  = 0.00;
                }
                /**
                 * Check if the budget_export_account_id is found. If not found, enter an entry in the
                 * missing accounts table
                 */
                if (!$budget_export_account_id){
                    /**
                     * Enter an entry into the missing accounts table
                     */
                    $missing_account_data = array(
                        'uid' => $message->id,
                        'chart_of_accounts' => $account->Account,
                        'financial_year_id' => $financial_year_id,
                        'admin_hierarchy_id' => $admin_hierarchy_id,
                        'debit_amount' => $debit_amount,
                        'credit_amount' => $credit_amount,
                        'JEDate' => $message->JEDate,
                        'bookID' => $message->BookID,
                        'reason' => 'GL_ACCOUNT_NOT_FOUND_IN_PLANREP',
                        'transaction_type' => 'WITHDRAWAL',
                        'financial_system_code' => 'EPICOR',
                        'created_at' => \Carbon\Carbon::now()
                    );

                    DB::table('budget_import_missing_accounts')->insert([$missing_account_data]);
                } else {
                    //The account has been found.
                    //Insert and get the id of the fund_allocated_from_financial_systems

                    DB::transaction(function () use ($message, $account, $budget_export_account_id, $debit_amount, $credit_amount, $period_id) {
                        $fund_allocated_from_financial_systems_id = DB::table('fund_allocated_from_financial_systems')
                        ->insertGetId([
                            'gl_account' => $account->Account,
                            'description' => 'AllocAccts',
                            'BookID' => $message->BookID,
                            'JEDate' => $message->JEDate,
                            'FiscalYear' => $message->FiscalYear,
                            'debit_amount' => $debit_amount,
                            'credit_amount' => $credit_amount,
                            'created_at' =>\Carbon\Carbon::now()
                        ]);

                        //Insert an entry in the fund allocated transactions table
                        $fund_allocated_transaction_id = DB::table('fund_allocated_transactions')
                        ->insertGetId([
                            'reference_code' => microtime(true) * 1000,
                            'data_source_id' => null,
                            'import_method_id' => null,
                        ]);

                        DB::table('fund_allocated_transaction_items')
                        ->insert([
                            'fund_allocated_transaction_id' => $fund_allocated_transaction_id,
                            'budget_export_account_id' => $budget_export_account_id,
                            'amount' => null,
                            'credit_amount' => $credit_amount,
                            'debit_amount' => $debit_amount,
                            'allocation_date' => $message->JEDate,
                            'period_id' => $period_id,
                            'fund_allocated_from_financial_system_id' => $fund_allocated_from_financial_systems_id,
                            'created_at' =>\Carbon\Carbon::now()
                        ]);
                    });
                }
            }
        }
    }

    public static function process_as_expenditure($message, $period_id, $financial_year_id,$admin_hierarchy_id){
        /***
         * Check if the message has been consumed previously to avoid duplicates
         */
        $message_id = isset($message->id) ? $message->id :null;
        $message_id = intval($message_id);

        $array = ['0'=>$message->DrAccounts, '1'=>$message->CrAccounts];
        foreach($array as $key=>$value){
            foreach($value as $account){
                //Get the budget export account id
                $budget_import_itm_coa = $account->Account;
                $raw_query = "select e.id
                                from budget_export_accounts e
                                inner join activities a on a.id = e.activity_id
                                where a.budget_type in ('APPROVED','CARRYOVER','SUPPLEMENTARY')
                                and e.chart_of_accounts ='".$budget_import_itm_coa."'";
                $budget_export_account = DB::select(DB::raw($raw_query));
                $budget_export_account_id = isset($budget_export_account[0]->id) ? $budget_export_account[0]->id : null;

                $gl_account = $account->Account;
                $gl_account = explode('-', $gl_account);
                $gfs_code_from_epicor = $gl_account[9];
                $gfs_code   = GfsCode::where('code','=',$gfs_code_from_epicor)->first();
                $gfs_code_id = isset($gfs_code->id) ? $gfs_code->id : null;

                $gl_account = $account->Account;
                $gl_account = explode('-', $gl_account);
                $budget_class_from_epicor = $gl_account[3];
                $budget_class = BudgetClass::where('code','=',$budget_class_from_epicor)->first();
                $budget_class_id = isset($budget_class->id) ? $budget_class->id : null;

                $gl_account = $account->Account;
                $gl_account = explode('-', $gl_account);
                $fund_source_from_epicor = $gl_account[8];
                $fund_source = FundSource::where('code','=',$fund_source_from_epicor)->first();
                $fund_source_id = isset($fund_source->id) ? $fund_source->id : null;

                if($key == 1){
                    $credit_amount = (double)$account->Amount;
                    $debit_amount  = 0.00;
                }else {
                    $debit_amount = (double)$account->Amount;
                    $credit_amount  = 0.00;
                }
                /**
                 * Check if the budget_export_account_id is found. If not found, enter an entry in the
                 * missing accounts table
                 */
                if (!$budget_export_account_id){
                    /**
                     * Enter an entry into the missing accounts table
                     */

                    $missing_account_data = array(
                        'uid' => $message->id,
                        'chart_of_accounts' => $account->Account,
                        'financial_year_id' => $financial_year_id,
                        'admin_hierarchy_id' => $admin_hierarchy_id,
                        'debit_amount' => $debit_amount,
                        'credit_amount' => $credit_amount,
                        'JEDate' => $message->JEDate,
                        'bookID' => $message->BookID,
                        'reason' => 'GL_ACCOUNT_NOT_FOUND_IN_PLANREP',
                        'transaction_type' => 'EXPENDITURE',
                        'financial_system_code' => 'EPICOR',
                        'created_at' => \Carbon\Carbon::now()
                    );
                    DB::table('budget_import_missing_accounts')->insert([$missing_account_data]);
                } else {
                    //The account has been found.
                    //Insert and get the id of the fund_allocated_from_financial_systems
                    $budget_type_raw_query = "select a.budget_type
                                                from budget_export_accounts e
                                                inner join activities a on e.activity_id = a.id
                                                where e.chart_of_accounts ='".$budget_import_itm_coa."' and a.budget_type in ('CARRYOVER','APPROVED','SUPPLEMENTARY')";
                    $budget_type = DB::select(DB::raw($budget_type_raw_query));
                    $budget_type = isset($budget_type[0]->budget_type) ? $budget_type[0]->budget_type : null;

                    $expenditure_data = array(
                        'budget_export_account_id' => $budget_export_account_id,
                        'period_id' => $period_id,
                        'cancelled' => false,
                        'created_at' => \Carbon\Carbon::now(),
                        'BookID' => $message->BookID,
                        'FiscalYear' => $message->FiscalYear,
                        'JEDate' => $message->JEDate,
                        'Account' => $account->Account,
                        'date_imported' => date("Y-m-d"),
                        'gfs_code_id' => $gfs_code_id,
                        'budget_class_id' => $budget_class_id,
                        'fund_source_id' => $fund_source_id,
                        'debit_amount' => $debit_amount,
                        'credit_amount' => $credit_amount,
                        'budget_type' => $budget_type
                    );

                    DB::transaction(function() use ($expenditure_data) {
                        DB::table('budget_import_items')->insert([$expenditure_data]);
                    });

                }
            }
        }
    }

    public static function process_as_revenue($message, $period_id,$financial_year_id, $admin_hierarchy_id){
        /***
         * Check if the message has been consumed previously to avoid duplicates
         */
        $message_id = isset($message->id) ? $message->id :null;
        $message_id = intval($message_id);

        $array = ['0'=>$message->DrAccounts, '1'=>$message->CrAccounts];
        foreach($array as $key=>$value){
            foreach($value as $account){
                //Get the budget export account id
                $budget_import_itm_coa = $account->Account;
                $raw_query = "select e.id, e.admin_hierarchy_id, e.section_id, e.financial_year_id, e.gfs_code_id,
                                e.admin_hierarchy_ceiling_id
                                from revenue_export_accounts e where chart_of_accounts ='".$budget_import_itm_coa."'
                                and e.financial_year_id = $financial_year_id";
                $revenue_export_account    = DB::select(DB::raw($raw_query));
                $revenue_export_account_id = isset($revenue_export_account[0]->id) ? $revenue_export_account[0]->id : null;
                $admin_hierarchy_ceiling_id = isset($revenue_export_account[0]->admin_hierarchy_ceiling_id) ?
                                                $revenue_export_account[0]->admin_hierarchy_ceiling_id : null;

                $section_id = isset($revenue_export_account[0]->section_id) ? $revenue_export_account[0]->section_id : null;
                $gl_account = $account->Account;
                $gl_account = explode('-', $gl_account);
                $gfs_code_from_epicor = $gl_account[9];
                $gfs_code   = GfsCode::where('code','=',$gfs_code_from_epicor)->first();
                $gfs_code_id = isset($gfs_code->id) ? $gfs_code->id : null;

                $gl_account = $account->Account;
                $gl_account = explode('-', $gl_account);
                $budget_class_from_epicor = $gl_account[3];
                $budget_class = BudgetClass::where('code','=',$budget_class_from_epicor)->first();
                $budget_class_id = isset($budget_class->id) ? $budget_class->id : null;

                $gl_account = $account->Account;
                $gl_account = explode('-', $gl_account);
                $fund_source_from_epicor = $gl_account[8];
                $fund_source = FundSource::where('code','=',$fund_source_from_epicor)->first();
                $fund_source_id = isset($fund_source->id) ? $fund_source->id : null;

                if($key == 0){
                        $debit_amount = (double)$account->Amount;
                        $credit_amount = 0.00;
                }else {
                        $credit_amount = (double)$account->Amount;
                        $debit_amount  = 0.00;
                }
                /**
                 * Check if the revenue_export_account_id is found. If not found, enter an entry in the
                 * missing accounts table
                 */
                if (!$revenue_export_account_id){
                    /**
                     * Enter an entry into the missing accounts table
                     */
                    $missing_account_data = array(
                        'uid' => $message->id,
                        'chart_of_accounts' => $account->Account,
                        'financial_year_id' => $financial_year_id,
                        'admin_hierarchy_id' => $admin_hierarchy_id,
                        'debit_amount' =>  $debit_amount,
                        'credit_amount' => $credit_amount,
                        'JEDate' => $message->JEDate,
                        'bookID' => $message->BookID,
                        'reason' => 'GL_ACCOUNT_NOT_FOUND_IN_PLANREP',
                        'transaction_type' => 'REVENUE',
                        'financial_system_code' => 'EPICOR',
                        'created_at' => \Carbon\Carbon::now()
                    );

                    DB::table('received_fund_missing_accounts')->insert([$missing_account_data]);
                } else {
                    //The account has been found.
                    $revenue_data = array(
                        'admin_hierarchy_ceiling_id' => $admin_hierarchy_ceiling_id,
                        'date_received' => $message->JEDate,
                        'reference_code' =>microtime(true) * 1000,
                        'period_id' => $period_id,
                        'fund_source_id' => $fund_source_id,
                        'gfs_code_id' => $gfs_code_id,
                        'revenue_export_account_id' => $revenue_export_account_id,
                        'debit_amount' => $debit_amount,
                        'credit_amount' => $credit_amount,
                        'fiscal_year' => $message->FiscalYear,
                        'account' => $account->Account,
                        'JEDate' => $message->JEDate
                    );

                try {
                    DB::transaction(function() use ($revenue_data) {
                        DB::table('received_fund_items')->insert([$revenue_data]);
                    });
                } catch (Exception $e){
                    Log::debug($e->getMessage());
                }
                }
            }
        }

    }

    /** save control ids */
    public function createGLActualSummaryControl($id, $company){
        $control_ids_data = array(
            'message_id' => $id,
            'company' => $company,
            'created_at' => \Carbon\Carbon::now()
        );
        DB::table('gl_actual_summary_control_ids')->insert([$control_ids_data]);
    }

    public static function process_as_reallocation($message, $period_id, $financial_year_id, $admin_hierarchy_id){
        /***
         * Check if the message has been consumed previously to avoid duplicates
         */
        $message_id = isset($message->id) ? $message->id :null;
        $message_id = intval($message_id);
        $accounts_to_credit = $message->CrAccounts;
        $accounts_to_debit = $message->DrAccounts;

        foreach($accounts_to_credit as $account){
            //Get the budget export account id
            $budget_import_itm_coa = $account->Account;
            $raw_query = "select e.id from budget_export_accounts e inner join activities a on a.id = e.activity_id where a.budget_type in ('APPROVED','CARRYOVER','SUPPLEMENTARY') and e.chart_of_accounts ='".$budget_import_itm_coa."'";
            $budget_export_account = DB::select(DB::raw($raw_query));
            $budget_export_account_id = isset($budget_export_account[0]->id) ? $budget_export_account[0]->id : null;

            /**
             * Check if the budget_export_account_id is found. If not found, enter an entry in the
             * missing accounts table
             */
            if (!$budget_export_account_id){
                /**
                 * Enter an entry into the missing accounts table
                 */
                $missing_account_data = array(
                    'uid' => $message->id,
                    'chart_of_accounts' => $account->Account,
                    'financial_year_id' => $financial_year_id,
                    'admin_hierarchy_id' => $admin_hierarchy_id,
                    'debit_amount' => 0.00,
                    'credit_amount' => (double)$account->Amount,
                    'JEDate' => $message->JEDate,
                    'bookID' => $message->BookID,
                    'reason' => 'GL_ACCOUNT_NOT_FOUND_IN_PLANREP',
                    'transaction_type' => 'WITHDRAWAL',
                    'financial_system_code' => 'EPICOR',
                    'created_at' => \Carbon\Carbon::now()
                );

                DB::table('budget_import_missing_accounts')->insert([$missing_account_data]);
            } else {
                //The account has been found.
                //Insert and get the id of the fund_allocated_from_financial_systems

                DB::transaction(function () use ($message, $account, $budget_export_account_id, $period_id) {
                    $fund_allocated_from_financial_systems_id = DB::table('fund_allocated_from_financial_systems')
                    ->insertGetId([
                        'gl_account' => $account->Account,
                        'description' => 'AllocAccts',
                        'BookID' => $message->BookID,
                        'JEDate' => $message->JEDate,
                        'FiscalYear' => $message->FiscalYear,
                        'debit_amount' => 0.00,
                        'credit_amount' => (double)$account->Amount,
                        'created_at' =>\Carbon\Carbon::now()
                    ]);

                    //Insert an entry in the fund allocated transactions table
                    $fund_allocated_transaction_id = DB::table('fund_allocated_transactions')
                    ->insertGetId([
                        'reference_code' => microtime(true) * 1000,
                        'data_source_id' => null,
                        'import_method_id' => null,
                    ]);

                    DB::table('fund_allocated_transaction_items')
                    ->insert([
                        'fund_allocated_transaction_id' => $fund_allocated_transaction_id,
                        'budget_export_account_id' => $budget_export_account_id,
                        'amount' => null,
                        'credit_amount' => (double)$account->Amount,
                        'debit_amount' => 0.00,
                        'allocation_date' => $message->JEDate,
                        'period_id' => $period_id,
                        'fund_allocated_from_financial_system_id' => $fund_allocated_from_financial_systems_id,
                        'created_at' =>\Carbon\Carbon::now()
                    ]);
                }, 2);
            }
        }

        foreach($accounts_to_debit as $account){
            //Get the budget export account id
            $budget_import_itm_coa = $account->Account;
            $raw_query = "select e.id from budget_export_accounts e inner join activities a on a.id = e.activity_id where a.budget_type in ('APPROVED','CARRYOVER','SUPPLEMENTARY') and e.chart_of_accounts ='".$budget_import_itm_coa."'";
            $budget_export_account = DB::select(DB::raw($raw_query));
            $budget_export_account_id = isset($budget_export_account[0]->id) ? $budget_export_account[0]->id : null;

            /**
             * Check if the budget_export_account_id is found. If not found, enter an entry in the
             * missing accounts table
             */
            if (!$budget_export_account_id){
                /**
                 * Enter an entry into the missing accounts table
                 */
                $missing_account_data = array(
                    'uid' => $message->id,
                    'chart_of_accounts' => $account->Account,
                    'financial_year_id' => $financial_year_id,
                    'admin_hierarchy_id' => $admin_hierarchy_id,
                    'debit_amount' => (double)$account->Amount,
                    'credit_amount' => 0.00,
                    'JEDate' => $message->JEDate,
                    'bookID' => $message->BookID,
                    'reason' => 'GL_ACCOUNT_NOT_FOUND_IN_PLANREP',
                    'transaction_type' => 'WITHDRAWAL',
                    'financial_system_code' => 'EPICOR',
                    'created_at' => \Carbon\Carbon::now()
                );

                DB::table('received_fund_missing_accounts')->insert([$missing_account_data]);
            } else {
                //The account has been found.
                //Insert and get the id of the fund_allocated_from_financial_systems

                DB::transaction(function () use ($message, $account, $budget_export_account_id, $period_id) {
                    $fund_allocated_from_financial_systems_id = DB::table('fund_allocated_from_financial_systems')
                    ->insertGetId([
                        'gl_account' => $account->Account,
                        'description' => 'AllocAccts',
                        'BookID' => $message->BookID,
                        'JEDate' => $message->JEDate,
                        'FiscalYear' => $message->FiscalYear,
                        'debit_amount' => (double)$account->Amount,
                        'credit_amount' => 0.00,
                        'created_at' =>\Carbon\Carbon::now()
                    ]);

                    //Insert an entry in the fund allocated transactions table
                    $fund_allocated_transaction_id = DB::table('fund_allocated_transactions')
                    ->insertGetId([
                        'reference_code' => microtime(true) * 1000,
                        'data_source_id' => null,
                        'import_method_id' => null,
                    ]);

                    DB::table('fund_allocated_transaction_items')
                    ->insert([
                        'fund_allocated_transaction_id' => $fund_allocated_transaction_id,
                        'budget_export_account_id' => $budget_export_account_id,
                        'amount' => null,
                        'credit_amount' => 0.00,
                        'debit_amount' => (double)$account->Amount,
                        'allocation_date' => $message->JEDate,
                        'period_id' => $period_id,
                        'fund_allocated_from_financial_system_id' => $fund_allocated_from_financial_systems_id,
                        'created_at' =>\Carbon\Carbon::now()
                    ]);
                }, 2);
            }
        }
    }


    // Called when the job is failing...
    public function failed_jobs(){
        //insert data into a failed_jobs table
        $failed_at = Carbon::now()->toDateTimeString();

        DB::table('failed_jobs')->insert(
            [
                'connection' => 'default', 'queue' => $this->queue,
                'payload' => 'default', 'exception' => $this->exception, 'failed_at' => $failed_at
            ]
        );
    }

    public function updateFacilityStatus($body){
        $facility_id = $body->data->id;
        $status = $body->status;
        $feedback = $body->message;
        $f = Facility::find($facility_id);
        if(!is_null($f)){
            if ($status == 1) {
                //success
                $f->is_delivered_to_ffars = true;
                $f->ffars_response = $feedback;
                $f->save();
            } else {
                //failed
                $f->ffars_response = $feedback;
                $f->save();
            }
        }
    }

    public function updateSubBudgetClassStatus($body){
        $id = $body->data->id;
        $status = $body->status;
        $feedback = $body->message;
        $f = BudgetClass::find($id);
        if(!is_null($f)){
            if ($status == 1) {
                //success
                $f->is_delivered_to_ffars = true;
                $f->ffars_response = $feedback;
                $f->save();
            } else {
                //failed
                $f->ffars_response = $feedback;
                $f->save();
            }
        }
    }

    public function updateFundTypeStatus($body){
        $id = $body->data->id;
        $status = $body->status;
        $feedback = $body->message;
        $f = FundType::find($id);
        if(!is_null($f)){
            if ($status == 1) {
                //success
                $f->is_delivered_to_ffars = true;
                $f->ffars_response = $feedback;
                $f->save();
            } else {
                //failed
                $f->ffars_response = $feedback;
                $f->save();
            }
        }
    }

    public function updateFundSourceStatus($body){
        $id = $body->data->id;
        $status = $body->status;
        $feedback = $body->message;
        $f = FundSource::find($id);
        if(!is_null($f)){
            if ($status == 1) {
                //success
                $f->is_delivered_to_ffars = true;
                $f->ffars_response = $feedback;
                $f->save();
            } else {
                //failed
                $f->ffars_response = $feedback;
                $f->save();
            }
        }
    }

    public function updateProjectStatus($body){
        $id = $body->data->id;
        $status = $body->status;
        $feedback = $body->message;
        $f = Project::find($id);
        if(!is_null($f)){
            if ($status == 1) {
                //success
                $f->is_delivered_to_ffars = true;
                $f->ffars_response = $feedback;
                $f->save();
            } else {
                //failed
                $f->ffars_response = $feedback;
                $f->save();
            }
        }
    }

    public function updateGFSCodeStatus($body){
        $id = $body->data->id;
        $status = $body->status;
        $feedback = $body->message;
        $f = GfsCode::find($id);
        if(!is_null($f)){
            if ($status == 1) {
                //success
                $f->is_delivered_to_ffars = true;
                $f->ffars_response = $feedback;
                $f->save();
            } else {
                //failed
                $f->ffars_response = $feedback;
                $f->save();
            }
        }
    }

    public function updateActivityStatus($body){
        $id = $body->data->id;
        $status = $body->status;
        $feedback = $body->message;
        $f = FfarsActivity::find($id);
        if(!is_null($f)){
            if ($status == 1) {
                //success
                $f->is_delivered_to_ffars = true;
                $f->ffars_response = $feedback;
                $f->save();
            } else {
                //failed
                $f->ffars_response = $feedback;
                $f->save();
            }
        }
    }

    public function getFFARSSegmentFeedback(){
        $obj = new Tail();
        $obj->listenWithOptions($this->queue, $this->options, function ($message) {
            try {
                $properties = $message->get_properties();
                $applicationHeaders = $properties['application_headers'];
                $type = $applicationHeaders->getNativeData()['type'];
                $body = $message->body;
                switch ($type) {
                    case 'FACILITIES':
                        $this->updateFacilityStatus($body);
                        break;
                    case 'SUBBUDGETCLASSES':
                        $this->updateSubBudgetClassStatus($body);
                        break;
                    case 'FUNDTYPES':
                        $this->updateFundTypeStatus($body);
                        break;
                    case 'FUNDSOURCES':
                        $this->updateFundSourceStatus($body);
                        break;
                    case 'PROJECTS':
                        $this->updateProjectStatus($body);
                        break;
                    case 'GFSCODES':
                        $this->updateGFSCodeStatus($body);
                        break;
                    case 'ACTIVITIES':
                        $this->updateActivityStatus($body);
                        break;
                    default:
                        echo "type unknown";
                }
            } catch (Exception $e) {

            }
        });
    }

    public function getFfarsActualExpenditure(){
        try {
        $obj = new Tail();
        $obj->listenWithOptions($this->queue, $this->options, function ($msg) {
            /**
            * Define the parameters for RabbitMQ queueing
            */
            $feedback_options = array(
                'exchange' => 'EXPENDITURES_FROM_FFARS_EXCHANGE_FEEDBACK',
                'headers' => array('Type' => 'FeedBack', 'x-match'=> 'all')
                );
            $queue_name = ConfigurationSetting::getValueByKey('FFARS_ACTUAL_EXPENDITURE_FEEDBACK_QUEUE_NAME');

            $message = json_decode($msg->body);
            $admin_hierarchy_code = $message->Company;
            $chart_of_accounts_from_ffars = $message->Account;

            $council = AdminHierarchy::where('code', $admin_hierarchy_code)->first();
            $admin_hierarchy_id = isset($council->id) ? $council->id : null;

            //Check the current active period from the periods table. The period_group should not be annual
            $date_entry = $message->JEDate;
            $query = "select * from periods where '$date_entry'::date between start_date and end_date and periods.period_group_id != 8";
            $get_period = DB::select(DB::raw($query));
            $period_id = isset($get_period[0]->id) ? $get_period[0]->id: null;
            $gl_account = $message->Account;
            $gl_account = explode('-', $gl_account);
            $gfs_code_from_ffars = $gl_account[9];
            $gfs_code = GfsCode::where('code','=',$gfs_code_from_ffars)->first();
            $gfs_code_id = isset($gfs_code->id) ? $gfs_code->id : null;

            $facility_code_ffars = $gl_account[4];
            $facility_code_ffars = Facility::where('facility_code','=',$facility_code_ffars)->first();

            $budget_class_from_ffars = $gl_account[3];
            $budget_class = BudgetClass::where('code','=',$budget_class_from_ffars)->first();
            $budget_class_id = isset($budget_class->id) ? $budget_class->id : null;

            $fund_source_from_ffars = $gl_account[8];
            $fund_source = FundSource::where('code','=',$fund_source_from_ffars)->first();
            $fund_source_id = isset($fund_source->id) ? $fund_source->id : null;

            //Get the financial year by which the transaction was done by checking on the JEDate
            $je_date            = $message->JEDate;
            $query              = "select * from financial_years where '$je_date'::date
                                   between start_date and end_date";
            $get_financial_year = DB::select(DB::raw($query));
            $financial_year_id  = isset($get_financial_year[0]->id) ? $get_financial_year[0]->id: null;

            /**
             *Check for existing gl_account in PlanRep
            **/
            $budget_export_account = BudgetExportAccount::where('chart_of_accounts',$chart_of_accounts_from_ffars)
                                    ->where('financial_system_code','FFARS')
                                    ->where('financial_year_id',$financial_year_id)->first();

            $budget_export_account_id = isset($budget_export_account->id) ? $budget_export_account->id : null;

            $feedback_message = array();

            /**
            * Read the budget type from FFARS and convert it to the PlanRep's notation
            */
            $budget_type = isset($message->budget_type) ? $message->budget_type : null;

            if ($budget_type = 'BA'){
                $budget_type = 'APPROVED';
            } else if ($budget_type = 'BS') {
                $budget_type = 'SUPPLEMENTARY';
            } else if ($budget_type = 'CF'){
                $budget_type = 'CARRYOVER';
            }

            /**
             * Check if all the required fields have been defined
             */
            if ($budget_export_account_id && $admin_hierarchy_id && $budget_type && $budget_export_account_id && $gfs_code_id
                && $fund_source_id && $period_id && $budget_class_id){
                /**
                 * Check if the transaction exists in PlanRep
                 */
                $transaction_exists = FfarsActualControlId::where('transaction_id',$message->id)
                                                        ->where('transaction_type', 'EXPENDITURE')
                                                        ->first();

                /**
                 * If the transaction exists then it is a duplicate. Throw duplicate error and feedback
                 */
                if ($transaction_exists){
                    $duplicate_feedback = array(
                        'id' => $message->id,
                        'Company' => $message->Company,
                        'BookID' => $message->BookID,
                        'JournalCode' => $message->JournalCode,
                        'JournalNum' => $message->JournalNum,
                        'CurrencyCode' => $message->CurrencyCode,
                        'FiscalYear' => $message->FiscalYear,
                        'JEDate' => $message->JEDate,
                        'Account' => $message->Account,
                        'DebitAmount' => $message->DebitAmount,
                        'CreditAmount' => $message->CreditAmount,
                        'budget_type' => $message->budget_type,
                        'LogMessage' => 'Transaction failed to be generated in PlanRep. It is a duplicate of an existing transaction.',
                        'StatusCode' => 1
                    );
                    //Queue the message in RabbitMQ
                    array_push($feedback_message, $duplicate_feedback);
                    dispatch(new SendDataToRabbitMQJob($queue_name,array($duplicate_feedback), $feedback_options,''));
                } else {
                    //Insert a record in the ffars_control_ids table
                    /**
                    * Insert the transaction
                    */
                    $expenditure_data = array(
                        'budget_export_account_id' => $budget_export_account_id,
                        'period_id' => $period_id,
                        'cancelled' => false,
                        'BookID' => $message->BookID,
                        'FiscalYear' => $message->FiscalYear,
                        'JEDate' => $message->JEDate,
                        'Account' => $message->Account,
                        'date_imported' => date("Y-m-d"),
                        'gfs_code_id' => $gfs_code_id,
                        'budget_class_id' => $budget_class_id,
                        'fund_source_id' => $fund_source_id,
                        'debit_amount' => $message->DebitAmount,
                        'credit_amount' => $message->CreditAmount,
                        'budget_type' => $budget_type,
                        'created_at' => \Carbon\Carbon::now()
                    );

                    $control_ids_data = array(
                        'transaction_id' => $message->id,
                        'transaction_type' => 'EXPENDITURE',
                        'created_at' => \Carbon\Carbon::now()
                    );

                    DB::transaction(function() use ($expenditure_data, $control_ids_data) {
                        DB::table('budget_import_items')->insert([$expenditure_data]);
                        DB::table('ffars_actual_control_ids')->insert([$control_ids_data]);
                    },3);

                    //Queue the feedback in RabbitMQ
                    $success_feedback = array(
                        'id' => $message->id,
                        'Company' => $message->Company,
                        'BookID' => $message->BookID,
                        'JournalCode' => $message->JournalCode,
                        'JournalNum' => $message->JournalNum,
                        'CurrencyCode' => $message->CurrencyCode,
                        'FiscalYear' => $message->FiscalYear,
                        'JEDate' => $message->JEDate,
                        'Account' => $message->Account,
                        'DebitAmount' => $message->DebitAmount,
                        'CreditAmount' => $message->CreditAmount,
                        'budget_type' => $message->budget_type,
                        'LogMessage' => 'Transaction successfully generated in PlanRep.',
                        'StatusCode' => 1
                    );
                    array_push($feedback_message, $success_feedback);

                //    dispatch(new SendDataToRabbitMQJob($queue_name,array($success_feedback), $feedback_options,''));
                }

            } else {
                /**
                 * An error has occured as a missing field has been found
                 */
                if (!$budget_export_account_id){
                    /**
                     * Store the data in the missing GLs table
                     */
                                             /**
                     * The budget_export_account_id is missing. Store in the budget_import_missing_accounts
                     */
                    $missing_gl = array(
                        'uid' => $message->id,
                        'chart_of_accounts' => $message->Account,
                        'financial_year_id' => $financial_year_id,
                        'admin_hierarchy_id' => $admin_hierarchy_id,
                        'debit_amount' => $message->DebitAmount,
                        'credit_amount' => $message->CreditAmount,
                        'JEDate' => $message->JEDate,
                        'bookID' => $message->BookID,
                        'reason' => 'ACCOUNT_MISSING_IN_PLANREP',
                        'transaction_type' => 'EXPENDITURE',
                        'financial_system_code' => 'FFARS',
                        'created_at' =>\Carbon\Carbon::now()
                    );

                    DB::table('budget_import_missing_accounts')->insert([$missing_gl]);
                    $error_message = "The GL Account has not been found in PlanRep";
                }

                if (!$period_id){
                    $error_message = "The quarter has not been found in PlanRep";
                }

                if (!$period_id){
                    $error_message = "The quarter has not been found in PlanRep";
                }

                if (!$admin_hierarchy_id){
                    $error_message = "The council has not been found in PlanRep";
                }

                if (!$budget_type){
                    $error_message = "The budget type has not been found in PlanRep";
                }

                if (!$gfs_code_id){
                    $error_message = "The GFS code has not been found in PlanRep";
                }

                if (!$fund_source_id){
                    $error_message = "The fund source has not been found in PlanRep";
                }

                if (!$budget_class_id){
                    $error_message = "The budget class has not been found in PlanRep";
                }

                if (!$facility_code_ffars){
                    $error_message = "The facility has nor been found in PlanRep";
                }

                $error_feedback = array(
                    'id' => $message->id,
                    'Company' => $message->Company,
                    'BookID' => $message->BookID,
                    'JournalCode' => $message->JournalCode,
                    'JournalNum' => $message->JournalNum,
                    'CurrencyCode' => $message->CurrencyCode,
                    'FiscalYear' => $message->FiscalYear,
                    'JEDate' => $message->JEDate,
                    'Account' => $message->Account,
                    'DebitAmount' => $message->DebitAmount,
                    'CreditAmount' => $message->CreditAmount,
                    'budget_type' => $message->budget_type,
                    'LogMessage' => 'Transaction failed to be generated in PlanRep. '.$error_message,
                    'StatusCode' => 0
                );
                /**
                 * Queue the feedback in RabbitMQ
                 */
                array_push($error_feedback, $error_feedback);
                // dispatch(new SendDataToRabbitMQJob($queue_name,array($error_feedback), $feedback_options,''));
            }

            dispatch(new SendDataToRabbitMQJob($queue_name,$feedback_message, $feedback_options,''));
        });

    } Catch (Exception $e){
        Log::debug($e->getMessage());
    }
    }

    /**get budget request from FFARS */
    public function getBudgetRequestFromFFARS(){
        try {
            $obj = new Tail();
            $obj->listenWithOptions($this->queue, $this->options, function ($msg) {
                /**
                * Define the parameters for RabbitMQ queueing
                */
                $feedback_options = array(
                    'exchange' => 'BUDGET_REQUEST_EXCHANGE',
                    'headers' => array('Type' => 'BUDGET_REQUEST', 'x-match'=> 'all')
                    );
                $queue_name = ConfigurationSetting::getValueByKey('BUDGET_REQUEST_QUEUE_NAME');
                $message = json_decode($msg->body);
                $company = $message->company;
                $facility_code = $message->facilityCode;
                $budget_type = 'APPROVED';
                //check if budget exist
                $mtef = DB::table('mtefs as m')
                        ->join('mtef_sections as ms','ms.mtef_id','ms.id')
                        ->join('activities as ac', 'ac.mtef_section_id', 'ms.id')
                        ->join('activity_facilities as af', 'af.activity_id', 'ac.id')
                        ->join('facilities as f','f.id', 'af.facility_id')
                        ->join('admin_hierarchies ah', 'ah.id','m.admin_hierarchy_id')
                        ->where('ac.budget_type', $budget_type)
                        ->where('f.facility_code', $facility_code)
                        ->where('ah.code', $company)
                        ->select('m.id')
                        ->first();
                 if(isset($mtef->id)){
                    //create activity
                    BudgetExportAccountService::createActivities($mtef->id, $budget_type);

                   }else {
                    $error_message = "No budget found for the facility";
                 }
                });
            }Catch (Exception $e){
                Log::debug($e->getMessage());
            }
    }

    private static function getAccountType($gfs_code){
        //Get the account type of the GFS code
        $gfs_account_type = DB::table('gfs_codes')
            ->join('account_types', 'gfs_codes.account_type_id', '=', 'account_types.id')
            ->where('gfs_codes.code', $gfs_code)
            ->select('account_types.id')
            ->first();
        $gfs_account_type = isset($gfs_account_type->id) ? $gfs_account_type->id : null;
        if($gfs_account_type == null){
            //control account
            return 'control';
        }
        //Get the gfs code for expense from the configuration settings
        $expense_gfs = ConfigurationSetting::getValueByKey('GFS_ACCOUNT_TYPE_FOR_EXPENSES');

        //Get the gfs code for revenue from the configuration settings
        $revenue_gfs = ConfigurationSetting::getValueByKey('GFS_ACCOUNT_TYPE_FOR_REVENUE');

        foreach ($expense_gfs as $item) {
            if ($item == $gfs_account_type) {
                return 'expense';
            }
        }

        foreach ($revenue_gfs as $item) {
            if ($item == $gfs_account_type) {
                return 'revenue';
            }
        }
    }

    public function getFfarsActualRevenue(){
        try {
        $obj = new Tail();
        $obj->listenWithOptions($this->queue, $this->options, function ($msg) {
            /**
            * Define the parameters for RabbitMQ queueing
            */
            $feedback_options = array(
                'exchange' => 'REVENUE_FROM_FFARS_EXCHANGE_FEEDBACK',
                'headers' => array('Type' => 'FeedBack', 'x-match'=> 'all')
                );
            $queue_name = ConfigurationSetting::getValueByKey('FFARS_ACTUAL_REVENUE_FEEDBACK_QUEUE_NAME');

            $message = json_decode($msg->body);
            $admin_hierarchy_code = $message->Company;
            $chart_of_accounts_from_ffars = $message->Account;
            $je_date = $message->JEDate;

            $council = AdminHierarchy::where('code', $admin_hierarchy_code)->first();
            $admin_hierarchy_id = isset($council->id) ? $council->id : null;

            //Check the current active period from the periods table. The period_group should not be annual
            $query = "select * from periods where '$je_date'::date between start_date and end_date and periods.period_group_id != 8";
            $get_period = DB::select(DB::raw($query));
            $period_id = isset($get_period[0]->id) ? $get_period[0]->id: null;

            $gl_account = $message->Account;
            $gl_account = explode('-', $gl_account);
            $gfs_code_from_ffars = $gl_account[9];
            $gfs_code = GfsCode::where('code','=',$gfs_code_from_ffars)->first();
            $gfs_code_id = isset($gfs_code->id) ? $gfs_code->id : null;

            $facility_code_ffars = $gl_account[4];
            $facility_code_ffars = Facility::where('facility_code','=',$facility_code_ffars)->first();

            $fund_source_from_ffars = $gl_account[8];
            $fund_source = FundSource::where('code','=',$fund_source_from_ffars)->first();
            $fund_source_id = isset($fund_source->id) ? $fund_source->id : null;

            $budget_class_from_ffars = $gl_account[3];
            $budget_class = BudgetClass::where('code','=',$budget_class_from_ffars)->first();
            $budget_class_id = isset($budget_class->id) ? $budget_class->id : null;

            //Get the financial year by which the transaction was done by checking on the JEDate
            $query              = "select * from financial_years where '$je_date'::date
                                   between start_date and end_date";
            $get_financial_year = DB::select(DB::raw($query));
            $financial_year_id  = isset($get_financial_year[0]->id) ? $get_financial_year[0]->id: null;

            /**
             *Check for existing gl_account in PlanRep
            **/
            $revenue_export_accounts = ConsumeDataFromQueueJob::compareCOA($chart_of_accounts_from_ffars, $financial_year_id);

            $revenue_export_account_id = isset( $revenue_export_accounts->id)? $revenue_export_accounts->id:null;
            $admin_hierarchy_ceiling_id = isset($revenue_export_accounts->admin_hierarchy_ceiling_id) ? $revenue_export_accounts->admin_hierarchy_ceiling_id : null;
            $feedback_message = array();


            /**
             * Check if all the required fields have been defined
             */
            if ($revenue_export_account_id && $admin_hierarchy_ceiling_id && $period_id && $fund_source_id && $gfs_code_id
                && $revenue_export_account_id && $period_id){
                /**
                 * Check if the transaction exists in PlanRep
                 */
                $transaction_exists = FfarsActualControlId::where('transaction_id',$message->id)
                                                        ->where('transaction_type', 'REVENUE')
                                                        ->first();

                /**
                 * If the transaction exists then it is a duplicate. Throw duplicate error and feedback
                 */
                if ($transaction_exists){
                    $duplicate_feedback = array(
                        'id' => $message->id,
                        'Company' => $message->Company,
                        'BookID' => $message->BookID,
                        'JournalCode' => $message->JournalCode,
                        'JournalNum' => $message->JournalNum,
                        'CurrencyCode' => $message->CurrencyCode,
                        'FiscalYear' => $message->FiscalYear,
                        'JEDate' => $message->JEDate,
                        'Account' => $message->Account,
                        'DebitAmount' => $message->DebitAmount,
                        'CreditAmount' => $message->CreditAmount,
                        'LogMessage' => 'Transaction failed to be generated in PlanRep. It is a duplicate of an existing transaction.',
                        'StatusCode' => 1
                    );
                    //Queue the message in RabbitMQ
                    array_push($feedback_message, $duplicate_feedback);
                } else {
                    //Insert a record in the ffars_control_ids table
                    /**
                    * Insert the transaction
                    */
                    $revenue_data = array(
                        'admin_hierarchy_ceiling_id' => $revenue_export_accounts->admin_hierarchy_ceiling_id,
                        'revenue_export_account_id' => $revenue_export_account_id,
                        'period_id' => $period_id,
                        'reference_code' => null,
                        'gfs_code_id' => $gfs_code_id,
                        'fund_source_id' => $fund_source_id,
                        'debit_amount' => $message->DebitAmount,
                        'credit_amount' => $message->CreditAmount,
                        'fiscal_year' => $message->FiscalYear,
                        'JEDate' => $message->JEDate,
                        'account' => $message->Account,
                        'date_received' => date("Y-m-d"),
                        'created_at' => \Carbon\Carbon::now()
                    );

                    $control_ids_data = array(
                        'transaction_id' => $message->id,
                        'transaction_type' => 'REVENUE',
                        'created_at' => \Carbon\Carbon::now()
                    );

                    DB::transaction(function() use ($revenue_data, $control_ids_data) {
                        DB::table('received_fund_items')->insert([$revenue_data]);
                        DB::table('ffars_actual_control_ids')->insert([$control_ids_data]);
                    },3);

                    //Queue the feedback in RabbitMQ
                    $success_feedback = array(
                        'id' => $message->id,
                        'Company' => $message->Company,
                        'BookID' => $message->BookID,
                        'JournalCode' => $message->JournalCode,
                        'JournalNum' => $message->JournalNum,
                        'CurrencyCode' => $message->CurrencyCode,
                        'FiscalYear' => $message->FiscalYear,
                        'JEDate' => $message->JEDate,
                        'Account' => $message->Account,
                        'DebitAmount' => $message->DebitAmount,
                        'CreditAmount' => $message->CreditAmount,
                        'LogMessage' => 'Transaction successfully generated in PlanRep.',
                        'StatusCode' => 1
                    );
                    array_push($feedback_message, $success_feedback);
                }

            } else {
                /**
                 * An error has occured as a missing field has been found
                 */
                if (!$revenue_export_account_id){
                    /**
                     * Store the data in the missing GLs table
                     */
                                             /**
                     * The revenue_export_account_id is missing. Store in the received_fund_missing_accounts
                     */
                    $missing_gl = array(
                        'uid' => $message->id,
                        'chart_of_accounts' => $message->Account,
                        'financial_year_id' => $financial_year_id,
                        'admin_hierarchy_id' => $admin_hierarchy_id,
                        'debit_amount' => $message->DebitAmount,
                        'credit_amount' => $message->CreditAmount,
                        'JEDate' => $message->JEDate,
                        'bookID' => $message->BookID,
                        'reason' => 'ACCOUNT_MISSING_IN_PLANREP',
                        'transaction_type' => 'REVENUE',
                        'financial_system_code' => 'FFARS',
                        'created_at' =>\Carbon\Carbon::now(),
                    );

                    DB::table('received_fund_missing_accounts')->insert([$missing_gl]);
                    $error_message = "The GL Account has not been found in PlanRep";
                }

                if (!$period_id){
                    $error_message = "The quarter has not been found in PlanRep";
                }

                if (!$admin_hierarchy_id){
                    $error_message = "The council has not been found in PlanRep";
                }

                if (!$gfs_code_id){
                    $error_message = "The GFS code has not been found in PlanRep";
                }

                if (!$fund_source_id){
                    $error_message = "The fund source has not been found in PlanRep";
                }

                if (!$budget_class_id){
                    $error_message = "The budget class has not been found in PlanRep";
                }

                if (!$facility_code_ffars){
                    $error_message = "The facility has nor been found in PlanRep";
                }

                $error_feedback = array(
                    'id' => $message->id,
                    'Company' => $message->Company,
                    'BookID' => $message->BookID,
                    'JournalCode' => $message->JournalCode,
                    'JournalNum' => $message->JournalNum,
                    'CurrencyCode' => $message->CurrencyCode,
                    'FiscalYear' => $message->FiscalYear,
                    'JEDate' => $message->JEDate,
                    'Account' => $message->Account,
                    'DebitAmount' => $message->DebitAmount,
                    'CreditAmount' => $message->CreditAmount,
                    'LogMessage' => 'Transaction failed to be generated in PlanRep. '.$error_message,
                    'StatusCode' => 0
                );
                /**
                 * Queue the feedback in RabbitMQ
                 */
                array_push($error_feedback, $error_feedback);
            }

            dispatch(new SendDataToRabbitMQJob($queue_name,$feedback_message, $feedback_options,''));
        });

    } Catch (Exception $e){
        Log::error($e);
    }
    }

    /**an alternative way to compare chart of account for revenue */
    public static function compareCOA($gl_account, $financial_year_id){

        $revenue_export_accounts = RevenueExportAccount::where('chart_of_accounts',$gl_account)
                                       ->where('financial_system_code','FFARS')
                                       ->where('financial_year_id',$financial_year_id)
                                       ->first();
        if(isset($revenue_export_accounts->id)){
            return $revenue_export_accounts;
        } else {
            $array = explode('-', $gl_account);
            unset($array[9]);
            $new_gl_account = implode('-', $array);
            //search for revenue export account
            $revenue_export_accounts = RevenueExportAccount::where('chart_of_accounts','ilike',$new_gl_account.'%')
                                                            ->where('financial_system_code','FFARS')
                                                            ->where('financial_year_id',$financial_year_id)
                                                            ->first();
            return $revenue_export_accounts;
        }
    }

    /**
     * Read account balances from FFARS
     */
    private  function getFfarsAccountBalances(){
        try {
            $obj = new Tail();
            $obj->listenWithOptions($this->queue, $this->options, function ($msg) {

                /**
                 * Define the parameters for RabbitMQ queueing
                 */
                $feedback_options = array(
                    'exchange' => 'BALANCES_FROM_FFARS_QUEUE_EXCHANGE_FEEDBACK',
                    'headers' => array('Type' => 'FeedBack', 'x-match'=> 'all')
                );
                $queue_name = 'BALANCES_FROM_FFARS_QUEUE_FEEDBACK';

                $message = json_decode($msg->body);

                $gl_account = $message->account;
                $gl_account = explode('-', $gl_account);
                $council_code = $gl_account[1];
                $council_code = AdminHierarchy::where('code','=',$council_code)->first();
                $admin_hierarchy_id = isset($council_code->id) ? $council_code->id : null;

                $gl_account = $message->account;
                $gl_account = explode('-', $gl_account);
                $section_code = $gl_account[2];
                $section_code = Section::where('code','=',$section_code)->first();
                $section_id = isset($section_code->id) ? $section_code->id : null;

                $gl_account = $message->account;
                $gl_account = explode('-', $gl_account);
                $gfs_code_from_ffars = $gl_account[9];
                $gfs_code = GfsCode::where('code','=',$gfs_code_from_ffars)->first();
                $gfs_code_id = isset($gfs_code->id) ? $gfs_code->id : null;

                $facility_code_ffars = $gl_account[4];
                $facility_code_ffars = Facility::where('facility_code','=',$facility_code_ffars)->where('is_active', true)->first();
                $facility_id         = isset($facility_code_ffars->id) ? $facility_code_ffars->id : null;

                $fund_source_from_ffars = $gl_account[8];
                $fund_source_from_ffars = ( $fund_source_from_ffars == '20B') ? '20Z' :  $fund_source_from_ffars ;
                $fund_source = FundSource::where('code','=',$fund_source_from_ffars)->first();
                $fund_source_id = isset($fund_source->id) ? $fund_source->id : null;

                $budget_class_from_ffars = $gl_account[3];
                $budget_class = BudgetClass::where('code','=',$budget_class_from_ffars)->first();
                $budget_class_id = isset($budget_class->id) ? $budget_class->id : null;

                $apply_date = $message->JEDate;
                $financial_year_query = "select * from financial_years where '".$apply_date."'::date between start_date and end_date";
                $get_financial_year = DB::select(DB::raw($financial_year_query));
                $financial_year_id  = isset($get_financial_year[0]->id) ? $get_financial_year[0]->id: null;

                $data = [
                    'chart_of_accounts'  => $message->account,
                    'JEDate'             => $message->JEDate,
                    'debit_amount'       => (float)$message->DebitAmount,
                    'credit_amount'      => (float)$message->CreditAmount,
                    'budget_class_id'    => $budget_class_id,
                    'financial_year_id'  => $financial_year_id,
                    'section_id'         => $section_id,
                    'facility_id'        => $facility_id,
                    'admin_hierarchy_id' => $admin_hierarchy_id,
                    'gfs_code_id'        => $gfs_code_id,
                    'fund_source_id'     => $fund_source_id,
                    'ffars_id'           => $message->id,
                    'created_at'         => \Carbon\Carbon::now()
                ];

                /**
                 *
                 * Check if the ffars_id exists in the account_balances_table
                 **/
                $account_balance = AccountBalance::where('ffars_id',$message->id)->first();
                $ffars_id        = isset($account_balance->ffars_id) ? $account_balance->ffars_id : null;

                if ($ffars_id == null){
                    /**
                     * The ffars_id doesn't exist. Insert tha data
                     * */
                    /**
                     * Calculate the amount from the received message
                     */
                    if ($message->DebitAmount > $message->CreditAmount){
                        $amount = $message->DebitAmount - $message->CreditAmount;
                    } else {
                        $amount = $message->CreditAmount - $message->DebitAmount;
                    }
                    $ceilingId = AdminHierarchyCeiling::importCarryOverCeiling($financial_year_id,$admin_hierarchy_id,$section_id,$facility_id,$fund_source_id,$budget_class_id,$amount);

                    if($ceilingId != null){
                        $data['admin_hierarchy_ceiling_id'] = $ceilingId;
                        DB::table('account_balances')->insert([$data]);

                        //Send feedback to FFARS
                        $feedback_data = [
                            'id' => $message->id,
                            'account' => $message->account,
                            'JEDate'  => $message->JEDate,
                            'DebitAmount' => $message->DebitAmount,
                            'CreditAmount' => $message->CreditAmount,
                            'StatusCode' => 1,
                            'message' => 'The transaction generated successfully in PlanRep'
                        ];
                    }
                    else {
                         //Send feedback to FFARS
                        $feedback_data = [
                        'id' => $message->id,
                        'account' => $message->account,
                        'JEDate'  => $message->JEDate,
                        'DebitAmount' => $message->DebitAmount,
                        'CreditAmount' => $message->CreditAmount,
                        'StatusCode' => 0,
                        'message' => 'This is fundsource and budgetclass combination doesnot exist in Planrep'
                    ];

                    }

                } else {
                    //Send feedback to FFARS
                    $feedback_data = [
                        'id' => $message->id,
                        'account' => $message->account,
                        'JEDate'  => $message->JEDate,
                        'DebitAmount' => $message->DebitAmount,
                        'CreditAmount' => $message->CreditAmount,
                        'StatusCode' => 1,
                        'message' => 'This is a duplicate of an existing transaction in PlanRep'
                    ];
                }
                $feedback_message = array();
                array_push($feedback_message, $feedback_data);
                dispatch(new SendDataToRabbitMQJob($queue_name,$feedback_message, $feedback_options,''));
            });
        }Catch (Exception $e){
            Log::debug($e);
        }
    }

//    private function readFfarsAllocations(){
////        $obj = new Tail();
////        $obj->listenWithOptions($this->queue, $this->options, function ($message) {
////            $message = json_decode($message->body);
//            /**
//             * Read the transaction details from the message
//             */
//        $message =json_decode(File::get(storage_path('mydata/my_test_data.json')));
//        Log::debug($message);
//            $transactions = $message->transactions;
//            $uid = $message->uid;
//            $je_date = $message->je_date;
//
//            $check = FfarsActualControlId::where([
//                'transaction_id' => $uid,
//                'transaction_type' => 'FFARS-ALLOCATIONS'
//            ])->count();
//
//            if ($check == 0) {
//                //Check the current active period from the periods table. The period_group should not be annual
//                $query = "select * from periods where '" . $je_date . "'::date between start_date and end_date and periods.period_group_id != 8";
//                $get_period = DB::select(DB::raw($query));
//                $period_id = isset($get_period[0]->id) ? $get_period[0]->id : null;
//
//                //Check the current active financial year from the financial_years table.
//                $query = "select * from financial_years where '" . $je_date . "'::date between start_date and end_date";
//                $get_period = DB::select(DB::raw($query));
//                $financial_year_id = isset($get_period[0]->id) ? $get_period[0]->id : null;
//
//                //Insert into ffars actual control ids table
//                    FfarsActualControlId::create([
//                    'transaction_id' => $uid,
//                    'transaction_type' => 'FFARS-ALLOCATIONS'
//                    ]);
//                $valid_transactions = array();
//                $res_message = '';
//                foreach ($transactions as $transaction) {
//                    $account = $transaction->account;
//                    $budget_export_account = BudgetExportAccount::where([
//                        'chart_of_accounts' => $account,
//                        'financial_year_id' => $financial_year_id,
//                        'financial_system_code' => 'FFARS'
//                    ])->first();
//
//                    $budget_export_account_id = isset($budget_export_account->id)
//                        ? $budget_export_account->id : null;
//                    if ($budget_export_account_id !== null) {
//                        array_push($valid_transactions, $transaction);
//                    }
//                    $res_message .= "*" . $account;
//                }
//                $num_of_valid_trx = count($valid_transactions);
//                $num_of_trx = count($transactions);
//
//                if ($num_of_valid_trx == $num_of_trx) {
//                    foreach ($valid_transactions as $valid_transaction) {
//                        $account = $valid_transaction->account;
//                        $budget_export_account = BudgetExportAccount::where([
//                            'chart_of_accounts' => $account,
//                            'financial_year_id' => $financial_year_id,
//                            'financial_system_code' => 'FFARS'
//                        ])->first();
//
//                        $budget_export_account_id = isset($budget_export_account->id)
//                            ? $budget_export_account->id : null;
//                        /**
//                         * Insert the transaction
//                         **/
//                        //Insert and get the id of the fund_allocated_from_financial_systems
//                        DB::transaction(function () use ($valid_transaction, $account, $budget_export_account_id, $period_id, $je_date, $uid) {
//
//                            $fund_allocated_from_financial_systems_id = DB::table('fund_allocated_from_financial_systems')
//                                ->insertGetId([
//                                    'gl_account' => $account,
//                                    'description' => 'FFARS-ALLOCATIONS',
//                                    'BookID' => 'FFARS-ALLOCATIONS',
//                                    'JEDate' => $je_date,
//                                    'FiscalYear' => 'FFARS-ALLOCATIONS',
//                                    'debit_amount' => $valid_transaction->debit_amount,
//                                    'credit_amount' => $valid_transaction->credit_amount,
//                                    'created_at' => \Carbon\Carbon::now()
//                                ]);
//
//                            //Insert an entry in the fund allocated transactions table
//                            $fund_allocated_transaction_id = DB::table('fund_allocated_transactions')
//                                ->insertGetId([
//                                    'reference_code' => microtime(true) * 1000,
//                                    'data_source_id' => null,
//                                    'import_method_id' => null,
//                                ]);
//
//                            DB::table('fund_allocated_transaction_items')
//                                ->insert([
//                                    'fund_allocated_transaction_id' => $fund_allocated_transaction_id,
//                                    'budget_export_account_id' => $budget_export_account_id,
//                                    'amount' => null,
//                                    'credit_amount' => $valid_transaction->credit_amount,
//                                    'debit_amount' => $valid_transaction->debit_amount,
//                                    'allocation_date' => $je_date,
//                                    'period_id' => $period_id,
//                                    'fund_allocated_from_financial_system_id' => $fund_allocated_from_financial_systems_id,
//                                    'created_at' => \Carbon\Carbon::now()
//                                ]);
//                        }, 2);
//                    }
//                    /**
//                     * Create a feedback message for the valid transaction and queue it into RabbitMQ for FFARS
//                     */
//                    $msg = (array)$message;
//                    $msg['message'] = "The transaction has been generated successfully in PlanRep";
//                    $msg['statusCode'] = 1;
//                    $feedback_message = array();
//                    array_push($feedback_message, $msg);
//                } else {
//                    /**
//                     * Create the feedback message for transaction failure and send it to FFARS
//                     */
//                    $msg = (array)$message;
//                    $msg['message'] = "GL Accounts missing - " . $res_message;
//                    $msg['statusCode'] = 0;
//                    $feedback_message = array();
//                    array_push($feedback_message, $msg);
//                }
//            } else {
//                /**
//                 * The transaction is a duplicate. Create a feedback message and queue it into RabbitMQ
//                 */
//                $msg = (array)$message;
//                $msg['message'] = "This is a duplicate of an existing transaction. A new entry has not been generated in PlanRep";
//                $msg['statusCode'] = 1;
//                $feedback_message = array();
//                array_push($feedback_message, $msg);
//            }
//            Log::debug($uid);
//            Log::debug($feedback_message);
//            $this->returnFeedback($feedback_message);
//        //});
//    }
    private function readFfarsAllocations()
    {
        $obj = new Tail();
        $obj->listenWithOptions($this->queue, $this->options, function ($message) {
            $message = json_decode($message->body);
            $transactions = $message->transactions;
            $uid = $message->uid;
            $je_date = $message->je_date;

            $check = FfarsActualControlId::where([
                'ffars_uid' => $uid,
                'transaction_type' => 'FFARS-ALLOCATIONS'
            ])->count();

            if ($check == 0) {
                //Check the current active period from the periods table. The period_group should not be annual
                $query = "select * from periods where '" . $je_date . "'::date between start_date and end_date and periods.period_group_id != 8";
                $get_period = DB::select(DB::raw($query));
                $period_id = isset($get_period[0]->id) ? $get_period[0]->id : null;

                //Check the current active financial year from the financial_years table.
                $query = "select * from financial_years where '" . $je_date . "'::date between start_date and end_date";
                $get_period = DB::select(DB::raw($query));
                $financial_year_id = isset($get_period[0]->id) ? $get_period[0]->id : null;

                $valid_transactions = array();
                $res_message = '';

                foreach ($transactions as $transaction) {
                    $account = $transaction->account;
                    $budget_export_account = BudgetExportAccount::where([
                        'chart_of_accounts' => $account,
                        'financial_year_id' => $financial_year_id,
                        'financial_system_code' => 'FFARS'
                    ])->first();


                    if ($budget_export_account) {
                        array_push($valid_transactions, $transaction);
                    } else {
                        $res_message = "UID : " . $uid . " - ACC : " . $account;
                    }
                }
                $num_of_valid_trx = count($valid_transactions);
                $num_of_trx = count($transactions);

                if ($num_of_valid_trx == $num_of_trx) {
                    $success = true;
                    DB::beginTransaction();
                    try {
                        //Insert into ffars actual control ids table
                        FfarsActualControlId::create([
                            'transaction_id' => $uid,
                            'ffars_uid' => $uid,
                            'transaction_type' => 'FFARS-ALLOCATIONS'
                        ]);

                        foreach ($valid_transactions as $valid_transaction) {
                            $account = $valid_transaction->account;
                            $budget_export_account = BudgetExportAccount::where([
                                'chart_of_accounts' => $account,
                                'financial_year_id' => $financial_year_id,
                                'financial_system_code' => 'FFARS'
                            ])->first();

                            $budget_export_account_id = $budget_export_account->id;

                            /**
                             * Insert the transaction
                             **/
                            $fund_allocated_from_financial_systems_id = DB::table('fund_allocated_from_financial_systems')
                                ->insertGetId([
                                    'gl_account' => $account,
                                    'description' => 'FFARS-ALLOCATIONS',
                                    'BookID' => 'FFARS-ALLOCATIONS',
                                    'JEDate' => $je_date,
                                    'uid' => $uid,
                                    'FiscalYear' => 'FFARS-ALLOCATIONS',
                                    'debit_amount' => $valid_transaction->debit_amount,
                                    'credit_amount' => $valid_transaction->credit_amount,
                                    'created_at' => \Carbon\Carbon::now()
                                ]);
                            //Insert an entry in the fund allocated transactions table
                            $fund_allocated_transaction_id = DB::table('fund_allocated_transactions')
                                ->insertGetId([
                                    'reference_code' => microtime(true) * 1000,
                                    'data_source_id' => null,
                                    'import_method_id' => null,
                                ]);
                            DB::table('fund_allocated_transaction_items')
                                ->insert([
                                    'fund_allocated_transaction_id' => $fund_allocated_transaction_id,
                                    'budget_export_account_id' => $budget_export_account_id,
                                    'amount' => null,
                                    'credit_amount' => $valid_transaction->credit_amount,
                                    'debit_amount' => $valid_transaction->debit_amount,
                                    'allocation_date' => $je_date,
                                    'period_id' => $period_id,
                                    'fund_allocated_from_financial_system_id' => $fund_allocated_from_financial_systems_id,
                                    'created_at' => \Carbon\Carbon::now()
                                ]);
                        }
                    } catch (Exception $e) {
                        DB::rollback();
                        $msg = (array)$message;
                        $msg['message'] = 'Failed to save one or more entries in Planrep -- ';
                        $msg['statusCode'] = 0;
                        $feedback_message = array();
                        array_push($feedback_message, $msg);
                        $success = false;
                        Log::debug($e->getMessage());
                    }
                    if ($success) {
                        DB::commit();
                        /**
                         * Create a feedback message for the valid transaction and queue it into RabbitMQ for FFARS
                         */
                        $msg = (array)$message;
                        $msg['message'] = "The transaction has been generated successfully in PlanRep";
                        $msg['statusCode'] = 1;
                        $feedback_message = array();
                        array_push($feedback_message, $msg);
                    }
                } else {
                    /**
                     * Create the feedback message for transaction failure and send it to FFARS
                     */
                    $msg = (array)$message;
                    $msg['message'] = "GL Accounts missing - " . $res_message;
                    $msg['statusCode'] = 0;
                    $feedback_message = array();
                    array_push($feedback_message, $msg);
                }
            } else {
                /**
                 * The transaction is a duplicate. Create a feedback message and queue it into RabbitMQ
                 */
                $msg = (array)$message;
                $msg['message'] = "This is a duplicate of an existing transaction. A new entry has not been generated in PlanRep";
                $msg['statusCode'] = 1;
                $feedback_message = array();
                array_push($feedback_message, $msg);
            }
            $this->returnFeedback($feedback_message);
        });


    }
    private function returnFeedback($feedback_message){
        /**
         * Define the parameters for RabbitMQ queueing
         */
        $feedback_options = array(
            'exchange' => 'FFARS_ALLOCATION_TO_PLANREP_EXCHANGE_FEEDBACK',
            'headers' => array('Type' => 'FeedBack', 'x-match'=> 'all')
        );
        $queue_name = ConfigurationSetting::getValueByKey('FFARS_ALLOCATION_TO_PLANREP_FEEDBACK_QUEUE_NAME');
        dispatch(new SendDataToRabbitMQJob($queue_name,$feedback_message, $feedback_options,''));
    }

}
