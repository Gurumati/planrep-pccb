<?php

namespace App\Jobs;

use App\Models\Execution\FfarsActivity;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\Facility;
use App\Models\Setup\FundSource;
use App\Models\Setup\FundType;
use App\Models\Setup\GfsCode;
use App\Models\Setup\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ConsumeFromQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $type;
    protected $body;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($type, $body)
    {
        $this->type = $type;
        $this->body = $body;
    }

    /**
     * @param $type
     */
    private function updateFFARSSegments($type, $body)
    {
        if (isset($body->data)) {
            $id = $body->data->id;
            $status = $body->status;
            $response = $body->message;
            $errors = implode(',', $body->errors);
            $count = count($body->errors);
            if ($type == 'FACILITIES') {
                try {
                    $f = Facility::find($id);
                    ($status == 1) ? ($f->exported_to_ffars = true) : ($f->exported_to_ffars = false);
                    ($status == 1) ? ($f->is_delivered_to_ffars = true) : ($f->is_delivered_to_ffars = false);
                    $f->ffars_response = ($count > 0) ? ($response.", ".$errors) : ($response);
                    $f->save();
                } catch (\Exception $e) {
                    $this->exception = $e->getMessage();
                }
            }
            if ($type == 'SUBBUDGETCLASSES') {
                try {
                    $f = BudgetClass::find($id);
                    ($status == 1) ? ($f->exported_to_ffars = true) : ($f->exported_to_ffars = false);
                    ($status == 1) ? ($f->is_delivered_to_ffars = true) : ($f->is_delivered_to_ffars = false);
                    $f->ffars_response = ($count > 0) ? ($response.", ".$errors) : ($response);
                    $f->save();
                } catch (\Exception $e) {
                    $this->exception = $e->getMessage();
                }
            }

            if ($type == 'FUNDTYPES') {
                try {
                    $f = FundType::find($id);
                    ($status == 1) ? ($f->exported_to_ffars = true) : ($f->exported_to_ffars = false);
                    ($status == 1) ? ($f->is_delivered_to_ffars = true) : ($f->is_delivered_to_ffars = false);
                    $f->ffars_response = ($count > 0) ? ($response.", ".$errors) : ($response);
                    $f->save();
                } catch (\Exception $e) {
                    $this->exception = $e->getMessage();
                }
            }

            if ($type == 'FUNDSOURCES') {
                try {
                    $f = FundSource::find($id);
                    ($status == 1) ? ($f->exported_to_ffars = true) : ($f->exported_to_ffars = false);
                    ($status == 1) ? ($f->is_delivered_to_ffars = true) : ($f->is_delivered_to_ffars = false);
                    $f->ffars_response = ($count > 0) ? ($response.", ".$errors) : ($response);
                    $f->save();
                } catch (\Exception $e) {
                    $this->exception = $e->getMessage();
                }
            }

            if ($type == 'PROJECTS') {
                try {
                    $f = Project::find($id);
                    ($status == 1) ? ($f->exported_to_ffars = true) : ($f->exported_to_ffars = false);
                    ($status == 1) ? ($f->is_delivered_to_ffars = true) : ($f->is_delivered_to_ffars = false);
                    $f->ffars_response = ($count > 0) ? ($response.", ".$errors) : ($response);
                    $f->save();
                } catch (\Exception $e) {
                    $this->exception = $e->getMessage();
                }
            }

            if ($type == 'GFSCODES') {
                try {
                    $f = GfsCode::find($id);
                    ($status == 1) ? ($f->exported_to_ffars = true) : ($f->exported_to_ffars = false);
                    ($status == 1) ? ($f->is_delivered_to_ffars = true) : ($f->is_delivered_to_ffars = false);
                    $f->ffars_response = ($count > 0) ? ($response.", ".$errors) : ($response);
                    $f->save();
                } catch (\Exception $e) {
                    $this->exception = $e->getMessage();
                }
            }

            if ($type == 'ACTIVITIES') {
                try {
                    $f = FfarsActivity::find($id);
                    ($status == 1) ? ($f->exported_to_ffars = true) : ($f->exported_to_ffars = false);
                    ($status == 1) ? ($f->is_delivered_to_ffars = true) : ($f->is_delivered_to_ffars = false);
                    $f->ffars_response = ($count > 0) ? ($response.", ".$errors) : ($response);
                    $f->save();
                } catch (\Exception $e) {
                    $this->exception = $e->getMessage();
                }
            }
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->updateFFARSSegments($this->type, json_decode($this->body));
    }
}
