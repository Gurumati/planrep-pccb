<?php

namespace App\Jobs;
use App\Custom\Tail;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PublishDataToRabbitMQJob implements ShouldQueue {

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $queue_name;
    protected $message;
    protected $options;
    protected $exception;
    protected $budget_type;
    protected $current_item;

    public $tries = 3; //maximum number of attempts

    public function __construct($queue_name, $message, $options, $budget_type = null) {
        $this->queue_name = $queue_name;
        $this->message = $message;
        $this->options = $options;
        $this->budget_type = $budget_type;

    }


    public function handle() {
        try {
            $data = $this->message;
            $data_encoded = json_encode($data);
            $obj = new Tail();
            $obj->add($this->queue_name, $data_encoded, $this->options);

        }catch (Exception $e) {
            $this->exception = $e->getMessage();
            $this->failed_jobs();
        }
    }


    // Called when the job is failing...
    public function failed_jobs()
    {
        //insert data into a failed_jobs job table
        $failed_at = Carbon::now()->toDateTimeString();

        DB::table('failed_jobs')->insert(
            ['connection' => 'default', 'queue' => $this->queue_name,
                'payload' => json_encode($this->current_item), 'exception' => $this->exception, 'failed_at' => $failed_at]
        );
    }

    }