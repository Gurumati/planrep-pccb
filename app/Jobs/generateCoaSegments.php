<?php

namespace App\Jobs;

use App\Http\Controllers\Execution\CoaSegmentController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;


class generateCoaSegments implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//       CoaSegmentController::addAdminHierarchies();
//       CoaSegmentController::addSections();
//       CoaSegmentController::addSubBudgetClasses();
        CoaSegmentController::addFacilities();
        CoaSegmentController::addProjects();
//       CoaSegmentController::addFundSources();
//       CoaSegmentController::addFundTypes();
//       CoaSegmentController::addGfsCodes();
    }


}
