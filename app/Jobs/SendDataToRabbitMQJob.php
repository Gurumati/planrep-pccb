<?php

namespace App\Jobs;

use App\Custom\Tail;
use App\Http\Controllers\Setup\FacilityController;
use App\Models\Execution\FfarsActivity;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\Facility;
use App\Models\Setup\FundSource;
use App\Models\Setup\FundType;
use App\Models\Setup\GfsCode;
use App\Models\Setup\Project;
use Carbon\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class SendDataToRabbitMQJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $queue_name;
    public $message;
    public $options;
    public $exception;
    public $budget_type;
    public $current_item; //capture a current items from the list

    public $tries = 1; //maximum number of attempts

    public function __construct($queue_name, $message, $options, $budget_type = null)
    {
        $this->queue_name = $queue_name;
        $this->message = $message;
        $this->options = $options;
        $this->budget_type = $budget_type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {

            //initialize resulting list
            foreach ($this->message as $inputData) {
                $this->current_item = $inputData;

                $data = json_encode($inputData);
                $obj = new Tail();
                $obj->add($this->queue_name, $data, $this->options);
                //return response
                if ($this->budget_type == 'budget') {
                    $this->budgetExportResponse();
                } else if ($this->budget_type == 'glAcc') {
                    $this->glAccountResponse();
                } else if ($this->budget_type == 'control_budget') {
                    $this->controlAccountResponse();
                } else if ($this->budget_type == 'updateSegmentValues') {
                    $this->coaSegmentValuesResponse();
                } else if ($this->budget_type == 'revenue') {
                    $this->revenueExportResponse();
                }else if ($this->budget_type == 'muse_activities') {
                    $this->museActivitiesExportResponse();
                } else if ($this->budget_type == "updateFFARSSegments") {
                    $this->updateFFARSSegments($this->options['headers']['type']);
                }
                unset($this->current_item);
            }
        } catch (Exception $e) {
            $this->exception = $e->getMessage();
            $this->failed_jobs();
        }
    }

    // Called when the job is failing...
    public function failed_jobs()
    {
        //insert data into a failed_jobs table
        $failed_at = Carbon::now()->toDateTimeString();

        DB::table('failed_jobs')->insert(
            ['connection' => 'default', 'queue' => $this->queue_name,
                'payload' => json_encode($this->current_item), 'exception' => $this->exception, 'failed_at' => $failed_at]
        );
    }

    public function budgetExportResponse()
    {
        $item = $this->current_item['message'];
        $itemData = json_decode($item, true);
        foreach ($itemData['messageDetails'] as $data) {
            try{
            //update budget export to financial systems table
            DB::table('budget_export_to_financial_systems')
                ->where('id', $data['UID'])
                ->update(['is_sent' => true]);
            //get system code
            $result = DB::table('budget_export_to_financial_systems')
                      ->where('id', $data['UID'])
                      ->first();

            //check if exist
            $count = DB::table('budget_export_responses')
                     ->where('budget_export_to_financial_system_id', $data['UID'])
                     ->select('id')
                     ->count();
            if($count == 0){
                 //insert responses
                DB::table('budget_export_responses')->insert(
                    ['budget_export_to_financial_system_id' => $data['UID'], 'is_sent' => true,
                        'is_delivered' => false, 'response' => '', 'financial_system_code' => $result->queue_name]
                    );
            }
            }catch(Exception $e){
              Log::error(print_r($e->getMessage(), true));
            }
        }

    }

    public function revenueExportResponse()
    {
        $data = $this->current_item['message']['messageDetails'];
        $item = $this->current_item['message'];
        $itemData = json_decode($item, true);

        foreach ($this->current_item['message']['messageDetails'] as $data) {
            //update budget export to financial systems table
            DB::table('budget_export_to_financial_systems')
                ->where('id', $data['UID'])
                ->update(['is_sent' => true]);

            //insert responses
            DB::table('budget_export_responses')->insert(
                ['budget_export_to_financial_system_id' => $data['UID'], 'is_sent' => true,
                    'is_delivered' => false, 'response' => '', 'financial_system_code' => 'MUSE']
            );
        }

    }
    public function museActivitiesExportResponse()
    {

        $item = $this->current_item['message'];
        $itemData = json_decode($item, true);
          foreach ($itemData['messageDetails'] as $datum){
              DB::table('muse_activities')
                 ->where('activity_id',$datum['uid'])
                 ->update([
                     'exported_to_muse' => true
                 ]);
         }
    }
    public function glAccountResponse()
    {
        //update budget export accounts
        $item = $this->current_item["GLAcctDetails"];
        foreach ($item as $var){
            //check if it is revenue
            $result = DB::table('budget_export_accounts')
                ->where('chart_of_accounts', $var['GLAcctDispGLAcctDisp'])
                ->get();

            if (count($result) > 0) {
                DB::table('budget_export_accounts')
                    ->where('id', $var['UID'])
                    ->update(['is_sent' => true]);
            } else {
                //it is revenue budget
                DB::table('revenue_export_accounts')
                    ->where('id', $var['UID'])
                    ->update(['is_sent' => true]);
            }
        }
    }

    public function controlAccountResponse()
    {
        //update budget export accounts
        $control_code = $this->current_item['GLAcctDispGLAcctDisp'];
        $admin_hierarchy_id = DB::table('admin_hierarchies')
            ->where('code', $this->current_item['SegValue3'])
            ->select('id')
            ->first()->id;
        //check if
        if ($this->current_item['SegValue4'] == '0000') {
            DB::table('budget_control_accounts')
                ->insert(['code' => $control_code, 'admin_hierarchy_id' => $admin_hierarchy_id, 'is_sent' => true]);
        }

    }

    /**
     * This method updates the coa segment values table and sets is_sent as true in order to avoid duplicates
     * */
    public function coaSegmentValuesResponse()
    {
        $item = $this->current_item['SegmentValues'];
        try {
            foreach($item as $value){
                DB::table('coa_segment_company')
                    ->where('id', $value['UID'])
                    ->update(['is_sent' => true]);
            }
        } catch (Exception $e) {
            $this->exception = $e->getMessage();
        }

    }

    public function updateFFARSSegments($type)
    {
        if($type == 'FACILITIES'){
            try{
                $id = $this->current_item['id'];
                $f = Facility::find($id);
                $f->exported_to_ffars = true;
                $f->save();
            } catch (Exception $e) {
                $this->exception = $e->getMessage();
            }
        }
        if($type == 'SUBBUDGETCLASSES'){
            try{
                $id = $this->current_item['id'];
                $f = BudgetClass::find($id);
                $f->exported_to_ffars = true;
                $f->save();
            } catch (Exception $e) {
                $this->exception = $e->getMessage();
            }
        }

        if($type == 'FUNDTYPES'){
            try{
                $id = $this->current_item['id'];
                $f = FundType::find($id);
                $f->exported_to_ffars = true;
                $f->save();
            } catch (Exception $e) {
                $this->exception = $e->getMessage();
            }
        }

        if($type == 'FUNDSOURCES'){
            try{
                $id = $this->current_item['id'];
                $f = FundSource::find($id);
                $f->exported_to_ffars = true;
                $f->save();
            } catch (Exception $e) {
                $this->exception = $e->getMessage();
            }
        }

        if($type == 'PROJECTS'){
            try{
                $id = $this->current_item['id'];
                $f = Project::find($id);
                $f->exported_to_ffars = true;
                $f->save();
            } catch (Exception $e) {
                $this->exception = $e->getMessage();
            }
        }

        if($type == 'GFSCODES'){
            try{
                $id = $this->current_item['id'];
                $f = GfsCode::find($id);
                $f->exported_to_ffars = true;
                $f->save();
            } catch (Exception $e) {
                $this->exception = $e->getMessage();
            }
        }
    }



}
