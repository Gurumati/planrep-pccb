<?php

namespace App\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class PublishToQueue implements ShouldQueue {

    use InteractsWithQueue, SerializesModels;

    protected $message;
    protected $queue_name;
    protected $exchange;
    protected $header_options;
    protected $binding_options;
    protected $exchange_type;
    protected $routing_key;

    public function __construct($message, $queue_name, $exchange, $exchange_type, $routing_key='', $header_options = array(), $binding_options = array()) {
        $this->message         =  $message;
        $this->queue_name      =  $queue_name;
        $this->exchange        =  $exchange;
        $this->exchange_type   =  $exchange_type;
        $this->routing_key     =  $routing_key;
        $this->header_options  =  $header_options;
        $this->binding_options =  $binding_options;
    }

    private function publish() {
        /**
         * @param host address to rabbitmq server
         * @param port rabbitmq port
         * @param user rabbitmq user
         * @param password rabbitmq password
         */
        $server = Config::get("queue.connections.rabbitmq");
        $HOST = $server['host'];
        $PORT = $server['port'];;
        $USER = $server['username'];
        $PSWD = $server['password'];
        //$VHOST = $server['vhost'];

        $connection = new AMQPStreamConnection($HOST, $PORT, $USER, $PSWD);
        $channel    = $connection->channel();

        /**
         * creates an exchange
         * @param string $exchange name
         * @param string $type exchange type
         * @param bool $passive default false, required false
         * @param bool $durable default false, required true
         * @param bool $auto_delete default true, require false
         * @param bool $internal default false
         * @param bool $nowait default false
         * @param AMQPTable $arguments
         * @param string null
         * @return mixed|null default null
         */
        $channel->exchange_declare($this->exchange, $this->exchange_type, false, true, false, false, false, $this->binding_options);

        /**
         * creates a queue
         * @param string $queue queue name
         * @param bool $passive default false
         * @param bool $durable default false, required true
         * @param bool $exclusive default false
         * @param bool $auto_delete default true
         * @param bool $nowait default false
         * @param null $arguments default null
         * @param null $ticket default null
         * @return mixed|null
         */

        $channel->queue_declare($this->queue_name, false, true, false, false, false, $this->binding_options);

        /**
         * Binds queue to an exchange
         *
         * @param string $queue_name
         * @param string $exchange
         * @param string $routing_key, no routing keys since we're using a headers exchange
         * @param bool $nowait
         * @param null $arguments
         * @param null $ticket
         * @return mixed|null
         */
        $channel->queue_bind($this->queue_name, $this->exchange, $this->routing_key, false, $this->binding_options);

        /**
         * @param string $body
         * @param null $properties this include application_headers of type table_object
         */
        $this->message->set('application_headers', $this->header_options);

        /**
         * Publishes a message
         * @param AMQPMessage $msg
         * @param string $exchange
         * @param string $routing_key
         * @param bool $mandatory
         * @param bool $immediate
         * @param null $ticket
         */
        $channel->basic_publish($this->message, $this->exchange, $this->routing_key);
        $channel->close();
        $connection->close();
    }
    public function handle() {
        $this->publish();
    }
}