<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Execution\CeilingExportResponse;
use Illuminate\Support\Facades\DB;

class UpdateCeilingExportResponse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;
    public $publish;

    public function __construct($data,$publish)
    {
        $this->data = $data;
        $this->publish = $publish;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $ceilings = $this->data['message'];
        $code = $this->data['financial_system_code'];
        if($this->publish)
            $this->updateResponse($ceilings,$code);
        else
            $this->insertResponse($ceilings,$code);

    }

    public function updateResponse($ceilings,$code)
    {
        foreach ($ceilings as $ceiling)
        {
            DB::table('ceiling_export_responses')
                ->where('admin_hierarchy_ceiling_id', $ceiling->UID)
                ->where('financial_system_code', $code)
                ->where('period', $ceiling->period)
                ->update(['is_sent' => true]);
        }
    }


    public function insertResponse($ceilings,$code)
    {
        foreach ($ceilings as $ceiling) {
            $count = CeilingExportResponse::where('admin_hierarchy_ceiling_id', $ceiling->UID)
            ->where('financial_system_code',$code)
            ->where('period',$ceiling->period)->count();

            if($count < 1)
            {

            $response = new CeilingExportResponse();
            $response->admin_hierarchy_ceiling_id = $ceiling->UID;
            $response->period = $ceiling->period;
            $response->financial_system_code = $code;
            $response->response = "";
            $response->created_by = 54;
            $response->save();

            }
        }
    }
}
