<?php

const MESSAGE_ERROR_DESTRUCTIVE_WORDS = "SQL QUERY CONTAINS THE FOLLOWING DESCTRUCTIVE WORDS";
const MESSAGE_ERROR_UNREPLACED_PARAMS = "THE FOLLOWING PARAMS DON'T HAVE REPLACEMENTS";

if (!function_exists('replaceParams')) {
    /**
     *
     * return sql query without placeholder variable
     * e.g given the following sql $query = 'select * from where code = $P{code}'
     * replaceParams($query, ['name' => 'John Doe', 'age' => 20, 'code' => 322])
     * should return select * form where code = 322
     * @param string $query
     * @param array $params
     * @return $query
     *
     */
    function replaceParams($sqlQuery, $params = [])
    {
        // select * from tabel where code = $P{value}
        // $query = select * from tabel where code = $P{value}
        // $query = preg_replace("/(\$\w+\{\w+\})/", $value, $query);
        // Matches any value inside {} and saves in $matches array
        // preg_match returns 1 if there's a match or 0 if no match
        // then saves the match in the $matches array

        // remove new line characters and tabs and all two or more spaces with single space

        // check if query contains either drop, update, delete, truncate and return error message if it does
        $sqlQuery = queryHasDestructiveWords($sqlQuery)? queryHasDestructiveWords($sqlQuery) : $sqlQuery;

        $result = preg_replace_callback(
            '/\#P\{(.*?)\}/',
            function($el) use ($params) { return isset($params[$el[1]]) ? $params[$el[1]] : $el[0];},
            $sqlQuery
        );
        $result = allParamsHaveNotBeenReplaced($result)? allParamsHaveNotBeenReplaced($result) : $result;
        return $result;
    }
}

if (!function_exists('queryHasDestructiveWords')) {
    function queryHasDestructiveWords($sqlQuery)
    {
        if (preg_match_all('/(insert|update\b|drop|delete\b|truncate|add|create|insert|constraint|set)/i', $sqlQuery, $matches)) {
            $badWords = array_pop($matches);
            throw new \Exception(MESSAGE_ERROR_DESTRUCTIVE_WORDS. " \n".join("\r\n", $badWords));
        } else {
            return $sqlQuery;
        }
    }
}

if(!function_exists('allParamsHaveNotBeenReplaced')) {
    function allParamsHaveNotBeenReplaced($query) {
        if (preg_match_all('/(\#P\{.*?\})/', $query, $matches)) {
            $unReplacedPlaceholders = array_pop($matches);
            throw new \Exception(MESSAGE_ERROR_UNREPLACED_PARAMS. " \n".join("\r\n", $unReplacedPlaceholders));
        } else {
            return $query;
        }
    }
}
