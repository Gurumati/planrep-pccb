<?php

if (!function_exists('getDateRange')) {
    function getDateRange($periodType, $steps = 0)
    {
        $steps++;
        switch ($periodType) {
        case "FinancialJuly":
            return getPeriodPastFinancialYears($steps);
            break;
        case "Yearly":
            return getPeriodPastCalenderYears($steps);
            break;
        case "semiYearly":
            return getPeriodPastSemiYears($steps);
            break;
        case "monthly":
            return getPeriodPastMonths($steps);
            break;
        case "Quarterly":
            return getPeriodPastQuarters($steps);
            break;
        case "weekly":
            return getPeriodPastWeeks($steps);
            break;
        case "daily":
            return getPeriodPastDays($steps);
            break;
        default:
            throw new \Exception('Failed to resolve the date for the supplied period');
        }
    }
}

/**
 * calculates date by using today and number of weeks to go back
 *
 * @param int $periods
 *
 * @return stdClass
 */
if (!function_exists('getPeriodPastSemiYears')) {
    function getPeriodPastSemiYears($periods = 1)
    {
        $periods *= 6;
        $today = date('Y-m-d');

        $date_obj = date_create($today);
        date_sub($date_obj, date_interval_create_from_date_string("$periods months"));
        $dates_array = startAndEndOfSemiYear($date_obj->format('Y-m-d'));
        $result = getObject($dates_array[0], $dates_array[1]);
        return $result;
    }
}

/**
 * calculates date by using today and number of weeks to go back
 *
 * @param int $periods
 *
 * @return stdClass
 */
if (!function_exists('getPeriodPastQuarters')) {
    function getPeriodPastQuarters($periods = 1)
    {
        $periods *= 3;
        $today = date('Y-m-d');

        $date_obj = date_create($today);
        date_sub($date_obj, date_interval_create_from_date_string("$periods months"));
        $dates_array = startAndEndOfQuarter($date_obj->format('Y-m-d'));
        $result = getObject($dates_array[0], $dates_array[1]);
        return $result;
    }
}

/**
 * calculates date by using today and number of weeks to go back
 *
 * @param int $periods
 *
 * @return stdClass
 */
if (!function_exists('getPeriodPastWeeks')) {
    function getPeriodPastWeeks($periods = 1)
    {
        $today = date('Y-m-d');

        $date_obj = date_create($today);
        date_sub($date_obj, date_interval_create_from_date_string("$periods weeks"));
        $dates_array = startAndEndOfWeek($date_obj->format('Y-m-d'));
        $result = getObject($dates_array[0], $dates_array[1]);
        return $result;
    }
}
/**
 * calculates date by using today and number of days to go back
 *
 * @param int $periods
 *
 * @return stdClass
 */
if (!function_exists('getPeriodPastDays')) {
    function getPeriodPastDays($periods = 1)
    {

        $today = date('Y-m-d');

        $date_obj = date_create($today);
        date_sub($date_obj, date_interval_create_from_date_string("$periods days"));
        $date = $date_obj->format('Y-m-d');
        $result = getObject($date, $date);
        return $result;
    }
}

if (!function_exists('getPeriodPastMonths')) {
    function getPeriodPastMonths($periods = 1)
    {

        $today = date('Y-m-d');

        $date_obj = date_create($today);
        date_sub($date_obj, date_interval_create_from_date_string("$periods months"));
        $date = $date_obj->format('Y-m-d');
        $dates_array = startAndEndOfMonth($date);
        $result = getObject($dates_array[0], $dates_array[1]);
        return $result;
    }
}

/**
 * calculates date by using today and number of days to go back
 *
 * @param int $periods
 *
 * @return stdClass
 */
if (!function_exists('getPeriodPastFinancialYears')) {
    function getPeriodPastFinancialYears($periods = 0)
    {
        $today = date('Y-m-d');

        $date_obj = date_create($today);
        date_sub($date_obj, date_interval_create_from_date_string("$periods years"));
        $date = $date_obj->format('Y-m-d');
        $dates_array = startAndEndOfFinancialYear($date);
        $result = getObject($dates_array[0], $dates_array[1]);
        return $result;
    }
}

/**
 * calculates date by using today and number of days to go back
 *
 * @param int $periods
 *
 * @return stdClass
 */
if (!function_exists('getPeriodPastCalenderYears')) {
    function getPeriodPastCalenderYears($periods = 1)
    {
        $today = date('Y-m-d');

        $date_obj = date_create($today);
        date_sub($date_obj, date_interval_create_from_date_string("$periods years"));
        $date = $date_obj->format('Y-m-d');

        $dates_array = startAndEndOfYear($date);
        $result = getObject($dates_array[0], $dates_array[1]);
        return $result;
    }
}

/**
 * format the found dates as php std objec
 *  @param Date $start_date;
 * @param Date $end_date;
 * @return stdClass Ojbect
 *
 */
if (!function_exists('getObject')) {
    function getObject($startDate, $endDate)
    {
        $model = new \stdClass();
        $model->start_date = $startDate;
        $model->end_date = $endDate;
        return $model;
    }
}
/**
 * Determine start and end of financial year.
 * @param string $date
 */
if (!function_exists('startAndEndOfFinancialYear')) {
    function startAndEndOfFinancialYear($date)
    {
        //determine the start and end of year
        //$date = $date->format('Y-m-d');

        $date_array = explode('-', $date);
        $datetime = strtotime($date);

        if ($date_array[1] * 1 > 6) {
            //between july and dec
            //use this year as start and next year as end
            $start_date = date('Y-m-d', strtotime('this year july 1st', $datetime));
            $end_date = date('Y-m-d', strtotime('next year june 30th', $datetime));

        } else {
            //use this year as  end prev year as start.
            $start_date = date('Y-m-d', strtotime('last year july 1st', $datetime));
            $end_date = date('Y-m-d', strtotime('this year june 30th', $datetime));
        }
        return array($start_date, $end_date);
    }
}

if (!function_exists('startAndEndOfMonth')) {
    function startAndEndOfMonth($date)
    {
        //determine the start and end of thi month
        $datetime = strtotime($date);

        $start_date = date('01-m-Y', $datetime);
        $end_date = date('t-m-Y', $datetime);
        return array($start_date, $end_date);
    }
}

if (!function_exists('startAndEndOfWeek')) {
    function startAndEndOfWeek($date)
    {
        //$date = $date->format('Y-m-d');
        $ts = strtotime($date);
        $start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);

        return array(date('Y-m-d', $start),
            date('Y-m-d', strtotime('next saturday', $start)));
    }
}

if (!function_exists('startAndEndOfYear')) {
    function startAndEndOfYear($date)
    {
        $datetime = strtotime($date);
        $start_date = date('Y-01-01', $datetime);
        $end_date = date('Y-12-31', $datetime);
        return array($start_date, $end_date);
    }
}
/**
 * calculate start of end of quarter for given date
 * @param date
 *
 *@return array start and end dates
 * @throw Exception Date out of range.
 */
if (!function_exists('startAndEndOfQuarter')) {
    function startAndEndOfQuarter($date)
    {
        $date_array = explode('-', $date);
        $datetime = strtotime($date);
        $month = $date_array[1] * 1;
        if ($month <= 3) {
            $start_date = date('01-01-Y', $datetime);
            $end_date = date('31-03-Y', $datetime);

        } elseif ($month >= 4 and $month <= 6) {
            $start_date = date('01-04-Y', $datetime);
            $end_date = date('30-06-Y', $datetime);
        } elseif ($month >= 7 and $month <= 9) {
            $start_date = date('01-07-Y', $datetime);
            $end_date = date('30-09-Y', $datetime);
        } elseif ($month >= 10) {
            $start_date = date('01-10-Y', $datetime);
            $end_date = date('31-12-Y', $datetime);
        } else {
            throw new \Exception("Month out of range");
        }

        return array($start_date, $end_date);
    }
}

if (!function_exists('startAndEndOfSemiYear')) {
    function startAndEndOfSemiYear($date)
    {
        $date_array = explode('-', $date);
        $datetime   = strtotime($date);
        $month      = $date_array[1] * 1;
        if ($month <= 6) {
            $start_date = date('01-01-Y', $datetime);
            $end_date   = date('30-06-Y', $datetime);

        } elseif ($month >= 7) {
            $start_date = date('01-07-Y', $datetime);
            $end_date   = date('31-12-Y', $datetime);
        } else {
            throw new \Exception("Month out of range");
        }
        return array($start_date, $end_date);
    }
}
