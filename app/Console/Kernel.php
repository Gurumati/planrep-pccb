<?php

namespace App\Console;

use App\Console\Commands\sample;
use Illuminate\Console\Command;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\InvokeRabbitListener::class,
        Commands\FFARSResponse::class,
        Commands\FFARSFacilityVerification::class,
        Commands\FFARSBudgetResponse::class,
        Commands\getcoasegmentfeedback::class,
        Commands\getglaccountsqueuefeedback::class,
        Commands\getbudgetqueuefeedback::class,
        Commands\getglactualsummary::class,
        Commands\getffarsexpenditure::class,
        Commands\getffarsrevenue::class,
        Commands\resetAllCOA::class,
        Commands\getFfarsAccountBalances::class,
        Commands\ResolveMissingAccounts::class,
        Commands\ResolveRevenueDates::class,
        Commands\ResolveDuplicateRevenueTransactions::class,
        Commands\MatchEpicorRevenue::class,
        Commands\MatchFfarsRevenue::class,
        Commands\ResolveExpenditureDates::class,
        Commands\ReadFfarsAllocations::class,
        Commands\readMuseImplementations::class,
        Commands\SessionGcCommand::class


    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
