<?php

namespace App\Console\Commands;

use App\Http\Controllers\Report\BudgetImportMissingAccountsController;
use Illuminate\Console\Command;

class ResolveDuplicateRevenueTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'planrep:resolve-duplicate-revenue-transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes all duplicate revenue transactions from the DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        BudgetImportMissingAccountsController::removeRevenueDuplicates();
    }
}
