<?php

namespace App\Console\Commands;

use App\Jobs\ConsumeFromFacilityQueue;
use App\Jobs\ConsumeFromQueue;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use PhpAmqpLib\Connection\AMQPStreamConnection;


class FFARSFacilityVerification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ffars:facility';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listening to FFARS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private function consume()
    {

        $queue = "FACILITY_MATCH_REQUEST_QUEUE";

        /**
         * @param host address to rabbitmq server
         * @param port rabbitmq port
         * @param user rabbitmq user
         * @param password rabbitmq password
         */
        $server = Config::get("queue.connections.rabbitmq");
        $HOST = $server['host'];
        $PORT = $server['port'];;
        $USER = $server['username'];
        $PSWD = $server['password'];
        $VHOST = $server['vhost'];

        $connection = new AMQPStreamConnection($HOST, $PORT, $USER, $PSWD, $VHOST);
        $channel = $connection->channel();

        /**
         * creates a queue
         * @param string $queue queue name
         * @param bool $passive default false
         * @param bool $durable default false, required true
         * @param bool $exclusive default false
         * @param bool $auto_delete default true
         * @param bool $nowait default false
         * @param null $arguments default null
         * @param null $ticket default null
         * @return mixed|null
         */
        $channel->queue_declare($queue, false, true, false, false);
        echo ' [***] Waiting for data from the RabbitMQ server @', $HOST, ' To exit press CTRL+C', "\n";

        /**
         * this is where all the magic happens
         * when we get the data, we can send them into the queue so that they can be processed
         * later or do whatever whe want with it, the data is available in the $message variable
         */
        $callback = function ($message) {
            /*$properties = $message->get_properties();
            $applicationHeaders = $properties['application_headers'];
            $type = $applicationHeaders->getNativeData()['type'];*/
            dispatch(new ConsumeFromFacilityQueue($message->body));
            echo response()->json(['DATA-RECEIVED'=> "FACILITIES",'data'=>json_decode($message->body)])."\n";
        };

        /**
         * Starts a queue consumer
         *
         * @param string $queue
         * @param string $consumer_tag
         * @param bool $no_local
         * @param bool $no_ack
         * @param bool $exclusive
         * @param bool $nowait
         * @param callback|null $callback
         * @param int|null $ticket
         * @param array $arguments
         * @return mixed|string
         */
        $channel->basic_consume($queue, '', false, true, false, false, $callback);

        while (count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->consume();
    }
}
