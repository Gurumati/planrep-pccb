<?php

namespace App\Console\Commands;
use App\Models\Execution\ListenToRabbitMQ;
use Illuminate\Console\Command;

class getffarsrevenue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:getffarsrevenue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read actual revenue from ffars';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ListenToRabbitMQ::getFfarsActualRevenue();
    }
}
