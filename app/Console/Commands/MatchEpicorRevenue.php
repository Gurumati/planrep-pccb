<?php

namespace App\Console\Commands;

use App\Http\Controllers\Report\ReceivedFundMissingAccountsController;
use Illuminate\Console\Command;

class MatchEpicorRevenue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'planrep:match-epicor-revenue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Matches unknown revenue received from Epicor to the ones in PlanRep';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ReceivedFundMissingAccountsController::matchEpicorRevenue();
    }
}
