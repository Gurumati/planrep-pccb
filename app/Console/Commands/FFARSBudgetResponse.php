<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Execution\ListenToRabbitMQ;

class FFARSBudgetResponse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ffars-budget:feedback';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listen to FFARS budget responses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ListenToRabbitMQ::getFacilityBudgetFeedBack();
    }
}
