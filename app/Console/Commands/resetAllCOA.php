<?php

namespace App\Console\Commands;
use App\Http\Services\Budgeting\BudgetExportAccountService;
use Illuminate\Console\Command;

class resetAllCOA extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:resetAllCOA';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset all coa segments to accomodate changes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        BudgetExportAccountService::regenerateMissingAccounts();
    }
}
