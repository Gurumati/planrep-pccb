<?php

namespace App\Console\Commands;

use App\Http\Controllers\Report\BudgetImportMissingAccountsController;
use Illuminate\Console\Command;

class ResolveRevenueDates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'planrep:resolve-revenue-dates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command resolves the revenue items with the wrong financial year ids';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        BudgetImportMissingAccountsController::resolveRevenueYear();
    }
}
