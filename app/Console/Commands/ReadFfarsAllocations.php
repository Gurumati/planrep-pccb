<?php

namespace App\Console\Commands;

use App\Jobs\ConsumeDataFromQueueJob;
use App\Models\Setup\ConfigurationSetting;
use Illuminate\Console\Command;

class ReadFfarsAllocations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:read-ffars-allocations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command invokes rabbitMQ and reads allocations of funds from FFARS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        self::readFfarsAllocations();
    }

    private static function readFfarsAllocations(){
            $options = array('exchange' => 'FFARSALLOCATIONTOPLANREPExchange');
            $queue_name = ConfigurationSetting::getValueByKey('FFARS_ALLOCATION_TO_PLANREP_QUEUE_NAME');
            $action     = 'readFfarsAllocations';
           dispatch(new ConsumeDataFromQueueJob($queue_name, $options, $action));
    }
}
