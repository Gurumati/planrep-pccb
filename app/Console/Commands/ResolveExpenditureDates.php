<?php

namespace App\Console\Commands;

use App\Http\Controllers\Report\BudgetImportMissingAccountsController;
use Illuminate\Console\Command;

class ResolveExpenditureDates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'planrep:resolve-expenditure-dates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command reads the budget import items and matches them with the correct financial year';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        BudgetImportMissingAccountsController::resolveExpenditureDates();
    }
}
