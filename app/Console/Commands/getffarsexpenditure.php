<?php

namespace App\Console\Commands;
use App\Models\Execution\ListenToRabbitMQ;
use Illuminate\Console\Command;

class getffarsexpenditure extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:getffarsexpenditure';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listens to expenditures from ffars';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ListenToRabbitMQ::getFfarsActualExpenditure();
    }
}
