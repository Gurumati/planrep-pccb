<?php

namespace App\Console\Commands;

use App\Models\Execution\ListenToRabbitMQ;
use Illuminate\Console\Command;

class getglactualsummary extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:getglactualsummary';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Invokes PlanRep to consume actuals from Epicor though rabbitMQ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ListenToRabbitMQ::getGLActualSummary();
    }
}
