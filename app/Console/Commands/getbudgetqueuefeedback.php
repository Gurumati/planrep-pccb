<?php

namespace App\Console\Commands;

use App\Models\Execution\ListenToRabbitMQ;
use Illuminate\Console\Command;

class getbudgetqueuefeedback extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:getbudgetqueuefeedback';

    /**
     * Read feedback from the budget queue of rabbitMQ.
     *
     * @var string
     */
    protected $description = 'Get the feedback from the budget queue of rabbotMq';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ListenToRabbitMQ::getBudgetQueueFeedBack();
    }
}
