<?php

namespace App\Console\Commands;

use App\Models\Execution\ListenToRabbitMQ;
use Illuminate\Console\Command;

class getcoasegmentfeedback extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:getcoasegmentfeedback';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listen to rabbitMQ and get coa segment feedback';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ListenToRabbitMQ::getCoaSegmentFeedBack();
    }
}
