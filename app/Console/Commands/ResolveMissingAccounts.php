<?php

namespace App\Console\Commands;

use App\Http\Controllers\Report\BudgetImportMissingAccountsController;
use Illuminate\Console\Command;

class ResolveMissingAccounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'planrep:resolve-expenditure-gls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resolve missing GL Accounts in PlanRep';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        BudgetImportMissingAccountsController::match();
    }
}
