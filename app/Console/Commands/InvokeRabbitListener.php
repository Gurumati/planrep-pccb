<?php
/**
 * Created by PhpStorm.
 * User: milton
 * Date: 11/7/17
 * Time: 9:34 AM
 */


namespace App\Console\Commands;
use App\Jobs\Job;
use Illuminate\Console\Command;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use App\Jobs\ConsumeFromEpicor;

class InvokeRabbitListener extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:consume';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Invokes the RabbitMQ Consumer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    private function consume() {

        $queue = $this->ask('Enter the name of the Rabbit Queue');
        //$queue = 'GLActual_Queue_172.16.18.199';

        /**
         * @param host address to rabbitmq server
         * @param port rabbitmq port
         * @param user rabbitmq user
         * @param password rabbitmq password
         */
        $HOST     = config('app.epicor_rabbit_address');
        $PORT     = config('app.epicor_rabbit_port');
        $USERNAME = config('app.epicor_rabbit_username');
        $PASSWORD = config('app.epicor_rabbit_password');

        $connection = new AMQPStreamConnection($HOST, $PORT, $USERNAME, $PASSWORD);
        $channel    = $connection->channel();

        /**
         * creates a queue
         * @param string $queue queue name
         * @param bool $passive default false
         * @param bool $durable default false, required true
         * @param bool $exclusive default false
         * @param bool $auto_delete default true
         * @param bool $nowait default false
         * @param null $arguments default null
         * @param null $ticket default null
         * @return mixed|null
         */
        $channel->queue_declare($queue, false, true, false, false);
        echo ' [*] Waiting for data from the RabbitMQ server @', $HOST, ' To exit press CTRL+C', "\n";

        /**
         * this is where all the magic happens
         * when we get the data, we can send them into the queue so that they can be processed
         * later or do whatever whe want with it, the data is available in the $message variable
         */
        $callback = function($message){
            echo " [x] RECEIVED ", $message->body, "\n";
            echo " [x] DONE!...", "\n";
            dispatch(new ConsumeFromEpicor($message->body));
        };

        /**
         * Starts a queue consumer
         *
         * @param string $queue
         * @param string $consumer_tag
         * @param bool $no_local
         * @param bool $no_ack
         * @param bool $exclusive
         * @param bool $nowait
         * @param callback|null $callback
         * @param int|null $ticket
         * @param array $arguments
         * @return mixed|string
         */
        $channel->basic_consume($queue, '', false, true, false, false, $callback);

        while(count($channel->callbacks)) {
            $channel->wait();
        }

        $channel->close();
        $connection->close();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->consume();
    }
}