<?php

namespace App\Console\Commands;
use App\Jobs\ReadDataFromQueueJob;
use App\Models\Setup\ConfigurationSetting;
use App\Models\Execution\ListenToRabbitMQ;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class readMuseImplementations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rabbitmq:read-muse-implementations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command invokes rabbitMQ and reads Allocations,Expenditure and Revenues  from MUSE';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ListenToRabbitMQ::readMuseImplementations();
        //ListenToRabbitMQ::readMuseFeedback();
    }


}
