desc 'Clearing & Configuring Cache'
namespace :cache do
  task :clear do
    on roles(:app) do
      within release_path  do
        puts "Clearing & Configuring Cache"
        execute :php, "artisan cache:clear" # run migrations
        execute :php, "artisan config:clear" # run migrations
        execute :php, "artisan config:cache" # run migrations
        execute :php, "artisan route:clear" # run migrations
        execute :php, "artisan view:clear" # run migrations
        execute :composer, "dumpautoload" # run migrations
      end
    end
  end
end
