desc 'Log in to postgres database in production'
task :psql do
  on roles(:app) do |h|
    within release_path  do
      puts "running psql..."
      run_interactively "psql -U deploy -d ffars", h.user
    end
  end
end

def run_interactively(command, user)
  info "Running `#{command}` as #{user}@#{host}"
  exec %Q(ssh #{user}@#{host} -t "bash --login -c 'cd #{fetch(:deploy_to)}/current && #{command}'")
end
