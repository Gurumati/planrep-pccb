desc "Run npm install to enable node to automatically run"
  task :npminstall do
    on roles(:app) do
      within current_dir  do
        execute :npm, "install" # run the npm install command to install the node modules
      end
    end
  end