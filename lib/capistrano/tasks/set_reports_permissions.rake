desc "Set the permissions of the public directory to allow reports to be generated"
  task :setpermissions do
    on roles(:app) do
      within shared_path  do
        execute :chown, "-R deploy:apache public/reports" # make the public directory writable
      end

      within current_path  do
        execute :chown, "-R deploy:apache app/Http/Controllers/Report" # change the ownership of the reports directory
        execute :chmod, "-R 775 app/Http/Controllers/Report" # make the reports directory writable
        # execute :mkdir, "public/reports" # create the reports directory in the public folder of the project
        execute :chmod, "-R 775 public/reports" # make the reports directory writable
        execute :chown, "-R deploy:apache public/reports" # Set the permissions of the reports directory
      end
    end
  end
