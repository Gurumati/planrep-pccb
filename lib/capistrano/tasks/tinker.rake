desc 'Run tinker in production'
task :tinker do
  on roles(:app) do |h|
    within release_path  do
      puts "running php artisan tinker..."
      run_interactively "php artisan tinker", h.user
    end
  end
end
