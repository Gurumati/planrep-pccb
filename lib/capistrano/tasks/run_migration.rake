namespace :migration do
  desc 'Run php artistan migration'
  task :migrate do
    on roles(:app) do |h|
      within release_path  do
        puts "running migrations..."
        #execute :php, "artisan migrate" # run migrations
        run_interactively "php artisan migrate --force", h.user
        #execute :php, "artisan entrust:migration --no-interaction" # run migrations
      end
    end
  end

  desc 'check migration status'
  task :status do
    on roles(:app) do
      within release_path do
        puts 'checking migration status'
        execute :php, "artisan migrate:status"
      end
    end
  end
end
