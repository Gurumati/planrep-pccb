desc 'Composer tasks'
namespace :composer do
  desc "Clear & Dump the autoload files"
  task :dumpautoload do
    on roles(:app) do
      within release_path  do
        execute :composer, "dumpautoload" # run migrations
      end
    end
  end

  desc "Clear & Configure Composer Internal Cache"
  task :clearcache do
    on roles(:app) do
      within release_path  do
        execute :composer, "clearcache" # run migrations
      end
    end
  end

  desc "composer dump-autoload and clear cache"
  task :all  =>  [:dumpautoload, :clearcache]
end
