desc 'Run tinker in production'
task :config_seed do
  on roles(:app) do |h|
    within release_path  do
      puts "running php artisan tinker..."
#       run_interactively "php artisan db:seed --class=ConfigurationsSeeder", h.user
      run_interactively "php artisan db:seed", h.user
    end
  end
end
