(function (angular) {
    'use strict';
    angular.module('angularTreeview', [])
        .directive('treeModel', ['$compile', function ($compile) {
        return {
            restrict: 'A', link: function (scope, element, attrs) {
                var treeId = attrs.treeId;
                var treeModel = attrs.treeModel;
                var nodeId = attrs.nodeId;
                var nodeLabel = attrs.nodeLabel;
                var nodeChildren = attrs.nodeChildren;
                var searchQuery = attrs.searchQuery;
                var sortOrder = "'"+nodeLabel+"'";
                var template = '<ul>' + '<li data-ng-repeat="node in ' + treeModel + ' | filter:' + searchQuery + '  ">' + '<i class="collapsed" data-ng-class="{nopointer: !node.' + nodeChildren + '.length}"' + 'data-ng-show="!node.expanded && !node.fileicon" data-ng-click="' + treeId + '.selectNodeHead(node)"></i>' + '<i class="expanded" data-ng-show="node.expanded && !node.fileicon" data-ng-click="' + treeId + '.selectNodeHead(node)"></i>' + '<i class="normal" data-ng-show="node.fileicon"></i> ' +
                    '<span title="{{::node.' + nodeLabel + '}}" data-ng-class="node.selected" data-ng-click="' + treeId + '.selectNodeLabel(node)">{{node.' + nodeLabel + '}}</span>' +
                    '&nbsp;<span data-ng-bind-html="getIcon(node)" ></span>'+
                    '<div data-ng-show="node.expanded" data-tree-id="' + treeId + '" data-tree-model="node.' + nodeChildren + '" data-node-id=' + nodeId + ' data-node-label=' + nodeLabel + ' data-node-children=' + nodeChildren + ' data-search-query=' + searchQuery + '></div>' + '</li>' + '</ul>';
                if (treeId && treeModel) {
                    if (attrs.angularTreeview) {
                        scope[treeId] = scope[treeId] || {};
                        scope[treeId].selectNodeHead = scope[treeId].selectNodeHead || function (selectedNode) {
                                if (selectedNode[nodeChildren] !== undefined) {
                                    selectedNode.expanded = !selectedNode.expanded;
                                }
                            };
                        scope[treeId].selectNodeLabel = scope[treeId].selectNodeLabel || function (selectedNode) {
                                if (scope[treeId].currentNode && scope[treeId].currentNode.selected) {
                                    scope[treeId].currentNode.selected = undefined;
                                }
                                selectedNode.selected = 'selected';
                                scope[treeId].currentNode = selectedNode;
                            };
                    }
                    scope.getIcon=function (node) {
                        if(node.cas_plan_table_d_t_o !== undefined && node.cas_plan_table_d_t_o.length >0){
                            if(node.cas_plan_table_d_t_o[0].type === 1 && node.cas_plan_table_d_t_o[0].report_url === null){
                                return '<i class="fa fa-upload text-info"></i>';
                            }else if(node.cas_plan_table_d_t_o[0].type === 1 && node.cas_plan_table_d_t_o[0].report_url !==null) {
                                return '<i class="fa fa-file-pdf-o" style="color: #a21309"></i>';
                            }
                            else if(node.cas_plan_table_d_t_o[0].type === 2) {
                                return '<i class="fa fa-table text-info"></i>';
                            }

                        }else {
                            return '';
                        }
                    };
                    element.html('').append($compile(template)(scope));
                }
            }
        };
    }])
        .directive('treeModelScrutinization', ['$compile', function ($compile) {
        return {
            restrict: 'A', bindToController: true, link: function (scope, element, attrs) {
                var treeId = attrs.treeId;
                var treeModel = attrs.treeModelScrutinization;
                var nodeId = attrs.nodeId;
                var nodeLabel = attrs.nodeLabel;
                var nodeChildren = attrs.nodeChildren;
                var submittedBudgets = attrs.nodeSubmitted;
                var budgetingSections = attrs.budgetingSections;
                var searchQuery = attrs.searchQuery;
                var mtefSectionDecisions=attrs.nodeMtefSectionsDecisions;
                var scrutinStatuses=attrs.scrutinStatuses;
                var selectedAdminHierarchyId=attrs.selectedAdminHierarchyId;
                var nodeClick= attrs.nodeClick;
                var sortOrder = "'"+nodeLabel+"'";
                var expanded=attrs.expanded;
                var template = '<ul>' +
                      '<li data-ng-init="node.expanded=true" data-ng-repeat="node in ' + treeModel + ' | filter:' + searchQuery + ' | orderBy:' + sortOrder + ' ">' +
                      '<i class="collapsed" data-ng-class="{nopointer: !node.' + nodeChildren + '.length}"' + 'data-ng-if="!node.expanded && !node.fileicon" data-ng-click="' + treeId + '.selectNodeHead(node)"></i>' +
                      '<i class="expanded" data-ng-if="node.expanded && !node.fileicon" data-ng-click="' + treeId + '.selectNodeHead(node)"></i>' +
                      '<i class="normal" data-ng-if="node.fileicon"></i> ' +
                      '<span data-ng-if="!inBudgetingSection(node.id) || !inSubmitted(node.id)" data-ng-class="node.selected"  class="can_not_budget" >' +
                      ' {{::node.' + nodeLabel + '}} ' +
                      '<span data-ng-if="inBudgetingSection(node.id) && !inSubmitted(node.id)"> <i  class="fa fa-times" style="color:#ff2116;">&nbsp;</i>' +
                      '   <span data-ng-bind-html="getStatus(node.id)" ></span></span>'+
                      '</span>' +
                      '<span data-ng-if="inBudgetingSection(node.id) && inSubmitted(node.id)"  data-ng-class="node.selected"  class="can_budget" data-ng-click="' + nodeClick + '(node)">' +
                      '     {{::node.' + nodeLabel + '}}<i  class="fa fa-check" style="color:#14ff5b"></i>' +
                      '   <i class="sDecisionLevel">{{::getDecisionLevel(node.id)}})&nbsp;</i>'+ '<span data-ng-bind-html="getStatus(node.id)" ></span>'+
                      '</span>' +
                      '<div data-ng-show="node.expanded" data-expanded="false" data-node-click="'+nodeClick+'" data-tree-id="' + treeId + '" data-node-submitted="'+submittedBudgets+'" data-budgeting-sections="'+budgetingSections+'" data-tree-model-scrutinization="node.' + nodeChildren + '" data-node-id=' + nodeId + ' data-node-label=' + nodeLabel + ' data-node-children=' + nodeChildren + ' data-search-query=' + searchQuery + '></div>' +
                      '</li>' + '</ul>';
                if (treeId && treeModel) {
                    if (attrs.angularTreeview) {
                        scope[treeId] = scope[treeId] || {};
                        scope[treeId].selectNodeHead = scope[treeId].selectNodeHead || function (selectedNode) {
                                if (selectedNode[nodeChildren] !== undefined) {
                                    selectedNode.expanded = !selectedNode.expanded;
                                }
                            };
                        scope.inSubmitted=function (id) {
                            var inSubmitted=_.findWhere(JSON.parse(submittedBudgets),{section_id:id});
                            if(inSubmitted !== undefined)
                                return true;
                            else
                                return false;
                        };
                        scope.inBudgetingSection=function (id) {
                            var inBudgeting=_.findWhere(JSON.parse(budgetingSections),{section_id:id});
                            if(inBudgeting !== undefined)
                                return true;
                            else
                                return false;
                        };
                        scope.getDecisionLevel=function (id) {
                            var dcl=_.findWhere(JSON.parse(mtefSectionDecisions),{id:id});
                            if(dcl){
                                return dcl.decisionLevel;
                            }else
                                // return "NO_BUDGET";
                                return " ";
                        };
                        scope.getStatus=function (id) {
                            var s=_.findWhere(JSON.parse(scrutinStatuses),{section_id:id,admin_hierarchy_id:parseInt(selectedAdminHierarchyId)});
                            if(s !== undefined && s.status ==0){
                                return '&nbsp;&nbsp;&nbsp;<i style="color:mediumseagreen">In progress..</i>';
                            }else
                                if(s !== undefined &&s.status ==1)
                                {
                                    var recommendation='';
                                    if(s.recommendation ==-1){
                                        recommendation='&nbsp;&nbsp;&nbsp;<i style="color:red">To be return</i>';
                                    }else if(s.recommendation == 1){
                                         recommendation='&nbsp;&nbsp;&nbsp;<i style="color:#14ff5b">Passed</i>';
                                    }
                                    return '&nbsp;&nbsp;&nbsp;<i style="color:#1E88E5">Done</i>-'+recommendation;
                                }
                            else{
                                return '&nbsp;&nbsp;&nbsp;<i style="color:#cccccc">not stated</i>';
                            }
                        };
                        scope[treeId].selectNodeLabel = scope[treeId].selectNodeLabel || function (selectedNode) {
                                if (scope[treeId].currentNode && scope[treeId].currentNode.selected) {
                                    scope[treeId].currentNode.selected = undefined;
                                }
                                selectedNode.selected = 'selected';
                                scope[treeId].currentNode = selectedNode;
                            };
                    }
                    element.html('').append($compile(template)(scope));
                }
            }
        };
    }])
        .directive('treeModelAdminScrutinization', ['$compile', function ($compile) {
        return {
            restrict: 'A', bindToController: true, link: function (scope, element, attrs) {
                var treeId = attrs.treeId;
                var treeModel = attrs.treeModelAdminScrutinization;
                var nodeId = attrs.nodeId;
                var nodeLabel = attrs.nodeLabel;
                var nodeChildren = attrs.nodeChildren;
                var submittedBudgets = attrs.nodeSubmittedHierarchies;
                var budgetingLevels = attrs.nodeBudgetingLevels;
                var inUseLevels = attrs.nodeInUseLevels;
                var scrutinStatuses=attrs.scrutinStatuses;
                var allSections=attrs.allSections;
                var searchQuery = attrs.searchQuery;
                var mtefs=attrs.nodeMtefs;
                var nodeClick= attrs.nodeClick;
                var sortOrder = "'"+nodeLabel+"'";
                var expanded=attrs.expanded;
                var template = '<ul>' +
                      '<li data-ng-if="inUse(node.admin_hierarchy_level_id)" data-ng-init="node.expanded='+expanded+'" data-ng-repeat="node in ' + treeModel + ' | filter:' + searchQuery + ' | orderBy:' + sortOrder + ' ">' +
                      '<i class="collapsed" data-ng-class="{nopointer: !node.' + nodeChildren + '.length}"' + 'data-ng-if="!node.expanded && !node.fileicon" data-ng-click="' + treeId + '.selectNodeHead(node)"></i>' +
                      '<i class="expanded" data-ng-if="node.expanded && !node.fileicon" data-ng-click="' + treeId + '.selectNodeHead(node)"></i>' +
                      '<i class="normal" data-ng-if="node.fileicon"></i> ' +
                      '<span data-ng-if="!inBudgeting(node.admin_hierarchy_level_id) || !inSubmitted(node.id)" class="can_not_budget"   ' +
                          ' data-ng-class="node.selected"  class="last-child not-submitted" >' +
                      '{{::node.' + nodeLabel + '}}' +
                      '<span data-ng-if="inBudgeting(node.admin_hierarchy_level_id) && !inSubmitted(node.id)"><i  class="fa fa-times" style="color:#ff2116;"></i>' +
                      '<span data-ng-bind-html="getStatus(node.id)" ></span></span>'+
                      '</span>' +
                      '<span data-ng-if="inBudgeting(node.admin_hierarchy_level_id) && inSubmitted(node.id)" class="can_budget"   ' +
                          ' data-ng-class="node.selected" data-ng-click="' + nodeClick + '(node)">{{::node.' + nodeLabel + '}}' +
                          '<i  class="fa fa-check" style="color:#14ff5b"></i>' +
                          '<i class="sDecisionLevel">({{::getDecisionLevel(node.id)}})</i>'+'<span data-ng-bind-html="getStatus(node.id)" ></span>'+
                        '</span>' +
                      '<div data-expanded="true" data-ng-if="node.expanded" data-node-click="'+nodeClick+'" data-tree-id="' + treeId + '" data-node-submitted-hierarchies="'+submittedBudgets+'" data-node-budgeting-levels="'+budgetingLevels+'" data-tree-model-admin-scrutinization="node.' + nodeChildren + '" data-node-id=' + nodeId + ' data-node-label=' + nodeLabel + ' data-node-children=' + nodeChildren + ' data-search-query=' + searchQuery + '></div>' +
                      '</li>' + '</ul>';
                if (treeId && treeModel) {
                    if (attrs.angularTreeview) {
                        scope[treeId] = scope[treeId] || {};
                        scope[treeId].selectNodeHead = scope[treeId].selectNodeHead || function (selectedNode) {
                                if (selectedNode[nodeChildren] !== undefined) {
                                    selectedNode.expanded = !selectedNode.expanded;
                                }
                            };
                        scope.inSubmitted=function (id) {
                            var inSubmitted=_.findWhere(JSON.parse(submittedBudgets),{admin_hierarchy_id:id});
                            if(inSubmitted !== undefined)
                                return true;
                            else
                                return false;
                        };
                        scope.inBudgeting=function (id) {
                            var inBudgeting=_.findWhere(JSON.parse(budgetingLevels),{admin_hierarchy_level_id:id});
                            if(inBudgeting !== undefined)
                                return true;
                            else
                                return false;
                        };
                        scope.inUse=function (id) {
                            var isInUse=_.findWhere(JSON.parse(inUseLevels),{admin_hierarchy_level_id:id});
                            if(isInUse !== undefined)
                                return true;
                            else
                                return false;
                        };
                        scope.getDecisionLevel=function (id) {
                            var dcl=_.findWhere(JSON.parse(mtefs),{admin_hierarchy_id:id});
                            if(dcl){
                                 return dcl.decision_level.name;
                            }else
                                return "NO_BUDGET";
                        };
                        scope.getStatus=function (id) {
                            var all=parseInt(allSections,10);
                            var inProgress=_.where(JSON.parse(scrutinStatuses),{admin_hierarchy_id:id,status:0});
                            var done=_.where(JSON.parse(scrutinStatuses),{admin_hierarchy_id:id,status:1});
                            var passed=_.where(JSON.parse(scrutinStatuses),{admin_hierarchy_id:id,status:1,recommendation:1});
                            var returned=_.where(JSON.parse(scrutinStatuses),{admin_hierarchy_id:id,status:1,recommendation:-1});
                            var result='';


                            result += '&nbsp;&nbsp;&nbsp;<i style="padding:2px 10px;color:#cccccc">('+(all-inProgress.length-done.length)+')Not stated</i>';
                            if(inProgress.length)
                            result += '&nbsp;&nbsp;&nbsp;<i style="padding:2px 10px;color:mediumseagreen">('+inProgress.length+')In progress..</i>';
                            if(done.length)
                            result += '&nbsp;&nbsp;&nbsp;<i style="padding:2px 10px;color:#1E88E5">('+done.length+')Done</i>';
                            if(passed.length)
                            result += '&nbsp;&nbsp;&nbsp;<i style="padding:2px 10px;color:#14ff5b">('+passed.length+')Passed</i>';
                            if(returned.length)
                            result += '&nbsp;&nbsp;&nbsp;<i style="padding:2px 10px;color:red">('+returned.length+')To be returned</i>';

                            return result;
                        };
                        scope[treeId].selectNodeLabel = scope[treeId].selectNodeLabel || function (selectedNode) {
                                if (scope[treeId].currentNode && scope[treeId].currentNode.selected) {
                                    scope[treeId].currentNode.selected = undefined;
                                }
                                selectedNode.selected = 'selected';
                                scope[treeId].currentNode = selectedNode;
                            };
                    }
                    element.html('').append($compile(template)(scope));
                }
            }
        };
    }])
        .directive('treeModelBudget', ['$compile', function ($compile) {
        return {
            restrict: 'A', bindToController: true, link: function (scope, element, attrs) {
                var treeId = attrs.treeId;
                var treeModel = attrs.treeModelBudget;
                var nodeId = attrs.nodeId;
                var nodeLabel = attrs.nodeLabel;
                var nodeChildren = attrs.nodeChildren;
                var searchQuery = attrs.searchQuery;
                var nodeClick= attrs.nodeClick;
                var sortOrder = "'"+nodeLabel+"'";
                var template = '<ul>' +
                      '<li  data-ng-repeat="node in ' + treeModel + ' | filter:' + searchQuery + ' | orderBy:' + sortOrder + ' ">' +
                      '<i class="collapsed" data-ng-class="{nopointer: !node.' + nodeChildren + '.length}"' + 'data-ng-show="!node.expanded && !node.fileicon" data-ng-click="' + treeId + '.selectNodeHead(node)"></i>' +
                      '<i class="expanded" data-ng-show="node.expanded && !node.fileicon" data-ng-click="' + treeId + '.selectNodeHead(node)"></i>' +
                      '<i class="normal" data-ng-show="node.fileicon"></i> ' +
                      '<span data-ng-class="{\'has-child\':node.'+nodeChildren+'.length > 0}" title="{{node.' + nodeLabel + '}}" ' +
                          'data-ng-class="node.selected" data-ng-click="' + nodeClick + '(node)">' +
                          '<i data-ng-show="node.type==\'target\'" class="fa fa-list"></i> ' +
                          '<i data-ng-show="node.type==\'planChain\'" class="fa fa-clipboard"></i> ' +
                          '<i data-ng-show="node.type==\'activity\'" class="fa fa-list-ol"></i> ' +
                          '{{node.' + nodeLabel + '}}</span>' +
                      '<div data-ng-show="node.expanded" data-node-click="'+nodeClick+'" data-tree-id="' + treeId + '" data-tree-model-budget="node.' + nodeChildren + '" data-node-id=' + nodeId + ' data-node-label=' + nodeLabel + ' data-node-children=' + nodeChildren + ' data-search-query=' + searchQuery + '></div>' +
                      '</li>' + '</ul>';
                if (treeId && treeModel) {
                    if (attrs.angularTreeview) {
                        scope[treeId] = scope[treeId] || {};
                        scope[treeId].selectNodeHead = scope[treeId].selectNodeHead || function (selectedNode) {
                                if (selectedNode[nodeChildren] !== undefined) {
                                    selectedNode.expanded = !selectedNode.expanded;
                                }
                            };
                        scope[treeId].selectNodeLabel = scope[treeId].selectNodeLabel || function (selectedNode) {
                                if (scope[treeId].currentNode && scope[treeId].currentNode.selected) {
                                    scope[treeId].currentNode.selected = undefined;
                                }
                                selectedNode.selected = 'selected';
                                scope[treeId].currentNode = selectedNode;
                            };
                    }
                    element.html('').append($compile(template)(scope));
                }
            }
        };
    }])
        .directive('planChainTree', ['$compile', function ($compile) {
        return {
            restrict: 'A', bindToController: true, link: function (scope, element, attrs) {
                var treeModel = attrs.planChainTree;
                var x = attrs.chainModel;
              //  if(typeof(treeModel))
                console.log(treeModel);
                console.log(treeModel[0]);
                var template =
                    '<div class="col-md-4" >' +
                      '<select    data-ng-model="planChain['+x+']" data-ng-options="p as p.name for p in '+ treeModel +'">' +
                      '</select>' +
                      '</div>' +
                    '<div data-ng-if="planChain['+x+'].children.length >0" data-chain-model="so" data-plan-chain-tree="planChain['+x+'].children"></div>' ;
                if (true) {
                    element.html('').append($compile(template)(scope));
                }
                // scope.loadChild=function (planchain)
                // {
                //     scope.planChainId=planchain.name;
                // }
            }
        };
    }]);
})(angular);
