var report = angular.module('report-module', ['master-module', 'ngRoute', 'reportServices' ,'gridshore.c3js.chart']);

report.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {
        controller: ReportController,
        templateUrl: '/pages/dashboard/dashboard.html',
        resolve: ReportController.resolve,
        rights:['report']
    }).when('/cas-preview', {
        controller: ComprehensivePlanPreviewController,
        templateUrl: '/pages/report/comprehensive_plans/comprehensive-plan-preview.html',
        resolve: ComprehensivePlanPreviewController.resolve,
        rights:['report']
    }).when('/pe-report', {
        controller: PeSubmissionFormsReportController,
        templateUrl: '/pages/report/pe_submission_report/index.html',
        resolve: PeSubmissionFormsReportController.resolve,
        rights:['report']
    }).when('/printouts', {
        controller: PrintOutController,
        templateUrl: '/pages/report/printout/index.html',
        resolve: PrintOutController.resolve,
        rights:['report']
    }).when('/projections', {
        controller: ProjectionController,
        templateUrl: '/pages/report/projection/index.html',
        resolve: ProjectionController.resolve,
        rights:['report']
    }).when('/ceilings', {
        controller: CeilingController,
        templateUrl: '/pages/report/ceiling/index.html',
        resolve: CeilingController.resolve,
        rights:['report']
    }).when('/bar-chart', {
        controller: BarChartController,
        templateUrl: '/pages/report/bar-charts/index.html',
        resolve: BarChartController.resolve,
        rights:['report']
    }).when('/procurement-plan', {
        controller: ProcurementController,
        templateUrl: '/pages/report/procurement/index.html',
        resolve: ProcurementController.resolve,
        rights:['report']
    }).when('/budget-aggregations', {
        controller: BudgetAggregationController,
        templateUrl: '/pages/report/budgetAggregation/index.html',
        resolve: BudgetAggregationController.resolve,
        rights:['report']
    }).when('/facility-status-reports', {
        controller: FacilityStatusReportController,
        templateUrl: '/pages/report/facility/index.html',
        resolve: FacilityStatusReportController.resolve,
        rights:['report']
    }).when('/budget-summary', {
        controller: BudgetSummaryController,
        templateUrl: '/pages/report/budget_aggregation/summary.html',
        resolve: BudgetSummaryController.resolve,
        rights:['report']
    }).when('/cdr', {
        controller: CdrController,
        templateUrl: '/pages/report/cdr.html',
        resolve: CdrController.resolve,
        rights:['report.cdr']
    }).when('/consolidated-cdr-report', {
        controller: CdrConsolidatedController,
        templateUrl: '/pages/report/cdr/consolidated-cdr.html',
        resolve: CdrConsolidatedController.resolve,
        rights:['report.cdr']
    }).otherwise({redirectTo: '/'});
}]);
