var reportsService = angular.module('reportServices', ['ngResource']);

reportsService.factory('CasPlanService', function ($resource) {
    return $resource('/json/casPlans', {}, {});
});

reportsService.factory('PeriodsByFinancialYearService', function ($resource) {
    return $resource('/json/periodsByFinancialYear/:id', {id:'@id'}, {});
});

reportsService.factory('CasPlanContentsService', function ($resource) {
    return $resource('/json/casPlanContents/:cas_plan_id', {cas_plan_id: '@cas_plan_id'}, {});
});

reportsService.factory('CasPlanTableItemValueReportService', function ($resource) {
    return $resource('/json/casPlanTableItemValueReport', {table_id: '@table_id', admin_hierarchy_id:'@admin_hierarchy_id'}, {});
});

reportsService.factory('PostCasPlanTableItemValueService', function ($resource) {
    return $resource('/json/PostCasPlanTableItemValue', {}, {store: {method: 'POST'}});
});

reportsService.factory('PeFundSourcesService', function ($resource) {
    return $resource('/json/peFundSources', {}, {getPeFundSources:{method:'POST'}});
});

reportsService.factory('GeneratePeReportService', function ($resource) {
    return $resource('/json/peBudgetSubmissionFormReport', {}, {getReport:{method:'POST'}});
});

//PE forms report
reportsService.factory('PESubmissionFormReport', ['$resource', function ($resource) {
    return $resource('/json/pe_submission_report', {}, {
        paginated: {method: 'GET', params:{form_id: '@form_id',budget_class: '@budget_class'}, url: '/json/pe_submission_report/paginated'},
        peReports: {method: 'GET', url:'json/pe_submission_report/all'}
    });
}]);

//PRINTOUTS
reportsService.factory('PrintOutService', ['$resource', function ($resource) {
    return $resource('/json/reports/:id', {}, {
        fetchAll: {method: 'GET', url: '/json/reports/tree'},
    });
}]);

reportsService.factory('BarChartService', ['$resource', function ($resource) {
    return $resource('/json/reports/bar-chart', {}, {
        report_data: {method: 'GET', params:{report_name: '@report_name',id: '@id'}, url: '/json/reports/bar-chart'},
    });
}]);

//PROJECTIONS
reportsService.factory('ProjectionService', ['$resource', function ($resource) {
    return $resource('/report/projections/:id', {}, {
        projections: {method: 'GET', url: '/report/projections/fetchAll'},
        hierarchy_projections: {method: 'GET', url: '/report/projections/hierarchy_projections'},
        hierarchyYearProjections: {method: 'GET', url: '/report/projections/hierarchyYearProjections'},
    });
}]);

//CEILINGS
reportsService.factory('CeilingService', ['$resource', function ($resource) {
    return $resource('/report/ceilings/:id', {}, {
        ceilings: {method: 'GET', url: '/report/ceilings/fetchAll'},
        hierarchy_ceilings: {method: 'GET', url: '/report/ceilings/hierarchy_ceilings'},
        hierarchyYearCeilings: {method: 'GET', url: '/report/ceilings/hierarchyYearCeilings'},
    });
}]);

//PROCUREMENT
reportsService.factory('ProcurementService', ['$resource', function ($resource) {
    return $resource('/report/procurements/:id', {}, {
        paginated: {method: 'GET', url: '/report/procurements/paginated'},
        breakdown: {method: 'GET', url: '/json/procurements/:id/items'},
    });
}]);

//Service to pull all admin hierarchies
reportsService.factory('GetAllAdminHierarchiesService', ['$resource', function ($resource) {
    return $resource('/report/procurements/:id', {}, {
        paginated: {method: 'GET', url: '/report/procurements/paginated'},
    });
}]);

//PERFORMANCE INDICATOR ACTUAL VALUES
reportsService.factory('PerformanceIndicatorFinancialYearActualValueService', ['$resource', function ($resource) {
    return $resource('/report/performanceIndicatorFinancialYearValues/:id', {}, {
        paginated: {method: 'GET', url: '/report/performanceIndicatorFinancialYearValues/paginated'},
        update: {method: 'PUT', params: {id: '@id'}},
    });
}]);



//REPORT
reportsService.factory('BudgetAggregationService', ['$resource', function ($resource) {
    return $resource('/report/budgetAggregations/:id', {}, {
        summary: {method: 'GET', url: '/report/budgetAggregations/summary'},
        summaryV2: {method: 'GET', url: '/report/budgetAggregations/summaryV2'},
    });
}]);

//FACILITY STATUS REPORT
reportsService.factory('FacilityStatusReportService', ['$resource', function ($resource) {
    return $resource('/report/facilityStatusReports/:id', {}, {
        summary: {method: 'GET', url: '/report/facilityStatusReports/summary'}
    });
}]);

//The service to get all the cas contents and their tables
reportsService.factory('GetCasPlanContentService', function ($resource) {
    return $resource('/json/generateCasPlanToDownload/:id', {id:'@id'}, {getCompPlanContent:{method:'GET'}});
});

//The service to get all the urls of the cas plan table details
reportsService.factory('downloadCasPlanService', function ($resource) {
    return $resource('/json/downLoadCasPlanInSingleFile', {}, {generateSingleDocument:{method:'POST'}});
});