function PeSubmissionFormsReportController($scope,
                                           $filter,
                                           PESubmissionFormReport,
                                           PeFundSourcesService,
                                           GeneratePeReportService,
                                           getJasperCasReportService,
                                           GetAllFinancialYearService,
                                           LevelAdminHierarchyService
){
    $scope.title = "Personal Emoluments Report";
    $scope.printNow = false;
    $scope.showPrintButton = false;
    $scope.hasVerticalTotal = false;
    $scope.verticalTotal = [];

    LevelAdminHierarchyService.query(function (data) {
        $scope.adminHierarchies = data;
    });

    PeFundSourcesService.query({}, function (data) {
        $scope.peFundSources = data;
    });

    GetAllFinancialYearService.get({}, function (data) {
        $scope.financialYears = data.data;
    });

    PESubmissionFormReport.peReports({}, function (data) {
        $scope.budgetSubmissionSubForms = data.reports;
    });


    $scope.generateReport = function() {
        $scope.reportUrl = '';
        $scope.peReport  = false;
        var report_url = $scope.reports.sql_query;
        var data = {reportParameter: $scope.reportParameter, report_url: report_url, report_type: 'pdf'};
        //send report parameters
        getJasperCasReportService.casReport(data, function (data) {
            $scope.reportUrl = data.reportUrl;
            $scope.peReport = true;
        });
    };

};
