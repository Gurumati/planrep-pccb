function PrintOutController($scope, PrintOutModel, $uibModal,ConfirmDialogService,PrintOutService) {

    $scope.printouts = PrintOutModel;
    $scope.printoutsCopy = angular.copy(PrintOutModel);
    if ($scope.printouts.length > 0){
        $scope.printouts[0].expanded = true;
    }
    $scope.title = "PRINTOUTS";

    $scope.generate = function (printout) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/report/printout/generate.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, PrintOutService) {

                $scope.createDataModel = {};

                $scope.name = printout.name;

                $scope.export_data = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    PrintOutService.save($scope.createDataModel, function (data) {
                            $uibModalInstance.close(data);
                        }, function (error) {
                            console.log(error);
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

PrintOutController.resolve = {
    PrintOutModel: function (PrintOutService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            PrintOutService.query({}, function (data) {
                deferred.resolve(data);
            });
        }, 900);
        return deferred.promise;
    }
};