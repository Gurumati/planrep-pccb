function CdrController($scope, FinancialYearModel, GlobalService,LevelAdminHierarchyService) {
    $scope.financialYears = FinancialYearModel;
    $scope.title = "CDR";
    $scope.loadAdminHierarchies = function (financialYearId) {
        $scope.financialYearId = financialYearId;
        LevelAdminHierarchyService.query(function (response) {
            $scope.adminHierarchies = response;
            $scope.loadItems = function (adminHierarchyId) {
                $scope.currentPage = 1;
                $scope.maxSize = 3;
                $scope.perPage = 10;
                $scope.adminHierarchyId = adminHierarchyId;

                GlobalService.cdr({
                    financialYearId: $scope.financialYearId,
                    adminHierarchyId: adminHierarchyId,
                    page: $scope.currentPage,
                    perPage: $scope.perPage
                }, function (data) {
                    $scope.items = data.items;
                }, function (response) {
                    console.log(response);
                });

                $scope.pageChanged = function () {
                    GlobalService.cdr({
                        financialYearId: $scope.financialYearId,
                        adminHierarchyId: adminHierarchyId,
                        page: $scope.currentPage,
                        perPage: $scope.perPage
                    }, function (data) {
                        $scope.items = data.items;
                    }, function (response) {
                        console.log(response);
                    });
                };
            };
        }, function (response) {
            console.log(response.data);
        });
           
    };
}

CdrController.resolve = {
    FinancialYearModel: function (AllFinancialYearService, $timeout, $q) {
        let deferred = $q.defer();
        $timeout(function () {
            AllFinancialYearService.query(function (data) {
                deferred.resolve(data);
            }, function (response) {
                console.log(response);
            });
        }, 900);
        return deferred.promise;
    }
};
