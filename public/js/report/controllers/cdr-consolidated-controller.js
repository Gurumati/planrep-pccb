function CdrConsolidatedController($scope, FinancialYearModel, GlobalService,RegionByLevelService) {
    $scope.financialYears = FinancialYearModel;
    $scope.title = "CONSOLIDATED CDR REPORT";

    $scope.loadRegions = function (financialYearId) {

        $scope.financialYearId = financialYearId;
        RegionByLevelService.query(function (data) {

            $scope.adminHierarchies = data;
            $scope.loadItems = function (regionId) {
                $scope.currentPage = 1;
                $scope.maxSize = 3;
                $scope.perPage = 10;

                GlobalService.consolidatedCdr({
                    financialYearId: financialYearId,
                    regionId:regionId,
                    page: $scope.currentPage,
                    perPage: $scope.perPage
                }, function (data) {
                    $scope.items = data.items;
                }, function (response) {
                });

                $scope.pageChanged = function () {
                    GlobalService.consolidatedCdr({
                        financialYearId: $scope.financialYearId,
                        page: $scope.currentPage,
                        perPage: $scope.perPage
                    }, function (data) {
                        $scope.items = data.items;
                    }, function (response) {
                    });
                };
            };
        }, function (response) {
        });
    };
}

CdrConsolidatedController.resolve = {
    FinancialYearModel: function (AllFinancialYearService, $timeout, $q) {
        let deferred = $q.defer();
        $timeout(function () {
            AllFinancialYearService.query(function (data) {
                deferred.resolve(data);
            }, function (response) {
            });
        }, 900);
        return deferred.promise;
    }
};
