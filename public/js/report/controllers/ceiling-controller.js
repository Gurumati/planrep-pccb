function CeilingController($scope, LevelAdminHierarchyService, GlobalService, CeilingService) {

    //$scope.ceilings = CeilingModel;
    $scope.title = "CEILINGS";

    LevelAdminHierarchyService.query(function (data) {
        $scope.admin_hierarchies = data;
    });

    GlobalService.financialYears(function (data) {
        $scope.financialYears = data.financialYears;
    });

    $scope.printHeader = false;

    $scope.printDiv = function (divName) {
        $scope.printHeader = true;
        var printContents = document.getElementById(divName).innerHTML;
        var popupWin = window.open('', '_blank', 'width=1400,height=600');
        popupWin.document.open();
        popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="/css/print.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
        popupWin.document.close();
    };

    $scope.hierarchyCeilings = function (admin_hierarchy_id) {
        CeilingService.hierarchy_ceilings(
            {
                admin_hierarchy_id: admin_hierarchy_id
            }, function (data) {
                $scope.ceilings = data.ceilings;
            }
        );
    };

    $scope.hierarchyYearCeilings = function (admin_hierarchy_id, financial_year_id) {
        CeilingService.hierarchyYearCeilings(
            {
                admin_hierarchy_id: admin_hierarchy_id,
                financial_year_id: financial_year_id
            }, function (data) {
                $scope.ceilings = data.ceilings;
            }
        );
    };

    $scope.totalAmount = function () {
        var total = 0;
        angular.forEach($scope.ceilings, function (value, key) {
            total += parseFloat(value.amount);
        });
        $scope.total_amount = total;
    };
    $scope.$watch('ceilings', function () {
        $scope.totalAmount();
    });

    $scope.getTotalGroupBy = function (values) {
        var total = 0;
        if (values !== undefined && values.length > 0) {
            for (var i = 0; i < values.length; i++) {
                var item = values[i];
                total += parseFloat(item.amount);
            }
        }
        return total;
    };

}

/*
CeilingController.resolve = {
    CeilingModel: function (CeilingService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            CeilingService.ceilings(function (data) {
                deferred.resolve(data.ceilings);
            });
        }, 900);
        return deferred.promise;
    }
};*/
