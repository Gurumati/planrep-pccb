function FacilityStatusReportController($scope, DataModel,FacilityStatusReportService,GlobalService,LevelAdminHierarchyService) {

    $scope.items = DataModel;
    $scope.title = "FACILITY_STATUS_REPORTS";
    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.pageChanged = function (admin_hierarchy_id) {
        FacilityStatusReportService.summary({admin_hierarchy_id:admin_hierarchy_id,page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data.items;
        });
    };

    GlobalService.facilityCustomDetails(function (data){
        $scope.facilityCustomDetails = data.facilityCustomDetails;
    });

    
    LevelAdminHierarchyService.query(function (data) {
        $scope.admin_hierarchies = data;
    });

    $scope.getTotal = function(){
        var total = 0;
        for(var i = 0; i < $scope.items.length; i++){
            var item = $scope.items[i];
            total += parseFloat(item.amount);
        }
        return total;
    };

    $scope.filterData = function (admin_hierarchy_id,currentPage,perPage) {
        if(perPage === '' || perPage === null){
            perPage = 10;
        }
        FacilityStatusReportService.summary({admin_hierarchy_id:admin_hierarchy_id,page:currentPage,perPage:perPage}, function (data) {
            $scope.items = data.items;
        });
    };
}

FacilityStatusReportController.resolve = {
    DataModel: function (FacilityStatusReportService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            FacilityStatusReportService.summary({page: 1, perPage: 10},function (data) {
                deferred.resolve(data.items);
            });
        }, 900);
        return deferred.promise;
    }
};