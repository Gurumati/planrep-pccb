function ReportController($scope, ReportProjectFundSourceService,FundSourceGfsCodeService, FinancialYearPeriodService) {
  $scope.title = "Reports";

  ReportProjectFundSourceService.get(function (data) {
    $scope.fundSources = data.fund_sources;
    $scope.totalFundSources = data.total;
  });

  $scope.getValue = function (gfsCodeId, periodId) {
    var ex = _.findWhere($scope.existingData, {period_id: periodId, gfs_code_id: gfsCodeId});
    if (ex)
      return ex.amount;
    else
      return 0;
  };

  $scope.amountDiv = function (fund_source_id) {
    FundSourceGfsCodeService.gfs_codes({fund_source_id: fund_source_id},
        function (data) {
          $scope.gfs_codes = data.gfs_codes;
          $scope.existingData = data.existingData;
          $scope.totalGfsCodes = data.total;
          $scope.fund_source_id = fund_source_id;
          $scope.fundSourceTitle = data.fundSourceTitle;
          $scope.projectionToCreate.gfsCodes = $scope.gfs_codes;
        }, function (error) {
          $scope.errorMessage = error.data.errorMessage;
        }
        );


    $scope.periodTotal = function (gfs_code) {
      var result = 0;
      var sum = 0;
      angular.forEach(gfs_code, function (value, key) {
        if (key == 'amount') {
          if (key.length > 0) {
            angular.forEach(value, function (value, key) {
              result = parseInt(result) + parseInt(value);
            });
          }
        }
      });
      if (result !== 0) {
        gfs_code.subTotal = result;
        //get sum
        angular.forEach($scope.projectionToCreate.gfsCodes, function (value, key) {
          sum = sum + parseInt(value.subTotal);
        });
        $('#TotalSum').val(thousands(sum));
        return thousands(result);
      }
    };

    function thousands (val) {
      while (/(\d+)(\d{3})/.test(val.toString())){
        val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
      }
      return val;
    }
  };
}
