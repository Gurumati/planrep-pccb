/**
 * Created by mrisho on 6/20/17.
 */
function BarChartController($scope, BarChartModel, $uibModal, BarChartService) {

  //  $scope.barChart = BarChartModel;

    BarChartService.get({}, function (data) {
        $scope.barChart = data;
        console.log(data);
    });


    $scope.graph={
      dataPoints:[
          {bod:20,int:40,name:"maleria"},
          {bod:40,int:20,name:"Maternal"},
      ]  ,
        dataColumns:[
            {id:"bod",name:"BOD",type:"bar"},
            {id:"int",name:"Interversion",type:"bar"}
        ],
        dataX:{id:"name"}
    };

    $scope.title = "Bar Chart";

}

BarChartController.resolve = {
    BarChartModel: function (BarChartService,$timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            BarChartService.get({}, function (data) {
                deferred.resolve(data);
            });
        }, 900);
        return deferred.promise;
    }
};
