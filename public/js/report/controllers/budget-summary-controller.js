function BudgetSummaryController($scope, AdminHierarchiesService, $timeout, localStorageService, $uibModal, GlobalService, BudgetAggregationService) {

    $scope.title = "BUDGET_SUMMARY";

    $scope.hierarchy_position = localStorageService.get(localStorageKeys.HIERARCHY_POSITION);

    AdminHierarchiesService.getUserAdminHierarchy({}, function (data) {
        $scope.userAdminHierarchy = data;
        $scope.userAdminHierarchyCopy = angular.copy($scope.userAdminHierarchy);
    });

    GlobalService.allFinancialYears(function (data) {
        $scope.financialYears = data.financialYears;
    });

    $scope.resetYear = function () {
        $scope.items = {};
        $scope.dataItems.financial_year = undefined;
    };

    $scope.resetType = function () {
        $scope.items = {};
        $scope.dataItems.budget_class = undefined;
    };

    $scope.resetType = function () {
        $scope.items = {};
        $scope.dataItems.type = undefined;
    };

    $scope.resetBudgetClass = function () {
        $scope.items = {};
        $scope.dataItems.budget_class = undefined;
    };

    $scope.resetHierarchy = function () {
        $scope.userAdminHierarchy = angular.copy($scope.userAdminHierarchyCopy);
        $scope.items = {};
    };

    $scope.printHeader = false;

    $scope.printDIV = function (divName) {
        $scope.printHeader = true;
        var printContents = document.getElementById(divName).innerHTML;
        var popupWin = window.open('', '_blank', 'width=1400,height=600');
        popupWin.document.open();
        popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="/css/print.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
        popupWin.document.close();
    };

    $scope.loadChildren = function (a) {
        var level_id = parseInt(a.admin_hierarchy_level_id);
        if (level_id > 0) {
            if (a.children === undefined) {
                a.children = [];
            }
            AdminHierarchiesService.getAdminHierarchyChildren({adminHierarchyId: a.id}, function (data) {
                a.children = data.adminHierarchies;
            });
        }
    };

    $scope.reportTypes = {
        "1":"Revenue",
        "2":"Expenditure"
    };

    $scope.budgetClasses = {
        "1":"Other Charges",
        "2":"Personal Emolument",
        "3":"Development",
    };

    $scope.dataItems = {};


    $scope.loadBudgetSummary = function (admin_hierarchy, budget_class, financial_year,type) {
        BudgetAggregationService.summaryV2({
            admin_hierarchy_id: admin_hierarchy.id,
            budget_class: budget_class,
            financial_year_id: financial_year.id,
            type: type,
        }, function (data) {
            $scope.items = data.items;
        }, function (error) {
            console.log(error);
        });

        $scope.columnTotalApprovedOwnSource = function(){
            var total = 0;
            for(var i = 0; i < $scope.items.length; i++){
                var item = $scope.items[i];
                total += parseFloat(item.own_source);
            }
            return total;
        };
        $scope.columnTotalApprovedGrants = function(){
            var total = 0;
            for(var i = 0; i < $scope.items.length; i++){
                var item = $scope.items[i];
                total += parseFloat(item.grants);
            }
            return total;
        };


        $scope.columnTotalActualOwnSource = function(){
            var total = 0;
            for(var i = 0; i < $scope.items.length; i++){
                var item = $scope.items[i];
                total += parseFloat(item.own_source_actual);
            }
            return total;
        };
        $scope.columnTotalActualGrants = function(col){
            var total = 0;
            for(var i = 0; i < $scope.items.length; i++){
                var item = $scope.items[i];
                total += parseFloat(item.grants_actual);
            }
            return total;
        };
    };
}