function ProcurementController($scope, ProcurementModel,ProcurementService, $uibModal,LevelAdminHierarchyService,AllFinancialYearService) {

    $scope.procurements = ProcurementModel;
    $scope.title = "PROCUREMENT_PLAN";

    $scope.totalAmount = function () {
        var total = 0;
        angular.forEach($scope.procurements, function (value, key) {
            amount = value.total;
           total += parseFloat(amount);
       });
        $scope.total_amount = total;
    };

    $scope.getTotalGroupBy = function(values){
        var total = 0;
        if(values !== undefined && values.length > 0){
            for(var i = 0; i < values.length; i++){
                var item = values[i];
                total += parseFloat(item.total);
            }
        }
        return total;
    };

    $scope.$watch('procurements', function () {
        $scope.totalAmount();
    });

    LevelAdminHierarchyService.query(function (data) {
        $scope.admin_hierarchies = data;
    });

    AllFinancialYearService.query(function (data) {
        $scope.financialYears = data;
    });

    $scope.filterData = function (admin_hierarchy_id,financial_year_id) {
        if(financial_year_id === null || financial_year_id === '' || financial_year_id === undefined){
            ProcurementService.paginated({admin_hierarchy_id:admin_hierarchy_id}, function (data) {
                $scope.procurements = data.procurements;
            });
        } else {
            ProcurementService.paginated({admin_hierarchy_id:admin_hierarchy_id,financial_year_id:financial_year_id}, function (data) {
                $scope.procurements = data.procurements;
            });
        }
    };

    $scope.breakdown = function (id,description) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/report/procurement/breakdown.html',
            backdrop: false,
            controller: function ($scope, $q, $uibModalInstance,ProcurementService) {
                $scope.name = description;
                ProcurementService.breakdown({id: id}, function (data) {
                        $scope.items = data.items;
                    }, function (error) {
                        console.log(error)
                    }
                );
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
}

ProcurementController.resolve = {
    ProcurementModel: function (ProcurementService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            ProcurementService.paginated(function (data) {
                deferred.resolve(data.procurements);
            });
        }, 900);
        return deferred.promise;
    }
};