function ComprehensivePlanPreviewController($scope,
                                            $location,
                                            $uibModal,
                                            CasPlansModel,
                                            CasPlanService,
                                            PeriodsByFinancialYearService,
                                            CasPlanContentsService,
                                            CasPlanTableByContentService,
                                            CasPlanTableDetailsService,
                                            AllFinancialYearService,
                                            getJasperCasReportService,
                                            GetFacilityBySectorService,
                                            LevelAdminHierarchyService,
                                            RegionByLevelService,
                                            downloadCasPlanService,
                                            PreExecutionService,
                                            localStorageService,
                                            FinancialYearsService
) {

    $scope.casPlans = CasPlansModel;
    $scope.title = "TITLE_COMPREHENSIVE_PLANS_REPORTS";
    $scope.destinationFolder = 'uploads/comprehensive_plans';
    $scope.isBlocked = false;
    $scope.period_id = null;
    $scope.isPeriodic=false;
    //load financial year
    AllFinancialYearService.query(function (data) {
        $scope.financialYears = data;
    });

    $scope.budget_types = ["APPROVED", "DISAPPROVED", "SUPPLEMENTARY", "CARRYOVER", "CURRENT"];

    $scope.loadContents = function (cas_plan) {
        $scope.isPeriodic = cas_plan.is_periodic;
        var cas_plan_id = cas_plan.id;
        CasPlanContentsService.query({cas_plan_id: cas_plan_id},
            function (data) {
                $scope.casPlanContents = data;
                $('#planTable').hide();
                $('#planContents').show();
            }
        );
        //load admin_hierarchy
        $scope.adminHierarchies = {};
        if (cas_plan.admin_hierarchy_level_id == 8) //level 8 is region
        {
            RegionByLevelService.query(function (data) {
                $scope.adminHierarchies = data;
            });
        } else {  //load councils
            //load admin hierarchies
            LevelAdminHierarchyService.query(function (data) {
                $scope.adminHierarchies = data;
            });
        }

    };

    $scope.downloadCasPlan = function (cas_plan_id, financial_year_id, admin_hierarchy_id, section_id) {
        var parameters = {};
        parameters.cas_plan_id = cas_plan_id.id;
        parameters.financial_year_id = financial_year_id;
        parameters.admin_hierarchy_id = admin_hierarchy_id;
        parameters.section_id = section_id;

        downloadCasPlanService.generateSingleDocument(parameters,
            function (data) {
                $scope.successMessage = data.successMessage;
            },
            function (error) {
            });
    };

    $scope.changePlanTable = function (cas_plan_table_id) {
        $scope.defaultTable = cas_plan_table_id;
    };

    $scope.setPeriodId = function(period_id){
        $scope.period_id = period_id;
    };

    $scope.loadPlanTables = function (cas_plan_content_id, report_type) {
        //check if parameters are defined
        CasPlanTableByContentService.get({cas_plan_content_id: cas_plan_content_id},
            function (resp) {
                var data = resp.cas_plan_table;
                $scope.planTables = data;
                if (data.length > 0) {

                    if ($scope.planTables[0].report_url !== null) {
                        //check if is html
                        if ($scope.planTables[0].is_html) {
                            var url = 'https://' + $location.$$host + '/' + $scope.planTables[0].report_url;
                            window.open(url, '_blank');
                        } else {
                            var params = [];
                            var path = $scope.planTables[0].report_url;
                            if ($scope.planTables[0].parameters !== null) {
                                if($scope.planTables[0].parameters.length > 0) {
                                    params = $scope.planTables[0].parameters.split(',');
                                }
                            }

                            if (params.length > 0) {
                                $scope.reportParams(params, path, report_type, $scope.reportTitle, $scope.admin_hierarchy_id, $scope.section_id, $scope.financial_year_id, $scope.facility, $scope.budget_type);
                            }else{
                                $scope.params = {};
                                $scope.jasperResourceUrl = 'https://planrep.pccb.go.tz/jasperserver/rest_v2/reports';
                                $scope.params.admin_hierarchy_id = $scope.admin_hierarchy_id;
                                $scope.params.financial_year_id = $scope.financial_year_id;
                                $scope.params.section_id = $scope.section_id;
                                $scope.params.budget_type = $scope.budget_type;
                                $scope.params.period_id   = $scope.period_id;
                                if ($scope.facility > 0) {
                                    $scope.params.facility_id = $scope.facility;
                                }
                                
                                const qs = Object.keys($scope.params).filter((key)=> 
                                $scope.params[key] !== undefined && $scope.params[key] !== null).map((key)=> `${key}=${$scope.params [key]}`).join('&');
                                window.open(`${$scope.jasperResourceUrl}/${path}.${report_type}?${qs}`, '_blank');
                            }

                            //report title
                            // $scope.reportTitle = $scope.planTables[0].name;
                            // var params = [];
                            // //load jasper report
                            // if ($scope.planTables[0].parameters !== null) {
                            //     if($scope.planTables[0].parameters.length > 0) {
                            //         params = $scope.planTables[0].parameters.split(',');
                            //     }
                            // }

                            // var report_url = $scope.planTables[0].report_url;

                            // if (params.length > 0) {

                            //     if ($scope.facility == undefined || ($scope.selectedPlan.is_facility == false)) {
                            //         $scope.facility = 0;
                            //     }
                            //     $scope.reportParams(params, report_url, report_type, $scope.reportTitle, $scope.admin_hierarchy_id, $scope.financial_year_id, $scope.facility, $scope.budget_type);
                            // } else {
                            //     $scope.params = {};
                            //     //default params
                            //     $scope.params.admin_hierarchy_id = $scope.admin_hierarchy_id;
                            //     $scope.params.financial_year_id = $scope.financial_year_id;
                            //     $scope.params.budget_type = $scope.budget_type;
                            //     $scope.params.period_id   = $scope.period_id;
                            //     if ($scope.facility > 0) {
                            //         $scope.params.facility_id = $scope.facility;
                            //     }
                            //     var mydata = {
                            //         reportParameter: $scope.params,
                            //         report_url: report_url,
                            //         report_type: report_type
                            //     };
                            //     //send report parameters
                            //     getJasperCasReportService.casReport(mydata, function (data) {
                            //         //$scope.reportFile = data.reportUrl;
                            //         if (data.noContent) {
                            //             window.alert("No data");
                            //         } else {
                            //             window.open(data.reportUrl, '_blank');
                            //         }
                            //     });
                            // }

                        }

                    } else if ($scope.planTables[0].type == 1) {
                        //check if is facility
                        if ($scope.facility == undefined || ($scope.selectedPlan.is_facility == false)) {
                            $scope.facility = 0;
                        }
                        //end
                        CasPlanTableDetailsService.query({
                            cas_plan_table_id: $scope.planTables[0].id,
                            facility: $scope.facility,
                            admin_hierarchy_id: $scope.admin_hierarchy_id,
                            section_id: $scope.section_id,
                            budget_type: $scope.budget_type,
                            period_id: $scope.period_id,
                            financial_year_id: $scope.financial_year_id
                        }, function (data) {
                            if (data[0] == undefined) {
                                window.alert('No data!');
                            } else {
                                $scope.casPlanTableUrl = data[0];
                                var url = 'https://' + $location.$$host + ':' + $location.$$port + '/' + $scope.destinationFolder + '/' + $scope.casPlanTableUrl.url;
                                window.open(url, '_blank');
                                if (data.length > 0) {
                                    $scope.description = $scope.casPlanTableUrl.description;
                                } else {
                                    $scope.description = '';
                                }
                            }

                        });
                    }
                } else {
                    $scope.errorMessage = 'NO_DATA_FOUND';
                }

            }
        );
    };

    $scope.backToContent = function () {
        $scope.loadContents($scope.selectedPlan.id);
    };

    $scope.reportParams = function (parameters, report_url, report_type, reportTitle, admin_hierarchy_id, section_id, financial_year_id, facility_id, budget_type) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/report/comprehensive_plans/partials/parameters.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, LoadReportParametersSources) {
                $scope.reportTitle = reportTitle;
                $scope.reportParameter = {};
                $scope.item = {};
                if (parameters.length > 0) {
                    var inputData = {
                        parameters: parameters,
                        admin_hierarchy_id: admin_hierarchy_id,
                        financial_year_id: financial_year_id,
                        section_id: section_id,
                        budget_type: budget_type
                    };
                    LoadReportParametersSources.getParams(inputData, function (data) {
                        $scope.item = data.parameter_data;
                        if ($scope.item.exchange_rate !== undefined) {
                            if ($scope.item.exchange_rate.length > 0) {
                                $scope.item.exchange_rate = $scope.item.exchange_rate;
                                $scope.reportParameter.exchange_rate = $scope.item.exchange_rate;
                            }
                        }
                        $scope.reportParamsData = $scope.item;
                    });

                }
                //function to show report
                $scope.getReport = function () {
                    //default params
                    $scope.reportParameter.admin_hierarchy_id = admin_hierarchy_id;
                    $scope.reportParameter.section_id = section_id;
                    $scope.reportParameter.financial_year_id = financial_year_id;
                    $scope.reportParameter.budget_type = budget_type;
                    
                    if (facility_id > 0) {
                        $scope.reportParameter.facility_id = facility_id;
                    }

                    $scope.jasperResourceUrl = 'https://planrep.pccb.go.tz/jasperserver/rest_v2/reports';
                   
                    const qs = Object.keys($scope.reportParameter).filter((key)=> 
                    $scope.reportParameter[key] !== undefined && $scope.reportParameter[key] !== null).map((key)=> `${key}=${$scope.reportParameter[key]}`).join('&');
                    window.open(`${$scope.jasperResourceUrl}/${report_url}.${report_type}?${qs}`, '_blank');

                    // var data = {
                    //     reportParameter: $scope.reportParameter,
                    //     report_url: report_url,
                    //     report_type: report_type
                    // };
                    // //send report parameters
                    // getJasperCasReportService.casReport(data, function (data) {
                    //     //$scope.reportUrl = data.reportUrl;
                    //     if (data.noContent) {
                    //         window.alert("No data");
                    //     } else {
                    //         window.open(data.reportUrl, '_blank');
                    //     }
                    // });
                    $scope.close();
                };

                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                //get interventions
                $scope.getIntervention = function (priority_area_id) {
                    angular.forEach($scope.reportParamsData.intervention_id, function (value) {
                        if (value.id == priority_area_id) {
                            $scope.reportParamIntervention = value.interventions;
                        }
                    });
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //return when function executed

            },
            function () {
                //If modal is closed

            });
    };

    $scope.getWord = function () {
        window.open($scope.report_in_rtf, '_blank');
    };

    $scope.loadFacilityByType = function () {

        $scope.facilityByType = [];
        angular.forEach($scope.facilities, function (value, key) {
            if (value.facility_type_id == $scope.facility_type) {
                $scope.facilityByType.push(value);
            }
        });
    };

    $scope.loadFacilityByCouncil = function () {
        //check if facility
        if ($scope.selectedPlan.is_facility) {
            //load facility
            var sector_id = $scope.selectedPlan.sector_id;
            var admin_hierarchy_id = $scope.admin_hierarchy_id;
            GetFacilityBySectorService.query({sector_id: sector_id, council: admin_hierarchy_id}, function (data) {
                $scope.facilities = data;
            });
        }
        //end
 };

    $scope.loadPeriodsByFinancialYear = function(financialYearId){
        PeriodsByFinancialYearService.get({id: financialYearId},
            function (data) {
                $scope.periods = data.data;
            }
        );
    };
    //check for project outputs
    $scope.checkProjectOutputs = function (financialYearId,budget_type) {
        FinancialYearsService.getByBudgetType({budgetType : budget_type},function (data) {
            if(data.financialYear == financialYearId) {
                PreExecutionService.projectOutputFilled({admin_hierarchy_id: $scope.admin_hierarchy_id,financial_year_id: financialYearId,budget_type:budget_type}, function(data){
                    if(data.items !=null && $scope.admin_hierarchy_id){
                        var user = angular.fromJson(localStorageService.get(localStorageKeys.USER));
                        if($scope.selectedPlan.id == 3 || $scope.selectedPlan.id == 9 || $scope.selectedPlan.id == 13){
                            if(user.decision_level_id != 5){
                                $scope.isBlocked = true;
                                $scope.errorMessage = "Complete filling the project output for development activities first. Go to Planning menu to update activities";
                                $scope.notfilledSection = data.items;
                            }else {
                                $scope.errorMessage = "The chosen PISC does not complete filling the project output for development activities";
                            }
                        }
                    }else {
                        $scope.isBlocked = false;
                        $scope.errorMessage = null;
                        $scope.notfilledSection = null;
                    }
                });
            }
        },function (error) {
            scope.errorMessage = error.data.errorMessage;
        });

    };
}

ComprehensivePlanPreviewController.resolve = {
    CasPlansModel: function (CasPlanService, $q) {
        var deferred = $q.defer();
        CasPlanService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};
