function ProjectionController($scope, LevelAdminHierarchyService, GlobalService, ProjectionService) {

        $scope.title = "PROJECTIONS";

        LevelAdminHierarchyService.query(function (data) {
            $scope.admin_hierarchies = data;
        });

        GlobalService.financialYears(function (data) {
            $scope.financialYears = data.financialYears;
        });

        $scope.printDiv = function (divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var popupWin = window.open('', '_blank', 'width=1400,height=600');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="/css/print.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
            popupWin.document.close();
        };

        $scope.hierarchyProjections = function (admin_hierarchy_id) {
            ProjectionService.hierarchy_projections({
                admin_hierarchy_id: admin_hierarchy_id
            }, function (data) {
                $scope.projections = data.projections;
            });
        };

        $scope.hierarchyYearProjections = function (admin_hierarchy_id, financial_year_id) {
            ProjectionService.hierarchyYearProjections({
                admin_hierarchy_id: admin_hierarchy_id,
                financial_year_id: financial_year_id
            }, function (data) {
                $scope.projections = data.projections;
            });
        };

        $scope.totalAmount = function () {
            var total = 0;
            angular.forEach($scope.projections, function (value, key) {
                total += parseFloat(value.amount);
            });
            $scope.total_amount = total;
        };
        $scope.$watch('projections', function () {
            $scope.totalAmount();
        });

        $scope.getTotalGroupBy = function (values) {
            var total = 0;
            if (values !== undefined && values.length > 0) {
                for (var i = 0; i < values.length; i++) {
                    var item = values[i];
                    total += parseFloat(item.amount);
                }
            }
            return total;
        };

}
