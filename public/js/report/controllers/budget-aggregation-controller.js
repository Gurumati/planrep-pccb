function BudgetAggregationController($scope,BudgetAggregationService,LevelAdminHierarchyService,AllFinancialYearService) {

    //$scope.items = DataModel;
    $scope.viewIndex = 2;
    $scope.title = "BUDGET_AGGREGATIONS";

    $scope.groupByOptions = {
        "1":"SUMMARY-LGAs",
        "2":"LGA-BUDGET-CLASS",
        "3":"SECTOR",
        "4":"SECTOR-LGAs",
        "5":"SECTOR-LGA-BUDGET-CLASS",
        "6":"SECTOR-COST-CENTRE",
        "7":"LGA-SECTOR-COST-CENTRE",
        "8":"LGA-SECTOR-BUDGET-CLASS-COST-CENTRE"
    };

    LevelAdminHierarchyService.query(function (data) {
        $scope.admin_hierarchies = data;
    });

    AllFinancialYearService.query(function (data) {
        $scope.financialYears = data;
    });

    $scope.getTotalGroupBy = function(values){
        var total = 0;
        if(values !== undefined && values.length > 0){
            for(var i = 0; i < values.length; i++){
                var item = values[i];
                total += parseFloat(item.amount);
            }
        }
        return total;
    };

    $scope.loadData = function (groupByOption) {
        var i = parseInt(groupByOption);
        BudgetAggregationService.summary({groupByOption:groupByOption}, function (data) {
            $scope.items = data.items;
            $scope.viewIndex = i;
        });

        $scope.getTotal = function(){
            var total = 0;
            for(var i = 0; i < $scope.items.length; i++){
                var item = $scope.items[i];
                total += parseFloat(item.amount);
            }
            return total;
        };
    };

    $scope.filterData = function (admin_hierarchy_id,groupByOption) {
        BudgetAggregationService.summary({admin_hierarchy_id:admin_hierarchy_id,groupByOption:groupByOption}, function (data) {
            $scope.items = data.items;
        });

        $scope.getTotal = function(){
            var total = 0;
            for(var i = 0; i < $scope.items.length; i++){
                var item = $scope.items[i];
                total += parseFloat(item.amount);
            }
            return total;
        };
    };

    $scope.filterDataByYear = function (admin_hierarchy_id,groupByOption,financial_year_id) {
        BudgetAggregationService.summary({admin_hierarchy_id:admin_hierarchy_id,groupByOption:groupByOption,financial_year_id:financial_year_id}, function (data) {
            $scope.items = data.items;
        });

        $scope.getTotal = function(){
            var total = 0;
            for(var i = 0; i < $scope.items.length; i++){
                var item = $scope.items[i];
                total += parseFloat(item.amount);
            }
            return total;
        };
    };
}

/*
BudgetAggregationController.resolve = {
    DataModel: function (BudgetAggregationService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            BudgetAggregationService.summary({groupByOption:2},function (data) {
                deferred.resolve(data.items);
            });
        }, 900);
        return deferred.promise;
    }
};*/
