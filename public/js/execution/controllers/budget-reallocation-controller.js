(function () {
    'use strict';

    angular
        .module('execution-module')
        .controller('BudgetReallocationController', BudgetReallocationController);

    BudgetReallocationController.$inject = ['$scope','selectedReallocation', 'ConfirmDialogService','BudgetReallocationService', '$window', '$q', '$route', 'FacilityService', 'FundSourceService', '$uibModal', 'SectionService'];

    function BudgetReallocationController($scope, selectedReallocation, ConfirmDialogService, BudgetReallocationService, $window, $q, $route, FacilityService, FundSourceService, $uibModal, SectionService) {
        $scope.title = "BUDGET_REALLOCATION";

        $scope.fromSelectedAccounts = [];
        $scope.toSelectedAccounts = [];
        $scope.reallocateFromReady = false;
        $scope.routeParams = $route.current.params;
        $scope.selectedReallocation = selectedReallocation.get();
        if($scope.selectedReallocation === undefined){
            BudgetReallocationService.getById({id: $scope.routeParams.reallocationId}, function(data){
                $scope.selectedReallocation = data.reallocation;
            });
        }

        $scope.newOptions = [{
            id: 1,
            name: "Existing Accounts"
        },
        {
            id: 2,
            name: "Existing Activity new Inputs"
        },
        {
            id: 3,
            name: "New Activity new Input"
        }
       ];

        $scope.filterChanged = function (filter) {
            $scope.selectedFinancialYearId = filter.selectedFinancialYearId;
            $scope.selectedFinancialYearName = filter.selectedFinancialYearName;
            $scope.selectedBudgetType = filter.selectedBudgetType;
            $scope.selectedAdminHierarchyId = filter.selectedAdminHierarchyId;
            $scope.selectedAdminHierarchyName = filter.selectedAdminHierarchyName;
            $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
            // $scope.selectedSectionLevelPosition = filter.selectedSectionLevelPosition;
            // $scope.selectedSectionId = filter.selectedSectionId;
            // $scope.selectedSectionName = filter.selectedSectionName;
            // $scope.selectedSectionLevelId = filter.selectedSectionLevelId;
            $scope.searchQuery = filter.searchQuery;
            $scope.selectedFacility = undefined;
            $scope.selectedAccount = undefined;
        };

         /**
         * Load Sections (Cost Centres) to reallocate from by user sector/level
         */
        (function loadFromSection() {
            SectionService.getUserCanBudgetSections({},
                function(data) {
                    $scope.fromSections = data.sections;
                    $scope.toSections = data.sections;
                });
        })();

        /**
         * Search from facility to reallocated from
         */
        $scope.searchFromFacility = function ($query) {
            var deferred = $q.defer();

            if ($query !== undefined &&
                $query !== '' &&
                $query.length >= 1 &&
                $scope.selectedFromSectionId !== undefined &&
                $scope.selectedAdminHierarchyId !== undefined) {

                $scope.facilityLoading = true;
                var facilityToExclude = {
                    'facilityIdsToExclude': []
                };
                FacilityService.searchByAdminAreaAndSection({
                        adminHierarchyId: $scope.selectedAdminHierarchyId,
                        sectionId: $scope.selectedFromSectionId,
                        isFacilityUser: 0,
                        searchQuery: $query,
                        noLoader: true
                    },
                    facilityToExclude,
                    function (data) {
                        $scope.facilityLoading = false;
                        deferred.resolve(data.facilities);
                    });
            } else {
                deferred.resolve([]);
            }
            return deferred.promise;
        };

        /**
         * Search facility to be realocated budget
         */
        $scope.searchToFacility = function ($query) {
            var deferred = $q.defer();

            if ($query !== undefined &&
                $query !== '' &&
                $query.length >= 1 &&
                $scope.selectedToSectionId !== undefined &&
                $scope.selectedAdminHierarchyId !== undefined) {

                $scope.facilityLoading = true;
                var facilityToExclude = {
                    'facilityIdsToExclude': []
                };
                FacilityService.searchByAdminAreaAndSection({
                        adminHierarchyId: $scope.selectedAdminHierarchyId,
                        sectionId: $scope.selectedToSectionId,
                        isFacilityUser: 0,
                        searchQuery: $query,
                        noLoader: true
                    },
                    facilityToExclude,
                    function (data) {
                        $scope.facilityLoading = false;
                        deferred.resolve(data.facilities);
                    });
            } else {
                deferred.resolve([]);
            }
            return deferred.promise;
        };

        /**
         * Selection of from facility changed get fund sources having budget
         */
        $scope.fromFacilityChanged = function () {
            $scope.selectedFundSourceId = undefined;
            $scope.fromFundSources = [];
            $scope.reset();
            $scope.getPlannedFundSources();
        };

        /**
         * To facility changed get fund sources that cn be budgted
         */
        $scope.toFacilityChanged = function (toFacility) {
            $scope.selectedToFacility = toFacility;
            $scope.showOptions = false;
            if(toFacility !== undefined && $scope.selectedToSectionId !== undefined){
                $scope.showOptions = true;
            }
        };

        var paramsValid = function () {
            if ($scope.selectedAdminHierarchyId === undefined ||
                $scope.selectedFromSectionId === undefined ||
                $scope.selectedFinancialYearId === undefined ||
                $scope.selectedFromFacility === undefined ||
                $scope.selectedFromFacility === null ||
                $scope.selectedBudgetType === undefined) {
                return false;
            }
            return true;
        };

        /**
         * Get from facility fund souces having budget
         */
        $scope.getPlannedFundSources = function () {
            if (!paramsValid()) {
                return;
            }
            $scope.fromSelectedAccounts = [];
            $scope.fromFundSources = [];
            $scope.reallocationAccounts = [];

            FundSourceService.getBudgetedByFacility({
                budgetType: $scope.selectedBudgetType,
                financialYearId: $scope.selectedFinancialYearId,
                adminHierarchyId: $scope.selectedAdminHierarchyId,
                sectionId: $scope.selectedFromSectionId,
                facilityId: $scope.selectedFromFacility.id
            }, function (data) {
                $scope.fromFundSources = data.fundSources;
            });
        };

        /**
         * Get from facility activities with budgets by fund source , budget type and fincial year
         */
        $scope.loadFromActivities = function () {
            $scope.reset();
            if (paramsValid()) {
                BudgetReallocationService.getReallocationActivities({
                    budgetType: $scope.selectedBudgetType,
                    financialYearId: $scope.selectedFinancialYearId,
                    adminHierarchyId: $scope.selectedAdminHierarchyId,
                    sectionId: $scope.selectedFromSectionId,
                    facilityId: $scope.selectedFromFacility.id,
                    fundSourceId: $scope.selectedFromFundSourceId
                }, function(data) {
                $scope.fromActivities = data.activities;
                });
           }
           $scope.getToFundSource();
        };

        /**
         * Get chart of accounts of selected from activity
         */
        $scope.loadReallocationAccounts = function () {
            $scope.reset();
            $scope.selectedAccount = undefined;
            if ($scope.selectedFromActivityFundSourceId === undefined) {
                return;
            }
            BudgetReallocationService.getReallocationAccount({
                selectedFromActivityFundSourceId: $scope.selectedFromActivityFundSourceId
                },
                function (data) {
                    $scope.reallocationAccounts = data.reallocationAccounts;

                });
        };

        /**
         * Get fund source that can be reallocated by selected from fund source
         */
        $scope.getToFundSource = function() {
            if ($scope.selectedFromFundSourceId === undefined ||
                $scope.selectedToSectionId === undefined) {
                return;
            }
            FundSourceService.getByMainBudgetClassAndSector({
                financialYearId: $scope.selectedFinancialYearId,
                sectionId: $scope.selectedToSectionId,
                fundSourceId: $scope.selectedFromFundSourceId
            }, function (data) {
                $scope.toFundSources = data.fundSources;
            });
        };

        $scope.totalChar = '';
        $scope.getChar = function (index) {
            var char = String.fromCharCode(64 + parseInt(index, 10));
            $scope.totalChar = char + '+' + $scope.totalChar;
            return char;
        };

        $scope.setFromSelectedAccount = function (acc) {
            $scope.fromSelectedAccount = acc;
        };
        $scope.getTotalByToAccount = function (id) {
            var totalToAccount = 0;
            $scope.fromSelectedAccounts.forEach(function (fromAcc) {
                fromAcc.toSelectedToAccount.forEach(function (toAcc) {
                    if (toAcc.id === id) {
                        totalToAccount = totalToAccount + toAcc.reallocatedAmount;
                    }
                });
            });

            return totalToAccount;
        };

        var selectedMtefSectionId;
        $scope.setOption = function () {
            $scope.showOptions = false;
            if ($scope.selectedToSectionId !== undefined) {
                BudgetReallocationService.getMtefSectionToReallocate({
                    sectionId: $scope.selectedToSectionId
                }, function (data) {
                    if (data !== undefined && data.id !== undefined) {
                        $scope.showOptions = true;
                        $scope.selectedMtefSectionId = data.id;
                        console.log(data);
                        selectedMtefSectionId = data.id;
                    }
                });
            }
        };
        $scope.loadReallocationTo = function () {
            $scope.reallocateFromReady = true;
        };
        $scope.backToFromAccount = function () {
            $scope.toOption = undefined;
            $scope.reallocateFromReady = false;
            $scope.selectedToSectionId = undefined;
            $scope.toReallocationAccounts = [];
            $scope.reallocateToReady = true;
            $scope.fromSelectedAccounts = [];
        };
        $scope.getFromAccountTotal = function () {
            var totalFrom = 0;
            angular.forEach($scope.fromSelectedAccounts, function (acc) {
                var deduction = (acc.allocated > acc.expediture)?acc.allocated : acc.expediture;
                totalFrom = totalFrom + (acc.budget - deduction);
            });
            return totalFrom;
        };

        /**
         * Toggle the selected from reallocation chart of account
         */
        $scope.toggleFromAccounts = function (a) {
            $scope.selectedAccount = angular.copy(a);
            setSelectedSection();
            loadAccountReallocation(a.id);
        };

        //If to sections has one item select it by default
        // var setSelectedSection = function(){
        //     //if($scope.toSections.length === 1){
        //        // $scope.selectedToSectionId = $scope.toSections[0].id;
        //         $scope.setOption();
        //         return;
        //     //}

        //     $scope.selectedToSectionId = undefined;
        // };
        /**
         * Get existing reallocation from selected chart of account
         * @param {*} accountId
         */
        var loadAccountReallocation = function (accountId) {
            BudgetReallocationService.getAccountReallocation({
                accountId: accountId
            }, function (data) {
                $scope.selectedAccount.reallocations = data.reallocations;
                $scope.setAvailable();
            });
        };

        $scope.delete= function(id) {
            ConfirmDialogService.showConfirmDialog(
                'Delete Item',
                'Are you sure you want to delete')
            .then(function () {
                BudgetReallocationService.deleteItem({
                          itemId: id
                        },
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            loadAccountReallocation($scope.selectedAccount.id);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {

                });
           };


        $scope.setAvailable = function () {
            var t = 0;
            angular.forEach($scope.selectedAccount.reallocations, function (r) {
                if (!r.is_approved) {
                    t = t + parseFloat(r.amount);
                }
            });
            if ($scope.selectedAccount.toAccounts !== undefined && $scope.selectedAccount.toAccounts.length) {
                angular.forEach($scope.selectedAccount.toAccounts, function (a) {
                    if (a.amount !== undefined) {
                        t = t + parseFloat(a.amount);
                    }
                });
            }
            var deduction = ($scope.selectedAccount.allocated > $scope.selectedAccount.expenditure)?$scope.selectedAccount.allocated:$scope.selectedAccount.expenditure;
            //$scope.selectedAccount.available = parseFloat($scope.selectedAccount.budget) - parseFloat(deduction) - t; uncomment this for actuals to be considered
            $scope.selectedAccount.available = parseFloat($scope.selectedAccount.budget) - t;
        };

        $scope.OptionChange = function (option) {
            var popupWidth = screen.width * 0.8;
            var popupHeight = screen.height * 0.9;
            var popupLeft = (screen.width - popupWidth) / 2;
            var popupTop = 0;
            var params = 'width=' + popupWidth + ', height=' + popupHeight + ',left=' + popupLeft + ',top=' + popupTop;
            var url;
            if (selectedMtefSectionId == undefined ||
                $scope.selectedToFacility === undefined ||
                $scope.selectedToFundSourceId === undefined) {
                return;
            }
            switch (option) {
                case 1:
                    url = "/execution#!/budget-accounts?sectionId=" + $scope.selectedToSectionId + "&budgetType=" + $scope.selectedBudgetType +
                        "&financialYearId=" + $scope.selectedFinancialYearId + "&fundSourceId=" + $scope.selectedToFundSourceId +
                        "&adminHierarchyId=" + $scope.selectedAdminHierarchyId + "&selectedFacilityId="+$scope.selectedToFacility.id+"&isPopup=true";

                    $window.open(url, "popup", params);
                    break;
                case 3:
                    url = "/planning#!/activities/" + selectedMtefSectionId + "/" + $scope.selectedBudgetType +
                        "?realoc=true&isPopup=true&fundSourceId=" + $scope.selectedToFundSourceId+ "&facilityId="+$scope.selectedToFacility.id;
                    $window.open(url, "popup", params);
                    break;
                case 2:
                    url = "/budgeting#!/activity-facility-fund-sources/" + selectedMtefSectionId + "/" +
                    $scope.selectedBudgetType + "?realoc=true&isPopup=true&fundSourceId=" + $scope.selectedToFundSourceId +
                    "&facilityId="+$scope.selectedToFacility.id;
                    $window.open(url, "popup", params);
                    break;
                default:
            }
        };

        $window.HandleReallocationResult = function (result) {
            var inputsIds = {
                inputIds: result
            };
            console.log(result);
            BudgetReallocationService.createAndGetReallocationToAccounts({
                mtefSectionId: $scope.selectedMtefSectionId
            }, inputsIds, function (data) {
                prepareReallocationAccounts(data.reallocationToAccounts);
            });
        };

        $window.HandleExistingReallocationResult = function (account) {
            $scope.$apply(function () {
                prepareReallocationAccounts(account);
            });
        };

        var prepareReallocationAccounts = function (toAccount) {
            if($scope.selectedAccount.toAccounts !== undefined && $scope.selectedAccount.toAccounts.length){
                toAccount.forEach(function(a){
                    $scope.selectedAccount.toAccounts.unshift(a);
                });
            }
            else{
                $scope.selectedAccount.toAccounts = angular.copy(toAccount);
            }
            $scope.reallocateToReady = true;
        };

        $scope.createReallocation = function (selectedAccount) {

            var reallocationItem = {
                "fromAccount": selectedAccount
            };
            var reallocationId = $scope.routeParams.reallocationId;

            var modalInstance = $uibModal.open({
                templateUrl: '/pages/execution/budget_reallocation/confirm-reallocation.html',
                backdrop: false,
                controller: function ($scope, BudgetReallocationService, $uibModalInstance) {

                    $scope.create = function () {
                        reallocationItem.comments = $scope.comments;
                        BudgetReallocationService.createReallocation({
                            reallocationId: reallocationId
                        }, reallocationItem, function (data) {
                            $uibModalInstance.close(data);
                        });
                    };

                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function (data) {
                    $scope.successMessage = data.successMessage;
                    $scope.selectedAccount.toAccounts = undefined;
                    loadAccountReallocation(selectedAccount.id);

                },
                function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
        };

        $scope.reset = function () {
            $scope.reallocationAccounts = [];
            $scope.selectedAccount = undefined;
            $scope.selectedToFacility = undefined;
            $scope.selectedFundSourceId = undefined;
            $scope.selectedToSectionId = undefined;
        };
    }
})();
