function ReallocationFeedbackController($scope, $uibModal, AllFinancialYearService, ReallocationFeedbackService) {
  $scope.title = 'Reallocation Feedback';

  $scope.system_codes = ['MUSE','NAVISION'];
  $scope.maxSize = 3;
  $scope.currentPage = 1;
  $scope.perPage = 30;
  $scope.filterChanged = function (filter) {
    $scope.selectedAdminHierarchyId = filter.selectedAdminHierarchyId;
    $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
  };

  AllFinancialYearService.query(function (response) {
    $scope.financialYears = response;
  });

  $scope.loadTransaction = function () {
    $scope.header = {
      admin_hierarchy_id: $scope.selectedAdminHierarchyId,
      financial_year_id: $scope.financialYear.id,
      transaction_type: $scope.transaction_type,
      system_code: $scope.system_code,
      page: $scope.currentPage,
      perPage: $scope.perPage
    };
    ReallocationFeedbackService.get($scope.header, function (data) {
      $scope.items = data;


      $scope.cleanResponse = function (response) {
        let clean = JSON.parse(response);
        return clean.message;
      };

    }, function (error) {
      console.log(error);
    });
  };

  $scope.pageChanged = function () {
    $scope.loadTransaction();
  };
}
