function PreExecutionController($scope, $uibModal, $route, ConfirmDialogService, SectionService, PreExecutionService, GlobalService) {

    $scope.title = "PRE_EXECUTION";

    SectionService.getUserCanBudgetSections(function (data) {
        $scope.canBudgetSections = data.sections;
    });

    $scope.loadBudgetClasses = function (sectionId) {
        $scope.sectionActivities = {};

        GlobalService.sectionBudgetClassesWithActivity({sectionId: sectionId}, function (data) {
            $scope.budgetClasses = data.items;
        });

        $scope.loadFundSources = function (budgetClassId) {
            PreExecutionService.costCentreFundSources({sectionId: sectionId, budgetClassId: budgetClassId}, function (data) {
                $scope.fundSources = data.fund_sources;
            });

            $scope.loadActivities = function (sectionId, budgetClassId, fundSourceId) {

                $scope.sectionActivities = {};

                $scope.currentPage = 1;
                $scope.perPage = 10;
                $scope.maxSize = 3;

                if (fundSourceId > 0) {
                    PreExecutionService.activities({
                        sectionId: sectionId,
                        budgetClassId: budgetClassId,
                        fundSourceId: fundSourceId,
                        page: $scope.currentPage,
                        perPage: $scope.perPage
                    }, function (data) {
                        $scope.sectionActivities = data.activities;
                        $scope.sectionId = sectionId;
                        $scope.budgetClassId = budgetClassId;
                        $scope.fundSourceId = fundSourceId;
                    });
                }

                $scope.pageChanged = function () {
                    PreExecutionService.activities({
                        sectionId: sectionId,
                        budgetClassId: budgetClassId,
                        fundSourceId: fundSourceId,
                        page: $scope.currentPage,
                        perPage: $scope.perPage
                    }, function (data) {
                        $scope.sectionActivities = data.activities;
                        $scope.sectionId = sectionId;
                        $scope.budgetClassId = budgetClassId;
                        $scope.fundSourceId = fundSourceId;
                    });
                };

                $scope.restoreAllData = function(sectionId,budgetClassId,fundSourceId){
                    GlobalService.restoreLostData({sectionId: sectionId, budgetClassId: budgetClassId,fundSourceId:fundSourceId}, function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.loadActivities(sectionId,budgetClassId,fundSourceId);
                    });
                };

                $scope.activityPhases = function (activity) {
                    var modalInstance = $uibModal.open({
                        templateUrl: '/pages/execution/pre_execution/phases.html',
                        backdrop: false,
                        controller: function ($scope, $uibModalInstance) {

                            $scope.activity = activity;

                            $scope.dateFormat = 'yyyy-MM-dd';

                            $scope.create = true;
                            $scope.update = false;
                            $scope.showProjectOutputForm = false;

                            $scope.showUpdateProjectOutputForm = false;

                            $scope.showUpdateMainTaskForm = false;

                            $scope.updateMainTaskForm = function (arrayList) {
                                $scope.showUpdateMainTaskForm = true;

                                $scope.phaseModel = angular.copy(arrayList[0]);

                                $scope.phaseModel.description = arrayList[0]['phase'];

                                $scope.updateMainTaskInfo = function () {

                                    if ($scope.updateMainTask.$invalid) {
                                        $scope.formHasErrors = true;
                                        return;
                                    }
                                    $scope.phaseModel.activityId = activity.id;
                                    $scope.phaseModel.id = arrayList[0]['activity_phase_id'];

                                    PreExecutionService.updateMainTaskData($scope.phaseModel, function (data) {
                                        $scope.milestones = data.milestones;
                                        $scope.showUpdateMainTaskForm = false;
                                        $scope.successMessage = data.successMessage;
                                    });
                                }
                            };

                            $scope.showNextBtn = false;

                            PreExecutionService.milestones({activityId: activity.id, facility_id: activity.facility_id}, function (data) {
                                $scope.milestones = data.milestones;
                            });

                            PreExecutionService.outputs({activityId: activity.id, facility_id: activity.facility_id}, function (data) {
                                $scope.outputs = data.outputs;
                            });

                            $scope.showInitPhase = false;
                            $scope.showPushMilestone = false;
                            $scope.showInitBtn = true;

                            $scope.totalAllocated = 0;
                            $scope.amount = 0;

                            $scope.showMinMilestoneForm = false;

                            $scope.addMoreMilestoneToPhase = function (arrayList) {
                                $scope.activity_phase_id = arrayList[0]['activity_phase_id'];
                                $scope.showMinMilestoneForm = true;

                                $scope.create = false;
                                $scope.update = true;

                                $scope.mModel = {};
                                $scope.mModel.amount = 0;
                                $scope.mDateRange = {};

                                $scope.clearDateRange = function (period) {
                                    $scope.mModel.start_date = period.start_date;
                                    $scope.mModel.end_date = period.end_date;
                                    $scope.mModel.period_id = period.id;
                                };

                                $scope.mModel.activity_phase_id = $scope.activity_phase_id;
                                $scope.mModel.amount = $scope.amount;

                                $scope.allocated = function (milestones, amount) {
                                    var total = 0;
                                    for (var i = 0; i < milestones.length; i++) {
                                        var item = milestones[i];
                                        total += parseFloat(item.amount);
                                    }
                                    return ((total + amount) + $scope.totalAllocated);
                                };

                                $scope.exceeded = function (budget, allocated) {
                                    return (budget - allocated) < 0;
                                };

                                $scope.insertMilestone = function () {
                                    $scope.mModel.facility_id = activity.facility_id;
                                    PreExecutionService.addMileStone({activityId: activity.id}, $scope.mModel,
                                        function (data) {
                                            $scope.successMessage = data.successMessage;
                                            $scope.milestones = data.milestones;
                                            $scope.mModel.amount = 0;
                                            $scope.showMinMilestoneForm = false;
                                        },
                                        function (error) {
                                            $scope.errorMessage = error.data.errorMessage;
                                            $scope.errors = error.data.errors;
                                        }
                                    );
                                }
                            };

                            $scope.showUpdateMilestoneForm = false;
                            $scope.edit = false;

                            $scope.updateMilestoneInfo = function (mModel) {
                                $scope.activity_phase_id = mModel.activity_phase_id;
                                $scope.showUpdateMilestoneForm = true;

                                $scope.totalAllocated = 0;
                                $scope.amount = 0;

                                $scope.create = false;
                                $scope.update = false;
                                $scope.edit = true;
                                $scope.originalAmount = mModel.amount;

                                $scope.mDateRange = {};

                                PreExecutionService.periods({activityId: activity.id}, function (data) {
                                    $scope.periods = data.periods;
                                    $scope.mModel = angular.copy(mModel);
                                    $scope.mModel.start_date = new Date($scope.mModel.start_date);
                                    $scope.mModel.end_date = new Date($scope.mModel.end_date);
                                    $scope.mModel.amount = 0;
                                });

                                $scope.clearDateRange = function (period) {
                                    $scope.mModel.start_date = period.start_date;
                                    $scope.mModel.end_date = period.end_date;
                                    $scope.mModel.period_id = period.id;
                                };

                                $scope.allocated = function (milestones, amount) {
                                    var total = 0;
                                    for (var i = 0; i < milestones.length; i++) {
                                        var item = milestones[i];
                                        total += parseFloat(item.amount);
                                    }
                                    return (total + amount) - $scope.originalAmount;
                                };

                                $scope.available = function (milestones, amount) {
                                    var total = 0;
                                    for (var i = 0; i < milestones.length; i++) {
                                        var item = milestones[i];
                                        total += parseFloat(item.amount);
                                    }
                                    return (total - $scope.originalAmount) - amount;
                                };

                                $scope.exceeded = function (budget, allocated) {
                                    return (budget - allocated) < 0;
                                };

                                $scope.updatePhaseMilestoneDataaaa = function () {
                                    $scope.mModel.activity_phase_id = $scope.activity_phase_id;
                                    $scope.mModel.period_id = $scope.period.id;
                                    $scope.mModel.activityId = activity.id;
                                    $scope.mModel.facility_id = activity.facility_id;
                                    $scope.mModel.id = mModel.id;

                                    PreExecutionService.updateMileStoneData($scope.mModel,
                                        function (data) {
                                            $scope.successMessage = data.successMessage;
                                            $scope.milestones = data.milestones;
                                            $scope.mModel.amount = 0;
                                            $scope.showUpdateMilestoneForm = false;
                                        },
                                        function (error) {
                                            $scope.errorMessage = error.data.errorMessage;
                                            $scope.errors = error.data.errors;
                                        }
                                    );
                                };

                                $scope.closeUpdatePhaseMilestoneForm = function () {
                                    $scope.showUpdateMilestoneForm = false;
                                }
                            };

                            $scope.allocated = function (milestones, amount) {
                                var total = 0;
                                for (var i = 0; i < milestones.length; i++) {
                                    var item = milestones[i];
                                    total += parseFloat(item.amount);
                                }
                                return ((total + amount) + $scope.totalAllocated);
                            };

                            $scope.revealInitPhase = function () {
                                $scope.showInitPhase = true;
                                $scope.showInitBtn = false;

                                $scope.phaseModel = {};

                                $scope.initPhase = function () {

                                    $scope.milestone = {};

                                    $scope.output = {};

                                    $scope.dateRange = {};

                                    $scope.clearDates = function (period) {
                                        $scope.dateRange.start_date = period.start_date;
                                        $scope.dateRange.end_date = period.end_date;
                                    };

                                    $scope.milestoneArray = [];

                                    $scope.outputArray = [];

                                    $scope.showPushMilestone = true;
                                    $scope.showInitPhase = false;

                                    $scope.dateDiff = function (date1, date2) {
                                        var dateone = new Date(date1);
                                        var datetwo = new Date(date2);
                                        return (datetwo - dateone) / 1000 / 60 / 60 / 24;
                                    };

                                    $scope.showAddMoreMilestoneBtn = true;

                                    $scope.addMoreMilestone = function () {
                                        $scope.showPushMilestone = true;
                                        $scope.showAddMoreMilestoneBtn = false;
                                    };

                                    $scope.pushMilestone = function (description, period, amount) {

                                        $scope.milestone.activity_id = activity.id;
                                        $scope.milestone.description = description;
                                        $scope.milestone.start_date = $scope.dateRange.start_date;
                                        $scope.milestone.end_date = $scope.dateRange.end_date;
                                        $scope.milestone.working_days = $scope.dateDiff($scope.dateRange.start_date, $scope.dateRange.end_date);
                                        $scope.milestone.period_id = period.id;
                                        $scope.milestone.period = period;
                                        $scope.milestone.amount = amount;
                                        $scope.totalAllocated = $scope.totalAllocated + amount;
                                        $scope.milestoneArray.push($scope.milestone);
                                        $scope.phaseModel.milestones = $scope.milestoneArray;
                                        $scope.milestone = {};
                                        $scope.subMilestone = "";
                                        $scope.amount = 0;
                                        $scope.start_date = "";
                                        $scope.end_date = "";
                                        $scope.successMessage = "Added Milestone";
                                        $scope.showPushMilestone = false;
                                        $scope.showAddMoreMilestoneBtn = true;
                                    };

                                    $scope.addOutput = function (projectType, expenditureCategory, projectOutput, outputValue) {

                                        if ($scope.newProjectOutputForm.$invalid) {
                                            $scope.formHasErrors = true;
                                            return;
                                        }

                                        $scope.output.projectTypeId = projectType.id;
                                        $scope.output.expenditureCategoryId = expenditureCategory.id;
                                        $scope.output.expenditureCategory = expenditureCategory;
                                        $scope.output.projectOutputId = projectOutput.id;
                                        $scope.output.projectOutput = projectOutput;
                                        $scope.output.value = outputValue;
                                        $scope.outputArray.push($scope.output);
                                        $scope.phaseModel.outputs = $scope.outputArray;
                                        $scope.output = {};
                                        $scope.expenditureCategory = "";
                                        $scope.projectOutput = "";
                                        $scope.outputValue = "";
                                        $scope.successMessage = "Added Output";
                                    };
                                };

                                $scope.sessionAllocated = function () {
                                    var total = 0;
                                    for (var i = 0; i < $scope.phaseModel.milestones.length; i++) {
                                        var item = $scope.phaseModel.milestones[i];
                                        total += parseFloat(item.amount);
                                    }
                                    return total;
                                };

                                $scope.enterProjectOutput = function () {
                                    $scope.showProjectOutputForm = true;

                                    GlobalService.projectTypes(function (data) {
                                        $scope.projectTypes = data.items;
                                    });

                                    $scope.loadExpenditureCategories = function (project_type_id) {
                                        PreExecutionService.expenditureCategoryByTaskNature({
                                            project_type_id: project_type_id,
                                            activity_task_nature_id: activity.activity_task_nature_id
                                        }, function (data) {
                                            $scope.expenditureCategories = data.items;
                                        });
                                    };

                                    $scope.loadProjectOutputs = function (expenditureCategoryId) {
                                        PreExecutionService.projectOutputs({expenditureCategoryId: expenditureCategoryId}, function (data) {
                                            $scope.projectOutputs = data.items;
                                        });
                                    };
                                };

                                $scope.createPhase = function () {
                                    $scope.phaseModel.facility_id = activity.facility_id;
                                    PreExecutionService.addPhase({activityId: activity.id}, $scope.phaseModel,
                                        function (data) {
                                            $uibModalInstance.close(data);
                                        },
                                        function (error) {
                                            $scope.errorMessage = error.data.errorMessage;
                                            $scope.errors = error.data.errors;
                                        }
                                    );
                                };
                            };

                            $scope.showAddMoreProjectOutput = true;


                            $scope.enterProjectOutput = function () {
                                $scope.showUpdateProjectOutputForm = true;
                                $scope.projectOutputData = {};

                                $scope.showAddMoreProjectOutput = false;

                                GlobalService.projectTypes(function (data) {
                                    $scope.projectTypes = data.items;
                                });

                                $scope.loadExpenditureCategories = function (project_type_id) {
                                    PreExecutionService.expenditureCategoryByTaskNature({
                                        project_type_id: project_type_id,
                                        activity_task_nature_id: activity.activity_task_nature_id
                                    }, function (data) {
                                        $scope.expenditureCategories = data.items;
                                    });
                                };

                                $scope.loadProjectOutputs = function (expenditureCategoryId) {
                                    PreExecutionService.projectOutputs({expenditureCategoryId: expenditureCategoryId}, function (data) {
                                        $scope.projectOutputs = data.items;
                                    });
                                };

                                $scope.addMoreProjectOutput = function () {

                                    if ($scope.updateProjectOutputForm.$invalid) {
                                        $scope.formHasErrors = true;
                                        return;
                                    }

                                    PreExecutionService.addProjectOutput({activityId: activity.id, facility_id: activity.facility_id}, $scope.projectOutputData,
                                        function (data) {
                                            $scope.successMessage = data.successMessage;
                                            $scope.outputs = data.outputs;
                                            $scope.showUpdateProjectOutputForm = false;
                                            $scope.showAddMoreProjectOutput = true;
                                        },
                                        function (error) {
                                            $scope.errorMessage = error.data.errorMessage;
                                            $scope.errors = error.data.errors;
                                        }
                                    );
                                };
                            };

                            $scope.showEditProjectOutputForm = false;

                            $scope.editOutput = function (output) {
                                $scope.showEditProjectOutputForm = true;

                                GlobalService.projectTypes(function (data) {
                                    $scope.projectOutputUpdate = angular.copy(output);
                                    $scope.projectTypes = data.items;
                                });

                                $scope.loadExpenditureCategories = function (project_type_id) {
                                    PreExecutionService.expenditureCategoryByTaskNature({
                                        project_type_id: project_type_id,
                                        activity_task_nature_id: activity.activity_task_nature_id
                                    }, function (data) {
                                        $scope.expenditureCategories = data.items;
                                    });
                                };

                                $scope.loadProjectOutputs = function (expenditureCategoryId) {
                                    PreExecutionService.projectOutputs({expenditureCategoryId: expenditureCategoryId}, function (data) {
                                        $scope.projectOutputs = data.items;
                                    });
                                };

                                $scope.editProjectOutput = function () {
                                    $scope.projectOutputUpdate.id = output.id;
                                    $scope.projectOutputUpdate.activityId = activity.id;
                                    $scope.projectOutputUpdate.facility_id = activity.facility_id;

                                    if ($scope.editProjectOutputForm.$invalid) {
                                        $scope.formHasErrors = true;
                                        return;
                                    }

                                    PreExecutionService.editProjectOutput($scope.projectOutputUpdate,
                                        function (data) {
                                            $scope.successMessage = data.successMessage;
                                            $scope.outputs = data.outputs;
                                            $scope.showEditProjectOutputForm = false;
                                        },
                                        function (error) {
                                            $scope.errorMessage = error.data.errorMessage;
                                            $scope.errors = error.data.errors;
                                        }
                                    );
                                };

                                $scope.closeProjectOutput = function () {
                                    $scope.showEditProjectOutputForm = false;
                                }
                            };

                            $scope.removeMilestone = function (id) {
                                PreExecutionService.removeMileStone({activityId: activity.id, id: id, facility_id: activity.facility_id},
                                    function (data) {
                                        //Successful function when
                                        $scope.successMessage = data.successMessage;
                                        $scope.milestones = data.milestones;
                                    },
                                    function (error) {
                                        $scope.errorMessage = error.data.errorMessage;
                                    }
                                );
                            };

                            $scope.removeOutput = function (id) {
                                PreExecutionService.removeOutput({activityId: activity.id, id: id, facility_id: activity.facility_id},
                                    function (data) {
                                        $scope.successMessage = data.successMessage;
                                        $scope.outputs = data.outputs;
                                    },
                                    function (error) {
                                        $scope.errorMessage = error.data.errorMessage;
                                    }
                                );
                            };

                            PreExecutionService.periods({activityId: activity.id}, function (data) {
                                $scope.periods = data.periods;
                            });

                            $scope.removePhase = function (id) {
                                PreExecutionService.removePhase({activityId: activity.id, id: id, facility_id: activity.facility_id},
                                    function (data) {
                                        //Successful function when
                                        $scope.successMessage = data.successMessage;
                                        $scope.phases = data.phases;
                                    },
                                    function (error) {
                                        $scope.errorMessage = error.data.errorMessage;
                                    }
                                );
                            };
                            //Function to close modal when cancel button clicked
                            $scope.close = function () {
                                $uibModalInstance.dismiss('cancel');
                            }
                        }
                    });
                    //Called when modal is closed by cancel or to store data
                    modalInstance.result.then(function (data) {
                        $scope.loadActivities(sectionId, budgetClassId, fundSourceId);
                        $scope.successMessage = data.successMessage;
                    }, function () {
                        $scope.loadActivities(sectionId, budgetClassId, fundSourceId);
                    });
                };
            };
        };
    };

    $scope.reloadActivities = function (sectionId, budgetClassId, fundSourceId) {

        $scope.sectionActivities = {};

        $scope.currentPage = 1;
        $scope.perPage = 10;
        $scope.maxSize = 3;

        if (fundSourceId > 0) {
            PreExecutionService.activities({
                sectionId: sectionId,
                budgetClassId: budgetClassId,
                fundSourceId: fundSourceId,
                page: $scope.currentPage,
                perPage: $scope.perPage
            }, function (data) {
                $scope.sectionActivities = data.activities;
            });
        }

        $scope.pageChanged = function () {
            PreExecutionService.activities({
                sectionId: sectionId,
                budgetClassId: budgetClassId,
                fundSourceId: fundSourceId,
                page: $scope.currentPage,
                perPage: $scope.perPage
            }, function (data) {
                $scope.sectionActivities = data.activities;
            });
        };

        $scope.activityPhases = function (activity) {
            var modalInstance = $uibModal.open({
                templateUrl: '/pages/execution/pre_execution/phases.html',
                backdrop: false,
                controller: function ($scope, $uibModalInstance) {

                    $scope.activity = activity;

                    $scope.dateFormat = 'yyyy-MM-dd';

                    $scope.create = true;
                    $scope.update = false;
                    $scope.showProjectOutputForm = false;

                    $scope.showUpdateProjectOutputForm = false;

                    $scope.showUpdateMainTaskForm = false;

                    $scope.updateMainTaskForm = function (arrayList) {
                        $scope.showUpdateMainTaskForm = true;

                        $scope.phaseModel = angular.copy(arrayList[0]);

                        $scope.phaseModel.description = arrayList[0]['phase'];

                        $scope.updateMainTaskInfo = function () {

                            if ($scope.updateMainTask.$invalid) {
                                $scope.formHasErrors = true;
                                return;
                            }
                            $scope.phaseModel.activityId = activity.id;
                            $scope.phaseModel.id = arrayList[0]['activity_phase_id'];

                            PreExecutionService.updateMainTaskData($scope.phaseModel, function (data) {
                                $scope.milestones = data.milestones;
                                $scope.showUpdateMainTaskForm = false;
                                $scope.successMessage = data.successMessage;
                            });
                        }
                    };

                    $scope.showNextBtn = false;

                    PreExecutionService.milestones({activityId: activity.id, facility_id: activity.facility_id}, function (data) {
                        $scope.milestones = data.milestones;
                    });

                    PreExecutionService.outputs({activityId: activity.id, facility_id: activity.facility_id}, function (data) {
                        $scope.outputs = data.outputs;
                    });

                    $scope.showInitPhase = false;
                    $scope.showPushMilestone = false;
                    $scope.showInitBtn = true;

                    $scope.totalAllocated = 0;
                    $scope.amount = 0;

                    $scope.showMinMilestoneForm = false;

                    $scope.addMoreMilestoneToPhase = function (arrayList) {
                        $scope.activity_phase_id = arrayList[0]['activity_phase_id'];
                        $scope.showMinMilestoneForm = true;

                        $scope.create = false;
                        $scope.update = true;

                        $scope.mModel = {};
                        $scope.mModel.amount = 0;
                        $scope.mDateRange = {};

                        $scope.clearDateRange = function (period) {
                            $scope.mModel.start_date = period.start_date;
                            $scope.mModel.end_date = period.end_date;
                            $scope.mModel.period_id = period.id;
                        };

                        $scope.mModel.activity_phase_id = $scope.activity_phase_id;
                        $scope.mModel.amount = $scope.amount;

                        $scope.allocated = function (milestones, amount) {
                            var total = 0;
                            for (var i = 0; i < milestones.length; i++) {
                                var item = milestones[i];
                                total += parseFloat(item.amount);
                            }
                            return ((total + amount) + $scope.totalAllocated);
                        };

                        $scope.exceeded = function (budget, allocated) {
                            return (budget - allocated) < 0;
                        };

                        $scope.insertMilestone = function () {
                            $scope.mModel.facility_id = activity.facility_id;
                            PreExecutionService.addMileStone({activityId: activity.id}, $scope.mModel,
                                function (data) {
                                    $scope.successMessage = data.successMessage;
                                    $scope.milestones = data.milestones;
                                    $scope.mModel.amount = 0;
                                    $scope.showMinMilestoneForm = false;
                                },
                                function (error) {
                                    $scope.errorMessage = error.data.errorMessage;
                                    $scope.errors = error.data.errors;
                                }
                            );
                        }
                    };

                    $scope.showUpdateMilestoneForm = false;
                    $scope.edit = false;

                    $scope.updateMilestoneInfo = function (mModel) {
                        $scope.activity_phase_id = mModel.activity_phase_id;
                        $scope.showUpdateMilestoneForm = true;

                        $scope.totalAllocated = 0;
                        $scope.amount = 0;

                        $scope.create = false;
                        $scope.update = false;
                        $scope.edit = true;
                        $scope.originalAmount = mModel.amount;

                        $scope.mDateRange = {};

                        PreExecutionService.periods({activityId: activity.id}, function (data) {
                            $scope.periods = data.periods;
                            $scope.mModel = angular.copy(mModel);
                            $scope.mModel.start_date = new Date($scope.mModel.start_date);
                            $scope.mModel.end_date = new Date($scope.mModel.end_date);
                            $scope.mModel.amount = 0;
                        });

                        $scope.clearDateRange = function (period) {
                            $scope.mModel.start_date = period.start_date;
                            $scope.mModel.end_date = period.end_date;
                            $scope.mModel.period_id = period.id;
                        };

                        $scope.allocated = function (milestones, amount) {
                            var total = 0;
                            for (var i = 0; i < milestones.length; i++) {
                                var item = milestones[i];
                                total += parseFloat(item.amount);
                            }
                            return (total + amount) - $scope.originalAmount;
                        };

                        $scope.available = function (milestones, amount) {
                            var total = 0;
                            for (var i = 0; i < milestones.length; i++) {
                                var item = milestones[i];
                                total += parseFloat(item.amount);
                            }
                            return (total - $scope.originalAmount) - amount;
                        };

                        $scope.exceeded = function (budget, allocated) {
                            return (budget - allocated) < 0;
                        };

                        $scope.updatePhaseMilestoneDataaaa = function () {
                            $scope.mModel.activity_phase_id = $scope.activity_phase_id;
                            $scope.mModel.period_id = $scope.period.id;
                            $scope.mModel.activityId = activity.id;
                            $scope.mModel.facility_id = activity.facility_id;
                            $scope.mModel.id = mModel.id;

                            PreExecutionService.updateMileStoneData($scope.mModel,
                                function (data) {
                                    $scope.successMessage = data.successMessage;
                                    $scope.milestones = data.milestones;
                                    $scope.mModel.amount = 0;
                                    $scope.showUpdateMilestoneForm = false;
                                },
                                function (error) {
                                    $scope.errorMessage = error.data.errorMessage;
                                    $scope.errors = error.data.errors;
                                }
                            );
                        };

                        $scope.closeUpdatePhaseMilestoneForm = function () {
                            $scope.showUpdateMilestoneForm = false;
                        }
                    };

                    $scope.allocated = function (milestones, amount) {
                        var total = 0;
                        for (var i = 0; i < milestones.length; i++) {
                            var item = milestones[i];
                            total += parseFloat(item.amount);
                        }
                        return ((total + amount) + $scope.totalAllocated);
                    };

                    $scope.revealInitPhase = function () {
                        $scope.showInitPhase = true;
                        $scope.showInitBtn = false;

                        $scope.phaseModel = {};

                        $scope.initPhase = function () {

                            $scope.milestone = {};

                            $scope.output = {};

                            $scope.dateRange = {};

                            $scope.clearDates = function (period) {
                                $scope.dateRange.start_date = period.start_date;
                                $scope.dateRange.end_date = period.end_date;
                            };

                            $scope.milestoneArray = [];

                            $scope.outputArray = [];

                            $scope.showPushMilestone = true;
                            $scope.showInitPhase = false;

                            $scope.dateDiff = function (date1, date2) {
                                var dateone = new Date(date1);
                                var datetwo = new Date(date2);
                                return (datetwo - dateone) / 1000 / 60 / 60 / 24;
                            };

                            $scope.showAddMoreMilestoneBtn = true;

                            $scope.addMoreMilestone = function () {
                                $scope.showPushMilestone = true;
                                $scope.showAddMoreMilestoneBtn = false;
                            };

                            $scope.pushMilestone = function (description, period, amount) {

                                $scope.milestone.activity_id = activity.id;
                                $scope.milestone.description = description;
                                $scope.milestone.start_date = $scope.dateRange.start_date;
                                $scope.milestone.end_date = $scope.dateRange.end_date;
                                $scope.milestone.working_days = $scope.dateDiff($scope.dateRange.start_date, $scope.dateRange.end_date);
                                $scope.milestone.period_id = period.id;
                                $scope.milestone.period = period;
                                $scope.milestone.amount = amount;
                                $scope.totalAllocated = $scope.totalAllocated + amount;
                                $scope.milestoneArray.push($scope.milestone);
                                $scope.phaseModel.milestones = $scope.milestoneArray;
                                $scope.milestone = {};
                                $scope.subMilestone = "";
                                $scope.amount = 0;
                                $scope.start_date = "";
                                $scope.end_date = "";
                                $scope.successMessage = "Added Milestone";
                                $scope.showPushMilestone = false;
                                $scope.showAddMoreMilestoneBtn = true;
                            };

                            $scope.addOutput = function (projectType, expenditureCategory, projectOutput, outputValue) {

                                if ($scope.newProjectOutputForm.$invalid) {
                                    $scope.formHasErrors = true;
                                    return;
                                }

                                $scope.output.projectTypeId = projectType.id;
                                $scope.output.expenditureCategoryId = expenditureCategory.id;
                                $scope.output.expenditureCategory = expenditureCategory;
                                $scope.output.projectOutputId = projectOutput.id;
                                $scope.output.projectOutput = projectOutput;
                                $scope.output.value = outputValue;
                                $scope.outputArray.push($scope.output);
                                $scope.phaseModel.outputs = $scope.outputArray;
                                $scope.output = {};
                                $scope.expenditureCategory = "";
                                $scope.projectOutput = "";
                                $scope.outputValue = "";
                                $scope.successMessage = "Added Output";
                            };
                        };

                        $scope.sessionAllocated = function () {
                            var total = 0;
                            for (var i = 0; i < $scope.phaseModel.milestones.length; i++) {
                                var item = $scope.phaseModel.milestones[i];
                                total += parseFloat(item.amount);
                            }
                            return total;
                        };

                        $scope.enterProjectOutput = function () {
                            $scope.showProjectOutputForm = true;

                            GlobalService.projectTypes(function (data) {
                                $scope.projectTypes = data.items;
                            });

                            $scope.loadExpenditureCategories = function (project_type_id) {
                                PreExecutionService.expenditureCategoryByTaskNature({
                                    project_type_id: project_type_id,
                                    activity_task_nature_id: activity.activity_task_nature_id
                                }, function (data) {
                                    $scope.expenditureCategories = data.items;
                                });
                            };

                            $scope.loadProjectOutputs = function (expenditureCategoryId) {
                                PreExecutionService.projectOutputs({expenditureCategoryId: expenditureCategoryId}, function (data) {
                                    $scope.projectOutputs = data.items;
                                });
                            }
                        };

                        $scope.createPhase = function () {
                            $scope.phaseModel.facility_id = activity.facility_id;
                            PreExecutionService.addPhase({activityId: activity.id}, $scope.phaseModel,
                                function (data) {
                                    $uibModalInstance.close(data);
                                },
                                function (error) {
                                    $scope.errorMessage = error.data.errorMessage;
                                    $scope.errors = error.data.errors;
                                }
                            );
                        };
                    };

                    $scope.showAddMoreProjectOutput = true;


                    $scope.enterProjectOutput = function () {
                        $scope.showUpdateProjectOutputForm = true;
                        $scope.projectOutputData = {};

                        $scope.showAddMoreProjectOutput = false;

                        GlobalService.projectTypes(function (data) {
                            $scope.projectTypes = data.items;
                        });

                        $scope.loadExpenditureCategories = function (project_type_id) {
                            PreExecutionService.expenditureCategoryByTaskNature({
                                project_type_id: project_type_id,
                                activity_task_nature_id: activity.activity_task_nature_id
                            }, function (data) {
                                $scope.expenditureCategories = data.items;
                            });
                        };

                        $scope.loadProjectOutputs = function (expenditureCategoryId) {
                            PreExecutionService.projectOutputs({expenditureCategoryId: expenditureCategoryId}, function (data) {
                                $scope.projectOutputs = data.items;
                            });
                        };

                        $scope.addMoreProjectOutput = function () {

                            if ($scope.updateProjectOutputForm.$invalid) {
                                $scope.formHasErrors = true;
                                return;
                            }

                            PreExecutionService.addProjectOutput({activityId: activity.id, facility_id: activity.facility_id}, $scope.projectOutputData,
                                function (data) {
                                    $scope.successMessage = data.successMessage;
                                    $scope.outputs = data.outputs;
                                    $scope.showUpdateProjectOutputForm = false;
                                    $scope.showAddMoreProjectOutput = true;
                                },
                                function (error) {
                                    $scope.errorMessage = error.data.errorMessage;
                                    $scope.errors = error.data.errors;
                                }
                            );
                        };
                    };

                    $scope.showEditProjectOutputForm = false;

                    $scope.editOutput = function (output) {
                        $scope.showEditProjectOutputForm = true;

                        GlobalService.projectTypes(function (data) {
                            $scope.projectOutputUpdate = angular.copy(output);
                            $scope.projectTypes = data.items;
                        });

                        $scope.loadExpenditureCategories = function (project_type_id) {
                            PreExecutionService.expenditureCategoryByTaskNature({
                                project_type_id: project_type_id,
                                activity_task_nature_id: activity.activity_task_nature_id
                            }, function (data) {
                                $scope.expenditureCategories = data.items;
                            });
                        };

                        $scope.loadProjectOutputs = function (expenditureCategoryId) {
                            PreExecutionService.projectOutputs({expenditureCategoryId: expenditureCategoryId}, function (data) {
                                $scope.projectOutputs = data.items;
                            });
                        };

                        $scope.editProjectOutput = function () {
                            $scope.projectOutputUpdate.id = output.id;
                            $scope.projectOutputUpdate.activityId = activity.id;
                            $scope.projectOutputUpdate.facility_id = activity.facility_id;

                            if ($scope.editProjectOutputForm.$invalid) {
                                $scope.formHasErrors = true;
                                return;
                            }

                            PreExecutionService.editProjectOutput($scope.projectOutputUpdate,
                                function (data) {
                                    $scope.successMessage = data.successMessage;
                                    $scope.outputs = data.outputs;
                                    $scope.showEditProjectOutputForm = false;
                                },
                                function (error) {
                                    $scope.errorMessage = error.data.errorMessage;
                                    $scope.errors = error.data.errors;
                                }
                            );
                        };

                        $scope.closeProjectOutput = function () {
                            $scope.showEditProjectOutputForm = false;
                        }
                    };

                    $scope.removeMilestone = function (id) {
                        PreExecutionService.removeMileStone({activityId: activity.id, id: id, facility_id: activity.facility_id},
                            function (data) {
                                //Successful function when
                                $scope.successMessage = data.successMessage;
                                $scope.milestones = data.milestones;
                            },
                            function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                            }
                        );
                    };

                    $scope.removeOutput = function (id) {
                        PreExecutionService.removeOutput({activityId: activity.id, id: id, facility_id: activity.facility_id},
                            function (data) {
                                $scope.successMessage = data.successMessage;
                                $scope.outputs = data.outputs;
                            },
                            function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                            }
                        );
                    };

                    PreExecutionService.periods({activityId: activity.id}, function (data) {
                        $scope.periods = data.periods;
                    });

                    $scope.removePhase = function (id) {
                        PreExecutionService.removePhase({activityId: activity.id, id: id, facility_id: activity.facility_id},
                            function (data) {
                                //Successful function when
                                $scope.successMessage = data.successMessage;
                                $scope.phases = data.phases;
                            },
                            function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                            }
                        );
                    };
                    //Function to close modal when cancel button clicked
                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    }
                }
            });
            //Called when modal is closed by cancel or to store data
            modalInstance.result.then(function (data) {
                $scope.loadActivities(sectionId, budgetClassId, fundSourceId);
                $scope.successMessage = data.successMessage;
            }, function () {
                $scope.loadActivities(sectionId, budgetClassId, fundSourceId);
            });
        };
    };

    $scope.assignMilestoneAndProjectOutput = function (activity,sectionId, budgetClassId, fundSourceId) {

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/pre_execution/assignMilestoneAndProjectOutput.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, GlobalService) {

                $scope.activity = activity;
                $scope.activity_id = activity.id;

                GlobalService.activityMilestones({activity_id: activity.id},
                    function (data) {
                        $scope.milestones = data.milestones;
                    },
                    function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );

                GlobalService.activityProjectOutputs({activity_id: activity.id},
                    function (data) {
                        $scope.outputs = data.outputs;
                    },
                    function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );

                $scope.retrieveOutMilestoneData = function (arrayList) {
                    GlobalService.retrieveMilestone({activity_phase_id: arrayList[0]['activity_phase_id'], facilityId: activity.facility_id, activityId: activity.id},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.milestones = data.milestones;
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };

                $scope.retrieveOutProjectOutputData = function(){
                    GlobalService.retrieveProjectOutput({facilityId: activity.facility_id,activityId:activity.id},
                        function (data) {
                            $scope.outputs = data.outputs;
                            $scope.successMessage = data.successMessage;
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };

                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        modalInstance.result.then(function (data) {
                $scope.reloadActivities(sectionId, budgetClassId, fundSourceId)
            },
            function () {
                $scope.reloadActivities(sectionId, budgetClassId, fundSourceId)
            });

    };
}