function ExportedTransactionsController($scope,ExportedTransactionService) {
    $scope.title          = 'EXPORTED_TRANSACTIONS';
    $scope.system_codes   = ['OTRMIS','MUSE','NAVISION'];

    $scope.filterChanged = function (filter) {
        $scope.financialYear     = filter.selectedFinancialYearId;
        $scope.financialYearName = filter.selectedFinancialYearName;
        $scope.BudgetType        = filter.selectedBudgetType;
        $scope.selectedAdminHierarchyId  = filter.selectedAdminHierarchyId;
        $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
    };
    
    $scope.loadExportedTransactions = function () {
            $scope.show_exported_data = false;
        ExportedTransactionService.exportedTransactions({admin_hierarchy_id: $scope.selectedAdminHierarchyId,
            budget_type: $scope.BudgetType, system_code: $scope.system_code,financial_year_id:$scope.financialYear},function (response) {
            $scope.exportData = response.exportedTransactions;
            $scope.show_exported_data = true;
        });
    }

}