
function ActivityImplementationPlanController($scope,ActivityImplementationPlanModel, ActivityImplementationService) {

    $scope.activities = ActivityImplementationPlanModel;

    $scope.title = "ACTIVITY_IMPLEMENTATION_PLAN";

    $scope.currentPage = 1;

    $scope.maxSize = 3;

    $scope.pageChanged = function () {
        ActivityImplementationService.paginatedPlan({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.activities = data;
        });
    };
  }

ActivityImplementationPlanController.resolve = {
    ActivityImplementationPlanModel: function (ActivityImplementationService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            ActivityImplementationService.paginatedPlan({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });

        }, 900);
        return deferred.promise;
    }
};