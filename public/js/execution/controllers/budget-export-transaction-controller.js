function BudgetExportTransactionController($scope, BudgetExportTransactionModel, $uibModal, ConfirmDialogService, DeleteBudgetExportTransactionService,
                                PaginatedBudgetExportTransactionService) {

    $scope.budgetExportTransactions = BudgetExportTransactionModel;
    $scope.title = "BUDGET_EXPORT_TRANSACTIONS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedBudgetExportTransactionService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.budgetExportTransactions = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/budget_export_transaction/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllBudgetTransactionTypeService, CreateBudgetExportTransactionService) {

                AllBudgetTransactionTypeService.query(function (data) {
                    $scope.budgetTransactionTypes = data;
                });

                $scope.budgetExportTransactionToCreate = {};

                $scope.store = function () {
                    if ($scope.createBudgetExportTransactionForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateBudgetExportTransactionService.store({perPage: $scope.perPage}, $scope.budgetExportTransactionToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.budgetExportTransactions = data.budgetExportTransactions;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (budgetExportTransactionToEdit, currentPage, perPage) {
        //console.log(budgetExportTransactionToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/budget_export_transaction/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllBudgetTransactionTypeService, UpdateBudgetExportTransactionService) {

                AllBudgetTransactionTypeService.query(function (data) {
                    $scope.budgetTransactionTypes = data;
                    $scope.budgetExportTransactionToEdit = angular.copy(budgetExportTransactionToEdit);
                });

                $scope.update = function () {
                    UpdateBudgetExportTransactionService.update({page: currentPage, perPage: perPage}, $scope.budgetExportTransactionToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.budgetExportTransactions = data.budgetExportTransactions;
                $scope.currentPage = $scope.budgetExportTransactions.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteBudgetExportTransactionService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.budgetExportTransactions = data.budgetExportTransactions;
                        $scope.currentPage = $scope.budgetExportTransactions.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/budget_export_transaction/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedBudgetExportTransactionService,
                                  RestoreBudgetExportTransactionService, EmptyBudgetExportTransactionTrashService, PermanentDeleteBudgetExportTransactionService) {
                TrashedBudgetExportTransactionService.query(function (data) {
                    $scope.trashedBudgetExportTransactions = data;
                });
                $scope.restoreBudgetExportTransaction = function (id) {
                    RestoreBudgetExportTransactionService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBudgetExportTransactions = data.trashedBudgetExportTransactions;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteBudgetExportTransactionService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBudgetExportTransactions = data.trashedBudgetExportTransactions;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyBudgetExportTransactionTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBudgetExportTransactions = data.trashedBudgetExportTransactions;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.budgetExportTransactions = data.budgetExportTransactions;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

BudgetExportTransactionController.resolve = {
    BudgetExportTransactionModel: function (PaginatedBudgetExportTransactionService, $q) {
        var deferred = $q.defer();
        PaginatedBudgetExportTransactionService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};