function ActualTransactionController($scope, $uibModal,AdminHierarchiesService, AllFinancialYearService, RevenueAllocationExpenditureService, GlobalService, FacilityService) {
    $scope.title = 'Revenue, Allocation & Expenditure';

  $scope.system_codes = ['MUSE','NAVISION'];
  $scope.transaction_types = ['EXPENDITURE', 'REVENUE', 'ALLOCATION'];
  $scope.maxSize = 3;
  $scope.currentPage = 1;
  $scope.perPage = 30;
  $total_debit= 0;
  $total_credit= 0;


     //start
  AdminHierarchiesService.getUserAdminHierarchyLevels({noLoader: true}, function (data) {
        $scope.adminLevels = data.adminLevels;
        $scope.adminLevel = $scope.adminLevels[0];
        $scope.sectionLevels = data.sectionLevels;
        $scope.sectionLevel = $scope.sectionLevels[0];
      },
      function (error) {

      });
  AdminHierarchiesService.getUserAdminHierarchy({noLoader: true}, function (data) {
        $scope.userAdminHierarchy = $scope.a1 = data;
        $scope.userAdminHierarchyCopy = $scope.selectedAdminHierarchy = angular.copy($scope.userAdminHierarchy);
        $scope.loadChildren($scope.userAdminHierarchy);
      },
      function (error) {

      });

       $scope.loadChildren = function (a) {
    if (a !== undefined && a !== null) {

      $scope.selectedAdminHierarchy = a;
      $scope.filterChanged(a);
      if (a.hierarchy_position <= 2) {
        getAdminHierarchyChildren(a);
      }
    }
  };

  function getAdminHierarchyChildren(a) {
    AdminHierarchiesService.getAdminHierarchyChildren({adminHierarchyId: a.id}, function (data) {
      a.children = data.adminHierarchies;
    });
  }

  /** load filter */
  $scope.filterChanged = function (filter) {
    $scope.selectedAdminHierarchyId  = filter.selectedAdminHierarchyId;
    $scope.selectedAdminHierarchyLevelPosition = filter.hierarchy_position;
  };

    AllFinancialYearService.query(function (response) {
        $scope.financialYears = response;
    });



    $scope.loadTransaction = function () {

        $scope.header = {
        admin_hierarchy_id: $scope.selectedAdminHierarchyId,
        financial_year_id: $scope.financialYear.id,
        transaction_type: $scope.transaction_type,
        system_code: $scope.system_code,
        page: $scope.currentPage,
        perPage: $scope.perPage
      };
      RevenueAllocationExpenditureService.get($scope.header, function (data) {
      $scope.transactions = data;
    }, function (error) {
      console.log(error);
    });
        // console.log("SUBMIT")
        // console.log($scope.header)
    }


}
