(function () {
    'use strict';

    angular.module('master-module').controller('OpenBudgetReallocationController', OpenBudgetReallocationController);
    OpenBudgetReallocationController.$inject     =  ["$scope", "$uibModal","$window", "ConfirmDialogService", "BudgetReallocationService","selectedReallocation"];
    function OpenBudgetReallocationController($scope, $uibModal,$window, ConfirmDialogService, BudgetReallocationService,selectedReallocation) {

        $scope.title = "Opened reallocation";
        $scope.currentPage = 1;
        $scope.maxSize = 3;

        $scope.pageChanged = function () {
            BudgetReallocationService.getOpen({
                    page: $scope.currentPage,
                    perPage: $scope.perPage
                },function (data) {
                    $scope.reallocations = data.reallocations;
                },
                function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                });
        };
        $scope.pageChanged();

        $scope.addItems = function(r){
            selectedReallocation.set(r);
            $window.location = '/execution#!/budget-reallocations/'+r.id+'?financialYearId='+r.financial_year.id;

        };
        $scope.submit =function(r){
            ConfirmDialogService.showConfirmDialog(
                'Submit Reallocation',
                'Are you sure you what to submit this reallocation request to '+ r.decision_level.next_decisions[0].name+'?')
                .then(function (d) {
                    BudgetReallocationService.submit({
                        id: r.id,
                        decisionLevelId: r.decision_level.next_decisions[0].id
                    }, function (data) {
                        $scope.pageChanged();
                    });
                });
        };

        $scope.createOrModify = function (dataModel) {
            var modalInstance = $uibModal.open({
                templateUrl: "/pages/execution/budget_reallocation/open.html",
                backdrop: false,
                controller:["$scope","$uibModalInstance",function ($scope, $uibModalInstance) {

                    $scope.request = {method:"POST", url:"/form/budgetReAllocations/open"};
                    $scope.reallocationToOpen = dataModel;

                    $uibModalInstance.rendered.then(function () {
                        var options = {
                            beforeSubmit: $scope.validate,
                            success: processResponse,
                            error: failureHandler,
                            headers: {
                                "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
                            }
                        };
                        $("#openReallocationForm").ajaxForm(options);
                    });

                    $scope.validate = function (formData) {
                        $scope.$apply(function () {
                            $scope.inProgress = true;
                            if ($scope.openReallocationForm.$invalid) {
                                $scope.formHasErrors = true;
                                $scope.inProgress = false;
                            }
                        });
                        $scope.$apply();
                        if ($scope.inProgress) {
                            $("#loader").show();
                        }
                        return $scope.inProgress;
                    };

                    var failureHandler = function (response) {
                        $("#loader").hide();
                        $scope.$apply(function () {
                            console.log(response);
                            $scope.errorMessage = response.responseJSON.errorMessage;
                            $scope.errors = response.responseJSON.errors;
                            $scope.inProgress = false;
                        });
                    };

                    function processResponse(response) {
                        $("#loader").hide();
                        if (response.successMessage) {
                            $uibModalInstance.close(response);
                        }
                    }
                    $scope.close = function () {
                        $uibModalInstance.dismiss();
                    };
                }]
            });
            modalInstance.result.then(function (data) {
                    $scope.successMessage = data.successMessage;
                    $scope.pageChanged();
                },
                function () {
                });

        };

        $scope.view = function (id) {
            var modalInstance = $uibModal.open({
                templateUrl: "/pages/execution/budget_reallocation/view.html",
                backdrop: false,
                controller: ["$scope", "$uibModalInstance","BudgetReallocationService", "ConfirmDialogService",function ($scope, $uibModalInstance, BudgetReallocationService,ConfirmDialogService) {
                   
                    $scope.pageChanged = function(){
                        BudgetReallocationService.getItems({
                            id: id,
                            perPage : $scope.perPage,
                            page: $scope.currentPage,
                            rejected: $scope.rejected
                        }, function (data) {
                            $scope.items = data;
                        });
                    };
                    $scope.pageChanged();
                   
                    $scope.close = function () {
                        $uibModalInstance.dismiss();
                    };

                   $scope.delete= function(id) {
                    ConfirmDialogService.showConfirmDialog(
                        'Delete Item',
                        'Are you sure you want to delete')
                    .then(function () {
                        BudgetReallocationService.deleteItem({
                                  itemId: id
                                },
                                function (data) {
                                    $scope.successMessage = data.successMessage;
                                    $scope.pageChanged();
                                },
                                function (error) {
                                    $scope.errorMessage = error.data.errorMessage;
                                }
                            );
                        },
                        function () {
    
                        });  
                   };

                   $scope.reuse= function(id) {
                    ConfirmDialogService.showConfirmDialog(
                        'Re use Item',
                        'Are you sure you want to re use item')
                    .then(function () {
                        BudgetReallocationService.reUseItem({
                                  itemId: id
                                },
                                function (data) {
                                    $scope.successMessage = data.successMessage;
                                    $scope.pageChanged();
                                },
                                function (error) {
                                    $scope.errorMessage = error.data.errorMessage;
                                }
                            );
                        },
                        function () {
    
                        });  
                   };
                }]
            });
            modalInstance.result.then(function (data) {
                    $scope.successMessage = data.successMessage;
                    $scope.pageChanged();
                },
                function () {
                    $scope.pageChanged();
                });

        };

        $scope.delete = function (id) {
            ConfirmDialogService.showConfirmDialog(
                "Confirm Delete Facility!",
                "Are sure you want to delete this?")
                .then(function () {
                        ReferenceDocument.delete({id: id, isNationalGuideline: isNationalGuideline, currentPage: currentPage, perPage: perPage},
                            function (data) {
                                $scope.successMessage = data.successMessage;
                                $scope.pageChanged();
                            }, function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                            }
                        );
                    },
                    function () {
                    });
        };
    }
})();
