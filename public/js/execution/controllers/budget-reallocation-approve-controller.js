(function () {
    'use strict';

    angular
        .module('execution-module')
        .controller('BudgetReallocationApproveController', BudgetReallocationApproveController);

    BudgetReallocationApproveController.$inject = ['$scope', '$uibModal', 'ConfirmDialogService', 'BudgetReallocationService'];

    function BudgetReallocationApproveController($scope, $uibModal, ConfirmDialogService, BudgetReallocationService) {

        $scope.title = "APPROVE_BUDGET_REALLOCATIONS";

        $scope.currentPage = 1;
        $scope.selectedReallocations = [];

        $scope.filterChanged = function (filter) {
            $scope.selectedFinancialYearId = filter.selectedFinancialYearId;
            $scope.selectedFinancialYearName = filter.selectedFinancialYearName;
            $scope.selectedBudgetType = filter.selectedBudgetType;
            $scope.selectedAdminHierarchyId = filter.selectedAdminHierarchyId;
            $scope.selectedAdminHierarchyName = filter.selectedAdminHierarchyName;
            $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
            $scope.selectedSectionLevelPosition = filter.selectedSectionLevelPosition;
            $scope.selectedSectionId = filter.selectedSectionId;
            $scope.selectedSectionName = filter.selectedSectionName;
            $scope.selectedSectionLevelId = filter.selectedSectionLevelId;
            $scope.searchQuery = filter.searchQuery;
            $scope.pageChanged();
        };


        $scope.maxSize = 3;
        $scope.pageChanged = function () {
            if ($scope.selectedAdminHierarchyId === undefined ||
                $scope.selectedSectionId === undefined ||
                $scope.selectedFinancialYearId === undefined ||
                $scope.selectedBudgetType === undefined) {
                return;
            }
            BudgetReallocationService.getReallocationToApprove({
                budgetType: $scope.selectedBudgetType,
                financialYearId: $scope.selectedFinancialYearId,
                adminHierarchyId: $scope.selectedAdminHierarchyId,
                sectionId: $scope.selectedSectionId,
                page: $scope.currentPage,
                perPage: $scope.perPage
            }, function (data) {
                $scope.budgetReallocationItems = data;
            });
        };

        $scope.pageChanged();

        $scope.approve = function () {

            var modalInstance = $uibModal.open({
                templateUrl: '/pages/execution/budget_reallocation_approve/confirm-approve-reallocation.html',
                backdrop: false,
                resolve: {
                    _data: {reallocationsToApprove:$scope.selectedReallocations},
                    action:{action:'APPROVE_REALLOCATION'}
                },
                controller: function ($scope,_data, action, BudgetReallocationService, $uibModalInstance) {
                    $scope.action = action.action;
                    $scope.save = function () {
                        var data = {reallocationsToApprove: _data.reallocationsToApprove, comments:$scope.comments};
                        BudgetReallocationService.approveReallocation({}, data, function (data) {
                            $uibModalInstance.close(data);
                        });
                    };

                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.selectedReallocations = [];
                $scope.pageChanged();
            },
            function () {
                    console.log('Modal dismissed at: ' + new Date());
            });
        };
        $scope.reject = function () {

            var modalInstance = $uibModal.open({
                templateUrl: '/pages/execution/budget_reallocation_approve/confirm-approve-reallocation.html',
                backdrop: false,
                resolve: {
                    _data: {reallocationsToApprove:$scope.selectedReallocations},
                    action:{action:'REJECT_REALLOCATION'}
                    
                },
                controller: function ($scope,_data,action, BudgetReallocationService, $uibModalInstance) {
                    $scope.action = action.action;
                    $scope.save = function () {
                        var data = {reallocationsToApprove: _data.reallocationsToApprove, comments:$scope.comments};
                        BudgetReallocationService.rejectReallocation({}, data, function (data) {
                            $uibModalInstance.close(data);
                        });
                    };

                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.selectedReallocations = [];
                $scope.pageChanged();
            },
            function () {
                    console.log('Modal dismissed at: ' + new Date());
            });
        };

        $scope.toggleToApprove = function (reallocation) {
            var existingReallocation = _.findWhere($scope.selectedReallocations, {
                id: reallocation.id
            });
            if (existingReallocation !== undefined) {
                angular.forEach($scope.selectedReallocations, function (value, index) {
                    if (value.id === reallocation.id) {
                        $scope.selectedReallocations.splice(index, 1);
                    }
                })
            } else {
                $scope.selectedReallocations.push(reallocation);
            }
        };


    }

})();