/**
 * Created by bamsi on 10/11/2017.
 */
function RevenueExportAccountController($scope,$uibModal,RevenueExportAccount)
{
    $scope.title = "REVENUE_BUDGET";
    $scope.currentPage = 1;
    $scope.dateFormat = 'yyyy-MM-DD';
    $scope.maxSize = 3;
    $scope.perPage = 25;
    $scope.system_codes = ['OTRMIS','MUSE','NAVISION'];


     /**
     * @description
     * If main filter changed set selected filters and load AdminHierarchyCeilings
     * @param filter {Object} {selectedFinancialYearId,selectedBudgetType,selectedAdminHierarchyId,selectedSectionId}
     */
    $scope.filterChanged = function (filter) {
        $scope.financialYear     = filter.selectedFinancialYearId;
        $scope.financialYearName = filter.selectedFinancialYearName;
        $scope.selectedAdminHierarchyId  = filter.selectedAdminHierarchyId;
        $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
        $scope.selectedBudgetType = filter.selectedBudgetType;
    };

    $scope.exportRevenue = function (adminHierarchyId, financialYearId,financialSystem) {

       
        RevenueExportAccount.exportRevenue({adminHierarchyId:adminHierarchyId,
                                                        financialYearId:financialYearId,
                                                        comments:'data',
                                                        financialSystem:financialSystem}, function (data) {
                                                            console.log(data)
                                                            if(data.successMessage){
                                                                $scope.successMessage =  data.successMessage;
                                                            }else{
                                                               $scope.errorMessage =  "Some thing went wrong";
                                                            }
                        

                                                        });

        /*
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/revenue_export_account/export-revenue.html',
            backdrop: false,
            controller: function ($scope, BudgetExportAccountService,RevenueExportAccount,$uibModalInstance) {
                $scope.active = 1;
                $scope.status = 0;
                $scope.prev = true;  //disable prev
                $scope.next = false; //enable next
                $scope.sub  = false; //hide submit button
                $scope.disable_sub = true;
                $scope.comment = '';
                $scope.error = 0;

                $scope.nextStep = function () {
                    if($scope.active === 1)
                    {
                        console.log(adminHierarchyId);
                        //check step one - coa segments
                        RevenueExportAccount.sendAccountSegment({adminHierarchyId:adminHierarchyId,financialYearId:financialYearId,
                             financial_system_code: financialSystem},
                        function (data) {
                            //success
                            $scope.active = 2;
                            $scope.status = 1;
                            $scope.prev = false; //enable prev button
                         }, function(data){
                            console.log(data);
                            $scope.error = 1;
                            $scope.segment_error = data.error;
                        });
                    }
                    else if($scope.active === 2)
                    {
                        //check step two
                        RevenueExportAccount.glAccountExist({adminHierarchyId:adminHierarchyId,financialYearId:financialYearId,
                            financial_system_code: financialSystem},
                          function (data) {
                                //success
                                $scope.active = 3;
                                $scope.status = 2;
                                $scope.sub = true;
                            }, function(error){
                                $scope.error = 1;

                            });

                    }
                    else if($scope.active === 3)
                    {
                        //check step three
                        if($scope.comment !== ''){
                            $scope.status = 3;
                            $scope.disable_sub = false;
                        }else{
                            $scope.status = 2;
                            $scope.disable_sub = true;
                        }
                    }
                };
                //prev steps
                $scope.prevStep = function ()
                {
                    if($scope.active === 2)
                    {
                        //check step one - coa segments
                        $scope.active = 1;
                        $scope.prev = true; //disable prev button

                    }else if($scope.active === 3){
                        //check step two
                        $scope.active = 2;
                        $scope.sub = false; //hide submit button

                    }
                };

                //send GL Accounts
                $scope.sendGLAccount = function () {
                    BudgetExportAccountService.sendGLAccount({adminHierarchyId:adminHierarchyId,financialYearId:financialYearId});
                    $scope.close();
                };

                //export budget
                $scope.exportRevenue = function () {
                    //TODO implement this
                    RevenueExportAccount.exportRevenue({adminHierarchyId:adminHierarchyId,
                                                        financialYearId:financialYearId,
                                                        comments:$scope.comment,
                                                        financialSystem:financialSystem}, function (data) {
                        $uibModalInstance.close(data);
                    },function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                        $scope.errors = error.data.errors;
                    });
                };

                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.errorMessage =   data.errorMessage;
                $scope.loadRevenueAccounts(adminHierarchyId, financialYearId,$scope.financialSystem);
            },
            function () {
                console.log('Modal dismissed at: ' + new Date());
            });

        */

    };

    $scope.loadRevenueAccounts = function (admin_hierarchy_id, financial_year_id,financialSystem) {
        $scope.accountsLoaded = false;
        if(admin_hierarchy_id !== undefined && financial_year_id !== undefined && financialSystem !== undefined) {
            RevenueExportAccount.getApproved({
                    adminHierarchyId: admin_hierarchy_id,
                    financialYearId: financial_year_id,
                    financialSystem: financialSystem,
                    currentPage: $scope.currentPage,
                    perPage: $scope.perPage,
                    budgetType: $scope.selectedBudgetType
                },
                function (data) {
                  console.log(data);
                    $scope.accountsLoaded = true;
                    $scope.revenueAccounts = data.revenue;
                    $scope.totalExport = data.exported;
                    $scope.getGrandTotal();
                });
        }
    };

    $scope.regenerateRevenueAccounts = function(){
        if($scope.selectedAdminHierarchyId === undefined ||
            $scope.financialYear === undefined ||
            $scope.selectedBudgetType === undefined) {
                return;
        }
        RevenueExportAccount.approve({
            financialYearId: $scope.financialYear,
            adminHierarchyId: $scope.selectedAdminHierarchyId,
            budgetType: $scope.selectedBudgetType,
            bySelection: false
        },
            function (data) {
                $scope.successMessage = data.successMessage;
        },function (error) {
            $scope.errorMessage = error.data.errorMessage;
        });

    };

    $scope.pageChanged = function () {
        $scope.loadRevenueAccounts($scope.adminHierarchyId,$scope.financialYear.id,$scope.financialSystem);
    };

    $scope.getGrandTotal = function () {
        var total = 0;
        angular.forEach($scope.revenueAccounts.data,function (value) {
            total += parseInt(value.amount);
        });
        $scope.grand_total = total;
    };


}

