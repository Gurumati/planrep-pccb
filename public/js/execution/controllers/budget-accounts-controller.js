(function () {
    'use strict';

    angular
        .module('execution-module')
        .controller('BudgetAccountsController', BudgetAccountsController);

    BudgetAccountsController.$inject = ['$scope', 'BudgetReallocationService', '$location'];
    function BudgetAccountsController($scope, BudgetReallocationService, $location) {
        
        $scope.title = "BUDGET_REALLOCATION_ACCOUNTS";
        $scope.queryParams = $location.search();
        $scope.toSelectedAccounts = [];
        $scope.inProgress = true;
        BudgetReallocationService.getToReallocationAccount({
            budgetType: $scope.queryParams.budgetType,
            financialYearId: $scope.queryParams.financialYearId,
            adminHierarchyId: $scope.queryParams.adminHierarchyId,
            sectionId: $scope.queryParams.sectionId,
            facilityId: $scope.queryParams.selectedFacilityId,
            fundSourceId: $scope.queryParams.fundSourceId
        }, function (data) {
            $scope.reallocationAccounts = data.reallocationAccounts;
            $scope.inProgress = false;
        }, function (error) {
            $scope.inProgress = false;
            $scope.erroMessage = error.data.errorMessage;
            $scope.errors = error.data.errors;
        });

        $scope.toggleFromAccounts = function (a) {
            var existingAccount = _.findWhere($scope.toSelectedAccounts, {
                id: a.id
            });
            if (existingAccount !== undefined) {
                angular.forEach($scope.toSelectedAccounts, function (value, index) {
                    if (value.id === a.id) {
                        $scope.toSelectedAccounts.splice(index, 1);
                    }
                });
            } else {
                $scope.toSelectedAccounts.push(a);
            }
        };

        $scope.continueToReallocation = function () {
            try {
                window.opener.HandleExistingReallocationResult($scope.toSelectedAccounts);
                window.close();
            } catch (err) {}
            window.close();
            return false;
        };




    }
})();
