function ActivityImplementationController($scope,
  $uibModal,
  GlobalService,
  ActivityImplementationService,
  AdminHierarchiesService,
  AllFinancialYearService) {

  $scope.title = "ACTIVITY_IMPLEMENTATION";
  $scope.maxSize = 3;
  $scope.currentPage = 1;
  $scope.perPage = 10;
  ActivityImplementationService.facilityTypes(function (data) {
    $scope.facilityTypes = data.facilityTypes;
  });

  $scope.budgetTypes = [
    {
      "id": 1,
      "name": "APPROVED"
    },
    {
      "id": 2,
      "name": "SUPPLEMENTARY"
    },
    {
      "id": 3,
      "name": "CARRYOVER"
    }
  ];

  $scope.accounts = [{
    "id": 1,
    "name": "PISC"
  },
    {
      "id": 2,
      "name": "Service Provider"
    }];
  AdminHierarchiesService.getUserAdminHierarchyLevels({ noLoader: true }, function (data) {
    $scope.adminLevels = data.adminLevels;
    $scope.adminLevel = $scope.adminLevels[0];
    $scope.sectionLevels = data.sectionLevels;
    $scope.sectionLevel = $scope.sectionLevels[0];
  },
    function (error) {

    });
  AdminHierarchiesService.getUserAdminHierarchy({ noLoader: true }, function (data) {
    $scope.userAdminHierarchy = $scope.a1 = data;
    $scope.userAdminHierarchyCopy = $scope.selectedAdminHierarchy = angular.copy($scope.userAdminHierarchy);
    $scope.loadChildren($scope.userAdminHierarchy);
  },
    function (error) {

    });

  $scope.loadChildren = function (a) {
    if (a !== undefined && a !== null) {
      $scope.selectedAdminHierarchy = a;
      $scope.filterChanged(a);
      if (a.hierarchy_position <= 2) {
        getAdminHierarchyChildren(a);
      } else {
        $scope.loadActivities();
      }
    }
  };

  function getAdminHierarchyChildren(a) {
    AdminHierarchiesService.getAdminHierarchyChildren({ adminHierarchyId: a.id }, function (data) {
      a.children = data.adminHierarchies;
    });
  }

  $scope.create = function (facilityActivity, period) {
    let modalInstance = $uibModal.open({
      templateUrl: '/pages/execution/activity_implementation/create.html',
      backdrop: false,
      controller: function ($scope,
        $http,
        GlobalService,
        $uibModalInstance,
        ActivityImplementationService,
        StatusService) {

        $scope.period = period;
        $scope.facilityActivity = angular.copy(facilityActivity);
        $scope.facilityActivity.budget = parseFloat(facilityActivity.budget);
        $scope.facilityActivity.expenditure = parseFloat(facilityActivity.expenditure);
        $scope.createDataModel = {};
        $scope.createDataModel.urls = [];
        $scope.createDataModel.activity_id = $scope.facilityActivity.activity_id;
        $scope.createDataModel.overall_achievement = $scope.facilityActivity.overall_achievement;
        $scope.createDataModel.period_id = period.id;
        $scope.createDataModel.facility_id = $scope.facilityActivity.facility_id;
        $scope.createDataModel.start_date = new Date(period.start_date);
        $scope.createDataModel.end_date = new Date(period.end_date);
        let x=facilityActivity.expenditure/facilityActivity.budget*100;
        $scope.createDataModel.completion_percentage =x.toFixed(2);
        var implementation = facilityActivity.implementation;
        if (implementation !== null) {
          $scope.createDataModel.id =  implementation.id;
          $scope.createDataModel.status_id =  implementation.status_id;
          $scope.createDataModel.achievement =  implementation.achievement;
          $scope.createDataModel.achievement_value = parseInt(implementation.achievement_value, 10);
          $scope.createDataModel.completion_percentage =  parseInt(implementation.completion_percentage, 10);
          $scope.createDataModel.remarks = implementation.remarks;
          $scope.createDataModel.latitude = parseInt(implementation.latitude, 10);
          $scope.createDataModel.longitude = parseInt(implementation.longitude,10);
          $scope.createDataModel.start_date = new Date(implementation.start_date);
          $scope.createDataModel.end_date = new Date(implementation.end_date);
          $scope.createDataModel.project_output_implemented_value = parseInt(implementation.project_output_implemented_value,10);
          $scope.createDataModel.procurement_method_id = implementation.procurement_method_id;
        }

        GlobalService.activityFacilityProjectOut({
          activityId: $scope.facilityActivity.activity_id,
          facilityId: $scope.facilityActivity.facility_id
        }, function(data){
          if (data) {
             $scope.createDataModel.project_output = data.project_output;
             $scope.createDataModel.planned_value = data.planned_value;
          }
        });

        GlobalService.procurementMethods(function (response) {
          $scope.proMethods = response.items;
        }, function (error) {
          console.log(error);
        });

        //TODO
        // Activity project output by activityId,  and facilityId

        StatusService.query(function (result) {
          $scope.activityStatuses = result;
        });

        $scope.reference_file_names = [];
        $scope.total_uploaded = 0;
        $scope.reverse = true;

        $scope.showPercentageErrorMessage = false;

        $scope.validatePercentage = function (input) {
          if ((input < 0) || (input > 100)) {
            $scope.showPercentageErrorMessage = true;
            $scope.percentageErrorMessage = "Percentage should be between 0 and 100";
          } else {
            $scope.showPercentageErrorMessage = false;
            $scope.percentageErrorMessage = undefined;
          }
        };

        $scope.validPercentage = function (input) {
          return (input < 0) || (input > 100);
        };

        $scope.uploadFile = function (files) {
          var formData = new FormData();
          formData.append('file', files[0]);
          if ($scope.reference_file_names.indexOf(files[0].name) == -1) {
            $scope.reference_file_names.push(files[0].name);
            uploadUrl = "/json/activityImplementation-document";

            $http.post(uploadUrl, formData, {
              withCredentials: true,
              headers: { 'Content-Type': undefined },
              transformRequest: angular.identity
            }).then(
              function (response) {
                if (response.data == 0) {
                  $scope.reference_file_names.pop();
                } else {
                  $scope.createDataModel.urls.push(response.data);
                }


              },
              function (e) {

                $scope.reference_file_names.pop();
                //$scope.errorMessage = error.data.errorMessage;
                console.log(e);
              });
          }
        };
        $scope.remove = function (filename) {
          var index = $scope.reference_file_names.indexOf(filename);
          ActivityImplementationService.removeFile({ "file_url": $scope.createDataModel.urls[index] },
            function (response) {
            },
            function (error) {

            });

          $scope.reference_file_names.splice(index, 1);
          $scope.createDataModel.urls.splice(index, 1);
        };

        $scope.store = function () {
          if ($scope.createActivityImplementationForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }

          $scope.createDataModel.url_names = $scope.reference_file_names;
          ActivityImplementationService.save($scope.createDataModel,
            function (data) {
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
              $scope.errors = error.data.errors;
            }
          );
        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
      $scope.successMessage = "Create Success";
      $scope.loadActivities();
    },
      function () {

      });

  };

  //TODO
  $scope.history = function(activityId, facilityId) {
     let modalInstance = $uibModal.open({
       templateUrl: '/pages/execution/activity_implementation/details.html',
       backdrop: false,
       controller: function($scope,$http,$uibModalInstance,ActivityImplementationService){
        $scope.title = 'Activity Implementation History';
        ActivityImplementationService.activityImplementationHistory({activityId:activityId,facilityId:facilityId},function (data) {
         $scope.activity_implementation =  data.activityHistory;

        });
        $scope.edit = function(item){
          console.log(item);
          $scope.close();
        };
         $scope.close = function () {
           $uibModalInstance.dismiss('cancel');
         };
       }
     });
  };

  /** load filter */
  $scope.filterChanged = function (filter) {
    $scope.selectedAdminHierarchyId = filter.id;
    $scope.selectedAdminHierarchyLevelPosition = filter.hierarchy_position;
  };

  /**load sections */
  GlobalService.costCentres({}, function (data) {
    $scope.sections = [];
    angular.forEach(data.costCentres, function (value, key) {
      if (value.section_level_id == 4) {
        $scope.sections.push(value);
      }
    });
  });

  AllFinancialYearService.query(function (response) {
    $scope.financialYears = response;
  });

  GlobalService.facilityTypes(function (data) {
    $scope.facilityTypes = data.facilityTypes;
  }, function (error) {
    console.log(error);
  });
  //load periods per financial year
  $scope.loadPeriods = function (financialYear) {
    GlobalService.financialYearPeriods({ financialYearId: financialYear.id }, function (data) {
      $scope.periods = data.items;
    }, function (error) {
      console.log(error);
    });
  };
  //load facilities per facility type
  $scope.loadFacilities = function (facilityType) {
    ActivityImplementationService.facilities({ facilityTypeId: facilityType.id }, function (data) {
      $scope.facilities = data.facilities;
    }, function (error) {

    });
  };

  //load budget classes
  $scope.loadBudgetClasses = function () {
    ActivityImplementationService.sectionBudgetClassesWithActivity(function (data) {
      $scope.budgetClasses = data.items;
    }, function (error) {

    });
  };
  //load fund sources based on sbc

  $scope.loadFundSources = function (facility) {
    ActivityImplementationService.costCentreFundSources({ facilityId: facility.id ,financialYearId:$scope.fy.id}, function (data) {
      $scope.fund_sources = data.fund_sources;
    }, function (error) {

    });
  };
  //load activities
  $scope.loadActivities = function () {
    if ($scope.selectedAdminHierarchyId === undefined ||
      $scope.budgetType === undefined ||
      $scope.facility === undefined ||
      $scope.period === undefined ||
      $scope.fy === undefined) {
      return;
    }
    let activityParams = {
      perPage: $scope.perPage,
      page: $scope.currentPage,
      facilityId: $scope.facility.id,
      //  budgetClassId: $scope.budget_class.id,
      fundSourceId: $scope.fund_source.id,
      financialYearId: $scope.fy.id,
      periodId: $scope.period.id,
      budgetType: $scope.budgetType.name,
    };
    ActivityImplementationService.paginated(activityParams, function (data) {
      //TODO
      $scope.activities = data;
      // console.log(data);
    }, function (error) {

    });
  };
}
ActivityImplementationController.resolve = {
  FinancialYearModel: function (AllFinancialYearService, $timeout, $q) {
    let deferred = $q.defer();
    $timeout(function () {
      AllFinancialYearService.query(function (data) {
        deferred.resolve(data);
      }, function (response) {
      });
    }, 900);
    return deferred.promise;
  }
};
