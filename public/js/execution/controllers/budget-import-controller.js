function BudgetImportController($scope, BudgetImportModel, $uibModal, ConfirmDialogService, DeleteBudgetImportService,
                                PaginatedBudgetImportService) {

    $scope.budgetImports = BudgetImportModel;
    $scope.title = "BUDGET_IMPORT";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedBudgetImportService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.budgetImports = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/budget_import/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateBudgetImportService,AllDataSourceService,AllImportMethodService) {

                AllDataSourceService.query(function (data) {
                    $scope.dataSources = data;
                });

                AllImportMethodService.query(function (data) {
                    $scope.importMethods = data;
                });
                
                $scope.budgetImportToCreate = {};

                $scope.store = function () {
                    if ($scope.createBudgetImportForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateBudgetImportService.store({perPage: $scope.perPage}, $scope.budgetImportToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.budgetImports = data.budgetImports;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (budgetImportToEdit, currentPage, perPage) {
        //console.log(budgetImportToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/budget_import/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateBudgetImportService,AllDataSourceService,AllImportMethodService) {

                AllDataSourceService.query(function (data) {
                    $scope.dataSources = data;
                });

                AllImportMethodService.query(function (data) {
                    $scope.importMethods = data;
                });

                $scope.budgetImportToEdit = angular.copy(budgetImportToEdit);
                $scope.update = function () {
                    UpdateBudgetImportService.update({page: currentPage, perPage: perPage}, $scope.budgetImportToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.budgetImports = data.budgetImports;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteBudgetImportService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.budgetImports = data.budgetImports;
                        $scope.currentPage = $scope.budgetImports.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/budget_import/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedBudgetImportService,
                                  RestoreBudgetImportService, EmptyBudgetImportTrashService, PermanentDeleteBudgetImportService) {
                TrashedBudgetImportService.query(function (data) {
                    $scope.trashedBudgetImports = data;
                });
                $scope.restoreBudgetImport = function (id) {
                    RestoreBudgetImportService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBudgetImports = data.trashedBudgetImports;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteBudgetImportService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBudgetImports = data.trashedBudgetImports;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyBudgetImportTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBudgetImports = data.trashedBudgetImports;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.budgetImports = data.budgetImports;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

BudgetImportController.resolve = {
    BudgetImportModel: function (PaginatedBudgetImportService, $q) {
        var deferred = $q.defer();
        PaginatedBudgetImportService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};