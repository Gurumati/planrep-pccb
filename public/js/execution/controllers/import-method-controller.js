function ImportMethodController($scope, ImportMethodModel, $uibModal, ConfirmDialogService, DeleteImportMethodService,
                                PaginatedImportMethodService) {

    $scope.importMethods = ImportMethodModel;
    $scope.title = "IMPORT_METHODS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedImportMethodService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.importMethods = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/import_method/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateImportMethodService) {
                
                $scope.importMethodToCreate = {};

                $scope.store = function () {
                    if ($scope.createImportMethodForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateImportMethodService.store({perPage: $scope.perPage}, $scope.importMethodToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.importMethods = data.importMethods;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (importMethodToEdit, currentPage, perPage) {
        //console.log(importMethodToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/import_method/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateImportMethodService) {
                $scope.importMethodToEdit = angular.copy(importMethodToEdit);
                $scope.update = function () {
                    UpdateImportMethodService.update({page: currentPage, perPage: perPage}, $scope.importMethodToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.importMethods = data.importMethods;
                $scope.currentPage = $scope.importMethods.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteImportMethodService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.importMethods = data.importMethods;
                        $scope.currentPage = $scope.importMethods.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/import_method/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedImportMethodService,
                                  RestoreImportMethodService, EmptyImportMethodTrashService, PermanentDeleteImportMethodService) {
                TrashedImportMethodService.query(function (data) {
                    $scope.trashedImportMethods = data;
                });
                $scope.restoreImportMethod = function (id) {
                    RestoreImportMethodService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedImportMethods = data.trashedImportMethods;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteImportMethodService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedImportMethods = data.trashedImportMethods;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyImportMethodTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedImportMethods = data.trashedImportMethods;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.importMethods = data.importMethods;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

ImportMethodController.resolve = {
    ImportMethodModel: function (PaginatedImportMethodService, $q) {
        var deferred = $q.defer();
        PaginatedImportMethodService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};