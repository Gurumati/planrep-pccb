function PerformanceIndicatorActualValueController($scope, PerformanceIndicators,FinancialYearsService,AllDataSourceService) {

    $scope.title = "PERFORMANCE_INDICATOR_FINANCIAL_YEAR_ACTUAL_VALUES";
    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.perPage = 10;


    $scope.loadPerformanceIndicators = function () {
        PerformanceIndicators.getForUser({searchQuery:$scope.searchQuery,page: $scope.currentPage, perPage: $scope.perPage,noLoader:true}, function (data) {
            $scope.performanceIndicatorLoaded = true;
            $scope.items = data;
            $scope.perPage = data.per_page;
            console.log($scope.items);
        });
    };
    $scope.loadPerformanceIndicators();
    $scope.pageChanged = function () {
         $scope.save(false);
         $scope.loadPerformanceIndicators();
    };
    $scope.searchIndicator = function () {
        $scope.loadPerformanceIndicators();
    };
    $scope.save = function (showMessage) {
        var toSave = {indicators: $scope.items.data};
        PerformanceIndicators.saveActualValues(toSave,function (data) {
            if(showMessage)
                $scope.successMessage = data.successMessage;
        });
    };
    FinancialYearsService.forIndicatorProjection({noLoader:true},function (data) {
        $scope.financialyearloaded = true;
        $scope.financialYears = data.financialYears;
        $scope.executionFinancialYear = data.executionFinancialYear;
    });
    $scope.isExecutionYear = function (f) {
        if( $scope.executionFinancialYear !== null && f.id ===  $scope.executionFinancialYear.id){
            return true;
        }
        else{
            return false;
        }
    };
    $scope.lessEqualToExecutionYear = function (f) {
        if($scope.executionFinancialYear != null) {
            if (f.end_date <= $scope.executionFinancialYear.end_date) {
                return true;
            } else {
                return false;
            }
        }else{
            return false;
        }
    };

    AllDataSourceService.query({},function (data) {
        $scope.dataSourcesLoaded = true;
        $scope.dataSources = data;
    });
    $scope.getFyInitialValues = function (baseline,f) {
        var x = {};
        x.financial_year_id = f.id;
        if(baseline !== undefined && baseline.financial_year_values !== undefined){
            var existing = _.findWhere(baseline.financial_year_values,{financial_year_id:f.id});
            if(existing !== undefined){
                x.planned_value = existing.planned_value;
                x.actual_value = (existing.actual_value !=null)?existing.actual_value:undefined;
                x.data_source_id = existing.data_source_id;
                x.id = existing.id;
            }else{
                x.planned_value = undefined;
                x.actual_value = undefined;
                x.data_source_id = undefined;
            }
        }
        if($scope.executionFinancialYear !== null && $scope.executionFinancialYear !== undefined && $scope.executionFinancialYear.id === f.id){
            x.isExecutionYear = true;
        }
        return x;
    };
}

PerformanceIndicatorActualValueController.resolve = {

};