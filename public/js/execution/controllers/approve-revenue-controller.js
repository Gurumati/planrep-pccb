/**
 * Created by bamsi on 10/11/2017.
 */
function ApproveRevenueController($scope, $uibModal, RevenueExportAccount, AdminHierarchiesService, AllFinancialYearService)
{
    $scope.title = "APPROVE_REVENUE_BUDGET";
    $scope.currentPage = 1;
    $scope.dateFormat = 'yyyy-MM-DD';
    $scope.maxSize = 3;
    $scope.selectedRevenue = [];

    AdminHierarchiesService.getBudgetingChildLevels(function (data) {
        $scope.adminLevels = data.adminLevels;
        if( $scope.adminLevels.length === 1 ) {
            $scope.adminLevel = $scope.adminLevels[0];
        }
        AdminHierarchiesService.getUserAdminHierarchy(function (data) {
            $scope.userAdminHierarchy = data;
            $scope.userAdminHierarchyCopy = angular.copy($scope.userAdminHierarchy);

        });
    });
    $scope.approve = function (bySelection) {
        if($scope.adminHierarchyId === undefined || $scope.financialYear === undefined || $scope.budgetType === undefined){
            return;
        }
        RevenueExportAccount.approve(
            {
                adminHierarchyId: $scope.adminHierarchyId,
                financialYearId: $scope.financialYear.id,
                budgetType: $scope.budgetType,
                bySelection: bySelection
            },
            $scope.selectedRevenue,
            function (data) {
                $scope.successMessage =  data.successMessage;
                $scope.loadRevenueAccounts($scope.adminHierarchyId,$scope.financialYear.id);
            });
    };
    $scope.loadRevenueAccounts = function (admin_hierarchy_id, financial_year_id) {
        RevenueExportAccount.get({adminHierarchyId: admin_hierarchy_id,
                                  financialYearId: financial_year_id,
                                  revenueType: $scope.revenueType,
                                  currentPage:$scope.currentPage,
                                  perPage:$scope.perPage,
                                  searchString: $scope.searchString},
            function (data) {
                    $scope.receivedFundItems = data.revenue;
            });
    };
    $scope.pageChanged  = function () {
        $scope.loadRevenueAccounts($scope.adminHierarchyId,$scope.financialYear.id);
    };

    $scope.searchRevenue = function () {
        $scope.loadRevenueAccounts($scope.adminHierarchyId,$scope.financialYear.id);
    };

    $scope.loadChildren = function (a, adminLevel) {
        var ad1 = parseInt(a.admin_hierarchy_level_id);
        var ad2 = parseInt(adminLevel.id);
        if (ad1 === ad2) {
            $scope.adminHierarchyId = a.id;
            //load revenue accounts
            $scope.loadRevenueAccounts(a.id,$scope.financialYear.id);
        } else {
            if (a.children === undefined) {
                a.children = [];
            }
            AdminHierarchiesService.getAdminHierarchyChildren({adminHierarchyId: a.id}, function (data) {
                a.children = data.adminHierarchies;
            });
        }
    };


    $scope.resetHierarchy = function () {
        $scope.userAdminHierarchy = angular.copy($scope.userAdminHierarchyCopy);
    };

    //load financial year
    AllFinancialYearService.query(function (data) {
        $scope.financialYears = data;
    });






}
