function PEExportController($scope, $uibModal, BudgetExportAccountService ) {

    $scope.title = "EXPORT_PE_BUDGET";

    BudgetExportAccountService.getPE({}, function (data) {
        $scope.PEBudget = data.budget;
    });

    $scope.exportAccounts = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/export-pe-budget/exportInputs.html',
            backdrop: false,
            controller: function ($scope, BudgetExportAccountService,$uibModalInstance) {
                $scope.active = 1; $scope.status = 0;
                $scope.prev = true;  //disable prev
                $scope.next = false; //enable next
                $scope.sub  = false; //hide submit button
                $scope.disable_sub = true;
                $scope.comment = '';
                $scope.error = 0;
                //next step
                $scope.nextStep = function () {
                    if($scope.active === 1)
                    {
                        //send Account Segments
                        BudgetExportAccountService.sendPESegment({}, function(data){
                            //success
                            $scope.active = 2;
                            $scope.status = 1;
                            $scope.prev = false; //enable prev button
                         }, function(data){
                            console.log(data);  
                            $scope.error = 1;
                            $scope.segment_error = data.error; 
                        });
                    }
                    else if($scope.active === 2)
                    {
                        //check step two
                        BudgetExportAccountService.sendPEGL({}, function(data){
                            $scope.error_message = data.error;
                            //$scope.gl_no = $scope.error_message.length;
                            if($scope.error_message !== undefined)
                            {
                                //flag error
                                $scope.error = 2;

                            }else {
                                //success
                                $scope.active = 3;
                                $scope.status = 2;
                                $scope.sub = true;
                            }
                        });

                    }else if($scope.active === 3)
                    {
                        //check step three
                        if($scope.comment !== ''){
                            $scope.status = 3;
                            $scope.disable_sub = false;
                        }else{
                            $scope.status = 2;
                            $scope.disable_sub = true;
                        }
                    }
                };
                //prev steps
                $scope.prevStep = function () {
                    if($scope.active === 2)
                    {
                        //check step one - coa segments
                        $scope.active = 1;
                        $scope.prev = true; //disable prev button

                    }else if($scope.active === 3){
                        //check step two
                        $scope.active = 2;
                        $scope.sub = false; //hide submit button

                    }
                };

                //export budget
                $scope.exportAccounts = function () {
                    BudgetExportAccountService.sendPE({}, function(data){
                        $uibModalInstance.close(data);
                    }, function(data){
                       console.log(data);
                    }
                   );
                };
            
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.errorMessage   = data.errorMessage;
            },
            function () {
                console.log('Modal dismissed at: ' + new Date());
            });

    };
};

