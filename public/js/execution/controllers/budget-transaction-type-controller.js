function BudgetTransactionTypeController($scope, BudgetTransactionTypeModel, $uibModal, ConfirmDialogService, DeleteBudgetTransactionTypeService,
                                PaginatedBudgetTransactionTypeService) {

    $scope.budgetTransactionTypes = BudgetTransactionTypeModel;
    $scope.title = "BUDGET_TRANSACTION_TYPES";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedBudgetTransactionTypeService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.budgetTransactionTypes = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/budget_transaction_type/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateBudgetTransactionTypeService) {
                
                $scope.budgetTransactionTypeToCreate = {};

                $scope.store = function () {
                    if ($scope.createBudgetTransactionTypeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateBudgetTransactionTypeService.store({perPage: $scope.perPage}, $scope.budgetTransactionTypeToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.budgetTransactionTypes = data.budgetTransactionTypes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (budgetTransactionTypeToEdit, currentPage, perPage) {
        //console.log(budgetTransactionTypeToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/budget_transaction_type/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateBudgetTransactionTypeService) {
                $scope.budgetTransactionTypeToEdit = angular.copy(budgetTransactionTypeToEdit);
                $scope.update = function () {
                    UpdateBudgetTransactionTypeService.update({page: currentPage, perPage: perPage}, $scope.budgetTransactionTypeToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.budgetTransactionTypes = data.budgetTransactionTypes;
                $scope.currentPage = $scope.budgetTransactionTypes.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteBudgetTransactionTypeService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.budgetTransactionTypes = data.budgetTransactionTypes;
                        $scope.currentPage = $scope.budgetTransactionTypes.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/budget_transaction_type/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedBudgetTransactionTypeService,
                                  RestoreBudgetTransactionTypeService, EmptyBudgetTransactionTypeTrashService, PermanentDeleteBudgetTransactionTypeService) {
                TrashedBudgetTransactionTypeService.query(function (data) {
                    $scope.trashedBudgetTransactionTypes = data;
                });
                $scope.restoreBudgetTransactionType = function (id) {
                    RestoreBudgetTransactionTypeService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBudgetTransactionTypes = data.trashedBudgetTransactionTypes;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteBudgetTransactionTypeService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBudgetTransactionTypes = data.trashedBudgetTransactionTypes;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyBudgetTransactionTypeTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBudgetTransactionTypes = data.trashedBudgetTransactionTypes;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.budgetTransactionTypes = data.budgetTransactionTypes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

BudgetTransactionTypeController.resolve = {
    BudgetTransactionTypeModel: function (PaginatedBudgetTransactionTypeService, $q) {
        var deferred = $q.defer();
        PaginatedBudgetTransactionTypeService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};