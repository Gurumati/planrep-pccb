function BudgetExportAccountToFinancialSystemController($scope,$route, $filter, DataModel, BudgetExportToFinancialSystemService) {

    $scope.items = DataModel;
    $scope.title = "BUDGET_EXPORT_TO_FINANCIAL_SYSTEMS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        BudgetExportToFinancialSystemService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data;
        });
    };

    $scope.allSelected = false;
    $scope.selectText = "Select All";

    $scope.toggleSelected = function () {
        $scope.allSelected = !$scope.allSelected;
        angular.forEach($scope.items.data, function (item) {
            item.checked = $scope.allSelected;
        });

        /*Change the text*/
        if ($scope.allSelected) {
            $scope.selectText = "Deselect All";
        } else {
            $scope.selectText = "Select All";
        }
    };
    $scope.selectedItems = function () {
        return $filter('filter')($scope.items.data, {checked: true});
    };

    $scope.sendItems = function (items) {
        angular.forEach(items, function (item) {
            BudgetExportToFinancialSystemService.send({id: item.id},
                function (data) {
                    $scope.successMessage = data.successMessage;
                }, function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                }
            );
        });
        $route.reload();
    };
}

BudgetExportAccountToFinancialSystemController.resolve = {
    DataModel: function (BudgetExportToFinancialSystemService, $q) {
        var deferred = $q.defer();
        BudgetExportToFinancialSystemService.paginated({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};