(function () {
    'use strict';

    angular.module('execution-module').controller('BudgetMassReallocationController', BudgetMassReallocationController);
    BudgetMassReallocationController.$inject = ['$scope', 'BudgetReallocationService', 'MoveActivity'];
    function BudgetMassReallocationController($scope, BudgetReallocationService, MoveActivity) {

        $scope.title = "BUDGET_REALLOCATION";

        MoveActivity.getBF(function (data) {
            $scope.budgetClasses = data.budgetClasses;
            $scope.fundSources = data.fundSources;
        });

        $scope.filterChanged = function (filter) {
            $scope.selectedFinancialYearId = filter.selectedFinancialYearId;
            $scope.selectedFinancialYearName = filter.selectedFinancialYearName;
            $scope.selectedBudgetType = filter.selectedBudgetType;
            $scope.selectedAdminHierarchyId = filter.selectedAdminHierarchyId;
            $scope.selectedAdminHierarchyName = filter.selectedAdminHierarchyName;
            $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
            $scope.selectedSectionLevelPosition = filter.selectedSectionLevelPosition;
            $scope.selectedSectionId = filter.selectedSectionId;
            $scope.selectedSectionName = filter.selectedSectionName;
            $scope.searchQuery = filter.searchQuery;
            $scope.loadBudget();
        };
        
        $scope.exportEpicorTransactions = true;

        $scope.loadBudget = function () {
            if ($scope.selectedAdminHierarchyId === undefined ||
                $scope.sectorId === undefined ||
                $scope.selectedFinancialYearId === undefined ||
                $scope.selectedBudgetType === undefined ||
                $scope.selectedAdminHierarchyLevelPosition < 2
            ) {
                return;
            }
            $scope.reallocations = undefined;
            BudgetReallocationService.getMassReallocation({
                budgetType: $scope.selectedBudgetType,
                financialYearId: $scope.selectedFinancialYearId,
                adminHierarchyId: $scope.selectedAdminHierarchyId,
                sectorId:  $scope.sectorId,
                level : $scope.selectedAdminHierarchyLevelPosition
            },
                function (data) {
                    console.log(data);
                    $scope.reallocations = data.reallocations;
                    $scope.fromBudgetClass = data.fromBudgetClass;
                    $scope.toBudgetClass = data.toBudgetClass;
                    $scope.fundSourcesToMove = data.fundSourcesToMove;
                },
                function (error) {

                });
        };
        $scope.exportTransactionsToEpicor = true;
        $scope.exportTransactionsToFfars = false;
        $scope.reallocate = function (r) {
            if (r.id === undefined ||
                $scope.sectorId === undefined ||
                $scope.selectedFinancialYearId === undefined ||
                $scope.selectedBudgetType === undefined ||
                $scope.selectedAdminHierarchyLevelPosition < 2 ||
                $scope.exportTransactionsToEpicor === undefined ||
                $scope.exportTransactionsToFfars === undefined
            ) {
                return;
            }

            BudgetReallocationService.reallocate({
                budgetType: $scope.selectedBudgetType,
                financialYearId: $scope.selectedFinancialYearId,
                adminHierarchyId: r.id,
                sectorId: $scope.sectorId,
                exportTransactionsToEpicor : $scope.exportTransactionsToEpicor,
                exportTransactionsToFfars : $scope.exportTransactionsToFfars
            },
                function (data) {
                   console.log(data);
                 //  $scope.loadBudget();
                   r.fromBudget = data.fromBudget;
                   r.toBudget = data.toBudget;
                   r.fromCeiling = data.fromCeiling;
                   r.toCeiling = data.toCeiling;
                },
                function (error) {
                  console.log(error);
                });
        };

    }
})();

