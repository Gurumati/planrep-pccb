function ReceivedFundItemController($scope,
                                    ReceivedFundItemModel,
                                    $uibModal,
                                    ConfirmDialogService,
                                    DeleteReceivedFundItemService,
                                    CanProjectGfsCodeService,
                                    PaginatedReceivedFundItemService) {

    $scope.receivedFundItems = ReceivedFundItemModel;
    $scope.title = "RECEIVED_FUNDS";

    $scope.currentPage = 1;

    $scope.dateFormat = 'yyyy-MM-DD';

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedReceivedFundItemService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.receivedFundItems = data;
        });
    };

    $scope.printFunds = function(divName){
        var docHead = document.head.outerHTML;
        var printContents = document.getElementById(divName).outerHTML;
        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

        var newWin = window.open("", "_blank", winAttr);
        var writeDoc = newWin.document;
        writeDoc.open();
        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
        writeDoc.close();
        newWin.focus();
    };

    $scope.totalReceived = function () {
        var total = 0;
        angular.forEach($scope.receivedFundItems.data, function (value, key) {
            // $scope.remainingBalance = parseFloat($scope.receivedFundItems.data[i].ceiling_amount) - total;
            total += parseFloat(value.received_amount);
        });
        $scope.total_received = total;
        // console.log($scope.total_received);
    };

    $scope.getSum = function(values){
        var sum = 0;
        angular.forEach(values, function (values,key){
            sum = sum + parseFloat(values.received_amount);
        });
        return sum;
    };

    $scope.$watch('receivedFundItems', function () {
        $scope.totalReceived();
    });

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/received_fund_item/create.html',
            backdrop: false,
            controller: function ($scope,
                                  $uibModalInstance,
                                  AllReceivedFundService,
                                  CanProjectFundSourceService,
                                  CanProjectGfsCodeService,
                                  AllPeriodService,GlobalService,
                                  CreateReceivedFundItemService) {

                CanProjectFundSourceService.get(function (data) {
                    console.log(data);
                    $scope.fundSources = data.fund_sources;
                });


                GlobalService.onlyRevenueOnes(function (data) {
                    $scope.gfsCodes = data.gfs_codes;
                });

                $scope.loadCanProjectGfsCodes = function (id) {
                    CanProjectGfsCodeService.get({id: id}, function (data) {
                            $scope.gfsCodes = data.gfsCodes;
                            console.log(data.gfsCodes + " GFS Code");
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };

                AllPeriodService.query(function (data) {
                    $scope.periods = data;
                });

                AllReceivedFundService.query(function (data) {
                    $scope.received_funds = data;
                });

                $scope.receivedFundItemToCreate = {};

                $scope.store = function () {
                    if ($scope.createReceivedFundItemForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateReceivedFundItemService.store({perPage: $scope.perPage}, $scope.receivedFundItemToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.receivedFundItems = data.receivedFundItems;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (receivedFundItemToEdit, currentPage, perPage) {
        //console.log(receivedFundItemToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/received_fund_item/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllReceivedFundService,SingleGfsCodeService,CanProjectFundSourceService,CanProjectGfsCodeService,AllPeriodService, UpdateReceivedFundItemService) {

                CanProjectFundSourceService.get(function (data) {
                    $scope.fundSources = data.fund_sources;
                    $scope.receivedFundItemToEdit = angular.copy(receivedFundItemToEdit);
                    $scope.receivedFundItemToEdit.date_received = new Date($scope.receivedFundItemToEdit.date_received);
                });

                $scope.loadCanProjectGfsCodes = function (id) {
                    CanProjectGfsCodeService.get({id: id}, function (data) {
                            $scope.gfsCodes = data.gfsCodes;
                            $("#fakeGfsCode").hide();
                            $("#realGfsCode").show();
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };

                SingleGfsCodeService.query({id:receivedFundItemToEdit.gfs_code_id}, function (data) {
                        $scope.allGfsCodes = data;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );

                AllPeriodService.query(function (data) {
                    $scope.periods = data;
                });

                AllReceivedFundService.query(function (data) {
                    $scope.received_funds = data;
                });

                $scope.update = function () {
                    UpdateReceivedFundItemService.update({page: currentPage, perPage: perPage}, $scope.receivedFundItemToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.receivedFundItems = data.receivedFundItems;
                $scope.currentPage = $scope.receivedFundItems.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteReceivedFundItemService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.receivedFundItems = data.receivedFundItems;
                        $scope.currentPage = $scope.receivedFundItems.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/received_fund_item/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedReceivedFundItemService,
                                  RestoreReceivedFundItemService, EmptyReceivedFundItemTrashService, PermanentDeleteReceivedFundItemService) {
                TrashedReceivedFundItemService.query(function (data) {
                    $scope.trashedReceivedFundItems = data;
                });
                $scope.restoreReceivedFundItem = function (id) {
                    RestoreReceivedFundItemService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedReceivedFundItems = data.trashedReceivedFundItems;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteReceivedFundItemService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedReceivedFundItems = data.trashedReceivedFundItems;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyReceivedFundItemTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedReceivedFundItems = data.trashedReceivedFundItems;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.receivedFundItems = data.receivedFundItems;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

ReceivedFundItemController.resolve = {
    ReceivedFundItemModel: function (PaginatedReceivedFundItemService, $q) {
        var deferred = $q.defer();
        PaginatedReceivedFundItemService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};