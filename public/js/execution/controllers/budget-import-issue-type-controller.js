function BudgetImportIssueTypeController($scope, BudgetImportIssueTypeModel, $uibModal, ConfirmDialogService, DeleteBudgetImportIssueTypeService,
                                PaginatedBudgetImportIssueTypeService) {

    $scope.budgetImportIssueTypes = BudgetImportIssueTypeModel;
    $scope.title = "BUDGET_IMPORT_ISSUE_TYPES";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedBudgetImportIssueTypeService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.budgetImportIssueTypes = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/budget_import_issue_type/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateBudgetImportIssueTypeService) {
                
                $scope.budgetImportIssueTypeToCreate = {};

                $scope.store = function () {
                    if ($scope.createBudgetImportIssueTypeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateBudgetImportIssueTypeService.store({perPage: $scope.perPage}, $scope.budgetImportIssueTypeToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.budgetImportIssueTypes = data.budgetImportIssueTypes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (budgetImportIssueTypeToEdit, currentPage, perPage) {
        //console.log(budgetImportIssueTypeToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/budget_import_issue_type/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateBudgetImportIssueTypeService) {
                $scope.budgetImportIssueTypeToEdit = angular.copy(budgetImportIssueTypeToEdit);
                $scope.update = function () {
                    UpdateBudgetImportIssueTypeService.update({page: currentPage, perPage: perPage}, $scope.budgetImportIssueTypeToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.budgetImportIssueTypes = data.budgetImportIssueTypes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteBudgetImportIssueTypeService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.budgetImportIssueTypes = data.budgetImportIssueTypes;
                        $scope.currentPage = $scope.dataSources.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/budget_import_issue_type/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedBudgetImportIssueTypeService,
                                  RestoreBudgetImportIssueTypeService, EmptyBudgetImportIssueTypeTrashService, PermanentDeleteBudgetImportIssueTypeService) {
                TrashedBudgetImportIssueTypeService.query(function (data) {
                    $scope.trashedBudgetImportIssueTypes = data;
                });
                $scope.restoreBudgetImportIssueType = function (id) {
                    RestoreBudgetImportIssueTypeService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBudgetImportIssueTypes = data.trashedBudgetImportIssueTypes;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteBudgetImportIssueTypeService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBudgetImportIssueTypes = data.trashedBudgetImportIssueTypes;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyBudgetImportIssueTypeTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBudgetImportIssueTypes = data.budgetImportIssueTypes;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.budgetImportIssueTypes = data.budgetImportIssueTypes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

BudgetImportIssueTypeController.resolve = {
    BudgetImportIssueTypeModel: function (PaginatedBudgetImportIssueTypeService, $q) {
        var deferred = $q.defer();
        PaginatedBudgetImportIssueTypeService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};