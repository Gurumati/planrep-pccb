function SetupSegmentExportController($scope, UserAdminHierarchy, FacilityService, $route, $timeout,
                                      GlobalService,AllFinancialYearService,
                                      AdminHierarchiesService) {
                                          

    $scope.title = "MUSE_SETUP_SEGMENTS_EXPORT";
    $scope.system_codes  = ['OTRMIS','MUSE','NPMIS'];

    /**
     * @description
     * If main filter changed set selected filters and load AdminHierarchyCeilings
     * @param filter {Object} {selectedFinancialYearId,selectedBudgetType,selectedAdminHierarchyId,selectedSectionId}
     */
     $scope.filterChanged = function (filter) {
        $scope.financialYear     = filter.selectedFinancialYearId;
        $scope.financialYearName = filter.selectedFinancialYearName;
        $scope.budgetType        = filter.selectedBudgetType;
        $scope.selectedAdminHierarchyId  = filter.selectedAdminHierarchyId;
        $scope.system_code  = filter.system_code;
        $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
     };

    $scope.showFacilities = false;
    $scope.showSubBudgetClasses = false;
    $scope.showFundTypes = false;
    $scope.showFundSources = false;
    $scope.showpProjects = false;
    $scope.showGFSCodes = false;
    $scope.showObjectives = false;
    $scope.showInstitutionEmployees = false;
    $scope.showActivities = false;

    $scope.showAlertSuccess = false;
    $scope.showAlertError = false;

    AllFinancialYearService.query(function (data) {
        $scope.financialYears = data;
    });

    $scope.alertSuccess = function () {
        $scope.showAlertSuccess = true;
        $timeout(function () {
            $scope.showAlertSuccess = false;
            //$route.reload();
        }, 5000);
    };

    $scope.alertError = function () {
        $scope.showAlertError = true;
        $timeout(function () {
            $scope.showAlertError = false;
            //$route.reload();
        }, 5000);
    };

    $scope.exportAllFacilities = function () {
        GlobalService.exportAllFacilities(
            function (data) {
                $scope.successMessage = data.successMessage;
                $scope.alertSuccess();
            }, function (error) {
                $scope.successMessage = error.data.successMessage;
                $scope.alertError();
            }
        );
    };

    $scope.exportAllSubBudgetClasses = function () {
        GlobalService.exportAllSubBudgetClasses(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.alertSuccess();
            }, function (error) {
                $scope.errorMessage = error.data.errorMessage;
                $scope.alertError();
            }
        );
    };

    $scope.exportAllFundTypes = function () {
        GlobalService.exportAllFundTypes(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.alertSuccess();
            }, function (error) {
                $scope.errorMessage = error.data.errorMessage;
                $scope.alertError();
            }
        );
    };

    $scope.exportAllFundSources = function () {
        GlobalService.exportAllFundSources(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.alertSuccess();
            }, function (error) {
                $scope.errorMessage = error.data.errorMessage;
                $scope.alertError();
            }
        );
    };

    $scope.exportAllProjects = function () {
        GlobalService.exportAllProjects(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.alertSuccess();
            }, function (error) {
                $scope.errorMessage = error.data.errorMessage;
                $scope.alertError();
            }
        );
    };

    $scope.exportAllObjectives = function () {
        GlobalService.exportAllObjectives({financialYearId:$scope.financialYear,budgetType:$scope.budgetType,adminHierarchyId:$scope.selectedAdminHierarchyId,system_code:$scope.system_code},function (data) {
                $scope.successMessage = data.successMessage;
                $scope.alertSuccess();
            }, function (error) {
                $scope.errorMessage = error.data.errorMessage;
                $scope.alertError();
            }
        );
    };
    $scope.exportAllInstitutionEmployees = function () {
        GlobalService.exportAllInstitutionEmployees({financialYearId:$scope.financialYear,budgetType:$scope.budgetType,adminHierarchyId:$scope.selectedAdminHierarchyId},function (data) {
                $scope.successMessage = data.successMessage;
                $scope.alertSuccess();
            }, function (error) {
                $scope.errorMessage = error.data.errorMessage;
                $scope.alertError();
            }
        );
    };

    $scope.exportAllActivities = function () {
        GlobalService.exportAllActivities({financialYearId:$scope.financialYear,budgetType:$scope.budgetType,adminHierarchyId:$scope.selectedAdminHierarchyId},function (data) {
                $scope.successMessage = data.successMessage;
                $scope.alertSuccess();
            }, function (error) {
                $scope.errorMessage = error.data.errorMessage;
                $scope.alertError();
            }
        );
    };

    $scope.exportAllGfsCodes = function () {
        GlobalService.exportAllGfsCodes(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.alertSuccess();
            }, function (error) {
                $scope.errorMessage = error.data.errorMessage;
                $scope.alertError();
            }
        );
    };

    // $scope.exportAllActivities = function (adminHierarchyId, facility_type,financialYearId,budgetType) {
    //   let activities = {adminHierarchyId:adminHierarchyId,
    //     facility_type: facility_type,
    //     financialYearId:financialYearId,
    //     budget_type:budgetType
    //   };
    //     GlobalService.exportAllActivities(
    //       activities,
    //       function (data) {
    //             $scope.successMessage = data.successMessage;
    //             $scope.alertSuccess();
    //         }, function (error) {
    //             $scope.errorMessage = error.data.errorMessage;
    //             $scope.alertError();
    //         }
    //     );
    // };

    $scope.loadData = function (segment_id) {
        console.log(segment_id);
        $scope.userAdminHierarchy = UserAdminHierarchy;
        $scope.userAdminHierarchyCopy = angular.copy(UserAdminHierarchy);
        GlobalService.facilityTypes(function (data) {
            $scope.facilityTypes = data.facilityTypes;
        });

        $scope.loadChildren = function (a, facilityType) {
            var ad1 = parseInt(a.admin_hierarchy_level_id);
            var ad2 = parseInt(facilityType.admin_hierarchy_level_id);
            if (ad1 === ad2) {
                FacilityService.facilitiesByTypeAndHierarchy({
                    admin_hierarchy_id: a.id,
                    facility_type_id: facilityType.id
                }, function (data) {
                    $scope.facilities = data.facilities;
                    $scope.admin = a;
                    $scope.facility_type = facilityType;


                    $scope.selectedFacilities = [];

                    $scope.checkAll = function () {
                        if ($scope.selectedAll) {
                            $scope.selectedAll = true;
                        } else {
                            $scope.selectedAll = false;
                        }
                        angular.forEach($scope.facilities, function (item) {
                            item.selected = $scope.selectedAll;
                        });
                    };

                    $scope.toggleFacility = function (a) {
                        var existingAccount = _.findWhere($scope.selectedFacilities, {id: a.id});
                        if (existingAccount !== undefined) {
                            angular.forEach($scope.selectedFacilities, function (value, index) {
                                if (value.id === a.id) {
                                    $scope.selectedFacilities.splice(index, 1);
                                }
                            });
                        } else {
                            $scope.selectedFacilities.push(a);
                        }

                        $scope.exportSelectedFacilities = function () {
                            GlobalService.exportAllSelectedFacilities($scope.selectedFacilities,
                                function (data) {
                                    $scope.successMessage = data.successMessage;
                                    $scope.alertSuccess();
                                }, function (error) {
                                    $scope.errorMessage = error.data.errorMessage;
                                    $scope.alertError();
                                }
                            );
                        };
                    };
                });
            } else {
                if (a.children === undefined) {
                    a.children = [];
                }
                AdminHierarchiesService.getAdminHierarchyChildren({adminHierarchyId: a.id}, function (data) {
                    a.children = data.adminHierarchies;
                });
            }
        };

        if (segment_id === '1') {
            GlobalService.onlyCouncils(function (data) {
                $scope.councils = data.councils;
            });
            $scope.showFacilities = true;
            $scope.showSubBudgetClasses = false;
            $scope.showFundTypes = false;
            $scope.showFundSources = false;
            $scope.showProjects = false;
            $scope.showGFSCodes = false;
            $scope.showObjectives = false;
            $scope.showInstitutionEmployees = false;
            $scope.showActivities = false;



            $scope.resetHierarchy = function () {
                $scope.userAdminHierarchy = angular.copy($scope.userAdminHierarchyCopy);
            };
        }
        if (segment_id === '2') {
            $scope.showFacilities = false;
            $scope.showSubBudgetClasses = true;
            $scope.showFundTypes = false;
            $scope.showFundSources = false;
            $scope.showProjects = false;
            $scope.showGFSCodes = false;
            $scope.showObjectives = false;
            $scope.showInstitutionEmployees = false;
            $scope.showActivities = false;

            GlobalService.budgetClasses(function (data) {
                $scope.subBudgetClasses = data.budgetClasses;

                $scope.selectedSubBudgetClasses = [];

                $scope.checkAll = function () {
                    if ($scope.selectedAll) {
                        $scope.selectedAll = true;
                    } else {
                        $scope.selectedAll = false;
                    }
                    angular.forEach($scope.subBudgetClasses, function (item) {
                        item.selected = $scope.selectedAll;
                    });
                };

                $scope.toggleSubBudgetClass = function (a) {
                    var existingAccount = _.findWhere($scope.selectedSubBudgetClasses, {id: a.id});
                    if (existingAccount !== undefined) {
                        angular.forEach($scope.selectedSubBudgetClasses, function (value, index) {
                            if (value.id === a.id) {
                                $scope.selectedSubBudgetClasses.splice(index, 1);
                            }
                        });
                    } else {
                        $scope.selectedSubBudgetClasses.push(a);
                    }

                    $scope.exportSelectedSubBudgetClasses = function () {
                        GlobalService.exportAllSelectedSubBudgetClasses($scope.selectedSubBudgetClasses,
                            function (data) {
                                $scope.successMessage = data.successMessage;
                                $scope.alertSuccess();
                            }, function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                                $scope.alertSuccess();
                            }
                        );
                    };
                };
            });

        }
        if (segment_id === '3') {
            $scope.showFacilities = false;
            $scope.showSubBudgetClasses = false;
            $scope.showFundTypes = true;
            $scope.showFundSources = false;
            $scope.showProjects = false;
            $scope.showGFSCodes = false;
            $scope.showObjectives = false;
            $scope.showInstitutionEmployees = false;
            $scope.showActivities = false;

            GlobalService.fundTypes(function (data) {
                $scope.fundTypes = data.fundTypes;
            });

            $scope.selectedFundTypes = [];

            $scope.checkAll = function () {
                if ($scope.selectedAll) {
                    $scope.selectedAll = true;
                } else {
                    $scope.selectedAll = false;
                }
                angular.forEach($scope.fundTypes, function (item) {
                    item.selected = $scope.selectedAll;
                });
            };

            $scope.toggleFundType = function (a) {
                var existingAccount = _.findWhere($scope.selectedFundTypes, {id: a.id});
                if (existingAccount !== undefined) {
                    angular.forEach($scope.selectedFundTypes, function (value, index) {
                        if (value.id === a.id) {
                            $scope.selectedFundTypes.splice(index, 1);
                        }
                    });
                } else {
                    $scope.selectedFundTypes.push(a);
                }

                $scope.exportSelectedFundTypes = function () {
                    GlobalService.exportAllSelectedFundTypes($scope.selectedFundTypes,
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.alertSuccess();
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.alertSuccess();
                        }
                    );
                };
            };
        }
        if (segment_id === '4') {
            $scope.showFacilities = false;
            $scope.showSubBudgetClasses = false;
            $scope.showFundTypes = false;
            $scope.showFundSources = true;
            $scope.showProjects = false;
            $scope.showGFSCodes = false;
            $scope.showObjectives = false;
            $scope.showInstitutionEmployees = false;
            $scope.showActivities = false;
            GlobalService.fundSources(function (data) {
                $scope.fundSources = data.fundSources;
            });

            $scope.selectedFundSources = [];

            $scope.checkAll = function () {
                if ($scope.selectedAll) {
                    $scope.selectedAll = true;
                } else {
                    $scope.selectedAll = false;
                }
                angular.forEach($scope.fundSources, function (item) {
                    item.selected = $scope.selectedAll;
                });
            };

            $scope.toggleFundSource = function (a) {
                var existingAccount = _.findWhere($scope.selectedFundSources, {id: a.id});
                if (existingAccount !== undefined) {
                    angular.forEach($scope.selectedFundSources, function (value, index) {
                        if (value.id === a.id) {
                            $scope.selectedFundSources.splice(index, 1);
                        }
                    });
                } else {
                    $scope.selectedFundSources.push(a);
                }

                $scope.exportSelectedFundSources = function () {
                    GlobalService.exportAllSelectedFundSources($scope.selectedFundSources,
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.alertSuccess();
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.alertSuccess();
                        }
                    );
                };
            };
        }
        if (segment_id === '5') {
            $scope.showFacilities = false;
            $scope.showSubBudgetClasses = false;
            $scope.showFundTypes = false;
            $scope.showFundSources = false;
            $scope.showProjects = true;
            $scope.showGFSCodes = false;
            $scope.showObjectives = false;
            $scope.showInstitutionEmployees = false;
            $scope.showActivities = false;

            GlobalService.projects(function (data) {
                $scope.projects = data.projects;
            });

            $scope.selectedProjects = [];

            $scope.checkAll = function () {
                if ($scope.selectedAll) {
                    $scope.selectedAll = true;
                } else {
                    $scope.selectedAll = false;
                }
                angular.forEach($scope.projects, function (item) {
                    item.selected = $scope.selectedAll;
                });
            };

            $scope.toggleProject = function (a) {
                var existingAccount = _.findWhere($scope.selectedProjects, {id: a.id});
                if (existingAccount !== undefined) {
                    angular.forEach($scope.selectedProjects, function (value, index) {
                        if (value.id === a.id) {
                            $scope.selectedProjects.splice(index, 1);
                        }
                    });
                } else {
                    $scope.selectedProjects.push(a);
                }

                $scope.exportSelectedProjects = function () {
                    GlobalService.exportAllSelectedProjects($scope.selectedProjects,
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.alertSuccess();
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.alertSuccess();
                        }
                    );
                };
            };
        }
        if (segment_id === '6') {
            GlobalService.gfsCodes(function (data) {
                $scope.gfsCodes = data.gfsCodes;
            });
            $scope.showFacilities = false;
            $scope.showSubBudgetClasses = false;
            $scope.showFundTypes = false;
            $scope.showFundSources = false;
            $scope.showProjects = false;
            $scope.showGFSCodes = true;
            $scope.showObjectives = false;
            $scope.showInstitutionEmployees = false;
            $scope.showActivities = false;

            $scope.selectedGfsCodes = [];

            $scope.checkAll = function () {
                if ($scope.selectedAll) {
                    $scope.selectedAll = true;
                } else {
                    $scope.selectedAll = false;
                }
                angular.forEach($scope.gfsCodes, function (item) {
                    item.selected = $scope.selectedAll;
                });
            };

            $scope.toggleGfsCode = function (a) {
                var existingAccount = _.findWhere($scope.selectedGfsCodes, {id: a.id});
                if (existingAccount !== undefined) {
                    angular.forEach($scope.selectedGfsCodes, function (value, index) {
                        if (value.id === a.id) {
                            $scope.selectedGfsCodes.splice(index, 1);
                        }
                    });
                } else {
                    $scope.selectedGfsCodes.push(a);
                }

                $scope.exportSelectedGfsCodes = function () {
                    GlobalService.exportAllSelectedGfsCodes($scope.selectedGfsCodes,
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.alertSuccess();
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.alertSuccess();
                        }
                    );
                };
            };
        }

        if (segment_id === '7') {
            $scope.showFacilities = false;
            $scope.showSubBudgetClasses = false;
            $scope.showFundTypes = false;
            $scope.showFundSources = false;
            $scope.showProjects = false;
            $scope.showGFSCodes = false;
            $scope.showObjectives = true;
            $scope.showInstitutionEmployees = false;
            $scope.showActivities = false;

            $scope.currentPage = 1;
            $scope.perPage = 150;
            $scope.maxSize = 3;

            GlobalService.objectives({financialYearId:$scope.financialYear,budgetType:$scope.budgetType,adminHierarchyId:$scope.selectedAdminHierarchyId,system_code: $scope.system_code,page:$scope.currentPage,perPage:$scope.perPage},function (data) {
                $scope.objectives = data.objectives;
            });


                $scope.selectedObjectives = [];

                $scope.checkAll = function () {
                    if ($scope.selectedAll) {
                        $scope.selectedAll = true;
                    } else {
                        $scope.selectedAll = false;
                    }
                    angular.forEach($scope.objectives, function (item) {
                        item.selected = $scope.selectedAll;
                    });
                };

                $scope.toggleObjective = function (a) {
                  var existingAccount = _.findWhere($scope.selectedObjectives, {id: a.id});
                     if (existingAccount !== undefined) {
                        angular.forEach($scope.selectedObjectives, function (value, index) {
                            if (value.id === a.id) {
                                $scope.selectedObjectives.splice(index, 1);
                            }
                        });
                    } else {
                        $scope.selectedObjectives.push(a);
                    }

                    $scope.exportSelectedObjectives = function () {
                        GlobalService.exportAllSelectedObjectives($scope.selectedObjectives,
                            function (data) {
                                $scope.successMessage = data.successMessage;
                                $scope.alertSuccess();
                            }, function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                                $scope.alertSuccess();
                            }
                        );
                    };
                };
        }

        if (segment_id === '8') {
            $scope.showFacilities = false;
            $scope.showSubBudgetClasses = false;
            $scope.showFundTypes = false;
            $scope.showFundSources = false;
            $scope.showProjects = false;
            $scope.showGFSCodes = false;
            $scope.showObjectives = false;
            $scope.showInstitutionEmployees = true;
            $scope.showActivities = false;

            $scope.currentPage = 1;
            $scope.perPage = 150;
            $scope.maxSize = 3;

            GlobalService.institutionEmployees({financialYearId:$scope.financialYear,budgetType:$scope.budgetType,adminHierarchyId:$scope.selectedAdminHierarchyId,page:$scope.currentPage,perPage:$scope.perPage},function (data) {
                $scope.institutionEmployees = data.institutionEmployees;
            });


                $scope.selectedInstitutionEmployees = [];

                $scope.checkAll = function () {
                    if ($scope.selectedAll) {
                        $scope.selectedAll = true;
                    } else {
                        $scope.selectedAll = false;
                    }
                    angular.forEach($scope.institutionEmployees, function (item) {
                        item.selected = $scope.selectedAll;
                    });
                };

                $scope.toggleInstitutionEmployees = function (a) {
                  var existingAccount = _.findWhere($scope.selectedInstitutionEmployees, {id: a.id});
                     if (existingAccount !== undefined) {
                        angular.forEach($scope.selectedInstitutionEmployees, function (value, index) {
                            if (value.id === a.id) {
                                $scope.selectedInstitutionEmployees.splice(index, 1);
                            }
                        });
                    } else {
                        $scope.selectedInstitutionEmployees.push(a);
                    }

                    $scope.exportSelectedInstitutionEmployees = function () {
                        GlobalService.exportAllSelectedInstitutionEmployees($scope.selectedInstitutionEmployees,
                            function (data) {
                                $scope.successMessage = data.successMessage;
                                $scope.alertSuccess();
                            }, function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                                $scope.alertSuccess();
                            }
                        );
                    };
                };
        }

        if (segment_id === '9') {
            $scope.showFacilities = false;
            $scope.showSubBudgetClasses = false;
            $scope.showFundTypes = false;
            $scope.showFundSources = false;
            $scope.showProjects = false;
            $scope.showGFSCodes = false;
            $scope.showObjectives = false;
            $scope.showInstitutionEmployees = false;
            $scope.showActivities = true;

            $scope.currentPage = 1;
            $scope.perPage = 150;
            $scope.maxSize = 3;

            GlobalService.activities({financialYearId:$scope.financialYear,budgetType:$scope.budgetType,adminHierarchyId:$scope.selectedAdminHierarchyId,page:$scope.currentPage,perPage:$scope.perPage},function (data) {
                $scope.activities = data.activities;
            });

                //  $scope.pageChanged = function () {
                //     GlobalService.activities({
                //         page: $scope.currentPage,
                //         perPage: $scope.perPage
                //     }, function (data) {
                //         $scope.activities = data.activities;
                //     });
                // };
            


        //     $scope.filterOptions = [
        //         {"id":1,"name":"All"},
        //         {"id":2,"name":"Exported"},
        //         {"id":3,"name":"Not Exported"}
        //     ];
        //   $scope.budgetTypes = [
        //     {"id":1,"name":"APPROVED"},
        //     {"id":2,"name":"REALLOCATION"},
        //     {"id":3,"name":"CARRYOVER"},
        //     {"id":4,"name":"CURRENT"}
        //   ];

            // $scope.loadActivities = function(adminHierarchyId, facility_type,filterOption,financialYearId,budget_type,searchQuery){
            //     GlobalService.exportActivities(
            //       {page: 1, perPage: 10,adminHierarchyId:adminHierarchyId,
            //         facility_type: facility_type,
            //         filterOption:filterOption,
            //         financialYearId:financialYearId,
            //         budget_type:budget_type,
            //         searchQuery:searchQuery},
            //       function (data) {
            //         $scope.activities = data.items;
            //     });

            //     $scope.pageChangedAct = function () {
            //         GlobalService.exportActivities({
            //             page: $scope.currentPage,
            //             perPage: $scope.perPage,
            //             adminHierarchyId:adminHierarchyId,
            //             facility_type: facility_type,
            //             filterOption:filterOption,
            //             financialYearId:financialYearId,
            //             budget_type:budget_type,
            //             searchQuery:searchQuery,
            //         }, function (data) {
            //             $scope.activities = data.items;
            //         });
            //     };


                $scope.selectedActivities = [];

                $scope.checkAll = function () {
                    if ($scope.selectedAll) {
                        $scope.selectedAll = true;
                    } else {
                        $scope.selectedAll = false;
                    }
                    angular.forEach($scope.activities, function (item) {
                        item.selected = $scope.selectedAll;
                    });
                };

                $scope.toggleActivity = function (a) {
                  var existingAccount = _.findWhere($scope.selectedActivities, {id: a.id});
                     if (existingAccount !== undefined) {
                        angular.forEach($scope.selectedActivities, function (value, index) {
                            if (value.id === a.id) {
                                $scope.selectedActivities.splice(index, 1);
                            }
                        });
                    } else {
                        $scope.selectedActivities.push(a);
                    }

                    $scope.exportSelectedActivities = function () {
                        GlobalService.exportAllSelectedActivities($scope.selectedActivities,
                            function (data) {
                                $scope.successMessage = data.successMessage;
                                $scope.alertSuccess();
                            }, function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                                $scope.alertSuccess();
                            }
                        );
                    };
                };
          //  };
        }
    };

    $scope.currentPage = 1;
    $scope.maxSize = 3;

    $scope.segments = {
        1: "Service Providers",
        // 2: "Sub-Budget Classes",
        // 3: "Fund Types",
        // 4: "Fund Sources",
        5: "Projects",
        //6: "GFS Codes",
        7: "Objectives and Targets",
        8: "HR details",
        9: "Activities"
    };
}

SetupSegmentExportController.resolve = {
    UserAdminHierarchy: function (AdminHierarchiesService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            AdminHierarchiesService.getUserAdminHierarchy({}, function (data) {
                deferred.resolve(data);
            });
        }, 900);
        return deferred.promise;
    }
};
