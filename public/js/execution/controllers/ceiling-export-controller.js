function CeilingExportController($scope, $uibModal,CeilingExportService){
  $scope.title = 'CEILING EXPORT AMOUNT';
  $scope.inProgress = true;
  $scope.ceilingAmounts = [];
  $scope.system_codes   = ['ERMS'];
  $scope.selectedItems  = [];
  $scope.perPage = 25;
  $scope.currentPage = 1;
  $scope.maxSize = 3;
  $scope.hideRejected = true;
  $scope.account_types = [{name: 'Bureau Account', value: 'BUREAU'}, {name: 'Service Provider Account', value: 'Service Provider'}];
  //$scope.financial_years = [];
  $scope.resendBudget = false;
  $scope.selectedFinalFinancialYear = null;




  CeilingExportService.getFinancialYears({noLoader: true},function (data) {
      console.log(data.financialYears);
      $scope.financial_years = data.financialYears;
      $scope.selectedFinancialYear = data.financialYears.filter(fy => fy.status == 2)[0].id;
      $scope.selectedFinalFinancialYear = $scope.selectedFinancialYear;

    },
    function (error) {
      console.log("error");
      console.log(error);
    });

  $scope.onchangePeriod = function (financialYearId){
    $scope.selectedFinalFinancialYear = financialYearId;
    $scope.loadFinancialYear();
  }


  $scope.loadFinancialYear = function (){
    CeilingExportService.getFinancialYears({noLoader: true},function (data) {
        $scope.financial_years = data.financialYears;
      },
      function (error) {
        console.log("error");
        console.log(error);
      });
  }


  $scope.filterChanged = function (filter) {
    $scope.selectedFinancialYearId = filter.selectedFinancialYearId;
    $scope.selectedFinancialYearName = filter.selectedFinancialYearName;
    $scope.selectedBudgetType = filter.selectedBudgetType;
    $scope.selectedAdminHierarchyId = filter.selectedAdminHierarchyId;
    $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
    $scope.selectedAdminHierarchyName = filter.selectedAdminHierarchyName;
    $scope.selectedSectionId = filter.selectedSectionId;
    $scope.selectedSectionName = filter.selectedSectionName;
    $scope.searchQuery = filter.searchQuery;
  }

  $scope.loadCeiling = function () {
    CeilingExportService.getCeilingsAmount({financialYear:$scope.selectedFinalFinancialYear,
      adminHierarchyId:$scope.selectedAdminHierarchyId,budgetType:$scope.selectedBudgetType,sectionId:$scope.selectedSectionId,financialSystemCode:$scope.system_codes}, function (data) {
      $scope.ceilingAmounts = data.adminHierarchyCeilings;
    },function (error) {
       console.log(error);
    })
  }

  $scope.exportCeilling = function exportCeilling(ceilling) {
    CeilingExportService.exportCeilingToFinancialSystem({data:ceilling},function (data) {
      $scope.successMessage = data.successMessage;
    },function (error) {
      $scope.errorMessage = error.data.errorMessage;
    })
  }



}
