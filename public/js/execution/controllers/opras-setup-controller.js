function OPRASSetupController($scope, UserAdminHierarchy, FacilityService, $route, $timeout,
           GlobalService,AllFinancialYearService,
           AdminHierarchiesService) {

    $scope.title = "OPRAS_SETUP_EXPORT";

    AllFinancialYearService.query(function (data) {
        $scope.financialYears = data;
    });

    $scope.alertSuccess = function () {
        $scope.showAlertSuccess = true;
        $timeout(function () {
            $scope.showAlertSuccess = false;
            //$route.reload();
        }, 5000);
    };

    $scope.alertError = function () {
        $scope.showAlertError = true;
        $timeout(function () {
            $scope.showAlertError = false;
            //$route.reload();
        }, 5000);
    };
    $scope.segments = {
        1: "Objectives"
    };
    $scope.currentPage = 1;
    $scope.perPage = 10;
$scope.loadData = function (segment_id) {
GlobalService.oprasSetups({page: 1, perPage: 10,segment_id:segment_id,financialYearId:$scope.financialYearId}, function (data) {
   $scope.objectives=data.qryData;
   $scope.targets=data.annualTargets;
   $scope.activities=data.annualActivities;
});
    $scope.pageChangedAct = function (segment_id) {
        GlobalService.oprasSetups({
            page: $scope.currentPage,
            perPage: $scope.perPage,
            segment_id:segment_id,
            financialYearId:$scope.financialYearId,
        }, function (data) {
            $scope.targets=data.annualTargets;
            $scope.activities=data.annualActivities;
        });
    };
};

//Export All Objectives
$scope.exportAllObjectives = function(){
    GlobalService.exportAllObjectives({financialYearId:$scope.financialYearId},function (data) {
        $scope.successMessage = data.successMessage;
        $scope.alertSuccess();
    }, function (error) {
        $scope.errorMessage = error.data.errorMessage;
        $scope.alertError();
    });
};
//Export All Annual Targets
    $scope.exportAllTargets = function(){
        GlobalService.exportAllTargets({financialYearId:$scope.financialYearId},function (data) {
            $scope.successMessage = data.successMessage;
            $scope.alertSuccess();
        }, function (error) {
            $scope.errorMessage = error.data.errorMessage;
            $scope.alertError();
        });
    };
//Export All Activities
    $scope.exportAllActivities = function(){
        GlobalService.exportAllOprasActivities({financialYearId:$scope.financialYearId},function (data) {
            $scope.successMessage = data.successMessage;
            $scope.alertSuccess();
        }, function (error) {
            $scope.errorMessage = error.data.errorMessage;
            $scope.alertError();
        });
    };
//Export selected Objectives
    $scope.selectedObjectives = [];
    $scope.toggleObjectives = function (a) {
        var existingAccount = _.findWhere($scope.selectedObjectives, {code: a.code});
        if (existingAccount !== undefined) {
            angular.forEach($scope.selectedObjectives, function (value, index) {
                if (value.code === a.code) {
                    $scope.selectedObjectives.splice(index, 1);
                }
            });
        } else {
            $scope.selectedObjectives.push(a);
        }
        $scope.exportSelectedObjectives = function () {
            GlobalService.exportAllSelectedObjectives($scope.selectedObjectives,
                function (data) {
                    $scope.successMessage = data.successMessage;
                    $scope.alertSuccess();
                }, function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.alertError();
                }
            );
            $scope.selectedObjectives = [];
        };
    };

//Export selected Annual Targets
    $scope.selectedTargets = [];
    $scope.toggleTargets = function (a) {
        var existingAccount = _.findWhere($scope.selectedTargets, {target_code: a.target_code});
        if (existingAccount !== undefined) {
            angular.forEach($scope.selectedTargets, function (value, index) {
                if (value.target_code === a.target_code) {
                    $scope.selectedTargets.splice(index, 1);
                }
            });
        } else {
            $scope.selectedTargets.push(a);
        }
        $scope.exportSelectedTargets = function () {
            GlobalService.exportAllSelectedTargets($scope.selectedTargets,
                function (data) {
                    $scope.successMessage = data.successMessage;
                    $scope.alertSuccess();
                }, function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.alertError();
                }
            );
            $scope.selectedTargets = [];
        };
    };
//Export selected Activities
    $scope.selectedActivities = [];
    $scope.toggleActivities = function (a) {
        var existingAccount = _.findWhere($scope.selectedActivities, {activity_code: a.activity_code});
        if (existingAccount !== undefined) {
            angular.forEach($scope.selectedActivities, function (value, index) {
                if (value.activity_code === a.activity_code) {
                    $scope.selectedActivities.splice(index, 1);
                }
            });
        } else {
            $scope.selectedActivities.push(a);
        }
        console.log($scope.selectedActivities);
        $scope.exportSelectedActivities = function () {
            GlobalService.exportAllSelectedOprasActivities($scope.selectedActivities,
                function (data) {
                    $scope.successMessage = data.successMessage;
                    $scope.alertSuccess();
                }, function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.alertError();
                }
            );
            $scope.selectedActivities = [];
        };
    };


}
OPRASSetupController.resolve = {
    UserAdminHierarchy: function (AdminHierarchiesService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            AdminHierarchiesService.getUserAdminHierarchy({}, function (data) {
                deferred.resolve(data);
            });
        }, 900);
        return deferred.promise;
    }
};
