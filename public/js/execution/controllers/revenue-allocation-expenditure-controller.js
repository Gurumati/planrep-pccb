function RevenueAllocationExpenditureController($scope, $uibModal,AdminHierarchiesService, AllFinancialYearService, RevenueAllocationExpenditureService, GlobalService, FacilityService) {
  $scope.title = 'Revenue, Allocation & Expenditure';

  $scope.system_codes = ['MUSE','NAVISION'];
  $scope.transaction_types = ['EXPENDITURE', 'REVENUE', 'ALLOCATION'];
  $scope.maxSize = 3;
  $scope.currentPage = 1;
  $scope.perPage = 30;

  $scope.filterChanged = function (filter) {
        $scope.financialYear     = filter.selectedFinancialYearId;
        $scope.financialYearName = filter.selectedFinancialYearName;
        $scope.BudgetType        = filter.selectedBudgetType;
        $scope.selectedAdminHierarchyId  = filter.selectedAdminHierarchyId;
        $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
     };
  //start
  AdminHierarchiesService.getUserAdminHierarchyLevels({noLoader: true}, function (data) {
        $scope.adminLevels = data.adminLevels;
        $scope.adminLevel = $scope.adminLevels[0];
        $scope.sectionLevels = data.sectionLevels;
        $scope.sectionLevel = $scope.sectionLevels[0];
      },
      function (error) {

      });
  AdminHierarchiesService.getUserAdminHierarchy({noLoader: true}, function (data) {
        $scope.userAdminHierarchy = $scope.a1 = data;
        $scope.userAdminHierarchyCopy = $scope.selectedAdminHierarchy = angular.copy($scope.userAdminHierarchy);
        $scope.loadChildren($scope.userAdminHierarchy);
      },
      function (error) {

      });


  $scope.loadChildren = function (a) {
    if (a !== undefined && a !== null) {

      $scope.selectedAdminHierarchy = a;
      $scope.filterChanged(a);
      if (a.hierarchy_position <= 2) {
        getAdminHierarchyChildren(a);
      }
    }
  };

  function getAdminHierarchyChildren(a) {
    AdminHierarchiesService.getAdminHierarchyChildren({adminHierarchyId: a.id}, function (data) {
      a.children = data.adminHierarchies;
    });
  }

  /** load filter */
  $scope.filterChanged = function (filter) {
    $scope.selectedAdminHierarchyId  = filter.id;
    $scope.selectedAdminHierarchyLevelPosition = filter.hierarchy_position;
  };

  /**load sections */
  GlobalService.costCentres({}, function(data){
    $scope.sections = [];
    angular.forEach(data.costCentres, function(value, key){
      if(value.section_level_id == 4){
        $scope.sections.push(value);
      }
    });
  });
  //end
  /** load filter */
  $scope.filterChanged = function (filter) {
    $scope.selectedAdminHierarchyId = filter.selectedAdminHierarchyId;
    $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
    $scope.loadWards($scope.selectedAdminHierarchyId);
  };

  AllFinancialYearService.query(function (response) {
    $scope.financialYears = response;
  });

  GlobalService.facilityTypes(function (data) {
    $scope.facilityTypes = data.facilityTypes;
  }, function (error) {
    console.log(error);
  });

  $scope.loadWards = function (councilId) {
    console.log("Ward")
    console.log(councilId)
    GlobalService.councilWards({council_id: councilId}, function (response) {
      $scope.wards = response.wards;
    }, function (error) {
      console.log(error);
    });
  };

  $scope.loadFacilities = function (wardId, facilityTypeId) {
    FacilityService.facilitiesAllByTypeCouncilUser({
      admin_hierarchy_id: wardId,
      facility_type_id: facilityTypeId
    }, function (data) {
      $scope.facilities = data.facilities;
    }, function (error) {
      console.log(error);
    });
  };

  $scope.resetFacility = function (wardId, facilityTypeId) {
    $scope.loadFacilities(wardId, facilityTypeId);
    $scope.facility = undefined;
    $scope.system_code = undefined;
  };

  /** get expenditure transactions*/
  $scope.loadTransaction = function () {
    if ($scope.facility) {
      $scope.header = {
        admin_hierarchy_id: $scope.a3.id,
        financial_year_id: $scope.financialYear.id,
        transaction_type: $scope.transaction_type,
        system_code: $scope.system_code,
        page: $scope.currentPage,
        perPage: $scope.perPage,
        facility_id: $scope.facility.id
      };
    } else {
      $scope.header = {
        admin_hierarchy_id: $scope.a3.id,
        financial_year_id: $scope.financialYear.id,
        transaction_type: $scope.transaction_type,
        system_code: $scope.system_code,
        page: $scope.currentPage,
        perPage: $scope.perPage
      };
    }
    RevenueAllocationExpenditureService.get($scope.header, function (data) {
      $scope.transactions = data;
    }, function (error) {
      console.log(error);
    });
  };

  $scope.pageChanged = function () {
    $scope.loadTransaction();
  };
}
