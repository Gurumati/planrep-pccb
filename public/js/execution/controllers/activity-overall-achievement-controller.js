function ActivityOverallAchievementController($scope, DataModel, $uibModal,
                                              ActivityPeriodService,
                                              ActivityImplementationService,
                                              AllPeriodService) {


    $scope.budgetClasses = DataModel;

    $scope.title = "ACTIVITY_OVERALL_ACHIEVEMENT";

    $scope.loadFundSources = function (budgetClassId) {

        ActivityImplementationService.costCentreFundSources({budgetClassId: budgetClassId}, function (data) {

            $scope.fundSources = data.fund_sources;

            $scope.loadPeriods = function (fundSourceId) {

                ActivityImplementationService.periods(function (response) {
                    $scope.periods = response.periods;

                    $scope.loadBudgetTypes = function (periodId) {
                        $scope.periodId = periodId;
                        $scope.budgetTypes = [
                            {
                                "id": 1,
                                "name": "APPROVED"
                            },
                            {
                                "id": 2,
                                "name": "SUPPLEMENTARY"
                            },
                            {
                                "id": 3,
                                "name": "CARRYOVER"
                            }
                        ];

                        $scope.filterItems = function (budgetType, searchQuery) {
                            $scope.budgetType = budgetType;

                            $scope.currentPage = 1;
                            $scope.perPage = 10;
                            $scope.maxSize = 3;

                            ActivityPeriodService.paginated({
                                page: $scope.currentPage,
                                perPage: $scope.perPage,
                                budgetClassId: budgetClassId,
                                fundSourceId: fundSourceId,
                                budgetType: budgetType,
                                periodId: periodId,
                                searchQuery: searchQuery
                            }, function (response) {
                                $scope.activities = response;
                            });

                            $scope.pageChanged = function () {
                                ActivityPeriodService.paginated({
                                    page: $scope.currentPage,
                                    perPage: $scope.perPage,
                                    budgetClassId: budgetClassId,
                                    fundSourceId: fundSourceId,
                                    budgetType: budgetType,
                                    periodId: periodId,
                                    searchQuery: searchQuery
                                }, function (response) {
                                    $scope.activities = response;
                                });
                            };

                            $scope.edit = function (updateDataModel, currentPage, perPage, budgetClassId, fundSourceId, budgetType, periodId) {
                                let modalInstance = $uibModal.open({
                                    templateUrl: '/pages/execution/activity_implementation/add-overall-comment.html',
                                    backdrop: false,
                                    controller: function ($scope, $uibModalInstance) {
                                        $scope.updateDataModel = angular.copy(updateDataModel);

                                        $scope.update = function () {
                                            $scope.updateDataModel.activityId = updateDataModel.id;
                                            $scope.updateDataModel.periodId = periodId;
                                            ActivityPeriodService.comment({
                                                    page: currentPage,
                                                    perPage: perPage,
                                                    budgetClassId: budgetClassId,
                                                    fundSourceId: fundSourceId,
                                                    budgetType: budgetType
                                                }, $scope.updateDataModel,
                                                function (data) {
                                                    $uibModalInstance.close(data);
                                                },
                                                function (error) {
                                                    $scope.errorMessage = error.data.errorMessage;
                                                }
                                            );
                                        };
                                        //Function to close modal when cancel button clicked
                                        $scope.close = function () {
                                            $uibModalInstance.dismiss('cancel');
                                        };
                                    }
                                });
                                //Called when modal is close by cancel or to store data
                                modalInstance.result.then(function (data) {
                                        //Service to create new financial year
                                        $scope.successMessage = data.successMessage;
                                        $scope.activities = data.items;
                                        $scope.currentPage = $scope.activities.current_page;
                                    },
                                    function () {
                                        //If modal is closed
                                        console.log('Modal dismissed at: ' + new Date());
                                    });
                            };
                        };
                    };
                }, function (error) {
                    console.log(error);
                });
            };
        });
    };
}

ActivityOverallAchievementController.resolve = {
    DataModel: function (ActivityImplementationService, $timeout, $q) {
        let deferred = $q.defer();
        ActivityImplementationService.sectionBudgetClassesWithActivity(function (data) {
            deferred.resolve(data.items);
        });
        return deferred.promise;
    }
};