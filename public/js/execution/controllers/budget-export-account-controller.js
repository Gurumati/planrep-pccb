function BudgetExportAccountController($scope,
                                       $uibModal,
                                       BudgetExportAccountService,
                                       AdminHierarchiesService,
                                       $animate,
                                       ConfirmDialogService,
                                       AllFinancialYearService,
                                       BudgetReallocationService) {

    $scope.title = "BUDGET_EXPORT_ACCOUNTS";
    $scope.isUserLevel = false;
    $animate.enabled(false);
    $scope.perPage = 25;
    $scope.maxSize = 3;
    $scope.check_all = false;
    $scope.check = 'Select All';
    $scope.currentPage = 1;
    $scope.showRealloc = false; //reset show reallocation

    $scope.system_codes  = ['MUSE'];
    $scope.account_types = [
      {name: 'Bureau Account', value: 'bureau'}
      ];

    /**
     * @description
     * If main filter changed set selected filters and load AdminHierarchyCeilings
     * @param filter {Object} {selectedFinancialYearId,selectedBudgetType,selectedAdminHierarchyId,selectedSectionId}
     */
    $scope.filterChanged = function (filter) {
        $scope.financialYear     = filter.selectedFinancialYearId;
        $scope.financialYearName = filter.selectedFinancialYearName;
        $scope.BudgetType        = filter.selectedBudgetType;
        $scope.selectedAdminHierarchyId  = filter.selectedAdminHierarchyId;
        $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
     };
    /**
     * load budget ready for export
     */
    $scope.loadBudget = function () {
        $scope.loadAccounts($scope.currentPage,$scope.perPage);
    };

    var loadTotals = function (adminHierarchyId, system_code) {
        BudgetExportAccountService.getTotals({adminHierarchyId:adminHierarchyId,
            financialYearId:$scope.financialYear,system_code: system_code,
            budget_type: $scope.BudgetType, account_type: $scope.account_type},
            function (data) {
            $scope.total     = data.total;
            $scope.exported  = data.exported;
            $scope.realloc   = data.realloc;
            $scope.not_delivered = data.not_delivered;
        });
    };

    $scope.loadAccounts = function ( page, perPage) {
        if($scope.financialYear !== undefined && $scope.system_code !== undefined )
        {
            /** empty data***/
          //  $scope.BudgetSummary = [];
           // $scope.budgetExportAccounts = [];
            BudgetExportAccountService.paginated({adminHierarchyId:$scope.selectedAdminHierarchyId,
                financialYear: $scope.financialYear,
                    financial_system_code: $scope.system_code,
                    budget_type: $scope.BudgetType,
                    account_type: $scope.account_type,
                    page: $scope.currentPage,
                    perPage: $scope.perPage
                },
                function (data) {

                    $scope.BudgetSummary = data.summary;
                    $scope.budgetExportAccounts = data.items;
                    $scope.currentPage = $scope.budgetExportAccounts.current_page;
                    $scope.getSectionTotal();
                    $scope.getProjectTotal();
                    $scope.getGrandTotal();
            });
            loadTotals($scope.selectedAdminHierarchyId, $scope.system_code);
        }

    };

    // $scope.budgetChanged = function () {
    //   // $scope.perPage = 100;
    //    $scope.loadAccounts($scope.currentPage,$scope.perPage);
    // };

    $scope.transferInputs = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/budget_export_account/transferInputs.html',
            backdrop: false,
            controller: function ($scope, $http, $uibModalInstance,LevelAdminHierarchyService, AllSystemService, AllBudgetExportTransactionService, BudgetExportAccountService) {

                AllBudgetExportTransactionService.query(function (data) {
                    $scope.budget_export_transactions = data;
                });

                AllSystemService.query(function (data) {
                    $scope.systems = data;
                });

                LevelAdminHierarchyService.query(function (data) {
                    $scope.adminHierarchies = data;
                });

                $scope.loadInputs = function (admin_hierarchy_id,budget_export_transaction_id, system_code,reference_number) {
                    BudgetExportAccountService.loadInputs({
                        admin_hierarchy_id: admin_hierarchy_id,
                        budget_export_transaction_id: budget_export_transaction_id,
                        system_code: system_code,
                        reference_number: reference_number
                    }, function (data) {
                        $uibModalInstance.close(data);
                    });
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.budgetExportAccounts = data.inputs;
                $scope.currentPage = data.current_page;
                budgetClassSummary();
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.getTotalGroupBy = function(values){
        var total = 0;
        if(values !== undefined && values.length > 0){
            for(var i = 0; i < values.length; i++){
                var item = values[i];
                total += parseFloat(item.amount);
            }
        }
        return total;
    };

    $scope.clearInputs = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/budget_export_account/clearInputs.html',
            backdrop: false,
            controller: function ($scope, $http,LevelAdminHierarchyService, $uibModalInstance, AllSystemService, AllBudgetExportTransactionService, BudgetExportAccountService) {

                AllBudgetExportTransactionService.query(function (data) {
                    $scope.budget_export_transactions = data;
                });

                AllSystemService.query(function (data) {
                    $scope.systems = data;
                });

                LevelAdminHierarchyService.query(function (data) {
                    $scope.adminHierarchies = data;
                });

                $scope.clearInputs = function (admin_hierarchy_id,budget_export_transaction_id, system_code) {
                    BudgetExportAccountService.clearInputs({
                        admin_hierarchy_id: admin_hierarchy_id,
                        budget_export_transaction_id: budget_export_transaction_id,
                        system_code: system_code
                    }, function (data) {
                        $uibModalInstance.close(data);
                    });
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.budgetExportAccounts = data.inputs;
                $scope.currentPage = data.current_page;
                budgetClassSummary();
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.reloadInputs = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/budget_export_account/reloadInputs.html',
            backdrop: false,
            controller: function ($scope, $http,LevelAdminHierarchyService, $uibModalInstance, AllSystemService, AllBudgetExportTransactionService, BudgetExportAccountService) {

                AllBudgetExportTransactionService.query(function (data) {
                    $scope.budget_export_transactions = data;
                });

                AllSystemService.query(function (data) {
                    $scope.systems = data;
                });

                LevelAdminHierarchyService.query(function (data) {
                    $scope.adminHierarchies = data;
                });
                $scope.reloadInputs = function (admin_hierarchy_id,budget_export_transaction_id, code,reference_number) {
                    BudgetExportAccountService.reloadInputs({
                        admin_hierarchy_id: admin_hierarchy_id,
                        budget_export_transaction_id: budget_export_transaction_id,
                        code: code,
                        reference_number: reference_number
                    }, function (data) {
                        $uibModalInstance.close(data);
                    });
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.errorMessage  = data.errorMessage;
                $scope.budgetExportAccounts = data.inputs;
                $scope.currentPage = data.current_page;
                budgetClassSummary();
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

  // skip segment and gl exports
  $scope.exportAccounts = function (adminHierarchyId, financialYearId, system_code, budget_type, account_type) {
    let _data = {adminHierarchyId:adminHierarchyId,
      financialYearId : financialYearId,
      comments : $scope.comment,
      financial_system_code : system_code,
      budget_type : budget_type,
      account_type: account_type
    };
    BudgetExportAccountService.exportAccounts(_data,
      function (data) {
          $scope.successMessage = data.successMessage;
      }, function(data){
        console.log(data);
      }
    );
  };

    // $scope.exportAccounts = function (adminHierarchyId, financialYearId, system_code, budget_type, account_type) {
    //     var modalInstance = $uibModal.open({
    //         templateUrl: '/pages/execution/budget_export_account/exportInputs.html',
    //         backdrop: false,
    //         controller: function ($scope, BudgetExportAccountService,$uibModalInstance) {
    //             $scope.active = 1; $scope.status = 0;
    //             $scope.prev = true;  //disable prev
    //             $scope.next = false; //enable next
    //             $scope.sub  = false; //hide submit button
    //             $scope.disable_sub = true;
    //             $scope.comment = '';
    //             $scope.error = 0;
    //             //next step
    //             $scope.nextStep = function () {
    //                 if($scope.active === 1)
    //                 {
    //                     //send Account Segments
    //                     BudgetExportAccountService.sendAccountSegment({adminHierarchyId:adminHierarchyId,financialYearId:financialYearId,
    //                         financial_system_code: system_code, budget_type: budget_type, account_type: account_type},
    //                     function(data){
    //                         //success
    //                         $scope.active = 2;
    //                         $scope.status = 1;
    //                         $scope.prev = false; //enable prev button
    //                      }, function(data){
    //                         console.log(data);
    //                         $scope.error = 1;
    //                         $scope.segment_error = data.error;
    //                     });
    //                 }
    //                 else if($scope.active === 2)
    //                 {
    //                     //check step two
    //                     BudgetExportAccountService.glAccountExist({adminHierarchyId:adminHierarchyId,financialYearId:financialYearId,
    //                         financial_system_code: system_code, budget_type: budget_type, account_type: account_type},
    //                     function (data) {
    //                         console.log(data);
    //                         $scope.error_message = data.errorMessage;
    //                         //$scope.gl_no = $scope.error_message.length;
    //                         if($scope.error_message !== undefined)
    //                         {
    //                             //flag error
    //                             $scope.error = 2;
    //
    //                         }else {
    //                             //success
    //                             $scope.active = 3;
    //                             $scope.status = 2;
    //                             $scope.sub = true;
    //                         }
    //                     });
    //
    //                 }else if($scope.active === 3)
    //                 {
    //                     //check step three
    //                     if($scope.comment !== ''){
    //                         $scope.status = 3;
    //                         $scope.disable_sub = false;
    //                     }else{
    //                         $scope.status = 2;
    //                         $scope.disable_sub = true;
    //                     }
    //                 }
    //             };
    //             //prev steps
    //             $scope.prevStep = function () {
    //                 if($scope.active === 2)
    //                 {
    //                     //check step one - coa segments
    //                     $scope.active = 1;
    //                     $scope.prev = true; //disable prev button
    //
    //                 }else if($scope.active === 3){
    //                     //check step two
    //                     $scope.active = 2;
    //                     $scope.sub = false; //hide submit button
    //
    //                 }
    //             };
    //
    //             //save text file
    //             function saveTextAsFile (data, filename){
    //
    //                 if(!data) {
    //                     console.error('Console.save: No data')
    //                     return;
    //                 }
    //
    //                 if(!filename) filename = 'console.json'
    //
    //                 var blob = new Blob([data], {type: 'text/plain'}),
    //                     e    = document.createEvent('MouseEvents'),
    //                     a    = document.createElement('a')
    //                 // FOR IE:
    //
    //                 if (window.navigator && window.navigator.msSaveOrOpenBlob) {
    //                     window.navigator.msSaveOrOpenBlob(blob, filename);
    //                 }
    //                 else{
    //                     var e = document.createEvent('MouseEvents'),
    //                         a = document.createElement('a');
    //
    //                     a.download = filename;
    //                     a.href = window.URL.createObjectURL(blob);
    //                     a.dataset.downloadurl = ['text/plain', a.download, a.href].join(':');
    //                     e.initEvent('click', true, false, window,
    //                         0, 0, 0, 0, 0, false, false, false, false, 0, null);
    //                     a.dispatchEvent(e);
    //                 }
    //             }
    //
    //             //download error
    //             $scope.downloadError = function(data)
    //             {
    //                var text = '';
    //                angular.forEach(data, function (value, key) {
    //                    text = text + value+"\r\n";
    //                });
    //                saveTextAsFile(text,"error.txt");
    //             };
    //
    //             //send GL Accounts
    //             $scope.sendGLAccount = function () {
    //                 BudgetExportAccountService.sendGLAccount({adminHierarchyId:adminHierarchyId,financialYearId:financialYearId}, function (data) {
    //                     $scope.gl_no = 0;
    //                     $scope.glResponse = data.message;
    //                     setTimeout($scope.close(), 3000);
    //                 });
    //             };
    //
    //             //export budget
    //             $scope.exportAccounts = function () {
    //                 BudgetExportAccountService.exportAccounts({adminHierarchyId:adminHierarchyId,
    //                     financialYearId : financialYearId,
    //                     comments : $scope.comment,
    //                     financial_system_code : system_code,
    //                     budget_type : budget_type,
    //                     account_type: account_type
    //                 },
    //                 function (data) {
    //                     $uibModalInstance.close(data);
    //                 }, function(data){
    //                    console.log(data);
    //                 }
    //                );
    //             };
    //             //budget export feedback
    //             $scope.budgetExportFeedback = function(){
    //                 var url = 'http://'+$location.$$host+':'+$location.$$port+'/execution#!/coa-segment-feedback';
    //                 window.open(url, '_parent');
    //             };
    //
    //             $scope.close = function () {
    //                 $uibModalInstance.dismiss('cancel');
    //             };
    //         }
    //     });
    //     modalInstance.result.then(function (data) {
    //             $scope.successMessage = data.successMessage;
    //             $scope.errorMessage   = data.errorMessage;
    //             $scope.loadAccounts(1,100);
    //             loadTotals($scope.selectedAdminHierarchyId, $scope.system_code);
    //         },
    //         function () {
    //             console.log('Modal dismissed at: ' + new Date());
    //         });
    //
    // };
    $scope.returnSentAccounts = function(system) {
        ConfirmDialogService.showConfirmDialog(
            'Return Sent Accounts '+system,
            'Confirm Return Sent Accounts '+ system)
        .then(function () {
            BudgetExportAccountService.returnSentAccounts({
                        adminHierarchyId: $scope.selectedAdminHierarchyId,
                        financialYearId: $scope.selectedFinancialYearId,
                        budgetType: $scope.selectedBudgetType,
                        financialSystem: system
                    },
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.getBudget();
                    },
                    function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
        );
    };

    $scope.reallocationPageChanged = function () {

        BudgetReallocationService.getApprovedReallocation({page: $scope.currentPage, perPage: 25, admin_hierarchy_id:$scope.selectedAdminHierarchyId,financial_year_id:$scope.financialYear},
            function (data) {
            $scope.budgetReallocationItems = data;
        });
    };

    $scope.exportReallocation = function () {
       $scope.showRealloc = true;
       //get budget reallocation
        $scope.currentPage = 1;
        $scope.reallocationPageChanged();
    };

    $scope.templateUrl = '/pages/execution/budget_export_account/message.html';
    $scope.rejectReallocation = function (item,comments) {

        if(comments !== undefined) {
            var status;
           if(comments === null || comments === ''){
               status = false;
           }else{
               status = true;
           }
           console.log(status);
           BudgetReallocationService.rejectReallocation({'item_id':item,'comments':comments,'is_approved':status});
           //reset status
           var keepGoing = true;
           angular.forEach($scope.budgetReallocationItems.data, function (value, key) {
              if(keepGoing){
                  if(value.id === item){
                      value.rejected = status;
                  }
              }
           });
       }


    };

    //show/hide rejected
    $scope.hideRejected = true;
    $scope.showRejected = function () {
        if($scope.hideRejected) {
            $scope.hideRejected = false;
        }else {
            $scope.hideRejected = true;
        }

    };

    //back to budget
    $scope.backToBudget = function () {
        $scope.showRealloc = false;
    };

    //export selected reallocation
    $scope.exportSelectedReallocation = function (admin_hierarchy_id, financial_year_id) {
        ConfirmDialogService.showConfirmDialog(
            'CONFIRM_EXPORT_BUDGET_REALLOCATION',
            'CONFIRM_BUDGET_EXPORT')
            .then(function () {
                    var data = [];
                    angular.forEach($scope.budgetReallocationItems.data, function (value, key) {
                        if(value.selected){
                            data.push(value);
                        }
                    });
                    console.log(data);
                    BudgetReallocationService.exportReallocation({'admin_hierarchy_id':admin_hierarchy_id,'financial_year_id':financial_year_id,'data':data},
                      function (data) {
                          //success msg
                          $scope.successMessage = data.successMessage;

                      }, function (error) {
                          //error msg
                          $scope.errorMessage = error.data.errorMessage;
                          $scope.failed_items = error.data.items;
                        });

                },
                function () {
                    console.log("NO");
                });
    };

    //get total of each section
    $scope.sub_total = [];
    $scope.getSectionTotal = function () {
        var total = 0;
        angular.forEach($scope.BudgetSummary, function (value, key) {
            $scope.sub_total[value.section_id] = value.amount;
        });
    };

    //get total of each section
    $scope.subTotal = [];
    $scope.getProjectTotal = function () {
        var total = 0;
        angular.forEach($scope.BudgetSummary, function (value, key) {
            $scope.subTotal[value.project_id] = value.amount;
            console.log($scope.subTotal[value.project_id]);
        });
    };

    $scope.getGrandTotal = function () {
        var total = 0;
        angular.forEach($scope.BudgetSummary,function (value) {
            total += parseInt(value.amount);
        });
        $scope.grand_total = total;
    };
}

/**
BudgetExportAccountController.resolve = {

};**/
