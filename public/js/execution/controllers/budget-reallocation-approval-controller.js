function BudgetReallocationApprovalController($scope, BudgetReallocationApproveService, Upload, $uibModal, ConfirmDialogService) {
        $scope.title = "BUDGET_REALLOCATION";
        $scope.currentPage = 1;
        $scope.perPage = 10;
        $scope.maxSize = 3;
        $scope.selectedReallocations = [];

        /** get list of councils */
        BudgetReallocationApproveService.getCouncils({}, function(data){
            $scope.admin_hierarchies = data.admin_hierarchies;
            $scope.decision_level    = data.user.decision_level_id;
            $scope.returnDecisionLevels = data.returnDecisionLevels;
        });

        /** load reallocations */
        $scope.loadReallocations = function(id){
            if(id === undefined){
                return;
            }
            BudgetReallocationApproveService.getReallocations({id: id, page: $scope.currentPage, perPage: $scope.perPage}, function(data){
                $scope.budgetReallocationItems = data.reallocations;
                $scope.total_reallocation_amount = data.totalAmount;
                $scope.total_reallocation  = $scope.budgetReallocationItems.total;
                $scope.document_url = data.document_url;
            });
        };

        $scope.pageChanged = function() {
            $scope.loadReallocations($scope.selectedCouncil);
        };

        $scope.return = function(id) {
            if($scope.returnDecisionLevels === undefined || !$scope.returnDecisionLevels.length){
                return;
            }
            ConfirmDialogService.showConfirmDialog(
                'Confirm return Reallocation',
                'Are you sure you want to return this reallocation to '+$scope.returnDecisionLevels[0].name)
            .then(function () {
                BudgetReallocationApproveService.returnReallocation({
                    id:id,
                    decisionLevelId: $scope.returnDecisionLevels[0].id
                }, function(data) {
                    $scope.successMessage = data.successMessage;
                    $scope.pageChanged();
                 });
                },
                function () {

                });
        };

        /** total reallocation */
        $scope.getTotal = function(items){
            var total = 0, count = 0;
            angular.forEach(items, function(value, key){
                total += parseInt(value.amount);
                count++;
            });

        };
        /** selected items */
        $scope.toggleToApprove = function (reallocation) {
            var existingReallocation = _.findWhere($scope.selectedReallocations, {
                id: reallocation.id
            });
            if (existingReallocation !== undefined) {
                angular.forEach($scope.selectedReallocations, function (value, index) {
                    if (value.id === reallocation.id) {
                        $scope.selectedReallocations.splice(index, 1);
                    }
                });
            } else {
                $scope.selectedReallocations.push(reallocation);
            }
        };

        /** approve reallocation */
        $scope.approve = function () {

            var modalInstance = $uibModal.open({
                templateUrl: '/pages/execution/budget_reallocation_approval/confirm-approve-reallocation.html',
                backdrop: false,
                resolve: {
                    _data: {reallocationsToApprove:$scope.budgetReallocationItems.data},
                    _params : {selectedCouncil : $scope.selectedCouncil, decision_level : $scope.decision_level, action: 'APPROVE REALLOCATION'}
                },
                controller: function ($scope,  _data, _params, $uibModalInstance) {
                    $scope.action = _params.action;
                    $scope.decision_level = _params.decision_level;
                    $scope.save = function (file) {
                        if($scope.decision_level === 5){
                            BudgetReallocationApproveService.approve({comments:$scope.comments, data: _data, file: file},
                                function(data){
                                    data.selectedCouncil = _params.selectedCouncil;
                                    $uibModalInstance.close(data);
                                }, function(error){
                                    $scope.errorMessage = error.errorMessage;
                                });
                        }else {
                            if(file === undefined){
                              $scope.invalidFileInput = true;
                            }
                            $scope.invalidFileInput = false; //reset error msg
                            file.upload = Upload.upload({
                                url: '/api/approveBudgetReallocation', //route to controller in laravel
                                method: 'POST',
                                data: {comments:$scope.comments, data: _data, file: file}
                            }). success(function(data){
                               data.selectedCouncil = _params.selectedCouncil;
                               $uibModalInstance.close(data);
                            }).error(function(error){
                                $scope.errorMessage = error.errorMessage;
                            });
                        }
                    };

                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.selectedReallocations = [];
                $scope.loadReallocations(data.selectedCouncil);
            },
            function () {
                    console.log('Modal dismissed at: ' + new Date());
            });
        };

        /** reject reallocation */
        $scope.reject = function () {

            var modalInstance = $uibModal.open({
                templateUrl: '/pages/execution/budget_reallocation_approval/reject-approve-reallocation.html',
                backdrop: false,
                resolve: {
                    _data: {comments:$scope.comments,reallocationsToApprove:$scope.selectedReallocations, selectedCouncil : $scope.selectedCouncil},
                    action:{action:'REJECT_REALLOCATION'}

                },
                controller: function ($scope, _data, action, BudgetReallocationApproveService, $uibModalInstance) {
                    $scope.action = action.action;
                    $scope.rejectedReallocations = _data.reallocationsToApprove;

                    $scope.save = function () {
                        var data = {reallocationsToApprove: _data.reallocationsToApprove, comments:$scope.comments};
                        BudgetReallocationApproveService.rejectReallocation(data, function (data) {
                            data.selectedCouncil = _data.selectedCouncil;
                            $uibModalInstance.close(data);
                        }, function(error){
                            $uibModalInstance.close(error);
                        });
                    };

                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.selectedReallocations = [];
                $scope.loadReallocations(data.selectedCouncil);
            },
            function () {
                    console.log('Modal dismissed at: ' + new Date());
            });
        };
}
