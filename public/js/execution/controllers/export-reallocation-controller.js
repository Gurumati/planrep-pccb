function ExportReallocationController($scope, $uibModal,
                                      BudgetReallocationService,
                                      BudgetExportAccountService){
    $scope.title          = 'EXPORT_REALLOCATION';
    $scope.transaction_id = [];
    $scope.system_codes   = ['MUSE'];
    $scope.selectedItems  = [];
    $scope.perPage = 25;
    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.hideRejected = true;
    $scope.account_types = [{name: 'Bureau Account', value: 'BUREAU'}, {name: 'Service Provider Account', value: 'Service Provider'}];
    $scope.resendBudget = false;

    /**
     * @description
     * If main filter changed set selected filters and load AdminHierarchyCeilings
     * @param filter {Object} {selectedFinancialYearId,selectedBudgetType,selectedAdminHierarchyId,selectedSectionId}
     */
    // export reallocation accounts
    $scope.filterChanged = function (filter) {
        $scope.financialYear     = filter.selectedFinancialYearId;
        $scope.financialYearName = filter.selectedFinancialYearName;
        $scope.BudgetType        = filter.selectedBudgetType;
        $scope.selectedAdminHierarchyId  = filter.selectedAdminHierarchyId;
        $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
    };

    /**get reallocations */
    $scope.getReallocation = function(){
        BudgetReallocationService.getApprovedReallocation({admin_hierarchy_id:$scope.selectedAdminHierarchyId,
                                                           financial_year_id:$scope.financialYear,
                                                           budget_type: $scope.BudgetType,
                                                           system_code: $scope.system_code,
                                                           perPage: 25},
            function (items) {
              $scope.budgetReallocationItems = items.data;
              //console.log(items.data);
        });
    };

    /**get failed budget */
    $scope.loadFailedBudget = function(){
        BudgetReallocationService.getFailedReallocation({admin_hierarchy_id:$scope.selectedAdminHierarchyId,
                                                            financial_year_id:$scope.financialYear,
                                                            budget_type: $scope.BudgetType,
                                                            system_code: $scope.system_code,
                                                            perPage: 25},
            function (items) {
            $scope.budgetReallocationItems = items.data;
            $scope.resendBudget = true;
        });
    };

  $scope.exportReallocationsToMUSE = function(selectedAdminHierarchyId,financialYear, system_code, BudgetType,budgetReallocationItems){
    var data = [];
    angular.forEach(budgetReallocationItems, function (value, key) {
      if(value.selected){
        data.push(value);
      }
    });
    BudgetReallocationService.exportReallocation({'admin_hierarchy_id':selectedAdminHierarchyId,'financial_year_id':financialYear,'budget_type':BudgetType, financial_system_code: system_code, 'data':data},
      function (data) {
        //success msg
        $scope.successMessage = data.successMessage;
        $uibModalInstance.close(data);
      }, function (error) {
        //error msg
        $scope.errorMessage = error.data.errorMessage;
        $scope.failed_items = error.data.items;
      });
  };

    //export selected reallocation
    $scope.exportSelectedReallocation = function (admin_hierarchy_id, financial_year_id, system_code, budget_type, budgetReallocationItems) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/export_budget_reallocation/exportInputs.html',
            backdrop: false,
            controller: function ($scope, BudgetExportAccountService,$uibModalInstance) {
                $scope.active = 1; $scope.status = 0;
                $scope.prev = true;  //disable prev
                $scope.next = false; //enable next
                $scope.sub  = false; //hide submit button
                $scope.disable_sub = true;
                $scope.comment = '';
                $scope.error = 0;
                //next step
                $scope.nextStep = function () {
                    if($scope.active === 1)
                    {
                        //send Account Segments
                        BudgetExportAccountService.sendAccountSegment({adminHierarchyId:admin_hierarchy_id,financialYearId:financial_year_id,
                            financial_system_code: system_code, budget_type: budget_type, account_type: null},
                        function(data){
                            //success
                            $scope.active = 2;
                            $scope.status = 1;
                            $scope.prev = false; //enable prev button
                         }, function(data){
                            $scope.error = 1;
                            $scope.segment_error = data.error;
                        });
                    }
                    else if($scope.active === 2)
                    {
                        //check step two
                        BudgetExportAccountService.glAccountExist({adminHierarchyId:admin_hierarchy_id,financialYearId:financial_year_id,
                            financial_system_code: system_code, budget_type: budget_type, account_type: null},
                        function (data) {
                            $scope.error_message = data.errorMessage;
                            if($scope.error_message !== undefined)
                            {
                                //flag error
                                $scope.error = 2;
                            }else {
                                //success
                                $scope.active = 3;
                                $scope.status = 2;
                                $scope.sub = true;
                            }
                        });

                    }else if($scope.active === 3)
                    {
                        //check step three
                        if($scope.comment !== ''){
                            $scope.status = 3;
                            $scope.disable_sub = false;
                        }else{
                            $scope.status = 2;
                            $scope.disable_sub = true;
                        }
                    }
                };
                //prev steps
                $scope.prevStep = function () {
                    if($scope.active === 2)
                    {
                        //check step one - coa segments
                        $scope.active = 1;
                        $scope.prev = true; //disable prev button

                    }else if($scope.active === 3){
                        //check step two
                        $scope.active = 2;
                        $scope.sub = false; //hide submit button

                    }
                };

                //save text file
                function saveTextAsFile (data, filename){

                    if(!data) {
                        console.error('Console.save: No data')
                        return;
                    }

                    if(!filename) filename = 'console.json'

                    var blob = new Blob([data], {type: 'text/plain'}),
                        e    = document.createEvent('MouseEvents'),
                        a    = document.createElement('a')
                    // FOR IE:

                    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                        window.navigator.msSaveOrOpenBlob(blob, filename);
                    }
                    else{
                        var e = document.createEvent('MouseEvents'),
                            a = document.createElement('a');

                        a.download = filename;
                        a.href = window.URL.createObjectURL(blob);
                        a.dataset.downloadurl = ['text/plain', a.download, a.href].join(':');
                        e.initEvent('click', true, false, window,
                            0, 0, 0, 0, 0, false, false, false, false, 0, null);
                        a.dispatchEvent(e);
                    }
                }

                //download error
                $scope.downloadError = function(data)
                {
                   var text = '';
                   angular.forEach(data, function (value, key) {
                       text = text + value+"\r\n";
                   });
                   saveTextAsFile(text,"error.txt");
                };

                //send GL Accounts
                $scope.sendGLAccount = function () {
                    BudgetExportAccountService.sendGLAccount({adminHierarchyId:admin_hierarchy_id,financialYearId:financial_year_id}, function (data) {
                        $scope.gl_no = 0;
                        $scope.glResponse = data.message;
                        setTimeout($scope.close(), 3000);
                    });
                };

                //export budget
                $scope.exportAccounts = function () {
                    var data = [];
                    angular.forEach(budgetReallocationItems, function (value, key) {
                        if(value.selected){
                            data.push(value);
                        }
                    });

                BudgetReallocationService.exportReallocation({'admin_hierarchy_id':admin_hierarchy_id,'financial_year_id':financial_year_id, financial_system_code: system_code, 'data':data},
                      function (data) {
                          //success msg
                          $scope.successMessage = data.successMessage;
                          $uibModalInstance.close(data);
                        }, function (error) {
                          //error msg
                          $scope.errorMessage = error.data.errorMessage;
                          $scope.failed_items = error.data.items;
                    });

                };
                //budget export feedback
                $scope.budgetExportFeedback = function(){
                    var url = 'http://'+$location.$$host+':'+$location.$$port+'/execution#!/coa-segment-feedback';
                    window.open(url, '_parent');
                };

                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.errorMessage   = data.errorMessage;
                $scope.getReallocation();
            },
            function () {
                console.log('Modal dismissed at: ' + new Date());
            });
    };
}
