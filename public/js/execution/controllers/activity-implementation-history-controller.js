function ActivityImplementationHistoryController($scope, ActivityImplementationHistoryModel, $route, $httpParamSerializer,
                                                 ActivityImplementationService, $uibModal) {
  $scope.activity_implementations = ActivityImplementationHistoryModel.activity_implementations;
  $scope.title = "TITLE_ACTIVITY_IMPLEMENTATION_HISTORY";
  $scope.currentPage = 1;

  $scope.budget = $route.current.params.budget;
  $scope.expenditure = $route.current.params.expenditure;

  $scope.params = {
    page: $route.current.params.page,
    perPage: $route.current.params.perPage,
    accountId: $route.current.params.accountType,
    facilityId: $route.current.params.facilityId,
    budgetClassId: $route.current.params.budgetClassId,
    fundSourceId: $route.current.params.fundSourceId,
    budgetType: $route.current.params.budgetType,
    periodId: $route.current.params.periodId
  };

  $scope.currentFilter = $httpParamSerializer($scope.params);

  $scope.edit = function (updateDataModel, budget, expenditure, currentPage, perPage) {
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/execution/activity_implementation/edit.html',
      backdrop: false,
      controller: function ($scope, $timeout, $http, $uibModalInstance, ActivityImplementationService, AllPeriodService, StatusService, GlobalService) {
        $scope.reference_file_names = [];
        $scope.total_uploaded = 0;

        ActivityImplementationService.periods(function (response) {
          $scope.all_period = response.periods;
          $scope.updateDataModel = angular.copy(updateDataModel);
          $scope.updateDataModel.latitude = parseInt(updateDataModel.latitude);
          $scope.updateDataModel.longitude = parseInt(updateDataModel.longitude);
          $scope.updateDataModel.period = updateDataModel.period;
          $scope.updateDataModel.start_date = new Date(updateDataModel.start_date);
          $scope.updateDataModel.end_date = new Date(updateDataModel.end_date);
          $scope.updateDataModel.completion_percentage = parseInt(updateDataModel.completion_percentage);
          $scope.updateDataModel.urls = [];
          $scope.percentage = updateDataModel.completion_percentage;
          $scope.activity = updateDataModel.activity;
          if (!('attachments' in updateDataModel)) {
            $scope.updateDataModel.attachments = [];
          }
          $scope.updateDataModel.attachments.forEach(function (element) {
            $scope.updateDataModel.urls.push(element.document_url);
            $scope.reference_file_names.push(element.description);
            $scope.total_uploaded += 1;
          });
        });

        StatusService.query(function (result) {
          $scope.all_status = result;
        });

        $scope.procurable = parseInt(updateDataModel.procurable);

        GlobalService.procurementMethods(function (response) {
          $scope.proMethods = response.items;
        }, function (error) {
          console.log(error);
        });

        $scope.projectOutput = {};
        GlobalService.activityFacilityProjectOut({
          activityId: updateDataModel.activity_id,
          facilityId: updateDataModel.facility_id
        }, function (response) {
          $scope.projectOutput = response.data;
        }, function (error) {
          console.log(error);
        });

        $scope.showPercentageErrorMessage = false;

        $scope.validatePercentage = function (input) {
          if ((input < 0) || (input > 100)) {
            $scope.showPercentageErrorMessage = true;
            $scope.percentageErrorMessage = "Percentage should be between 0 and 100";
          } else {
            $scope.showPercentageErrorMessage = false;
            $scope.percentageErrorMessage = undefined;
          }
        };

        $scope.validPercentage = function (input) {
          return (input < 0) || (input > 100);
        };

        $scope.budget = budget;
        $scope.expenditure = expenditure;
        $scope.percentage = Math.round((($scope.expenditure * 100)) / $scope.budget);

        loadOverallActivityAchievement(updateDataModel.activity_id, updateDataModel.facility_id, updateDataModel.period_id);

        function loadOverallActivityAchievement(activityId, facilityId, periodId) {
          ActivityImplementationService.facilityActivityBudgetExpenditure({
            activity_id: activityId,
            facility_id: facilityId,
            period_id: periodId
          }, function (result) {
            $scope.achievement = result.achievement;
          });
        }

        $scope.uploadFile = function (files) {
          var formData = new FormData();
          formData.append('file', files[0]);
          if ($scope.reference_file_names.indexOf(files[0].name) == -1) {
            $scope.reference_file_names.push(files[0].name);
            uploadUrl = "/json/activityImplementation-document";
            $http.post(uploadUrl, formData, {
              withCredentials: true,
              headers: {
                'Content-Type': undefined
              },
              transformRequest: angular.identity
            }).then(function (response) {
              if (response.data == 0) {
                $scope.reference_file_names.pop();
              } else {
                $scope.updateDataModel.urls.push(response.data);
              }
            }, function (e) {
              $scope.reference_file_names.pop();
            });
          }
        };
        $scope.remove = function (filename) {
          var index = $scope.reference_file_names.indexOf(filename);
          ActivityImplementationService.removeFile({"file_url": $scope.updateDataModel.urls[index]},
            function (response) {
            },
            function (error) {

            });

          $scope.reference_file_names.splice(index, 1);
          $scope.updateDataModel.urls.splice(index, 1);
        };

        $scope.update = function () {
          $scope.updateDataModel.url_names = $scope.reference_file_names;
          $scope.updateDataModel.overall_achievement = $scope.achievement;
          ActivityImplementationService.update($scope.updateDataModel, function (data) {
            $uibModalInstance.close(data);
          }, function (error) {
            $scope.errorMessage = error.data.errorMessage;
            $scope.errors = error.data.errors;
          });
        };

        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    modalInstance.result.then(function (data) {
      $scope.successMessage = data.successMessage;
      $scope.activity_implementations = data.items;
    });
  };
}

ActivityImplementationHistoryController.resolve = {
  ActivityImplementationHistoryModel: function (ActivityImplementationService, $timeout, $q, $routeParams, $route) {
    var deferred = $q.defer();
    var activity_id = $route.current.params.activity_id;
    var facility_id = $route.current.params.facility_id;
    $timeout(function () {
      ActivityImplementationService.byActivity({id: activity_id, facility_id: facility_id}, function (data) {
        deferred.resolve(data);
      });
    }, 900);
    return deferred.promise;
  }
};
