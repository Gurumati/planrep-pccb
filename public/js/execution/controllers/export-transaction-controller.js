function ExportTransactionController($scope, $uibModal, BudgetExportAccountService){
    $scope.title          = 'EXPORT_TRANSACTIONS';
    $scope.transaction_id = [];
    $scope.system_codes   = ['MUSE','NAVISION'];
    $scope.selectedItems  = [];
    $scope.status         = false;

    $scope.loadBudget = function(transaction){
        $scope.selectedItems.length = 0;
        var transaction_id         = transaction.id;
        $scope.default_transaction = transaction;
        BudgetExportAccountService.getBudgetAccounts({transaction_id: transaction_id}, function(data){
            $scope.budget_accounts   = data.transactions;
            $scope.selected_accounts = angular.copy($scope.budget_accounts);
            $scope.show_accounts     = true;
            $scope.total             = $scope.getTotal($scope.budget_accounts);
        });
    }

    /**get total */
    $scope.getTotal = function(data){
        var total = 0;
        data.forEach(item => {
            total += +item.amount;
        });
        console.log(total);
        return total;
    }
    /**load transactions */
    $scope.loadTransactions = function(){
        $scope.show_accounts = false;
        BudgetExportAccountService.getTransactions({admin_hierarchy_id: $scope.selectedAdminHierarchyId,
            budget_type: $scope.BudgetType,financial_year_id: $scope.financialYear, system_code: $scope.system_code, status : $scope.status}, function(data){
            $scope.transactions   = data.transactions;
        });
    }

    /** filter */
    $scope.filterChanged = function (filter) {
        $scope.financialYear     = filter.selectedFinancialYearId;
        $scope.financialYearName = filter.selectedFinancialYearName;
        $scope.BudgetType        = filter.selectedBudgetType;
        $scope.selectedAdminHierarchyId  = filter.selectedAdminHierarchyId;
        $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
    };

    $scope.exportAccounts = function (transaction_id, system_code, selectedItems) {
                //export budget
                //$scope.exportAccounts = function () {
                    var items = selectedItems.join(',');
                    BudgetExportAccountService.exportTransaction({transaction_id:transaction_id, system_code : system_code, selectedItems: items},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                    }, function(data){
                       console.log(data);
                    }
                   );
                //};

    };

    /** liste selected items */
    $scope.addItem = function(item, status){
        if(status){
            $scope.selectedItems.push(item);
        }else {
            var index = $scope.selectedItems.indexOf(item);
            if (index > -1) {
               $scope.selectedItems.splice(index, 1);
            }
        }
    }
    /** filter accounts */
    $scope.filterAccounts = function(facility_code){
        $scope.selected_accounts.length = 0;
        angular.forEach($scope.budget_accounts, function(value, key){
            if(value.chart_of_accounts.includes(facility_code)){
                $scope.selected_accounts.push(value);
            }
        });
    }
}
