function ReceivedFundController($scope, ReceivedFundModel, $uibModal, ConfirmDialogService, DeleteReceivedFundService,
                                PaginatedReceivedFundService) {

    $scope.receivedFunds = ReceivedFundModel;
    $scope.title = "RECEIVED_FUNDS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedReceivedFundService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.receivedFunds = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/received_fund/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllDataSourceService,AllImportMethodService, CreateReceivedFundService) {

                AllDataSourceService.query(function (data) {
                    $scope.dataSources = data;
                });

                AllImportMethodService.query(function (data) {
                    $scope.importMethods = data;
                });

                $scope.receivedFundToCreate = {};

                $scope.store = function () {
                    if ($scope.createReceivedFundForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateReceivedFundService.store({perPage: $scope.perPage}, $scope.receivedFundToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.receivedFunds = data.receivedFunds;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (receivedFundToEdit, currentPage, perPage) {
        //console.log(receivedFundToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/received_fund/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllDataSourceService,AllImportMethodService, UpdateReceivedFundService) {
                AllDataSourceService.query(function (data) {
                    $scope.dataSources = data;
                    $scope.receivedFundToEdit = angular.copy(receivedFundToEdit);
                });

                AllImportMethodService.query(function (data) {
                    $scope.importMethods = data;
                });

                $scope.update = function () {
                    UpdateReceivedFundService.update({page: currentPage, perPage: perPage}, $scope.receivedFundToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.receivedFunds = data.receivedFunds;
                $scope.currentPage = $scope.receivedFunds.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteReceivedFundService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.receivedFunds = data.receivedFunds;
                        $scope.currentPage = $scope.receivedFunds.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/received_fund/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedReceivedFundService,
                                  RestoreReceivedFundService, EmptyReceivedFundTrashService, PermanentDeleteReceivedFundService) {
                TrashedReceivedFundService.query(function (data) {
                    $scope.trashedReceivedFunds = data;
                });
                $scope.restoreReceivedFund = function (id) {
                    RestoreReceivedFundService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedReceivedFunds = data.trashedReceivedFunds;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteReceivedFundService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedReceivedFunds = data.trashedReceivedFunds;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyReceivedFundTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedReceivedFunds = data.trashedReceivedFunds;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.receivedFunds = data.receivedFunds;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

ReceivedFundController.resolve = {
    ReceivedFundModel: function (PaginatedReceivedFundService, $q) {
        var deferred = $q.defer();
        PaginatedReceivedFundService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};