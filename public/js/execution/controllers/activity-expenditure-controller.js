function ActivityExpenditureController($scope, $uibModal,AdminHierarchiesService,  ActivityExpenditureService, GlobalService,FinancialYearModel){
    $scope.title = 'Expenditures Mismatch';

    $scope.system_codes  = ['FFARS','EPICOR'];
    $scope.transaction_types = ['EXPENDITURE', 'REVENUE', 'ALLOCATION'];
    $scope.maxSize = 3;
    $scope.currentPage = 1;
    $scope.perPage = 10;
   $scope.financialYears = FinancialYearModel;
    AdminHierarchiesService.getUserAdminHierarchyLevels({noLoader: true}, function (data) {
            $scope.adminLevels = data.adminLevels;
            $scope.adminLevel = $scope.adminLevels[0];
            $scope.sectionLevels = data.sectionLevels;
            $scope.sectionLevel = $scope.sectionLevels[0];
        },
        function (error) {

        });
    AdminHierarchiesService.getUserAdminHierarchy({noLoader: true}, function (data) {
            $scope.userAdminHierarchy = $scope.a1 = data;
            $scope.userAdminHierarchyCopy = $scope.selectedAdminHierarchy = angular.copy($scope.userAdminHierarchy);
            $scope.loadChildren($scope.userAdminHierarchy);
        },
        function (error) {

        });

    $scope.loadChildren = function (a) {
        if (a !== undefined && a !== null) {
            $scope.selectedAdminHierarchy = a;
            $scope.filterChanged(a);
            if (a.hierarchy_position <= 2) {
                getAdminHierarchyChildren(a);
            }
        }
    };

    function getAdminHierarchyChildren(a) {
        AdminHierarchiesService.getAdminHierarchyChildren({adminHierarchyId: a.id}, function (data) {
            a.children = data.adminHierarchies;
        });
    }

    /** load filter */
    $scope.filterChanged = function (filter) {
        $scope.selectedAdminHierarchyId  = filter.id;
        $scope.selectedAdminHierarchyLevelPosition = filter.hierarchy_position;
    };

    /**load sections */
    GlobalService.costCentres({}, function(data){
       $scope.sections = [];
       angular.forEach(data.costCentres, function(value, key){
         if(value.section_level_id == 4){
           $scope.sections.push(value);
         }
       });
    });

    /** get expenditure transactions*/
    $scope.loadTransaction = function(perPage = 10){
        ActivityExpenditureService.get({admin_hierarchy_id:  $scope.selectedAdminHierarchyId,
                                        transaction_type: $scope.transaction_type,
                                        system_code: $scope.system_code,
                                        cost_centre: $scope.cost_centre,
                                        page: $scope.currentPage,
                                        financial_year_id:$scope.financialYearId,
                                        perPage: perPage},
            function(data){
            $scope.transactions = data;
        });
    };

    $scope.pageChanged = function(){
        $scope.loadTransaction($scope.perPage);
    };
    /** find match */
    $scope.findMatch = function(item, currentPage, perPage){
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/activity_expenditure/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,ActivityExpenditureService) {
              $scope.transaction = angular.copy(item);
              //get suggestions
              ActivityExpenditureService.matchList({id: item.id,
                                                    transaction_type: item.transaction_type
                                                   }, function(data){
                    $scope.match_list = data.items;
              }, function(error){
                  $scope.errorMessage = error.data.items;
              });
              //match item
              $scope.Match = function(item, transaction){
                ActivityExpenditureService.match({id: item.id,
                                                  transaction_type: transaction.transaction_type,
                                                  transaction_id : transaction.id
                                }, function(data){
                                    console.log(data);
                                    $uibModalInstance.close(data);
                                }, function(error){
                                    $scope.errorMessage = error.errorMessage;
                });
              };
              //close model
              $scope.close = function () {
                $uibModalInstance.dismiss('cancel');
              };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
            $scope.successMessage = data.successMessage;
            $scope.loadTransaction();
        },
        function () {
            //If modal is closed
            console.log('Modal dismissed at: ' + new Date());
        });
    };

    /**auto matching */
    $scope.autoMatchTransaction = function(items, transaction_type){
        var data= [];
        angular.forEach(items, function(value){
           data.push(value.id);
        });
        console.log(data);
        ActivityExpenditureService.automatch({
                'transaction_type': transaction_type,
                'items' : data
            }, function(data){
               $scope.successMessage = data.successMessage;
               $scope.loadTransaction();
            }, function(error){
               console.log(error);
               $scope.errorMessage = error.errorMessage;
        });
    };

}

ActivityExpenditureController.resolve = {
    FinancialYearModel: function (AllFinancialYearService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            AllFinancialYearService.query(function (data) {
                deferred.resolve(data);
            }, function (response) {
            });
        }, 900);
        return deferred.promise;
    }
};
