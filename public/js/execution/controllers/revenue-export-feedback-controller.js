function RevenueExportFeedBackController($scope,$uibModal,RevenueExportFeedback) {
  $scope.title = "EXPORTED_REVENUE_BUDGET";
  $scope.currentPage = 1;
  $scope.dateFormat = 'yyyy-MM-DD';
  $scope.maxSize = 3;
  $scope.perPage = 25;
  $scope.transaction_id = [];
  $scope.selectedItems  = [];
  $scope.system_codes = ['OTRMIS','MUSE','NAVISION'];

  $scope.filterChanged = function (filter) {
    $scope.financialYear     = filter.selectedFinancialYearId;
    $scope.financialYearName = filter.selectedFinancialYearName;
    $scope.selectedAdminHierarchyId  = filter.selectedAdminHierarchyId;
    $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
    $scope.selectedBudgetType = filter.selectedBudgetType;
  };

  $scope.loadRevenueFeedBacks = function (admin_hierarchy_id, financial_year_id,financialSystem) {
    $scope.accountsLoaded = false;
    $scope.selectedItems.length = 0;
    if(admin_hierarchy_id !== undefined && financial_year_id !== undefined && financialSystem !== undefined) {
      RevenueExportFeedback.getRevenueFeedback({
          adminHierarchyId: admin_hierarchy_id,
          financialYearId: financial_year_id,
          financialSystem: financialSystem,
          currentPage: $scope.currentPage,
          perPage: $scope.perPage,
          budgetType: $scope.selectedBudgetType
        },
        function (data) {
          console.log(data.feedback);
          $scope.accountsLoaded = true;
          $scope.revenueFeedbacks = data.feedback;
          $scope.transaction_id = $scope.revenueFeedbacks[0].transaction_id;
        });
    }
  };

        $scope.exportAccounts = function (transaction_id, system_code, selectedItems) {
            //export budget
            //$scope.exportAccounts = function () {
                var items = selectedItems.join(',');
                RevenueExportFeedback.exportTransaction({transaction_id:transaction_id, system_code : system_code, selectedItems: items},
                function (data) {
                    $scope.successMessage = data.successMessage;
                }, function(data){
                  console.log(data);
                }
              );
            //};

        };

      /** liste selected items */
      $scope.addItem = function(item, status){
        if(status){
            $scope.selectedItems.push(item);
        }else {
            var index = $scope.selectedItems.indexOf(item);
            if (index > -1) {
               $scope.selectedItems.splice(index, 1);
            }
        }
    }

        /** filter accounts */
        $scope.filterAccounts = function(facility_code){
          $scope.revenueFeedbacks.length = 0;
          angular.forEach($scope.feedback = data.feedback, function(value, key){
              if(value.chart_of_accounts.includes(facility_code)){
                  $scope.revenueFeedbacks.push(value);
              }
          });
      }
}
