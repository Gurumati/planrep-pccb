function DataSourceController($scope, DataSourceModel, $uibModal, ConfirmDialogService, DeleteDataSourceService,
                                PaginatedDataSourceService) {

    $scope.dataSources = DataSourceModel;
    $scope.title = "DATA_SOURCE";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedDataSourceService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.dataSources = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/data_source/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateDataSourceService) {
                
                $scope.dataSourceToCreate = {};

                $scope.store = function () {
                    if ($scope.createDataSourceForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateDataSourceService.store({perPage: $scope.perPage}, $scope.dataSourceToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.dataSources = data.dataSources;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (dataSourceToEdit, currentPage, perPage) {
        //console.log(dataSourceToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/data_source/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateDataSourceService) {
                $scope.dataSourceToEdit = angular.copy(dataSourceToEdit);
                $scope.update = function () {
                    UpdateDataSourceService.update({page: currentPage, perPage: perPage}, $scope.dataSourceToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.dataSources = data.dataSources;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteDataSourceService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.dataSources = data.dataSources;
                        $scope.currentPage = $scope.dataSources.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/execution/data_source/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedDataSourceService,
                                  RestoreDataSourceService, EmptyDataSourceTrashService, PermanentDeleteDataSourceService) {
                TrashedDataSourceService.query(function (data) {
                    $scope.trashedDataSources = data;
                });
                $scope.restoreDataSource = function (id) {
                    RestoreDataSourceService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedDataSources = data.trashedDataSources;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteDataSourceService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedDataSources = data.trashedDataSources;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyDataSourceTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedDataSources = data.trashedDataSources;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.dataSources = data.dataSources;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

DataSourceController.resolve = {
    DataSourceModel: function (PaginatedDataSourceService, $q) {
        var deferred = $q.defer();
        PaginatedDataSourceService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};