function CoaSegmentFeedBackController($scope,
                                      CoaSegmentFeedBackService,
                                      RabbitMqFeedbackService,
                                      BudgetExportResponseService,
                                      FFARSBudgetExportResponseService,
                                      ConfirmDialogService,
                                      AdminHierarchiesService,
                                      AllFinancialYearService,
                                      glAccountFeedBackService) {

    $scope.title = "COA_SEGMENT_FEEDBACK";
    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.perPage = 15;
    $scope.coa_segments_active = 'active';
    $scope.coa_segment = true;
    $scope.gl_account_active = '';
    $scope.gl_account = false;
    $scope.budget_export_response_active = '';
    $scope.budget_export_responses = false;
    $scope.budget_export_to_ffars_feedback_active = '';
    $scope.budget_export_to_ffars_feedback = false;

    AllFinancialYearService.query({}, function(data){
        $scope.financialYears = data;
    });

    $scope.showFeedback = function(financial_year_id,system_code, admin_hierarchy_id){
        /**
         * Get the feedback of the COA segments
         */
        CoaSegmentFeedBackService.getCoaSegments({
                page: $scope.currentPage,
                per_page: $scope.perPage,
                financial_year_id:financial_year_id,
                system_code:system_code,
                admin_hierarchy_id:admin_hierarchy_id},
            function (data) {
                $scope.coa_segments = data;
            });

        /**
         * Get GL feedback messages
         */
        glAccountFeedBackService.getGLAccountIssues({
                page: $scope.currentPage,
                per_page: $scope.perPage,
                financial_year_id:financial_year_id,
                admin_hierarchy_id:admin_hierarchy_id,
                system_code:system_code
            },
            function (data) {
                $scope.gl_account_issues = data.items;
            }
        );

        /**Budget Export Responses*/
        BudgetExportResponseService.paginated({
            page: $scope.currentPage,
            per_page: $scope.per_page,
            financial_year_id:financial_year_id,
            admin_hierarchy_id:admin_hierarchy_id,
            system_code:system_code
        }, function (data) {
            $scope.items = data.items;
        });

    };

    $scope.loadChildren = function (a, adminLevel) {
        var ad1 = parseInt(a.admin_hierarchy_level_id);
        var ad2 = parseInt(adminLevel.id);
        if (ad1 === ad2) {
            $scope.adminHierarchyId = a.id;
            $scope.showSubmit = true;
        }else{
            if (a.children === undefined) {
                a.children = [];
            }
            AdminHierarchiesService.getAdminHierarchyChildren({adminHierarchyId: a.id}, function (data) {
                a.children = data.adminHierarchies;
            });
        }
    };

    $scope.resetHierarchy = function () {
        $scope.userAdminHierarchy = angular.copy($scope.userAdminHierarchyCopy);
    };

    AdminHierarchiesService.getBudgetingChildLevels(function (data) {
        $scope.adminLevels = data.adminLevels;
        if( $scope.adminLevels.length === 1 ) {
            $scope.adminLevel = $scope.adminLevels[0];
        }
        AdminHierarchiesService.getUserAdminHierarchy(function (data) {
            $scope.userAdminHierarchy = data;
            $scope.userAdminHierarchyCopy = angular.copy($scope.userAdminHierarchy);

        });
    });

    $scope.toggle_active = function (active) {
        if (active === 1) {
            $scope.coa_segments_active = 'active';
            $scope.coa_segment = true;

            $scope.gl_account_active = '';
            $scope.gl_account = false;

            $scope.budget_export_response_active = '';
            $scope.budget_export_responses = false;

            $scope.budget_export_to_ffars_feedback_active = '';
            $scope.budget_export_to_ffars_feedback = false;
        } else if (active === 2) {
            $scope.coa_segments_active = '';
            $scope.coa_segment = false;

            $scope.gl_account_active = 'active';
            $scope.gl_account = true;

            $scope.budget_export_response_active = '';
            $scope.budget_export_responses = false;

            $scope.budget_export_to_ffars_feedback_active = '';
            $scope.budget_export_to_ffars_feedback = false;
        }else if (active === 4){
            $scope.coa_segments_active = '';
            $scope.coa_segment = false;

            $scope.gl_account_active = '';
            $scope.gl_account = false;

            $scope.budget_export_response_active = '';
            $scope.budget_export_responses = false;

            $scope.budget_export_to_ffars_feedback_active = '';
            $scope.budget_export_to_ffars_feedback = true;

        } else {
            $scope.coa_segments_active = '';
            $scope.coa_segment = false;

            $scope.gl_account_active = '';
            $scope.gl_account = false;

            $scope.budget_export_response_active = 'active';
            $scope.budget_export_responses = true;

            $scope.budget_export_to_ffars_feedback_active = '';
            $scope.budget_export_to_ffars_feedback = false;
        }
    };



    $scope.pageChangedGL = function () {
        glAccountFeedBackService.getGLAccountIssues({page: $scope.currentPage, per_page: $scope.perPage},
            function (data) {
                $scope.gl_account_issues = data.items;
            }
        );
    };

    $scope.pageChangedCOASEG = function () {
        CoaSegmentFeedBackService.getCoaSegments({page: $scope.currentPage, per_page: $scope.perPage}, function (data) {
            $scope.coa_segments = data;
        });
    };

    /**budget export to ffars response */
    FFARSBudgetExportResponseService.ffarsBudget({page: $scope.currentPage, per_page: $scope.perPage}, function (data) {
        $scope.failed_budgets = data.failed_budgets;
    });
    /**resend budget */
    $scope.refreshFFARSBudgetFeedback = function(){
        FFARSBudgetExportResponseService.resendFFARSBudget({},
            function (data) {
                $scope.successMessage = data.successMessage;
            },
            function (error) {
                console.log(error);
            });
    };

    $scope.pageChangedFF = function () {
        FFARSBudgetExportResponseService.ffarsBudget({page: $scope.currentPage, per_page: $scope.perPage}, function (data) {
        $scope.failed_budgets = data.failed_budgets;
       });
    };

    $scope.pageChangedBR = function () {
        BudgetExportResponseService.paginated({page: $scope.currentPage, per_page: $scope.perPage}, function (data) {
            $scope.items = data.items;
        });
    };

    $scope.getJsonResponse = function (responseString) {
        try {
            return JSON.parse(responseString);
        }
        catch (e) {
            console.log(e);
        }
    };

    /*End Budget Export Responses*/

    $scope.clear_coa_segment_issue = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'Clear Segment!',
            'Are sure you want to clear this entry?')
            .then(function () {
                    CoaSegmentFeedBackService.clearCOASegments({id: id}, function (data) {
                        $scope.coa_segments = data.coa_segments;
                        $scope.successMessage = data.successMessage;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    });
                },
                function () {
                    console.log("NO");
                });
    };

    $scope.clear_gl_account_issue = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'Clear General Ledger Issue!',
            'Are sure you want to clear this entry?')
            .then(function () {
                    glAccountFeedBackService.clearGLAccountIssues({id: id, per_page: $scope.perPage},
                        function (data) {
                            $scope.gl_account_issues = data.items;
                        }, function (error) {
                            console.log(error);
                        });
                },
                function () {
                    console.log("NO");
                });
    };

    $scope.clearBudgetExportIssue = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'Clear Budget Export Issue!',
            'Are sure you want to clear this entry?')
            .then(function () {
                    BudgetExportResponseService.clearExportIssue({id: id, per_page: $scope.perPage},
                        function (data) {
                            $scope.items = data.items;
                            $scope.successMessage = data.successMessage;
                        },
                        function (error) {
                            console.log(error);
                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    };

    $scope.regenerateSegments = function () {
        CoaSegmentFeedBackService.generate_coa_segments({}),
            function (data) {
                console.log(data);
            }, function (error) {
            console.log(error);
        }
    };

    $scope.regenerateSegmentsFeedback = function () {
        RabbitMqFeedbackService.getCoaSegmentFeedBack(function (data) {
                $scope.successMessage = "Segments Re-generated Successfully!";
            }, function (error) {
                console.log(error);
            }
        );
    };

    $scope.regenerateGlAccountsFeedback = function () {
        RabbitMqFeedbackService.getGLAccountsQueueFeedBack(function (data) {
                $scope.successMessage = "GL Accounts Synced Successfully!";
            }, function (error) {
                console.log(error);
            }
        );
    };

    $scope.regenerateBudgetExportFeedback = function () {
        RabbitMqFeedbackService.getBudgetQueueFeedBack(function (data) {
                $scope.successMessage = "Budget Exported Response Re-generated Successfully!";
            }, function (error) {
                console.log(error);
            }
        );
    };

    $scope.resend = function () {
        CoaSegmentFeedBackService.resend_coa_segments({}),
            function (data) {
                console.log(data);
            }, function (error) {
            console.log(error);
        }
    };


}

