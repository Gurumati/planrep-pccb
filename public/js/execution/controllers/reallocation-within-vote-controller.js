(function () {
  'use strict';
  angular.module('execution-module').controller('ReallocationWithinVoteController', ReallocationWithinVoteController);
  ReallocationWithinVoteController.$inject = ['$scope','$uibModal','ConfirmDialogService','ReallocationWithinVoteService','AllFinancialYearService','openReallocation','BudgetReallocationService'];
  function ReallocationWithinVoteController($scope,$uibModal,ConfirmDialogService,ReallocationWithinVoteService,AllFinancialYearService,openReallocation,BudgetReallocationService) {
    $scope.title = "Reallocation Within Votes";
    $scope.budgetTypes = ['APPROVED','CARRYOVER'];
    $scope.amountAllocated = 0.00;
    $scope.allowAllocation = true;
    $scope.showActivities = 0;
    $scope.budgetToAllocate = [];
    $scope.allReallocation = [];

    AllFinancialYearService.query({},function (data) {
     $scope.financialYears = data;
    });

    ReallocationWithinVoteService.getReallocation({},function (data) {
     $scope.allReallocation = data.reallocations;
    });

    ReallocationWithinVoteService.getVotes({},function (data) {
      $scope.votes = data.votes;
     });

    $scope.voteChanged = function () {
        ReallocationWithinVoteService.getPiscs({voteId:$scope.selectedVote.id}, function (data) {
        $scope.adminHierarchies = data.adminHierarchies;
         });
    };
    $scope.piscChanged = function (form) {
        ReallocationWithinVoteService.getFundSources({adminId:$scope.selectedFromPisc.id,financialYearId:$scope.financialYearId},function (data) {
        $scope.fundSources = data.fundSources;
         });
         $scope.activities = [];
         $scope.fileUploadCheck(form);
    };
    $scope.financialYearChanged = function(){
      ReallocationWithinVoteService.getReallocation({financialYear:$scope.financialYearId},function (data) {
        $scope.allReallocation = data.reallocations;
        console.log($scope.allReallocation);
       });
    }

    $scope.getSubBudgetClass = function (){
      ReallocationWithinVoteService.getSubBudgetClasses({fundSourceId:$scope.selectedFundSource.id,financialYearId:$scope.financialYearId},function (data) {
        $scope.subBudgetClasses = data.subBudgetClasses;
        });
    }
    $scope.getActivities = function () {
      let items = {
        adminId:$scope.selectedFromPisc.id,
        financialYearId:$scope.financialYearId,
        budgetType:$scope.budgetType,
        fundSourceId:$scope.selectedFundSource.id,
        subBudgetClass:$scope.selectedSubBudgetClass.id,
        perPage:25
      };
        console.log(items);
        ReallocationWithinVoteService.getActivities(items,function (data) {
        $scope.activities = data.activities.data;
         });
    };

    $scope.getSelectedItem = function (activities,status) {
      $scope.amountAllocated
      if(status){
        $scope.amountAllocated = $scope.amountAllocated + parseFloat(activities.amount);
        $scope.budgetToAllocate.push(activities);
      }else{
        $scope.isChecked = false;
        $scope.amountAllocated = $scope.amountAllocated - parseFloat(activities.amount);
        $.each($scope.budgetToAllocate, function(i){
          if($scope.budgetToAllocate[i] === activities) {
            $scope.budgetToAllocate.splice(i,1);
            return false;
          }
       });
      }

      if($scope.amountAllocated > 0){
        $scope.allowAllocation = false;
      }else{
        $scope.allowAllocation = true;
      }
    }
    $scope.SelectAllGLAccount = function (budgetExportAccount,isChecked){
      angular.forEach(budgetExportAccount, function(value, key) {
        var target = angular.element(document).find('#'+ value.chart_of_accounts);
        var checkbox = angular.element(document.getElementById(value.chart_of_accounts));
        if(checkbox[0].ariaChecked === 'false'){
          $scope.getSelectedItem(value,isChecked);
        }
        if(isChecked === false){
          $scope.amountAllocated = 0.00;
          $scope.budgetToAllocate = [];
        }
      });
      $scope.checked = isChecked;
    }

    $scope.allocatesGlAccount = function (reallocationForm){
      var selecteFromPisc = reallocationForm.selectedFromPisc;
      var selectedToPisc = reallocationForm.topisc;
      var fundSource = reallocationForm.fundSource;
      $scope.errorMessage = [];
      $scope.successMessage = [];
      $scope.hasError = false;
      $scope.success = false;

      if(selecteFromPisc.$viewValue == undefined ||  selectedToPisc.$viewValue === undefined){
        $scope.errorMessage.push('Please Select  PISC From and PISC to Reallocate');
      }else{
        var allocation_data = {
          budgetType:reallocationForm.budgetType.$modelValue,
          piscFrom:reallocationForm.selectedFromPisc.$modelValue.id ,
          piscTo:reallocationForm.topisc.$modelValue.id,
          budgetReallocationId:$scope.realocations[0].id,
          financialYear:reallocationForm.financialYearId.$viewValue,
          budgetClass:$scope.selectedSubBudgetClass.id,
          fundSource:$scope.selectedFundSource.id,
          budget:$scope.budgetToAllocate
        };
        BudgetReallocationService.storeReallocationitemsAmount({
          budget: allocation_data
        }, function (data) {
          $scope.successMessage.push(data.successMessage);
          $scope.budgetToAllocate = [];
          $scope.getActivities();
        },
        function (error) {
          $scope.errorMessage.push(error.data.errorMessage);
        });
      }
    }

    $scope.closeAlert = function (index) {
      $scope.errorMessage.splice(index, 1);
    }

    $scope.openReallocation = function(data) {
      var modalInstance = $uibModal.open({
        templateUrl: '/pages/execution/reallocation-within-vote/open-reallocation.html',
        backdrop: false,
        size: "md",
        controller: function ($scope, $uibModalInstance) {
          $scope.InputFile = [];
          $scope.errorMessageMain = '';
          $scope.canCreate = true;

          $scope.inProgress = false;
          var reallocations = this;

          $scope.storeReallocation = function(form) {
            $scope.errorMessageMain = '';

            if(form.ref.$modelValue === undefined || form.comments.$modelValue === undefined || $scope.InputFile.length ===0){
              $scope.errorMessageMain ='Please Fill All Field and atleast one attachement';
            }else{
              var inputData = {
                              ref_no:form.ref.$modelValue,
                              comments:form.comments.$modelValue,
                              piscFrom:data.selectedFromPisc.$modelValue.id ,
                              piscTo:data.topisc.$modelValue.id,
                              financialYear:data.financialYearId.$viewValue,
                              fundSource:data.fundSource.$viewValue.id
                            };
              BudgetReallocationService.savePiscReallocation({
                inputData: inputData,
                attachement:$scope.InputFile
              }, function (data) {

              },
              function (error) {
                $scope.errorMessageMain = error.data.errorMessage;
              });
              }
          }

          $scope.getFile =function(file){
            var file =  angular.element(document.getElementById("document"))[0].files[0];
            var reader = new FileReader();
            var base64Pdf = null;
            var data = null;

            if(file !== undefined){
              reader.readAsDataURL(file); // read file as data url
              reader.onload = (event) => { // called once readAsDataURL is completed
                base64Pdf = event.target.result.substr(event.target.result.indexOf(',') + 1);
                data ={file:file.name,pdf:base64Pdf,desc:null};
                var exist = $scope.containsObject(data,$scope.InputFile);
                if(!exist){
                  $scope.InputFile.push(data);
                }else{
                  $scope.errorMessageMain ='Attachement Exist';
                }
              }
            }

          }

          $scope.removeFile = function(file){
            $.each($scope.InputFile, function(i){
              if($scope.InputFile[i] === file) {
                $scope.InputFile.splice(i,1);
                return false;
              }
           });
          }

          $scope.close = function () {
            $uibModalInstance.dismiss('cancel');
          };

          $scope.containsObject =function(obj, list) {
            var i;
            for (i = 0; i < list.length; i++) {
              if (list[i].file === obj.file) {
                return true;
              }
            }
            return false;
        }
        }
      });
    }

    $scope.fileUploadCheck = function (form) {
      var selecteFromPisc = form.selectedFromPisc;
      var selectedToPisc = form.topisc;
      var finencialYear = form.financialYearId.$viewValue;
      var fundSource = form.fundSource;
      $scope.errorMessage = [];
      $scope.showFile = false;
      $scope.showActivities = 0;
      if(!(selecteFromPisc.$viewValue == undefined ||  selectedToPisc.$viewValue === undefined ||  fundSource.$viewValue === undefined)){
        if(selecteFromPisc.$viewValue.id !== selectedToPisc.$viewValue.id){
          $scope.showFile = true;
          openReallocation.get({piscFrom:selecteFromPisc.$viewValue.id,piscTo:selectedToPisc.$viewValue.id,finencialYear:finencialYear},function (data) {
            $scope.realocations = data.reallocations;
            if(data.reallocations.length > 0){
              $scope.showActivities = $scope.activities.length;
              $scope.canCreate = false;
              $scope.alert = 'alert-success';
            }else{
              $scope.alert = 'alert-danger';
            }
           });

        }else{
          $scope.errorMessage.push('Please Select Different PISC to Reallocate');
        }
      }
    }

    $scope.openDocuments = function(url){
      window.open(url,'_blank');
    }

    $scope.previewReallocation = function(data) {
      $scope.errorMessage = [];
      if(data.total_amount !=='0'){
        var modalInstance = $uibModal.open({
          templateUrl: '/pages/execution/reallocation-within-vote/view-reallocation.html',
          backdrop: false,
          size: "lg",
          windowClass: 'my-modal-popup',
          controller: function ($scope, $uibModalInstance) {
            $scope.selectedReallocation = data;
            $scope.reallocationItem = [];
            $scope.errorMessage = [];
            $scope.successMessage = [];
            $scope.curPage = 1,
            $scope.itemsPerPage = 1,
            $scope.maxSize = 1000;

            ReallocationWithinVoteService.getReallocationItems({reallocationId:data.id}, function (data) {
              $scope.reallocationItem = data.reallocationItem;
            });

            $scope.close = function () {
               $uibModalInstance.dismiss('cancel');
            };

            $scope.deleteItem = function(item){
              ReallocationWithinVoteService.deleteItem({reallocationId:data.id,id:item.id}, function (data) {
                $scope.reallocationItem = data.reallocationItem;
              });
            }

            $scope.approveAllocation = function(item,status,action){
              var modalInstance = $uibModal.open({
                templateUrl: '/pages/execution/reallocation-within-vote/comment-dialog.html',
                backdrop: false,
                size: "lg",
                windowClass: 'comment-popup',
                controller: function ($scope, $uibModalInstance) {
                  console.log(item);
                  console.log(status);
                  if(status === true && action ===1 ){
                    $scope.action = 'Approve';}
                    else if(status === false && action ===1)
                    {$scope.action = 'Disapprove';}
                    else if(status === true && action ===0){
                      $scope.action = 'Reject';
                    }else if(status === false && action ===0){
                      $scope.action = 'Unreject';
                  }
                  $scope.confirmApprove = function(){

                    BudgetReallocationService.approveReallocation({
                      comments:$scope.comments,
                      data:item=='All'?item:[item],
                      status:status,
                      reallocationId:data.id,
                      action:action,
                    }, function (data) {
                      //$scope.successMessage.push('Reallocation Successfull '+$scope.action);
                      $scope.close();
                    },
                    function (error) {
                      // $scope.errorMessage.push(error.data.errorMessage);
                      $scope.close();
                    });
                  }

                  $scope.close = function () {
                    ReallocationWithinVoteService.getReallocationItems({reallocationId:data.id}, function (data) {
                      $scope.reallocationItem = data.reallocationItem;
                    });
                    $uibModalInstance.dismiss('cancel');
                  };
                }
              });
            }

            $scope.rejectAllocation = function(item,status){
              console.log(item);
              console.log(status);
            }

          }
        });
      }else{
        $scope.errorMessage.push('No Items For This Reallocation');
      }

    }

    $scope.backToReallocation = function(){
      $scope.showActivities = 0;
      ReallocationWithinVoteService.getReallocation({},function (data) {
        $scope.allReallocation = data.reallocations;
       });
    }

  }
})();
