var executionService = angular.module('executionServices', ['ngResource']);


//IMPORT METHODS
executionService.factory('PaginatedImportMethodService', function ($resource) {
  return $resource('/json/import_methods/paginated', {}, {});
});

executionService.factory('CreateImportMethodService', function ($resource) {
  return $resource('/json/import_method/create', {}, {store: {method: 'POST'}});
});

executionService.factory('UpdateImportMethodService', function ($resource) {
  return $resource('/json/import_method/update', {}, {update: {method: 'POST'}});
});

executionService.factory('DeleteImportMethodService', function ($resource) {
  return $resource('/json/import_method/delete/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

executionService.factory('TrashedImportMethodService', function ($resource) {
  return $resource('/json/import_methods/trashed', {}, {});
});

executionService.factory('RestoreImportMethodService', function ($resource) {
  return $resource('/json/import_method/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

executionService.factory('EmptyImportMethodTrashService', function ($resource) {
  return $resource('/json/import_methods/emptyTrash', {}, {});
});

executionService.factory('PermanentDeleteImportMethodService', function ($resource) {
  return $resource('/json/import_method/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});

//RECEIVED FUNDS
executionService.factory('PaginatedReceivedFundService', function ($resource) {
  return $resource('/json/receivedFunds/paginated', {}, {});
});

executionService.factory('CreateReceivedFundService', function ($resource) {
  return $resource('/json/receivedFund/create', {}, {store: {method: 'POST'}});
});

executionService.factory('UpdateReceivedFundService', function ($resource) {
  return $resource('/json/receivedFund/update', {}, {update: {method: 'POST'}});
});

executionService.factory('DeleteReceivedFundService', function ($resource) {
  return $resource('/json/receivedFund/delete/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

executionService.factory('TrashedReceivedFundService', function ($resource) {
  return $resource('/json/receivedFunds/trashed', {}, {});
});

executionService.factory('RestoreReceivedFundService', function ($resource) {
  return $resource('/json/receivedFund/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

executionService.factory('EmptyReceivedFundTrashService', function ($resource) {
  return $resource('/json/receivedFunds/emptyTrash', {}, {});
});

executionService.factory('PermanentDeleteReceivedFundService', function ($resource) {
  return $resource('/json/receivedFund/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});

//DATA SOURCE FUNDS
executionService.factory('PaginatedDataSourceService', function ($resource) {
  return $resource('/json/dataSources/paginated', {}, {});
});

executionService.factory('CreateDataSourceService', function ($resource) {
  return $resource('/json/dataSources/create', {}, {store: {method: 'POST'}});
});

executionService.factory('UpdateDataSourceService', function ($resource) {
  return $resource('/json/dataSources/update', {}, {update: {method: 'POST'}});
});

executionService.factory('DeleteDataSourceService', function ($resource) {
  return $resource('/json/dataSources/delete/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

executionService.factory('TrashedDataSourceService', function ($resource) {
  return $resource('/json/dataSources/trashed', {}, {});
});

executionService.factory('RestoreDataSourceService', function ($resource) {
  return $resource('/json/dataSources/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

executionService.factory('EmptyDataSourceTrashService', function ($resource) {
  return $resource('/json/dataSources/emptyTrash', {}, {});
});
executionService.factory('PermanentDeleteDataSourceService', function ($resource) {
  return $resource('/json/dataSources/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});
//RECEIVED FUND ITEMS
executionService.factory('PaginatedReceivedFundItemService', function ($resource) {
  return $resource('/json/receivedFundItems/paginated', {}, {});
});

executionService.factory('CreateReceivedFundItemService', function ($resource) {
  return $resource('/json/receivedFundItem/create', {}, {store: {method: 'POST'}});
});

executionService.factory('UpdateReceivedFundItemService', function ($resource) {
  return $resource('/json/receivedFundItem/update', {}, {update: {method: 'POST'}});
});

executionService.factory('DeleteReceivedFundItemService', function ($resource) {
  return $resource('/json/receivedFundItem/delete/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

executionService.factory('TrashedReceivedFundItemService', function ($resource) {
  return $resource('/json/receivedFundItems/trashed', {}, {});
});

executionService.factory('RestoreReceivedFundItemService', function ($resource) {
  return $resource('/json/receivedFundItem/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

executionService.factory('EmptyReceivedFundItemTrashService', function ($resource) {
  return $resource('/json/receivedFundItems/emptyTrash', {}, {});
});

executionService.factory('PermanentDeleteReceivedFundItemService', function ($resource) {
  return $resource('/json/receivedFundItem/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});
//Exported transactions service
executionService.factory('ExportedTransactionService', ['$resource', function ($resource) {
  return $resource('/json/exportedTransactions/:id', {}, {
    exportedTransactions: {
      method: 'GET', url: '/json/exportedTransactions',
      params: {
        admin_hierarchy_id: '@admin_hierarchy_id',
        budget_type: '@budget_type',
        system_code: '@system_code'
      }
    }
  });
}]);

//BUDGET EXPORT ACCOUNT
executionService.factory('BudgetExportAccountService', ['$resource', function ($resource) {
  return $resource('/json/budgetExportAccounts/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/budgetExportAccounts/paginated',
      params: {
        adminHierarchyId: '@adminHierarchyId',
        financialYear: '@financialYear',
        page: '@page',
        perPage: '@perPage',
        financial_system_code: '@financial_system_code',
        budget_type: '@budget_type',
        account_type: '@account_type'
      }
    },
    loadInputs: {method: 'GET', url: '/json/budgetExportAccounts/loadInputs'},
    clearInputs: {method: 'GET', url: '/json/budgetExportAccounts/clearInputs'},
    reloadInputs: {method: 'GET', url: '/json/budgetExportAccounts/reloadInputs'},
    exportAccounts: {
      method: 'GET',
      url: '/json/budgetExportAccounts/exportAccounts',
      params: {
        adminHierarchyId: '@adminHierarchyId',
        financialYearId: '@financialYearId',
        system_code: '@system_code',
        budget_type: '@budget_type'
      }
    },
    budgetClassSummary: {method: 'GET', url: '/json/budgetExportAccounts/budgetClassSummary'},
    getTotals: {
      method: 'GET',
      url: '/json/budgetExportAccounts/getTotals',
      params: {
        adminHierarchyId: '@adminHierarchyId',
        financialYearId: '@financialYearId',
        system_code: '@system_code',
        budget_type: '@budget_type',
        account_type: '@account_type'
      }
    },
    glAccountExist: {
      method: 'GET',
      url: '/json/budgetExportAccounts/glAccountExist',
      params: {
        adminHierarchyId: '@adminHierarchyId',
        financialYearId: '@financialYearId',
        financial_system_code: '@financial_system_code',
        budget_type: '@budget_type',
        account_type: '@account_type'
      }
    },
    sendAccountSegment: {
      method: 'GET',
      url: '/json/budgetExportAccounts/sendAccountSegment',
      params: {
        adminHierarchyId: '@adminHierarchyId',
        financialYearId: '@financialYearId',
        financial_system_code: '@financial_system_code',
        budget_type: '@budget_type',
        account_type: '@account_type'
      }
    },
    sendGLAccount: {
      method: 'POST',
      url: '/json/budgetExportAccounts/sendGLAccount',
      params: {adminHierarchyId: '@adminHierarchyId', financialYearId: '@financialYearId'}
    },
    getPE: {method: 'GET', url: '/json/budgetExportAccounts/getPE'},
    sendPE: {method: 'GET', url: '/json/budgetExportAccounts/sendPE'},
    sendPESegment: {method: 'GET', url: '/json/budgetExportAccounts/sendPESegment'},
    sendPEGL: {method: 'GET', url: '/json/budgetExportAccounts/sendPEGL'},
    getTransactions: {
      method: 'GET',
      url: '/json/budgetExportAccounts/getTransactions',
      params: {
        admin_hierarchy_id: '@admin_hierarchy_id',
        financial_year_id: '@financial_year_id',
        budget_type: '@budget_type',
        system_code: '@system_code'
      }
    },
    getBudgetAccounts: {
      method: 'GET',
      url: '/json/budgetExportAccounts/getExportAccounts',
      params: {transaction_id: '@transaction_id'}
    },
    sendTransactionGL: {
      method: 'GET',
      url: '/json/budgetExportAccounts/sendTransactionGL',
      params: {transaction_id: '@transaction_id'}
    },
    sendTransactionSegment: {
      method: 'GET',
      url: '/json/budgetExportAccounts/sendTransactionSegment',
      params: {transaction_id: '@transaction_id'}
    },
    exportTransaction: {
      method: 'GET',
      url: '/json/budgetExportAccounts/sendTransaction',
      params: {transaction_id: '@transaction_id', system_code: '@system_code', selectedItems: '@selectedItems'}
    }
  });
}]);

/** Revenue export feedback*/
executionService.factory('RevenueExportFeedback', ['$resource', function ($resource) {
return $resource('',{},{
  getRevenueFeedback: {
    method: 'GET',
    url: '/json/revenue-export-feedback',
    params: {
      adminHierarchyId: '@adminHierarchyId',
      financialYearId: '@financialYearId',
      financialSystem: '@financialSystem'
    }
  },
  exportTransaction: {
    method: 'GET',
    url: '/json/revenue-export-account/sendTransaction',
    params: {transaction_id: '@transaction_id', system_code: '@system_code', selectedItems: '@selectedItems'}
  }
});
}]);
/** Revenue export account*/
executionService.factory('RevenueExportAccount', ['$resource', function ($resource) {
  return $resource('/json/revenue-export-accounts/:id', {}, {
    get: {
      method: 'GET',
      url: '/json/revenue-export-accounts/by-admin-hierarchy/:adminHierarchyId/by-financial-year/:financialYearId',
      params: {adminHierarchyId: '@adminHierarchyId', financialYearId: '@financialYearId'}
    },
    approve: {
      method: 'PUT',
      url: '/json/revenue-export-accounts/approve/by-selection/:bySelection/by-admin-hierarchy/:adminHierarchyId/by-financial-year/:financialYearId/:budgetType',
      params: {
        adminHierarchyId: '@adminHierarchyId',
        financialYearId: '@financialYearId',
        budgetType: '@budgetType',
        bySelection: '@bySelection'
      }
    },
    getApproved: {
      method: 'GET',
      url: '/json/revenue-export-accounts/get-approved/by-admin-hierarchy',
      params: {
        adminHierarchyId: '@adminHierarchyId',
        financialYearId: '@financialYearId',
        financialSystem: '@financialSystem'
      }
    },
    exportRevenue: {
      method: 'GET',
      url: '/json/revenue-export-accounts/exportRevenue',
      params: {
        adminHierarchyId: '@adminHierarchyId',
        financialYearId: '@financialYearId',
        comments: '@comments',
        financialSystem: '@financialSystem'
      }
    },
    sendAccountSegment: {
      method: 'GET',
      url: '/json/revenue-export-accounts/sendAccountSegment',
      params: {
        adminHierarchyId: '@adminHierarchyId',
        financialYearId: '@financialYearId',
        financial_system_code: '@financial_system_code'
      }
    },
    glAccountExist: {
      method: 'GET',
      url: '/json/revenue-export-accounts/glAccountExist',
      params: {
        adminHierarchyId: '@adminHierarchyId',
        financialYearId: '@financialYearId',
        financial_system_code: '@financial_system_code'
      }
    },
  });
}]);

/** Financial Systems*/
executionService.factory('BudgetExportToFinancialSystemService', ['$resource', function ($resource) {
  return $resource('/json/toFinancialSystems/:id', {}, {
    paginated: {method: 'GET', url: '/json/toFinancialSystems/paginated'},
    send: {method: 'GET', url: '/json/toFinancialSystems/:id/send'}
  });
}]);

/** Budget transaction types*/
executionService.factory('PaginatedBudgetTransactionTypeService', function ($resource) {
  return $resource('/json/budgetTransactionTypes/paginated', {}, {});
});

executionService.factory('CreateBudgetTransactionTypeService', function ($resource) {
  return $resource('/json/budgetTransactionType/create', {}, {store: {method: 'POST'}});
});

executionService.factory('UpdateBudgetTransactionTypeService', function ($resource) {
  return $resource('/json/budgetTransactionType/update', {}, {update: {method: 'POST'}});
});

executionService.factory('DeleteBudgetTransactionTypeService', function ($resource) {
  return $resource('/json/budgetTransactionType/delete/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

executionService.factory('TrashedBudgetTransactionTypeService', function ($resource) {
  return $resource('/json/budgetTransactionTypes/trashed', {}, {});
});

executionService.factory('RestoreBudgetTransactionTypeService', function ($resource) {
  return $resource('/json/budgetTransactionType/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

executionService.factory('EmptyBudgetTransactionTypeTrashService', function ($resource) {
  return $resource('/json/budgetTransactionTypes/emptyTrash', {}, {});
});

executionService.factory('PermanentDeleteBudgetTransactionTypeService', function ($resource) {
  return $resource('/json/budgetTransactionType/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});

//BUDGET EXPORT TRANSACTIONS
executionService.factory('PaginatedBudgetExportTransactionService', function ($resource) {
  return $resource('/json/budgetExportTransactions/paginated', {}, {});
});

executionService.factory('CreateBudgetExportTransactionService', function ($resource) {
  return $resource('/json/budgetExportTransaction/create', {}, {store: {method: 'POST'}});
});

executionService.factory('UpdateBudgetExportTransactionService', function ($resource) {
  return $resource('/json/budgetExportTransaction/update', {}, {update: {method: 'POST'}});
});

executionService.factory('DeleteBudgetExportTransactionService', function ($resource) {
  return $resource('/json/budgetExportTransaction/delete/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

executionService.factory('TrashedBudgetExportTransactionService', function ($resource) {
  return $resource('/json/budgetExportTransactions/trashed', {}, {});
});

executionService.factory('RestoreBudgetExportTransactionService', function ($resource) {
  return $resource('/json/budgetExportTransaction/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

executionService.factory('EmptyBudgetExportTransactionTrashService', function ($resource) {
  return $resource('/json/budgetExportTransactions/emptyTrash', {}, {});
});

executionService.factory('PermanentDeleteBudgetExportTransactionService', function ($resource) {
  return $resource('/json/budgetExportTransaction/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});

//BUDGET_IMPORT_ISSUE
executionService.factory('PaginatedBudgetImportIssueTypeService', function ($resource) {
  return $resource('/json/budgetImportIssueTypes/paginated', {}, {});
});

executionService.factory('CreateBudgetImportIssueTypeService', function ($resource) {
  return $resource('/json/budgetImportIssueTypes/create', {}, {store: {method: 'POST'}});
});

executionService.factory('UpdateBudgetImportIssueTypeService', function ($resource) {
  return $resource('/json/budgetImportIssueTypes/update', {}, {update: {method: 'POST'}});
});

executionService.factory('DeleteBudgetImportIssueTypeService', function ($resource) {
  return $resource('/json/budgetImportIssueTypes/delete/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

executionService.factory('TrashedBudgetImportIssueTypeService', function ($resource) {
  return $resource('/json/budgetImportIssueTypes/trashed', {}, {});
});

executionService.factory('RestoreBudgetImportIssueTypeService', function ($resource) {
  return $resource('/json/budgetImportIssueTypes/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

executionService.factory('EmptyBudgetImportIssueTypeTrashService', function ($resource) {
  return $resource('/json/budgetImportIssueTypes/emptyTrash', {}, {});
});
executionService.factory('PermanentDeleteBudgetImportIssueTypeService', function ($resource) {
  return $resource('/json/budgetImportIssueTypes/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});

//BUDGET IMPORT
executionService.factory('PaginatedBudgetImportService', function ($resource) {
  return $resource('/json/budgetImports/paginated', {}, {});
});

executionService.factory('CreateBudgetImportService', function ($resource) {
  return $resource('/json/budgetImports/create', {}, {store: {method: 'POST'}});
});

executionService.factory('UpdateBudgetImportService', function ($resource) {
  return $resource('/json/budgetImports/update', {}, {update: {method: 'POST'}});
});

executionService.factory('DeleteBudgetImportService', function ($resource) {
  return $resource('/json/budgetImports/delete/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

executionService.factory('TrashedBudgetImportService', function ($resource) {
  return $resource('/json/budgetImports/trashed', {}, {});
});

executionService.factory('RestoreBudgetImportService', function ($resource) {
  return $resource('/json/budgetImports/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

executionService.factory('EmptyBudgetImportTrashService', function ($resource) {
  return $resource('/json/budgetImports/emptyTrash', {}, {});
});

executionService.factory('PermanentDeleteBudgetImportService', function ($resource) {
  return $resource('/json/budgetImports/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});


//BUDGET REALLOCATIONS
executionService.factory('BudgetReallocationService', ['$resource', function ($resource) {
  return $resource('/json/budgetReallocations/', {}, {
    getOpen: {
      method: 'GET',
      url: '/json/budgetReAllocations/get-open',
    },
    getById: {
      method: 'GET',
      url: '/json/budgetReAllocations/get-open/:id',
      params: {id: '@id'}
    },
    getItems: {
      method: 'GET',
      url: '/json/budgetReAllocations/get-items/:id',
      params: {id: '@id'}
    },
    deleteItem: {
      method: 'DELETE',
      url: '/json/budgetReAllocations/delete-item/:itemId',
      params: {itemId: '@itemId'}
    },
    reUseItem: {
      method: 'PUT',
      url: '/json/budgetReAllocations/reuse-item/:itemId',
      params: {itemId: '@itemId'}
    },
    submit: {
      method: 'POST',
      url: '/json/budgetReAllocations/submit/:id/:decisionLevelId',
      params: {
        id: '@id',
        decisionLevelId: '@decisionLevelId'
      }
    },
    getReallocationAccount: {
      method: 'GET',
      url: '/json/budgetReAllocations/reallocationAccount/:selectedFromActivityFundSourceId',
      params: {
        selectedFromActivityFundSourceId: '@selectedFromActivityFundSourceId'
      }
    },
    getToReallocationAccount: {
      method: 'GET',
      url: '/json/budgetReAllocations/toReallocationAccount'
    },
    getReallocationActivities: {
      method: 'GET',
      url: '/json/budgetReAllocations/reallocationActivities/:budgetType/:financialYearId/:adminHierarchyId/:sectionId/:facilityId/:fundSourceId',
      params: {
        budgetType: '@budgetType',
        financialYearId: '@financialYearId',
        adminHierarchyId: '@adminHierarchyId',
        sectionId: '@sectionId',
        facilityId: '@facilityId',
        fundSourceId: '@fundSourceId'
      }
    },
    getReallocationToApprove: {
      method: 'GET',
      url: '/json/budgetReAllocations/reallocationToApprove/:budgetType/:financialYearId/:adminHierarchyId/:sectionId',
      params: {
        budgetType: '@budgetType',
        financialYearId: '@financialYearId',
        adminHierarchyId: '@adminHierarchyId',
        sectionId: '@sectionId'
      }
    },
    getAccountReallocation: {
      url: '/json/budgetReAllocations/accountReallocations/:accountId',
      params: {
        accountId: '@accountId'
      }
    },
    getMtefSectionToReallocate: {
      method: 'GET',
      url: '/json/budgetReAllocations/mtefSectionToReallocate/:sectionId',
      params: {sectionId: '@sectionId'}
    },
    createReallocation: {
      method: 'POST', url: '/json/budgetReAllocations/createReallocation/:reallocationId',
      params: {reallocationId: '@reallocationId'}
    },
    getApprovedReallocation: {
      method: 'GET',
      url: '/json/budgetReAllocations/getApprovedReallocation',
      params: {
        admin_hierarchy_id: '@admin_hierarchy_id', financial_year_id: '@financial_year_id', budget_type: '@budget_type',
        system_code: '@system_code'
      }
    },
    getFailedReallocation: {
      method: 'GET',
      url: '/json/budgetReAllocations/getFailedReallocation',
      params: {
        admin_hierarchy_id: '@admin_hierarchy_id', financial_year_id: '@financial_year_id', budget_type: '@budget_type',
        system_code: '@system_code'
      }
    },
    approveReallocation: {method: 'POST', url: '/json/budgetReAllocations/approveReallocation'},
    rejectReallocation: {method: 'POST', url: '/json/budgetReAllocations/rejectReallocation'},
    exportReallocation: {method: 'POST', url: '/json/budgetReAllocations/exportReallocation'},
    createAndGetReallocationToAccounts: {
      method: 'POST',
      url: '/json/budgetReAllocations/createAndGetReallocationToAccounts/:mtefSectionId',
      params: {mtefSectionId: '@mtefSectionId'}
    },
    getMassReallocation: {
      method: 'GET',
      url: '/json/budget-mass-reallocation/:budgetType/:financialYearId/:adminHierarchyId/:sectorId/:level',
      params: {
        budgetType: '@budgetType',
        financialYearId: '@financialYearId',
        adminHierarchyId: '@adminHierarchyId',
        sectorId: '@sectorId',
        level: '@level'
      }
    },
    reallocate: {
      method: 'POST',
      url: '/json/budget-mass-reallocation/reallocate/:budgetType/:financialYearId/:adminHierarchyId/:sectorId/:exportTransactionsToEpicor/:exportTransactionsToFfars',
      params: {
        budgetType: '@budgetType',
        financialYearId: '@financialYearId',
        adminHierarchyId: '@adminHierarchyId',
        sectorId: '@sectorId',
        exportTransactionsToEpicor: '@exportTransactionsToEpicor',
        exportTransactionsToFfars: '@exportTransactionsToFfars'

      }
    },
    savePiscReallocation: {
      method: 'POST',
      url: '/json/openReallocation/:inputData/:attachement',
      params: {
        inputData: '@inputData',
        attachement: '@attachement'
      }
    },
    storeReallocationitemsAmount: {
      method: 'POST',
      url: '/json/openReallocation/:budget',
      params: {
        budget: '@budget',
      }
    },
    approveReallocation: {
      method: 'POST',
      url: '/json/approveReallocation/:comments/:data/:status/:reallocationId',
      params: {
        comments: '@comments',
        data: '@data',
        status: '@status',
        reallocationId: '@reallocationId'
      }
    }


  });
}]);

executionService.factory('CeilingExportService', function ($resource) {
  return $resource('/json/CeilingExportAmount', {}, {
    getFinancialYears: {
      method: 'GET',
      url: '/json/CeilingExportAmount/fetch-financial-years',
    },
    getCeilingsAmount:{
      method: 'GET',
      url: '/json/CeilingExportAmount/get-ceilings-amount',
    },
    exportCeilingToFinancialSystem:{
      method: 'POST',
      url: '/json/CeilingExportAmount/export-ceiling-to-financial-system',
    }
  })
});

executionService.factory('CoaSegmentFeedBackService', ['$resource', function ($resource) {
  return $resource('json/coa_segments/:id', {}, {
    getCoaSegments: {method: 'GET', url: '/json/coa_segments'},
    generate_coa_segments: {method: 'GET', url: '/json/generate_coa_segments'},
    resend_coa_segments: {method: 'GET', url: '/json/send_coa_segments'},
    clearCOASegments: {method: 'GET', url: '/json/clear_coa_segments/:id'}
  });
}]);

executionService.factory('glAccountFeedBackService', ['$resource', function ($resource) {
  return $resource('/json/gl_account_issues', {}, {
    getGLAccountIssues: {method: 'GET', url: 'json/gl_account_issues'},
    clearGLAccountIssues: {method: 'GET', url: 'json/clear_gl_account_issues/:id', params: {id: '@id'}}
  });
}]);

executionService.factory('PublishCoaSegmentsService', function ($resource) {
  return $resource('json/publish_coa_segments', {}, {});
});

executionService.factory('SocketIO', ['$rootScope', function ($rootScope) {

  var socket = io('http://localhost:3000');

  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },

    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      });
    }
  };
}]);

executionService.factory('BudgetExportResponseService', ['$resource', function ($resource) {
  return $resource('BudgetExportResponseService:id', {}, {
    paginated: {method: 'GET', url: '/json/budgetExportResponses/paginated'},
  });
}]);

executionService.factory('FFARSBudgetExportResponseService', ['$resource', function ($resource) {
  return $resource('/json/FFARSbudgetExportResponses/:id', {}, {
    ffarsBudget: {method: 'GET', url: '/json/FFARSbudgetExportResponses/paginated'},
    resendFFARSBudget: {method: 'GET', url: '/json/FFARSbudgetExportResponses/resend_budget'}
  });
}]);

executionService.factory('RabbitMqFeedbackService', ['$resource', function ($resource) {
  return $resource('/json/rabbitMqFeedback', {}, {
    getCoaSegmentFeedBack: {method: 'GET', url: '/json/rabbitMqFeedback/getCoaSegmentFeedBack'},
    getGLAccountsQueueFeedBack: {method: 'GET', url: '/json/rabbitMqFeedback/getGLAccountsQueueFeedBack'},
    getBudgetQueueFeedBack: {method: 'GET', url: '/json/rabbitMqFeedback/getBudgetQueueFeedBack'},
  });
}]);

executionService.factory('BudgetExportResponseService', ['$resource', function ($resource) {
  return $resource('/json/budgetExportResponses/:id', {}, {
    paginated: {method: 'GET', url: '/json/budgetExportResponses/paginated'},
    clearExportIssue: {method: 'GET', url: '/json/budgetExportResponses/clearExportIssue'},
  });
}]);


executionService.factory('SetupSegmentExportService', ['$resource', function ($resource) {
  return $resource('/ffars/exports/facilities', {}, {
    facilities: {method: 'GET', url: '/ffars/exports/facilities'},
    subBudgetClasses: {method: 'GET', url: '/ffars/exports/sub-budget-classes'},
    projects: {method: 'GET', url: '/ffars/exports/projects'},
    gfsCodes: {method: 'GET', url: '/ffars/exports/gfs-codes'},
    fundSources: {method: 'GET', url: '/ffars/exports/fund-sources'},
    fundTypes: {method: 'GET', url: '/ffars/exports/fund-types'},
  });
}]);
executionService.factory('PreExecutionService', ['$resource', function ($resource) {
  return $resource('/json/execution/pre-execution/:id', {}, {
    paginated: {method: 'GET', url: '/json/execution/pre-execution/paginated'},
    activities: {method: 'GET', url: '/json/execution/pre-execution/activities'},
    addMileStone: {method: 'POST', url: '/json/execution/pre-execution/addMileStone'},
    updateMainTaskData: {method: 'POST', url: '/json/execution/pre-execution/updateMainTaskData'},
    updateMileStoneData: {method: 'POST', url: '/json/execution/pre-execution/updateMileStoneData'},
    addProjectOutput: {method: 'POST', url: '/json/execution/pre-execution/addProjectOutput'},
    removeMileStone: {method: 'GET', url: '/json/execution/pre-execution/removeMileStone'},
    removeOutput: {method: 'GET', url: '/json/execution/pre-execution/removeOutput'},
    milestones: {method: 'GET', url: '/json/execution/pre-execution/milestones'},
    outputs: {method: 'GET', url: '/json/execution/pre-execution/outputs'},
    periods: {method: 'GET', url: '/json/execution/pre-execution/periods'},
    costCentreFundSources: {method: 'GET', url: '/json/execution/pre-execution/costCentreFundSources'},
    addPhase: {method: 'POST', url: '/json/execution/pre-execution/addPhase'},
    editProjectOutput: {method: 'POST', url: '/json/execution/pre-execution/editProjectOutput'},
    removePhase: {method: 'GET', url: '/json/execution/pre-execution/removePhase'},
    update: {method: 'PUT', params: {id: '@id'}},
    expenditureCategoryByTaskNature: {
      method: 'GET',
      url: '/json/expenditureCategories/expenditureCategoryByTaskNature'
    },
    projectOutputs: {method: 'GET', url: '/json/expenditureCategories/projectOutputs'},
  });
}]);

executionService.factory('ActivityPeriodService', ['$resource', function ($resource) {
  return $resource('/api/activityPeriods/:id', {}, {
    paginated: {method: 'GET', url: '/api/activityPeriods/paged'},
    comment: {method: 'POST', url: '/api/activityPeriods/comment'},
  });
}]);

executionService.factory('BudgetReallocationApproveService', ['$resource', function ($resource) {
  return $resource('/api/budgetReallocationApprove/:id', {}, {
    getCouncils: {method: 'GET', url: '/api/budgetReallocationCouncils'},
    getReallocations: {method: 'GET', url: '/api/getbudgetReallocations', params: {id: '@id'}},
    returnReallocation: {
      method: 'POST',
      url: '/api/returnBudgetReallocation/:id/:decisionLevelId',
      params: {id: '@id', decisionLevelId: '@decisionLevelId'}
    },
    rejectReallocation: {method: 'POST', url: '/api/budgetReallocationApprove/reject'},
    approve: {method: 'POST', url: '/api/approveBudgetReallocation'}
  });
}]);

executionService.factory('ActivityExpenditureService', ['$resource', function ($resource) {
  return $resource('/api/activityExpenditure/:id', {}, {
    matchList: {method: 'GET', url: '/api/getMatchedItems'},
    match: {method: 'GET', url: '/api/MatchItem'},
    automatch: {method: 'POST', url: '/api/autoMatchItems'}
  });
}]);

executionService.factory('RevenueAllocationExpenditureService', ['$resource', function ($resource) {
  return $resource('/api/revenueAllocationExpenditure/:id', {}, {
    items: {method: 'GET', url: '/api/revenueAllocationExpenditure'},
  });
}]);

executionService.factory('ReallocationFeedbackService', ['$resource', function ($resource) {
  return $resource('/api/reallocationFeedback/:id', {}, {
    items: {method: 'GET', url: '/api/reallocationFeedback'},
  });
}]);






