var services = angular.module("master.services", ["ngResource"]);
/**Activity Categories */
services.factory("ActivityCategories", [
  "$http",
  function ($http) {
    return {
      getActive: function () {
        return $http.get("/json/activity-categories");
      },
    };
  },
]);
/**Activity Category*/
services.factory("ActivityCategory", [
  "$resource",
  function ($resource) {
    return $resource("/json/activity-categories", {
      query: {
        method: "GET",
      },
    });
  },
]);
/**Activity Category*/
/**Reference documents start*/
services.factory("ReferenceDocument", [
  "$resource",
  function ($resource) {
    return $resource(
      "/json/reference-documents/:isNationalGuideline/:id",
      {
        isNationalGuideline: "@isNationalGuideline",
      },
      {
        query: {
          method: "GET",
          isArray: true,
        },
        paginated: {
          method: "GET",
          url: "/json/reference-documents/:isNationalGuideline/paginated",
          params: {
            isNationalGuideline: "@isNationalGuideline",
          },
        },
        toggleActive: {
          method: "PUT",
          url: "/json/reference-documents/:isNationalGuideline/toggle-active",
          params: {
            isNationalGuideline: "@isNationalGuideline",
          },
        },
        update: {
          method: "PUT",
          params: {
            isNationalGuideline: "@isNationalGuideline",
            id: "@id",
          },
        },
      }
    );
  },
]);

services.factory("MoveActivity", function ($resource) {
  return $resource(
    "/json/move-activity",
    {},
    {
      getBF: {
        method: "GET",
        url: "/json/move-activity-data",
      },
      getMtefs: {
        method: "GET",
        url: "/json/move-activity-mtefs",
        isArray: true,
      },
      getMtefSections: {
        method: "GET",
        isArray: true,
        url: "/json/move-activity-mtef-sections/:mtefId",
        params: {
          mtefId: "@mtefId",
        },
      },
      getActivities: {
        method: "GET",
        isArray: true,
        url:
          "/json/move-activity-activities/:budgetType/:mtefId/:mtefSectionId",
        params: {
          budgetType: "@budgetType",
          mtefId: "@mtefId",
          mtefSectionId: "@mtefSectionId",
        },
      },
      moveBudgetClass: {
        method: "PUT",
        url: "/json/move-activity-budget-class/:activityId/:budgetClassId",
        params: {
          activityId: "@activityId",
          budgetClassId: "@budgetClassId",
        },
      },
      moveFundSource: {
        method: "PUT",
        url:
          "/json/move-facility-fund-source/:activityFacilityFundSourceId/:fundSourceId",
        params: {
          activityFacilityFundSourceId: "@activityFacilityFundSourceId",
          fundSourceId: "@fundSourceId",
        },
      },
      moveCostCentre: {
        method: "PUT",
        url: "/json/move-activity-cost-centre/:activityId/:mtefSectionId",
        params: {
          activityId: "@activityId",
          mtefSectionId: "@mtefSectionId",
        },
      },
      moveFacility: {
        method: "PUT",
        url: "/json/move-activity-facility/:activityFacilityId/:facilityId",
        params: {
          activityFacilityId: "@activityFacilityId",
          facilityId: "@facilityId",
        },
      },
      moveTarget: {
        method: "PUT",
        url: "/json/move-activity-target/:activityId/:targetId",
        params: {
          activityId: "@activityId",
          targetId: "@targetId",
        },
      },
    }
  );
});
services.factory("MoveCeiling", function ($resource) {
  return $resource(
    "/json/move-ceiling",
    {},
    {
      getCeiling: {
        url:
          "/json/move-ceiling/getCeiling/:financialYearId/:budgetType/:adminHierarchyId/:sectorId",
        params: {
          financialYearId: "@financialYearId",
          budgetType: "@budgetType",
          adminHierarchyId: "@adminHierarchyId",
          sectorId: "@sectorId",
        },
      },
      getAdminCeiling: {
        url:
          "/json/move-ceiling/getAdminCeiling/:financialYearId/:budgetType/:adminHierarchyId/:sectorId/:ceilingId",
        params: {
          financialYearId: "@financialYearId",
          budgetType: "@budgetType",
          adminHierarchyId: "@adminHierarchyId",
          sectorId: "@sectorId",
          ceilingId: "@ceilingId",
        },
      },
      moveAdminCeiling: {
        url:
          "/json/move-ceiling/moveAdminCeiling/:financialYearId/:budgetType/:adminHierarchyId/:sectorId/:ceilingId/:newCeilingId",
        params: {
          financialYearId: "@financialYearId",
          budgetType: "@budgetType",
          adminHierarchyId: "@adminHierarchyId",
          sectorId: "@sectorId",
          ceilingId: "@ceilingId",
          newCeilingId: "@newCeilingId",
        },
      },
    }
  );
});

/** reference document end*/

services.factory("SectionsByLevel", function ($resource) {
  return $resource(
    "/json/sectionsByLevel/:levelId/:adminHierarchyId",
    {
      levelId: "@levelId",
      adminHierarchyId: "@adminHierarchyId",
    },
    {}
  );
});

/** Session*/
services.factory("sessions", function ($resource) {
  return $resource("/api/advertisements", {}, {});
});

//Reallocation within vote pisc-to-pisc
services.factory("openReallocation", function ($resource) {
  return $resource(
    "/json/openReallocation/:piscFrom/:piscTo/:finencialYear",
    {
      piscFrom: "@piscFrom",
      piscTo: "@piscTo",
      finencialYear: "@finencialYear",
    },
    {}
  );
});

//End Reallocation within vote pisc-to-pisc

services.factory("SectionsByLevelFilter", function ($resource) {
  return $resource(
    "/json/sectionsByLevelFilter/:levelId",
    {
      levelId: "@levelId",
    },
    {}
  );
});

services.factory("Plan", function ($resource) {
  return $resource(
    "/json/plan/:id",
    {},
    {
      filter: {
        url:
          "/json/plan/filter/:budgetType/:financialYearId/:adminHierarchyId/:sectionId",
        method: "GET",
        params: {
          budgetType: "@budgetType",
          financialYearId: "@financialYearId",
          adminHierarchyId: "@adminHierarchyId",
          sectionId: "@sectionId",
        },
      },
    }
  );
});

services.factory("Budgets", function ($resource) {
  return $resource("/json/budgets", {}, {});
});

services.factory("ReportDashletService", function ($resource) {
  return $resource("/json/reportDashlets", {}, {});
});

services.factory("AllAdminHierarchyLevelsService", function ($resource) {
  return $resource("/json/allAdminHierarchyLevels", {}, {});
});

services.factory("AllAdminHierarchyLevelService", function ($resource) {
  return $resource("/json/allAdminHierarchyLevels", {}, {});
});
services.factory("AdminHierarchyLevelsByUserService", function ($resource) {
  return $resource("/json/adminHierarchyLevelsByUserService", {}, {});
});

services.factory("PaginateSectionService", function ($resource) {
  return $resource("/json/PaginateSections", {}, {});
});
services.factory("AllBudgetClassService", function ($resource) {
  return $resource("/json/allBudgetClasses", {}, {});
});

services.factory("PlanningBudgetClassService", function ($resource) {
  return $resource("/json/planningBudgetClasses", {}, {});
});

services.factory("AllPeriodService", function ($resource) {
  return $resource("/json/periods", {}, {});
});

//return periods for a comprehensive plan
services.factory("casPlanPeriodsService", function ($resource) {
  return $resource(
    "/json/cas_plan_periods",
    { table_id: "@table_id" },
    { store: { method: "GET" } }
  );
});

services.factory("ResponsiblePersonsService", function ($resource) {
  return $resource(
    "/json/responsiblePersons/:id",
    {
      id: "@id",
    },
    {
      getByAdminAreaAndSector: {
        url:
          "/json/responsible-persons-by-admin-area-sector/:adminHierarchyId/:sectionId",
        params: {
          adminHierarchyId: "@adminHierarchyId",
          sectionId: "@sectionId",
        },
      },
    }
  );
});

services.factory("ProjectsService", function ($resource) {
  return $resource(
    "/json/projects",
    {},
    {
      getBySector: {
        method: "GET",
        url: "/json/projects-by-sector/:sectorId",
        params: {
          sectorId: "@sectorId",
        },
      },
    }
  );
});

services.factory("FundSourceService", function ($resource) {
  return $resource(
    "/json/fundSources",
    {},
    {
      getBudgetedByFacility: {
        method: "GET",
        url:
          "/json/fundSources/budgetedByFacility/:budgetType/:financialYearId/:adminHierarchyId/:sectionId/:facilityId",
        params: {
          budgetType: "@budgetType",
          financialYearId: "@financialYearId",
          adminHierarchyId: "@adminHierarchyId",
          sectionId: "@sectionId",
          facilityId: "@facilityId",
        },
      },
      getByBudgetClassAndSector: {
        method: "GET",
        url:
          "/json/fund-sources/by-financial-year/:financialYearId/by-budget-class/:budgetClassId/by-sector/:sectionId",
        params: {
          budgetClassId: "@budgetClassId",
          sectionId: "@sectionId",
          financialYearId: "@financialYearId",
        },
      },
      getByMainBudgetClassAndSector: {
        method: "GET",
        url:
          "/json/fund-sources/by-financial-year/:financialYearId/by-main-budget-class/:fundSourceId/by-sector/:sectionId",
        params: {
          fundSourceId: "@fundSourceId",
          sectionId: "@sectionId",
          financialYearId: "@financialYearId",
        },
      },
    }
  );
});

services.factory("FinancialYearsService", [
  "$resource",
  function ($resource) {
    return $resource(
      "/json/financialYears",
      {},
      {
        forIndicatorProjection: {
          method: "GET",
          url: "/json/financialYears/forIndicatorProjection",
        },
        getUsableFinancialYears: {
          method: "GET",
          url: "/json/getUsableFinancialYears",
        },
        getByBudgetType: {
          method: "GET",
          url: "/json/financial-years/by-budget-type/:budgetType",
          params: {
            budgetType: "@budgetType",
          },
        },
      }
    );
  },
]);
services.factory("BudgetType", function ($resource) {
  return $resource("/json/budget-types", {}, {});
});
services.factory("AccessRightsService", function ($resource) {
  return $resource("/json/accessRights", {}, {});
});

services.factory("SectionLevelsService", function ($resource) {
  return $resource("/json/sectionLevels", {}, {});
});
services.factory("SectionLevelsByUserService", function ($resource) {
  return $resource("/json/allSectionLevelsByUser", {}, {});
});

services.factory("SectionService", function ($resource) {
  return $resource(
    "/json/sections",
    {},
    {
      getUserCanBudgetSections: {
        method: "GET",
        url: "/json/userCanBudgetSections",
      },
      getByFacility: {
        url:
          "/json/sections/by-section-level/:sectionLevelId/by-facility/:facilityId/by-fund-source/:fundSourceId",
        params: {
          sectionLevelId: "@sectionLevelId",
          facilityId: "@facilityId",
          fundSourceId: "@fundSourceId",
        },
      },
    }
  );
});

services.factory("SectionToActivateService", function ($resource) {
  return $resource("/json/sections-to-be-activated", {}, {});
});

services.factory("ActivateSectionService", function ($resource) {
  return $resource(
    "/json/activate-section",
    {},
    {
      activate: {
        method: "POST",
      },
    }
  );
});

services.factory("GeoLocationLevelsService", function ($resource) {
  return $resource("/json/geoLocationLevels", {}, {});
});

services.factory("AllGeolocationLevelsService", function ($resource) {
  return $resource("/json/allGeolocationLevels", {}, {});
});

services.factory("GeoLocationService", function ($resource) {
  return $resource(
    "/json/geoLocations",
    {},
    {
      getGeoLocationChildren: {
        method: "GET",
        url: "/json/geoLocations/getGeoLocationChildren/:id",
        params: {
          id: "@id",
        },
      },
    }
  );
});

services.factory("AdminHierarchyLevelsService", function ($resource) {
  return $resource("/json/adminHierarchyLevels", {}, {});
});

services.factory("AdminHierarchiesService", function ($resource) {
  return $resource(
    "/json/adminHierarchies",
    {},
    {
      getUserAdminHierarchy: {
        method: "GET",
        url: "/json/adminHierarchies/userAdminHierarchy",
      },
      getUserAdminHierarchyLevels: {
        method: "GET",
        url: "/json/admin-hierarchy-levels/get-user-level-and-children",
      },
      getBudgetingChildLevels: {
        method: "GET",
        url: "/json/adminHierarchies/getBudgetingChildLevels",
      },
      getAdminHierarchyChildren: {
        method: "GET",
        url:
          "/json/adminHierarchies/getAdminHierarchyChildren/:adminHierarchyId",
        params: {
          adminHierarchyId: "@adminHierarchyId",
        },
      },
    }
  );
});

services.factory("ReferenceTypesService", function ($resource) {
  return $resource("/json/referenceTypes", {}, {});
});

services.factory("FundersService", function ($resource) {
  return $resource("/json/funders", {}, {});
});

services.factory("UnitsService", function ($resource) {
  return $resource("/json/paginate", {}, {});
});
services.factory("AllUnitsService", function ($resource) {
  return $resource("/json/units", {}, {});
});

services.factory("UserGroupsService", function ($resource) {
  return $resource("/json/userGroups", {}, {});
});

services.factory("BudgetClassService", function ($resource) {
  return $resource("/json/budgetClasses", {}, {});
});

services.factory("DecisionLevelService", function ($resource) {
  return $resource("/json/decisionLevels", {}, {});
});

services.factory("AllFacilityTypeService", function ($resource) {
  return $resource("/json/allFacilityTypes", {}, {});
});

services.factory("AllProcurementTypeService", function ($resource) {
  return $resource("/json/procurementTypes", {}, {});
});

services.factory("AllAccountTypeService", function ($resource) {
  return $resource("/json/accountTypes", {}, {});
});

services.factory("PlanChainsService", function ($resource) {
  return $resource(
    "/json/planChains",
    {},
    {
      getTopLevel: {
        method: "GET",
        url: "/json/plan-chains/get-top-level",
      },
      getChildren: {
        method: "GET",
        url: "/json/plan-chains/get-children/:parentId",
        params: {
          parentId: "@parentId",
        },
      },
    }
  );
});

services.factory("selectedPlanChain", function () {
  var _selectedPlanChain = [];
  return {
    get: function (level) {
      return _selectedPlanChain[level];
    },
    set: function (planChainLevel, level) {
      _selectedPlanChain[level] = planChainLevel;
    },
  };
});

services.factory("selectedScrutiny", function () {
  var _selectedScrutiny = [];
  return {
    get: function (type) {
      return _selectedScrutiny[type];
    },
    set: function (type, scrutiny) {
      _selectedScrutiny[type] = scrutiny;
    },
  };
});

services.factory("selectedReallocation", function () {
  var _selectedReallocation;
  return {
    get: function () {
      return _selectedReallocation;
    },
    set: function (reallocation) {
      _selectedReallocation = reallocation;
    },
  };
});

services.factory("ProjectFundersService", function ($resource) {
  return $resource("/json/projectFunders", {}, {});
});

services.factory("AllGfsCodeCategoryService", function ($resource) {
  return $resource("/json/gfsCodeCategories", {}, {});
});

services.factory("AllFacilityService", function ($resource) {
  return $resource("/json/facilities", {}, {});
});

services.factory("FacilityService", function ($resource) {
  return $resource(
    "/json/facilities",
    {},
    {
      facilitiesByTypeAndHierarchy: {
        method: "GET",
        url: "/json/facilities/facilitiesByTypeAndHierarchy",
      },
      facilitiesByTypeCouncilUser: {
        method: "GET",
        url: "/json/facilities/facilitiesByTypeCouncilUser",
      },
      facilitiesAllByTypeCouncilUser: {
        method: "GET",
        url: "/json/facilities/facilitiesAllByTypeCouncilUser",
      },

      facilitiesByTypeCouncilSetup: {
        method: "GET",
        url: "/json/facilities/facilitiesByTypeCouncilSetup",
      },
      setBankAccount: {
        method: "POST",
        url: "/json/facilities/setBankAccount",
      },
      deleteBankAccount: {
        method: "GET",
        url: "/json/facilities/deleteBankAccount",
      },
      setCustomDetails: {
        method: "POST",
        url: "/json/facilities/setCustomDetails",
      },
      deleteCustomDetail: {
        method: "GET",
        url: "/json/facilities/deleteCustomDetail",
      },
      getByUserSection: {
        method: "GET",
        isArray: true,
        url: "/json/facilityByUserSection",
      },
      search: {
        method: "GET",
        url: "/json/facilities/search-facility",
      },
      searchByPlanningSection: {
        method: "POST",
        url:
          "/json/facility/searchByPlanningSection/:mtefSectionId/:isFacilityAccount",
        params: {
          mtefSectionId: "@mtefSectionId",
          isFacilityAccount: "@isFacilityAccount",
        },
      },
      searchByAdminAreaAndSection: {
        method: "GET",
        url:
          "/json/facility/searchByAdmiAreaAndSection/:adminHierarchyId/:sectionId/:isFacilityUser",
        params: {
          adminHierarchyId: "@adminHierarchyId",
          sectionId: "@sectionId",
          isFacilityUser: "@isFacilityUser",
        },
      },
      getAllByPlanningSection: {
        method: "POST",
        url:
          "/json/facility/getAllByPlanningSection/:mtefSectionId/:isFacilityAccount",
        params: {
          mtefSectionId: "@mtefSectionId",
          isFacilityAccount: "@isFacilityAccount",
        },
      },
      getByUser: {
        method: "GET",
        url: "/json/facility/get-by-user/:userId",
        params: {
          userId: "@userId",
        },
      },
      getHomeFacilityAndSuperviedFacilityTypes: {
        method: "GET",
        url:
          "/json/facility/get-home-facility-and-supervised-facility-types/:adminHierarchyId/:sectionId",
        params: {
          adminHierarchyId: "@adminHierarchyId",
          sectionId: "@sectionId",
        },
      },
    }
  );
});

services.factory("AllFacilityTypeService", function ($resource) {
  return $resource("/json/all-facility-types", {}, {});
});

services.factory("GsfCodeService", function ($resource) {
  return $resource("/json/gfsCodes", {}, {});
});
services.factory("ExpenditureGsfCodeService", function ($resource) {
  return $resource(
    "/json/expenditureGfsCodes",
    {},
    {
      search: {
        method: "POST",
        url: "/json/expenditureGfsCodes/search",
      },
    }
  );
});

services.factory("AllPlanChainService", function ($resource) {
  return $resource("/json/allPlanChains", {}, {});
});

services.factory("PlanChainTypesService", function ($resource) {
  return $resource("/json/planChainTypes", {}, {});
});

services.factory("PlanChain", function ($resource) {
  return $resource(
    "/json/plan-chain",
    {},
    {
      getByTypeAndSection: {
        method: "GET",
        url:
          "/json/plan-chain/by-type/:planChainTypeId/by-section/:mtefSectionId",
        params: {
          mtefSectionId: "@mtefSectionId",
          planChainTypeId: "@planChainTypeId",
        },
      },
      getChildrenBySection: {
        method: "GET",
        url: "/json/plan-chain/by-parent/:parentId/by-section/:mtefSectionId",
        params: {
          mtefSectionId: "@mtefSectionId",
          parentId: "@parentId",
        },
      },
    }
  );
});

services.factory("FundSourceCategoriesService", function ($resource) {
  return $resource("/json/fundSourceCategories", {}, {});
});

services.factory("RolesService", function ($resource) {
  return $resource(
    "/json/roles",
    {},
    {
      getByUser: {
        url: "/json/roleByUser",
        method: "GET",
        isArray: true,
      },
    }
  );
});

services.factory("AllProjectionService", function ($resource) {
  return $resource("/json/projections", {}, {});
});

services.factory("AllFinancialYearService", function ($resource) {
  return $resource("/json/allFinancialYears", {}, {});
});

services.factory("AllFundSourceService", function ($resource) {
  return $resource("/json/allFundSources", {}, {});
});

services.factory("SectorsService", function ($resource) {
  return $resource("/json/sectors", {}, {}); //This get all sectors;
});

services.factory("ReferenceDocumentTypesService", function ($resource) {
  return $resource("/json/referenceDocumentTypes", {}, {}); //This gets all reference documents as an array
});
services.factory("UnUsedReferenceDocumentTypesService", function ($resource) {
  return $resource(
    "/json/unusedReferenceDocumentTypes/:isNationalGuideline",
    {
      isNationalGuideline: "@isNationalGuideline",
    },
    {}
  ); //This gets all reference documents as an array
});

services.factory("AllSectionService", function ($resource) {
  return $resource("/json/allSections", {}, {});
});

services.factory("AllGenericTypeService", function ($resource) {
  return $resource("/json/referenceTypes", {}, {});
});
services.factory("ReferenceTypeWithReferencesService", function ($resource) {
  return $resource(
    "/json/referenceTypesReference/:linkLevel",
    {
      linkLevel: "@linkLevel",
    },
    {}
  );
});

services.factory("AllProjectionService", function ($resource) {
  return $resource("/json/projections", {}, {});
});

services.factory("AllAdminHierarchyService", function ($resource) {
  return $resource("/json/allAdminHierarchies", {}, {});
});

services.factory("AllProjectionPeriodService", function ($resource) {
  return $resource(
    "/json/allProjectionPeriods/:projection_id",
    {
      projection_id: "@projection_id",
    },
    {
      periods: {
        method: "GET",
      },
    }
  );
});

services.factory("AllProjectionBudgetClassService", function ($resource) {
  return $resource(
    "/json/allProjectionBudgetClasses/:projection_id",
    {
      projection_id: "@projection_id",
    },
    {
      breakdown: {
        method: "GET",
      },
    }
  );
});

services.factory("adminHierarchySectMappingService", function ($resource) {
  return $resource("/json/adminHierarchySectMapping", {}, {});
});

services.factory("LongTermTargetService", function ($resource) {
  return $resource("/json/longTermTargets", {}, {}); //This get all sectors;
});

services.factory("MtefAnnualTargetsService", function ($resource) {
  return $resource("/json/mtefAnnualTargets", {}, {});
});
services.factory("UserAnnualTargetsService", function ($resource) {
  return $resource(
    "/json/annualTargetByUser/:mtefId/:mtefSectionId",
    {
      $mtefId: "@mtefId",
      mtefSectionId: "@mtefSectionId",
    },
    {}
  );
});

//The project data form services start here
services.factory("ProjectDataFormsService", function ($resource) {
  return $resource(
    "/json/ProjectDataForms",
    {},
    {
      index: {
        method: "GET",
      },
    }
  );
});
services.factory("CreateProjectDataFormsService", function ($resource) {
  return $resource(
    "/json/createProjectDataForm",
    {},
    {
      store: {
        method: "POST",
      },
    }
  );
});
services.factory("UpdateProjectDataFormsService", function ($resource) {
  return $resource(
    "/json/UpdateProjectDataForms",
    {},
    {
      update: {
        method: "POST",
      },
    }
  );
});
services.factory("DeleteProjectDataFormsService", function ($resource) {
  return $resource(
    "/json/DeleteProjectDataForms/:id",
    {
      id: "@id",
    },
    {
      delete: {
        method: "DELETE",
      },
    }
  );
});
services.factory("AllProjectDataFormContentsService", function ($resource) {
  return $resource(
    "/json/allProjectDataFormContents/:form_id",
    {
      form_id: "@form_id",
    },
    {
      index: {
        method: "GET",
      },
    }
  );
});

//The project data form services start here
services.factory("ProjectDataFormQuestionService", function ($resource) {
  return $resource(
    "/json/projectDataFormQuestions",
    {},
    {
      index: {
        method: "GET",
      },
    }
  );
});

services.factory("ListFilledProjectDataFormContentsService", function (
  $resource
) {
  return $resource(
    "/json/ListFormContents/:form_id",
    {
      form_id: "@form_id",
    },
    {
      listFormContents: {
        method: "GET",
      },
    }
  );
});

services.factory("LoadQuestionValuesService", function ($resource) {
  return $resource(
    "/json/GetQuestionValues/:project_id/:content_id",
    {
      project_id: "@project_id",
      content_id: "@content_id",
    },
    {
      getQuestionValues: {
        method: "GET",
      },
    }
  );
});

services.factory("GetProjectDataFormQuestionsService", function ($resource) {
  return $resource(
    "/json/GetProjectDataFormQuestions/:content_id",
    {
      content_id: "@content_id",
    },
    {
      getQuestionsAndValues: {
        method: "GET",
      },
    }
  );
});

// services.factory('GetAllProjectDataFormsService', function ($resource) {
//     return $resource('/json/projectDataFormQuestions', {}, {fetchAll: {method: 'GET'}});
// });
// services.factory('UpdateProjectDataFormsService', function ($resource) {
//     return $resource('/json/UpdateProjectDataForms', {}, {update: {method: 'POST'}});
// });
// services.factory('DeleteProjectDataFormsService', function ($resource) {
//     return $resource('/json/projectDataFormQuestionByContent/:id', {id: '@id'}, {getQuestionByContent: {method: 'GET'}});
// });

//Project data form question service ends here

services.factory("ProjectDataFormQuestionValuesService", [
  "$resource",
  function ($resource) {
    return $resource(
      "/json/saveProjectDataFormQuestionValues",
      {},
      {
        getQuestionValues: {
          method: "GET",
          url: "/json/getProjectDataFormQuestionValues/:id",
        },
        saveData: {
          method: "POST",
          url: "/json/saveProjectDataFormQuestionValues",
        },
        getProjects: {
          method: "GET",
          url: "/json/getUsedProjects/:formId",
          params: {
            formId: "@formId",
          },
        },
        // loadProjectDataFormLineItemValues: {method: 'POST', url: '/json/loadProjectDataFormLineItemValues'},
      }
    );
  },
]);
services.factory("ReallocationWithinVoteService", [
  "$resource",
  function ($resource) {
    return $resource(
      "/json/reallocation-within-vote",
      {},
      {
        getVotes: {
          method: "GET",
          url: "/json/reallocation-within-vote/get-votes",
        },
        getReallocation: {
          method: "GET",
          url: "/json/reallocation-within-vote/get-reallocation-btn-pisc",
        },
        getReallocationItems: {
          method: "GET",
          url: "/json/reallocation-within-vote/get-reallocation-items",
        },
        deleteItem: {
          method: "GET",
          url: "/json/reallocation-within-vote/delete-item",
        },
        getPiscs: {
          method: "GET",
          url: "/json/reallocation-within-vote/get-piscs",
        },
        getFundSources: {
          method: "GET",
          url: "/json/reallocation-within-vote/get-fund-sources",
        },
        getSubBudgetClasses: {
          method: "GET",
          url: "/json/reallocation-within-vote/get-sub-budget-classes",
        },
        getActivities: {
          method: "GET",
          url: "/json/reallocation-within-vote/get-activities",
        },
        approveReallocation: {
          method: "GET",
          url: "/json/reallocation-within-vote/approve",
        },
        saveAll: {
          method: "POST",
          url: "/json/reallocation-within-vote",
        },
      }
    );
  },
]);

//The project data form services end here
services.factory("UserCanTargetSectionsService", function ($resource) {
  return $resource("/json/userCanTargetSections", {}, {});
});

services.factory("AllSectorService", function ($resource) {
  return $resource("/json/allSectors", {}, {});
});

services.factory("AllPiscsService", function ($resource) {
  return $resource("/json/adminHierarchy/piscs", {}, {});
});

services.factory("AllBodListService", function ($resource) {
  return $resource("/json/bodLists/all", {}, {});
});

services.factory("AllLongTermTargetService", function ($resource) {
  return $resource("/json/allLongTermTargets", {}, {});
});

services.factory("ReferenceDocumentService", function ($resource) {
  return $resource("/json/referenceDocuments", {}, {}); //This get all sectors;
});

services.factory("PlanChainService", function ($resource) {
  return $resource("/json/planChains", { type: "@type" }, {}); //This get all sectors;
});

services.factory("AllDecisionLevelService", function ($resource) {
  return $resource("/json/allDecisionLevels", {}, {});
});

services.factory("TargetTypeService", function ($resource) {
  return $resource("/json/targetTypes", {}, {}); //This get all sectors;
});

services.factory("CeilingService", function ($resource) {
  return $resource("/json/ceilings", {}, {});
});

services.factory("PaginateSectionService", function ($resource) {
  return $resource("/json/PaginateSections", {}, {});
});

services.factory("AllMTEFSectionService", function ($resource) {
  return $resource("/json/allMTEFSections", {}, {});
});

services.factory("selectedPlan", [
  "$q",
  "Activity",
  function ($q, Activity) {
    var _selectedPlan;
    var deferred = $q.defer();
    return {
      get: function (mtefSectionId) {
        if (
          _selectedPlan !== undefined &&
          _selectedPlan.plan.id === parseInt(mtefSectionId, 10)
        ) {
          deferred.resolve(_selectedPlan);
        } else {
          deferred = $q.defer();
          Activity.getPlan(
            {
              mtefSectionId: mtefSectionId,
            },
            function (data) {
              deferred.resolve(data);
            },
            function (error) {
              deferred.reject(error);
            }
          );
        }
        return deferred.promise;
      },
      set: function (plan) {
        _selectedPlan = plan;
      },
    };
  },
]);

services.factory("selectedActivityFacilityFundSource", [
  "$q",
  "$timeout",
  "ActivityFacilityFundSource",
  function ($q, $timeout, ActivityFacilityFundSource) {
    var _selectedActivityFacilityFundSource;
    var deferred = $q.defer();
    return {
      get: function (id) {
        if (
          _selectedActivityFacilityFundSource !== undefined &&
          _selectedActivityFacilityFundSource.id === parseInt(id, 10)
        ) {
          deferred.resolve(_selectedActivityFacilityFundSource);
        } else {
          deferred = $q.defer();
          $timeout(function () {
            ActivityFacilityFundSource.get(
              {
                id: id,
              },
              function (data) {
                deferred.resolve(data);
              },
              function (error) {
                deferred.reject(error);
              }
            );
          }, 100);
        }
        return deferred.promise;
      },
      set: function (aff) {
        _selectedActivityFacilityFundSource = aff;
      },
    };
  },
]);
services.factory("Activity", function ($resource) {
  return $resource(
    "/json/activities/:id",
    {
      id: "@id",
    },
    {
      createOrUpdate: {
        method: "POST",
        url:
          "/json/activities/:financialYearId/:budgetType/:adminHierarchyId/:sectionId",
        params: {
          financialYearId: "@financialYearId",
          budgetType: "@budgetType",
          adminHierarchyId: "@adminHierarchyId",
          sectionId: "@sectionId",
        },
      },

      getPlan: {
        method: "GET",
        url: "/json/getPlan/:mtefSectionId",
        params: {
          mtefSectionId: "@mtefSectionId",
        },
      },
      paginate: {
        method: "POST",
        url: "/json/activities/paginated/:mtefSectionId/:targetId/:budgetType",
        params: {
          mtefSectionId: "@mtefSectionId",
          targetId: "@targetId",
          budgetType: "@budgetType",
        },
      },
      getByMtefSectionAndBudgetType: {
        method: "GET",
        url:
          "/json/activities/by-mtef-section/:mtefSectionId/by-budget-type/:budgetType",
        params: {
          mtefSectionId: "@mtefSectionId",
          budgetType: "@budgetType",
        },
      },
      getFacilities: {
        method: "GET",
        url: "/json/activityFacilities/:activityId",
        params: {
          activityId: "@activityId",
        },
      },
      toggleFacilityAccount: {
        method: "PUT",
        url: "/json/activities/toggle-facility-account/:id/:isFacilityAccount",
        params: {
          id: "@id",
          isFacilityAccount: "@isFacilityAccount",
        },
      },
        disApproveBySystem: {
        method: "GET",
        url:
          "/json/activities/disapprove-by-system/:adminHierarchyId/:financialYearId/:budgetType/:financialSystem",
        params: {
          adminHierarchyId: "@adminHierarchyId",
          financialYearId: "@financialYearId",
          budgetType: "@budgetType",
          financialSystem: "@financialSystem",
        },
      },

      disApprove: {
        method: "GET",
        url: "/json/activities/disapprove/:id",
        params: {
          id: "@id",
        },
      },
      returnSentAccounts: {
        method: "GET",
        url:
          "/json/budgetExportAccounts/returnsentAccounts/:adminHierarchyId/:financialYearId/:budgetType/:financialSystem",
        params: {
          adminHierarchyId: "@adminHierarchyId",
          financialYearId: "@financialYearId",
          budgetType: "@budgetType",
          financialSystem: "@financialSystem",
        },
      },
      approve: {
        method: "GET",
        url: "/json/activities/approve/:id",
        params: {
          id: "@id",
        },
      },
      approveBySystem: {
        method: "GET",
        url:
          "/json/activities/approve-by-system/:adminHierarchyId/:financialYearId/:budgetType/:financialSystem",
        params: {
          adminHierarchyId: "@adminHierarchyId",
          financialYearId: "@financialYearId",
          budgetType: "@budgetType",
          financialSystem: "@financialSystem",
        },
      },
      getByBudgetClass: {
        method: "GET",
        url:
          "/json/activitiesBySectionAndBudgetClass/:mtefSectionId/:budgetClassId",
        params: {
          mtefSectionId: "@mtefSectionId",
          budgetClassId: "@budgetClassId",
        },
      },
      refreshCode: {
        method: "POST",
        url: "/json/refresh-activity-code/:id",
        params: {
          id: "@id",
        },
      },
    }
  );
});

services.factory("LowestPlanChainsService", function ($resource) {
  return $resource("/json/getLowestPlanChains", {}, {});
});

services.factory("AnnualTargetsService", function ($resource) {
  return $resource(
    "/json/getAnnualTargets/:mtefId",
    {
      mtefId: "@mtefId",
    },
    {}
  );
});

services.factory("SavePlanActivitiesService", function ($resource) {
  return $resource(
    "/json/savePlanActivities/:mtefSectionId",
    {
      mtefSectionId: "@mtefSectionId",
    },
    {
      store: {
        method: "POST",
      },
    }
  );
});

services.factory("PingService", function ($resource) {
  return $resource(
    "/json/ping",
    {},
    {
      ping: {
        method: "GET",
      },
    }
  );
});

services.factory("AllBudgetSubmissionFormsService", function ($resource) {
  return $resource("/json/budgetSubmissionForms", {}, {});
});

services.factory("SectionDecisionLevels", function ($resource) {
  return $resource("/json/sectionBudgetDecisionLevel", {}, {});
});

services.factory("MainBudgetDecisionLevels", function ($resource) {
  return $resource("/json/mainBudgetDecisionLevels", {}, {});
});

services.factory("TrashFundSourceCategoryService", function ($resource) {
  return $resource("/json/trashedFundSourceCategories", {}, {});
});

services.factory("AllProjectService", function ($resource) {
  return $resource("/json/projects", {}, {});
});

services.factory("AllLGALevelService", function ($resource) {
  return $resource("/json/allLGALevels", {}, {});
});

services.factory("AllPriorityAreaService", function ($resource) {
  return $resource("/json/priority_areas/all", {}, {});
});

services.factory("AllInterventionCategoryService", function ($resource) {
  return $resource("/json/intervention_categories/all", {}, {});
});

services.factory("InterventionCategoryTreeService", function ($resource) {
  return $resource(
    "/json/intervention_categories/entireTree/:id",
    {
      id: "@id",
    },
    {}
  );
});

services.factory("EntireTreeInterventionCategoryService", function ($resource) {
  return $resource("/json/intervention_categories/wholeTree", {}, {});
});

services.factory("AllInterventionService", function ($resource) {
  return $resource("/json/interventions/all", {}, {});
});

services.factory("AllSectorProblemService", function ($resource) {
  return $resource("/json/sector_problems/all", {}, {});
});

services.factory("AllMTEFSectorProblemService", function ($resource) {
  return $resource("/json/mtef_sector_problems/all", {}, {});
});

services.factory("AllMTEFService", function ($resource) {
  return $resource("/json/mtefs/all", {}, {});
});

services.factory("AllAdminHierarchyGfsCodeService", function ($resource) {
  return $resource("/json/admin_hierarchy_gfs_codes/all", {}, {});
});

services.factory("AllGfsCodeService", function ($resource) {
  return $resource("/json/gfs_codes/all", {}, {});
});

services.factory("AllSectionGfsCodeService", function ($resource) {
  return $resource("/json/section_gfs_codes/all", {}, {});
});

services.factory("sectionGfsCodes", function ($resource) {
  return $resource("/json/admin_hierarchy_gfs_codes/all", {}, {});
});

services.factory("PriorityWithInterventionAndProblems", function ($resource) {
  return $resource(
    "/json/priorityWithInterventionAndProblems/:linkLevel",
    {
      linkLevel: "@linkLevel",
    },
    {}
  );
});

services.factory("CanProjectFundSourceService", function ($resource) {
  return $resource("/json/fund_sources/can_project", {}, {});
});

services.factory("ReportProjectFundSourceService", function ($resource) {
  return $resource("/json/fund_sources/for_report", {}, {});
});

services.factory("FundSourceGfsCodeService", function ($resource) {
  return $resource('/json/fund_source/gfs_codes/:fund_source_id', {
    fund_source_id: '@fund_source_id'
  }, {
    gfs_codes: {
      method: 'GET'
    },
    projectionGfsCodes: {
      method: 'GET',
      url: '/json/fund_source/gfs_codes/:budgetType/:financialYearId/:adminHierarchyId/:sectionId/:fundSourceId',
      params: {
        budgetType: '@budgetType',
        financialYearId: '@financialYearId',
        adminHierarchyId: '@adminHierarchyId',
        sectionId: '@sectionId',
        fundSourceId: '@fundSourceId'
      }
    },
    getProjections: {
      method: 'GET',
      url: '/json/fund_source/projections/:budgetType/:financialYearId/:adminHierarchyId/:sectionId/:fundSourceId',
      params: {
        budgetType: '@budgetType',
        financialYearId: '@financialYearId',
        adminHierarchyId: '@adminHierarchyId',
        sectionId: '@sectionId',
        fundSourceId: '@fundSourceId'
      }
    },
    deleteProjection: {
      method: 'DELETE',
      url: '/json/fund_source/projections/:id',
      params: {
        id: '@id'
      }
    }
  });
});

services.factory("CanProjectGfsCodeService", function ($resource) {
  return $resource(
    "/json/fund_source/canProjectGfsCodes/:id",
    {
      id: "@id",
    },
    {}
  );
});

services.factory("FinancialYearPeriodService", function ($resource) {
  return $resource(
    "/json/financial_year/periods/:financial_year_id",
    {
      financial_year_id: "@financial_year_id",
    },
    {
      periods: {
        method: "GET",
      },
    }
  );
});
services.factory("CurrentFinancialYearPeriodService", function ($resource) {
  return $resource(
    "/json/currentFinancialYear/:financialYearId/periods",
    {
      financialYearId: "@financialYearId",
    },
    {
      getPeriodsByActivity: {
        url: "/json/periods/get-by-activity/:activityId",
        params: {
          activityId: "activityId",
        },
      },
    }
  );
});
services.factory("PlanningPeriodService", function ($resource) {
  return $resource('/json/currentFinancialYear/planningPeriods', {}, {
    byYear: {
      url: '/json/period-by-financial-year/:financialYearId',
      params: {
        financialYearId: '@financialYearId'
      }
    }
  });
});

services.factory("CreateProjectionService", function ($resource) {
  return $resource('/json/projections', {}, {
    creatOrUpdate: {
      method: 'POST',
      url: '/json/createProjection/:budgetType/:financialYearId/:adminHierarchyId/:sectionId/:fundSourceId',
      params: {
        budgetType: '@budgetType',
        financialYearId: '@financialYearId',
        adminHierarchyId: '@adminHierarchyId',
        sectionId: '@sectionId',
        fundSourceId: '@fundSourceId'
      }
    },
    init: {
      method: 'POST',
      url: '/json/initializeProjection/:budgetType/:financialYearId/:adminHierarchyId/:sectionId/:fundSourceId/:gfsCodeId',
      params: {
        budgetType: '@budgetType',
        financialYearId: '@financialYearId',
        adminHierarchyId: '@adminHierarchyId',
        sectionId: '@sectionId',
        fundSourceId: '@fundSourceId',
        gfsCodeId: '@gfsCodeId'
      }
    }
  });
});

services.factory("AllLinkLevelService", function ($resource) {
  return $resource("/json/link_levels/all", {}, {});
});

services.factory("AllTaskNatureService", function ($resource) {
  return $resource("/json/task_natures/all", {}, {});
});
services.factory("GetSectionBudgetService", function ($resource) {
  return $resource(
    "/json/getSectionBudgetService/:adminHierarchyId/:sectionId/:planType",
    {
      adminHierarchyId: "@adminHierarchyId",
      sectionId: "@sectionId",
      planType: "@planType",
    },
    {}
  );
});

services.factory("AllExpenditureCentreGroupService", function ($resource) {
  return $resource("/json/expenditure_centre_groups/all", {}, {});
});

services.factory("AllExpenditureCentreService", function ($resource) {
  return $resource("/json/expenditure_centres/all", {}, {});
});

services.factory("AllExpenditureCentreValueService", function ($resource) {
  return $resource("/json/expenditure_centre_values/all", {}, {});
});

services.factory("AllBdcMainGroupService", function ($resource) {
  return $resource("/json/bdc_main_groups/all", {}, {});
});

services.factory("AllBdcGroupService", function ($resource) {
  return $resource("/json/bdc_groups/all", {}, {});
});

services.factory("AllBdcMainGroupFundSourceService", function ($resource) {
  return $resource("/json/main_group_fund_sources/all", {}, {});
});

services.factory("AllExpenditureCentreGfsCodeService", function ($resource) {
  return $resource("/json/expenditure_centre_gfs_codes/all", {}, {});
});

services.factory("AllGroupFinancialYearValueService", function ($resource) {
  return $resource("/json/group_financial_year_values/all", {}, {});
});

services.factory("AllSectorExpenditureSubCentreService", function ($resource) {
  return $resource("/json/sectorExpenditureSubCentres/all", {}, {});
});
services.factory("PlanningFundSources", function ($resource) {
  return $resource(
    "/json/planningFundSources",
    {},
    {
      getByUser: {
        url: "/json/get-user-planning-fund-sources/:mtefSectionId/:budgetType",
        method: "GET",
        isArray: true,
        params: {
          mtefSectionId: "@mtefSectionId",
          budgetType: "@budgetType",
        },
      },
    }
  );
});
services.factory("CreateOrUpdateActivity", function ($resource) {
  return $resource(
    "/json/createOrUpdateActivity/:mtefSectionId",
    {
      mtefSectionId: "@mtefSectionId",
    },
    {
      save: {
        method: "POST",
      },
    }
  );
});
services.factory("CreateOrUpdateInputs", function ($resource) {
  return $resource(
    "/json/createOrUpdateInputs/:facilityFundSourceId/:mtefSectionId/:budgetClassId",
    {
      facilityFundSourceId: "@facilityFundSourceId",
      mtefSectionId: "@mtefSectionId",
      budgetClassId: "@budgetClassId",
    },
    {
      save: {
        method: "POST",
      },
    }
  );
});
services.factory("ToggleFacilityService", function ($resource) {
  return $resource(
    "/json/toggleFacility",
    {},
    {
      toggleFacility: {
        method: "POST",
      },
    }
  );
});
services.factory("UpdatePasswordService", function ($resource) {
  return $resource(
    "/json/updatePassword",
    {},
    {
      update: {
        method: "POST",
      },
    }
  );
});

services.factory("CasPlanService", function ($resource) {
  return $resource("/json/casPlans", {}, {});
});

services.factory("CasPlanContentsService", function ($resource) {
  return $resource(
    "/json/casPlanContents/:cas_plan_id",
    {
      cas_plan_id: "@cas_plan_id",
    },
    {}
  );
});

services.factory("CasPlanTableByContentService", function ($resource) {
  return $resource(
    "/json/casPlanTableByContent/:cas_plan_content_id",
    {
      cas_plan_content_id: "@cas_plan_table_content_id",
    },
    {
      method: "GET",
    }
  );
});

services.factory("casPlanTableIsEditableService", function ($resource) {
  return $resource(
    "/json/casPlanTableIsEditable/:casPlanId",
    {
      casPlanId: "@casPlanId",
    },
    {
      method: "GET",
    }
  );
});

services.factory("CasPlanTableColumnByTableService", function ($resource) {
  return $resource(
    "/json/CasPlanTableColumnsByTable/:cas_plan_table_id",
    {
      cas_plan_table_id: "@cas_plan_table_id",
    },
    {
      method: "GET",
    }
  );
});

services.factory("CasPlanTableItemByTableService", function ($resource) {
  return $resource(
    "/json/CasPlanTableItemByTable/:cas_plan_table_id",
    {
      cas_plan_table_id: "@cas_plan_table_id",
    },
    {
      method: "GET",
    }
  );
});

services.factory("AllGfsCodeSubCategoryService", function ($resource) {
  return $resource("/json/gfs_code_sub_categories/all", {}, {});
});

services.factory("AllImportMethodService", function ($resource) {
  return $resource("/json/import_methods/all", {}, {});
});

services.factory("AllDataSourceService", function ($resource) {
  return $resource("/json/data_sources/all", {}, {});
});

services.factory("AllReceivedFundService", function ($resource) {
  return $resource("/json/receivedFunds/all", {}, {});
});

services.factory("SingleGfsCodeService", function ($resource) {
  return $resource(
    "/json/gfs_code/single/:id",
    {
      id: "@id",
    },
    {}
  );
});

services.factory("AllBudgetExportAccountService", function ($resource) {
  return $resource("/json/budgetExportAccounts/all", {}, {});
});

services.factory("AllActivityService", function ($resource) {
  return $resource("/json/activities/all", {}, {});
});

services.factory("AllActivityInputGfsCodeService", function ($resource) {
  return $resource("/json/budget-export-accounts/gfs_codes", {}, {});
});

services.factory("AllParentBudgetClassService", function ($resource) {
  return $resource("/json/budget_classes/parents", {}, {});
});

services.factory("LoadSubBudgetClassService", function ($resource) {
  return $resource(
    "/json/budget_classes/subs/:id",
    {
      id: "@id",
    },
    {}
  );
});

services.factory("AllBudgetTransactionTypeService", function ($resource) {
  return $resource("/json/budgetTransactionTypes/all", {}, {});
});

services.factory("AllBudgetExportTransactionService", function ($resource) {
  return $resource("/json/budgetExportTransactions/all", {}, {});
});

services.factory("AllSystemService", function ($resource) {
  return $resource("/json/systems/all", {}, {});
});

services.factory("AllSectorExpenditureSubCentreGroupService", function (
  $resource
) {
  return $resource("/json/sectorExpenditureSubCentreGroups/all", {}, {});
});

services.factory("AllCasAssessmentCategoryService", function ($resource) {
  return $resource("/json/casAssessmentCategories/all", {}, {});
});

services.factory("AllCASPlanService", function ($resource) {
  return $resource("/json/cas_plans/all", {}, {});
});

services.factory("AllCasAssessmentCategoryVersionService", function (
  $resource
) {
  return $resource("/json/casAssessmentCategoryVersions/all", {}, {});
});

services.factory("BudgetingFundSourceCategoryService", function ($resource) {
  return $resource("/json/fund_source_categories/budgeting", {}, {});
});

services.factory("CarryOverFundSourceCategoryService", function ($resource) {
  return $resource("/json/fund_source_categories/carry_over", {}, {});
});

services.factory("AllCasAssessmentStateService", function ($resource) {
  return $resource("/json/casAssessmentStates/all", {}, {});
});

services.factory("AllCASAssessmentSubCriteriaByCriteriaService", function (
  $resource
) {
  return $resource(
    "/json/casAssessmentSubCriteriaByCriteria/all/:criteria_id",
    {
      criteria_id: "@criteria_id",
    },
    {}
  );
});

services.factory("AllCASAssessmentCriteriaByCategoryService", function (
  $resource
) {
  return $resource(
    "/json/casAssessmentCriteriaByCategory/all/:category_id",
    {
      category_id: "@category_id",
    },
    {}
  );
});

services.factory("FetchAllCasAssessmentSubCriteriaOptionService", function (
  $resource
) {
  return $resource("/json/casAssessmentSubCriteriaOptions/fetchAll", {}, {});
});

services.factory("RemoveCeilingSector", function ($resource) {
  return $resource(
    "/json/removeCeilingSector/:ceilingId/:sectorId",
    {
      ceilingId: "@ceilingId",
      sectorId: "@sectorId",
    },
    {
      save: {
        method: "POST",
      },
    }
  );
});

services.factory("AddCeilingSector", function ($resource) {
  return $resource(
    "/json/addCeilingSector/:ceilingId/:sectorId",
    {
      ceilingId: "@ceilingId",
      sectorId: "@sectorId",
    },
    {
      save: {
        method: "POST",
      },
      toggleExtendsToFacility: {
        method: "POST",
        url: "/json/toggleExtendsToFacility/:ceilingId/:extendsToFacility",
        params: {
          ceilingId: "@ceilingId",
          extendsToFacility: "@extendsToFacility",
        },
      },
    }
  );
});
services.factory("SaveAllocationCeilingsService", function ($resource) {
  return $resource(
    "/json/saveAllocationCeilings",
    {},
    {
      save: {
        method: "POST",
      },
    }
  );
});
services.factory("AllocationCeilingsService", function ($resource) {
  return $resource(
    "/json/getAllocationCeilings/:budgetType/:financialYearId/:adminHierarchyId/:sectionId/:fundSourceId",
    {
      fundSourceId: "@fundSourceId",
      budgetType: "@budgetType",
      financialYearId: "@financialYearId",
      adminHierarchyId: "@adminHierarchyId",
      sectionId: "@sectionId",
    },
    {}
  );
});

services.factory("AllCasAssessmentCategoryVersionStateService", function (
  $resource
) {
  return $resource("/json/casAssessmentCategoryVersionStates/all", {}, {});
});

services.factory("AllFundTypeService", function ($resource) {
  return $resource("/json/fundTypes/all", {}, {});
});

services.factory("AllCeilingChainService", function ($resource) {
  return $resource("/json/ceilingChains/all", {}, {});
});

services.factory("AllSectionLevelService", function ($resource) {
  return $resource("/json/sectionLevels", {}, {});
});

services.factory("AllCasAssessmentCriteriaOptionService", function ($resource) {
  return $resource("/json/casAssessmentCriteriaOptions/all", {}, {});
});

services.factory("AllCasAssessmentSubCriteriaOptionService", function (
  $resource
) {
  return $resource("/json/casAssessmentSubCriteriaOptions/all", {}, {});
});

services.factory("AllCalendarEventService", function ($resource) {
  return $resource("/json/calendarEvents/all", {}, {});
});

services.factory("AllCalendarService", function ($resource) {
  return $resource("/json/calendars/all", {}, {});
});

services.factory("AllBodVersion", function ($resource) {
  return $resource("/json/bodVersions/all", {}, {});
});

services.factory("AllBodInterventionService", function ($resource) {
  return $resource("/json/bodInterventions/all", {}, {});
});

services.factory("AllCalenderEventReminderRecipientService", function (
  $resource
) {
  return $resource("/json/eventRecipients/all", {}, {});
});

services.factory("AllNotificationService", function ($resource) {
  return $resource("/json/notifications/all", {}, {});
});

services.factory("AllFundSourceCategoryService", function ($resource) {
  return $resource("/json/fund_source_categories/all", {}, {});
});

services.factory("FundSourceCategoryTreeService", function ($resource) {
  return $resource(
    "/json/fund_source_categories/entireTree/:id",
    {
      id: "@id",
    },
    {}
  );
});

services.factory("EntireTreeFundSourceCategoryService", function ($resource) {
  return $resource("/json/fund_source_categories/wholeTree", {}, {});
});

services.factory("ExpenditureGfsCodeService", function ($resource) {
  return $resource("/json/gfs_codes/expenditure", {}, {});
});

services.factory("RevenueGfsCodeService", function ($resource) {
  return $resource("/json/gfs_codes/revenue", {}, {});
});

services.factory("BaselineStatisticsService", function ($resource) {
  return $resource(
    "/json/getBaselineStatistics",
    {},
    {
      index: {
        method: "GET",
      },
    }
  );
});
services.factory("ApproveMtefService", function ($resource) {
  return $resource(
    "/json/approveMtef/:adminHierarchyId",
    {
      adminHierarchyId: "@adminHierarchyId",
    },
    {
      approve: {
        method: "POST",
      },
    }
  );
});

services.factory("CasPlanTableDetailsService", function ($resource) {
  return $resource(
    "/json/casPlanTableDetails",
    {
      cas_plan_table_id: "@cas_plan_table_id",
      facility: "@facility",
      admin_hierarchy_id: "@admin_hierarchy_id",
      period_id: "@period_id",
      financial_year_id: "@financial_year_id",
    },
    {
      getCasPlanTableDetails: {
        method: "GET",
      },
    }
  );
});

services.factory("AllPeriodGroupService", function ($resource) {
  return $resource("/json/periodGroups/all", {}, {});
});

services.factory("LoadReportParametersSources", function ($resource) {
  return $resource(
    "/json/LoadReportParameters",
    {},
    {
      getParams: {
        method: "POST",
      },
    }
  );
});

services.factory("GetCasPlanContentByIdService", function ($resource) {
  return $resource(
    "/json/getCasContentById/:cas_plan_content_id",
    {
      cas_plan_content_id: "@cas_plan_content_id",
    },
    {
      getCasContentById: {
        method: "GET",
      },
    }
  );
});

services.factory("AllReportService", function ($resource) {
  return $resource("/json/reports/all", {}, {});
});

services.factory("LevelAdminHierarchyService", function ($resource) {
  return $resource("/json/admin_hierarchy_levels/admin_hierarchies", {}, {});
});

services.factory("RegionByLevelService", function ($resource) {
  return $resource("/json/admin_hierarchy_levels/regions_by_level", {}, {});
});

services.factory("SectorSectionService", function ($resource) {
  return $resource(
    "/json/sectors/:sector_id/planning_units",
    {
      sector_id: "@sector_id",
    },
    {
      planning_units: {
        method: "GET",
      },
    }
  );
});

services.factory("GetAllFinancialYearService", function ($resource) {
  return $resource("/json/paginatedFinancialYears", {}, {});
});

services.factory("CasPlanTableSelectOptionItemsService", function ($resource) {
  return $resource(
    "/json/casPlanTableOptionItems/:group_id",
    {
      group_id: "@group_id",
    },
    {}
  );
});

services.factory("CasPlanTableSelectFacilityListService", function ($resource) {
  return $resource("/json/casPlanTableSelectFacilityList", {}, {});
});

services.factory("CasPlanTableSelectFacilityByCouncilService", function (
  $resource
) {
  return $resource(
    "/json/casPlanTableSelectFacilityByCouncil/:admin_hierarchy",
    {
      admin_hierarchy: "@admin_hierarchy",
    },
    {}
  );
});

services.factory("AllUserProfileKeyService", function ($resource) {
  return $resource("/json/userProfileKeys/all", {}, {});
});

services.factory("BudgetSubmissionSubFormService", function ($resource) {
  return $resource("/json/budgetSubmissionSubForms", {}, {});
});

services.factory("ActivityService", function ($resource) {
  return $resource("/json/activities", {}, {});
});

services.factory("GetAllFacilityTypeService", function ($resource) {
  return $resource("/json/getAllFacilityTypes", {}, {});
});

services.factory("ActivityImplementationService", function ($resource) {
  return $resource(
    "/json/activityImplementation/:id",
    {},
    {
      query: {
        method: "GET",
        isArray: false,
      },
      paginated: {
        method: "GET",
        url: "/json/activityImplementation/paginated",
      },
      sectionBudgetClassesWithActivity: {
        method: "GET",
        url: "/json/activityImplementation/budgetClasses",
      },
      costCentreFundSources: {
        method: "GET",
        url: "/json/activityImplementation/fundSources",
      },
      periods: {
        method: "GET",
        url: "/json/periods/executionPeriods",
      },
      facilities: {
        method: "GET",
        url: "/json/activityImplementation/facilities",
      },
      facilityTypes: {
        method: "GET",
        url: "/json/activityImplementation/facility-types",
      },
      paginatedPlan: {
        method: "GET",
        url: "/json/activityImplementation/paginatedPlan",
      },
      byActivity: {
        method: "GET",
        url: "/json/activityImplementation/implementation-byActivity",
      },

      facilityActivityBudgetExpenditure: {
        method: "GET",
        url: "/json/activityImplementation/activityFacilityBudgetExpenditure",
      },
      activityImplementationHistory: {
        method: "GET",
        url: "/json/activityImplementation/implementationHistory",
      },

      update: {
        method: "PUT",
      },

      trashed: {
        method: "GET",
        url: "/json/ActivityImplementation/trashed",
      },
      restore: {
        method: "GET",
        url: "/json/ActivityImplementation/:id/restore",
      },
      permanentDelete: {
        method: "GET",
        url: "/json/ActivityImplementation/:id/permanentDelete",
      },
      emptyTrash: {
        method: "GET",
        url: "/json/ActivityImplementation/emptyTrash",
      },
      removeFile: {
        method: "PUT",
        params: {
          file_url: "@file_url",
        },
        url: "/json/activityImplementation/document-remove",
      },
    }
  );
});

services.factory("ActivityService", function ($resource) {
  return $resource(
    "/json/activities/:id",
    {},
    {
      query: {
        method: "GET",
        isArray: true,
      },
      paginated: {
        method: "GET",
        url: "/json/Activity/paginated",
      },
      update: {
        method: "PUT",
        params: {
          id: "@id",
        },
      },
      trashed: {
        method: "GET",
        url: "/json/Activity/trashed",
      },
      restore: {
        method: "GET",
        url: "/json/Activity/:id/restore",
      },
      permanentDelete: {
        method: "GET",
        url: "/json/Activity/:id/permanentDelete",
      },
      emptyTrash: {
        method: "GET",
        url: "/json/Activity/emptyTrash",
      },
    }
  );
});

services.factory("StatusService", function ($resource) {
  return $resource(
    "/json/activityStatuses/:id",
    {},
    {
      query: {
        method: "GET",
        isArray: true,
      },
      paginated: {
        method: "GET",
        url: "/json/activityStatuses/paginated",
      },
      update: {
        method: "PUT",
        params: {
          id: "@id",
        },
      },
      trashed: {
        method: "GET",
        url: "/json/activityStatuses/trashed",
      },
      restore: {
        method: "GET",
        url: "/json/activityStatuses/:id/restore",
      },
      permanentDelete: {
        method: "GET",
        url: "/json/activityStatuses/:id/permanentDelete",
      },
      emptyTrash: {
        method: "GET",
        url: "/json/activityStatuses/emptyTrash",
      },
    }
  );
});

services.factory("ForwardMtefSectionService", function ($resource) {
  return $resource(
    "/json/forwardMtefSectionService",
    {},
    {
      forward: { method: "POST" },
      setAddressed: {
        method: "POST",
        url: "/json/scrutinization/setAddressed/:itemType/:itemId",
        params: { itemType: "@itemType", itemId: "@itemId" },
      },
    }
  );
});

services.factory("ReturnMtefSectionService", function ($resource) {
  return $resource(
    "/json/returnMtefSectionService",
    {},
    { return: { method: "POST" } }
  );
});
services.factory("ForwardAllMtefSectionByUserService", function ($resource) {
  return $resource(
    "/json/forwardAllMtefSectionByUser",
    {},
    { forward: { method: "POST" } }
  );
});
services.factory("ForwardMtefByUserService", function ($resource) {
  return $resource(
    "/json/forwardMtefByUser",
    {},
    { forward: { method: "POST" } }
  );
});
services.factory("ReturnMtefByUserService", function ($resource) {
  return $resource(
    "/json/returnMtefService",
    {},
    { return: { method: "POST" } }
  );
});
services.factory("LoadCurrentMtefCommentsService", function ($resource) {
  return $resource(
    "/json/loadCurrentMtefComments/:adminHierarchyId/:decisionLevelId/",
    {},
    {}
  );
});

services.factory("AllActivityStatusService", function ($resource) {
  return $resource("/json/activityStatuses/all", {}, {});
});

services.factory("AllBudgetReallocationService", function ($resource) {
  return $resource("/json/budgetReallocations/all", {}, {});
});

services.factory("AllUserService", function ($resource) {
  return $resource("/json/users/all", {}, {});
});

services.factory("AllCASAssessmentRoundService", function ($resource) {
  return $resource("/json/assessment_rounds/all", {}, {});
});

services.factory("AllRegionService", function ($resource) {
  return $resource("/json/regions/all", {}, {});
});

services.factory("FacilitiesByHierarchyAndSection", function ($resource) {
  return $resource("/json/facilitiesByHierarchyAndSection", {}, {});
});
services.factory("PriorityArea", function ($resource) {
  return $resource(
    "/json/priority-areas",
    {},
    {
      withInterventionAndProblems: {
        url:
          "/json/priority_areas-with-intervations/by-link-level/:linkLevel/by-plan-chain/:planChainId/:financialYearId/by-admin-hierarchy/:adminHierarchyId/by-section/:sectionId",
        method: "GET",
        params: {
          linkLevel: "@linkLevel",
          planChainId: "@planChainId",
          financialYearId: "@financialYearId",
          adminHierarchyId: "@adminHierarchyId",
          sectionId: "@sectionId",
        },
      },
      getCurrentVersion: {
        url: "/json/priority-areas/get-current-version",
      },
    }
  );
});
services.factory("PerformanceIndicators", [
  "$resource",
  function ($resource) {
    return $resource(
      "/json/performanceIndicators/:id",
      {},
      {
        getByTarget: {
          method: "GET",
          url: "/json/performanceIndicators/getByTarget/:targetId",
          params: {
            targetId: "@targetId",
          },
        },
        getByPlanChain: {
          method: "GET",
          url: "/json/performanceIndicators/getByPlanChain/:planChainId",
          params: {
            planChainId: "@planChainId",
          },
        },
        getForUser: {
          method: "GET",
          url: "/json/performanceIndicators/getForUser",
        },
        saveValues: {
          method: "POST",
          url: "/json/performanceIndicators/saveValues",
        },
        saveActualValues: {
          method: "POST",
          url: "/json/performanceIndicators/saveActualValues",
        },
      }
    );
  },
]);

services.factory("GlobalService", function ($resource) {
  return $resource(
    "/json/users/:id",
    {},
    {
      regions: {
        method: "GET",
        url: "/json/admin_hierarchies/regions",
      },
      getPicsParentName: {
        method: "GET",
        url: "/json/admin_hierarchies/get-pics-name-from-sp",
      },
      financialYearPeriods: {
        method: "GET",
        url: "/json/periods/getByFinancialYearId",
      },
      cdr: {
        method: "GET",
        url: "/json/reports/cdr",
      },
      consolidatedCdr: {
        method: "GET",
        url: "/json/reports/cdrConsolidated",
      },
      oprasSetups: {
        method: "GET",
        url: "/json/opras/oprasSetups",
      },
      lgas: {
        method: "GET",
        url: "/json/admin_hierarchies/:region_id/lgas",
      },
      admin_hierarchies: {
        method: "GET",
        url: "/json/admin_hierarchies/admin_hierarchies",
      },
      procurementMethods: {
        method: "GET",
        url: "/api/procurementMethods",
      },
      advertisements: {
        method: "GET",
        url: "/api/advertisements",
      },
      executionYearPeriods: {
        method: "GET",
        url: "/json/periods/executionPeriods",
      },
      activityFacilityProjectOut: {
        method: "GET",
        url: "/api/activityFacilityProjectOut",
      },
      activityTaskNatures: {
        method: "GET",
        url: "/json/activityTaskNatures/all",
      },
      taskNatures: {
        method: "GET",
        url: "/json/activityTaskNatures",
      },
      performanceIndicators: {
        method: "GET",
        url: "/json/performanceIndicators/all",
      },
      performanceIndicatorsByPlanChainId: {
        method: "GET",
        url: "/json/performanceIndicators/getByPlanChainId",
      },
      baseline_values: {
        method: "GET",
        url: "/json/performanceIndicatorBaselineValues/all",
      },
      performanceIndicatorBaselineValues: {
        method: "GET",
        url: "/json/performanceIndicatorBaselineValues/all",
      },
      nextFinancialYears: {
        method: "GET",
        url: "/json/financialYears/nextFinancialYears",
      },
      projectionFinancialYears: {
        method: "GET",
        url:
          "/json/financialYears/:start_financial_year_id/:end_financial_year_id/projectionFinancialYears",
      },
      facilityOwnerships: {
        method: "GET",
        url: "/json/facilityOwnerships/all",
      },
      facilityPhysicalStates: {
        method: "GET",
        url: "/json/facilityPhysicalStates/all",
      },
      facilityStarRatings: {
        method: "GET",
        url: "/json/facilityStarRatings/all",
      },
      facilityCustomDetails: {
        method: "GET",
        url: "/json/facilityCustomDetails/all",
      },
      myFacilityCustomDetails: {
        method: "GET",
        url: "/json/facilities/myFacilityCustomDetails",
      },
      facilityCustomDetailMappings: {
        method: "GET",
        url: "/json/facilityCustomDetailMappings/all",
      },
      facilityTypes: {
        method: "GET",
        url: "/json/facilityTypes/fetchAll",
      },
      similarFacilities: {
        method: "GET",
        url: "/json/facilities/similarFacilities",
      },
      allFacilities: {
        method: "GET",
        url: "/json/facilities/allFacilities",
      },
      loadFacilityCustomDetails: {
        method: "GET",
        url: "/json/facilityCustomDetailMappings/loadFacilityCustomDetails",
      },
      bodInterventions: {
        method: "GET",
        url: "/json/bodVersions/bodInterventions",
      },
      toDateFinancialYears: {
        method: "GET",
        url: "/json/financialYears/toDateFinancialYears",
      },
      facilityTypeAdminHierarchies: {
        method: "GET",
        url: "/json/facilityTypes/facilityTypeAdminHierarchies",
      },
      globalAdminHierarchies: {
        method: "GET",
        url: "/json/adminHierarchies/globalAdminHierarchies",
      },
      wards: {
        method: "GET",
        url: "/json/adminHierarchies/wards",
      },
      villages: {
        method: "GET",
        url: "/json/adminHierarchies/villages",
      },
      wardVillages: {
        method: "GET",
        url: "/json/adminHierarchies/wardVillages",
      },
      referenceDocuments: {
        method: "GET",
        url: "/json/reference-documents/fetchAll",
      },
      sectors: {
        method: "GET",
        url: "/json/sectors/fetchAll",
      },
      pscBySector: {
        method: "GET",
        url: "/json/sectorsPsc/fetch",
        params: {
          sectorId: "sectorId",
        },

      },
      sectorSections: {
        method: "GET",
        url: "/json/sectors/sections",
      },
      filterSectorSections: {
        method: "GET",
        url: "/json/sectors/filterSections",
      },
      nationalTargets: {
        method: "GET",
        url: "/json/nationalTargets/all",
      },
      priorityAreas: {
        method: "GET",
        url: "/json/priorityAreas/fetchAll",
      },
      planningMatrices: {
        method: "GET",
        url: "/json/planningMatrices/all",
      },
      genericSectorProblems: {
        method: "GET",
        url: "/json/genericSectorProblems/all",
      },
      genericSectorProblemsByPriority: {
        method: "GET",
        url: "/json/genericSectorProblems/getByPriorityArea/:priorityAreaId",
        params: {
          priorityAreaId: "priorityAreaId",
        },
      },
      genericTargets: {
        method: "GET",
        url: "/json/genericTargets/all",
      },
      planChains: {
        method: "GET",
        url: "/json/planChains/fetchAll",
      },
      planChainPerformanceIndicators: {
        method: "GET",
        url: "/json/planChains/performanceIndicators",
      },
      genericTargetPerformanceIndicators: {
        method: "GET",
        url: "/json/genericTargets/performanceIndicators",
      },
      removePerformanceIndicator: {
        method: "GET",
        url: "/json/genericTargets/removePerformanceIndicator",
      },
      addPerformanceIndicator: {
        method: "POST",
        url: "/json/genericTargets/addPerformanceIndicator",
      },
      genericActivities: {
        method: "POST",
        url: "/json/genericActivities/all",
      },
      interventions: {
        method: "GET",
        url: "/json/interventions/fetchAll",
      },
      budgetClasses: {
        method: "GET",
        url: "/json/budgetClasses/fetchAll",
      },
      sectionBudgetClassesWithActivity: {
        method: "GET",
        url: "/json/execution/pre-execution/sectionBudgetClassesWithActivity",
      },
      nationalReferences: {
        method: "GET",
        url: "/json/nationalReferences/fetchAll",
      },
      costCentres: {
        method: "GET",
        url: "/json/sections/fetchAll",
      },
      genericTargetCostCentres: {
        method: "GET",
        url: "/json/genericTargets/costCentres",
      },
      genericTargetNationalReferences: {
        method: "GET",
        url: "/json/genericTargets/nationalReferences",
      },
      addNationalReference: {
        method: "POST",
        url: "/json/genericTargets/addNationalReference",
      },
      removeNationalReference: {
        method: "GET",
        url: "/json/genericTargets/removeNationalReference",
      },
      addCostCentre: {
        method: "POST",
        url: "/json/genericTargets/addCostCentre",
      },
      removeCostCentre: {
        method: "GET",
        url: "/json/genericTargets/removeCostCentre",
      },
      serviceOutputs: {
        method: "GET",
        url: "/json/planChains/fetchAll",
      },
      serviceOutputSectors: {
        method: "GET",
        url: "/json/planChains/sectors",
      },
      fundSources: {
        method: "GET",
        url: "/json/fundSources/fetchAll",
      },
      nationalGuideLineRefDocs: {
        method: "GET",
        url: "/json/reference-documents/national-guidelines",
      },
      expenditureCentres: {
        method: "GET",
        url: "/json/expenditureCentres/fetchAll",
      },
      lgaLevels: {
        method: "GET",
        url: "/json/lgaLevels/fetchAll",
      },
      activityCategories: {
        method: "GET",
        url: "/json/activityCategories/fetchAll",
      },
      onlyRegions: {
        method: "GET",
        url: "/json/geoLocations/fetch-all-regions",
        //url: '/json/geoLocations/levelCouncils'
      },
      loadPiscs: {
        method: "GET",
        url: "/json/adminHierarchy/piscs",
      },
      loadServiceProviderType: {
        method: "GET",
        url: "/json/adminHierarchy/service-provider-type",
      },
      onlyRevenueOnes: {
        method: "GET",
        url: "/json/gfs_codes/onlyRevenueOnes",
      },
      groupExpenditureCentres: {
        method: "GET",
        url: "/json/expenditureCentreGroups/groupExpenditureCentres",
      },
      linkSpecs: {
        method: "GET",
        url: "/json/linkSpecs/fetchAll",
      },
      groupCostCentres: {
        method: "GET",
        url: "/json/expenditureCentreGroups/groupCostCentres",
      },
      loadExpenditureGroupCostCentres: {
        method: "GET",
        url: "/json/expenditureCentreGroups/loadExpenditureGroupCostCentres",
      },
      planningUnits: {
        method: "GET",
        url: "/json/sections/planningUnits",
      },
      periodGroups: {
        method: "GET",
        url: "/json/periodGroups/fetchAll",
      },
      userRegions: {
        method: "GET",
        url: "/json/adminHierarchies/userRegions",
      },
      userCouncils: {
        method: "GET",
        url: "/json/adminHierarchies/userCouncils",
      },
      assetConditions: {
        method: "GET",
        url: "/json/assetConditions/fetchAll",
      },
      assetUses: {
        method: "GET",
        url: "/json/assetUses/fetchAll",
      },
      transportFacilityTypes: {
        method: "GET",
        url: "/json/transportFacilityTypes/fetchAll",
      },
      transportFacilities: {
        method: "GET",
        url: "/json/transportFacilities/fetchAll",
      },
      sectionLevelsByUser: {
        method: "GET",
        url: "/json/sectionLevels/getLevelsByUser",
      },
      sectionLevelSections: {
        method: "GET",
        url: "/json/sectionLevels/getSections",
      },
      spnumber: {
        method: "GET",
        url: "/json/sectorProblems/spnumber",
      },
      facilityTypeSector: {
        method: "GET",
        url: "/json/facilityTypes/sector",
      },
      financialYears: {
        method: "GET",
        url: "/json/financialYears/fetchAll",
      },
      referenceDocumentTypes: {
        method: "GET",
        url: "/json/referenceDocumentTypes/fetchAll",
      },
      ceilings: {
        method: "GET",
        url: "/report/ceilings/fetchAll",
      },
      priorityInterventions: {
        method: "GET",
        url: "/json/priorityAreas/interventions",
      },
      priorityAreaGenericSectorProblems: {
        method: "GET",
        url: "/json/priorityAreas/genericSectorProblems",
      },
      sectorProblemGenericTargets: {
        method: "GET",
        url: "/json/genericSectorProblems/genericTargets",
      },
      sectorCostCentres: {
        method: "GET",
        url: "/json/sections/sectorCostCentres",
      },
      gfsCodes: {
        method: "GET",
        url: "/json/gfsCodes/fetchAll",
      },
      projects: {
        method: "GET",
        url: "/json/projects/fetchAll",
      },
      institutionEmployees: {
        method: "GET",
        url: "/json/institutionEmployees/getEmployeesDetails",
      },
      objectives: {
        method: "GET",
        url: "/json/objectives/fetchAll",
      },
      activities: {
        method: "GET",
        url: "/json/activities/fetchAll",
      },
      fundTypes: {
        method: "GET",
        url: "/json/fundTypes/fetchAll",
      },
      exportAllSelectedFacilities: {
        method: "POST",
        url: "/json/facilities/exportAllSelectedFacilities",
      },
      exportAllFacilities: {
        method: "GET",
        url: "/json/facilities/exportAllFacilities",
      },
      exportAllSelectedSubBudgetClasses: {
        method: "POST",
        url: "/json/budgetClasses/exportAllSelectedSubBudgetClasses",
      },
      exportAllSubBudgetClasses: {
        method: "GET",
        url: "/json/budgetClasses/exportAllSubBudgetClasses",
      },
      exportAllSelectedFundTypes: {
        method: "POST",
        url: "/json/fundTypes/exportAllSelectedFundTypes",
      },
      exportAllFundTypes: {
        method: "GET",
        url: "/json/fundTypes/exportAllFundTypes",
      },
      exportAllSelectedFundSources: {
        method: "POST",
        url: "/json/fundSources/exportAllSelectedFundSources",
      },
      exportAllFundSources: {
        method: "GET",
        url: "/json/fundSources/exportAllFundSources",
      },
      exportAllSelectedProjects: {
        method: "POST",
        url: "/json/projects/exportAllSelectedProjects",
      },
      exportAllProjects: {
        method: "GET",
        url: "/json/projects/exportAllProjects",
      },
      exportAllSelectedGfsCodes: {
        method: "POST",
        url: "/json/gfsCodes/exportAllSelectedGfsCodes",
      },
      exportAllGfsCodes: {
        method: "GET",
        url: "/json/gfsCodes/exportAllGfsCodes",
      },
      facilityLevelActivities: {
        method: "GET",
        url: "/json/facilities/activities",
      },
      exportActivities: {
        method: "GET",
        url: "/json/activities/export",
      },
      paginatedFacilityLevelActivities: {
        method: "GET",
        url: "/json/facilities/activities",
      },
      exportAllSelectedActivities: {
        method: "POST",
        url: "/json/activities/exportAllSelectedActivitiesToMUSE",
      },
      exportAllActivities: {
        method: "GET",
        url: "/json/activities/exportAllActivities",
      },
      exportAllObjectives: {
        method: "GET",
        url: "/json/opras/exportAllObjectives",
      },
      //OPRAS Setup
      exportAllObjectives: {
        method: "GET",
        url: "/json/objectives/exportAllObjectives",
      },
      exportAllSelectedObjectives: {
        method: "POST",
        url: "/json/opras/exportAllSelectedObjectives",
      },
      exportAllInstitutionEmployees: {
        method: "GET",
        url: "/json/institutionEmployees/exportAllInstitutionEmployees",
      },
      exportAllTargets: {
        method: "GET",
        url: "/json/opras/exportAllTargets",
      },
      exportAllSelectedTargets: {
        method: "POST",
        url: "/json/opras/exportAllSelectedTargets",
      },
      exportAllOprasActivities: {
        method: "GET",
        url: "/json/opras/exportAllOprasActivities",
      },
      exportAllSelectedOprasActivities: {
        method: "POST",
        url: "/json/opras/exportAllSelectedOprasActivities",
      },
      fetchSpecificCouncils: {
        method: "GET",
        url: "/json/geoLocations/get-council-from-regions",
        //url: '/json/geoLocations/get-council-wards'
      },
      facilityTypeCustomDetails: {
        method: "GET",
        url: "/json/facilities/facilityTypeCustomDetails",
      },
      levelUsers: {
        method: "GET",
        url: "/json/users/levelUsers",
      },
      userDetails: {
        method: "GET",
        url: "/json/userDetails/getUsersDetails",
      },
      adminHierarchyLevels: {
        method: "GET",
        url: "/json/adminHierarchyLevels/fetchAll",
      },
      geoLocationLevels: {
        method: "GET",
        url: "/json/geoLocationLevels/fetchAll",
      },
      historicalDataTables: {
        method: "GET",
        url: "/api/historicalDataTables/fetchAll",
      },
      sectorDepartments: {
        method: "GET",
        url: "/api/sectors/departments",
      },
      userSectors: {
        method: "GET",
        url: "/api/sectors/userSectors",
      },
      departmentCostCentres: {
        method: "GET",
        url: "/api/sections/costCentres",
      },
      previousFinancialYears: {
        method: "GET",
        url: "/json/financialYears/previousFinancialYears",
      },
      filterInterventionByCategory: {
        method: "GET",
        url: "/json/interventions/filterInterventionByCategory",
      },
      sectorDepartmentFundSources: {
        method: "GET",
        url: "/json/fund-sources/sectorDepartmentFundSources",
      },
      revenueFundSources: {
        method: "GET",
        url: "/json/fund-sources/revenueFundSources",
      },
      parentInterventionCategories: {
        method: "GET",
        url: "/json/intervention-categories/parents",
      },
      childrenInterventionCategories: {
        method: "GET",
        url: "/json/intervention-categories/children",
      },
      gfsCodeCostCentres: {
        method: "GET",
        url: "/api/gfsCodes/costCentres",
      },
      addGfsCodeCostCentre: {
        method: "POST",
        url: "/api/gfsCodes/addCostCentres",
      },
      removeGfsCodeCostCentre: {
        method: "GET",
        url: "/api/gfsCodes/removeCostCentre",
      },
      getUsableFinancialYears: {
        method: "GET",
        url: "/api/financialYears/getUsableFinancialYears",
      },
      allFinancialYears: {
        method: "GET",
        url: "/json/financialYears/allFinancialYears",
      },
      projectTypes: {
        method: "GET",
        url: "/json/projectTypes",
      },
      facilityHasFacility: {
        method: "GET",
        url: "/json/facilities/facilityHasFacility",
      },
      activityMilestones: {
        method: "GET",
        url: "/json/execution/pre-execution/activityMilestones",
      },
      activityProjectOutputs: {
        method: "GET",
        url: "/json/execution/pre-execution/activityProjectOutputs",
      },
      retrieveMilestone: {
        method: "GET",
        url: "/json/execution/pre-execution/retrieveMilestone",
      },
      retrieveProjectOutput: {
        method: "GET",
        url: "/json/execution/pre-execution/retrieveProjectOutput",
      },
      restoreLostData: {
        method: "GET",
        url: "/json/execution/pre-execution/restoreLostData",
      },
      ffarsActivityAdminHierarchies: {
        method: "GET",
        url: "/api/execution/ffarsActivityAdminHierarchies",
      },
      onlyCouncils:{
        method: "GET",
        url: "/api/execution/otrPisc",
      },
    }
  );
});

services.factory("CreateResponsiblePersonService", function ($resource) {
  return $resource(
    "/json/createResponsiblePerson",
    {},
    {
      store: {
        method: "POST",
      },
    }
  );
});

//comprehensive plan service
services.factory("getJasperCasReportService", function ($resource) {
  return $resource(
    "/json/getJasperCasReport",
    {},
    {
      casReport: {
        method: "POST",
      },
    }
  );
});

//facilities by sector
services.factory("GetFacilityBySectorService", function ($resource) {
  return $resource(
    "/json/facilitiesBySector/:sector_id/:council",
    {
      sector_id: "@sector_id",
      council: "@council",
    },
    {}
  );
});
//get facility count
services.factory("FacilityCountService", function ($resource) {
  return $resource(
    "/json/FacilityCount",
    {},
    {
      facilityCount: {
        method: "GET",
      },
    }
  );
});

services.factory("PreExecutionService", [
  "$resource",
  function ($resource) {
    return $resource(
      "/json/execution/pre-execution/:id",
      {},
      {
        paginated: {
          method: "GET",
          url: "/json/execution/pre-execution/paginated",
        },
        activities: {
          method: "GET",
          url: "/json/execution/pre-execution/activities",
        },
        addMileStone: {
          method: "POST",
          url: "/json/execution/pre-execution/addMileStone",
        },
        updateMainTaskData: {
          method: "POST",
          url: "/json/execution/pre-execution/updateMainTaskData",
        },
        updateMileStoneData: {
          method: "POST",
          url: "/json/execution/pre-execution/updateMileStoneData",
        },
        addProjectOutput: {
          method: "POST",
          url: "/json/execution/pre-execution/addProjectOutput",
        },
        removeMileStone: {
          method: "GET",
          url: "/json/execution/pre-execution/removeMileStone",
        },
        removeOutput: {
          method: "GET",
          url: "/json/execution/pre-execution/removeOutput",
        },
        milestones: {
          method: "GET",
          url: "/json/execution/pre-execution/milestones",
        },
        outputs: {
          method: "GET",
          url: "/json/execution/pre-execution/outputs",
        },
        periods: {
          method: "GET",
          url: "/json/execution/pre-execution/periods",
        },
        costCentreFundSources: {
          method: "GET",
          url: "/json/execution/pre-execution/costCentreFundSources",
        },
        addPhase: {
          method: "POST",
          url: "/json/execution/pre-execution/addPhase",
        },
        editProjectOutput: {
          method: "POST",
          url: "/json/execution/pre-execution/editProjectOutput",
        },
        removePhase: {
          method: "GET",
          url: "/json/execution/pre-execution/removePhase",
        },
        update: {
          method: "PUT",
          params: {
            id: "@id",
          },
        },
        expenditureCategoryByTaskNature: {
          method: "GET",
          url: "/json/expenditureCategories/expenditureCategoryByTaskNature",
        },
        projectOutputs: {
          method: "GET",
          url: "/json/expenditureCategories/projectOutputs",
        },
        projectOutputFilled: {
          method: "GET",
          url: "/json/execution/pre-execution/project-output-filled",
        },
      }
    );
  },
]);
//NO MORE UNNECESSARY SERVICES - USE THE ABOVE GlobalService and add your service as a function

services.factory("FeatureNotification", function ($resource) {
  return $resource(
    "/api/feature-notifications/:id",
    { id: "@id" },
    {
      getActive: {
        url: "/api/feature-notification/get-active",
      },
    }
  );
});
