function DashboardController($scope, DashboardStats, $timeout, SocketIO, $uibModal, $mdToast) {

    DashboardStats.getUserSection().
    then(function (data) {
        $scope.userSectionName = data;
    }, function (error) {
        $scope.userSectionNameError = "Can't Load Data";
    });

    
    /**
     * Filter
     */
    (function(){
        DashboardStats.activeFeatureNotifcation().then(function(data) {
            $scope.featureNotification = data.notifications;
            if ($scope.featureNotification.length) {
                $mdToast.show(
                    $mdToast.simple()
                    .textContent('There are new planrep updates; See bellow')
                    .position('center right')
                    .hideDelay(10000))
                  .then(function() {
                  }).catch(function() {
                  });
            }
            // console.log(data);
        });
    })();

   
    $scope.aggregation = false;

    $scope.allBarGraphs = [{
            key: "bySectorAggregate",
            name: "Procurable Against Total Budget",
            adminLevels: [1, 2, 3],
            sectionLevels: [1]
        },
        {
            key: "byFundSource",
            name: "Budget by Fund Sources",
            adminLevels: [3],
            sectionLevels: [4]
        },
        {
            key: "byBudgetClass",
            name: "Budget by Budget Classes",
            adminLevels: [3],
            sectionLevels: [4]
        },
        {
            key: "byCostCentre",
            name: "Budget by Cost Centre",
            adminLevels: [3],
            sectionLevels: [3]
        },
        {
            key: "bySector",
            name: "Budget by Sector",
            adminLevels: [4],
            sectionLevels: [1, 2]
        }
    ];

    /**
     * @description
     * If main filter changed set selected filters and load AdminHierarchyCeilings
     * @param filter {Object} {selectedFinancialYearId,selectedBudgetType,selectedAdminHierarchyId,selectedSectionId}
     */
    $scope.filterChanged = function (filter) {
        $scope.selectedFinancialYearId = filter.selectedFinancialYearId;
        $scope.selectedFinancialYearName = filter.selectedFinancialYearName;
        $scope.selectedBudgetType = filter.selectedBudgetType;
        $scope.selectedAdminHierarchyId = filter.selectedAdminHierarchyId;
        $scope.selectedAdminHierarchyName = filter.selectedAdminHierarchyName;
        $scope.selectedSection = filter.selectedSection;
        $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
        $scope.selectedAdminHierarchyLevelId = filter.selectedAdminHierarchyLevelId;
        $scope.selectedSectionLevelPosition = filter.selectedSectionLevelPosition;
        $scope.selectedSectionLevelId = filter.selectedSectionLevelId;
        $scope.selectedSectionId = filter.selectedSectionId;
        $scope.selectedSectionName = filter.selectedSectionName;
        $scope.searchQuery = filter.searchQuery;
        clearGraphs();
        $scope.loadBudget();

    };

    $scope.aggregationChanged = function () {
        clearGraphs();
        $scope.loadBudget();
    };

    var setUserLevelGraphs = function (adminLevel, sectionLevel) {
        $scope.userBarGraphs = _.filter($scope.allBarGraphs, function (graph) {
            return graph.adminLevels.indexOf(adminLevel) !== -1 && graph.sectionLevels.indexOf(sectionLevel) !== -1;
        });
        $scope.selectedBudgetGraph = $scope.userBarGraphs[0];

    };


    /**
     * user todo
     * */

    $scope.todos = {
        data: []
    };
    $scope.todosCurrentPage = 1;
    $scope.budgetCurrentPage = 1;
    $scope.loadTodos = function () {
        $scope.todosIsloading = true;
        DashboardStats.getUserTodo($scope.todosCurrentPage).then(function (data) {
            $scope.todos = data.todos;
            $scope.todosIsloading = false;
            $scope.todosCurrentPage = data.todos.current_page;
        });
    };

    $scope.loadTodos();

    $scope.loadBudget = function () {
        if ($scope.selectedAdminHierarchyId === undefined ||
            $scope.selectedSectionId === undefined ||
            $scope.selectedFinancialYearId === undefined ||
            $scope.selectedBudgetType === undefined) {
            return;
        }
        $scope.budgetBarGraph.data = [];
        $scope.budgetBarGraph.xAxisname = undefined;
        $scope.budgetBarIsLoading = true;
        setUserLevelGraphs($scope.selectedAdminHierarchyLevelPosition, $scope.selectedSectionLevelPosition);
        loadTotalProblemCeiling();
        // if(undefined !== $scope.selectedSectionId) {
        //     loadAllocationProblems($scope.selectedAdminHierarchyId, $scope.selectedSectionId);
        // }
        if ($scope.selectedBudgetGraph) {
            switch ($scope.selectedBudgetGraph.key) {
                case "byFundSource":
                    loadByFundSource();
                    break;
                case "byBudgetClass":
                    loadByBudgetClass();
                    break;
                case "byCostCentre":
                    loadByCostCentre();
                    break;
                case "bySector":
                    loadBySector();
                    break;
            }
        }
        loadCeilingAndBudget();
        loadPEBudget();
        loadTotalBudget();
        loadProcurableAgainstTotalBudget();
        loadByDecisionLevel();
        loadInComplete();
        loadAllocationProblems();

    };

    var loadCeilingAndBudget = function () {
        $scope.ceilingBudgetCompareBarGraphLoading = true;
        $scope.ceilingBudgetCompareBarGraph.dataset = [];
        $scope.ceilingBudgetCompareBarGraph.categories = [];
        
        DashboardStats.getBudgetByCeiling($scope.selectedBudgetType, $scope.selectedFinancialYearId, $scope.selectedAdminHierarchyId, $scope.selectedSectionId).
        then(function (data) {
            // console.log(data);
            setCeilingBudgetCompareBarGraph(data);
            $scope.ceilingBudgetCompareBarGraph.xAxisname = "Budget Ceiling";
        }, function (error) {
            $scope.ceilingBudgetCompareBarGraphLoading = false;
            $scope.errorMessage = error.data.errorMessage;
        });
    };

    var loadPEBudget = function () {
        $scope.peCeilingBudgetBarGraphLoading = true;
        $scope.peCeilingBudgetBarGraph.dataset = [];
        $scope.peCeilingBudgetBarGraph.categories = [];

        DashboardStats.getPEBudgetByPECeiling($scope.selectedBudgetType, $scope.selectedFinancialYearId, $scope.selectedAdminHierarchyId, $scope.selectedSectionId).
        then(function (data) {
            setPeCeilingBudgetBarGraph(data);
            $scope.peCeilingBudgetBarGraph.xAxisname = "Budget Ceiling";
        }, function (error) {
            $scope.peCeilingBudgetBarGraphLoading = false;
            $scope.errorMessage = error.data.errorMessage;
        });
    };

    var loadTotalBudget = function () {
        $scope.totalBudgetPieChartLoading = true;
        $scope.totalBudgetPieChart.data = [];

        DashboardStats.getTotalBudgetByClass($scope.selectedBudgetType, $scope.selectedFinancialYearId, $scope.selectedAdminHierarchyId, $scope.selectedSectionId).
        then(function (data) {
            // console.log(data);
            setTotalBudgetPieChart(data);
        }, function (error) {
            $scope.totalBudgetPieChartLoading = false;
            $scope.errorMessage = error.data.errorMessage;
        });
    };

    var loadProcurableAgainstTotalBudget = function () {
        $scope.procurableTotalBudgetBarGraphLoading = true;
        $scope.procurableTotalBudgetBarGraph.dataset = [];
        $scope.procurableTotalBudgetBarGraph.categories = [];

        DashboardStats.getProcurableAgainstTotalBudget($scope.selectedBudgetType, $scope.selectedFinancialYearId, $scope.selectedAdminHierarchyId, $scope.selectedSectionId).
        then(function (data) {
            // console.log(data);
            getProcurableTotalBudgetBarGraph(data);
        }, function (error) {
            $scope.procurableTotalBudgetBarGraphLoading = false;
            $scope.errorMessage = error.data.errorMessage;
        });
    };

    var loadByDecisionLevel = function() {
        $scope.byDecisionLevelLoading = true;
        DashboardStats.getPercentageByDecisionLevel($scope.selectedBudgetType, $scope.selectedFinancialYearId, $scope.selectedAdminHierarchyId, $scope.selectedSectionId).
        then(function (data) {
            $scope.byDecisionLevel = data;
            $scope.byDecisionLevelLoading = false;
        }, function (error) {
            $scope.byDecisionLevelLoading = false;
            $scope.errorMessage = error.data.errorMessage;
        });
    };

    var loadInComplete = function() {
        DashboardStats.getIncompCeilingBudget($scope.selectedBudgetType, $scope.selectedFinancialYearId, $scope.selectedAdminHierarchyId, $scope.selectedSectionId, true)
        .then(function(data){
           $scope.totalIncomplete = data.total;
        }, function(error) {
            // console.log(error);
            $scope.errorMessage = error.data.errorMessage;
        });
    };

    var loadTotalProblemCeiling = function () {
        $scope.totalProblemCeilingLoading = true;
        DashboardStats.getTotalProblemCeiling(
            $scope.selectedBudgetType, 
            $scope.selectedFinancialYearId, 
            $scope.selectedAdminHierarchyId, 
            $scope.selectedSectionId
        ).then(function (data) {
            $scope.totalProblemCeiling = data.total;
            $scope.totalProblemCeilingLoading = false;
        }, function (error) {
            $scope.totalProblemCeilingLoading = false;
            $scope.errorMessage = error.data.errorMessage;
        });
    };

    var loadAllocationProblems = function () {
        $scope.allocationProblemLoading = true;
        DashboardStats.getAllocationProblem(
            $scope.selectedBudgetType, 
            $scope.selectedFinancialYearId, 
            $scope.selectedAdminHierarchyId, 
            $scope.selectedSectionId
        ).then(function (data) {
            $scope.allocationProblem = data.data;
            $scope.allocationProblemLoading = false;
        }, function (error) {
            $scope.allocationProblemLoading = false;
        });
    };

    $scope.viewProblemCeiling = function (selectedBudgetType, selectedFinancialYearId, selectedAdminHierarchyId, selectedSectionId) {

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/dashboard/ceiling-with-problem.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, DashboardStats) {

                $scope.ceilingIsLoading = true;
                $scope.selectedBudgetType = selectedBudgetType;
                $scope.currentPage =1;
                $scope.perPage = 20;
                $scope.pageChanged = function() {
                    DashboardStats.getIncompCeilingBudgetItems(
                        selectedBudgetType, 
                        selectedFinancialYearId, 
                        selectedAdminHierarchyId, 
                        selectedSectionId,
                        $scope.currentPage,
                        $scope.perPage
                    ).then(function (data) {
                            $scope.ceilingIsLoading = false;
                            $scope.ceilings = data;
                        },
                        function (error) {
                            $scope.ceilingIsLoading = false;
                        });
                };
                $scope.pageChanged();
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (data) {},
            function () {
                // console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.viewAllocationProblem = function (allocationProblem, selectedAdminHierarchyId, sectionId, userAdminHierarchyId, userSectionId) {

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/dashboard/allocation-with-problem.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {
                $scope.allocationProblem = allocationProblem;
                $scope.selectedAdminHierarchyId = selectedAdminHierarchyId;
                $scope.userAdminHierarchyId = userAdminHierarchyId;
                $scope.userSectionId = userSectionId;
                $scope.sectionId = sectionId;
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (data) {},
            function () {
                // console.log('Modal dismissed at: ' + new Date());
            });

    };

    /**
     * Get budget by budget Class
     * */
    var loadByBudgetClass = function () {
        DashboardStats.getBudgetByBudgetClass($scope.selectedBudgetType, $scope.selectedFinancialYearId, $scope.selectedAdminHierarchyId, $scope.selectedSectionId).
        then(function (data) {
            $scope.budgetBarGraph.data = data.byBudgetClass;
            $scope.budgetBarGraph.xAxisname = "Budget by Budget Class";
            $scope.budgetBarIsLoading = false;
        }, function (error) {
            $scope.budgetBarIsLoading = false;
            $scope.errorMessage = error.data.errorMessage;
        });
    };
    /**
     * Get budget by budget Class
     * */
    var loadByFundSource = function () {
        DashboardStats.getBudgetByFundSource($scope.selectedBudgetType, $scope.selectedFinancialYearId, $scope.selectedAdminHierarchyId, $scope.selectedSectionId).
        then(function (data) {
            $scope.budgetBarGraph.data = data.byFundSource;
            $scope.budgetBarGraph.xAxisname = "Budget by Fund Sources";
            $scope.budgetBarIsLoading = false;
        }, function (error) {
            $scope.budgetBarIsLoading = false;
            $scope.errorMessage = error.data.errorMessage;
        });
    };

    /***
     * By sector
     */
    var loadBySector = function () {
        DashboardStats.getBudgetBySector($scope.selectedBudgetType, $scope.selectedFinancialYearId, $scope.selectedAdminHierarchyId, $scope.selectedSectionId).
        then(function (data) {
            $scope.budgetBarGraph.data = data.bySector;
            $scope.budgetBarGraph.xAxisname = "Budget by Sector";
            $scope.budgetBarIsLoading = false;
        });
    };

    /**
     * By Cost centre
     * */
    var loadByCostCentre = function () {
        DashboardStats.getBudgetByBudgetByCostCenter($scope.selectedBudgetType, $scope.selectedFinancialYearId, $scope.selectedAdminHierarchyId, $scope.selectedSectionId).
        then(function (data) {
            $scope.budgetBarGraph.data = data.byCostCentre;
            $scope.budgetBarGraph.xAxisname = "Budget by Sector";
            $scope.budgetBarIsLoading = false;
        });
    };

    var clearGraphs = function () {
        $timeout(function () {
            $scope.ceilingBudgetCompareBarGraph.categories = undefined;
            $scope.ceilingBudgetCompareBarGraph.dataset = undefined;
            $scope.peCeilingBudgetBarGraph.categories = undefined;
            $scope.peCeilingBudgetBarGraph.dataset = undefined;
            $scope.totalBudgetPieChart.data = undefined;
            $scope.procurableTotalBudgetBarGraph.categories = undefined;
            $scope.procurableTotalBudgetBarGraph.dataset = undefined;
        }, 100);

    };

    var setPeCeilingBudgetBarGraph = function (data) {
        var seriesWithMap = [{
          seriesname: "ceiling",
          showValues: 0,
          data: [],
          column: "ceiling"
        },
        {
          seriesname: "budget",
          showValues: 0,
          data: [],
          column: "budget"
        },
        {
          seriesname: "completion",
          parentYAxis: "S",
          renderAs: "line",
          data: [],
          column: "completion"
        }
        ];
    
        var category = [];
        $scope.peCeilingBudgetBarGraph.dataset = [];
        $scope.peCeilingBudgetBarGraph.categories = [];
        if (data.length) {
          angular.forEach(data, function (d) {
            var c = {
              "stepSkipped": false,
              "appliedSmartLabel": true
            };
            c.label = d.label;
            category.push(c);
            angular.forEach(seriesWithMap, function (s) {
              s.data.push({
                value: d[s.column]
              });
            });
          });
          $scope.peCeilingBudgetBarGraph.dataset = seriesWithMap;
        }
        $scope.peCeilingBudgetBarGraph.categories = [{
          category: category
        }];
        $scope.peCeilingBudgetBarGraphLoading = false;
    };

    var setCeilingBudgetCompareBarGraph = function (data) {
        var seriesWithMap = [{
                seriesname: "ceiling",
                "showValues": 0,
                data: [],
                column: "ceiling"
            },
            {
                seriesname: "budget",
                "showValues": 0,
                data: [],
                column: "budget"
            },
            {
                seriesname: "completion",
                "parentYAxis": "S",
                renderAs: "line",
                data: [],
                column: "completion"
            }
        ];

        var category = [];
        $scope.ceilingBudgetCompareBarGraph.dataset = [];
        $scope.ceilingBudgetCompareBarGraph.categories = [];
        if (data.length) {
            angular.forEach(data, function (d) {
                var c = {
                    "stepSkipped": false,
                    "appliedSmartLabel": true
                };
                c.label = d.label;
                category.push(c);
                angular.forEach(seriesWithMap, function (s) {
                    s.data.push({
                        value: d[s.column]
                    });
                });
            });
            $scope.ceilingBudgetCompareBarGraph.dataset = seriesWithMap;
        }
        $scope.ceilingBudgetCompareBarGraph.categories = [{
            category: category
        }];
        $scope.ceilingBudgetCompareBarGraphLoading = false;
    };

    var setTotalBudgetPieChart = function (data) {

        var totalBudget = 0;
        $scope.totalBudgetPieChart.data = [];
        if (data.length) {

            angular.forEach(data, function (d) {

                var c = {
                    label: "",
                    value: 0
                };
                c.label = d.name;
                c.value = d.budget;
                totalBudget = totalBudget + d.budget;
                $scope.totalBudgetPieChart.data.push(c);
            });
            $scope.totalBudgetPieChart.chart.caption = "TZS "+ numberWithCommas(parseInt(totalBudget).toFixed(2));
        }
        $scope.totalBudgetPieChartLoading = false;
    };

    function numberWithCommas(x) {
        x = x.toString();
        var pattern = /(-?\d+)(\d{3})/;
        while (pattern.test(x))
            x = x.replace(pattern, "$1,$2");
        return x;
    }

    var getProcurableTotalBudgetBarGraph = function (data) {
        var seriesWithMap = [{
                seriesname: "Total Budget",
                showValues: 0,
                data: [],
                column: "total_budget"
            },
            {
                seriesname: "Procurable Budget",
                showValues: 0,
                data: [],
                column: "procurable_budget"
            },
            {
                seriesname: "Ratio",
                parentYAxis: "S",
                renderAs: "line",
                data: [],
                column: "ratio"
            }
        ];

        var category = [];
        $scope.procurableTotalBudgetBarGraph.dataset = [];
        $scope.procurableTotalBudgetBarGraph.categories = [];
        if (data.length) {
            angular.forEach(data, function (d) {
                var c = {
                    "stepSkipped": false,
                    "appliedSmartLabel": true
                };
                c.label = d.label;
                category.push(c);
                angular.forEach(seriesWithMap, function (s) {
                    s.data.push({
                        value: d[s.column]
                    });
                });
            });
        }
        $scope.procurableTotalBudgetBarGraph.dataset = seriesWithMap;
        $scope.procurableTotalBudgetBarGraph.categories = [{
            category: category
        }];
        $scope.procurableTotalBudgetBarGraphLoading = false;
    };

    $scope.budgetBarGraph = {
        "chart": {
            "yAxisName": "Budget",
            "showSum": "1",
            "formatNumberScale": 0,
            "yFormatNumberScale": 0,
            "xFormatNumberScale": 0,
            "numberPrefix": "",
            "labelDisplay": "rotate",
            "slantLabels": "1",
            "theme": "fint",
            "baseFont": "Inherit",
            "exportEnabled": "1",
            "exportFormats": "PNG=Export as High Quality Image|PDF=Export as Printable|XLSX=Export Chart Data",
            "exportTargetWindow": "_self",
            "paletteColors": "#01B8AA"
        },
        "data": []
    };
    /**
     * By Celing
     * @type {{}}
     */

    $scope.ceilingBudgetCompareBarGraph = {
        "chart": {
            "pYAxisName": "Amount (Tsh)",
            "sYAxisName": "Completion %",
            "formatNumberScale": 0,
            "yFormatNumberScale": 0,
            "xFormatNumberScale": 0,
            "sNumberSuffix": "%",
            "numberPrefix": "",
            "labelDisplay": "rotate",
            "slantLabels": "1",
            "connectNullData": "1",
            "theme": "fint",
            "baseFont": "Inherit",
            "exportEnabled": "1",
            "exportFormats": "PNG=Export as High Quality Image|PDF=Export as Printable|XLSX=Export Chart Data",
            "exportTargetWindow": "_self",
        },
        "categories": [],
        "dataset": []
    };

    $scope.peCeilingBudgetBarGraph = {
        "chart": {
          "pYAxisName": "Amount (Tsh)",
          "sYAxisName": "Completion %",
          "formatNumberScale": 0,
          "yFormatNumberScale": 0,
          "xFormatNumberScale": 0,
          "sNumberSuffix": "%",
          "numberPrefix": "",
          "labelDisplay": "rotate",
          "slantLabels": "1",
          "connectNullData": "1",
          "theme": "fint",
          "baseFont": "Inherit",
          "exportEnabled": "1",
          "exportFormats": "PNG=Export as High Quality Image|PDF=Export as Printable|XLSX=Export Chart Data",
          "exportTargetWindow": "_self",
        },
        "categories": [],
        "dataset": []
    };

    $scope.totalBudgetPieChart = {
        chart: {
          caption: 0,
          subcaption: "",
          showvalues: "1",
          showpercentintooltip: "0",
          numberprefix: "TZS ",
          enablemultislicing: "1",
          theme: "candy",
          baseFont: "Inherit",
          exportEnabled: "1",
          exportFormats: "PNG=Export as High Quality Image|PDF=Export as Printable|XLSX=Export Chart Data",
          exportTargetWindow: "_self"
        },
        data: []
    };

    $scope.procurableTotalBudgetBarGraph = {
        chart: {
            pYAxisName: "Amount (Tsh)",
            sYAxisName: "Ratio %",
            formatNumberScale: 0,
            yFormatNumberScale: 0,
            xFormatNumberScale: 0,
            sNumberSuffix: "%",
            numberPrefix: "",
            labelDisplay: "rotate",
            slantLabels: "1",
            connectNullData: "1",
            theme: "fint",
            baseFont: "Inherit",
            exportEnabled: "1",
            exportFormats: "PNG=Export as High Quality Image|PDF=Export as Printable|XLSX=Export Chart Data",
            exportTargetWindow: "_self",
        },
        categories: [],
        dataset: []
    };


    $scope.plan = {};

    SocketIO.on('comprehensive_plan:newComprehensive', function (data) {
        console.log(data);
        $scope.plan = data;
    });
}

DashboardController.resolve = {

};
