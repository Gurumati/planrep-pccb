(function () {
    'use strict';

    var dashboardModels = angular.module('dashboardModels', ['ngResource']);


    dashboardModels.factory('DashboardStats', ['$resource', '$http', function ($resource, $http) {

        return {


            /**
             *
             * table with admin areas (poralg & region)
             */
            getAdminHierarchyBudgetWithChildren: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/getAdminHierarchyBudgetWithChildren/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },

            /**
             *
             * pie chart based on current user's admin hierarch id (poralg & region)
             */
            getPercentageByDecisionLevel: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/getPercentageByDecisionLevel/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },

            /**
             *
             * pie chart based on current user's admin hierarch id (poralg & region)
             *
             */
            getPercentageByStatus: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/getPercentageByStatus/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },

            /**
             *
             * pie chart based on current user's admin hierarch id (council user)
             *
             */
            getBudgetBySector: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/getBudgetBySector/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },

            /**
             *
             * pie chart based on current user's admin hierarchy id (council user)
             *
             */
            getBudgetByBudgetClass: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/getBudgetByBudgetClass/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },
            /**
             *
             * pie chart based on current user's admin hierarchy id (council user)
             *
             */
            getBudgetByFundSource: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/getBudgetByFundSource/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },

            /**
             *
             * pie chart based on current user's admin hierarch id (council user)
             *
             */
            getBudgetByBudgetByCostCenter: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/getBudgetByCostCentre/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },
            /**
             * user todolist (calendar activity)
             */
            getUserTodo: function ($currentPage) {
                return $http.get('/json/calendars/get-user-todos/?page=' + $currentPage + '&noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },

            getUserSection: function () {
                return $http.get('/json/dashBoard/getUserSection' + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },

            getBudgetByCeiling: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/getBudgetByCeiling/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },

            getTotalCeilingAndTotalBudget: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/getTotalCeilingAgainstTotalBudget/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },
            getProcurableAgainstTotalBudget: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/getProcurableAgainstTotalBudget/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },
            getTotalBudgetByClass: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/getTotalBudgetByClass/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },
            getBudgetAndCeilingBySector: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/getBudgetAndCeilingBySector/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },
            getTotalProblemCeiling: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/get-total-ceiling-problem/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },
            getProblemCeiling: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/get-ceiling-problem/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },
            getTotalAllocationProblem: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/get-total-allocation-problem/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },
            getAllocationProblem: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/get-allocation-problem/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },
            getIncompCeilingBudget: function (budgetType, financialYearId, adminHierarchyId, sectionId, isTotal) {
                return $http.get('/json/dashBoard/get-incomp-ceiling-budget/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '/' + isTotal + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },
            getIncompCeilingBudgetItems: function (budgetType, financialYearId, adminHierarchyId, sectionId, page, perPage) {
                return $http.get('/json/dashBoard/get-incomp-ceiling-budget/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '/false' + '?page=' + page + '&perPage=' + perPage + '&noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },
            downloadIncompCeilingBudget: function (budgetType, financialYearId, adminHierarchyId) {
                return $http.get('/json/dashBoard/download-incomp-ceiling-budget/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId)
                    .then(function (response) {
                        return response.data;
                    });
            },
            ///new services
            getPEBudgetByPECeiling: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/getPEBudgetByPECeiling/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },
            getTotalPECeilingAndTotalPEBudget: function (budgetType, financialYearId, adminHierarchyId, sectionId) {
                return $http.get('/json/dashBoard/getTotalPECeilingAndTotalPEBudget/' + budgetType + '/' + financialYearId + '/' + adminHierarchyId + '/' + sectionId + '?noLoader=true')
                    .then(function (response) {
                        return response.data;
                    });
            },
            activeFeatureNotifcation: function () {
                return $http.get('/api/feature-notifications/get-active')
                    .then(function (response) {
                        return response.data;
                    });
            }
        };
    }]);

    dashboardModels.factory('SocketIO', ['$rootScope', function ($rootScope) {
        /**Temporary disable socket*/
        return {
            on: function (eventName, callback) {
            },
            emit: function (eventName, data, callback) {
            }
        };


        /**uncomment to enable socket
        
                var socket = io('http://localhost:3000');
        
                return {
                    on : function(eventName, callback) {
                        socket.on(eventName, function() {
                            var args = arguments;
                            $rootScope.$apply(function() {
                                callback.apply(socket, args);
                            });
                        });
                    },
        
                    emit : function(eventName, data, callback) {
                        socket.emit(eventName, data, function() {
                            var args = arguments;
                            $rootScope.$apply(function() {
                                if (callback) {
                                    callback.apply(socket, args);
                                }
                            });
                        });
                    }
                };
         */


    }]);
})();