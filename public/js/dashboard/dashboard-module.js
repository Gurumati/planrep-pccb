
var dashboard = angular.module('dashboard-module',['master-module','ngRoute', 'ng-fusioncharts',
    'gridshore.c3js.chart', 'uiGmapgoogle-maps', 'ngMaterial','dashboardModels']);

dashboard.config(['$routeProvider',function ($routeProvider) {
    $routeProvider.
    when('/', {
        controller:DashboardController,
        templateUrl:'/pages/dashboard/dashboard.html',
        resolve:DashboardController.resolve,
        rights: ['dashboard']
    }) .when('/access-denied', {
        controller: 'AccessDeniedController',
        templateUrl: '/pages/access-denied.html',
        rights: ['dashboard']
    })
    .otherwise({redirectTo:'/'});

}]);
