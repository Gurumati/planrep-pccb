
services.factory('AuthorizationService', function (localStorageService, $window) {

    var rights = localStorageService.get(localStorageKeys.RIGHT);

    var preAuthorize = function () {
        var permissions = Array.prototype.slice.call(arguments);
        if(!hasRight(permissions)){
            $window.location = "/public/pages/access-denied.html";
            return false;
        }
        return true;
    };

    var hasRight = function(permissions){
        if (rights) {
            var rightNames = _.pluck(JSON.parse(rights), 'name');
            var hasRight = _.intersection(permissions, rightNames);
            if(hasRight.length > 0){
                return true;
            }}
        else{
            return false;
        }
    };

    var preAuthorizeReporting = function () {
        return rights && _.find(JSON.parse(rights), function (right) {
                return right.type === 'REPORTING';
            });
    };

    var hasPermission = function () {
        var permissions = Array.prototype.slice.call(arguments);
        return hasRight(permissions);
    };

    return{
        preAuthorize: preAuthorize,
        hasPermission: hasPermission,
        preAuthorizeReporting: preAuthorizeReporting
    };
});
