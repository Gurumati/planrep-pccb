var userService = angular.module('userServices', ['ngResource']);

userService.factory('UserProfileService',function ($resource) {
    return $resource('/json/user/profile',{},{});
});

userService.factory('UserMessageService',function ($resource) {
    return $resource('/json/user/messages',{},{});
});