var auth = angular.module('user-module', ['master-module', 'ngRoute', 'userServices']);

auth.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {
        controller: UserProfileController,
        templateUrl: '/pages/user/profile.html',
        resolve: UserProfileController.resolve,
        rights:['profile']
    }).when('/profile', {
        controller: UserProfileController,
        templateUrl: '/pages/user/profile.html',
        resolve: UserProfileController.resolve,
        rights:['profile']
    }).when('/messages', {
        controller: UserMessageController,
        templateUrl: '/pages/user/messages.html',
        resolve: UserMessageController.resolve,
        rights:['profile']
    })
    .otherwise({redirectTo: '/'});
}]);
