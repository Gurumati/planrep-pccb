function UserMessageController($scope, UserMessageModel) {
    $scope.userMessages = UserMessageModel;
    $scope.title = "USER_MESSAGES";
}

UserMessageController.resolve = {
    UserMessageModel: function (UserMessageService, $q) {
        var deferred = $q.defer();
        UserMessageService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};