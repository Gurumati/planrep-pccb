var assessmentService = angular.module('assessmentServices', ['ngResource']);

//CAS ASSESSMENT CATEGORIES
assessmentService.factory('PaginatedCasAssessmentCategoryService', function ($resource) {
    return $resource('/json/casAssessmentCategories/paginated', {}, {});
});

assessmentService.factory('CreateCasAssessmentCategoryService', function ($resource) {
    return $resource('/json/casAssessmentCategory/create', {}, {store: {method: 'POST'}});
});

assessmentService.factory('UpdateCasAssessmentCategoryService', function ($resource) {
    return $resource('/json/casAssessmentCategory/update', {}, {update: {method: 'POST'}});
});

assessmentService.factory('DeleteCasAssessmentCategoryService', function ($resource) {
    return $resource('/json/casAssessmentCategory/delete/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

assessmentService.factory('TrashedCasAssessmentCategoryService', function ($resource) {
    return $resource('/json/casAssessmentCategories/trashed', {}, {});
});

assessmentService.factory('RestoreCasAssessmentCategoryService', function ($resource) {
    return $resource('/json/casAssessmentCategory/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

assessmentService.factory('EmptyCasAssessmentCategoryTrashService', function ($resource) {
    return $resource('/json/casAssessmentCategories/emptyTrash', {}, {});
});

assessmentService.factory('PermanentDeleteCasAssessmentCategoryService', function ($resource) {
    return $resource('/json/casAssessmentCategory/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});

//cas assessment round start
assessmentService.factory('CasAssessmentRoundsService', function ($resource) {
    return $resource('/json/CasAssessmentRounds', {}, {});
});

assessmentService.factory('CreateCasAssessmentRoundsService', function ($resource) {
    return $resource('/json/CreateCasAssessmentRounds', {}, {store: {method: 'POST'}});
});

assessmentService.factory('UpdateCasAssessmentRoundsService', function ($resource) {
    return $resource('/json/UpdateCasAssessmentRounds', {}, {update: {method: 'POST'}});
});

assessmentService.factory('DeleteCasAssessmentRoundsService', function ($resource) {
    return $resource('/json/DeleteCasAssessmentRounds/:id', {id: '@id'}, {delete: {method: 'GET'}});
});
//cas assessment round end

//cas assessment possible score start
assessmentService.factory('CasAssessmentSubCriteriaPossibleScoreService', function ($resource) {
    return $resource('/json/CasAssessmentSubCriteriaPossibleScore/:sub_criteria_id', {sub_criteria_id:'@sub_criteria_id'}, {});
});

assessmentService.factory('CreateCasAssessmentSubCriteriaPossibleScoreService', function ($resource) {
    return $resource('/json/CreateCasAssessmentSubCriteriaPossibleScore', {}, {store: {method: 'POST'}});
});

assessmentService.factory('UpdateCasAssessmentSubCriteriaPossibleScoreService', function ($resource) {
    return $resource('/json/UpdateCasAssessmentSubCriteriaPossibleScore', {}, {update: {method: 'POST'}});
});

assessmentService.factory('DeleteCasAssessmentSubCriteriaPossibleScoreService', function ($resource) {
    return $resource('/json/DeleteCasAssessmentSubCriteriaPossibleScore/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

//cas assessment possible score end

//CAS ASSESSMENT CATEGORY VERSIONS
assessmentService.factory('PaginatedCasAssessmentCategoryVersionService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersions/paginated', {}, {});
});

assessmentService.factory('FilteredCasAssessmentCategoryVersionService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersions/filtered', {id:'@id', financial_year: '@financial_year'}, {});
});

assessmentService.factory('CasAssessmentCategoryVersionService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersions', {financial_year_id: '@financial_year_id'}, {});
});

assessmentService.factory('CreateCasAssessmentCategoryVersionService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersion/create', {}, {store: {method: 'POST'}});
});

assessmentService.factory('UpdateCasAssessmentCategoryVersionService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersion/update', {}, {update: {method: 'POST'}});
});

assessmentService.factory('DeleteCasAssessmentCategoryVersionService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersion/delete/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

assessmentService.factory('TrashedCasAssessmentCategoryVersionService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersions/trashed', {}, {});
});

assessmentService.factory('RestoreCasAssessmentCategoryVersionService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersion/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

assessmentService.factory('EmptyCasAssessmentCategoryVersionTrashService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersions/emptyTrash', {}, {});
});

assessmentService.factory('PermanentDeleteCasAssessmentCategoryVersionService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersion/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});

//CAS ASSESSMENT STATES
assessmentService.factory('PaginatedCasAssessmentStateService', function ($resource) {
    return $resource('/json/casAssessmentStates/paginated', {}, {});
});

assessmentService.factory('CreateCasAssessmentStateService', function ($resource) {
    return $resource('/json/casAssessmentState/create', {}, {store: {method: 'POST'}});
});

assessmentService.factory('UpdateCasAssessmentStateService', function ($resource) {
    return $resource('/json/casAssessmentState/update', {}, {update: {method: 'POST'}});
});

assessmentService.factory('DeleteCasAssessmentStateService', function ($resource) {
    return $resource('/json/casAssessmentState/delete/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

assessmentService.factory('TrashedCasAssessmentStateService', function ($resource) {
    return $resource('/json/casAssessmentStates/trashed', {}, {});
});

assessmentService.factory('RestoreCasAssessmentStateService', function ($resource) {
    return $resource('/json/casAssessmentState/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

assessmentService.factory('EmptyCasAssessmentStateTrashService', function ($resource) {
    return $resource('/json/casAssessmentStates/emptyTrash', {}, {});
});

assessmentService.factory('PermanentDeleteCasAssessmentStateService', function ($resource) {
    return $resource('/json/casAssessmentState/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});

//CAS ASSESSMENT CATEGORY VERSION STATES
assessmentService.factory('PaginatedCasAssessmentCategoryVersionStateService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersionStates/paginated', {}, {});
});

assessmentService.factory('CreateCasAssessmentCategoryVersionStateService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersionState/create', {}, {store: {method: 'POST'}});
});

assessmentService.factory('UpdateCasAssessmentCategoryVersionStateService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersionState/update', {}, {update: {method: 'POST'}});
});

assessmentService.factory('DeleteCasAssessmentCategoryVersionStateService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersionState/delete/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

assessmentService.factory('TrashedCasAssessmentCategoryVersionStateService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersionStates/trashed', {}, {});
});

assessmentService.factory('RestoreCasAssessmentCategoryVersionStateService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersionState/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

assessmentService.factory('EmptyCasAssessmentCategoryVersionStateTrashService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersionStates/emptyTrash', {}, {});
});

assessmentService.factory('PermanentDeleteCasAssessmentCategoryVersionStateService', function ($resource) {
    return $resource('/json/casAssessmentCategoryVersionState/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});

//cas assessment criteria report set start
assessmentService.factory('CasAssessmentSubCriteriaReportSetService', function ($resource) {
    return $resource('/json/CasAssessmentSubCriteriaReportSet/:sub_criteria_id', {sub_criteria_id:'@sub_criteria_id'}, {});
});
//CAS ASSESSMENT CATEGORY CRITERIA OPTIONS
assessmentService.factory('PaginatedCasAssessmentCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentCriteriaOptions/paginated/:category_version_id', {category_version_id:'@category_version_id'}, {});
});

assessmentService.factory('FilteredCasAssessmentCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentFilteredCriteria/:category_version_id', {category_version_id:'@category_version_id'}, {});
});

assessmentService.factory('AllCouncilsService', function ($resource) {
    return $resource('/json/allCouncils/:category_version_id', {category_version_id:'@category_version_id'}, {});
});

assessmentService.factory('AllRoundsService', function ($resource) {
    return $resource('/json/allRounds/:category_version_id/:council_id', {category_version_id:'@category_version_id',council_id:'@council_id'}, {});
});

assessmentService.factory('SelectedRoundsService', function ($resource) {
    return $resource('/json/assigned_Rounds/:category_version_id/:council_id', {category_version_id:'@category_version_id',council_id:'@council_id'}, {});
});

assessmentService.factory('ExportReportService', function ($resource) {
    return $resource('/json/export_report/:id/:report_name/:folder', {id:'@id',report_name:'@report_name',folder:'@folder'}, {});
});

assessmentService.factory('CreateCasAssessmentCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentCriteriaOption/create', {}, {store: {method: 'POST'}});
});

assessmentService.factory('UpdateCasAssessmentCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentCriteriaOption/update', {}, {update: {method: 'POST'}});
});

assessmentService.factory('DeleteCasAssessmentCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentCriteriaOption/delete/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

assessmentService.factory('TrashedCasAssessmentCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentCriteriaOptions/trashed', {}, {});
});

assessmentService.factory('RestoreCasAssessmentCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentCriteriaOption/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

assessmentService.factory('EmptyCasAssessmentCriteriaOptionTrashService', function ($resource) {
    return $resource('/json/casAssessmentCriteriaOptions/emptyTrash', {}, {});
});

assessmentService.factory('PermanentDeleteCasAssessmentCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentCriteriaOption/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});


//CAS ASSESSMENT CATEGORY SUB CRITERIA OPTIONS
assessmentService.factory('PaginatedCasAssessmentSubCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentSubCriteriaOptions/paginated/', {}, {});
});


assessmentService.factory('FilteredAssessmentSubCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentSubCriteriaOptions/paginated', {}, {});
});

/*assessmentService.factory('FilteredAssessmentSubCriteriaScoresService', function ($resource) {
    return $resource('/json/casAssessmentSubCriteriaOption/create', {}, {store: {method: 'POST'}});
});*/

assessmentService.factory('CreateCasAssessmentSubCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentSubCriteriaOption/create', {}, {store: {method: 'POST'}});
});

assessmentService.factory('UpdateCasAssessmentSubCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentSubCriteriaOption/update', {}, {update: {method: 'POST'}});
});

assessmentService.factory('DeleteCasAssessmentSubCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentSubCriteriaOption/delete/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

assessmentService.factory('TrashedCasAssessmentSubCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentSubCriteriaOptions/trashed', {}, {});
});

assessmentService.factory('RestoreCasAssessmentSubCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentSubCriteriaOption/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

assessmentService.factory('EmptyCasAssessmentSubCriteriaOptionTrashService', function ($resource) {
    return $resource('/json/casAssessmentSubCriteriaOptions/emptyTrash', {}, {});
});

assessmentService.factory('FilteredSubCriteriaOptionsService', function ($resource) {
    return $resource('/json/subCriteria/:criteria_id/:result_id', {criteria_id: '@criteria_id',result_id:'@result_id'}, {});
});

assessmentService.factory('getSpServiceService', function ($resource) {
    return $resource('/json/strategicPlan/:adminHierarchId/:financialYearId', {adminHierarchId: '@adminHierarchId',financialYearId:'@financialYearId'}, {});
});

assessmentService.factory('FilteredSubCriteriaScoresService', function ($resource) {
    return $resource('/json/possibleScores/:sub_criteria_id', {sub_criteria_id: '@sub_criteria_id'}, {});
});
assessmentService.factory('PermanentDeleteCasAssessmentSubCriteriaOptionService', function ($resource) {
    return $resource('/json/casAssessmentSubCriteriaOption/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});

assessmentService.factory('ToggleFreeScoreService',function ($resource) {
    return $resource('/json/casAssessmentSubCriteriaOption/toggleFreeScore',{},{toggleFreeScore:{method:'POST'}});
});

assessmentService.factory('CreateCasAssessmentSubCriteriaReportSetService', function ($resource) {
    return $resource('/json/CreateCasAssessmentSubCriteriaReportSet', {}, {store: {method: 'POST'}});
});

assessmentService.factory('UpdateCasAssessmentSubCriteriaReportSetService', function ($resource) {
    return $resource('/json/UpdateCasAssessmentSubCriteriaReportSet', {}, {update: {method: 'POST'}});
});

assessmentService.factory('DeleteCasAssessmentSubCriteriaReportSetService', function ($resource) {
    return $resource('/json/DeleteCasAssessmentSubCriteriaReportSet/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

assessmentService.factory('DeleteCasAssessmentSubCriteriaReportSetService', function ($resource) {
    return $resource('/json/DeleteCasAssessmentSubCriteriaReportSet/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

assessmentService.factory('AssessmentResultService', function ($resource) {
    return $resource('/json/OpenAssessmentResult/:category_version_id/:period_id/:round_id/:admin_hierarchy_id',
        {category_version_id:'@category_version_id',period_id:'@period_id',round_id:'@round_id',admin_hierarchy_id:'@admin_hierarchy_id'}, {});
});

assessmentService.factory('UpdateResultsService',function ($resource) {
    return $resource('/json/UpdateResults',{},{updateResults:{method:'POST'}});
});

assessmentService.factory('UpdateResultDetailsService',function ($resource) {
    return $resource('/json/UpdateResultDetails',{},{updateResultDetails:{method:'POST'}});
});

assessmentService.factory('UpdateReplyService',function ($resource) {
    return $resource('/json/UpdateReply/:id/:comment/:category/:category_id/:status'
        ,{id:'@id',comment:'@comment',category:'@category',category_id:'@category_id',status:'@status'},{});
});

assessmentService.factory('GetReplyService',function ($resource) {
    return $resource('/json/GetReply/:id/:category/:category_id',{id:'@id',category:'@category',category_id:'@category_id'},{});
});

assessmentService.factory('MyAssessmentResultsService',function ($resource) {
    return $resource('/json/MyAssessmentResults',
                    {category_version_id: '@category_version_id',assessment_group: '@assessment_group', financial_year: '@financial_year'},{});
});

assessmentService.factory('PeriodsService',function ($resource) {
    return $resource('/json/periods/:category_version_id/:council_id/:round_id/:financial_year_id',
        {category_version_id:'@category_version_id',council_id:'@council_id', round_id:'@round_id', financial_year_id: '@financial_year_id'},{});
});

assessmentService.factory('AssignedPeriodsService',function ($resource) {
    return $resource('/json/assigned_periods/:category_version_id/:council_id/:round_id/:financial_year_id',
        {category_version_id:'@category_version_id',council_id:'@council_id', round_id:'@round_id', financial_year_id: '@financial_year_id'},{});
});

assessmentService.factory('ReportSetService',function ($resource) {
    return $resource('/json/ReportSet/:sub_criteria_id',{sub_criteria_id:'@sub_criteria_id'},{});
});






//cas assessment criteria report set end