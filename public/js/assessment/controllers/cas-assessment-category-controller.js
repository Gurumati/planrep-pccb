function CasAssessmentCategoryController($scope, CasAssessmentCategoryModel, $uibModal, ConfirmDialogService,
                                         DeleteCasAssessmentCategoryService,
                                PaginatedCasAssessmentCategoryService) {

    $scope.casAssessmentCategories = CasAssessmentCategoryModel;
    $scope.title = "CAS_ASSESSMENT_CATEGORY_LIST";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedCasAssessmentCategoryService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.casAssessmentCategories = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_category/create.html',
            backdrop: false,
            controller: function ($scope,GlobalService, $uibModalInstance,AllCASPlanService, CreateCasAssessmentCategoryService) {

                AllCASPlanService.query(function (data) {
                    $scope.cas_plans = data;
                });

                GlobalService.periodGroups(function (data) {
                    $scope.periodGroups = data.periodGroups;
                });
                
                $scope.casAssessmentCategoryToCreate = {};

                $scope.store = function () {
                    if ($scope.createCasAssessmentCategoryForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateCasAssessmentCategoryService.store({perPage: $scope.perPage}, $scope.casAssessmentCategoryToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.casAssessmentCategories = data.casAssessmentCategories;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (casAssessmentCategoryToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_category/edit.html',
            backdrop: false,
            controller: function ($scope,GlobalService, $uibModalInstance,AllCASPlanService, UpdateCasAssessmentCategoryService) {
                AllCASPlanService.query(function (data) {
                    $scope.cas_plans = data;
                    $scope.casAssessmentCategoryToEdit = angular.copy(casAssessmentCategoryToEdit);
                });
                GlobalService.periodGroups(function (data) {
                    $scope.periodGroups = data.periodGroups;
                });
                $scope.update = function () {
                    UpdateCasAssessmentCategoryService.update({page: currentPage, perPage: perPage}, $scope.casAssessmentCategoryToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.casAssessmentCategories = data.casAssessmentCategories;
                $scope.currentPage = $scope.casAssessmentCategories.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteCasAssessmentCategoryService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.casAssessmentCategories = data.casAssessmentCategories;
                        $scope.currentPage = $scope.casAssessmentCategories.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_category/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedCasAssessmentCategoryService,
                                  RestoreCasAssessmentCategoryService, EmptyCasAssessmentCategoryTrashService, PermanentDeleteCasAssessmentCategoryService) {
                TrashedCasAssessmentCategoryService.query(function (data) {
                    $scope.trashedCASAssessmentCategories = data;
                });
                $scope.restoreCasAssessmentCategory = function (id) {
                    RestoreCasAssessmentCategoryService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCASAssessmentCategories = data.trashedCASAssessmentCategories;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteCasAssessmentCategoryService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCASAssessmentCategories = data.trashedCASAssessmentCategories;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyCasAssessmentCategoryTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCASAssessmentCategories = data.trashedCASAssessmentCategories;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.casAssessmentCategories = data.casAssessmentCategories;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

CasAssessmentCategoryController.resolve = {
    CasAssessmentCategoryModel: function (PaginatedCasAssessmentCategoryService, $q) {
        var deferred = $q.defer();
        PaginatedCasAssessmentCategoryService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};