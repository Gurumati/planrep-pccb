function CasAssessmentSubCriteriaPossibleScoreController($scope,
                                                         CasAssessmentCategoryAllModel,
                                                         $uibModal,
                                                         ConfirmDialogService,
                                                         CasAssessmentSubCriteriaPossibleScoreService,
                                                         AllCASAssessmentSubCriteriaByCriteriaService,
                                                         AllCASAssessmentCriteriaByCategoryService,
                                                         AllCasAssessmentCategoryVersionService,
                                                         DeleteCasAssessmentSubCriteriaPossibleScoreService)
{
    $scope.casAssessmentCategories = CasAssessmentCategoryAllModel;
    $scope.title = "CAS_ASSESSMENT_SUB_CRITERIA_POSSIBLE_SCORE";

    $scope.loadCriteria = function (category_id) {
        AllCASAssessmentCriteriaByCategoryService.query({category_id: category_id}, function (data) {
            $scope.casAssessmentCriteriaOptions = data;
        });
    };
    $scope.loadSubCriteria = function (criteria_id) {
        AllCASAssessmentSubCriteriaByCriteriaService.query({criteria_id: criteria_id}, function (data) {
            $scope.casAssessmentSubCriteriaOptions = data;
        });
    };
    
    $scope.loadPossibleScore = function (sub_criteria_id) {
        console.log(sub_criteria_id);
        CasAssessmentSubCriteriaPossibleScoreService.query({sub_criteria_id:sub_criteria_id},
            function (data) {
                $scope.casAssessmentCriteriaScores = data;
            }
        );
    };

    $scope.create = function (sub_criteria_id) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_sub_criteria_possible_score/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,CreateCasAssessmentSubCriteriaPossibleScoreService) {

                $scope.casAssessmentSubCriteriaPossibleScoreToCreate = {};

                $scope.store = function () {
                    if ($scope.createCasAssessmentSubCriteriaPossibleScoreForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    $scope.casAssessmentSubCriteriaPossibleScoreToCreate.cas_assessment_sub_criteria_option_id = sub_criteria_id;
                    console.log($scope.casAssessmentSubCriteriaPossibleScoreToCreate);
                    CreateCasAssessmentSubCriteriaPossibleScoreService.store( $scope.casAssessmentSubCriteriaPossibleScoreToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.casAssessmentCriteriaScores = data.casAssessmentCriteriaScores;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (casAssessmentSubCriteriaPossibleScoreToEdit) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_sub_criteria_possible_score/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,UpdateCasAssessmentSubCriteriaPossibleScoreService) {

                $scope.casAssessmentSubCriteriaPossibleScoreToEdit = angular.copy(casAssessmentSubCriteriaPossibleScoreToEdit);
                $scope.update = function () {

                    if ($scope.updateCasAssessmentSubCriteriaPossibleScoreForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }

                    UpdateCasAssessmentSubCriteriaPossibleScoreService.update( $scope.casAssessmentSubCriteriaPossibleScoreToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.casAssessmentCriteriaScores = data.casAssessmentCriteriaScores;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteCasAssessmentSubCriteriaPossibleScoreService.delete({id: id},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.casAssessmentCriteriaScores = data.casAssessmentCriteriaScores;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
}

CasAssessmentSubCriteriaPossibleScoreController.resolve = {
    CasAssessmentCategoryAllModel: function (AllCasAssessmentCategoryVersionService, $q) {
        var deferred = $q.defer();
        AllCasAssessmentCategoryVersionService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};
