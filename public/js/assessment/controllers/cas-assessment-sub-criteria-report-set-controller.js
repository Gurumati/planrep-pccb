function CasAssessmentSubCriteriaReportSetController($scope,
                                                     CasAssessmentCategoryAllModel,
                                                         $uibModal,
                                                         ConfirmDialogService,
                                                         CasAssessmentSubCriteriaReportSetService,
                                                         AllCasAssessmentCategoryVersionService,
                                                         AllCASAssessmentCriteriaByCategoryService,
                                                         AllCASAssessmentSubCriteriaByCriteriaService,
                                                         DeleteCasAssessmentSubCriteriaReportSetService)
{
    $scope.casAssessmentCategories = CasAssessmentCategoryAllModel;
    $scope.title = "CAS_ASSESSMENT_SUB_CRITERIA_REPORT_SET";

    $scope.loadCriteria = function (category_id) {
        AllCASAssessmentCriteriaByCategoryService.query({category_id: category_id}, function (data) {
            $scope.casAssessmentCriteriaOptions = data;
        });
    };
    $scope.loadSubCriteria = function (criteria_id) {
        AllCASAssessmentSubCriteriaByCriteriaService.query({criteria_id: criteria_id}, function (data) {
            $scope.casAssessmentSubCriteriaOptions = data;
        });
    };
    
    $scope.loadReportSet = function (sub_criteria_id) {
        CasAssessmentSubCriteriaReportSetService.query({sub_criteria_id:sub_criteria_id},
            function (data) {
                $scope.casAssessmentReportSets = data;
            }
        );
    };

    $scope.create = function (sub_criteria_id) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_sub_criteria_report_set/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,CreateCasAssessmentSubCriteriaReportSetService,CasPlanContentsService,CasPlanService) {

                $scope.casAssessmentSubCriteriaReportSetToCreate = {};

                //load Plans
                CasPlanService.query({}, function (data) {
                    $scope.casPlans = data;
                });
                //loadContents
                $scope.loadContents = function (cas_plan_id) {
                    CasPlanContentsService.query({cas_plan_id: cas_plan_id}, function (data) {
                        $scope.casPlanContents = data;
                        $scope.flatter(data, $scope.casPlanContents);
                    });
                };


                $scope.store = function () {
                    if ($scope.createCasAssessmentSubCriteriaPossibleScoreForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    $scope.casAssessmentSubCriteriaReportSetToCreate.cas_assessment_sub_criteria_option_id = sub_criteria_id;

                    CreateCasAssessmentSubCriteriaReportSetService.store( $scope.casAssessmentSubCriteriaReportSetToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.casAssessmentReportSets = data.casAssessmentReportSets;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteCasAssessmentSubCriteriaReportSetService.delete({id: id},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.casAssessmentReportSets = data.casAssessmentReportSets;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
}

CasAssessmentSubCriteriaReportSetController.resolve = {
    CasAssessmentCategoryAllModel: function (AllCasAssessmentCategoryVersionService, $q) {
        var deferred = $q.defer();
        AllCasAssessmentCategoryVersionService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};
