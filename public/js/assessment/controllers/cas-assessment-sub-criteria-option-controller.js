function CasAssessmentSubCriteriaOptionController($scope, CasAssessmentCategoryAllModel,
                                                  $uibModal, ConfirmDialogService,
                                                  DeleteCasAssessmentSubCriteriaOptionService,
                                                  AllCasAssessmentCategoryVersionService,
                                                  AllCASAssessmentCriteriaByCategoryService,
                                                  ToggleFreeScoreService,
                                                  PaginatedCasAssessmentSubCriteriaOptionService) {

    $scope.casAssessmentCategories = CasAssessmentCategoryAllModel;
    $scope.title = "CAS_ASSESSMENT_SUB_CRITERIA_OPTIONS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;

    $scope.loadCriteria = function (category_id) {
        AllCASAssessmentCriteriaByCategoryService.query({category_id: category_id}, function (data) {
            $scope.casAssessmentCriteriaOptions = data;
        });
    };

    $scope.loadSubCriteria = function (CriteriaOption) {
        console.log(CriteriaOption);
        PaginatedCasAssessmentSubCriteriaOptionService.get({page: $scope.currentPage, perPage: $scope.perPage, CriteriaOption: CriteriaOption}, function (data) {
            $scope.subCriteriaOptions = data;
        });
    };

    $scope.pageChanged = function () {
        PaginatedCasAssessmentSubCriteriaOptionService.get({page: $scope.currentPage, perPage: $scope.perPage, CriteriaOption: $scope.CriteriaOption}, function (data) {
            $scope.subCriteriaOptions = data;
        });
    };

    $scope.create = function (CriteriaOption) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_sub_criteria_option/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllCasAssessmentCriteriaOptionService, CreateCasAssessmentSubCriteriaOptionService) {

                AllCasAssessmentCriteriaOptionService.query(function (data) {
                    $scope.criteriaOptions = data;
                });

                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    $scope.createDataModel.cas_assessment_criteria_option_id = CriteriaOption;
                    CreateCasAssessmentSubCriteriaOptionService.store({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.subCriteriaOptions = data.subCriteriaOptions;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
    $scope.edit = function (updateDataModel, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_sub_criteria_option/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllCasAssessmentCriteriaOptionService, UpdateCasAssessmentSubCriteriaOptionService) {
                AllCasAssessmentCriteriaOptionService.query(function (data) {
                    $scope.criteriaOptions = data;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });

                $scope.update = function () {
                    if ($scope.updateForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateCasAssessmentSubCriteriaOptionService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.subCriteriaOptions = data.subCriteriaOptions;
                $scope.currentPage = $scope.subCriteriaOptions.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteCasAssessmentSubCriteriaOptionService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.subCriteriaOptions = data.subCriteriaOptions;
                        $scope.currentPage = $scope.subCriteriaOptions.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_sub_criteria_option/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance,
                                  TrashedCasAssessmentSubCriteriaOptionService,
                                  RestoreCasAssessmentSubCriteriaOptionService,
                                  EmptyCasAssessmentSubCriteriaOptionTrashService,
                                  PermanentDeleteCasAssessmentSubCriteriaOptionService) {
                TrashedCasAssessmentSubCriteriaOptionService.query(function (data) {
                    $scope.trashedSubCriteriaOptions = data;
                });
                $scope.restoreCasAssessmentSubCriteriaOption = function (id) {
                    RestoreCasAssessmentSubCriteriaOptionService.restore({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedSubCriteriaOptions = data.trashedSubCriteriaOptions;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteCasAssessmentSubCriteriaOptionService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedSubCriteriaOptions = data.trashedSubCriteriaOptions;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyCasAssessmentSubCriteriaOptionTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedSubCriteriaOptions = data.trashedSubCriteriaOptions;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.subCriteriaOptions = data.subCriteriaOptions;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
    $scope.toggleFreeScore = function (id, is_free_score,perPage) {
        $scope.switchModel = {};
        $scope.switchModel.id = id;
        $scope.switchModel.is_free_score = is_free_score;
        ToggleFreeScoreService.toggleFreeScore({perPage:perPage},$scope.switchModel,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    };
}

CasAssessmentSubCriteriaOptionController.resolve = {
    CasAssessmentCategoryAllModel: function (AllCasAssessmentCategoryVersionService, $q) {
        var deferred = $q.defer();
        AllCasAssessmentCategoryVersionService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};