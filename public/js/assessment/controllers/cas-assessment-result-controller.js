/**
 * Created by mrisho on 19/May/2017.
 */
function CasAssessmentResultController($scope,
                                       $route,
                                       UpdateResultsService,
                                       UpdateResultDetailsService,
                                       MyAssessmentResultsService,
                                       PeriodsService,
                                       AssignedPeriodsService,
                                       ReportSetService,
                                       FilteredSubCriteriaScoresService,
                                       AssessmentResultService,
                                       FilteredSubCriteriaOptionsService,
                                       AllCouncilsService,
                                       SelectedRoundsService,
                                       AllRoundsService,
                                       GetFacilityBySectorService,
                                       FilteredCasAssessmentCriteriaOptionService,
                                       FilteredCasAssessmentCategoryVersionService,
                                       PaginatedCasAssessmentCriteriaOptionService,
                                       LoadReportParametersSources,
                                       ExportReportService,
                                       getJasperCasReportService,
                                       GetReplyService,
                                       UpdateReplyService,
                                       localStorageService,
                                       FinancialYearsService,
                                       CasAssessmentCategoryVersionService,
                                       CasAssessmentCriteriaOptionModel,
                                       $animate,
                                       getSpServiceService,
                                       $location

) {

    $scope.categoryVersions = CasAssessmentCriteriaOptionModel;
    $scope.financial_year = $scope.categoryVersions.financial_year;
    $animate.enabled(false);
    $scope.title = "CAS_ASSESSMENT_CRITERIA_RESULTS";
    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.criteria = false;
    $scope.criteria_id = 0;
    $scope.showSubCriteria = false;
    $scope.result_list = false;
    $scope.total = [];
    $scope.isAttended = [];
    $scope.isFullyAttended = true;
    $scope.main_total = 0;
    $scope.group_name = "";
    $scope.reportParameter = {};
    $scope.preview = 0;
    $scope.other_levels = false;
    $scope.replies = false;
    $scope.currentSubCriteria = 0;
    $scope.previwedSbuCriteria = [];
    $scope.previuos_status = false;
    $scope.pisc_id = null;
    $scope.param_category_id = $route.current.params.assessment_category_id;
    $scope.param_hierarchy_position = $route.current.params.hierarchy_position;

    /** get financial year */
    FinancialYearsService.query({}, function(data){
       $scope.financial_years = data;
    });

    $scope.loadAssessmentCategories = function(){
        /** check if financial_year and category id is set */
        if($scope.param_category_id !== '0' ){
            FilteredCasAssessmentCategoryVersionService.get({id :$scope.param_category_id, financial_year: $scope.financial_year.id}, function (data) {
                $scope.categoryVersions = data;
                var default_id= parseInt($scope.categoryVersions.default);
                if(default_id !== 0) {
                    //var categoryVersion = _.where($scope.categoryVersions.category_versions, {id: default_id});
                    $scope.openCriteria($scope.categoryVersions.category_versions[0]);
                }
            });
        }else {
            CasAssessmentCategoryVersionService.get({financial_year_id: $scope.financial_year.id}, function (data) {
                $scope.categoryVersions = data;
            });
        }

    };

    $scope.returnCouncils = function () {
        AllCouncilsService.query({category_version_id: $scope.category_version_id}, function (data) {

            $scope.adminHierarchies = data;
            console.log(data);
        });
    };

    $scope.getReplies = function (id,category,category_id, sub_criteria) {
        GetReplyService.get({
            id: id,
            category: category,
            category_id: category_id
        }, function (data) {
            if(category === 2 || category === 4 ) {
                $scope.result_replies = data.reply;
                $scope.reply_user = data.user_id;

            }
            if(category === 3 || category === 5)
            {
                $scope.result_detail_replies = data.reply;
                $scope.replies = true;
                $scope.report_list = true;
                $scope.sub_criteria_comment = sub_criteria.remarks;
                $scope.sub_criteria_name = sub_criteria.cas_assessment_sub_criteria.serial_number + ": " + sub_criteria.cas_assessment_sub_criteria.name;
            }
        });
    };

    $scope.editReplyDetail = function(resultDetailReply)
    {
        $scope.reply_detail_id = resultDetailReply.id;
        $scope.reply_detail_comment = resultDetailReply.comment;
    };

    $scope.editReply = function(resultReply) {
        $scope.reply_id = resultReply.id;
        $scope.reply_comment = resultReply.comment;
    };

    $scope.updateReplies = function (id,comment,category,category_id,status) {
        UpdateReplyService.get({
            id:id,comment:comment,
            category:category,category_id:category_id,
            status:status
        }, function (data) {
            if(category === 2 || category === 4 ) {
                $scope.result_replies = data.reply;
                $scope.reply_user = data.user_id;
                $scope.reply_id = 0;
            }
            if(category === 3 || category === 5) {

                $scope.result_detail_replies = data.reply;

                $scope.reply_detail_id = 0;
            }
        });
    };

    $scope.returnRounds = function (council_id) {
        $scope.pisc_id = council_id;
        SelectedRoundsService.query({
            category_version_id: $scope.category_version_id,
            council_id: council_id
        }, function (data) {
            $scope.rounds = data;
        });
    };

    $scope.returnAssignedPeriods = function (council_id, round_id) {

        AssignedPeriodsService.query({
            category_version_id: $scope.category_version_id,
            council_id: council_id,
            round_id: round_id,
            financial_year_id: $scope.financial_year.id
        }, function (data) {
            $scope.assignedPeriods = data;
        });
    };
    $scope.returnPeriods = function (council_id, round_id) {

        PeriodsService.query({
            category_version_id: $scope.category_version_id,
            council_id: council_id,
            round_id: round_id,
            financial_year_id: $scope.financial_year.id
        }, function (data) {
            $scope.periods = data;
        });
    };

    $scope.returnReportSet = function (sub_criteria) {
        ReportSetService.query({sub_criteria_id: sub_criteria.cas_assessment_sub_criteria.id}, function (data) {
            $scope.reportSet = data;
            $scope.sub_criteria_name = sub_criteria.cas_assessment_sub_criteria.serial_number + ": " + sub_criteria.cas_assessment_sub_criteria.name;
            $scope.report_list = true;
            $scope.replies = false;
            $scope.currentSubCriteria = sub_criteria.cas_assessment_sub_criteria.id;
          //  $scope.report_list_tittle = true;
        });
    };

    $scope.returnRelies = function (sub_criteria) {
        ReportSetService.query({sub_criteria_id: sub_criteria.cas_assessment_sub_criteria.id}, function (data) {
            $scope.reportSet = data;
            $scope.sub_criteria_name = sub_criteria.cas_assessment_sub_criteria.serial_number + ": " + sub_criteria.cas_assessment_sub_criteria.name;
            $scope.sub_criteria_comment = sub_criteria.remarks;
            $scope.replies = true;
            $scope.report_list = true;
            //  $scope.report_list_tittle = true;
        });
    };

    $scope.close_report_list = function () {
        $scope.report_list = false;
        $scope.replies = false;
    };

    $scope.pageChanged = function () {
        PaginatedCasAssessmentCriteriaOptionService.get({
            page: $scope.currentPage,
            perPage: $scope.perPage
        }, function (data) {
            $scope.criteriaOptions = data;
        });
    };

    $scope.possibleScores = function (sub_criteria) {
        FilteredSubCriteriaScoresService.query({sub_criteria_id: sub_criteria.cas_assessment_sub_criteria.id}, function (data) {
            $scope.possibleScoreRows = data;
        });
        $scope.sub_criteria_id = sub_criteria.cas_assessment_sub_criteria.id;
        $scope.score = true;
        $scope.comment = false;
    };

    $scope.save_scores = function (sub_criteria, criteria, result_id) {
        UpdateResultDetailsService.updateResultDetails({}, sub_criteria,
            function (data) {
            }
        );

        FilteredSubCriteriaOptionsService.query({
            criteria_id: $scope.criteria_id,
            result_id: $scope.assessment_result.id
        }, function (data) {
            $scope.subCriteriaOptions = data;
        });
        $scope.report_list = false;
        $scope.sub_criteria_id = 0;
        $scope.score = false;
    };

    $scope.openAssessment = function (category_version_id, period_id, round_id, admin_hierarchy_id,facility) {
        AssessmentResultService.get({
                category_version_id: category_version_id,
                period_id: period_id,
                round_id: round_id,
                admin_hierarchy_id: admin_hierarchy_id
            }, function (data) {
                $scope.assessment_result = angular.copy(data);

                $scope.main_total = 0;
                var t = 0;
                angular.forEach($scope.criteriaOptions, function (value, index) {
                    $scope.return_subtotal(value.id);
                });

                $scope.assessment_open = true;
                $scope.jasperResourseUrl = 'https://planrep.pccb.go.tz/jasperserver/rest_v2/reports';
                $scope.admin_hierarchy_id = admin_hierarchy_id;
                $scope.round_id = round_id;
                $scope.period_id = period_id;
                $scope.facility = facility;
                $scope.previuos_status = $scope.assessment_result.is_confirmed;
                (`${$scope.jasperResourceUrl}/${path}.${report_type}?${qs}`, '_blank')

            }
        );

    };
    $scope.editAssessment = function (result) {
        $scope.criteria = true;
        $scope.result_list = false;
        $scope.return_result_list = true;

        FilteredCasAssessmentCriteriaOptionService.query({category_version_id: result.cas_assessment_category_version_id}, function (data) {
            $scope.criteriaOptions = data;
        });

        $scope.admin_hierarchy_id = result.mtef.admin_hierarchy_parent.id;
        $scope.round_id = result.cas_assessment_round_id;
        $scope.period_id = result.period_id;

        AssessmentResultService.get({
                category_version_id: result.cas_assessment_category_version_id,
                period_id: result.period_id,
                round_id: result.id,
                admin_hierarchy_id: 0
            }, function (data) {
                $scope.assessment_result = angular.copy(data);

                $scope.main_total = 0;
                var t = 0;
                angular.forEach($scope.criteriaOptions, function (value, index) {
                    $scope.return_subtotal(value.id);
                });

                $scope.assessment_open = true;
            GetReplyService.get({
                id: 0,
                category: 2,
                category_id: $scope.assessment_result.id
            }, function (reply_data) {
                $scope.result_replies = reply_data.reply;
                $scope.reply_user = data.user_id;
            });
            }
        );

        $scope.returnCouncils();
        $scope.returnRounds($scope.admin_hierarchy_id);
        $scope.returnPeriods($scope.admin_hierarchy_id, $scope.round_id);
    };

    $scope.expand_group = function (r) {
        $scope.group_name = r;
    };

    $scope.collapse_group = function (r) {
         $scope.group_name = "";
    };

    $scope.return_subtotal = function (criteria_id) {
        var is_attended = true;
        FilteredSubCriteriaOptionsService.query({
            criteria_id: criteria_id,
            result_id: $scope.assessment_result.id
        }, function (data) {
            var v = 0;
            angular.forEach(data, function (value, index) {
                v = angular.copy(v + parseInt(value.sub_criteria_score));
                 if(value.cas_assessment_sub_criteria_possible_score_id === null) {
                     is_attended = false;
                 }
            });
            if(!is_attended) {
                $scope.isFullyAttended = false;
            }
            $scope.total[criteria_id] = v;
            $scope.isAttended[criteria_id] = is_attended;
            $scope.main_total += v;
        });
        //   return $scope.total;
    };

    $scope.close_results = function () {
        $scope.result_list = false;
    };

    $scope.finishAssessment = function (assessment_result) {
        var statement = "This will save Results";
        if(assessment_result.is_confirmed) {
            statement = "This will submit Results to other levels";

            if(!$scope.isFullyAttended)
            {
                statement = "You can't submit before completing the assessment, do you want to submit it anyways?";
            }
        }

        if (confirm(statement)) {
        assessment_result.score_value = angular.copy($scope.main_total);
      //  if($scope.assessment_group == 1) {
            UpdateResultsService.updateResults({}, assessment_result,
                function (data) {

                }
            );
     //   }

        MyAssessmentResultsService.get({
            category_version_id: $scope.cas_assessment_category_version_id,
            assessment_group: $scope.assessment_group
        }, function (data) {
            $scope.resultList = data;
            $scope.category_name =  $scope.category_version.cas_assessment_category.name;
            $scope.minimum_passmark =  $scope.category_version.minimum_passmark;
            $scope.category_version_id =  $scope.category_version.id;
        });

        $scope.report_list = false;
        $scope.result_list =  $scope.return_result_list;
        $scope.sub_criteria_id = 0;
        $scope.criteria_id = 0;
        $scope.score = false;
        $scope.comment = false;
        $scope.criteria = false;
        $scope.assessment_open = false;
        $scope.main_total = 0;
        }
        else
        {
            assessment_result.is_confirmed = $scope.previuos_status;
        }
    };

    $scope.saveResults = function (criteria) {
        $scope.criteria_id = 0;
        $scope.report_list = false;
        $scope.main_total = 0;
        angular.forEach($scope.criteriaOptions, function (value, index) {
            $scope.return_subtotal(value.id);
        });
        $scope.assessing = false;
    };

    $scope.save_comment = function (sub_criteria) {
        UpdateResultDetailsService.updateResultDetails({}, sub_criteria,
            function (data) {

            }
        );

        FilteredSubCriteriaOptionsService.query({
            criteria_id: $scope.criteria_id,
            result_id: $scope.assessment_result.id
        }, function (data) {
            $scope.subCriteriaOptions = data;
        });

        $scope.sub_criteria_id = 0;
        $scope.comment = false;
    };

    $scope.edit_comment = function (sub_criteria) {
        $scope.sub_criteria_id = sub_criteria.cas_assessment_sub_criteria.id;
        $scope.comment = true;
        $scope.score = false;
    };

    $scope.preview = function (){
        getSpServiceService.query({
            adminHierarchId: $scope.pisc_id,
            financialYearId: $scope.financial_year.id
        }, function (data) {
            $scope.subCriteriaOptions = data;
            if(data.length > 0){
                var url_sp = data[0].document_url;
                var url = 'https://' + $location.$$host + ':' + $location.$$port + url_sp;
                window.open(url, '_blank');
            }else{
                alert('No Strategic Plan Found Found')
            }
        });
    }

    $scope.exportAssessment = function (result,report_name,folder) {
        if($scope.assessment_open)
        {
            result.score_value = angular.copy($scope.main_total);

            if($scope.assessment_group === 1) {
                UpdateResultsService.updateResults({}, result,
                    function (data) {

                    }
                );
            }
        }

        // $scope.result_preview = true;
        // $scope.fileUrl  = "";
        // ExportReportService.get({
        //     id: result.id,
        //     report_name: report_name,
        //     folder: folder
        // }, function (data) {
        //     $scope.fileUrl = data.fileUrl;
        // });
        // $scope.preview = 0;

        var path = 'PlanRep/assessment/'+report_name;
        var report_type='pdf';
        $scope.params = {};
        $scope.jasperResourceUrl = 'https://planrep.pccb.go.tz/jasperserver/rest_v2/reports';
        $scope.params.id = result.id;
        $scope.params.period_id   = $scope.period_id;
        if ($scope.facility > 0) {
            $scope.params.facility_id = $scope.facility;
        }
        const qs = Object.keys($scope.params).filter((key)=>
        $scope.params[key] !== undefined && $scope.params[key] !== null).map((key)=> `${key}=${$scope.params [key]}`).join('&');
        window.open(`${$scope.jasperResourceUrl}/${path}.${report_type}?${qs}`, '_blank');
    };

    $scope.exportPreviewReport = function (id,report_url,table_id) {
        if(table_id>=13 && table_id<=29){
            $scope.result_preview = true;
            $scope.fileUrl  = "";
            //default params
            var period_id = $scope.period_id;
            var fund_source_id = $scope.reportParameter.fund_source_id;
            var facility = $scope.facility;
            var report_type='html';
            var admin_hierarchy_id = $scope.admin_hierarchy_id;
            var fy=_.where($scope.periods,{id:period_id});
            var financial_year_id = 16;
            // var financial_year_id = $scope.financial_year_id;

            $scope.params = {};
            $scope.jasperResourceUrl = 'https://planrep.pccb.go.tz/jasperserver/rest_v2/reports';
            $scope.params.admin_hierarchy_id = admin_hierarchy_id;
            $scope.params.financial_year_id = financial_year_id;
            $scope.params.fund_source_id = fund_source_id;
            $scope.params.budget_type = 'CURRENT';
            $scope.params.period_id   = period_id;
            if (facility > 0) {
                $scope.params.facility_id = facility;
            }
            const qs = Object.keys($scope.params).filter((key)=>
            $scope.params[key] !== undefined && $scope.params[key] !== null).map((key)=> `${key}=${$scope.params [key]}`).join('&');

        let assessment_report_url = `${$scope.jasperResourceUrl}${report_url}`;

        window.open(`${assessment_report_url}.${report_type}?${qs}`, '_blank');

        var data = {reportParameter: $scope.reportParameter, reportUrl: assessment_report_url};

        $scope.errorMessage = data;
        //send report parameters
        getJasperCasReportService.casReport(data, function (data) {
                $scope.fileUrl = data.report_url;
            // $scope.fileUrl = detailFrame;
            }, function (error){
                console.log(error);
            });
            $scope.preview = 0;

        $scope.previwedSbuCriteria.push($scope.currentSubCriteria);
        }else{
            $scope.result_preview = true;
            $scope.fileUrl  = "";
            //default params
            var period_id = $scope.period_id;
            var facility = $scope.facility;
            var admin_hierarchy_id = $scope.admin_hierarchy_id;
            var fy=_.where($scope.periods,{id:period_id});
            var financial_year_id = 16;
            //var financial_year_id = fy[0]['financial_year_id']; uncomment when undefined financial_year_id cleared

            $scope.reportParameter['admin_hierarchy_id'] = admin_hierarchy_id;
            $scope.reportParameter['financial_year_id']  = financial_year_id;
            $scope.reportParameter['table_id']  = table_id;
            $scope.reportParameter['period_id']  = period_id;
            $scope.reportParameter['facility_id']  = facility;
            $scope.reportParameter['is_assessment_report']  = true;

            var data = {reportParameter: $scope.reportParameter, report_url: report_url};

            $scope.errorMessage = data;
            //send report parameters
            getJasperCasReportService.casReport(data, function (data) {
                $scope.fileUrl = data.reportUrl;
            }, function (error){
                console.log(error);
            });

            $scope.preview = 0;
            $scope.previwedSbuCriteria.push($scope.currentSubCriteria);
        }
    }

    ;
    $scope.showReportParameters = function (id,report_name,parameters) {
        $scope.preview = id;
        $scope.preview_tittle = report_name;

        if(parameters === null || parameters === undefined) {
            parameters = "";
        }

        var parameterArray = parameters.split(',');
        var admin_hierarchy_id = $scope.admin_hierarchy_id;
        var inputData = {parameters: parameterArray,  admin_hierarchy_id:admin_hierarchy_id};
        // console.log(inputData);
        LoadReportParametersSources.getParams(inputData, function (data) {
            $scope.reportParamsData = data.parameter_data;
            if ($scope.reportParamsData.exchange_rate !== undefined) {
                $scope.reportParameter.exchange_rate = $scope.reportParamsData.exchange_rate;
            }
        }, function (error){
            console.log(error);
        });

        $scope.reportParameter = {};

    };

    //get interventions
    $scope.getIntervention = function (priority_area_id) {
        console.log($scope.reportParamsData.intervention_id);
        angular.forEach($scope.reportParamsData.intervention_id, function(value){
            console.log(value.id);
            if(value.id == priority_area_id){
                console.log(value.interventions);
                $scope.reportParamIntervention = value.interventions;
            }
        });
    };

    $scope.closePreview = function () {
        $scope.preview = 0;
    };

    $scope.close_preview_result = function () {
        $scope.result_preview = false;
    };

    $scope.openResults = function (category_version, assessment_group) {
        /**
        if($scope.param_category_id == 0){
            $scope.param_category_id = category_version;
            $scope.loadAssessmentCategories();
        } **/

        MyAssessmentResultsService.get({
            category_version_id: category_version.id,
            assessment_group: assessment_group,
            financial_year: $scope.financial_year
        }, function (data) {
            $scope.resultList = data;
            $scope.category_name = category_version.cas_assessment_category.name;
            $scope.minimum_passmark = category_version.minimum_passmark;
            $scope.category_version_id = category_version.id;
            $scope.group_name = $scope.resultList.region_name;
            console.log(data);
          //  $scope.result_list = true;
        });
      //  $scope.group_name = $scope.resultList.region_name;
        $scope.category_version = category_version;
        $scope.assessment_group = assessment_group;
        $scope.cas_assessment_category_version_id = category_version.id;
        $scope.result_list = true;
        $scope.other_levels = false;

    };

    $scope.openCriteria = function (category_version) {
     $scope.cas_plan_id = category_version.cas_assessment_category.cas_plan_id;
      if($scope.cas_plan_id===7)
      {
        $scope.isEditable = false;
        //load facility
        var user = angular.fromJson(localStorageService.get(localStorageKeys.USER));
        var sector_id = user['sector'].id;
        var admin_hierarchy_id = user['admin_hierarchy_id'];
        GetFacilityBySectorService.query({sector_id: sector_id, council: admin_hierarchy_id}, function(data){
          $scope.facilities = data;
        });
      }
        FilteredCasAssessmentCriteriaOptionService.query({category_version_id: category_version.id}, function (data) {
            $scope.criteriaOptions = data;
            $scope.assessment_group = 1;
        });
        $scope.category_name =  category_version.cas_assessment_category.name;
        $scope.minimum_passmark =  category_version.minimum_passmark;
        $scope.criteria = true;
        $scope.return_result_list = false;
        $scope.category_version_id =  category_version.id;
        $scope.category_version = category_version;
        $scope.returnCouncils();

      $scope.loadFacilityByType = function (item) {
       $scope.facilityByType = [];
        angular.forEach($scope.facilities,function(value,key){
          if(value.facility_type_id == item)
          {
            $scope.facilityByType.push(value);
          }
        });
      };

    };

    $scope.subCriteria = function (criteria, result_id) {

        FilteredSubCriteriaOptionsService.query({
            criteria_id: criteria.id,
            result_id: $scope.assessment_result.id
        }, function (data) {
            $scope.subCriteriaOptions = data;
        });
        $scope.report_list = false;
        $scope.criteria_name = criteria.name;
        $scope.criteria_id = criteria.id;
        $scope.showSubCriteria = true;
        $scope.assessing = true;
    };

    /**
     * function to load right
     * **/
    $scope.loadRights = function () {
        $scope.rights = localStorageService.get(localStorageKeys.RIGHT);
    }();

    $scope.hasPermission = function (permission) {
        if ($scope.rights !== undefined && $scope.rights !== null) {
            var rights = JSON.parse($scope.rights);
            return rights.indexOf(permission) > -1;
        }
        return false;
    };

}

CasAssessmentResultController.resolve = {

    CasAssessmentCriteriaOptionModel: function (CasAssessmentCategoryVersionService, $q) {
        var deferred = $q.defer();
        CasAssessmentCategoryVersionService.get({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};
