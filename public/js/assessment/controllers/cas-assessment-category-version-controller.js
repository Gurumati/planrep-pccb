function CasAssessmentCategoryVersionController($scope, CasAssessmentCategoryVersionModel, $uibModal, ConfirmDialogService,
                                         DeleteCasAssessmentCategoryVersionService,
                                         FinancialYearsService,
                                         PaginatedCasAssessmentCategoryVersionService) {

    $scope.versions = CasAssessmentCategoryVersionModel;
    $scope.title = "CAS_ASSESSMENT_CATEGORY_VERSIONS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedCasAssessmentCategoryVersionService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.versions = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_category_version/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllCasAssessmentCategoryService,AllCasAssessmentStateService, ReferenceDocument, CreateCasAssessmentCategoryVersionService) {

                 /** get financial year */
                FinancialYearsService.query({}, function(data){
                    $scope.financial_years = data;
                });

                AllCasAssessmentCategoryService.query(function (data) {
                    $scope.categories = data;
                });

                ReferenceDocument.query(function (data) {
                    $scope.ref_docs = data;
                });

                AllCasAssessmentStateService.query(function (data) {
                    $scope.states = data;
                });

                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateCasAssessmentCategoryVersionService.store({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.versions = data.versions;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_category_version/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllCasAssessmentCategoryService,AllCasAssessmentStateService,ReferenceDocument, UpdateCasAssessmentCategoryVersionService) {
                AllCasAssessmentCategoryService.query(function (data) {
                    $scope.categories = data;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });

                 /** get financial year */
                FinancialYearsService.query({}, function(data){
                    $scope.financial_years = data;
                });
                
                ReferenceDocument.query(function (data) {
                    $scope.ref_docs = data;
                });

                AllCasAssessmentStateService.query(function (data) {
                    $scope.states = data;
                });

                $scope.update = function () {
                    if ($scope.updateForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateCasAssessmentCategoryVersionService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.versions = data.versions;
                $scope.currentPage = $scope.versions.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteCasAssessmentCategoryVersionService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.versions = data.versions;
                        $scope.currentPage = $scope.versions.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_category_version/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedCasAssessmentCategoryVersionService,
                                  RestoreCasAssessmentCategoryVersionService, EmptyCasAssessmentCategoryVersionTrashService, PermanentDeleteCasAssessmentCategoryVersionService) {
                TrashedCasAssessmentCategoryVersionService.query(function (data) {
                    $scope.trashedVersions = data;
                });
                $scope.restoreCasAssessmentCategoryVersion = function (id) {
                    RestoreCasAssessmentCategoryVersionService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedVersions = data.trashedVersions;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteCasAssessmentCategoryVersionService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedVersions = data.trashedVersions;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyCasAssessmentCategoryVersionTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedVersions = data.trashedVersions;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.versions = data.versions;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

CasAssessmentCategoryVersionController.resolve = {
    CasAssessmentCategoryVersionModel: function (PaginatedCasAssessmentCategoryVersionService, $q) {
        var deferred = $q.defer();
        PaginatedCasAssessmentCategoryVersionService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};