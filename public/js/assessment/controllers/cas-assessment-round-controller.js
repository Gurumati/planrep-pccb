function CasAssessmentRoundController($scope,
                                      CASAssessmentRoundModel,
                                      $uibModal,
                                      ConfirmDialogService,
                                      CasAssessmentRoundsService,
                                      DeleteCasAssessmentRoundsService
                                      ) {

    $scope.casAssessmentRounds = CASAssessmentRoundModel;
    $scope.title = "CAS_ASSESSMENT_ROUNDS";

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_round/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,CreateCasAssessmentRoundsService) {

                $scope.casAssessmentRoundToCreate = {};

                $scope.store = function () {
                    if ($scope.createCasAssessmentRoundForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateCasAssessmentRoundsService.store( $scope.casAssessmentRoundToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.casAssessmentRounds = data.casAssessmentRounds;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (casAssessmentRoundToEdit) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_round/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,UpdateCasAssessmentRoundsService) {

                $scope.casAssessmentRoundToEdit = angular.copy(casAssessmentRoundToEdit);

                $scope.update = function () {

                    if ($scope.updateCasAssessmentRoundForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }

                    UpdateCasAssessmentRoundsService.update( $scope.casAssessmentRoundToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.casAssessmentRounds = data.casAssessmentRounds;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteCasAssessmentRoundsService.delete({id: id},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.casAssessmentRounds = data.casAssessmentRounds;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
}

CasAssessmentRoundController.resolve = {
    CASAssessmentRoundModel: function (CasAssessmentRoundsService, $q) {
        var deferred = $q.defer();
        CasAssessmentRoundsService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};
