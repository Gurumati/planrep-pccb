function CasAssessmentStateController($scope, CasAssessmentStateModel, $uibModal, ConfirmDialogService,
                                         DeleteCasAssessmentStateService,
                                PaginatedCasAssessmentStateService) {

    $scope.casAssessmentStates = CasAssessmentStateModel;
    $scope.title = "CAS_ASSESSMENT_STATES";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedCasAssessmentStateService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.casAssessmentStates = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_state/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateCasAssessmentStateService) {
                
                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateCasAssessmentStateService.store({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.casAssessmentStates = data.casAssessmentStates;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_state/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllCASPlanService, UpdateCasAssessmentStateService) {
                AllCASPlanService.query(function (data) {
                    $scope.cas_plans = data;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });
                $scope.update = function () {
                    UpdateCasAssessmentStateService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.casAssessmentStates = data.casAssessmentStates;
                $scope.currentPage = $scope.casAssessmentStates.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteCasAssessmentStateService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.casAssessmentStates = data.casAssessmentStates;
                        $scope.currentPage = $scope.casAssessmentStates.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_state/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedCasAssessmentStateService,
                                  RestoreCasAssessmentStateService, EmptyCasAssessmentStateTrashService, PermanentDeleteCasAssessmentStateService) {
                TrashedCasAssessmentStateService.query(function (data) {
                    $scope.trashedCasAssessmentStates = data;
                });
                $scope.restoreCasAssessmentState = function (id) {
                    RestoreCasAssessmentStateService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCasAssessmentStates = data.trashedCasAssessmentStates;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteCasAssessmentStateService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCasAssessmentStates = data.trashedCasAssessmentStates;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyCasAssessmentStateTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCasAssessmentStates = data.trashedCasAssessmentStates;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.casAssessmentStates = data.casAssessmentStates;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

CasAssessmentStateController.resolve = {
    CasAssessmentStateModel: function (PaginatedCasAssessmentStateService, $q) {
        var deferred = $q.defer();
        PaginatedCasAssessmentStateService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};