function CasAssessmentCriteriaOptionController($scope, CasAssessmentCategoryAllModel,
                                               $uibModal, ConfirmDialogService,
                                               DeleteCasAssessmentCriteriaOptionService,
                                               AllCasAssessmentCategoryVersionService,
                                               PaginatedCasAssessmentCriteriaOptionService) {

    $scope.casAssessmentCategories = CasAssessmentCategoryAllModel;
    $scope.title = "CAS_ASSESSMENT_CRITERIA_OPTIONS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedCasAssessmentCriteriaOptionService.get({page: $scope.currentPage, perPage: $scope.perPage, category_version_id: $scope.category_version_id}, function (data) {
            $scope.criteriaOptions = data;
        });
    };

    $scope.loadCriteriaOption = function (category_version_id) {
        PaginatedCasAssessmentCriteriaOptionService.get({page: $scope.currentPage, perPage: $scope.perPage,category_version_id: category_version_id }, function (data) {
            $scope.criteriaOptions = data;
        });
    };

    $scope.create = function (category_version_id) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_criteria_option/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllCasAssessmentCategoryVersionService, CreateCasAssessmentCriteriaOptionService) {

                AllCasAssessmentCategoryVersionService.query(function (data) {
                    $scope.categoryVersions = data;
                });

                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    $scope.createDataModel.cas_assessment_category_version_id = category_version_id;
                    console.log($scope.createDataModel);
                    CreateCasAssessmentCriteriaOptionService.store({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.criteriaOptions = data.criteriaOptions;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_criteria_option/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllCasAssessmentCategoryVersionService, UpdateCasAssessmentCriteriaOptionService) {

                AllCasAssessmentCategoryVersionService.query(function (data) {
                    $scope.categoryVersions = data;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });

                $scope.update = function () {
                    if ($scope.updateForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateCasAssessmentCriteriaOptionService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.criteriaOptions = data.criteriaOptions;
                $scope.currentPage = $scope.criteriaOptions.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteCasAssessmentCriteriaOptionService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.criteriaOptions = data.criteriaOptions;
                        $scope.currentPage = $scope.criteriaOptions.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_criteria_option/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance,
                                  TrashedCasAssessmentCriteriaOptionService,
                                  RestoreCasAssessmentCriteriaOptionService,
                                  EmptyCasAssessmentCriteriaOptionTrashService,
                                  PermanentDeleteCasAssessmentCriteriaOptionService) {
                TrashedCasAssessmentCriteriaOptionService.query(function (data) {
                    $scope.trashedCriteriaOptions = data;
                });
                $scope.restoreCasAssessmentCriteriaOption = function (id) {
                    RestoreCasAssessmentCriteriaOptionService.restore({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCriteriaOptions = data.trashedCriteriaOptions;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteCasAssessmentCriteriaOptionService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCriteriaOptions = data.trashedCriteriaOptions;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyCasAssessmentCriteriaOptionTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCriteriaOptions = data.trashedCriteriaOptions;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.criteriaOptions = data.criteriaOptions;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

CasAssessmentCriteriaOptionController.resolve = {
    CasAssessmentCategoryAllModel: function (AllCasAssessmentCategoryVersionService, $q) {
        var deferred = $q.defer();
        AllCasAssessmentCategoryVersionService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};