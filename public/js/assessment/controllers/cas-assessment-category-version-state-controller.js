function CasAssessmentCategoryVersionStateController($scope, CasAssessmentCategoryVersionStateModel, $uibModal, ConfirmDialogService,
                                         DeleteCasAssessmentCategoryVersionStateService,
                                         PaginatedCasAssessmentCategoryVersionStateService) {

    $scope.versionStates = CasAssessmentCategoryVersionStateModel;
    $scope.title = "CAS_ASSESSMENT_CATEGORY_VERSION_STATES";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedCasAssessmentCategoryVersionStateService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.versionStates = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_category_version_state/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllCasAssessmentCategoryVersionService,AllCasAssessmentStateService, CreateCasAssessmentCategoryVersionStateService) {

                AllCasAssessmentCategoryVersionService.query(function (data) {
                    $scope.versions = data;
                });

                AllCasAssessmentStateService.query(function (data) {
                    $scope.states = data;
                });

                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateCasAssessmentCategoryVersionStateService.store({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.versionStates = data.versionStates;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_category_version_state/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllCasAssessmentCategoryVersionService,AllCasAssessmentStateService, UpdateCasAssessmentCategoryVersionStateService) {

                AllCasAssessmentCategoryVersionService.query(function (data) {
                    $scope.versions = data;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });

                AllCasAssessmentStateService.query(function (data) {
                    $scope.states = data;
                });


                $scope.update = function () {
                    if ($scope.updateForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateCasAssessmentCategoryVersionStateService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.versionStates = data.versionStates;
                $scope.currentPage = $scope.versionStates.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteCasAssessmentCategoryVersionStateService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.versionStates = data.versionStates;
                        $scope.currentPage = $scope.versionStates.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/assessment/cas_assessment_category_version_state/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedCasAssessmentCategoryVersionStateService,
                                  RestoreCasAssessmentCategoryVersionStateService, EmptyCasAssessmentCategoryVersionStateTrashService, PermanentDeleteCasAssessmentCategoryVersionStateService) {
                TrashedCasAssessmentCategoryVersionStateService.query(function (data) {
                    $scope.trashedVersionStates = data;
                });
                $scope.restoreCasAssessmentCategoryVersionState = function (id) {
                    RestoreCasAssessmentCategoryVersionStateService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedVersionStates = data.trashedVersionStates;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteCasAssessmentCategoryVersionStateService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedVersionStates = data.trashedVersionStates;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyCasAssessmentCategoryVersionStateTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedVersionStates = data.trashedVersionStates;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.versionStates = data.versionStates;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

CasAssessmentCategoryVersionStateController.resolve = {
    CasAssessmentCategoryVersionStateModel: function (PaginatedCasAssessmentCategoryVersionStateService, $q) {
        var deferred = $q.defer();
        PaginatedCasAssessmentCategoryVersionStateService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};