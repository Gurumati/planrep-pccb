(function () {

    "use strict";

    angular.module('master-module')
        .directive('budgetInfo', ['$route', 'selectedPlan', function ($route, selectedPlan) {
            return {
                restrict: 'E',
                scope: {
                    onLoad: '&onLoad'
                },
                templateUrl: '/pages/directives/budget-info.html',
                link: function (scope, elem, attr) {
                    scope.routeParams = $route.current.params;
                    var mtefSectionId = (scope.routeParams.mtefSectionId !== undefined) ? scope.routeParams.mtefSectionId : 0;
                    scope.isLoading = true;
                    /**
                     * Mtef Section
                     */
                    (function () {
                        selectedPlan.get(mtefSectionId).then(function (data) {
                            selectedPlan.set(data);
                            scope.isLoading = false;
                            scope.budgetInfo = data.plan;
                            scope.onLoad({budgetInfo: data.plan});
                        }).catch(function (error) {
                            console.log(error);
                            scope.isLoading = false;
                        });
                    })();

                }
            };

        }])
        .directive('modalMovable', ['$document',
            function ($document) {
                return {
                    restrict: 'AC',
                    link: function (scope, iElement, iAttrs) {
                        var startX = 0,
                            startY = 0,
                            x = 0,
                            y = 0;

                        var dialogWrapper = iElement.parent();

                        dialogWrapper.css({
                            position: 'relative'
                        });

                        dialogWrapper.on('mousedown', function (event) {
                            // Prevent default dragging of selected content
                            event.preventDefault();
                            startX = event.pageX - x;
                            startY = event.pageY - y;
                            $document.on('mousemove', mousemove);
                            $document.on('mouseup', mouseup);
                        });

                        function mousemove(event) {
                            y = event.pageY - startY;
                            x = event.pageX - startX;
                            dialogWrapper.css({
                                top: y + 'px',
                                left: x + 'px'
                            });
                        }

                        function mouseup() {
                            $document.unbind('mousemove', mousemove);
                            $document.unbind('mouseup', mouseup);
                        }
                    }
                };
            }
        ])
        .directive('breadcrumb', function () {
            return {
                restrict: 'E',
                scope: {
                    title: '@',
                },
                templateUrl: '/pages/breadcrumb.html'
            };
        })
        .directive('breadcrumbLoading', function () {
            return {
                restrict: 'E',
                scope: {
                    title: '@',
                },
                templateUrl: '/pages/breadcrumb-loading.html'
            };
        })
        .directive('prProgress', function () {
            return {
                restrict: 'E',
                scope: {
                    title: '@',
                },
                template: '<div class="pr-progress"><div class="indeterminate"></div></div>'
            };
        })
        .directive("filesInput", function () {
            return {
                require: "ngModel",
                link: function postLink(scope, elem, attrs, ngModel) {
                    elem.on("change", function (e) {
                        var files = elem[0].files;
                        ngModel.$setViewValue(files);
                    });
                }
            };
        })
        .directive("perPage", function () {
            return {
                restrict: "E",
                templateUrl: '/pages/per-page.html',
                require: 'ngModel',
                link: function (scope, element, attrs, ctrl) {
                    scope.perPageNumberChange = function (perPageNumber) {
                        ctrl.$setViewValue(perPageNumber);
                    };
                    scope.perPageOptions = [10, 20, 25, 50, 100, 250, 500, 1000];
                    //scope.perPageOptions = [10, 20, 25, 50, 100, 250, 500,1000,2000,5000,10000,20000,50000];
                    scope.perPageNumber = scope.perPageOptions[0];
                }
            };
        })
        .directive('treeSelect', ['$compile', function ($compile) {
            return {
                restrict: 'A',
                scope: {data: "=data", ngModel: '='},
                replace: true,
                link: function (scope, element, attrs) {
                    var opt = '<select ng-model="ngModel" class="select-search form-control" id="reference">';

                    function fillWithChildren(parents, level) {
                        angular.forEach(parents, function (value, key) {
                            if (value.children !== undefined && value.children.length > 0) {
                                opt = opt + '<option disabled  ng-value="' + value.id + '">' + level + value.name + '</option>';
                                level = level + '&nbsp;&nbsp;';
                                fillWithChildren(value.children, level);
                            }
                            else {
                                opt = opt + '<option ng-value="' + value.id + '">' + level + value.name + '</option>';
                            }
                        });
                    }

                    fillWithChildren(scope.data, '&nbsp;');
                    opt = opt + '</select>';
                    element.append($compile(opt)(scope));
                }
            };
        }])
        .directive("totalPages", function () {
            return {
                restrict: "E",
                template: '<pre ng-if="numOfPages" class="total-pages" style="max-width:120px;background:#fff">Page: {{currentPage}} of {{numOfPages}}</pre>',
                link: function (scope, element, attrs, ctrl) {
                    scope.currentPage = attrs.currentPage;
                    scope.totalItems = attrs.totalItems;
                    scope.perPage = attrs.perPage;
                    scope.numOfPages = Math.ceil(parseInt(scope.totalItems, 10) / parseInt(scope.perPage, 10));
                }
            };
        })
        .directive('numberToCurrency', ['$filter', function ($filter) {
            return {
                require: 'ngModel',
                link: function (scope, element, attrs, ngModel) {

                    function toNumber(value) {
                        if (value !== undefined && value !== '') {
                            var x = value.replace(/,/g, '');
                            // console.log(x);
                            x = x.trim();
                            return parseFloat(x);
                        }
                        else {
                            return null;
                        }
                    }

                    function formatter(value) {
                        if (value !== undefined && value !== '') {
                            return $filter('currency')(value, '', 2);
                        }
                        else {
                            return null;
                        }
                    }

                    ngModel.$parsers.push(function (value) {
                        return toNumber(value);
                    });
                    ngModel.$formatters.push(function (value) {
                        return formatter(value);
                    });
                    element.bind('focus', function () {
                        var val = element.val();
                        element.val(toNumber(val));
                    });

                    element.bind('blur', function () {
                        element.val(formatter(element.val()));
                    });
                }
            };
        }])
        .directive('numberToPercentage', ['$filter', function ($filter) {
            return {
                require: 'ngModel',
                link: function (scope, element, attrs, ngModel) {

                    function toNumber(value) {
                        if (value !== undefined && value !== '') {
                            var x = value.replace(/%/g, '');
                            x = x.trim();
                            return parseFloat(x);
                        }
                        else {
                            return null;
                        }
                    }

                    function formatter(value) {
                        if (value !== undefined && value !== '') {
                            return $filter('number')(value, 2) + '%';
                        }
                        else {
                            return null;
                        }
                    }

                    ngModel.$parsers.push(function (value) {
                        return toNumber(value);
                    });
                    ngModel.$formatters.push(function (value) {
                        return formatter(value);
                    });
                    element.bind('focus', function () {
                        var val = element.val();
                        element.val(toNumber(val));
                    });

                    element.bind('blur', function () {
                        element.val(formatter(element.val()));
                    });
                }
            };
        }])
        .directive('validNumber', function () {
            return {
                require: '?ngModel',
                link: function (scope, element, attrs, ngModelCtrl) {
                    if (!ngModelCtrl) {
                        return;
                    }

                    ngModelCtrl.$parsers.push(function (val) {
                        if (angular.isUndefined(val)) {
                            var val = '';
                        }

                        var clean = val.replace(/[^-0-9\.]/g, '');
                        var negativeCheck = clean.split('-');
                        var decimalCheck = clean.split('.');
                        if (!angular.isUndefined(negativeCheck[1])) {
                            negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                            clean = negativeCheck[0] + '-' + negativeCheck[1];
                            if (negativeCheck[0].length > 0) {
                                clean = negativeCheck[0];
                            }

                        }

                        if (!angular.isUndefined(decimalCheck[1])) {
                            decimalCheck[1] = decimalCheck[1].slice(0, 2);
                            clean = decimalCheck[0] + '.' + decimalCheck[1];
                        }

                        if (val !== clean) {
                            ngModelCtrl.$setViewValue(clean);
                            ngModelCtrl.$render();
                        }
                        return clean;
                    });

                    element.bind('keypress', function (event) {
                        if (event.keyCode === 32) {
                            event.preventDefault();
                        }
                    });
                }
            };
        })
        .directive('numbersOnly', function () {
            return {
                require: 'ngModel',
                link: function (scope, element, attr, ngModelCtrl) {
                    function fromUser(text) {
                        if (text) {
                            var transformedInput = text.replace(/[^0-9-]/g, '');
                            if (transformedInput !== text) {
                                ngModelCtrl.$setViewValue(transformedInput);
                                ngModelCtrl.$render();
                            }
                            return transformedInput;
                        }
                        return undefined;
                    }

                    ngModelCtrl.$parsers.push(fromUser);
                }
            };
        });

})();