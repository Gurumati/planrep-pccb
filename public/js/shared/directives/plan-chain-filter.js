(function () {
  "use strict";

  angular.module("master-module").directive("planChainFilter", [
    "$route",
    "PlanChainTypesService",
    "PlanChainsService",
    "selectedPlanChain",
    function (
      $route,
      PlanChainTypesService,
      PlanChainsService,
      selectedPlanChain
    ) {
      return {
        restrict: "E",
        scope: {
          onChange: "&onChange",
        },
        templateUrl: "/pages/directives/plan-chain-filter.html",
        link: function (scope, elem, attr) {
          var routeParams = $route.current.params;
          (function () {
            PlanChainTypesService.query(function (data) {
              scope.planChainTypes = data;
              getTopPlanChain();
            });
          })();

          function getTopPlanChain() {
            PlanChainsService.getTopLevel(
              {
                mtefSectionId: routeParams.mtefSectionId,
                reallocation:
                  routeParams.realoc == undefined ? false : routeParams.realoc,
              },
              function (data) {
                scope.topPlanChains = data.planChains;
                var p1 = selectedPlanChain.get(1);
                if (p1 !== undefined) {
                  scope.p1 = p1 !== undefined ? p1.id : undefined;
                  scope.loadChildren(p1.id, 1);
                }
              }
            );
          }

          scope.loadChildren = function (p, level) {
            if (p !== undefined && p !== null) {
              scope.selectedPlanChainId = p;
              scope.filterChanged();
              getPlanChainChildren(p, level);
            }
          };

          function getPlanChainChildren(pId, level) {
            PlanChainsService.getChildren(
              {
                parentId: pId,
                mtefSectionId: routeParams.mtefSectionId,
              },
              function (data) {
                scope.planChainTypes[level].children = data.planChains;
                if (level == 1) {
                  var p2 = selectedPlanChain.get(2);
                  scope.p2 = p2 !== undefined ? p2.id : undefined;
                  scope.selectedPlanChainId = p2.id;
                }
                scope.filterChanged();
              }
            );
          }

          scope.filterChanged = function () {
            var selectedPlanChainId = scope.selectedPlanChainId
              ? scope.selectedPlanChainId
              : undefined;
            scope.onChange({
              selectedPlanChainId: selectedPlanChainId,
            });
          };
        },
      };
    },
  ]);
})();
