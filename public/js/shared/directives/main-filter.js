(function() {

    "use strict";

    angular.module('master-module')
        .directive('mainFilter', ['$route','$location','AdminHierarchiesService', 'SectionsByLevel', 'FinancialYearsService','BudgetType',
                           function ($route,$location,AdminHierarchiesService, SectionsByLevel,FinancialYearsService,BudgetType) {
            return {
                restrict: 'E',
                scope: {
                    onChange: '&onChange'
                },
                templateUrl: '/pages/directives/main-filter.html',
                link: function (scope, elem, attr) {
                    scope.dontShow = (attr.dontShow) ? attr.dontShow : [];
                    var params = $route.current.params;
                    var queryParams = $location.search();

                    scope.errorMessage = undefined;

                    (function () {
                        AdminHierarchiesService.getUserAdminHierarchyLevels({noLoader: true}, function (data) {
                                scope.adminLevels = data.adminLevels;
                                scope.adminLevel = scope.adminLevels[0];
                                scope.sectionLevels = data.sectionLevels;
                                scope.sectionLevel = scope.sectionLevels[0];
                                getPlanTypes();
                            },
                            function (error) {

                            });


                    })();
                    function getPlanTypes() {
                       if(params !== undefined && params.budgetType !== undefined){
                           scope.budgetTypes = [params.budgetType];
                           scope.selectedBudgetType = params.budgetType;
                           scope.getBudgetTypeFinancialYear();
                       }
                       else {
                           BudgetType.get({
                            financialYearId: queryParams.financialYearId
                           },function (data) {
                               scope.budgetTypes = data.budgetTypes;
                               scope.selectedBudgetType = data.budgetTypes[0];
                               scope.getBudgetTypeFinancialYear();
                           });
                       }
                    }

                    scope.getBudgetTypeFinancialYear = function() {
                        FinancialYearsService.getByBudgetType({budgetType : scope.selectedBudgetType},function (data) {
                            if(data.financialYear !== null) {
                                scope.financialYears = [data.financialYear];
                                scope.selectedFinancialYear = data.financialYear;
                                scope.previousFinancialYear = data.previousFinancialYear;
                                getUserAdminHierarchy();
                                scope.loadSectionsByLevel();
                            }else{
                                scope.errorMessage = 'NO_FINANCIAL_YEAR_FOUND';
                            }
                        },function (error) {
                             scope.errorMessage = error.data.errorMessage;
                        });
                    };
                    scope.budgetTypeChanged = function () {
                        scope.financialYears = [];
                        scope.selectedFinancialYear = undefined;
                        scope.errorMessage = undefined;
                        FinancialYearsService.getByBudgetType({budgetType : scope.selectedBudgetType},function (data) {
                            if(data.financialYear !== null) {
                                scope.financialYears = [data.financialYear];
                                scope.selectedFinancialYear = data.financialYear;
                                getUserAdminHierarchy();
                                scope.loadSectionsByLevel();
                            }else{
                                scope.errorMessage = 'NO_FINANCIAL_YEAR_FOUND';
                            }
                            scope.filterChanged();
                        },function (error) {
                             scope.errorMessage = error.data.errorMessage;
                        });
                    };

                    scope.getFinancialYears = function() {
                        FinancialYearsService.getUsableFinancialYears(function (data) {
                            scope.financialYears = data.financialYears;
                            scope.selectedFinancialYear = data.financialYears[0];
                            getUserAdminHierarchy();
                            scope.loadSectionsByLevel();
                        }, function(error) {
                         });
                    };

                    function getUserAdminHierarchy() {
                        AdminHierarchiesService.getUserAdminHierarchy({noLoader: true}, function (data) {
                                 scope.userAdminHierarchy = scope.a1 = data;
                                scope.userAdminHierarchyCopy = scope.selectedAdminHierarchy = angular.copy(scope.userAdminHierarchy);
                                scope.loadChildren(scope.userAdminHierarchy);
                                scope.filterChanged();
                            },
                            function (error) {

                            });
                    }

                    scope.loadSectionsByLevel = function () {
                        scope.admin = scope.selectedAdminHierarchy==undefined?0:scope.selectedAdminHierarchy.id;
                        SectionsByLevel.query({levelId: scope.sectionLevel.id, noLoader: true,adminHierarchyId:scope.admin}, function (data) {
                                scope.sections = data;
                                scope.selectedSection =scope.sections[0];
                                scope.filterChanged();
                            },
                            function (error) {
                               console.log(error);
                            });
                    };
                    scope.searchQueryChanged = function () {
                        _.debounce(scope.filterChanged(),1000);
                    };
                    scope.loadChildren = function (a) {
                         if (a !== undefined && a !== null) {
                            scope.selectedAdminHierarchy = a;
                            scope.filterChanged();
                            if (a.hierarchy_position <= 3) {
                                getAdminHierarchyChildren(a);
                            }
                        }
                    };

                    function getAdminHierarchyChildren(a) {
                         AdminHierarchiesService.getAdminHierarchyChildren({adminHierarchyId: a.id}, function (data) {
                             a.children = data.adminHierarchies;
                        });
                    }

                    scope.resetHierarchy = function () {
                        scope.userAdminHierarchy = angular.copy($scope.userAdminHierarchyCopy);
                    };

                    scope.filterChanged = function () {
                         scope.selectedFilters = {
                            selectedBudgetType : (scope.selectedBudgetType) ? scope.selectedBudgetType : undefined,
                            selectedFinancialYearId : (scope.selectedFinancialYear) ? scope.selectedFinancialYear.id : undefined,
                            selectedFinancialYearName : (scope.selectedFinancialYear) ? scope.selectedFinancialYear.name : undefined,
                            selectedAdminHierarchyId : (scope.selectedAdminHierarchy) ? scope.selectedAdminHierarchy.id : undefined,
                            selectedAdminHierarchyName : (scope.selectedAdminHierarchy) ? scope.selectedAdminHierarchy.name : undefined,
                            selectedAdminHierarchyLevelPosition : (scope.selectedAdminHierarchy) ? scope.selectedAdminHierarchy.hierarchy_position : undefined,
                            selectedAdminHierarchyLevelId : (scope.selectedAdminHierarchy) ? scope.selectedAdminHierarchy.admin_hierarchy_level_id : undefined,
                            selectedSectionLevelPosition : (scope.sectionLevel) ? scope.sectionLevel.hierarchy_position : undefined,
                            selectedSectionLevelId : (scope.sectionLevel) ? scope.sectionLevel.id : undefined,
                            selectedSectionId : (scope.selectedSection) ? scope.selectedSection.id : undefined,
                            selectedSectionName : (scope.selectedSection) ? scope.selectedSection.name : undefined,
                            searchQuery : (scope.searchQuery) ? scope.searchQuery : undefined
                        };
                        scope.onChange({filter: scope.selectedFilters});

                    };
                }
            };

        }]);

})();
