function GroupFinancialYearValueController($scope, GroupFinancialYearValueModel, $uibModal,
                                     ConfirmDialogService, DeleteGroupFinancialYearValueService, PaginatedGroupFinancialYearValueService) {

    $scope.groupFinancialYearValues = GroupFinancialYearValueModel;
    $scope.title = "BUDGET_DISTRIBUTION_CONDITION_GROUP_FINANCIAL_YEAR_VALUES";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedGroupFinancialYearValueService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.groupFinancialYearValues = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/group_financial_year_value/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllFinancialYearService,AllBdcGroupService, CreateGroupFinancialYearValueService) {
                $scope.groupFinancialYearValueToCreate = {};

                AllFinancialYearService.query(function (data) {
                    $scope.financialYears = data;
                });

                AllBdcGroupService.query(function (data) {
                    $scope.bdcGroups = data;
                });

                $scope.store = function () {
                    if ($scope.createGroupFinancialYearValueForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateGroupFinancialYearValueService.store({perPage: $scope.perPage}, $scope.groupFinancialYearValueToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.groupFinancialYearValues = data.groupFinancialYearValues;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (groupFinancialYearValueToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/group_financial_year_value/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllFinancialYearService,AllBdcGroupService, UpdateGroupFinancialYearValueService) {

                AllFinancialYearService.query(function (data) {
                    $scope.financialYears = data;
                    $scope.groupFinancialYearValueToEdit = angular.copy(groupFinancialYearValueToEdit);
                });

                AllBdcGroupService.query(function (data) {
                    $scope.bdcGroups = data;
                });
                
                $scope.update = function () {
                    if ($scope.updateGroupFinancialYearValueForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateGroupFinancialYearValueService.update({page: currentPage, perPage: perPage}, $scope.groupFinancialYearValueToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.groupFinancialYearValues = data.groupFinancialYearValues;
                $scope.currentPage = $scope.groupFinancialYearValues.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('CONFIRM_DELETE', 'Are you sure you want to trash this record?').then(function () {
                DeleteGroupFinancialYearValueService.delete({group_financial_year_value_id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.groupFinancialYearValues = data.groupFinancialYearValues;
                        $scope.currentPage = $scope.groupFinancialYearValues.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/group_financial_year_value/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedGroupFinancialYearValueService,
                                  RestoreGroupFinancialYearValueService,EmptyGroupFinancialYearValueTrashService, PermanentDeleteGroupFinancialYearValueService) {
                TrashedGroupFinancialYearValueService.query(function (data) {
                    $scope.trashedGroupFinancialYearValues = data;
                });
                $scope.restoreGroupFinancialYearValue = function (id) {
                    RestoreGroupFinancialYearValueService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedGroupFinancialYearValues = data.trashedGroupFinancialYearValues;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteGroupFinancialYearValueService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedGroupFinancialYearValues = data.trashedGroupFinancialYearValues;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyGroupFinancialYearValueTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedGroupFinancialYearValues = data.trashedGroupFinancialYearValues;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.groupFinancialYearValues = data.groupFinancialYearValues;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

GroupFinancialYearValueController.resolve = {
    GroupFinancialYearValueModel: function (PaginatedGroupFinancialYearValueService, $q) {
        var deferred = $q.defer();
        PaginatedGroupFinancialYearValueService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};