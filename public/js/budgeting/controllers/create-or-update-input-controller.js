(function () {
    'use strict';

    angular.module('budgeting-module').controller('CreateOrUpdateInputController', CreateOrUpdateInputController);
    CreateOrUpdateInputController.$inject = ['$scope', '$q', '$uibModalInstance', 'activity', 'input', 'params', 'periods', 'activityPeriods', 'units', 'procurementTypes', 'forwardFinancialYears', 'ExpenditureGsfCodeService', 'ConfirmDialogService', 'ActivityFacilityFundSourceInput'];

    function CreateOrUpdateInputController($scope, $q, $uibModalInstance, activity, input, params, periods, activityPeriods, units, procurementTypes, forwardFinancialYears, ExpenditureGsfCodeService, ConfirmDialogService, ActivityFacilityFundSourceInput) {

        $scope.units = units;
        $scope.procurementTypes = procurementTypes;
        $scope.periods = periods;
        $scope.forwardFinancialYears = forwardFinancialYears;
        $scope.showAddForm = true;
        $scope.gfsToExclude = [];
        $scope.budgetType = params.budgetType;
        $scope.selectedFundSourceName = params.selectedFundSourceName;
        $scope.addedInputs = [];
        $scope.isRealloc = params.isRealloc;
        $scope.activityPeriods = activityPeriods;
        $scope.activity = activity;

        function initiateInput() {
            $scope.formHasErrors = false;
            $scope.inputToCreate = {
                activity_facility_fund_source_id: params.selectedActivityFacilityFundSourceId,
                has_breakdown: false,
                is_in_kind: false,
                is_procurement: false,
                input_breakdowns: [],
                input_forwards: [],
                budget_type: $scope.budgetType
            };

            initForwards();
            if ($scope.budgetType === 'REALLOCATION') {
                $scope.inputToCreate.quantity = 0;
                $scope.inputToCreate.unit_price = 0;
                $scope.inputToCreate.frequency = 0;
            }
            if ($scope.isRealloc) {
                $scope.inputToCreate.unit_price = 0.00;
            }
            setCeiling();

        }

        function setCeiling() {
            var totalAdded = 0;
            angular.forEach($scope.addedInputs, function (i) {
                totalAdded = totalAdded + (i.unit_price * i.quantity * i.frequency);
            });
            $scope.selectedFundSourceBalance = parseFloat(params.selectedFundSourceBalance.toFixed(2)) - parseFloat(totalAdded.toFixed(2));
        }

        $scope.getPeriodValue = function (id) {
            if (undefined === input) {
                return undefined;
            }
            var exist = _.findWhere(input.activity_periods, {
                period_id: id
            });
            if (exist) {
                return parseInt(exist.percentage, 10);
            } else {
                return undefined;
            }
        };

        $scope.getTotalPercentage = function (periods) {
            var totalPerc = 0;
            angular.forEach(periods, function (p) {
                if (p !== undefined) {
                    totalPerc = totalPerc + parseInt(p, 10);
                }
            });
            return totalPerc;
        };

        function initForwards() {
            if ($scope.budgetType === 'CURRENT' || $scope.budgetType === 'APPROVED') {
                angular.forEach($scope.forwardFinancialYears, function (year, index) {
                    $scope.inputToCreate.input_forwards[index] = {};
                    $scope.inputToCreate.input_forwards[index]['financial_year_id'] = year.id;
                    $scope.inputToCreate.input_forwards[index]['quantity'] = getForwardedQuantity(year.id);
                    $scope.inputToCreate.input_forwards[index]['frequency'] = getForwardedFrequency(year.id);
                });
                $scope.formHasErrors = false;
            }
        }

        if (input === undefined) {
            initiateInput();
            $scope.inputs = [];
            $scope.editMode = false;
        } else {
            $scope.inputToCreate = input;
            $scope.editMode = true;
          //  initForwards();
            $scope.selectedFundSourceBalance = parseFloat((params.selectedFundSourceBalance + (input.quantity * input.frequency * input.unit_price)).toFixed(2));
        }

        $scope.getTotalInput = function (quantity, frequency, unit_price) {
            return parseFloat((quantity * frequency * unit_price).toFixed(2));
        };

        $scope.searchGfsCode = function ($query) {
            var deferred = $q.defer();
            if ($query !== undefined && $query !== '' && $query.length >= 1) {
                $scope.gfsCodeLoading = true;
                var gfsCodesToExclude = {
                    'gfsCodesIdsToExclude': $scope.gfsToExclude
                };
                ExpenditureGsfCodeService.search({
                        searchQuery: $query,
                        noLoader: true,
                        activityFacilityFundSourceId: params.selectedActivityFacilityFundSourceId
                    },
                    gfsCodesToExclude,
                    function (data) {
                        $scope.gfsCodeLoading = false;
                        deferred.resolve(data.gfsCodes);
                    },
                    function (error) {
                        $scope.gfsCodeLoading = false;
                    });
            } else {
                deferred.resolve([]);
            }
            return deferred.promise;
        };

        // $scope.gfsCodeChanged = function (gfs) {
        //     if (null !== gfs && gfs.gfs_code_sub_category.is_procurement) {
        //         $scope.inputToCreate.is_procurement = true;
        //     } else {
        //         $scope.inputToCreate.is_procurement = false;
        //     }
        // };

      // Govella
        $scope.gfsCodeChanged = function (gfs) {
          $scope.procurement_type = {
              id: gfs.procurement_type.id,
              name: gfs.procurement_type.name
          };
        $scope.selectedValue = $scope.procurement_type.id;

          if (gfs !== null && gfs.is_procurement) {
            $scope.inputToCreate.is_procurement = true;
          } else {
              $scope.inputToCreate.is_procurement = false;
          }
      };

      // end of Govella

        $scope.inActivity = function (periodId) {
            var exist = _.findWhere(activity.periods, {
                period_id: periodId
            });
            if (exist) {
                return true;
            } else {
                return false;
            }
        };

        $scope.breakDownToAdd = {};
        $scope.addBreakDown = function () {

            var breakDownToAddCopy = angular.copy($scope.breakDownToAdd);
            $scope.inputToCreate.input_breakdowns.push(breakDownToAddCopy);
            $scope.breakDownToAdd = {};

            $scope.totalToBreakDown = $scope.inputToCreate.quantity*$scope.inputToCreate.frequency*$scope.inputToCreate.unit_price;
            $scope.usedAmount = $scope.getBreakDownTotal();

            $scope.breakDownBalance = $scope.totalToBreakDown - $scope.getBreakDownTotal();
        };

        $scope.deleteBreakDown = function (brk) {
            var index = $scope.inputToCreate.input_breakdowns.indexOf(brk);
            $scope.inputToCreate.input_breakdowns.splice(index, 1);
        };
        $scope.getBreakDownTotal = function () {
            var total = 0;
            if ($scope.inputToCreate.has_breakdown && $scope.inputToCreate.input_breakdowns !== undefined) {
                angular.forEach($scope.inputToCreate.input_breakdowns, function (b) {
                    total = total + (b.quantity * b.unit_price * b.frequency);
                });
            }
            return total.toFixed(2);
        };

        $scope.save = function (andContinue) {
            if ($scope.createInputForm.$invalid) {
                $scope.formHasErrors = true;
                return;
            }
            ActivityFacilityFundSourceInput.createOrUpdate({},
                Object.assign({},
                    $scope.inputToCreate, {
                        mtef_section_id: params.mtefSectionId,
                        activity_id: params.activityId,
                        gfs_code_id: $scope.inputToCreate.gfs_code.id,
                        periods: $scope.inputToCreate.periods,
                        gfs_code: undefined,
                        procurement_type: undefined,
                        unit: undefined,
                        input_comments: undefined
                    }),
                function (data) {
                    if (!andContinue) {
                        $uibModalInstance.close(data);
                    } else {
                        $scope.addedInputs.push(data.addedInput);
                        $scope.newMode = true;
                        initiateInput();
                    }
                },
                function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.errors = error.data.errors;
                });
        };

        function getForwardedQuantity(fid) {
            var forwardq = _.findWhere($scope.inputToCreate.input_forwards, {
                financial_year_id: fid
            });
            if (forwardq) {
                return forwardq.quantity;
            } else {
                return 0;
            }
        }

        function getForwardedFrequency(fid) {
            var forwardf = _.findWhere($scope.inputToCreate.input_forwards, {
                financial_year_id: fid
            });
            if (forwardf) {
                return forwardf.frequency;
            } else {
                return 0;
            }
        }

        $scope.editInput = function (i) {
            // console.log(i);
        };
        $scope.deleteInput = function (input) {
            ConfirmDialogService.showConfirmDialog(
                    'TITLE_CONFIRM_INPUT',
                    'CONFIRM_DELETE')
                .then(function () {
                        ActivityFacilityFundSourceInput.delete({
                                id: input.id
                            },
                            function (data) {
                                $scope.successMessage = data.successMessage;
                                var i = _.indexOf($scope.addedInputs, input); // getting index.
                                $scope.addedInputs.splice(i, 1);
                                setCeiling();
                            },
                            function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                            }
                        );
                    },
                    function () {});
        };
        $scope.close = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }
})();
