function PeBudgetController($scope, AllPeBudgetModel, $uibModal,
                            ConfirmDialogService,
                            DeletePeBudgetService, GetAllPeBudgetService) {

    $scope.peBudgets = AllPeBudgetModel;
    $scope.title = "PE_BUDGETS";
    $scope.dateFormat = 'yyyy-MM-DD';

    $scope.currentPage = 1;

    $scope.pageChanged = function () {
        GetAllPeBudgetService.get({page: $scope.currentPage}, function (data) {
            $scope.peBudgets = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreatePeBudgetService, AllSectionService, AllMtefService) {
                $scope.peBudgetToCreate = {};
                //Function to store data and close modal when Create button clicked
                AllSectionService.query(function (data) {
                    $scope.sections = data;
                });
                AllMtefService.query(function (data) {
                    $scope.mtefs = data;
                });
                $scope.store = function () {
                    if ($scope.createPeBudgetForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreatePeBudgetService.store($scope.peBudgetToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.peBudgets = data.peBudgets;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (peBudgetToEdit, currentPage) {
        console.log(peBudgetToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: 'edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllSectionService, AllMtefService, UpdatePeBudgetService) {
                AllSectionService.query(function (data) {
                    $scope.sections = data;
                    $scope.peBudgetToEdit = angular.copy(peBudgetToEdit);
                });
                AllMtefService.query(function (data) {
                    $scope.mtefs = data;
                });
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    UpdatePeBudgetService.update({page: currentPage}, $scope.peBudgetToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.peBudgets = data.peBudgets;
                $scope.currentPage = $scope.peBudgets.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete PE Budget!', 'Are sure you want to delete this?').then(function () {
                //console.log("YES");
                DeletePeBudgetService.delete({pe_budget_id: id, page: currentPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.peBudgets = data.peBudgets;
                        $scope.currentPage = $scope.peBudgets.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    }
}

PeBudgetController.resolve = {
    AllPeBudgetModel: function (GetAllPeBudgetService, $q) {
        var deferred = $q.defer();
        GetAllPeBudgetService.get({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};