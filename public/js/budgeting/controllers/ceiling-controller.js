function CeilingController($scope, AllCeilingModel, $uibModal, $timeout, ToggleCeilingService,
  ConfirmDialogService, AllSectorService, RemoveCeilingSector, AddCeilingSector,
  DeleteCeilingService, GetAllCeilingService, DeleteCeilingSectorService, $animate) {

  $scope.ceilings = AllCeilingModel;
  $scope.title = "TITLE_CEILINGS";
  $scope.currentPage = 1;
  $scope.maxSize = 3;
  $animate.enabled(false);
  $scope.pageChanged = function () {
    GetAllCeilingService.get({ page: $scope.currentPage, perPage: $scope.perPage, searchQuery: $scope.searchQuery }, function (data) {
      $scope.ceilings = data;
    });
  };
  $scope.searchCeiling = function () {
    $scope.pageChanged();
  };

  $scope.enterToSearch = function (keyEvent) {
    if (keyEvent.charCode === 13) {
      $scope.pageChanged();
    }
  };
  AllSectorService.query(function (data) {
    $scope.sectors = data;
  });

  function arrayObjectIndexOf(myArray, searchTerm, property) {
    for (var i = 0, len = myArray.length; i < len; i++) {
      if (myArray[i][property] === searchTerm) { return i; }
    }
    return -1;
  }
  $scope.toggleSectorSelection = function (ceiling, sector) {
    var idx = arrayObjectIndexOf(ceiling.sectors, sector.id, 'id');
    if (idx !== -1) {
      RemoveCeilingSector.save({ ceilingId: ceiling.id, sectorId: sector.id }, function (data) {
        $scope.successMessage = data.successMessage;
        ceiling.sectors.splice(idx, 1);
        $timeout(function () {
          $scope.successMessage = undefined;
        }, 900);
      }, function (error) {
        $scope.errorMessage = error.data.errorMessage;
        $scope.pageChanged();
      });
    }
    else {
      AddCeilingSector.save({ ceilingId: ceiling.id, sectorId: sector.id }, function (data) {
        $scope.successMessage = data.successMessage;
        ceiling.sectors.push(sector);
        $timeout(function () {
          $scope.successMessage = undefined;
        }, 900);
      });
    }
  };
  $scope.toggleExtendsToFacility = function (cel) {
    AddCeilingSector.toggleExtendsToFacility({ ceilingId: cel.id, extendsToFacility: cel.extends_to_facility }, function (data) {

    });
  };
  $scope.toggleActive = function (ceilingId) {
    ToggleCeilingService.change({ ceilingId: ceilingId }, function (data) {
      $scope.pageChanged();
    }, function (error) {
      $scope.errorMessage = error.data.errorMessage;
      $scope.pageChanged();
    });
  };
  $scope.sectorExists = function (ceiling, sector) {
    var exist = _.findWhere(ceiling.sectors, { id: sector.id });
    if (exist) {
      return true;
    }
    else {
      return false;
    }
  };
  $scope.create = function () {
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/budgeting/ceiling/create.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, AllSectorService, AllMTEFSectionService, CreateCeilingService, AllGfsCodeService, AllBudgetClassService) {
        $scope.ceilingToCreate = {};
        //Function to store data and close modal when Create button clicked
        AllBudgetClassService.query(function (data) {
          $scope.budgetClasses = data;
        });

        AllMTEFSectionService.query(function (data) {
          $scope.mtefSections = data;
        });
        AllSectorService.query(function (data) {
          $scope.sectors = data;
        });
        AllGfsCodeService.query(function (data) {
          $scope.gfs_codes = data;
        });
        $scope.store = function () {
          if ($scope.createCeilingForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          CreateCeilingService.store({ perPage: $scope.perPage }, $scope.ceilingToCreate,
            function (data) {
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };

        /*$scope.checkBudgetClass = function (budget_class_id) {
            if(budget_class_id == 1){
                $('#BudgetClassProjectDIV').show();
            } else {
                $('#BudgetClassProjectDIV').hide();
            }
        };*/


        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
      //Service to create new financial year
      $scope.successMessage = data.successMessage;
      $scope.ceilings = data.ceilings;
      $scope.currentPage = data.current_page;
    },
      function () {
        //If modal is closed
        console.log('Modal dismissed at: ' + new Date());
      });

  };
  $scope.edit = function (ceilingToEdit, currentPage, perPage) {
    console.log(ceilingToEdit);
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/budgeting/ceiling/edit.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, AllGfsCodeService, AllBudgetClassService, UpdateCeilingService) {
        AllBudgetClassService.query(function (data) {
          $scope.budgetClasses = data;
          $scope.ceilingToEdit = angular.copy(ceilingToEdit);
        });

        AllGfsCodeService.query(function (data) {
          $scope.gfs_codes = data;
        });

        //Function to store data and close modal when Create button clicked
        $scope.update = function () {
          UpdateCeilingService.update({ page: currentPage, perPage: perPage }, $scope.ceilingToEdit,
            function (data) {
              //Successful function when
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
      //Service to create new financial year
      $scope.successMessage = data.successMessage;
      $scope.ceilings = data.ceilings;
      $scope.currentPage = $scope.ceilings.current_page;
    },
      function () {
        //If modal is closed
        console.log('Modal dismissed at: ' + new Date());
      });
  };
  $scope.delete = function (id, currentPage, perPage) {
    ConfirmDialogService.showConfirmDialog(
      'Confirm Delete Ceiling',
      'Are sure you want to delete this?')
      .then(function () {
        DeleteCeilingService.delete({ ceiling_id: id, page: currentPage, perPage: perPage },
          function (data) {
            $scope.successMessage = data.successMessage;
            $scope.ceilings = data.ceilings;
            $scope.currentPage = $scope.ceilings.current_page;
          }, function (error) {
            $scope.errorMessage = error.data.errorMessage;
          }
        );
      },
        function () {
          console.log("NO");
        });
  };
  $scope.sectors = function (ceiling) {
    console.log(ceiling);
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/budgeting/ceiling/sectors.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, AllCeilingSectorService, AllSectorService,
        AddSectorService) {
        $scope.ceilingSectorToCreate = {};
        $scope.ceiling_id = ceiling.id;
        $scope.name = ceiling.name;

        AllSectorService.query(function (data) {
          $scope.sectors = data;
        });

        AllCeilingSectorService.sectors({ ceiling_id: ceiling.id }, function (data) {
          $scope.successMessage = data.successMessage;
          $scope.ceiling_sectors = data.ceiling_sectors;
        }, function (error) {

        }
        );

        $scope.deleteCeilingSector = function (ceiling_sector_id, ceiling_id) {
          console.log('called');
          DeleteCeilingSectorService.deleteCeilingSector({
            ceiling_sector_id: ceiling_sector_id,
            ceiling_id: ceiling_id
          }, function (data) {
            //$scope.successMessage = data.successMessage;
            $scope.ceiling_sectors = data.ceiling_sectors;
          }, function (error) {

          }
          );
        };
        $scope.add_sector = function () {
          var ceilingSectorToSave = {
            "sector_id": $scope.ceilingSectorToCreate.sector_id,
            "ceiling_id": $scope.ceiling_id
          };
          AddSectorService.add_sector(ceilingSectorToSave, function (data) {
            //$uibModalInstance.close(data);
            $scope.ceiling_sectors = data.ceiling_sectors;
          },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
      //dfsafhsakfhskflsa
    },
      function () {
        //If modal is closed
        console.log('Modal dismissed at: ' + new Date());
      });
  };
  $scope.getDate = function (date) {
    return new Date(date);
  };
  $scope.viewUsage = function (ceiling) {
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/budgeting/ceiling/usage.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, AdminHierarchyCeiling) {
        $scope.ceiling = ceiling;
        $scope.currentPage = 1;
        $scope.perPage = 10;
        $scope.getDate = function (date) {
          return new Date(date);
        };
        $scope.pageChanged = function () {
          AdminHierarchyCeiling.getByCeiling({
            ceilingId: ceiling.id,
            page: $scope.currentPage,
            perPage: $scope.perPage
          }, function (data) {
            $scope.adminCeilings = data.admCeilings;
            $scope.similar = data.similar;
          });
        };
        $scope.pageChanged();
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {

    },
      function () {
        //If modal is closed
        console.log('Modal dismissed at: ' + new Date());
      });
  };
  $scope.amounts = function (ceiling) {
    console.log(ceiling);
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/budgeting/ceiling/amounts.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, AllAdminHierarchyCeilingService, AllMTEFSectionService, DeleteAdminHierarchyCeilingService, AddAdminHierarchyCeilingService) {
        $scope.adminHierarchyCeilingToCreate = {};
        $scope.ceiling_id = ceiling.id;
        $scope.ceiling_name = ceiling.name;
        AllAdminHierarchyCeilingService.query({ ceiling_id: ceiling.id }, function (data) {
          console.log("ADMIN HIERARCHY CEILINGS:" + data);
          $scope.admin_hierarchy_ceilings = data;
        }, function (error) {

        }
        );
        AllMTEFSectionService.query(function (data) {
          $scope.mtefSections = data;
        });
        $scope.deleteAdminHierarchyCeiling = function (admin_hierarchy_ceiling_id, ceiling_id) {
          console.log('called');
          DeleteAdminHierarchyCeilingService.deleteAdminHierarchyCeiling({
            admin_hierarchy_ceiling_id: admin_hierarchy_ceiling_id,
            ceiling_id: ceiling_id
          }, function (data) {
            $scope.admin_hierarchy_ceilings = data.admin_hierarchy_ceilings;
          }, function (error) {

          }
          );
        };
        $scope.addAdminHierarchyCeiling = function () {
          var adminHierarchyCeilingToSave = {
            "mtef_section_id": $scope.adminHierarchyCeilingToCreate.mtef_section_id,
            "amount": $scope.adminHierarchyCeilingToCreate.amount,
            "ceiling_id": $scope.ceiling_id
          };
          AddAdminHierarchyCeilingService.addAdminHierarchyCeiling(adminHierarchyCeilingToSave, function (data) {
            $scope.admin_hierarchy_ceilings = data.admin_hierarchy_ceilings;
          },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    modalInstance.result.then(function (data) {
    },
      function () {
        console.log('Modal dismissed at: ' + new Date());
      });
  };
}

CeilingController.resolve = {
  AllCeilingModel: function (GetAllCeilingService, $q) {
    var deferred = $q.defer();
    GetAllCeilingService.get({ page: 1, perPage: 10 }, function (data) {
      deferred.resolve(data);
    });
    return deferred.promise;
  }
};
