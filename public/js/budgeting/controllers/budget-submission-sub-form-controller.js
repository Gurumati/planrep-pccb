function BudgetSubmissionSubFormController($scope, BudgetSubmissionSubFormsModel, DeleteBudgetSubmissionSubFormService,
                                           ParentBudgetSubmissionSubFormService, $uibModal, ConfirmDialogService) {

    $scope.budgetSubmissionSubForms = BudgetSubmissionSubFormsModel;
    $scope.title = "TITLE_BUDGET_SUBMISSION_SUB_FORMS";
    $scope.dateFormat = 'yyyy-MM-dd';


    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/budget_submission/sub_form/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,CreateBudgetSubmissionSubFormService,ParentBudgetSubmissionFormService) {

                $scope.loadParent = function (id) {
                    ParentBudgetSubmissionSubFormService.query({id: id}, function (data)
                        {
                            $scope.BudgetFormParents = data;
                        },
                        function(error) {
                            console.log(error);
                        }
                    );
                }

               //load all forms
                ParentBudgetSubmissionFormService.query({}, function (data)
                    {
                        $scope.BudgetForms = data;
                    },
                    function(error) {
                        console.log(error);
                    }
                );

                $scope.budgetSubmissionSubFormToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createBudgetSubmissionSubForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    if($scope.budgetSubmissionSubFormToCreate.is_lowest == undefined)
                    {
                        $scope.budgetSubmissionSubFormToCreate.is_lowest = false;
                    }
                    CreateBudgetSubmissionSubFormService.store($scope.budgetSubmissionSubFormToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            console.log(error);
                            $scope.errorMessage=error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage=data.successMessage;
                $scope.budgetSubmissionSubForms=data.budgetSubmissionSubForms;
                clearTree();

            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
                clearTree();
            });

    };

    $scope.edit = function (budgetSubmissionSubFormToEdit) {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/budget_submission/sub_form/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,UpdateBudgetSubmissionSubFormService,ParentBudgetSubmissionFormService) {

                $scope.budgetSubmissionSubFormToEdit = angular.copy(budgetSubmissionSubFormToEdit);

                $scope.loadParent = function (id) {
                    ParentBudgetSubmissionSubFormService.query({id:id}, function (data)
                        {
                            $scope.BudgetFormParents = data;
                        },
                        function(error) {
                            console.log(error);
                        }
                    );
                }
                //load Parent forms
                $scope.loadParent($scope.budgetSubmissionSubFormToEdit.id);
                //load all forms
                ParentBudgetSubmissionFormService.query({}, function (data)
                    {
                        $scope.BudgetForms = data;
                    },
                    function(error) {
                        console.log(error);
                    }
                );


                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updateBudgetSubmissionSubForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }

                    UpdateBudgetSubmissionSubFormService.store($scope.budgetSubmissionSubFormToEdit,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            console.log(error);
                            $scope.errorMessage=error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage=data.successMessage;
                $scope.budgetSubmissionSubForms=data.budgetSubmissionSubForms;
                clearTree();

            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
                clearTree();
            });

    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'TITLE_CONFIRM_DELETE_BUDGET_FORM_LEVEL',
            'CONFIRM_DELETE')
            .then(function () {
                    DeleteBudgetSubmissionSubFormService.delete({id: id},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.budgetSubmissionSubForms = data.budgetSubmissionSubForms;
                        }, function (error) {
                            $scope.errorMessage=error.data.errorMessage;
                        }
                    );
                },
                function () {

                });
    };

    var clearTree=function () {
        $scope.budgetSubFormNodeSearchQuery=undefined;
        $scope.budgetSubFormNode.currentNode=undefined;
    };
}

BudgetSubmissionSubFormController.resolve = {

    BudgetSubmissionSubFormsModel: function (BudgetSubmissionSubFormService,$q) {
        var deferred = $q.defer();
        BudgetSubmissionSubFormService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};
