function ScrutinizationBudgetController($scope, $http, $location, $window, Scrutiny, Activity, $uibModal, selectedScrutinization, UserScrutinizations, BudgetDialogService, localStorageService) {

    $scope.queryParams = $location.search();
    $scope.currentPage = 1;
    $scope.perPage = 10;
    $scope.selectedActivityId = 0;
    $scope.selectedScrutinization = selectedScrutinization;
    $scope.activities = {};
    $scope.facilityIndex = undefined;
    $scope.setFacilityIndex = function (index) {
        $scope.facilityIndex = index;
    };
    var changeStatus = function (status, loader) {
        UserScrutinizations.updateStatus({
                scrutinId: $scope.selectedScrutinization.id,
                status: status,
                noLoader: loader
            },
            function (data) {
                $scope.selectedScrutinization.status = status;
            },
            function () {

            }
        );
    };
    $scope.getTotalByFacility = function (f) {
        var total = 0;
        angular.forEach(f.activity_facility_fund_sources2, function (fund) {
            if (undefined !== fund.inputs) {
                angular.forEach(fund.inputs, function (input) {
                    total += (input.quantity * input.frequency * input.unit_price);
                });
            }
        });
        return total;
    };
    $scope.moveByMtefSection = function (mtefSectionId, direction) {

        var toDecisionLevelId = (direction === 1) ? $scope.queryParams.nextDecId : $scope.queryParams.backDecId;
        var toDecisionLevels = [];
        var confirmTitle = (direction === 1) ? 'TITLE_CONFIRM_FORWARD' : 'TITLE_CONFIRM_RETURN';
        var confirmDesc = (direction === 1) ? 'TITLE_CONFIRM_FORWARD' : 'TITLE_CONFIRM_RETURN';

        BudgetDialogService.showConfirmDialog(
                confirmTitle,
                confirmDesc,
                toDecisionLevels, [], direction)
            .then(function (d) {
                Scrutiny.moveByMtefSection({
                        mtefSectionId: mtefSectionId,
                        fromDecisionLevelId: $scope.queryParams.decId,
                        toDecisionLevelId: toDecisionLevelId
                    },
                    d,
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.goBack();
                    },
                    function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    });
            });
    };
    if ($scope.selectedScrutinization.status === 0) {
        changeStatus(1, false);
    }
    if (undefined === $scope.queryParams.facility) {
        $scope.queryParams.facility = 'all';
        $location.search('facility', 'all');
        $scope.selectedFacility = 'ALL';
    }
    if (undefined === $scope.queryParams.budgetSectionId) {
        $scope.queryParams.budgetSectionId = 0;
        $location.search('budgetSectionId', 0);
        $scope.selectedSection = undefined;
    }
    if (undefined === $scope.queryParams.scrutinizationId) {
        $scope.queryParams.scrutinizationId = 0;
        $location.search('scrutinizationId', 0);
    }

    $scope.loadBudget = function () {
        Activity.getByMtefSectionAndBudgetType({
                mtefSectionId: $scope.queryParams.budgetSectionId,
                page: $scope.activities.current_page,
                perPage: $scope.perPage,
                budgetType: 'CURRENT',
                facility: $scope.queryParams.facility
            },
            function (data) {
                $scope.activitiesLoading = false;
                $scope.activities = data.activities;
            });
    };

    $scope.loadInputs = function (a, page) {
        Activity.getFacilities({
                page: page,
                perPage: $scope.perPage,
                facility: $scope.queryParams.facility,
                activityId: a.id

            },
            function (data) {
                a.activityFacilities = data;

            });
    };
    $scope.loadRights = function () {
        $scope.rights = localStorageService.get(localStorageKeys.RIGHT);
    }();

    $scope.hasPermission = function (permission) {
        if ($scope.rights !== undefined && $scope.rights !== null) {
            var rights = JSON.parse($scope.rights);
            return rights.indexOf(permission) > -1;
        }
        return false;
    };
    $scope.loadBudgetCeiling = function () {
        $http.get('/json/dashBoard/getBudgetByCeiling/' + $scope.selectedScrutinization.admin_hierarchy_id + '?sectionId=' + $scope.selectedScrutinization.section_id + '&noLoader=true')
            .then(function (response) {
                $scope.budgetByCelings = response.data;
            });
    };

    $scope.showCeiling = function (budgetByCelings) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/scrutinization/budget-by-ceiling.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {
                $scope.budgetByCelings = budgetByCelings;
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(
            function (data) {},
            function () {
                
            });
    };

    $scope.loadBudget();
    $scope.loadBudgetCeiling();

    $scope.hasComments = function (type, itemId, mtefSectionId, comments) {
        var comment = _.filter(comments, function (c) {
            return c.addressed === false && c.item_type === type && c.item_id === itemId && c.comments !== "";
        });
        if (comment.length) {
            return true;
        } else {
            return false;
        }
    };
    $scope.facilityHasComments = function (f, scrutinId) {
        var comments = [];
        if (f.activity_facility_fund_sources2.length > 0) {
            angular.forEach(f.activity_facility_fund_sources2, function (fun) {
                if (fun.inputs.length > 0) {
                    angular.forEach(fun.inputs, function (inp) {
                        var comment = _.filter(inp.input_comments, function (c) {
                            return c.scrutinization_id === scrutinId && c.comments !== "";
                        });
                        comments = comments.concat(comment);
                    });
                }
            });
        }
        if (comments.length) {
            return true;
        } else {
            return false;
        }
    };

    $scope.returnMtefSection = function (scrutinId, mtefSectionId, fromDecisionLevelId) {
        UserScrutinizations.getReturnDecisionLevels({
                scrutinId: scrutinId,
                mtefSectionId: mtefSectionId
            },
            function (data) {
                moveMtefs(scrutinId, mtefSectionId, fromDecisionLevelId, data.decisionLevels);
            },
            function (error) {
                $scope.errorMessage = error.data.errorMessage;
            }
        );
    };
    var moveMtefs = function (scrutinId, mtefSectionId, fromDecisionLevelId, toDecisionLevels) {
        BudgetDialogService.showConfirmDialog(
                'TITLE_CONFIRM_RETURN',
                'TITLE_CONFIRM_RETURN', toDecisionLevels)
            .then(function (_data) {
                    var movement = {};
                    movement.mtef_section_id = mtefSectionId;
                    movement.from_decision_level_id = fromDecisionLevelId;
                    movement.to_decision_level_id = _data.toDecisionLevelId;
                    movement.comments = _data.comments;
                    movement.scrutinId = scrutinId;
                    movement.direction = -1;
                    UserScrutinizations.returnMtefSection(movement,
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.goBack();

                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;

                        }
                    );
                },
                function () {

                });
    };

    $scope.goBack = function () {
        var admin = (undefined !== $scope.queryParams.admin) ? $scope.queryParams.admin : 0;
        var dpt = (undefined !== $scope.queryParams.dpt) ? $scope.queryParams.dpt : 0;
        var p1 = (undefined !== $scope.queryParams.p1) ? $scope.queryParams.p1 : 0;
        $window.location = '/budgeting#!/scrutinization?admin=' + admin + '&dpt=' + dpt + '&p1=' + p1 + '&selectedP1Id=' + $scope.queryParams.selectedP1Id +
            '&selectedAdminIds=' + $scope.queryParams.selectedAdminIds + '&selectedSectorIds=' + $scope.queryParams.selectedSectorIds +
            '&selectedAssignments=' + $scope.queryParams.selectedAssignments + '&selectedStatus=' + $scope.queryParams.selectedStatus;
    };
    $scope.saveAndFinish = function (scrutinId, status) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/scrutinization/recommendation.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UserScrutinizations) {
                $scope.recommendation = undefined;
                $scope.save = function () {
                    $scope.saveRecommendationInProgress = true;
                    UserScrutinizations.setRecommendation({
                            recommendation: $scope.recommendation,
                            scrutinId: scrutinId,
                            status: status
                        },
                        function (data) {
                            $scope.saveRecommendationInProgress = false;
                            $scope.budgetLoaded = false;
                            $scope.selectedScrutin = undefined;
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.saveRecommendationInProgress = false;
                        }
                    );

                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(
            function (data) {
                $scope.selectedScrutinization.status = data.scrutin.status;
                $scope.selectedScrutinization.recommendation = data.scrutin.recommendation;
            });


    };

    $scope.showAndAddComments = function (itemType, itemId, comments, mtefSectionId, description) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/scrutinization/comment.html',
            backdrop: true,
            controller: function ($scope, $uibModalInstance, UserScrutinizations) {
                var commentsToSave = angular.copy(comments);
                var existing = _.findWhere(comments, {
                    item_type: itemType,
                    item_id: itemId,
                    addressed: false
                });
                $scope.saveCommentInProgress = false;
                $scope.commentType = 1;
                $scope.itemType = itemType;
                $scope.itemDescription = description;
                if (existing === undefined) {
                    var comment = {};
                    comment.comments = "";
                    comment.item_type = itemType;
                    comment.item_id = itemId;
                    comment.addressed = false;
                    comment.direction = -1;
                    comments.push(comment);
                }
                $scope.comments = comments;
                $scope.save = function () {

                    $scope.saveCommentInProgress = true;
                    var existing = _.findWhere(comments, {
                        item_type: itemType,
                        item_id: itemId,
                        addressed: false
                    });
                    UserScrutinizations.saveItemComments({
                            noLoader: true
                        }, existing,
                        function (data) {
                            $scope.saveCommentInProgress = false;
                            existing = data.comment;
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.saveCommentInProgress = false;
                            $scope.errorMessage = error.data.errorMessage;
                        });
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(
            function (data) {
            },
            function () {
            });
    };

}
ScrutinizationBudgetController.resolve = {
    selectedScrutinization: function (UserScrutinizations, $q, $timeout, $location) {
        var params = $location.search();
        var id = (undefined !== params.scrutinizationId) ? params.scrutinizationId : 0;
        var deferred = $q.defer();
        $timeout(function () {
            UserScrutinizations.get({
                id: id
            }, function (data) {
                deferred.resolve(data);
            });
        }, 100);

        return deferred.promise;
    }
};
