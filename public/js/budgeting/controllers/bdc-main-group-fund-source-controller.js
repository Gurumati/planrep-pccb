function BdcMainGroupFundSourceController($scope, BdcMainGroupFundSourceModel, $uibModal,ToggleBdcMainGroupFundSourceService,
                                     ConfirmDialogService, DeleteBdcMainGroupFundSourceService, PaginatedBdcMainGroupFundSourceService) {

    $scope.mainGroupFundSources = BdcMainGroupFundSourceModel;
    $scope.title = "BUDGET_DISTRIBUTION_CONDITION_MAIN_GROUP_FUND_SOURCES";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedBdcMainGroupFundSourceService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.mainGroupFundSources = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/main_group_fund_source/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, ReferenceDocument, AllFundSourceService, AllBdcMainGroupService, CreateBdcMainGroupFundSourceService) {
                $scope.mainGroupFundSourceToCreate = {};

                AllFundSourceService.query(function (data) {
                    $scope.fundSources = data;
                });

                AllBdcMainGroupService.query(function (data) {
                    $scope.mainGroups = data;
                });

                ReferenceDocument.query(function (data) {
                    $scope.reference_docs = data;
                });

                $scope.store = function () {
                    if ($scope.createBdcMainGroupFundSourceForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateBdcMainGroupFundSourceService.store({perPage: $scope.perPage}, $scope.mainGroupFundSourceToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.mainGroupFundSources = data.mainGroupFundSources;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (mainGroupFundSourceToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/main_group_fund_source/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,ReferenceDocument, AllFundSourceService,AllBdcMainGroupService, UpdateBdcMainGroupFundSourceService) {

                AllFundSourceService.query(function (data) {
                    $scope.fundSources = data;
                    $scope.mainGroupFundSourceToEdit = angular.copy(mainGroupFundSourceToEdit);
                });

                AllBdcMainGroupService.query(function (data) {
                    $scope.mainGroups = data;
                });

                ReferenceDocument.query(function (data) {
                    $scope.reference_docs = data;
                });
                
                $scope.update = function () {
                    if ($scope.updateBdcMainGroupFundSourceForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateBdcMainGroupFundSourceService.update({page: currentPage, perPage: perPage}, $scope.mainGroupFundSourceToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.mainGroupFundSources = data.mainGroupFundSources;
                $scope.currentPage = $scope.mainGroupFundSources.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('CONFIRM_DELETE', 'Are you sure you want to trash this record?').then(function () {
                DeleteBdcMainGroupFundSourceService.delete({main_group_fund_source_id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.mainGroupFundSources = data.mainGroupFundSources;
                        $scope.currentPage = $scope.mainGroupFundSources.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/main_group_fund_source/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedBdcMainGroupFundSourceService,
                                  RestoreBdcMainGroupFundSourceService, EmptyBdcMainGroupFundSourceTrashService, PermanentDeleteBdcMainGroupFundSourceService) {
                TrashedBdcMainGroupFundSourceService.query(function (data) {
                    $scope.trashedMainGroupFundSources = data;
                });
                $scope.restoreBdcMainGroupFundSource = function (id) {
                    RestoreBdcMainGroupFundSourceService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedMainGroupFundSources = data.trashedMainGroupFundSources;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteBdcMainGroupFundSourceService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedMainGroupFundSources = data.trashedMainGroupFundSources;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyBdcMainGroupFundSourceTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedMainGroupFundSources = data.trashedMainGroupFundSources;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.mainGroupFundSources = data.mainGroupFundSources;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.toggleActive = function (id, is_active,perPage) {
        $scope.mainGroupFundSourceToActivate = {};
        $scope.mainGroupFundSourceToActivate.id = id;
        $scope.mainGroupFundSourceToActivate.is_active = is_active;
        ToggleBdcMainGroupFundSourceService.toggleActive({perPage:perPage},$scope.mainGroupFundSourceToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    };
}

BdcMainGroupFundSourceController.resolve = {
    BdcMainGroupFundSourceModel: function (PaginatedBdcMainGroupFundSourceService, $q) {
        var deferred = $q.defer();
        PaginatedBdcMainGroupFundSourceService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};