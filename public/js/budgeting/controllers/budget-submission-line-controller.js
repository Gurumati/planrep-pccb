function BudgetSubmissionLineController(
  $scope,
  $filter,
  BudgetSubmissionLinesModel,
  loadBudgetSectionService,
  loadBudgetSubmissionSelectOptionService,
  BudgetSubFormDefinitionService,
  UpdateBudgetSubmissionLineService,
  ResetBudgetSubmissionLineService,
  BudgetSubmissionFieldValuesService,
  BudgetSubmissionBudgetClassesService,
  DeleteBudgetSubmissionValuesServices,
  EditBudgetSubmissionValuesServices,
  SubmissionFormSelectOptionService,
  AllFinancialYearService,
  ConfirmDialogService
) {
  $scope.BudgetForms = BudgetSubmissionLinesModel;
  $scope.title = "TITLE_BUDGET_SUBMISSION_ITEMS";
  $scope.dateFormat = "yyyy-MM-dd";
  $scope.budgetSubmissionLineToCreate = {};
  $scope.budgetToDelete = {};
  $scope.budgetToEdit = {};
  $scope.round = []; //initialize round
  $scope.budgetSubmissionLineToCreate = {};
  $scope.budgetSubmissionLineToCreate.columns = [];
  // $scope.SectionWithData = [];
  $scope.uploadFile = false;
  $scope.btn_active = true;
  $scope.isEditMode = [];
  $scope.isMultiple = false;

  //load financial years
  AllFinancialYearService.query(function (data) {
    $scope.financialYears = data;
  });

  $scope.undoAll = function () {
    $scope.selectedForm = null;
    $scope.btn_active = true;
    $scope.errorMessage = null;
    $scope.budget_classes = null;
  };

  $scope.loadSection = function () {
    loadBudgetSectionService.query({}, function (data) {
      $scope.budgetSections = data;
    });
  };

  $scope.getChildren = function () {
    loadBudgetSubmissionSelectOptionService.query({}, function (data) {
      $scope.selectOptions = data;
    });
  };

  $scope.loadBudgetClasses = function (selectedForm) {
      BudgetSubmissionBudgetClassesService.get(
        {
          sub_form_id: selectedForm,
          financial_year_id: $scope.financial_year_id,
        },
        function (data) {
          $scope.budget_classes = data.budget_classes;
          if(selectedForm===14){
            $scope.getChildren();
          }else{
            $scope.loadSection();
          }
          $scope.formColumns = {};
          //$scope.downloadFile(selectedForm);
        },
        function (error) {
          $scope.errorMessage = data.errorMessage;
        }
      );
    //check for multiple
    $scope.isMultiple = $scope.hasMultiple(selectedForm);
  };

  $scope.loadFundSources = function (budget_class_id) {
    angular.forEach($scope.budget_classes, function (value, key) {
      if (value.budget_class_id === budget_class_id) {
        $scope.budget_class_id = budget_class_id;
        $scope.FundSources = value.fund_sources;
        $scope.mtef_id = value.mtef_id;
        $scope.facility_id = value.facility_id;
        $scope.is_locked = value.activity_locked;
        if ($scope.is_locked) {
          //$(":button").prop('disabled', true);
          $scope.btn_active = false;
          $scope.errorMessage = "BUDGET_IS_LOCKED";
        }
        $scope.loadBudgetForm();
      }
    });
  };

  $scope.loadBudgetForm = function () {
    $scope.SectionWithData = [];
    $scope.vertical_total = false;
    $scope.form_id = $scope.selectedForm;
    BudgetSubFormDefinitionService.query(
      { form_id: $scope.form_id },
      function (data) {
        $scope.formColumns = data;
        //load budget form data
        BudgetSubmissionFieldValuesService.getValues(
          {
            form_id: $scope.form_id,
            mtef_id: $scope.mtef_id,
            budget_class_id: $scope.budget_class_id,
          },
          function (data) {
            $scope.budgetFieldValues = data.lineValues;
            $scope.budgetFieldLines = data.lineData;
            if ($scope.budgetFieldValues !== undefined) {
              $scope.sectionExists();
            }
          }
        );
        //end load activity

        //load select options
        var filterGroup = [];
        angular.forEach($scope.formColumns, function (value, key) {
          if (value.type === "select") {
            filterGroup.push(value.select_option);
          }
          if (value.is_vertical_total) {
            $scope.vertical_total = true;
          }
        });
        if (filterGroup.length > 0) {
          SubmissionFormSelectOptionService.query(
            { select_group: filterGroup },
            function (data) {
              $scope.selectOptions = data;
            }
          );
        }
        //end
        $scope.budgetSubmissionLineToCreate.columns[1] = angular.copy(
          $scope.formColumns
        );
        $scope.editFormColumns = angular.copy($scope.formColumns);

        $scope.budgetSubmissionLineToCreate.sub_form_id = $scope.form_id;
        //get length of columns
        $scope.inputLength = $scope.formColumns.length;
        angular.forEach($scope.formColumns, function (value, key) {
          $scope.inputLength += value.child_form.length;
        });
      }
    );

    //load rounds
    if ($scope.round.length === 0) {
      $scope.getRound(1);
    }
    //check sections data
  };
  //this is a function for calculating total sub-items in item
  $scope.getTotal = function (data, item) {
    var total = 0;
    for (var i = 0; i < data.length; i++) {
      if (data[i].field_value) {
        total = total + data[i].field_value;
      }
    }
    item.field_value = total;
    if (item.field_value !== undefined && $scope.ceilingAmt !== undefined) {
      if ($scope.ceilingAmt < 0) {
        $scope.errorMessage =
          "PE Ceiling Amount not Allocated for this Cost Centre. Please contact Authorities";
        $scope.btn_active = false;
      }
      if (item.field_value < 0) {
        $scope.errorMessage = "Please Enter Valid Amount";
        $scope.btn_active = false;
      }
    }
    return total;
  };

  //this is a function for calculating total sub-items in the item lis display
  $scope.getInlineTotal = function (item) {
    //get sub items
    var subItems = item.child_form;
    var varData = $scope.budgetToEdit.columns;
    var data = $filter("filter")($scope.editFormColumns, { id: item.id });

    var total = 0;

    angular.forEach(subItems, function (value, key) {
      angular.forEach(data[0].child_form, function (value2, key2) {
        if (value.id === value2.id) {
          if (value2.field_value !== undefined) {
            total = total + +value2.field_value;
          } else {
            var field_value = 0;
            //read data
            angular.forEach(varData, function (v, k) {
              if (value.id === k) {
                field_value = v;
              }
            });

            total = total + +field_value;
          }
        }
      });
    });
    item.field_value = total;
    return total;
  };

  $scope.filter = function (section_id) {
    var data = [];
    angular.forEach($scope.budgetFieldValues, function (value, key) {
      //convert object into array
      var res = [];
      for (var x in value) {
        value.hasOwnProperty(x) && (res[x] = value[x]);
      }
      //find section id;
      if (res["section_id"] === section_id) {
        data = res;
      }
    });
    return data;
  };

  $scope.sectionExists = function () {
    var sections = [];
    angular.forEach($scope.budgetSections, function (value, key) {
      var section_id = value.id;
      if ($scope.budgetFieldValues !== undefined) {
        var result = $scope.filter(section_id);

        if (result.id !== undefined) {
          sections.push({
            section_id: value.id,
            section_code: value.code,
            section_name: value.name,
          });
        }
      }
    });

    //generate field values
    angular.forEach(sections, function (v, k) {
      angular.forEach($scope.budgetFieldLines, function (value, key) {
        var x = $filter("filter")($scope.budgetFieldValues, {
          budget_submission_line_id: value.id,
          section_id: v.section_id,
        });
        if (x.length > 0) {
          let testValue = null;
          let dataCount = 0;
          let testData = [];
          let mainarray = [];
          let xcount = 0;
          angular.forEach(x, function (value2) {
            xcount++;
            if (dataCount == 0) {
              testValue = value2.budget_submission_definition_id;
            }
            if (value2.budget_submission_definition_id === testValue) {
              if (dataCount !== 0) {
                mainarray.push(testData);
              }
              testData = [];
              testData.push(value2);
            } else {
              testData.push(value2);
              if (xcount === x.length) {
                mainarray.push(testData);
              }
            }
            dataCount++;
          });

          for (let i = 0; i < mainarray.length; i++) {
            var result3 = {};
            angular.forEach(mainarray[i], function (value2, key2) {
              result3[value2.budget_submission_definition_id] =
                value2.field_value;
            });
            result3["name"] = v.section_name;
            result3["section_id"] = v.section_id;
            result3["budget_submission_line_id"] = value.id;
            $scope.SectionWithData.push(result3);
          }
          $scope.isEditMode[value.id] = false;
        }
        x.length = 0;
      });
    });
  };

  $scope.getVerticalTotal = function (item) {
    if (item.is_vertical_total) {
      var total = 0;
      var result = [];
      angular.forEach($scope.budgetFieldValues, function (value, key) {
        if (item.id == value.budget_submission_definition_id) {
          result.push(value);
        }
      });

      angular.forEach(result, function (value, key) {
        if (value.field_value !== undefined) {
          var toNumber = eval(value.field_value);
          total = total + toNumber;
        }
      });
      if (item.type == "currency" || item.type == "formula") {
        total = $filter("number")(total, 2);
      }
      return total;
    }
  };

  $scope.getFieldValues = function (section_id) {
    let formIds = [10, 11];
    //update selection code
    var option = $("#section option:selected").text();
    $("#selected_section").html(option);
    //load activity_id for the section
    let peData = {
      mtef_id: $scope.mtef_id,
      section_id: section_id,
      budget_class_id: $scope.budget_class_id,
      fund_source_id: $scope.budgetSubmissionLineToCreate.fund_source_id,
      financial_year_id: $scope.financial_year_id,
    };
    BudgetSubmissionFieldValuesService.getActivity(
      peData,
      function (data) {
        $scope.activity_id = data.activity_id;
        if (
          data.balance < 0 &&
          formIds.indexOf($scope.budgetSubmissionLineToCreate.sub_form_id) ===
            -1
        ) {
          $scope.infoMessage =
            "Budget vs Ceiling mismatch. Please review your budget on all forms to match allocated ceiling";
          $scope.ceilingAmt = parseFloat(data.totalPECeiling);
          $scope.budgetedAmt = parseFloat(data.totalPEBudgeted);
          $scope.balance = parseFloat(data.balance);
          $scope.btn_active = false;
          return;
        } else {
          $scope.ceilingAmt = parseFloat(data.totalPECeiling);
          $scope.budgetedAmt = parseFloat(data.totalPEBudgeted);
          $scope.balance = data.balance;
          $scope.btn_active = true;
        }
        $scope.btn_active = true;
        $scope.errorMessage = null;
      },
      function (error) {
        $scope.errorMessage = error.data.errorMessage;
      }
    );
  };

  $scope.getRound = function (position) {
    if ($scope.formColumns.length > 0) {
      $scope.budgetSubmissionLineToCreate.columns[position] = angular.copy(
        $scope.formColumns
      );
    }
    $scope.round.push(position);
  };

  $scope.store = function () {
    let totalPE = $scope.budgetSubmissionLineToCreate.columns[1][1].field_value;
    let peCeiling = $scope.ceilingAmt;
    let budgetedAmt = $scope.budgetedAmt;
    let toBeBudgeted = parseFloat(peCeiling) - parseFloat(budgetedAmt);
    let formIds = [10, 11];
    if (
      angular.isUndefined($scope.budgetSubmissionLineToCreate.fund_source_id)
    ) {
      $scope.formHasErrors = true;
      $scope.fund_source_id = true;
      return;
    }

    if (angular.isUndefined($scope.budgetSubmissionLineToCreate.section_id)) {
      $scope.formHasErrors = true;
      $scope.section_id = true;
      return;
    }

    if (
      parseFloat(toBeBudgeted) < parseFloat(totalPE) &&
      formIds.indexOf($scope.budgetSubmissionLineToCreate.sub_form_id) === -1
    ) {
      let diff = parseFloat(totalPE) - parseFloat(toBeBudgeted);
      $scope.infoMessage =
        "You have exceeded by  " +
        diff +
        " Tshs. Your ceiling balance is " +
        toBeBudgeted +
        " Tshs";
      $scope.btn_active = false;
      return;
    }
    if (
      peCeiling === 0 &&
      formIds.indexOf($scope.budgetSubmissionLineToCreate.sub_form_id) === -1
    ) {
      $scope.infoMessage =
        "You have no ceiling, you cant budget for this Section. Contact Authorities";
      $scope.btn_active = false;
      return;
    }

    $scope.budgetSubmissionLineToCreate.activity_id = $scope.activity_id;
    $scope.budgetSubmissionLineToCreate.facility_id = $scope.facility_id;

    UpdateBudgetSubmissionLineService.store(
      $scope.budgetSubmissionLineToCreate,
      function (data) {
        //success message
        $scope.successMessage = data.successMessage;
        $scope.loadBudgetForm();
        $scope.getFieldValues($scope.budgetSubmissionLineToCreate.section_id);
      },
      function (error) {
        $scope.errorMessage = error.data.errorMessage;
      }
    );
  };
  //edit function
  $scope.editBudget = function () {
    ConfirmDialogService.showConfirmDialog(
      "TITLE_CONFIRM_UPDATE_PE_BUDGET",
      "CONFIRM_UPDATE"
    ).then(
      function () {
        let peCeiling = $scope.ceilingAmt;
        let budgetedAmt = $scope.budgetedAmt;
        let balance = parseFloat(peCeiling) - parseFloat(budgetedAmt);
        let diff =
          parseFloat($scope.editFormColumns[1].field_value) -
          parseFloat($scope.totalBeforeEdit);
        if (diff > balance) {
          let variance = diff - balance;
          $scope.errorMessage =
            "You have exceeded by  " +
            variance +
            " Tshs. Your ceiling balance is " +
            balance +
            " Tshs";
          return;
        }

        if (
          $scope.budgetToEdit.columns.budget_submission_line_id > 0 &&
          $scope.btn_active === true
        ) {
          var objectToEdit = {
            section_id: $scope.budgetToEdit.columns.section_id,
            form_id: $scope.form_id,
            activity_id: $scope.activity_id,
            columns: $scope.editFormColumns,
            budget_submission_line_id:
              $scope.budgetToEdit.columns.budget_submission_line_id,
            facility_id: $scope.facility_id,
          };

          EditBudgetSubmissionValuesServices.edit(
            objectToEdit,
            function (data) {
              $scope.successMessage = data.successMessage;
              $scope.loadBudgetForm();
              $scope.getFieldValues($scope.budgetToEdit.columns.section_id);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        }
      },
      function () {}
    );
  };
  //delete function
  $scope.deleteBudget = function () {
    ConfirmDialogService.showConfirmDialog(
      "TITLE_CONFIRM_DELETE_PE_BUDGET",
      "CONFIRM_DELETE"
    ).then(
      function () {
        if ($scope.budgetToEdit.columns.budget_submission_line_id > 0) {
          var objectToDelete = {
            section_id: $scope.budgetToEdit.columns.section_id,
            form_id: $scope.form_id,
            activity_id: $scope.activity_id,
            budget_submission_line_id:
              $scope.budgetToEdit.columns.budget_submission_line_id,
          };

          DeleteBudgetSubmissionValuesServices.delete(
            objectToDelete,
            function (data) {
              $scope.successMessage = data.successMessage;
              $scope.budgetFieldValues = data.lineValues;
              $scope.budgetFieldLines = data.lineData;
              $scope.loadBudgetForm();
              $scope.getFieldValues($scope.budgetToEdit.columns.section_id);
              $scope.round.length = 0;
              $scope.SectionWithData.length = 0;
              $scope.budgetToEdit.length = 0;
              $scope.getFieldValues($scope.budgetToEdit.columns.section_id);
              $scope.getRound(1);
              $scope.sectionExists();
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        }
      },
      function () {}
    );
  };

  $scope.totalBeforeEdit = 0;
  $scope.toggleEditMode = function (row) {
    //row.budget_submission_line_id
    //clear other lines
    $scope.totalBeforeEdit = row[5];
    $scope.clearAllMode();
    $scope.isEditMode[row.budget_submission_line_id] =
      !$scope.isEditMode[row.budget_submission_line_id];
  };

  $scope.clearAllMode = function () {
    angular.forEach($scope.budgetFieldLines, function (value, key) {
      $scope.isEditMode[value.id] = false;
    });
  };
  //input control functions
  $scope.isCurrency = function (input) {
    if (input == "currency") {
      return true;
    }
  };

  $scope.isInputDate = function (input) {
    if (input == "date") {
      return true;
    }
  };

  $scope.isSelect = function (input) {
    if (input == "select") {
      // //get selection option
      return true;
    }
  };

  $scope.isInput = function (input) {
    if (input == "text" || input == "number" || input == "checkbox") {
      return true;
    }
  };
  //validate inputs
  $scope.inputIsNumber = function (input) {
    if (input.field_value !== undefined) {
      if (input.type === "currency" || input.type === "number") {
        if (
          !(!isNaN(input.field_value) && angular.isNumber(+input.field_value))
        ) {
          $scope.formHasErrors = true;
          input.error = true;
          $scope.btn_active = false;
        } else {
          $scope.formHasErrors = false;
          input.error = false;
          $scope.btn_active = true;
        }
      } else {
        if (!isNaN(input.field_value) && angular.isNumber(+input.field_value)) {
          $scope.formHasErrors = true;
          input.error = true;
          $scope.btn_active = false;
        } else {
          $scope.formHasErrors = false;
          input.error = false;
          $scope.btn_active = true;
        }
      }
    }
  };

  //input control function end
  $scope.hasMultiple = function (form_id) {
    var x = $filter("filter")($scope.BudgetForms, { id: form_id });

    if (x.length > 0) {
      return x[0].is_multiple;
    } else {
      return false;
    }
  };

  $scope.getFormula = function (item, i) {
    var data_type = ""; //initialize data type
    var formula = item.formula;
    var patt = /C[0-9]+/gm;
    if (angular.isNumber(i)) {
      var dataObject = $scope.budgetSubmissionLineToCreate.columns[i];
    }

    if (formula != null) {
      var result = formula.match(patt);
      angular.forEach(result, function (value, key) {
        var col = value.replace("C", "");
        //get data
        if (dataObject.length > 0) {
          angular.forEach(dataObject, function (value2, key2) {
            if (value2.column_number == col) {
              if (value2.field_value !== undefined) {
                var x = value2.field_value;
                data_type = value2.type;
              } else {
                var x = 0;
              }
              formula = formula.replace(value, x);
            }
          });
        }
      });
      var returnValue = $scope.$eval(formula);

      if (returnValue > 0) {
        if ($.isNumeric(returnValue) && data_type == "currency") {
          returnValue = returnValue.toFixed(2);
          returnValue = returnValue
            .toString()
            .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "1,");
        }
        item.field_value = returnValue;
        return returnValue;
      } else {
        item.field_value = returnValue;
        return returnValue;
      }
    }
  };

  $scope.resetContributions = function () {
    var mtefs = {
      mtef_id: $scope.mtef_id,
      budget_class_id: $scope.budget_class_id,
      section_id: $scope.section_id,
      activity_id: $scope.activity_id,
      facility_id: $scope.facility_id,
    };
    ConfirmDialogService.showConfirmDialog(
      "TITLE_CONFIRM_RESET_PE_BUDGET",
      "CONFIRM_RESET"
    ).then(function () {
      var payload = [];
      angular.forEach($scope.SectionWithData, function (value, key) {
        payload.push(value);
      });
      ResetBudgetSubmissionLineService.resetContributions(
        { items: payload, activity: mtefs },
        function (data) {
          if (data.status === 200) {
            $scope.successMessage = data.message;
          } else {
            $scope.errorMessage = "Error resetting Contribution values";
          }
        }
      );
    });
  };

  $scope.getInlineFormula = function (varData, item) {
    var data_type = ""; //initialize input data type
    var formula = item.formula;
    var patt = /C[0-9]+/gm;
    var dataObject = $scope.editFormColumns;
    var varData = varData;

    if (formula != null) {
      var result = formula.match(patt);
      angular.forEach(result, function (value, key) {
        var col = value.replace("C", "");
        //get data
        if (dataObject.length > 0) {
          angular.forEach(dataObject, function (value2, key2) {
            if (value2.column_number == col) {
              if (value2.field_value !== undefined) {
                var x = value2.field_value;
                data_type = value2.type;
              } else {
                var x = 0;
                //read data
                angular.forEach(varData, function (v, k) {
                  if (value2.id == k) {
                    x = v;
                  }
                });
              }

              formula = formula.replace(value, x);
            }
          });
        }
      });

      var returnValue = $scope.$eval(formula);

      if (returnValue > 0) {
        if ($.isNumeric(returnValue) && data_type == "currency") {
          returnValue = returnValue.toFixed(2);
          returnValue = returnValue
            .toString()
            .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "1,");
        }
        item.field_value = returnValue;
        return returnValue;
      } else {
        item.field_value = returnValue;
        return returnValue;
      }
    }
  };
}

BudgetSubmissionLineController.resolve = {
  BudgetSubmissionLinesModel: function (BudgetSubmissionSubFormService, $q) {
    var deferred = $q.defer();
    BudgetSubmissionSubFormService.query({}, function (data) {
      deferred.resolve(data);
    });
    return deferred.promise;
  },
};
