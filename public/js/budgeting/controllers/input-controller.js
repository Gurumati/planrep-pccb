(function () {

    'use strict';

    angular.module('budgeting-module').controller('ActivityFacilityFundSourceInputController', ActivityFacilityFundSourceInputController);
    ActivityFacilityFundSourceInputController.$inject = ['$scope', '$window', '$location', '$uibModal', '$route', 'selectedActivityFacilityFundSource', 'CommentDialogService', 'ActivityFacilityFundSourceInput', 'ConfirmDialogService', 'ForwardMtefSectionService', '$timeout', 'BudgetDialogService', 'AdminHierarchyCeiling'];

    function ActivityFacilityFundSourceInputController($scope, $window, $location, $uibModal, $route, selectedActivityFacilityFundSource, CommentDialogService, ActivityFacilityFundSourceInput, ConfirmDialogService, ForwardMtefSectionService, $timeout, BudgetDialogService, AdminHierarchyCeiling) {

        $scope.title = "TITLE_BUDGETING";
        $scope.routeParams = $route.current.params;
        $scope.queryParams = $location.search();
        $scope.inputIsLoading = false;
        $scope.planIsLoading = true;

        /**
         * @description
         * Data passed when creating input
         * @type {{units: Array, procurementTypes: Array, periods: Array, forwardFinancialYears: Array, budgetType, mtefSectionId, selectedActivityFacilityFundSourceId: undefined, selectedFundSource: undefined}}
         */

        $scope.onBudgetInfoLoaded = function (budgetInfo) {
            $scope.plan = budgetInfo;
            $scope.planIsLoading = false;
            getSelectedActivityFacilityFundSource();
        };

        /**
         * @description
         * Get selected activity facility fund source
         */
        function getSelectedActivityFacilityFundSource() {
            selectedActivityFacilityFundSource.get($scope.routeParams.activityFacilityFundSourceId).
            then(function (data) {
                selectedActivityFacilityFundSource.set(data.activityFacilityFundSource);
                $scope.selectedActivityFacilityFundSource = data.activityFacilityFundSource;
                loadCeiling();
                $scope.planIsLoading = false;
            }).catch(function (error) {
                console.log(error);
                $scope.planIsLoading = false;
            });
        }

        /**
         * @description
         */
        function loadCeiling() {
            AdminHierarchyCeiling.getFundSourceCeiling({
                mtefSectionId: $scope.routeParams.mtefSectionId,
                budgetType: $scope.routeParams.budgetType,
                budgetClassId: $scope.selectedActivityFacilityFundSource.activity_facility.activity.budget_class_id,
                isFacilityAccount: $scope.selectedActivityFacilityFundSource.activity_facility.activity.is_facility_account,
                fundSourceId: $scope.selectedActivityFacilityFundSource.fund_source_id,
                facilityId: $scope.selectedActivityFacilityFundSource.activity_facility.facility_id,
                financialYearId:$scope.plan.mtef.financial_year_id
            }, function (data) {
                $scope.ceiling = data;
                paginate();
            }, function (error) {

            });
        }

        /**
         * @description
         * Paginate Inputs by selected activity facility fund source id
         */
        function paginate() {
            $scope.inputIsLoading = true;
            ActivityFacilityFundSourceInput.paginate({
                    page: $scope.currentPage,
                    perPage: $scope.perPage,
                    activityFacilityFundSourceId: $scope.routeParams.activityFacilityFundSourceId
                },
                function (data) {
                    $scope.activityFacilityFundSourceInputs = data.activityFacilityFundSourceInputs;
                    $scope.totalBudget = data.totalBudget;
                    $scope.inputIsLoading = false;
                },
                function (error) {
                    console.log(error);
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.inputIsLoading = false;
                });
        }
        $scope.pageChanged = function () {
            paginate();
        };

        $scope.hasToAddress = function(comments) {
            var notAddressed = _.where(comments,{addressed:false});
            return (notAddressed.length === 0) ? false : true;
        };

        $scope.addressComments = function(type, title, comments){
            CommentDialogService.showDialog(type, title, comments).then(function(){
                if(type === 'MTEF_SECTION'){
                    hasComments();
                }
                if(type === 'INPUT'){
                  //  $scope.pageChanged();
                }
            });
        };

        $scope.goBack = function () {
            var search = ($scope.queryParams.searchQuery === 'undefined') ? '%' : $scope.queryParams.searchQuery;
            var url = '/budgeting#!/activity-facility-fund-sources/' + $scope.routeParams.mtefSectionId + '/' + $scope.routeParams.budgetType +
                '?searchQuery=' + search + '&facilityId=' + $scope.queryParams.facilityId +
                '&fundSourceId=' + $scope.queryParams.fundSourceId + '&budgetClassId=' + $scope.queryParams.budgetClassId;

            if ($scope.queryParams.realoc) {
                url = url + '&realoc=true&isPopup=true';
            }
            $window.location = url;
        };

        $scope.assesBudget = function () {
            var modalInstance = $uibModal.open({
                templateUrl: '/pages/budgeting/partials/asses-budget.html',
                backdrop: true,
                controller: function ($scope, $uibModalInstance, BudgetDistributionConditions) {
                    BudgetDistributionConditions.get({}, function (data) {
                        $scope.totalBudget = data.totalBudget;
                        $scope.expenditureCentres = data.expenditureCentres;
                        $scope.mainGroups = data.mainGroups;

                    });
                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(
                function (data) {},
                function () {});
        };
        $scope.forwardMtefSection = function () {
            $scope.budgetClass = null;
            var nextDecisionLevels = $scope.plan.decision_level.next_decisions;
            BudgetDialogService.showConfirmDialog(
                    'TITLE_CONFIRM_FORWARD',
                    'CONFIRM_FORWARD', nextDecisionLevels)
                .then(function (data) {
                        var movement = {};
                        movement.mtef_section_id = $scope.plan.id;
                        movement.from_decision_level_id = $scope.plan.decision_level.id;
                        movement.to_decision_level_id = data.toDecisionLevelId;
                        movement.forward_message = data.comments;
                        movement.direction = 1; //Forward direction

                        ForwardMtefSectionService.forward(movement,
                            function (data) {
                                $scope.successMessage = data.successMessage;
                                $scope.plan.is_locked = true;
                                $scope.plan.decision_level = data.decisionLevel;
                            },
                            function (error) {
                                $scope.errorMessage = error.data.errorMessage;

                            }
                        );
                    },
                    function () {
                        console.log("NO");
                    });
        };

        $scope.continueToReallocation = function () {
            try {
                window.opener.HandleReallocationResult($scope.selectedReallocatedTo);
                window.close();
            } catch (err) {}
            window.close();
            return false;
        };

        $scope.createOrUpdateInput = function (input) {

            var modalInstance = $uibModal.open({
                templateUrl: '/pages/budgeting/partials/create-edit-input.html',
                backdrop: false,
                resolve: {
                    activity: $scope.selectedActivityFacilityFundSource.activity_facility.activity,
                    input: input,
                    params: {
                        budgetType: $scope.routeParams.budgetType,
                        mtefSectionId: $scope.routeParams.mtefSectionId,
                        isRealloc: ($scope.queryParams.realoc !== undefined) ? true : false,
                        selectedActivityFacilityFundSourceId: $scope.selectedActivityFacilityFundSource.id,
                        selectedFundSourceName: $scope.selectedActivityFacilityFundSource.fund_source.name,
                        activityId: $scope.selectedActivityFacilityFundSource.activity_facility.activity_id,
                        selectedFundSourceBalance: $scope.ceiling.ceilingBalance
                    },
                    periods: function ($q, CurrentFinancialYearPeriodService, $timeout) {
                        var deferred = $q.defer();
                        $timeout(function () {
                            CurrentFinancialYearPeriodService.query({
                                financialYearId: $scope.plan.mtef.financial_year_id
                            }, function (data) {
                                deferred.resolve(data);
                            }, function (error) {
                                deferred.reject(error);
                            });
                        }, 10);

                        return deferred.promise;
                    },
                    activityPeriods: function ($q, CurrentFinancialYearPeriodService, $timeout) {
                        var deferred = $q.defer();
                        $timeout(function () {
                            CurrentFinancialYearPeriodService.getPeriodsByActivity({
                                activityId: $scope.selectedActivityFacilityFundSource.activity_facility.activity_id
                            }, function (data) {
                                deferred.resolve(data.periods);
                            }, function (error) {
                                deferred.reject(error);
                            });
                        }, 10);
                        return deferred.promise;
                    },
                    units: function ($q, AllUnitsService, $timeout) {
                        var deferred = $q.defer();
                        $timeout(function () {
                            AllUnitsService.query({}, function (data) {
                                deferred.resolve(data);
                            }, function (error) {
                                deferred.reject(error);
                            });
                        }, 10);
                        return deferred.promise;
                    },
                    procurementTypes: function ($q, AllProcurementTypeService, $timeout) {
                        var deferred = $q.defer();
                        $timeout(function () {
                            AllProcurementTypeService.query(function (data) {
                                deferred.resolve(data);
                            }, function (error) {
                                deferred.reject(error);
                            });
                        }, 10);
                        return deferred.promise;
                    },
                    forwardFinancialYears: function ($q, ForwardFinancialYearsService, $timeout) {
                        var deferred = $q.defer();
                        $timeout(function () {
                            ForwardFinancialYearsService.query({
                                financialYearId: $scope.plan.mtef.financial_year_id,
                            }, function (data) {
                                deferred.resolve(data);
                            }, function (error) {
                                deferred.reject(error);
                            });
                        }, 10);
                        return deferred.promise;
                    }
                },
                controller: 'CreateOrUpdateInputController'
            });
            modalInstance.result.then(function (data) {
                    loadCeiling();
                    $scope.successMessage = data.successMessage;
                    $scope.clearMessage();
                },
                function () {
                    console.log('Modal dismissed at: ' + new Date());
                    loadCeiling();
                });
        };

        $scope.delete = function (id) {
            ConfirmDialogService.showConfirmDialog(
                    'TITLE_CONFIRM_INPUT',
                    'CONFIRM_DELETE')
                .then(function () {
                        ActivityFacilityFundSourceInput.delete({
                                id: id
                            },
                            function (data) {
                                $scope.successMessage = data.successMessage;
                                loadCeiling();
                                $scope.clearMessage();
                            },
                            function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                            }
                        );
                    },
                    function () {});
        };

        $scope.clearMessage = function () {
            $timeout(function () {
                $scope.successMessage = undefined;
                $scope.errorMessage = undefined;
            }, 2000);
        };

        $scope.selectedReallocatedTo = [];

        $scope.toggleSelectedReallocationTo = function (id) {
            var idx = $scope.selectedReallocatedTo.indexOf(id);
            if (idx === -1) {
                $scope.selectedReallocatedTo.push(id);
            } else {
                $scope.selectedReallocatedTo.splice(idx, 1);
            }
        };

    }
})();
