function ScrutinizationController($scope, Scrutiny, UserScrutinizations, BudgetDialogService, $window, $timeout, $location, localStorageService, selectedScrutiny) {

  $scope.user = JSON.parse(localStorageService.get(localStorageKeys.USER));
  $scope.parentAdminHierarchyId = $scope.user.admin_hierarchy_id;
  $scope.parentSectionId = $scope.user.section_id;
  $scope.queryParams = $location.search();

  var loadAdminHierarchies = function () {
    Scrutiny.adminHierarchies({
      parentAdminHierarchyId: $scope.parentAdminHierarchyId,
      parentSectionId: $scope.parentSectionId
    }, function (data) {
      $scope.adminHierarchies = data.adminHierarchies;
      $scope.allChildAdminHierarchies = data.allChildren;
      $scope.allSectors = data.allSectors;
      var regions = _.groupBy($scope.adminHierarchies, function (adm) {
        return adm.region;
      });
      $scope.regions = $.map(regions, function (councils, key) {
        return [{
          region: key,
          data: councils
        }];
      });
      $scope.selectedRegion = selectedScrutiny.get('REGION');
      if ($scope.regions.length === 1) {
        $scope.selectedRegion = $scope.regions[0];
        selectedScrutiny.set('REGION', $scope.selectedRegion);
        if ($scope.selectedRegion.data.length === 1) {
          $scope.selectedCouncil = $scope.selectedRegion.data[0];
          selectedScrutiny.set('COUNCIL', $scope.selectedCouncil);
          $scope.loadDepartments($scope.selectedRegion);
        } else {
          $scope.selectedCouncil = selectedScrutiny.get('COUNCIL');
          if ($scope.selectedCouncil !== undefined) {
            $scope.loadSectors();
          }
        }
      }
      else if($scope.selectedRegion !== undefined){
        $scope.selectedCouncil = selectedScrutiny.get('COUNCIL');
        if ($scope.selectedCouncil !== undefined) {
          $scope.loadSectors();
        }
      }
      $scope.total = data.total;
      $scope.decisionLevel = data.decisionLevel;
      $scope.nextDecisionLevels = data.decisionLevel.next_decisions;
      $scope.returnDecisionLevels = data.returnDecisionLevels;

    }, function (error) {
      $scope.errorMessage = error.data.errorMessage;
    });
  };
  loadAdminHierarchies();

  $scope.setSelectdRegion = function () {
    $timeout(function () {
      selectedScrutiny.set('REGION', $scope.selectedRegion);
    }, 900);
  };


  $scope.loadSectors = function () {
    selectedScrutiny.set('COUNCIL', $scope.selectedRegion);
    Scrutiny.sectors({
      parentAdminHierarchyId: $scope.parentAdminHierarchyId,
      parentSectionId: $scope.parentSectionId,
      mtefId: $scope.selectedRegion.mtefId
    }, function (data) {
      $scope.sectors = data.sectors;
      $scope.selectedSector = undefined;
      $scope.costCentres = [];
      if ($scope.sectors.length === 1) {
        $scope.selectedSector = $scope.sectors[0];
        selectedScrutiny.set('SECTOR', $scope.selectedSector);
        $scope.loadDepartments();
      } else {
        $scope.selectedSector = selectedScrutiny.get('SECTOR');
        if ($scope.selectedSector !== undefined) {
          $scope.loadDepartments();
        }
      }
    });
  };

  $scope.loadDepartments = function (selectedRegion) {
    $scope.costCentres= []
     selectedScrutiny.set('SECTOR',$scope.selectedSector);
    Scrutiny.departments({
      parentAdminHierarchyId: $scope.parentAdminHierarchyId,
      parentSectionId: $scope.parentSectionId,
      mtefId: selectedRegion.data[0].mtefId,
      sectorId: 1
    }, function (data) {
      $scope.departments = data.departments;
      $scope.allDepartments = data.allDepartments;
      $scope.users = data.users;
      $scope.selectedDepartment = undefined;
      if ($scope.departments.length === 1){
        $scope.selectedDepartment = $scope.departments[0];
        $scope.loadCostCentres();
      }
    });
  };
  $scope.loadCostCentres = function () {
    $scope.costCentres= []
    selectedScrutiny.set('SECTOR',$scope.selectedSector);
    Scrutiny.costCentres({
      parentAdminHierarchyId: $scope.parentAdminHierarchyId,
      parentSectionId: $scope.parentSectionId,
      mtefId: $scope.selectedRegion.data[0].mtefId,
      sectorId:1
    }, function (data) {
      $scope.costCentres = data.costCentres;
      $scope.allCostCentres = data.allCostCentres;
      $scope.users = data.users;
    });
  };

  $scope.assignUserBySector = function (selectedSector) {
    selectedSector.successMessage = undefined;
    selectedSector.errorMessage = undefined;
    selectedSector.inProgress = true;
    $timeout(function () {
      if (selectedSector.user_id === undefined) {
        selectedSector.inProgress = false;
        return;
      }
      Scrutiny.assignUserBySector({
        mtefId: $scope.selectedRegion.data[0].mtefId,
        sectorId: 1,
        userId: selectedSector.user_id
      }, function (data) {
        selectedSector.successMessage = data.successMessage;
        selectedSector.inProgress = false;
        $scope.loadCostCentres();

      }, function (error) {
        selectedSector.inProgress = false;
        selectedSector.errorMessage = error.data.errorMessage;
      });
    }, 900);

  };

  $scope.assignUserByCostCentre = function (costCentre) {
    costCentre.successMessage = undefined;
    costCentre.errorMessage = undefined;
    costCentre.inProgress = true;
    $timeout(function () {
      Scrutiny.assignUserByCostCentre({
        mtefSectionId: costCentre.mtef_section_id,
        userId: costCentre.user_id
      }, function (data) {
        costCentre.successMessage = data.successMessage;
        costCentre.inProgress = false;
      }, function (error) {
        costCentre.inProgress = false;
        costCentre.user_id = undefined;
        costCentre.errorMessage = error.data.errorMessage;
      });
    }, 900);

  };

  $scope.moveByMtef = function (mtefId, direction) {

    var toDecisionLevels = (direction === 1) ? $scope.nextDecisionLevels : $scope.returnDecisionLevels;
    var confirmTitle = (direction === 1) ? 'TITLE_CONFIRM_FORWARD' : 'TITLE_CONFIRM_RETURN';
    var confirmDesc = (direction === 1) ? 'TITLE_CONFIRM_FORWARD' : 'TITLE_CONFIRM_RETURN';

    BudgetDialogService.showConfirmDialog(
      confirmTitle,
      confirmDesc,
      toDecisionLevels, [], direction)
      .then(function (d) {
        Scrutiny.moveByMtef({
            mtefId: mtefId,
            fromDecisionLevelId: $scope.decisionLevel.id,
            toDecisionLevelId: d.toDecisionLevelId
          },
          d,
          function (data) {
            $scope.successMessage = data.successMessage;
            loadAdminHierarchies();
            $scope.costCentres = [];
          },
          function (error) {
            $scope.errorMessage = error.data.errorMessage;
          });
      });
  };
  $scope.moveBySector = function (mtefId, sectorId, direction) {

    var toDecisionLevels = (direction === 1) ? $scope.nextDecisionLevels : $scope.returnDecisionLevels;
    var confirmTitle = (direction === 1) ? 'TITLE_CONFIRM_FORWARD' : 'TITLE_CONFIRM_RETURN';
    var confirmDesc = (direction === 1) ? 'TITLE_CONFIRM_FORWARD' : 'TITLE_CONFIRM_RETURN';

    BudgetDialogService.showConfirmDialog(
      confirmTitle,
      confirmDesc,
      toDecisionLevels, [], direction)
      .then(function (d) {
        Scrutiny.moveBySector({
            mtefId: mtefId,
            sectorId: $scope.selectedDepartment.department_id,
            fromDecisionLevelId: $scope.decisionLevel.id,
            toDecisionLevelId: d.toDecisionLevelId
          },
          d,
          function (data) {
            $scope.successMessage = data.successMessage;
            $scope.costCentres = [];
            loadAdminHierarchies();
            $scope.departments = [];
            $scope.selectedDepartment = undefined;
            $scope.selectedRegion = undefined;
            
          },
          function (error) {
            $scope.errorMessage = error.data.errorMessage;
          });
      });
  };

  $scope.moveByMtefSection = function (mtefSectionId, direction) {

    var toDecisionLevels = (direction === 1) ? $scope.nextDecisionLevels : $scope.returnDecisionLevels;
    var confirmTitle = (direction === 1) ? 'TITLE_CONFIRM_FORWARD' : 'TITLE_CONFIRM_RETURN';
    var confirmDesc = (direction === 1) ? 'TITLE_CONFIRM_FORWARD' : 'TITLE_CONFIRM_RETURN';

    BudgetDialogService.showConfirmDialog(
      confirmTitle,
      confirmDesc,
      toDecisionLevels, [], direction)
      .then(function (d) {
        Scrutiny.moveByMtefSection({
            mtefSectionId: mtefSectionId,
            fromDecisionLevelId: $scope.decisionLevel.id,
            toDecisionLevelId: d.toDecisionLevelId
          },
          d,
          function (data) {
            $scope.successMessage = data.successMessage;
            $scope.loadCostCentres();
          },
          function (error) {
            $scope.errorMessage = error.data.errorMessage;
          });
      });
  };


  $scope.loadRights = function () {
    $scope.rights = localStorageService.get(localStorageKeys.RIGHT);
  }();

  $scope.hasPermission = function (permission) {
    if ($scope.rights !== undefined && $scope.rights !== null) {
      var rights = JSON.parse($scope.rights);
      return rights.indexOf(permission) > -1;
    }
    return false;
  };

  $scope.assignUser = function (scrutins, userId) {
    // scrutins.assignmentInProgress=true;
    var scrutinIds = {
      scrutinIds: _.pluck(scrutins, 'id')
    };
    UserScrutinizations.assignUser({
        userId: userId,
        noLoader: true
      }, scrutinIds, function (data) {
        $scope.loadScrutinizations();
        //   scrutin.assignmentInProgress=false;
        $scope.getByStatus($scope.scrutinizations);
      },
      function (error) {

      }
    );
  };

  $scope.assignToMe = function (scrutins) {
    $scope.assignUser(scrutins, $scope.meId);
  };


  $scope.returnAllByUser = function (scrutinizations) {

    UserScrutinizations.getAllReturnDecisionLevels({}, function (data) {
      returnAll(scrutinizations, data.decisionLevels);
    }, function (error) {
      $scope.errorMessage = error.data.errorMessage;
    });

  };

  var returnAll = function (scrutinizations, toDecisionLevels) {
    BudgetDialogService.showConfirmDialog(
      'TITLE_CONFIRM_RETURN',
      'TITLE_CONFIRM_RETURN', toDecisionLevels, scrutinizations, -1)
      .then(function (_data) {
          var movement = {};
          movement.to_decision_level_id = _data.toDecisionLevelId;
          movement.comments = _data.comments;
          movement.scrutinizations = scrutinizations;
          movement.direction = -1;
          UserScrutinizations.returnAllByUser(movement,
            function (data) {
              $scope.successMessage = data.successMessage;
              $scope.loadUserData();

            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;

            }
          );
        },
        function () {});
  };


  var changeStatus = function (status, loader) {
    UserScrutinizations.updateStatus({
        scrutinId: $scope.selectedScrutin.id,
        status: status,
        noLoader: loader
      },
      function (data) {
        $scope.selectedScrutin.status = status;
        $scope.successmessage = data.successMessage;
      },
      function () {

      }
    );
  };

  $scope.forwardAllByUser = function (scrutins) {

    BudgetDialogService.showConfirmDialog(
      'TITLE_CONFIRM_FORWARD',
      'TITLE_CONFIRM_FORWARD', $scope.nextDecisionLevels, scrutins, 1)
      .then(function (_data) {
          var movement = {};
          movement.comments = _data.comments;
          movement.to_decision_level_id = _data.toDecisionLevelId;
          movement.direction = 1;
          movement.scrutinizations = scrutins;

          UserScrutinizations.forwardAllByUser(movement,
            function (data) {
              $scope.successMessage = data.successMessage;
              $scope.loadUserData();
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;

            }
          );
        },
        function () {});
  };


  $scope.backToSections = function () {
    $scope.selectedScrutin = undefined;
    $scope.budgetLoaded = false;
  };

  $scope.loadBudget = function (mtef_section_id) {
    $window.location = '/budgeting#!/scrutinization-budget?budgetSectionId=' + mtef_section_id +
      '&decId=' + $scope.decisionLevel.id +
      '&nextDecId=' + (($scope.nextDecisionLevels.length > 0) ? $scope.nextDecisionLevels[0].id : 0) +
      '&backDecId=' + $scope.returnDecisionLevels[0].id;
  };



  $scope.returnMtefSection = function (scrutin, mtefSectionId, fromDecisionLevelId) {
    UserScrutinizations.getReturnDecisionLevels({
        scrutinId: scrutinId,
        mtefSectionId: mtefSectionId
      },
      function (data) {
        moveMtefs(scrutin, mtefSectionId, fromDecisionLevelId, data.decisionLevels);
      },
      function (error) {
        $scope.errorMessage = error.data.errorMessage;
      }
    );
  };

  var moveMtefs = function (scrutin, mtefSectionId, fromDecisionLevelId, toDecisionLevels) {
    BudgetDialogService.showConfirmDialog(
      'TITLE_CONFIRM_RETURN',
      'TITLE_CONFIRM_RETURN', toDecisionLevels, [scrutin], -1)
      .then(function (_data) {
          var movement = {};
          movement.mtef_section_id = mtefSectionId;
          movement.from_decision_level_id = fromDecisionLevelId;
          movement.to_decision_level_id = _data.toDecisionLevelId;
          movement.comments = _data.comments;
          movement.scrutinId = scrutin.id;
          movement.direction = -1;
          UserScrutinizations.returnMtefSection(movement,
            function (data) {
              $scope.successMessage = data.successMessage;
              $scope.backToSections();
              $scope.loadUserData();
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;

            }
          );
        },
        function () {});
  };
  $('a.sidebar-main-toggle').trigger('click');

}

ScrutinizationController.resolve = {

};
