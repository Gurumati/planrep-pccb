(function () {

    'use strict';

  angular.module('budgeting-module').controller('ActivityFacilityFundSourceController', ActivityFacilityFundSourceController);
  ActivityFacilityFundSourceController.$inject = [
    '$scope',
    '$q',
    '$window',
    '$location',
    '$route',
    '$http',
    'ActivityFacilityFundSource',
    'PlanningBudgetClassService',
    'PlanningFundSources',
    'FacilityService',
    'BudgetDialogService',
    'ForwardMtefSectionService',
    'CommentDialogService'];

  function ActivityFacilityFundSourceController(
    $scope,
    $q,
    $window,
    $location,
    $route,
    $http,
    ActivityFacilityFundSource,
    PlanningBudgetClassService,
    PlanningFundSources,
    FacilityService,
    BudgetDialogService,
    ForwardMtefSectionService,
    CommentDialogService ) {

        $scope.title = "TITLE_BUDGETING";
        $scope.queryParams = $location.search();
        $scope.routeParams = $route.current.params;
        $scope.planIsLoading = true;

        if (undefined === $scope.queryParams.facilityId) {
            $scope.queryParams.facility = 0;
            $location.search('facilityId', 0);
        }
        if (undefined === $scope.queryParams.fundSourceId) {
            $scope.queryParams.fundSourceId = 0;
            $location.search('fundSourceId', 0);
        }
        if (undefined === $scope.queryParams.budgetClassId) {
            $scope.queryParams.budgetClassId = 0;
            $location.search('budgetClassId', 0);
        }
        if (undefined === $scope.queryParams.searchQuery || 'undefined' === $scope.queryParams.searchQuery) {
            $scope.queryParams.searchQuery = '%';
            $location.search('searchQuery', '%');
        }
     function budgetAndCeiling() {
        if ($scope.plan === undefined) {
          return;
        }
        $scope.budget = 0.00;
        $scope.ceiling = 0.00;
        $scope.completion = 0.00;
       $http.get('/json/dashBoard/getBudgetByCeiling/'+$scope.routeParams.budgetType+'/'+$scope.plan.mtef.financial_year_id+'/' + $scope.plan.mtef.admin_hierarchy_id + '/' + $scope.plan.section_id +'?noLoader=true')
        .then(function (data) {
          if (data.data && data.data.length > 0) {
            data.data.forEach(d => {
              $scope.budget = $scope.budget + parseFloat(d.budget);
              $scope.ceiling = $scope.ceiling + parseFloat(d.ceiling);
              $scope.completion = (d.ceiling !== 0) ? Math.round(($scope.budget / $scope.ceiling) * 100) : 0.00;
            });
          }

        });
      }


    $scope.onBudgetInfoLoaded = function (budgetInfo) {
      $scope.plan = budgetInfo;
      budgetAndCeiling();
      $scope.planIsLoading = false;
      hasComments();
    };

        /**
         * Budget Classes
         */
        (function () {
            PlanningBudgetClassService.query(function (data) {
                $scope.budgetClasses = data;
                $scope.budgetClassLoaded = true;
            }, function (error) {
                console.log(error);
                $scope.budgetClassLoaded = true;
                $scope.errorMessage = 'Failed to load Budget Classes';
            });
        })();

        /**
         * Fund Sources
         */
        (function () {
            PlanningFundSources.getByUser({
                mtefSectionId: $scope.routeParams.mtefSectionId,
                budgetType: $scope.routeParams.budgetType
            }, function (data) {
                $scope.fundSources = data;
                $scope.fundSourcesLoaded = true;
            }, function (error) {
                console.log(error);
                $scope.fundSourcesLoaded = true;
                $scope.errorMessage = 'Failed to load Fund Source';
            });
        })();

        var hasComments = function(){
            var exist = _.where($scope.plan.mtef_section_comments,{addressed:false});
            $scope.hasComments = (exist.length > 0)? true: false;
        };

        $scope.hasToAddress = function(comments) {
            var notAddressed = _.where(comments,{addressed:false});
            return (notAddressed.length === 0) ? false : true;
        };

        $scope.addressComments = function(type, title, comments){
            CommentDialogService.showDialog(type, title, comments).then(function(){
                if(type === 'MTEF_SECTION'){
                    hasComments();
                }
                if(type === 'ACTIVITY'){
                    //$scope.pageChanged();
                }
            });
        };

        $scope.pageChanged = function () {
            getActivityFacilityFundSources();
        };
        $scope.searchQueryChanged = function () {
            $location.search('searchQuery', $scope.queryParams.searchQuery);
            getActivityFacilityFundSources();
        };
        $scope.facilityChanged = function (facilityId) {
            $scope.queryParams.facilityId = facilityId;
            $location.search('facilityId', $scope.queryParams.facilityId);
            getActivityFacilityFundSources();
        };
        $scope.fundSourceChanged = function () {
            $location.search('fundSourceId', $scope.queryParams.fundSourceId);
            getActivityFacilityFundSources();
        };
        $scope.budgetClassChanged = function () {
            $location.search('budgetClassId', $scope.queryParams.budgetClassId);
            getActivityFacilityFundSources();
        };
        $scope.pageChanged = function () {
            getActivityFacilityFundSources();
        };

        $scope.showComments = function(comments) {

        };

        function getActivityFacilityFundSources() {
            $scope.affIsLoading = true;
            ActivityFacilityFundSource.paginate({
                mtefSectionId: $scope.routeParams.mtefSectionId,
                budgetType: $scope.routeParams.budgetType,
                facilityId: $scope.queryParams.facilityId,
                fundSourceId: $scope.queryParams.fundSourceId,
                budgetClassId: $scope.queryParams.budgetClassId,
                searchQuery: $scope.queryParams.searchQuery,
                page: $scope.currentPage,
                perPage: $scope.perPage
            }, function (data) {
                $scope.activityFacilityFundSources = data.activityFacilityFundSources;
                $scope.affIsLoading = false;
            }, function (error) {
                $scope.errorMessage = error.data.errorMessage;
                $scope.affIsLoading = false;
            });
        }

        $scope.searchFacility = function ($query) {
            var deferred = $q.defer();
            if ($query !== null && $query !== undefined && $query !== '' && $query.length >= 1) {
                $scope.facilityLoading = true;
                var facilityToExclude = {
                    'facilityIdsToExclude': []
                };
                FacilityService.searchByPlanningSection({
                        mtefSectionId: $scope.routeParams.mtefSectionId,
                        isFacilityAccount: true,
                        searchQuery: $query,
                        noLoader: true
                    },
                    facilityToExclude,
                    function (data) {
                        $scope.facilityLoading = false;
                        data.facilities.unshift({
                            id: 0,
                            name: "All",
                            type: "Facilities"
                        });
                        deferred.resolve(data.facilities);
                    },
                    function (error) {
                        console.log(error);
                        deferred.reject('Error');
                    });
            } else {
                deferred.resolve([{
                    id: 0,
                    name: "All",
                    type: "Facilities"
                }]);
            }
            return deferred.promise;
        };

        $scope.openInputs = function (affId) {
            var url = '/budgeting#!/activity-facility-fund-source-inputs/' + $scope.routeParams.mtefSectionId + '/' + $scope.routeParams.budgetType + '/' + affId +
                '?searchQuery=' + $scope.queryParams.searchQuery + '&facilityId=' + $scope.queryParams.facilityId +
                '&fundSourceId=' + $scope.queryParams.fundSourceId + '&budgetClassId=' + $scope.queryParams.budgetClassId;
            if ($scope.queryParams.realoc) {
                url = url + '&realoc=true&isPopup=true';
            }
            $window.location = url;
        };

        $scope.goBack = function () {
            $window.location = '/budgeting#!/plans/2/' + $scope.routeParams.budgetType;
        };

        $scope.forwardMtefSection = function () {
            $scope.budgetClass = null;
            var nextDecisionLevels = $scope.plan.decision_level.next_decisions;
            BudgetDialogService.showConfirmDialog(
                    'TITLE_CONFIRM_FORWARD',
                    'CONFIRM_FORWARD', nextDecisionLevels)
                .then(function (data) {
                        var movement = {};
                        movement.mtef_section_id = $scope.plan.id;
                        movement.from_decision_level_id = $scope.plan.decision_level.id;
                        movement.to_decision_level_id = data.toDecisionLevelId;
                        movement.forward_message = data.comments;
                        movement.direction = 1; //Forward direction

                        ForwardMtefSectionService.forward(movement,
                            function (data) {
                                $scope.successMessage = data.successMessage;
                                $scope.plan.is_locked = true;
                                $scope.plan.decision_level = data.decisionLevel;
                            },
                            function (error) {
                                $scope.errorMessage = error.data.errorMessage;

                            }
                        );
                    },
                    function () {
                        console.log("NO");
                    });
        };

        //This function handles notification process to TR about carryover and 
        //reallocation completion by trigered by PSC
        $scope.sendNotificationToOtr = function () {
            $scope.budgetClass = null;
            var nextDecisionLevels = $scope.plan.decision_level.next_decisions;
            BudgetDialogService.showConfirmDialog(
                    'TITLE_CONFIRM_NOTIFICATION',
                    'CONFIRM_NOTIFICATION', nextDecisionLevels)
                .then(function (data) {
                        var movement = {};
                        movement.mtef_section_id = $scope.plan.id;
                        movement.from_decision_level_id = $scope.plan.decision_level.id;
                        movement.to_decision_level_id = 1;
                        movement.forward_message = data.comments;
                        movement.direction = 1; //Forward direction

                        ForwardMtefSectionService.forward(movement,
                            function (data) {
                                $scope.successMessage = data.successMessage;
                                $scope.plan.is_locked = true;
                                $scope.plan.decision_level = data.decisionLevel;
                            },
                            function (error) {
                                $scope.errorMessage = error.data.errorMessage;

                            }
                        );
                    },
                    function () {
                        console.log("NO");
                    });
        };

    }

})();
