(function () {
    'use strict';

    angular.module('budgeting-module').controller('AdminHierarchyCeilingController', AdminHierarchyCeilingController);
    AdminHierarchyCeilingController.$inject     =  [
        '$scope', '$uibModal', '$timeout','AdminHierarchyCeiling', 'PreExecutionService', 'localStorageService', 'ConfirmDialogService','Upload'];
    function AdminHierarchyCeilingController(
        $scope, $uibModal, $timeout,AdminHierarchyCeiling, PreExecutionService, localStorageService, ConfirmDialogService, Upload) {

        $scope.title = "MENU_ADMIN_HIERARCHY_CEILINGS";
        $scope.currentPage = 1;
        $scope.maxSize = 5;
        $scope.docs = [];
        /**
         * @description
         * Pagination page changed loadAdminHierarchyCeilings
         */
        $scope.pageChanged = function () {
            loadAdminHierarchyCeilings();
        };

        /**
         * @description
         * If main filter changed set selected filters and load AdminHierarchyCeilings
         * @param filter {Object} {selectedFinancialYearId,selectedBudgetType,selectedAdminHierarchyId,selectedSectionId}
         */
        $scope.filterChanged = function (filter) {
            $scope.adminHierarchyCeilings = [];
            $scope.errorMessage = undefined;
            $scope.selectedFinancialYearId = filter.selectedFinancialYearId;
            $scope.selectedFinancialYearName = filter.selectedFinancialYearName;
            $scope.selectedBudgetType = filter.selectedBudgetType;
            $scope.selectedAdminHierarchyId = filter.selectedAdminHierarchyId;
            $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
            $scope.selectedAdminHierarchyName = filter.selectedAdminHierarchyName;
            $scope.selectedSectionId = filter.selectedSectionId;
            $scope.selectedSectionName = filter.selectedSectionName;
            $scope.searchQuery = filter.searchQuery;
            $scope.selectedLevelPosition = filter.selectedSectionLevelPosition;
            if($scope.selectedBudgetType === 'CURRENT') {
               checkProjectOutput();
            }

            loadAdminHierarchyCeilings();
        };

        /**
         * @description
         * Load paginated AdminHierarchyCeilings by {selectedBudgetType,financialYearId,adminHierarchyId,sectionId}
         * @return {Object} {data:[{AdminHierarchyCeiling}],page,total_pages}
         */
        var loadAdminHierarchyCeilings = function () {
            if ($scope.selectedAdminHierarchyId === undefined ||
                $scope.selectedSectionId === undefined ||
                $scope.selectedFinancialYearId === undefined ||
                $scope.selectedBudgetType === undefined) {
                return;
            }
            if ($scope.selectedBudgetType === 'SUPPLEMENTARY' &&    $scope.selectedLevelPosition === 1) {
               loadDoc();
            } else {
                loadPage();
            }

        };

        var loadPage = function() {
            AdminHierarchyCeiling.paginate({
                budgetType: $scope.selectedBudgetType,
                financialYearId: $scope.selectedFinancialYearId,
                adminHierarchyId: $scope.selectedAdminHierarchyId,
                sectionId: $scope.selectedSectionId,
                searchQuery: $scope.searchQuery,
                page: $scope.currentPage,
                perPage: $scope.perPage
            },
            function (data) {
                $scope.adminHierarchyCeilings = data.adminHierarchyCeilings;
                $scope.atStart = data.atStart;
                loadNextCeilingChain();
            });
        };

        var loadDoc = function() {
            console.log('loading docs');
            AdminHierarchyCeiling.getDocs({
                budgetType: $scope.selectedBudgetType,
                financialYearId: $scope.selectedFinancialYearId,
                adminHierarchyId: $scope.selectedAdminHierarchyId,
            }, function(data) {
                $scope.docs = data.ceilingDocs;
                // if ($scope.docs.length > 0) {
                // }
              loadPage();

            });
        };

        $scope.upload = function(file) {
            console.log(file);
            if (file === undefined) {
                $scope.invalidFileInput = true;
                return;
            }
            $scope.invalidFileInput = false;
            file.upload = Upload.upload({
                url: '/api/admin-ceiling-docs/upload', //route to controller in laravel
                method: 'POST',
                data: {
                    budgetType: $scope.selectedBudgetType,
                    financialYearId: $scope.selectedFinancialYearId,
                    adminHierarchyId: $scope.selectedAdminHierarchyId,
                    file: file
                }
            }). success(function(data){
               $scope.docs = data.ceilingDocs;
               loadPage();
            }).error(function(error){
                $scope.errorMessage = error.errorMessage;
            });
        };

        $scope.removeFile = function (item) {
            AdminHierarchyCeiling.deleteDoc(item,function (data) {
              $scope.docs = data.ceilingDocs;
              loadPage();
            });
        }

        /**
         * @description
         * load next Allocation admin hierarchy ceiling chain for selectedAdminHierarchyId and selectedSectionId
         * @return {Object} {}
         */
        var loadNextCeilingChain = function () {
            AdminHierarchyCeiling.getNextCeilingChain(
                {
                    adminHierarchyId: $scope.selectedAdminHierarchyId,
                    sectionId: $scope.selectedSectionId
                },
                function (data) {
                    console.log(data);
                    $scope.nextCeilingChain = data.nextCeilingChain;
                },
                function (error) {

                });
        };

        $scope.loadRights = function () {
            $scope.rights = localStorageService.get(localStorageKeys.RIGHT);
        }();

        $scope.hasPermission = function (permission) {
            if ($scope.rights !== undefined && $scope.rights !== null) {
                var rights = JSON.parse($scope.rights);
                return rights.indexOf(permission) > -1;
            }
            return false;
        };

        /**
         * @description
         * Toggle Admin Hierarchy Ceiling locked or unlocked by id
         *
         * @param id
         * @param isLocked
         */
        $scope.toggleLocked = function (id, isLocked) {
            AdminHierarchyCeiling.toggleLocked({
                adminHierarchyCeilingId: id,
                isLocked: isLocked
            }, function (data) {
                $scope.successMessage = data.successMessage;
                loadAdminHierarchyCeilings();
            }, function (error) {
                console.log(error);
                loadAdminHierarchyCeilings();
                $scope.errorMessage = error.data.errorMessage;
            });
        };

        $scope.toggleAllLocked = function (budgetType, financialYearId, adminHierarchyId, ceilingId, status) {
            console.log(status);
            AdminHierarchyCeiling.toggleAllLocked({
                budgetType: budgetType,
                adminHierarchyId: adminHierarchyId,
                financialYearId: financialYearId,
                ceilingId: ceilingId,
                status: (status == false) ? 0 : 1
            }, function (data) {
                $scope.successMessage = data.successMessage;
                loadAdminHierarchyCeilings();
            }, function (error) {
                console.log(error);
                loadAdminHierarchyCeilings();
                $scope.errorMessage = error.data.errorMessage;
            });
        };


        var clearMessage = function () {
            $timeout(function () {
                $scope.successMessage=undefined;
                $scope.errorMessage=undefined;
            },2000);
        };

        $scope.edit = function (adminHierarchyCeilingToEdit, currentPage) {
            var modalInstance = $uibModal.open({
                templateUrl: 'edit.html',
                backdrop: false,
                controller: function ($scope, $uibModalInstance, UpdateAdminHierarchyCeilingService) {
                    $scope.adminHierarchyCeilingToEdit = angular.copy(adminHierarchyCeilingToEdit);
                    $scope.amountPattern = /^[0-9]{0,15}$/;

                    $scope.update = function () {
                        UpdateAdminHierarchyCeilingService.update({page: currentPage}, $scope.adminHierarchyCeilingToEdit,
                            function (data) {
                                $uibModalInstance.close(data);
                            },
                            function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                                $scope.errors = error.data.errors;
                            }
                        );

                    };
                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function (data) {
                    $scope.successMessage = data.successMessage;
                    loadAdminHierarchyCeilings();
                    clearMessage();
                },
                function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
        };

        $scope.delete = function (id) {

            ConfirmDialogService.showConfirmDialog(
                    'Delete Ceiling',
                    'Are you sure you want to delete this ceiling')
                .then(function () {
                    AdminHierarchyCeiling.delete({id:id},
                        function (data) {
                            loadAdminHierarchyCeilings();
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {

            });
        };

        /**
         *
         * @param budgetType
         * @param financialYearId
         * @param adminHierarchyId
         * @param sectionId
         */
        $scope.initiate = function (budgetType, financialYearId, adminHierarchyId, sectionId) {
            var modalInstance = $uibModal.open({
                templateUrl: 'initiate-ceiling.html',
                backdrop: false,
                controller: function ($scope, $uibModalInstance, AdminHierarchyCeiling) {

                    $scope.pageChanged = function () {
                        $scope.loadCeiling();
                    };
                    $scope.loadCeiling = function () {
                        AdminHierarchyCeiling.paginateCeilingToInitiate({
                            budgetType : budgetType,
                            financialYearId : financialYearId,
                            adminHierarchyId : adminHierarchyId,
                            sectionId : sectionId,
                            searchQueryFund : $scope.searchQueryFund,
                            searchQueryBc : $scope.searchQueryBc
                        }, function (data) {
                            $scope.ceilings = data;
                        }, function (error) {
                            console.log(error);
                            $scope.errorMessage = error.data.errorMessage;
                        });
                    };
                    $scope.initiate = function (ceilingId) {
                        AdminHierarchyCeiling.initiate({
                            ceilingId : ceilingId,
                            budgetType : budgetType,
                            financialYearId : financialYearId,
                            adminHierarchyId : adminHierarchyId,
                            sectionId : sectionId
                        },function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.loadCeiling();
                        },function (error) {
                            console.log(error);
                            $scope.errorMessage = error.data.errorMessage;
                        });
                    };
                    $scope.loadCeiling();

                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function (data) {
                    $scope.successMessage = data.successMessage;
                    loadAdminHierarchyCeilings();
                    clearMessage();
                },
                function () {
                    loadAdminHierarchyCeilings();
                    console.log('Modal dismissed at: ' + new Date());
                });
        };

        /**
         *
         * @param budgetType
         * @param financialYearId
         * @param adminHierarchyId
         * @param sectionId
         */
        $scope.initiateBankBalance = function (financialYearId, adminHierarchyId, sectionId) {
            var modalInstance = $uibModal.open({
                templateUrl: '/pages/budgeting/bank_account_balance/initiate.html',
                backdrop: false,
                controller: function ($scope, $uibModalInstance, AdminHierarchyCeiling, BankAccount) {

                    //Get Bank balances
                    BankAccount.get({}, function(data){
                       $scope.bankAccounts = data. bankAccounts;
                    });

                    //Get Bank balance amount and ceilings
                    $scope.loadBackAccountBalance = function(){
                        if($scope.bankAccount === undefined){
                            return;
                        }
                        BankAccount.getBalanceAndCeiling({
                            id: $scope.bankAccount.id,
                            financialYearId: financialYearId,
                            adminHierarchyId: adminHierarchyId,
                            sectionId: sectionId
                        }, function(data){
                            $scope.bankAccount.balance = data.balance;
                            $scope.bankAccount.ceilings = data.ceilings;
                        });
                    };
                    $scope.saveBalance = function(bankAccount){
                        console.log(bankAccount.amount);
                    };
                    //Save amount and ceilings

                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function (data) {
                    $scope.successMessage = data.successMessage;
                    loadAdminHierarchyCeilings();
                    clearMessage();
                },
                function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
        };


        /**
         *
         * @param adminHierarchyCeilingToAllocate
         * @param currentPage
         * @param nextCeilingChain
         */
        $scope.firstLowerLevel = function (adminHierarchyCeilingToAllocate, currentPage, nextCeilingChain) {
            var modalInstance = $uibModal.open({
                templateUrl: 'first_dissemination.html',
                backdrop: false,
                controller: function ($scope, $filter, $uibModalInstance,ConfirmDialogService, AdminHierarchyCeiling, UpdateLowerCeilingsService) {
                    $scope.adminHierarchyCeilingToEdit = angular.copy(adminHierarchyCeilingToAllocate);
                    $scope.nextCeilingChain = nextCeilingChain;
                    $scope.inProgress = true;
                    $scope.errorMessage = undefined;

                    var loadChildren = function() {
                        $scope.childrenLoading = true;
                        AdminHierarchyCeiling.nextLowerCeilingChain({
                            budgetType:adminHierarchyCeilingToAllocate.budget_type,
                            ceilingId: adminHierarchyCeilingToAllocate.ceiling_id,
                            financialYearId: adminHierarchyCeilingToAllocate.financial_year_id,
                            parentAdminHierarchyId: adminHierarchyCeilingToAllocate.admin_hierarchy_id,
                            parentSectionId: adminHierarchyCeilingToAllocate.section_id
                        }, function (data) {
                            $scope.childrenLoading = false;
                            $scope.lowerLevels = data.adminHierarchyCeilings;
                            $scope.inProgress = false;
                            $scope.errorMessage = undefined;
                            $scope.available = parseFloat($scope.adminHierarchyCeilingToEdit.amount);
                            $scope.total = 0;
                            angular.forEach($scope.lowerLevels, function (value, index) {
                                $scope.total += parseFloat(value.amount);
                            });
                        }, function (error) {
                            $scope.childrenLoading = false;
                            $scope.inProgress = false;
                            $scope.errorMessage = error.data.errorMessage;
                        });
                    };
                    loadChildren();

                    $scope.getIfBalance = function (total, available) {
                        var diff = Math.ceil(available) - Math.ceil(total);
                        return diff > 100;
                    };

                    $scope.getIfExceded = function (total, available) {
                        var diff = Math.ceil(total) - Math.ceil(available);
                        return diff > 0;
                    };

                    $scope.getAllocationDifference = function (total, available) {
                        var diff = Math.ceil(available) - Math.ceil(total);
                        return (diff < 0);
                    };

                    $scope.valueChanged = function () {
                        $scope.total = 0;
                        angular.forEach($scope.lowerLevels, function (value, index) {
                            $scope.total += parseFloat(value.amount);
                        });
                    };
                    $scope.getInitialPercentage = function (amount) {
                        var percentage = amount * 100 / $scope.available;
                        return $filter('number')(percentage, 2);
                    };
                    $scope.setPercentage = function (c) {
                        if (c !== undefined) {
                            c.isTouched = true;
                            var percentage = c.amount * 100 / $scope.available;
                            c.percentage = $filter('number')(percentage, 2);
                            $scope.valueChanged();
                        }
                    };
                    $scope.setAmount = function (c) {
                        if (c !== undefined) {
                            c.amount = ($scope.available * c.percentage) / 100;
                            c.isTouched = true;
                            $scope.valueChanged();
                        }
                    };

                    $scope.update = function () {
                        var toSave = _.filter($scope.lowerLevels,function (c) {
                            return c.isTouched;
                        });
                        $scope.errorMessage = undefined;
                        $scope.errorSectionId = undefined;
                        $scope.errorFacilityId = undefined;
                        $scope.errorAmount = undefined;

                        if(toSave.length) {
                            toSave = toSave.map(function (c) {
                                return {id:c.id, amount:c.amount};
                            });
                            UpdateLowerCeilingsService.update({
                                    adminHierarchyCeilingId: $scope.adminHierarchyCeilingToEdit.id,
                                    disseminationStatusCode:($scope.total === $scope.available) ? 1 : 0
                                }, toSave,
                                function (data) {
                                    adminHierarchyCeilingToAllocate.dissemination_status = 1;
                                    $uibModalInstance.close(data);
                                },
                                function (error) {
                                    $scope.errorMessage = error.data.errorMessage;
                                    $scope.errorSectionId = error.data.sectionId;
                                    $scope.errorFacilityId = error.data.facilityId;
                                    $scope.errorAmount = error.data.amount;
                                }
                            );
                        }
                        else {
                            $uibModalInstance.close({successMessage:'Ceiling already saved successfully'});
                        }
                    };
                    $scope.delete = function (id) {

                        ConfirmDialogService.showConfirmDialog(
                                'Delete Ceiling',
                                'Are you sure you want to delete this ceiling')
                            .then(function () {
                                AdminHierarchyCeiling.delete({
                                        id:id
                                        },
                                        function (data) {
                                            loadChildren();
                                        },
                                        function (error) {
                                            $scope.errorMessage = error.data.errorMessage;
                                        }
                                    );
                                },
                                function () {

                                });
                    };
                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            modalInstance.result.then(function (data) {
                    $scope.successMessage = data.successMessage;
                    loadAdminHierarchyCeilings();
                    clearMessage();
                },
                function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
        };

        var checkProjectOutput = function () {
            PreExecutionService.projectOutputFilled(
                {
                    admin_hierarchy_id: $scope.selectedAdminHierarchyId,
                    sectionId: $scope.selectedSectionId
                },
                function(data){
                if(data.items != null){
                    $scope.isBlocked = true;
                    $scope.errorMessage = "Complete filling the Project Output for development activities first. Go to Planning menu to update activities";
                    $scope.notfilledSection = data.items;
                }else {
                    $scope.isBlocked = false;
                    $scope.errorMessage = undefined;
                    $scope.notfilledSection = null;
                }
            });
        };

    }

})();
