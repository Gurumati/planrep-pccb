function CurrentFinancialYearPlansController($scope, $animate, BudgetService, ConfirmDialogService, Activity) {

    $scope.title = "CURRENT_FINANCIAL_YEAR_PLAN";
    $scope.currentPage = 1;
    $scope.maxSize = 5;
    $animate.enabled(false);

    $scope.approveBudget = function (mtefId) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Approve Financial Year Plan!',
            'Are sure you want to approve this financial year')
            .then(function () {
                    BudgetService.approveBudget({mtefId:mtefId, budget_type: $scope.selectedBudgetType},function (data) {
                        $scope.getBudget();
                        $scope.successMessage = data.successMessage;
                    },function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    });
                },
                function () {
                });
    };

    $scope.disapproveBudget = function (mtefId) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Disapprove Financial Year Plan!',
            'Are sure you want to disApprove this financial year')
            .then(function () {
                    BudgetService.disapproveBudget({mtefId:mtefId, budget_type: $scope.selectedBudgetType},function (data) {
                        $scope.getBudget();
                        $scope.successMessage = data.successMessage;
                    },function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    });
                },
                function () {
                });
    };

    $scope.approveCeiling = function (financialYearId,adminHierarchyId, budget_type) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Approve Ceilings!',
            'Are sure you want to approve this Ceiling')
            .then(function () {
                    BudgetService.approveCeiling({financialYearId:financialYearId,adminHierarchyId:adminHierarchyId, budget_type: budget_type},
                        function (data) {
                            $scope.getBudget();
                            $scope.successMessage = data.successMessage;
                    },function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    });
                },
                function () {
                });
    };

    $scope.disapproveCeiling = function (financialYearId,adminHierarchyId, budget_type) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Disapprove Ceiling!',
            'Are sure you want to disApprove this ceiling')
            .then(function () {
                    BudgetService.disapproveCeiling({financialYearId:financialYearId,adminHierarchyId:adminHierarchyId,budget_type},
                        function (data) {
                            $scope.getBudget();
                            $scope.successMessage = data.successMessage;
                    },function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    });
                },
                function () {
                });
    };

    /** regereneate segments  */
    $scope.resetBudget = function (mtefId, budget_type) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Approve Financial Year Plan!',
            'Are sure you want to reset budget for the current financial year plans')
            .then(function () {
                    BudgetService.resetBudget({mtefId:mtefId, budget_type: budget_type},function (data) {
                        $scope.getBudget();
                        $scope.successMessage = data.successMessage;
                    },function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    });
                },
                function () {
                });
    };


    /**
     * @description
     * If main filter changed set selected filters and load AdminHierarchyCeilings
     * @param filter {Object} {selectedFinancialYearId,selectedBudgetType,selectedAdminHierarchyId,selectedSectionId}
     */
    $scope.filterChanged = function (filter) {
        $scope.selectedFinancialYearId = filter.selectedFinancialYearId;
        $scope.selectedFinancialYearName = filter.selectedFinancialYearName;
        $scope.selectedBudgetType = filter.selectedBudgetType;
        $scope.selectedAdminHierarchyId = filter.selectedAdminHierarchyId;
        $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
        if($scope.selectedAdminHierarchyId !== null && $scope.selectedBudgetType !== null && $scope.selectedAdminHierarchyLevelPosition !== 1){
            $scope.getBudget();
        }       
    };

    $scope.getBudget = function() {
        BudgetService.getAggregateBudget({admin_hierarchy_id : $scope.selectedAdminHierarchyId, 
            hierarchy_position : $scope.selectedAdminHierarchyLevelPosition,
            financial_year_id  : $scope.selectedFinancialYearId,
            budget_type        : $scope.selectedBudgetType},function (data) {
                $scope.plans = data.plans;
                },function (error) {
                $scope.errorMessage = error.data.errorMessage;
            });
    };

    $scope.getAbsDiff = function(ceiling,peCeiling, budget, peBudget) {
      var diff =(parseInt(ceiling,10) + parseInt(peCeiling,10)) - (parseInt(budget,10) + parseInt(peBudget,10));
      return  Math.abs(diff);
    };

    $scope.disApproveBySystem = function(system) {
        ConfirmDialogService.showConfirmDialog(
            'Disapprove activity for '+system,
            'Confirm disapprove activity for '+ system)
        .then(function () {
                Activity.disApproveBySystem({
                        adminHierarchyId: $scope.selectedAdminHierarchyId,
                        financialYearId: $scope.selectedFinancialYearId,
                        budgetType: $scope.selectedBudgetType,
                        financialSystem: system
                    },
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.getBudget();
                    },
                    function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    };

    $scope.approveBySystem = function (system) {

        ConfirmDialogService.showConfirmDialog(
                'Approve activities for '+ system,
                'Confirm approve activity for '+ system)
            .then(function () {
                    Activity.approveBySystem({
                            adminHierarchyId: $scope.selectedAdminHierarchyId,
                            financialYearId: $scope.selectedFinancialYearId,
                            budgetType: $scope.selectedBudgetType,
                            financialSystem: system
                        },
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.clearMessage();
                            loadTargets();
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {

                });
    };

}