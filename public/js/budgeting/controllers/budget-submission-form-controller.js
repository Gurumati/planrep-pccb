function BudgetSubmissionFormController($scope,
                              AllBudgetSubmissionFormsModel,
                              BudgetSubmissionDefinitionsModel,
                              $uibModal,
                              ConfirmDialogService,
                                        PaginateBudgetSubmissionDefinitionService,
                              AllUnitsService,
                              ExpenditureGsfCodeService,
                              AllBudgetSubmissionFormsService,
                              UpdateBudgetSubmissionFormService,
                              UpdateBudgetSubmissionFormDefinitionService,
                              DeleteBudgetSubmissionDefinitionService,
                              FundSourceService,
                              CreateBudgetSubmissionColumnService,
                              BudgetClassService,
                              GetBudgetSubmissionFormsService,
                              CreateBudgetSubmissionFormService,
                              DeleteBudgetSubmissionFormService,
                              SubmissionFormSelectGroupService
                              ) {

    $scope.budgetSubmissionForms = AllBudgetSubmissionFormsModel;
    $scope.budgetSubmissionDefinitions = BudgetSubmissionDefinitionsModel;
    $scope.title = "PE_BUDGET_SUBMISSION_FORM";
    $scope.dateFormat = 'yyyy-MM-DD';
    //Initialize the mode by which the UI will be loaded
    $scope.columnListMode = false;
    $scope.defaultMode = true;
    $scope.maxSize = 3;
    $scope.currentPage = 1;


    $scope.PageChanged = function (form_id) {
        PaginateBudgetSubmissionDefinitionService.get({form_id: form_id, page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
            $scope.budgetSubmissionDefinitions = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/budget_submission/form/create.html',
            backdrop: false,
            controller: function ($scope,
                                  $uibModalInstance,
                                  CreateBudgetSubmissionFormService,
                                  BudgetClassService
                                  ) {
                //This service pulls all budget classes from the database
                BudgetClassService.query({}, function (data) {
                        $scope.budgetClasses=data;
                    },
                    function (error) {
                        console.log(error);
                    });

                //get fund sources
                FundSourceService.query({}, function (data) {
                        $scope.fundSources = data;
                    },
                    function (error) {
                        console.log(error);
                    });
                //end fund sources
                $scope.budgetSubmissionFormToCreate = {};
                //Function to store data and close modal when Create button clicked

                $scope.store = function () {
                    if ($scope.createBudgetSubmissionForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateBudgetSubmissionFormService.store($scope.budgetSubmissionFormToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.budgetSubmissionForms = data.budgetSubmissionForms;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.addColumn = function (form_id) {
        //Modal Form for adding a column to the budget submission definition table
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/budget_submission/form/add-column.html',
            backdrop: false,
            controller: function ($scope,
                                  $uibModalInstance,
                                  CreateBudgetSubmissionColumnService,
                                  loadParentFormColumnService
            ) {
                $scope.isSelect = false;
                //load parent form
                loadParentFormColumnService.query({column_id: 0, form_id: form_id}, function (data) {
                    $scope.ParentColumns = data;
                });
                //This service pulls all units from the database
                AllUnitsService.query({}, function (data) {
                        $scope.units=data;
                    },
                    function (error) {
                        console.log(error);
                    });

                //This service pulls all GFS codes from the database
                ExpenditureGsfCodeService.query({}, function (data) {
                        $scope.gfsCodes=data;
                    },
                    function (error) {
                        console.log(error);
                    });

                //This service pulls all GFS codes from the database
                AllBudgetSubmissionFormsService.query({}, function (data) {
                        $scope.budgetSubmissionForms=data;
                    },
                    function (error) {
                        console.log(error);
                    });

                SubmissionFormSelectGroupService.query({}, function (data) {
                        $scope.selectOptions=data;
                    },
                    function (error) {
                        console.log(error);
                    });

                //compare type
                $scope.compareType = function () {
                   if($scope.budgetSubmissionDefinitionFormToCreate.type == 'select')
                   {
                       $scope.isSelect = true;
                   }else {
                       $scope.isSelect = false;
                   }
                };

                $scope.budgetSubmissionDefinitionFormToCreate = {};
                //Function to store data and close modal when Create button clicked

                $scope.storeColumn = function () {
                    if ($scope.createBudgetSubmissionDefinitionForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                     $scope.budgetSubmissionDefinitionFormToCreate.budget_submission_form_id = form_id;
                     CreateBudgetSubmissionColumnService.store($scope.budgetSubmissionDefinitionFormToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.budgetSubmissionDefinitions = data.budgetSubmissionDefinitions;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    //The function below modifies a budget submission form
    $scope.edit = function (budgetSubmissionFormToEdit) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/budget_submission/form/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {

                //This service pulls all budget classes from the database
                BudgetClassService.query({}, function (data) {
                        $scope.budgetClasses=data;
                    },
                    function (error) {
                        console.log(error);
                    });

                //get fund sources
                FundSourceService.query({}, function (data) {
                        $scope.fundSources = data;
                    },
                    function (error) {
                        console.log(error);
                    });
                //end fund sources

                $scope.budgetSubmissionFormToEdit = angular.copy(budgetSubmissionFormToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    $uibModalInstance.close($scope.budgetSubmissionFormToEdit);
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (budgetSubmissionFormToEdit) {
                //Service to create new budget class
                UpdateBudgetSubmissionFormService.update(budgetSubmissionFormToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage = data.successMessage;
                        $scope.budgetSubmissionForms = data.budgetSubmissionForms;
                    },
                    function (error) {
                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.deleteBudgetSubmissionForm = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Delete Budget Submission Form!',
            'Are sure you want to delete this?')
            .then(function () {
                    console.log("YES");
                    DeleteBudgetSubmissionFormService.delete({form_id: id},
                        function (data) {
                            $scope.message = data.message;
                            $scope.budgetSubmissionForms = data.budgetSubmissionForms;
                        }, function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }

    $scope.deleteBudgetSubmissionDefinition = function (budgetSubmissionDefinition, currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Delete Budget Submission Column!',
            'Are sure you want to delete this?')
            .then(function () {
                    console.log("YES");
                    DeleteBudgetSubmissionDefinitionService.delete({currentPage: currentPage, perPage: perPage}, budgetSubmissionDefinition,
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.budgetSubmissionDefinitions = data.budgetSubmissionDefinitions;
                            $scope.currentPage = $scope.budgetSubmissionDefinitions.current_page;
                        }, function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }

    $scope.viewColumns = function (form_id) {
        $scope.defaultMode = false;
        $scope.columnListMode=true;
        $scope.form_id = form_id;
        $scope.perPage = 10;

        PaginateBudgetSubmissionDefinitionService.get({form_id:form_id,page:1,perPage:$scope.perPage}, function (data) {
            $scope.budgetSubmissionDefinitions = data;
            $scope.totalItems = data.total;
            $scope.perPage = data.per_page;
            $scope.currentPage = $scope.budgetSubmissionDefinitions.current_page;

            },
            function (error) {
                $scope.errorMessage = error.data.errorMessage;
            });
    };

    $scope.editColumns = function (budgetSubmissionDefinitionFormToEdit,currentPage,perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/budget_submission/form/edit-column.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, loadParentFormColumnService) {
                $scope.budgetSubmissionDefinitionFormToEdit = angular.copy(budgetSubmissionDefinitionFormToEdit);

                //load parent form
                loadParentFormColumnService.query({column_id: $scope.budgetSubmissionDefinitionFormToEdit.id, form_id: $scope.budgetSubmissionDefinitionFormToEdit.budget_submission_form_id},
                    function (data) {
                        $scope.ParentColumns = data;
                    });

                //This service pulls all units from the database
                AllUnitsService.query({}, function (data) {
                        $scope.units=data;
                    },
                    function (error) {
                        console.log(error);
                    });

                //This service pulls all GFS codes from the database
                ExpenditureGsfCodeService.query({}, function (data) {
                        $scope.gfsCodes=data;
                    },
                    function (error) {
                        console.log(error);
                    });

                SubmissionFormSelectGroupService.query({}, function (data) {
                        $scope.selectOptions=data;
                    },
                    function (error) {
                        console.log(error);
                    });

                //compare type
                $scope.compareType = function () {
                    if($scope.budgetSubmissionDefinitionFormToEdit.type == 'select')
                    {
                        $scope.isSelect = true;
                    }else {
                        $scope.isSelect = false;
                    }
                };

                //Function to store data and close modal when Create button clicked
                $scope.updateColumns = function () {
                    $uibModalInstance.close($scope.budgetSubmissionDefinitionFormToEdit);
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (budgetSubmissionDefinitionFormToEdit) {
                //Service to create new budget class
                UpdateBudgetSubmissionFormDefinitionService.update({page: currentPage,perPage:perPage}, budgetSubmissionDefinitionFormToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage = data.successMessage;
                        $scope.budgetSubmissionDefinitions = data.budgetSubmissionDefinitions;
                        $scope.currentPage = $scope.budgetSubmissionDefinitions.current_page;
                    },
                    function (error) {
                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.backToForms = function () {
        $scope.defaultMode    = true;
        $scope.columnListMode = false;
    };

}

BudgetSubmissionFormController.resolve = {
    AllBudgetSubmissionFormsModel: function (GetBudgetSubmissionFormsService, $q) {
        var deferred = $q.defer();
        GetBudgetSubmissionFormsService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    },
    BudgetSubmissionDefinitionsModel: function (PaginateBudgetSubmissionDefinitionService, $q) {
        var deferred = $q.defer();
        PaginateBudgetSubmissionDefinitionService.get({page:1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};
