function CeilingSectorController($scope, AllCeilingSectorModel,ConfirmDialogService,GetAllCeilingSectorService,RemoveCeilingSectorService) {

    $scope.ceiling_sectors = AllCeilingSectorModel;
    $scope.title = "CEILING_SECTORS";

    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        GetAllCeilingSectorService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
            $scope.ceiling_sectors = data;
        });
    };
    $scope.delete = function (id, currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Delete Ceiling Sector',
            'Are sure you want to delete this?')
            .then(function () {
                    RemoveCeilingSectorService.delete({ceiling_sector_id: id, page: currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.ceiling_sectors = data.ceiling_sectors;
                            $scope.currentPage = $scope.ceiling_sectors.current_page;
                        }, function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    };
}

CeilingSectorController.resolve = {
    AllCeilingSectorModel: function (GetAllCeilingSectorService, $q) {
        var deferred = $q.defer();
        GetAllCeilingSectorService.get({page:1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};