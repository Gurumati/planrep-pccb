function ExpenditureCentreValueController($scope, ExpenditureCentreValueModel, $uibModal,ToggleExpenditureCentreValueService,
                                     ConfirmDialogService, DeleteExpenditureCentreValueService, PaginatedExpenditureCentreValueService) {

    $scope.expenditureCentreValues = ExpenditureCentreValueModel;
    $scope.title = "EXPENDITURE_CENTRE_VALUES";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedExpenditureCentreValueService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.expenditureCentreValues = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/expenditure_centre_value/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllExpenditureCentreService, CreateExpenditureCentreValueService) {
                $scope.expenditureCentreValueToCreate = {};

                AllExpenditureCentreService.query(function (data) {
                    $scope.expenditureCentres = data;
                });

                $scope.store = function () {
                    if ($scope.createExpenditureCentreValueForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateExpenditureCentreValueService.store({perPage: $scope.perPage}, $scope.expenditureCentreValueToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.expenditureCentreValues = data.expenditureCentreValues;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (expenditureCentreValueToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/expenditure_centre_value/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllExpenditureCentreService, UpdateExpenditureCentreValueService) {

                AllExpenditureCentreService.query(function (data) {
                    $scope.expenditureCentres = data;
                    $scope.expenditureCentreValueToEdit = angular.copy(expenditureCentreValueToEdit);
                });
                
                $scope.update = function () {
                    if ($scope.updateExpenditureCentreValueForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateExpenditureCentreValueService.update({page: currentPage, perPage: perPage}, $scope.expenditureCentreValueToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.expenditureCentreValues = data.expenditureCentreValues;
                $scope.currentPage = $scope.expenditureCentreValues.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('CONFIRM_DELETE', 'Are you sure you want to trash this record?').then(function () {
                DeleteExpenditureCentreValueService.delete({expenditure_centre_value_id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.expenditureCentreValues = data.expenditureCentreValues;
                        $scope.currentPage = $scope.expenditureCentreValues.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/budgeting/expenditure_centre_value/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedExpenditureCentreValueService,
                                  RestoreExpenditureCentreValueService, EmptyExpenditureCentreValueTrashService, PermanentDeleteExpenditureCentreValueService) {
                TrashedExpenditureCentreValueService.query(function (data) {
                    $scope.trashedExpenditureCentreValues = data;
                });
                $scope.restoreExpenditureCentreValue = function (id) {
                    RestoreExpenditureCentreValueService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedExpenditureCentreValues = data.trashedExpenditureCentreValues;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteExpenditureCentreValueService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedExpenditureCentreValues = data.trashedExpenditureCentreValues;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyExpenditureCentreValueTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedExpenditureCentreValues = data.trashedExpenditureCentreValues;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.expenditureCentreValues = data.expenditureCentreValues;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.toggleActive = function (id, is_active,perPage) {
        $scope.expenditureCentreValueToActivate = {};
        $scope.expenditureCentreValueToActivate.id = id;
        $scope.expenditureCentreValueToActivate.is_active = is_active;
        ToggleExpenditureCentreValueService.toggleActive({perPage:perPage},$scope.expenditureCentreValueToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    };
}

ExpenditureCentreValueController.resolve = {
    ExpenditureCentreValueModel: function (PaginatedExpenditureCentreValueService, $q) {
        var deferred = $q.defer();
        PaginatedExpenditureCentreValueService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};