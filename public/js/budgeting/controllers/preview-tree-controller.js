function PreviewTreeController($scope, PreviewTreesModel, $uibModal,
                               GetAFullPlanChainService) {

    $scope.fullPlanChain = PreviewTreesModel;
    $scope.title = "FULL_PLAN_CHAIN";
    $scope.dateFormat = 'yyyy-MM-dd';
    // $scope.currentPage=1;

    // $scope.pageChanged=function () {
    //     GetAFullPlanChainService.get({page:$scope.currentPage}, function (data) {
    //         $scope.fullPlanChain=data;
    //     });
    // };

}

PreviewTreeController.resolve = {
    PreviewTreesModel: function (GetAFullPlanChainService, $q) {
        var deferred = $q.defer();
        // GetAFullPlanChainService.get({page:1}, function (data) {
        //     deferred.resolve(data);
        // });
        GetAFullPlanChainService.get( function (data) {
            deferred.resolve(data);
        });


        return deferred.promise;
    }
};