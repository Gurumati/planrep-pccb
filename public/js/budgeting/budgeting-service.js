var budgetingServices = angular.module('budgetingServices', ['ngResource']);

// Retiring Employees
budgetingServices.factory('GetAllRetiringEmployeeService', function ($resource) {
    return $resource('/json/paginatedRetiringEmployees', {}, {});
});
budgetingServices.factory('CreateRetiringEmployeeService', function ($resource) {
    return $resource('/json/createRetiringEmployee', {}, {store: {method: 'POST'}});
});
budgetingServices.factory('UpdateRetiringEmployeeService', function ($resource) {
    return $resource('/json/updateRetiringEmployee', {}, {update: {method: 'POST'}});
});
budgetingServices.factory('DeleteRetiringEmployeeService', function ($resource) {
    return $resource('/json/deleteRetiringEmployee/:retiring_employee_id', {retiring_employee_id: '@retiring_employee_id'}, {delete: {method: 'GET'}});
});
budgetingServices.factory('AllSectionService', function ($resource) {
    return $resource('/json/all-sections', {}, {});
});

budgetingServices.factory('AllAdminHierarchyService', function ($resource) {
    return $resource('/json/all-admin-hierarchies', {}, {});
});

// PE BUDGETS
budgetingServices.factory('GetAllPeBudgetService', function ($resource) {
    return $resource('/json/paginatedPeBudgets', {}, {});
});

budgetingServices.factory('CreatePeBudgetService', function ($resource) {
    return $resource('/json/createPeBudget', {}, {store: {method: 'POST'}});
});
budgetingServices.factory('UpdatePeBudgetService', function ($resource) {
    return $resource('/json/updatePeBudget', {}, {update: {method: 'POST'}});
});
budgetingServices.factory('DeletePeBudgetService', function ($resource) {
    return $resource('/json/deletePeBudget/:pe_budget_id', {pe_budget_id: '@pe_budget_id'}, {delete: {method: 'GET'}});
});

budgetingServices.factory('AllSectionService', function ($resource) {
    return $resource('/json/allSections', {}, {});
});

budgetingServices.factory('AllMtefService', function ($resource) {
    return $resource('/json/allMtefs', {}, {});
});

//CEILINGS
budgetingServices.factory('GetAllCeilingService', function ($resource) {
    return $resource('/json/paginatedCeilings', {}, {});
});

budgetingServices.factory('GetAllCeilingSectorService', function ($resource) {
    return $resource('/json/paginatedCeilingSectors', {}, {});
});

budgetingServices.factory('CreateCeilingService', function ($resource) {
    return $resource('/json/createCeiling', {}, {store: {method: 'POST'}});
});
budgetingServices.factory('UpdateCeilingService', function ($resource) {
    return $resource('/json/updateCeiling', {}, {update: {method: 'POST'}});
});

budgetingServices.factory('DeleteCeilingService', function ($resource) {
    return $resource('/json/deleteCeiling/:ceiling_id', {ceiling_id: '@ceiling_id'}, {delete: {method: 'GET'}});
});

budgetingServices.factory('ToggleCeilingService', function ($resource) {
    return $resource('/json/toggleActiveCeiling/:ceilingId', {ceilingId: '@ceilingId'}, {change: {method: 'PUT'}});
});


//The services for the budget submission forms start here
budgetingServices.factory('GetBudgetSubmissionFormsService', function ($resource) {
    return $resource('/json/budgetSubmissionForms', {}, {});
});
budgetingServices.factory('DownloadBudgetSubmissionFormsService', function ($resource) {
    return $resource('/json/downloadBudgetSubmissionFormExcel/:form_id', {form_id: '@form_id'}, {});
});
budgetingServices.factory('BudgetSubmissionDefinitionsService', function ($resource) {
    return $resource('/json/budgetSubmissionDefinitions/:form_id', {form_id: '@form_id'}, {index: {method: 'GET'}});
});
budgetingServices.factory('PaginateBudgetSubmissionDefinitionService', function ($resource) {
    return $resource('/json/paginateBudgetSubmissionDefinitions', {}, {paginateIndex: {method: 'GET'}});
});
budgetingServices.factory('CreateBudgetSubmissionFormService', function ($resource) {
    return $resource('/json/createBudgetSubmissionForm', {}, {store: {method: 'POST'}});
});
budgetingServices.factory('UpdateBudgetSubmissionFormService', function ($resource) {
    return $resource('/json/updateBudgetSubmissionForm', {}, {update: {method: 'POST'}});
});
budgetingServices.factory('DeleteBudgetSubmissionFormService', function ($resource) {
    return $resource('/json/deleteBudgetSubmissionForm/:form_id', {form_id: '@form_id'}, {delete: {method: 'GET'}});
});
budgetingServices.factory('DeleteBudgetSubmissionDefinitionService', function ($resource) {
    return $resource('/json/deleteBudgetSubmissionDefinition', {}, {delete: {method: 'POST'}});
});
//The services for the budget submission forms end here

//ceiling
budgetingServices.factory('AllCeilingSectorService', function ($resource) {
    return $resource('/json/allCeilingSectors/:ceiling_id', {ceiling_id: '@ceiling_id'}, {sectors: {method: 'GET'}});
});
budgetingServices.factory('DeleteCeilingSectorService', function ($resource) {
    return $resource('/json/deleteCeilingSector/:ceiling_sector_id/:ceiling_id', {ceiling_sector_id: '@ceiling_sector_id',ceiling_id: '@ceiling_id'}, {deleteCeilingSector: {method: 'GET'}});
});
budgetingServices.factory('RemoveCeilingSectorService', function ($resource) {
    return $resource('/json/removeCeilingSector/:ceiling_sector_id', {ceiling_sector_id: '@ceiling_sector_id'}, {delete: {method: 'GET'}});
});
budgetingServices.factory('AddSectorService', function ($resource) {
    return $resource('/json/addSector', {}, {add_sector: {method: 'POST'}});
});
budgetingServices.factory('BankAccount', function ($resource) {
    return $resource('/json/bank-accounts/:id', {id: '@id'}, {
        getBalanceAndCeiling:{
            url:'/json/bank-accounts/balance-and-ceiling/:id/:financialYearId/:adminHierarchyId/:sectionId',
            params: {
                id: '@id',
                financialYearId: '@financialYearId',
                adminHiearchyId: '@adminHierarchyId',
                sectionId: '@sectionId'
            }
        }
    });
});
budgetingServices.factory('AdminHierarchyCeiling', function ($resource) {
    return $resource('/json/adminHierarchy-ceiling/:id', {id: '@id'}, {
        paginate :{ url:'/json/admin-hierarchy-ceiling/paginate/:budgetType/:financialYearId/:adminHierarchyId/:sectionId',
            method:'GET',
            params:{budgetType:'@budgetType',financialYearId:'@financialYearId',adminHierarchyId:'@adminHierarchyId',sectionId:'@sectionId'}},
        paginateCeilingToInitiate :{ url:'/json/admin-hierarchy-ceiling/ceiling-to-initiate/paginate/:budgetType/:financialYearId/:adminHierarchyId/:sectionId',
            method:'GET',
            params:{budgetType:'@budgetType',financialYearId:'@financialYearId',adminHierarchyId:'@adminHierarchyId',sectionId:'@sectionId'}},
        initiate :{ url:'/json/admin-hierarchy-ceiling/initiate/:ceilingId/:budgetType/:financialYearId/:adminHierarchyId/:sectionId',
            method:'PUT',
            params:{ceilingId:'@ceilingId',budgetType:'@budgetType',financialYearId:'@financialYearId',adminHierarchyId:'@adminHierarchyId',sectionId:'@sectionId'}},
        toggleLocked:{url:'/json/admin-hierarchy-ceiling/toggle-locked/:adminHierarchyCeilingId/:isLocked',
            method:'PUT',
            params:{adminHierarchyCeilingId:'@adminHierarchyCeilingId',isLocked:'@isLocked'}},
        toggleAllLocked:{
            url:'/json/admin-hierarchy-ceiling/toggle-all-locked',
            method:'PUT'},
        delete:{url:'/json/admin-hierarchy-ceiling/delete/:id',
            method:'DELETE',
            params:{id:'@id'}},
        getNextCeilingChain:{url:'/json/admin-hierarchy-ceiling/next-ceiling-chain/:adminHierarchyId/:sectionId',
            method:'GET',
            params:{adminHierarchyId:'@adminHierarchyId',sectionId:'@sectionId'}},
        getFundSourceCeiling:{url:'/json/admin-hierarchy-ceiling/fund-source-ceiling/:mtefSectionId/:budgetType/:budgetClassId/:fundSourceId/:isFacilityAccount/:facilityId/:financialYearId',
            method:'GET',
            params:{mtefSectionId:'@mtefSectionId', budgetType:'@budgetType', budgetClassId:'@budgetClassId', fundSourceId:'@fundSourceId', isFacilityAccount:'@isFacilityAccount', facilityId:'@facilityId'}},
        nextLowerCeilingChain:{url:'/json/admin-hierarchy-ceiling/next-lower-level/:ceilingId/:budgetType/:financialYearId/:parentAdminHierarchyId/:parentSectionId',
            method:'GET',
            params:{ceilingId:'@ceilingId',budgetType:'@budgetType',financialYearId:'@financialYearId',parentAdminHierarchyId:'@parentAdminHierarchyId',parentSectionId:'@parentSectionId'}},
        getByCeiling:{
            url: '/json/admin-hierarchy-ceiling/get-by-ceiling/:ceilingId',
            method: 'GET',
            params: {ceilingId:'@ceilingId'}
        },
        getDocs:{
            url: '/json/admin-ceiling-docs/:financialYearId/:adminHierarchyId/:budgetType',
            method: 'GET',
            params: {
                financialYearId:'@financialYearId',
                adminHierarchyId:'@adminHierarchyId',
                budgetType:'@budgetType'
            }
        },
        deleteDoc:{
            url: 'api/admin-ceiling-docs/delete',
            method: 'POST',
            params: {}
        }
    });
});

budgetingServices.factory('AllAdminHierarchyCeilingService', function ($resource) {
    return $resource('/json/allAdminHierarchyCeilings/:ceiling_id', {ceiling_id: '@ceiling_id'}, {});
});

budgetingServices.factory('DeleteAdminHierarchyCeilingService', function ($resource) {
    return $resource('/json/deleteAdminHierarchyCeiling/:admin_hierarchy_ceiling_id/:ceiling_id', {admin_hierarchy_ceiling_id: '@admin_hierarchy_ceiling_id',ceiling_id: '@ceiling_id'}, {deleteAdminHierarchyCeiling: {method: 'GET'}});
});

budgetingServices.factory('AddAdminHierarchyCeilingService', function ($resource) {
    return $resource('/json/addAdminHierarchyCeiling', {}, {addAdminHierarchyCeiling: {method: 'POST'}});
});

//The services for budget submission forms start here
budgetingServices.factory('BudgetSubmissionSubFormService', function ($resource) {
    return $resource('/json/budgetSubmissionSubForms', {}, {});
});

budgetingServices.factory('ParentBudgetSubmissionFormService', function ($resource) {
    return $resource('/json/parentBudgetSubmissionForm', {}, {store: {method: 'GET'}});
});
budgetingServices.factory('ParentBudgetSubmissionSubFormService', function ($resource) {
    return $resource('/json/parentBudgetSubmissionSubForm/:id', {id: '@id'}, {store: {method: 'GET'}});
});
budgetingServices.factory('UpdateBudgetSubmissionSubFormService', function ($resource) {
    return $resource('/json/updateBudgetSubmissionSubForm', {}, {store: {method: 'POST'}});
});

budgetingServices.factory('UpdateBudgetSubmissionFormDefinitionService', function ($resource) {
    return $resource('/json/updateBudgetSubmissionDefinition', {}, {update: {method: 'POST'}});
});

budgetingServices.factory('DeleteBudgetSubmissionSubFormService', function ($resource) {
    return $resource('/json/deleteBudgetSubmissionSubForm/:id', {id: '@id'}, {delete: {method: 'GET'}});
});

budgetingServices.factory('CreateBudgetSubmissionSubFormService', function ($resource) {
    return $resource('/json/createBudgetSubmissionSubForm', {}, {store: {method: 'POST'}});
});
//The services for budget submission forms end here

budgetingServices.factory('DeleteBudgetSubmissionValuesServices', function ($resource) {
    return $resource('/json/DeleteBudgetSubmissionValues', {}, {delete: {method: 'POST'}});
});
budgetingServices.factory('EditBudgetSubmissionValuesServices', function ($resource) {
    return $resource('/json/EditBudgetSubmissionValues', {}, {edit: {method: 'POST'}});
});
/**
budgetingServices.factory('BudgetSubmissionFieldValuesService', function ($resource) {
    return $resource('/json/budgetSubmissionFieldValues/:form_id/:mtef_id', { form_id: '@form_id', mtef_id: '@mtef_id'}, {loadBudget: {method: 'GET'}});
}); **/
budgetingServices.factory('BudgetSubmissionFieldValuesService', function ($resource) {
    return $resource('/json/budgetSubmissionFieldValues', {}, {
      getValues: {method:'GET',params:{form_id: '@form_id', mtef_id: '@mtef_id', budget_class_id: '@budget_class_id'} },
      getActivity: {method: 'GET', url: '/json/loadBudgetSubmissionActivity', params: {mtef_id: '@mtef_id', section_id: '@section_id', budget_class_id: '@budget_class_id', fund_source_id: '@fund_source_id', financial_year_id: '@financial_year_id'}}
	});
});
budgetingServices.factory('UpdateBudgetSubmissionLineService', function ($resource) {
    return $resource('/json/updateBudgetSubmissionLine', {}, {store: {method: 'POST'}});
});

budgetingServices.factory('ResetBudgetSubmissionLineService', function ($resource) {
    return $resource('/json/resetContributions', {}, {resetContributions: {method: 'POST'}});
});

budgetingServices.factory('loadBudgetSectionService', function ($resource) {
    return $resource('/json/loadBudgetSections', { }, {loadSection: {method: 'GET'}});
});

budgetingServices.factory('loadBudgetSubmissionSelectOptionService', function ($resource) {
    return $resource('/json/BudgetSubmissionSelectOptions', {}, {
      getChildren: {
        method: 'GET',
      }
    });
  });
budgetingServices.factory('loadParentFormColumnService', function ($resource) {
    return $resource('/json/loadParentBudgetColumn/:column_id/:form_id', {column_id: '@column_id',form_id: '@form_id' }, {});
});
budgetingServices.factory('BudgetSubmissionActivityService', function ($resource) {
    return $resource('/json/budgetSubmissionActivity/:budget_class_id', { budget_class_id: '@budget_class_id' }, {BudgetSectionActivity: {method: 'GET'}});
});
budgetingServices.factory('CreateBudgetSubmissionColumnService', function ($resource) {
    return $resource('/json/createBudgetSubmissionColumn', {}, {store: {method: 'POST'}});
});
budgetingServices.factory('BudgetSubFormDefinitionService', function ($resource) {
    return $resource('/json/budgetSubFormDefinitions/:form_id', {form_id: '@form_id'}, {index: {method: 'GET'}});
});
budgetingServices.factory('BudgetSubmissionBudgetClassesService', function ($resource) {
    return $resource('/json/BudgetSubmissionBudgetClasses/:sub_form_id/:financial_year_id', {sub_form_id: '@sub_form_id', financial_year_id: '@financial_year_id'}, {loadBudgetClasses: {method: 'GET'}});
});
budgetingServices.factory('SubmissionFormSelectGroupService', function ($resource) {
    return $resource('/json/SubmissionFormSelectGroup', { }, {index: {method: 'GET'}});
});
budgetingServices.factory('SubmissionFormSelectOptionService', function ($resource) {
    return $resource('/json/SubmissionFormSelectOptions/:select_group', {select_group: '@select_group' }, {});
});
//end budget submission line items

budgetingServices.factory('DeleteInputService', function ($resource) {
    return $resource('/json/deleteInput/:inputId/:mtefSectionId', {inputId: '@inputId',mtefSectionId:'@mtefSectionId'}, {delete: {method: 'POST'}});
});

budgetingServices.factory('CreateAdminHierarchyCeilingService', function ($resource) {
    return $resource('/json/createAdminHierarchyCeiling', {}, {store: {method: 'POST'}});
});
budgetingServices.factory('UpdateAdminHierarchyCeilingService', function ($resource) {
    return $resource('/json/updateAdminHierarchyCeiling', {}, {update: {method: 'POST'}});
});
budgetingServices.factory('DeleteAdminHierarchyCeilingService', function ($resource) {
    return $resource('/json/deleteAdminHierarchyCeilings/:ceiling_id', {ceiling_id: '@ceiling_id'}, {});
});

budgetingServices.factory('UpdateLowerCeilingsService', function ($resource) {
    return $resource('/json/updateLowerCeilings/:adminHierarchyCeilingId', {adminHierarchyCeilingId:'@adminHierarchyCeilingId'}, {update: {method: 'POST'}});
});

budgetingServices.factory('UserSections', function ($resource) {
    return $resource('/json/getUserSections', {}, {});
});

budgetingServices.factory('GetFullPlanChainService', function ($resource) {
    return $resource('/json/budget-preview-trees/:adminHierarchyId/:sectionId/:planType', {adminHierarchyId: '@adminHierarchyId',sectionId: '@sectionId',planType:'@planType'}, {});
});

budgetingServices.factory('ForwardFinancialYearsService', function ($resource) {
    return $resource('/json/forwardFinancialYears/:financialYearId', {}, {financialYearId:'@financialYearId'});
});



//EXPENDITURE CENTRE VALUES
budgetingServices.factory('PaginatedExpenditureCentreValueService', function ($resource) {
    return $resource('/json/expenditure_centre_values/paginated', {}, {});
});

budgetingServices.factory('CreateExpenditureCentreValueService',function ($resource) {
    return $resource('/json/expenditure_centre_value/create',{},{store:{method:'POST'}});
});

budgetingServices.factory('UpdateExpenditureCentreValueService',function ($resource) {
    return $resource('/json/expenditure_centre_value/update',{},{update:{method:'POST'}});
});

budgetingServices.factory('DeleteExpenditureCentreValueService', function ($resource) {
    return $resource('/json/expenditure_centre_value/delete/:expenditure_centre_value_id', {expenditure_centre_value_id: '@expenditure_centre_value_id'}, {delete: {method: 'GET'}});
});

budgetingServices.factory('TrashedExpenditureCentreValueService', function ($resource) {
    return $resource('/json/expenditure_centre_values/trashed', {}, {});
});

budgetingServices.factory('RestoreExpenditureCentreValueService', function ($resource) {
    return $resource('/json/expenditure_centre_value/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

budgetingServices.factory('EmptyExpenditureCentreValueTrashService', function ($resource) {
    return $resource('/json/expenditure_centre_values/emptyTrash', {}, {});
});

budgetingServices.factory('PermanentDeleteExpenditureCentreValueService', function ($resource) {
    return $resource('/json/expenditure_centre_value/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});

budgetingServices.factory('ToggleExpenditureCentreValueService',function ($resource) {
    return $resource('/json/expenditure_centre_value/toggleActive',{},{toggleActive:{method:'POST'}});
});

//BUDGET DISTRIBUTION CONDITION FUND SOURCES
budgetingServices.factory('PaginatedBdcMainGroupFundSourceService', function ($resource) {
    return $resource('/json/main_group_fund_sources/paginated', {}, {});
});

budgetingServices.factory('CreateBdcMainGroupFundSourceService',function ($resource) {
    return $resource('/json/main_group_fund_source/create',{},{store:{method:'POST'}});
});

budgetingServices.factory('UpdateBdcMainGroupFundSourceService',function ($resource) {
    return $resource('/json/main_group_fund_source/update',{},{update:{method:'POST'}});
});

budgetingServices.factory('DeleteBdcMainGroupFundSourceService', function ($resource) {
    return $resource('/json/main_group_fund_source/delete/:main_group_fund_source_id', {main_group_fund_source_id: '@main_group_fund_source_id'}, {delete: {method: 'GET'}});
});

budgetingServices.factory('TrashedBdcMainGroupFundSourceService', function ($resource) {
    return $resource('/json/main_group_fund_sources/trashed', {}, {});
});

budgetingServices.factory('RestoreBdcMainGroupFundSourceService', function ($resource) {
    return $resource('/json/main_group_fund_source/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

budgetingServices.factory('EmptyBdcMainGroupFundSourceTrashService', function ($resource) {
    return $resource('/json/main_group_fund_sources/emptyTrash', {}, {});
});

budgetingServices.factory('PermanentDeleteBdcMainGroupFundSourceService', function ($resource) {
    return $resource('/json/main_group_fund_source/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});

budgetingServices.factory('ToggleBdcMainGroupFundSourceService',function ($resource) {
    return $resource('/json/main_group_fund_source/toggleActive',{},{toggleActive:{method:'POST'}});
});

//GROUP FINANCIAL YEAR VALUES
budgetingServices.factory('PaginatedGroupFinancialYearValueService', function ($resource) {
    return $resource('/json/group_financial_year_values/paginated', {}, {});
});

budgetingServices.factory('CreateGroupFinancialYearValueService',function ($resource) {
    return $resource('/json/group_financial_year_value/create',{},{store:{method:'POST'}});
});

budgetingServices.factory('UpdateGroupFinancialYearValueService',function ($resource) {
    return $resource('/json/group_financial_year_value/update',{},{update:{method:'POST'}});
});

budgetingServices.factory('DeleteGroupFinancialYearValueService', function ($resource) {
    return $resource('/json/group_financial_year_value/delete/:group_financial_year_value_id', {group_financial_year_value_id: '@group_financial_year_value_id'}, {delete: {method: 'GET'}});
});

budgetingServices.factory('TrashedGroupFinancialYearValueService', function ($resource) {
    return $resource('/json/group_financial_year_values/trashed', {}, {});
});

budgetingServices.factory('RestoreGroupFinancialYearValueService', function ($resource) {
    return $resource('/json/group_financial_year_value/restore/:id', {id: '@id'}, {restore: {method: 'GET'}});
});

budgetingServices.factory('EmptyGroupFinancialYearValueTrashService', function ($resource) {
    return $resource('/json/group_financial_year_values/emptyTrash', {}, {});
});

budgetingServices.factory('PermanentDeleteGroupFinancialYearValueService', function ($resource) {
    return $resource('/json/group_financial_year_value/permanentDelete/:id', {id: '@id'}, {permanentDelete: {method: 'GET'}});
});

budgetingServices.factory('ActivityFacilityFundSource', function ($resource) {
    return $resource('/json/activity-facility-fund-sources/:id', {id: '@id'}, {
        paginate: {
            method: 'GET',
            url : '/json/activity-facility-fund-sources/by-mtef-section/:mtefSectionId/by-budget-type/:budgetType',
            params : {mtefSectionId : '@mtefSectionId', budgetType : '@budgetType'}
        }});
});
budgetingServices.factory('ActivityFacilityFundSourceInput', function ($resource) {
    return $resource('/json/activity-facility-fund-source-inputs/:id', {id: '@id'}, {
        paginate: {
            method: 'GET',
            url : '/json/activity-facility-fund-source-inputs/:activityFacilityFundSourceId',
            params : {activityFacilityFundSourceId : '@activityFacilityFundSourceId'}
        },
        createOrUpdate :{
            method :'POST',
            url : '/json/activity-facility-fund-source-inputs/create-or-update'
        }});
});

budgetingServices.factory('UserScrutinizations', function ($resource) {
    return $resource('/json/getUserScrutinizations/:id', {id:'@id'}, {
        assignUser:{method:'POST',url:'/json/scrutinization/assignUser/:userId',params:{userId:'@userId'}},
        getBudget:{method:'GET',url:'/json/activitiesByMtefSectionService/:mtefSectionId',params:{mtefSectionId:'@mtefSectionId'}},
        saveItemComments:{method:'POST', url:'/json/scrutinization/saveItemComments'},
        updateStatus:{method:'GET', url:'/json/scrutinization/updateStatus/:scrutinId/:status',params:{scrutinId:'@scrutinId',status:'@status'}},
        getReturnDecisionLevels:{method:'GET', url:'/json/scrutinization/getReturnDecisionLevels/:scrutinId/:mtefSectionId',params:{scrutinId:'@scrutinId',mtefSectionId:'@mtefSectionId'}},
        returnMtefSection:{method:'POST', url:'/json/scrutinization/returnMtefSection'},
        getUserNextDecisionLevels:{method:'GET', url:'/json/scrutinization/getUserNextDecisionLevels'},
        forwardAllByUser:{method:'POST', url:'/json/scrutinization/forwardAllByUser'},
        setRecommendation:{method:'POST', url:'/json/scrutinization/setRecommendation/:scrutinId/:recommendation/:status',params:{recommendation:'@recommendation',scrutinId:'@scrutinId',status:'@status'}},
        getAllReturnDecisionLevels:{method:'GET', url:'/json/scrutinization/getAllReturnDecisionLevels'},
        returnAllByUser:{method:'POST', url:'/json/scrutinization/returnAllByUser'},
        budgetByCeiling:{method:'GET', url:'/json/dashBoard/getBudgetByCeiling/:adminHierarchyId?noLoader=true'},
        getP1Admin:{method:'GET', url:'/json/scrutinization/get-p1-admin',isArray:true},
        getAdmin:{method:'GET', url:'/json/scrutinization/get-admin/by-p1',isArray:true},
        getSectors:{method:'GET', url:'/json/scrutinization/get-sectors/by-admin',isArray:true},
        getScrutins:{method:'GET', url:'/json/scrutinization/get-scrutins/by-sector',isArray:true},
        getUserExpectedScrutins:{method:'GET', url:'/json/scrutinization/get-user-expected-scrutins',isArray:true},
        clearScrutin:{method:'DELETE',url:'/json/scrutinization/clear/:id',params:{id:'@id'}}
    });
});

budgetingServices.factory('Scrutiny', function ($resource) {
    return $resource('/json/scrutiny/:id', {id:'@id'}, {
        adminHierarchies:{method:'GET',
            url:'/json/scrutiny/get-distinct-submitted-hierarchies/:parentAdminHierarchyId/:parentSectionId',
            params:{parentAdminHierarchyId:'@parentAdminHierarchyId',parentSectionId:'@parentSectionId'}},
        sectors:{method:'GET',
            url:'/json/scrutiny/get-distinct-submitted-sectors/:parentAdminHierarchyId/:parentSectionId/:mtefId',
            params:{parentAdminHierarchyId:'@parentAdminHierarchyId',parentSectionId:'@parentSectionId',mtefId:'@mtefId'}},
        departments:{method:'GET',
            url:'/json/scrutiny/get-distinct-submitted-departments/:parentAdminHierarchyId/:parentSectionId/:mtefId/:sectorId',
            params:{parentAdminHierarchyId:'@parentAdminHierarchyId',parentSectionId:'@parentSectionId',mtefId:'@mtefId',sectorId:'@sectorId'}},
        costCentres:{method:'GET',
            url:'/json/scrutiny/get-distinct-submitted-cost-centres/:parentAdminHierarchyId/:parentSectionId/:mtefId/:sectorId',
            params:{parentAdminHierarchyId:'@parentAdminHierarchyId',parentSectionId:'@parentSectionId',mtefId:'@mtefId',sectorId:'@sectorId'}},
        moveByMtef:{method:'POST',
            url:'/json/scrutiny/move-by-mtef/:mtefId/:fromDecisionLevelId/:toDecisionLevelId',
            params:{mtefId:'@mtefId',fromDecisionLevelId:'@fromDecisionLevelId',toDecisionLevelId:'@toDecisionLevelId'}},
        moveBySector:{method:'POST',
            url:'/json/scrutiny/move-by-sector/:mtefId/:sectorId/:fromDecisionLevelId/:toDecisionLevelId',
            params:{mtefId:'@mtefId',sectorId : '@sectorId',fromDecisionLevelId:'@fromDecisionLevelId',toDecisionLevelId:'@toDecisionLevelId'}},
        moveByMtefSection:{method:'POST',
            url:'/json/scrutiny/move-by-mtef-section/:mtefSectionId/:fromDecisionLevelId/:toDecisionLevelId',
            params:{mtefSectionId:'@mtefSectionId',fromDecisionLevelId:'@fromDecisionLevelId',toDecisionLevelId:'@toDecisionLevelId'}},
        assignUserBySector:{method:'POST',
            url:'/json/scrutiny/assign-user/by-sector/:mtefId/:sectorId/:userId',
            params:{mtefId:'@mtefId',sectorId:'@sectorId',userId:'@userId'}},
        assignUserByCostCentre:{method:'POST',
            url:'/json/scrutiny/assign-user/by-cost-centre/:mtefSectionId/:userId',
            params:{mtefSectionId:'@mtefSectionId',userId:'@userId'}}
    });
});

budgetingServices.factory('BudgetService', function ($resource) {
    return $resource('/json/getCurrentFinancialYearPlans', {}, {
            currentFinancialYear:{method:'GET'},
            approveBudget:{method:'GET',url:'/json/approveCurrentFinancialYearPlan',params:{mtefId:'@mtefId',budget_type:'@budget_type'}},
            resetBudget:{method:'GET',url:'/json/resetCurrentFinancialYearPlan',params:{mtefId:'@mtefId', budget_type:'@budget_type'}},
            disapproveBudget:{method:'GET',url:'/json/disapproveCurrentFinancialYearPlan',params:{mtefId:'@mtefId',budget_type:'@budget_type'}},
            approveCeiling : {method:'GET', url:'/json/approve-ceiling',params:{financialYearId:'@financialYearId',adminHierarchyId:'adminHierarchyId', budget_type : '@budget_type'}},
            disapproveCeiling : {method:'GET', url:'/json/dis-approve-ceiling', params:{financialYearId:'@financialYearId',adminHierarchyId:'adminHierarchyId', budget_type : '@budget_type'}},
            getAggregateBudget:{method: 'GET', url : '/json/getAggregateBudget/:admin_hierarchy_id/:hierarchy_position/:financial_year_id/:budget_type'}
    });
});

budgetingServices.factory('BudgetDistributionConditions', function ($resource) {
    return $resource('/json/budgetDistribution/getByExpenditureCentres', {}, {});
});
