var budgeting = angular.module("budgeting-module", [
  "master-module",
  "ngRoute",
  "budgetingServices",
]);

budgeting.config([
  "$routeProvider",
  function ($routeProvider) {
    $routeProvider
      .when("/plans/:stage/:budgetType", {
        controller: "BudgetingController",
        templateUrl: "/pages/planning/planning.html",
        reloadOnSearch: false,
        rights: ["planning", "budgeting"],
      })
      .when("/budget-submission-forms", {
        controller: BudgetSubmissionFormController,
        templateUrl: "/pages/budgeting/budget_submission/form/index.html",
        resolve: BudgetSubmissionFormController.resolve,
        rights: ["settings.manage.budgeting.submission_form"],
      })
      .when("/ceilings", {
        controller: CeilingController,
        templateUrl: "/pages/budgeting/ceiling/index.html",
        resolve: CeilingController.resolve,
        rights: ["budgeting.manage.ceilings"],
      })
      .when("/admin-hierarchy-ceilings/budgetType/:budgetType", {
        controller: "AdminHierarchyCeilingController",
        templateUrl: "/pages/budgeting/admin-hierarchy-ceiling.html",
        reloadOnSearch: false,
        rights: ["budgeting.manage.ceilings"],
      })
      .when("/budgetSubmissionSubForms", {
        controller: BudgetSubmissionSubFormController,
        templateUrl: "/pages/budgeting/budget_submission/sub_form/index.html",
        resolve: BudgetSubmissionSubFormController.resolve,
        rights: ["budgeting"],
      })
      .when("/budget-submission-sub-forms", {
        controller: BudgetSubmissionSubFormController,
        templateUrl: "/pages/budgeting/budget_submission/sub_form/index.html",
        resolve: BudgetSubmissionSubFormController.resolve,
        rights: ["settings.manage.budgeting.submission_form"],
      })
      .when("/activity-facility-fund-sources/:mtefSectionId/:budgetType", {
        controller: "ActivityFacilityFundSourceController",
        templateUrl: "/pages/budgeting/activity-facility-fund-source.html",
        reloadOnSearch: false,
        rights: ["budgeting"],
      })
      .when(
        "/activity-facility-fund-source-inputs/:mtefSectionId/:budgetType/:activityFacilityFundSourceId",
        {
          controller: "ActivityFacilityFundSourceInputController",
          templateUrl: "/pages/budgeting/input.html",
          reloadOnSearch: false,
          rights: ["budgeting"],
        }
      )
      .when("/budgetSubmissionLines", {
        controller: BudgetSubmissionLineController,
        templateUrl: "/pages/budgeting/budget-submission-line.html",
        resolve: BudgetSubmissionLineController.resolve,
        rights: ["budgeting.manage.submission.form"],
      })
      .when("/projections", {
        controller: "ProjectionController",
        templateUrl: "/pages/planning/projection/index.html",
        reloadOnSearch: false,
        rights: ["budgeting"],
      })
      .when("/ceiling-sectors", {
        controller: CeilingSectorController,
        templateUrl: "/pages/budgeting/ceiling/sector/index.html",
        resolve: CeilingSectorController.resolve,
        rights: ["budgeting.manage.ceilings"],
      })
      .when("/scrutinization", {
        controller: ScrutinizationController,
        templateUrl: "/pages/budgeting/scrutinization/index.html",
        resolve: ScrutinizationController.resolve,
        reloadOnSearch: false,
        rights: ["budgeting.manage.scrutinization"],
      })
      .when("/scrutinization-budget", {
        controller: ScrutinizationBudgetController,
        templateUrl: "/pages/budgeting/scrutinization/budget.html",
        resolve: ScrutinizationBudgetController.resolve,
        reloadOnSearch: false,
        rights: ["budgeting.manage.scrutinization"],
      })
      .when("/budget-distribution-condition-main-group-fund-sources", {
        controller: BdcMainGroupFundSourceController,
        templateUrl: "/pages/budgeting/main_group_fund_source/index.html",
        resolve: BdcMainGroupFundSourceController.resolve,
        rights: ["settings.manage.budget_settings"],
      })
      .when("/current-financial-year-plans/:budgetType", {
        controller: CurrentFinancialYearPlansController,
        templateUrl:
          "/pages/budgeting/scrutinization/current-financial-year-plans.html",
        rights: ["budgeting.manage.financial_year_plans"],
      })
      .otherwise({
        redirectTo: "/",
      });
  },
]);
