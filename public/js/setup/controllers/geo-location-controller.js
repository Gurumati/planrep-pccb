function GeoLocationController($scope, UserGeoLocation, $uibModal, GeographicalLocationService, GlobalService,
  ConfirmDialogService, $timeout, Upload, LoadParentGeoLocationsService,
  DeleteGeoLocationService, AllGeolocationLevelsService, GeoLocationService) {

  $scope.userGeoLocation = UserGeoLocation;
  $scope.location = UserGeoLocation[0];

  $scope.title = "TITLE_GEO_LOACTION";

  AllGeolocationLevelsService.query({}, function (data) {
    $scope.geoLocationLevels = data.locations;
  });

  GlobalService.geoLocationLevels(function (data) {
    $scope.geoLocLevels = data.geoLocLevels;
  });


  $scope.showUploadForm = false;
  $scope.showButtons = true;
  $scope.showList = true;

  $scope.revealUploadForm = function () {
    $scope.showUploadForm = true;
    $scope.showButtons = false;
    $scope.showList = false;
  };

  $scope.closeUploadForm = function () {
    $scope.showUploadForm = false;
    $scope.showButtons = true;
    $scope.showList = true;
  };

  $scope.loadChildren = function (a) {
    if (a.children === undefined) {
      a.children = [];
    }
    a.expanded = true;
    GeoLocationService.getGeoLocationChildren({ id: a[0].id }, function (data) {
      a.children = data.geographicalLocations;
      console.log(data);
    });
  };

  $scope.loadMoreChildren = function (a) {
    if (a.children === undefined) {
      a.children = [];
    }
    a.expanded = true;
    GeoLocationService.getGeoLocationChildren({ id: a.id }, function (data) {
      a.children = data.geographicalLocations;
      console.log(data);
    });
  };
  $scope.setSelected = function (a, p) {
    $scope.selectedGeoLocation = a;
    $scope.selectedGeoLocationParent = p;
  };
  $scope.toggleCollapse = function (a) {
    a.expanded = false;
  };

  $scope.loadParentGeoLocations = function (geoLocationLevelId) {
    LoadParentGeoLocationsService.query({ childGeoLocationLevelId: geoLocationLevelId },
      function (data) {
        $scope.geoLocationParents = data;
      },
      function (error) {
      }
    );
  };

  $scope.import_geo_locations = function () {
    GeographicalLocationService.import({}, function (data) {
      $scope.geoLocations = data.geoLocations;
    }, function (error) {
      $scope.errorMessage = error.data.errorMessage;
    }
    );
  };

  $scope.geoLocationToCreate = {};

  $scope.uploadFile = function (file) {
    file.upload = Upload.upload({
      url: '/geoLocations/upload',
      method: 'POST',
      data: {
        geo_location_level_id: $scope.geoLocationToCreate.geo_location_level_id,
        file: file
      },
    });

    file.upload.then(function (response) {
      $timeout(function () {
        file.result = response.data;
        $scope.errorRows = response.data.errorRows;
        $scope.duplicates = response.data.duplicates;
        $scope.parentErrors = response.data.parentErrors;
        $scope.successMessage = response.data.successMessage;
        $scope.closeUploadForm();
        $scope.loadChildren($scope.userGeoLocation);
      });
    }, function (response) {
      if (response.status > 0) {
        $scope.errorMsg = response.status + ': ' + response.data;
        $scope.errorMessage = response.data.errorMessage;
      }
    }, function (evt) {
      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    });
  };

  $scope.create = function () {

    //Modal Form for creating financial year
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/geographical_location/create.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, CreateGeoLocationService, GeoLocationLevelsService, LoadParentGeoLocationsService) {

        GeoLocationLevelsService.get({}, function (data) {
          $scope.geoLocationLevels = data;
        });

        $scope.loadParentGeoLocations = function (geoLocationLevelId) {
          LoadParentGeoLocationsService.query({ childGeoLocationLevelId: geoLocationLevelId },
            function (data) {
              $scope.geoLocationParents = data;
            },
            function (error) {
            }
          );
        };
        $scope.geoLocationToCreate = {};
        //Function to store data and close modal when Create button clicked
        $scope.store = function () {
          if ($scope.createGeoLocationForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }

          CreateGeoLocationService.store($scope.geoLocationToCreate,
            function (data) {
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );

        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        }
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
      $scope.successMessage = data.successMessage;
      $scope.loadChildren($scope.userGeoLocation);

    },
      function () {
      });
  };

  $scope.edit = function (geoLocationToEdit) {
    console.log(geoLocationToEdit);
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/geographical_location/update.html',
      backdrop: false,
      controller: function ($scope, $q, $uibModalInstance, UpdateGeoLocationService, GeoLocationLevelsService, LoadParentGeoLocationsService) {

        GeoLocationLevelsService.get({}, function (levels) {
          LoadParentGeoLocationsService.get({ childGeoLocationLevelId: geoLocationToEdit.geo_location_level_id },
            function (data) {
              $scope.geoLocationParents = data.geoLocationParents;
              $scope.geoLocationLevels = levels.data;
              $scope.geoLocationToEdit = angular.copy(geoLocationToEdit);
            }
          );

        });

        $scope.loadParentGeoLocations = function (geoLocationLevelId) {
          LoadParentGeoLocationsService.get({ childGeoLocationLevelId: geoLocationLevelId },
            function (data) {
              $scope.geoLocationParents = data;
            },
            function (error) {
            }
          );
        };

        //Function to update data and close modal when update button clicked
        $scope.update = function () {
          UpdateGeoLocationService.update($scope.geoLocationToEdit,
            function (data) {
              //Successful function when
              $scope.geoLocationToEdit = undefined;
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );

        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        }
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
      $scope.successMessage = data.successMessage;
      $scope.loadChildren($scope.selectedGeoLocationParent);
    },
      function () {
      });
  };

  $scope.delete = function (id) {
    console.log(id);
    ConfirmDialogService.showConfirmDialog(
      'Confirm Delete!', 'Are sure you want to delete this location?')
      .then(function () {
        DeleteGeoLocationService.delete({ id: id },
          function (data) {
            $scope.successMessage = data.successMessage;
            $scope.loadChildren($scope.selectedGeoLocationParent);
          }, function (error) {
            $scope.errorMessage = error.data.errorMessage;
          }
        );
      },
        function () {
          clearTree();
        });
  };

 // Upload Start here
  $scope.$on('$viewContentLoaded', function () {
    var options = {
      beforeSubmit: $scope.validate,
      success: processResponse,
      error: failureHandler,
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    };
    $('#uploadGeoLocations').ajaxForm(options);
  });

  $scope.validate = function (formData) {
    $scope.$apply(function () {
      $scope.inProgress = true;
    });
    if ($scope.inProgress)
      $('#loader').show();
    return $scope.inProgress;
  };

  var failureHandler = function (response) {
    $('#loader').hide();
    $scope.$apply(function () {
      console.log(response);
      $scope.inProgress = false;
    });
  };

  function processResponse(response) {
    $('#loader').hide();
    console.log(response)
    if (response.successMessage) {
      console.log(response.successMessage);
    }
  }

}

GeoLocationController.resolve = {
  UserGeoLocation: function (GeoLocationService, $timeout, $q) {
    var deferred = $q.defer();
    $timeout(function () {
      GeoLocationService.get({}, function (data) {
        deferred.resolve(data.locations);
      });
    }, 100);
    return deferred.promise;
  }
};
