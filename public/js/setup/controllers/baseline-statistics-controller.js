function BaselineStatisticsContoller($scope,
                                     FinancialYearModel,
                                     $uibModal,FinancialYearService,
                                     UpdateBaselineStatisticService,
                                     ConfirmDialogService,CarryVersionService,
                                     DeleteBaselineStatisticsService,
                                     BaselineStatisticsService) {

  $scope.financialYears = FinancialYearModel;
    $scope.title = "BASELINE_STATISTICS";

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        BaselineStatisticsService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.baselineStatistics = data;
        });
    };
    $scope.loadVersions = function(financialYearId){
      FinancialYearService.versions({id: financialYearId, type: 'BASELINE_STATISTICS'}, function (response) {
        $scope.versions = response.data;
      });
    }
    $scope.loadBaselineData = function(versionId) {

    let x = {page: 1,perPage: 10,versionId: versionId,financialYearId: $scope.financialYear.id};

      BaselineStatisticsService.get(x, function (response) {
        $scope.baselineStatistics = response;
      });

    };
   $scope.create = function (financialYearId,versionId) {
        var modalInstance = $uibModal.open({
          templateUrl: '/pages/setup/baseline_statistics/create.html',
          backdrop: false,
          controller: function ($scope, $uibModalInstance, CreateBaselineStatisticsService) {
            $scope.baselineStatisticsToCreate = {};

            //Function to store data and close modal when Create button clicked
            $scope.store = function () {
              if ($scope.createBaselineStatisticsForm.$invalid) {
                $scope.formHasErrors = true;
                return;
              }
              $scope.baselineStatisticsToCreate.versionId=versionId;
              $scope.baselineStatisticsToCreate.financialYearId=financialYearId;

              CreateBaselineStatisticsService.store({perPage: $scope.perPage}, $scope.baselineStatisticsToCreate,
                function (data) {
                  $uibModalInstance.close(data);
                },
                function (error) {
                  $scope.errorMessage = error.data.errorMessage;
                  $scope.errors = error.data.errors;
                }
              );

            };
            //Function to close modal when cancel button clicked
            $scope.close = function () {
              $uibModalInstance.dismiss('cancel');
            };

          }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
            //Service to create new financial year
            $scope.successMessage = data.successMessage;
            $scope.baselineStatistics = data.baselineStatistics;
            $scope.loadBaselineData(versionId);
          },
          function () {
            //If modal is closed
            console.log('Modal dismissed at: ' + new Date());
          });

      };
  $scope.carry = function (currentFinancialYearId, item) {
    CarryVersionService.showDialog('BLD',currentFinancialYearId, item).then(function(data){
      console.log(data);
    }, function(){

    });
  };
      $scope.edit = function (baselineStatisticsToEdit, currentPage, perPage) {

        var modalInstance = $uibModal.open({
          templateUrl: '/pages/setup/baseline_statistics/edit.html',
          backdrop: false,
          controller: function ($scope, $uibModalInstance) {
            $scope.baselineStatisticsToEdit = angular.copy(baselineStatisticsToEdit);

            //Function to store data and close modal when Create button clicked
            $scope.update = function () {
              $uibModalInstance.close($scope.baselineStatisticsToEdit);
            };
            //Function to close modal when cancel button clicked
            $scope.close = function () {
              $uibModalInstance.dismiss('cancel');
            };
          }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (baselineStatisticsToEdit) {
            //Service to create new unit
            UpdateBaselineStatisticService.update({page: currentPage, perPage: perPage}, baselineStatisticsToEdit,
              function (data) {
                //Successful function when
                $scope.successMessage = data.successMessage;
                $scope.baselineStatistics = data.baselineStatistics; //After save return all units and update $scope.units
              },
              function (error) {
                $scope.errorMessage = error.data.errorMessage;
                $scope.errors = error.data.errors;
              }
            );
          },
          function () {
            //If modal is closed
            console.log('Modal dismissed at: ' + new Date());
          });
      };

      $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Deletion!', 'Are sure you want to delete this record?').then(function () {
            console.log("YES");
            DeleteBaselineStatisticsService.delete({
                baselineStatisticId: id,
                currentPage: currentPage,
                perPage: perPage
              },
              function (data) {
                $scope.successMessage = data.successMessage;
                $scope.units = data.units;
              }, function (error) {
                $scope.errorMessage = error.data.errorMessage;
                $scope.errors = error.data.errors;
              }
            );
          },
          function () {
            console.log("NO");
          });
      };



}

BaselineStatisticsContoller.resolve = {
  FinancialYearModel: function (AllFinancialYearService, $q) {
    let deferred = $q.defer();
    AllFinancialYearService.query(function (data) {
      deferred.resolve(data);
    });
    return deferred.promise;
  }
};
