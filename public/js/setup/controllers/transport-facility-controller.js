function TransportFacilityController($scope, DataModel, $uibModal,
                                 ConfirmDialogService, TransportFacilityService) {

    $scope.items = DataModel;
    $scope.title = "TRANSPORT_SERVICE_PROVIDER";

    $scope.currentPage = 1;

    $scope.dateFormat = 'yyyy-MM-dd';

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        TransportFacilityService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/planning/transport_facility/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, TransportFacilityService,GlobalService) {

                $scope.createDataModel = {};
                $scope.ownershipType = [
                  {id:1,name:'Public'},
                  {id:2,name:'Private'}
                ];
                $scope.insuranceTypes = [
                  {id:1,name:'A. Insured comprehensive'},
                  {id:2,name:'B. insured 3rd party'},
                  {id:3,name:'C. Not insured'},
                  {id:4,name:'D. Not Applicable'}
                ];
                GlobalService.transportFacilityTypes(function (data) {
                    $scope.transportFacilityTypes = data.transportFacilityTypes;
                });

                GlobalService.assetUses(function (data) {
                    $scope.assetUses = data.assetUses;
                });

                GlobalService.assetConditions(function (data) {
                    $scope.assetConditions = data.assetConditions;
                });

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }

                    TransportFacilityService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        //console.log(updateDataModel);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/planning/transport_facility/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,GlobalService, TransportFacilityService) {
              $scope.ownershipType = [
                {id:1,name:'Public'},
                {id:2,name:'Private'}
              ];
              $scope.insuranceTypes = [
                {id:1,name:'A. Insured comprehensive'},
                {id:2,name:'B. insured 3rd party'},
                {id:3,name:'C. Not insured'},
                {id:4,name:'D. Not Applicable'}
              ];
                GlobalService.transportFacilityTypes(function (data) {
                    $scope.transportFacilityTypes = data.transportFacilityTypes;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                    $scope.updateDataModel.date_of_acquisition = new Date($scope.updateDataModel.date_of_acquisition);
                });

                GlobalService.assetUses(function (data) {
                    $scope.assetUses = data.assetUses;
                });

                GlobalService.assetConditions(function (data) {
                    $scope.assetConditions = data.assetConditions;
                });

                $scope.update = function () {
                    TransportFacilityService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = $scope.items.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                TransportFacilityService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.items = data.items;
                        $scope.currentPage = $scope.items.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/planning/transport_facility/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance, TransportFacilityService) {
                TransportFacilityService.trashed(function (data) {
                    $scope.trashedItems = data.trashedItems;
                });
                $scope.restoreItem = function (id) {
                    TransportFacilityService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    TransportFacilityService.permanentDelete({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    TransportFacilityService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

TransportFacilityController.resolve = {
    DataModel: function (TransportFacilityService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            TransportFacilityService.paginated({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        }, 900);
        return deferred.promise;
    }
};
