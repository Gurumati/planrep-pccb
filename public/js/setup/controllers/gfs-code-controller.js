function GfsCodeController($scope, GfsCodeModel, $uibModal, ConfirmDialogService, DeleteGfsCodeService, DeleteIfrsGfsCodeService,
                           ActivateIsProcurementService, ActivateGfsCodeService, UpdateGfsCodeService,
                           PaginateGfsCodeService, AllFundSourceService, LoadGfsCodeSubCategoryService, ProcurementTypeService,
                           $timeout, SearchGfsCodeService, PaginateGfsCodeMappingService, Upload, LoadAccountTypeService, loadGfsCodeCategoriesService) {

  $scope.gfsCodes = GfsCodeModel;
  $scope.title = "TITLE_GFS_CODES";
  $scope.ifrstitle = "IFRS_GFS_CODES";

  $scope.currentPage = 1;
  $scope.maxSize = 3;
  $scope.perPage = 10;

  $scope.pageChanged = function () {
    PaginateGfsCodeService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
      $scope.gfsCodes = data;
    });
  };

  // $scope.ifrspageChanged = function () {
  //   PaginateifrsGfsCodeService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
  //     $scope.gfsCodes = data;
  //   });
  // };

  $scope.showButtons = true;
  $scope.showUploadForm = false;

  $scope.toggleUploadForm = function (state) {
    $scope.showButtons = state !== true;
    $scope.showUploadForm = state;
  };

  $scope.search = function (searchText) {
    SearchGfsCodeService.get({searchText: searchText}, function (data) {
      $scope.gfsCodes = data;
    }, function (error) {
      console.log(error);
    });
  };

  LoadAccountTypeService.query({}, function (data) {
      $scope.accountTypes = data;
    },
    function (error) {
      console.log(error);
    });

  //load mapped gfs code categories
  $scope.ifrspageChange = function () {
    PaginateGfsCodeMappingService.getMappedGFS({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
      $scope.mappedgfscodes = data.data;
    });
  };

  var init = function () {
    $scope.ifrspageChange();
  };
  init();


  //  $scope.loadmappedGfsCode.query({},function(){
  //
  // });
  //load gfs code categories
  loadGfsCodeCategoriesService.query({},
    function (data) {
      $scope.gfsCategories = data;
    },
    function (error) {
      console.log(error);
    }
  );

  $scope.loadSubCats = function (id) {
    LoadGfsCodeSubCategoryService.get({id: id}, function (data) {
        $scope.gfsCodeSubCategories = data.data;
      }, function (error) {
        $scope.errorMessage = error.data.errorMessage;
      }
    );
  };

  AllFundSourceService.query({},
    function (data) {
      $scope.fundSources = data;
    },
    function (error) {
      console.log(error);
    }
  );

  $scope.uploadFile = function (file) {
    file.upload = Upload.upload({
      url: '/gfs-codes/upload',
      method: 'POST',
      data: {
        account_type_id: $scope.account_type_id,
        gfs_code_sub_category_id: $scope.gfs_code_sub_category_id,
        fund_source_id: $scope.fund_source_id,
        file: file
      },
    });

    file.upload.then(function (response) {
      $timeout(function () {
        file.result = response.data;
        $scope.gfsCodes = response.data.gfsCodes;
        $scope.toggleUploadForm(false);
        $scope.successMessage = response.data.successMessage;
      });
    }, function (response) {
      if (response.status > 0) {
        $scope.errorMsg = response.status + ': ' + response.data;
        $scope.errorMessage = response.data.errorMessage;
        $scope.errorData = response.data.errorData;
        $scope.errorRows = response.data.errorRows;
      }
    }, function (evt) {
      // Math.min is to fix IE which reports 200% sometimes
      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    });
  };
  $scope.createmapping = function () {
    let modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/gfs_code/create-mapping.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, AllFundSourceService, LoadGfsCodeSubCategoryService, loadGfsCodeCategoriesService, CreateGfsCode, PaginateGfsCodeMappingService) {
        $scope.gfsCodeToCreate = {};
        PaginateGfsCodeService.get({page: 1, perPage: 5000}, function (data) {
          $scope.gfscodes = data;
        });

        //load gfs code categories
        loadGfsCodeCategoriesService.query({},
          function (data) {
            $scope.gfsCodeCategories = data;
          },
          function (error) {
            console.log(error);
          }
        );

        $scope.loadSubs = function (id) {
          LoadGfsCodeSubCategoryService.get({id: id}, function (response) {
              $scope.gfsCodeSubCategories = response.data;
            }, function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        //Function to store data and close modal when Create button clicked
        $scope.store = function () {
          if ($scope.createGfsCodeForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          PaginateGfsCodeMappingService.store({perPage: $scope.perPage}, $scope.gfsCodeToCreate,
            function (data) {
              $scope.successMessage = data.successMessage;
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );

        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
  };
  $scope.editMapping = function (gfsCodeToEdit) {
    let modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/gfs_code/edit-mapping.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, AllFundSourceService, LoadGfsCodeSubCategoryService, loadGfsCodeCategoriesService, CreateGfsCode, PaginateGfsCodeMappingService, UpdateifrsGfsCodeService) {
        $scope.gfsCodeToEdit = {};
        PaginateGfsCodeService.get({page: 1, perPage: 5000}, function (data) {
          $scope.gfscodes = data;
        });

        //load gfs code categories
        loadGfsCodeCategoriesService.query({},
          function (data) {
            $scope.gfsCodeCategories = data;
          },
          function (error) {
            console.log(error);
          }
        );

        $scope.loadSubs = function (id) {
          LoadGfsCodeSubCategoryService.get({id: id}, function (response) {
              $scope.gfsCodeSubCategories = response.data;
            }, function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        //Function to store data and close modal when Create button clicked
        $scope.store = function () {
          if ($scope.createGfsCodeForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          PaginateGfsCodeMappingService.store({perPage: $scope.perPage}, $scope.gfsCodeToCreate,
            function (data) {
              $scope.successMessage = data.successMessage;
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        //Function to store data and close modal when Create button clicked
        $scope.update = function () {
          $uibModalInstance.close($scope.gfsCodeToEdit);
        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (gfsCodeToEdit) {

        //Service to create new financial year
        UpdateifrsGfsCodeService.update({page: currentPage, perPage: perPage}, gfsCodeToEdit,
          function (data) {
            //Successful function when

            $scope.successMessage = data.successMessage;
            $scope.gfsCodes = data.gfsCodes;
            $scope.currentPage = $scope.gfsCodes.current_page;
          },
          function (error) {
            console.log(error);
          }
        );
      },
      function () {
        console.log('Modal dismissed at: ' + new Date());
      });
  };
  $scope.deletemapping = function (id, currentPage, perPage) {
    ConfirmDialogService.showConfirmDialog(
      'TITLE_CONFIRM_DELETE_GFS_CODE',
      'CONFIRM_DELETE')
      .then(function () {
          DeleteIfrsGfsCodeService.delete({gfsCodeId: id, currentPage: currentPage, perPage: perPage},
            function (data) {
              $scope.successMessage = data.successMessage;
              $scope.gfsCodes = data.gfsCodes;
            }, function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        },
        function () {
        });
  };
  $scope.create = function () {
    let modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/gfs_code/create.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, AllFundSourceService, LoadGfsCodeSubCategoryService, LoadAccountTypeService, loadGfsCodeCategoriesService, CreateGfsCode, ProcurementTypeService) {
        $scope.gfsCodeToCreate = {};
        $scope.gfsCodeToCreate.is_procurement = false;
        //load account types
        LoadAccountTypeService.query({}, function (data) {
            $scope.accountTypes = data;
          },
          function (error) {
            console.log(error);
          });

        //load gfs code categories
        loadGfsCodeCategoriesService.query({},
          function (data) {
            $scope.gfsCodeCategories = data;
          },
          function (error) {
            console.log(error);
          }
        );
        ProcurementTypeService.query({},
          function (data) {
            $scope.procurement_types = data;
          },
          function (error) {
            console.log(error);
          }
        );

        $scope.loadSubs = function (id) {
          LoadGfsCodeSubCategoryService.get({id: id}, function (response) {
              $scope.gfsCodeSubCategories = response.data;
            }, function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        //Function to store data and close modal when Create button clicked
        $scope.store = function () {
          if ($scope.createGfsCodeForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          CreateGfsCode.store({perPage: $scope.perPage}, $scope.gfsCodeToCreate,
            function (data) {
              // console.log($scope.gfsCodeToCreate); // Govella
              $scope.successMessage = data.successMessage;
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );

        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when the modal is closed by the cancel button or to store data
    modalInstance.result.then(function (data) {
        //Service to create new gfs code
        $scope.successMessage = data.successMessage;
        $scope.gfsCodes = data.gfsCodes;
      },
      function () {
        //If modal is closed
        console.log('Modal dismissed at: ' + new Date());
      });

  };

  $scope.edit = function (gfsCodeToEdit, currentPage, perPage) {
    // $scope.loadProcurementType(gfsCodeToEdit.procurement_type_id);
    let modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/gfs_code/edit.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, AllFundSourceService, LoadGfsCodeSubCategoryService, LoadAccountTypeService, loadGfsCodeCategoriesService, ProcurementTypeService) {
        // load procurement types
        ProcurementTypeService.query({},
          function (data) {
            $scope.procurement_types = data;
          },
          function (error) {
            console.log(error);
          }
        );

        //load account types
        LoadAccountTypeService.query({}, function (data) {
            $scope.accountTypes = data;
            $scope.gfsCodeToEdit = angular.copy(gfsCodeToEdit);
          },
          function (error) {
            console.log(error);
          });


        //load gfs code categories
        //load gfs code categories
        loadGfsCodeCategoriesService.query({},
          function (data) {
            $scope.gfsCodeCategories = data;
          },
          function (error) {
            console.log(error);
          }
        );

        $scope.loadSubs = function (id) {
          LoadGfsCodeSubCategoryService.get({id: id}, function (response) {
              $scope.gfsCodeSubCategories = response.data;
            }, function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };

        //Function to store data and close modal when Create button clicked
        $scope.update = function () {
          $uibModalInstance.close($scope.gfsCodeToEdit);
        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (gfsCodeToEdit) {
        //Service to create new financial year
        UpdateGfsCodeService.update({page: currentPage, perPage: perPage}, gfsCodeToEdit,
          function (data) {
            // this is where the actual update takes place.
            //Successful function when
            $scope.successMessage = data.successMessage;
            $scope.gfsCodes = data.gfsCodes;
            $scope.currentPage = $scope.gfsCodes.current_page;
          },
          function (error) {
            console.log(error);
          }
        );
      },
      function () {
        console.log('Modal dismissed at: ' + new Date());
      });
  };

  // $scope.loadProcurementType = function(id) {
  //   loadProcurementTypeService.get({id: id}, function(response) {
  //     console.log(response.data);
  //   }, function (error) {
  //     console.log(error);
  //   });
  // };

  $scope.costCentres = function (gfsCode) {
    let modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/gfs_code/cost-centres.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, GlobalService) {
        GlobalService.gfsCodeCostCentres({gfs_code_id: gfsCode.id}, function (data) {
          $scope.items = data.items;
        });

        $scope.gfs_code_name = gfsCode.name + '- ' + gfsCode.code;

        GlobalService.sectors(function (data) {
          $scope.sectors = data.sectors;
        });

        $scope.loadDepartments = function (sector_id) {
          GlobalService.sectorDepartments({sector_id: sector_id}, function (data) {
            $scope.departments = data.departments;
          });
        };

        $scope.loadCostCentres = function (department_id) {
          GlobalService.departmentCostCentres({department_id: department_id}, function (data) {
            $scope.sections = data.sections;
          });
        };

        $scope.formData = {};

        $scope.addCostCentres = function () {
          GlobalService.addGfsCodeCostCentre({gfs_code_id: gfsCode.id}, $scope.formData, function (data) {
            $scope.items = data.items;
            $scope.successMessage = data.successMessage;
          });
        };

        $scope.removeCostCentre = function (id) {
          GlobalService.removeGfsCodeCostCentre({gfs_code_id: gfsCode.id, id: id}, function (data) {
            $scope.items = data.items;
            $scope.successMessage = data.successMessage;
          });
        };

        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
  };

  $scope.delete = function (id, currentPage, perPage) {
    ConfirmDialogService.showConfirmDialog(
      'TITLE_CONFIRM_DELETE_GFS_CODE',
      'CONFIRM_DELETE')
      .then(function () {
          DeleteGfsCodeService.delete({gfsCodeId: id, currentPage: currentPage, perPage: perPage},
            function (data) {
              $scope.successMessage = data.successMessage;
              $scope.gfsCodes = data.gfsCodes;
            }, function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        },
        function () {
        });
  };

  $scope.activateGfsCode = function (gfsCodeId, gfsCodeStatus, perPage) {
    $scope.gfsCodeToActivate = {};
    $scope.gfsCodeToActivate.id = gfsCodeId;
    $scope.gfsCodeToActivate.is_active = gfsCodeStatus;
    ActivateGfsCodeService.activateGfsCode({page: $scope.currentPage, perPage: perPage}, $scope.gfsCodeToActivate,
      function (data) {
        $scope.successMessage = false;
        $scope.errorMessage = false;
        $scope.activateMessageProcurement = false;
        $scope.activateMessage = data.activateMessage;
      });
  }

  $scope.activateIsProcurement = function (gfsCodeId, gfsCodeStatus, perPage) {
    $scope.gfsCodeToActivate = {};
    $scope.gfsCodeToActivate.id = gfsCodeId;
    $scope.gfsCodeToActivate.is_procurement = gfsCodeStatus;
    ActivateIsProcurementService.activateIsProcurement({
        page: $scope.currentPage,
        perPage: perPage
      }, $scope.gfsCodeToActivate,
      function (data) {
        $scope.successMessage = false;
        $scope.errorMessage = false;
        $scope.activateMessage = false;
        $scope.activateMessageProcurement = data.activateMessageProcurement;
      });
  };
}

GfsCodeController.resolve = {
  GfsCodeModel: function (PaginateGfsCodeService, $timeout, $q) {
    var deferred = $q.defer();
    $timeout(function () {
      PaginateGfsCodeService.get({page: 1, perPage: 10}, function (data) {
        deferred.resolve(data);
      });
    }, 900);
    return deferred.promise;
  }
};
