function MoveCeilingController($scope, MoveCeiling, GlobalService, SectorsService, FinancialYear, ConfirmDialogService) {
  $scope.title = "Move Ceiling";

  FinancialYear.get({}, function (data) {
    $scope.financialYears = data.financialYears;
  });

  //Admin hiearchies
  GlobalService.onlyCouncils({}, function (data) {
    console.log(data);
    $scope.adminHierarchies = data.councils;
  });
  //Load sectors
  SectorsService.query({}, function (data) {
    $scope.sectors = data;
  });

  //Load ceiling by fy,adm, bty,sect
  $scope.loadCeiling = function () {
    MoveCeiling.getCeiling({
      financialYearId: $scope.financialYearId,
      adminHierarchyId: $scope.adminHierarchyId,
      budgetType: $scope.budgetType,
      sectorId: $scope.sectorId
    }, function (data) {
      $scope.ceilings = data.ceilings;
      $scope.newCeilings = data.newCeilings;
    });
  };

  $scope.loadAdminCeiling = function () {
    $scope.newCeilingId = undefined;
    MoveCeiling.getAdminCeiling({
      financialYearId: $scope.financialYearId,
      adminHierarchyId: $scope.adminHierarchyId,
      budgetType: $scope.budgetType,
      sectorId: $scope.sectorId,
      ceilingId: $scope.ceilingId
    }, function (data) {
      $scope.adminHierarchyCeilings = data.adminCeilings;
    });
  };

  $scope.moveCeiling = function (newCeilingId) {
    if (newCeilingId === undefined) {
      return;
    }
    ConfirmDialogService.showConfirmDialog(
      'Confirm Move ceiling',
      'Are you sure you want to move the ceiling')
      .then(function () {
          MoveCeiling.moveAdminCeiling({
            financialYearId: $scope.financialYearId,
            adminHierarchyId: $scope.adminHierarchyId,
            budgetType: $scope.budgetType,
            sectorId: $scope.sectorId,
            ceilingId: $scope.ceilingId,
            newCeilingId: newCeilingId
          }, function (data) {
            $scope.successMessage = data.successMessage;
            $scope.loadAdminCeiling();
            $scope.loadCeiling();
          });
        },
        function () {

        });

  };
}


