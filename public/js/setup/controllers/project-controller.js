function ProjectController($scope, FinancialYearModel, AllFinancialYearService, FinancialYearService, $uibModal, ProjectService, ConfirmDialogService, Upload, $timeout, AllSectorService, AllParentBudgetClassService,
  DeleteProjectService, AllFundSourceService, PaginatedProjectService, LoadSubBudgetClassService) {

  $scope.financialYears = FinancialYearModel;
  $scope.title = "TITLE_PROJECT";

  $scope.loadVersions = function (financialYearId) {
    $scope.financialYearId = financialYearId;
    FinancialYearService.versions({ id: financialYearId, type: 'PROJECT' }, function (response) {
      $scope.versions = response.data;

      $scope.loadProjects = function (versionId) {
        $scope.currentPage = 1;
        $scope.perPage = 10;
        $scope.maxSize = 3;

        PaginatedProjectService.get({
          page: 1,
          perPage: 10,
          financialYearId: financialYearId,
          versionId: versionId,
        }, function (data) {
          $scope.projects = data.original.projects;
        });

        $scope.pageChanged = function () {
          PaginatedProjectService.get({
            page: $scope.currentPage,
            perPage: $scope.perPage,
            versionId: versionId,
            financialYearId: financialYearId,
          }, function (data) {
            $scope.projects = data.original.projects;
          });
        };

        $scope.search = function (financialYearId, versionId, searchText) {
          ProjectService.search({
            searchText: searchText,
            financialYearId: financialYearId,
            versionId: versionId
          }, function (data) {
            $scope.projects = data.projects.data;
          });
        };

        /*AllFundSourceService.query(function (data) {
            $scope.fundSources = data;
        });*/

        AllSectorService.query(function (data) {
          $scope.sectors = data;
        });

        AllParentBudgetClassService.query(function (data) {
          $scope.budgetClasses = data;
        });

        $scope.showList = true;
        $scope.showUploadForm = false;

        $scope.toggleUploadForm = function (state) {
          if (state === true) {
            $scope.showList = false;
          } else {
            $scope.showList = true;
          }
          $scope.showUploadForm = state;
        };

        $scope.loadSubBudgetClasses = function (id) {
          LoadSubBudgetClassService.get({ id: id }, function (data) {
            $scope.subBudgetClasses = data.subBudgetClasses;
          }, function (error) {
            $scope.errorMessage = error.data.errorMessage;
          }
          );
        };

        $scope.uploadFile = function (financialYearId, versionId, file) {
          file.upload = Upload.upload({
            url: '/files/projects/upload',
            method: 'POST',
            data: {
              file: file,
              versionId: versionId,
              financialYearId: financialYearId
            },
          });

          file.upload.then(function (response) {
            $timeout(function () {
              file.result = response.data;
              $scope.projects = response.data.projects.original.projects;
              $scope.toggleUploadForm(false);
              $scope.successMessage = response.data.successMessage;
              $scope.errors = response.data.errors;
            });
          }, function (response) {
            if (response.status > 0) {
              $scope.errorMsg = response.status + ': ' + response.data;
              $scope.errorMessage = response.data.errorMessage;
              $scope.successMessage = response.data.successMessage;
              $scope.projects = response.data.projects.original.projects;
              $scope.errors = response.data.errors;
            }
          }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
          });
        };

        $scope.create = function (financialYearId, versionId) {
          let modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/project/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllSectorService, CreateProjectService, AllFundSourceService, AllParentBudgetClassService, LoadSubBudgetClassService) {

              AllFundSourceService.query(function (data) {
                $scope.fundSources = data;
              });

              AllSectorService.query(function (data) {
                $scope.sectors = data;
              });

              AllParentBudgetClassService.query(function (data) {
                $scope.budgetClasses = data;
              });

              $scope.loadSubBudgetClasses = function (id) {
                LoadSubBudgetClassService.get({ id: id }, function (data) {
                  $scope.subBudgetClasses = data.subBudgetClasses;
                }, function (error) {
                  $scope.errorMessage = error.data.errorMessage;
                }
                );
              };

              $scope.projectToCreate = {};
              //Function to store data and close modal when Create button clicked
              $scope.store = function () {
                if ($scope.createProjectForm.$invalid) {
                  $scope.formHasErrors = true;
                  return;
                }
                if ($scope.projectToCreate.sub_budget_class_id === undefined) {
                  $scope.projectToCreate.sub_budget_class_id = $scope.projectToCreate.budget_class_id;
                }
                CreateProjectService.store({
                  perPage: $scope.perPage,
                  financialYearId: financialYearId,
                  versionId: versionId
                }, $scope.projectToCreate,
                  function (data) {
                    $uibModalInstance.close(data);
                  },
                  function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.errors = error.data.errors;
                  }
                );

              };
              //Function to close modal when cancel button clicked
              $scope.close = function () {
                $uibModalInstance.dismiss('cancel');
              };
            }
          });
          //Called when modal is close by cancel or to store data
          modalInstance.result.then(function (data) {
            $scope.successMessage = data.successMessage;
            $scope.projects = data.projects.original.projects;
            $scope.currentPage = data.current_page;
          },
            function () {
              //If modal is closed
              console.log('Modal dismissed at: ' + new Date());
            });

        };

        $scope.edit = function (financialYearId, versionId, projectToEdit, currentPage, perPage) {
          var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/project/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateProjectService, AllParentBudgetClassService, LoadSubBudgetClassService, AllFundSourceService) {
              //Calling service for section
              AllFundSourceService.query(function (data) {
                $scope.fundSources = data;
                $scope.projectToEdit = angular.copy(projectToEdit);
              });

              AllParentBudgetClassService.query(function (data) {
                $scope.budgetClasses = data;
              });

              $scope.loadSubBudgetClasses = function (id) {
                LoadSubBudgetClassService.get({ id: id }, function (data) {
                  $scope.subBudgetClasses = data.subBudgetClasses;
                }, function (error) {
                  $scope.errorMessage = error.data.errorMessage;
                }
                );
              };

              //Function to store data and close modal when Create button clicked
              $scope.update = function () {
                if ($scope.updateProjectForm.$invalid) {
                  $scope.formHasErrors = true;
                  return;
                }
                if ($scope.projectToEdit.sub_budget_class_id === undefined) {
                  $scope.projectToEdit.sub_budget_class_id = $scope.projectToEdit.budget_class_id
                }
                UpdateProjectService.update({
                  page: currentPage,
                  perPage: perPage,
                  financialYearId: financialYearId,
                  versionId: versionId
                }, $scope.projectToEdit,
                  function (data) {
                    //Successful function when
                    $uibModalInstance.close(data);
                  },
                  function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.errors = error.data.errors;
                  }
                );

              };
              //Function to close modal when cancel button clicked
              $scope.close = function () {
                $uibModalInstance.dismiss('cancel');
              };
            }
          });
          //Called when modal is close by cancel or to store data
          modalInstance.result.then(function (data) {
            $scope.successMessage = data.successMessage;
            $scope.projects = data.projects.original.projects;
            $scope.currentPage = $scope.projects.current_page;
          },
            function () {
              //If modal is closed
              console.log('Modal dismissed at: ' + new Date());
            });
        };

        $scope.project_sectors = function (project) {
          var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/project/sectors.html',
            backdrop: false,
            controller: function ($scope, $q, $uibModalInstance, AddSectorProjectService, DeleteSectorProjectService, LoadSectorProjectService, AllSectorService) {

              $scope.sectorProjectToCreate = {};
              $scope.project_id = project.id;
              $scope.name = project.name;
              $scope.code = project.code;

              LoadSectorProjectService.get({ project_id: project.id }, function (data) {
                $scope.sector_projects = data.sector_projects;
              }, function (error) {
                console.log(error);
              }
              );

              AllSectorService.query({}, function (data) {
                $scope.sectors = data;
              },
                function (error) {
                  console.log(error);
                });

              $scope.add_sector = function () {
                var sectorProjectToSave = {
                  "sector_id": $scope.sectorProjectToCreate.sector_id,
                  "project_id": $scope.project_id,
                };
                AddSectorProjectService.add_sector(sectorProjectToSave, function (data) {
                  $scope.successMessage = data.successMessage;
                  $scope.sector_projects = data.sector_projects;
                  console.log(data.project_id)
                },
                  function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.sector_projects = data.sector_projects;
                  }
                );
              };

              $scope.deleteSector = function (sector_project_id, project_id) {
                console.log('called');
                DeleteSectorProjectService.delete({
                  sector_project_id: sector_project_id, project_id: project_id
                }, function (data) {
                  $scope.successMessage = data.successMessage;
                  $scope.sector_projects = data.sector_projects;
                }, function (error) {

                }
                );
              };

              //Function to close modal when cancel button clicked
              $scope.close = function () {
                $uibModalInstance.dismiss('cancel');
              };
            }
          });
          //Called when modal is close by cancel or to store data
          modalInstance.result.then(function (data) {
            $scope.successMessage = data.successMessage;
          },
            function () {
              //If modal is closed
              console.log('Modal dismissed at: ' + new Date());
            });
        };

        $scope.projectFundSources = function (project) {
          var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/project/fund-sources.html',
            backdrop: false,
            controller: function ($scope, $q, $uibModalInstance, AddSectorProjectService, DeleteSectorProjectService, AllFundSourceService) {

              $scope.formData = {};
              $scope.project_id = project.id;
              $scope.name = project.name;
              $scope.code = project.code;
              $scope.items = project.fund_sources;


              ProjectService.fundSources({ project_id: project.id }, function (data) {
                $scope.items = data.items;
              },
                function (error) {
                  console.log(error)
                }
              );

              AllFundSourceService.query(function (data) {
                $scope.fundSources = data;
              });


              $scope.addFundSource = function () {
                ProjectService.addFundSources({ project_id: project.id }, $scope.formData, function (data) {
                  $scope.successMessage = data.successMessage;
                  $scope.items = data.items;
                },
                  function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.items = data.items;
                  }
                );
              };

              $scope.deleteFundSource = function (id) {
                ProjectService.removeFundSource({
                  id: id, project_id: project.id
                }, function (data) {
                  $scope.successMessage = data.successMessage;
                  $scope.items = data.items;
                }, function (error) {
                  console.log(error);
                }
                );
              };

              //Function to close modal when cancel button clicked
              $scope.close = function () {
                $uibModalInstance.dismiss('cancel');
              }
            }
          });
          //Called when modal is close by cancel or to store data
          modalInstance.result.then(function (data) {
            $scope.successMessage = data.successMessage;
          },
            function () {
              //If modal is closed
              console.log('Modal dismissed at: ' + new Date());
            });
        };

        $scope.delete = function (id, financialYearId, versionId, currentPage, perPage) {
          ConfirmDialogService.showConfirmDialog('TITLE_CONFIRM_DELETE_PROJECT', 'CONFIRM_DELETE').then(function () {
            console.log("YES");
            DeleteProjectService.delete({
              projectId: id,
              currentPage: currentPage,
              perPage: perPage,
              financialYearId: financialYearId,
              versionId: versionId
            },
              function (data) {
                $scope.successMessage = data.successMessage;
            //data.projects.original.projects
                $scope.projects = data.projects.original.projects;
                $scope.currentPage = $scope.projects.current_page;
              }, function (error) {
                $scope.errorMessage = error.data.errorMessage;
              }
            );
          },
            function () {
              console.log("NO");
            });
        };

        $scope.copy = function (financialYear, version) {
          let modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/project/copy.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllFinancialYearService, ProjectService, FinancialYearService) {

              $scope.sourceFinancialYear = financialYear;
              $scope.sourceVersion = version;

              AllFinancialYearService.query(function (data) {
                $scope.financialYears = data;
                $scope.loadFinancialYearVersions = function (financialYearId) {
                  FinancialYearService.otherVersions({
                    financialYearId: financialYearId,
                    currentVersionId: versionId,
                    type: 'PROJECT',
                    currentFinancialYearId: $scope.sourceFinancialYear.id,
                  }, function (response) {
                    $scope.financialYearVersions = response.data;
                    $scope.copyProjects = function (sourceFinancialYearId, sourceVersionId, destinationVersionId) {
                      ProjectService.copyProjects({
                        sourceVersionId: sourceVersionId,
                        destinationVersionId: destinationVersionId,
                        sourceFinancialYearId: sourceFinancialYearId
                      }, function (response) {
                        $uibModalInstance.close(response);
                      });
                    };
                  });
                };
              });

              $scope.close = function () {
                $uibModalInstance.dismiss('cancel');
              };
            }
          });
          modalInstance.result.then(function (response) {
            $scope.successMessage = response.message;
          },
            function () {

            });
        };
      };
    });
  };
}

ProjectController.resolve = {
  FinancialYearModel: function (AllFinancialYearService, $q) {
    let deferred = $q.defer();
    AllFinancialYearService.query(function (data) {
      deferred.resolve(data);
    });
    return deferred.promise;
  }
};
