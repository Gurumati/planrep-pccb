function ProjectFunderController($scope, ProjectFundersModel, $uibModal, ConfirmDialogService,
                                 DeleteProjectFunderService, ActivateProjectFunderService,
                                 PaginateProjectFunderService) {

    $scope.projectFunders = ProjectFundersModel;
    $scope.title = "TITLE_PROJECT_FUNDERS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginateProjectFunderService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.projectFunders = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/project_funder/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateProjectFunderService, FundersService, ProjectsService) {

                FundersService.query({}, function (data) {
                        $scope.funders = data;
                    },
                    function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
                ProjectsService.query({}, function (data) {
                        $scope.projects = data;
                    },
                    function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
                $scope.projectFunderToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createProjectFunderForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }

                    /*if (!!$scope.projectFunderToCreate.is_loan.selected) {
                        $scope.projectFunderToCreate.push('is_loan', false);
                    }*/

                    CreateProjectFunderService.store($scope.projectFunderToCreate,
                        function (data) {
                            console.log(data);
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.projectFunders = data.projectFunders;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (projectFunderToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/project_funder/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateProjectFunderService, FundersService, ProjectsService) {

                FundersService.query({}, function (data) {
                        $scope.funders = data;
                    },
                    function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
                ProjectsService.query({}, function (data) {
                        $scope.projects = data;
                    },
                    function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
                $scope.projectFunderToEdit = angular.copy(projectFunderToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updateProjectFunderForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateProjectFunderService.update({page: currentPage, perPage: perPage}, $scope.projectFunderToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.projectFunders = data.projectFunders;
                $scope.currentPage = $scope.projectFunders.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog(
            'TITLE_CONFIRM_PROJECT_FUNDERS',
            'CONFIRM_DELETE')
            .then(function () {
                    console.log("YES");
                    DeleteProjectFunderService.delete({projectFunderId: id, currentPage: currentPage, perPage: perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.projectFunders = data.projectFunders;
                            $scope.currentPage = $scope.projectFunders.current_page;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }

    $scope.activateProjectFunder = function (projectFunderId, projectFunderStatus, perPage) {
        $scope.projectFunderToActivate = {};
        $scope.projectFunderToActivate.id = projectFunderId;
        $scope.projectFunderToActivate.is_active = projectFunderStatus;
        ActivateProjectFunderService.activate({perPage: perPage}, $scope.projectFunderToActivate,
            function (data) {
                $scope.successMessage = false;
                $scope.errorMessage = false;
                $scope.activateMessage = data.activateMessage;
            });
    }
}

ProjectFunderController.resolve = {
    ProjectFundersModel: function (PaginateProjectFunderService, $q) {
        var deferred = $q.defer();
        PaginateProjectFunderService.get({page: 1, perPage: 2}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};