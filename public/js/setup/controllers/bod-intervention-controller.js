function BodInterventionController($scope, BodInterventionModel, $uibModal, ConfirmDialogService, DeleteBodInterventionService,
                                PaginatedBodInterventionService) {

    $scope.bodInterventions = BodInterventionModel;
    $scope.title = "BOD_INTERVENTION";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedBodInterventionService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.bodInterventions = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bod_intervention/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateBodInterventionService,AllBodVersion,AllFundSourceService,
                                  AllInterventionCategoryService) {

                AllInterventionCategoryService.query(function (data) {
                    $scope.intervention_categories = data;
                });

               AllBodVersion.query(function (data) {
                    $scope.bod_versions = data;
                });

                AllFundSourceService.query(function (data) {
                    $scope.fund_sources = data;
                });

                $scope.bodInterventionToCreate = {};

                $scope.store = function () {
                    if ($scope.createBodInterventionForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateBodInterventionService.store({perPage: $scope.perPage}, $scope.bodInterventionToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.bodInterventions = data.bodInterventions;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (bodInterventionToEdit, currentPage, perPage) {
        //console.log(bodListToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bod_intervention/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateBodInterventionService,AllBodVersion,AllFundSourceService,
                                  AllInterventionCategoryService) {

                AllInterventionCategoryService.query(function (data) {
                    $scope.intervention_categories = data;
                });

                AllBodVersion.query(function (data) {
                    $scope.bod_versions = data;
                });

                AllFundSourceService.query(function (data) {
                    $scope.fund_sources = data;
                });

                $scope.bodInterventionToEdit = angular.copy(bodInterventionToEdit);
                $scope.update = function () {
                    UpdateBodInterventionService.update({page: currentPage, perPage: perPage}, $scope.bodInterventionToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.bodInterventions = data.bodInterventions;
                $scope.currentPage = $scope.bodInterventions.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteBodInterventionService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.bodInterventions = data.bodInterventions;
                        $scope.currentPage = $scope.bodVersions.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
   
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bod_intervention/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedBodInterventionService,
                                  RestoreBodInterventionService, EmptyBodInterventionTrashService, PermanentDeleteBodInterventionService) {
                TrashedBodInterventionService.query(function (data) {
                    $scope.trashedBodInterventions = data;
                });
                $scope.restoreBodIntervention = function (id) {
                    RestoreBodInterventionService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBodInterventions = data.trashedBodInterventions;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteBodInterventionService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBodInterventions = data.trashedBodInterventions;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyBodInterventionTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBodInterventions = data.trashedBodInterventions;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.bodInterventions = data.bodInterventions;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

BodInterventionController.resolve = {
    BodInterventionModel: function (PaginatedBodInterventionService, $q) {
        var deferred = $q.defer();
        PaginatedBodInterventionService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};