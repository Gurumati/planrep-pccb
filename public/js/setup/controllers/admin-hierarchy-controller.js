function AdminHierarchyController($scope, UserAdminHierarchy, $uibModal, AdministrativeHierarchyService, GlobalService,
    ConfirmDialogService, $timeout, Upload, LoadParentAdminHierarchiesService,
    DeleteAdminHierarchyService, AllAdminHierarchyLevelsService, AdminHierarchiesService) {


    $scope.userAdminHierarchy = UserAdminHierarchy;


    $scope.title = "TITLE_ADMIN_HIERARCHY";

    AllAdminHierarchyLevelsService.query({}, function (data) {
        $scope.adminHierarchyLevels = data;
    });

    GlobalService.adminHierarchyLevels(function (data) {
        $scope.ahLevels = data.ahLevels;
    });

    $scope.showUploadForm = false;
    $scope.showButtons = true;
    $scope.showList = true;

    $scope.revealUploadForm = function () {
        $scope.showUploadForm = true;
        $scope.showButtons = false;
        $scope.showList = false;
    };

    $scope.closeUploadForm = function () {
        $scope.showUploadForm = false;
        $scope.showButtons = true;
        $scope.showList = true;
    };

    /*$scope.hideUploadForm = function () {
        $scope.showUploadForm = false;
        $scope.showButtons = true;
        $scope.showList = true;
    };*/


    $scope.loadChildren = function (a) {
        if (a.children === undefined) {
            a.children = [];
        }
        a.expanded = true;
        AdminHierarchiesService.getAdminHierarchyChildren({ adminHierarchyId: a.id }, function (data) {
            a.children = data.adminHierarchies;
        });
    };
    $scope.setSelected = function (a, p) {
        $scope.selectedAdminHierarchy = a;
        $scope.selectedAdminHierarchyParent = p;
    };
    $scope.toggleCollapse = function (a) {
        a.expanded = false;
    };

    $scope.loadParentAdminHierarchies = function (adminHierarchyLevelId) {
        LoadParentAdminHierarchiesService.query({ childAdminHierarchyLevelId: adminHierarchyLevelId },
            function (data) {
                $scope.adminHierarchyParents = data;
            },
            function (error) {
            }
        );
    };


    $scope.import_admin_hierarchies = function () {
        AdministrativeHierarchyService.import({}, function (data) {
            $scope.adminHierarchies = data.adminHierarchies;
        }, function (error) {
            $scope.errorMessage = error.data.errorMessage;
        }
        );
    };

    $scope.adminHierarchyToCreate = {};

    $scope.uploadFile = function (file) {
        file.upload = Upload.upload({
            url: '/admin-hierarchies/upload',
            method: 'POST',
            data: {
                admin_hierarchy_level_id: $scope.adminHierarchyToCreate.admin_hierarchy_level_id,
                file: file
            },
        });
        file.upload.then(function (response) {
            $timeout(function () {
                file.result = response.data;
                $scope.errorRows = response.data.errorRows;
                $scope.duplicates = response.data.duplicates;
                $scope.parentErrors = response.data.parentErrors;
                $scope.successMessage = response.data.successMessage;
                $scope.closeUploadForm();
                $scope.loadChildren($scope.userAdminHierarchy);
            });
        }, function (response) {
            if (response.status > 0) {
                $scope.errorMsg = response.status + ': ' + response.data;
                $scope.errorMessage = response.data.errorMessage;
            }
        }, function (evt) {
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });

    };


    $scope.create = function () {
        //Modal Form for creating financial year
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/admin_hierarchy/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateAdminHierarchyService, AllAdminHierarchyLevelsService, LoadParentAdminHierarchiesService, PiscTypeService, CofogService, OnMinistryLoadParentAdminHierarchiesService) {

                $scope.parentLebal = "LABEL_ADMIN_HIERARCHY_PARENT";
                $scope.selectParent = "SELECT_PARENT";
                AllAdminHierarchyLevelsService.query({}, function (data) {
                    $scope.adminHierarchyLevels = data;
                });
                // get pisc types
                PiscTypeService.getPiscType(function (data) {
                    $scope.pisc_types = data.pisc_type;
                });

                //COFOG

                CofogService.getAllParent(function (data) {
                    $scope.cofogs = data.cofog;
                });


                $scope.loadParentAdminHierarchies = function (adminHierarchyLevelId) {
                    if (adminHierarchyLevelId === 4) {
                        $scope.piscProperty = true;
                        $scope.piscPropertyRequired = true;
                        $scope.parentLebal = "Sub vote";
                        $scope.selectParent = "Sub vote";
                        let adminHierarchyLevelIdEdited = adminHierarchyLevelId - 1;
                        LoadParentAdminHierarchiesService.query({ childAdminHierarchyLevelId: adminHierarchyLevelIdEdited },
                            function (data) {
                                $scope.adminHierarchyMinistry = data;
                            },
                            function (error) {
                            }
                        );


                    } else {
                        $scope.adminHierarchyToCreate.cofog_id = null;
                        $scope.adminHierarchyToCreate.pisc_type_id = null;
                        $scope.piscProperty = false;
                        $scope.piscPropertyRequired = false;
                        $scope.parentLebal = "LABEL_ADMIN_HIERARCHY_PARENT";
                        $scope.selectParent = "SELECT_PARENT";
                    }

                    $scope.onchangeMinistry = function (id) {
                        OnMinistryLoadParentAdminHierarchiesService.query({ ministryId: id },
                            function (data) {
                                $scope.adminHierarchyParents = data;

                            },
                            function (error) {
                            }
                        );
                    }


                    LoadParentAdminHierarchiesService.query({ childAdminHierarchyLevelId: adminHierarchyLevelId },
                        function (data) {
                            $scope.adminHierarchyParents = data;

                        },
                        function (error) {
                        }
                    );
                };
                $scope.adminHierarchyToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createAdminHierarchyForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }

                    CreateAdminHierarchyService.store($scope.adminHierarchyToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            console.log(error);
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
            //Service to create new financial year
            $scope.successMessage = data.successMessage;
            $scope.loadChildren($scope.userAdminHierarchy);

        },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (adminHierarchyToEdit) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/admin_hierarchy/edit.html',
            backdrop: false,
            controller: function ($scope, $q, $uibModalInstance, UpdateAdminHierarchyService, AllAdminHierarchyLevelsService, LoadParentAdminHierarchiesService, CofogService, PiscTypeService, LoadParentByIdService) {
                //  console.log(adminHierarchyToEdit.admin_hierarchy_level_id );
                AllAdminHierarchyLevelsService.query({}, function (data) {
                    LoadParentAdminHierarchiesService.query({ childAdminHierarchyLevelId: adminHierarchyToEdit.admin_hierarchy_level_id },
                        function (data2) {
                            $scope.adminHierarchyParents = data2;
                            $scope.adminHierarchyLevels = data;
                            $scope.adminHierarchyToEdit = angular.copy(adminHierarchyToEdit);

                            if ($scope.adminHierarchyToEdit.admin_hierarchy_level_id == 4) {
                                $scope.piscProperty = true;
                                $scope.piscPropertyRequired = true;
                                var id = adminHierarchyToEdit.parent_id;
                                $scope.parentLebal = "Sub vote";
                                $scope.selectParent = "Sub vote";
                                LoadParentByIdService.getAllParent({ childId: id }, function (data) {
                                    $scope.selectedVote = data.Selected.length < 0 ? null : data.Selected[0].id;
                                    $scope.allVotes = data.adminHierarchies;
                                });

                            } else {
                                $scope.adminHierarchyToEdit.cofog_id = null;
                                $scope.adminHierarchyToEdit.pisc_type_id = null;
                                // $scope.adminHierarchyToEdit.parent_id = null;
                                $scope.piscProperty = false;
                                $scope.piscPropertyRequired = false;
                                $scope.parentLebal = "LABEL_ADMIN_HIERARCHY_PARENT";
                                $scope.selectParent = "SELECT_PARENT";
                            }

                            $scope.onchangeVote = function (event) {
                                $scope.adminHierarchyToEdit.parent_id = null;
                                LoadParentByIdService.getAllParent({ parentId: event }, function (data) {
                                    $scope.adminHierarchyParents = data.childAdminHierarchies;
                                });
                            }

                            // $scope.admin_hierarchy_level = adminHierarchyToEdit.admin_hierarchy_level_id <= 2 ? false : true;
                        }
                    );

                });

                $scope.loadParentAdminHierarchies = function (adminHierarchyLevelId) {
                    if (adminHierarchyLevelId == 4) {
                        $scope.piscProperty = true;
                        $scope.piscPropertyRequired = true;
                    } else {
                        $scope.adminHierarchyToEdit.cofog_id = null;
                        $scope.adminHierarchyToEdit.pisc_type_id = null;
                        $scope.piscProperty = false;
                        $scope.piscPropertyRequired = false;
                    }
                    LoadParentAdminHierarchiesService.query({ childAdminHierarchyLevelId: adminHierarchyLevelId },
                        function (data) {
                            $scope.adminHierarchyParents = data;
                        },
                        function (error) {
                            console.log(error);
                        }
                    );
                };

                //COFOG

                CofogService.getAllParent(function (data) {
                    $scope.cofogs = data.cofog;
                });

                // Pisc type
                PiscTypeService.getPiscType(function (data) {
                    $scope.pisc_types = data.pisc_type;
                });
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    /*if ($scope.updateAdminHierarchyForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }*/
                    UpdateAdminHierarchyService.update($scope.adminHierarchyToEdit,
                        function (data) {
                            //Successful function when
                            $scope.adminHierarchyToEdit = undefined;
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
            $scope.successMessage = data.successMessage;
            $scope.loadChildren($scope.selectedAdminHierarchyParent);
        },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'TITLE_CONFIRM_DELETE_ADMIN_HIERARCHY',
            'CONFIRM_DELETE')
            .then(function () {
                DeleteAdminHierarchyService.delete({ admin_hierarchy_id: id },
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.loadChildren($scope.selectedAdminHierarchyParent);
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
                function () {
                    clearTree();
                });
    };

    $scope.projects = function (adminHierarchyToEdit) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/admin_hierarchy/projects.html',
            backdrop: false,
            controller: function ($scope, $q, $uibModalInstance, AddAdminHierarchyProjectService, DeleteAdminHierarchyProjectService2, AllProjectService, AllAdminHierarchyProjectService, UpdateAdminHierarchyService, AllAdminHierarchyLevelsService, LoadParentAdminHierarchiesService) {

                $scope.adminHierarchyProjectToCreate = {};
                $scope.admin_hierarchy_id = adminHierarchyToEdit.id;
                $scope.name = adminHierarchyToEdit.name;


                AllAdminHierarchyProjectService.projects({ admin_hierarchy_id: adminHierarchyToEdit.id }, function (data) {
                    $scope.successMessage = data.successMessage;
                    $scope.admin_hierarchy_projects = data.admin_hierarchy_projects;
                    console.log(data);
                }, function (error) {
                    console.log(error)
                }
                );

                AllProjectService.query(function (data) {
                    $scope.projects = data;
                });

                $scope.add_project = function () {
                    var adminHierarchyProjectToSave = {
                        "project_id": $scope.adminHierarchyProjectToCreate.project_id,
                        "admin_hierarchy_id": $scope.admin_hierarchy_id
                    };
                    AddAdminHierarchyProjectService.add_project(adminHierarchyProjectToSave, function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.admin_hierarchy_projects = data.admin_hierarchy_projects;
                    },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.admin_hierarchy_projects = error.data.admin_hierarchy_projects;
                        }
                    );
                };

                $scope.deleteAdminHierarchyProject = function (admin_hierarchy_project_id, admin_hierarchy_id) {
                    console.log('called');
                    DeleteAdminHierarchyProjectService2.delete({
                        admin_hierarchy_project_id: admin_hierarchy_project_id,
                        admin_hierarchy_id: admin_hierarchy_id
                    }, function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.admin_hierarchy_projects = data.admin_hierarchy_projects;
                    }, function (error) {

                    }
                    );
                };

                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
            $scope.successMessage = data.successMessage;
        },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    //Upload Start here
    $scope.$on('$viewContentLoaded', function () {
        var options = {
            beforeSubmit: $scope.validate,
            success: processResponse,
            error: failureHandler,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        };
        $('#uploadAdminHierarchies').ajaxForm(options);
    });

    $scope.validate = function (formData) {
        $scope.$apply(function () {
            $scope.inProgress = true;
            // if($scope.uploadAdminHierarchy.$invalid)
            // {
            //     $scope.formHasErrors=true;
            //     $scope.inProgress =false;
            // }
        });
        if ($scope.inProgress)
            $('#loader').show();
        return $scope.inProgress;
    };

    var failureHandler = function (response) {
        $('#loader').hide();
        $scope.$apply(function () {
            console.log(response);
            $scope.inProgress = false;
        });
    };

    function processResponse(response) {
        $('#loader').hide();
        console.log(response)
        if (response.successMessage) {
            console.log(response.successMessage);
        }
    }


}

AdminHierarchyController.resolve = {
    UserAdminHierarchy: function (AdminHierarchiesService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            AdminHierarchiesService.getUserAdminHierarchy({}, function (data) {
                deferred.resolve(data);
            });
        }, 100);
        return deferred.promise;
    }
};
