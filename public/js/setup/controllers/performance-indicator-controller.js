function PerformanceIndicatorController(
  $scope,
  FinancialYearModel,
  PlanChainService,
  CarryVersionService,
  LowestPlanChainsService,
  AllFinancialYearService,
  FinancialYearService,
  Upload,
  $timeout,
  $uibModal,
  AllPlanChainService,
  ConfirmDialogService,
  PerformanceIndicatorService
) {
  $scope.financialYears = FinancialYearModel;
  $scope.title = "PERFORMANCE_INDICATORS";

  $scope.showList = true;
  $scope.showUploadForm = false;

  $scope.loadVersions = function (financialYearId) {
    $scope.financialYearId = financialYearId;

    FinancialYearService.versions(
      { id: financialYearId, type: "PERFORMANCE_INDICATOR" },
      function (response) {
        $scope.versions = response.data;
      }
    );

    $scope.loadPerformanceIndicators = function (versionId) {
      $scope.versionId = versionId;

      $scope.currentPage = 1;
      $scope.maxSize = 3;
      $scope.perPage = 10;

      PerformanceIndicatorService.paginated(
        {
          searchQuery: $scope.searchQuery,
          page: $scope.currentPage,
          perPage: $scope.perPage,
          financialYearId: financialYearId,
          versionId: versionId,
        },
        function (response) {
          $scope.items = response.data;
        }
      );

      $scope.searchQuery = "";
      $scope.pageChanged = function () {
        PerformanceIndicatorService.paginated(
          {
            searchQuery: $scope.searchQuery,
            page: $scope.currentPage,
            perPage: $scope.perPage,
            financialYearId: $scope.financialYearId,
            versionId: $scope.versionId,
          },
          function (response) {
            $scope.items = response.data;
          }
        );
      };

      $scope.searchList = function (searchQuery) {
        PerformanceIndicatorService.search(
          {
            searchQuery: searchQuery,
            perPage: $scope.perPage,
            page: $scope.currentPage,
            financialYearId: $scope.financialYearId,
            versionId: $scope.versionId,
          },
          function (response) {
            $scope.items = response.data;
          }
        );
      };

      $scope.toggleUploadForm = function (state) {
        if (state === true) {
          $scope.showList = false;
        } else {
          $scope.showList = true;
        }
        $scope.showUploadForm = state;
      };

      /*LowestPlanChainsService.query(function (data) {
                console.log(data);
                $scope.lowestPlanChainType = data[0];
            });*/

      PlanChainService.lowestPlanChains(function (data) {
        $scope.lowestPlanChainTypes = data.items[0].plan_chains;
      });

      $scope.uploadFile = function (file) {
        file.upload = Upload.upload({
          url: "/performanceIndicators/upload",
          method: "POST",
          data: {
            plan_chain_id: $scope.createDataModel.plan_chain_id,
            file: file,
            financialYearId: $scope.financialYearId,
            versionId: $scope.versionId,
            perPage: $scope.perPage,
          },
        });

        file.upload.then(
          function (response) {
            $timeout(function () {
              file.result = response.data.data;
              $scope.items = response.data.data.items;
              $scope.successMessage = response.data.message;
              $scope.toggleUploadForm(false);
            });
          },
          function (response) {
            if (response.status > 0) {
              $scope.errorMsg = response.status + ": " + response.data.data;
              $scope.errorMessage = response.data.message;
            }
          },
          function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            file.progress = Math.min(
              100,
              parseInt((100.0 * evt.loaded) / evt.total)
            );
          }
        );
      };

      $scope.create = function (financialYearId, versionId) {
        var modalInstance = $uibModal.open({
          templateUrl: "/pages/setup/performance_indicator/create.html",
          backdrop: false,
          controller: function (
            $scope,
            $uibModalInstance,
            AllFinancialYearService,
            PlanChainService
          ) {
            PlanChainService.lowestPlanChains(function (data) {
              // $scope.lowestPlanChainTypes = data.items[0].plan_chains;
              $scope.lowestPlanChainTypes = data.items;
              console.log("data", data);
            });

            $scope.createDataModel = {};

            $scope.store = function () {
              if ($scope.createForm.$invalid) {
                $scope.formHasErrors = true;
                return;
              }
              $scope.createDataModel.financialYearId = financialYearId;
              $scope.createDataModel.versionId = versionId;
              $scope.createDataModel.perPage = $scope.perPage;
              $scope.createDataModel.page = $scope.currentPage;
              PerformanceIndicatorService.save(
                $scope.createDataModel,
                function (data) {
                  $uibModalInstance.close(data);
                },
                function (error) {
                  $scope.errorMessage = error.data.errorMessage;
                }
              );
            };
            $scope.close = function () {
              $uibModalInstance.dismiss("cancel");
            };
          },
        });

        modalInstance.result.then(
          function (response) {
            $scope.successMessage = response.message;
            $scope.items = response.data;
            $scope.currentPage = $scope.items.current_page;
          },
          function () {}
        );
      };

      $scope.copy = function (financialYear, version) {
        let modalInstance = $uibModal.open({
          templateUrl: "/pages/setup/performance_indicator/copy.html",
          backdrop: false,
          controller: function (
            $scope,
            $uibModalInstance,
            AllFinancialYearService,
            InterventionService,
            PriorityAreaService,
            FinancialYearService
          ) {
            $scope.sourceFinancialYear = financialYear;
            $scope.sourceVersion = version;

            AllFinancialYearService.query(function (data) {
              $scope.financialYears = data;

              $scope.loadFinancialYearVersions = function (financialYearId) {
                FinancialYearService.otherVersions(
                  {
                    financialYearId: financialYearId,
                    currentVersionId: versionId,
                    type: "PERFORMANCE_INDICATOR",
                    currentFinancialYearId: $scope.sourceFinancialYear.id,
                  },
                  function (response) {
                    $scope.financialYearVersions = response.data;
                    $scope.copyPerformanceIndicators = function (
                      sourceFinancialYearId,
                      sourceVersionId,
                      destinationVersionId
                    ) {
                      PerformanceIndicatorService.copyPerformanceIndicators(
                        {
                          sourceVersionId: sourceVersionId,
                          destinationVersionId: destinationVersionId,
                          sourceFinancialYearId: sourceFinancialYearId,
                        },
                        function (response) {
                          $uibModalInstance.close(response);
                        }
                      );
                    };
                  }
                );
              };
            });

            $scope.close = function () {
              $uibModalInstance.dismiss("cancel");
            };
          },
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(
          function (response) {
            $scope.successMessage = response.message;
          },
          function () {}
        );
      };

      $scope.edit = function (
        updateDataModel,
        currentPage,
        perPage,
        financialYearId,
        versionId,
        planChainVersionId
      ) {
        var modalInstance = $uibModal.open({
          templateUrl: "/pages/setup/performance_indicator/edit.html",
          backdrop: false,
          controller: function (
            $scope,
            AllFinancialYearService,
            FinancialYearService,
            PlanChainService,
            $uibModalInstance
          ) {
            PlanChainService.lowestPlanChains(function (data) {
              $scope.updateDataModel = angular.copy(updateDataModel);
              $scope.lowestPlanChainTypes = data.items;
            });

            $scope.update = function () {
              $scope.updateDataModel.financialYearId = financialYearId;
              $scope.updateDataModel.versionId = versionId;
              $scope.updateDataModel.page = currentPage;
              $scope.updateDataModel.perPage = perPage;

              PerformanceIndicatorService.update(
                { id: $scope.updateDataModel.performance_indicator_id },
                $scope.updateDataModel,
                function (data) {
                  $uibModalInstance.close(data);
                },
                function (error) {
                  $scope.errorMessage = error.data.errorMessage;
                }
              );
            };
            $scope.close = function () {
              $uibModalInstance.dismiss("cancel");
            };
          },
        });

        modalInstance.result.then(
          function (response) {
            $scope.successMessage = response.message;
            $scope.items = response.data;
            $scope.currentPage = $scope.items.current_page;
          },
          function () {}
        );
      };

      $scope.delete = function (
        id,
        currentPage,
        perPage,
        financialYearId,
        versionId
      ) {
        ConfirmDialogService.showConfirmDialog(
          "Confirm Delete!",
          "Are sure you want to delete this?"
        ).then(
          function () {
            PerformanceIndicatorService.delete(
              {
                id: id,
                page: currentPage,
                perPage: perPage,
                financialYearId: financialYearId,
                versionId: versionId,
              },
              function (data) {
                $scope.successMessage = data.message;
                $scope.items = data.data;
                $scope.currentPage = $scope.items.current_page;
              },
              function (error) {
                $scope.errorMessage = error.data.message;
              }
            );
          },
          function () {
            console.log("NO");
          }
        );
      };

      $scope.toggleQualitative = function (id, is_qualitative) {
        $scope.dataModel = {};
        $scope.dataModel.id = id;
        $scope.dataModel.is_qualitative = is_qualitative;
        $scope.dataModel.financialYearId = $scope.financialYearId;
        $scope.dataModel.versionId = $scope.versionId;
        $scope.dataModel.perPage = $scope.perPage;
        PerformanceIndicatorService.toggleQualitative(
          $scope.dataModel,
          function (data) {
            $scope.successMessage = data.message;
          }
        );
      };

      $scope.toggleLessIsGood = function (id, less_is_good) {
        $scope.dataModel = {};
        $scope.dataModel.id = id;
        $scope.dataModel.less_is_good = less_is_good;
        $scope.dataModel.financialYearId = $scope.financialYearId;
        $scope.dataModel.versionId = $scope.versionId;
        $scope.dataModel.perPage = $scope.perPage;
        PerformanceIndicatorService.toggleLessIsGood(
          {},
          $scope.dataModel,
          function (data) {
            $scope.successMessage = data.message;
          }
        );
      };
    };
  };

  $scope.carry = function (currentFinancialYearId, item) {
    CarryVersionService.showDialog("PI", currentFinancialYearId, item).then(
      function (data) {
        console.log(data);
      },
      function () {}
    );
  };
}

PerformanceIndicatorController.resolve = {
  FinancialYearModel: function (AllFinancialYearService, $q) {
    let deferred = $q.defer();
    AllFinancialYearService.query(function (data) {
      deferred.resolve(data);
    });
    return deferred.promise;
  },
};
