function TransportFacilityTypeController($scope, TransportFacilityTypeModel, $uibModal,
                                 ConfirmDialogService, TransportFacilityTypeService) {

    $scope.items = TransportFacilityTypeModel;
    $scope.title = "TRANSPORT_FACILITY_TYPES";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        TransportFacilityTypeService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/transport_facility_type/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, TransportFacilityTypeService) {

                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    TransportFacilityTypeService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        //console.log(updateDataModel);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/transport_facility_type/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, TransportFacilityTypeService) {

                $scope.updateDataModel = angular.copy(updateDataModel);

                $scope.update = function () {
                    TransportFacilityTypeService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = $scope.items.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                TransportFacilityTypeService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.items = data.items;
                        $scope.currentPage = $scope.items.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/transport_facility_type/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance, TransportFacilityTypeService) {
                TransportFacilityTypeService.trashed(function (data) {
                    $scope.trashedItems = data.trashedItems;
                });
                $scope.restoreItem = function (id) {
                    TransportFacilityTypeService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    TransportFacilityTypeService.permanentDelete({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    TransportFacilityTypeService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

TransportFacilityTypeController.resolve = {
    TransportFacilityTypeModel: function (TransportFacilityTypeService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            TransportFacilityTypeService.paginated({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        }, 100);
        return deferred.promise;
    }
};