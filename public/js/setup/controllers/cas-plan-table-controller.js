function CasPlanTableController($scope,
                                $uibModal,
                                CasPlanTablesModel,
                                CasPlanTableGroupTypesService,
                                GetCasGroupColumnByGroupTypeService,
                                CasPlanTablePaginationService,
                                UpdateCasPlanTableService,
                                DeleteCasPlanTableService,
                                CasPlanContentsService,
                                CasPlanTableColumnPaginationService,
                                CasPlanTableColumnsService,
                                DeleteCasPlanTableColumnService,
                                UpdateCasPlanTableColumnService,
                                CasPlanTableNamesService,
                                UpdateCasPlanTableItemService,
                                DeleteCasPlanTableItemService,
                                AllAdminHierarchyLevelsService,
                                CasPlanTableSearchByNameService,
                                ConfirmDialogService) {

    $scope.casPlans = CasPlanTablesModel;
    $scope.title = "CAS_PLAN_TABLES";
    $scope.defaultMode = true;
    $scope.columnListMode=false;
    $scope.maxSize = 3;
    $scope.currentPage = 1;
    $scope.perPage = 10;
    $scope.default_params = true;

    $scope.loadContents = function (cas_plan_id) {
        var perPage = 30;
        CasPlanTablePaginationService.indexPaginated({cas_plan_id: cas_plan_id, perPage: perPage},function (data) {
            $scope.planTables = data;
        });
    };

    $scope.pageChanged = function () {
        var params = {};
        params.cas_plan_id = $scope.selectedPlan;
        params.page = $scope.currentPage;
        params.perPage = 10;
        CasPlanTablePaginationService.indexPaginated(params, function (data) {
            $scope.planTables = data;
        });
    };

    $scope.casPlaTableColumnPageChanged = function (table_id) {
        CasPlanTableColumnPaginationService.get({page: $scope.currentPage,perPage:$scope.perPage, table_id: table_id}, function (data) {
            $scope.casPlanTableColumns = data;
        });
    };

    $scope.create = function (cas_plan_id) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/cas_plan_table/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,CreateCasPlanTableService,GetAllCasPlanTablesService, LoadReportParametersSources) {
                $scope.parameter_values = {};
                $scope.set_param_values = function (param_values) {
                    $scope.params = param_values.split(',');

                    if($scope.params.length > 0){
                        var inputData = {parameters: $scope.params};
                        LoadReportParametersSources.getParams(inputData, function (data) {
                            $scope.reportParamsData = data.parameter_data;
                            console.log($scope.reportParamsData);
                        });
                    }
                };

                //flatter method
                $scope.flatter = function (data_array, result) {
                    angular.forEach(data_array, function (value, key) {
                        result.push(value);
                        if(value.children.length > 0)
                        {
                            return $scope.flatter(value.children, result);
                        }
                    });
                    return result;
                };
                //get cas plan content
                CasPlanContentsService.query({cas_plan_id: cas_plan_id},
                    function(data){
                        $scope.casPlanContents = [];
                        $scope.flatter(data, $scope.casPlanContents);
                    });

                //Get the list of cas plan tables
                GetAllCasPlanTablesService.query(function (data) {
                    $scope.cas_plans = data;
                }, function (error){
                    console.log(error);
                });
                $scope.casPlanTableToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createCasPlanTableForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    $scope.casPlanTableToCreate.cas_plan_id = cas_plan_id;
                    CreateCasPlanTableService.store({perPage:$scope.perPage},$scope.casPlanTableToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                           $scope.errorMessage=error.data.errorMessage;
                        }
                    );

                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage=data.successMessage;
                $scope.planTables=data.planTables;
                $scope.currentPage = $scope.planTables.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.edit = function (cas_plan_id,casTableToEdit, currentPage,perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/cas_plan_table/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,GetAllCasPlanTablesService, LoadReportParametersSources) {
                $scope.casTableToEdit = angular.copy(casTableToEdit);
                $scope.casTableToEdit.cas_plan_id = cas_plan_id;

                //Get the list of cas plan tables
                GetAllCasPlanTablesService.query(function (data) {
                    $scope.cas_plans = data;
                }, function (error){
                    console.log(error);
                });

                // $scope.default_vales = {};

                $scope.set_param_values = function (param_values) {
                    $scope.params = param_values.split(',');
                    if($scope.params.length > 0){
                        var inputData = {parameters: $scope.params};
                        LoadReportParametersSources.getParams(inputData, function (data) {
                            $scope.reportParamsData = data.parameter_data;
                        });
                    }
                };

                //flatter method
                $scope.flatter = function (data_array, result) {
                    angular.forEach(data_array, function (value, key) {
                        result.push(value);
                        if(value.children.length > 0)
                        {
                            return $scope.flatter(value.children, result);
                        }
                    });
                    return result;
                };
                //get cas plan content
                CasPlanContentsService.query({cas_plan_id: $scope.casTableToEdit.cas_plan_id},
                    function(data){
                        $scope.casPlanContents = [];
                        $scope.flatter(data, $scope.casPlanContents);
                    });

                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.editCasPlanTableForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    $uibModalInstance.close($scope.casTableToEdit);
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (casTableToEdit) {
                //Service to create cas plan table
                var params = {page:currentPage,perPage:perPage};
                UpdateCasPlanTableService.update(params,casTableToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage=data.successMessage;
                        $scope.planTables = data.planTables;
                        $scope.currentPage = $scope.planTables.current_page;
                    },
                    function (error) {
                        console.log(error);
                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.viewColumns = function (table_id) {
        $scope.defaultMode = false;
        $scope.columnListMode=true;
        $scope.table_id = table_id;
        $scope.perPage = 10;

        CasPlanTableColumnPaginationService.get({page:1,perPage:$scope.perPage,table_id:table_id}, function (data) {
                $scope.casPlanTableColumns = data;
                $scope.totalItems = data.total;
                $scope.perPage = data.per_page;
                $scope.currentPage = $scope.casPlanTableColumns.current_page;

            },
            function (error) {
                $scope.errorMessage = error.data.errorMessage;
            });
    };

    $scope.backToTable = function () {
        $scope.defaultMode = true;
        $scope.columnListMode=false;
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Deletion!',
            'Are sure you want to delete this record?')
            .then(function () {
                    console.log("YES");
                    DeleteCasPlanTableService.delete({id: id,cas_plan_id: $scope.selectedPlan, currentPage:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage=data.successMessage;
                            $scope.planTables = data.planTables;
                            $scope.currentPage = $scope.planTables.current_page;
                        }, function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    };

    $scope.addColumn = function (table_id) {
        //Modal Form for adding a column to the cas plan tables
        var modalInstance = $uibModal.open({
            templateUrl: 'add-column.html',
            backdrop: false,
            controller: function ($scope,
                                  CasPlanTableNamesService,
                                  CreateCasPlanTableColumnService,
                                  CasPlanTableSelectOptionsService,
                                  $uibModalInstance
            ) {

                //The json object to store the allowed types
                $scope.types    = ["Checkbox","Text","Number","Formula","Select"];
                $scope.listType = ['Facility',"Vehicles"];
                $scope.is_select = false;
                //check if is select
                $scope.typeSelect = function (item) {
                  if(item == 'Select')
                  {
                      $scope.is_select = true;
                  } else {
                      $scope.is_select = false;
                  }
                };

                //load admin hierarhy levels
                AllAdminHierarchyLevelsService.query({}, function (data) {
                    $scope.adminLevels = data;
                });

                //load cas plan table select options
                CasPlanTableSelectOptionsService.query(function (data) {
                    $scope.selectOptions = data;
                });

                //load cas plan table id
                CasPlanTableNamesService.query(function (data) {
                    $scope.casPlanTables = data;
                });

                //load cas plan table columns
                CasPlanTableColumnsService.query({table_id: table_id},function (data) {

                    $scope.casPlanTableColumns = data;
                    // $scope.currentPage = $scope.casPlanTableColumns.current_page;
                });

                //load cas plan table group types
                CasPlanTableGroupTypesService.query(function (data) {
                    $scope.group_types = data;
                });

                $scope.load_group_columns = function (group_type_id) {
                    GetCasGroupColumnByGroupTypeService.query({group_type_id: group_type_id},function (data) {
                        $scope.groupColumnByTypes = data;
                    });
                };

                $scope.casPlanTableColumnFormToCreate = {};
                //Function to store data and close modal when Create button clicked

                $scope.storeColumn = function () {
                    if ($scope.createCasPlanTableColumnForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    $scope.casPlanTableColumnFormToCreate.cas_plan_table_id = table_id;
                    console.log($scope.casPlanTableColumnFormToCreate);
                    CreateCasPlanTableColumnService.store({perPage: $scope.perPage},$scope.casPlanTableColumnFormToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.casPlanTableColumns = data.casPlanTableColumns;
                $scope.currentPage = $scope.casPlanTableColumns.current_page;

            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.deleteCasPlanTableColumn = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Delete Column!',
            'Are sure you want to delete this?')
            .then(function () {
                    console.log("YES");
                    DeleteCasPlanTableColumnService.delete({currentPage: currentPage, perPage: perPage,id: id,},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.casPlanTableColumns = data.casPlanTableColumns;
                            $scope.currentPage = $scope.casPlanTableColumns.current_page;
                        }, function (error) {
                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    };

    $scope.editColumns = function (CasPlanTableColumnFormToEdit,currentPage,perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/cas_plan_table/edit-column.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CasPlanTableSelectOptionsService) {
                $scope.CasPlanTableColumnFormToEdit = angular.copy(CasPlanTableColumnFormToEdit);

                //The json object to store the allowed types
                $scope.types    = ["Checkbox","Text","Number","Formula","Select"];
                $scope.listType = ['Facility',"Vehicles"];
                $scope.is_select = false;

                //check if is select
                $scope.typeSelect = function (item) {
                    if(item == 'Select')
                    {
                        $scope.is_select = true;
                    } else {
                        $scope.is_select = false;
                    }
                };

                //load admin hierarhy levels
                AllAdminHierarchyLevelsService.query({}, function (data) {
                    $scope.adminLevels = data;
                });

                //load cas plan table select options
                CasPlanTableSelectOptionsService.query(function (data) {
                    $scope.selectOptions = data;
                });

                //load cas plan table id
                CasPlanTableNamesService.query(function (data) {
                    $scope.casPlanTables = data;
                });

                //load cas plan table columns
                CasPlanTableColumnsService.query({table_id:$scope.CasPlanTableColumnFormToEdit.cas_plan_table_id },function (data) {
                    $scope.casPlanTableColumns = data;
                });

                //Function to store data and close modal when Create button clicked
                $scope.updateColumns = function () {
                    $uibModalInstance.close($scope.CasPlanTableColumnFormToEdit);
                };

                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (CasPlanTableColumnFormToEdit) {

                //Service to create new budget class
                UpdateCasPlanTableColumnService.update({page: currentPage,perPage:perPage}, CasPlanTableColumnFormToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage = data.successMessage;
                        $scope.casPlanTableColumns = data.casPlanTableColumns;
                        $scope.currentPage = $scope.casPlanTableColumns.current_page;
                    },
                    function (error) {
                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.viewItems = function (cas_plan_table_id) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/cas_plan_table_item/index.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CasPlanTableItemsService, CreateCasPlanTableItemService) {
                $scope.casPlanTableItemToCreate = {};
                $scope.currentPage = 1;
                $scope.perPage = 10;
                $scope.maxSize = 3;

                $scope.createItem = false;
                $scope.editItem = false;
                $scope.viewItems  = true; //show items

                CasPlanTableItemsService.get({cas_plan_table_id: cas_plan_table_id, page:$scope.currentPage,perPage:$scope.perPage},function (data) {
                    $scope.totalItems = data.total;
                    $scope.perPage = data.per_page;
                    $scope.planTableItems = data;
                });

                $scope.addItem = function () {
                   $scope.editItem = false;
                   $scope.viewItems  = false;
                   $scope.createItem = true;
                   $scope.casPlanTableItemToCreate = {}; //clear data;
                };

                $scope.updateItem = function (casPlanTableItem, currentPage,perPage) {
                   $scope.editItem = true;
                   $scope.viewItems  = false;
                   $scope.createItem = false;
                   $scope.casPlanTableItemToUpdate = angular.copy(casPlanTableItem); //clear data;
                   $scope.currentPage = currentPage;
                   $scope.perPage = perPage;
                };

                $scope.back = function () {
                   $scope.viewItems  = true;
                   $scope.createItem = false;
                   $scope.editItem = false;
                };

                $scope.pageItemChanged = function () {
                    CasPlanTableItemsService.get({cas_plan_table_id: cas_plan_table_id,
                                                  page: $scope.currentPage,
                                                  perPage: $scope.perPage},
                        function (data) {
                            $scope.planTableItems = data;
                    });
                };

                $scope.store = function () {
                    if ($scope.createCasPlanTableItemForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    $scope.casPlanTableItemToCreate.cas_plan_table_id = cas_plan_table_id;
                    CreateCasPlanTableItemService.store({perPage:$scope.perPage}, $scope.casPlanTableItemToCreate,
                        function (data) {
                           $scope.successMessage = data.successMessage;
                           $scope.planTableItems = data.planTableItems;
                           $scope.back();
                        },
                        function (error) {
                            $scope.errorMessage=error.data.errorMessage;
                        }
                    );
                };

                $scope.updateRow = function () {
                    if ($scope.editCasPlanTableItemForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }

                    UpdateCasPlanTableItemService.edit({perPage:$scope.perPage}, $scope.casPlanTableItemToUpdate,
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.planTableItems = data.planTableItems;
                            $scope.back();
                        },
                        function (error) {
                            $scope.errorMessage=error.data.errorMessage;
                        }
                    );
                };

                $scope.deleteRow = function (id,currentPage,perPage) {
                    ConfirmDialogService.showConfirmDialog(
                        'Confirm Deletion!',
                        'Are sure you want to delete this record?')
                        .then(function () {
                                console.log("YES");
                                DeleteCasPlanTableItemService.delete({id: id,currentPage:currentPage,perPage:perPage},
                                    function (data) {
                                        $scope.successMessage=data.successMessage;
                                        $scope.planTableItems = data.planTableItems;
                                        $scope.back();

                                    }, function (error) {
                                        $scope.errorMessage=error.data.errorMessage;
                                    }
                                );
                            },
                            function () {
                                console.log("NO");
                            });
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });

    };

    $scope.constraints = function (table_id) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/cas_plan_table/constraints.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CasPlanTableConstraintsService) {

                CasPlanTableConstraintsService.getCasPlanTableConstraints({id: table_id}, function (data) {
                    $scope.constraintToCreateEdit = data;
                    try {
                        $scope.constraintToCreateEdit.order_by = JSON.parse($scope.constraintToCreateEdit.order_by);
                    }
                    catch(e) {
                        $scope.constraintToCreateEdit.order_by=[];
                    }

                });

                //Service to get all the columns of the selected table
                CasPlanTableColumnsService.query({table_id: table_id}, function (data) {
                    $scope.casPlanTableColumns = data;
                    checkEditCreate();
                });


                var checkEditCreate = function(){
                    if ($scope.constraintToCreateEdit === undefined){
                        //The data is empty therefore create the model to create data
                        $scope.constraintToCreateEdit = {};
                        $scope.constraintToCreateEdit.cas_plan_table_id = table_id;
                    } else {
                        //The data is not empty therefore create the model to edit data
                        $scope.constraintToCreateEdit.cas_plan_table_id = table_id;
                        $scope.constraintToCreateEdit = angular.copy($scope.constraintToCreateEdit);
                    }
                };

                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.editCreateConstraintForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    console.log($scope.constraintToCreateEdit);
                    var constraintToCreateEdit=angular.copy($scope.constraintToCreateEdit);
                    constraintToCreateEdit.order_by=JSON.stringify(constraintToCreateEdit.order_by);
                    constraintToCreateEdit.cas_plan_table_id = table_id;
                    CasPlanTableConstraintsService.save(constraintToCreateEdit,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage=error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
    };

    $scope.searchTable = function () {
      if($scope.searchQuery !== undefined)
      {
          CasPlanTableSearchByNameService.get({searchQuery: $scope.searchQuery, cas_plan_id: $scope.selectedPlan}, function (data) {
              $scope.planTables = data;
          });
      }
    };
}

CasPlanTableController.resolve = {
    CasPlanTablesModel: function (CasPlanService, $q) {
        var deferred = $q.defer();
        CasPlanService.query({page:1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};
