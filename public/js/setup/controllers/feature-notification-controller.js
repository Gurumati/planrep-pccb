(function () {
    'use strict';

    angular.module('setup-module').controller(
        'FeatureNotificationController', 
         FeatureNotificationController);

    FeatureNotificationController.$inject     =  [
        '$scope', 
        '$uibModal',
        'ConfirmDialogService',
        'FeatureNotification'];

    function FeatureNotificationController(
        $scope,
        $uibModal,
        ConfirmDialogService,
        FeatureNotification) {

        $scope.perPage = 10;
       
        $scope.pageChanged = function () {
            FeatureNotification.get({
                page: $scope.currentPage,
                perPage:$scope.perPage
            }, function (data) {
                $scope.featureNotification = data;
                setPagination(data);
            });
        };

        var setPagination = function (pagination) {
            $scope.currentPage = pagination.current_page;
            $scope.perPage = pagination.per_page;
        };

        $scope.pageChanged();

        $scope.update = function (featureNotification) {
            var modalInstance = $uibModal.open({
                templateUrl: '/pages/setup/feature_notification/update.html',
                backdrop: false,
                controller: ['$scope','$uibModalInstance','FeatureNotification',
                    function ($scope, $uibModalInstance, FeatureNotification) {
                    $scope.featureNotification = featureNotification;
                    if ($scope.featureNotification.id) {
                        $scope.featureNotification.expiration_date = new Date(featureNotification.expiration_date);
                    }
                    $scope.update = function () {
                        if ($scope.featureNotificationForm.$invalid) {
                            $scope.formHasErrors = true;
                            return;
                        }
                        FeatureNotification.save({}, $scope.featureNotification,
                            function (data) {
                                $uibModalInstance.close(data);
                            },
                            function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                                $scope.errors = error.data.errors;
                            }
                        );
                    };
                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }]
            });
            modalInstance.result.then(function (data) {
                $scope.messsage = data.successMessage;
                $scope.pageChanged(); 
            },
            function () {
               console.log('Modal dismissed at: ' + new Date());
            });

        };

        $scope.delete = function (id) {
            ConfirmDialogService.showConfirmDialog(
                'Delete Notification',
                'Delete Notification')
                .then(function () {
                        FeatureNotification.delete({id: id},
                            function (data) {
                                $scope.messsage = data.successMessage;
                                $scope.messageClass = 'alert-success';
                                $scope.activityCategories = data.activityCategories;
                                $scope.pageChanged();
                            }, function (error) {
                                $scope.messsage = error.data.errorMessage;
                                $scope.messageClass = 'alert-warning';
                            }
                        );
                    },
                    function () {

                    });
        };
    }

})();
