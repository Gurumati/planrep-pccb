function AdminHierarchyProjectController($scope, AdminHierarchyProjectModel, $uibModal,
                                         ConfirmDialogService, DeleteAdminHierarchyProjectService, PaginatedAdminHierarchyProjectService) {

    $scope.adminHierarchyProjects = AdminHierarchyProjectModel;
    $scope.title = "TITLE_ADMIN_HIERARCHY_PROJECTS";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedAdminHierarchyProjectService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.adminHierarchyProjects = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/admin_hierarchy_project/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllAdminHierarchyService, AllProjectService, CreateAdminHierarchyProjectService) {
                $scope.adminHierarchyProjectToCreate = {};
                AllAdminHierarchyService.query(function (data) {
                    $scope.adminHierarchies = data;
                });
                AllProjectService.query(function (data) {
                    $scope.projects = data;
                });
                $scope.store = function () {
                    if ($scope.createAdminHierarchyProjectForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateAdminHierarchyProjectService.store({perPage: $scope.perPage}, $scope.adminHierarchyProjectToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.adminHierarchyProjects = data.adminHierarchyProjects;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (adminHierarchyProjectToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/admin_hierarchy_project/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllAdminHierarchyService, AllProjectService, UpdateAdminHierarchyProjectService) {
                AllAdminHierarchyService.query(function (data) {
                    $scope.adminHierarchies = data;
                    $scope.adminHierarchyProjectToEdit = angular.copy(adminHierarchyProjectToEdit);
                });
                AllProjectService.query(function (data) {
                    $scope.projects = data;
                });
                $scope.update = function () {
                    if ($scope.updateAdminHierarchyProjectForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateAdminHierarchyProjectService.update({page: currentPage, perPage: perPage}, $scope.adminHierarchyProjectToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.adminHierarchyProjects = data.adminHierarchyProjects;
                $scope.currentPage = $scope.adminHierarchyProjects.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('TITLE_CONFIRM_DELETE_ADMIN_HIERARCHY_PROJECT', 'CONFIRM_DELETE').then(function () {
                DeleteAdminHierarchyProjectService.delete({admin_hierarchy_project_id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.adminHierarchyProjects = data.adminHierarchyProjects;
                        $scope.currentPage = $scope.adminHierarchyProjects.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    }
}

AdminHierarchyProjectController.resolve = {
    AdminHierarchyProjectModel: function (PaginatedAdminHierarchyProjectService,$timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            PaginatedAdminHierarchyProjectService.get({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        }, 900);
        return deferred.promise;
    }
};