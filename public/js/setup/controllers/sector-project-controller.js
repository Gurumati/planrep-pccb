function SectorProjectController($scope, SectorProjectModel, $uibModal,
                                         ConfirmDialogService, DeleteSectorProjectService, PaginatedSectorProjectService) {

    $scope.sectorProjects = SectorProjectModel;
    $scope.title = "TITLE_SECTOR_PROJECTS";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedSectorProjectService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.sectorProjects = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/sector_project/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllSectorService, AllProjectService, CreateSectorProjectService) {
                $scope.sectorProjectToCreate = {};
                AllSectorService.query(function (data) {
                    $scope.sectors = data;
                });
                AllProjectService.query(function (data) {
                    $scope.projects = data;
                });
                $scope.store = function () {
                    if ($scope.createSectorProjectForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateSectorProjectService.store({perPage: $scope.perPage}, $scope.sectorProjectToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.sectorProjects = data.sectorProjects;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (sectorProjectToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/sector_project/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllSectorService, AllProjectService, UpdateSectorProjectService) {
                AllSectorService.query(function (data) {
                    $scope.sectors = data;
                    $scope.sectorProjectToEdit = angular.copy(sectorProjectToEdit);
                });
                AllProjectService.query(function (data) {
                    $scope.projects = data;
                });
                $scope.update = function () {
                    if ($scope.updateSectorProjectForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateSectorProjectService.update({page: currentPage, perPage: perPage}, $scope.sectorProjectToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.sectorProjects = data.sectorProjects;
                $scope.currentPage = $scope.sectorProjects.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('TITLE_CONFIRM_DELETE_SECTOR_PROJECT', 'CONFIRM_DELETE').then(function () {
                DeleteSectorProjectService.delete({sector_project_id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.sectorProjects = data.sectorProjects;
                        $scope.currentPage = $scope.sectorProjects.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    }
}

SectorProjectController.resolve = {
    SectorProjectModel: function (PaginatedSectorProjectService, $q) {
        var deferred = $q.defer();
        PaginatedSectorProjectService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};