function ExpenditureCentreGfsCodeController($scope, ExpenditureCentreGfsCodeModel, $uibModal,
                                     ConfirmDialogService, DeleteExpenditureCentreGfsCodeService, PaginatedExpenditureCentreGfsCodeService) {

    $scope.expenditureCentreGfsCodes = ExpenditureCentreGfsCodeModel;
    $scope.title = "EXPENDITURE_CENTRE_GFS_CODES";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedExpenditureCentreGfsCodeService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.expenditureCentreGfsCodes = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/expenditure_centre_gfs_code/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllExpenditureCentreService, AllGfsCodeService, CreateExpenditureCentreGfsCodeService) {
                $scope.expenditureCentreGfsCodeToCreate = {};

                AllExpenditureCentreService.query(function (data) {
                    $scope.expenditure_centres = data;
                });

                AllGfsCodeService.query(function (data) {
                    $scope.gfs_codes = data;
                });

                $scope.store = function () {
                    if ($scope.createExpenditureCentreGfsCodeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateExpenditureCentreGfsCodeService.store({perPage: $scope.perPage}, $scope.expenditureCentreGfsCodeToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.expenditureCentreGfsCodes = data.expenditureCentreGfsCodes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (expenditureCentreGfsCodeToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/expenditure_centre_gfs_code/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllExpenditureCentreService, AllGfsCodeService, UpdateExpenditureCentreGfsCodeService) {

                AllExpenditureCentreService.query(function (data) {
                    $scope.expenditure_centres = data;
                    $scope.expenditureCentreGfsCodeToEdit = angular.copy(expenditureCentreGfsCodeToEdit);
                });

                AllGfsCodeService.query(function (data) {
                    $scope.gfs_codes = data;
                });
                $scope.update = function () {
                    if ($scope.updateExpenditureCentreGfsCodeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateExpenditureCentreGfsCodeService.update({page: currentPage, perPage: perPage}, $scope.expenditureCentreGfsCodeToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.expenditureCentreGfsCodes = data.expenditureCentreGfsCodes;
                $scope.currentPage = $scope.expenditureCentreGfsCodes.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('CONFIRM_DELETE', 'Are you sure you want to trash this record?').then(function () {
                DeleteExpenditureCentreGfsCodeService.delete({expenditure_centre_gfs_code_id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.expenditureCentreGfsCodes = data.expenditureCentreGfsCodes;
                        $scope.currentPage = $scope.expenditureCentreGfsCodes.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/expenditure_centre_gfs_code/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedExpenditureCentreGfsCodeService,
                                  RestoreExpenditureCentreGfsCodeService, EmptyExpenditureCentreGfsCodeTrashService, PermanentDeleteExpenditureCentreGfsCodeService) {
                TrashedExpenditureCentreGfsCodeService.query(function (data) {
                    $scope.trashedExpenditureCentreGfsCodes = data;
                });
                $scope.restoreExpenditureCentreGfsCode = function (id) {
                    RestoreExpenditureCentreGfsCodeService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedExpenditureCentreGfsCodes = data.trashedExpenditureCentreGfsCodes;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteExpenditureCentreGfsCodeService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedExpenditureCentreGfsCodes = data.trashedExpenditureCentreGfsCodes;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyExpenditureCentreGfsCodeTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedExpenditureCentreGfsCodes = data.trashedExpenditureCentreGfsCodes;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.expenditureCentreGfsCodes = data.expenditureCentreGfsCodes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

ExpenditureCentreGfsCodeController.resolve = {
    ExpenditureCentreGfsCodeModel: function (PaginatedExpenditureCentreGfsCodeService, $q) {
        var deferred = $q.defer();
        PaginatedExpenditureCentreGfsCodeService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};