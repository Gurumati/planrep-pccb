function UserGroupController($scope, UserGroupsModel, $uibModal,
                                 CreateUserGroupService,
                                 UpdateUserGroupService,
                                 ConfirmDialogService,
                                 ToggleUserGroupService,
                                 DeleteUserGroupService,
                                 UserGroupsService) {

    $scope.userGroups = UserGroupsModel;
    $scope.title = "TITLE_USER_GROUP";
    $scope.dateFormat = 'yyyy-MM-dd';

    $scope.pageChanged=function(){
        UserGroupsService.get({page:$scope.currentPage}, function(data){
            $scope.userGroups=data;
        });
    };


    $scope.create = function () {
        //Modal Form for creating unit year

        var modalInstance = $uibModal.open({
            templateUrl: 'create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,CreateUserGroupService) {
                $scope.userGroupToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createUserGroupForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateUserGroupService.store($scope.userGroupToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                           $scope.errorMessage=error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage=data.successMessage;
                $scope.userGroups=data.userGroups;

            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (userGroupToEdit,currentPage) {
        console.log(userGroupToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: 'edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {
                $scope.userGroupToEdit = angular.copy(userGroupToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    $uibModalInstance.close($scope.userGroupToEdit);
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (userGroupToEdit) {
                //Service to create new unit
                UpdateUserGroupService.update({page:currentPage},userGroupToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage=data.successMessage;
                        $scope.userGroups = data.userGroups; //After save return all userGroups and update $scope.userGroups
                    },
                    function (error) {

                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

   $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Delete UserGroup !',
            'Are sure you want to delete this?')
            .then(function () {
                    console.log("YES");
                    DeleteUserGroupService.delete({userGroupId: id},
                        function (data) {
                            //Successful function when
                            $scope.successMessage=data.successMessage;
                            $scope.userGroups = data.userGroups;
                        }, function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }
        $scope.toggleUserGroup = function (userGroup_id, userGroup_status) {
        $scope.userGroupToActivate = {};
        $scope.userGroupToActivate.id = userGroup_id;
        $scope.userGroupToActivate.is_active = userGroup_status;
        ToggleUserGroupService.toggleUserGroup($scope.userGroupToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    }
}

UserGroupController.resolve = {

    UserGroupsModel: function (UserGroupsService, $q) {
        var deferred = $q.defer();
       UserGroupsService.get({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};