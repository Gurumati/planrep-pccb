function SectorController($scope, PaginatedSectorModel, $uibModal,
                          UpdateSectorService,
                          ConfirmDialogService,
                          DeleteSectorService,
                          SectorsPaginationService) {

    $scope.sectors = PaginatedSectorModel;
    $scope.title = "SECTORS";

    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        SectorsPaginationService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
            $scope.sectors = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating sector

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/sector/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateSectorService) {
                $scope.sectorToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createSectorForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateSectorService.store({perPage:$scope.perPage},$scope.sectorToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.sectors = data.sectors;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (sectorToEdit, currentPage,perPage) {
        console.log(sectorToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/sector/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {
                $scope.sectorToEdit = angular.copy(sectorToEdit);
                $scope.update = function () {
                    $uibModalInstance.close($scope.sectorToEdit);
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (sectorToEdit) {
                //Service to create new financial year
                UpdateSectorService.update({page: currentPage,perPage:perPage}, sectorToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage = data.successMessage;
                        $scope.sectors = data.sectors;
                        $scope.currentPage = $scope.sectors.current_page;
                    },
                    function (error) {

                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete Sector!', 'Are sure you want to delete this?').then(function () {
                console.log("YES");
                DeleteSectorService.delete({sectorId: id,page:currentPage,perPage:perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.sectors = data.sectors;
                        $scope.currentPage = $scope.sectors.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    }

}

SectorController.resolve = {
    PaginatedSectorModel: function (SectorsPaginationService, $q) {
        var deferred = $q.defer();
        SectorsPaginationService.get({page: 1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};