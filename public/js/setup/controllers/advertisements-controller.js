function AdvertisementsController($scope, DataModel, $uibModal, ConfirmDialogService,AdvertisementService) {
    $scope.items = DataModel;
    $scope.title = "ADVERTISEMENTS_MANAGEMENT";
    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.perPage = 10;

    $scope.pageChanged = function () {
        AdvertisementService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (response) {
            $scope.items = response.items;
        }, function (error) {
        });
    };
//switch
    $scope.adStatusChange = function (item) {
    AdvertisementService.changeAdStatus({id:item.id,status:item.status}, function (data) {
        $scope.items = data.items;
    },function (error) {

    });
    };

    $scope.create = function () {
        let modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/advertisements/create.html',
            backdrop: false,
            controller: function ($scope, $http, $uibModalInstance) {
                $scope.createDataModel = {};
                $scope.createDataModel.urls = [];
                $scope.reference_file_names = [];
              $scope.options = {
                minDate: new Date(), // set this to whatever date you want to set
              };
                $scope.remove = function (filename) {
                    var index = $scope.reference_file_names.indexOf(filename);
                    AdvertisementService.removeFile({"file_url": $scope.createDataModel.urls[index]},
                        function (response) {
                        },
                        function (error) {

                        });

                    $scope.reference_file_names.splice(index, 1);
                    $scope.createDataModel.urls.splice(index, 1);
                };
                $scope.store = function () {
                    if ($scope.reference_file_names.length < 1) {
                        $scope.formHasErrors = true;
                        $scope.errorMessage = 'Invalid Image Format';
                        return;
                    }

                    AdvertisementService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                //upload image
                $scope.uploadFile = function (files) {
                    var formData = new FormData();
                    formData.append('file', files[0]);
                    if ($scope.reference_file_names.indexOf(files[0].name) == -1) {
                        $scope.reference_file_names.push(files[0].name);
                        uploadUrl = "/api/upload-advertisements";

                        $http.post(uploadUrl, formData, {
                            withCredentials: true,
                            headers: {'Content-Type': undefined},
                            transformRequest: angular.identity
                        }).then(
                            function (response) {
                                if (response.data == 0) {
                                    $scope.reference_file_names.pop();
                                } else {
                                    $scope.createDataModel.urls.push({ad_url:response.data});
                                }
                            },
                            function (e) {

                                $scope.reference_file_names.pop();
                                //$scope.errorMessage = error.data.errorMessage;
                                console.log(e);
                            });
                    }
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (response) {
                $scope.successMessage = response.successMessage;
                $scope.items = response.items;
                $scope.currentPage = response.items.current_page;
            },
            function () {

            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        let modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/advertisements/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {

                $scope.updateDataModel = angular.copy(updateDataModel);

                $scope.update = function () {
                    if ($scope.updateForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    AdvertisementService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (response) {
                //Service to create new financial year
                $scope.successMessage = response.successMessage;
                $scope.items = response.items;
                $scope.currentPage = response.items.current_page;
            },
            function () {

            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                AdvertisementService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.message;
                        $scope.items = data.data;
                        $scope.currentPage = $scope.items.data.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
}
AdvertisementsController.resolve = {
    DataModel: function (AdvertisementService, $q) {
        let deferred = $q.defer();
        AdvertisementService.paginated({page: 1, perPage: 10}, function (response) {
            deferred.resolve(response.items);
        });
        return deferred.promise;
    }
};
