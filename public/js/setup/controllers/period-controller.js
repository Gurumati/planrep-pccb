function PeriodController($scope, AllPeriodModel, $uibModal,
                          TogglePeriodService,
                          ConfirmDialogService,
                          DeletePeriodService,GetAllPeriodService,CalendarFinancialYears) {

    $scope.periods = AllPeriodModel;
    $scope.title = "TITLE_PERIODS";

    $scope.dateFormat = 'yyyy-MM-DD';
    $scope.financialYear={id:undefined};
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        GetAllPeriodService.get({page: $scope.currentPage,perPage:$scope.perPage,financialYearId:$scope.financialYear.id}, function (data) {
            $scope.periods = data;
        });
    };
    CalendarFinancialYears.query(function (data) {
        $scope.financialYears = data;
    });

    $scope.getPeriodsByFinancialYear=function () {
        $scope.pageChanged();
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/period/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreatePeriodService, FinancialYearsService,AllPeriodGroupService) {

                AllPeriodGroupService.query(function (data) {
                    $scope.periodGroups = data;
                });

                $scope.periodToCreate = {};
                //Function to store data and close modal when Create button clicked
                FinancialYearsService.query(function (data) {
                    $scope.financialYears = data;
                });
                $scope.store = function () {
                    if ($scope.createPeriodForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreatePeriodService.store({perPage:$scope.perPage},$scope.periodToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.periods = data.periods;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (periodToEdit, currentPage,perPage) {
        console.log(periodToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/period/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, FinancialYearsService, UpdatePeriodService,AllPeriodGroupService) {

                AllPeriodGroupService.query(function (data) {
                    $scope.periodGroups = data;
                });

                FinancialYearsService.query(function (data) {
                    $scope.financialYears = data;
                    $scope.periodToEdit = angular.copy(periodToEdit);
                    //Convert date from string to date format (very important)
                    $scope.periodToEdit.start_date = new Date($scope.periodToEdit.start_date);
                    $scope.periodToEdit.end_date = new Date($scope.periodToEdit.end_date);
                });

                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    UpdatePeriodService.update({page: currentPage,perPage:perPage},$scope.periodToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.periods = data.periods;
                $scope.currentPage = $scope.periods.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Delete Financial Year!',
            'Are sure you want to delete this?')
            .then(function () {
                    console.log("YES");
                    DeletePeriodService.delete({period_id: id,page:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.periods = data.periods;
                            $scope.currentPage = $scope.periods.current_page;
                        }, function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    };

    $scope.togglePeriod = function (period_id, period_status,perPage) {
        $scope.periodToActivate = {};
        $scope.periodToActivate.id = period_id;
        $scope.periodToActivate.is_active = period_status;
        TogglePeriodService.togglePeriod({perPage:perPage},$scope.periodToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/period/trash.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, TrashedPeriodService,
                                  RestorePeriodService, EmptyPeriodTrashService, PermanentDeletePeriodService) {
                TrashedPeriodService.query(function (data) {
                    $scope.trashedPeriods = data;
                });
                $scope.restorePeriod = function (id) {
                    RestorePeriodService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedPeriods = data.trashedPeriods;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeletePeriodService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedPeriods = data.trashedPeriods;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyPeriodTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedPeriods = data.trashedPeriods;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.periods = data.periods;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

PeriodController.resolve = {
    AllPeriodModel: function (GetAllPeriodService, $q,$timeout) {
        var deferred = $q.defer();
        $timeout(function () {
            GetAllPeriodService.get({page: 1,perPage:10}, function (data) {
                deferred.resolve(data);
            });
        },100);

        return deferred.promise;
    }
};
