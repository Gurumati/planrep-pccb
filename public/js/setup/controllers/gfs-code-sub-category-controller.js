function GfsCodeSubCategoryController($scope, GfsCodeSubCategoryModel,GfsCodeSubCategoryService,Upload,$timeout, $uibModal,
                                      ConfirmDialogService, DeleteGfsCodeSubCategoryService,
                                PaginatedGfsCodeSubCategoryService,AllGfsCodeCategoryService) {

    $scope.gfsCodeSubCategories = GfsCodeSubCategoryModel;
    $scope.title = "GFS_CODE_SUB_CATEGORIES";

    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.perPage = 10;
    $scope.pageChanged = function () {
        GfsCodeSubCategoryService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (response) {
            $scope.gfsCodeSubCategories = data;
        });
    };

    AllGfsCodeCategoryService.query(function (data) {
        $scope.gfsCodeCategories = data;
    });

    $scope.showButtons = true;
    $scope.showUploadForm = false;

    $scope.toggleUploadForm = function (state) {
        if(state===true){
            $scope.showButtons = false;
        } else{
            $scope.showButtons = true;
        }
        $scope.showUploadForm = state;
    };

    $scope.uploadFile = function (file) {
        file.upload = Upload.upload({
            url: '/api/gfsCodeSubCategories/upload',
            method: 'POST',
            data: {gfs_code_category_id:$scope.gfs_code_category_id,file: file},
        });

        file.upload.then(function (response) {
            $timeout(function () {
                file.result = response.data;
                $scope.gfsCodeSubCategories = response.data.data;
                $scope.toggleUploadForm(false);
                $scope.successMessage = response.data.message;
            });
        }, function (response) {
            $scope.errorMessage = response.message;
            $scope.gfsCodeSubCategories = response.data.data;
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/gfs_code_sub_category/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateGfsCodeSubCategoryService,AllGfsCodeCategoryService) {

                AllGfsCodeCategoryService.query(function (data) {
                    $scope.gfsCodeCategories = data;
                });
                
                $scope.gfsCodeSubCategoryToCreate = {};

                $scope.store = function () {
                    if ($scope.createGfsCodeSubCategoryForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateGfsCodeSubCategoryService.store({perPage: $scope.perPage}, $scope.gfsCodeSubCategoryToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (response) {
                //Service to create new financial year
                $scope.successMessage = response.message;
                $scope.gfsCodeSubCategories = response.data;
                $scope.currentPage = $scope.gfsCodeSubCategories.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (gfsCodeSubCategoryToEdit, currentPage, perPage) {
        let modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/gfs_code_sub_category/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllSectorService,AllLinkLevelService,AllGfsCodeCategoryService, UpdateGfsCodeSubCategoryService) {
                AllGfsCodeCategoryService.query(function (data) {
                    $scope.gfsCodeCategories = data;
                    $scope.gfsCodeSubCategoryToEdit = angular.copy(gfsCodeSubCategoryToEdit);
                });
                $scope.update = function () {
                    UpdateGfsCodeSubCategoryService.update({page: currentPage, perPage: perPage}, $scope.gfsCodeSubCategoryToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (response) {
                //Service to create new financial year
                $scope.successMessage = response.message;
                $scope.gfsCodeSubCategories = response.data;
                $scope.currentPage = $scope.gfsCodeSubCategories.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteGfsCodeSubCategoryService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.message;
                        $scope.gfsCodeSubCategories = data.data;
                        $scope.currentPage = $scope.gfsCodeSubCategories.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };

    $scope.toggleProcurement = function (id, isProcurement, perPage) {
        $scope.toggleData = {};
        $scope.toggleData.id = id;
        $scope.toggleData.is_procurement = isProcurement;
        $scope.toggleData.perPage = $scope.perPage;
        GfsCodeSubCategoryService.toggleProcurement($scope.toggleData,
            function (response) {
                $scope.successMessage = response.message;
            });
    };
}

GfsCodeSubCategoryController.resolve = {
    GfsCodeSubCategoryModel: function (GfsCodeSubCategoryService, $q) {
        var deferred = $q.defer();
        GfsCodeSubCategoryService.paginated({page: 1, perPage: 10}, function (response) {
            deferred.resolve(response.data);
        });
        return deferred.promise;
    }
};