function GfsCodeCategoryController($scope, AllGfsCodeCategoryModel, $uibModal,
                               ConfirmDialogService,Upload,$timeout, PaginatedProcurementTypeService,
                               DeleteGfsCodeCategoryService,GetAllGfsCodeCategoryService) {

    $scope.gfsCodeCategories = AllGfsCodeCategoryModel;
    $scope.title = "TITLE_GFS_CODE_CATEGORY";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        GetAllGfsCodeCategoryService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
          $scope.gfsCodeCategories = data;
          console.log($scope.gfsCodeCategories);
        });
      
      // Govella
      PaginatedProcurementTypeService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
          $scope.procurement_types = data;
          console.log($scope.procurement_types);
        });
    };

    $scope.uploadFile = function (file) {
        file.upload = Upload.upload({
            url: '/json/gfs_code_categories/upload',
            method: 'POST',
            data: {file: file},
        });

        file.upload.then(function (response) {
            $timeout(function () {
                file.result = response.data;
                $scope.gfsCodeCategories = response.data.gfsCodeCategories;
                $scope.successMessage = response.data.successMessage;
            });
        }, function (response) {
            if (response.status > 0){
                $scope.errorMsg = response.status + ': ' + response.data;
                $scope.errorMessage = response.data.errorMessage;
                $scope.successMessage = response.data.successMessage;
            }
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/gfs_code_category/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateGfsCodeCategoryService) {
                $scope.gfsCodeCategoryToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createGfsCodeCategoryForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateGfsCodeCategoryService.store({perPage:$scope.perPage},$scope.gfsCodeCategoryToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.gfsCodeCategories = data.gfsCodeCategories;
                $scope.currentPage = $scope.gfsCodeCategories.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (gfsCodeCategoryToEdit,currentPage,perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/gfs_code_category/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateGfsCodeCategoryService) {
                $scope.gfsCodeCategoryToEdit = angular.copy(gfsCodeCategoryToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updateGfsCodeCategoryForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateGfsCodeCategoryService.update({page: currentPage,perPage:perPage},$scope.gfsCodeCategoryToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.gfsCodeCategories = data.gfsCodeCategories;
                $scope.currentPage = $scope.gfsCodeCategories.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog('TITLE_CONFIRM_DELETE_GFS_CODE_CATEGORY', 'CONFIRM_DELETE').then(function () {
                    DeleteGfsCodeCategoryService.delete({gfs_code_category_id: id,currentPage:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.gfsCodeCategories = data.gfsCodeCategories;
                            $scope.currentPage = $scope.gfsCodeCategories.current_page;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }
}

GfsCodeCategoryController.resolve = {
    AllGfsCodeCategoryModel: function (GetAllGfsCodeCategoryService, $q) {
        var deferred = $q.defer();
        GetAllGfsCodeCategoryService.get({page:1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};