function LgaLevelController($scope, LgaLevelModel, $uibModal,
                               ConfirmDialogService, DeleteLgaLevelService, PaginatedLgaLevelService) {

    $scope.lgaLevels = LgaLevelModel;
    $scope.title = "PISC_LEVELS";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedLgaLevelService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
            $scope.lgaLevels = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/lga_level/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateLgaLevelService) {
                $scope.lgaLevelToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createLgaLevelForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateLgaLevelService.store({perPage:$scope.perPage},$scope.lgaLevelToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.lgaLevels = data.lgaLevels;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (lgaLevelToEdit, currentPage,perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/lga_level/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateLgaLevelService) {
                $scope.lgaLevelToEdit = angular.copy(lgaLevelToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updateLgaLevelForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateLgaLevelService.update({page: currentPage,perPage:perPage}, $scope.lgaLevelToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.lgaLevels = data.lgaLevels;
                $scope.currentPage = $scope.lgaLevels.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog('TITLE_CONFIRM_DELETE', 'CONFIRM_DELETE').then(function () {
                    DeleteLgaLevelService.delete({lga_level_id: id,page:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.lgaLevels = data.lgaLevels;
                            $scope.currentPage = $scope.lgaLevels.current_page;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {

                });
    };
    
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/lga_level/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedLgaLevelService,
                                  RestoreLgaLevelService, EmptyLgaLevelTrashService, PermanentDeleteLgaLevelService) {
                TrashedLgaLevelService.query(function (data) {
                    $scope.trashedLgaLevels = data;
                });
                $scope.restoreLgaLevel = function (id) {
                    RestoreLgaLevelService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedLgaLevels = data.trashedLgaLevels;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteLgaLevelService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedLgaLevels = data.trashedLgaLevels;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyLgaLevelTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedLgaLevels = data.trashedLgaLevels;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.lgaLevels = data.lgaLevels;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
}

LgaLevelController.resolve = {
    LgaLevelModel: function (PaginatedLgaLevelService, $q) {
        var deferred = $q.defer();
        PaginatedLgaLevelService.get({page: 1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};