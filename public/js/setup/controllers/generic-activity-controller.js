function GenericActivityController($scope, $uibModal, ConfirmDialogService, GenericActivityService,PlanningMatrixService) {

    $scope.title = "GENERIC_ACTIVITIES";

    PlanningMatrixService.all(function(data){
        $scope.planningMatrices = data.planningMatrices;
    });

    $scope.pageChanged = function(){
        if($scope.planningMatrixId === undefined){
            return;
        }
        GenericActivityService.paginate({
            page: $scope.currentPage,
            perPage: $scope.perPage,
            planningMatrixId : $scope.planningMatrixId
        }, function (data) {
            $scope.items = data;
        },function (error) {
            $scope.items = [];
        });
    };

    $scope.createOrUpdate = function (genericActivity) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/generic_activity/create.html',
            backdrop: false,
            resolve:{
                planningMatrixId: $scope.planningMatrixId
            },
            controller: function ($scope, $uibModalInstance, planningMatrixId, GenericActivityService, GenericTargetService,GlobalService,PriorityArea) {

                $scope.genericActivity = genericActivity;

                GenericTargetService.byPlanningMatrix({
                    planningMatrixId: planningMatrixId
                }, function(data){
                    $scope.genericTargets = data.genericTargets;
                });

                PriorityArea.getCurrentVersion(function (data) {
                    $scope.priorityAreas = data.priorityAreas;
                });

                $scope.loadInterventions = function () {
                    if($scope.genericActivity.priority_area_id === undefined){
                        return;
                    }
                    GlobalService.priorityInterventions({priority_area_id: $scope.genericActivity.priority_area_id}, 
                    function (data) {
                        $scope.interventions = data.interventions;
                        $scope.genericSectorProblems = data.genericSectorProblems;
                        $scope.nationalTargets = data.nationalTargets;
                    });
                };
                $scope.loadInterventions();
                $scope.createOrUpdate = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    GenericActivityService.createOrUpdate({}, $scope.genericActivity,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.pageChanged();
            },
            function () {
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                GenericActivityService.delete({
                        id: id
                    },
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.pageChanged();
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };

}