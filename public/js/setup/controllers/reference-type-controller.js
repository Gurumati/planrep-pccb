function ReferenceTypeController($scope, FinancialYearModel, FinancialYearService, ReferenceTypeService, AllFinancialYearService, $uibModal, ConfirmDialogService, DeleteReferenceTypeService, ReferencesService) {

    $scope.financialYears = FinancialYearModel;
    $scope.title = "TITLE_REFERENCE_TYPES";

    $scope.loadVersions = function (financialYearId) {
        $scope.financialYearId = financialYearId;

        FinancialYearService.versions({id: financialYearId, type: 'NATIONAL_REFERENCE_TYPE'}, function (response) {
            $scope.versions = response.data;

            $scope.loadReferenceTypes = function (versionId) {
                ReferenceTypeService.query({versionId: versionId}, function (response) {
                    $scope.referenceTypes = response;
                    $scope.references = [];
                    $scope.linkLevels = [{"id": 1, "name": "TARGETS"}, {"id": 2, "name": "ACTIVITIES"}];

                    $scope.create = function (linkLevels) {
                        let modalInstance = $uibModal.open({
                            templateUrl: '/pages/setup/reference_type/create.html',
                            backdrop: false,
                            controller: function ($scope, $uibModalInstance, ReferenceTypeService, CreateReferenceTypeService, SectorsService) {

                                $scope.referenceTypeToCreate = {sectors: []};
                                $scope.linkLevels = linkLevels;
                                SectorsService.query({}, function (data) {
                                    $scope.sectors = data;
                                });
                                $scope.store = function () {
                                    if ($scope.createReferenceTypeForm.$invalid) {
                                        $scope.formHasErrors = true;
                                        return;
                                    }

                                    ReferenceTypeService.save({versionId: versionId}, $scope.referenceTypeToCreate,
                                        function (data) {
                                            $uibModalInstance.close(data);
                                        },
                                        function (error) {
                                            console.log(error);
                                            $scope.errorMessage = error.data.errorMessage;
                                        }
                                    );

                                };
                                $scope.close = function () {
                                    $uibModalInstance.dismiss('cancel');
                                };
                                $scope.toggleSelection = function toggleSelection(s) {
                                    var idx = $scope.referenceTypeToCreate.sectors.indexOf(s.id);

                                    // Is currently selected
                                    if (idx > -1) {
                                        $scope.referenceTypeToCreate.sectors.splice(idx, 1);
                                    }

                                    // Is newly selected
                                    else {
                                        $scope.referenceTypeToCreate.sectors.push(s.id);
                                    }
                                };

                                $scope.selectAll = function toggleSelection(s) {
                                    $scope.referenceTypeToCreate.sectors = [];
                                    angular.forEach($scope.sectors, function (value, index) {
                                        $scope.referenceTypeToCreate.sectors.push(value.id);
                                    });
                                };

                                $scope.unSelectAll = function toggleSelection(s) {
                                    $scope.referenceTypeToCreate.sectors = [];
                                };
                            }
                        });
                        modalInstance.result.then(function (data) {
                                $scope.successMessage = data.successMessage;
                                $scope.referenceTypes = data.referenceTypes;
                            },
                            function () {
                                console.log('Modal dismissed at: ' + new Date());
                            });

                    };

                    $scope.copy = function (financialYear, version) {
                        let modalInstance = $uibModal.open({
                            templateUrl: '/pages/setup/reference_type/copy.html',
                            backdrop: false,
                            controller: function ($scope, $uibModalInstance, AllFinancialYearService, ReferenceTypeService, FinancialYearService) {

                                $scope.sourceFinancialYear = financialYear;
                                $scope.sourceVersion = version;

                                AllFinancialYearService.query(function (data) {
                                    $scope.financialYears = data;

                                    $scope.loadFinancialYearVersions = function (financialYearId) {
                                        FinancialYearService.otherVersions({
                                            financialYearId: financialYearId,
                                            currentVersionId: versionId,
                                            type: 'NATIONAL_REFERENCE_TYPE',
                                            currentFinancialYearId: $scope.sourceFinancialYear.id,
                                        }, function (response) {
                                            $scope.financialYearVersions = response.data;
                                            $scope.copyReferences = function (sourceFinancialYearId, sourceVersionId, destinationVersionId) {
                                                ReferenceTypeService.copyReferences({
                                                    sourceVersionId: sourceVersionId,
                                                    destinationVersionId: destinationVersionId,
                                                    sourceFinancialYearId: sourceFinancialYearId
                                                }, function (response) {
                                                    $uibModalInstance.close(response);
                                                });
                                            };
                                        });
                                    };
                                });

                                $scope.close = function () {
                                    $uibModalInstance.dismiss('cancel');
                                };
                            }
                        });
                        //Called when modal is close by cancel or to store data
                        modalInstance.result.then(function (response) {
                                $scope.successMessage = response.message;
                            },
                            function () {

                            });

                    };

                    $scope.edit = function (referenceTypeToEdit, linkLevels) {
                        var modalInstance = $uibModal.open({
                            templateUrl: '/pages/setup/reference_type/edit.html',
                            backdrop: false,
                            controller: function ($scope, $q, $uibModalInstance, ReferenceTypeService, UpdateReferenceTypeService, SectorsService) {
                                $scope.referenceTypeToEdit = angular.copy(referenceTypeToEdit);
                                $scope.referenceTypeToEdit.sectors = [];
                                $scope.linkLevels = linkLevels;
                                angular.forEach($scope.referenceTypeToEdit.reference_type_sectors, function (sector) {
                                    $scope.referenceTypeToEdit.sectors.push(sector.id);
                                });

                                SectorsService.query({}, function (data) {
                                    $scope.sectors = data;
                                });
                                $scope.update = function () {
                                    if ($scope.updateReferenceTypeForm.$invalid) {
                                        $scope.formHasErrors = true;
                                        return;
                                    }
                                    ReferenceTypeService.update({versionId: versionId}, $scope.referenceTypeToEdit,
                                        function (data) {
                                            $scope.referenceTypeToEdit = undefined;
                                            $uibModalInstance.close(data);
                                        },
                                        function (error) {
                                            $scope.errorMessage = error.data.errorMessage;
                                        }
                                    );


                                };
                                $scope.close = function () {
                                    $uibModalInstance.dismiss('cancel');
                                };
                                $scope.toggleSelection = function toggleSelection(s) {
                                    var idx = $scope.referenceTypeToEdit.sectors.indexOf(s.id);

                                    // Is currently selected
                                    if (idx > -1) {
                                        $scope.referenceTypeToEdit.sectors.splice(idx, 1);
                                    }

                                    // Is newly selected
                                    else {
                                        $scope.referenceTypeToEdit.sectors.push(s.id);
                                    }
                                };
                                $scope.selectAll = function toggleSelection(s) {
                                    $scope.referenceTypeToEdit.sectors = [];
                                    angular.forEach($scope.sectors, function (value, index) {
                                        $scope.referenceTypeToEdit.sectors.push(value.id);
                                    });
                                };
                                $scope.unSelectAll = function toggleSelection(s) {
                                    $scope.referenceTypeToEdit.sectors = [];
                                };
                            }
                        });
                        modalInstance.result.then(function (data) {
                                $scope.successMessage = data.successMessage;
                                $scope.referenceTypes = data.referenceTypes;
                            },
                            function () {
                                console.log('Modal dismissed at: ' + new Date());
                                clearTree();
                            });
                    };

                    $scope.delete = function (id) {
                        ConfirmDialogService.showConfirmDialog(
                            'TITLE_CONFIRM_DELETE_REFERENCE_TYPE',
                            'CONFIRM_DELETE')
                            .then(function () {
                                    ReferenceTypeService.delete({id: id, versionId: versionId},
                                        function (data) {
                                            $scope.successMessage = data.successMessage;
                                            $scope.referenceTypes = data.referenceTypes;
                                        }, function (error) {
                                            $scope.errorMessage = error.data.errorMessage;
                                        }
                                    );
                                },
                                function () {

                                });
                    };

                    $scope.setSelectedReference = function (gen) {
                        $scope.selectedReference = gen;
                        ReferencesService.query({referenceTypeId: gen.id}, function (data) {
                            $scope.references = data;
                        }, function (error) {
                            console.log(error);
                        });
                    };

                    $scope.createReference = function (selectedReferenceType) {
                        let modalInstance = $uibModal.open({
                            templateUrl: '/pages/setup/reference_type/create_reference.html',
                            backdrop: false,
                            controller: function ($scope, $uibModalInstance, CreateReferenceService, ParentReferences) {
                                $scope.lineItemToCreate = {};
                                ParentReferences.query({
                                    referenceTypeId: selectedReferenceType.id,
                                    referenceId: 0
                                }, function (data) {
                                    $scope.parentReferences = data;
                                });

                                $scope.linkLevels = ["TARGET", "ACTIVITY"];

                                $scope.lineItemToCreate.reference_type_id = selectedReferenceType.id;
                                $scope.store = function () {
                                    if ($scope.createLineItemForm.$invalid) {
                                        $scope.formHasErrors = true;
                                        return;
                                    }

                                    CreateReferenceService.store($scope.lineItemToCreate,
                                        function (data) {
                                            $uibModalInstance.close(data);
                                        },
                                        function (error) {
                                            console.log(error);
                                            $scope.errorMessage = error.data.errorMessage;
                                        }
                                    );

                                }
                                //Function to close modal when cancel button clicked
                                $scope.close = function () {
                                    $uibModalInstance.dismiss('cancel');
                                }
                            }
                        });
                        //Called when modal is close by cancel or to store data
                        modalInstance.result.then(function (data) {
                                //Service to create new financial year
                                $scope.successMessage = data.successMessage;
                                $scope.references = data.references;
                            },
                            function () {
                                //If modal is closed
                                console.log('Modal dismissed at: ' + new Date());
                            });

                    };

                    $scope.editLineItem = function (lineItemToUpdate) {
                        //Modal Form for creating Reference TYPE

                        var modalInstance = $uibModal.open({
                            templateUrl: '/pages/setup/reference_type/edit_reference.html',
                            backdrop: false,
                            controller: function ($scope, $uibModalInstance, UpdateReferenceService, ParentReferences) {

                                ParentReferences.query({
                                    referenceTypeId: lineItemToUpdate.reference_type_id,
                                    referenceId: lineItemToUpdate.id
                                }, function (data) {
                                    $scope.parentReferences = data;
                                    $scope.lineItemToUpdate = lineItemToUpdate;
                                });
                                $scope.linkLevels = ["TARGET", "ACTIVITY"];
                                $scope.update = function () {
                                    if ($scope.updateLineItemForm.$invalid) {
                                        $scope.formHasErrors = true;
                                        return;
                                    }

                                    UpdateReferenceService.update($scope.lineItemToUpdate,
                                        function (data) {
                                            $uibModalInstance.close(data);
                                        },
                                        function (error) {
                                            console.log(error);
                                            $scope.errorMessage = error.data.errorMessage;
                                        }
                                    );

                                }
                                //Function to close modal when cancel button clicked
                                $scope.close = function () {
                                    $uibModalInstance.dismiss('cancel');
                                }
                            }
                        });
                        //Called when modal is close by cancel or to store data
                        modalInstance.result.then(function (data) {
                                //Service to create new financial year
                                $scope.successMessage = data.successMessage;
                                $scope.References = data.References;
                            },
                            function () {
                                //If modal is closed
                                console.log('Modal dismissed at: ' + new Date());
                            });

                    };
                }, function (error) {
                    console.log(error);
                });
            };
        }, function (error) {
            console.log(error);
        });
    };

    $scope.carry = function (currentFinancialYearId, item) {
        CarryVersionService.showDialog('NRT',currentFinancialYearId, item).then(function(data){
            console.log(data);
        }, function(){
            
        });
    };
}

ReferenceTypeController.resolve = {
    FinancialYearModel: function (AllFinancialYearService, $q) {
        let deferred = $q.defer();
        AllFinancialYearService.query(function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};