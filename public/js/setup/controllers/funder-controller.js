function FunderController($scope, FunderModel, $uibModal, ConfirmDialogService, DeleteFunderService,
                          ActivateFunderService, PaginateFunderService) {

    $scope.funders = FunderModel;
    $scope.title = "TITLE_FUNDERS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginateFunderService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.funders = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/funder/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateFunderService) {
                $scope.funderToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createFunderForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    /*if (!!$scope.funderToCreate.is_local.selected) {
                        $scope.funderToCreate.push('is_local', false);
                    }*/

                    CreateFunderService.store({perPage:$scope.perPage},$scope.funderToCreate,
                        function (data) {
                            console.log(data);
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.funders = data.funders;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (funderToEdit, currentPage,perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/funder/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateFunderService) {
                $scope.funderToEdit = angular.copy(funderToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updateFunderForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateFunderService.update({page: currentPage,perPage:perPage}, $scope.funderToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.funders = data.funders;
                $scope.currentPage = $scope.funders.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog(
            'TITLE_CONFIRM_DELETE_ACCESS_RIGHT',
            'CONFIRM_DELETE')
            .then(function () {
                    DeleteFunderService.delete({funderId: id,currentPage:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.funders = data.funders;
                            $scope.currentPage = $scope.funders.current_page;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    };

    $scope.activateFunder = function (funderId, funderStatus,perPage) {
        $scope.funderToActivate = {};
        $scope.funderToActivate.id = funderId;
        $scope.funderToActivate.is_active = funderStatus;
        ActivateFunderService.activate({perPage:perPage},$scope.funderToActivate,
            function (data) {
                $scope.successMessage = false;
                $scope.errorMessage = false;
                $scope.activateMessage = data.activateMessage;
            });

    }
}

FunderController.resolve = {
    FunderModel: function (PaginateFunderService, $q) {
        var deferred = $q.defer();
        PaginateFunderService.get({page:1,perPage:2}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};