function GenericTargetController($scope, $uibModal, GenericTargetService, PlanningMatrixService,ConfirmDialogService) {

    $scope.title = "GENERIC_TARGETS";

    PlanningMatrixService.all(function(data){
        $scope.planningMatrices = data.planningMatrices;
    });

    $scope.pageChanged = function (planningMatrixId) {
        if(planningMatrixId === undefined){
            return;
        }
        GenericTargetService.paginated({
            page: $scope.currentPage,
            perPage: $scope.perPage,
            planningMatrixId: planningMatrixId
        }, function (data) {
            $scope.items = data;
        });
    };

    $scope.createOrUpdate = function (genericTarget) {

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/generic_target/create.html',
            backdrop: false,
            resolve: {
                planChains: ['$q', 'LowestPlanChainsService', function($q, LowestPlanChainsService){
                    var deffered = $q.defer();
                    LowestPlanChainsService.query(function (data) {
                        deffered.resolve(data[0]);
                    });
                    return deffered.promise;
                }]
            },
            controller: function ($scope, $uibModalInstance, planChains, GenericTargetService,PerformanceIndicators,ActivityCategory) {

              ActivityCategory.query(function (data) {
                $scope.targetTypes = data;
              });

                $scope.genericTarget = genericTarget;
                $scope.planChains = planChains;

                $scope.loadPerformanceIndicators = function (plan_chain_id) {
                    PerformanceIndicators.getByPlanChain({
                        planChainId:  plan_chain_id
                    }, function (data) {
                        $scope.performanceIndicators = data.indicators;
                    });
                };
                if($scope.genericTarget.id !== undefined){
                    $scope.loadPerformanceIndicators($scope.genericTarget.plan_chain_id);
                }

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    GenericTargetService.save($scope.genericTarget,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.pageChanged($scope.planningMatrixId);
            },
            function () {
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog(
            'CONFIRM_GENERIC_TARGET_DELETE',
            'CONFIRM_DELETE')
            .then(function () {
                    GenericTargetService.delete({id: id,page:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.pageChanged($scope.planningMatrixId);
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {

                });
    };

}
