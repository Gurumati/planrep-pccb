function BankAccountController($scope, FinancialYearModel, FinancialYearService, $uibModal,
                                ConfirmDialogService, BankAccountService) {

    $scope.financialYears = FinancialYearModel;
    $scope.title = "BANK_ACCOUNTS";

    $scope.loadVersions = function (financialYearId) {

        $scope.financialYearId = financialYearId;

        FinancialYearService.versions({id: financialYearId, type: 'BANK_ACCOUNT'}, function (response) {
            $scope.versions = response.data;
            $scope.loadBankAccounts = function (versionId) {
                $scope.versionId = versionId;
                $scope.currentPage = 1;
                $scope.perPage = 10;
                $scope.maxSize = 3;

                BankAccountService.paginated({
                    page: 1,
                    perPage: 10,
                    financialYearId: financialYearId,
                    versionId: versionId,
                }, function (response) {
                    $scope.items = response.data;
                });

                $scope.pageChanged = function () {
                    BankAccountService.paginated({
                        page: $scope.currentPage,
                        perPage: $scope.perPage,
                        financialYearId: financialYearId,
                        versionId: versionId,
                    }, function (response) {
                        $scope.items = response.data;
                    });
                };

                $scope.create = function (financialYearId, versionId) {
                    let modalInstance = $uibModal.open({
                        templateUrl: '/pages/setup/bank_account/create.html',
                        backdrop: false,
                        controller: function ($scope, $uibModalInstance) {
                            $scope.createDataModel = {};
                            $scope.store = function () {
                                if ($scope.createForm.$invalid) {
                                    $scope.formHasErrors = true;
                                    return;
                                }
                                BankAccountService.save({
                                        perPage: $scope.perPage,
                                        financialYearId: financialYearId,
                                        versionId: versionId
                                    }, $scope.createDataModel,
                                    function (data) {
                                        $uibModalInstance.close(data);
                                    },
                                    function (error) {
                                        $scope.errorMessage = error.data.errorMessage;
                                        $scope.errors = error.data.errors;
                                    }
                                );
                            };
                            $scope.close = function () {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    });
                    modalInstance.result.then(function (response) {
                            $scope.successMessage = response.message;
                            $scope.items = response.data;
                            $scope.currentPage = $scope.items.current_page;
                        },
                        function () {
                            console.log('Modal dismissed at: ' + new Date());
                        });

                };

                $scope.edit = function (updateDataModel, financialYearId, versionId, currentPage, perPage) {
                    let modalInstance = $uibModal.open({
                        templateUrl: '/pages/setup/bank_account/edit.html',
                        backdrop: false,
                        controller: function ($scope, $uibModalInstance) {
                            $scope.updateDataModel = angular.copy(updateDataModel);

                            $scope.update = function () {

                                if ($scope.updateForm.$invalid) {
                                    $scope.formHasErrors = true;
                                    return;
                                }

                                BankAccountService.update({
                                        id:updateDataModel.id,
                                        page: currentPage,
                                        perPage: perPage,
                                        financialYearId: financialYearId,
                                        versionId: versionId,
                                    }, $scope.updateDataModel,
                                    function (data) {
                                        $uibModalInstance.close(data);
                                    },
                                    function (error) {
                                        $scope.errorMessage = error.data.errorMessage;
                                        $scope.errors = error.data.errors;
                                    }
                                );
                            };
                            $scope.close = function () {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    });
                    modalInstance.result.then(function (response) {
                            $scope.successMessage = response.message;
                            $scope.items = response.data;
                            $scope.currentPage = response.data.current_page;
                        },
                        function () {
                            console.log('Modal dismissed at: ' + new Date());
                        });
                };

                $scope.copy = function (financialYear, version) {
                    let modalInstance = $uibModal.open({
                        templateUrl: '/pages/setup/bank_account/copy.html',
                        backdrop: false,
                        controller: function ($scope, $uibModalInstance, AllFinancialYearService, BankAccountService, PriorityAreaService, FinancialYearService) {

                            $scope.sourceFinancialYear = financialYear;
                            $scope.sourceVersion = version;

                            AllFinancialYearService.query(function (data) {
                                $scope.financialYears = data;
                                $scope.loadFinancialYearVersions = function (financialYearId) {
                                    FinancialYearService.otherVersions({
                                        financialYearId: financialYearId,
                                        currentVersionId: versionId,
                                        type: 'BANK_ACCOUNT',
                                        currentFinancialYearId: $scope.sourceFinancialYear.id,
                                    }, function (response) {
                                        $scope.financialYearVersions = response.data;
                                        $scope.copyBankAccounts = function (sourceFinancialYearId, sourceVersionId, destinationVersionId) {
                                            BankAccountService.copyBankAccounts({
                                                sourceVersionId: sourceVersionId,
                                                destinationVersionId: destinationVersionId,
                                                sourceFinancialYearId: sourceFinancialYearId
                                            }, function (response) {
                                                $uibModalInstance.close(response);
                                            });
                                        };
                                    });
                                };
                            });

                            $scope.close = function () {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    });
                    modalInstance.result.then(function (response) {
                            $scope.successMessage = response.message;
                        },
                        function () {

                        });
                };

                $scope.delete = function (id, financialYearId, versionId, currentPage, perPage) {
                    ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                            BankAccountService.delete({
                                    id: id,
                                    page: currentPage,
                                    perPage: perPage,
                                    financialYearId: financialYearId,
                                    versionId: versionId
                                }, function (response) {
                                    $scope.successMessage = response.message;
                                    $scope.items = response.data;
                                    $scope.currentPage = $scope.items.current_page;
                                }, function (error) {
                                    $scope.errorMessage = error.data.errorMessage;
                                }
                            );
                        },
                        function () {
                            console.log("NO");
                        });
                };
            };
        });
    };
}

BankAccountController.resolve = {
    FinancialYearModel: function (AllFinancialYearService, $q) {
        let deferred = $q.defer();
        AllFinancialYearService.query(function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};