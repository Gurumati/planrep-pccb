function FundSourceController($scope, FinancialYearModel, $uibModal, $timeout, Upload, ConfirmDialogService, ExpenditureGsfCodeService,
    FundSourceService, AllFinancialYearService, FinancialYearService,CarryVersionService,
    DeleteFundSourceService, FundSourceCategoriesService) {

$scope.financialYears = FinancialYearModel;
$scope.title = "FUND_SOURCES";
$scope.btnDisabled = true;
$scope.selectedSbcVersion = null;

$scope.loadVersions = function (financialYearId) {

$scope.financialYearId = financialYearId;

FinancialYearService.versions({id: financialYearId, type: 'FUND_SOURCE'}, function (response) {
$scope.versions = response.data;

$scope.getversion = function (sbcversion) {
$scope.selectedSbcVersion = sbcversion.id;
$scope.btnDisabled = false;
}

$scope.loadFundSources = function (versionId) {
$scope.versionId = versionId;

FinancialYearService.versions({id: financialYearId, type: 'BUDGET_CLASS'}, function (response) {
$scope.sbcVersions = response.data;
});
$scope.btnDisabled = true;
FundSourceService.paginated({
page: $scope.currentPage,
perPage: $scope.perPage,
financialYearId: financialYearId,
versionId: versionId,
}, function (data) {
$scope.fundSources = data;
});

$scope.currentPage = 1;
$scope.maxSize = 3;
$scope.perPage = 10;

$scope.pageChanged = function () {
FundSourceService.paginated({
page: $scope.currentPage,
perPage: $scope.perPage,
financialYearId: financialYearId,
versionId: versionId,
searchText: $scope.searchText,
}, function (data) {
$scope.fundSources = data;
});
};

ExpenditureGsfCodeService.query(function (data) {
$scope.expenditureGfsCodes = data;
});

$scope.search = function (financialYearId, versionId, searchText) {
FundSourceService.search({
financialYearId: financialYearId,
versionId: versionId,
searchText: searchText
}, function (data) {
$scope.fundSources = data;
});
};

$scope.showList = true;

$scope.showUploadForm = false;

$scope.toggleUploadForm = function (state) {
if (state === true) {
$scope.showList = false;
} else {
$scope.showList = true;
}
$scope.showUploadForm = state;
};

FundSourceCategoriesService.query({}, function (data) {
$scope.fundSourceCategories = data;
});

$scope.uploadFile = function (financialYearId, versionId, file) {
file.upload = Upload.upload({
url: '/files/fund-sources/upload',
method: 'POST',
data: {
  fund_source_category_id: $scope.fund_source_category_id,
  financialYearId: financialYearId,
  versionId: versionId,
  file: file
}
});

file.upload.then(function (response) {
$timeout(function () {
  file.result = response.data;
  $scope.fundSources = response.data.fundSources;
  $scope.toggleUploadForm(false);
  $scope.successMessage = response.data.successMessage;
});
}, function (response) {
if (response.status > 0) {
  $scope.errorMsg = response.status + ': ' + response.data;
  $scope.errorMessage = response.data.errorMessage;
  $scope.errorData = response.data.errorData;
  $scope.errorRows = response.data.errorRows;
  $scope.fundSources = response.data.fundSources;
}
}, function (evt) {
// Math.min is to fix IE which reports 200% sometimes
file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
});
};

$scope.create = function (financialYearId, versionId, expenditureGfsCodes) {
let bcVersion = $scope.selectedSbcVersion;
let modalInstance = $uibModal.open({
templateUrl: '/pages/setup/fund_source/create.html',
backdrop: false,
resolve: {
  revenueGfsCodes: ['$q','RevenueGfsCodeService', function($q, RevenueGfsCodeService){
      var deffered = $q.defer();
      RevenueGfsCodeService.byFundSourceVersion({
          fundSourceVersionId: versionId,
          financialYearId: financialYearId
      },function(data){
          deffered.resolve(data.gfsCodes);
      },function(error){
          deffered.reject(error);
      });
      return deffered.promise;
  }]
},
bcVersion: function(){
  return bcVersion;
},
financialYearId: function(){
  return financialYearId;
},
controller: function ($scope,$uibModalInstance, revenueGfsCodes, BudgetClassService, FinancialYearService, AllGfsCodeService, SectorsService, FundSourceCeilingsService, AddFundSourceCeilingSectorsService,
                    CreateFundSourceService, AllFinancialYearService, SubBudgetClassesService, FundSourceCategoriesService, LoadParentFundSourcesService, AllFundTypeService) {
  $scope.newForm = true;
  $scope.revenueGfsCodes = revenueGfsCodes;
  $scope.expenditureGfsCodes = expenditureGfsCodes;
  $scope.gfsToAdd = {};
  SectorsService.query({}, function (data) {
      $scope.sectors = data;
  });
  AllFundTypeService.query(function (data) {
      $scope.fundTypes = data;
  });
  FundSourceCategoriesService.query({}, function (data) {
      $scope.fundSourceCategories = data;
  });
  $scope.expandCeiling = function (id, showSector, showGfs) {
      $scope.expandedCeiling = id;
      $scope.showSector = showSector;
      $scope.showGfs = showGfs;
  };
  $scope.fundSourceToCreate = {
      subBudget_classes: [],
      gfsCodes: [],
      allocation_subBudget_classes: []
  };
  $scope.toggleSectors = function toggleSectors(ceilingSectors, s) {
      if (ceilingSectors === undefined) {
          ceilingSectors = [];
      }

      let idx = ceilingSectors.indexOf(s.id);
      if (idx > -1) {
          ceilingSectors.splice(idx, 1);
      } else {
          ceilingSectors.unshift(s.id);
      }
  };
  $scope.addExpenditureGfs = function (ceilingGfs, _gfsToAdd) {
      let gfsToAdd = _gfsToAdd;
      if (ceilingGfs === undefined) {
          ceilingGfs = [];
      }
      ceilingGfs.unshift(angular.copy(gfsToAdd));
      $scope.expenditureGfsToAdd = undefined;
      gfsToAdd = undefined;
  };
  $scope.removeExpenditureGfs = function (ceilingGfs, indexToRemove) {
      ceilingGfs.splice(indexToRemove, 1);
  };
  $scope.toggleForm = function (val) {
      $scope.newForm = val;
  };
  $scope.filterAllocationBudgetClasses = function (subBudgetClasses) {
      $scope.allocationSubBudgetClasses = $.grep($scope.subBudgetClasses, function (b) {
          return $.inArray(b.id, subBudgetClasses) === -1;
      });
  };


  $scope.toggleBudgetClassSelection = function (subBudgetClasses, sbc) {
      if (subBudgetClasses === undefined) {
          subBudgetClasses = [];
      }
      let idx = subBudgetClasses.indexOf(sbc.id);
      // Is currently selected
      if (idx > -1) {
          subBudgetClasses.splice(idx, 1);
      }
      // Is newly selected
      else {
          subBudgetClasses.unshift(sbc.id);
      }
  };
  $scope.toggleAllocationBudgetClassSelection = function (allocationSubBudgetClasses, sbc) {
      if (allocationSubBudgetClasses === undefined) {
          allocationSubBudgetClasses = [];
      }
      let idx = allocationSubBudgetClasses.indexOf(sbc.id);
      // Is currently selected
      if (idx > -1) {
          allocationSubBudgetClasses.splice(idx, 1);
      }
      // Is newly selected
      else {
          allocationSubBudgetClasses.unshift(sbc.id);
      }
  };
  $scope.loadParentFundSourceCategories = function (fundSourceCategoryId) {
      LoadParentFundSourcesService.query({childFundSourceCategoryId: fundSourceCategoryId},
          function (data) {
              $scope.fundSourceCategoryParents = data;
          },
          function (error) {
          }
      );
  };
  BudgetClassService.subBudgetClasses({versionId:bcVersion,financialYearId:financialYearId},function (response) {
      $scope.subBudgetClasses = response.data;
      $scope.filterAllocationBudgetClasses([]);
  }, function (error) {
      console.log(error);
  });

  function updateGfsToDisplay(gfsCodesArray) {
      console.log($scope.revenueGfsCodes);
      let toExclude = _.pluck(gfsCodesArray, 'code');
      $scope.gfsToDisplay = $.grep($scope.revenueGfsCodes, function (gfsObject) {
          return $.inArray(gfsObject.code, toExclude) === -1;
      });
  }

  updateGfsToDisplay([]);
  $scope.addGfsCode = function (gfsToAdd) {
      $scope.fundSourceToCreate.gfsCodes.unshift(gfsToAdd);
      updateGfsToDisplay($scope.fundSourceToCreate.gfsCodes);
  };

  $scope.removeGfs = function (index) {
      $scope.fundSourceToCreate.gfsCodes.splice(index, 1);
      updateGfsToDisplay($scope.fundSourceToCreate.gfsCodes);
  };

  $scope.store = function () {
      if ($scope.createFundSourceForm.$invalid) {
          $scope.formHasErrors = true;
          console.log("form ha error");
          return;
      }

      CreateFundSourceService.store({
              financialYearId: financialYearId,
              versionId: versionId,
              bcVersion:bcVersion,
              page: $scope.currentPage,
              perPage: $scope.perPage,
          }, $scope.fundSourceToCreate,
          function (data) {
              if (data.fundSourceCeilings.length === 0) {
                  $uibModalInstance.close(data);
              }
              $scope.newForm = false;
              $scope.fundSourceCeilings = data.fundSourceCeilings;
              $scope.fundSourceId = data.fundSourceId;
              $scope.successMessage = data.successMessage;
              $scope.fundSources = data.fundSources;
          },
          function (error) {
              $scope.errorMessage = error.data.errorMessage;
              $scope.errors = error.data.errors;
          }
      );

  };

  $scope.store_sectors = function () {
      var data = {ceilings: $scope.fundSourceCeilings, fund_source_id: $scope.fundSourceId};
      if ($scope.linkSectorForm.$invalid) {
          $scope.formHasErrors = true;
          return;
      }
      AddFundSourceCeilingSectorsService.store_sectors(data,
          function (data) {
              $uibModalInstance.close(data);
          },
          function (error) {
              $scope.errorMessage = error.data.errorMessage;
          }
      );

  };
  $scope.close = function () {
      $uibModalInstance.dismiss('cancel');
  };
}
});
modalInstance.result.then(function (data) {
  $scope.successMessage = data.successMessage;
  $scope.pageChanged();

},
function () {
  console.log('Modal dismissed at: ' + new Date());
});
};

$scope.edit = function (financialYearId, versionId, fundSourceToEdit, expenditureGfsCodes, currentPage, perPage) {
let bcVersion = $scope.selectedSbcVersion;
let modalInstance = $uibModal.open({
templateUrl: '/pages/setup/fund_source/edit.html',
backdrop: false,
resolve: {
  revenueGfsCodes: ['$q','RevenueGfsCodeService', function($q, RevenueGfsCodeService){
      var deffered = $q.defer();
      RevenueGfsCodeService.byFundSourceVersion({
          fundSourceVersionId: versionId,
          financialYearId: financialYearId
      },function(data){
          deffered.resolve(data.gfsCodes);
      }, function(error){
          deffered.reject(error);
      });
      return deffered.promise;
  }]
},
bcVersion: function(){
  return bcVersion;
},
financialYearId: function(){
  return financialYearId;
},
controller: function ($scope, $uibModalInstance, revenueGfsCodes, BudgetClassService, AllGfsCodeService, SectorsService, FundSourceCeilingsService, AddFundSourceCeilingSectorsService,
                    UpdateFundSourceService, AllFinancialYearService, SubBudgetClassesService, FundSourceCategoriesService, LoadParentFundSourcesService) {

                      $scope.newForm = true;
  BudgetClassService.subBudgetClasses({versionId:bcVersion,financialYearId:financialYearId},function (response) {
      $scope.subBudgetClasses = response.data;
      
      $scope.fundSourceToEdit = fundSourceToEdit;
      fundSourceToEdit.subBudget_classes = [];
      fundSourceToEdit.allocation_subBudget_classes = [];
      angular.forEach(fundSourceToEdit.fund_source_budget_classes, function (fb) {
          if (!fundSourceToEdit.can_project || (fundSourceToEdit.can_project && fundSourceToEdit.subBudget_classes.length === 0)) {
              fundSourceToEdit.subBudget_classes.push(fb.budget_class_id);
          } else if (fundSourceToEdit.can_project && fundSourceToEdit.subBudget_classes.length >= 1) {
              fundSourceToEdit.allocation_subBudget_classes.push(fb.budget_class_id);
          }
      });
      $scope.filterAllocationBudgetClasses(fundSourceToEdit.subBudget_classes);
  });

  SectorsService.query({}, function (data) {
      $scope.sectors = data;
  });

  $scope.revenueGfsCodes = revenueGfsCodes;
  $scope.expenditureGfsCodes = expenditureGfsCodes;
  $scope.gfsToAdd = {};
  FundSourceCategoriesService.query({}, function (data) {
      $scope.fundSourceCategories = data;
  });

  $scope.expandCeiling = function (id, showSector, showGfs) {
      $scope.expandedCeiling = id;
      $scope.showSector = showSector;
      $scope.showGfs = showGfs;
  };

  $scope.toggleSectors = function toggleSectors(ceilingSectors, s) {
      if (ceilingSectors === undefined) {
          ceilingSectors = [];
      }

      var idx = ceilingSectors.indexOf(s.id);
      if (idx > -1) {
          ceilingSectors.splice(idx, 1);
      } else {
          ceilingSectors.unshift(s.id);
      }
  };
  $scope.addExpenditureGfs = function (ceilingGfs, _gfsToAdd) {
      var gfsToAdd = _gfsToAdd;
      if (ceilingGfs === undefined) {
          ceilingGfs = [];
      }
      ceilingGfs.unshift(angular.copy(gfsToAdd));
      $scope.expenditureGfsToAdd = undefined;
      gfsToAdd = undefined;
  };
  $scope.removeExpenditureGfs = function (ceilingGfs, indexToRemove) {
      ceilingGfs.splice(indexToRemove, 1);
  };
  $scope.toggleForm = function (val) {
      $scope.newForm = val;
  };
  $scope.filterAllocationBudgetClasses = function (subBudgetClasses) {
      $scope.allocationSubBudgetClasses = $.grep($scope.subBudgetClasses, function (b) {
          return $.inArray(b.id, subBudgetClasses) === -1;
      });
  };

  $scope.toggleBudgetClassSelection = function (subBudgetClasses, sbc) {
      if (subBudgetClasses === undefined) {
          subBudgetClasses = [];
      }
      var idx = subBudgetClasses.indexOf(sbc.id);
      // Is currently selected
      if (idx > -1) {
          subBudgetClasses.splice(idx, 1);
      }
      // Is newly selected
      else {
          subBudgetClasses.unshift(sbc.id);
      }
  };
  $scope.toggleAllocationBudgetClassSelection = function (allocationSubBudgetClasses, sbc) {
      if (allocationSubBudgetClasses === undefined) {
          allocationSubBudgetClasses = [];
      }
      let idx = allocationSubBudgetClasses.indexOf(sbc.id);
      if (idx > -1) {
          allocationSubBudgetClasses.splice(idx, 1);
      } else {
          allocationSubBudgetClasses.unshift(sbc.id);
      }
  };

  function updateGfsToDisplay(gfsCodesArray) {
      let toExclude = _.pluck(gfsCodesArray, 'code');
      $scope.gfsToDisplay = $.grep($scope.revenueGfsCodes, function (gfsObject) {
          return $.inArray(gfsObject.code, toExclude) === -1;
      });
  }

  updateGfsToDisplay([]);
  $scope.addGfsCode = function (gfsToAdd) {
      console.log(fundSourceToEdit);
      $scope.fundSourceToEdit.gfs_codes.unshift(gfsToAdd);
      updateGfsToDisplay($scope.fundSourceToEdit.gfs_codes);
  };

  $scope.removeGfs = function (index) {
      $scope.fundSourceToEdit.gfs_codes.splice(index, 1);
      updateGfsToDisplay($scope.fundSourceToEdit.gfs_codes);
  };
  $scope.update = function () {
      if ($scope.updateFundSourceForm.$invalid) {
          $scope.formHasErrors = true;
          return;
      }
      $scope.fundSourceToEdit.financialYearId = financialYearId;
      $scope.fundSourceToEdit.versionId = versionId;
      $scope.fundSourceToEdit.perPage = $scope.perPage;
      $scope.fundSourceToEdit.page = $scope.currentPage;
      UpdateFundSourceService.update({
              financialYearId: financialYearId,
              versionId: versionId,
              bcVersion:bcVersion,
              perPage: perPage,
              page: currentPage,
          }, $scope.fundSourceToEdit,
          function (data) {
              if (data.fundSourceCeilings.length === 0) {
                  $uibModalInstance.close(data);
              }
              $scope.newForm = false;
              $scope.fundSourceCeilings = data.fundSourceCeilings;
              $scope.fundSourceId = data.fundSourceId;
              $scope.successMessage = data.successMessage;
              $scope.fundSources = data.fundSources;

          },
          function (error) {
              $scope.errorMessage = error.data.errorMessage;
              $scope.errors = error.data.errors;
          }
      );

  };

  $scope.store_sectors = function () {
      var data = {ceilings: $scope.fundSourceCeilings, fund_source_id: $scope.fundSourceId};
      if ($scope.linkSectorForm.$invalid) {
          $scope.formHasErrors = true;
          return;
      }
      AddFundSourceCeilingSectorsService.store_sectors(data,
          function (data) {
              $uibModalInstance.close(data);
          },
          function (error) {
              $scope.errorMessage = error.data.errorMessage;
          }
      );

  };
  $scope.close = function () {
      $uibModalInstance.dismiss('cancel');
  };
}
});
modalInstance.result.then(function (data) {
  $scope.successMessage = data.successMessage;
  $scope.pageChanged();

},
function () {
  console.log('Modal dismissed at: ' + new Date());
});
};

$scope.delete = function (financialYearId, versionId, id, currentPage, perPage) {
ConfirmDialogService.showConfirmDialog(
'TITLE_CONFIRM_DELETE_FUND_SOURCE',
'CONFIRM_DELETE')
.then(function () {
      DeleteFundSourceService.delete({
              fundSourceId: id,
              financialYearId: financialYearId,
              versionId: versionId,
              page: currentPage,
              perPage: perPage,
          },
          function (data) {
              $scope.successMessage = data.successMessage;
              $scope.fundSources = data.fundSources;
              $scope.currentPage = data.fundSources.current_page;
          }, function (error) {
              $scope.errorMessage = error.data.errorMessage;
          }
      );
  },
  function () {

  });
};

$scope.copy = function (financialYear, version) {
let modalInstance = $uibModal.open({
templateUrl: '/pages/setup/fund_source/copy.html',
backdrop: false,
controller: function ($scope, $uibModalInstance, AllFinancialYearService, FundSourceService, FinancialYearService) {

  $scope.sourceFinancialYear = financialYear;
  $scope.sourceVersion = version;

  AllFinancialYearService.query(function (data) {
      $scope.financialYears = data;

      $scope.loadFinancialYearVersions = function (financialYearId) {

          FinancialYearService.otherVersions({
              financialYearId: financialYearId,
              currentVersionId: versionId,
              type: 'FUND_SOURCE',
              currentFinancialYearId: $scope.sourceFinancialYear.id,
          }, function (response) {
              $scope.financialYearVersions = response.data;
              $scope.copyFundSources = function (sourceFinancialYearId, sourceVersionId, destinationVersionId) {
                  FundSourceService.copyFundSources({
                      sourceFinancialYearId: sourceFinancialYearId,
                      sourceVersionId: sourceVersionId,
                      destinationVersionId: destinationVersionId
                  }, function (response) {
                      $uibModalInstance.close(response);
                  });
              };
          });
      };
  });

  $scope.close = function () {
      $uibModalInstance.dismiss('cancel');
  };
}
});
modalInstance.result.then(function (response) {
  $scope.successMessage = response.message;
},
function () {

});

};
};
});
};

$scope.carry = function (currentFinancialYearId, item) {
CarryVersionService.showDialog('FS',currentFinancialYearId, item).then(function(data){
}, function(){

});
};

}

FundSourceController.resolve = {
FinancialYearModel: function (AllFinancialYearService, $q) {
let deferred = $q.defer();
AllFinancialYearService.query(function (data) {
deferred.resolve(data);
});
return deferred.promise;
}
};
