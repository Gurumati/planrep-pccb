function ProjectDataFormQuestionsController($scope,
                                            QuestionsModel,
                                            $uibModal,
                                            ProjectDataFormQuestionService,
                                            UpdateProjectDataFormQuestionsService,
                                            DeleteProjectDataFormQuestionsService,
                                            ConfirmDialogService,
                                            AllProjectDataFormContentsService,
                                            ProjectDataFormsService,
                                            ProjectDataFormQuestionsService
) {

    $scope.questions = QuestionsModel;
    $scope.title = "PROJECT_DATA_FORM_QUESTIONS";
    $scope.maxSize = 3;


    $scope.pageChanged = function () {
        ProjectDataFormQuestionsService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
            $scope.units = data;
        }, function (error) {
            console.log(error);
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/project_data_form_questions/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,CreateProjectDataFormQuestionsService) {

                $scope.selectOptionsToCreate = {};
                $scope.selectOptionsToCreate.selectOptions = [];
                $scope.selectOption = {};

                ProjectDataFormsService.query({},function (data) {
                    $scope.project_data_forms = data;
                });

                $scope.load_contents = function(project_data_form_id){
                    AllProjectDataFormContentsService.query({form_id: project_data_form_id},function (data) {
                        $scope.projectDataFormContents = data;
                    }, function(error){
                        console.log(error);
                    });
                };


                $scope.questionToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createQuestionForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }


                    $scope.questionToCreate.select_options = $scope.selectOptionsToCreate.selectOptions;
                  CreateProjectDataFormQuestionsService.store({perPage:$scope.perPage},$scope.questionToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        }, function (error) {
                            $scope.errorMessage=error.data.errorMessage;
                        }
                    );
                }

                $scope.addSelectOption = function(options){
                    $scope.selectOptionsToCreate.selectOptions.push(options);
                    $scope.selectOption = {};
                };

                $scope.removeSelectOption = function(index){
                    $scope.selectOptionsToCreate.selectOptions.splice(index,1);
                };


                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage=data.successMessage;
                $scope.questions=data.questions;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.edit = function (questionToEdit) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/project_data_form_questions/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {
                $scope.selectOption ={};
                $scope.questionToEdit = angular.copy(questionToEdit);
                $scope.questionToEdit = JSON.parse(JSON.stringify($scope.questionToEdit).split('"option_value":').join('"value":'));

                ProjectDataFormsService.query({},function (data) {
                    $scope.project_data_forms = data;
                }, function (error){
                    console.log(error);
                });

                $scope.load_contents = function(project_data_form_id){
                    AllProjectDataFormContentsService.query({form_id: project_data_form_id},function (data) {
                        $scope.projectDataFormContents = data;
                    }, function (error) {
                        console.log(error);
                    });
                };

                $scope.addSelectOption = function(options){
                    $scope.questionToEdit.select_options.push(options);
                    $scope.selectOption = {};
                };

                $scope.removeSelectOption = function(index){
                    $scope.questionToEdit.select_options.splice(index,1);
                };

                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    $uibModalInstance.close($scope.questionToEdit);
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        var questionToEdit2 = $scope.questionToEdit;
        modalInstance.result.then(function (questionToEdit2) {
                //Service to update the question
                UpdateProjectDataFormQuestionsService.update({},questionToEdit2,
                    function (data) {
                        //Successful function when
                        $scope.successMessage=data.successMessage;
                        $scope.questions = data.questions;
                    },
                    function (error) {
                        console.log(error);
                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Deletion!',
            'Are sure you want to delete this record?')
            .then(function () {
                DeleteProjectDataFormQuestionsService.delete({id: id},
                        function (data) {
                            $scope.successMessage=data.successMessage;
                            $scope.questions = data.questions;
                        }, function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }
}

ProjectDataFormQuestionsController.resolve = {
    QuestionsModel: function (ProjectDataFormQuestionsService, $q) {
        var deferred = $q.defer();
        ProjectDataFormQuestionsService.query({page:1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};
