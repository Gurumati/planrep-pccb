function UnitController($scope,
                        UnitsModel,
                        $uibModal,
                        Upload,
                        $timeout,
                        UpdateUnitService,
                        ConfirmDialogService,
                        ToggleUnitService,
                        DeleteUnitService,
                        UnitsService) {

    $scope.units = UnitsModel;
    $scope.title = "UNITS";

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        UnitsService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.units = data;
        });
    };

    $scope.showButtons = true;
    $scope.showUploadForm = false;

    $scope.toggleUploadForm = function (state) {
        if(state===true){
            $scope.showButtons = false;
        } else{
            $scope.showButtons = true;
        }
        $scope.showUploadForm = state;
    };

    $scope.uploadFile = function (file) {
        file.upload = Upload.upload({
            url: '/json/units/upload',
            method: 'POST',
            data: {file: file},
        });

        file.upload.then(function (response) {
            $timeout(function () {
                file.result = response.data;
                $scope.units = response.data.units;
                $scope.toggleUploadForm(false);
                $scope.successMessage = response.data.successMessage;
            });
        }, function (response) {
            if (response.status > 0) {
                $scope.errorMsg = response.status + ': ' + response.data;
                $scope.errorMessage = response.data.errorMessage;
                $scope.successMessage = response.data.successMessage;
            }
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/unit/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateUnitService) {
                $scope.unitToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createUnitForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateUnitService.store({perPage: $scope.perPage}, $scope.unitToCreate,
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.units = data.units;

            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (unitToEdit, currentPage, perPage) {

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/unit/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {
                $scope.unitToEdit = angular.copy(unitToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    $uibModalInstance.close($scope.unitToEdit);
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (unitToEdit) {
                //Service to create new unit
                UpdateUnitService.update({page: currentPage, perPage: perPage}, unitToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage = data.successMessage;
                        $scope.units = data.units; //After save return all units and update $scope.units
                    },
                    function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Deletion!',
            'Are sure you want to delete this record?')
            .then(function () {
                    console.log("YES");
                    DeleteUnitService.delete({unitId: id, currentPage: currentPage, perPage: perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.units = data.units;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }

    $scope.toggleUnit = function (unit_id, unit_status, perPage) {
        $scope.unitToActivate = {};
        $scope.unitToActivate.id = unit_id;
        $scope.unitToActivate.is_active = unit_status;
        ToggleUnitService.toggleUnit({perPage: perPage}, $scope.unitToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    }
}

UnitController.resolve = {

    UnitsModel: function (UnitsService, $q) {
        var deferred = $q.defer();
        UnitsService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};