function ReportController($scope, ReportModel, $uibModal,
                                  ConfirmDialogService,ReportService) {

    $scope.reports = ReportModel;
    $scope.reportsCopy = angular.copy(ReportModel);
    if ($scope.reports.length > 0)
        $scope.reports[0].expanded = true;

    $scope.title = "REPORT_MENU";

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/report/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllReportService, ReportService) {

                AllReportService.query({}, function (data) {
                    $scope.reports = data;
                });
                $scope.createDataModel = {};
                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    ReportService.save($scope.createDataModel, function (data) {
                            $uibModalInstance.close(data);
                        }, function (error) {
                            console.log(error);
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.reports = data.reports;
                if ($scope.reports.length > 0)
                    $scope.reports[0].expanded = true;

                $scope.reportsCopy = angular.copy(data.reports);
                clearTree();
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
                clearTree();
            });

    };

    $scope.edit = function (updateDataModel) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/report/edit.html',
            backdrop: false,
            controller: function ($scope, $q, $uibModalInstance, AllReportService, ReportService) {
                AllReportService.query({}, function (data) {
                    $scope.reports = data;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });

                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updateForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    ReportService.update($scope.updateDataModel,
                        function (data) {
                            $scope.updateDataModel = undefined;
                            $uibModalInstance.close(data);
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.reports = data.reports;
                if ($scope.reports.length > 0)
                    $scope.reports[0].expanded = true;
                $scope.reportsCopy = angular.copy(data.reports);
                clearTree();
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
                clearTree();
            });
    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog('TITLE_CONFIRM_DELETE', 'CONFIRM_DELETE').then(function () {
                ReportService.delete({id: id}, function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.reports = data.reports;
                        if ($scope.reports.length > 0)
                            $scope.reports[0].expanded = true;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {
                clearTree();
            });
    };

    var clearTree = function () {
        $scope.reportNodeSearchQuery = undefined;
        $scope.reportNode.currentNode = undefined;
        $scope.reports = angular.copy($scope.reportsCopy);
        if ($scope.reports.length > 0)
            $scope.reports[0].expanded = true;
    };
}

ReportController.resolve = {
    ReportModel: function (ReportService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            ReportService.query({}, function (data) {
                deferred.resolve(data);
            });
        }, 900);
        return deferred.promise;
    }
};