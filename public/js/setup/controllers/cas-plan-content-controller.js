function CasPlanContentController($scope,
                                  $uibModal,
                                  CasPlanContentModel,
                                  CasPlanService,
                                  UpdateCasPlanContentService,
                                  DeleteCasPlanContentService,
                                  CasPlanContentsService,
                                  ConfirmDialogService
                                ) {

    $scope.casPlans = CasPlanContentModel;
    $scope.title = "CAS_PLAN_CONTENTS";

    $scope.maxSize = 3;

    $scope.loadContents = function (cas_plan_id) {
        CasPlanContentsService.query({cas_plan_id: cas_plan_id},function (data) {
            $scope.casPlanContents = data;
        });
    };


    $scope.create = function (cas_plan_id) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/cas_plan_content/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,CreateCasPlanContentService,  CasPlanService) {
                $scope.casPlanContentToCreate = {};
                $scope.casPlanContentToCreate.cas_plan_id = cas_plan_id;

                $scope.flatter = function (data_array, result) {
                    angular.forEach(data_array, function (value, key) {
                        result.push(value);
                        if(value.children.length > 0)
                        {
                            return $scope.flatter(value.children, result);
                        }
                    });
                    return result;
                };

                CasPlanContentsService.query({cas_plan_id: cas_plan_id}, function (data) {
                    //$scope.casPlanContents = data;
                      $scope.casPlanContents = [];
                      $scope.flatter(data, $scope.casPlanContents);
                });

                CasPlanService.query(function (data) {
                    $scope.planNames = data;
                });

                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createCasPlanContentForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateCasPlanContentService.store({},$scope.casPlanContentToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                           $scope.errorMessage=error.data.errorMessage;
                        }
                    );

                };

                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage=data.successMessage;
                $scope.casPlanContents=data.casPlanContents;

                clearTree();
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
                clearTree();
            });

    };

    $scope.edit = function (casPlanContentToEdit) {
        
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/cas_plan_content/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {
                $scope.casPlanContentToEdit = angular.copy(casPlanContentToEdit);

                $scope.flatter = function (data_array, result) {
                    angular.forEach(data_array, function (value, key) {
                        result.push(value);
                        if(value.children.length > 0)
                        {
                            return $scope.flatter(value.children, result);
                        }
                    });
                    return result;
                };

                CasPlanContentsService.query({cas_plan_id: $scope.casPlanContentToEdit.cas_plan_id}, function (data) {
                    //$scope.casPlanContents = data;
                    $scope.casPlanContents = [];
                    $scope.flatter(data, $scope.casPlanContents);
                });

                CasPlanService.query(function (data) {
                    $scope.planNames = data;
                });

                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    $uibModalInstance.close($scope.casPlanContentToEdit);
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (casTableToEdit) {
                //Service to create cas plan table
                UpdateCasPlanContentService.update({},casTableToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage=data.successMessage;
                        $scope.casPlanContents = data.casPlanContents; //After save return all units and update $scope.units
                    }

                );
                clearTree();
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
                clearTree();
            });
    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Deletion!',
            'Are sure you want to delete this record?')
            .then(function () {
                    console.log("YES");
                    DeleteCasPlanContentService.delete({id: id},
                        function (data) {
                            $scope.successMessage=data.successMessage;
                            $scope.casPlanContents = data.casPlanContents;
                        }, function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }

    var clearTree=function () {
        $scope.casPlanContentNodeSearchQuery=undefined;
        $scope.casPlanContentNode.currentNode=undefined;
    };
}

CasPlanContentController.resolve = {

    CasPlanContentModel: function (CasPlanService, $q) {
        var deferred = $q.defer();
        CasPlanService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};