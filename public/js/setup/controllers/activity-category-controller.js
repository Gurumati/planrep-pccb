(function () {
    'use strict';

    angular.module('setup-module').controller('ActivityCategoryController', ActivityCategoryController);
    ActivityCategoryController.$inject     =  ['$scope', '$uibModal','$location','$translate','ConfirmDialogService','ActivityCategory'];
    function ActivityCategoryController($scope, $uibModal,$location,$translate,ConfirmDialogService,ActivityCategory) {

        $scope.perPage = 10;
        if(undefined === $location.search().page){
            $scope.currentPage = 1;
        }else{
            $scope.currentPage = $location.search().page;
        }

        $translate('TITLE_ACTIVITY_CATEGORY').then(function (title) {
            $scope.title = title;
              }, function (translationId) {
            $scope.title = translationId;
        });

        $scope.pageChanged = function () {
            ActivityCategory.paginated({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
                $scope.activityCategories = data;
                setPagination(data);
            });
        };
        var setPagination = function (pagination) {
            $scope.currentPage = pagination.current_page;
            $scope.perPage = pagination.per_page;
            $location.search('page',$scope.currentPage);
        };
        $scope.pageChanged();

        $scope.create = function () {
            var modalInstance = $uibModal.open({
                templateUrl: '/pages/setup/activity_category/create.html',
                backdrop: false,
                controller: ['$scope','$uibModalInstance','ActivityCategory',function ($scope, $uibModalInstance, ActivityCategory) {
                    $scope.activityCategoryToCreate = {};
                    $scope.store = function () {
                        if ($scope.createActivityCategoryForm.$invalid) {
                            $scope.formHasErrors = true;
                            return;
                        }
                        ActivityCategory.save({perPage:$scope.perPage},$scope.activityCategoryToCreate,
                            function (data) {
                                $uibModalInstance.close(data);
                            },
                            function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                                $scope.errors = error.data.errors;
                            }
                        );
                    };
                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }]
            });
            modalInstance.result.then(function (data) {
                    $scope.messsage = data.successMessage;
                    $scope.messageClass = 'alert-success';
                    $scope.activityCategories = data.activityCategories;
                    setPagination(data);
                },
                function () {
                    console.log('Modal dismissed at: ' + new Date());
                });

        };

        $scope.edit = function (activityCategoryToEdit, currentPage,perPage) {
            var modalInstance = $uibModal.open({
                templateUrl: '/pages/setup/activity_category/edit.html',
                backdrop: false,
                controller:['$scope', '$uibModalInstance','ActivityCategory',function ($scope, $uibModalInstance, ActivityCategory) {
                    $scope.activityCategoryToEdit = angular.copy(activityCategoryToEdit);
                    $scope.update = function () {
                        if ($scope.updateActivityCategoryForm.$invalid) {
                            $scope.formHasErrors = true;
                            return;
                        }
                        ActivityCategory.update({page: currentPage,perPage:perPage}, $scope.activityCategoryToEdit,
                            function (data) {
                                $uibModalInstance.close(data);
                            },
                            function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                                $scope.errors = error.data.errors;
                            }
                        );
                    };
                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }]
            });
            modalInstance.result.then(function (data) {
                    $scope.messsage = data.successMessage;
                    $scope.messageClass = 'alert-success';
                    $scope.activityCategories = data.activityCategories;
                    setPagination(data);
                },
                function () {
                    console.log('Modal dismissed at: ' + new Date());
                });
        };

        $scope.delete = function (id,currentPage,perPage) {
            ConfirmDialogService.showConfirmDialog(
                'TITLE_CONFIRM_DELETE_ACTIVITY_CATEGORY',
                'CONFIRM_DELETE')
                .then(function () {
                        ActivityCategory.delete({id: id,page:currentPage,perPage:perPage},
                            function (data) {
                                $scope.messsage = data.successMessage;
                                $scope.messageClass = 'alert-success';
                                $scope.activityCategories = data.activityCategories;
                                setPagination(data);
                            }, function (error) {
                                $scope.messsage = error.data.errorMessage;
                                $scope.messageClass = 'alert-warning';
                            }
                        );
                    },
                    function () {

                    });
        };
        $scope.toggleActive = function (activity_category_id, activity_status,perPage) {
            $scope.activityCategoryToActivate = {};
            $scope.activityCategoryToActivate.id = activity_category_id;
            $scope.activityCategoryToActivate.is_active = activity_status;
            ActivityCategory.toggleActive({perPage:$scope.perPage,page:$scope.currentPage},
                $scope.activityCategoryToActivate,
                function (data) {
                    $scope.messsage = data.action;
                    $scope.messageClass = data.alertClass;
                    $scope.activityCategories = data.activityCategories;
                    setPagination(data);
                });
        };

    }

})();
