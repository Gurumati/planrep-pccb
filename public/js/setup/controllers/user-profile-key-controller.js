function UserProfileKeyController($scope, UserProfileKeyModel, $uibModal,
                                 ConfirmDialogService, UserProfileKeyService) {

    $scope.userProfileKeys = UserProfileKeyModel;
    $scope.title = "USER_PROFILE_KEYS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        UserProfileKeyService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.userProfileKeys = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/user_profile_key/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UserProfileKeyService) {

                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UserProfileKeyService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.userProfileKeys = data.userProfileKeys;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        //console.log(updateDataModel);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/user_profile_key/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UserProfileKeyService) {

                $scope.updateDataModel = angular.copy(updateDataModel);

                $scope.update = function () {
                    UserProfileKeyService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.userProfileKeys = data.userProfileKeys;
                $scope.currentPage = $scope.userProfileKeys.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                UserProfileKeyService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.userProfileKeys = data.userProfileKeys;
                        $scope.currentPage = $scope.userProfileKeys.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/user_profile_key/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance, UserProfileKeyService) {
                UserProfileKeyService.trashed(function (data) {
                    $scope.trashedUserProfileKeys = data.trashedUserProfileKeys;
                });
                $scope.restoreUserProfileKey = function (id) {
                    UserProfileKeyService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedUserProfileKeys = data.trashedUserProfileKeys;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    UserProfileKeyService.permanentDelete({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedUserProfileKeys = data.trashedUserProfileKeys;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    UserProfileKeyService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedUserProfileKeys = data.trashedUserProfileKeys;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.userProfileKeys = data.userProfileKeys;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

UserProfileKeyController.resolve = {
    UserProfileKeyModel: function (UserProfileKeyService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            UserProfileKeyService.paginated({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        }, 900);
        return deferred.promise;
    }
};