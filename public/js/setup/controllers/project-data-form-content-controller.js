function ProjectDataFormContentsController ($scope,
                                  $uibModal,
                                  ProjectDataFormContentModel,
                                  ProjectDataFormsService,
                                  CreateProjectDataFormContentsService,
                                  UpdateProjectDataFormContentsService,
                                  DeleteProjectDataFormContentsService,
                                  ProjectDataFormContentsService,
                                  ConfirmDialogService
                                ) {

    $scope.projectDataForms = ProjectDataFormContentModel;
    $scope.title = "PROJECT_DATA_FORM_CONTENTS";

    $scope.maxSize = 3;

    $scope.loadContents = function (form_id) {
        ProjectDataFormContentsService.query({form_id: form_id},function (data) {
            $scope.projectDataFormContents = data;
        });
    };


    $scope.create = function (form_id) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/project_data_form_contents/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,CreateProjectDataFormContentsService,  ProjectDataFormContentsService) {
                $scope.projectDataFormContentToCreate = {};
                $scope.projectDataFormContentToCreate.project_data_form_id = form_id;

                $scope.flatter = function (data_array, result) {
                    angular.forEach(data_array, function (value, key) {
                        result.push(value);
                        if(value.children.length > 0)
                        {
                            return $scope.flatter(value.children, result);
                        }
                    });
                    return result;
                };

                ProjectDataFormContentsService.query({form_id: form_id}, function (data) {
                    //$scope.casPlanContents = data;
                    $scope.projectDataFormContents = [];
                    $scope.flatter(data, $scope.projectDataFormContents);
                });

                ProjectDataFormsService.query(function (data) {
                    $scope.projectDataForms = data;
                });

                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createProjectDataFormContentForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateProjectDataFormContentsService.store({},$scope.projectDataFormContentToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage=error.data.errorMessage;
                        }
                    );
                };

                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage=data.successMessage;
                $scope.projectDataFormContents=data.projectDataFormContents;

                clearTree();
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
                clearTree();
            });
    };

    $scope.edit = function (projectDataFormContentToEdit) {
        
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/project_data_form_contents/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {
                $scope.projectDataFormContentToEdit = angular.copy(projectDataFormContentToEdit);

                $scope.flatter = function (data_array, result) {
                    angular.forEach(data_array, function (value, key) {
                        result.push(value);
                        if(value.children.length > 0)
                        {
                            return $scope.flatter(value.children, result);
                        }
                    });
                    return result;
                };

                ProjectDataFormContentsService.query({form_id: $scope.projectDataFormContentToEdit.project_data_form_id}, function (data) {
                    //$scope.casPlanContents = data;
                    $scope.projectDataFormContents = [];
                    $scope.flatter(data, $scope.projectDataFormContents);
                });

                ProjectDataFormsService.query(function (data) {
                    $scope.projectDataForms = data;
                });

                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    $uibModalInstance.close($scope.projectDataFormContentToEdit);
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (projectDataFormContentToEdit) {
                //Service to create cas plan table
                UpdateProjectDataFormContentsService.update({},projectDataFormContentToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage=data.successMessage;
                        $scope.projectDataFormContents = data.projectDataFormContents; //After save return all units and update $scope.units
                    }

                );
                clearTree();
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
                //clearTree();
            });
    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Deletion!',
            'Are sure you want to delete this record?')
            .then(function () {
                    console.log("YES");
                    DeleteProjectDataFormContentsService.delete({id: id},
                        function (data) {
                            $scope.successMessage=data.successMessage;
                            $scope.projectDataFormContents = data.projectDataFormContents;
                        }, function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }

    var clearTree=function () {
        $scope.projectDataFormContentNodeSearchQuery=undefined;
        $scope.projectDataFormContentNode.currentNode=undefined;
    };
}

ProjectDataFormContentsController.resolve = {

    ProjectDataFormContentModel: function (ProjectDataFormsService, $q) {
        var deferred = $q.defer();
        ProjectDataFormsService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};