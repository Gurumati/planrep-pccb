function SectionController($scope,
                           SectionsModel,
                           $uibModal,
                           ConfirmDialogService,
                           DeleteSectionService,
                           SectionToActivateService) {

  $scope.sections = SectionsModel;
  $scope.title = "TITLE_SECTIONS";
  $scope.dateFormat = 'yyyy-MM-dd';

  SectionToActivateService.query({}, function (data) {
    $scope.sectionsToActivate = data;
  });

  $scope.activate = function (sectionToActivate) {
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/section/section-activation.html',
      backdrop: false,
      size: "md",
      controller: function ($scope, $uibModalInstance, ActivateSectionService) {
        $scope.inProgress = false;
        if (sectionToActivate.hasOwnProperty('child_sections')) {
          $scope.sectionsPayload = {
            "admin_hierarchy_id": sectionToActivate.id,
            "admin_hierarchy_name": sectionToActivate.name,
            "cost_center": sectionToActivate.child_sections
          }
        } else {
          $scope.sectionsPayload = {
            "admin_hierarchy_id": sectionToActivate.pisc,
            "admin_hierarchy_name": sectionToActivate.pisc_name,
            "cost_center": [sectionToActivate]
          }
        }

        $scope.activate = function () {
          $scope.inProgress = true;
          ActivateSectionService.activate($scope.sectionsPayload,
            function (data) {
              $scope.inProgress = false;
              $scope.successData = data.sections;
              $scope.progessTitle = "Activation Completed";
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.inProgress = false;
              $scope.openError = true;
              $scope.errorMessage = error.data.errorMessage;
            });
        };
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };

        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
            $scope.successMessage = data.successMessage;
            $scope.sections = data.sections;
            clearTree();
          },
          function () {
            //If modal is closed
            console.log('Modal dismissed at: ' + new Date());
            clearTree();
          });
      }
    });
  }

  $scope.create = function () {
    //Modal Form for creating financial year

    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/section/create.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, CreateSectionService, SectionLevelsService, LoadParentSectionsService, AllPiscsService, AllSectorService, GlobalService, PiscBySectorService,SectionsByLevel) {

        SectionLevelsService.query({}, function (data) {
          $scope.sectionLevels = data;
        });

        AllSectorService.query({}, function (data) {
          $scope.sectors = data;
        });

        AllPiscsService.get({}, function (data) {
          $scope.piscs = data.piscs;
        });

        $scope.loadParentSections = function (sectionLevelId) {
          const sectionLevel = $scope.sectionLevels.find(element => element.id == sectionLevelId);
          const secLev = sectionLevel == null?0:sectionLevel.hierarchy_position;
          LoadParentSectionsService.query({childSectionLevelId: sectionLevelId},
            function (data) {
              if(secLev === 4){
                if($scope.sectionToCreate.admin_id !== undefined){
                  SectionsByLevel.query({levelId: (sectionLevelId-1),adminHierarchyId:$scope.sectionToCreate.admin_id},
                    function (sectionData) {
                      $scope.sectionParents = sectionData;
                    });
                }else{
                  $scope.sectionParents = null;
                }
              }else{
                $scope.sectionParents = data;
              }
            },
            function (error) {
            }
          );
        };

        $scope.loadAdminSection = function (admin_id) {
          if($scope.sectionToCreate.section_level_id !== undefined){
            SectionsByLevel.query({levelId: ($scope.sectionToCreate.section_level_id - 1),adminHierarchyId:admin_id},
              function (sectionData) {
                $scope.sectionParents = sectionData;
              });
          }else{
            $scope.sectionParents = null;
          }
        }

        $scope.loadAdminHierarchy = function (sectorId) {
          PiscBySectorService.query({childSectorId: sectorId},
            function (data) {
              $scope.sectorPiscs = data;
            },
            function (error) {
            }
          );
        };
        $scope.sectionToCreate = {};
        //Function to store data and close modal when Create button clicked
        $scope.store = function () {
          if ($scope.createSectionForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }

          CreateSectionService.store($scope.sectionToCreate,
            function (data) {
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );

        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
        //Service to create new financial year
        $scope.successMessage = data.successMessage;
        $scope.sections = data.sections;
        clearTree();

      },
      function () {
        //If modal is closed
        console.log('Modal dismissed at: ' + new Date());
        clearTree();
      });

  };

  $scope.edit = function (sectionToEdit) {
    if (sectionToEdit.section_level_id !== undefined) {
      var modalInstance = $uibModal.open({
        templateUrl: '/pages/setup/section/edit.html',
        backdrop: false,
        controller: function ($scope, $q, PiscBySectorService, $uibModalInstance, UpdateSectionService, SectionLevelsService, LoadParentSectionsService, AllSectorService,SectionsByLevel,GlobalService) {

          SectionLevelsService.query({}, function (data) {
            LoadParentSectionsService.query({childSectionLevelId: sectionToEdit.section_level_id},
              function (data2) {
                const sectionLevel = data.find(element => element.id == sectionToEdit.section_level_id);
                const secLev = sectionLevel == null?0:sectionLevel.hierarchy_position;
                if(secLev === 4) {
                  SectionsByLevel.query({levelId: (sectionToEdit.section_level_id-1),adminHierarchyId:sectionToEdit.admin_id},
                  function (sectionData) {
                    $scope.sectionParents = sectionData;
                  });
                } else {
                  $scope.sectionParents = data2;
                }
                $scope.sectionLevels = data;
                $scope.sectionToEdit = angular.copy(sectionToEdit);
                $scope.section_level = sectionToEdit.section_level_id <= 2 ? false : true;
              }
            );
          });

          AllSectorService.query({}, function (data) {
            PiscBySectorService.query({childSectorId: sectionToEdit.sector_id},
              function (data2) {
                $scope.piscs = data2;
                $scope.sectors = data;
              });
          });

          $scope.loadAdminHierarchy = function (sectorId) {
            PiscBySectorService.query({childSectorId: sectorId},
              function (data) {
                $scope.piscs = data;
              },
              function (error) {
              }
            );
          };

          $scope.loadParentSections = function (sectionLevelId) {
            const sectionLevel = $scope.sectionLevels.find(element => element.id == sectionLevelId);
            const secLev = sectionLevel == null?0:sectionLevel.hierarchy_position;
            LoadParentSectionsService.query({childSectionLevelId: sectionLevelId},
              function (data) {
                if(secLev === 4){
                  if($scope.sectionToCreate.admin_id !== undefined){
                    SectionsByLevel.query({levelId: (sectionLevelId-1),adminHierarchyId:$scope.sectionToCreate.admin_id},
                      function (sectionData) {
                        $scope.sectionParents = sectionData;
                      });
                  }else{
                    $scope.sectionParents = null;
                  }
                }else{
                  $scope.sectionParents = data;
                }
              },
              function (error) {
              }
            );
          };

          $scope.loadAdminSection = function (admin_id) {
            if($scope.sectionToEdit.section_level_id !== undefined){
              SectionsByLevel.query({levelId: ($scope.sectionToEdit.section_level_id-1),adminHierarchyId:admin_id},
                function (sectionData) {
                  $scope.sectionParents = sectionData;
                });
            }else{
              $scope.sectionParents = null;
            }
          }

          //Function to store data and close modal when Create button clicked
          $scope.update = function () {
            if ($scope.updateSectionForm.$invalid) {
              $scope.formHasErrors = true;
              return;
            }
            UpdateSectionService.update($scope.sectionToEdit,
              function (data) {
                //Successful function when
                $scope.sectionToEdit = undefined;
                $uibModalInstance.close(data);
              },
              function (error) {
                $scope.errorMessage = error.data.errorMessage;
              }
            );
          };
          //Function to close modal when cancel button clicked
          $scope.close = function () {
            $uibModalInstance.dismiss('cancel');
          };
        }
      });
      //Called when modal is close by cancel or to store data
      modalInstance.result.then(function (data) {
          $scope.successMessage = data.successMessage;
          $scope.sections = data.sections;
          clearTree();
        },
        function () {
          //If modal is closed
          console.log('Modal dismissed at: ' + new Date());
          clearTree();
        });
    }

  }

  $scope.delete = function (id) {
    ConfirmDialogService.showConfirmDialog(
      'TITLE_CONFIRM_DELETE_SECTION',
      'CONFIRM_DELETE')
      .then(function () {
          DeleteSectionService.delete({sectionId: id},
            function (data) {
              $scope.successMessage = data.successMessage;
              $scope.sections = data.sections;
            }, function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        },
        function () {

        });
  };
  var clearTree = function () {
    $scope.sectionNodeSearchQuery = undefined;
    //$scope.sectionNode.currentNode = undefined;
  };
}

SectionController.resolve = {
  SectionsModel: function (SectionService, $q) {
    var deferred = $q.defer();
    SectionService.query({}, function (data) {
      deferred.resolve(data);
    });
    return deferred.promise;
  }
}
