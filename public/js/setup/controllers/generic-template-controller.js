function GenericTemplateController($scope, AllPlanChainModel, $uibModal,AllPlanChainService, GetGenericTemplateService) {

    $scope.planChains = AllPlanChainModel;
    $scope.title = "TITLE_GENERIC_TEMPLATES";
    $scope.dateFormat = 'yyyy-MM-dd';
    $scope.genericTemplates = [];

    $scope.setSelectedPlanChain = function (gen) {
        $scope.selectedPlanChain = gen;
        GetGenericTemplateService.query({plan_chain_id: gen.id}, function (data) {
            $scope.genericTemplates = data;
        })
    };

    $scope.createGenericTemplate = function (selectedPlanChainData) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/generic_template/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateGenericTemplateService, ParentGenericTemplates) {
                ParentGenericTemplates.query({
                    plan_chain_id: selectedPlanChainData.id,
                    childTemplateId: 0
                }, function (data) {
                    $scope.parentGenericTemplates = data;
                });
                $scope.genericTemplateToCreate = {};
                $scope.genericTemplateToCreate.plan_chain_id = selectedPlanChainData.id;
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createGenericTemplateForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }

                    CreateGenericTemplateService.store($scope.genericTemplateToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            console.log(error);
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.genericTemplates = data.genericTemplates;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.editGenericTemplate = function (genericTemplateToUpdate) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/generic_template/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateGenericTemplateService, ParentGenericTemplates) {
                ParentGenericTemplates.query({
                    plan_chain_id: genericTemplateToUpdate.plan_chain_id,
                    childTemplateId: genericTemplateToUpdate.id
                }, function (data) {
                    $scope.parentGenericTemplates = data;
                    $scope.genericTemplateToUpdate = genericTemplateToUpdate;
                });
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updateGenericTemplateForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateGenericTemplateService.update($scope.genericTemplateToUpdate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            console.log(error);
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.genericTemplates = data.genericTemplates;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

}

GenericTemplateController.resolve = {
    AllPlanChainModel: function (AllPlanChainService, $q) {
        var deferred = $q.defer();
        AllPlanChainService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};