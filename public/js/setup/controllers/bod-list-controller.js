function BodListController($scope, BodListModel, $uibModal, ConfirmDialogService, DeleteBodListService,
                                PaginatedBodListService) {

    $scope.bodLists = BodListModel;
    $scope.title = "BOD_LIST";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedBodListService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.bodLists = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bod_list/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateBodListService,AllSectorService) {


                AllSectorService.query(function (data) {
                    $scope.sectors = data;
                });

                $scope.bodListToCreate = {};

                $scope.store = function () {
                    if ($scope.createBodListForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateBodListService.store({perPage: $scope.perPage}, $scope.bodListToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.bodLists = data.bodLists;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (bodListToEdit, currentPage, perPage) {
        //console.log(bodListToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bod_list/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateBodListService,AllSectorService) {

                AllSectorService.query(function (data) {
                    $scope.sectors = data;
                });

                $scope.bodListToEdit = angular.copy(bodListToEdit);
                $scope.update = function () {
                    UpdateBodListService.update({page: currentPage, perPage: perPage}, $scope.bodListToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.bodLists = data.bodLists;
                $scope.currentPage = $scope.bodLists.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteBodListService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.bodLists = data.bodLists;
                        $scope.currentPage = $scope.bodLists.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bod_list/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedBodListService,
                                  RestoreBodListService, EmptyBodListTrashService, PermanentDeleteBodListService) {
                TrashedBodListService.query(function (data) {
                    $scope.trashedBodLists = data;
                });
                $scope.restoreBodList = function (id) {
                    RestoreBodListService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBodLists = data.trashedBodLists;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteBodListService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBodLists = data.trashedBodLists;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyBodListTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBodLists = data.trashedBodLists;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.bodLists = data.bodLists;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

BodListController.resolve = {
    BodListModel: function (PaginatedBodListService, $q) {
        var deferred = $q.defer();
        PaginatedBodListService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};