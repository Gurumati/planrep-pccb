function ResponsiblePersonController($scope, ResponsiblePersonModel, $uibModal, ConfirmDialogService,
                                     DeleteResponsiblePersonService,
                                     ActivateResponsiblePersonService) {

    $scope.responsiblePersons = ResponsiblePersonModel;
    $scope.title = "TITLE_RESPONSIBLE_PERSONS";

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/responsible_person/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,GlobalService, SectionLevelsService, LoadSectionsService, CreateResponsiblePersonService) {
                //load section levels
                GlobalService.sectionLevelsByUser(function (data) {
                    $scope.sectionLevels = data.sectionLevels;
                });
                //load sections
                $scope.loadSections = function (sectionLevelId) {
                    GlobalService.sectionLevelSections({section_level_id:sectionLevelId},function (data) {
                        $scope.sections = data.sections;
                    });
                };

                $scope.responsiblePersonToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createResponsiblePersonForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    };

                    CreateResponsiblePersonService.store({perPage:$scope.perPage},$scope.responsiblePersonToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.responsiblePersons = data.responsiblePersons;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (responsiblePersonToEdit,currentPage,perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/responsible_person/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,GlobalService, UpdateResponsiblePersonService,AllSectionService) {
                GlobalService.sectionLevelsByUser(function (data) {
                    $scope.sectionLevels = data.sectionLevels;
                    $scope.responsiblePersonToEdit = angular.copy(responsiblePersonToEdit);
                });
                AllSectionService.query({}, function (data) {
                        $scope.allSections = data;
                    },
                    function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
                //load section function defined
                $scope.loadSections = function (sectionLevelId) {
                    GlobalService.sectionLevelSections({section_level_id:sectionLevelId},function (data) {
                        $scope.sections = data.sections;
                    });
                };
                //load default sections called
                $scope.loadSections(responsiblePersonToEdit.section.id);
                //end load sections
                $scope.update = function () {
                    if ($scope.updateResponsiblePersonForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateResponsiblePersonService.update({page: currentPage,perPage:perPage},$scope.responsiblePersonToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.responsiblePersons = data.responsiblePersons;
                $scope.currentPage = $scope.responsiblePersons.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog(
            'TITLE_CONFIRM_DELETE_RESPONSIBLE_PERSON',
            'CONFIRM_DELETE')
            .then(function () {
                    console.log("YES");
                    DeleteResponsiblePersonService.delete({responsiblePersonId: id,page:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.responsiblePersons = data.responsiblePersons;
                            $scope.currentPage = $scope.responsiblePersons.current_page;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }

    $scope.activateResponsiblePerson = function (responsiblePersonId, responsiblePersonStatus,perPage) {
        $scope.responsiblePersonToActivate = {};
        $scope.responsiblePersonToActivate.id = responsiblePersonId;
        $scope.responsiblePersonToActivate.is_active = responsiblePersonStatus;
        ActivateResponsiblePersonService.activate({perPage:perPage},$scope.responsiblePersonToActivate,
            function (data) {
                $scope.successMessage = false;
                $scope.errorMessage = false;
                $scope.activateMessage = data.activateMessage;
            });

    }
}

ResponsiblePersonController.resolve = {
    ResponsiblePersonModel: function (PaginatedResponsiblePersonService, $q) {
        var deferred = $q.defer();
        PaginatedResponsiblePersonService.get({page:1,perPage:10}, function (data) {
            deferred.resolve(data.responsiblePersons);
        });
        return deferred.promise;
    }
};