function BodVersionController($scope, BodVersionModel, $uibModal, ConfirmDialogService, DeleteBodVersionService,
                              PaginatedBodVersionService, ToggleBodVersionService) {

    $scope.bodVersions = BodVersionModel;
    $scope.title = "BOD_VERSION";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedBodVersionService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.bodVersions = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bod_version/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateBodVersionService, AllBodListService, AllFinancialYearService, ReferenceDocument) {


                AllBodListService.query(function (data) {
                    $scope.bod_lists = data;
                });

                AllFinancialYearService.query(function (data) {
                    $scope.financial_years = data;
                });

                ReferenceDocument.query(function (data) {
                    $scope.reference_documents = data;
                });

                $scope.bodVersionToCreate = {};

                $scope.store = function () {
                    if ($scope.createBodVersionForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateBodVersionService.store({perPage: $scope.perPage}, $scope.bodVersionToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.bodVersions = data.bodVersions;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (bodVersionToEdit, currentPage, perPage) {
        //console.log(bodListToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bod_version/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateBodVersionService,
                                  AllBodListService, AllFinancialYearService, ReferenceDocument) {

                AllBodListService.query(function (data) {
                    $scope.bod_lists = data;
                });
                AllFinancialYearService.query(function (data) {
                    $scope.financial_years = data;
                });
                ReferenceDocument.query(function (data) {
                    $scope.reference_documents = data;
                });

                $scope.bodVersionToEdit = angular.copy(bodVersionToEdit);
                $scope.update = function () {
                    UpdateBodVersionService.update({page: currentPage, perPage: perPage}, $scope.bodVersionToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.bodVersions = data.bodVersions;
                $scope.currentPage = $scope.bodVersions.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.toggleBodVersion = function (bod_version_id, bodVersion_status, perPage) {
        $scope.bodVersionToActivate = {};
        $scope.bodVersionToActivate.id = bod_version_id;
        $scope.bodVersionToActivate.active = bodVersion_status;
        ToggleBodVersionService.toggleBodVersion({perPage: perPage}, $scope.bodVersionToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    };

    $scope.link = function (bodVersionToEdit, currentPage, perPage, bodlist) {

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bod_version/bod_version_intervention_categories.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, GlobalService, AllBodInterventionService, DeleteBodInterventionService,
                                  UpdateBodVersionService, AllFundSourceService, AllFinancialYearService,
                                  AllInterventionCategoryService,
                                  CreateBodInterventionService,
                                  AllBodListService) {

                $scope.bodlistname = bodlist;

                AllInterventionCategoryService.query(function (data) {
                    $scope.intervention_categories = data;
                });

                AllFundSourceService.query(function (data) {
                    $scope.fund_sources = data;
                });

                AllFinancialYearService.query(function (data) {
                    $scope.financial_years = data;
                });

                GlobalService.bodInterventions({bod_version_id: bodVersionToEdit}, function (data) {
                    $scope.bodInterventions = data.bodInterventions;
                });

                AllBodListService.query(function (data) {
                    $scope.bod_lists = data;
                });

                $scope.loadInterventions = function (intervention_category_id) {
                    GlobalService.filterInterventionByCategory({intervention_category_id: intervention_category_id}, function (data) {
                        $scope.interventions = data.interventions;
                    });
                };

                $scope.store = function () {
                    /*if ($scope.createBodInterventionForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }*/
                    CreateBodInterventionService.store({
                            perPage: $scope.perPage,
                            bod_version_id: bodVersionToEdit
                        }, $scope.bodInterventionToCreate,
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.bodInterventionToCreate = undefined;
                            $scope.bodInterventions = data.bodInterventions;
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };


                $scope.delete = function (id, currentPage, perPage) {
                    ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                            DeleteBodInterventionService.delete({
                                    id: id,
                                    page: currentPage,
                                    perPage: perPage,
                                    bod_version_id: bodVersionToEdit
                                },
                                function (data) {
                                    $scope.successMessage = data.successMessage;
                                    $scope.bodInterventions = data.bodInterventions;
                                    console.log(data.bodInterventions);

                                }, function (error) {

                                }
                            );
                        },
                        function () {
                            console.log("NO");
                        });
                };
                $scope.bodVersionToEdit = angular.copy(bodVersionToEdit);
                $scope.update = function () {
                    UpdateBodVersionService.update({page: currentPage, perPage: perPage}, $scope.bodVersionToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.bodInterventions = data.bodInterventions;
                $scope.currentPage = $scope.bodInterventions.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteBodVersionService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.currentPage = $scope.bodVersions.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bod_version/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance, TrashedBodVersionService,
                                  RestoreBodVersionService, EmptyBodVersionTrashService, PermanentDeleteBodVersionService) {
                TrashedBodVersionService.query(function (data) {
                    $scope.trashedBodVersions = data;
                });
                $scope.restoreBodVersion = function (id) {
                    RestoreBodVersionService.restore({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBodVersions = data.trashedBodVersions;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteBodVersionService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBodVersions = data.trashedBodVersions;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyBodVersionTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBodVersions = data.trashedBodVersions;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.bodVersions = data.bodVersions;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}


BodVersionController.resolve = {
    BodVersionModel: function (PaginatedBodVersionService, $q) {
        var deferred = $q.defer();
        PaginatedBodVersionService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};