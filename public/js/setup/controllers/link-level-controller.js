function LinkLevelController($scope, LinkLevelModel, $uibModal,
                             ConfirmDialogService,
                             DeleteLinkLevelService, TruncateLinkLevelService, PaginatedLinkLevelService) {

    $scope.linkLevels = LinkLevelModel;
    $scope.title = "LINK_LEVELS";

    $scope.orderByField = 'name';
    $scope.reverseSort = false;

    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedLinkLevelService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.linkLevels = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/link_level/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateLinkLevelService) {
                $scope.linkLevelToCreate = {};

                $scope.balance_types = ["DR", "CR"];
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createLinkLevelForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateLinkLevelService.store({perPage: $scope.perPage}, $scope.linkLevelToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.linkLevels = data.linkLevels;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.edit = function (linkLevelToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/link_level/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateLinkLevelService) {
                $scope.linkLevelToEdit = angular.copy(linkLevelToEdit);
                $scope.balance_types = ["DR", "CR"];
                $scope.update = function () {
                    if ($scope.updateLinkLevelForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateLinkLevelService.update({page: currentPage, perPage: perPage}, $scope.linkLevelToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.linkLevels = data.linkLevels;
                $scope.currentPage = $scope.linkLevels.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Account Type Delete', 'Are you sure you want to delete this record').then(function () {
                DeleteLinkLevelService.delete({link_level_id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.linkLevels = data.linkLevels;
                        $scope.currentPage = $scope.linkLevels.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    };

    $scope.truncate = function () {
        ConfirmDialogService.showConfirmDialog('Confirm Data Delete', 'Are you sure you want to delete all records').then(function () {
                TruncateLinkLevelService.get({}, function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.linkLevels = data.linkLevels;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {
                $scope.errorMessage = error.data.errorMessage;
            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/link_level/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance, TrashedLinkLevelService,
                                  RestoreLinkLevelService, EmptyLinkLevelTrashService, PermanentDeleteLinkLevelService) {
                TrashedLinkLevelService.query(function (data) {
                    $scope.trashedLinkLevels = data;
                });
                $scope.restoreLinkLevel = function (id) {
                    RestoreLinkLevelService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedLinkLevels = data.trashedLinkLevels;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteLinkLevelService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedLinkLevels = data.trashedLinkLevels;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyLinkLevelTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedLinkLevels = data.trashedLinkLevels;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.linkLevels = data.linkLevels;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

}

LinkLevelController.resolve = {
    LinkLevelModel: function (PaginatedLinkLevelService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            PaginatedLinkLevelService.get({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        }, 100);
        return deferred.promise;
    }
};
