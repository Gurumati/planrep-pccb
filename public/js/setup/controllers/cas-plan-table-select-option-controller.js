function CasPlanTableSelectOptionController($scope,
                                            CasPlanTableSelectOptionModel,
                                            $uibModal,
                                            ConfirmDialogService,
                                            casPlanTableSelectOptionService) {

    $scope.planTableOptions = CasPlanTableSelectOptionModel;
    $scope.title = "CAS_PLAN_TABLE_SELECT_OPTION";

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/cas_plan_table_select_option/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {

                casPlanTableSelectOptionService.get({}, function (data) {
                    $scope.casPlanTableSelectOptions = data.all;
                });

                $scope.store = function () {
                    if ($scope.createCasPlanTableSelectOptionForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    casPlanTableSelectOptionService.save($scope.createCasPlanTableSelectOption,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.planTableOptions = data.planTableOptions;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (casPlanTableSelectOption) {
        //console.log(updateDataModel);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/cas_plan_table_select_option/update.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {

                casPlanTableSelectOptionService.get({}, function (data) {
                    $scope.casPlanTableSelectOptions = data.all;
                });

                $scope.updateCasPlanTableSelectOption = angular.copy(casPlanTableSelectOption);

                $scope.update = function () {
                    casPlanTableSelectOptionService.update($scope.updateCasPlanTableSelectOption,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.planTableOptions = data.planTableOptions;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                casPlanTableSelectOptionService.delete({id: id},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.planTableOptions = data.planTableOptions;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
}

CasPlanTableSelectOptionController.resolve = {
    CasPlanTableSelectOptionModel: function (casPlanTableSelectOptionService, $q) {
        var deferred = $q.defer();
        casPlanTableSelectOptionService.get({}, function (data) {
            deferred.resolve(data.all);
        });
        return deferred.promise;
    }
};
