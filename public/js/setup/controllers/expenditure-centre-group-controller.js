function ExpenditureCentreGroupController($scope, ExpenditureCentreGroupModel, $uibModal,
                               ConfirmDialogService,ExpenditureCentreGroupService, DeleteExpenditureCentreGroupService, PaginatedExpenditureCentreGroupService) {

    $scope.expenditureCentreGroups = ExpenditureCentreGroupModel;
    $scope.title = "EXPENDITURE_CENTRE_GROUPS";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedExpenditureCentreGroupService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
            $scope.expenditureCentreGroups = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/expenditure_centre_group/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,GlobalService, CreateExpenditureCentreGroupService) {
                $scope.createDataModel = {};

                GlobalService.fundSources(function (data) {
                    $scope.fundSources = data.fundSources;
                });

                GlobalService.nationalGuideLineRefDocs(function (data) {
                    $scope.refDocs = data.referenceDocs;
                });

                GlobalService.planningUnits({section_level_id:3},function (data) {
                    $scope.sections = data.sections;
                });

                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateExpenditureCentreGroupService.store({perPage:$scope.perPage},$scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.expenditureCentreGroups = data.expenditureCentreGroups;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.toggleActive = function (id, active,perPage) {
        $scope.dataToActive = {};
        $scope.dataToActive.id = id;
        $scope.dataToActive.active = active;
        ExpenditureCentreGroupService.toggleActive({perPage:perPage},$scope.dataToActive,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    };

    $scope.edit = function (updateDataModel, currentPage,perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/expenditure_centre_group/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,GlobalService, UpdateExpenditureCentreGroupService) {

                GlobalService.fundSources(function (data) {
                    $scope.fundSources = data.fundSources;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });

                GlobalService.nationalGuideLineRefDocs(function (data) {
                    $scope.refDocs = data.referenceDocs;
                });

                GlobalService.sectors(function (data) {
                    $scope.sectors = data.sectors;
                });

                GlobalService.planningUnits({section_level_id:3},function (data) {
                    $scope.sections = data.sections;
                });

                $scope.update = function () {
                    if ($scope.updateForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateExpenditureCentreGroupService.update({page: currentPage,perPage:perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.expenditureCentreGroups = data.expenditureCentreGroups;
                $scope.currentPage = $scope.expenditureCentreGroups.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog('CONFIRM_DELETE', 'Are you sure you want to trash this record?').then(function () {
                    DeleteExpenditureCentreGroupService.delete({expenditure_centre_group_id: id,page:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.expenditureCentreGroups = data.expenditureCentreGroups;
                            $scope.currentPage = $scope.expenditureCentreGroups.current_page;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {

                });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/expenditure_centre_group/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedExpenditureCentreGroupService,
                                  RestoreExpenditureCentreGroupService, EmptyExpenditureCentreGroupTrashService, PermanentDeleteExpenditureCentreGroupService) {
                TrashedExpenditureCentreGroupService.query(function (data) {
                    $scope.trashedExpenditureCentreGroups = data;
                });
                $scope.restoreExpenditureCentreGroup = function (id) {
                    RestoreExpenditureCentreGroupService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedExpenditureCentreGroups = data.trashedExpenditureCentreGroups;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteExpenditureCentreGroupService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedExpenditureCentreGroups = data.trashedExpenditureCentreGroups;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyExpenditureCentreGroupTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedExpenditureCentreGroups = data.trashedExpenditureCentreGroups;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.expenditureCentreGroups = data.expenditureCentreGroups;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

ExpenditureCentreGroupController.resolve = {
    ExpenditureCentreGroupModel: function (PaginatedExpenditureCentreGroupService, $q) {
        var deferred = $q.defer();
        PaginatedExpenditureCentreGroupService.get({page: 1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};