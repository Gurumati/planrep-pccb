function CalendarController($scope, CalendarModel, $uibModal,
                            ConfirmDialogService, CalendarService,CalendarFinancialYears) {

    $scope.calendars = CalendarModel;
    $scope.title = "TITLE_CALENDARS";

    $scope.currentPage = 1;
    $scope.financialYear={id:undefined};
    $scope.dateFormat = 'yyyy-MM-dd';

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        CalendarService.paginated({page: $scope.currentPage, perPage: $scope.perPage,financialYearId:$scope.financialYear.id}, function (data) {
            $scope.calendars = data;
        });
    };

    $scope.getCalendarByFinancialYear=function () {
       $scope.pageChanged();
    };
    CalendarFinancialYears.query(function (data) {
        $scope.financialYears = data;
    });


    $scope.createOrEdit = function (calendar,currentPage,perPage,financialYear) {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/calendar/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,SectionLevelsService, AllSectorService, AllAdminHierarchyLevelService,
                                  CalendarEventService, CalendarFinancialYears) {
                $scope.editMode=false;

                $scope.setDateLimit=function (f) {
                    $scope.startMinDate=new Date(f.start_date);
                    $scope.startMaxDate= new Date(f.end_date);
                    $scope.endMaxDate=new Date(f.end_date);
                    $scope.initDate=new Date(f.start_date);
                };
                console.log(calendar);

                AllAdminHierarchyLevelService.query(function (data) {
                    $scope.hierarchyLevels = data;
                    CalendarFinancialYears.query(function (data) {
                        $scope.financial_years = data;
                        AllSectorService.query(function (data) {
                            $scope.sectors = data;
                            $scope.createDataModel = calendar;

                            if(calendar !== undefined){
                                $scope.editMode=true;
                                CalendarEventService.getByYearAndLevel({financialYearId:calendar.financial_year.id,hierarchyPosition:calendar.hierarchy_position},function (data) {
                                    data.events.push(calendar.calendar_event);
                                    $scope.calendar_events = data.events;
                                    $scope.createDataModel.start_date=new Date(calendar.start_date);
                                    $scope.createDataModel.end_date=new Date(calendar.end_date);
                                });
                                $scope.setDateLimit(calendar.financial_year);
                            }
                            else{
                                $scope.createDataModel={};
                                if(financialYear) {
                                    $scope.createDataModel.financial_year = financialYear;
                                    $scope.setDateLimit(financialYear);
                                }
                            }
                        });
                    });
                });

                $scope.getEventsByYearAndLevel=function (financialYearId,hierarchyPosition) {
                    CalendarEventService.getByYearAndLevel({financialYearId:financialYearId,hierarchyPosition:hierarchyPosition},function (data) {
                        $scope.calendar_events = data.events;
                    });
                };
                SectionLevelsService.query({}, function (data) {
                    $scope.sectionLevels = data;
                });

                $scope.plusDate = function (date, plusValue) {
                    date = new Date(date);
                    date.setDate(date.getDate() + plusValue);
                    return date;
                };
                $scope.initSmsTemplate=function (event) {
                    $scope.createDataModel.before_start_reminder_sms=event.before_start_reminder_sms;
                    $scope.createDataModel.before_end_reminder_sms=event.before_end_reminder_sms;
                    $scope.createDataModel.before_start_reminder_days=event.before_start_reminder_days;
                    $scope.createDataModel.before_end_reminder_days=event.before_end_reminder_days;
                };

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }

                    CalendarService.save({perPage: $scope.perPage,financialYearId:$scope.createDataModel.financial_year.id}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                $scope.update = function () {
                    CalendarService.update({id:$scope.createDataModel.id,page: currentPage, perPage: perPage,financialYearId:$scope.createDataModel.financial_year.id}, $scope.createDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.calendars = data.calendars;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        //console.log(updateDataModel);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/calendar/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllSectorService,SectionLevelsService,
                                  AllAdminHierarchyLevelService, AllFinancialYearService, AllCalendarEventService,
                                  CalendarService) {
                AllSectorService.query(function (data) {
                    $scope.sectors = data;
                });



                AllFinancialYearService.query(function (data) {
                    $scope.financial_years = data;
                });

                AllCalendarEventService.query(function (data) {
                    $scope.calendar_events = data;
                });

                $scope.plusDate = function (date, plusValue) {
                    date = new Date(date);
                    date.setDate(date.getDate() + plusValue);
                    return date;
                };
                SectionLevelsService.query({}, function (data) {
                    $scope.sectionLevels = data;
                });

                AllAdminHierarchyLevelService.query(function (data) {
                    $scope.hierarchyLevels = data;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                    $scope.updateDataModel.start_date = new Date($scope.updateDataModel.start_date);
                    $scope.updateDataModel.end_date = new Date($scope.updateDataModel.end_date);
                });

                $scope.update = function () {
                    CalendarService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.calendars = data.calendars;
                $scope.currentPage = $scope.calendars.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                CalendarService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.calendars = data.calendars;
                        $scope.currentPage = $scope.calendars.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/calendar/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance, CalendarService) {
                CalendarService.trashed(function (data) {
                    $scope.trashedCalendars = data.trashedCalendars;
                });
                $scope.restoreCalendar = function (id) {
                    CalendarService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCalendars = data.trashedCalendars;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    CalendarService.permanentDelete({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCalendars = data.trashedCalendars;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };

                $scope.emptyTrash = function () {
                    CalendarService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCalendars = data.trashedCalendars;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.calendars = data.calendars;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

CalendarController.resolve = {
    CalendarModel: function (CalendarService, $q,$timeout) {
        var deferred = $q.defer();
        $timeout(function () {
            CalendarService.paginated({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        },100);
        return deferred.promise;
    }
};