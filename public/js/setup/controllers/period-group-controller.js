function PeriodGroupController($scope, PeriodGroupModel, $uibModal, ConfirmDialogService, DeletePeriodGroupService,
                                PaginatedPeriodGroupService) {

    $scope.periodGroups = PeriodGroupModel;
    $scope.title = "TITLE_PERIOD_GROUPS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedPeriodGroupService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.periodGroups = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/period_group/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreatePeriodGroupService) {


                $scope.periodGroupToCreate = {};

                $scope.store = function () {
                    if ($scope.createPeriodGroupForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreatePeriodGroupService.store({perPage: $scope.perPage}, $scope.periodGroupToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.periodGroups = data.periodGroups;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (periodGroupToEdit, currentPage, perPage) {
        //console.log(bodListToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/period_group/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdatePeriodGroupService) {


                $scope.periodGroupToEdit = angular.copy(periodGroupToEdit);
                $scope.update = function () {
                    UpdatePeriodGroupService.update({page: currentPage, perPage: perPage}, $scope.periodGroupToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.periodGroups = data.periodGroups;
                $scope.currentPage = $scope.periodGroups.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeletePeriodGroupService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.periodGroups = data.periodGroups;
                        $scope.currentPage = $scope.periodGroups.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/period_group/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedPeriodGroupService,
                                  RestorePeriodGroupService, EmptyPeriodGroupTrashService, PermanentDeletePeriodGroupService) {
                TrashedPeriodGroupService.query(function (data) {
                    $scope.trashedPeriodGroups = data;
                });
                $scope.restorePeriodGroup = function (id) {
                    RestorePeriodGroupService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedPeriodGroups = data.trashedPeriodGroups;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeletePeriodGroupService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedPeriodGroups = data.trashedPeriodGroups;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyPeriodGroupTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedPeriodGroups = data.trashedPeriodGroups;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.periodGroups = data.periodGroups;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

PeriodGroupController.resolve = {
    PeriodGroupModel: function (PaginatedPeriodGroupService, $q) {
        var deferred = $q.defer();
        PaginatedPeriodGroupService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};