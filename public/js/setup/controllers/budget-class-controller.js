function BudgetClassController($scope, BudgetClassesService, FinancialYearModel,
                               $uibModal, AllFinancialYearService,
                               UpdateBudgetClassService,
                               DeleteBudgetClassService, FinancialYearService,
                               ConfirmDialogService,
                               DeleteBudgetTemplateService, CarryVersionService) {

    $scope.financialYears = FinancialYearModel;
    $scope.title = "BUDGET_CLASSES";

    $scope.loadVersions = function (financialYearId) {
        $scope.financialYearId = financialYearId;

        FinancialYearService.versions({id: financialYearId, type: 'BUDGET_CLASS'}, function (response) {
            $scope.versions = response.data;

            $scope.loadBudgetClasses = function (versionId) {
                $scope.versionId = versionId;

                BudgetClassesService.query({financialYearId: financialYearId, versionId: versionId}, function (data) {
                    $scope.budgetClasses = data;
                });

                $scope.create = function (financialYearId, versionId) {
                    var modalInstance = $uibModal.open({
                        templateUrl: '/pages/setup/budget_class/create.html',
                        backdrop: false,

                        controller: function ($scope, $uibModalInstance, CreateBudgetClassService, ParentBudgetClassesService) {

                            ParentBudgetClassesService.query({
                                childId: 0,
                                financialYearId: financialYearId,
                                versionId: versionId
                            }, function (data) {
                                $scope.budgetClasses = data;
                            });

                            $scope.budgetClassToCreate = {};
                            /*$scope.budgetClassToCreate.is_automatic = false;*/
                            $scope.budgetClassToCreate.versionId = versionId;
                            $scope.budgetClassToCreate.budget_class_templates = [{type: 1}, {type: 2}, {type: 3}, {type: 4}];
                            //Function to store data and close modal when Create button clicked
                            /*$scope.automatic = function(isAuto){
                                $scope.budgetClassToCreate.is_automatic = isAuto ? true : false;
                            };*/
                            $scope.store = function () {
                                if ($scope.createBudgetClassForm.$invalid) {
                                    $scope.formHasErrors = true;
                                    return;
                                }
                                CreateBudgetClassService.store($scope.budgetClassToCreate,
                                    function (data) {
                                        $uibModalInstance.close(data);
                                    },
                                    function (error) {
                                        $scope.errorMessage = error.data.errorMessage;
                                    }
                                );

                            };
                            //Function to close modal when cancel button clicked
                            $scope.close = function () {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    });
                    //Called when modal is close by cancel or to store data
                    modalInstance.result.then(function (data) {
                            //Service to create new budget class
                            $scope.successMessage = data.successMessage;
                            $scope.budgetClasses = data.budgetClasses;
                        },
                        function () {
                            //If modal is closed
                            console.log('Modal dismissed at: ' + new Date());
                        });

                };

                $scope.edit = function (financialYearId, versionId, budgetClassToEdit) {
                    var modalInstance = $uibModal.open({
                        templateUrl: '/pages/setup/budget_class/edit.html',
                        backdrop: false,
                        controller: function ($scope, $uibModalInstance, ParentBudgetClassesService) {
                            ParentBudgetClassesService.query({
                                childId: budgetClassToEdit.id,
                                financialYearId: financialYearId,
                                versionId: versionId
                            }, function (data) {
                                $scope.budgetClasses = data;
                            });

                            $scope.budgetClassToEdit = angular.copy(budgetClassToEdit);
                            $scope.budgetClassToEdit.versionId = versionId;

                            if ($scope.budgetClassToEdit.budget_class_templates.length == 0 && $scope.budgetClassToEdit.is_automatic) {
                                $scope.budgetClassToEdit.budget_class_templates = [{type: 1}, {type: 2}, {type: 3}, {type: 4}];
                            }
                            //Function to store data and close modal when Create button clicked
                            $scope.update = function () {
                                $uibModalInstance.close($scope.budgetClassToEdit);
                            };
                            //Function to close modal when cancel button clicked
                            $scope.close = function () {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    });
                    //Called when modal is close by cancel or to store data
                    modalInstance.result.then(function (budgetClassToEdit) {
                            //Service to create new budget class
                            UpdateBudgetClassService.update(budgetClassToEdit,
                                function (data) {
                                    //Successful function when
                                    $scope.successMessage = data.successMessage;
                                    $scope.budgetClasses = data.budgetClasses; //After save return all budget classes and update $scope.budgetClasses
                                },
                                function (error) {

                                }
                            );
                        },
                        function () {
                            //If modal is closed
                            console.log('Modal dismissed at: ' + new Date());
                        });
                };

                $scope.delete = function (versionId, id) {
                    ConfirmDialogService.showConfirmDialog(
                        'Confirm Delete Budget Class!',
                        'Are sure you want to delete this?')
                        .then(function () {
                                console.log("YES");
                                DeleteBudgetClassService.delete({
                                        id: id,
                                        versionId: versionId
                                    },
                                    function (data) {
                                        $scope.successMessage = data.successMessage;
                                        $scope.budgetClasses = data.budgetClasses;
                                    }, function (error) {

                                    }
                                );
                            },
                            function () {
                                console.log("NO");
                            });
                };

                $scope.budgetTemplate = function (budgetClass) {
                    //Modal Form for creating a budget class

                    var modalInstance = $uibModal.open({
                        templateUrl: '/pages/setup/budget_class/budget_template.html',
                        backdrop: false,
                        controller: function ($scope, $uibModalInstance, CreateBudgetTemplateService, LoadBudgetTemplateService, AllBudgetTemplateService) {
                            $scope.templateToCreate = {};
                            $scope.budgetForm = false;
                            //load Template
                            AllBudgetTemplateService.get({budget_class_id: budgetClass}, function (data) {
                                $scope.budgetTemplates = data.budgetTemplates;
                            });
                            $scope.loadTemplate = function (template_type) {
                                $scope.budgetForm = true;
                                LoadBudgetTemplateService.query({
                                    template_type: template_type,
                                    budget_class_id: budgetClass
                                }, function (data) {
                                    if (data.length > 0) {
                                        $scope.templateToCreate = data[0];
                                    } else {
                                        //clear other inputs
                                        $scope.templateToCreate = {};
                                        $scope.templateToCreate.type = template_type;
                                    }
                                });
                            }
                            //show templatee
                            $scope.showTemplateForm = function () {
                                $scope.budgetForm = true;
                            }
                            $scope.hideTemplateForm = function () {
                                $scope.budgetForm = false;
                            }
                            //Function to store data and close modal when Create button clicked
                            $scope.storeTemplate = function () {
                                if ($scope.createBudgetTemplate.$invalid) {
                                    $scope.formHasErrors = true;
                                    return;
                                }
                                $scope.templateToCreate.budget_class_id = budgetClass;
                                CreateBudgetTemplateService.storeTemplate($scope.templateToCreate,
                                    function (data) {
                                        $scope.successMessage = data.successMessage;
                                        $scope.budgetTemplates = data.budgetTemplates;
                                        $scope.budgetForm = false;
                                        //$uibModalInstance.close(data);
                                    },
                                    function (error) {
                                        $scope.errorMessage = error.data.errorMessage;
                                    }
                                );

                            }
                            //delete function
                            $scope.deleteTemplate = function (id) {
                                ConfirmDialogService.showConfirmDialog(
                                    'CONFIRM_BUDGET_CLASS_TEMPLATE',
                                    'Are sure you want to delete this?')
                                    .then(function () {
                                            DeleteBudgetTemplateService.delete({budgetTemplateId: id},
                                                function (data) {
                                                    $scope.message = data.successMessage;
                                                    $scope.budgetTemplates = data.budgetTemplates;
                                                }, function (error) {

                                                }
                                            );
                                        },
                                        function () {

                                        });
                            }
                            //Function to close modal when cancel button clicked
                            $scope.close = function () {
                                $uibModalInstance.dismiss('cancel');
                            }
                        }

                    });
                    //Called when modal is close by cancel or to store data
                    modalInstance.result.then(function (data) {
                            //Service to create new budget class
                            $scope.successMessage = data.successMessage;
                        },
                        function () {
                            //If modal is closed
                            console.log('Modal dismissed at: ' + new Date());
                        });

                };

                $scope.copy = function (financialYear, version) {
                    let modalInstance = $uibModal.open({
                        templateUrl: '/pages/setup/budget_class/copy.html',
                        backdrop: false,
                        controller: function ($scope, $uibModalInstance, AllFinancialYearService, BudgetClassService, FinancialYearService) {

                            $scope.sourceFinancialYear = financialYear;
                            $scope.sourceVersion = version;

                            AllFinancialYearService.query(function (data) {
                                $scope.financialYears = data;

                                $scope.loadFinancialYearVersions = function (financialYearId) {

                                    FinancialYearService.otherVersions({
                                        financialYearId: financialYearId,
                                        currentVersionId: versionId,
                                        type: 'BUDGET_CLASS',
                                        currentFinancialYearId: $scope.sourceFinancialYear.id,
                                    }, function (response) {
                                        $scope.financialYearVersions = response.data;
                                        $scope.copySubBudgetClasses = function (sourceFinancialYearId, sourceVersionId, destinationVersionId) {
                                            BudgetClassService.copy({
                                                sourceFinancialYearId: sourceFinancialYearId,
                                                sourceVersionId: sourceVersionId,
                                                destinationVersionId: destinationVersionId
                                            }, function (response) {
                                                $uibModalInstance.close(response);
                                            });
                                        };
                                    });
                                };
                            });

                            $scope.close = function () {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    });
                    //Called when modal is close by cancel or to store data
                    modalInstance.result.then(function (response) {
                            $scope.successMessage = response.message;
                        },
                        function () {

                        });

                };
            };
        });
    };

    $scope.carry = function (currentFinancialYearId, item) {
        CarryVersionService.showDialog('BC',currentFinancialYearId, item).then(function(data){
            console.log(data);
        }, function(){
            
        });
    };
}

BudgetClassController.resolve = {
    FinancialYearModel: function (AllFinancialYearService, $q) {
        let deferred = $q.defer();
        AllFinancialYearService.query(function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};