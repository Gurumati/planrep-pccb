function RoleController($scope, RolesModel,$uibModal,
                                 ConfirmDialogService) {



    var processRoles=function (roles) {
        $scope.roles=roles;
        $scope.roles.forEach(function (role) {
            var obj=JSON.parse(role.permissions);
            if(obj !== null) {
                var array = $.map(obj, function (value, index) {
                    return [index];
                });
                role.permissions = array;
            }else {
                role.permissions=[];
            }
        });
    };
    processRoles(RolesModel);
    $scope.title = "TITLE_ROLES";
    $scope.dateFormat = 'yyyy-MM-dd';


    $scope.create = function () {
        //Modal Form for creating financial year
        var modalInstance = $uibModal.open({
            templateUrl: 'create.html',
            backdrop: false,
            resolve:{
                AccessRightsModel:function (AccessRightsService,$q) {
                    var deferred = $q.defer();
                    AccessRightsService.query({}, function (data) {
                        deferred.resolve(data);
                    });
                    return deferred.promise;
                }
            },
            controller: function ($scope, $uibModalInstance,CreateRoleService,AccessRightsModel,AdminHierarchyLevelsByUserService) {
                $scope.roleToCreate = {};
                $scope.rights = AccessRightsModel;
                AdminHierarchyLevelsByUserService.query({noLoader:true}, function (data) {
                    $scope.levels = data;
                    $scope.adminLevelsLoaded=true;
                });
                $scope.store = function () {
                    if ($scope.createRoleForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateRoleService.store($scope.roleToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                           $scope.errorMessage=error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage=data.successMessage;
                processRoles(data.roles);
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (roleToEdit) {
        var modalInstance = $uibModal.open({
            templateUrl: 'edit.html',
            backdrop: false,
            resolve:{
                AccessRightsModel:function (AccessRightsService,$q) {
                    var deferred = $q.defer();
                    AccessRightsService.query({}, function (data) {
                        deferred.resolve(data);
                    });
                    return deferred.promise;
                }
            },
            controller: function ($scope, $uibModalInstance,UpdateRoleService,AccessRightsModel,AdminHierarchyLevelsByUserService) {
                $scope.rights = AccessRightsModel;
                $scope.roleToEdit = angular.copy(roleToEdit);
                AdminHierarchyLevelsByUserService.query({noLoader:true}, function (data) {
                    $scope.levels = data;
                    $scope.adminLevelsLoaded=true;
                });
                $scope.update = function () {
                    if ($scope.updateRoleForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateRoleService.update($scope.roleToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage=error.data.errorMessage;
                        }
                    );


                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage=data.successMessage;
                processRoles(data.roles);
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    //
    // $scope.delete = function (id) {
    //     ConfirmDialogService.showConfirmDialog(
    //         'TITLE_CONFIRM_DELETE_ACCESS_RIGHT',
    //         'CONFIRM_DELETE')
    //         .then(function () {
    //                 console.log("YES");
    //                 DeleteAccessRightService.delete({accessRightId: id},
    //                     function (data) {
    //                         $scope.successMessage = data.successMessage;
    //                         $scope.accessRights = data.accessRights;
    //                     }, function (error) {
    //                         $scope.errorMessage=error.data.errorMessage;
    //                     }
    //                 );
    //             },
    //             function () {
    //                 console.log("NO");
    //             });
    // }

}

RoleController.resolve = {

    RolesModel: function (RolesService, $q,$timeout) {
        var deferred = $q.defer();
        $timeout(function () {
            RolesService.query({}, function (data) {
                deferred.resolve(data);
            });
        },100);

        return deferred.promise;
    }


};
