function ActivityStatusController($scope, ActivityStatusModel, $uibModal, ConfirmDialogService,ToggleActivityStatusService, DeleteActivityStatusService,
                                PaginatedActivityStatusService) {

    $scope.activityStatuses = ActivityStatusModel;
    $scope.title = "ACTIVITY_STATUS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedActivityStatusService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.activityStatuses = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/activity_status/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateActivityStatusService) {

                $scope.activityStatusToCreate = {};

                $scope.store = function () {
                    if ($scope.createActivityStatusForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateActivityStatusService.store({perPage: $scope.perPage}, $scope.activityStatusToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.activityStatuses = data.activityStatuses;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (activityStatusToEdit, currentPage, perPage) {
        //console.log(bodListToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/activity_status/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateActivityStatusService,AllActivityStatusService) {

                AllActivityStatusService.query(function (data) {
                    $scope.activityStatus = data;
                });

                $scope.activityStatusToEdit = angular.copy(activityStatusToEdit);
                $scope.update = function () {
                    UpdateActivityStatusService.update({page: currentPage, perPage: perPage}, $scope.activityStatusToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.activityStatuses = data.activityStatuses;
                $scope.currentPage = $scope.activityStatuses.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteActivityStatusService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.activityStatuses = data.activityStatuses;
                        $scope.currentPage = $scope.activityStatuses.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };

    $scope.toggleActivityStatus = function (activity_status_id, status,perPage) {
        $scope.activityStatusToActivate = {};
        $scope.activityStatusToActivate.id = activity_status_id;
        $scope.activityStatusToActivate.is_default = status;
        ToggleActivityStatusService.toggleActivityStatus({perPage:perPage},$scope.activityStatusToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/activity_status/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedActivityStatusService,
                                  RestoreActivityStatusService, EmptyActivityStatusService, PermanentDeleteActivityStatusService) {
                TrashedActivityStatusService.query(function (data) {
                    $scope.trashedActivityStatuses = data;
                });
                $scope.restoreActivityStatus = function (id) {
                    RestoreActivityStatusService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedActivityStatuses = data.trashedActivityStatuses;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteActivityStatusService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedActivityStatuses = data.trashedActivityStatuses;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyActivityStatusService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedActivityStatuses = data.trashedActivityStatuses;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.activityStatuses = data.activityStatuses;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

ActivityStatusController.resolve = {
    ActivityStatusModel: function (PaginatedActivityStatusService, $q) {
        var deferred = $q.defer();
        PaginatedActivityStatusService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};