function CasGroupColumnController($scope,
                                  CasGroupColumnModel,
                                  $uibModal,
                                  CreateCasGroupColumnsService,
                                  UpdateCasGroupColumnsService,
                                  ConfirmDialogService,
                                  ToggleUnitService,
                                  DeleteCasGroupColumnsService,
                                  CasPlanTableGroupTypesService,
                                  PaginateCasGroupColumnsService) {

    $scope.casGroupColumns = CasGroupColumnModel;
    $scope.title = "CAS_GROUP_COLUMNS";
    $scope.maxSize = 3;

    $scope.pageChanged = function () {
        PaginateCasGroupColumnsService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
            $scope.casGroupColumns = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/cas_group_column/create.html',
            backdrop: false,
            controller: function ($scope,
                                  $uibModalInstance,
                                  CreateCasGroupColumnsService,
                                  CasPlanTableGroupTypesService
            ) {

                CasPlanTableGroupTypesService.query(function (data) {
                    $scope.casGroupTypes=data;
                });

                $scope.casGroupColumnToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createCasGroupColumnForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateCasGroupColumnsService.store({perPage:$scope.perPage},$scope.casGroupColumnToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                           $scope.errorMessage=error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage=data.successMessage;
                $scope.casGroupColumns=data.casGroupColumns;

            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (casGroupColumnToEdit, currentPage,perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/cas_group_column/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {

                CasPlanTableGroupTypesService.query(function (data) {
                    $scope.casGroupTypes=data;
                });

                $scope.casGroupColumnToEdit = angular.copy(casGroupColumnToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    $uibModalInstance.close($scope.casGroupColumnToEdit);
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (casGroupColumnToEdit) {
                //Service to create a new cas group column
                UpdateCasGroupColumnsService.update({page:currentPage,perPage:perPage},casGroupColumnToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage=data.successMessage;
                        $scope.casGroupColumns = data.casGroupColumns; //After save return all casGroupColumns and update $scope.casGroupColumns\
                        $scope.currentPage = $scope.casGroupTypes.current_page;
                    },
                    function (error) {

                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Deletion!',
            'Are sure you want to delete this record?')
            .then(function () {
                    console.log("YES");
                    DeleteCasGroupColumnsService.delete({id: id,currentPage:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage=data.successMessage;
                            $scope.casGroupColumns = data.casGroupColumns;
                        }, function (error) {
                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }
}

CasGroupColumnController.resolve = {
    CasGroupColumnModel: function (PaginateCasGroupColumnsService, $q) {
        var deferred = $q.defer();
        PaginateCasGroupColumnsService.get({page:1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};