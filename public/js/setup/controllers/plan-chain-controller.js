function PlanChainController(
  $scope,
  FinancialYearModel,
  $uibModal,
  FinancialYearService,
  AllFinancialYearService,
  ConfirmDialogService,
  DeletePlanChainService,
  PlanChainTypesService,
  PlanChainService,
  ActivatePlanChainService,
  PlanChainsTypeService,
  SectorsService,
  PaginatePlanChainService,
  CarryVersionService,
  localStorageService
) {
  $scope.financialYears = FinancialYearModel;
  $scope.title = "TITLE_PLAN_CHAIN";

  $scope.loadVersions = function (financialYearId) {
    $scope.financialYearId = financialYearId;

    FinancialYearService.versions(
      {
        id: financialYearId,
        type: "PLAN_CHAIN",
      },
      function (response) {
        $scope.versions = response.data;

        $scope.loadPlanChainTypes = function (versionId) {
          $scope.versionId = versionId;

          PlanChainTypesService.query({}, function (data) {
            $scope.PlanChainTypes = data;
            console.log("data1")
            console.log(data)
          });

          $scope.loadPlanChains = function (
            financialYearId,
            versionId,
            planChainTypeId
          ) {
            $scope.planChainTypeId = planChainTypeId;

            $scope.is_sectoral = false;
            $scope.parent_component = "Plan Sequence";

            $scope.currentPage = 1;
            $scope.maxSize = 3;
            $scope.perPage = 10;
            $scope.hierarchy_position = localStorageService.get(
              localStorageKeys.HIERARCHY_POSITION
            );
            PaginatePlanChainService.get(
              {
                page: $scope.currentPage,
                perPage: $scope.perPage,
                planChainTypeId: planChainTypeId,
                financialYearId: financialYearId,
                versionId: versionId,
              },
              function (data) {
                $scope.planChains = data.planChains;
              }
            );

            $scope.pageChanged = function () {
              PaginatePlanChainService.get(
                {
                  page: $scope.currentPage,
                  perPage: $scope.perPage,
                  planChainTypeId: $scope.planChainTypeId,
                  financialYearId: $scope.financialYearId,
                  versionId: $scope.versionId,
                  searchText: $scope.searchText,
                },
                function (data) {
                  $scope.planChains = data;
                }
              );
            };

            // $scope.search = function (financialYearId, versionId, planChainTypeId, searchText) {
            //     PlanChainService.search({
            //         searchText: searchText,
            //         financialYearId: financialYearId,
            //         versionId: versionId,
            //         planChainTypeId: planChainTypeId,
            //     }, function (data) {
            //         $scope.planChains = data;
            //     });
            // };

            $scope.priorityAreas = function (planningSequence) {
              let modalInstance = $uibModal.open({
                templateUrl: "/pages/setup/plan_chain/priority-areas.html",
                backdrop: false,

                controller: function (
                  $scope,
                  $uibModalInstance,
                  PriorityAreaService,
                  AllFinancialYearService,
                  PlanChainService
                ) {
                  $scope.planningSequence = planningSequence;

                  PlanChainService.priorityAreas(
                    {
                      id: planningSequence.id,
                    },
                    function (response) {
                      $scope.priorityAreas = response.data;
                    },
                    function (error) {
                      console.log(error);
                    }
                  );

                  AllFinancialYearService.query(
                    function (response) {
                      $scope.financialYears = response;

                      $scope.loadVersions = function (financialYearId) {
                        $scope.financialYearId = financialYearId;

                        FinancialYearService.versions(
                          {
                            id: financialYearId,
                            type: "PRIORITY_AREA",
                          },
                          function (response) {
                            $scope.versions = response.data;

                            $scope.loadPriorityAreas = function (versionId) {
                              $scope.versionId = versionId;

                              PriorityAreaService.fetchAll(
                                {
                                  financialYearId: financialYearId,
                                  versionId: versionId,
                                },
                                function (response) {
                                  $scope.versionPriorityAreas = response.data;
                                },
                                function (error) {
                                  console.log(error);
                                }
                              );
                            };
                          },
                          function (error) {
                            console.log(error);
                          }
                        );
                      };
                    },
                    function (error) {
                      console.log(error);
                    }
                  );
                  $scope.priorityAreaData = {};

                  $scope.addPriorityAreas = function () {
                    $scope.priorityAreaData.planChainId = planningSequence.id;
                    PlanChainService.addPriorityAreas(
                      $scope.priorityAreaData,
                      function (response) {
                        $scope.priorityAreas = response.data;
                        $scope.successMessage = response.message;
                      },
                      function (error) {
                        console.log(error);
                      }
                    );
                  };

                  $scope.removePriorityArea = function (id) {
                    PlanChainService.removePriorityArea(
                      {
                        id: id,
                        planChainId: planningSequence.id,
                      },
                      function (response) {
                        $scope.priorityAreas = response.data;
                        $scope.successMessage = response.message;
                      },
                      function (error) {
                        console.log(error);
                      }
                    );
                  };

                  $scope.close = function () {
                    $uibModalInstance.dismiss("cancel");
                  };
                },
              });
              modalInstance.result.then(
                function (data) {
                  $scope.successMessage = data.successMessage;
                  $scope.planChains = data.planChains;
                  $scope.currentPage = $scope.planChains.current_page;
                },
                function () {
                  //If modal is closed
                  console.log("Modal dismissed at: " + new Date());
                }
              );
            };

            $scope.copy = function (financialYear, version) {
              let modalInstance = $uibModal.open({
                templateUrl: "/pages/setup/plan_chain/copy.html",
                backdrop: false,
                controller: function (
                  $scope,
                  $uibModalInstance,
                  AllFinancialYearService,
                  PlanChainService,
                  FinancialYearService
                ) {
                  $scope.sourceFinancialYear = financialYear;
                  $scope.sourceVersion = version;

                  AllFinancialYearService.query(function (data) {
                    $scope.financialYears = data;

                    $scope.loadFinancialYearVersions = function (
                      financialYearId
                    ) {
                      FinancialYearService.otherVersions(
                        {
                          financialYearId: financialYearId,
                          currentVersionId: versionId,
                          type: "PLAN_CHAIN",
                          currentFinancialYearId: $scope.sourceFinancialYear.id,
                        },
                        function (response) {
                          $scope.financialYearVersions = response.data;
                          $scope.copyPlanningSequences = function (
                            sourceFinancialYearId,
                            sourceVersionId,
                            destinationVersionId
                          ) {
                            PlanChainService.copyPlanChains(
                              {
                                sourceVersionId: sourceVersionId,
                                destinationVersionId: destinationVersionId,
                                sourceFinancialYearId: sourceFinancialYearId,
                              },
                              function (response) {
                                $uibModalInstance.close(response);
                              }
                            );
                          };
                        }
                      );
                    };
                  });

                  $scope.close = function () {
                    $uibModalInstance.dismiss("cancel");
                  };
                },
              });
              //Called when modal is close by cancel or to store data
              modalInstance.result.then(
                function (response) {
                  $scope.successMessage = response.message;
                },
                function () { }
              );
            };

            $scope.create = function (
              financialYearId,
              versionId,
              PlanChainTypes,
              selectedChainType,
              perPage
            ) {
              let modalInstance = $uibModal.open({
                templateUrl: "/pages/setup/plan_chain/create.html",
                backdrop: false,
                controller: function (
                  $scope,
                  $uibModalInstance,
                  PlanChainService,
                  PlanChainTypesService,
                  CreatePlanChainService,
                  LoadParentPlanChainService,
                  SectorsService,
                  PlanChainSectorOnlyService
                ) {

                  $scope.checkObjectiveValid = /^[a-zA-Z]{1,1}$/;
                  $scope.is_sectoral = false;
                  $scope.PlanChainTypes = PlanChainTypes;

                  $scope.validateObjCode = function (objCode) {
                    $scope.objCodeExist = false;
                    if (objCode !== undefined && objCode.length == 1) {
                      $scope.validatingObjCode = true;
                      CreatePlanChainService.validateObjCode(
                        {
                          objCode: objCode,
                          //adminHierarchyId: plan_chain.admin_hierarchy_id !== undefined ? plan_chain.admin_hierarchy_id : 0,
                          noLoader: true,
                        },
                        function (data) {
                          $scope.validatingObjCode = false;
                          if (data.exist) {
                            $scope.objCodeExist = true;
                          } else {
                            $scope.objCodeExist = false;
                          }
                        },
                        function () {
                          $scope.validatingObjCode = false;
                        }
                      );
                    }
                  };



                  SectorsService.query(
                    {},
                    function (data) {
                      $scope.sectors = data;
                      if (data.length === 1)
                        angular.forEach(data, function (value, index) {
                          $scope.selection.push(value.id);
                        });
                    },
                    function (error) {
                      $scope.errorMessage = error.data.errorMessage;
                    }
                  );

                  $scope.selection = [];

                  // Toggle selection for a given sector by id
                  $scope.toggleSelection = function toggleSelection(s) {
                    var idx = $scope.selection.indexOf(s.id);

                    // Is currently selected
                    if (idx > -1) {
                      $scope.selection.splice(idx, 1);
                    }

                    // Is newly selected
                    else {
                      $scope.selection.push(s.id);
                    }
                  };

                  $scope.selectAll = function toggleSelection(s) {
                    $scope.selection = [];
                    angular.forEach($scope.sectors, function (value, index) {
                      $scope.selection.push(value.id);
                    });
                  };

                  $scope.unSelectAll = function toggleSelection(s) {
                    $scope.selection = [];
                  };

                  $scope.loadParent = function (childId) {
                    let plan_chain_type = _.findWhere($scope.PlanChainTypes, {
                      id: parseInt(childId),
                    });

                    LoadParentPlanChainService.query(
                      {
                        childId: childId,
                        financialYearId: financialYearId,
                        versionId: versionId,
                      },
                      function (data) {
                        angular.forEach($scope.PlanChainTypes, function (
                          value,
                          key
                        ) {
                          if (value.id == data[0].plan_chain_type_id) {
                            $scope.parent_component = value.name;
                          }
                        });
                        $scope.Parents = data;
                        $scope.is_sectoral = plan_chain_type.is_sectoral;
                      },
                      function (error) {
                        console.log(error);
                      }
                    );
                  };

                  $scope.planChainToCreate = {};
                  $scope.planChainToCreate.plan_chain_type = selectedChainType;
                  $scope.planChainToCreate.sectors = $scope.selection;
                  $scope.planChainToCreate.financialYearId = financialYearId;
                  $scope.planChainToCreate.versionId = versionId;
                  $scope.planChainToCreate.perPage = perPage;
                  if (selectedChainType)
                    $scope.loadParent(selectedChainType.id);
                  $scope.store = function () {
                    $scope.planChainToCreate.sectors = $scope.selection;

                    if ($scope.createPlanChainForm.$invalid) {
                      $scope.formHasErrors = true;
                      return;
                    }

                    CreatePlanChainService.store(
                      $scope.planChainToCreate,
                      function (data) {
                        $scope.planChains = data.planChains;
                        $uibModalInstance.close(data);
                      },
                      function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                      }
                    );
                  };
                  $scope.close = function () {
                    $uibModalInstance.dismiss("cancel");
                  };
                },
              });
              modalInstance.result.then(
                function (data) {
                  //Service to create new financial year
                  $scope.successMessage = data.successMessage;
                  $scope.planChains = data.planChains;
                  $scope.currentPage = data.current_page;
                },
                function () {
                  console.log("Modal dismissed at: " + new Date());
                }
              );
            };

            $scope.edit = function (
              financialYearId,
              versionId,
              planChainTypeId,
              planChainToEdit,
              PlanChainTypes,
              currentPage,
              perPage
            ) {
              let modalInstance = $uibModal.open({
                templateUrl: "/pages/setup/plan_chain/edit.html",
                backdrop: false,

                controller: function (
                  $scope,
                  $uibModalInstance,
                  UpdatePlanChainService,
                  PlanChainSectorOnlyService,
                  LoadParentPlanChainService,
                  SectorsService
                ) {
                  $scope.PlanChainTypes = PlanChainTypes;
                  let plan_chain_type = _.where($scope.PlanChainTypes, {
                    id: parseInt(planChainToEdit.plan_chain_type_id),
                  });
                  $scope.is_sectoral = plan_chain_type[0].is_sectoral;

                  SectorsService.query(
                    {},
                    function (data) {
                      $scope.sectors = data;
                    },
                    function (error) {
                      $scope.errorMessage = error.data.errorMessage;
                    }
                  );

                  $scope.selection = [];

                  PlanChainSectorOnlyService.query({}, function (data) {
                    angular.forEach(data, function (value, index) {
                      if (value.plan_chain_id == planChainToEdit.id) {
                        $scope.selection.push(value.sector_id);
                      }
                    });
                  });

                  // Toggle selection for a given sector by id
                  $scope.toggleSelection = function toggleSelection(s) {
                    var idx = $scope.selection.indexOf(s.id);

                    // Is currently selected
                    if (idx > -1) {
                      $scope.selection.splice(idx, 1);
                    }

                    // Is newly selected
                    else {
                      $scope.selection.push(s.id);
                    }
                  };

                  $scope.selectAll = function toggleSelection(s) {
                    $scope.selection = [];
                    angular.forEach($scope.sectors, function (value, index) {
                      $scope.selection.push(value.id);
                    });
                  };

                  $scope.unSelectAll = function toggleSelection(s) {
                    $scope.selection = [];
                  };

                  //load Parent chain
                  $scope.loadParent = function (childId) {
                    var plan_chain_type = _.where($scope.PlanChainTypes, {
                      id: parseInt(childId),
                    });

                    LoadParentPlanChainService.query(
                      {
                        childId: childId,
                        financialYearId: financialYearId,
                        versionId: versionId,
                      },
                      function (data) {
                        angular.forEach($scope.PlanChainTypes, function (
                          value,
                          key
                        ) {
                          if (
                            data.length &&
                            value.id == data[0].plan_chain_type_id
                          ) {
                            $scope.parent_component = value.name;
                          }
                        });

                        $scope.Parents = data;
                        $scope.is_sectoral = plan_chain_type[0].is_sectoral;
                      },
                      function (error) {
                        //  console.log(error);
                      }
                    );
                  };

                  $scope.planChainToEdit = angular.copy(planChainToEdit);

                  $scope.loadParent($scope.planChainToEdit.plan_chain_type_id);

                  $scope.update = function () {
                    $scope.planChainToEdit.sectors = $scope.selection;
                    $scope.planChainToEdit.financialYearId = financialYearId;
                    $scope.planChainToEdit.versionId = versionId;
                    $scope.planChainToEdit.planChainTypeId = planChainTypeId;

                    if ($scope.updatePlanChainForm.$invalid) {
                      $scope.formHasErrors = true;
                      return;
                    }
                    UpdatePlanChainService.update(
                      {
                        page: currentPage,
                        perPage: perPage,
                      },
                      $scope.planChainToEdit,
                      function (data) {
                        $uibModalInstance.close(data);
                      },
                      function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                      }
                    );
                  };
                  $scope.close = function () {
                    $uibModalInstance.dismiss("cancel");
                  };
                },
              });
              //Called when modal is close by cancel or to store data
              modalInstance.result.then(
                function (data) {
                  $scope.successMessage = data.successMessage;
                  $scope.planChains = data.planChains;
                  $scope.currentPage = $scope.planChains.current_page;
                },
                function () {
                  //If modal is closed
                  console.log("Modal dismissed at: " + new Date());
                }
              );
            };

            $scope.delete = function (
              financialYearId,
              versionId,
              planChainTypeId,
              id,
              currentPage,
              perPage,
              planChainCount
            ) {
              if (planChainCount > 0) {
                return;
              }
              ConfirmDialogService.showConfirmDialog(
                "TITLE_CONFIRM_PLAN_CHAIN",
                "CONFIRM_DELETE"
              ).then(
                function () {
                  DeletePlanChainService.delete(
                    {
                      financialYearId: financialYearId,
                      versionId: versionId,
                      planChainTypeId: planChainTypeId,
                      planChainId: id,
                      currentPage: currentPage,
                      perPage: perPage,
                    },
                    function (data) {
                      $scope.successMessage = data.successMessage;
                      $scope.planChains = data.planChains;
                      $scope.currentPage = $scope.planChains.current_page;
                    },
                    function (error) {
                      $scope.errorMessage = error.data.errorMessage;
                    }
                  );
                },
                function () { }
              );
            };

            $scope.activatePlanChain = function (
              planChainId,
              planChainStatus,
              perPage
            ) {
              $scope.planChainToActivate = {};
              $scope.planChainToActivate.id = planChainId;
              $scope.planChainToActivate.is_active = planChainStatus;
              ActivatePlanChainService.activate(
                {
                  perPage: perPage,
                },
                $scope.planChainToActivate,
                function (data) {
                  $scope.successMessage = false;
                  $scope.errorMessage = false;
                  $scope.activateMessage = data.activateMessage;
                }
              );
            };
          };
        };
      }
    );
  };

  $scope.carry = function (currentFinancialYearId, item) {
    CarryVersionService.showDialog("PC", currentFinancialYearId, item).then(
      function (data) { },
      function () { }
    );
  };
}

PlanChainController.resolve = {
  FinancialYearModel: function (AllFinancialYearService, $q) {
    let deferred = $q.defer();
    AllFinancialYearService.query(function (data) {
      deferred.resolve(data);
    });
    return deferred.promise;
  },
};
