function AssessorAssignmentController($scope,$http, AssessorAssignmentModel, ConfirmDialogService, AllUserService, AllCasAssessmentCategoryVersionService,
                                      AllCASAssessmentRoundService, localStorageService,
                                      AssessorAssignmentService, LevelAdminHierarchyService, GlobalService) {

    $scope.items = AssessorAssignmentModel;
    $scope.title = "ASSESSOR_ASSIGNMENTS";

    $scope.currentPage = 1;
    $scope.perPage = 10;

    $scope.maxSize = 3;

    $scope.pageChanged = function () {
        AssessorAssignmentService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data;
        });
    };

    $scope.admin_hierarchy_id = parseInt(localStorageService.get(localStorageKeys.ADMIN_HIERARCHY_ID));
    $scope.admin_hierarchy_level_id = parseInt(localStorageService.get(localStorageKeys.ADMIN_HIERARCHY_LEVEL_ID));
    $scope.hierarchy_position = parseInt(localStorageService.get(localStorageKeys.HIERARCHY_POSITION));
    $scope.section_id = parseInt(localStorageService.get(localStorageKeys.SECTION_ID));

    $scope.showCreateForm = false;
    $scope.showEditForm = false;
    $scope.showButtons = true;
    $scope.showList = true;

    $scope.create = function () {

        $scope.showCreateForm = true;
        $scope.showEditForm = false;
        $scope.showButtons = false;
        $scope.showList = false;

        GlobalService.levelUsers(function (data) {
            $scope.users = data.users;
        }, function (error) {
            console.log(error);
        });
        //$scope.hierarchy_position = 1;
        //PORALG - LOAD REGIONS
        if ($scope.hierarchy_position === 1) {
            GlobalService.userRegions(function (data) {
                $scope.regions = data.regions;
            });
        }
        //REGION - LOAD DCs
        if ($scope.hierarchy_position === 2) {
            GlobalService.userCouncils(function (data) {
                $scope.councils = data.councils;
            });
        }

        $scope.hasAdminSelection = function (id) {
            var exist = _.where($scope.createDataModel.admin_hierarchy_ids, {admin_hierarchy_id: id});
            if (exist.length > 0) {
                return true;
            } else {
                return false;
            }
        };
        $scope.toggleAdminSelection = function (id) {
            var idx = $scope.createDataModel.admin_hierarchy_ids.indexOf(id);

            // Is currently selected
            if (idx > -1) {
                $scope.createDataModel.admin_hierarchy_ids.splice(idx, 1);
            }
            // Is newly selected
            else {
                $scope.createDataModel.admin_hierarchy_ids.push(id);
            }
        };

        $scope.loadPeriods = function (cas_assessment_category_version_id) {
            AssessorAssignmentService.periods({cas_assessment_category_version_id: cas_assessment_category_version_id}, function (data) {
                $scope.periods = data.periods;
            });
        };

        AllCASAssessmentRoundService.query({}, function (data) {
            $scope.assessment_rounds = data;
        });

        AllCasAssessmentCategoryVersionService.query({}, function (data) {
            $scope.versions = data;
        });

        $scope.createDataModel = {admin_hierarchy_ids: []};

        $scope.getLevelUsers = function (searchText) {
          return $http.get('/json/users/levelUsers?searchText=' + searchText)
              .then(function (response) {
                  return response.data.users;
              });
        };

        $scope.levelUserChange = function(item) {
            if (item !== undefined) {
                $scope.createDataModel.user_id = item.id;
            }
        };

        $scope.store = function () {
            if ($scope.createForm.$invalid) {
                $scope.formHasErrors = true;
                return;
            }
            AssessorAssignmentService.save({perPage: $scope.perPage}, $scope.createDataModel,
                function (data) {
                    $scope.successMessage = data.successMessage;
                    $scope.items = data.items;
                    $scope.currentPage = data.current_page;
                    $scope.closeForm();
                },
                function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                }
            );
        };
    };

    $scope.revealEditForm = function () {
        $scope.showCreateForm = false;
        $scope.showEditForm = true;
        $scope.showButtons = false;
        $scope.showList = false;
    };

    $scope.closeForm = function () {
        $scope.showCreateForm = false;
        $scope.showEditForm = false;
        $scope.showButtons = true;
        $scope.showList = true;
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                AssessorAssignmentService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.items = data.items;
                        $scope.currentPage = $scope.items.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/assessor_assignment/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance, AssessorAssignmentService) {
                AssessorAssignmentService.trashed(function (data) {
                    $scope.trashedAssessorAssignments = data.trashedAssessorAssignments;
                });
                $scope.restoreAssessorAssignment = function (id) {
                    AssessorAssignmentService.restore({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedAssessorAssignments = data.trashedAssessorAssignments;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    AssessorAssignmentService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedAssessorAssignments = data.trashedAssessorAssignments;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    AssessorAssignmentService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedAssessorAssignments = data.trashedAssessorAssignments;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.toggleActive = function (id, active) {
        $scope.toggleData = {};
        $scope.toggleData.id = id;
        $scope.toggleData.active = active;
        AssessorAssignmentService.toggleActive({}, $scope.toggleData, function (data) {
            $scope.action = data.action;
            $scope.alertType = data.alertType;
        });
    }
}

AssessorAssignmentController.resolve = {
    AssessorAssignmentModel: function (AssessorAssignmentService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            AssessorAssignmentService.paginated({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        }, 100);
        return deferred.promise;
    }
};