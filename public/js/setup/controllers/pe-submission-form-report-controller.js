function PeSubmissionFormReportController($scope,
                                          PeReportsModel,
                                          $uibModal,
                                          ConfirmDialogService,
                                          peSubmissionFormReportsService,
                                          GetAllPeReportsService,
                                          BudgetSubmissionSubFormService
                                          ) {

    $scope.reports = PeReportsModel;
    $scope.title = "PE_SUBMISSION_FORM_REPORTS";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        peSubmissionFormReportsService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
            $scope.reports = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/pe_submission_form_report/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,peSubmissionFormReportsService, GetAllPeReportsService, BudgetSubmissionSubFormService) {
                $scope.reportToCreate = {};

                //A service to pull all existing reports
                GetAllPeReportsService.query(function (data) {
                    $scope.pe_report_parents = data;
                    console.log($scope.pe_report_parents);
                });

                BudgetSubmissionSubFormService.query(function (data) {
                    $scope.subforms = data;
                    console.log($scope.subforms);
                });

                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createReportForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    peSubmissionFormReportsService.save({perPage:$scope.perPage},$scope.reportToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                           $scope.errorMessage=error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage=data.successMessage;
                $scope.reports=data.reports;

            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (reportToEdit, currentPage,perPage) {
        
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/pe_submission_form_report/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {

                //A service to pull all existing services
                GetAllPeReportsService.query(function (data) {
                    $scope.pe_report_parents = data;
                });

                BudgetSubmissionSubFormService.query(function (data) {
                    $scope.subforms = data;
                });

                $scope.reportToEdit = angular.copy(reportToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    $uibModalInstance.close($scope.reportToEdit);
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (reportToEdit) {
                //Service to create new unit
                peSubmissionFormReportsService.update({page:currentPage,perPage:perPage},reportToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage=data.successMessage;
                        $scope.reports = data.reports; //After save return all units and update $scope.units
                    },
                    function (error) {

                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Deletion!',
            'Are sure you want to delete this record?')
            .then(function () {
                    console.log("YES");
                    peSubmissionFormReportsService.delete({id: id,currentPage:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage=data.successMessage;
                            $scope.reports = data.reports;
                        }, function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }

    $scope.toggleReport = function (report_id, report_status,perPage) {
        $scope.reportToActivate = {};
        $scope.reportToActivate.id = report_id;
        $scope.reportToActivate.active = report_status;
        peSubmissionFormReportsService.toggleReport({perPage:perPage},$scope.reportToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    }
}

PeSubmissionFormReportController.resolve = {

    PeReportsModel: function (peSubmissionFormReportsService, $q) {
       var deferred = $q.defer();
        peSubmissionFormReportsService.get({page:1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};