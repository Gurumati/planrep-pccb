/**
 * Created by Kachinga on 22/May/2017.
 */
function CalendarEventReminderRecipientController($scope, CalendarEventReminderRecipientModel, $uibModal,
                                                  ConfirmDialogService, CalendarEventReminderRecipientService) {

    $scope.eventRecipients = CalendarEventReminderRecipientModel;
    $scope.title = "CALENDAR_EVENT_REMINDER_RECIPIENTS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        CalendarEventReminderRecipientService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.eventRecipients = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/event_recipient/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllCalendarEventService, CalendarEventReminderRecipientService) {

                AllCalendarEventService.query(function (data) {
                    $scope.calendar_events = data;
                });

                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CalendarEventReminderRecipientService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.eventRecipients = data.eventRecipients;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        //console.log(updateDataModel);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/event_recipient/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllCalendarEventService, CalendarEventReminderRecipientService) {
                AllCalendarEventService.query(function (data) {
                    $scope.calendar_events = data;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });

                $scope.update = function () {
                    CalendarEventReminderRecipientService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.eventRecipients = data.eventRecipients;
                $scope.currentPage = $scope.eventRecipients.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                CalendarEventReminderRecipientService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.eventRecipients = data.eventRecipients;
                        $scope.currentPage = $scope.eventRecipients.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/event_recipient/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance, CalendarEventReminderRecipientService) {
                CalendarEventReminderRecipientService.trashed(function (data) {
                    $scope.trashedEventRecipients = data.trashedEventRecipients;
                });
                $scope.restoreCalendarEventReminderRecipient = function (id) {
                    CalendarEventReminderRecipientService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedEventRecipients = data.trashedEventRecipients;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    CalendarEventReminderRecipientService.permanentDelete({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedEventRecipients = data.trashedEventRecipients;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };

                $scope.emptyTrash = function () {
                    CalendarEventReminderRecipientService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedEventRecipients = data.trashedEventRecipients;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.eventRecipients = data.eventRecipients;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

CalendarEventReminderRecipientController.resolve = {
    CalendarEventReminderRecipientModel: function (CalendarEventReminderRecipientService, $q) {
        var deferred = $q.defer();
        CalendarEventReminderRecipientService.paginated({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};