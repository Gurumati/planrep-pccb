function AccountReturnController($scope,$timeout, $q, DataModel, $uibModal, GlobalService, ExpenditureGfsCodeService,ConfirmDialogService, AccountReturnService) {

    $scope.items = DataModel;
    $scope.title = "ACCOUNT_RETURNS";
    $scope.currentPage = 1;
    $scope.maxSize = 3;

    $scope.showList = true;
    $scope.showCreateBtn = true;
    $scope.showCreateForm = false;
    $scope.showUpdateForm = false;
    $scope.showGfsCodes = false;

    $scope.pageChanged = function () {
        AccountReturnService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data;
        });
    };

    $scope.create = function () {
        $scope.showList = false;
        $scope.showCreateBtn = false;
        $scope.showCreateForm = true;
        $scope.showUpdateForm = false;
        $scope.showGfsCodes = false;

        GlobalService.sectors(function (data) {
            $scope.sectors = data.sectors;
        });

        $scope.loadSections = function (sector_id) {
            GlobalService.filterSectorSections({sector_id: sector_id}, function (data) {
                $scope.sections = data.sections;
            });
        };

        ExpenditureGfsCodeService.query(function (data) {
            $scope.gfs_codes = data;
        });

        $scope.searchTerm;
        $scope.clearSearchTerm = function() {
            $scope.searchTerm = '';
        };

        $scope.formDataModel = {};

        $scope.store = function () {
            AccountReturnService.save({perPage: $scope.perPage}, $scope.formDataModel,
                function (data) {
                    $scope.items = data.items;
                    $scope.successMessage = data.successMessage;
                    $scope.showList = true;
                    $scope.showCreateBtn = true;
                    $scope.showCreateForm = false;
                    $scope.showUpdateForm = false;
                    $scope.showGfsCodes = false;
                },
                function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.errors = error.data.errors;
                }
            );
        };
        $scope.close = function () {
            $scope.showList = true;
            $scope.showCreateBtn = true;
            $scope.showCreateForm = false;
            $scope.showUpdateForm = false;
            $scope.showGfsCodes = false;
        };
    };

    $scope.edit = function (formDataModel, currentPage, perPage) {

        $scope.showList = false;
        $scope.showCreateBtn = false;
        $scope.showCreateForm = false;
        $scope.showUpdateForm = true;
        $scope.showGfsCodes = false;

        ExpenditureGfsCodeService.query(function (data) {
            $scope.gfs_codes = data;
            $scope.formDataModel = angular.copy(formDataModel);
        });

        GlobalService.sectors(function (data) {
            $scope.sectors = data.sectors;
        });

        $scope.loadSections = function (sector_id) {
            GlobalService.filterSectorSections({sector_id:sector_id},function (data) {
                $scope.sections = data.sections;
            });
        };


        $scope.update = function () {
            if ($scope.formData.$invalid) {
                $scope.formHasErrors = true;
                return;
            }
            AccountReturnService.update({page: currentPage, perPage: perPage}, $scope.formDataModel,
                function (data) {
                    $scope.items = data.items;
                    $scope.successMessage = data.successMessage;
                    $scope.showList = true;
                    $scope.showCreateBtn = true;
                    $scope.showCreateForm = false;
                    $scope.showUpdateForm = false;
                    $scope.showGfsCodes = false;
                },
                function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.errors = error.data.errors;
                }
            );
        };
        $scope.close = function () {
            $scope.showList = true;
            $scope.showCreateBtn = true;
            $scope.showCreateForm = false;
            $scope.showUpdateForm = false;
            $scope.showGfsCodes = false;
        }
    };

    $scope.gfsCodes = function (accountReturn, currentPage, perPage) {
        $scope.showList = false;
        $scope.showCreateBtn = false;
        $scope.showCreateForm = false;
        $scope.showUpdateForm = false;
        $scope.showGfsCodes = true;


        $scope.account_return_id = accountReturn.id;
        $scope.account_return_name = accountReturn.name;
        $scope.account_return_code = accountReturn.code;

        AccountReturnService.gfsCodes({account_return_id: $scope.account_return_id}, function (data) {
            $scope.items = data.items;
        });

        $scope.formDataModel = {};

        ExpenditureGfsCodeService.query(function (data) {
            $scope.gfs_codes = data;
        });

        $scope.searchTerm;
        $scope.clearSearchTerm = function() {
            $scope.searchTerm = '';
        };

        $scope.addGfsCodes = function () {
            $scope.formDataModel.account_return_id = $scope.account_return_id;
            AccountReturnService.addGfsCodes($scope.formDataModel,
                function (data) {
                    $scope.items = data.items;
                    $scope.successMessage = data.successMessage;
                },
                function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.errors = error.data.errors;
                }
            );
        };

        $scope.removeGfsCode = function (id) {
            $scope.formDataModel.account_return_id = $scope.account_return_id;
            AccountReturnService.removeGfsCode({
                    id: id,
                    account_return_id: accountReturn.id
                }, $scope.formDataModel,
                function (data) {
                    $scope.items = data.items;
                },
                function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.errors = error.data.errors;
                }
            );
        };
        $scope.close = function () {
            $scope.showList = true;
            $scope.showCreateBtn = true;
            $scope.showCreateForm = false;
            $scope.showUpdateForm = false;
            $scope.showGfsCodes = false;
        }
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                AccountReturnService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.items = data.items;
                        $scope.currentPage = $scope.items.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                        $scope.errors = error.data.errors;
                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
}

AccountReturnController.resolve = {
    DataModel: function (AccountReturnService, $q) {
        var deferred = $q.defer();
        AccountReturnService.paginated({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};