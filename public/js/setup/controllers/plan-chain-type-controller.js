function PlanChainTypeController($scope, PlanChainTypesModel, PaginatedPlanChainTypeService, $uibModal, ConfirmDialogService, DeletePlanChainTypeService, ActivatePlanChainTypeService) {

    $scope.planChainTypes = PlanChainTypesModel;
    $scope.title = "TITLE_PLAN_CHAIN_TYPES";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedPlanChainTypeService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.planChainTypes = data;
        });
    };


    $scope.create = function () {
        var modalInstance = $uibModal.open({

            templateUrl: 'create.html',
          //  templateUrl: '/pages/setup/plan_chain_type/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreatePlanChainTypeService) {
                $scope.planChainTypeToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createPlanChainTypeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreatePlanChainTypeService.store({perPage: $scope.perPage}, $scope.planChainTypeToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.planChainTypes = data.planChainTypes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (planChainTypeToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: 'edit.html',

         //   templateUrl: '/pages/setup/plan_chain_type/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdatePlanChainTypeService) {
                $scope.planChainTypeToEdit = angular.copy(planChainTypeToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updatePlanChainTypeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdatePlanChainTypeService.update({page: currentPage, perPage: perPage}, $scope.planChainTypeToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.planChainTypes = data.planChainTypes;
                $scope.currentPage = $scope.planChainTypes.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog(
            'TITLE_CONFIRM_DELETE_PLAN_CHAIN_TYPES',
            'CONFIRM_DELETE')
            .then(function () {
                    console.log("YES");
                    DeletePlanChainTypeService.delete({planChainTypeId: id, currentPage: currentPage, perPage: perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.planChainTypes = data.planChainTypes;
                            $scope.currentPage = $scope.planChainTypes.current_page;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }

    $scope.activatePlanChainType = function (planChainTypeId, planChainTypeStatus, perPage) {
        $scope.planChainTypeToActivate = {};
        $scope.planChainTypeToActivate.id = planChainTypeId;
        $scope.planChainTypeToActivate.is_active = planChainTypeStatus;
        ActivatePlanChainTypeService.activate({perPage: perPage}, $scope.planChainTypeToActivate,
            function (data) {
                $scope.successMessage = false;
                $scope.errorMessage = false;
                $scope.activateMessage = data.activateMessage;
            });

    }
}


PlanChainTypeController.resolve = {
    PlanChainTypesModel: function (PaginatedPlanChainTypeService, $q,$timeout) {
        var deferred = $q.defer();
        $timeout(function () {
            PaginatedPlanChainTypeService.get({page: 1, perPage: 2}, function (data) {
                deferred.resolve(data);
            });
        },100);
        return deferred.promise;
    }
};