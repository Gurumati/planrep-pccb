function FundTypeController($scope, PaginatedFundTypeModel, $uibModal,
                               ConfirmDialogService,
                               DeleteFundTypeService, PaginatedFundTypeService) {

    $scope.fundTypes = PaginatedFundTypeModel;
    $scope.title = "FUND_TYPES";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedFundTypeService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
            $scope.fundTypes = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/fund_type/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateFundTypeService) {
                $scope.createDataModel = {};
                $scope.checkCode = /^[0-9]{1}$/;
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateFundTypeService.store({perPage:$scope.perPage},$scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.fundTypes = data.fundTypes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
             });

    };

    $scope.edit = function (updateDataModel, currentPage,perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/fund_type/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateFundTypeService) {
                $scope.updateDataModel = angular.copy(updateDataModel);
                //Function to store data and close modal when Create button clicked
              $scope.checkCode = /^[0-9]{1}$/;
                $scope.update = function () {
                    if ($scope.updateForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateFundTypeService.update({page: currentPage,perPage:perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.fundTypes = data.fundTypes;
                $scope.currentPage = $scope.fundTypes.current_page;
            },
            function () {
                //If modal is closed
             });
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog('DELETE_CONFIRMATION', 'Are you sure you want to delete this Record??')
            .then(function () {
                    DeleteFundTypeService.delete({id: id,page:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.fundTypes = data.fundTypes;
                            $scope.currentPage = $scope.fundTypes.current_page;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {

                });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/fund_type/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedFundTypeService,
                                  RestoreFundTypeService, EmptyFundTypeTrashService, PermanentDeleteFundTypeService) {
                TrashedFundTypeService.query(function (data) {
                    $scope.trashedFundTypes = data;
                });
                $scope.restoreFundType = function (id) {
                    RestoreFundTypeService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedFundTypes = data.trashedFundTypes;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteFundTypeService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedFundTypes = data.trashedFundTypes;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyFundTypeTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedFundTypes = data.trashedFundTypes;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.fundTypes = data.fundTypes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

FundTypeController.resolve = {
    PaginatedFundTypeModel: function (PaginatedFundTypeService, $q) {
        var deferred = $q.defer();
        PaginatedFundTypeService.get({page: 1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};
