function BdcMainGroupController($scope, BdcMainGroupModel, $uibModal,
                                     ConfirmDialogService, DeleteBdcMainGroupService, PaginatedBdcMainGroupService) {

    $scope.bdcMainGroups = BdcMainGroupModel;
    $scope.title = "BDC_MAIN_GROUPS";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedBdcMainGroupService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.bdcMainGroups = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bdc_main_group/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllSectorService, CreateBdcMainGroupService) {
                $scope.bdcMainGroupToCreate = {};

                AllSectorService.query(function (data) {
                    $scope.sectors = data;
                });

                $scope.store = function () {
                    if ($scope.createBdcMainGroupForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateBdcMainGroupService.store({perPage: $scope.perPage}, $scope.bdcMainGroupToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errors = error.data.errors;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.bdcMainGroups = data.bdcMainGroups;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (bdcMainGroupToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bdc_main_group/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllSectorService, UpdateBdcMainGroupService) {
                AllSectorService.query(function (data) {
                    $scope.sectors = data;
                    $scope.bdcMainGroupToEdit = angular.copy(bdcMainGroupToEdit);
                });
                $scope.update = function () {
                    if ($scope.updateBdcMainGroupForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateBdcMainGroupService.update({page: currentPage, perPage: perPage}, $scope.bdcMainGroupToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errors = error.data.errors;
                        }
                    );


                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.bdcMainGroups = data.bdcMainGroups;
                $scope.currentPage = $scope.bdcMainGroups.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('CONFIRM_DELETE', 'Are you sure you want to trash this record?').then(function () {
                DeleteBdcMainGroupService.delete({bdc_main_group_id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.bdcMainGroups = data.bdcMainGroups;
                        $scope.currentPage = $scope.bdcMainGroups.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bdc_main_group/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedBdcMainGroupService,
                                  RestoreBdcMainGroupService, EmptyBdcMainGroupTrashService, PermanentDeleteBdcMainGroupService) {
                TrashedBdcMainGroupService.query(function (data) {
                    $scope.trashedBdcMainGroups = data;
                });
                $scope.restoreBdcMainGroup = function (id) {
                    RestoreBdcMainGroupService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBdcMainGroups = data.trashedBdcMainGroups;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteBdcMainGroupService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBdcMainGroups = data.trashedBdcMainGroups;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyBdcMainGroupTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBdcMainGroups = data.trashedBdcMainGroups;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.bdcMainGroups = data.bdcMainGroups;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

BdcMainGroupController.resolve = {
    BdcMainGroupModel: function (PaginatedBdcMainGroupService, $q) {
        var deferred = $q.defer();
        PaginatedBdcMainGroupService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};