function ProjectDataFormQuestionItemValueController($scope,
                                                    ProjectDataFormsService,
                                                    ListFilledProjectDataFormContentsService,
                                                    GetProjectDataFormQuestionsService,
                                                    LoadQuestionValuesService,
                                                    ProjectDataFormQuestionValuesService
) {

    $scope.title = "FILL_PROJECT_DATA_FORM";

    //load project profile data forms
    ProjectDataFormsService.query(function(data){
        $scope.projectDataForms = data;
    }, function (error){
        console.log(error);
    });

    $scope.saveQuestionValues = function () {
      console.log("form data",$scope.form_data);
        ProjectDataFormQuestionValuesService.saveData($scope.form_data, function(data){
            $scope.successMessage = data.successMessage;
        }, function (error) {
            console.log(error);
        });
    };

    $scope.loadContents = function (form_id) {
        $scope.form_id = form_id;
        ListFilledProjectDataFormContentsService.query({form_id: form_id},function (data) {
            $scope.projectDataFormContents = data;
        }, function(error){
            console.log(error);
        });

      ProjectDataFormQuestionValuesService.getProjects({formId:form_id},function(data){
        $scope.projects = data.projects
      }, function(error){
        console.log(error);
      });
    };

    $scope.loadQuestionValues = function(content_id, project_id){
        if (content_id != undefined && content_id != null && project_id != undefined && project_id != undefined){
            LoadQuestionValuesService.query({project_id:project_id, content_id:content_id}, function(data){
                $scope.prepare_variables(data);
                // $scope.questions_values = data;
            }, function (error){
                console.log(error);
            });
        }
    };

    $scope.prepare_variables = function(data){
        var form_data = [];
        for (var i = 0; i < data.length; i=i+1){
            var obj = {};
            obj.project_data_form_question_item_value_id = data[i].id;
            obj.project_data_form_question_id = data[i].project_data_form_question_id;
            obj.project_id = data[i].project_id;
            obj.admin_hierarchy_id = data[i].admin_hierarchy_id;
            obj.financial_year_id = data[i].financial_year_id;
            obj.input_type = data[i].project_data_form_question.input_type;
            obj.description = data[i].project_data_form_question.description;
            obj.question_number = data[i].project_data_form_question.question_number;
            obj.sort_order = data[i].project_data_form_question.sort_order;
            obj.select_options = data[i].project_data_form_question.select_options;
            obj.value = data[i].value;
            form_data.push(obj);
        }
        $scope.form_data = form_data;
    };
}
