function ExpenditureCategoryController ($scope, DataModel, $uibModal,
                                      ConfirmDialogService,ExpenditureCategoryService) {

    $scope.items = DataModel;
    $scope.title = "EXPENDITURE_CATEGORIES";
    $scope.currentPage = 1;
    $scope.maxSize = 3;

    $scope.pageChanged = function () {
        ExpenditureCategoryService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/expenditure_category/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,GlobalService) {

                GlobalService.taskNatures(function (data) {
                    $scope.taskNatures = data.items;
                });

                GlobalService.projectTypes(function (data) {
                    $scope.projectTypes = data.items;
                });

                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    ExpenditureCategoryService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/expenditure_category/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,GlobalService) {

                GlobalService.taskNatures(function (data) {
                    $scope.taskNatures = data.items;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });

                GlobalService.projectTypes(function (data) {
                    $scope.projectTypes = data.items;
                });

                GlobalService.sectors(function (data) {
                    $scope.sectors = data.sectors;
                });

                $scope.update = function () {
                    ExpenditureCategoryService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = $scope.items.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                ExpenditureCategoryService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.items = data.items;
                        $scope.currentPage = $scope.items.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.toggleActive = function(id) {
        ExpenditureCategoryService.toggleActive({id: id}, function(data) {
            $scope.successMessage = data.successMessage;
            $scope.pageChanged();
        });
    };
}

ExpenditureCategoryController .resolve = {
    DataModel: function (ExpenditureCategoryService, $q) {
        var deferred = $q.defer();
        ExpenditureCategoryService.paginated({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};