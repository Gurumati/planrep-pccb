function CasGroupTypesController($scope,
                                 CasGroupTypesModel,
                                 $uibModal,
                                 PaginateCasPlanTableGroupTypesService,
                                 UpdateCasGroupTypeService,
                                 CreateCasGroupTypeService,
                                 DeleteCasGroupTypeService,
                                 ConfirmDialogService,
                                 DeleteUnitService
                                 ) {

    $scope.casGroupTypes = CasGroupTypesModel;
    $scope.title = "CAS_GROUP_TYPES";

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginateCasPlanTableGroupTypesService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
            $scope.casGroupTypes = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/cas_group_types/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,CreateCasGroupTypeService) {
                $scope.casGroupTypeToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createCasGroupTypeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateCasGroupTypeService.store({perPage:$scope.perPage},$scope.casGroupTypeToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                           $scope.errorMessage=error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage=data.successMessage;
                $scope.casGroupTypes=data.casGroupTypes;

            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.edit = function (casGroupTypeToEdit, currentPage,perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/cas_group_types/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {
                $scope.casGroupTypeToEdit = angular.copy(casGroupTypeToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    $uibModalInstance.close($scope.casGroupTypeToEdit);
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (casGroupTypeToEdit) {
                //Service to create new unit
                UpdateCasGroupTypeService.update({page:currentPage,perPage:perPage},casGroupTypeToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage=data.successMessage;
                        $scope.casGroupTypes = data.casGroupTypes; //After save return all units and update $scope.casGroupTypes\
                        $scope.currentPage = $scope.casGroupTypes.current_page;
                    },
                    function (error) {

                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Deletion!',
            'Are sure you want to delete this record?')
            .then(function () {
                    console.log("YES");
                    DeleteCasGroupTypeService.delete({id: id,currentPage:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage=data.successMessage;
                            $scope.casGroupTypes = data.casGroupTypes;
                        }, function (error) {
                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }

}

CasGroupTypesController.resolve = {

    CasGroupTypesModel: function (PaginateCasPlanTableGroupTypesService, $q) {
        var deferred = $q.defer();
        PaginateCasPlanTableGroupTypesService.get({page:1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};