function CeilingChainController($scope, CeilingChainModel, $uibModal, ConfirmDialogService,CeilingChainService) {

    $scope.ceilingChains = CeilingChainModel;
    $scope.title = "CEILING_CHAIN";
    console.log(CeilingChainModel);

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        CeilingChainService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.ceilingChains = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/ceiling_chain/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CeilingChainService,AllAdminHierarchyLevelService,AllSectionLevelService,AllCeilingChainService) {

                AllAdminHierarchyLevelService.query(function (data) {
                    $scope.hierarchyLevels = data;
                });

                AllSectionLevelService.query(function (data) {
                    $scope.sectionLevels = data;
                });

                AllCeilingChainService.query(function (data) {
                    $scope.ceilingChains = data;
                });

                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CeilingChainService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.ceilingChains = data.ceilingChains;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        //console.log(updateDataModel);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/ceiling_chain/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllAdminHierarchyLevelService,AllSectionLevelService,AllCeilingChainService, CeilingChainService) {

                AllAdminHierarchyLevelService.query(function (data) {
                    $scope.hierarchyLevels = data;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });

                AllSectionLevelService.query(function (data) {
                    $scope.sectionLevels = data;
                });

                AllCeilingChainService.query(function (data) {
                    $scope.ceilingChains = data;
                });
                $scope.update = function () {
                    CeilingChainService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.ceilingChains = data.ceilingChains;
                $scope.currentPage = $scope.ceilingChains.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                CeilingChainService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.ceilingChains = data.ceilingChains;
                        $scope.currentPage = $scope.ceilingChains.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/ceiling_chain/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, CeilingChainService) {
                CeilingChainService.trashed(function (data) {
                    $scope.trashedCeilingChains = data.trashedCeilingChains;
                });
                $scope.restoreCeilingChain = function (id) {
                    CeilingChainService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCeilingChains = data.trashedCeilingChains;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    CeilingChainService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCeilingChains = data.trashedCeilingChains;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    CeilingChainService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCeilingChains = data.trashedCeilingChains;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.ceilingChains = data.ceilingChains;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

CeilingChainController.resolve = {
    CeilingChainModel: function (CeilingChainService,$timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            CeilingChainService.paginated({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        }, 900);

        return deferred.promise;
    }
};