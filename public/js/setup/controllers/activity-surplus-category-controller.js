function SurplusCategoryController($scope, DataModel, $uibModal,
                                   ConfirmDialogService,SurplusCategoryService) {
  $scope.items = DataModel;
  $scope.title = "ACTIVITY_SURPLUS_CATEGORIES";
  $scope.currentPage = 1;
  $scope.maxSize = 3;
  $scope.pageChanged = function () {
    SurplusCategoryService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
      $scope.items = data;
    });
  };
//switch
  $scope.catStatusChange = function (item) {
    SurplusCategoryService.catStatusChange({id:item.id,is_active:item.is_active}, function (data) {
      $scope.items = data;
      $scope.successMessage = 'SURPLUS_CATEGORY_UPDATED_SUCCESSFULLY';
    },function (error) {

    });
  };
  $scope.create = function () {
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/surplus/create.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance,SurplusCategoryService) {
        $scope.store = function () {
          if ($scope.createSurplusCategoryForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          SurplusCategoryService.save({perPage: $scope.perPage}, $scope.categoryToCreate,
            function (data) {
              $uibModalInstance.close(data);

            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    modalInstance.result.then(function (data) {
        $scope.successMessage = 'SURPLUS_CATEGORY_CREATED_SUCCESSFULLY';
        $scope.items = data;
        $scope.currentPage = data.current_page;
      },
      function () {
        console.log('Modal dismissed at: ' + new Date());
      });

  };
  $scope.edit = function (updateDataModel, currentPage, perPage) {

    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/surplus/edit.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance,SurplusCategoryService) {
        $scope.categoryToEdit = angular.copy(updateDataModel);
        $scope.update = function () {
          SurplusCategoryService.update({page: currentPage, perPage: perPage}, $scope.categoryToEdit,
            function (data) {
              //Successful function when
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
        //Service to create new financial year
        $scope.successMessage = 'SURPLUS_CATEGORY_UPDATED_SUCCESSFULLY';
        $scope.items = data;
        $scope.currentPage = $scope.items.current_page;
      },
      function () {
        //If modal is closed
        console.log('Modal dismissed at: ' + new Date());
      });
  };

  $scope.delete = function (id, currentPage, perPage) {
    ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
        SurplusCategoryService.delete({id: id, page: currentPage, perPage: perPage},
          function (data) {
            $scope.successMessage = 'SURPLUS_CATEGORY_DELETED';
            $scope.items = data;
            $scope.currentPage = $scope.items.current_page;
          }, function (error) {
            $scope.errorMessage = error.data.errorMessage;
          }
        );
      },
      function () {
        console.log("NO");
      });
  };

}
SurplusCategoryController .resolve = {
  DataModel: function (SurplusCategoryService, $q) {
    var deferred = $q.defer();
    SurplusCategoryService.paginated({page: 1, perPage: 10}, function (data) {
      deferred.resolve(data);
    });
    return deferred.promise;
  }
};
