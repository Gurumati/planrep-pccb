function SectorProblemController($scope, $uibModal, SectorProblemModel, ConfirmDialogService, DeleteSectorProblemService,
                                 PaginatedSectorProblemService, ToggleSectorProblemService) {

    $scope.sectorProblems = SectorProblemModel;
    $scope.title = "SECTOR_PROBLEMS";
    $scope.currentPage = 1;
    $scope.maxSize = 3;

    $scope.pageChanged = function () {
        PaginatedSectorProblemService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.sectorProblems = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/sector_problem/create.html',
            backdrop: false,
            controller: function ($scope, GlobalService, $uibModalInstance, CreateSectorProblemService, PriorityArea) {

                PriorityArea.getCurrentVersion(function (data) {
                    $scope.priority_areas = data.priorityAreas;
                });

                $scope.genericLoaded = false;
                $scope.paramSet = false;

                $scope.sectorProblemToCreate = {};

                $scope.loadGenerics = function (priorityAreaId) {
                    $scope.genericLoaded = false;
                    $scope.paramSet = false;
                    $scope.loadingGenericTemplates = true;
                    GlobalService.genericSectorProblemsByPriority({priorityAreaId: priorityAreaId}, function (data) {
                        $scope.genericSectorProblems = data.genericSectorProblems;
                        $scope.genericLoaded = true;
                        $scope.loadingGenericTemplates = false;
                        if ($scope.genericSectorProblems.length > 0) {

                        }
                        else {
                            $scope.paramSet = true;
                        }
                    });

                    GlobalService.spnumber({priority_area_id: priorityAreaId}, function (data) {
                        $scope.spnumber = data.spnumber;
                        $("#spnumber").val(data.spnumber);
                    });
                };
                $scope.showParam = function (selectedGenericSectorProblem) {
                    console.log(selectedGenericSectorProblem);
                    $scope.selectedGenericSectorProblemParams = [];
                    var params = selectedGenericSectorProblem.params.split(",");
                    angular.forEach(params, function (p) {
                        var x = {};
                        x.name = p;
                        x.value = undefined;
                        $scope.selectedGenericSectorProblemParams.push(x);
                    });
                    console.log($scope.selectedGenericSectorProblemParams);
                };

                $scope.createFromTemplate = function (template, params) {
                    var description = template.description;
                    angular.forEach(params, function (p) {
                        var replace = p.name;
                        var re = new RegExp(replace, "g");
                        description = description.replace(re, p.value);
                    });
                    console.log(description);
                    $scope.sectorProblemToCreate.description = description;
                    $scope.paramSet = true;
                };

                $scope.store = function () {
                    if ($scope.createSectorProblemForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateSectorProblemService.store({perPage: $scope.perPage}, $scope.sectorProblemToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.sectorProblems = data.sectorProblems;
                $scope.currentPage = data.current_page;
            },
            function () {
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (sectorProblemToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/sector_problem/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, PriorityArea, UpdateSectorProblemService) {
                $scope.sectorProblemToEdit = sectorProblemToEdit;
               
                PriorityArea.getCurrentVersion(function (data) {
                    $scope.priority_areas = data.priorityAreas;
                });

                $scope.update = function () {
                    UpdateSectorProblemService.update({page: currentPage, perPage: perPage}, $scope.sectorProblemToEdit,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.sectorProblems = data.sectorProblems;
                $scope.currentPage = $scope.sectorProblems.current_page;
            },
            function () {
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteSectorProblemService.delete({sector_problem_id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.sectorProblems = data.sectorProblems;
                        $scope.currentPage = $scope.sectorProblems.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/sector_problem/trash.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, TrashedSectorProblemService,
                                  RestoreSectorProblemService, EmptySectorProblemTrashService, PermanentDeleteSectorProblemService) {
                TrashedSectorProblemService.query(function (data) {
                    $scope.trashedSectorProblems = data;
                });
                $scope.restoreSectorProblem = function (id) {
                    RestoreSectorProblemService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedSectorProblems = data.trashedSectorProblems;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteSectorProblemService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedSectorProblems = data.trashedSectorProblems;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptySectorProblemTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedSectorProblems = data.trashedSectorProblems;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.sectorProblems = data.sectorProblems;
                $scope.currentPage = data.current_page;
            },
            function () {
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.toggleActive = function (id, is_active, perPage) {
        $scope.sectorProblemToActivate = {};
        $scope.sectorProblemToActivate.id = id;
        $scope.sectorProblemToActivate.is_active = is_active;
        ToggleSectorProblemService.toggleActive({perPage: perPage}, $scope.sectorProblemToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    };
}

SectorProblemController.resolve = {
    SectorProblemModel: function (PaginatedSectorProblemService, $q) {
        var deferred = $q.defer();
        PaginatedSectorProblemService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};