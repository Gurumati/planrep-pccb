function FixActivityCodeController($scope,ActivityWithInvalidCode) {
  $scope.title = "Fix Activity Code";

    var loadInvalidChain = function () {
        ActivityWithInvalidCode.totalByInvalidChainCode(function (data) {
            $scope.totalByInvalidChain = data.total;
        });
    };
    loadInvalidChain();

    var loadInvalid = function () {
        ActivityWithInvalidCode.get(function (data) {
            $scope.total = data.total;
        });
    };
    loadInvalid();

    var loadInvalidDuplicate = function () {
        ActivityWithInvalidCode.totalByDuplicateCode(function (data) {
            $scope.totalByDuplicateCode = data.total;
        });
    };
    loadInvalidDuplicate();

    $scope.limitByInvalidChain = 100;
    $scope.fixByInvalidChainCode = function () {
        $scope.successMessageByInvalidChain = undefined;
        $scope.errorMessageByInvalidChain = undefined;
        ActivityWithInvalidCode.fixByInvalidChainCode({limit:$scope.limitByInvalidChain},function (data) {
            $scope.totalByInvalidChain = data.total;
            $scope.processedByInvalidChain = data.processed;
            $scope.successMessageByInvalidChain= "Processed";
            loadInvalid();
            loadInvalidDuplicate();
        },function (error) {
            console.log(error);
            $scope.errorMessageByInvalidChain="Error processing";
        });
    };

    $scope.limit = 100;
    $scope.fixCode = function () {
        $scope.successMessage = undefined;
        $scope.errorMessage = undefined;
        ActivityWithInvalidCode.fixCode({limit:$scope.limit},function (data) {
               $scope.total = data.total;
               $scope.processed = data.processed;
               $scope.successMessage= "Processed";
                loadInvalidChain();
                loadInvalidDuplicate();
        },function (error) {
            console.log(error);
            $scope.errorMessage="Error processing";
        });
    };

    $scope.limitByDuplicateCode = 100;
    $scope.fixByDuplicateCode = function () {
        $scope.successMessageByDuplicateCode = undefined;
        $scope.errorMessageByDuplicateCode = undefined;
        ActivityWithInvalidCode.fixByDuplicateCode({limit:$scope.limitByDuplicateCode},function (data) {
               $scope.totalByDuplicateCode = data.total;
               $scope.processedByDuplicateCode = data.processed;
               $scope.successMessageByDuplicateCode= "Processed";
               loadInvalid();
               loadInvalidChain();
        },function (error) {
            console.log(error);
            $scope.errorMessageByDuplicateCode="Error processing";
        });
    };

}
