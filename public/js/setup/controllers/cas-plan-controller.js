function CasPlanController($scope,
                           CasPlanModel,
                           SectorsService,
                           CasPlanService,
                           $uibModal,
                           CreateCsaPlanService,
                           DeleteCasPlanService,
                           AllAdminHierarchyLevelsService,
                           ConfirmDialogService
) {

    $scope.casPlans = CasPlanModel;
    $scope.title = "CAS_PLANS";
    $scope.dateFormat = 'yyyy-MM-DD';
    
    $scope.create = function () {
        //Modal Form for creating sector

        var modalInstance = $uibModal.open({
            templateUrl: 'create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {
                //load sectors
                SectorsService.query({}, function (data) {
                    $scope.sectors = data;
                });

                CasPlanService.query({}, function (data) {
                    $scope.casPlans = data;
                });

                AllAdminHierarchyLevelsService.query({}, function (data) {
                    $scope.adminHierarchyLevels = data;
                });

                $scope.financial_year_types = [{code: 'planning', name: 'planning'}, {code: 'execution', name: 'execution'}];
                $scope.casPlanToCreate = {};
                //store data
                $scope.store = function () {
                    if ($scope.createCasPlanForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateCsaPlanService.store($scope.casPlanToCreate,
                        function (data) {
                            $scope.casPlans = data.casPlans;
                            $scope.successMessage = data.successMessage;
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                };

                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.casPlans = data.casPlans;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.edit = function (casPlanToEdit) {
        var modalInstance = $uibModal.open({
            templateUrl: 'edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateCasPlanService) {
                $scope.financial_year_types = [{code: 'planning', name: 'planning'}, {code: 'execution', name: 'execution'}];
                //load sectors
                SectorsService.query({}, function (data) {
                    $scope.sectors = data;
                });

                CasPlanService.query({}, function (data) {
                    $scope.casPlans = data;
                });

                AllAdminHierarchyLevelsService.query({}, function (data) {
                    $scope.adminHierarchyLevels = data;
                });

                $scope.casPlanToEdit = angular.copy(casPlanToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updateCasPlanForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateCasPlanService.edit($scope.casPlanToEdit,
                        function (data) {
                            //Successful function when
                            $scope.casPlans = data.casPlans;
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.casPlans = data.casPlans;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'TITLE_CONFIRM_DELETE_CAS_PLAN',
            'CONFIRM_DELETE')
            .then(function () {

                    DeleteCasPlanService.delete({casPlanId: id},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.casPlans = data.casPlans;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }
};

CasPlanController.resolve = {

    CasPlanModel: function (CasPlanService, $q) {

        var deferred = $q.defer();
        CasPlanService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};