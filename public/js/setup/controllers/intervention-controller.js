function InterventionController($scope, FinancialYearModel, FinancialYearService, $uibModal,
                                ConfirmDialogService, InterventionService,CarryVersionService) {

    $scope.financialYears = FinancialYearModel;
    $scope.title = "INTERVENTIONS";

    $scope.loadVersions = function (financialYearId) {

        $scope.financialYearId = financialYearId;

        FinancialYearService.versions({id: financialYearId, type: 'INTERVENTION'}, function (response) {
            $scope.versions = response.data;
            $scope.loadInterventions = function (versionId) {

                $scope.versionId = versionId;

                InterventionService.paginated({
                    page: 1,
                    perPage: 10,
                    financialYearId: financialYearId,
                    versionId: versionId,
                }, function (data) {
                    $scope.interventions = data;
                });

                $scope.currentPage = 1;
                $scope.perPage = 10;
                $scope.maxSize = 3;

                $scope.pageChanged = function () {
                    InterventionService.paginated({
                        page: $scope.currentPage,
                        perPage: $scope.perPage,
                        financialYearId: financialYearId,
                        versionId: versionId,
                    }, function (data) {
                        $scope.interventions = data;
                    });
                };

                $scope.create = function () {
                    var modalInstance = $uibModal.open({
                        templateUrl: '/pages/setup/intervention/create.html',
                        backdrop: false,
                        controller: function ($scope, $uibModalInstance, AllFinancialYearService, AllLinkLevelService, GlobalService, InterventionService, AllParentBudgetClassService, LoadSubBudgetClassService, PriorityAreaService) {

                            GlobalService.parentInterventionCategories(function (data) {
                                $scope.parentInterventionCategories = data.items;
                            });

                            $scope.loadChildren = function (parent_id) {
                                GlobalService.childrenInterventionCategories({parent_id: parent_id}, function (data) {
                                        $scope.childrenInterventionCategories = data.items;
                                    }, function (error) {
                                        console.log(error);
                                    }
                                );
                            };

                            AllFinancialYearService.query(function (data) {
                                $scope.financialYears = data;
                                $scope.loadVersions = function (financialYearId) {
                                    FinancialYearService.versions({
                                        id: financialYearId,
                                        type: 'PRIORITY_AREA'
                                    }, function (response) {
                                        $scope.versions = response.data;
                                        $scope.loadPriorityAreas = function (versionId) {
                                            PriorityAreaService.fetchAll({
                                                financialYearId: financialYearId,
                                                versionId: versionId
                                            }, function (response) {
                                                $scope.priority_areas = response.data;
                                            });
                                        };
                                    });
                                };
                            });

                            AllLinkLevelService.query(function (data) {
                                $scope.linkLevels = data;
                            });

                            AllParentBudgetClassService.query(function (data) {
                                $scope.budgetClasses = data;
                            });

                            $scope.loadSubBudgetClasses = function (id) {
                                LoadSubBudgetClassService.get({id: id}, function (data) {
                                        $scope.subBudgetClasses = data.subBudgetClasses;
                                    }, function (error) {
                                        $scope.errorMessage = error.data.errorMessage;
                                    }
                                );
                            };

                            $scope.interventionToCreate = {};

                            $scope.store = function () {
                                if ($scope.createInterventionForm.$invalid) {
                                    $scope.formHasErrors = true;
                                    return;
                                }
                                $scope.interventionToCreate.perPage = $scope.perPage;
                                $scope.interventionToCreate.financialYearId = financialYearId;
                                $scope.interventionToCreate.versionId = versionId;
                                InterventionService.save($scope.interventionToCreate,
                                    function (data) {
                                        $uibModalInstance.close(data);
                                    },
                                    function (error) {
                                        $scope.errorMessage = error.data.errorMessage;
                                    }
                                );
                            };
                            $scope.close = function () {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    });
                    //Called when modal is close by cancel or to store data
                    modalInstance.result.then(function (data) {
                            //Service to create new financial year
                            $scope.successMessage = data.successMessage;
                            $scope.interventions = data.interventions;
                            $scope.currentPage = data.current_page;
                        },
                        function () {
                            //If modal is closed
                            console.log('Modal dismissed at: ' + new Date());
                        });

                };

                $scope.copy = function (financialYear, version) {
                    let modalInstance = $uibModal.open({
                        templateUrl: '/pages/setup/intervention/copy.html',
                        backdrop: false,
                        controller: function ($scope, $uibModalInstance, AllFinancialYearService, InterventionService, PriorityAreaService, FinancialYearService) {

                            $scope.sourceFinancialYear = financialYear;
                            $scope.sourceVersion = version;

                            AllFinancialYearService.query(function (data) {
                                $scope.financialYears = data;

                                $scope.loadFinancialYearVersions = function (financialYearId) {
                                    FinancialYearService.otherVersions({
                                        financialYearId: financialYearId,
                                        currentVersionId: versionId,
                                        type: 'INTERVENTION',
                                        currentFinancialYearId: $scope.sourceFinancialYear.id,
                                    }, function (response) {
                                        $scope.financialYearVersions = response.data;
                                        $scope.copyInterventions = function (sourceFinancialYearId, sourceVersionId, destinationVersionId) {
                                            InterventionService.copyInterventions({
                                                sourceVersionId: sourceVersionId,
                                                destinationVersionId: destinationVersionId,
                                                sourceFinancialYearId: sourceFinancialYearId
                                            }, function (response) {
                                                $uibModalInstance.close(response);
                                            });
                                        };
                                    });
                                };
                            });

                            $scope.close = function () {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    });
                    //Called when modal is close by cancel or to store data
                    modalInstance.result.then(function (response) {
                            $scope.successMessage = response.message;
                        },
                        function () {

                        });

                };

                $scope.edit = function (interventionToEdit, currentPage, perPage) {
                    let modalInstance = $uibModal.open({
                        templateUrl: '/pages/setup/intervention/edit.html',
                        backdrop: false,
                        controller: function ($scope, $uibModalInstance, AllFinancialYearService, FinancialYearService, PriorityAreaService, InterventionService, AllLinkLevelService, GlobalService, EntireTreeInterventionCategoryService, AllParentBudgetClassService, LoadSubBudgetClassService, AllPriorityAreaService) {

                            GlobalService.parentInterventionCategories(function (data) {
                                $scope.parentInterventionCategories = data.items;
                                $scope.interventionToEdit = angular.copy(interventionToEdit);
                            });

                            $scope.loadChildren = function (parent_id) {
                                GlobalService.childrenInterventionCategories({parent_id: parent_id}, function (data) {
                                        $scope.childrenInterventionCategories = data.items;
                                    }, function (error) {
                                        console.log(error);
                                    }
                                );
                            };

                            AllFinancialYearService.query(function (data) {
                                $scope.financialYears = data;
                                $scope.loadVersions = function (financialYearId) {
                                    FinancialYearService.versions({
                                        id: financialYearId,
                                        type: 'PRIORITY_AREA'
                                    }, function (response) {
                                        $scope.versions = response.data;
                                        $scope.loadPriorityAreas = function (versionId) {
                                            PriorityAreaService.fetchAll({
                                                financialYearId: financialYearId,
                                                versionId: versionId
                                            }, function (response) {
                                                $scope.priority_areas = response.data;
                                            });
                                        };
                                    });
                                };
                            });

                            AllParentBudgetClassService.query(function (data) {
                                $scope.budgetClasses = data;
                            });

                            AllLinkLevelService.query(function (data) {
                                $scope.linkLevels = data;
                            });

                            $scope.loadSubBudgetClasses = function (id) {
                                LoadSubBudgetClassService.get({id: id}, function (data) {
                                        $scope.subBudgetClasses = data.subBudgetClasses;
                                    }, function (error) {
                                        $scope.errorMessage = error.data.errorMessage;
                                    }
                                );
                            };
                            $scope.update = function () {

                                $scope.interventionToEdit.perPage = perPage;
                                $scope.interventionToEdit.currentPage = currentPage;
                                $scope.interventionToEdit.financialYearId = financialYearId;
                                $scope.interventionToEdit.versionId = versionId;

                                InterventionService.update({id: $scope.interventionToEdit.id}, $scope.interventionToEdit,
                                    function (data) {
                                        $uibModalInstance.close(data);
                                    },
                                    function (error) {
                                        $scope.errorMessage = error.data.errorMessage;
                                    }
                                );
                            };
                            $scope.close = function () {
                                $uibModalInstance.dismiss('cancel');
                            };
                        }
                    });
                    modalInstance.result.then(function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.interventions = data.interventions;
                            $scope.currentPage = $scope.interventions.current_page;
                        },
                        function () {
                            //If modal is closed
                            console.log('Modal dismissed at: ' + new Date());
                        });
                };

                $scope.delete = function (id, currentPage, perPage) {
                    ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                            InterventionService.delete({
                                    id: id,
                                    currentPage: currentPage,
                                    perPage: perPage,
                                    financialYearId: financialYearId,
                                    versionId: versionId
                                }, function (data) {
                                    $scope.successMessage = data.successMessage;
                                    $scope.interventions = data.interventions;
                                    $scope.currentPage = $scope.interventions.current_page;
                                }, function (error) {
                                    $scope.errorMessage = error.data.errorMessage;
                                }
                            );
                        },
                        function () {
                            console.log("NO");
                        });
                };

                $scope.toggleActive = function (id, active, perPage, currentPage) {
                    InterventionService.active({
                        id: id,
                        active: active,
                        perPage: perPage,
                        currentPage: currentPage,
                        financialYearId: financialYearId,
                        versionId: versionId
                    }, function (data) {
                        $scope.successMessage = data.message;
                    });
                };

                $scope.togglePrimary = function (id, primary, perPage, currentPage) {
                    InterventionService.primary({
                        id: id,
                        primary: primary,
                        perPage: perPage,
                        currentPage: currentPage,
                        financialYearId: financialYearId,
                        versionId: versionId
                    }, function (data) {
                        $scope.successMessage = data.message;
                    });
                };
            };
        });
    };

    $scope.carry = function (currentFinancialYearId, item) {
        CarryVersionService.showDialog('IN',currentFinancialYearId, item).then(function(data){
            console.log(data);
        }, function(){
            
        });
    };
}

InterventionController.resolve = {
    FinancialYearModel: function (AllFinancialYearService, $q) {
        let deferred = $q.defer();
        AllFinancialYearService.query(function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};