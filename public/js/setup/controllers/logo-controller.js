function LogoController($scope, $timeout, $uibModal, ConfirmDialogService, PicsLogo, LogoService) {


    $scope.title = "Bureau_LOGO_MANAGEMENT";
    $scope.PicsLogos = PicsLogo.piscs;
    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.perPage = 10;

    $scope.pageChanged = function () {
        LogoService.paginated({
            onlyActive: true,
            page: $scope.currentPage, perPage: $scope.perPage
        }, function (data) {
            $scope.PicsLogos = data.piscs;
        });
    }

    $scope.addLogo = function (data) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/logo/add_logo.html',
            backdrop: false,
            windowClass: 'center-modal',
            controller: function ($scope, LogoService, $http, $uibModalInstance, CreateFacilityService, GlobalService) {
                $scope.piscs = data
                /// console.log($scope.piscs.id)
                $scope.reference_file_names = [];
                $scope.logoToCreate = {};
                $scope.logoToCreate.urls = [];
                $scope.logoToCreate.admin_hierarchy_id = $scope.piscs.id;

                $scope.remove = function (filename) {
                    $scope.PreviewImage = null
                    $scope.showImage = false

                    var index = $scope.reference_file_names.indexOf(filename);
                    LogoService.removeFile({ "file_url": $scope.logoToCreate.urls[index] },
                        function (response) {
                        },
                        function (error) {

                        });

                    $scope.reference_file_names.splice(index, 1);
                    $scope.logoToCreate.urls.splice(index, 1);
                    //$scope.logoToCreate = null
                };


                $scope.SelectFile = function (e) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $scope.PreviewImage = e.target.result;
                        $scope.$apply();
                    };

                    reader.readAsDataURL(e.target.files[0]);
                    if (e.target.files[0].type.indexOf("image") !== -1) {
                        $scope.name = e.target.files[0].name;
                        $scope.type = e.target.files[0].type;
                        $scope.size = ((e.target.files[0].size / (1024 * 1024)) > 1) ? (e.target.files[0].size / (1024 * 1024)).toFixed(2) + " MB" : (e.target.files[0].size / 1024).toFixed(2) + " KB";
                        $scope.realImageSize = e.target.files[0].size;
                        $scope.showImage = true
                        $scope.buttonDisable = false;
                        $scope.errorMessage = null
                        //uploading Process 

                        if ($scope.realImageSize <= 102400) {
                            var formData = new FormData();
                            formData.append('file', e.target.files[0]);
                            $scope.reference_file_names.push(e.target.files[0].name);
                            uploadUrl = "/api/upload-logo";

                            $http.post(uploadUrl, formData, {
                                withCredentials: true,
                                headers: { 'Content-Type': undefined },
                                transformRequest: angular.identity
                            }).then(
                                function (response) {
                                    if (response.data == 0) {
                                        $scope.reference_file_names.pop();
                                        $scope.errorMessage = "Something went wrong";
                                    } else {
                                        $scope.logoToCreate.urls.push({ ad_url: response.data });
                                    }
                                },
                                function (e) {

                                    $scope.reference_file_names.pop();
                                    //$scope.errorMessage = error.data.errorMessage;
                                    console.log(e);
                                });
                            ///end of Upload
                        } else {
                            alert("Unsupported Image size, will not be saved")
                            // $uibModalInstance.dismiss('cancel');
                        }

                    } else {

                        // $uibModalInstance.dismiss('cancel');
                        $scope.showImage = false
                        $scope.errorMessage = "Unsupported file type";
                        $scope.buttonDisable = true;
                    }

                };

                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.store = function () {
                    if ($scope.logoForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }

                    $scope.logoToCreate.realImageSize = $scope.realImageSize;
                    LogoService.save({ perPage: $scope.perPage }, $scope.logoToCreate,
                        function (data) {
                            if (data.errorMessage) {
                                $scope.errorMessage = data.errorMessage;
                            } else {
                                $uibModalInstance.close(data);
                            }

                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                }

            }

        });

        modalInstance.result.then(function (response) {
            //Service to create new financial year
            $scope.successMessage = response.successMessage;
            // $scope.items = response.items;
            $scope.PicsLogos = response.piscs;
            $scope.currentPage = response.piscs.current_page;
        },
            function () {

            });
    }


    $scope.viewLogo = function (data) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/logo/view.html',
            backdrop: false,
            windowClass: 'center-modal',
            controller: function ($scope, $uibModalInstance) {
                $scope.piscs = data

                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });

    }

    $scope.delete = function (id, path, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {

            LogoService.delete({ id: id, path: path, page: currentPage, perPage: perPage },
                function (data) {
                    $scope.successMessage = data.successMessage;
                    $scope.PicsLogos = data.piscs;
                    $scope.currentPage = data.piscs.current_page;
                }, function (error) {
                    $scope.errorMessage = data.errorMessage;
                }
            );
        },
            function () {
                console.log("NO");
            });
    };

}

LogoController.resolve = {

    PicsLogo: function (LogoService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            LogoService.fetchAll({ page: 1, perPage: 10 }, function (data) {
                deferred.resolve(data);
            });
        }, 900);
        return deferred.promise;
    }
};