function CalendarEventController($scope, CalendarEventModel, $uibModal,
                                 ConfirmDialogService, CalendarEventService) {

    $scope.calendarEvents = CalendarEventModel;
    $scope.title = "TITLE_CALENDAR_EVENTS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        CalendarEventService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.calendarEvents = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/calendar_event/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CalendarEventService) {

                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CalendarEventService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.calendarEvents = data.calendarEvents;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        //console.log(updateDataModel);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/calendar_event/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CalendarEventService) {

                $scope.updateDataModel = angular.copy(updateDataModel);

                $scope.update = function () {
                    CalendarEventService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.calendarEvents = data.calendarEvents;
                $scope.currentPage = $scope.calendarEvents.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                CalendarEventService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.calendarEvents = data.calendarEvents;
                        $scope.currentPage = $scope.calendarEvents.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/calendar_event/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance, CalendarEventService) {
                CalendarEventService.trashed(function (data) {
                    $scope.trashedCalendarEvents = data.trashedCalendarEvents;
                });
                $scope.restoreCalendarEvent = function (id) {
                    CalendarEventService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCalendarEvents = data.trashedCalendarEvents;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    CalendarEventService.permanentDelete({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCalendarEvents = data.trashedCalendarEvents;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    CalendarEventService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedCalendarEvents = data.trashedCalendarEvents;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.calendarEvents = data.calendarEvents;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

CalendarEventController.resolve = {
    CalendarEventModel: function (CalendarEventService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            CalendarEventService.paginated({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        }, 100);
        return deferred.promise;
    }
};