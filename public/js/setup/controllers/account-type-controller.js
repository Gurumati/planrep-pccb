function AccountTypeController($scope, GetAllAccountTypeModel, $uibModal,
                               ConfirmDialogService,
                               DeleteAccountTypeService, GetAllAccountTypeService) {

    $scope.accountTypes = GetAllAccountTypeModel;
    $scope.title = "TITLE_ACCOUNT_TYPES";

    $scope.orderByField = 'name';
    $scope.reverseSort = false;

    $scope.currentPage = 1;
    $scope.maxSize = 3;

    $scope.pageChanged = function () {
        GetAllAccountTypeService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.accountTypes = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/account_type/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateAccountTypeService) {
                $scope.accountTypeToCreate = {};

                $scope.balance_types = ["DR", "CR"];
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createAccountTypeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateAccountTypeService.store({perPage: $scope.perPage}, $scope.accountTypeToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.accountTypes = data.accountTypes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.edit = function (accountTypeToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/account_type/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateAccountTypeService) {
                $scope.accountTypeToEdit = angular.copy(accountTypeToEdit);
                $scope.balance_types = ["DR", "CR"];
                $scope.update = function () {
                    if ($scope.updateAccountTypeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateAccountTypeService.update({page: currentPage, perPage: perPage}, $scope.accountTypeToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.accountTypes = data.accountTypes;
                $scope.currentPage = $scope.accountTypes.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Account Type Delete', 'Are you sure you want to delete this record').then(function () {
                DeleteAccountTypeService.delete({account_type_id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.accountTypes = data.accountTypes;
                        $scope.currentPage = $scope.accountTypes.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/account_type/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedAccountTypeService,
                                  RestoreAccountTypeService, EmptyAccountTypeTrashService, PermanentDeleteAccountTypeService) {
                TrashedAccountTypeService.query(function (data) {
                    $scope.trashedAccountTypes = data;
                });
                $scope.restoreAccountType = function (id) {
                    RestoreAccountTypeService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedAccountTypes = data.trashedAccountTypes;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteAccountTypeService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedAccountTypes = data.trashedAccountTypes;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyAccountTypeTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedAccountTypes = data.trashedAccountTypes;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.accountTypes = data.accountTypes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

}

AccountTypeController.resolve = {
    GetAllAccountTypeModel: function (GetAllAccountTypeService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            GetAllAccountTypeService.get({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        }, 900);
        return deferred.promise;
    }
};