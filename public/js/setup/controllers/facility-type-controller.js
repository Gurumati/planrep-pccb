function FacilityTypeController($scope, AllFacilityTypeModel, $uibModal,
                                ConfirmDialogService,
                                DeleteFacilityTypeService,
                                PaginatedFacilityTypeService) {

    $scope.facilityTypes = AllFacilityTypeModel;
    $scope.title = "SERVICE_PROVIDER_TYPES";
    $scope.dateFormat = 'yyyy-MM-DD';

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedFacilityTypeService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.facilityTypes = data;
        });
    };

    $scope.create = function () {

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/facility_type/create.html',
            backdrop: false,
            controller: function ($scope,
                                  $uibModalInstance,
                                  AllSectorService,
                                  LoadSectionService,
                                  SectionLevelsService,
                                  CreateFacilityTypeService,
                                  GlobalService,
                                  AllAdminHierarchyLevelsService) {

                GlobalService.lgaLevels(function (data) {
                    $scope.allLgaLevels = data.lgaLevels;
                });

                AllSectorService.query(function (data) {
                    $scope.sectors = data;
                });

                SectionLevelsService.query(function (data) {
                    $scope.sectionLevels = data;
                });

                AllAdminHierarchyLevelsService.query(function (data) {
                    $scope.admin_hierarchy_levels = data;
                });

                $scope.loadSections = function (section_level_id, sector_id) {
                    LoadSectionService.query({
                        section_level_id: section_level_id,
                        sector_id: sector_id
                    }, function (data) {
                        $scope.sections = data;
                    });
                };

                $scope.facilityTypeToCreate = {};

                $scope.store = function () {
                    if ($scope.createFacilityTypeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateFacilityTypeService.store({perPage: $scope.perPage}, $scope.facilityTypeToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.facilityTypes = data.facilityTypes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (facilityTypeToEdit, currentPage, perPage) {
        let modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/facility_type/edit.html',
            backdrop: false,
            controller: function ($scope,
                                  $uibModalInstance,
                                  SectionLevelsService,
                                  AllSectorService,
                                  LoadSectionService,
                                  LoadSectionServiceByPiscs,
                                  GlobalService,
                                  UpdateFacilityTypeService,
                                  AllPiscsService,
                                  AllAdminHierarchyLevelsService) {
                SectionLevelsService.query(function (data) {
                    $scope.sectionLevels = data;
                    $scope.facilityTypeToEdit = angular.copy(facilityTypeToEdit);
                    $scope.facilityTypeToEdit.admin_id = null;
                });
                AllPiscsService.get({}, function (data) {
                    $scope.piscs = data.piscs;
                  });

                AllSectorService.query(function (data) {
                    $scope.sectors = data;
                });
                $scope.loadSectionsByLevel = function (section_level_id,sector_id,admin_id) {
                    $scope.sections = [];
                    if(section_level_id != 3 && section_level_id !=4){
                        LoadSectionService.query({
                            section_level_id: section_level_id,
                            sector_id: sector_id
                        }, function (data) {
                            $scope.sections = data;
                            $scope.facilityTypeToEdit.sections = $scope.sections;
                        });
                    }
                };

                $scope.loadSections = function (section_level_id,sector_id,admin_id) {
                    LoadSectionServiceByPiscs.query({
                        section_level_id: section_level_id,
                        admin_id:admin_id
                    }, function (data) {
                        $scope.sections = data;
                        $scope.facilityTypeToEdit.sections = $scope.sections;
                    });
                };

                GlobalService.lgaLevels(function (data) {
                    $scope.allLgaLevels = data.lgaLevels;
                });

                AllAdminHierarchyLevelsService.query(function (data) {
                    $scope.admin_hierarchy_levels = data;
                });


                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    UpdateFacilityTypeService.update({page: currentPage, perPage: perPage}, $scope.facilityTypeToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.facilityTypes = data.facilityTypes;
                $scope.currentPage = $scope.facilityTypes.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete Facility Type!', 'Are sure you want to delete this?').then(function () {
                DeleteFacilityTypeService.delete({facility_type_id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.facilityTypes = data.facilityTypes;
                        $scope.currentPage = $scope.facilityTypes.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/facility_type/trash.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, TrashedFacilityTypeService,
                                  RestoreFacilityTypeService, EmptyFacilityTypeTrashService, PermanentDeleteFacilityTypeService) {
                TrashedFacilityTypeService.query(function (data) {
                    $scope.trashedFacilityTypes = data;
                });
                $scope.restoreFacilityType = function (id) {
                    RestoreFacilityTypeService.restore({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedFacilityTypes = data.trashedFacilityTypes;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteFacilityTypeService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedFacilityTypes = data.trashedFacilityTypes;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyFacilityTypeTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedFacilityTypes = data.trashedFacilityTypes;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.facilityTypes = data.facilityTypes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

FacilityTypeController.resolve = {
    AllFacilityTypeModel: function (PaginatedFacilityTypeService, $q) {
        var deferred = $q.defer();
        PaginatedFacilityTypeService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};