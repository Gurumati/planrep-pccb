function ProjectOutputController ($scope, DataModel, $uibModal,
                                      ConfirmDialogService,ProjectOutputService) {

    $scope.items = DataModel;
    $scope.title = "PROJECT_OUTPUTS";
    $scope.currentPage = 1;
    $scope.maxSize = 3;

    $scope.pageChanged = function () {
        ProjectOutputService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/project_output/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,ExpenditureCategoryService,GlobalService) {

                ExpenditureCategoryService.fetchAll(function (data) {
                    $scope.expenditureCategories = data.items;
                });

                GlobalService.sectors(function (data) {
                    $scope.sectors = data.sectors;
                });

                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    ProjectOutputService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/project_output/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,ExpenditureCategoryService,GlobalService) {

                ExpenditureCategoryService.fetchAll(function (data) {
                    $scope.expenditureCategories = data.items;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });

                GlobalService.sectors(function (data) {
                    $scope.sectors = data.sectors;
                });

                $scope.update = function () {
                    ProjectOutputService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = $scope.items.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                ProjectOutputService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.items = data.items;
                        $scope.currentPage = $scope.items.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
}

ProjectOutputController .resolve = {
    DataModel: function (ProjectOutputService, $q) {
        var deferred = $q.defer();
        ProjectOutputService.paginated({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};