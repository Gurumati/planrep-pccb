function FundSourceCategoryController($scope, FundSourceCategoryModel, $uibModal,Upload,$timeout,AllFundTypeService,
                                ConfirmDialogService,FundSourceCategoryService) {

    $scope.fundSourceCategories = FundSourceCategoryModel;
    $scope.title = "FUND_SOURCE_CATEGORIES";
    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        FundSourceCategoryService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.fundSourceCategories = data;
        });
    };

    AllFundTypeService.query(function (data) {
        $scope.fundTypes = data;
    });

    $scope.showButtons = true;
    $scope.showUploadForm = false;

    $scope.toggleUploadForm = function (state) {
        if(state===true){
            $scope.showButtons = false;
        } else{
            $scope.showButtons = true;
        }
        $scope.showUploadForm = state;
    };

    $scope.uploadFile = function (file) {
        file.upload = Upload.upload({
            url: '/json/fund_source_categories/upload',
            method: 'POST',
            data: {fund_type_id: $scope.fund_type_id, file: file},
        });

        file.upload.then(function (response) {
            $timeout(function () {
                file.result = response.data;
                $scope.fundSourceCategories = response.data.fundSourceCategories;
                $scope.toggleUploadForm(false);
                $scope.successMessage = response.data.successMessage;
            });
        }, function (response) {
            if (response.status > 0){
                $scope.errorMsg = response.status + ': ' + response.data;
                $scope.errorMessage = response.data.errorMessage;
                $scope.successMessage = response.data.successMessage;
            }
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    }

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/fund_source_category/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllFundTypeService) {

                AllFundTypeService.query(function (data) {
                    $scope.fundTypes = data;
                });

                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    FundSourceCategoryService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.fundSourceCategories = data.fundSourceCategories;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
    $scope.edit = function (updateDataModel, currentPage, perPage) {
        //console.log(updateDataModel);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/fund_source_category/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllFundTypeService) {
                AllFundTypeService.query(function (data) {
                    $scope.fundTypes = data;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });
                $scope.plusDate = function (date, plusValue) {
                    date = new Date(date);
                    date.setDate(date.getDate() + plusValue);
                    return date;
                };

                $scope.update = function () {
                    FundSourceCategoryService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.fundSourceCategories = data.fundSourceCategories;
                $scope.currentPage = $scope.fundSourceCategories.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                FundSourceCategoryService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.fundSourceCategories = data.fundSourceCategories;
                        $scope.currentPage = $scope.fundSourceCategories.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/fund_source_category/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance) {
                FundSourceCategoryService.trashed(function (data) {
                    $scope.trashedFundSourceCategories = data.trashedFundSourceCategories;
                });
                $scope.restoreFundSourceCategory = function (id) {
                    FundSourceCategoryService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedFundSourceCategories = data.trashedFundSourceCategories;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    FundSourceCategoryService.permanentDelete({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedFundSourceCategories = data.trashedFundSourceCategories;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };

                $scope.emptyTrash = function () {
                    FundSourceCategoryService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedFundSourceCategories = data.trashedFundSourceCategories;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.fundSourceCategories = data.fundSourceCategories;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

FundSourceCategoryController.resolve = {
    FundSourceCategoryModel: function (FundSourceCategoryService, $q) {
        var deferred = $q.defer();
        FundSourceCategoryService.paginated({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};