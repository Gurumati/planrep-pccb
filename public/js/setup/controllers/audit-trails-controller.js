function AuditTrailsController($scope, $uibModal, AuditService, ConfirmDialogService) {
  $scope.searchQuery = undefined;
  $scope.title = "AUDIT_TRAILS";
  $scope.dateFormat = "yyyy-MM-dd";
  $scope.userLoaded = false;
  $scope.startDate = new Date();
  $scope.endDate= new Date();
  $scope.currentDate = new Date();
  $scope.graphLoading=true;
  $scope.searchData  = function (noLoader) {
    console.log($scope.startDate);
    console.log($scope.endDate);
    console.log($scope.is_active);
  };

  // move this to the GfsCode model in the backend for date formatting.
  // public function getCreatedAtAttribute($value) {
  //   return Carbon::createFromFormat('Y-m-d H:i:s', $value)->diffForHumans();
  // }

  AuditService.query({}, function (data) {
    $scope.searchTerm = '';
    $scope.allAuditTrails = data;

    $scope.getCounter = function (index) {
      return index;
    };

    $scope.getModelName = function (str) {
      var parts = str.split('\\');
      return parts[parts.length - 1];
    };

    $scope.getContext = function (event) {
      if (event === 'created') {
        return 'has been created';
      }

      if (event === 'updated') {
        return 'has been updated';
      }

      if (event === 'deleted') {
        return 'has been deleted';
      }

    };

  }, function (error) {
    console.log(error);
  });

  // AuditService.get().$promise
  //   .then(function(data) {
  //     console.log("data")
  //     $scope.allAuditTrails = data.data;
  //     console.log($scope.allAuditTrails);
  //   })
  //   .catch(function (error) {
  //     console.log(error)
  //   })
}

// AuditTrailsController.resolve = {};
