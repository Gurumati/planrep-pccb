function AdminHierarchyLevelController($scope, AdminHierarchyLevelsModel, $uibModal,
    ConfirmDialogService,ToggleAdminHierarchyLevelService,
    DeleteAdminHierarchyLevelService, AdminHierarchyLevelsService) {

  $scope.adminHierarchyLevels = AdminHierarchyLevelsModel;
  $scope.title = "TITLE_ADMIN_HIERARCHY_LEVELS";
  $scope.dateFormat = 'yyyy-MM-dd';
  $scope.currentPage = 1;

  $scope.maxSize = 3;
  $scope.pageChanged = function () {
    AdminHierarchyLevelsService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
      $scope.adminHierarchyLevels = data;
    });
  };

  $scope.create = function () {
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/admin_hierarchy_level/create.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, CreateAdminHierarchyLevelService) {
        $scope.adminHierarchyLevelToCreate = {};
        //Function to store data and close modal when Create button clicked
        $scope.store = function () {
          if ($scope.createAdminHierarchyLevelForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          CreateAdminHierarchyLevelService.store({perPage:$scope.perPage},$scope.adminHierarchyLevelToCreate,
              function (data) {
                $uibModalInstance.close(data);
              },
              function (error) {
                $scope.errorMessage = error.data.errorMessage;
              }
              );

        };

        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
      //Service to create new financial year
      $scope.successMessage = data.successMessage;
      $scope.adminHierarchyLevels = data.adminHierarchyLevels;
      $scope.currentPage = data.current_page;
    },
    function () {
      //If modal is closed
      console.log('Modal dismissed at: ' + new Date());
    });

  };

  $scope.edit = function (adminHierarchyLevelToEdit, currentPage,perPage) {
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/admin_hierarchy_level/edit.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, UpdateAdminHierarchyLevelService) {
        $scope.adminHierarchyLevelToEdit = angular.copy(adminHierarchyLevelToEdit);
        //Function to store data and close modal when Create button clicked
        $scope.update = function () {
          if ($scope.updateAdminHierarchyLevelForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          UpdateAdminHierarchyLevelService.update({page: currentPage,perPage:perPage}, $scope.adminHierarchyLevelToEdit,
              function (data) {
                //Successful function when
                $uibModalInstance.close(data);
              },
              function (error) {
                $scope.errorMessage = error.data.errorMessage;
              }
              );
        };

        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
      console.log(data);
      $scope.successMessage = data.successMessage;
      $scope.adminHierarchyLevels = data.adminHierarchyLevels;
      $scope.currentPage = $scope.adminHierarchyLevels.current_page;
    },
    function () {
      //If modal is closed
      console.log('Modal dismissed at: ' + new Date());
    });
  };

  $scope.delete = function (id,currentPage,perPage) {
    ConfirmDialogService.showConfirmDialog(
        'TITLE_CONFIRM_DELETE_ADMIN_HIERARCHY_LEVEL',
        'CONFIRM_DELETE')
      .then(function () {
        DeleteAdminHierarchyLevelService.delete({id: id,page:currentPage,perPage:perPage},
            function (data) {
              $scope.successMessage = data.successMessage;
              $scope.adminHierarchyLevels = data.adminHierarchyLevels;
              $scope.currentPage = $scope.adminHierarchyLevels.current_page;
            }, function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
            );
      },
      function () {

      });
  };
  $scope.toggleAdminHierarchyLevel = function (admin_hierarchy_level_id, is_active,perPage) {
    $scope.adminHierarchyLevelToActivate = {};
    $scope.adminHierarchyLevelToActivate.id = admin_hierarchy_level_id;
    $scope.adminHierarchyLevelToActivate.is_active = is_active;
    ToggleAdminHierarchyLevelService.toggleAdminHierarchyLevel({perPage:perPage},$scope.adminHierarchyLevelToActivate,
        function (data) {
          $scope.action = data.action;
          $scope.alertType = data.alertType;
        });
  };

  $scope.trash = function () {
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/admin_hierarchy_level/trash.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, TrashedAdminHierarchyLevelService,
          RestoreAdminHierarchyLevelService, EmptyAdminHierarchyLevelTrashService, PermanentDeleteAdminHierarchyLevelService) {
        TrashedAdminHierarchyLevelService.query(function (data) {
          $scope.trashedAdminHierarchyLevels = data;
        });
        $scope.restoreAdminHierarchyLevel = function (id) {
          RestoreAdminHierarchyLevelService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.successMessage = data.successMessage;
            $scope.trashedAdminHierarchyLevels = data.trashedAdminHierarchyLevels;
          }, function (error) {

          }
          );
        };
        $scope.permanentDelete = function (id) {
          PermanentDeleteAdminHierarchyLevelService.permanentDelete({
            id: id,
            currentPage: $scope.currentPage,
            perPage: $scope.perPage
          }, function (data) {
            $scope.successMessage = data.successMessage;
            $scope.trashedAdminHierarchyLevels = data.trashedAdminHierarchyLevels;
          }, function (error) {

          }
          );
        };

        $scope.emptyTrash = function () {
          EmptyAdminHierarchyLevelTrashService.get({}, function (data) {
            $scope.successMessage = data.successMessage;
            $scope.trashedAdminHierarchyLevels = data.trashedAdminHierarchyLevels;
          }, function (error) {
            $scope.errorMessage = error.data.errorMessage;
          }
          );
        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
      //Service to create new financial year
      $scope.successMessage = data.successMessage;
      $scope.adminHierarchyLevels = data.adminHierarchyLevels;
      $scope.currentPage = data.current_page;
    },
    function () {
      //If modal is closed
      console.log('Modal dismissed at: ' + new Date());
    });

  };
}

AdminHierarchyLevelController.resolve = {
  AdminHierarchyLevelsModel: function (AdminHierarchyLevelsService,$timeout, $q) {
    var deferred = $q.defer();
    $timeout(function () {
      AdminHierarchyLevelsService.get({page: 1,perPage:10}, function (data) {
        deferred.resolve(data);
      });
    }, 900);
    return deferred.promise;
  }
};
