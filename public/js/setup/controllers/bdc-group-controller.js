function BdcGroupController($scope, BdcGroupModel, $uibModal, ConfirmDialogService,
                            DeleteBdcGroupService, PaginatedBdcGroupService) {

    $scope.bdcGroups = BdcGroupModel;
    $scope.title = "BDC_GROUPS";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedBdcGroupService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.bdcGroups = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bdc_group/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllBdcMainGroupService,AllBdcMainGroupFundSourceService, CreateBdcGroupService) {
                $scope.bdcGroupToCreate = {};

                AllBdcMainGroupFundSourceService.query(function (data) {
                    $scope.mainGroupFundSources = data;
                });

                $scope.store = function () {
                    if ($scope.createBdcGroupForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateBdcGroupService.store({perPage: $scope.perPage}, $scope.bdcGroupToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.bdcGroups = data.bdcGroups;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (bdcGroupToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bdc_group/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllBdcMainGroupService,AllBdcMainGroupFundSourceService, UpdateBdcGroupService) {
                AllBdcMainGroupFundSourceService.query(function (data) {
                    $scope.mainGroupFundSources = data;
                    $scope.bdcGroupToEdit = angular.copy(bdcGroupToEdit);
                });

                $scope.update = function () {
                    if ($scope.updateBdcGroupForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateBdcGroupService.update({page: currentPage, perPage: perPage}, $scope.bdcGroupToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.bdcGroups = data.bdcGroups;
                $scope.currentPage = $scope.bdcGroups.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('CONFIRM_DELETE', 'Are you sure you want to trash this record?').then(function () {
                DeleteBdcGroupService.delete({bdc_group_id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.bdcGroups = data.bdcGroups;
                        $scope.currentPage = $scope.bdcGroups.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/bdc_group/trash.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance, TrashedBdcGroupService,
                                  RestoreBdcGroupService, EmptyBdcGroupTrashService, PermanentDeleteBdcGroupService) {
                TrashedBdcGroupService.query(function (data) {
                    $scope.trashedBdcGroups = data;
                });
                $scope.restoreBdcGroup = function (id) {
                    RestoreBdcGroupService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBdcGroups = data.trashedBdcGroups;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteBdcGroupService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBdcGroups = data.trashedBdcGroups;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyBdcGroupTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedBdcGroups = data.trashedBdcGroups;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.bdcGroups = data.bdcGroups;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
}

BdcGroupController.resolve = {
    BdcGroupModel: function (PaginatedBdcGroupService, $q) {
        var deferred = $q.defer();
        PaginatedBdcGroupService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};