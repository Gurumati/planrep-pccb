function TargetTypeController($scope, TargetTypeModel, $uibModal, ConfirmDialogService,
                                    DeleteTargetTypeService, ToggleTargetTypeService,
                                    PaginatedTargetTypeService) {

    $scope.targetTypes = TargetTypeModel;
    $scope.title = "TARGET_TYPES";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedTargetTypeService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.targetTypes = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/target_type/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateTargetTypeService) {
                $scope.targetTypeToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createTargetTypeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateTargetTypeService.store({perPage: $scope.perPage}, $scope.targetTypeToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.targetTypes = data.targetTypes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (targetTypeToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/target_type/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateTargetTypeService) {
                $scope.targetTypeToEdit = angular.copy(targetTypeToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updateTargetTypeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateTargetTypeService.update({
                            page: currentPage,
                            perPage: perPage
                        }, $scope.targetTypeToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.targetTypes = data.targetTypes;
                $scope.currentPage = $scope.targetTypes.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog(
            'TITLE_CONFIRM_DELETE_TARGET_TYPE',
            'CONFIRM_DELETE')
            .then(function () {
                    DeleteTargetTypeService.delete({target_type_id: id, page: currentPage, perPage: perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.targetTypes = data.targetTypes;
                            $scope.currentPage = $scope.targetTypes.current_page;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {

                });
    }
    $scope.toggleTargetType = function (target_type_id, activity_status, perPage) {
        $scope.targetTypeToActivate = {};
        $scope.targetTypeToActivate.id = target_type_id;
        $scope.targetTypeToActivate.is_active = activity_status;
        ToggleTargetTypeService.toggleTargetType({perPage: perPage}, $scope.targetTypeToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    }
}

TargetTypeController.resolve = {
    TargetTypeModel: function (PaginatedTargetTypeService, $q) {
        var deferred = $q.defer();
        PaginatedTargetTypeService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};