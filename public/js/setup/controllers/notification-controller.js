function NotificationController($scope, NotificationModel, $uibModal,
                            ConfirmDialogService,NotificationService) {

    $scope.notifications = NotificationModel;
    $scope.title = "NOTIFICATIONS";
    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        NotificationService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.notifications = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/notification/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {
                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    NotificationService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.notifications = data.notifications;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        //console.log(updateDataModel);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/notification/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {
                $scope.updateDataModel = angular.copy(updateDataModel);
                $scope.statuses = [0,1,-1];
                $scope.plusDate = function (date, plusValue) {
                    date = new Date(date);
                    date.setDate(date.getDate() + plusValue);
                    return date;
                };

                $scope.update = function () {
                    NotificationService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.notifications = data.notifications;
                $scope.currentPage = $scope.notifications.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                NotificationService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.notifications = data.notifications;
                        $scope.currentPage = $scope.notifications.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/notification/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance) {
                NotificationService.trashed(function (data) {
                    $scope.trashedNotifications = data.trashedNotifications;
                });
                $scope.restoreNotification = function (id) {
                    NotificationService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedNotifications = data.trashedNotifications;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    NotificationService.permanentDelete({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedNotifications = data.trashedNotifications;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };

                $scope.emptyTrash = function () {
                    NotificationService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedNotifications = data.trashedNotifications;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.notifications = data.notifications;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

NotificationController.resolve = {
    NotificationModel: function (NotificationService, $q) {
        var deferred = $q.defer();
        NotificationService.paginated({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};