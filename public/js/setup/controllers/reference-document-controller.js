(function () {
    'use strict';

    angular.module('master-module').controller('ReferenceDocumentController', ReferenceDocumentController);
    ReferenceDocumentController.$inject     =  ["$scope", "$route", "$uibModal", "ConfirmDialogService", "localStorageService", "ReferenceDocument"];
    function ReferenceDocumentController($scope, $route, $uibModal, ConfirmDialogService, localStorageService, ReferenceDocument) {

        $scope.title = "REFERENCE_DOCUMENTS";
        $scope.currentPage = 1;
        $scope.maxSize = 3;
        $scope.isNationalGuideline = ($route.current.params.isNationalGuideline) ? $route.current.params.isNationalGuideline : 0;

        var user = JSON.parse(localStorageService.get(localStorageKeys.USER));

        $scope.hasPermission = function (right) {
            return user.rights.indexOf(right) !== -1;
        };

        $scope.pageChanged = function () {
            ReferenceDocument.paginated({
                    isNationalGuideline: $scope.isNationalGuideline,
                    page: $scope.currentPage,
                    perPage: $scope.perPage
                },function (data) {
                    $scope.referenceDocuments = data;
                },
                function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                });
        };
        $scope.pageChanged();

        $scope.create = function (isNationalGuideline) {
            var modalInstance = $uibModal.open({
                templateUrl: "/pages/setup/reference_document/create.html",
                backdrop: false,
                controller:["$scope","$uibModalInstance","UnUsedReferenceDocumentTypesService", "FinancialYearsService",function ($scope, $uibModalInstance, UnUsedReferenceDocumentTypesService, FinancialYearsService) {

                    $scope.isNationalGuideline = isNationalGuideline;
                    $scope.namePattern = /^.{4,50}$/;
                     $scope.request = {method:"POST", url:"/form/reference-documents/"+isNationalGuideline};

                    UnUsedReferenceDocumentTypesService.query({isNationalGuideline: isNationalGuideline}, function (data) {
                        $scope.referenceDocumentTypes = data;
                    });

                    FinancialYearsService.query(function (data) {
                        $scope.startFinancialYear = data;
                    });

                    FinancialYearsService.query(function (data) {
                        $scope.endFinancialYear = data;
                    });

                    $uibModalInstance.rendered.then(function () {
                        var options = {
                            beforeSubmit: $scope.validate,
                            success: processResponse,
                            error: failureHandler,
                            headers: {
                                "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
                            }
                        };
                        $("#createReferenceDocumentForm").ajaxForm(options);
                    });

                    $scope.validate = function (formData) {
                        $scope.$apply(function () {
                            $scope.inProgress = true;
                            if ($scope.createReferenceDocumentForm.$invalid) {
                                $scope.formHasErrors = true;
                                $scope.inProgress = false;
                            }
                        });
                        $scope.$apply();
                        if ($scope.inProgress) {
                            $("#loader").show();
                        }
                        return $scope.inProgress;
                    };

                    var failureHandler = function (response) {
                        $("#loader").hide();
                        $scope.$apply(function () {
                            $scope.errorMessage = response.responseJSON.errorMessage;
                            $scope.errors = response.responseJSON.errors;
                            $scope.inProgress = false;
                        });
                    };

                    function processResponse(response) {
                        $("#loader").hide();
                        if (response.successMessage) {
                            $uibModalInstance.close(response);
                        }
                    }
                    $scope.close = function () {
                        $uibModalInstance.dismiss();
                    };
                }]
            });
            modalInstance.result.then(function (data) {
                    $scope.successMessage = data.successMessage;
                    $scope.pageChanged();
                },
                function () {
                });

        };

        $scope.edit = function (referenceDocumentToEdit, currentPage, perPage, isNationalGuideline) {
            var modalInstance = $uibModal.open({
                templateUrl: "/pages/setup/reference_document/edit.html",
                backdrop: false,
                controller: function ($scope, $uibModalInstance,$timeout, UnUsedReferenceDocumentTypesService, FinancialYearsService) {

                    $scope.isNationalGuideline = isNationalGuideline;
                    $scope.request = {method:"POST", url:"/form/reference-documents/"+isNationalGuideline+"/"+referenceDocumentToEdit.id};

                    UnUsedReferenceDocumentTypesService.query({isNationalGuideline: isNationalGuideline}, function (data) {
                        data.push(referenceDocumentToEdit.reference_document_type);
                        $scope.referenceDocumentTypes = data;
                        FinancialYearsService.query(function (data) {
                            $scope.financialYears = data;
                            $timeout(function () {
                                $scope.referenceDocumentToEdit = angular.copy(referenceDocumentToEdit);
                            },300);

                        });
                    });

                    $uibModalInstance.rendered.then(function () {
                        var options = {
                            beforeSubmit: $scope.validate,
                            success: processResponse,
                            error: failureHandler,
                            type :'POST',
                            headers: {
                                "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
                            }
                        };
                        $("#editReferenceDocumentForm").ajaxForm(options);
                    });

                    $scope.validate = function (formData) {
                        $scope.$apply(function () {
                            $scope.inProgress = true;
                            if ($scope.editReferenceDocumentForm.$invalid) {
                                $scope.formHasErrors = true;
                                $scope.inProgress = false;
                            }
                        });
                        $scope.$apply();
                        if ($scope.inProgress) {
                            $("#loader").show();
                        }
                        return $scope.inProgress;
                    };

                    var failureHandler = function (response) {
                        $("#loader").hide();
                        $scope.$apply(function () {
                            $scope.errorMessage = response.responseJSON.errorMessage;
                            $scope.errors = response.responseJSON.errors;
                            $scope.inProgress = false;
                        });
                    };

                    function processResponse(response) {
                        $("#loader").hide();
                        if (response.successMessage) {
                            $uibModalInstance.close(response);
                        }
                    }
                    $scope.close = function () {
                        $uibModalInstance.dismiss();
                    };
                }
            });
            modalInstance.result.then(function (data) {
                    $scope.successMessage = data.successMessage;
                    $scope.pageChanged();
                },
                function () {
                });
        };

        $scope.delete = function (id, currentPage, perPage, isNationalGuideline) {
            ConfirmDialogService.showConfirmDialog(
                "Confirm Delete!",
                "Are sure you want to delete this Document?")
                .then(function () {
                        ReferenceDocument.delete({id: id, isNationalGuideline: isNationalGuideline, currentPage: currentPage, perPage: perPage},
                            function (data) {
                                $scope.successMessage = data.successMessage;
                                $scope.pageChanged();
                            }, function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                            }
                        );
                    },
                    function () {
                    });
        };
    }
})();
