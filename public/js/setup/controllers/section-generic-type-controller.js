function SectionGenericTypeController($scope, AllSectionGenericTypeModel, $uibModal,
                                      ConfirmDialogService,
                                      DeleteSectionGenericTypeService, GetAllSectionGenericTypeService) {

    $scope.sectionGenericTypes = AllSectionGenericTypeModel;
    $scope.title = "TITLE_SECTION_GENERIC_TYPES";
    $scope.dateFormat = 'yyyy-MM-DD';

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        GetAllSectionGenericTypeService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.sectionGenericTypes = data;
        });
    };
    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/section_generic_type/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateSectionGenericTypeService, AllSectionService, AllGenericTypeService) {
                $scope.sectionGenericTypeToCreate = {};
                //Function to store data and close modal when Create button clicked
                AllSectionService.query(function (data) {
                    $scope.sections = data;
                });
                AllGenericTypeService.query(function (data) {
                    $scope.genericTypes = data;
                });
                $scope.store = function () {
                    if ($scope.createSectionGenericTypeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateSectionGenericTypeService.store({perPage: $scope.perPage}, $scope.sectionGenericTypeToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.sectionGenericTypes = data.sectionGenericTypes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (sectionGenericTypeToEdit, currentPage, perPage) {
        console.log(sectionGenericTypeToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/section_generic_type/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateSectionGenericTypeService, AllSectionService, AllGenericTypeService) {
                AllSectionService.query(function (data) {
                    $scope.sections = data;
                    $scope.sectionGenericTypeToEdit = angular.copy(sectionGenericTypeToEdit);
                });
                AllGenericTypeService.query(function (data) {
                    $scope.genericTypes = data;
                });
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    UpdateSectionGenericTypeService.update({page: currentPage, perPage: perPage}, $scope.sectionGenericTypeToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.sectionGenericTypes = data.sectionGenericTypes;
                $scope.currentPage = $scope.sectionGenericTypes.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete Section Generic Type!', 'Are sure you want to delete this?').then(function () {
                DeleteSectionGenericTypeService.delete({section_generic_type_id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.sectionGenericTypes = data.sectionGenericTypes;
                        $scope.currentPage = $scope.sectionGenericTypes.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    }
}

SectionGenericTypeController.resolve = {
    AllSectionGenericTypeModel: function (GetAllSectionGenericTypeService, $q) {
        var deferred = $q.defer();
        GetAllSectionGenericTypeService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};