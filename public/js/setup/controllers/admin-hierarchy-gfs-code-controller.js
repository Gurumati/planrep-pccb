function AdminHierarchyGfsCodeController($scope, AdminHierarchyGfsCodeModel, $uibModal, ConfirmDialogService,
                                         DeleteAdminHierarchyGfsCodeService,
                                PaginatedAdminHierarchyGfsCodeService) {

    $scope.adminHierarchyGfsCodes = AdminHierarchyGfsCodeModel;
    $scope.title = "ADMIN_HIERARCHY_GFS_CODES";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedAdminHierarchyGfsCodeService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.adminHierarchyGfsCodes = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/admin_hierarchy_gfs_code/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateAdminHierarchyGfsCodeService, AllGfsCodeService) {

                AllGfsCodeService.query(function (data) {
                    $scope.gfs_codes = data;
                });
                
                $scope.adminHierarchyGfsCodeToCreate = {};

                $scope.store = function () {
                    if ($scope.createAdminHierarchyGfsCodeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateAdminHierarchyGfsCodeService.store({perPage: $scope.perPage}, $scope.adminHierarchyGfsCodeToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.adminHierarchyGfsCodes = data.adminHierarchyGfsCodes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
    
    $scope.removeData = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteAdminHierarchyGfsCodeService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.adminHierarchyGfsCodes = data.adminHierarchyGfsCodes;
                        $scope.currentPage = $scope.adminHierarchyGfsCodes.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
}

AdminHierarchyGfsCodeController.resolve = {
    AdminHierarchyGfsCodeModel: function (PaginatedAdminHierarchyGfsCodeService,$timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            PaginatedAdminHierarchyGfsCodeService.get({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        }, 900);
        return deferred.promise;
    }
};