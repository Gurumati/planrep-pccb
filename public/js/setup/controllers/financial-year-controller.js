function FinancialYearController($scope, FinancialYear, AllFinancialYearModel, $uibModal,
                                 ToggleFinancialYearService,
                                 UpdateFinancialYearService,
                                 ConfirmDialogService,
                                 DeleteFinancialYearService,
                                 GetAllFinancialYearService) {

    $scope.financialYears = AllFinancialYearModel;
    $scope.title = "TITLE_FINANCIAL_YEARS";
    $scope.dateFormat = 'yyyy-MM-dd';
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        GetAllFinancialYearService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.financialYears = data;
        });
    };


    $scope.periods = function (financialYear) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/financial_year/periods.html',
            backdrop: false,
            controller: function ($scope, $q, $uibModalInstance, AddFinancialYearPeriodService, DeleteFinancialYearPeriod, AllFinancialYearPeriodService) {

                $scope.periodToCreate = {};
                $scope.financial_year_id = financialYear.id;
                $scope.name = financialYear.name;


                AllFinancialYearPeriodService.periods({financial_year_id: financialYear.id}, function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.financial_year_periods = data.financial_year_periods;
                        console.log(data);
                    }, function (error) {
                    }
                );

                $scope.add_period = function () {
                    var periodToSave = {
                        "name": $scope.periodToCreate.name,
                        "start_date": $scope.periodToCreate.start_date,
                        "end_date": $scope.periodToCreate.end_date,
                        "financial_year_id": $scope.financial_year_id,
                    };
                    AddFinancialYearPeriodService.add_period(periodToSave, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.financial_year_periods = data.financial_year_periods;
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.financial_year_periods = data.financial_year_periods;
                        }
                    );
                };

                $scope.deleteFinancialYearPeriod = function (period_id, financial_year_id) {
                    console.log('called');
                    DeleteFinancialYearPeriod.delete({
                            period_id: period_id,
                            financial_year_id: financial_year_id
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.financial_year_periods = data.financial_year_periods;
                        }, function (error) {

                        }
                    );
                };

                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.versions = function (financialYear) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/financial_year/versions.html',
            backdrop: false,
            controller: function ($scope, $q, $uibModalInstance, FinancialYearService, VersionService) {

                $scope.versionData = {};
                $scope.financialYear = financialYear;
                $scope.financialYearId = financialYear.id;

                FinancialYearService.versions({id: financialYear.id}, function (response) {
                        $scope.versions = response.data;
                    }, function (error) {

                    }
                );

                VersionService.fetchAll(function (response) {
                        $scope.allVersions = response.data;
                    }, function (error) {

                    }
                );

                $scope.addVersion = function () {
                    $scope.versionData.financial_year_id = financialYear.id;
                    FinancialYearService.addVersion($scope.versionData, function (response) {
                            if (response.success) {
                                $scope.versions = response.data;
                                $scope.successMessage = response.message;
                            } else {
                                $scope.errors = response.errors;
                            }
                        }, function (error) {

                        }
                    );
                };

                $scope.removeVersion = function (id) {
                    FinancialYearService.removeVersion({
                            financialYearId: $scope.financialYearId,
                            id: id
                        }, function (response) {
                            if(response.success){
                                $scope.versions = response.data;
                                $scope.successMessage = response.message;
                            } else{
                                $scope.errors = response.errors;
                            }
                        }, function (error) {

                        }
                    );
                };

                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/financial_year/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, GlobalService, CreateFinancialYearService, LastFinancialYear) {

                GlobalService.financialYears(function (data) {
                    $scope.previousFinancialYears = data.financialYears;
                });

                LastFinancialYear.get(function (data) {
                    $scope.lastFinancialYear = data;
                    $scope.financialYearToCreate = {start_date: $scope.plusDate(data.end_date, 1)};
                });
                $scope.plusDate = function (date, plusValue) {
                    date = new Date(date);
                    date.setDate(date.getDate() + plusValue);
                    return date;
                };
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createFinancialYearForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateFinancialYearService.store({perPage: $scope.perPage}, $scope.financialYearToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );

                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.financialYears = data.financialYears;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (financialYearToEdit, currentPage, perPage) {
        console.log(financialYearToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/financial_year/edit.html',
            backdrop: false,
            controller: function ($scope, GlobalService, $uibModalInstance) {
                GlobalService.financialYears(function (data) {
                    $scope.previousFinancialYears = data.financialYears;
                    $scope.financialYearToEdit = angular.copy(financialYearToEdit);
                    $scope.financialYearToEdit.start_date = new Date($scope.financialYearToEdit.start_date);
                    $scope.financialYearToEdit.end_date = new Date($scope.financialYearToEdit.end_date);
                });
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    $uibModalInstance.close($scope.financialYearToEdit);
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (financialYearToEdit) {
                //Service to create new financial year
                UpdateFinancialYearService.update({page: currentPage, perPage: perPage}, financialYearToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage = data.successMessage;
                        $scope.financialYears = data.financialYears;
                        $scope.currentPage = $scope.financialYears.current_page;
                    },
                    function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                        $scope.errors = error.data.errors;
                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Delete Financial Year!',
            'Are sure you want to delete this?')
            .then(function () {
                    DeleteFinancialYearService.delete({financialYearId: id, currentPage: currentPage, perPage: perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.financialYears = data.financialYears;
                            $scope.currentPage = $scope.financialYears.current_page;
                        }, function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    };

    $scope.toggleFinancialYear = function (financial_year_id, status, perPage) {
        $scope.financialYearToActivate = {};
        $scope.financialYearToActivate.id = financial_year_id;
        $scope.financialYearToActivate.is_active = status;
        ToggleFinancialYearService.toggleFinancialYear({perPage: perPage}, $scope.financialYearToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    };

    $scope.openFinancialYear = function (financialYear, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/financial_year/open_financial_year.html',
            backdrop: false,
            size: "md",
            controller: function ($scope, $uibModalInstance, OpenFinancialYearService, OpenFinancialYearPreRequestService) {
                $scope.inProgress = false;
                $scope.financialYearToOpen = financialYear;

                $scope.runPrerequest = true;
                OpenFinancialYearPreRequestService.check({id: $scope.financialYearToOpen.id},

                  function (data) {
                        $scope.preRequest = data;
                        $scope.runPrerequest = false;
                    },
                    function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                        $scope.runPrerequest = false;
                    }
                );
                $scope.open = function () {
                    $scope.inProgress = true;
                    OpenFinancialYearService.open({
                            page: currentPage,
                            perPage: perPage,
                            id: $scope.financialYearToOpen.id
                        },
                        function (data) {
                            $scope.inProgress = false;
                            $scope.successData = data;
                            $scope.errorMessage = undefined;
                            $scope.progessTitle = "OPEN_COMPLETED";
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            console.log(error);
                            $scope.inProgress = false;
                            $scope.openError = true;
                            $scope.progessTitle = undefined;
                            $scope.errorMessage = error.data.errorMessage;
                        });
                };

                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.financialYears = data.financialYears;
            },
            function () {
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.closeFinancialYear = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Close Financial Year!',
            'Are sure you want to change financial year to closed')
            .then(function () {
                    FinancialYear.close({id: id}, function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.pageChanged();
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    });
                },
                function () {
                    console.log("NO");
                });
    };

    $scope.executeFinancialYear = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Change Financial Year!',
            'Are sure you want to change financial year to execution')
            .then(function () {
                    FinancialYear.execute({id: id}, function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.pageChanged();
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    });
                },
                function () {
                    console.log("NO");
                });

    };

    $scope.reExecuteFinancialYear = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Change Financial Year!',
            'Are sure you want to re-excute financial year')
            .then(function () {
                    FinancialYear.reExecute({id: id}, function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.pageChanged();
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    });
                },
                function () {
                    console.log("NO");
                });

    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/financial_year/trash.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, TrashedFinancialYearService,
                                  RestoreFinancialYearService, EmptyFinancialYearTrashService, PermanentDeleteFinancialYearService) {
                TrashedFinancialYearService.query(function (data) {
                    $scope.trashedFinancialYears = data;
                });
                $scope.restoreFinancialYear = function (id) {
                    RestoreFinancialYearService.restore({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedFinancialYears = data.trashedFinancialYears;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteFinancialYearService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedFinancialYears = data.trashedFinancialYears;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyFinancialYearTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedFinancialYears = data.trashedFinancialYears;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.financialYears = data.financialYears;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

FinancialYearController.resolve = {
    AllFinancialYearModel: function (GetAllFinancialYearService, $q, $timeout) {
        var deferred = $q.defer();
        $timeout(function () {
            GetAllFinancialYearService.get({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        }, 100);

        return deferred.promise;
    }
};
