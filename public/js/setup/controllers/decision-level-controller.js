function DecisionLevelController($scope,
                               AllDecisionLevelModel,
                               $uibModal,
                               UpdateDecisionLevelService,GetAllDecisionLevelService,
                               DeleteDecisionLevelService,
                               ConfirmDialogService, DefaultDecisionLevelService) {

    $scope.decisionLevels = AllDecisionLevelModel;
    $scope.title = "DECISION_LEVELS";
    console.log(AllDecisionLevelModel);

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        GetAllDecisionLevelService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
            $scope.decisionLevels = data;
        });
    };


    $scope.create = function () {
        //Modal Form for creating a decision level

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/decision_level/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,CreateDecisionLevelService,DecisionLevelService,AllAdminHierarchyLevelsService,AllSectionLevelService) {
                $scope.decisionLevelToCreate = {};
                //Function to store data and close modal when Create button clicked

                DecisionLevelService.query(function (data) {
                    $scope.allDecisionLevels = data;
                });

                AllAdminHierarchyLevelsService.query(function (data) {
                    $scope.allAdminHierarchyLevels = data;
                });
                AllSectionLevelService.query(function (data) {
                    $scope.sectionLevels = data;
                });

                $scope.store = function () {
                    if ($scope.createDecisionLevelForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateDecisionLevelService.store({perPage:$scope.perPage},$scope.decisionLevelToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage=error.data.errorMessage;
                        }
                    );

                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create a new decision level
                $scope.successMessage=data.successMessage;
                $scope.decisionLevels=data.decisionLevels;

            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (decisionLevelToEdit,currentPage,perPage) {
        console.log(decisionLevelToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/decision_level/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,DecisionLevelService,AllAdminHierarchyLevelsService,AllSectionLevelService) {
                DecisionLevelService.query(function (data) {
                    $scope.allDecisionLevels = data;
                    $scope.decisionLevelToEdit = angular.copy(decisionLevelToEdit);
                });

                AllAdminHierarchyLevelsService.query(function (data) {
                    $scope.allAdminHierarchyLevels = data;
                });
                AllSectionLevelService.query(function (data) {
                    $scope.sectionLevels = data;
                });
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    $uibModalInstance.close($scope.decisionLevelToEdit);
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (decisionLevelToEdit) {
                //Service to create a new decision level
                UpdateDecisionLevelService.update({page: currentPage,perPage:perPage},decisionLevelToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage=data.successMessage;
                        $scope.decisionLevels = data.decisionLevels;
                        $scope.currentPage = $scope.decisionLevels.current_page;
                    },
                    function (error) {

                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm delete!',
            'Are sure you want to delete this planning and budgeting decision level?')
            .then(function () {
                    console.log("YES");
                    DeleteDecisionLevelService.delete({decisionLevelId: id,page:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage=data.successMessage;
                            $scope.decisionLevels = data.decisionLevels;
                            $scope.currentPage = $scope.decisionLevels.current_page;
                        }, function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    };

    $scope.defaultDecisionLevel = function (decision_level, status,perPage) {
        $scope.defaultDecisionLevelToActivate = {};
        $scope.defaultDecisionLevelToActivate.id = decision_level.id;
        $scope.defaultDecisionLevelToActivate.is_default = status;
        DefaultDecisionLevelService.defaultDecisionLevel({perPage:perPage},$scope.defaultDecisionLevelToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            },function (error) {
                decision_level.is_default=false;
                console.log(error);
                $scope.errorMessage=error.data.errorMessage;
            });
    }
}

DecisionLevelController.resolve = {
    AllDecisionLevelModel: function (GetAllDecisionLevelService, $q) {
        var deferred = $q.defer();
        GetAllDecisionLevelService.get({page: 1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};