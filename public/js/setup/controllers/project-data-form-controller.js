function ProjectDataFormController($scope,
                                   ProjectDataFormModel,
                                   SectorsService,
                                   ProjectsService,
                                   $uibModal,
                                   ProjectDataFormsService,
                                   CreateProjectDataFormsService,
                                   UpdateProjectDataFormsService,
                                   DeleteProjectDataFormsService,
                                   ConfirmDialogService
) {

  $scope.projectDataForms = ProjectDataFormModel;
  $scope.title = "PROJECT_DATA_FORMS";
  $scope.dateFormat = 'yyyy-MM-DD';


  $scope.create = function () {
    //Modal Form for creating a project data form
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/project_data_forms/create.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance) {
        //load sectors
        SectorsService.query({}, function (data) {
          $scope.sectors = data;
        });

        //load projects
        $scope.loadProjects = function (sectorId) {
          ProjectsService.getBySector({sectorId: sectorId},
            function (data) {
              $scope.projects = data.projects;
            },
            function (error) {
            }
          );
        };

        $scope.projectFormToCreate = {};
        //store data
        $scope.store = function () {
          if ($scope.createProjectForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          CreateProjectDataFormsService.store($scope.projectFormToCreate,
            function (data) {
              $scope.projectDataForms = data.projectDataForms;
              $scope.successMessage = data.successMessage;
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );

        };

        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        }
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
        //Service to create new financial year
        $scope.successMessage = data.successMessage;
        $scope.projectDataForms = data.projectDataForms;
      },
      function () {
        //If modal is closed
        console.log('Modal dismissed at: ' + new Date());
      });

  };

  $scope.edit = function (projectFormToEdit) {
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/project_data_forms/edit.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, UpdateCasPlanService) {
        //load sectors
        SectorsService.query({}, function (data) {
          $scope.sectors = data;
        });

        //load projects
        $scope.loadProjects = function (sectorId) {
          ProjectsService.getBySector({sectorId: sectorId},
            function (data) {
              $scope.projects = data.projects;
            },
            function (error) {
            }
          );
        };

        $scope.projectFormToEdit = angular.copy(projectFormToEdit);
        //Function to store data and close modal when Create button clicked
        $scope.update = function () {
          if ($scope.editProjectForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          UpdateProjectDataFormsService.update($scope.projectFormToEdit,
            function (data) {
              //Successful function when
              $scope.projectDataForms = data.projectDataForms;
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );

        }
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        }
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
        $scope.successMessage = data.successMessage;
        $scope.projectDataForms = data.projectDataForms;
      },
      function () {
        //If modal is closed
        console.log('Modal dismissed at: ' + new Date());
      });
  };

  $scope.delete = function (id) {
    ConfirmDialogService.showConfirmDialog(
      'TITLE_CONFIRM_DELETE_FORM',
      'CONFIRM_DELETE')
      .then(function () {

          DeleteProjectDataFormsService.delete({id: id},
            function (data) {
              $scope.successMessage = data.successMessage;
              $scope.projectDataForms = data.projectDataForms;
            }, function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        },
        function () {
          console.log("NO");
        });
  }
};

ProjectDataFormController.resolve = {

  ProjectDataFormModel: function (ProjectDataFormsService, $q) {

    var deferred = $q.defer();
    ProjectDataFormsService.query({}, function (data) {
      deferred.resolve(data);
    });
    return deferred.promise;
  }
};
