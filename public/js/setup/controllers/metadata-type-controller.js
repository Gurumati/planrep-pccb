function MetadataTypeController($scope, DataModel, $uibModal, ConfirmDialogService, MetadataTypeService,MetadataSourceService) {

    $scope.items = DataModel;
    $scope.title = "METADATA_TYPES";

    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.perPage = 10;

    $scope.pageChanged = function () {
        MetadataTypeService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (response) {
            $scope.items = response.data;
        }, function (error) {
            console.log(error);
        });
    };

    $scope.create = function () {
        let modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/metadata_types/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, MetadataTypeService) {
                $scope.dataModel = {};

                MetadataSourceService.fetchAll(function (response) {
                    $scope.metadataSources = response.data;
                }, function (error) {
                    console.log(error);
                });

                $scope.store = function () {
                    if ($scope.dataForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    MetadataTypeService.save({perPage: $scope.perPage}, $scope.dataModel,
                        function (response) {
                            $uibModalInstance.close(response);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.message;
                            $scope.errors = error.data.errors;
                        }
                    );
                };

                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });

        modalInstance.result.then(function (response) {
            $scope.successMessage = response.message;
            $scope.items = response.data;
            $scope.currentPage = $scope.items.current_page;
        }, function () {

        });
    };

    $scope.edit = function (dataModel, currentPage, perPage) {
        let modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/metadata_types/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, MetadataTypeService) {
                MetadataSourceService.fetchAll(function (response) {
                    $scope.metadataSources = response.data;
                    $scope.dataModel = angular.copy(dataModel);
                }, function (error) {
                    console.log(error);
                });
                $scope.update = function () {
                    if ($scope.dataForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    MetadataTypeService.update({id: $scope.dataModel.id, page: currentPage, perPage: perPage}, $scope.dataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.message;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (response) {
                $scope.successMessage = response.message;
                $scope.items = response.data;
                $scope.currentPage = $scope.items.current_page;
            }, function () {

            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm item delete', 'Are you sure you want to delete this record').then(function () {
                MetadataTypeService.delete({id: id, page: currentPage, perPage: perPage},
                    function (response) {
                        $scope.successMessage = response.message;
                        $scope.items = response.data;
                        $scope.currentPage = $scope.items.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    };
}

MetadataTypeController.resolve = {
    DataModel: function (MetadataTypeService, $timeout, $q) {
        let deferred = $q.defer();
        MetadataTypeService.paginated({page: 1, perPage: 10}, function (response) {
            deferred.resolve(response.data);
        });
        return deferred.promise;
    }
};