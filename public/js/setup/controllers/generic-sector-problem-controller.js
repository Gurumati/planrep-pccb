function GenericSectorProblemController($scope, DataModel, $uibModal,
                                  ConfirmDialogService, GenericSectorProblemService) {

    $scope.items = DataModel;
    $scope.title = "GENERIC_SECTOR_PROBLEMS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        GenericSectorProblemService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/generic_sector_problem/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, GenericSectorProblemService, GlobalService) {

                $scope.createDataModel = {};

                GlobalService.priorityAreas(function (data) {
                    $scope.priorityAreas = data.data;
                });

                GlobalService.planningMatrices(function (data) {
                    $scope.planningMatrices = data.planningMatrices;
                });

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    GenericSectorProblemService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        //console.log(updateDataModel);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/generic_sector_problem/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, GlobalService, GenericSectorProblemService) {

                GlobalService.priorityAreas(function (data) {
                    $scope.priorityAreas = data.data;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });

                GlobalService.planningMatrices(function (data) {
                    $scope.planningMatrices = data.planningMatrices;
                });

                $scope.update = function () {
                    GenericSectorProblemService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = $scope.items.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                GenericSectorProblemService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.items = data.items;
                        $scope.currentPage = $scope.items.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/generic_sector_problem/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance, GenericSectorProblemService) {
                GenericSectorProblemService.trashed(function (data) {
                    $scope.trashedItems = data.trashedItems;
                });
                $scope.restoreItem = function (id) {
                    GenericSectorProblemService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    GenericSectorProblemService.permanentDelete({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    GenericSectorProblemService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

GenericSectorProblemController.resolve = {
    DataModel: function (GenericSectorProblemService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            GenericSectorProblemService.paginated({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        }, 900);
        return deferred.promise;
    }
};