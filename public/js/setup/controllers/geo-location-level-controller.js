function GeoLocationLevelController($scope, GeoLocationLevelsModel, $uibModal,
  ConfirmDialogService,ToggleGeoLocationLevelService,
  DeleteGeoLocationLevelService, GeoLocationLevelsService) {

$scope.geoLocationLevels = GeoLocationLevelsModel;
$scope.title = "TITLE_GEO_LOCATION_LEVELS";
$scope.dateFormat = 'yyyy-MM-dd';
$scope.currentPage = 1;

$scope.maxSize = 3;
$scope.pageChanged = function () {
  GeoLocationLevelsService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
    $scope.geoLocationLevels = data;
  });
};

$scope.create = function () {
  var modalInstance = $uibModal.open({
    templateUrl: '/pages/setup/geographical_location_level/create.html',
    backdrop: false,
    controller: function ($scope, $uibModalInstance, CreateGeoLocationLevelService) {
      $scope.geoLocationLevelToCreate = {};
      //Function to store data and close modal when Create button clicked
      $scope.save = function () {
        if ($scope.createGeoLocationLevelForm.$invalid) {
          $scope.formHasErrors = true;
          return;
        }
        CreateGeoLocationLevelService.store({perPage:$scope.perPage},$scope.geoLocationLevelToCreate,
            function (data) {
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
            );

      };

      //Function to close modal when cancel button clicked
      $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
      };
    }
  });
  //Called when modal is close by cancel or to store data
  modalInstance.result.then(function (data) {
    //Service to create new financial year
    $scope.successMessage = data.successMessage;
    $scope.geoLocationLevels = data.geoLocationLevels;
    $scope.currentPage = data.current_page;
  },
  function () {
  });

};

$scope.edit = function (geoLocationLevelToEdit, currentPage,perPage) {
  var modalInstance = $uibModal.open({
    templateUrl: '/pages/setup/geographical_location_level/edit.html',
    backdrop: false,
    controller: function ($scope, $uibModalInstance, UpdateGeoLocationLevelService) {
      $scope.geoLocationLevelToEdit = angular.copy(geoLocationLevelToEdit);
      //Function to store data and close modal when Create button clicked
      $scope.update = function () {
        if ($scope.updateGeoLocationLevelForm.$invalid) {
          $scope.formHasErrors = true;
          return;
        }
        UpdateGeoLocationLevelService.update({page: currentPage,perPage:perPage}, $scope.geoLocationLevelToEdit,
            function (data) {
              //Successful function when
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
            );
      };

      //Function to close modal when cancel button clicked
      $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
      };
    }
  });
  //Called when modal is close by cancel or to store data
  modalInstance.result.then(function (data) {
    console.log(data);
    $scope.successMessage = data.successMessage;
    $scope.geoLocationLevels = data.geoLocationLevels;
    $scope.currentPage = $scope.geoLocationLevels.current_page;
  },
  function () {
  });
};

$scope.delete = function (id,currentPage,perPage) {
  ConfirmDialogService.showConfirmDialog(
      'Confirm Delete!', 'Are sure you want to delete this level?')
    .then(function () {
      DeleteGeoLocationLevelService.delete({id: id,page:currentPage,perPage:perPage},
          function (data) {
            $scope.successMessage = data.successMessage;
            $scope.geoLocationLevels = data.geoLocationLevels;
            $scope.currentPage = $scope.geoLocationLevels.current_page;
          }, function (error) {
            $scope.errorMessage = error.data.errorMessage;
          }
          );
    },
    function () {

    });
};
$scope.toggleGeoLocationLevel = function (geo_location_level_id, is_active,perPage) {
  $scope.geoLocationLevelToActivate = {};
  $scope.geoLocationLevelToActivate.id = geo_location_level_id;
  $scope.geoLocationLevelToActivate.is_active = is_active;
  ToggleGeoLocationLevelService.toggleGeoLocationLevel({perPage:perPage},$scope.geoLocationLevelToActivate,
      function (data) {
        $scope.action = data.action;
        $scope.alertType = data.alertType;
      });
};

$scope.trash = function () {
  var modalInstance = $uibModal.open({
    templateUrl: '/pages/setup/geographical_location_level/trash.html',
    backdrop: false,
    controller: function ($scope, $uibModalInstance, TrashedGeoLocationLevelService,
      RestoreGeoLocationLevelService, EmptyGeoLocationLevelTrashService, PermanentDeleteGeoLocationLevelService) {
        TrashedGeoLocationLevelService.query(function (data) {
        $scope.trashedGeoLocationLevels = data;
      });
      $scope.restoreGeoLocationLevel = function (id) {
        RestoreGeoLocationLevelService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
          $scope.successMessage = data.successMessage;
          $scope.trashedGeoLocationLevels = data.trashedGeoLocationLevels;
        }, function (error) {

        }
        );
      };
      $scope.permanentDelete = function (id) {
        PermanentDeleteGeoLocationLevelService.permanentDelete({
          id: id,
          currentPage: $scope.currentPage,
          perPage: $scope.perPage
        }, function (data) {
          $scope.successMessage = data.successMessage;
          $scope.trashedGeoLocationLevels = data.trashedGeoLocationLevels;
        }, function (error) {

        }
        );
      };

      $scope.emptyTrash = function () {
        EmptyGeoLocationLevelTrashService.get({}, function (data) {
          $scope.successMessage = data.successMessage;
          $scope.trashedGeoLocationLevels = data.trashedGeoLocationLevels;
        }, function (error) {
          $scope.errorMessage = error.data.errorMessage;
        }
        );
      };
      //Function to close modal when cancel button clicked
      $scope.close = function () {
        $uibModalInstance.dismiss('cancel');
      };
    }
  });
  //Called when modal is close by cancel or to store data
  modalInstance.result.then(function (data) {
    //Service to create new financial year
    $scope.successMessage = data.successMessage;
    $scope.geoLocationLevels = data.geoLocationLevels;
    $scope.currentPage = data.current_page;
  },
  function () {
  });

};
}

GeoLocationLevelController.resolve = {
GeoLocationLevelsModel: function (GeoLocationLevelsService,$timeout, $q) {
  var deferred = $q.defer();
  $timeout(function () {
    GeoLocationLevelsService.get({page: 1,perPage:10}, function (data) {
      deferred.resolve(data);
    });
  }, 900);
  return deferred.promise;
}
};
