function AccessRightController($scope, AccessRightsModel, $uibModal,

                                 ConfirmDialogService,
                                 DeleteAccessRightService) {

    $scope.accessRights = AccessRightsModel;
    $scope.title = "TITLE_ACCESS_RIGHT";
    $scope.dateFormat = 'yyyy-MM-dd';


    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: 'create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,CreateAccessRightService) {
                $scope.accessRightToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createAccessRightForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateAccessRightService.store($scope.accessRightToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                           $scope.errorMessage=error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage=data.successMessage;
                $scope.accessRights=data.accessRights;

            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (accessRightToEdit) {
        var modalInstance = $uibModal.open({
            templateUrl: 'edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,UpdateAccessRightService) {
                $scope.accessRightToEdit = angular.copy(accessRightToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updateAccessRightForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateAccessRightService.update($scope.accessRightToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage=error.data.errorMessage;
                        }
                    );


                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage=data.successMessage;
                $scope.accessRights=data.accessRights;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'TITLE_CONFIRM_DELETE_ACCESS_RIGHT',
            'CONFIRM_DELETE')
            .then(function () {
                    console.log("YES");
                    DeleteAccessRightService.delete({accessRightId: id},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.accessRights = data.accessRights;
                        }, function (error) {
                            $scope.errorMessage=error.data.errorMessage;
                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    };

}

AccessRightController.resolve = {

    AccessRightsModel: function (AccessRightsService, $q,$timeout) {
        var deferred = $q.defer();
        $timeout(function () {
            AccessRightsService.query({}, function (data) {
                deferred.resolve(data);
            });
        },100);
        return deferred.promise;
    }
};
