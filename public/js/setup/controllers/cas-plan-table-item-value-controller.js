
function CasPlanTableItemValuesController($scope,
                           $uibModal,
                           CasPlanModel,
                           CasPlanContentsService,
                           CasPlanService,
                           CasPlanTableByContentService,
                           CasPlanTableColumnByTableService,
                           CasPlanTableItemByTableService,
                           casPlanTableColumnFormulaService,
                           BaselineStatisticsService,
                           LoadStatisticsService,
                           ConfirmDialogService
) {

    $scope.casPlans = CasPlanModel;
    $scope.title = "CAS_PLANS_TABLE_ITEM_VALUE";
    $scope.dateFormat = 'yyyy-MM-DD';

    $scope.loadContents = function (cas_plan_id) {
        CasPlanContentsService.query({cas_plan_id: cas_plan_id},
            function (data) {
               $scope.casPlanContents = data;
            }
        );
    };

    $scope.loadSelectedPlanTables = function (cas_plan_content_id) {
        CasPlanTableByContentService.get({cas_plan_content_id: cas_plan_content_id},
            function (resp) {
                var data = resp.cas_plan_table;
               $scope.planTables = data;
            }
        );
    };

    $scope.loadSelectedPlanForm = function (selectedPlanTable) {
        $scope.lowerColumns     = [];
        $scope.secondColumns    = [];
        $scope.firstColumns     = [];
        $scope.columns          = [];
        $scope.rows             = [];
        $scope.dimension        = 1;
        $scope.HasverticalTotal = false;
        $scope.singleTypeTable  = false;
        $scope.is_list          = false;
        //check if is single typed table
        $scope.singleValuedTable(selectedPlanTable);
        //flatter Column function
        $scope.flattenColumn = function(arrayData) {
            //find no of rows
            angular.forEach(arrayData, function (value,key) {
                angular.forEach(value, function (value2,key2) {
                   if($scope.dimension < 2) {
                       $scope.dimension = 2;
                   }
                   angular.forEach(value2, function (value3, key3) {
                       $scope.dimension = 3;
                   });
                });
            }) ;
            //end

            angular.forEach(arrayData,function(value,key){
                var colspan = value.children.length;
                if (colspan == 0) {
                    var rowspan = $scope.dimension;
                    colspan = 1;
                }else {
                    var rowspan = 1;
                }
                //check if is list
                if(value.is_list && key == 0 )
                {
                    $scope.is_list = true;
                }
                if(value.is_lowest == true) {
                   $scope.columns.push({id: value.id, type: value.type, formula: value.formula, vertical_total: value.vertical_total, is_lowest: value.is_lowest}) ;
                   if(value.vertical_total)
                   {
                       $scope.HasverticalTotal = true;
                   }
                }
                $scope.firstColumns.push({id: value.id, name: value.name, type: value.type, rowspan: rowspan, colspan: colspan});
                angular.forEach(value.children, function (value2, key2) {
                    var colspan2 = value2.children.length;
                    if (colspan2 == 0) {
                        var rowspan = $scope.dimension -1 ;
                        colspan2 = 1;
                    }else {
                        var rowspan = 1;
                    }
                    if(value2.is_lowest == true) {
                        $scope.columns.push({id: value2.id, type: value2.type, formula: value2.formula, vertical_total: value2.vertical_total}) ;
                        if(value2.vertical_total)
                        {
                            $scope.HasverticalTotal = true;
                        }
                    }
                    $scope.firstColumns[$scope.firstColumns.length -1].colspan = colspan * colspan2;
                    $scope.secondColumns.push({id: value2.id, name: value2.name, colspan: colspan2,rowspan: rowspan, type: value2.type});

                    angular.forEach(value2.children, function (value3, key3) {
                        $scope.lowerColumns.push({id: value3.id, name: value3.name, type: value3.type});
                        $scope.columns.push({id: value3.id, type: value3.type, formula: value3.formula, vertical_total: value3.vertical_total}) ;
                        if(value3.vertical_total)
                        {
                            $scope.HasverticalTotal = true;
                        }
                    });

                });
            });
        };
        //end flatten column

        //flatter row function
        $scope.flattenRow = function (arrayData) {

            //find no of rows
            angular.forEach(arrayData, function (value,key) {
                angular.forEach(value.children, function (value2,key2) {
                    if($scope.dimension < 2) {
                        $scope.dimension = 2;
                    }
                    angular.forEach(value2.children, function (value3, key3) {
                        $scope.dimension = 3;
                    });
                });
            }) ;
            //end
            $scope.dimensionRow = $scope.dimension;
            angular.forEach(arrayData,function(value,key){
                var rowspan = value.children.length;
                var colspan = $scope.dimension;
                if (rowspan == 0) {
                    rowspan = 1;
                }else
                {
                    colspan = 1;
                }

                var row = {id: value.id, name: value.description, colspan: colspan, rowspan: rowspan };
                $scope.rows.push(row);
                angular.forEach(value.children, function (value2, key2) {
                    var rowspan2 = value2.children.length;
                    var colspan2 = 1;
                    if (rowspan2 == 0) {
                        rowspan2 = 1;
                        var row2 = {id: value2.id,name: value2.description, colspan: colspan2, rowspan: rowspan2};
                        if(key2 == 0)
                        {
                          $scope.rows[$scope.rows.length -1 ].children = row2;
                        }else
                        {
                          $scope.rows.push(row2);
                        }

                    }else
                    {
                        colspan2 = 1;
                        var row2 = {id: value2.id, name: value2.description, colspan: colspan2, rowspan: rowspan2};
                    }

                    angular.forEach(value2.children, function (value3, key3) {
                        var colspan3 = 1;
                        var rowspan3 = 1;
                        var row3 = {id: value3.id,name: value3.description, colspan: colspan3, rowspan: rowspan3 };
                        if(key3 == 0)
                        {
                            $scope.rows[$scope.rows.length -1 ].children = row3;
                        }else
                        {
                            $scope.rows.push(row3);
                        }

                    });

                });
            });
        };
        //end flatter row
        //load columns
        CasPlanTableColumnByTableService.query({cas_plan_table_id:selectedPlanTable},
            function (data) {
                $scope.flattenColumn(data);
        });
        //load Rows
        CasPlanTableItemByTableService.query({cas_plan_table_id:selectedPlanTable},
            function (data) {
                $scope.dimension = 1; //reset dimension
                if(data.length > 0)
                {
                    $scope.flattenRow(data);
                }else
                {
                    var row = {id: 0, name: 'ITEM I', colspan: 1, rowspan: 1 };
                    $scope.rows.push(row);
                }

                $scope.planTableItems = data;
        });
    };

    $scope.editFormula = function (column) {

            var modalInstance = $uibModal.open({
                templateUrl: 'edit-formula.html',
                backdrop: false,
                controller: function ($scope, $uibModalInstance) {
                    $scope.casPlanTableFormula = {};
                    $scope.casPlanTableFormula = angular.copy(column);

                    //The service to pull the list of the baseline statistics which have been defined by the administrator
                    LoadStatisticsService.query({}, function (data) {
                        $scope.baselineStatistics = data;
                    });

                    $scope.store = function () {
                        casPlanTableColumnFormulaService.updateFormula($scope.casPlanTableFormula,
                            function (data) {

                                $uibModalInstance.close(data);
                            },
                            function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                            }
                        );

                    };

                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            //Called when modal is close by cancel or to store data
            modalInstance.result.then(function (data) {
                    //update columns
                    $scope.successMessage = data.successMessage;
                },
                function () {
                    //If modal is closed
                    console.log('Modal dismissed at: ' + new Date());
                });
    };

    $scope.singleValuedTable = function (selectedPlanTable) {
        angular.forEach($scope.planTables, function (value, key) {
           if(value.id == selectedPlanTable && value.type==1)
           {
               $scope.singleTypeTable = true;
               $scope.report_url = value.report_url;
           }
        });
    };
}

CasPlanTableItemValuesController.resolve = {
    CasPlanModel: function (CasPlanService, $q) {
        var deferred = $q.defer();
        CasPlanService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};
