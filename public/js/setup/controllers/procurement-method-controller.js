function ProcurementMethodController($scope, DataModel, $uibModal, ConfirmDialogService, ProcurementMethodService) {

  $scope.items = DataModel;
  $scope.title = "PROCUREMENT_METHODS";
  $scope.currentPage = 1;
  $scope.maxSize = 3;
  $scope.perPage = 10;

  $scope.pageChanged = function () {
    ProcurementMethodService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (response) {
      $scope.items = response.items;
    }, function (error) {
      console.log(error);
    });
  };

  $scope.create = function () {
    let modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/procurement_method/create.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance) {
        $scope.createDataModel = {};
        $scope.store = function () {
          if ($scope.createForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          ProcurementMethodService.save({perPage: $scope.perPage}, $scope.createDataModel,
            function (data) {
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
              $scope.errors = error.data.errors;
            }
          );
        };
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    modalInstance.result.then(function (response) {
        $scope.successMessage = response.successMessage;
        $scope.items = response.items;
        $scope.currentPage = response.items.current_page;
      },
      function () {

      });

  };

  $scope.edit = function (updateDataModel, currentPage, perPage) {
    let modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/procurement_method/edit.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance) {

        $scope.updateDataModel = angular.copy(updateDataModel);

        $scope.update = function () {
          if ($scope.updateForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          ProcurementMethodService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
            function (data) {
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
              $scope.errors = error.data.errors;
            }
          );
        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (response) {
        //Service to create new financial year
        $scope.successMessage = response.successMessage;
        $scope.items = response.items;
        $scope.currentPage = response.items.current_page;
      },
      function () {

      });
  };

  $scope.delete = function (id, currentPage, perPage) {
    ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
        ProcurementMethodService.delete({id: id, page: currentPage, perPage: perPage},
          function (data) {
            $scope.successMessage = data.message;
            $scope.items = data.data;
            $scope.currentPage = $scope.items.data.current_page;
          }, function (error) {
            $scope.errorMessage = error.data.errorMessage;
          }
        );
      },
      function () {
        console.log("NO");
      });
  };
}

ProcurementMethodController.resolve = {
  DataModel: function (ProcurementMethodService, $q) {
    let deferred = $q.defer();
    ProcurementMethodService.paginated({page: 1, perPage: 10}, function (response) {
      deferred.resolve(response.items);
    });
    return deferred.promise;
  }
};
