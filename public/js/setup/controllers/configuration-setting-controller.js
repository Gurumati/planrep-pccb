function ConfigurationSettingController($scope, Configurations, SaveConfigurationService) {

    $scope.title = "TITLE_CONFIGURATION";

    var group = function (configs) {
        $scope.configurations = _.groupBy(configs, function (config) {
            return config.group_name;
        });
    };
    console.log(Configurations);
    group(Configurations);
    $scope.getOptions = function (optionsString) {
        if(optionsString !== null ) {
            return optionsString.split(',');
        }
        else {return [];}
    };

    $scope.save = function (data) {
        SaveConfigurationService.update(data, function (data) {
            $scope.successMessage = data.successMessage;
            group(data.configs);
        });
    };
}

ConfigurationSettingController.resolve = {
    Configurations: function (ConfigurationsService, $q) {
        var deferred = $q.defer();
        ConfigurationsService.query(function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};