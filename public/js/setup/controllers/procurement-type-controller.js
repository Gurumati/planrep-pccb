function ProcurementTypeController($scope, AllProcurementTypeModel, $uibModal,
                                   ConfirmDialogService, PaginatedProcurementTypeService, ToggleProcurementTypeService,
                                   DeleteProcurementTypeService) {

    $scope.procurementTypes = AllProcurementTypeModel;
    $scope.title = "TITLE_PROCUREMENT_TYPES";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedProcurementTypeService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.procurementTypes = data;
        });
    };

    $scope.playAudio = function (file) {
        if (file === 'alert') {
            document.getElementById('audio-alert').play();
        }
        if (file === 'fail') {
            document.getElementById('audio-fail').play();
        }

    };

    $scope.create = function () {
        //Modal Form for creating financial year
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/procurement_type/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateProcurementTypeService) {
                $scope.procurementTypeToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createProcurementTypeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateProcurementTypeService.store({perPage: $scope.perPage}, $scope.procurementTypeToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.procurementTypes = data.procurementTypes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (procurementTypeToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/procurement_type/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateProcurementTypeService) {
                $scope.procurementTypeToEdit = angular.copy(procurementTypeToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updateProcurementTypeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateProcurementTypeService.update({
                            currentPage: currentPage,
                            perPage: perPage
                        }, $scope.procurementTypeToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.procurementTypes = data.procurementTypes;
                $scope.currentPage = $scope.procurementTypes.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog(
            'TITLE_CONFIRM_DELETE_PROCUREMENT_TYPE',
            'CONFIRM_DELETE')
            .then(function () {
                    console.log("YES");
                    DeleteProcurementTypeService.delete({
                            procurement_type_id: id,
                            currentPage: currentPage,
                            perPage: perPage
                        },
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.procurementTypes = data.procurementTypes;
                            $scope.currentPage = $scope.procurementTypes.current_page;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }
    $scope.toggleProcurementType = function (id, is_active, perPage) {
        $scope.procurementTypeToActivate = {};
        $scope.procurementTypeToActivate.id = id;
        $scope.procurementTypeToActivate.is_active = is_active;
        ToggleProcurementTypeService.toggleProcurementType({perPage: perPage}, $scope.procurementTypeToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
                if(is_active === true)
                    $scope.playAudio('alert');

                if(is_active === false)
                    $scope.playAudio('fail');
            });
    }
}

ProcurementTypeController.resolve = {
    AllProcurementTypeModel: function (PaginatedProcurementTypeService, $q) {
        var deferred = $q.defer();
        PaginatedProcurementTypeService.get({page: 1, perPage: 2}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};