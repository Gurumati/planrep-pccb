function ActivityTaskNatureController ($scope, DataModel, $uibModal,
                                      ConfirmDialogService,ActivityTaskNatureService) {

    $scope.items = DataModel;
    $scope.title = "ACTIVITY_TASK_NATURES";
    $scope.currentPage = 1;
    $scope.maxSize = 3;

    $scope.pageChanged = function () {
        ActivityTaskNatureService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/activity_task_nature/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,ActivityCategories) {
                ActivityCategories.getActive().then(function (response) {
                        $scope.activityCategories = response.data;
                     }, function (error) {
                         $scope.errorMessage = error.data.errorMessage;
                });

                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    ActivityTaskNatureService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        //console.log(updateDataModel);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/activity_task_nature/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,ActivityCategories) {

                ActivityCategories.getActive().then(function (response) {
                    $scope.activityCategories = response.data;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                }, function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                });


                $scope.update = function () {
                    ActivityTaskNatureService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = $scope.items.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                ActivityTaskNatureService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.items = data.items;
                        $scope.currentPage = $scope.items.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {
                console.log("NO");
            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/activity_task_nature/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance) {
                ActivityTaskNatureService.trashed(function (data) {
                    $scope.trashedTaskNatures = data.trashedTaskNatures;
                });
                $scope.restoreActivityTaskNature = function (id) {
                    ActivityTaskNatureService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedTaskNatures = data.trashedTaskNatures;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    ActivityTaskNatureService.permanentDelete({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedTaskNatures = data.trashedTaskNatures;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };

                $scope.emptyTrash = function () {
                    ActivityTaskNatureService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedTaskNatures = data.trashedTaskNatures;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.toggleActive = function(id) {
        ActivityTaskNatureService.toggleActive({id: id}, function(data) {
            $scope.successMessage = data.successMessage;
            $scope.pageChanged();
        });
    };
}

ActivityTaskNatureController .resolve = {
    DataModel: function (ActivityTaskNatureService, $q) {
        var deferred = $q.defer();
        ActivityTaskNatureService.paginated({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};