function BudgetSubmissionSelectOptionController($scope,
                                                $uibModal,
                                                BudgetSubmissionSelectOptionService,
                                                ConfirmDialogService,
                                                optionsModel) {
    $scope.options = optionsModel;
    $scope.title = "TITLE_SECTIONS";
    $scope.dateFormat = 'yyyy-MM-dd';

    BudgetSubmissionSelectOptionService.query({}, function (data) {
      $scope.errorMessage = false;
      $scope.options = data;
    });
  $scope.loadChildren = function (a) {
    if (a.children === undefined) {
      a.children = [];
    }
    $scope.expanded = true;
    BudgetSubmissionSelectOptionService.getChildren({id: a.id}, function (data) {

      $scope.children = data.children;
    });
  };
  $scope.setSelected = function (a) {
    $scope.selectedOptionId = a.id;
  };
    $scope.create = function (options) {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/select_options/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,BudgetSubmissionSelectOptionService) {
                $scope.sectionToCreate = {};
                $scope.selectionGroup  = options;
                //Function to store data and close modal when Create button clicked

                $scope.store = function () {
                    if ($scope.createSelectOptionForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                  if($scope.optionToCreate.parent_id != null){
                    BudgetSubmissionSelectOptionService.save($scope.optionToCreate,
                      function (data) {
                        $uibModalInstance.close(data);
                      },
                      function (error) {
                        console.log(error);
                        $scope.errorMessage=error.data.errorMessage;
                      }
                    );
                  }else {
                    $scope.errorMessage="Group is required";
                  }
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
          $scope.successMessage=data.successMessage;
          //reload data
          BudgetSubmissionSelectOptionService.query({}, function (data) {
              $scope.options = data;
          });
        },
        function () {
            //If modal is closed
            console.log('Modal dismissed at: ' + new Date());

        });
    };

    $scope.edit = function (optionToEdit, options) {
      if(optionToEdit.id == 1 || optionToEdit.id == 2){
           $scope.errorMessage ="This value can't be updated";
      }else {
        $scope.errorMessage =false;
        var modalInstance = $uibModal.open({
          templateUrl: '/pages/setup/select_options/edit.html',
          backdrop: false,
          controller: function ($scope, $q, $uibModalInstance, BudgetSubmissionSelectOptionService) {
            $scope.optionToEdit = angular.copy(optionToEdit);
            $scope.selectionGroup = options;

            //Function to store data and close modal when Create button clicked
            $scope.update = function () {
              if ($scope.updateSelectOptionForm.$invalid) {
                $scope.formHasErrors = true;
                return;
              }
              if($scope.optionToEdit.parent_id != null) {
                BudgetSubmissionSelectOptionService.update($scope.optionToEdit, function (data) {
                    $uibModalInstance.close(data);
                  },
                  function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                  }
                );
              }else {
                $scope.errorMessage="Group is required";
              }
            };
            //Function to close modal when cancel button clicked
            $scope.close = function () {
              $uibModalInstance.dismiss('cancel');
            }
          }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
            $scope.successMessage = data.successMessage;
            BudgetSubmissionSelectOptionService.query({}, function (data) {
              $scope.options = data;
            });
          },
          function () {
            //If modal is closed
            console.log('Modal dismissed at: ' + new Date());
          });
      }
    };

    $scope.delete = function (id) {
        if(id == 1 || id == 2){
          $scope.errorMessage ="This value can't be deleted";
        }else{
          $scope.errorMessage =false;
        ConfirmDialogService.showConfirmDialog('TITLE_CONFIRM_DELETE_OPTION', 'CONFIRM_DELETE').then(function () {
            BudgetSubmissionSelectOptionService.delete({id: id},
              function (data) {
                $scope.successMessage = data.successMessage;
                BudgetSubmissionSelectOptionService.query({}, function (data) {
                  $scope.options = data;
                });
              }, function (error) {
                $scope.errorMessage = error.data.errorMessage;
              }
            );
          },
          function () {
            console.log("NO");
          });
      }
    }
}
BudgetSubmissionSelectOptionController.resolve = {
    optionsModel: function (BudgetSubmissionSelectOptionService, $q) {
        var deferred = $q.defer();
        BudgetSubmissionSelectOptionService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};
