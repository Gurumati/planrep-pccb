function NationalTargetController($scope, DataModel, $uibModal, GlobalService, Upload, $timeout,
                                  ConfirmDialogService, NationalTargetService) {

    $scope.items = DataModel;
    $scope.title = "NATIONAL_TARGETS";

    $scope.currentPage = 1;
    $scope.maxSize = 3;

    $scope.pageChanged = function () {
        NationalTargetService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data;
        });
    };

    GlobalService.priorityAreas(function (data) {
        $scope.priorityAreas = data.data;
    });

    $scope.showList = true;
    $scope.showUploadForm = false;

    $scope.toggleUploadForm = function (state) {
        if (state === true) {
            $scope.showList = false;
        } else {
            $scope.showList = true;
        }
        $scope.showUploadForm = state;
    };

    $scope.uploadFile = function (file) {
        file.upload = Upload.upload({
            url: '/files/national-targets/upload',
            method: 'POST',
            data: {priority_area_id: $scope.priority_area_id, file: file}
        });

        file.upload.then(function (response) {
            $timeout(function () {
                file.result = response.data;
                $scope.items = response.data.items;
                $scope.toggleUploadForm(false);
                $scope.successMessage = response.data.successMessage;
            });
        }, function (response) {
            if (response.status > 0) {
                $scope.errorMsg = response.status + ': ' + response.data;
                $scope.errorMessage = response.data.errorMessage;
                $scope.errorData = response.data.errorData;
                $scope.errorRows = response.data.errorRows;
                $scope.items = response.data.items;
            }
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/national_target/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, NationalTargetService, GlobalService) {

                $scope.createDataModel = {};

                GlobalService.priorityAreas(function (data) {
                    $scope.priorityAreas = data.data;
                });

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                   
                    NationalTargetService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        //console.log(updateDataModel);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/national_target/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, GlobalService, NationalTargetService) {
                GlobalService.priorityAreas(function (data) {
                    $scope.priorityAreas = data.data;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });

                $scope.update = function () {
                    NationalTargetService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = $scope.items.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                NationalTargetService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.items = data.items;
                        $scope.currentPage = $scope.items.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/national_target/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance, NationalTargetService) {
                NationalTargetService.trashed(function (data) {
                    $scope.trashedItems = data.trashedItems;
                });
                $scope.restoreNationalTarget = function (id) {
                    NationalTargetService.restore({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    NationalTargetService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    NationalTargetService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

NationalTargetController.resolve = {
    DataModel: function (NationalTargetService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            NationalTargetService.paginated({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        }, 900);
        return deferred.promise;
    }
};