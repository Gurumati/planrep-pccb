function ExpenditureCentreController($scope, ExpenditureCentreModel, $uibModal, AllExpenditureCentreGroupService,AllTaskNatureService,
                                     ConfirmDialogService, GlobalService, $http,ExpenditureCentreService, ExpenditureGfsCodeService) {

    $scope.expenditureCentres = ExpenditureCentreModel;
    $scope.title = "EXPENDITURE_CENTRES";

    AllExpenditureCentreGroupService.query(function (data) {
        $scope.expenditureCentreGroups = data;
    });

    $scope.loadExpenditureCentres = function (expenditure_centre_group) {
        var group_id = expenditure_centre_group.id;
        GlobalService.groupExpenditureCentres({group_id: group_id}, function (data) {
            $scope.expenditureCentres = data.expenditureCentres;
        });
    };

    $scope.showList = true;
    $scope.showGFSCodes = false;
    $scope.showTaskNatures = false;
    $scope.showCostCentres = false;

    $scope.gfsCodeList = function (expenditureCentre) {

        $scope.showList = false;
        $scope.showGFSCodes = true;

        $scope.createDataModel = {};
        $scope.expenditure_centre_id = expenditureCentre.id;
        $scope.code = expenditureCentre.code;
        $scope.name = expenditureCentre.name;

        ExpenditureGfsCodeService.query(function (data) {
            $scope.expenditureGfsCodes = data;
        });

        $scope.getExpenditureGfsCodes = function (searchText) {
            return $http.get('/json/expenditureGfsCodes/searchGfs?searchQuery=' + searchText)
                .then(function (response) {
                    return response.data.gfsCodes;
                });
        };

        $scope.expenditureGfsCodeChange = function(item) {
            if (item !== undefined) {
                $scope.createDataModel.gfs_code_id = item.id;
            }
        };

        ExpenditureCentreService.links({id: expenditureCentre.id}, function (data) {
                $scope.successMessage = data.successMessage;
                $scope.links = data.links;
            }, function (error) {
                console.log(error)
            }
        );

        $scope.addItem = function () {
            var dataToSave = {
                "gfs_code_id": $scope.createDataModel.gfs_code_id,
                "expenditure_centre_id": $scope.expenditure_centre_id,
                "activity_category_id": $scope.createDataModel.activity_category_id,
                "lga_level_id": $scope.createDataModel.lga_level_id
            };
            ExpenditureCentreService.addLink(dataToSave, function (data) {
                    $scope.successMessage = data.successMessage;
                    $scope.links = data.links;
                },
                function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.links = data.links;
                }
            );
        };

        $scope.deleteItem = function (id, expenditure_centre_id) {
            ExpenditureCentreService.removeLink({
                    id: id,
                    expenditure_centre_id: expenditure_centre_id
                }, function (data) {
                    $scope.successMessage = data.successMessage;
                    $scope.links = data.links;
                }, function (error) {

                }
            );
        };
        $scope.close = function () {
            $scope.showList = true;
            $scope.showGFSCodes = false;
        }
    };

    $scope.taskNatureList = function (expenditureCentre) {

        $scope.showList = false;
        $scope.showTaskNatures = true;

        $scope.createDataModel = {};
        $scope.expenditure_centre_id = expenditureCentre.id;
        $scope.code = expenditureCentre.code;
        $scope.name = expenditureCentre.name;

        AllTaskNatureService.query(function (data) {
            $scope.taskNatures = data;
        });

        ExpenditureCentreService.taskNatures({expenditure_centre_id: expenditureCentre.id}, function (data) {
                $scope.successMessage = data.successMessage;
                $scope.items = data.taskNatures;
            }, function (error) {
                console.log(error)
            }
        );

        $scope.addItem = function () {
            var dataToSave = {
                "task_natures": $scope.createDataModel.task_natures,
                "expenditure_centre_id": $scope.expenditure_centre_id,
            };
            ExpenditureCentreService.addTaskNature(dataToSave, function (data) {
                    $scope.successMessage = data.successMessage;
                    $scope.items = data.taskNatures;
                },
                function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.items = data.taskNatures;
                }
            );
        };

        $scope.deleteItem = function (id, expenditure_centre_id) {
            ExpenditureCentreService.removeTaskNature({
                    id: id,
                    expenditure_centre_id: expenditure_centre_id
                }, function (data) {
                    $scope.successMessage = data.successMessage;
                $scope.items = data.taskNatures;
                }, function (error) {

                }
            );
        };
        $scope.close = function () {
            $scope.showList = true;
            $scope.showTaskNatures = false;
        }
    };

    $scope.costCentreList = function (expenditureCentre) {

        $scope.showList = false;
        $scope.showCostCentres = true;

        $scope.createDataModel = {};
        $scope.expenditure_centre_id = expenditureCentre.id;
        $scope.code = expenditureCentre.code;
        $scope.name = expenditureCentre.name;

        GlobalService.groupCostCentres({section_id: expenditureCentre.expenditure_centre_group.section_id}, function (data) {
            $scope.sections = data.sections;
        });

        ExpenditureCentreService.costCentres({expenditure_centre_id: expenditureCentre.id}, function (data) {
                $scope.items = data.costCentres;
            }, function (error) {
                console.log(error)
            }
        );

        $scope.addItem = function () {
            var dataToSave = {
                "sections": $scope.createDataModel.sections,
                "expenditure_centre_id": $scope.expenditure_centre_id,
            };
            ExpenditureCentreService.addCostCentre(dataToSave, function (data) {
                    $scope.successMessage = data.successMessage;
                    $scope.items = data.costCentres;
                },
                function (error) {
                    $scope.errorMessage = error.data.errorMessage;
                    $scope.items = data.costCentres;
                }
            );
        };

        $scope.deleteItem = function (id, expenditure_centre_id) {
            ExpenditureCentreService.removeCostCentre({
                    id: id,
                    expenditure_centre_id: expenditure_centre_id
                }, function (data) {
                    $scope.successMessage = data.successMessage;
                $scope.items = data.costCentres;
                }, function (error) {

                }
            );
        };
        $scope.close = function () {
            $scope.showList = true;
            $scope.showCostCentres = false;
        }
    };

    $scope.min_max = function (expenditureCentre) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/expenditure_centre/values.html',
            backdrop: false,
            controller: function ($scope, $q, GlobalService, $uibModalInstance, ExpenditureCentreService) {

                $scope.createDataModel = {};
                $scope.expenditure_centre_id = expenditureCentre.id;
                $scope.code = expenditureCentre.code;
                $scope.name = expenditureCentre.name;

                ExpenditureCentreService.values({id: expenditureCentre.id}, function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.values = data.values;
                    }, function (error) {
                        console.log(error)
                    }
                );

                $scope.addItem = function () {
                    var dataToSave = {
                        "min_value": $scope.createDataModel.min_value,
                        "max_value": $scope.createDataModel.max_value,
                        "expenditure_centre_id": $scope.expenditure_centre_id
                    };
                    ExpenditureCentreService.addValue(dataToSave, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.values = data.values;
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.values = data.values;
                        }
                    );
                };

                $scope.deleteItem = function (id, expenditure_centre_id) {
                    ExpenditureCentreService.removeValue({
                            id: id,
                            expenditure_centre_id: expenditure_centre_id
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.values = data.values;
                        }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                        $scope.values = data.values;
                        }
                    );
                };

                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.create = function (expenditure_centre_group) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/expenditure_centre/create.html',
            backdrop: false,
            controller: function ($scope,$route, $uibModalInstance,ExpenditureGfsCodeService, GlobalService, ExpenditureCentreService, AllTaskNatureService) {
                $scope.createDataModel = {};

                $scope.groupName = expenditure_centre_group.name;
                $scope.expenditure_centre_group = expenditure_centre_group;

                AllExpenditureCentreGroupService.query(function (data) {
                    $scope.expenditureCentreGroups = data;
                });

                AllTaskNatureService.query(function (data) {
                    $scope.taskNatures = data;
                });

                GlobalService.linkSpecs(function (data) {
                    $scope.linkSpecs = data.linkSpecs;
                });

                ExpenditureGfsCodeService.query(function (data) {
                    $scope.expenditureGfsCodes = data;
                });

                GlobalService.activityCategories(function (data) {
                    $scope.activityCategories = data.items;
                });

                GlobalService.lgaLevels(function (data) {
                    $scope.lgaLevels = data.lgaLevels;
                });

                GlobalService.groupCostCentres({section_id: expenditure_centre_group.section_id}, function (data) {
                    $scope.sections = data.sections;
                });

                GlobalService.expenditureCentres(function (data) {
                    $scope.allExpenditureCentres = data.expenditureCentres;
                });

                $scope.store = function () {
                    /*if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }*/
                    ExpenditureCentreService.save({group_id: expenditure_centre_group.id}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.expenditureCentres = data.expenditureCentres;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/expenditure_centre/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,ActivityCategories, ExpenditureGfsCodeService, GlobalService, AllExpenditureCentreGroupService, AllTaskNatureService) {

                AllExpenditureCentreGroupService.query(function (data) {
                    $scope.expenditureCentreGroups = data;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });

                GlobalService.linkSpecs(function (data) {
                    $scope.linkSpecs = data.linkSpecs;
                });

                GlobalService.activityCategories(function (data) {
                    $scope.activityCategories = data.items;
                });

                GlobalService.lgaLevels(function (data) {
                    $scope.lgaLevels = data.lgaLevels;
                });


                GlobalService.expenditureCentres(function (data) {
                    $scope.allExpenditureCentres = data.expenditureCentres;
                });

                $scope.update = function () {
                    if ($scope.updateForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    ExpenditureCentreService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },

                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.expenditureCentres = data.expenditureCentres;
                $scope.currentPage = $scope.expenditureCentres.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('CONFIRM_DELETE', 'Are you sure you want to trash this record?').then(function () {
                ExpenditureCentreService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.expenditureCentres = data.expenditureCentres;
                        $scope.currentPage = $scope.expenditureCentres.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/expenditure_centre/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance,ExpenditureCentreService) {
                ExpenditureCentreService.trashed(function (data) {
                    $scope.trashedExpenditureCentres = data.trashedExpenditureCentres;
                });
                $scope.restoreExpenditureCentre = function (id) {
                    ExpenditureCentreService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedExpenditureCentres = data.trashedExpenditureCentres;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    ExpenditureCentreService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedExpenditureCentres = data.trashedExpenditureCentres;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    ExpenditureCentreService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedExpenditureCentres = data.trashedExpenditureCentres;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.expenditureCentres = data.expenditureCentres;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

ExpenditureCentreController.resolve = {
    ExpenditureCentreModel: function (ExpenditureCentreService, $q) {
        var deferred = $q.defer();
        ExpenditureCentreService.fetchAll(function (data) {
            deferred.resolve(data.expenditureCentres);
        });
        return deferred.promise;
    }
};