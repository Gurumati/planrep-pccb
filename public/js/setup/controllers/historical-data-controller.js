function HistoricalDataController ($scope, DataModel, $uibModal,GlobalService, ConfirmDialogService,HistoricalDataService) {

    $scope.items = DataModel;
    $scope.title = "Custom Historical Data";
    $scope.currentPage = 1;
    $scope.perPage = 10;
    $scope.maxSize = 3;
    
    $scope.pageChanged = function () {
        HistoricalDataService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/historical-data/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {

                $scope.createDataModel = {};

                $scope.dataGrouping = {
                    'COUNCIL':'Council',
                    'DEPARTMENT':'Department',
                };

                $scope.tableTypes = {
                    'REVENUE':'REVENUE',
                    'EXPENDITURE':'EXPENDITURE',
                };

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    HistoricalDataService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/historical-data/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {

                $scope.updateDataModel = angular.copy(updateDataModel);

                $scope.dataGrouping = {
                    'COUNCIL':'Council',
                    'DEPARTMENT':'Department',
                };


                $scope.tableTypes = {
                    'REVENUE':'REVENUE',
                    'EXPENDITURE':'EXPENDITURE',
                };

                $scope.update = function () {
                    HistoricalDataService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = $scope.items.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.showColumns = function (table) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/historical-data/columns.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {

                $scope.dataTable = table;

                HistoricalDataService.columns({historical_data_table_id: table.id}, function (data) {
                        $scope.columns = data.columns;
                    },
                    function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );

                $scope.columnData = {};

                $scope.addColumn = function () {
                    HistoricalDataService.addHistoricalDataColumn({historical_data_table_id: table.id},$scope.columnData,
                        function (data) {
                            $scope.columns = data.columns;
                            $scope.successMessage = data.successMessage;
                            $scope.columnData = {};
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                $scope.editDataColumn = false;
                $scope.editColumn = function (item) {
                    $scope.editDataColumn = true;
                    $scope.row_id = item.id;
                    $scope.columnDataUpdate = angular.copy(item);

                    $scope.updateColumn = function () {
                        HistoricalDataService.updateDataColumn({historical_data_table_id:item.id,id:item.id},$scope.columnDataUpdate,
                            function (data) {
                                $scope.columns = data.columns;
                                $scope.successMessage = data.successMessage;
                                $scope.editDataColumn = false;
                                $scope.row_id = undefined;
                            },
                            function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                                $scope.errors = error.data.errors;
                            }
                        );
                    };

                    $scope.cancelUpdateColumn = function () {
                        $scope.editDataColumn = false;
                        $scope.row_id = undefined;
                    }
                };

                $scope.removeColumn = function (id) {
                    HistoricalDataService.removeHistoricalDataColumn({historical_data_table_id: table.id,id:id},
                        function (data) {
                            $scope.columns = data.columns;
                            $scope.successMessage = data.successMessage;
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };

                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                HistoricalDataService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.items = data.items;
                        $scope.currentPage = $scope.items.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
}

HistoricalDataController .resolve = {
    DataModel: function (HistoricalDataService, $q) {
        var deferred = $q.defer();
        HistoricalDataService.paginated({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};