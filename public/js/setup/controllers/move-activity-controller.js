function MoveActivityController($scope,MoveActivity,FacilityService,$q, FinancialYear) {
    $scope.title = "Move Activity";

    FinancialYear.get({}, function(data){
        $scope.financialYears = data.financialYears;
    });

    $scope.loadMtef = function(financialYearId){
        MoveActivity.getMtefs({financialYearId:financialYearId}, function (data) {
            $scope.mtefs= data;    
        });
    };

     MoveActivity.getBF(function (data) {
         $scope.budgetClasses= data.budgetClasses;
         $scope.fundSources= data.fundSources;

     });

     $scope.loadMtefSections = function (mtefId, financialYearId) {
         MoveActivity.getMtefSections(
             {mtefId:mtefId, financialYearId:financialYearId
                },function (data) {
             $scope.mtefSections= data;
         });
     };

     $scope.getActivities = function (budgetType,mtefId,mtefSectionId) {
         $scope.activities=[];
         if(budgetType === undefined || mtefId === undefined || mtefSectionId === undefined) {
            return;
         }
         MoveActivity.getActivities({budgetType:budgetType,mtefId: mtefId, mtefSectionId: mtefSectionId}, function (data) {
            $scope.activities = data;
        });
     };

    $scope.searchFacility =function ($query,a) {
        var deferred = $q.defer();
        if($query !== undefined && $query !== '' && $query.length >= 1) {
            $scope.facilityLoading = true;
            var facilityToExclude = {'facilityIdsToExclude':[]};
            FacilityService.searchByPlanningSection({mtefSectionId:a.mtef_section_id,
                    searchQuery: $query,
                    isFacilityAccount:a.is_facility_account, noLoader: true
                },
                facilityToExclude,
                function (data) {
                    $scope.facilityLoading = false;
                    deferred.resolve(data.facilities);
                });
        }
        else{
            deferred.resolve([]);
        }
        return deferred.promise;
    };

     $scope.moveCostCentre = function (a,newMtefSectionId) {
         MoveActivity.moveCostCentre({activityId:a.id,mtefSectionId:newMtefSectionId},function (data) {
             console.log(data);
             $scope.successMessage = data.successMessage;
             a.newCostCentre = data.newActivity.mtef_section.section.name;
             a.costCentreChanged = true;
             a.newTargets = data.targets;
         },function (error) {
             a.costCentreChangedError = true;
         });
     };

    $scope.moveTarget = function (a,newTargetId) {
        MoveActivity.moveTarget({activityId:a.id,targetId:newTargetId},function (data) {
            $scope.successMessage = data.successMessage;
            a.newTarget = data.newActivity.annual_target.description;
            a.newTargetCode = data.newActivity.annual_target.code;
            a.targetChanged = true;
        },function (error) {
            a.targetChangedError = true;
        });
    };

     $scope.moveFacility = function (af, newfacility) {
         console.log(af);
         console.log(newfacility);
         MoveActivity.moveFacility({activityFacilityId:af.id,facilityId:newfacility.id},function (data) {
             $scope.successMessage = data.successMessage;
             af.newFacility = data.newFacility.facility.name;
             af.facilityChanged = true;
         },function (error) {
             af.facilityChangedError = true;
         });
     };

     $scope.moveFundSource = function (fu,newFundSourceId) {
         MoveActivity.moveFundSource({activityFacilityFundSourceId:fu.id,fundSourceId:newFundSourceId},function (data) {
             $scope.successMessage = data.successMessage;
             fu.newFundSource = data.newFundSource.fund_source.name;
             fu.fundSourceChanged = true;
         },function (error) {
             fu.fundSourceChangedError = true;
         });
     };

    $scope.moveBudgetClass = function (a,newBudgetClassId) {
        MoveActivity.moveBudgetClass({activityId:a.id,budgetClassId:newBudgetClassId},function (data) {
            $scope.successMessage = data.successMessage;
            a.newBudgetClass = data.newActivity.budget_class.name;
            a.budgetClassChanged = true;
        },function (error) {
            a.budgetClassChangedError = true;
        });
    };
}


