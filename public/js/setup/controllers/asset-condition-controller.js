function AssetConditionController($scope, AssetConditionModel, $uibModal,
                                  ConfirmDialogService, AssetConditionService) {

    $scope.items = AssetConditionModel;
    $scope.title = "ASSET_CONDITIONS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        AssetConditionService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/asset_condition/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AssetConditionService) {

                $scope.createDataModel = {};

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    AssetConditionService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        //console.log(updateDataModel);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/asset_condition/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AssetConditionService) {

                $scope.updateDataModel = angular.copy(updateDataModel);

                $scope.update = function () {
                    AssetConditionService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = $scope.items.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                AssetConditionService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.items = data.items;
                        $scope.currentPage = $scope.items.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                        $scope.errors = error.data.errors;
                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/asset_condition/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance, AssetConditionService) {
                AssetConditionService.trashed(function (data) {
                    $scope.trashedItems = data.trashedItems;
                });
                $scope.restoreItem = function (id) {
                    AssetConditionService.restore({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    AssetConditionService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };

                $scope.emptyTrash = function () {
                    AssetConditionService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

AssetConditionController.resolve = {
    AssetConditionModel: function (AssetConditionService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            AssetConditionService.paginated({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        }, 100);
        return deferred.promise;
    }
};