function VersionTypeController($scope, DataModel, $uibModal, ConfirmDialogService, VersionTypeService) {

    $scope.items = DataModel;
    $scope.title = "VERSION_TYPES";
    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.perPage = 10;
    $scope.dateFormat = 'yyyy-MM-dd';

    $scope.pageChanged = function () {
        VersionTypeService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data.data;
        });
    };

    $scope.create = function () {
        let modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/version_type/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {
                $scope.createDataModel = {};
                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    VersionTypeService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.message;
                $scope.items = data.data;
                $scope.currentPage = data.data.current_page;
            },
            function () {

            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        let modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/version_type/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance) {

                $scope.updateDataModel = angular.copy(updateDataModel);

                $scope.update = function () {
                    if ($scope.updateForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    VersionTypeService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.message;
                $scope.items = data.data;
                $scope.currentPage = $scope.items.data.current_page;
            },
            function () {

            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                VersionTypeService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.message;
                        $scope.items = data.data;
                        $scope.currentPage = $scope.items.data.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
}

VersionTypeController.resolve = {
    DataModel: function (VersionTypeService, $q) {
        var deferred = $q.defer();
        VersionTypeService.paginated({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data.data);
        });
        return deferred.promise;
    }
};