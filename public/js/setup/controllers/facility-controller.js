function FacilityController($scope, UserAdminHierarchy, loadServiceProviders, $uibModal, Upload, $timeout, GlobalService,
  ToggleFacilityService, FacilityService, localStorageService,
  ConfirmDialogService, SearchFacilityService,
  DeleteFacilityService, AdminHierarchiesService) {

  $scope.userAdminHierarchy = UserAdminHierarchy;
  $scope.userAdminHierarchyCopy = angular.copy(UserAdminHierarchy);

  //onload a service Provider
  $scope.facilities = loadServiceProviders;

  $scope.hierarchy_position = localStorageService.get(localStorageKeys.HIERARCHY_POSITION);
  $scope.levelID = localStorageService.get(localStorageKeys.ADMIN_HIERARCHY_LEVEL_ID);

  $scope.title = "SERVICE_PROVIDERS";

  $scope.loadCouncilFacility = function (a, facilityType) {
    FacilityService.facilitiesByTypeCouncilSetup({
      admin_hierarchy_id: a.id,
      facility_type_id: facilityType.id,
      page: 1, perPage: 10
    }, function (data) {
      $scope.facilities = data.facilities;
      ///console.log($scope.facilities)
    }, function (error) {
      console.log(error);
    });

    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.perPage = 10;

    $scope.pageChanged = function () {
      FacilityService.facilitiesByTypeCouncilSetup({
        admin_hierarchy_id: a.id,
        facility_type_id: facilityType.id,
        page: $scope.currentPage, perPage: $scope.perPage
      }, function (data) {
        $scope.facilities = data.facilities;
      }, function (error) {
        console.log(error);
      });
    };
  };

  $scope.search = function (searchText) {
    FacilityService.search({
      searchText: searchText,
      onlyActive: true,
      page: 1, perPage: 10
    }, function (data) {
      $scope.facilities = data.facilities;
    });

    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.perPage = 10;

    $scope.pageChanged = function () {
      FacilityService.search({
        searchText: searchText,
        onlyActive: true,
        page: $scope.currentPage, perPage: $scope.perPage
      }, function (data) {
        $scope.facilities = data.facilities;
      });
    };
  };

  GlobalService.facilityTypes(function (data) {
    $scope.facilityTypes = data.facilityTypes;
  });

  GlobalService.facilityOwnerships(function (data) {
    $scope.facilityOwnerships = data.facilityOwnerships;
  });

  GlobalService.onlyRegions(function (data) {
    $scope.regions = data.regions;
  });

  $scope.loadWards = function (council_id) {
    GlobalService.fetchSpecificCouncils({ council_id: council_id }, function (data) {
      $scope.councils = data.councils;
    });
  };
  $scope.showAlertSuccess = false;
  $scope.showAlertError = false;
  $scope.alertSuccess = function () {
    $scope.showAlertSuccess = true;
    $timeout(function () {
      $scope.showAlertSuccess = false;
    }, 10000);
  };

  $scope.alertError = function () {
    $scope.showAlertError = true;
    $timeout(function () {
      $scope.showAlertError = false;
    }, 10000);
  };

  $scope.currentPage = 1;
  $scope.maxSize = 3;

  $scope.showUploadForm = false;
  $scope.showButtons = true;
  $scope.showList = true;
  $scope.showFilter = true;

  $scope.revealUploadForm = function () {
    $scope.showUploadForm = true;
    $scope.showButtons = false;
    $scope.showList = false;
    $scope.showFilter = false;
  };

  $scope.closeUploadForm = function () {
    $scope.showUploadForm = false;
    $scope.showButtons = true;
    $scope.showList = true;
    $scope.showFilter = true;
  };

  $scope.hideUploadForm = function () {
    $scope.showUploadForm = false;
    $scope.showButtons = true;
    $scope.showList = true;
    $scope.showFilter = true;
  };

  $scope.customDetails = function (facility) {
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/facility/custom-details.html',
      backdrop: false,
      controller: function ($scope, $route, GlobalService, $uibModalInstance, FacilityService) {

        $scope.formDataModel = {};

        $scope.facility = facility;



        $scope.facilityDetailValues = facility.facility_custom_details;
        console.log($scope.facilityDetailValues);

        $scope.getExistingValues = function (id) {
          var x = _.findWhere($scope.facilityDetailValues, {
            facility_custom_detail_id: id
          });
          if (x !== undefined) {
            return x.detail_value;
          } else {
            return undefined;
          }
        };

        GlobalService.facilityTypes(function (data) {
          $scope.facilityTypes = data.facilityTypes;
        });

        GlobalService.facilityOwnerships(function (data) {
          $scope.facilityOwnerships = data.facilityOwnerships;
        });

        GlobalService.myFacilityCustomDetails({
          facility_type_id: facility.facility_type_id,
          facility_ownership_id: facility.facility_ownership_id
        }, function (data) {
          $scope.mappedCustomDetails = data.facilityCustomDetails;
        });


        $scope.addCustomDetails = function () {

          if ($scope.createForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          $scope.customDetailValues = { details: $scope.mappedCustomDetails };

          FacilityService.setCustomDetails({ facility_id: facility.id }, $scope.customDetailValues,
            function (data) {
              $scope.facility_custom_details = data.facility_custom_details;
              $scope.successMessage = data.successMessage;
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        $scope.removeCustomDetail = function (id) {
          FacilityService.deleteCustomDetail({ id: id, facility_id: facility.id },
            function (data) {
              $scope.facility_custom_details = data.facility_custom_details;
              $scope.successMessage = data.successMessage;
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
          reloadChildren(facility.admin_hierarchy, facility.facility_type);
        };
      }
    });
  };

  $scope.bankAccounts = function (facility) {
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/facility/bank-accounts.html',
      backdrop: false,
      controller: function ($scope, $route, $uibModalInstance, FacilityService) {
        $scope.bankAccounts = facility.bank_accounts;

        $scope.facility = facility;

        $scope.formDataModel = {};
        $scope.checkAccountNumberValid = /^[a-zA-Z0-9]{10,15}$/;

        $scope.addBankAccount = function () {
          if ($scope.formDataModel.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          FacilityService.setBankAccount({ facility_id: facility.id }, $scope.formDataModel,
            function (data) {
              $scope.bankAccounts = data.bankAccounts;
              $scope.successMessage = data.successMessage;
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        $scope.removeBankAccount = function (id) {
          FacilityService.deleteBankAccount({ id: id, facility_id: facility.id },
            function (data) {
              $scope.bankAccounts = data.bankAccounts;
              $scope.successMessage = data.successMessage;
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
          reloadChildren(facility.admin_hierarchy, facility.facility_type)
        }
      }
    });
  };

  function reloadChildren(a, facilityType) {
    FacilityService.facilitiesByTypeCouncilSetup({
      admin_hierarchy_id: a.id,
      facility_type_id: facilityType.id
    }, function (data) {
      $scope.facilities = data.facilities;
      $scope.admin = a;
      $scope.facilityType = facilityType;
    }, function (error) {
      console.log(error);
    });
  }

  function reloadChildrenByID(admin_hierarchy_id, facility_type_id) {
    FacilityService.facilitiesByTypeCouncilSetup({
      admin_hierarchy_id: admin_hierarchy_id,
      facility_type_id: facility_type_id
    }, function (data) {
      $scope.facilities = data.facilities;
    });
  }

  $scope.loadChildren = function (a) {
    var level_id = parseInt(a.admin_hierarchy_level_id);
    if (level_id > 0) {
      if (a.children === undefined) {
        a.children = [];
      }
      AdminHierarchiesService.getAdminHierarchyChildren({ adminHierarchyId: a.id }, function (data) {
        a.children = data.adminHierarchies;
      });
    }
  };

  $scope.resetHierarchy = function () {
    $scope.userAdminHierarchy = angular.copy($scope.userAdminHierarchyCopy);
  };

  $scope.uploadFile = function (file) {
    file.upload = Upload.upload({
      url: '/json/facilities/upload',
      method: 'POST',
      data: {
        facility_type_id: $scope.facility_type_id,
        admin_hierarchy_id: $scope.admin_hierarchy_id,
        facility_ownership_id: $scope.facility_ownership_id,
        file: file
      },
    });

    file.upload.then(function (response) {
      $timeout(function () {
        file.result = response.data;
        $scope.facilities = response.data.facilities;
        $scope.hideUploadForm();
        $scope.errorData = response.data.errorData;
        $scope.errorRows = response.data.errorRows;
        $scope.duplicates = response.data.duplicates;
        $scope.accountErrors = response.data.accountErrors;
        $scope.successMessage = response.data.successMessage;
        reloadChildrenByID($scope.admin_hierarchy_id, $scope.facility_type_id);
        $scope.alertSuccess();
      });
    }, function (response) {
      if (response.status > 0) {
        $scope.errorMsg = response.status + ': ' + response.data;
        $scope.errorMessage = response.data.errorMessage;
        reloadChildrenByID($scope.admin_hierarchy_id, $scope.facility_type_id);
        $scope.alertError();
      }
    }, function (evt) {
      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    });
  };

  $scope.create = function () {
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/facility/create.html',
      backdrop: false,
      windowClass: 'center-modal',
      controller: function ($scope, $uibModalInstance, CreateFacilityService, GlobalService) {

        $scope.facilityToCreate = {};

        GlobalService.facilityTypes(function (data) {
          $scope.facilityTypes = data.facilityTypes;
        });

        GlobalService.onlyRegions(function (data) {
          $scope.regions = data.regions;
        });
        // GlobalService.onlyCouncils(function (data) {
        //   $scope.councils = data.councils;
        // });

        GlobalService.loadPiscs(function (data) {
          $scope.piscs = data.piscs;
        });


        $scope.onChangesptype = function (sptype) {

          //get service provider code
          $scope.customStyle = {};
          GlobalService.loadServiceProviderType({ sptype: sptype }, function (data) {
            if (data.type[0].code == "001") {
              $scope.facilityToCreate.facility_code = "00000000"
              $scope.truefalse = true;
              $scope.customStyle.style = { "background-color": "#e0e0eb" };
            } else {
              $scope.facilityToCreate.facility_code = ""
              $scope.truefalse = false;
              $scope.customStyle.style = { "background-color": "white" };
            }

          });
        }

        $scope.checkCode = function (event) {
          if (event.target.value == "00000000") {
            $scope.facilityToCreate.facility_code = ""
            ConfirmDialogService.showConfirmDialog('Please note that', 'Code 00000000 is for HQ Service Provider type').then(function () {
              console.log("")
            }, function () {
              console.log("")
            });

          }
        }

        $scope.loadWards = function (region_id) {
          GlobalService.fetchSpecificCouncils({ region_id: region_id }, function (data) {
            $scope.councils = data.councils;
          });
        };
        // $scope.loadWards = function (council_id) {
        //   GlobalService.councilWards({council_id: council_id}, function (data) {
        //     $scope.wards = data.wards;
        //   });
        // };

        GlobalService.facilityOwnerships(function (data) {
          $scope.facilityOwnerships = data.facilityOwnerships;
        });

        $scope.store = function () {
          ///console.log($scope.facilityToCreate);
          if ($scope.createFacilityForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          CreateFacilityService.store({ perPage: $scope.perPage }, $scope.facilityToCreate,
            function (data) {
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
              $scope.errors = error.data.errors;
            }
          );
        };

        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
      $scope.successMessage = data.successMessage;
      reloadChildren(data.facility.admin_hierarchy, data.facility.facility_type)
    },
      function () {
        //If modal is closed
        console.log('Modal dismissed at: ' + new Date());
      });

  };

  $scope.edit = function (facilityToEdit, admin, facility_type) {
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/facility/edit.html',
      backdrop: false,
      windowClass: 'center-modal',
      controller: function ($scope, $uibModalInstance, UpdateFacilityService, GlobalService) {


        GlobalService.loadPiscs(function (data) {
          $scope.piscs = data.piscs;
        });


        GlobalService.onlyRegions(function (data) {
          $scope.regions = data.regions;
        });

        $scope.loadWards = function (region_id) {
          GlobalService.fetchSpecificCouncils({ region_id: region_id }, function (data) {
            $scope.councils = data.councils;
            // let councilsData = data.councils;
            // councilsData["geo_location_id"] = councilsData["id"];
            // delete councilsData["id"];
            // $scope.councils = councilsData;
          });
        };

        GlobalService.onlyRegions(function (data) {
          $scope.councils = data.councils;
          var serviceProviderData = angular.copy(facilityToEdit);

          $scope.loadWards(serviceProviderData.geolocations.ward_parent.id)
          serviceProviderData.region_id = serviceProviderData.geolocations.ward_parent.id;
          delete serviceProviderData.geolocations.ward_parent.id;
          // $scope.facilityToEdit = data;

          serviceProviderData.geo_location_id = serviceProviderData.geolocations.id;
          $scope.facilityToEdit = serviceProviderData;
          if (serviceProviderData.facility_type.code == "001") {
            $scope.customStyle = {};
            $scope.facilityToEdit.facility_code = "00000000"
            $scope.truefalse = true;
            $scope.customStyle.style = { "background-color": "#c2c2d6b" };
          }

          // $scope.facilityToEdit = angular.copy(facilityToEdit);
          // console.log($scope.facilityToEdit)

        });

        $scope.checkCode = function (event) {
          if (event.target.value == "00000000") {
            $scope.facilityToEdit.facility_code = ""
            ConfirmDialogService.showConfirmDialog('Please note that', 'Code 00000000 is for HQ Service Provider type').then(function () {
              console.log("")
            }, function () {
              console.log("")
            });

          }
        }

        //^(0)?[0-9]{9,13}$
        $scope.mobilePattern = /^[0-9]{10}$/;
        $scope.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;

        $scope.onChangesptype = function (sptype) {
          //get service provider code
          $scope.customStyle = {};
          GlobalService.loadServiceProviderType({ sptype: sptype }, function (data) {
            if (data.type[0].code == "001") {
              $scope.facilityToEdit.facility_code = "00000000"
              $scope.truefalse = true;
              $scope.customStyle.style = { "background-color": "#c2c2d6" };
            } else {
              $scope.facilityToEdit.facility_code = ""
              $scope.truefalse = false;
              $scope.customStyle.style = { "background-color": "white" };
            }

          });
        }

        $scope.fetchSpecificCouncils = function (council_id, facility_type_id) {
          GlobalService.councilWards({ council_id: council_id }, function (data) {
            $scope.wards = data.wards;
          });
          GlobalService.similarFacilities({
            council_id: council_id,
            facility_type_id: facility_type_id
          }, function (data) {
            $scope.similarFacilities = data.similarFacilities;
          });
        };

        $scope.loadVillages = function (ward_id) {
          GlobalService.wardVillages({ ward_id: ward_id }, function (data) {
            $scope.villages = data.villages;
          });
        };

        GlobalService.facilityTypes(function (data) {
          $scope.facilityTypes = data.facilityTypes;
        });

        GlobalService.facilityOwnerships(function (data) {
          $scope.facilityOwnerships = data.facilityOwnerships;
        });

        GlobalService.facilityStarRatings(function (data) {
          $scope.facilityStarRatings = data.facilityStarRatings;
        });

        GlobalService.facilityPhysicalStates(function (data) {
          $scope.facilityPhysicalStates = data.facilityPhysicalStates;
        });

        $scope.update = function () {
          if ($scope.updateFacilityForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          UpdateFacilityService.update({ id: facilityToEdit.id }, $scope.facilityToEdit,
            function (data) {
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
              $scope.errors = error.data.errors;
            }
          );
        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
      //Service to create new financial year
      $scope.successMessage = data.successMessage;
      reloadChildren(data.facility.admin_hierarchy, data.facility.facility_type)
    },
      function () {
        //If modal is closed
        console.log('Modal dismissed at: ' + new Date());
      });
  };

  $scope.delete = function (id, admin, facility_type) {
    ConfirmDialogService.showConfirmDialog('Confirm Delete Facility!', 'Are sure you want to delete this?').then(function () {
      console.log("YES");
      DeleteFacilityService.delete({ facility_id: id },
        function (data) {
          $scope.successMessage = data.successMessage;
          reloadChildren(admin, facility_type)
        }, function (error) {

        }
      );
    },
      function () {
        console.log("NO");
      });
  };

  $scope.toggleFacility = function (facility_id, facility_status, perPage) {
    $scope.facilityToActivate = {};
    $scope.facilityToActivate.id = facility_id;
    $scope.facilityToActivate.is_active = facility_status;
    ToggleFacilityService.toggleFacility({ perPage: perPage }, $scope.facilityToActivate,
      function (data) {
        $scope.action = data.action;
        $scope.errorMessage = data.errorMessage;
        $scope.alertType = data.alertType;
      });
  };
}
FacilityController.resolve = {
  UserAdminHierarchy: function (AdminHierarchiesService, $timeout, $q) {
    var deferred = $q.defer();
    $timeout(function () {
      AdminHierarchiesService.getUserAdminHierarchy({}, function (data) {
        deferred.resolve(data);
      });
    }, 900);
    return deferred.promise;
  },
  loadServiceProviders: function ($q, FacilityService) {
    var deferred = $q.defer();
    var searchText = " ";
    FacilityService.search({
      searchText: searchText,
      onlyActive: true,
      page: 1, perPage: 10
    }, function (data) {

      deferred.resolve(data.facilities);
    });

    return deferred.promise;
  }
};
