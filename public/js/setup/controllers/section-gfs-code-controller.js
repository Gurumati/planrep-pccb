function SectionGfsCodeController($scope, SectionGfsCodeModel, $uibModal,
                                  ConfirmDialogService, DeleteSectionGfsCodeService,
                                  PaginatedSectionGfsCodeService) {

    $scope.sectionGfsCodes = SectionGfsCodeModel;
    $scope.title = "SECTION_GFS_CODES";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedSectionGfsCodeService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.sectionGfsCodes = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/section_gfs_code/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllAdminHierarchyGfsCodeService, CreateSectionGfsCodeService) {
                $scope.sectionGfsCodeToCreate = {};

                AllAdminHierarchyGfsCodeService.query(function (data) {
                    $scope.gfs_codes = data;
                });
                $scope.store = function () {
                    if ($scope.createSectionGfsCodeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateSectionGfsCodeService.store({perPage: $scope.perPage}, $scope.sectionGfsCodeToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.sectionGfsCodes = data.sectionGfsCodes;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('CONFIRM_REMOVE_GFS_CODE', 'CONFIRM_DELETE').then(function () {
                DeleteSectionGfsCodeService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.sectionGfsCodes = data.sectionGfsCodes;
                        $scope.currentPage = $scope.sectionGfsCodes.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    };
}

SectionGfsCodeController.resolve = {
    SectionGfsCodeModel: function (PaginatedSectionGfsCodeService, $q) {
        var deferred = $q.defer();
        PaginatedSectionGfsCodeService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};