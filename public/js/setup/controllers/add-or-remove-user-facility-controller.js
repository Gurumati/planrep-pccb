(function () {
    'use strict';

    angular.module('setup-module').controller('AddOrRemoveUserFacilityController', AddOrRemoveUserFacilityController);
    AddOrRemoveUserFacilityController.$inject = ['$scope', '$uibModalInstance', 'user', 'FacilityService', 'UsersService', '$q'];

    function AddOrRemoveUserFacilityController($scope, $uibModalInstance, user, FacilityService, UsersService, $q) {

        $scope.currentPage = 1;
        $scope.user = angular.copy(user);

        $scope.facilityToAdd = undefined;

        $scope.pageChanged = function () {
            $scope.facilityLoading = true;

            FacilityService.getByUser({
                page: $scope.currentPage,
                userId: user.id
            }, function (data) {
                $scope.userFacilities = data.userFacilities;
                $scope.facilityLoading = false;

            }, function (error) {
                console.log(error);
                $scope.facilityLoading = false;

            });
        };

        $scope.searchFacility = function ($query) {
            var deferred = $q.defer();
            if ($query !== undefined && $query !== '' && $query.length >= 1) {
                FacilityService.searchByAdminAreaAndSection({
                        adminHierarchyId: $scope.user.admin_hierarchy_id,
                        sectionId: $scope.user.section_id,
                        searchQuery: $query,
                        isFacilityUser: 0,
                        noLoader: true
                    },
                    function (data) {
                        $scope.facilityLoading = false;
                        deferred.resolve(data.facilities);
                    });
            } else {
                deferred.resolve([]);
            }
            return deferred.promise;
        };

        $scope.addFacility = function (facilityId) {
            UsersService.addUserFacility({
                userId: user.id,
                facilityId: facilityId
            }, function (data) {
                $scope.facilityToAdd = undefined;
                $scope.pageChanged();
            }, function (error) {
                console.log(error);
                $scope.facilityToAdd = undefined;

            });
        };
        $scope.deleteExistingFacility = function (facilityId) {
            UsersService.removeUserFacility({
                userId: user.id,
                facilityId: facilityId
            }, function (data) {
                $scope.facilityToAdd = undefined;
                $scope.pageChanged();

            }, function (error) {
                console.log(error);
                $scope.facilityToAdd = undefined;

            });
        };

        $scope.pageChanged();
        $scope.close = function () {
            $uibModalInstance.close();
        };
    }
})();