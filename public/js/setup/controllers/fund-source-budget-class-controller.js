function FundSourceBudgetClassController($scope, FinancialYearModel, AllFinancialYearService, $uibModal, ConfirmDialogService, FundSourceBudgetClassService) {

    $scope.financialYears = FinancialYearModel;
    $scope.title = "FUND_SOURCE_BUDGET_CLASSES";

    $scope.loadItems = function (financialYearId) {
        $scope.currentPage = 1;
        $scope.perPage = 10;
        $scope.maxSize = 3;

        FundSourceBudgetClassService.paginated({
            page: $scope.currentPage,
            perPage: $scope.perPage,
            financialYearId: financialYearId
        }, function (data) {
            $scope.items = data;
        });

        $scope.pageChanged = function () {
            FundSourceBudgetClassService.paginated({
                page: $scope.currentPage,
                perPage: $scope.perPage,
                financialYearId: financialYearId
            }, function (data) {
                $scope.items = data;
            });
        };

        $scope.edit = function (updateDataModel, currentPage, perPage) {
            let modalInstance = $uibModal.open({
                templateUrl: '/pages/setup/fund_source/fund-type-bank-account.html',
                backdrop: false,
                controller: function ($scope, $uibModalInstance, AllFundTypeService, BankAccountService) {

                    AllFundTypeService.query(function (data) {
                        $scope.fundTypes = data;
                        $scope.updateDataModel = angular.copy(updateDataModel);
                    }, function (error) {
                        console.log(error);
                    });

                    BankAccountService.fetchAll(function (response) {
                        $scope.bankAccounts = response.data;
                    }, function (error) {
                        console.log(error);
                    });

                    $scope.update = function () {
                        FundSourceBudgetClassService.update({
                                page: currentPage,
                                perPage: perPage
                            }, $scope.updateDataModel,
                            function (data) {
                                $uibModalInstance.close(data);
                            },
                            function (error) {
                                $scope.errorMessage = error.data.errorMessage;
                            }
                        );
                    };
                    //Function to close modal when cancel button clicked
                    $scope.close = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });
            //Called when modal is close by cancel or to store data
            modalInstance.result.then(function (data) {
                    //Service to create new financial year
                    $scope.successMessage = data.successMessage;
                    $scope.items = data.items;
                    $scope.currentPage = $scope.items.current_page;
                },
                function () {
                    //If modal is closed
                    console.log('Modal dismissed at: ' + new Date());
                });
        };
    };
}

FundSourceBudgetClassController.resolve = {
    FinancialYearModel: function (AllFinancialYearService, $q) {
        let deferred = $q.defer();
        AllFinancialYearService.query(function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};