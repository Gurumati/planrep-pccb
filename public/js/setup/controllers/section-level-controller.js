function SectionLevelController($scope, AllSectionLevelModel, $uibModal,
                                 ConfirmDialogService,ToggleSectionLevelService,
                                 DeleteSectionLevelService,GetAllSectionLevelService) {

    $scope.sectionLevels = AllSectionLevelModel;
    $scope.title = "TITLE_SECTION_LEVELS";

    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        GetAllSectionLevelService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
            $scope.sectionLevels = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/section_level/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,CreateSectionLevelService,LoadParentLevelService) {
                $scope.sectionLevelToCreate = {};
                //call parent levels
                LoadParentLevelService.query({sectionLevelId: 0}, function (data) {
                    $scope.parentSectionLevels = data;
                });
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createSectionLevelForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    console.log($scope.sectionLevelToCreate);
                    CreateSectionLevelService.store({perPage:$scope.perPage},$scope.sectionLevelToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                           $scope.errorMessage=error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage=data.successMessage;
                $scope.sectionLevels=data.sectionLevels;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (sectionLevelToEdit,currentPage,perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/section_level/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,UpdateSectionLevelService,LoadParentLevelService) {
                $scope.sectionLevelToEdit = angular.copy(sectionLevelToEdit);
                //call parent levels
                console.log($scope.sectionLevelToEdit.id);
                LoadParentLevelService.query({sectionLevelId: $scope.sectionLevelToEdit.id}, function (data) {
                    $scope.parentSectionLevels = data;
                    console.log(data);
                });
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updateSectionLevelForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateSectionLevelService.update({page: currentPage,perPage:perPage},$scope.sectionLevelToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage=error.data.errorMessage;
                        }
                    );


                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage=data.successMessage;
                $scope.sectionLevels=data.sectionLevels;
                $scope.currentPage = $scope.sectionLevels.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog(
            'TITLE_CONFIRM_DELETE_SECTION_LEVEL',
            'CONFIRM_DELETE')
            .then(function () {
                    DeleteSectionLevelService.delete({sectionLevelId: id,page:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.sectionLevels = data.sectionLevels;
                            $scope.currentPage = $scope.sectionLevels.current_page;
                        }, function (error) {
                            $scope.errorMessage=error.data.errorMessage;
                        }
                    );
                },
                function () {
                    
                });
    }
    $scope.toggleSectionLevel = function (id, is_active,perPage) {
        $scope.sectionLevelToActivate = {};
        $scope.sectionLevelToActivate.id = id;
        $scope.sectionLevelToActivate.is_active = is_active;
        ToggleSectionLevelService.toggleSectionLevel({perPage:perPage},$scope.sectionLevelToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    }
}

SectionLevelController.resolve = {
    AllSectionLevelModel: function (GetAllSectionLevelService, $q) {
        var deferred = $q.defer();
        GetAllSectionLevelService.get({page: 1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};