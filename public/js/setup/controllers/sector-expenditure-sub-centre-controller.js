function SectorExpenditureSubCentreController($scope, SectorExpenditureSubCentreModel, $uibModal,
                                              ConfirmDialogService, DeleteSectorExpenditureSubCentreService,
                                              PaginatedSectorExpenditureSubCentreService) {

    $scope.sectorExpenditureSubCentres = SectorExpenditureSubCentreModel;
    $scope.title = "SECTOR_EXPENDITURE_SUB_CENTRES";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedSectorExpenditureSubCentreService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.sectorExpenditureSubCentres = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/sector_expenditure_sub_centre/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllSectorService, AllExpenditureCentreService,
                                  AllBdcGroupService, CreateSectorExpenditureSubCentreService,AllGfsCodeService) {
                $scope.sectorExpenditureSubCentreToCreate = {};

                AllExpenditureCentreService.query(function (data) {
                    $scope.expenditureCentres = data;
                });

                AllBdcGroupService.query(function (data) {
                    $scope.bdcGroups = data;
                });

                AllSectorService.query(function (data) {
                    $scope.sectors = data;
                });

                AllGfsCodeService.query(function (data) {
                    $scope.gfs_codes = data;
                });

                $scope.store = function () {
                    if ($scope.createSectorExpenditureSubCentreForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateSectorExpenditureSubCentreService.store({perPage: $scope.perPage}, $scope.sectorExpenditureSubCentreToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.sectorExpenditureSubCentres = data.sectorExpenditureSubCentres;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (sectorExpenditureSubCentreToEdit, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/sector_expenditure_sub_centre/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllSectorService,AllGfsCodeService, AllExpenditureCentreService, AllBdcGroupService, UpdateSectorExpenditureSubCentreService) {

                AllExpenditureCentreService.query(function (data) {
                    $scope.expenditureCentres = data;
                    $scope.sectorExpenditureSubCentreToEdit = angular.copy(sectorExpenditureSubCentreToEdit);
                });

                AllBdcGroupService.query(function (data) {
                    $scope.bdcGroups = data;
                });

                AllSectorService.query(function (data) {
                    $scope.sectors = data;
                });

                AllGfsCodeService.query(function (data) {
                    $scope.gfs_codes = data;
                });

                $scope.update = function () {
                    if ($scope.updateSectorExpenditureSubCentreForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateSectorExpenditureSubCentreService.update({page: currentPage, perPage: perPage}, $scope.sectorExpenditureSubCentreToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.sectorExpenditureSubCentres = data.sectorExpenditureSubCentres;
                $scope.currentPage = $scope.sectorExpenditureSubCentres.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('CONFIRM_DELETE', 'Are you sure you want to trash this record?').then(function () {
                DeleteSectorExpenditureSubCentreService.delete({sectorExpenditureSubCentre_id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.sectorExpenditureSubCentres = data.sectorExpenditureSubCentres;
                        $scope.currentPage = $scope.sectorExpenditureSubCentres.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/sector_expenditure_sub_centre/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance, TrashedSectorExpenditureSubCentreService,
                                  RestoreSectorExpenditureSubCentreService, EmptySectorExpenditureSubCentreTrashService, PermanentDeleteSectorExpenditureSubCentreService) {
                TrashedSectorExpenditureSubCentreService.query(function (data) {
                    $scope.trashedSectorExpenditureSubCentres = data;
                });
                $scope.restoreSectorExpenditureSubCentre = function (id) {
                    RestoreSectorExpenditureSubCentreService.restore({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedSectorExpenditureSubCentres = data.trashedSectorExpenditureSubCentres;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteSectorExpenditureSubCentreService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedSectorExpenditureSubCentres = data.trashedSectorExpenditureSubCentres;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptySectorExpenditureSubCentreTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedSectorExpenditureSubCentres = data.trashedSectorExpenditureSubCentres;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.sectorExpenditureSubCentres = data.sectorExpenditureSubCentres;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.gfs_codes = function (sectorExpenditureSubCentre) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/sector_expenditure_sub_centre/gfs_codes.html',
            backdrop: false,
            controller: function ($scope, $q, $uibModalInstance, ExpenditureGfsCodeService, AddExpenditureSubCentreGfsCodeService, DeleteExpenditureSubCentreGfsCodeService, ExpenditureSubCentreGfsCodeService) {

                $scope.gfsCodeToCreate = {};
                $scope.bdc_sector_expenditure_sub_centre_id = sectorExpenditureSubCentre.id;
                $scope.number = sectorExpenditureSubCentre.number;
                $scope.name = sectorExpenditureSubCentre.name;

                ExpenditureGfsCodeService.query(function (data) {
                    $scope.expenditureGfsCodes = data;
                });


                ExpenditureSubCentreGfsCodeService.get({id: sectorExpenditureSubCentre.id}, function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.expenditureSubCentreGfsCodes = data.expenditureSubCentreGfsCodes;
                    }, function (error) {
                        console.log(error)
                    }
                );

                $scope.add_gfs_code = function () {
                    var gfsCodeToSave = {
                        "gfs_code_id": $scope.gfsCodeToCreate.gfs_code_id,
                        "bdc_sector_expenditure_sub_centre_id": $scope.bdc_sector_expenditure_sub_centre_id,
                    };
                    AddExpenditureSubCentreGfsCodeService.add_gfs_code(gfsCodeToSave, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.expenditureSubCentreGfsCodes = data.expenditureSubCentreGfsCodes;
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.expenditureSubCentreGfsCodes = data.expenditureSubCentreGfsCodes;
                        }
                    );
                };

                $scope.deleteGfsCode = function (id, parent_id) {
                    DeleteExpenditureSubCentreGfsCodeService.get({
                        id: id,
                        parent_id: parent_id
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.expenditureSubCentreGfsCodes = data.expenditureSubCentreGfsCodes;
                        }, function (error) {

                        }
                    );
                };

                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
}

SectorExpenditureSubCentreController.resolve = {
    SectorExpenditureSubCentreModel: function (PaginatedSectorExpenditureSubCentreService, $q) {
        var deferred = $q.defer();
        PaginatedSectorExpenditureSubCentreService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};