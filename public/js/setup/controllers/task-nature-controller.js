function TaskNatureController($scope, TaskNatureModel, $uibModal,
                               ConfirmDialogService, DeleteTaskNatureService, PaginatedTaskNatureService) {

    $scope.taskNatures = TaskNatureModel;
    $scope.title = "TASK_NATURES";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedTaskNatureService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
            $scope.taskNatures = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/task_nature/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateTaskNatureService) {
                $scope.taskNatureToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createTaskNatureForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateTaskNatureService.store({perPage:$scope.perPage},$scope.taskNatureToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.taskNatures = data.taskNatures;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (taskNatureToEdit, currentPage,perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/task_nature/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateTaskNatureService) {
                $scope.taskNatureToEdit = angular.copy(taskNatureToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updateTaskNatureForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateTaskNatureService.update({page: currentPage,perPage:perPage}, $scope.taskNatureToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.taskNatures = data.taskNatures;
                $scope.currentPage = $scope.taskNatures.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog('CONFIRM_DELETE', 'Are you sure you want to trash this record?').then(function () {
                    DeleteTaskNatureService.delete({task_nature_id: id,page:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.taskNatures = data.taskNatures;
                            $scope.currentPage = $scope.taskNatures.current_page;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {

                });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/task_nature/trash.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, TrashedTaskNatureService,
                                  RestoreTaskNatureService, EmptyTaskNatureTrashService, PermanentDeleteTaskNatureService) {
                TrashedTaskNatureService.query(function (data) {
                    $scope.trashedTaskNatures = data;
                });
                $scope.restoreTaskNature = function (id) {
                    RestoreTaskNatureService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedTaskNatures = data.trashedTaskNatures;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteTaskNatureService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedTaskNatures = data.trashedTaskNatures;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyTaskNatureTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedTaskNatures = data.trashedTaskNatures;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.taskNatures = data.taskNatures;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

TaskNatureController.resolve = {
    TaskNatureModel: function (PaginatedTaskNatureService, $q) {
        var deferred = $q.defer();
        PaginatedTaskNatureService.get({page: 1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};