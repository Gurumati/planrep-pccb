function VersionController($scope, DataModel, $uibModal, ConfirmDialogService, VersionService) {

    $scope.items = DataModel;
    $scope.title = "VERSIONS";
    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.perPage = 10;
    $scope.dateFormat = 'yyyy-MM-dd';

    $scope.pageChanged = function () {
        VersionService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data.data;
        });
    };

    $scope.create = function () {
        let modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/version/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, VersionTypeService) {
                $scope.createDataModel = {};

                VersionTypeService.fetchAll(function (response) {
                        $scope.types = response.data;
                    },
                    function (error) {
                        console.log(error);
                    }
                );

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    VersionService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.message;
                $scope.items = data.data;
                $scope.currentPage = data.data.current_page;
            },
            function () {

            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        let modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/version/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, VersionTypeService) {
                VersionTypeService.fetchAll(function (response) {
                        $scope.types = response.data;
                        $scope.updateDataModel = angular.copy(updateDataModel);
                        $scope.updateDataModel.date = new Date($scope.updateDataModel.date);
                    },
                    function (error) {
                        console.log(error);
                    }
                );

                $scope.update = function () {
                    VersionService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };

                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });

        modalInstance.result.then(function (data) {
                $scope.successMessage = data.message;
                $scope.items = data.data;
                $scope.currentPage = $scope.items.data.current_page;
            },
            function () {

            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                VersionService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.message;
                        $scope.items = data.data;
                        $scope.currentPage = $scope.items.data.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {
                console.log("NO");
            });
    };

    $scope.toggleCurrent = function (version, isCurrent, currentPage, perPage) {
        VersionService.toggleCurrent({
            id: version.id,
            page: currentPage,
            is_current: isCurrent,
            perPage: perPage,
            versionTypeId: version.type.id
        }, function (data) {
            $scope.successMessage = data.message;
            $scope.items = data.data;
            $scope.currentPage = $scope.items.current_page;
        });
    };
}

VersionController.resolve = {
    DataModel: function (VersionService, $q) {
        var deferred = $q.defer();
        VersionService.paginated({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data.data);
        });
        return deferred.promise;
    }
};