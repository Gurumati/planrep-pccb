function DemoSectionInformationController($scope, DemoSectionInfoService,SaveForm,GetForm) {

    DemoSectionInfoService.query({},function (data) {
        $scope.listData=data;
    });

    $scope.addLineItem=function (lineItem) {
        $scope.myForm.form_line_items.push(lineItem);
    };

    // $scope.myForm={name:"Priority Area", type:"TABLE_FORM",
    //               form_fields:[{name:"CBI",form_fields:[{name:"sub one"},{name:"sub two"},{name:"sub three"}]},
    //                            {name:"No. fo Corp available",form_fields:[]}],
    //               form_line_items:[{name:"Health Promotion"},{name:"Malaria Rate"}]
    // };

    GetForm.get({name:'Priority Area'},function (data) {
        $scope.myForm=data.form[0];
        console.log($scope.myForm);
    });

    $scope.saveForm=function () {
        SaveForm.save($scope.myForm,function (data) {
            $scope.myForm=data.form[0];
        })
    };

   // $scope.fieldValues=prepareForm($scope.myForm);

    $scope.prepareForm=function () {
        $scope.fieldValue=angular.copy($scope.myForm);
        if($scope.fieldValue.form_line_items.length > 0){
            angular.forEach($scope.fieldValue.form_line_items, function(item) {

                item.form_fields=angular.copy($scope.fieldValue.form_fields);

            })
        }
     };
    $scope.saveField=function (field) {
        $scope.parent.form_fields.push(field);
        $scope.showAdd=false;
    };

    $scope.addChild=function (parent) {
        $scope.fieldToAdd={form_fields:[]};
        $scope.showAdd=true;
        $scope.parent=parent;
    };

   // prepareForm($scope.myForm);

  //  $scope.myForm.header2=[];

    $scope.fieldValues=[];

}

DemoSectionInformationController.resolve = {


};