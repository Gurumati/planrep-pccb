function InterventionCategoryController($scope, InterventionCategoryModel, $uibModal,
                                        ConfirmDialogService,DeleteInterventionCategoryService) {

    $scope.interventionCategories = InterventionCategoryModel;
    $scope.interventionCategoryCopy = angular.copy(InterventionCategoryModel);
    if ($scope.interventionCategories.length > 0) {
        $scope.interventionCategories[0].expanded = true;
    }
    $scope.title = "INTERVENTION_CATEGORIES";

    $scope.create = function () {
        //Modal Form for creating financial year

        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/intervention_category/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateInterventionCategoryService, AllInterventionCategoryService) {

                AllInterventionCategoryService.query({}, function (data) {
                    $scope.allInterventionCategories = data;
                });

                $scope.interventionCategoryToCreate = {};

                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createInterventionCategoryForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateInterventionCategoryService.store({}, $scope.interventionCategoryToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            console.log(error);
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.interventionCategories = data.interventionCategories;
                if ($scope.interventionCategories.length > 0) {
                    $scope.interventionCategories[0].expanded = true;
                }
                $scope.interventionCategoryCopy = angular.copy(data.interventionCategories);
                clearTree();
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
                clearTree();
            });

    };

    $scope.edit = function (interventionCategoryToEdit) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/intervention_category/edit.html',
            backdrop: false,
            controller: function ($scope, $q, $uibModalInstance, InterventionCategoryTreeService, UpdateInterventionCategoryService) {
                InterventionCategoryTreeService.query({id: interventionCategoryToEdit.id}, function (data) {
                    $scope.allInterventionCategories = data;
                    $scope.interventionCategoryToEdit = angular.copy(interventionCategoryToEdit);
                });

                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updateInterventionCategoryForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateInterventionCategoryService.update($scope.interventionCategoryToEdit,
                        function (data) {
                            //Successful function when
                            $scope.interventionCategoryToEdit = undefined;
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.interventionCategories = data.interventionCategories;
                if ($scope.interventionCategories.length > 0) {
                    $scope.interventionCategories[0].expanded = true;
                }
                $scope.interventionCategoryCopy = angular.copy(data.interventionCategories);
                clearTree();
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
                clearTree();
            });
    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog('CONFIRM_DELETE_INTERVENTION_CATEGORY', 'CONFIRM_DELETE').then(function () {
                    DeleteInterventionCategoryService.delete({intervention_category_id: id},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.interventionCategories = data.interventionCategories;
                            if ($scope.interventionCategories.length > 0){
                                $scope.interventionCategories[0].expanded = true;
                            }
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {
                    clearTree();
                });
    };

    var clearTree = function () {
        $scope.sectionNodeSearchQuery = undefined;
        $scope.interventionCategoryNode.currentNode = undefined;
        $scope.interventionCategories = angular.copy($scope.interventionCategoryCopy);
        if ($scope.interventionCategories.length > 0) {
            $scope.interventionCategories[0].expanded = true;
        }
    };
}

InterventionCategoryController.resolve = {
    InterventionCategoryModel: function (AllInterventionCategoryService, $q) {
        var deferred = $q.defer();
        AllInterventionCategoryService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};