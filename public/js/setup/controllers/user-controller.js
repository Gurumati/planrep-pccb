function UserController($scope, $uibModal, UsersService, ConfirmDialogService) {
  $scope.currentPage = 1;
  $scope.maxSize = 3;
  $scope.perPage = 10;
  $scope.searchQuery = undefined;
  $scope.title = "TITLE_USERS";
  $scope.dateFormat = "yyyy-MM-dd";
  $scope.userLoaded = false;

  $scope.permissions = function (user) {
    if (user.permissions !== null) {
      var array = $.map(user.permissions, function (value, index) {
        return [
          {
            name: index,
          },
        ];
      });
      user.permissions = array;
    } else {
      user.permissions = [];
    }
  };
  $scope.loadUsers = function (noLoader) {
    UsersService.get(
      {
        userNameQuery: $scope.userNameQuery,
        firstNameQuery: $scope.firstNameQuery,
        lastNameQuery: $scope.lastNameQuery,
        adminQuery: $scope.adminQuery,
        sectionQuery: $scope.sectionQuery,
        page: $scope.currentPage,
        perPage: $scope.perPage,
        noLoader: noLoader,
      },
      function (UsersModel) {
        $scope.usersModel = UsersModel;
        $scope.users = UsersModel.data;
        $scope.userLoaded = true;
      }
    );
  };
  $scope.pageChanged = function () {
    $scope.loadUsers(false);
  };
  $scope.enterToSearch = function (keyEvent) {
    if (keyEvent.charCode === 13) {
      $scope.searchUser();
    }
  };
  $scope.searchUser = function () {
    $scope.loadUsers(false);
  };
  $scope.loadUsers(true);

  $scope.createOrEdit = function (user, currentPage, perPage) {
    $scope.checkNumber = false;
    console.log("user", user);
    var modalInstance = $uibModal.open({
      templateUrl: "/pages/setup/user/create-or-edit.html?t=1467948997000",
      backdrop: false,
      controller: function (
        $scope,
        $q,
        $uibModalInstance,
        CreateUserService,
        AccessRightsService,
        DecisionLevelsByAdminHierarchyLevelService,
        FacilityService,
        RolesService,
        AdminHierarchyLevelsByUserService,
        AdminHierarchiesByLevel,
        SectionLevelsByUserService,
        SectionsByLevel,
        SectionsByLevelFilter
      ) {
        $scope.adminLevelsLoaded = false;
        $scope.sectionLevelsLoaded = false;
        $scope.rolesLoaded = false;
        $scope.namePattern =  /^[a-z]+\.[a-z]+$/;
        $scope.nameUserPattern = /^[a-zA-Z'\- ._]*$/;
        $scope.titlePattern = /^[a-zA-Z ]*$/;

        AdminHierarchyLevelsByUserService.query(
          {
            noLoader: true,
          },
          function (data) {
            $scope.levels = data;
            $scope.adminLevelsLoaded = true;
          }
        );

        SectionLevelsByUserService.query(
          {
            noLoader: true,
          },
          function (data) {
            $scope.sectionLevels = data;
            $scope.sectionLevelsLoaded = true;
          }
        );

        // $scope.checkNumberValid = /^[0-9]{5,12}$/;
        //$scope.checkNumberValid = /^[_A-z0-9]*((-|\s)*[_A-z0-9])*$/;

        $scope.validateChequeNumber = function (chequesNumber) {

          if (/^[a-zA-Z0-9- ]*$/.test(chequesNumber) == false && chequesNumber && chequesNumber != null) {
            $scope.checkAndPFNumber = true
            return;
          }
          $scope.checkAndPFNumber = false

          $scope.chequeNumberExist = false;
          if (chequesNumber !== undefined && chequesNumber.length > 3) {
            $scope.validatingChequeNumber = true;
            CreateUserService.validateCheckNumber(
              {
                chequesNumber: chequesNumber,
                userId: user.id !== undefined ? user.id : 0,
                noLoader: true,
              },
              function (data) {
                $scope.validatingChequeNumber = false;
                if (data.exist) {
                  $scope.chequeNumberExist = true;
                } else {
                  $scope.chequeNumberExist = false;
                }
              },
              function () {
                $scope.validatingChequeNumber = false;
              }
            );
          }
        };
        $scope.emailPattern = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
        $scope.validateEmail = function (email) {
          $scope.emailExist = false;
          if (email !== undefined && email.length > 6) {
            $scope.validatingEmail = true;
            CreateUserService.validateEmail(
              {
                email: email,
                userId: user.id !== undefined ? user.id : 0,
                noLoader: true,
              },
              function (data) {
                $scope.validatingEmail = false;
                if (data.exist) {
                  $scope.emailExist = true;
                } else {
                  $scope.emailExist = false;
                }
              },
              function () {
                $scope.validatingEmail = false;
              }
            );
          }
        };

        $scope.searchFacility = function ($query) {
          var deferred = $q.defer();
          if ($query !== undefined && $query !== "" && $query.length >= 1) {
            $scope.facilityLoading = true;

            FacilityService.searchByAdminAreaAndSection(
              {
                adminHierarchyId: $scope.userToCreate.admin_hierarchy_id,
                sectionId: $scope.userToCreate.section_id,
                isFacilityUser: 1,
                searchQuery: $query,
                noLoader: true,
              },
              function (data) {
                $scope.facilityLoading = false;
                deferred.resolve(data.facilities);
              }
            );
          } else {
            deferred.resolve([]);
          }
          return deferred.promise;
        };
        $scope.validateUserName = function (userName) {
          $scope.userNameExist = false;
          if (userName !== undefined && userName.length > 3) {
            $scope.validatingUserName = true;
            CreateUserService.validateUserName(
              {
                userName: userName,
                userId: user.id !== undefined ? user.id : 0,
                noLoader: true,
              },
              function (data) {
                $scope.validatingUserName = false;
                if (data.exist) {
                  $scope.userNameExist = true;
                } else {
                  $scope.userNameExist = false;
                }
              },
              function () {
                $scope.validatingUserName = false;
              }
            );
          }
        };

        if (user.id !== undefined) {
          if (user.admin_hierarchy === undefined) {
            user.admin_hierarchy = {
              admin_hierarchy_level_id: 0,
            };
          }
          if (user.section === undefined) {
            user.section = {
              section_level_id: 0,
            };
          }
          AdminHierarchiesByLevel.query(
            {
              levelId: user.admin_hierarchy.admin_hierarchy_level_id,
              noLoader: true,
            },
            function (data) {
              $scope.adminHierarchies = data;
              DecisionLevelsByAdminHierarchyLevelService.query(
                {
                  adminHierarchyLevelId:
                    user.admin_hierarchy.admin_hierarchy_level_id,
                  noLoader: true,
                },
                function (data) {
                  $scope.decisionLevels = data;
                  SectionsByLevelFilter.query(
                    {
                      levelId: user.section.section_level_id,
                      noLoader: true,
                    },
                    function (data) {
                      $scope.sections = data;
                      AccessRightsService.query(
                        {
                          noLoader: true,
                        },
                        function (rights) {
                          RolesService.getByUser(
                            {
                              noLoader: true,
                            },
                            function (data) {
                              $scope.rights = rights;
                              $scope.roles = data;
                              $scope.userToCreate = user;
                              $scope.rolesLoaded = true;
                            }
                          );
                        }
                      );
                    }
                  );
                }
              );
            }
          );
        } else {
          AccessRightsService.query({}, function (data) {
            $scope.rights = data;
          });
          RolesService.getByUser({}, function (data) {
            $scope.roles = data;
            $scope.rolesLoaded = true;
          });
          $scope.userToCreate = user;
          $scope.userToCreate.roles = [];
        }

        $scope.preLoading = false;
        $scope.loadAdminDataByLevel = function (levelId) {
          $scope.preLoading = true;
          $scope.loadAdminHierarchiesByLevel(levelId);
          $scope.loadDecisionLevels(levelId);
        };
        $scope.loadingHierarchies = false;
        $scope.loadAdminHierarchiesByLevel = function (levelId) {
          $scope.loadingHierarchies = true;
          AdminHierarchiesByLevel.query(
            {
              levelId: levelId,
              noLoader: true,
            },
            function (data) {
              $scope.adminHierarchies = data;
              $scope.loadingHierarchies = false;
            }
          );
        };

        $scope.loadSectionsByLevel = function (levelId) {
          $scope.loadingSections = true;
          $scope.adminId = null;
          if (user.admin_hierarchy["id"]) {
            $scope.adminId = user.admin_hierarchy.id;
          } else {
            $scope.adminId = $scope.userToCreate.admin_hierarchy_id;
          }
          SectionsByLevelFilter.query(
            {
              levelId: levelId,
              noLoader: true,
            },
            function (data) {
              $scope.sections = data;
              $scope.preLoading = false;
              $scope.loadingSections = false;
            },
            function (error) {
              $scope.loadingSections = false;
            }
          );
        };
        $scope.loadingDecionLevels = false;
        $scope.loadDecisionLevels = function (levelId) {
          $scope.loadingDecionLevels = true;
          DecisionLevelsByAdminHierarchyLevelService.query(
            {
              adminHierarchyLevelId: levelId,
              noLoader: true,
            },
            function (data) {
              $scope.decisionLevels = data;
              $scope.loadingDecionLevels = false;
            }
          );
        };
        $scope.inRoles = function (userRoles, roleId) {
          var existing = _.findWhere(userRoles, {
            id: roleId,
          });
          if (existing) {
            return true;
          } else {
            return false;
          }
        };
        $scope.toggleRoleSelection = function (userRoles, role) {
          if (userRoles === undefined) {
            userRoles = [];
          }
          var idx = -1;
          for (var i = 0, len = userRoles.length; i < len; i++) {
            if (userRoles[i].id === role.id) {
              idx = i;
              break;
            }
          }
          // Is currently selected
          if (idx > -1) {
            userRoles.splice(idx, 1);
          }
          // Is newly selected
          else {
            userRoles.push(role);
          }
        };
        $scope.store = function () {

          if ($scope.createUserForm.$invalid || $scope.checkNumber.$invalid) {
            if ($scope.checkNumber.$invalid) {
              $scope.formChecknoHasErrors = true;
            }
            $scope.formHasErrors = true;
            return;
          }

          if (/^[a-zA-Z0-9- ]*$/.test($scope.userToCreate.cheque_number) == false) {
            $scope.errorMessage = "Invalid PF or Check number format";
            return;
          }

          CreateUserService.store(
            {
              page: currentPage,
              perPage: perPage,
            },
            $scope.userToCreate,
            function (data) {
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss("cancel");
        };
      },
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(
      function (UsersModel) {
        //Service to create new financial year
        $scope.successMessage = UsersModel.successMessage;
        $scope.userModel = UsersModel.users;
        $scope.users = UsersModel.users.data;
        $scope.searchQuery = undefined;
        // if (UsersModel.user != null && !UsersModel.user.is_facility_user) {
        //   $scope.addOrRemoveActivityFacility(UsersModel.user);
        // }
      },
      function () {
        //If modal is closed
        console.log("Modal dismissed at: " + new Date());
      }
    );
  };

  $scope.addOrRemoveActivityFacility = function (user) {
    var modalInstance = $uibModal.open({
      templateUrl: "/pages/setup/user/add-or-remove-user-facility.html",
      backdrop: false,
      resolve: {
        user: user,
      },
      controller: "AddOrRemoveUserFacilityController",
    });
    modalInstance.result.then(
      function (u) { },
      function () {
        $scope.pageChanged();
        console.log("Modal dismissed at: " + new Date());
      }
    );
  };

  $scope.resetPassword = function (userId) {
    var modalInstance = $uibModal.open({
      templateUrl: "/pages/setup/user/admin-reset-password.html",
      backdrop: false,
      controller: function ($scope, $uibModalInstance, UpdatePasswordService) {
        $scope.user = {
          id: userId,
        };

        //$scope.regex = "^(?=.*\\d)(?=.*[a-zA-Z]).{6,10}$";
        $scope.regex = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-/_]).{8,}$";
        $scope.reset = function () {
          if ($scope.resetPasswordForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          UpdatePasswordService.update(
            $scope.user,
            function (data) {
              //Successful function when
              $uibModalInstance.close(data);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss("cancel");
        };
      },
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(
      function (data) {
        $scope.successMessage = data.successMessage;
      },
      function () {
        //If modal is closed
        console.log("Modal dismissed at: " + new Date());
      }
    );
  };

  $scope.getUserEvents = function (user) {
    var modalInstance = $uibModal.open({
      templateUrl: "/pages/setup/user/user-events.html",
      backdrop: false,
      controller: function ($scope, $uibModalInstance, UsersService) {
        $scope.perPage = 10;
        $scope.currentPage = 1;
        $scope.user = user;
        $scope.pageChanged = function () {
          UsersService.getUserEvents(
            {
              userId: user.id,
              perPage: $scope.perPage,
              page: $scope.currentPage,
              actionQuery: $scope.actionQuery,
              dateQuery: $scope.dateQuery,
            },
            function (data) {
              $scope.events = data;
            }
          );
        };
        $scope.pageChanged();
        $scope.close = function () {
          $uibModalInstance.dismiss("cancel");
        };
      },
    });
    modalInstance.result.then(
      function (data) {
        $scope.successMessage = data.successMessage;
      },
      function () {
        //If modal is closed
        console.log("Modal dismissed at: " + new Date());
      }
    );
  };

  $scope.activateUser = function (userId) {
    ConfirmDialogService.showConfirmDialog(
      "Confirm Activate User!",
      "Are sure you want to activate user"
    ).then(
      function () {
        UsersService.activateUser(
          {
            userId: userId,
          },
          function (data) {
            $scope.successMessage = data.successMessage;
            $scope.loadUsers(false);
          },
          function (error) {
            $scope.errorMessage = error.data.errorMessage;
          }
        );
      },
      function () { }
    );
  };
  $scope.deActivateUser = function (userId) {
    ConfirmDialogService.showConfirmDialog(
      "Confirm De-activate User!",
      "Are sure you want to De-activate user"
    ).then(
      function () {
        UsersService.deActivateUser(
          {
            userId: userId,
          },
          function (data) {
            $scope.successMessage = data.successMessage;
            $scope.loadUsers(false);
          },
          function (error) {
            $scope.errorMessage = error.data.errorMessage;
          }
        );
      },
      function () { }
    );
  };

  $scope.delete = function (id) { };

  $("a.sidebar-main-toggle").trigger("click");
}

UserController.resolve = {};
