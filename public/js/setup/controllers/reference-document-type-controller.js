function ReferenceDocumentTypesController (
                          $scope,
                          ReferenceDocumentTypesModel,
                          $uibModal,
                          PaginateReferenceDocumentTypesService,
                          ConfirmDialogService,
                          DeleteReferenceDocumentTypesService) {

    $scope.referenceDocumentTypes = ReferenceDocumentTypesModel;
    $scope.title = "REFERENCE_DOCUMENT_TYPES";

    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginateReferenceDocumentTypesService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
            $scope.referenceDocumentTypes = data;
        },function (error) {
            $scope.errorMessage = error.data.errorMessage;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/reference_document_type/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateReferenceDocumentTypesService) {
                $scope.referenceDocumentTypeToCreate = {};

                $scope.store = function () {
                    if ($scope.createReferenceDocumentTypeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateReferenceDocumentTypesService.store({perPage:$scope.perPage},$scope.referenceDocumentTypeToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.referenceDocumentTypes = data.referenceDocumentTypes;

            },
            function () {
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (referenceDocumentTypeToEdit,currentPage,perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/reference_document_type/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateReferenceDocumentTypesService) {
                $scope.referenceDocumentTypeToEdit = angular.copy(referenceDocumentTypeToEdit);
                $scope.update = function () {
                    if ($scope.updateReferenceDocumentTypeForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateReferenceDocumentTypesService.update({page: currentPage,perPage:perPage},$scope.referenceDocumentTypeToEdit,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                };
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.referenceDocumentTypes = data.referenceDocumentTypes;
                $scope.currentPage = $scope.referenceDocumentTypes.current_page;
            },
            function () {
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id,currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Delete Facility!',
            'Are sure you want to delete this?')
            .then(function () {
                    DeleteReferenceDocumentTypesService.delete({id: id,page:currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.referenceDocumentTypes = data.referenceDocumentTypes;
                            $scope.currentPage = $scope.referenceDocumentTypes.current_page;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }

}

ReferenceDocumentTypesController.resolve = {
    ReferenceDocumentTypesModel: function (PaginateReferenceDocumentTypesService, $q) {
        var deferred = $q.defer();
        PaginateReferenceDocumentTypesService.get({page: 1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};