function MetadataItemController($scope, DataModel, $uibModal, ConfirmDialogService, MetadataItemService) {

  $scope.dataSources = DataModel;
  $scope.title = "METADATA_ITEMS";
  $scope.dataSets = {};
  $scope.items = {};
  $scope.syncData = function (dataSourceId) {
    MetadataItemService.syncData({dataSourceId: dataSourceId},function (response) {
      $scope.dataSources = response.data;
      $scope.dataSets = {};
      $scope.items = {};
      $scope.successMessage = response.message;
    }, function (error) {
      console.log(error);
    });
  };

  $scope.loadDataSets = function (sourceId) {
    $scope.dataSourceId = sourceId;
    MetadataItemService.dataSourceDataSets({dataSourceId:sourceId},function (response) {
      $scope.dataSets = response.data;
      $scope.loadDataElements = function (dataSetUID) {
        $scope.currentPage = 1;
        $scope.perPage = 10;
        $scope.maxSize = 3;
        $scope.searchText = "";

        MetadataItemService.dataElements({dataSetUID: dataSetUID,sourceID:sourceId, page: $scope.currentPage, perPage: $scope.perPage}, function (response) {
          $scope.items = response.data;
        }, function (error) {
          console.log(error);
        });

        $scope.pageChanged = function (searchText) {
          MetadataItemService.dataElements({
            dataSetUID: dataSetUID,
            page: $scope.currentPage,
            perPage: $scope.perPage,
            searchText: searchText,
            sourceID:sourceId
          }, function (response) {
            $scope.items = response.data;
          }, function (error) {
            console.log(error);
          });
        };

        $scope.edit = function (dataSetUID, dataModel, currentPage, perPage) {
          let modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/metadata_items/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, MetadataItemService) {
              $scope.dataModel = angular.copy(dataModel);
              $scope.options = JSON.parse(dataModel.category_option_combos);
              $scope.categoryOptionCombos = $scope.options.categoryOptionCombos;

              $scope.update = function () {
                if ($scope.dataForm.$invalid) {
                  $scope.formHasErrors = true;
                  return;
                }
                MetadataItemService.update({id: dataModel.id,dataSourceId:$scope.dataSourceId, page: currentPage, perPage: perPage, dataSetUID: dataSetUID}, $scope.dataModel,
                  function (data) {
                    $uibModalInstance.close(data);
                  },
                  function (error) {
                    $scope.errorMessage = error.data.message;
                    $scope.errors = error.data.errors;
                  }
                );
              };
              $scope.close = function () {
                $uibModalInstance.dismiss('cancel');
              };
            }
          });

          modalInstance.result.then(function (response) {
              $scope.successMessage = response.message;
              $scope.items = response.data;
              $scope.currentPage = $scope.items.current_page;
            },
            function () {
              console.log('Modal dismissed at: ' + new Date());
            });
        };
      };

    },function (error) {
      console.log(error);
    });
  };

}

MetadataItemController.resolve = {
  DataModel: function (MetadataSourceService, $timeout, $q) {
    let deferred = $q.defer();
    MetadataSourceService.fetchAll(function (response) {
      deferred.resolve(response.data);
    }, function (error) {
      console.log(error);
    });
    return deferred.promise;
  }
};
