function PriorityAreaController($scope, FinancialYearModel, $uibModal, AllFinancialYearService, PriorityAreaService,
                                FinancialYearService, ConfirmDialogService,CarryVersionService,
                                PaginatedPriorityAreaService, TogglePriorityAreaService) {

    $scope.financialYears = FinancialYearModel;
    $scope.title = "PRIORITY_AREAS";

    $scope.loadVersions = function (financialYearId) {

        FinancialYearService.versions({id: financialYearId, type: 'PRIORITY_AREA'}, function (response) {
            $scope.versions = response.data;
        });

        $scope.loadPriorityAreas = function (versionId) {

            $scope.currentPage = 1;
            $scope.maxSize = 3;
            $scope.perPage = 10;

            PaginatedPriorityAreaService.get({
                page: 1,
                perPage: 10,
                versionId: versionId,
                financialYearId: financialYearId,
            }, function (response) {
                $scope.priorityAreas = response.data;
            });

            $scope.pageChanged = function () {
                PaginatedPriorityAreaService.get({
                    page: $scope.currentPage,
                    perPage: $scope.perPage,
                    versionId: versionId,
                    financialYearId: financialYearId,
                }, function (response) {
                    $scope.priorityAreas = response.data;
                });
            };

            $scope.copy = function (financialYear, version) {
                let modalInstance = $uibModal.open({
                    templateUrl: '/pages/setup/priority_area/copy.html',
                    backdrop: false,
                    controller: function ($scope, $uibModalInstance, AllFinancialYearService, PriorityAreaService, FinancialYearService) {

                        $scope.sourceFinancialYear = financialYear;
                        $scope.sourceVersion = version;

                        AllFinancialYearService.query(function (data) {
                            $scope.financialYears = data;

                            $scope.loadFinancialYearVersions = function (financialYearId) {

                                FinancialYearService.otherVersions({
                                    financialYearId: financialYearId,
                                    currentVersionId: versionId,
                                    type: 'PRIORITY_AREA',
                                    currentFinancialYearId: $scope.sourceFinancialYear.id,
                                }, function (response) {
                                    $scope.financialYearVersions = response.data;
                                    $scope.copyPriorityAreas = function (sourceFinancialYearId, sourceVersionId, destinationVersionId) {
                                        PriorityAreaService.copyPriorityAreas({
                                            sourceFinancialYearId: sourceFinancialYearId,
                                            sourceVersionId: sourceVersionId,
                                            destinationVersionId: destinationVersionId
                                        }, function (response) {
                                            $uibModalInstance.close(response);
                                        });
                                    };
                                });
                            };
                        });

                        $scope.close = function () {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }
                });
                //Called when modal is close by cancel or to store data
                modalInstance.result.then(function (response) {
                        $scope.successMessage = response.message;
                    },
                    function () {

                    });

            };

            $scope.create = function () {
                let modalInstance = $uibModal.open({
                    templateUrl: '/pages/setup/priority_area/create.html',
                    backdrop: false,
                    controller: function ($scope, $uibModalInstance, CreatePriorityAreaService, AllLinkLevelService, AllSectorService) {

                        AllSectorService.query(function (data) {
                            $scope.sectors = data;
                        });

                        AllLinkLevelService.query(function (data) {
                            $scope.link_levels = data;
                        });

                        $scope.priorityAreaToCreate = {};

                        $scope.store = function () {
                            if ($scope.createPriorityAreaForm.$invalid) {
                                $scope.formHasErrors = true;
                                return;
                            }
                            $scope.priorityAreaToCreate.perPage = $scope.perPage;
                            $scope.priorityAreaToCreate.versionId = versionId;
                            $scope.priorityAreaToCreate.financialYearId = financialYearId;
                            CreatePriorityAreaService.store($scope.priorityAreaToCreate,
                                function (data) {
                                    $uibModalInstance.close(data);
                                },
                                function (error) {
                                    $scope.errorMessage = error.data.errorMessage;
                                    $scope.errors = error.data.data.errors;
                                }
                            );
                        };
                        //Function to close modal when cancel button clicked
                        $scope.close = function () {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }
                });
                //Called when modal is close by cancel or to store data
                modalInstance.result.then(function (response) {
                        //Service to create new financial year
                        $scope.successMessage = response.message;
                        $scope.priorityAreas = response.data;
                        $scope.currentPage = $scope.priorityAreas.current_page;
                    },
                    function () {
                        //If modal is closed
                        console.log('Modal dismissed at: ' + new Date());
                    });

            };

            $scope.edit = function (priorityAreaToEdit, currentPage, perPage) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/pages/setup/priority_area/edit.html',
                    backdrop: false,
                    controller: function ($scope, $uibModalInstance, AllSectorService, AllLinkLevelService, UpdatePriorityAreaService) {
                        $scope.priorityAreaToEdit = angular.copy(priorityAreaToEdit);
                        $scope.priorityAreaToEdit.link_level = parseInt(priorityAreaToEdit.link_level);
                        AllLinkLevelService.query(function (data) {
                            $scope.link_levels = data;
                        });
                        $scope.update = function () {
                            UpdatePriorityAreaService.update({
                                    page: currentPage,
                                    perPage: perPage,
                                    versionId: versionId,
                                    financialYearId: financialYearId,
                                }, $scope.priorityAreaToEdit,
                                function (response) {
                                    //Successful function when
                                    $uibModalInstance.close(response);
                                },
                                function (error) {
                                    $scope.errorMessage = error.data.errorMessage;
                                    $scope.errors = error.data.data.errors;
                                }
                            );
                        };
                        //Function to close modal when cancel button clicked
                        $scope.close = function () {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }
                });
                //Called when modal is close by cancel or to store data
                modalInstance.result.then(function (response) {
                        $scope.successMessage = response.message;
                        $scope.priorityAreas = response.data;
                        $scope.currentPage = $scope.priorityAreas.current_page;
                    },
                    function () {

                    });
            };

            $scope.sectors = function (priorityAreaToEdit, currentPage, perPage) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/pages/setup/priority_area/sectors.html',
                    backdrop: false,
                    controller: function ($scope, $uibModalInstance, PriorityAreaService, AllSectorService) {

                        PriorityAreaService.sectors({priorityAreaId: priorityAreaToEdit.id}, function (response) {
                            $scope.sectors = response.data;
                        });

                        $scope.priorityArea = priorityAreaToEdit;

                        $scope.removeSector = function (id) {
                            PriorityAreaService.removeSector({
                                priorityAreaId: priorityAreaToEdit.id,
                                id: id
                            }, function (response) {
                                $scope.sectors = response.data;
                                $scope.successMessage = response.message;
                            });
                        };

                        AllSectorService.query(function (data) {
                            $scope.allSectors = data;
                        });

                        $scope.sectorData = {};
                        $scope.addSector = function () {
                            $scope.sectorData.priorityAreaId = priorityAreaToEdit.id;
                            PriorityAreaService.addSector({
                                    page: currentPage,
                                    perPage: perPage,
                                    versionId: versionId,
                                    financialYearId: financialYearId,
                                }, $scope.sectorData,
                                function (response) {
                                    $scope.sectors = response.data;
                                    $scope.successMessage = response.message;
                                },
                                function (error) {
                                    $scope.errorMessage = error.data.errorMessage;
                                }
                            );
                        };
                        //Function to close modal when cancel button clicked
                        $scope.close = function () {
                            $uibModalInstance.dismiss('cancel');
                        };
                    }
                });
                //Called when modal is close by cancel or to store data
                modalInstance.result.then(function (response) {
                        $scope.successMessage = response.message;
                        $scope.priorityAreas = response.data;
                        $scope.currentPage = $scope.priorityAreas.current_page;
                    },
                    function () {

                    });
            };

            $scope.delete = function (id, currentPage, perPage, versionId) {
                ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                        PriorityAreaService.removePriorityArea({
                                id: id,
                                page: currentPage,
                                perPage: perPage,
                                versionId: versionId,
                                financialYearId: financialYearId
                            },
                            function (response) {
                                $scope.successMessage = response.message;
                                $scope.priorityAreas = response.data;
                                $scope.currentPage = $scope.priorityAreas.current_page;
                            }, function (error) {

                            }
                        );
                    },
                    function () {
                        console.log("NO");
                    });
            };

            $scope.toggleActive = function (id, is_active, perPage) {
                $scope.priorityAreaToActivate = {};
                $scope.priorityAreaToActivate.id = id;
                $scope.priorityAreaToActivate.is_active = is_active;
                TogglePriorityAreaService.toggleActive({
                        perPage: perPage,
                        versionId: versionId
                    }, $scope.priorityAreaToActivate,
                    function (response) {
                        $scope.action = response.action;
                        $scope.successMessage = response.message;
                        $scope.alertType = response.alertType;
                    });
            };
        };
    };

    $scope.carry = function (currentFinancialYearId, item) {
        CarryVersionService.showDialog('PA',currentFinancialYearId, item).then(function(data){
            console.log(data);
        }, function(){

        });
    };
}

PriorityAreaController.resolve = {
    FinancialYearModel: function (AllFinancialYearService, $q) {
        let deferred = $q.defer();
        AllFinancialYearService.query(function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};
