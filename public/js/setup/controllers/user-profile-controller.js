function UserProfileController($scope, UserProfile, localStorageService, $uibModal, $window,CreateUserService) {
  $scope.title = "USER_PROFILE";
  UserProfile.get({}, function (data) {
    $scope.userProfile = data;
  });
  $scope.clearMessages = function () {
    $scope.formHasErrors = false;
    $scope.inProgress = false;
  };

  var options = {
    beforeSubmit: $scope.validate,
    success: processResponse,
    error: failureHandler,
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
    }
  };
  $('#profilePictureForm').ajaxForm(options);

  $scope.validate = function (formData) {
    $scope.$apply(function () {
      $scope.inProgress = true;
      if ($scope.profilePictureForm.$invalid || $scope.profilePicture === undefined) {
        $scope.picFormHasErrors = true;
        $scope.inProgress = false;
        return;
      }
    });
    $scope.$apply();
    if ($scope.inProgress)
      $('#loader').show();
    return $scope.inProgress;
  };

  var failureHandler = function (response) {
    $('#loader').hide();
    $scope.$apply(function () {
      $scope.inProgress = false;
    });
  };

  function processResponse(response) {
    $('#loader').hide();
    if (response.successMessage) {
      $scope.$apply(function () {
        $scope.successMessage = response.successMessage;
        $scope.userProfile.profile_picture_url = response.profile_picture_url;
        localStorageService.add(localStorageKeys.PROFILE_PICTURE, response.profile_picture_url);
      });

    }
  }
  $scope.emailPattern = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
  $scope.validateEmail = function (email) {
      $scope.emailExist = false;
    if (email !== undefined && email.length > 6) {
      $scope.validatingEmail = true;
      CreateUserService.validateEmail({
        email: email,
        userId: ($scope.userProfile !== undefined) ? $scope.userProfile.id : 0,
        noLoader: true
      }, function (data) {
        console.log('data',data);
        $scope.validatingEmail = false;
        if (data.exist) {
          $scope.emailExist = true;
        } else {
          $scope.emailExist = false;
        }
      }, function () {
        $scope.validatingEmail = false;
      });
    }
  };

  $scope.updateProfile = function () {
    if ($scope.updateUserProfileForm.$invalid) {
      $scope.formHasErrors = true;
      return;
    }

    UserProfile.update({id: $scope.userProfile}, $scope.userProfile, function (data) {
      $scope.successMessage = data.successMessage;
    });
  };
  $scope.resetPassword = function (userId) {
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/setup/user/reset-password.html',
      backdrop: false,
      windowClass: 'center-modal',
      controller: function ($scope, $uibModalInstance, UpdatePasswordService) {
        $scope.user = {"id": userId};
        //$scope.passwordPattern = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}:;<>,.?\~_+-=\|]).{6,10}$/;
        $scope.passwordPattern = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-/_]).{8,}$";
        $scope.reset = function () {
          if ($scope.resetPasswordForm.$invalid) {
            $scope.formHasErrors = true;
            return;
          }
          UpdatePasswordService.update($scope.user,
            function (data) {
              if (data.successMessage) {
                $uibModalInstance.close(data);
                localStorage.clear();
                document.cookie = 'laravel_session' + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
                $window.location = "/logout";

              } else {
                $scope.errorMessage = data.errorMessage;
              }

            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
        $scope.successMessage = data.successMessage;
      },
      function () {
        //If modal is closed
      });
  };

}

UserProfileController.resolve = {};
