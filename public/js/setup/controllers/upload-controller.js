function UploadController($scope, Upload, $timeout) {

    $scope.title = "Template Files Upload Tool";

    $scope.uploadFile = function (file) {
        file.upload = Upload.upload({
            url: '/files/template-files/upload',
            method: 'POST',
            data: {
                file: file
            }
        });

        file.upload.then(function (response) {
            $timeout(function () {
                file.result = response.data;
                $scope.successMessage = response.data.successMessage;
            });
        }, function (response) {
            if (response.status > 0) {
                $scope.errorMsg = response.status + ': ' + response.data;
                $scope.errorMessage = response.data.errorMessage;
                $scope.successMessage = response.data.successMessage;
            }
        }, function (evt) {
            // Math.min is to fix IE which reports 200% sometimes
            file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
        });
    };


    $scope.uploadFiles = function(files, errFiles) {
        $scope.files = files;
        $scope.errFiles = errFiles;
        angular.forEach(files, function(file) {
            file.upload = Upload.upload({
                method: 'POST',
                url: '/files/template-files/upload',
                data: {file: file}
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
            }, function (response) {
                if (response.status > 0)
                    $scope.errorMsg = response.status + ': ' + response.data;
            }, function (evt) {
                file.progress = Math.min(100, parseInt(100.0 *
                    evt.loaded / evt.total));
            });
        });
    };
}