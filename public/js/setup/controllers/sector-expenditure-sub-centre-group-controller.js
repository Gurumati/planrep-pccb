function SectorExpenditureSubCentreGroupController($scope, SectorExpenditureSubCentreGroupModel, $uibModal,
                                              ConfirmDialogService, DeleteSectorExpenditureSubCentreGroupService,
                                              PaginatedSectorExpenditureSubCentreGroupService) {

    $scope.sectorExpenditureSubCentreGroups = SectorExpenditureSubCentreGroupModel;
    $scope.title = "SECTOR_EXPENDITURE_SUB_CENTRE_GROUPS";
    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedSectorExpenditureSubCentreGroupService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.sectorExpenditureSubCentreGroups = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/sector_expenditure_sub_centre_group/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllSectorExpenditureSubCentreService, AllBdcGroupService,
                                  CreateSectorExpenditureSubCentreGroupService) {
                $scope.formDataModel = {};

                AllSectorExpenditureSubCentreService.query(function (data) {
                    $scope.subCentres = data;
                });

                AllBdcGroupService.query(function (data) {
                    $scope.bdcGroups = data;
                });

                $scope.store = function () {
                    if ($scope.formData.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateSectorExpenditureSubCentreGroupService.store({perPage: $scope.perPage}, $scope.formDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.sectorExpenditureSubCentreGroups = data.sectorExpenditureSubCentreGroups;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (formDataModel, currentPage, perPage) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/sector_expenditure_sub_centre_group/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, AllSectorExpenditureSubCentreService, AllBdcGroupService, UpdateSectorExpenditureSubCentreGroupService) {

                AllSectorExpenditureSubCentreService.query(function (data) {
                    $scope.subCentres = data;
                    $scope.formDataModel = angular.copy(formDataModel);
                });

                AllBdcGroupService.query(function (data) {
                    $scope.bdcGroups = data;
                });

                $scope.update = function () {
                    if ($scope.formData.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdateSectorExpenditureSubCentreGroupService.update({page: currentPage, perPage: perPage}, $scope.formDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                            $scope.errors = error.data.errors;
                        }
                    );


                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.sectorExpenditureSubCentreGroups = data.sectorExpenditureSubCentreGroups;
                $scope.currentPage = $scope.sectorExpenditureSubCentreGroups.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('CONFIRM_DELETE', 'Are you sure you want to trash this record?').then(function () {
                DeleteSectorExpenditureSubCentreGroupService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.sectorExpenditureSubCentreGroups = data.sectorExpenditureSubCentreGroups;
                        $scope.currentPage = $scope.sectorExpenditureSubCentreGroups.current_page;
                    }, function (error) {
                        $scope.errorMessage = error.data.errorMessage;
                    }
                );
            },
            function () {

            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/sector_expenditure_sub_centre_group/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance, TrashedSectorExpenditureSubCentreGroupService,
                                  RestoreSectorExpenditureSubCentreGroupService, EmptySectorExpenditureSubCentreGroupTrashService, PermanentDeleteSectorExpenditureSubCentreGroupService) {
                TrashedSectorExpenditureSubCentreGroupService.query(function (data) {
                    $scope.trashedSectorExpenditureSubCentreGroups = data;
                });
                $scope.restoreSectorExpenditureSubCentreGroup = function (id) {
                    RestoreSectorExpenditureSubCentreGroupService.restore({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedSectorExpenditureSubCentreGroups = data.trashedSectorExpenditureSubCentreGroups;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteSectorExpenditureSubCentreGroupService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedSectorExpenditureSubCentreGroups = data.trashedSectorExpenditureSubCentreGroups;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptySectorExpenditureSubCentreGroupTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedSectorExpenditureSubCentreGroups = data.trashedSectorExpenditureSubCentreGroups;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.sectorExpenditureSubCentreGroups = data.sectorExpenditureSubCentreGroups;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

SectorExpenditureSubCentreGroupController.resolve = {
    SectorExpenditureSubCentreGroupModel: function (PaginatedSectorExpenditureSubCentreGroupService, $q) {
        var deferred = $q.defer();
        PaginatedSectorExpenditureSubCentreGroupService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};