function PlanningMatrixController($scope, DataModel, $uibModal,
                                  ConfirmDialogService, PlanningMatrixService) {

    $scope.items = DataModel;
    $scope.title = "PLANNING_MATRICES";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PlanningMatrixService.paginated({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.items = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/planning_matrix/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, PlanningMatrixService, GlobalService) {

                $scope.createDataModel = {};

                GlobalService.referenceDocuments(function (data) {
                    $scope.referenceDocs = data.referenceDocs;
                });

                GlobalService.sectors(function(data){
                    $scope.sectors = data.sectors;
                });

                $scope.selectedSectors = function (sectors) {
                    var sectorId = sectors.id;
                    GlobalService.pscBySector({sectorId:sectorId},function(data){
                        $scope.adminhierarchies = data.psc;
                    });
                }

                $scope.store = function () {
                    if ($scope.createForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    PlanningMatrixService.save({perPage: $scope.perPage}, $scope.createDataModel,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (updateDataModel, currentPage, perPage) {
        //console.log(updateDataModel);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/planning_matrix/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, GlobalService, PlanningMatrixService) {

                GlobalService.referenceDocuments(function (data) {
                    $scope.referenceDocs = data.referenceDocs;
                    $scope.updateDataModel = angular.copy(updateDataModel);
                });

                GlobalService.sectors(function(data){
                    $scope.sectors = data.sectors;
                });


                $scope.update = function () {
                    PlanningMatrixService.update({page: currentPage, perPage: perPage}, $scope.updateDataModel,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = $scope.items.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.toggleActive = function (id, is_active, perPage) {
        $scope.itemToToggle = {};
        $scope.itemToToggle.id = id;
        $scope.itemToToggle.is_active = is_active;
        PlanningMatrixService.toggleActive({perPage: perPage}, $scope.itemToToggle, function (data) {
            $scope.action = data.action;
            $scope.alertType = data.alertType;
        });
    };

    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                PlanningMatrixService.delete({id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.items = data.items;
                        $scope.currentPage = $scope.items.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };
    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/setup/planning_matrix/trash.html',
            backdrop: false,
            controller: function ($scope, $route, $uibModalInstance, PlanningMatrixService) {
                PlanningMatrixService.trashed(function (data) {
                    $scope.trashedItems = data.trashedItems;
                });
                $scope.restorePlanningMatrix = function (id) {
                    PlanningMatrixService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PlanningMatrixService.permanentDelete({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    PlanningMatrixService.emptyTrash({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedItems = data.trashedItems;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                    $route.reload();
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.items = data.items;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

PlanningMatrixController.resolve = {
    DataModel: function (PlanningMatrixService, $timeout, $q) {
        var deferred = $q.defer();
        $timeout(function () {
            PlanningMatrixService.paginated({page: 1, perPage: 10}, function (data) {
                deferred.resolve(data);
            });
        }, 900);
        return deferred.promise;
    }
};