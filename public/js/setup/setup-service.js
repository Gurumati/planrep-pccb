var setupServices = angular.module('setupServices', ['ngResource']);

/** Audit trails */
setupServices.factory('AuditService', function ($resource) {
  return $resource('/json/allAuditTrails');
});

/**Activity Categories Start*/
setupServices.factory('ActivityCategory', ['$resource', function ($resource) {
  return $resource('/json/activity-categories/:id', {}, {


    query: {
      method: 'GET',
      isArray: true
    },
    paginated: {
      method: 'GET',
      url: '/json/activity-categories/paginated'
    },
    toggleActive: {
      method: 'PUT',
      url: '/json/activity-categories/toggle-active'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    }
  });
}]);

/**Activity Categories End **/
setupServices.factory('FinancialYear', function ($resource) {
  return $resource('/json/financial-year/:id', {
    id: '@id'
  }, {
    execute: {
      method: 'PUT',
      url: '/json/financial-year/execute/:id',
      params: {
        id: '@id'
      }
    },
    reExecute: {
      method: 'PUT',
      url: '/json/financial-year/reExecute/:id',
      params: {
        id: '@id'
      }
    },
    close: {
      method: 'PUT',
      url: '/json/financial-year/close/:id',
      params: {
        id: '@id'
      }
    }
  });
});
/**
 * Service to fix invalid activity code ie activities with code length more than 8 characters
 */
setupServices.factory("ActivityWithInvalidCode", function ($resource) {
  return $resource('/json/get-total-activity-with-invalid-code', {}, {
    fixCode: {
      method: 'POST',
      url: '/json/fix-activity-with-invalid-code/:limit',
      params: {
        limit: '@limit'
      }
    },
    totalByInvalidChainCode: {
      method: 'GET',
      url: '/json/get-total-activity-with-invalid-chain-code'
    },
    fixByInvalidChainCode: {
      method: 'POST',
      url: '/json/fix-activity-with-invalid-chain-code/:limit',
      params: {
        limit: '@limit'
      }
    },
    totalByDuplicateCode: {
      method: 'GET',
      url: '/json/get-total-activity-with-duplicate-code'
    },
    fixByDuplicateCode: {
      method: 'POST',
      url: '/json/fix-activity-with-duplicate-code/:limit',
      params: {
        limit: '@limit'
      }
    },
  });
});

setupServices.factory('CreateFinancialYearService', function ($resource) {
  return $resource('/json/createFinancialYear', {}, {
    store: {
      method: 'POST'
    }
  }); //This create financial year and return existing financial years after creating
});
setupServices.factory('UpdateFinancialYearService', function ($resource) {
  return $resource('/json/updateFinancialYear', {}, {
    update: {
      method: 'POST'
    }
  }); //This create financial year and return existing financial years after creating
});
setupServices.factory('DeleteFinancialYearService', function ($resource) {
  return $resource('/json/deleteFinancialYear/:financialYearId', {
    financialYearId: '@financialYearId'
  }, {
    delete: {
      method: 'POST'
    }
  }); //This create financial year and return existing financial years after creating
});
setupServices.factory('OpenFinancialYearService', function ($resource) {
  return $resource('/json/openFinancialYear/:id', {
    id: '@id'
  }, {
    open: {
      method: 'POST'
    }
  });
});
setupServices.factory('OpenFinancialYearPreRequestService', function ($resource) {
  return $resource('/json/openFinancialYearPreRequest/:id', {
    id: '@id'
  }, {
    check: {
      method: 'POST'
    }
  });
});
setupServices.factory('CalendarFinancialYears', function ($resource) {
  return $resource('/json/calenderFinancialYear', {}, {});
});

setupServices.factory('DemoSectionInfoService', function ($resource) {
  return $resource('/json/demoSectionInformation', {}, {}); //This create financial year and return existing financial years after creating
});

setupServices.factory('SaveForm', function ($resource) {
  return $resource('/json/saveForm', {}, {
    save: {
      method: 'POST'
    }
  }); //This create financial year and return existing financial years after creating
});
setupServices.factory('GetForm', function ($resource) {
  return $resource('/json/getForm/:name', {
    name: '@name'
  }, {}); //This create financial year and return existing financial years after creating
});

//Services for AccessRight CRUD
setupServices.factory('CreateAccessRightService', function ($resource) {
  return $resource('/json/createAccessRight', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateAccessRightService', function ($resource) {
  return $resource('/json/updateAccessRight', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteAccessRightService', function ($resource) {
  return $resource('/json/deleteAccessRight/:accessRightId', {
    accessRightId: '@accessRightId'
  }, {
    delete: {
      method: 'POST'
    }
  });
});
//End AccessRight CRUD

//Services for Section Levels CRUD
setupServices.factory('CreateSectionLevelService', function ($resource) {
  return $resource('/json/createSectionLevel', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateSectionLevelService', function ($resource) {
  return $resource('/json/updateSectionLevel', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteSectionLevelService', function ($resource) {
  return $resource('/json/deleteSectionLevel/:sectionLevelId', {
    sectionLevelId: '@sectionLevelId'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
setupServices.factory('LoadParentLevelService', function ($resource) {
  return $resource('/json/loadParentLevelSectionLevel/:sectionLevelId', {
    sectionLevelId: '@sectionLevelId'
  }, {});
});

setupServices.factory('ToggleSectionLevelService', function ($resource) {
  return $resource('/json/toggleSectionLevel', {}, {
    toggleSectionLevel: {
      method: 'POST'
    }
  });
});

setupServices.factory('GetAllSectionLevelService', function ($resource) {
  return $resource('/json/paginatedSectionLevels', {}, {});
});

//Services for Section Levels CRUD
setupServices.factory('Section', function ($resource) {
  return $resource('/json/sections/:id', {
    id: '@id'
  }, {
    getBySector: {
      method: 'GET',
      url: '/json/sections/by-sector/:sectorId',
      params: {
        sectorId: '@sectorId'
      }
    }
  });
});

setupServices.factory('CreateSectionService', function ($resource) {
  return $resource('/json/createSection', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateSectionService', function ($resource) {
  return $resource('/json/updateSection', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteSectionService', function ($resource) {
  return $resource('/json/deleteSection/:sectionId', {
    sectionId: '@sectionId'
  }, {
    delete: {
      method: 'POST'
    }
  });
});

setupServices.factory('LoadParentSectionsService', function ($resource) {
  return $resource('/json/loadParentSections/:childSectionLevelId', {
    childSectionLevelId: '@childSectionLevelId'
  }, {});
});

services.factory('PiscBySectorService', function ($resource) {
  return $resource('/json/piscBysector/:childSectorId', {
    childSectorId: '@childSectorId'
  }, {});
});
//End Section CRUD

//Services for geographical location levels CRUD
setupServices.factory('CreateGeoLocationLevelService', function ($resource) {
  return $resource('/json/createGeolocationLevel', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateGeoLocationLevelService', function ($resource) {
  return $resource('/json/updateGeoLocationLevel', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('ToggleGeoLocationLevelService', function ($resource) {
  return $resource('/json/toggleGeoLocationLevel', {}, {
    toggleGeoLocationLevel: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteGeoLocationLevelService', function ($resource) {
  return $resource('/json/deleteGeoLocationLevel/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedGeoLocationLevelService', function ($resource) {
  return $resource('/json/geo_location_levels/trashed', {}, {});
});

setupServices.factory('RestoreGeoLocationLevelService', function ($resource) {
  return $resource('/json/geo_location_level/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyGeoLocationLevelTrashService', function ($resource) {
  return $resource('/json/geo_location_level/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteGeoLocationLevelService', function ($resource) {
  return $resource('/json/geo_location_level/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});
//end geographical location levels service crud

//Geo locations service crud

setupServices.factory('CreateGeoLocationService', function ($resource) {
  return $resource('/json/createGeoLocation', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateGeoLocationService', function ($resource) {
  return $resource('/json/updateGeoLocation', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteGeoLocationService', function ($resource) {
  return $resource('/json/deleteGeoLocation/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('LoadParentGeoLocationsService', function ($resource) {
  return $resource('/json/loadParentGeoLocations/:childGeoLocationLevelId', {
    childGeoLocationLevelId: '@childGeoLocationLevelId'
  }, {});
});

setupServices.factory('GeographicalLocationService', ['$resource', function ($resource) {
  return $resource('/json/administrativeHierarchies/:id', {}, {
    import: {
      method: 'GET',
      url: '/json/api/geo_locations/import'
    }
  });
}]);
// Service for pisc type
setupServices.factory('PiscTypeService', function ($resource) {
  return $resource('/api/get-pisc-type', {}, {
    getPiscType: {
      method: 'GET',
      url: '/api/get-pisc-type'
    }
  });
});

// Service for COFOG
setupServices.factory('CofogService', function ($resource) {
  return $resource('/api/get-allCofog', {}, {
    getAllParent: {
      method: 'GET',
      url: '/api/get-allCofog'
    }
  });
});

//Services for Admin Hierarchy Levels CRUD
setupServices.factory('CreateAdminHierarchyLevelService', function ($resource) {
  return $resource('/json/createAdminHierarchyLevel', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateAdminHierarchyLevelService', function ($resource) {
  return $resource('/json/updateAdminHierarchyLevel', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('ToggleAdminHierarchyLevelService', function ($resource) {
  return $resource('/json/toggleAdminHierarchyLevel', {}, {
    toggleAdminHierarchyLevel: {
      method: 'POST'
    }
  });
});

setupServices.factory('CreateAdminHierarchyService', function ($resource) {
  return $resource('/json/createAdminHierarchy', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateAdminHierarchyService', function ($resource) {
  return $resource('/json/updateAdminHierarchy', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteAdminHierarchyService', function ($resource) {
  return $resource('/json/deleteAdminHierarchy/:admin_hierarchy_id', {
    admin_hierarchy_id: '@admin_hierarchy_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('LoadParentAdminHierarchiesService', function ($resource) {
  return $resource('/json/loadParentAdminHierarchies/:childAdminHierarchyLevelId', {
    childAdminHierarchyLevelId: '@childAdminHierarchyLevelId'
  }, {});
});

setupServices.factory('LoadParentByIdService', function ($resource) {
  return $resource('/json/Load-parent-by-id', {}, {
    getAllParent: {
      method: 'GET',
      url: '/json/Load-parent-by-id'
    }
  });
});

setupServices.factory('OnMinistryLoadParentAdminHierarchiesService', function ($resource) {
  return $resource('/json/onMinistryloadParentAdminHierarchies/:ministryId', {
    ministryId: '@ministryId'
  }, {});
});

//ADMIN HIERARCHY PROJECTS

setupServices.factory('AllAdminHierarchyProjectService', function ($resource) {
  return $resource('/json/allAdminHierarchyProjects/:admin_hierarchy_id', {
    admin_hierarchy_id: '@admin_hierarchy_id'
  }, {
    projects: {
      method: 'GET'
    }
  });
});

setupServices.factory('AddAdminHierarchyProjectService', function ($resource) {
  return $resource('/json/addAdminHierarchyProject', {}, {
    add_project: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteAdminHierarchyProjectService2', function ($resource) {
  return $resource('/json/delete_AdminHierarchyProject/:admin_hierarchy_project_id/:admin_hierarchy_id', {
    admin_hierarchy_project_id: '@admin_hierarchy_project_id',
    admin_hierarchy_id: '@admin_hierarchy_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});


//End Section CRUD

//Reference Type CRUD
setupServices.factory('CreateReferenceTypeService', function ($resource) {
  return $resource('/json/createReferenceType', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateReferenceTypeService', function ($resource) {
  return $resource('/json/updateReferenceType', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateReferenceService', function ($resource) {
  return $resource('/json/updateReference', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteReferenceTypeService', function ($resource) {
  return $resource('/json/deleteReferenceType/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'POST'
    }
  });
});
setupServices.factory('ReferencesService', function ($resource) {
  return $resource('/json/getByReferenceTypeId/:referenceTypeId', {
    referenceTypeId: '@referenceTypeId'
  }, {});
});
setupServices.factory('CreateReferenceService', function ($resource) {
  return $resource('/json/createReference', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('ParentReferences', function ($resource) {
  return $resource('/json/getParentByReferenceTypeId/:referenceTypeId/:referenceId', {
    referenceTypeId: '@referenceTypeId',
    referenceId: '@referenceId'
  }, {});
});

//Services for Funders Levels CRUD
setupServices.factory('PaginateFunderService', function ($resource) {
  return $resource('/json/paginateFunders', {}, {
    paginateIndex: {}
  });
});
setupServices.factory('CreateFunderService', function ($resource) {
  return $resource('/json/createFunder', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateFunderService', function ($resource) {
  return $resource('/json/updateFunder', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteFunderService', function ($resource) {
  return $resource('/json/deleteFunder/:funderId', {
    funderId: '@funderId'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
setupServices.factory('ActivateFunderService', function ($resource) {
  return $resource('/json/activateFunder', {}, {
    activate: {
      method: 'POST'
    }
  });
});
//End Funders CRUD

//Services for Responsible Persons CRUD
setupServices.factory('PaginatedResponsiblePersonService', function ($resource) {
  return $resource('/json/paginatedResponsiblePersons', {}, {});
});

setupServices.factory('UpdateResponsiblePersonService', function ($resource) {
  return $resource('/json/updateResponsiblePerson', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteResponsiblePersonService', function ($resource) {
  return $resource('/json/deleteResponsiblePerson/:responsiblePersonId', {
    responsiblePersonId: '@responsiblePersonId'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
setupServices.factory('LoadSectionsService', function ($resource) {
  return $resource('/json/loadSections/:SectionLevelId', {
    SectionLevelId: '@SectionLevelId'
  }, {});
});
setupServices.factory('ActivateResponsiblePersonService', function ($resource) {
  return $resource('/json/activateResponsiblePerson', {}, {
    activate: {
      method: 'POST'
    }
  });
});
//End Responsible Persons CRUD

//Access right for unit
setupServices.factory('CreateUnitService', function ($resource) {
  return $resource('/json/createUnit', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateUnitService', function ($resource) {
  return $resource('/json/updateUnit', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteUnitService', function ($resource) {
  return $resource('/json/deleteUnit/:unitId', {
    unitId: '@unitId'
  }, {
    delete: {
      method: 'POST'
    }
  });
});
setupServices.factory('ToggleUnitService', function ($resource) {
  return $resource('/json/toggleUnit', {}, {
    toggleUnit: {
      method: 'POST'
    }
  });
});

///End of unit CRUD

//Access right for userGroup
setupServices.factory('CreateUserGroupService', function ($resource) {
  return $resource('/json/createUserGroup', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateUserGroupService', function ($resource) {
  return $resource('/json/updateUserGroup', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteUserGroupService', function ($resource) {
  return $resource('/json/deleteUserGroup/:userGroupId', {
    userGroupId: '@userGroupId'
  }, {
    delete: {
      method: 'POST'
    }
  });
});
setupServices.factory('ToggleUserGroupService', function ($resource) {
  return $resource('/json/toggleUserGroup', {}, {
    toggleUserGroup: {
      method: 'POST'
    }
  });
});
///End of userGroup CRUD

//The budget class services for CRUD start here
setupServices.factory('CreateBudgetClassService', function ($resource) {
  return $resource('/json/createBudgetClass', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateBudgetClassService', function ($resource) {
  return $resource('/json/updateBudgetClass', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteBudgetClassService', function ($resource) {
  return $resource('/json/deleteBudgetClass/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'DELETE'
    }
  });
});
setupServices.factory('BudgetClassesService', function ($resource) {
  return $resource('/json/budgetClasses', {}, {});
});
setupServices.factory('ParentBudgetClassesService', function ($resource) {
  return $resource('/json/parentBudgetClasses/:childId/:financialYearId/:versionId', {
    childId: '@childId',
    financialYearId: '@financialYearId',
    versionId: '@versionId',
  }, {});
});
/*setupServices.factory('ParentBudgetClasses',function ($resource) {
 return $resource('/json/getParentByBudgetClassesId/:plan_chain_id/:childTemplateId',{plan_chain_id:'@plan_chain_id',childTemplateId:'@childTemplateId'},{});
 });*/


///End of BudgetClasses CRUD
setupServices.factory('CreateBudgetTemplateService', function ($resource) {
  return $resource('/json/createBudgetTemplate', {}, {
    storeTemplate: {
      method: 'POST'
    }
  });
});
setupServices.factory('AllBudgetTemplateService', function ($resource) {
  return $resource('/json/allBudgetTemplate/:budget_class_id', {
    budget_class_id: '@budget_class_id'
  }, {});
});

setupServices.factory('SubBudgetClassesService', function ($resource) {
  return $resource('/json/subBudgetClassesService', {});
});
setupServices.factory('DeleteBudgetTemplateService', function ($resource) {
  return $resource('/json/deleteBudgetTemplate/:budgetTemplateId', {
    budgetTemplateId: '@budgetTemplateId'
  }, {
    delete: {
      method: 'POST'
    }
  });
});
setupServices.factory('LoadBudgetTemplateService', function ($resource) {
  return $resource('/json/loadBudgetTemplate/:template_type/:budget_class_id', {
    template_type: '@template_type',
    budget_class_id: '@budget_class_id'
  }, {
    storeTemplate: {
      method: 'GET'
    }
  });
});

//The budget class services for CRUD ends here


//The decision level services for CRUD start here
setupServices.factory('CreateDecisionLevelService', function ($resource) {
  return $resource('/json/createDecisionLevel', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateDecisionLevelService', function ($resource) {
  return $resource('/json/updateDecisionLevel', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteDecisionLevelService', function ($resource) {
  return $resource('/json/deleteDecisionLevel/:decisionLevelId', {
    decisionLevelId: '@decisionLevelId'
  }, {
    delete: {
      method: 'POST'
    }
  });
});
setupServices.factory('DecisionLevelsByAdminHierarchyLevelService', function ($resource) {
  return $resource('/json/decisionLevelsByAdminHierarchyLevel/:adminHierarchyLevelId', {
    adminHierarchyLevelId: '@adminHierarchyLevelId'
  }, {});
});
setupServices.factory('GetAllDecisionLevelService', function ($resource) {
  return $resource('/json/paginatedDecisionLevels', {}, {});
});

setupServices.factory('DefaultDecisionLevelService', function ($resource) {
  return $resource('/json/defaultDecisionLevel', {}, {
    defaultDecisionLevel: {
      method: 'POST'
    }
  });
});
//The decision level services for CRUD end here

//The services for the facility types CRUD start here
setupServices.factory('GetAdminHierarchiesByFacilityTypeIDService', function ($resource) {
  return $resource('/json/getAdminHierarchyByFacilityTypeId/:id', {
    id: '@id'
  }, {
    getAdminHierarchiesByFacilityTypeId: {
      method: 'GET'
    }
  });
});

setupServices.factory('CreateFacilityTypeService', function ($resource) {
  return $resource('/json/createFacilityType', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateFacilityTypeService', function ($resource) {
  return $resource('/json/updateFacilityType', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteFacilityTypeService', function ($resource) {
  return $resource('/json/deleteFacilityType/:facility_type_id', {
    facility_type_id: '@facility_type_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('PaginatedFacilityTypeService', function ($resource) {
  return $resource('/json/paginatedFacilityTypes', {}, {});
});
//The services for the facility types CRUD ends here

//Services for Procurement Type CRUD

setupServices.factory('PaginatedProcurementTypeService', function ($resource) {
  return $resource('/json/paginatedProcurementTypes', {}, {});
});

// Govella
setupServices.factory('ProcurementTypeService', function ($resource) {
  return $resource('/json/procurementTypes', {}, {});
});

// setupServices.factory('loadProcurementTypeService', function ($resource) {
//   return $response('route');
// });

setupServices.factory('CreateProcurementTypeService', function ($resource) {
  return $resource('/json/createProcurementType', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateProcurementTypeService', function ($resource) {
  return $resource('/json/updateProcurementType', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteProcurementTypeService', function ($resource) {
  return $resource('/json/deleteProcurementType/:procurement_type_id', {
    procurement_type_id: '@procurement_type_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('ToggleProcurementTypeService', function ($resource) {
  return $resource('/json/toggleProcurementType', {}, {
    toggleProcurementType: {
      method: 'POST'
    }
  });
});
//End Procurement Type CRUD

//Services for Account Type CRUD
setupServices.factory('CreateAccountTypeService', function ($resource) {
  return $resource('/json/createAccountType', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateAccountTypeService', function ($resource) {
  return $resource('/json/updateAccountType', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteAccountTypeService', function ($resource) {
  return $resource('/json/deleteAccountType/:account_type_id', {
    account_type_id: '@account_type_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
//End Account Type CRUD

//Services for Plan chains  CRUD
setupServices.factory('PaginatePlanChainsService', function ($resource) {
  return $resource('/json/paginatePlanChain', {}, {});
});
setupServices.factory('PlanChainsTypeService', function ($resource) {
  return $resource('/json/planChainsType/:planChainTypeId', {
    planChainTypeId: '@planChainTypeId'
  }, {
    loadPlanChainType: {
      method: 'GET'
    }
  });
});

setupServices.factory('CreatePlanChainService', function ($resource) {
  return $resource('/json/createPlanChain', {}, {
    store: {
      method: 'POST'
    },
    validateObjCode: {
      method: 'GET',
      url: '/json/planChains/validateObjCode/:objCode',
      params: {
        objCode: '@objCode'
      }
    }

  });
});
setupServices.factory('UpdatePlanChainService', function ($resource) {
  return $resource('/json/updatePlanChain', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeletePlanChainService', function ($resource) {
  return $resource('/json/deletePlanChain/:planChainId', {
    planChainId: '@planChainId'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
setupServices.factory('ActivatePlanChainService', function ($resource) {
  return $resource('/json/activatePlanChain', {}, {
    activate: {
      method: 'POST'
    }
  });
});
//End Plan Chains CRUD

//Services for Project Funder  CRUD
setupServices.factory('CreateProjectFunderService', function ($resource) {
  return $resource('/json/createProjectFunder', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('PaginateProjectFunderService', function ($resource) {
  return $resource('/json/paginateProjectFunders', {}, {});
});
setupServices.factory('UpdateProjectFunderService', function ($resource) {
  return $resource('/json/updateProjectFunder', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteProjectFunderService', function ($resource) {
  return $resource('/json/deleteProjectFunder/:projectFunderId', {
    projectFunderId: '@projectFunderId'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
setupServices.factory('ActivateProjectFunderService', function ($resource) {
  return $resource('/json/activateProjectFunder', {}, {
    activate: {
      method: 'POST'
    }
  });
});
//End Project Funder CRUD

//Services for Period CRUD
setupServices.factory('CreatePeriodService', function ($resource) {
  return $resource('/json/createPeriod', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdatePeriodService', function ($resource) {
  return $resource('/json/updatePeriod', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeletePeriodService', function ($resource) {
  return $resource('/json/deletePeriod/:period_id', {
    period_id: '@period_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
setupServices.factory('TogglePeriodService', function ($resource) {
  return $resource('/json/togglePeriod', {}, {
    togglePeriod: {
      method: 'POST'
    }
  });
});
//End Period CRUD

//Services for GFS CODE CATEGORY CRUD
setupServices.factory('GetAllGfsCodeCategoryService', function ($resource) {
  return $resource('/json/paginatedGfsCodeCategories', {}, {});
});

setupServices.factory('CreateGfsCodeCategoryService', function ($resource) {
  return $resource('/json/createGfsCodeCategory', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateGfsCodeCategoryService', function ($resource) {
  return $resource('/json/updateGfsCodeCategory', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteGfsCodeCategoryService', function ($resource) {
  return $resource('/json/deleteGfsCodeCategory/:gfs_code_category_id', {
    gfs_code_category_id: '@gfs_code_category_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
//End GFS CODE CATEGORY CRUD

//Services for FACILITY CRUD
setupServices.factory('GetAllFacilityService', function ($resource) {
  return $resource('/json/paginatedFacilities', {}, {});
});

setupServices.factory('CreateFacilityService', function ($resource) {
  return $resource('/json/createFacility', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateFacilityService', function ($resource) {
  return $resource('/json/updateFacility', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteFacilityService', function ($resource) {
  return $resource('/json/deleteFacility/:facility_id', {
    facility_id: '@facility_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});


//End FACILITY CRUD

setupServices.factory('LoadSectionsService', function ($resource) {
  return $resource('/json/loadSections/:SectionLevelId', {
    SectionLevelId: '@SectionLevelId'
  }, {});
});
setupServices.factory('LoadVillageStreetService', function ($resource) {
  return $resource('/json/loadSections/:SectionLevelId', {
    SectionLevelId: '@SectionLevelId'
  }, {});
});
//The services for the facility CRUD ends here

//The services for the gfs code CRUD start here
setupServices.factory('CreateGfsCode', function ($resource) {
  return $resource('/json/createGfsCode', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('AllGfsCodeService', function ($resource) {
  return $resource('/json/gfsCodes', {}, {});
});
services.factory('RevenueGfsCodeService', function ($resource) {
  return $resource('/json/revenueGfsCodes', {}, {
    byFundSourceVersion: {
      url: '/json/revenueGfsCodes/:fundSourceVersionId/:financialYearId',
      params: {
        fundSourceVersionId: '@fundSourceVersionId',
        financialYearId: '@financialYearId'
      }
    }
  });
});

setupServices.factory('PaginateGfsCodeService', function ($resource) {
  return $resource('/json/paginateGfsCodes', {}, {});
});
setupServices.factory('PaginateGfsCodeMappingService', function ($resource) {
  return $resource('/json/GfsCode-mapping', {}, {
    store: {
      method: 'POST'
    },
    getMappedGFS: {
      method: 'GET',
      url: '/json/get-GfsCode-mapping'
    },
  });
});
setupServices.factory('UpdateGfsCodeService', function ($resource) {
  return $resource('/json/updateGfsCode', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteIfrsGfsCodeService', function ($resource) {
  return $resource('/json/deleteIfrsGfsCode/:gfsCodeId', {
    gfsCodeId: '@gfsCodeId'
  }, {
    delete: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteGfsCodeService', function ($resource) {
  return $resource('/json/deleteGfsCode/:gfsCodeId', {
    gfsCodeId: '@gfsCodeId'
  }, {
    delete: {
      method: 'POST'
    }
  });
});
setupServices.factory('LoadAccountTypeService', function ($resource) {
  return $resource('/json/loadGfsCodeAccountTypes', {}, {});
});
setupServices.factory('loadGfsCodeCategoriesService', function ($resource) {
  return $resource('/json/loadGfsCodeCategories', {}, {});
});
setupServices.factory('ActivateGfsCodeService', function ($resource) {
  return $resource('/json/activateGfsCode', {}, {
    activateGfsCode: {
      method: 'POST'
    }
  });
});
setupServices.factory('ActivateIsProcurementService', function ($resource) {
  return $resource('/json/activateIsProcurement', {}, {
    activateIsProcurement: {
      method: 'POST'
    }
  });
});
//The services for the gfs code CRUD ends here

//Start Generic Template CRUD
setupServices.factory('UpdateGenericTemplateService', function ($resource) {
  return $resource('/json/updateGenericTemplate', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('GetGenericTemplateService', function ($resource) {
  return $resource('/json/getByPlanChainId/:plan_chain_id', {
    plan_chain_id: '@plan_chain_id'
  }, {});
});
setupServices.factory('CreateGenericTemplateService', function ($resource) {
  return $resource('/json/createGenericTemplate', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('ParentGenericTemplates', function ($resource) {
  return $resource('/json/getParentByPlanChainId/:plan_chain_id/:childTemplateId', {
    plan_chain_id: '@plan_chain_id',
    childTemplateId: '@childTemplateId'
  }, {});
});
//End Generic Template CRUD

//Services for Plan Chain Type Levels CRUD
setupServices.factory('PaginatedPlanChainTypeService', function ($resource) {
  return $resource('/json/paginatedPlanChainTypes', {}, {});
});

setupServices.factory('CreatePlanChainTypeService', function ($resource) {
  return $resource('/json/createPlanChainType', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdatePlanChainTypeService', function ($resource) {
  return $resource('/json/updatePlanChainType', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeletePlanChainTypeService', function ($resource) {
  return $resource('/json/deletePlanChainType/:planChainTypeId', {
    planChainTypeId: '@planChainTypeId'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
setupServices.factory('ActivatePlanChainTypeService', function ($resource) {
  return $resource('/json/activatePlanChainType', {}, {
    activate: {
      method: 'POST'
    }
  });
});
setupServices.factory('LoadParentPlanChainService', function ($resource) {
  return $resource('/json/loadParentPlanChain/:childId/:financialYearId/:versionId', {
    childId: '@childId',
    financialYearId: '@financialYearId',
    versionId: '@versionId',
  }, {});
});

setupServices.factory('PaginatePlanChainService', function ($resource) {
  return $resource('/json/paginatePlanChain', {}, {
    paginateIndex: {
      method: 'GET'
    }
  });
});
//End Plan Chain Types CRUD

//Services for Projects  CRUD
setupServices.factory('PaginatedProjectService', function ($resource) {
  return $resource('/json/paginatedProjects', {}, {});
});

setupServices.factory('CreateProjectService', function ($resource) {
  return $resource('/json/createProject', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateProjectService', function ($resource) {
  return $resource('/json/updateProject', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteProjectService', function ($resource) {
  return $resource('/json/deleteProject/:projectId', {
    projectId: '@projectId'
  }, {
    delete: {
      method: 'POST'
    }
  });
});
//End Fund Projects CRUD

//Services for Fund Source Category CRUD
setupServices.factory('PaginatedFundSourceCategoryService', function ($resource) {
  return $resource('/json/paginatedFundSourceCategories', {}, {});
});

setupServices.factory('CreateFundSourceCategoryService', function ($resource) {
  return $resource('/json/createFundSourceCategory', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateFundSourceCategoryService', function ($resource) {
  return $resource('/json/updateFundSourceCategory', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteFundSourceCategoryService', function ($resource) {
  return $resource('/json/deleteFundSourceCategory/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'POST'
    }
  });
});
setupServices.factory('RestoreFundSourceCategoryService', function ($resource) {
  return $resource('/json/restoreFundSourceCategory/:id', {
    id: '@id'
  }, {
    restoreFundSourceCategory: {
      method: 'GET'
    }
  });
});
setupServices.factory('PermanentDeleteFundSourceCategoryService', function ($resource) {
  return $resource('/json/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});
//End Fund Source Category CRUD

//Services for Fund Source CRUD

setupServices.factory('CreateFundSourceService', function ($resource) {
  return $resource('/json/createFundSource', {}, {
    store: {
      method: 'POST'
    }
  });
});


setupServices.factory('FundSourceBudgetClassesService', function ($resource) {
  return $resource('/json/fundSourceBudgetClasses/:fund_source_id', {
    fund_source_id: '@fund_source_id'
  }, {});
});

setupServices.factory('FundSourceRevenueCodesService', function ($resource) {
  return $resource('/json/fundSourceRevenueCodes/:fund_source_id', {
    fund_source_id: '@fund_source_id'
  }, {});
});

setupServices.factory('AddFundSourceCeilingSectorsService', function ($resource) {
  return $resource('/json/addCeilingSectors', {}, {
    store_sectors: {
      method: 'POST'
    }
  });
});

setupServices.factory('EditFundSourceCeilingSectorsService', function ($resource) {
  return $resource('/json/editCeilingSectors', {}, {
    store_sectors: {
      method: 'POST'
    }
  });
});

setupServices.factory('CeilingSectorsService', function ($resource) {
  return $resource('/json/ceilingSectors/:fund_source_id', {
    fund_source_id: '@fund_source_id'
  }, {}); //This get all sectors;
});

setupServices.factory('CeilingGfsCodesService', function ($resource) {
  return $resource('/json/ceilingGfsCodes/:fund_source_id', {
    fund_source_id: '@fund_source_id'
  }, {}); //This get all sectors;
});

setupServices.factory('AddFundSourceRevenueCodesService', function ($resource) {
  return $resource('/json/addFundSourceRevenueCodes', {}, {
    store_revenue_codes: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateFundSourceService', function ($resource) {
  return $resource('/json/updateFundSource', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteFundSourceService', function ($resource) {
  return $resource('/json/deleteFundSource/:fundSourceId/:financialYearId/:versionId', {
    fundSourceId: '@fundSourceId',
    financialYearId: '@financialYearId',
    versionId: '@versionId',
  }, {
    delete: {
      method: 'POST'
    }
  });
});
setupServices.factory('LoadParentFundSourcesService', function ($resource) {
  return $resource('/json/loadParentFundSources/:childFundSourceCategoryId', {
    childFundSourceCategoryId: '@childFundSourceCategoryId'
  }, {});
});
//End Fund Source CRUD

setupServices.factory('GetAllAccountTypeService', function ($resource) {
  return $resource('/json/paginatedAccountTypes', {}, {});
});

setupServices.factory('GetAllPeriodService', function ($resource) {
  return $resource('/json/paginatedPeriods', {}, {});
});

setupServices.factory('CreateRoleService', function ($resource) {
  return $resource('/json/createRole', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateRoleService', function ($resource) {
  return $resource('/json/updateRole', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('UsersService', function ($resource) {
  return $resource('/json/users', {}, {
    activateUser: {
      method: 'PUT',
      url: '/json/activateUser/:userId',
      params: {
        userId: '@userId'
      }
    },
    deActivateUser: {
      method: 'PUT',
      url: '/json/deActivateUser/:userId',
      params: {
        userId: '@userId'
      }
    },
    getUserEvents: {
      method: 'GET',
      url: '/json/user/get-events/:userId',
      params: {
        userId: '@userId'
      }
    },
    addUserFacility: {
      method: 'PUT',
      url: '/json/user/add-facility/:userId/:facilityId',
      params: {
        userId: '@userId',
        facilityId: '@facilityId'
      }
    },
    removeUserFacility: {
      method: 'PUT',
      url: '/json/user/remove-facility/:userId/:facilityId',
      params: {
        userId: '@userId',
        facilityId: '@facilityId'
      }
    }
  });
});

setupServices.factory('CreateUserService', function ($resource) {
  return $resource('/json/createUser', {}, {
    store: {
      method: 'POST'
    },
    validateCheckNumber: {
      method: 'GET',
      url: 'json/users/validateChequeNumber/:userId/:chequesNumber',
      params: {
        chequesNumber: '@chequesNumber',
        userId: '@userId'
      }
    },
    validateEmail: {
      method: 'GET',
      url: 'json/users/validateEmail/:userId/:email',
      params: {
        email: '@email',
        userId: '@userId'
      }
    },
    validateUserName: {
      method: 'GET',
      url: '/json/users/validateUserName/:userId/:userName',
      params: {
        userName: '@userName',
        userId: '@userId'
      }
    }
  });
});
setupServices.factory('UpdateUserService', function ($resource) {
  return $resource('/json/updateUser', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('AdminHierarchiesByLevel', function ($resource) {
  return $resource('/json/adminHierarchiesByLevel/:levelId', {
    levelId: '@levelId'
  }, {});
});

setupServices.factory('ToggleFinancialYearService', function ($resource) {
  return $resource('/json/toggleFinancialYear', {}, {
    toggleFinancialYear: {
      method: 'POST'
    }
  });
});

setupServices.factory('GetGenericTemplateService', function ($resource) {
  return $resource('/json/getByPlanChainId/:plan_chain_id', {
    plan_chain_id: '@plan_chain_id'
  }, {});
});

setupServices.factory('GetAllProjectionService', function ($resource) {
  return $resource('/json/paginatedProjections', {}, {});
});

setupServices.factory('CreateProjectionService', function ($resource) {
  return $resource('/json/createProjection', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateProjectionService', function ($resource) {
  return $resource('/json/updateProjection', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteProjectionService', function ($resource) {
  return $resource('/json/deleteProjection/:projection_id', {
    projection_id: '@projection_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('SectorsPaginationService', function ($resource) {
  return $resource('/json/sectorsPagination', {}, {}); //This get all sectors;
});
setupServices.factory('CreateSectorService', function ($resource) {
  return $resource('/json/createSector', {}, {
    store: {
      method: 'POST'
    }
  }); //This create sector and return existing sectors after creating
});

setupServices.factory('UpdateSectorService', function ($resource) {
  return $resource('/json/updateSector', {}, {
    update: {
      method: 'POST'
    }
  }); //This create sector and return existing sectors after creating
});

//The services to control the reference documents end here

//The services to control the reference documents begin here
setupServices.factory('PaginateReferenceDocumentTypesService', function ($resource) {
  return $resource('/json/paginateReferenceDocumentTypes', {}, {}); //This gets all reference documents as an object for pagination
});
setupServices.factory('CreateReferenceDocumentTypesService', function ($resource) {
  return $resource('/json/createReferenceDocumentType', {}, {
    store: {
      method: 'POST'
    }
  }); //This creates the reference documents
});
setupServices.factory('UpdateReferenceDocumentTypesService', function ($resource) {
  return $resource('/json/updateReferenceDocumentType/:id', {
    id: '@id'
  }, {
    update: {
      method: 'POST'
    }
  }); //This updates the reference documents
});
setupServices.factory('DeleteReferenceDocumentTypesService', function ($resource) {
  return $resource('/json/deleteReferenceDocumentType/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'POST'
    }
  }); //This deletes the reference documents
});
//The services to control the reference documents end here

setupServices.factory('DeleteSectorService', function ($resource) {
  return $resource('/json/deleteSector/:sectorId', {
    sectorId: '@sectorId'
  }, {
    delete: {
      method: 'POST'
    }
  });
});

//SECTION PE ITEM CATEGORIES
setupServices.factory('GetAllPeItemCategoryService', function ($resource) {
  return $resource('/json/paginatedPEItemCategories', {}, {});
});

setupServices.factory('CreatePeItemCategoryService', function ($resource) {
  return $resource('/json/createPeItemCategory', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdatePeItemCategoryService', function ($resource) {
  return $resource('/json/updatePeItemCategory', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeletePeItemCategoryService', function ($resource) {
  return $resource('/json/deletePeItemCategory/:pe_item_category_id', {
    pe_item_category_id: '@pe_item_category_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('getLongTermTargetByRefDocIdService', function ($resource) {
  return $resource('/json/getLongTermTargetByRefDocId/:doc_Id', {
    doc_Id: '@doc_Id'
  }, {
    getLongTermTargetByRefDocId: {
      method: 'GET'
    }
  });
});
setupServices.factory('ConfigurationsService', function ($resource) {
  return $resource('/json/configurations', {}, {});
});
setupServices.factory('SaveConfigurationService', function ($resource) {
  return $resource('/json/updateConfigurations', {}, {
    update: {
      method: 'POST'
    }
  });
});


setupServices.factory('UpdateLongTermTargetService', function ($resource) {
  return $resource('/json/updateLongTermTarget', {}, {
    update: {
      method: 'POST'
    }
  }); //This create sector and return existing sectors after creating
});

setupServices.factory('DeleteLongTermTargetService', function ($resource) {
  return $resource('/json/deleteLongTermTarget/:longTermTargetId', {
    longTermTargetId: '@longTermTargetId'
  }, {
    delete: {
      method: 'POST'
    }
  });
});

setupServices.factory('LongTermTargetsPaginationService', function ($resource) {
  return $resource('/json/longTermTargetsPagination', {}, {}); //This get all sectors;
});
//TARGET TYPES
setupServices.factory('PaginatedTargetTypeService', function ($resource) {
  return $resource('/json/paginatedTargetTypes', {}, {});
});

setupServices.factory('CreateTargetTypeService', function ($resource) {
  return $resource('/json/createTargetType', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateTargetTypeService', function ($resource) {
  return $resource('/json/updateTargetType', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteTargetTypeService', function ($resource) {
  return $resource('/json/deleteTargetType/:target_type_id', {
    target_type_id: '@target_type_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
setupServices.factory('ToggleTargetTypeService', function ($resource) {
  return $resource('/json/toggleTargetType', {}, {
    toggleTargetType: {
      method: 'POST'
    }
  });
});
setupServices.factory('CreateLongTermTargetService', function ($resource) {
  return $resource('/json/createLongTermTarget', {}, {
    store: {
      method: 'POST'
    }
  }); //This create sector and return existing sectors after creating
});

// ADMIN HIERARCHY PROJECTS
setupServices.factory('PaginatedAdminHierarchyProjectService', function ($resource) {
  return $resource('/json/paginatedAdminHierarchyProjects', {}, {});
});
setupServices.factory('CreateAdminHierarchyProjectService', function ($resource) {
  return $resource('/json/createAdminHierarchyProject', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateAdminHierarchyProjectService', function ($resource) {
  return $resource('/json/updateAdminHierarchyProject', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteAdminHierarchyProjectService', function ($resource) {
  return $resource('/json/deleteAdminHierarchyProject/:admin_hierarchy_project_id', {
    admin_hierarchy_project_id: '@admin_hierarchy_project_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});


// SECTOR PROJECTS
setupServices.factory('PaginatedSectorProjectService', function ($resource) {
  return $resource('/json/paginatedSectorProjects', {}, {});
});
setupServices.factory('CreateSectorProjectService', function ($resource) {
  return $resource('/json/createSectorProject', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateSectorProjectService', function ($resource) {
  return $resource('/json/updateSectorProject', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteSectorProjectService', function ($resource) {
  return $resource('/json/deleteSectorProject/:sector_project_id', {
    sector_project_id: '@sector_project_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

//FINANCIAL YEAR PERIODS
setupServices.factory('AllFinancialYearPeriodService', function ($resource) {
  return $resource('/json/allFinancialYearPeriods/:financial_year_id', {
    financial_year_id: '@financial_year_id'
  }, {
    periods: {
      method: 'GET'
    }
  });
});

setupServices.factory('AddFinancialYearPeriodService', function ($resource) {
  return $resource('/json/addFinancialYearPeriod', {}, {
    add_period: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteFinancialYearPeriod', function ($resource) {
  return $resource('/json/deleteFinancialYearPeriod/:period_id/:financial_year_id', {
    period_id: '@period_id',
    financial_year_id: '@financial_year_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('PlanChainSectorOnlyService', function ($resource) {
  return $resource('/json/planChainSectorsOnly', {}, {
    store: {
      method: 'POST'
    }
  });
});

//Financial Years - trash implementation
setupServices.factory('TrashedFinancialYearService', function ($resource) {
  return $resource('/json/financial_years/trashed', {}, {});
});

setupServices.factory('RestoreFinancialYearService', function ($resource) {
  return $resource('/json/financial_year/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyFinancialYearTrashService', function ($resource) {
  return $resource('/json/financial_year/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteFinancialYearService', function ($resource) {
  return $resource('/json/financial_year/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});


//Account Types - trash implementation
setupServices.factory('TrashedAccountTypeService', function ($resource) {
  return $resource('/json/account_types/trashed', {}, {});
});

setupServices.factory('RestoreAccountTypeService', function ($resource) {
  return $resource('/json/account_type/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyAccountTypeTrashService', function ($resource) {
  return $resource('/json/account_type/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteAccountTypeService', function ($resource) {
  return $resource('/json/account_type/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//Financial Year Periods - trash implementation
setupServices.factory('TrashedPeriodService', function ($resource) {
  return $resource('/json/periods/trashed', {}, {});
});

setupServices.factory('RestorePeriodService', function ($resource) {
  return $resource('/json/period/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyPeriodTrashService', function ($resource) {
  return $resource('/json/period/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeletePeriodService', function ($resource) {
  return $resource('/json/period/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//Admin Hierarchy Level - trash implementation
setupServices.factory('DeleteAdminHierarchyLevelService', function ($resource) {
  return $resource('/json/deleteAdminHierarchyLevel/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedAdminHierarchyLevelService', function ($resource) {
  return $resource('/json/admin_hierarchy_levels/trashed', {}, {});
});

setupServices.factory('RestoreAdminHierarchyLevelService', function ($resource) {
  return $resource('/json/admin_hierarchy_level/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyAdminHierarchyLevelTrashService', function ($resource) {
  return $resource('/json/admin_hierarchy_level/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteAdminHierarchyLevelService', function ($resource) {
  return $resource('/json/admin_hierarchy_level/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

setupServices.factory('LoadSectionServiceByPiscs', function ($resource) {
  return $resource('/json/section_by_admin/filter/:section_level_id/:admin_id', {
    section_level_id: '@section_level_id',
    admin_id: '@admin_id'
  }, {});
});
setupServices.factory('LoadSectionService', function ($resource) {
  return $resource('/json/section/filter/:section_level_id/:sector_id', {
    section_level_id: '@section_level_id',
    sector_id: '@sector_id'
  }, {});
});

//Facility Type - trash implementation
setupServices.factory('TrashedFacilityTypeService', function ($resource) {
  return $resource('/json/facility_types/trashed', {}, {});
});

setupServices.factory('RestoreFacilityTypeService', function ($resource) {
  return $resource('/json/facility_type/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyFacilityTypeTrashService', function ($resource) {
  return $resource('/json/facility_type/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteFacilityTypeService', function ($resource) {
  return $resource('/json/facility_type/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//PRIORITY AREAS
setupServices.factory('PaginatedPriorityAreaService', function ($resource) {
  return $resource('/json/priority_areas/paginated', {}, {});
});

setupServices.factory('CreatePriorityAreaService', function ($resource) {
  return $resource('/json/priority_area/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdatePriorityAreaService', function ($resource) {
  return $resource('/json/priority_area/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('TogglePriorityAreaService', function ($resource) {
  return $resource('/json/priority_area/toggle', {}, {
    toggleActive: {
      method: 'POST'
    }
  });
});

//Intervention Category
setupServices.factory('PaginatedInterventionCategoryService', function ($resource) {
  return $resource('/json/intervention_categories/paginated', {}, {});
});

setupServices.factory('CreateInterventionCategoryService', function ($resource) {
  return $resource('/json/intervention_category/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateInterventionCategoryService', function ($resource) {
  return $resource('/json/intervention_category/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteInterventionCategoryService', function ($resource) {
  return $resource('/json/intervention_category/delete/:intervention_category_id', {
    intervention_category_id: '@intervention_category_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

// cas plans
setupServices.factory('CreateCsaPlanService', function ($resource) {
  return $resource('/json/createCasPlan', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateCasPlanService', function ($resource) {
  return $resource('/json/updateCasPlan', {}, {
    edit: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteCasPlanService', function ($resource) {
  return $resource('/json/deleteCasPlan/:casPlanId', {
    casPlanId: '@casPlanId'
  }, {
    delete: {
      method: 'GET'
    }
  });
});


//end cas plans

//Start cas plan tables
setupServices.factory('CasPlanTablePaginationService', function ($resource) {
  return $resource('/json/casPlanTableIndex', {}, {
    cas_plan_id: '@cas_plan_id',
    page: '@page',
    perPage: '@perPage',
    indexPaginated: {
      method: 'GET'
    }
  });
});
setupServices.factory('CasPlanTableNamesService', function ($resource) {
  return $resource('/json/casPlanTablesSingle', {}, {
    index: {
      method: 'GET'
    }
  });
});
setupServices.factory('GetAllCasPlanTablesService', function ($resource) {
  return $resource('/json/getAllCasPlanTables', {}, {
    index: {
      method: 'GET'
    }
  });
});
setupServices.factory('CreateCasPlanTableService', function ($resource) {
  return $resource('/json/createCasPlanTable', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateCasPlanTableService', function ($resource) {
  return $resource('/json/updateCasPlanTable', {
    params: '@params'
  }, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteCasPlanTableService', function ($resource) {
  return $resource('/json/deleteCasPlanTable/:id/:cas_plan_id', {
    id: '@id',
    cas_plan_id: '@cas_plan_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
setupServices.factory('CasPlanTableColumnsService', function ($resource) {
  return $resource('/json/casPlanTableColumn/:table_id', {
    table_id: '@table_id'
  }, {
    getCasPlanTableColumnsByTableId: {
      method: 'GET'
    }
  });
});

setupServices.factory('CasPlanTableSearchByNameService', function ($resource) {
  return $resource('/json/casPlanTableSearchByName', {
    searchQuery: '@searchQuery'
  }, {
    searchByName: {
      method: 'GET'
    }
  });
});
//End cas plan tables

//SECTOR PROBLEMS
setupServices.factory('PaginatedSectorProblemService', function ($resource) {
  return $resource('/json/sector_problems/paginated', {}, {});
});

setupServices.factory('CreateSectorProblemService', function ($resource) {
  return $resource('/json/sector_problem/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateSectorProblemService', function ($resource) {
  return $resource('/json/sector_problem/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('ToggleSectorProblemService', function ($resource) {
  return $resource('/json/sector_problem/toggleActive', {}, {
    toggleActive: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteSectorProblemService', function ($resource) {
  return $resource('/json/sector_problem/delete/:sector_problem_id', {
    sector_problem_id: '@sector_problem_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedSectorProblemService', function ($resource) {
  return $resource('/json/sector_problems/trashed', {}, {});
});

setupServices.factory('RestoreSectorProblemService', function ($resource) {
  return $resource('/json/sector_problem/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptySectorProblemTrashService', function ($resource) {
  return $resource('/json/sector_problems/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteSectorProblemService', function ($resource) {
  return $resource('/json/sector_problem/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//ADMIN HIERARCHY GFS CODE
setupServices.factory('PaginatedAdminHierarchyGfsCodeService', function ($resource) {
  return $resource('/json/admin_hierarchy_gfs_codes/paginated', {}, {});
});

setupServices.factory('CreateAdminHierarchyGfsCodeService', function ($resource) {
  return $resource('/json/admin_hierarchy_gfs_code/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteAdminHierarchyGfsCodeService', function ($resource) {
  return $resource('/json/admin_hierarchy_gfs_code/delete/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

//SECTION GFS CODES
setupServices.factory('PaginatedSectionGfsCodeService', function ($resource) {
  return $resource('/json/section_gfs_codes/paginated', {}, {});
});

setupServices.factory('CreateSectionGfsCodeService', function ($resource) {
  return $resource('/json/section_gfs_code/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteSectionGfsCodeService', function ($resource) {
  return $resource('/json/section_gfs_code/delete/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

// cas plan contents
setupServices.factory('CreateCasPlanContentService', function ($resource) {
  return $resource('/json/createCasPlanContent', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateCasPlanContentService', function ($resource) {
  return $resource('/json/updateCasPlanContent', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteCasPlanContentService', function ($resource) {
  return $resource('/json/deleteCasPlanContent/:id', {
    casPlanId: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

//end cas plan contents

setupServices.factory('FundSourceCeilingsService', function ($resource) {
  return $resource('/json/fundSourceCeilings/:fundSourceId', {
    fundSourceId: '@fundSourceId'
  }, {});
});


setupServices.factory('SetFundSourceCanProjectService', function ($resource) {
  return $resource('/json/fund_source/canProject/:id', {
    id: '@id'
  }, {
    canProject: {
      method: 'GET'
    }
  });
});

//Cas plan table columns begin here
setupServices.factory('CreateCasPlanTableColumnService', function ($resource) {
  return $resource('/json/createCasPlanTableColumn', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteCasPlanTableColumnService', function ($resource) {
  return $resource('/json/deleteCasPlanTableColumn/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
setupServices.factory('UpdateCasPlanTableColumnService', function ($resource) {
  return $resource('/json/updateCasPlanTableColumn', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('casPlanTableColumnFormulaService', function ($resource) {
  return $resource('/json/casPlanTableColumnFormula', {}, {
    updateFormula: {
      method: 'POST'
    }
  });
});
setupServices.factory('CasPlanTableColumnPaginationService', function ($resource) {
  return $resource('/json/paginatedCasPlanTableColumn', {}, {
    paginateIndex: {
      method: 'GET'
    }
  });
});

//Cas plan table columns end here

//cas plan table items begin here
setupServices.factory('CasPlanTableItemsService', function ($resource) {
  return $resource('/json/casPlanTableItems', {}, {});
});
setupServices.factory('CreateCasPlanTableItemService', function ($resource) {
  return $resource('/json/CreateCasPlanTableItem', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateCasPlanTableItemService', function ($resource) {
  return $resource('/json/UpdateCasPlanTableItem', {}, {
    edit: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteCasPlanTableItemService', function ($resource) {
  return $resource('/json/DeleteCasPlanTableItem/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

//table items end here

setupServices.factory('SearchGfsCodeService', function ($resource) {
  return $resource('/json/gfs_codes/search', {}, {});
});

setupServices.factory('LastFinancialYear', function ($resource) {
  return $resource('/json/lastFinancialYear', {}, {});
});
//cas plan table item values
setupServices.factory('CasPlanTableItemValueService', function ($resource) {
  return $resource('/json/CasPlanTableItemValue/:cas_plan_table_id', {
    cas_plan_table_id: '@cas_plan_table_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
//end plan table item values


//LGA LEVELS
setupServices.factory('PaginatedLgaLevelService', function ($resource) {
  return $resource('/json/lga_levels/paginated', {}, {});
});

setupServices.factory('CreateLgaLevelService', function ($resource) {
  return $resource('/json/lga_level/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateLgaLevelService', function ($resource) {
  return $resource('/json/lga_level/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteLgaLevelService', function ($resource) {
  return $resource('/json/lga_level/delete/:lga_level_id', {
    lga_level_id: '@lga_level_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedLgaLevelService', function ($resource) {
  return $resource('/json/lga_levels/trashed', {}, {});
});

setupServices.factory('RestoreLgaLevelService', function ($resource) {
  return $resource('/json/lga_level/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyLgaLevelTrashService', function ($resource) {
  return $resource('/json/lga_levels/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteLgaLevelService', function ($resource) {
  return $resource('/json/lga_level/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//Cas Plan Table Group Types Start Here
setupServices.factory('CasPlanTableGroupTypesService', function ($resource) {
  return $resource('/json/GetCasPlanTableGroupTypes', {}, {
    index: {
      method: 'GET'
    }
  });
});
setupServices.factory('PaginateCasPlanTableGroupTypesService', function ($resource) {
  return $resource('/json/PaginatedCasPlanTableGroupTypes', {}, {
    paginateIndex: {
      method: 'GET'
    }
  });
});
setupServices.factory('CreateCasGroupTypeService', function ($resource) {
  return $resource('/json/CreateCasGroupType', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateCasGroupTypeService', function ($resource) {
  return $resource('/json/UpdateCasGroupType', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteCasGroupTypeService', function ($resource) {
  return $resource('/json/DeleteCasGroupType/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
//Cas Plan Table Group Types End Here

//Cas Plan Table Group Columns Start Here
setupServices.factory('CasGroupColumnsService', function ($resource) {
  return $resource('/json/GetCasGroupColumns', {}, {
    index: {
      method: 'GET'
    }
  });
});
setupServices.factory('PaginateCasGroupColumnsService', function ($resource) {
  return $resource('/json/PaginateCasGroupColumn', {}, {
    paginateIndex: {
      method: 'GET'
    }
  });
});
setupServices.factory('CreateCasGroupColumnsService', function ($resource) {
  return $resource('/json/CreateCasGroupColumn', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateCasGroupColumnsService', function ($resource) {
  return $resource('/json/UpdateCasGroupColumn', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteCasGroupColumnsService', function ($resource) {
  return $resource('/json/DeleteCasGroupColumn/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
//Cas Plan Table Group Columns End Here


//Cas group columns start here
setupServices.factory('GetCasGroupColumnByGroupTypeService', function ($resource) {
  return $resource('/json/GetCasGroupColumnByType/:group_type_id', {
    group_type_id: '@group_type_id'
  }, {
    method: 'GET'
  });
});
//Cas group columns end here

//link levels
setupServices.factory('PaginatedLinkLevelService', function ($resource) {
  return $resource('/json/link_levels/paginated', {}, {});
});

setupServices.factory('CreateLinkLevelService', function ($resource) {
  return $resource('/json/link_level/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateLinkLevelService', function ($resource) {
  return $resource('/json/link_level/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteLinkLevelService', function ($resource) {
  return $resource('/json/link_level/delete/:link_level_id', {
    link_level_id: '@link_level_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedLinkLevelService', function ($resource) {
  return $resource('/json/link_levels/trashed', {}, {});
});

setupServices.factory('RestoreLinkLevelService', function ($resource) {
  return $resource('/json/link_level/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyLinkLevelTrashService', function ($resource) {
  return $resource('/json/link_levels/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteLinkLevelService', function ($resource) {
  return $resource('/json/link_level/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TruncateLinkLevelService', function ($resource) {
  return $resource('/json/link_levels/truncateData', {}, {});
});

//TASK NATURE
setupServices.factory('PaginatedTaskNatureService', function ($resource) {
  return $resource('/json/task_natures/paginated', {}, {});
});

setupServices.factory('CreateTaskNatureService', function ($resource) {
  return $resource('/json/task_nature/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateTaskNatureService', function ($resource) {
  return $resource('/json/task_nature/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteTaskNatureService', function ($resource) {
  return $resource('/json/task_nature/delete/:task_nature_id', {
    task_nature_id: '@task_nature_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedTaskNatureService', function ($resource) {
  return $resource('/json/task_natures/trashed', {}, {});
});

setupServices.factory('RestoreTaskNatureService', function ($resource) {
  return $resource('/json/task_nature/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyTaskNatureTrashService', function ($resource) {
  return $resource('/json/task_natures/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteTaskNatureService', function ($resource) {
  return $resource('/json/task_nature/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//EXPENDITURE CENTRE GROUP
setupServices.factory('PaginatedExpenditureCentreGroupService', function ($resource) {
  return $resource('/json/expenditure_centre_groups/paginated', {}, {});
});

setupServices.factory('CreateExpenditureCentreGroupService', function ($resource) {
  return $resource('/json/expenditure_centre_group/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateExpenditureCentreGroupService', function ($resource) {
  return $resource('/json/expenditure_centre_group/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteExpenditureCentreGroupService', function ($resource) {
  return $resource('/json/expenditure_centre_group/delete/:expenditure_centre_group_id', {
    expenditure_centre_group_id: '@expenditure_centre_group_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedExpenditureCentreGroupService', function ($resource) {
  return $resource('/json/expenditure_centre_groups/trashed', {}, {});
});

setupServices.factory('RestoreExpenditureCentreGroupService', function ($resource) {
  return $resource('/json/expenditure_centre_group/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyExpenditureCentreGroupTrashService', function ($resource) {
  return $resource('/json/expenditure_centre_groups/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteExpenditureCentreGroupService', function ($resource) {
  return $resource('/json/expenditure_centre_group/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//EXPENDITURE CENTRE
setupServices.factory('PaginatedExpenditureCentreService', function ($resource) {
  return $resource('/json/expenditure_centres/paginated', {}, {});
});

setupServices.factory('CreateExpenditureCentreService', function ($resource) {
  return $resource('/json/expenditure_centre/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateExpenditureCentreService', function ($resource) {
  return $resource('/json/expenditure_centre/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteExpenditureCentreService', function ($resource) {
  return $resource('/json/expenditure_centre/delete/:expenditure_centre_id', {
    expenditure_centre_id: '@expenditure_centre_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedExpenditureCentreService', function ($resource) {
  return $resource('/json/expenditure_centres/trashed', {}, {});
});

setupServices.factory('RestoreExpenditureCentreService', function ($resource) {
  return $resource('/json/expenditure_centre/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyExpenditureCentreTrashService', function ($resource) {
  return $resource('/json/expenditure_centres/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteExpenditureCentreService', function ($resource) {
  return $resource('/json/expenditure_centre/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//BUDGET DISTRIBUTION CONDITIONS MAIN SECTOR GROUPS
setupServices.factory('PaginatedBdcMainGroupService', function ($resource) {
  return $resource('/json/bdc_main_groups/paginated', {}, {});
});

setupServices.factory('CreateBdcMainGroupService', function ($resource) {
  return $resource('/json/bdc_main_group/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateBdcMainGroupService', function ($resource) {
  return $resource('/json/bdc_main_group/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteBdcMainGroupService', function ($resource) {
  return $resource('/json/bdc_main_group/delete/:bdc_main_group_id', {
    bdc_main_group_id: '@bdc_main_group_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedBdcMainGroupService', function ($resource) {
  return $resource('/json/bdc_main_groups/trashed', {}, {});
});

setupServices.factory('RestoreBdcMainGroupService', function ($resource) {
  return $resource('/json/bdc_main_group/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyBdcMainGroupTrashService', function ($resource) {
  return $resource('/json/bdc_main_groups/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteBdcMainGroupService', function ($resource) {
  return $resource('/json/bdc_main_group/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//BUDGET DISTRIBUTION CONDITIONS SECTOR GROUPS
setupServices.factory('PaginatedBdcGroupService', function ($resource) {
  return $resource('/json/bdc_groups/paginated', {}, {});
});

setupServices.factory('CreateBdcGroupService', function ($resource) {
  return $resource('/json/bdc_group/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateBdcGroupService', function ($resource) {
  return $resource('/json/bdc_group/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteBdcGroupService', function ($resource) {
  return $resource('/json/bdc_group/delete/:bdc_group_id', {
    bdc_group_id: '@bdc_group_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedBdcGroupService', function ($resource) {
  return $resource('/json/bdc_groups/trashed', {}, {});
});

setupServices.factory('RestoreBdcGroupService', function ($resource) {
  return $resource('/json/bdc_group/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyBdcGroupTrashService', function ($resource) {
  return $resource('/json/bdc_groups/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteBdcGroupService', function ($resource) {
  return $resource('/json/bdc_group/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//EXPENDITURE CENTRE GFS CODES
setupServices.factory('PaginatedExpenditureCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditure_centre_gfs_codes/paginated', {}, {});
});

setupServices.factory('CreateExpenditureCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditure_centre_gfs_code/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateExpenditureCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditure_centre_gfs_code/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteExpenditureCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditure_centre_gfs_code/delete/:expenditure_centre_gfs_code_id', {
    expenditure_centre_gfs_code_id: '@expenditure_centre_gfs_code_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedExpenditureCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditure_centre_gfs_codes/trashed', {}, {});
});

setupServices.factory('RestoreExpenditureCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditure_centre_gfs_code/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyExpenditureCentreGfsCodeTrashService', function ($resource) {
  return $resource('/json/expenditure_centre_gfs_codes/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteExpenditureCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditure_centre_gfs_code/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//SECTOR EXPENDITURE CENTRE SUB CENTRES
setupServices.factory('PaginatedSectorExpenditureSubCentreService', function ($resource) {
  return $resource('/json/sectorExpenditureSubCentres/paginated', {}, {});
});

setupServices.factory('CreateSectorExpenditureSubCentreService', function ($resource) {
  return $resource('/json/sectorExpenditureSubCentre/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateSectorExpenditureSubCentreService', function ($resource) {
  return $resource('/json/sectorExpenditureSubCentre/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteSectorExpenditureSubCentreService', function ($resource) {
  return $resource('/json/sectorExpenditureSubCentre/delete/:sectorExpenditureSubCentre_id', {
    sectorExpenditureSubCentre_id: '@sectorExpenditureSubCentre_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedSectorExpenditureSubCentreService', function ($resource) {
  return $resource('/json/sectorExpenditureSubCentres/trashed', {}, {});
});

setupServices.factory('RestoreSectorExpenditureSubCentreService', function ($resource) {
  return $resource('/json/sectorExpenditureSubCentre/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptySectorExpenditureSubCentreTrashService', function ($resource) {
  return $resource('/json/sectorExpenditureSubCentres/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteSectorExpenditureSubCentreService', function ($resource) {
  return $resource('/json/expenditure_centre_gfs_code/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//GFS CODES - REDONE
setupServices.factory('PaginatedGfsCodeService', function ($resource) {
  return $resource('/json/gfs_codes/paginated', {}, {});
});

// setupServices.factory('PaginateifrsGfsCodeService', function ($resource) {
//   return $resource('/json/gfs_codes/paginated', {}, {});
// });

setupServices.factory('CreateGfsCodeService', function ($resource) {
  return $resource('/json/gfs_code/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateGfsCodeService', function ($resource) {
  return $resource('/json/gfs_code/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateifrsGfsCodeService', function ($resource) {
  return $resource('/json/ifrsgfs_code/update', {}, {
    update: {
      method: 'POST'
    }
  });
});


// setupServices.factory('DeleteifrsGfsCodeService', function ($resource) {
//   return $resource('/json/gfs_code/delete/:gfs_code_id', {
//     gfs_code_id: '@gfs_code_id'
//   }, {
//     delete: {
//       method: 'GET'
//     }
//   });
// });

setupServices.factory('DeleteGfsCodeService', function ($resource) {
  return $resource('/json/gfs_code/delete/:gfs_code_id', {
    gfs_code_id: '@gfs_code_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedGfsCodeService', function ($resource) {
  return $resource('/json/gfs_codes/trashed', {}, {});
});

setupServices.factory('RestoreGfsCodeService', function ($resource) {
  return $resource('/json/gfs_code/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyGfsCodeTrashService', function ($resource) {
  return $resource('/json/gfs_codes/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteGfsCodeService', function ($resource) {
  return $resource('/json/gfs_code/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

setupServices.factory('ToggleProcurableGfsCodeService', function ($resource) {
  return $resource('/json/gfs_code/toggleProcurable', {}, {
    toggleProcurable: {
      method: 'POST'
    }
  });
});

setupServices.factory('ToggleActiveGfsCodeService', function ($resource) {
  return $resource('/json/gfs_code/toggleActive', {}, {
    toggleActive: {
      method: 'POST'
    }
  });
});

//GFS CODE SUB CATS
setupServices.factory('PaginatedGfsCodeSubCategoryService', function ($resource) {
  return $resource('/json/gfs_code_sub_categories/paginated', {}, {});
});

setupServices.factory('CreateGfsCodeSubCategoryService', function ($resource) {
  return $resource('/json/gfs_code_sub_category/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateGfsCodeSubCategoryService', function ($resource) {
  return $resource('/json/gfs_code_sub_category/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteGfsCodeSubCategoryService', function ($resource) {
  return $resource('/json/gfs_code_sub_category/delete/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('LoadGfsCodeSubCategoryService', function ($resource) {
  return $resource('/json/gfs_code_sub_categories/filter/:id', {
    id: '@id'
  }, {});
});

// SECTOR PROJECTS
setupServices.factory('LoadSectorProjectService', function ($resource) {
  return $resource('/json/sector_projects/sectors/:project_id', {
    project_id: '@project_id'
  }, {});
});

setupServices.factory('AddSectorProjectService', function ($resource) {
  return $resource('/json/sector_projects/create', {}, {
    add_sector: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteSectorProjectService', function ($resource) {
  return $resource('/json/sector_project/delete/:sector_project_id/:project_id', {
    sector_project_id: '@sector_project_id',
    project_id: '@project_id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

//SECTOR EXPENDITURE CENTRE SUB CENTRE GROUPS
setupServices.factory('PaginatedSectorExpenditureSubCentreGroupService', function ($resource) {
  return $resource('/json/sectorExpenditureSubCentreGroups/paginated', {}, {});
});

setupServices.factory('CreateSectorExpenditureSubCentreGroupService', function ($resource) {
  return $resource('/json/sectorExpenditureSubCentreGroup/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateSectorExpenditureSubCentreGroupService', function ($resource) {
  return $resource('/json/sectorExpenditureSubCentreGroup/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteSectorExpenditureSubCentreGroupService', function ($resource) {
  return $resource('/json/sectorExpenditureSubCentreGroup/delete/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedSectorExpenditureSubCentreGroupService', function ($resource) {
  return $resource('/json/sectorExpenditureSubCentreGroups/trashed', {}, {});
});

setupServices.factory('RestoreSectorExpenditureSubCentreGroupService', function ($resource) {
  return $resource('/json/sectorExpenditureSubCentreGroup/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptySectorExpenditureSubCentreGroupTrashService', function ($resource) {
  return $resource('/json/sectorExpenditureSubCentreGroups/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteSectorExpenditureSubCentreGroupService', function ($resource) {
  return $resource('/json/sectorExpenditureSubCentreGroup/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});


//BOD LIST SERVICES
setupServices.factory('PaginatedBodListService', function ($resource) {
  return $resource('/json/bodLists/paginated', {}, {});
});

setupServices.factory('CreateBodListService', function ($resource) {
  return $resource('/json/bodLists/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateBodListService', function ($resource) {
  return $resource('/json/bodLists/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteBodListService', function ($resource) {
  return $resource('/json/bodLists/delete/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedBodListService', function ($resource) {
  return $resource('/json/bodLists/trashed', {}, {});
});

setupServices.factory('RestoreBodListService', function ($resource) {
  return $resource('/json/bodLists/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyBodListTrashService', function ($resource) {
  return $resource('/json/bodLists/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteBodListService', function ($resource) {
  return $resource('/json/bodLists/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//BOD VERSION SERVICES
setupServices.factory('PaginatedBodVersionService', function ($resource) {
  return $resource('/json/bodVersions/paginated', {}, {});
});

setupServices.factory('CreateBodVersionService', function ($resource) {
  return $resource('/json/bodVersions/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateBodVersionService', function ($resource) {
  return $resource('/json/bodVersions/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteBodVersionService', function ($resource) {
  return $resource('/json/bodVersions/delete/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedBodVersionService', function ($resource) {
  return $resource('/json/bodVersions/trashed', {}, {});
});

setupServices.factory('RestoreBodVersionService', function ($resource) {
  return $resource('/json/bodVersions/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyBodVersionTrashService', function ($resource) {
  return $resource('/json/bodVersions/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteBodVersionService', function ($resource) {
  return $resource('/json/bodVersions/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//FUND TYPES
setupServices.factory('PaginatedFundTypeService', function ($resource) {
  return $resource('/json/fundTypes/paginated', {}, {});
});

setupServices.factory('CreateFundTypeService', function ($resource) {
  return $resource('/json/fundType/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateFundTypeService', function ($resource) {
  return $resource('/json/fundType/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteFundTypeService', function ($resource) {
  return $resource('/json/fundType/delete/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedFundTypeService', function ($resource) {
  return $resource('/json/fundTypes/trashed', {}, {});
});

setupServices.factory('RestoreFundTypeService', function ($resource) {
  return $resource('/json/fundType/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyFundTypeTrashService', function ($resource) {
  return $resource('/json/fundTypes/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteFundTypeService', function ($resource) {
  return $resource('/json/fundType/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});


//The services of the baseline statistics start here
setupServices.factory('CreateBaselineStatisticsService', function ($resource) {
  return $resource('/json/createBaselineStatistics', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateBaselineStatisticService', function ($resource) {
  return $resource('/json/editBaselineStatistics', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('LoadStatisticsService', function ($resource) {
  return $resource('/json/getBaselineStatisticsSingle', {}, {
    index: {
      method: 'GET'
    }
  });
});
setupServices.factory('DeleteBaselineStatisticsService', function ($resource) {
  return $resource('/json/deleteBaselineStatistics/:baselineStatisticId', {
    baselineStatisticId: '@baselineStatisticId'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

//The services of the baseline statistics end here

//FUND TYPES
setupServices.factory('PaginatedFundTypeService', function ($resource) {
  return $resource('/json/fundTypes/paginated', {}, {});
});

setupServices.factory('CreateFundTypeService', function ($resource) {
  return $resource('/json/fundType/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateFundTypeService', function ($resource) {
  return $resource('/json/fundType/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteFundTypeService', function ($resource) {
  return $resource('/json/fundType/delete/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedFundTypeService', function ($resource) {
  return $resource('/json/fundTypes/trashed', {}, {});
});

setupServices.factory('RestoreFundTypeService', function ($resource) {
  return $resource('/json/fundType/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyFundTypeTrashService', function ($resource) {
  return $resource('/json/fundTypes/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteFundTypeService', function ($resource) {
  return $resource('/json/fundType/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//CEILING CHAINS
setupServices.factory('CeilingChainService', ['$resource', function ($resource) {
  return $resource('/json/ceilingChains/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/ceilingChains/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/ceilingChains/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/ceilingChains/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/ceilingChains/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/ceilingChains/emptyTrash'
    }
  });
}]);

setupServices.factory('SearchFacilityService', function ($resource) {
  return $resource('/json/facilities/search', {}, {});
});

setupServices.factory('FacilityTypeCodeService', function ($resource) {
  return $resource('/json/facilityTypes/code/:id', {
    id: '@id'
  }, {});
});

//CALENDAR EVENTS
setupServices.factory('CalendarEventService', ['$resource', function ($resource) {
  return $resource('/json/calendarEvents/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/calendarEvents/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/calendarEvents/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/calendarEvents/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/calendarEvents/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/calendarEvents/emptyTrash'
    },
    getByYearAndLevel: {
      method: 'GET',
      params: {
        financialYearId: '@financialYearId',
        hierarchyPosition: '@hierarchyPosition'
      },
      url: '/json/calendar-events/get-by-year-and-level/:financialYearId/:hierarchyPosition'
    }
  });
}]);

//CALENDARS
setupServices.factory('CalendarService', ['$resource', function ($resource) {
  return $resource('/json/calendars/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/calendars/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/calendars/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/calendars/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/calendars/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/calendars/emptyTrash'
    }
  });
}]);

//CALENDAR EVENT REMINDER RECIPIENTS
setupServices.factory('CalendarEventReminderRecipientService', ['$resource', function ($resource) {
  return $resource('/json/eventRecipients/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/eventRecipients/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/eventRecipients/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/eventRecipients/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/eventRecipients/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/eventRecipients/emptyTrash'
    }
  });
}]);

//NOTIFICATIONS
setupServices.factory('NotificationService', ['$resource', function ($resource) {
  return $resource('/json/notifications/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/notifications/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/notifications/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/notifications/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/notifications/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/notifications/emptyTrash'
    }
  });
}]);


//EXPENDITURE CENTRE GFS CODES
setupServices.factory('ExpenditureCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditureCentreGfsCode/:id/gfs_codes', {
    id: '@id'
  }, {});
});

setupServices.factory('AddExpenditureCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditureCentreGfsCode/add_gfs_code', {}, {
    add_gfs_code: {
      method: 'POST'
    }
  });
});


//BOD VERSION SERVICES
setupServices.factory('PaginatedBodInterventionService', function ($resource) {
  return $resource('/json/bodInterventions/paginated', {}, {});
});

setupServices.factory('CreateBodInterventionService', function ($resource) {
  return $resource('/json/bodInterventions/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateBodInterventionService', function ($resource) {
  return $resource('/json/bodInterventions/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteBodInterventionService', function ($resource) {
  return $resource('/json/bodInterventions/delete/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedBodInterventionService', function ($resource) {
  return $resource('/json/bodInterventions/trashed', {}, {});
});

setupServices.factory('RestoreBodInterventionService', function ($resource) {
  return $resource('/json/bodInterventions/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyBodInterventionTrashService', function ($resource) {
  return $resource('/json/bodInterventions/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteBodInterventionService', function ($resource) {
  return $resource('/json/bodInterventions/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

setupServices.factory('DeleteExpenditureCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditureCentreGfsCode/:expenditure_centre_gfs_code_id/:expenditure_centre_id/delete', {
    expenditure_centre_gfs_code_id: '@expenditure_centre_gfs_code_id',
    expenditure_centre_id: '@expenditure_centre_id'
  }, {});
});

setupServices.factory('ExpenditureCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditureCentreGfsCode/:id/gfs_codes', {
    id: '@id'
  }, {});
});

setupServices.factory('AddExpenditureCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditureCentreGfsCode/add_gfs_code', {}, {
    add_gfs_code: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteExpenditureCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditureCentreGfsCode/:expenditure_centre_gfs_code_id/:expenditure_centre_id/delete', {
    expenditure_centre_gfs_code_id: '@expenditure_centre_gfs_code_id',
    expenditure_centre_id: '@expenditure_centre_id'
  }, {});
});

//EXPENDITURE SUB CENTRE GFS CODES
setupServices.factory('ExpenditureSubCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditureSubCentreGfsCode/:id/gfs_codes', {
    id: '@id'
  }, {});
});

setupServices.factory('AddExpenditureSubCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditureSubCentreGfsCode/add_gfs_code', {}, {
    add_gfs_code: {
      method: 'POST'
    }
  });

});

setupServices.factory('DeleteExpenditureSubCentreGfsCodeService', function ($resource) {
  return $resource('/json/expenditureSubCentreGfsCode/:id/:parent_id/delete', {
    id: '@id',
    parent_id: '@parent_id'
  }, {});

});

//FUND SOURCE CATEGORIES
setupServices.factory('FundSourceCategoryService', ['$resource', function ($resource) {
  return $resource('/json/fund_source_categories/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/fund_source_categories/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/fund_source_categories/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/fund_source_categories/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/fund_source_categories/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/fund_source_categories/emptyTrash'
    }
  });
}]);

//PERIOD GROUP SERVICES
setupServices.factory('PaginatedPeriodGroupService', function ($resource) {
  return $resource('/json/periodGroups/paginated', {}, {});
});

setupServices.factory('CreatePeriodGroupService', function ($resource) {
  return $resource('/json/periodGroups/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdatePeriodGroupService', function ($resource) {
  return $resource('/json/periodGroups/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeletePeriodGroupService', function ($resource) {
  return $resource('/json/periodGroups/delete/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedPeriodGroupService', function ($resource) {
  return $resource('/json/periodGroups/trashed', {}, {});
});

setupServices.factory('RestorePeriodGroupService', function ($resource) {
  return $resource('/json/periodGroups/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyPeriodGroupTrashService', function ($resource) {
  return $resource('/json/periodGroups/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeletePeriodGroupService', function ($resource) {
  return $resource('/json/periodGroups/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

//The routes of the cas plan table select options begin here
setupServices.factory('CasPlanTableSelectOptionsService', function ($resource) {
  return $resource('/json/casPlanTableOptions', {}, {
    index: {
      method: 'GET'
    }
  });
});


//The routes of the cas plan table select options end here

//REPORT MENU
setupServices.factory('ReportService', ['$resource', function ($resource) {
  return $resource('/json/reports/:id', {}, {
    fetchAll: {
      method: 'GET',
      url: '/json/reports/tree'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
  });
}]);

//cas plan table select options
setupServices.factory('casPlanTableSelectOptionService', ['$resource', function ($resource) {
  return $resource('/json/casPlanTableSelectOptions/:id', {}, {
    update: {
      method: 'POST',
      url: '/json/UpdateCasPlanTableSelectOptions',
      params: {
        id: '@id'
      }
    },
  });
}]);
//end

//USER PROFILE KEYS
setupServices.factory('UserProfileKeyService', ['$resource', function ($resource) {
  return $resource('/json/userProfileKeys/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/userProfileKeys/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/userProfileKeys/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/userProfileKeys/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/userProfileKeys/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/userProfileKeys/emptyTrash'
    }
  });
}]);
//USER PROFILE KEYS
setupServices.factory('UserProfile', ['$resource', function ($resource) {
  return $resource('/json/userProfile/:id', {}, {
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    }
  });
}]);

//pe submission form report
setupServices.factory('peSubmissionFormReportsService', ['$resource', function ($resource) {
  return $resource('/json/peSubmissionFormReports/:id', {}, {
    update: {
      method: 'POST',
      url: '/json/updatePeSubmissionFormReport'
    },
    toggleReport: {
      method: 'POST',
      url: '/json/togglePeSubmissionFormReport'
    }
  });
}]);
//end

//Service to get all pe reports
setupServices.factory('GetAllPeReportsService', ['$resource', function ($resource) {
  return $resource('/json/getAllPeReports', {}, {
    // update: {method: 'POST', url: '/json/peSubmissionFormReports', params: {id: '@id'}},
  });
}]);
//end

// Service to deal with cas plan table constraints
setupServices.factory('CasPlanTableConstraintsService', ['$resource', function ($resource) {
  return $resource('/json/casPlanTableConstraints', {}, {
    update: {
      method: 'POST',
      url: '/json/casPlanTableConstraints',
      params: {
        id: '@id'
      }
    },
    getCasPlanTableConstraints: {
      method: 'POST',
      url: '/json/getCasPlanTableConstraints',
      params: {
        id: '@id'
      }
    },
  });
}]);
// Service to deal with cas plan table constraints ends here


setupServices.factory('AdministrativeHierarchyService', ['$resource', function ($resource) {
  return $resource('/json/administrativeHierarchies/:id', {}, {
    import: {
      method: 'GET',
      url: '/json/api/admin_hierarchies/import'
    }
  });
}]);

//Activity status service
setupServices.factory('PaginatedActivityStatusService', function ($resource) {
  return $resource('/json/activityStatuses/paginated', {}, {});
});

setupServices.factory('CreateActivityStatusService', function ($resource) {
  return $resource('/json/activityStatuses/create', {}, {
    store: {
      method: 'POST'
    }
  });
});

setupServices.factory('UpdateActivityStatusService', function ($resource) {
  return $resource('/json/activityStatuses/update', {}, {
    update: {
      method: 'POST'
    }
  });
});

setupServices.factory('DeleteActivityStatusService', function ($resource) {
  return $resource('/json/activityStatuses/delete/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});

setupServices.factory('TrashedActivityStatusService', function ($resource) {
  return $resource('/json/activityStatuses/trashed', {}, {});
});

setupServices.factory('RestoreActivityStatusService', function ($resource) {
  return $resource('/json/activityStatuses/restore/:id', {
    id: '@id'
  }, {
    restore: {
      method: 'GET'
    }
  });
});

setupServices.factory('EmptyActivityStatusService', function ($resource) {
  return $resource('/json/activityStatuses/emptyTrash', {}, {});
});

setupServices.factory('PermanentDeleteActivityStatusService', function ($resource) {
  return $resource('/json/activityStatuses/permanentDelete/:id', {
    id: '@id'
  }, {
    permanentDelete: {
      method: 'GET'
    }
  });
});

setupServices.factory('ToggleActivityStatusService', function ($resource) {
  return $resource('/json/toggleActivityStatus', {}, {
    toggleActivityStatus: {
      method: 'POST'
    }
  });
});

//ASSESSOR ASSIGNMENTS
setupServices.factory('AssessorAssignmentService', ['$resource', function ($resource) {
  return $resource('/json/assessorAssignments/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/assessorAssignments/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/assessorAssignments/trashed'
    },
    toggleActive: {
      method: 'POST',
      url: '/json/assessorAssignments/toggleActive'
    },
    restore: {
      method: 'GET',
      url: '/json/assessorAssignments/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/assessorAssignments/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/assessorAssignments/emptyTrash'
    },
    periods: {
      method: 'GET',
      url: '/json/assessorAssignments/periods'
    },
  });
}]);

setupServices.factory('ToggleBodVersionService', function ($resource) {
  return $resource('/json/toggleBodVersion', {}, {
    toggleBodVersion: {
      method: 'POST'
    }
  });
});
// Surplus categories
setupServices.factory('SurplusCategoryService', ['$resource', function ($resource) {
  return $resource('/json/activitySurplusCategories/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/activitySurplusCategories/paginated'
    },
    fetchAll: {
      method: 'GET',
      url: '/json/activitySurplusCategories'
    },
    catStatusChange: {
      method: 'GET',
      url: '/json/activitySurplusCategories-changeCatStatus'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/activitySurplusCategories/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/activitySurplusCategories/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/activitySurplusCategories/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/activitySurplusCategories/emptyTrash'
    },
    toggleActive: {
      method: 'GET',
      url: '/json/activitySurplusCategories/toggleActive/:id',
      params: {
        id: '@id'
      }
    }
  });
}]);
//ACTIVITY TASK NATURES
setupServices.factory('ActivityTaskNatureService', ['$resource', function ($resource) {
  return $resource('/json/activityTaskNatures/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/activityTaskNatures/paginated'
    },
    fetchAll: {
      method: 'GET',
      url: '/json/activityTaskNatures'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/activityTaskNatures/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/activityTaskNatures/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/activityTaskNatures/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/activityTaskNatures/emptyTrash'
    },
    toggleActive: {
      method: 'GET',
      url: '/json/activityTaskNatures/toggleActive/:id',
      params: {
        id: '@id'
      }
    }
  });
}]);

//PERFORMANCE INDICATORS
setupServices.factory('PerformanceIndicatorService', ['$resource', function ($resource) {
  return $resource('/json/performanceIndicators/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/performanceIndicators/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/performanceIndicators/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/performanceIndicators/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/performanceIndicators/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/performanceIndicators/emptyTrash'
    },
    toggleQualitative: {
      method: 'POST',
      url: '/json/performanceIndicators/toggleQualitative'
    },
    toggleLessIsGood: {
      method: 'POST',
      url: '/json/performanceIndicators/toggleLessIsGood'
    },
    search: {
      method: 'GET',
      url: '/json/performanceIndicators/search'
    },
    copyPerformanceIndicators: {
      method: 'GET',
      url: '/json/performanceIndicators/copy'
    },
  });
}]);

//FACILITY OWNERSHIPS
setupServices.factory('FacilityOwnershipService', ['$resource', function ($resource) {
  return $resource('/json/facilityOwnerships/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/facilityOwnerships/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/facilityOwnerships/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/facilityOwnerships/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/facilityOwnerships/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/facilityOwnerships/emptyTrash'
    }
  });
}]);

//FACILITY OWNERSHIPS
setupServices.factory('FacilityPhysicalStateService', ['$resource', function ($resource) {
  return $resource('/json/facilityPhysicalStates/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/facilityPhysicalStates/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/facilityPhysicalStates/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/facilityPhysicalStates/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/facilityPhysicalStates/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/facilityPhysicalStates/emptyTrash'
    }
  });
}]);

//FACILITY STAR RATING
setupServices.factory('FacilityStarRatingService', ['$resource', function ($resource) {
  return $resource('/json/facilityStarRatings/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/facilityStarRatings/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/facilityStarRatings/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/facilityStarRatings/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/facilityStarRatings/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/facilityStarRatings/emptyTrash'
    }
  });
}]);

//FACILITY CUSTOM DETAILS
setupServices.factory('FacilityCustomDetailService', ['$resource', function ($resource) {
  return $resource('/json/facilityCustomDetails/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/facilityCustomDetails/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/facilityCustomDetails/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/facilityCustomDetails/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/facilityCustomDetails/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/facilityCustomDetails/emptyTrash'
    }
  });
}]);


//FACILITY CUSTOM DETAIL MAPPINGS
setupServices.factory('FacilityCustomDetailMappingService', ['$resource', function ($resource) {
  return $resource('/json/facilityCustomDetailMappings/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/facilityCustomDetailMappings/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/facilityCustomDetailMappings/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/facilityCustomDetailMappings/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/facilityCustomDetailMappings/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/facilityCustomDetailMappings/emptyTrash'
    }
  });
}]);

//NATIONAL TARGETS
setupServices.factory('NationalTargetService', ['$resource', function ($resource) {
  return $resource('/json/nationalTargets/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/nationalTargets/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/nationalTargets/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/nationalTargets/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/nationalTargets/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/nationalTargets/emptyTrash'
    }
  });
}]);

//PLANNING MATRICES
setupServices.factory('PlanningMatrixService', ['$resource', function ($resource) {
  return $resource('/json/planningMatrices/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/planningMatrices/paginated'
    },
    all: {
      method: 'GET',
      url: '/json/planningMatrices/all'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/planningMatrices/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/planningMatrices/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/planningMatrices/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/planningMatrices/emptyTrash'
    },
    toggleActive: {
      method: 'POST',
      url: '/json/planningMatrices/toggleActive'
    },
  });
}]);


//GENERIC SECTOR PROBLEMS
setupServices.factory('GenericSectorProblemService', ['$resource', function ($resource) {
  return $resource('/json/genericSectorProblems/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/genericSectorProblems/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/genericSectorProblems/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/genericSectorProblems/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/genericSectorProblems/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/genericSectorProblems/emptyTrash'
    },
  });
}]);

//GENERIC TARGETS
setupServices.factory('GenericTargetService', ['$resource', function ($resource) {
  return $resource('/json/genericTargets/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/genericTargets/:planningMatrixId/paginated',
      params: {planningMatrixId: '@planningMatrixId'}
    },
    byPlanningMatrix: {
      method: 'GET',
      url: '/json/genericTargets/byPlanningMatrix/:planningMatrixId',
      params: {planningMatrixId: '@planningMatrixId'}
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/genericTargets/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/genericTargets/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/genericTargets/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/genericTargets/emptyTrash'
    },
    toggleActive: {
      method: 'POST',
      url: '/json/genericTargets/toggleActive'
    },
  });
}]);

//GENERIC ACTIVITIES
setupServices.factory('GenericActivityService', ['$resource', function ($resource) {
  return $resource('/json/genericActivities/:id', {}, {
    paginate: {
      method: 'GET',
      url: '/json/genericActivities/paginate/:planningMatrixId',
      params: {planningMatrixId: '@planningMatrixId'}
    },
    createOrUpdate: {
      method: 'POST'
    }
  });
}]);

//EXPENDITURE CENTRE GROUPS
setupServices.factory('ExpenditureCentreGroupService', ['$resource', function ($resource) {
  return $resource('/json/expenditureCentreGroups/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/expenditureCentreGroups/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/expenditureCentreGroups/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/expenditureCentreGroups/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/expenditureCentreGroups/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/expenditureCentreGroups/emptyTrash'
    },
    toggleActive: {
      method: 'POST',
      url: '/json/expenditureCentreGroups/toggleActive'
    }
  });
}]);

//EXPENDITURE CENTRE
setupServices.factory('ExpenditureCentreService', ['$resource', function ($resource) {
  return $resource('/json/expenditureCentres/:id', {}, {
    fetchAll: {
      method: 'GET',
      url: '/json/expenditureCentres/fetchAll'
    },
    paginated: {
      method: 'GET',
      url: '/json/expenditureCentres/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/expenditureCentres/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/expenditureCentres/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/expenditureCentres/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/expenditureCentres/emptyTrash'
    },
    links: {
      method: 'GET',
      url: '/json/expenditureCentres/links'
    },
    addLink: {
      method: 'POST',
      url: '/json/expenditureCentres/addLink'
    },
    removeLink: {
      method: 'GET',
      url: '/json/expenditureCentres/removeLink'
    },
    addValue: {
      method: 'POST',
      url: '/json/expenditureCentres/addValue'
    },
    removeValue: {
      method: 'GET',
      url: '/json/expenditureCentres/removeValue'
    },
    values: {
      method: 'GET',
      url: '/json/expenditureCentres/values'
    },
    taskNatures: {
      method: 'GET',
      url: '/json/expenditureCentres/taskNatures'
    },
    addTaskNature: {
      method: 'POST',
      url: '/json/expenditureCentres/addTaskNature'
    },
    removeTaskNature: {
      method: 'GET',
      url: '/json/expenditureCentres/removeTaskNature'
    },
    costCentres: {
      method: 'GET',
      url: '/json/expenditureCentres/costCentres'
    },
    addCostCentre: {
      method: 'POST',
      url: '/json/expenditureCentres/addCostCentre'
    },
    removeCostCentre: {
      method: 'GET',
      url: '/json/expenditureCentres/removeCostCentre'
    },
  });
}]);

//The project data form content services start here
setupServices.factory('ProjectDataFormContentsService', function ($resource) {
  return $resource('/json/projectDataFormContents/:form_id', {
    form_id: '@form_id'
  }, {
    index: {
      method: 'GET'
    }
  });
});
setupServices.factory('CreateProjectDataFormContentsService', function ($resource) {
  return $resource('/json/createProjectDataFormContents', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateProjectDataFormContentsService', function ($resource) {
  return $resource('/json/updateProjectDataFormContents', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteProjectDataFormContentsService', function ($resource) {
  return $resource('/json/deleteProjectDataFormContents/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
//The project data form content services end here


//The project data form content services start here
setupServices.factory('ProjectDataFormQuestionsService', function ($resource) {
  return $resource('/json/projectDataFormQuestions', {}, {
    index: {
      method: 'GET'
    }
  });
});
setupServices.factory('CreateProjectDataFormQuestionsService', function ($resource) {
  return $resource('/json/createProjectDataFormQuestions', {}, {
    store: {
      method: 'POST'
    }
  });
});
setupServices.factory('UpdateProjectDataFormQuestionsService', function ($resource) {
  return $resource('/json/updateProjectDataFormQuestions', {}, {
    update: {
      method: 'POST'
    }
  });
});
setupServices.factory('DeleteProjectDataFormQuestionsService', function ($resource) {
  return $resource('/json/deleteProjectDataFormQuestions/:id', {
    id: '@id'
  }, {
    delete: {
      method: 'GET'
    }
  });
});
//The project data form content services end here

//LINK SPECS
setupServices.factory('LinkSpecificationService', ['$resource', function ($resource) {
  return $resource('/json/linkSpecs/:id', {}, {
    fetchAll: {
      method: 'GET',
      url: '/json/linkSpecs/fetchAll'
    },
    paginated: {
      method: 'GET',
      url: '/json/linkSpecs/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/linkSpecs/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/linkSpecs/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/linkSpecs/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/linkSpecs/emptyTrash'
    },
  });
}]);

//ASSET CONDITIONS
setupServices.factory('AssetConditionService', ['$resource', function ($resource) {
  return $resource('/json/assetConditions/:id', {}, {
    fetchAll: {
      method: 'GET',
      url: '/json/assetConditions/fetchAll'
    },
    paginated: {
      method: 'GET',
      url: '/json/assetConditions/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/assetConditions/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/assetConditions/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/assetConditions/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/assetConditions/emptyTrash'
    },
  });
}]);

//ASSET USES
setupServices.factory('AssetUseService', ['$resource', function ($resource) {
  return $resource('/json/assetUses/:id', {}, {
    fetchAll: {
      method: 'GET',
      url: '/json/assetUses/fetchAll'
    },
    paginated: {
      method: 'GET',
      url: '/json/assetUses/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/assetUses/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/assetUses/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/assetUses/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/assetUses/emptyTrash'
    },
  });
}]);

//TRANSPORT FACILITY TYPES
setupServices.factory('TransportFacilityTypeService', ['$resource', function ($resource) {
  return $resource('/json/transportFacilityTypes/:id', {}, {
    fetchAll: {
      method: 'GET',
      url: '/json/transportFacilityTypes/fetchAll'
    },
    paginated: {
      method: 'GET',
      url: '/json/transportFacilityTypes/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/transportFacilityTypes/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/transportFacilityTypes/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/transportFacilityTypes/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/transportFacilityTypes/emptyTrash'
    },
  });
}]);

//REFERENCE DOCUMENTS
setupServices.factory('ReferenceDocumentService', ['$resource', function ($resource) {
  return $resource('/json/referenceDocuments/:id', {}, {
    fetchAll: {
      method: 'GET',
      url: '/json/referenceDocuments/fetchAll'
    },
    paginated: {
      method: 'GET',
      url: '/json/referenceDocuments/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/referenceDocuments/trashed'
    },
    updateCustom: {
      method: 'POST',
      url: '/json/referenceDocuments/:id/updateCustom'
    },
    restore: {
      method: 'GET',
      url: '/json/referenceDocuments/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/referenceDocuments/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/referenceDocuments/emptyTrash'
    },
    toggleActive: {
      method: 'POST',
      url: '/json/referenceDocuments/toggleActive'
    },
  });
}]);

setupServices.factory('PlanChainService', ['$resource', function ($resource) {
  return $resource('/json/planChains/:id', {}, {
    search: {
      method: 'GET',
      url: '/json/planChains/search'
    },
    loadParentPlanChains: {
      method: 'GET',
      url: '/json/planChains/loadParentPlanChains'
    },
    lowestPlanChains: {
      method: 'GET',
      url: '/json/planChains/lowestPlanChains'
    },
    copyPlanChains: {
      method: 'GET',
      url: '/json/planChains/copyPlanChains'
    },
    priorityAreas: {
      method: 'GET',
      url: '/json/planChains/priorityAreas'
    },
    addPriorityAreas: {
      method: 'POST',
      url: '/json/planChains/addPriorityAreas'
    },
    removePriorityArea: {
      method: 'GET',
      url: '/json/planChains/removePriorityArea'
    },
    getNextFinancialYearVersion: {
      url: '/json/planChains/nextFinancialYearVersion/:currentFinancialYearId/:planChainId',
      params: {
        currentFinancialYearId: '@currentFinancialYearId',
        planChainId: '@planChainId',
      }
    },
    carryToNextVersion: {
      method: 'POST',
      url: '/json/planChain/:planChainId/add-to-version/:versionId',
      params: {
        planChainId: '@planChainId',
        versionId: '@versionId'
      }
    }
  });
}]);

//fund sources
setupServices.factory('FundSourceService', ['$resource', function ($resource) {
  return $resource('/json/fundSources/:id', {}, {
    fetchAll: {
      method: 'GET',
      url: '/json/fundSources/fetchAll'
    },
    paginated: {
      method: 'GET',
      url: '/json/fundSources/paginated'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    trashed: {
      method: 'GET',
      url: '/json/fundSources/trashed'
    },
    restore: {
      method: 'GET',
      url: '/json/fundSources/:id/restore'
    },
    permanentDelete: {
      method: 'GET',
      url: '/json/fundSources/:id/permanentDelete'
    },
    emptyTrash: {
      method: 'GET',
      url: '/json/fundSources/emptyTrash'
    },
    toggleActive: {
      method: 'POST',
      url: '/json/fundSources/toggleActive'
    },
    search: {
      method: 'GET',
      url: '/json/fundSources/searchFundSource'
    },
    copyFundSources: {
      method: 'GET',
      url: '/json/fundSources/copy'
    },
  });
}]);


setupServices.factory('ProjectService', ['$resource', function ($resource) {
  return $resource('/json/projects/:id', {}, {
    search: {
      method: 'GET',
      url: '/json/projects/search'
    },
    copyProjects: {
      method: 'GET',
      url: '/json/projects/copy'
    },
    addFundSources: {
      method: 'POST',
      url: '/json/projects/addFundSources'
    },
    removeFundSource: {
      method: 'GET',
      url: '/json/projects/removeFundSource'
    },
    fundSources: {
      method: 'GET',
      url: '/json/projects/fundSources'
    }
  });
}]);


//budget submission select options
setupServices.factory('BudgetSubmissionSelectOptionService', ['$resource', function ($resource) {
  return $resource('/json/BudgetSubmissionSelectOption/:id', {}, {
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    getChildren: {
      method: 'GET',
      url: '/json/BudgetSubmissionSelectOption/getChildren'
    }
  });
}]);

//Historical Data Tables
setupServices.factory('HistoricalDataService', ['$resource', function ($resource) {
  return $resource('/api/historicalDataTables/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/api/historicalDataTables/paginated'
    },
    addHistoricalDataColumn: {
      method: 'POST',
      url: '/api/historicalDataTables/addColumn'
    },
    removeHistoricalDataColumn: {
      method: 'GET',
      url: '/api/historicalDataTables/removeColumn'
    },
    columns: {
      method: 'GET',
      url: '/api/historicalDataTables/columns'
    },
    updateDataColumn: {
      method: 'POST',
      url: '/api/historicalDataTables/updateDataColumn'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    }
  });
}]);

//PROJECT TYPES
setupServices.factory('ProjectTypeService', ['$resource', function ($resource) {
  return $resource('/json/projectTypes/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/projectTypes/paged'
    },
    fetchAll: {
      method: 'GET',
      url: '/json/projectTypes'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    }
  });
}]);


//EXPENDITURE CATEGORY
setupServices.factory('ExpenditureCategoryService', ['$resource', function ($resource) {
  return $resource('/json/expenditureCategories/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/expenditureCategories/paged'
    },
    fetchAll: {
      method: 'GET',
      url: '/json/expenditureCategories'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    toggleActive: {
      method: 'GET',
      url: '/json/expenditureCategories/toggleActive/:id',
      params: {
        id: '@id'
      }
    }
  });
}]);

//PROJECT OUTPUT
setupServices.factory('ProjectOutputService', ['$resource', function ($resource) {
  return $resource('/json/projectOutputs/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/json/projectOutputs/paged'
    },
    fetchAll: {
      method: 'GET',
      url: '/json/projectOutputs'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    }
  });
}]);

setupServices.factory('BudgetClassService', ['$resource', function ($resource) {
  return $resource('/json/budgetClasses/:id', {}, {
    copy: {
      method: 'GET',
      url: '/json/budgetClasses/copy'
    },
    subBudgetClasses: {
      method: 'GET',
      url: '/json/budgetClasses/children'
    },
  });
}]);

//FUND SOURCE BUDGET CLASSES
setupServices.factory('FundSourceBudgetClassService', ['$resource', function ($resource) {
  return $resource('/api/fundSourceBudgetClasses/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/api/fundSourceBudgetClasses/paged'
    },
    fetchAll: {
      method: 'GET',
      url: '/api/fundSourceBudgetClasses'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    }
  });
}]);

setupServices.factory('AccountReturnService', ['$resource', function ($resource) {
  return $resource('/api/accountReturns/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/api/accountReturns/paged'
    },
    fetchAll: {
      method: 'GET',
      url: '/api/accountReturns'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    addGfsCodes: {
      method: 'POST',
      url: '/api/accountReturns/addGfsCodes'
    },
    gfsCodes: {
      method: 'GET',
      url: '/api/accountReturns/gfsCodes'
    },
    removeGfsCode: {
      method: 'GET',
      url: '/api/accountReturns/removeGfsCode'
    },
  });
}]);

setupServices.factory('VersionTypeService', ['$resource', function ($resource) {
  return $resource('/api/versionTypes/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/api/versionTypes/paged'
    },
    fetchAll: {
      method: 'GET',
      url: '/api/versionTypes'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
  });
}]);

setupServices.factory('VersionService', ['$resource', function ($resource) {
  return $resource('/api/versions/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/api/versions/paged'
    },
    fetchAll: {
      method: 'GET',
      url: '/api/versions'
    },
    toggleCurrent: {
      method: 'GET',
      url: '/api/versions/toggleCurrent'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    getNextFinancialYearVersion: {
      url: '/api/versions/nextFinancialYearVersion/:versionType/:currentFinancialYearId/:itemId',
      params: {
        versionType: '@versionType',
        currentFinancialYearId: '@currentFinancialYearId',
        itemId: '@itemId',
      }
    },
    carryToNextVersion: {
      method: 'POST',
      url: '/api/versions/:versionType/:itemId/add-to-version/:versionId',
      params: {
        itemId: '@itemId',
        versionType: '@versionType',
        versionId: '@versionId'
      }
    }
  });
}]);

setupServices.factory('FinancialYearService', ['$resource', function ($resource) {
  return $resource('/api/financialYears/:id', {}, {
    versions: {
      method: 'GET',
      url: '/json/financialYears/versions'
    },
    otherVersions: {
      method: 'GET',
      url: '/json/financialYears/otherFinancialYearVersions'
    },
    addVersion: {
      method: 'POST',
      url: '/json/financialYears/addVersion'
    },
    removeVersion: {
      method: 'GET',
      url: '/json/financialYears/removeVersion'
    },
  });
}]);

setupServices.factory('PriorityAreaService', ['$resource', function ($resource) {
  return $resource('/api/priority_areas/:id', {}, {
    sectors: {
      method: 'GET',
      url: '/json/priority_areas/sectors'
    },
    fetchAll: {
      method: 'GET',
      url: '/json/priority_areas/all'
    },
    removeSector: {
      method: 'GET',
      url: '/json/priority_areas/removeSector'
    },
    addSector: {
      method: 'POST',
      url: '/json/priority_areas/addSector'
    },
    copyPriorityAreas: {
      method: 'GET',
      url: '/json/priority_areas/copyPriorityAreas'
    },
    removePriorityArea: {
      method: 'GET',
      url: '/json/priority_areas/removePriorityArea'
    },
  });
}]);

setupServices.factory('GfsCodeSubCategoryService', ['$resource', function ($resource) {
  return $resource('/api/gfsCodeSubCategories/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/api/gfsCodeSubCategories/paged'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    getByCategory: {
      method: 'GET',
      url: '/api/gfsCodeSubCategories/getByCategory'
    },
    toggleProcurement: {
      method: 'POST',
      url: '/api/gfsCodeSubCategories/toggleProcurement'
    },
    upload: {
      method: 'POST',
      url: '/api/gfsCodeSubCategories/upload'
    },
  });
}]);

setupServices.factory('InterventionService', ['$resource', function ($resource) {
  return $resource('/api/interventions/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/api/interventions/paged'
    },
    active: {
      method: 'GET',
      url: '/api/interventions/active'
    },
    primary: {
      method: 'GET',
      url: '/api/interventions/primary'
    },
    copyInterventions: {
      method: 'GET',
      url: '/api/interventions/copy'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    }
  });
}]);

setupServices.factory('ReferenceTypeService', ['$resource', function ($resource) {
  return $resource('/api/referenceTypes/:id', {}, {
    copyReferences: {
      method: 'GET',
      url: '/api/referenceTypes/copy'
    },
    delete: {
      method: 'DELETE',
      url: '/api/referenceTypes/:id/:versionId',
      params: {
        id: '@id',
        versionId: '@versionId'
      }
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    }
  });
}]);

setupServices.factory('BankAccountService', ['$resource', function ($resource) {
  return $resource('/api/bankAccounts/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/api/bankAccounts/paged'
    },
    copyBankAccounts: {
      method: 'GET',
      url: '/api/bankAccounts/copyBankAccounts'
    },
    fetchAll: {
      method: 'GET',
      url: '/api/bankAccounts'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
  });
}]);

setupServices.factory('MetadataSourceService', ['$resource', function ($resource) {
  return $resource('/api/metadataSources/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/api/metadataSources/paged'
    },
    fetchAll: {
      method: 'GET',
      url: '/api/metadataSources'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
  });
}]);

setupServices.factory('MetadataTypeService', ['$resource', function ($resource) {
  return $resource('/api/metadataTypes/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/api/metadataTypes/paged'
    },
    fetchAll: {
      method: 'GET',
      url: '/api/metadataTypes'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
  });
}]);

setupServices.factory('MetadataItemService', ['$resource', function ($resource) {
  return $resource('/api/metadataItems/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/api/metadataItems/paged'
    },
    dataSets: {
      method: 'GET',
      url: '/api/metadataItems/dataSets'
    },
    dataSourceDataSets: {
      method: 'GET',
      url: '/api/metadataItems/dataSourceDataSets'
    },
    syncData: {
      method: 'GET',
      url: '/api/metadataItems/sync'
    },
    dataElements: {
      method: 'GET',
      url: '/api/metadataItems/dataElements'
    },
    fetchAll: {
      method: 'GET',
      url: '/api/metadataItems'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
  });
}]);


setupServices.factory('ProcurementMethodService', ['$resource', function ($resource) {
  return $resource('/api/procurementMethods/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/api/procurementMethods/paged'
    },
    fetchAll: {
      method: 'GET',
      url: '/api/procurementMethods'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
  });
}]);
setupServices.factory('AdvertisementService', ['$resource', function ($resource) {
  return $resource('/api/advertisements/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/api/advertisements/paged'
    },
    fetchAll: {
      method: 'GET',
      url: '/api/advertisements'
    },
    changeAdStatus: {
      method: 'GET',
      url: '/api/advertisements-changeAdStatus'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    removeFile: {
      method: 'PUT',
      params: {
        file_url: '@file_url'
      },
      url: '/api/advertisements-remove'
    }
  });
}]);

/////logo Services
setupServices.factory('LogoService', ['$resource', function ($resource) {
  return $resource('/api/logo/:id', {}, {
    paginated: {
      method: 'GET',
      url: '/api/logos/paged'
    },
    fetchAll: {
      method: 'GET',
      url: '/api/get-by-pisc'
    },
    save: {
      method: 'POST',
      url: '/api/save-logo-to-db'
    },

    search: {
      method: 'GET',
      url: '/api/advertisements-changeAdStatus'
    },
    update: {
      method: 'PUT',
      params: {
        id: '@id'
      }
    },
    removeFile: {
      method: 'PUT',
      params: {
        file_url: '@file_url'
      },
      url: '/api/logo-remove'
    }
  });
}]);
