var notificationService = angular.module('notificationServices', ['ngResource']);

notificationService.factory('NotificationService', function ($resource) {
    return $resource('json/all_notifications',  {}, {});
});

notificationService.factory('SocketIO', ['$rootScope', function($rootScope) {

    var socket = io('http://127.0.0.1:3000');

    return {
        on : function(eventName, callback) {
            socket.on(eventName, function() {
                var args = arguments;
                $rootScope.$apply(function() {
                    callback.apply(socket, args);
                });
            });
        },

        emit : function(eventName, data, callback) {
            socket.emit(eventName, data, function() {
                var args = arguments;
                $rootScope.$apply(function() {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            });
        }
    };
}]);
