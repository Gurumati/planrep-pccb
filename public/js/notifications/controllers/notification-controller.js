function NotificationController($scope, NotificationModel,localStorageService, $uibModal,SocketIO, ConfirmDialogService,
                                NotificationService) {

   // $scope.Notifications = NotificationModel;
    $scope.title = "NOTIFICATIONS";
    $scope.currentPage = 1;
    $scope.perPage = 10;
    $scope.maxSize = 10;

    $scope.user_id = localStorageService.get(localStorageKeys.USER_ID);
    var channel_and_event = "user_notifications" + ":" + $scope.user_id.toString();
    SocketIO.on(channel_and_event,function (data) {

        NotificationService.get({page: 1, perPage: 10}, function (data) {
            $scope.notifications = data;
        });

    })

    NotificationService.get({page: 1, perPage: 10}, function (data) {
        $scope.notifications = data;
    });


    $scope.pageChanged = function () {
        NotificationService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.notifications = data;
        });
    };

}



NotificationController.resolve = {
    NotificationModel: function (NotificationService, $q) {
        var deferred = $q.defer();
        NotificationService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};