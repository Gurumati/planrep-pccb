var notification = angular.module('notification-module', ['master-module', 'ngRoute', 'notificationServices']);

notification.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/', {
        controller: NotificationController,
        templateUrl: '/pages/notifications/index.html',
        resolve: NotificationController.resolve,
    }).otherwise({redirectTo: '/'});

}]);