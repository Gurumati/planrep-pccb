var currentRequests = [];
var app = angular.module(
  "master-module",
  [
    "master.services",
    "planrep.localStorage",
    "ngRoute",
    "ngFileUpload",
    "ngAnimate",
    "ui.bootstrap",
    "pascalprecht.translate",
    "angularTreeview",
    "tree.dropdown",
    "textAngular",
    "angular.filter",
    "ui.select",
    "ngSanitize",
    "ngMaterial",
    "ngMessages",
    "ngMdIcons",
    "yaru22.angular-timeago",
  ],
  function ($httpProvider) {
    var interceptor = [
      "localStorageService",
      "$q",
      "$window",
      "$rootScope",
      function (localStorageService, $q, $window, $rootScope) {
        function responseSuccess(response) {
          $("#loader").hide();
          return response;
        }

        function responseError(response) {
          $("#loader").hide();
          switch (response.status) {
            case 403:
              $window.location = "/#!/access-denied";
              break;
            case 401:
              $rootScope.preventReload = response.config.method !== "GET";
              $rootScope.modalShown = true;
              break;
            case 402:
              $rootScope.resertPassowrdModalShown = true;
              break;
            case 500:
              break;
            case 404:
              break;
          }
          return $q.reject(response);
        }

        function request(config) {
          var defer = $q.defer();
          currentRequests.push(defer);
          config.timeout = defer.promise;
          if (
            (config.params !== undefined &&
              (config.params.noLoader === undefined ||
                config.params.noLoader === false)) ||
            config.params === undefined
          ) {
            $("#loader").show();
          } else {
            $("#loader").hide();
          }
          config.headers["X-Request-With"] = "XMLHttpRequest";
          return config;
        }

        return {
          request: request,
          response: responseSuccess,
          responseError: responseError,
        };
      },
    ];
    $httpProvider.interceptors.push(interceptor);
  }
);
app.run(function (
  $window,
  $rootScope,
  $timeout,
  $document,
  $interval,
  sessions,
  PingService,
  $location,
  $animate,
  $http
) {
  $animate.enabled(false);

  // My Codes
  var IDLE_TIMEOUT = 800; //seconds
  var _idleSecondsTimer = null;
  var _idleSecondsCounter = 0;

  document.onclick = function () {
    _idleSecondsCounter = 0;
  };

  document.onmousemove = function () {
    _idleSecondsCounter = 0;
  };

  document.onkeypress = function () {
    _idleSecondsCounter = 0;
  };

  _idleSecondsTimer = window.setInterval(CheckIdleTime, 1000);

  function CheckIdleTime() {
    _idleSecondsCounter++;
    var oPanel = document.getElementById("SecondsUntilExpire");
    if (oPanel) oPanel.innerHTML = IDLE_TIMEOUT - _idleSecondsCounter + "";
    if (_idleSecondsCounter >= IDLE_TIMEOUT) {
      // sessions.get({},function (data) {
      //   console.log('data',data);
      // });
      // console.log("Time expired!",_idleSecondsCounter);
    }
  }

  //End MyCodes

  var rootQueryParams = $location.search();
  $rootScope.isPopupWindow =
    undefined !== rootQueryParams.isPopup ? true : false;
  $rootScope.pageLoaded = true;

  $rootScope.cancelAll = function () {
    angular.forEach(currentRequests, function (request) {
      request.resolve();
    });
  };

  $rootScope.online = navigator.onLine;
  $window.addEventListener(
    "offline",
    function () {
      $rootScope.$apply(function () {
        $rootScope.online = false;
      });
    },
    false
  );

  $window.addEventListener(
    "online",
    function () {
      $rootScope.$apply(function () {
        $rootScope.online = true;
      });
    },
    false
  );
});
app.directive("hasPermission", [
  "localStorageService",
  function (localStorageService) {
    return function (scope, element, attrs) {
      var rights = localStorageService.get(localStorageKeys.RIGHT);
      if (rights !== undefined && rights !== null) {
        try {
          rights = JSON.parse(scope.rights);
        } catch (error) {}
        if (rights.indexOf(attrs.hasPermission) > -1) {
          element.show = true;
        } else {
          element.remove();
        }
      } else {
        element.remove();
      }
    };
  },
]);

app.filter("propsFilter", function () {
  return function (items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);

      items.forEach(function (item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});
app.factory("ConfirmDialogService", [
  "$q",
  "$uibModal",
  function ($q, $uibModal) {
    var _showConfirmDialog = function (title, message) {
      var defer = $q.defer();

      var modalInstance = $uibModal.open({
        animation: true,
        size: "sm",
        backdrop: false,
        templateUrl: "/pages/confirm-dialog.html",
        controller: function ($scope, $uibModalInstance) {
          $scope.title = title;
          $scope.message = message;

          $scope.ok = function () {
            $uibModalInstance.close();
            defer.resolve();
          };

          $scope.cancel = function () {
            $uibModalInstance.dismiss();
            defer.reject();
          };
        },
      });

      return defer.promise;
    };

    return {
      showConfirmDialog: _showConfirmDialog,
    };
  },
]);

app.factory("CommentDialogService", [
  "$q",
  "$uibModal",
  function ($q, $uibModal) {
    var showDialog = function (type, title, comments) {
      var defer = $q.defer();

      var modalInstance = $uibModal.open({
        animation: true,
        size: "lg",
        backdrop: false,
        templateUrl: "/pages/comment-dialog.html",
        controller: function (
          $scope,
          $uibModalInstance,
          ForwardMtefSectionService
        ) {
          $scope.title = title;
          $scope.comments = comments;

          $scope.address = function (c) {
            c.isLoading = true;
            ForwardMtefSectionService.setAddressed(
              {
                itemType: type,
                itemId: c.id,
              },
              c,
              function (data) {
                c.isLoading = false;
                c.addressed = true;
              },
              function (error) {
                console.log(error);
                c.isLoading = false;
              }
            );
          };

          $scope.done = function () {
            defer.resolve();
            $uibModalInstance.close();
          };
          $scope.cancel = function () {
            defer.resolve();
            $uibModalInstance.dismiss();
          };
        },
      });

      return defer.promise;
    };

    return {
      showDialog: showDialog,
    };
  },
]);
app.factory("BudgetDialogService", [
  "$q",
  "$uibModal",
  function ($q, $uibModal) {
    var _showConfirmDialog = function (
      title,
      message,
      nextDecisionLevels,
      submittedScrutins,
      direction
    ) {
      var defer = $q.defer();

      var modalInstance = $uibModal.open({
        animation: true,
        size: "lg",
        backdrop: false,
        templateUrl: "/pages/budget-dialog.html",
        controller: function ($scope, $uibModalInstance, UserScrutinizations) {
          $scope.title = title;
          $scope.message = message;
          $scope.submittedScrutins = submittedScrutins;
          $scope.decisionLevels = nextDecisionLevels;
          $scope.direction = direction;
          if ($scope.decisionLevels.length === 1) {
            $scope.toDecisionLevelId = $scope.decisionLevels[0].id;
          }
          $scope.expectedLoaded = false;
          if ($scope.direction === 1) {
            UserScrutinizations.getUserExpectedScrutins(function (data) {
              $scope.expected = data;
              getNotSubmitted();
            });
          } else {
            $scope.notSubmitted = [];
            $scope.expectedLoaded = true;
          }
          $scope.byStatus = _.groupBy($scope.submittedScrutins, function (s) {
            if (s.status === 2) {
              return "finished";
            } else if (s.status === 1) {
              return "onProgress";
            } else if (s.status === 0) {
              return "notStarted";
            }
          });

          var getNotSubmitted = function () {
            var submittedIds = _.pluck(
              $scope.submittedScrutins,
              "cost_centre_id"
            );
            $scope.notSubmitted = _.filter($scope.expected, function (s) {
              return submittedIds.indexOf(s.id) === -1;
            });
            $scope.expectedLoaded = true;
          };

          $scope.ok = function () {
            var data = {
              comments: $scope.comments,
              toDecisionLevelId: $scope.toDecisionLevelId,
              direction: direction,
            };
            defer.resolve(data);
            $uibModalInstance.close();
          };

          $scope.cancel = function () {
            $uibModalInstance.dismiss();
            defer.reject();
          };
        },
      });

      return defer.promise;
    };

    return {
      showConfirmDialog: _showConfirmDialog,
    };
  },
]);
app.factory("ScrutinDialogService", [
  "$q",
  "$uibModal",
  function ($q, $uibModal) {
    var _showConfirmDialog = function (title) {
      var defer = $q.defer();

      var modalInstance = $uibModal.open({
        animation: true,
        size: "sm",
        backdrop: false,
        templateUrl: "/pages/scrutinization-dialog.html",
        controller: function ($scope, $uibModalInstance) {
          $scope.title = title;
          $scope.ok = function () {
            console.log($scope.recommendation);
            $uibModalInstance.close();

            defer.resolve($scope.recommendation);
          };

          $scope.cancel = function () {
            $uibModalInstance.dismiss();
            defer.reject();
          };
        },
      });

      return defer.promise;
    };

    return {
      showConfirmDialog: _showConfirmDialog,
    };
  },
]);

app.factory("CarryVersionService", [
  "$q",
  "$uibModal",
  function ($q, $uibModal) {
    var _showDialog = function (versionType, currentFinancialYearId, item) {
      var defer = $q.defer();

      var modalInstance = $uibModal.open({
        templateUrl: "/pages/setup/version/carry.html",
        backdrop: false,
        resolve: {
          nextFinancialYearVersion: [
            "$q",
            "VersionService",
            function ($q, VersionService) {
              var deferred = $q.defer();
              VersionService.getNextFinancialYearVersion(
                {
                  currentFinancialYearId: currentFinancialYearId,
                  itemId: item.id,
                  versionType: versionType,
                },
                function (data) {
                  deferred.resolve(data);
                },
                function (error) {
                  deferred.reject(error);
                }
              );
              return deferred.promise;
            },
          ],
        },
        controller: function (
          $scope,
          $uibModalInstance,
          nextFinancialYearVersion,
          VersionService
        ) {
          $scope.item = item;
          $scope.item.nextVersion = nextFinancialYearVersion;

          $scope.addToNextVersion = function (versionId, itemId) {
            VersionService.carryToNextVersion(
              {
                versionType: versionType,
                versionId: versionId,
                itemId: itemId,
              },
              function (data) {
                $scope.successMessage = "Successfull added";
                $scope.item.nextVersion.itemVersionId = data.itemVersionId;
              },
              function (error) {
                $scope.errorMessage = "Something went wrong";
                console.log(error);
              }
            );
          };

          $scope.close = function () {
            defer.reject();
            $uibModalInstance.dismiss();
          };
        },
      });
      return defer.promise;
    };

    return {
      showDialog: _showDialog,
    };
  },
]);
app.config(function ($translateProvider) {
  $translateProvider.useSanitizeValueStrategy("sanitize");
  $translateProvider.translations("en", translationsEN);
  $translateProvider.translations("sw", translationsSW);
  $translateProvider.preferredLanguage("en");
});
app.factory("SocketIO", [
  "$rootScope",
  function ($rootScope) {
    /**Temporary disable socket */
    return {
      on: function (eventName, callback) {},
      emit: function (eventName, data, callback) {},
    };

    /**
   var socket = io('http://172.16.18.165:3000');

   return {
      on: function (eventName, callback) {
          socket.on(eventName, function () {
              var args = arguments;
              $rootScope.$apply(function () {
                  callback.apply(socket, args);
              });
          });
      },

      emit: function (eventName, data, callback) {
          socket.emit(eventName, data, function () {
              var args = arguments;
              $rootScope.$apply(function () {
                  if (callback) {
                      callback.apply(socket, args);
                  }
              });
          });
      }
  };*/
  },
]);
app.factory("Notifications", function ($resource) {
  return $resource("/json/notifications/:user_id", { user_id: "@user_id" }, {});
});
app.controller("HeaderController", [
  "$scope",
  "$translate",
  "localStorageService",
  "$window",
  "SocketIO",
  "Notifications",
  "$uibModal",
  function (
    $scope,
    $translate,
    localStorageService,
    $window,
    SocketIO,
    Notifications,
    $uibModal
  ) {
    $scope.showIt = false;
    $scope.language = localStorageService.get(localStorageKeys.LANGUAGE);
    $translate.use($scope.language);
    $scope.menus = [];

    $scope.user_id = localStorageService.get(localStorageKeys.USER_ID);
    $scope.user = JSON.parse(localStorageService.get(localStorageKeys.USER));

    Notifications.get({ user_id: $scope.user_id }, function (data) {
      $scope.plan = data.notifications;
      $scope.unreads = data.unreads;
    });

    var channel_and_event =
      "user_notifications" + ":" + $scope.user_id.toString();
    SocketIO.on(channel_and_event, function (data) {
      console.log(data);
      $scope.plan = data.data;
      var msg = data.message;
      var url = data.url;
      var icon = data.icon;
      var unreads = _.where($scope.plan, { read_at: null });
      $scope.unreads = unreads.length;

      var audioElement = document.createElement("audio");
      audioElement.setAttribute("src", "/audio/notification_iphone.mp3");
      audioElement.play();
    });

    if (JSON.parse(localStorageService.get(localStorageKeys.USER))) {
      if (!JSON.parse(localStorageService.get(localStorageKeys.USER)).password_reset) {
        //console.log("change Status")

        $scope.modalForcePasswordShown = true;
        var modalInstance = $uibModal.open({
          animation: true,
          size: "sm",
          backdrop: false,
          templateUrl: "/pages/force-change-password.html",
          //templateUrl: '/pages/setup/user/reset-password.html',
          controller: function (
            $scope,
            $uibModalInstance,
            UpdatePasswordService
          ) {
            $scope.passwordToResetForm = {};
            //$scope.regex = '^(?=.*\\d)(?=.*[a-zA-Z]).{6,10}$';
            $scope.regex =
              "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-/_]).{8,}$";
            $scope.disableSignInButton = false;

            $scope.changePassword = function () {
              if ($scope.changePasswordForm.$invalid) {
                // $scope.changePasswordForm = true;
                return;
              } else if (
                $scope.passwordToResetForm.newUserPassword !=
                $scope.passwordToResetForm.confirmPassword
              ) {
                $scope.errorMessage = "Two password do not match";
                return;
              }

              UpdatePasswordService.update(
                $scope.passwordToResetForm,
                function (data) {
                  if (data.successMessage) {
                    $scope.successMessage = data.successMessage;
                    $scope.errorMessage = null;
                    $scope.disableSignInButton = true;
                    // $uibModalInstance.close(data);
                    setTimeout(function () {
                      localStorage.clear();
                      document.cookie =
                        "laravel_session" +
                        "=;expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/";
                      $window.location = "/logout";
                    }, 2000);
                  } else {
                    $scope.errorMessage = data.errorMessage;
                  }
                },
                function (error) {
                  $scope.errorMessage = error.data.errorMessage;
                }
              );
            };

            $scope.logout = function () {
              $uibModalInstance.close();
              $window.location = "/logout";
              setTimeout(function () {
                localStorage.clear();
                document.cookie =
                  "laravel_session" +
                  "=;expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/";
              }, 2000);
            };
          },
        });
      } else {
        $scope.modalForcePasswordShown = false;
      }
    }

    $scope.toggleHamburger = function () {
      $scope.showIt = !$scope.showIt;
    };

    $scope.showSideBar = function ($event) {
      $scope.showIt = !$scope.showIt;
      console.log($event);
      openCloseSidebar($event);
    };

    function openCloseSidebar(e) {
      e.preventDefault();

      // Toggle min sidebar class
      $("body").toggleClass("sidebar-xs");
      $(".navbar-header").toggleClass("hidden-header");
      $(".cirled").toggleClass("small-img");
    }

    $scope.changeLanguage = function (key) {
      localStorageService.add(localStorageKeys.LANGUAGE, key);
      $scope.language = key;
      $translate.use(key);
    };

    $scope.loadRights = (function () {
      $scope.rights = localStorageService.get(localStorageKeys.RIGHT);
    })();

    $scope.hasPermission = function (permission) {
      if ($scope.rights !== undefined && $scope.rights !== null) {
        var rights = JSON.parse($scope.rights);
        return rights.indexOf(permission) > -1;
      }
      return false;
    };

    $scope.$watch("online", function (newStatus) {
      $scope.online = newStatus;
    });
    $scope.fullName = localStorageService.get(localStorageKeys.FULLNAME);
    $scope.roles = localStorageService.get(localStorageKeys.ROLES);
    $scope.adminHierarchy = localStorageService.get(
      localStorageKeys.ADMIN_HIERARCHY
    );
    $scope.admin_hierarchy_id = localStorageService.get(
      localStorageKeys.ADMIN_HIERARCHY_ID
    );
    $scope.section = localStorageService.get(localStorageKeys.SECTION);
    $scope.section_id = localStorageService.get(localStorageKeys.SECTION_ID);
    $scope.sector = localStorageService.get(localStorageKeys.SECTOR);
    $scope.profilePictureUrl = localStorageService.get(
      localStorageKeys.PROFILE_PICTURE
    );
    $scope.user_id = localStorageService.get(localStorageKeys.USER_ID);

    $scope.logout = function () {
      localStorageService.remove(localStorageKeys.RIGHT);
      localStorageService.remove(localStorageKeys.USERNAME);
      localStorageService.remove(localStorageKeys.USER_ID);
      localStorageService.remove(localStorageKeys.FULLNAME);
      localStorageService.remove(localStorageKeys.USER_ID);
      localStorageService.remove(localStorageKeys.ADMIN_HIERARCHY);
      localStorageService.remove(localStorageKeys.ADMIN_HIERARCHY_ID);
      localStorageService.remove(localStorageKeys.ADMIN_HIERARCHY_LEVEL_ID);
      localStorageService.remove(localStorageKeys.HIERARCHY_POSITION);
      localStorageService.remove(localStorageKeys.SECTION);
      localStorageService.remove(localStorageKeys.SECTION_ID);
      localStorageService.remove(localStorageKeys.SECTOR);
      localStorageService.remove(localStorageKeys.PROFILE_PICTURE);
      document.cookie =
        "laravel_session" + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/";
      $window.location = "/logout";
    };
  },
]);
app.value("loginConfig", { modalShown: false, preventReload: false });
app.controller("LoginController", [
  "$scope",
  "$http",
  "localStorageService",
  "$uibModal",
  function ($scope, $http, localStorageService, $uibModal) {
    var FORGOT_PASSWORD = "/public/pages/forgot-password.html";
    $http.get("/api/advertisements").then(function (data) {
      $scope.advertisements = data.data;
    });
    var validateLoginForm = function () {
      if ($scope.user_name === undefined || $scope.user_name.trim() === "") {
        $scope.formError = true; //"USER_NAME_REQUIRED";
        $scope.loginError = false;
        return false;
      }
      if ($scope.password === undefined) {
        $scope.formError = true; //"PASSWORD_REQUIRED";
        $scope.loginError = false;
        return false;
      }
      $scope.formError = false;
      $scope.loginError = false;
      return true;
    };

    $scope.doLogin = function () {
      if (!validateLoginForm()) {
        return;
      }

      $scope.disableSignInButton = true;
      var data = $.param({
        user_name: $scope.user_name,
        password: $scope.password,
      });
      $http({
        method: "POST",
        url: "/login",
        data: data,
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
      }).then(
        function (success) {
          if (success.data.status == 500) {
            $scope.disableSignInButton = false;
            $scope.loginError = success.data.errorMessage;
            $scope.disableSignInButton = false;
            return;
          }
          //force reset pwd on first login
          // if (success.data.password_reset === false){
          //
          // }
          $scope.loginConfig = {};
          var data = success.data;
          var user = angular.copy(data);
          loadUserRights();
          if (data.error) {
            $scope.loginError = data.error;
            return;
          }
          user.rights = getRights(data.permissions, data.roles);
          localStorageService.add(
            localStorageKeys.RIGHT,
            getRights(data.permissions, data.roles)
          );
          localStorageService.add(localStorageKeys.USERNAME, data.user_name);
          localStorageService.add(
            localStorageKeys.ROLES,
            data.roles.length === 0 ? "" : data.roles[0].name
          );
          localStorageService.add(
            localStorageKeys.FULLNAME,
            data.first_name + " " + data.last_name
          );
          localStorageService.add(localStorageKeys.USER_ID, data.id);
          if (data.admin_hierarchy !== undefined) {
            localStorageService.add(
              localStorageKeys.ADMIN_HIERARCHY,
              data.admin_hierarchy.name
            );
            localStorageService.add(
              localStorageKeys.ADMIN_HIERARCHY_ID,
              data.admin_hierarchy.id
            );
            if (data.admin_hierarchy_level !== undefined) {
              user.admin_hierarchy.admin_hierarchy_level = {
                id: data.admin_hierarchy_level.id,
                name: data.admin_hierarchy_level.name,
              };
              localStorageService.add(
                localStorageKeys.ADMIN_HIERARCHY_LEVEL_ID,
                data.admin_hierarchy_level.id
              );
              localStorageService.add(
                localStorageKeys.HIERARCHY_POSITION,
                data.admin_hierarchy_level.hierarchy_position
              );
            }
          }
          if (data.section !== undefined) {
            localStorageService.add(
              localStorageKeys.SECTION,
              data.section.name
            );
            localStorageService.add(
              localStorageKeys.SECTION_ID,
              data.section.id
            );
          }
          if (data.sector !== undefined) {
            localStorageService.add(localStorageKeys.SECTOR, data.sector.name);
          }
          localStorageService.add(
            localStorageKeys.PROFILE_PICTURE,
            data.profile_picture_url
          );
          // user.profile_picture_url = data.profile_picture_url;
          localStorageService.add(localStorageKeys.USER, JSON.stringify(user));

          if (window.location.href.indexOf("/login") !== -1) {
            window.location = "/"; //data.homePage;
            return;
          }
          if (!$scope.loginConfig.preventReload) {
            location.reload();
            return;
          }
          $rootScope.modalShown = false;
          $rootScope.preventReload = false;
          $scope.disableSignInButton = false;
        },
        function (error) {
          var data = error.data;
          $scope.disableSignInButton = false;
          $scope.loginError = data.error;
          $scope.disableSignInButton = false;
        }
      );
    };

    $scope.rights = null;

    function loadUserRights() {
      $scope.rights = localStorageService.get(localStorageKeys.RIGHT);
    }

    $scope.goToForgotPassword = function () {
      window.location = FORGOT_PASSWORD;
    };

    function getRoleRights(roles) {
      var rights = [];
      angular.forEach(roles, function (role) {
        var r = _.map(JSON.parse(role.permissions), function (num, key) {
          if (num) {
            return key;
          }
        });
        rights = _.union(rights, r);
      });
      return rights;
    }

    function getRights(rightList, roles) {
      var rights = [];
      if (!rightList) {
        return rights;
      }
      rights = _.map(rightList, function (num, key) {
        if (num) {
          return key;
        }
      });
      var roleRights = getRoleRights(roles);
      rights = _.union(rights, roleRights);
      return JSON.stringify(rights);
    }
  },
]);
app.controller("ChangePasswordController", [
  "$scope",
  "UpdatePasswordService",
  "$route",
  "localStorageService",
  "$rootScope",
  function (
    $scope,
    UpdatePasswordService,
    $route,
    localStorageService,
    $rootScope
  ) {
    $scope.regex = "^(?=.*\\d)(?=.*[a-zA-Z]).{6,10}$";
    $scope.changePassword = function () {
      if ($scope.changePasswordForm.$invalid) {
        $scope.formHasErrors = true;
        return;
      }
      $scope.user = {
        id: localStorageService.get(localStorageKeys.USER_ID),
        newPassword: $scope.newPassword,
      };
      UpdatePasswordService.update(
        $scope.user,
        function (data) {
          $rootScope.resertPassowrdModalShown = false;
          $route.reload();
        },
        function (error) {
          $scope.errorMessage = error.data.errorMessage;
        }
      );
    };
  },
]);

// $scope.changePasswordFirstTime = function () {
//     console.log("Hapa AACC")
// }
