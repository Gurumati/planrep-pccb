var planningServices = angular.module('planningServices', ['ngResource']);

planningServices.factory("ActivityFacility", function ($resource) {
    return $resource('/json/activity-facilities/:id', { id: '@id' }, {
        getByActivityPaginate: { url: '/json/activity-facilities/get-by-activity/:activityId/paginate', params: { activityId: '@activityId' } },
        addForActivity: { method: 'POST', url: '/json/activity-facilities/add-for-activity/:activityId', params: { activityId: '@activityId' } },
        deleteFromActivity: { method: 'DELETE', url: '/json/activity-facilities/delete-from-activity/:activityFacilityId', params: { activityFacilityId: '@activityFacilityId' } },
        deleteAllFromActivity: { method: 'DELETE', url: '/json/activity-facilities/delete-all-from-activity/:activityId', params: { activityId: '@activityId' } },
        getActivityFacilityProject: { method: 'GET', url: '/json/activity-facility-project' }
    });
});
planningServices.factory("ActivityFacilityFundSource", function ($resource) {
    return $resource('/json/activity-facilities/:id', { id: '@id' }, {
        deleteFromActivityFacility: { url: '/json/' }
    });
});

planningServices.factory('GetAllProjectionService', function ($resource) {
    return $resource('/json/paginatedProjections', {}, {});
});

planningServices.factory('UpdateProjectionService', function ($resource) {
    return $resource('/json/updateProjection', {}, { update: { method: 'POST' } });
});
planningServices.factory('DeleteProjectionService', function ($resource) {
    return $resource('/json/deleteProjection/:projection_id', { projection_id: '@projection_id' }, { delete: { method: 'GET' } });
});

planningServices.factory('UpdateProjectionBudgetClassService', function ($resource) {
    return $resource('/json/updateProjectionBudgetClassService', {}, { update_amounts: { method: 'POST' } });
});

//Admin hierarchy section mapping starts here
planningServices.factory('adminHierarchySectMappingService', function ($resource) {
    return $resource('/json/adminHierarchySectMapping', {}, {});
});
planningServices.factory('CreateAdminSectionMappingService', function ($resource) {
    return $resource('/json/createAdminHierarchySectMapping', {}, { store: { method: 'POST' } });
});
//Admin hierarchy section mapping ends here

planningServices.factory('AllProjectionPeriodService', function ($resource) {
    return $resource('/json/allProjectionPeriods/:projection_id', { projection_id: '@projection_id' }, { periods: { method: 'GET' } });
});

planningServices.factory('UpdateProjectionPeriodService', function ($resource) {
    return $resource('/json/updateProjectionPeriodPercentage', {}, { update_periods: { method: 'POST' } });
});

// PE Contributions
planningServices.factory('GetAllPEContributionService', function ($resource) {
    return $resource('/json/paginatedPEContributions', {}, {});
});

planningServices.factory('CreatePEContributionService', function ($resource) {
    return $resource('/json/createPEContribution', {}, { store: { method: 'POST' } });
});
planningServices.factory('UpdatePEContributionService', function ($resource) {
    return $resource('/json/updatePEContribution', {}, { update: { method: 'POST' } });
});
planningServices.factory('DeletePEContributionService', function ($resource) {
    return $resource('/json/deletePEContribution/:pe_contribution_id', { pe_contribution_id: '@pe_contribution_id' }, { delete: { method: 'GET' } });
});

planningServices.factory('TogglePEContributionService', function ($resource) {
    return $resource('/json/togglePEContribution', {}, { togglePEContribution: { method: 'POST' } });
});

planningServices.factory('LongTermTargetsService', function ($resource) {
    return $resource('/json/longTermTargets', {}, {
        getGenericTarget: { url: 'json/longTermTargets/getGenericTargetByPlanChain/:planChainId', method: 'GET', params: { planChainId: '@planChainId' } },
        refreshCode: { url: '/json/longTermTargets/refreshCode/:id', method: 'GET', params: { id: '@id' } }
    }); //This get all sectors;
});

planningServices.factory('AnnualTarget', function ($resource) {
    return $resource('/json/annual-targets', {}, {
        byMtefSectionPaginated: {
            url: '/json/annual-targets/by-mtef-section/paginated/:mtefId/:mtefSectionId/:budgetType',
            method: 'GET',
            params: { mtefId: '@mtefId', mtefSectionId: '@mtefSectionId', budgetType: '@budgetType' }
        },
        byMtefSectionAndPlanChain: {
            url: '/json/annual-targets/by-mtef-section/by-plan-chain/:mtefId/:mtefSectionId/:budgetType/:planChainId',
            method: 'GET',
            params: { mtefId: '@mtefId', mtefSectionId: '@mtefSectionId', budgetType: '@budgetType', planChainId: '@planChainId' }
        },
        confirm: {
            url: '/json/annual-target/confirm/:id',
            method: 'PUT',
            params: { id: '@id' }
        },
        initiate: {
            url: '/json/annual-target/initiate/:financialYearId/:longTermTargetId',
            method: 'POST',
            params: { financialYearId: '@financialYearId', longTermTargetId: '@longTermTargetId' }
        }

    });
});

planningServices.factory('AnnualTargetsService', function ($resource) {
    return $resource('/json/annualTargets', {}, {});
});
planningServices.factory('CreateAnnualTargetService', function ($resource) {
    return $resource('/json/createAnnualTarget', {}, { store: { method: 'POST' } });
});


planningServices.factory('CreateMtefAnnualTargetService', function ($resource) {
    return $resource('/json/createMtefAnnualTarget', {}, { store: { method: 'POST' } });
});
planningServices.factory('UpdateAnnualTargetService', function ($resource) {
    return $resource('/json/updateAnnualTarget', {}, { update: { method: 'POST' } });
});
planningServices.factory('DeleteAnnualTargetService', function ($resource) {
    return $resource('/json/deleteAnnualTarget/:annualTargetId', { annualTargetId: '@annualTargetId' }, { delete: { method: 'GET' } });
});

planningServices.factory('AnnualTargetsPaginationService', function ($resource) {
    return $resource('/json/AnnualTargetsPagination', {}, {});
});

planningServices.factory('MtefAnnualTargetsPaginationService', function ($resource) {
    return $resource('/json/mtefAnnualTargetsPagination', {}, {});
});

//END LowestPlanChainsServiceAccess right for AnnualTarget

// Section Targets
planningServices.factory('GetAllSectorTargetService', function ($resource) {
    return $resource('/json/paginatedSectorTargets', {}, {});
});

planningServices.factory('SectorTargetService', function ($resource) {
    return $resource('/json/sectorTargets', {}, {});
});

planningServices.factory('SectorTargetOnlyService', function ($resource) {
    return $resource('/json/sectorTargetsOnly', {}, { store: { method: 'POST' } });
});

planningServices.factory('CreateSectorTargetService', function ($resource) {
    return $resource('/json/createSectorTarget', {}, { store: { method: 'POST' } });
});
planningServices.factory('UpdateSectorTargetService', function ($resource) {
    return $resource('/json/updateSectorTarget', {}, { update: { method: 'POST' } });
});
planningServices.factory('DeleteSectorTargetService', function ($resource) {
    return $resource('/json/deleteSectorTarget/:sector_target_id', { sector_target_id: '@sector_target_id' }, { delete: { method: 'GET' } });
});

// MTEF
planningServices.factory('GetAllMTEFService', function ($resource) {
    return $resource('/json/paginatedMtefs', {}, {});
});

planningServices.factory('CreateMTEFService', function ($resource) {
    return $resource('/json/createMtef', {}, { store: { method: 'POST' } });
});
planningServices.factory('UpdateMTEFService', function ($resource) {
    return $resource('/json/updateMtef', {}, { update: { method: 'POST' } });
});

planningServices.factory('DeleteMTEFService', function ($resource) {
    return $resource('/json/deleteMtef/:mtef_id', { mtef_id: '@mtef_id' }, { delete: { method: 'GET' } });
});

planningServices.factory('ToggleISFinalService', function ($resource) {
    return $resource('/json/toggleISFinal', {}, { toggleISFinal: { method: 'POST' } });
});

planningServices.factory('FinalMTEFService', function ($resource) {
    return $resource('/json/FinalMtef/:mtef_id', { mtef_id: '@mtef_id' }, { delete: { method: 'GET' } });
});

planningServices.factory('TargetTypeService', function ($resource) {
    return $resource('/json/targetTypes', {}, {}); //This get all sectors;
});

planningServices.factory('SectorsService', function ($resource) {
    return $resource('/json/sectors', {}, {}); //This get all sectors;
});

planningServices.factory('LongTermTargetsNoAnnualService', function ($resource) {
    return $resource('/json/longTermTargetsNoAnnual', {}, {}); //This get all sectors;
});


//Admin hierarchy section mapping starts here
planningServices.factory('adminHierarchySectMappingService', function ($resource) {
    return $resource('/json/adminHierarchySectMapping', {}, {});
});
planningServices.factory('CreateAdminSectionMappingService', function ($resource) {
    return $resource('/json/createAdminHierarchySectMapping', {}, { store: { method: 'POST' } });
});
//Admin hierarchy section mapping ends here

planningServices.factory('DeleteActivityService', function ($resource) {
    return $resource('/json/deleteActivity/:activityId/:mtefSectionId', { activityId: '@activityId', mtefSectionId: '@mtefSectionId' }, { delete: { method: 'POST' } });
});

//The service below is for search
planningServices.factory('AutoCompleteFacility', function ($resource) {
    return $resource('/json/autoCompleteFacility/:key_word', { key_word: '@key_word' }, {});
});

planningServices.factory('SavePlanActivitiesService', function ($resource) {
    return $resource('/json/savePlanActivities/:mtefSectionId', { mtefSectionId: '@mtefSectionId' }, { store: { method: 'POST' } });
});

planningServices.factory('Target', function ($resource) {
    return $resource('/json/targets/:id', { id: '@id' }, {
        paginateByPlanChain: {
            method: 'GET',
            url: '/json/targets/by-plan-chain/:planChainId/:perPage',
            params: { planChainId: '@planChainId',perPage: '@perPage'}
        }
    });
});

planningServices.factory('LongTermTargetsPaginationService', function ($resource) {
    return $resource('/json/longTermTargetsPagination', {}, {}); //This get all sectors;
});
planningServices.factory('CreateLongTermTargetService', function ($resource) {
    return $resource('/json/createLongTermTarget', {}, { store: { method: 'POST' } }); //This create sector and return existing sectors after creating
});
planningServices.factory('UpdateLongTermTargetService', function ($resource) {
    return $resource('/json/updateLongTermTarget', {}, { update: { method: 'POST' } }); //This create sector and return existing sectors after creating
});

planningServices.factory('DeleteLongTermTargetService', function ($resource) {
    return $resource('/json/deleteLongTermTarget/:longTermTargetId',
        { longTermTargetId: '@longTermTargetId' },
        {
            delete: { method: 'POST' }
        });
});

planningServices.factory('PlanChainSectorOnlyService', function ($resource) {
    return $resource('/json/planChainSectorsOnly', {}, { store: { method: 'POST' } });
});
services.factory('PlanningService', function ($resource) {
    return $resource('/json/planningData/:mtefSectionId/:targetId', { mtefSectionId: '@mtefSectionId', targetId: '@targetId' }, {
        getProjects: { method: 'GET', url: '/json/projects/by-budget-class/:budgetClassId/by-section/:sectionId', params: { budgetClassId: '@budgetClassId', sectionId: '@sectionId' } }
    });
});

planningServices.factory('LongTermTargetsPaginationService', function ($resource) {
    return $resource('/json/longTermTargetsPagination', {}, {}); //This get all sectors;
});
planningServices.factory('CreateLongTermTargetService', function ($resource) {
    return $resource('/json/createLongTermTarget', {}, { store: { method: 'POST' } }); //This create sector and return existing sectors after creating
});
planningServices.factory('UpdateLongTermTargetService', function ($resource) {
    return $resource('/json/updateLongTermTarget', {}, { update: { method: 'POST' } }); //This create sector and return existing sectors after creating
});
planningServices.factory('DeleteLongTermTargetService', function ($resource) {
    return $resource('/json/deleteLongTermTarget/:longTermTargetId', { longTermTargetId: '@longTermTargetId' }, { delete: { method: 'POST' } });
});

//MTEF SECTOR PROBLEMS
planningServices.factory('PaginatedMtefSectorProblemService', function ($resource) {
    return $resource('/json/mtef_sector_problems/paginated', {}, {});
});

planningServices.factory('CreateMtefSectorProblemService', function ($resource) {
    return $resource('/json/mtef_sector_problem/create', {}, { store: { method: 'POST' } });
});

planningServices.factory('UpdateMtefSectorProblemService', function ($resource) {
    return $resource('/json/mtef_sector_problem/update', {}, { update: { method: 'POST' } });
});

planningServices.factory('DeleteMtefSectorProblemService', function ($resource) {
    return $resource('/json/mtef_sector_problem/delete/:mtef_sector_problem_id', { mtef_sector_problem_id: '@mtef_sector_problem_id' }, { delete: { method: 'GET' } });
});

planningServices.factory('TrashedMtefSectorProblemService', function ($resource) {
    return $resource('/json/mtef_sector_problems/trashed', {}, {});
});

planningServices.factory('RestoreMtefSectorProblemService', function ($resource) {
    return $resource('/json/mtef_sector_problem/restore/:id', { id: '@id' }, { restore: { method: 'GET' } });
});

planningServices.factory('EmptyMtefSectorProblemTrashService', function ($resource) {
    return $resource('/json/mtef_sector_problems/emptyTrash', {}, {});
});

planningServices.factory('PermanentDeleteMtefSectorProblemService', function ($resource) {
    return $resource('/json/mtef_sector_problem/permanentDelete/:id', { id: '@id' }, { permanentDelete: { method: 'GET' } });
});

planningServices.factory('AssignGfsCodeService', function ($resource) {
    return $resource('/json/fund_source/assignGfsCodes', {}, { assignGfsCodes: { method: 'POST' } });
});

planningServices.factory('SectionGfsCodeService', function ($resource) {
    return $resource('/json/section/gfs_codes', {}, {});
});

//ADMIN HIERARCHY CEILING PERIOD
planningServices.factory('PaginatedAdminHierarchyCeilingPeriodService', function ($resource) {
    return $resource('/json/admin_hierarchy_ceiling_periods/paginated', {}, {});
});

planningServices.factory('LoadBudgetClassesService', function ($resource) {
    return $resource('/json/loadBudgetClasses/:childrenId', { childrenId: '@childrenBudgetClassesId' }, {});
});

//Comprehensive plans table item values
planningServices.factory('PostCasPlanTableItemValueService', function ($resource) {
    return $resource('/json/PostCasPlanTableItemValue', {}, { store: { method: 'POST' } });
});
planningServices.factory('PostCasPlanTableItemConstantValueService', function ($resource) {
    return $resource('/json/PostCasPlanTableItemConstantValue', {}, { store: { method: 'POST' } });
});
planningServices.factory('LoadCasPlanTableItemValueService', function ($resource) {
    return $resource('/json/LoadCasPlanTableItemValue',
        {
            table_id: '@table_id',
            facility: '@facility',
            period_id: '@period_id',
            financial_year_id: '@financial_year_id'
        }, {});
});


//end comprehensive plans table item values

//Cas Plan Table Column Admin Hierarchy
planningServices.factory('CasPlanTableItemAdminHierarchyService', function ($resource) {
    return $resource('/json/CasPlanTableItemAdminHierarchy/:cas_plan_table_id', { cas_plan_table_id: '@cas_plan_table_id' }, {});
});
planningServices.factory('CasPlanTabularTableService', function ($resource) {
    return $resource('/json/CasPlanTabularTable/:cas_plan_id', { cas_plan_id: '@cas_plan_id' }, {});
});
planningServices.factory('CreateCasPlanTableItemAdminHierarchyService', function ($resource) {
    return $resource('/json/CreateCasPlanTableItemAdminHierarchy', {}, { store: { method: 'POST' } });
});
//end Cas Plan Table Column Admin Hierarchy

//The services for the cas plan table details begin here
planningServices.factory('CreateCasPlanTableDetailsService', function ($resource) {
    return $resource('/json/createCasPlanTableDetails', {}, { store: { method: 'POST' } });
});

planningServices.factory('EditCasPlanTableDetailsService', function ($resource) {
    return $resource('/json/updateCasPlanTableDetails/:id', { id: 'id' }, { update: { method: 'POST' } });
});
//The services for the cas plan table details end here

//The services of the baseline data values begin here
planningServices.factory('BaselineStatisticsService', function ($resource) {
    return $resource('/json/baselineStatistics', {}, { index: { method: 'POST' } });
});

planningServices.factory('BaselineStatisticValuesService', function ($resource) {
    return $resource('/json/baselineStatisticValues', {}, { index: { method: 'POST' } });
});

planningServices.factory('CreateBaselineStatisticValuesService', function ($resource) {
    return $resource('/json/createBaselineStatisticValues', {}, { store: { method: 'POST' } });
});

planningServices.factory('UpdateBaselineStatisticValuesService', function ($resource) {
    return $resource('/json/updateBaselineStatisticValues', {}, { update: { method: 'POST' } });
});

planningServices.factory('DeleteBaselineStatisticValuesService', function ($resource) {
    return $resource('/json/deleteBaselineStatisticValues', {}, { delete: { method: 'POST' } });
});
//The services of the baseline data values end here


//The services of the assets begin here
planningServices.factory('UpdateAssetService', function ($resource) {
    return $resource('/json/updateAsset', {}, { update: { method: 'POST' } });
});

planningServices.factory('CreateAssetService', function ($resource) {
    return $resource('/json/createAsset', {}, { store: { method: 'POST' } });
});

planningServices.factory('LoadParentAssetsService', function ($resource) {
    return $resource('/json/loadParentAssets', {}, {});
});

planningServices.factory('DeleteAssetService', function ($resource) {
    return $resource('/json/deleteAsset/:assetId', { assetId: '@assetId' }, { delete: { method: 'GET' } });
});
//The services of the assets end here

//PERFORMANCE INDICATOR BASELINE VALUES
planningServices.factory('TransportFacilityService', ['$resource', function ($resource) {
    return $resource('/json/performanceIndicatorBaselineValues/:id', {}, {
        paginated: { method: 'GET', url: '/json/performanceIndicatorBaselineValues/paginated' },
        update: { method: 'PUT', params: { id: '@id' } },
        trashed: { method: 'GET', url: '/json/performanceIndicatorBaselineValues/trashed' },
        restore: { method: 'GET', url: '/json/performanceIndicatorBaselineValues/:id/restore' },
        permanentDelete: { method: 'GET', url: '/json/performanceIndicatorBaselineValues/:id/permanentDelete' },
        emptyTrash: { method: 'GET', url: '/json/performanceIndicatorBaselineValues/emptyTrash' },
        financialYearBaselineValues: { method: 'GET', url: '/json/performanceIndicatorBaselineValues/financialYearBaselineValues' },
        addProjection: { method: 'POST', url: '/json/performanceIndicatorBaselineValues/addProjection' },
        deleteProjection: { method: 'GET', url: '/json/performanceIndicatorBaselineValues/deleteProjection' },
        facilitiesByTypeCouncilUser: { method: 'GET', url: '/json/facilities/facilitiesByTypeCouncilUser' },
    });
}]);


planningServices.factory('GenericActivity', function ($resource) {
    return $resource('/json/activities/getGenericActivitiesByTarget/:targetId', { targetId: '@targetId' }, {
        getByTarget: { method: 'GET' }
    });
});

//TRANSPORT FACILITIES
planningServices.factory('TransportFacilityService', ['$resource', function ($resource) {
    return $resource('/json/transportFacilities/:id', {}, {
        paginated: { method: 'GET', url: '/json/transportFacilities/paginated' },
        update: { method: 'PUT', params: { id: '@id' } },
        trashed: { method: 'GET', url: '/json/transportFacilities/trashed' },
        restore: { method: 'GET', url: '/json/transportFacilities/:id/restore' },
        permanentDelete: { method: 'GET', url: '/json/transportFacilities/:id/permanentDelete' },
        emptyTrash: { method: 'GET', url: '/json/transportFacilities/emptyTrash' }
    });
}]);


//FACILITIES - PLANNING
/**Moved to services.js*/

planningServices.factory('UpdateFacilityService', function ($resource) {
    return $resource('/json/updateFacility', {}, { update: { method: 'POST' } });
});

planningServices.factory('SearchFacilityService', function ($resource) {
    return $resource('/json/facilities/search', {}, {});
});

planningServices.factory('GetAdminHierarchiesByFacilityTypeIDService', function ($resource) {
    return $resource('/json/getAdminHierarchyByFacilityTypeId/:id', { id: '@id' }, { getAdminHierarchiesByFacilityTypeId: { method: 'GET' } });
});

//Historical Data Tables
planningServices.factory('HistoricalDataService', ['$resource', function ($resource) {
    return $resource('/api/historicalDataTables/:id', {}, {
        dataRows: { method: 'GET', url: '/api/historicalDataTables/dataRows' },
        saveRowValues: { method: 'POST', url: '/api/historicalDataTables/saveRowValues' },
    });
}]);

///cofog
planningServices.factory('CofogService', function ($resource) {
    return $resource('/api/get-parent-cofog:id', {}, {
        getParent: {
            method: 'GET',
            url: '/api/get-parent-cofog'
        },
        getCofogIdFromLongTermTargetReference: {
            method: 'GET',
            url: '/api/getCofogIdFromLongTermTargetReference/:id', params: { long_term_target_id: '@long_term_target_id' }
        }
    });
});

//submit cas plans for assessment
planningServices.factory('casPlanSubmissionService', ['$resource', function ($resource) {
    return $resource('/api/casPlanSubmission/:id', {}, {
    });
}]);

