function MTEFController($scope, AllMTEFModel, $uibModal,
                        ConfirmDialogService,
                        DeleteMTEFService,ToggleISFinalService, GetAllMTEFService,FinalMTEFService) {

    $scope.mtefs = AllMTEFModel;
    $scope.title = "TITLE_MTEFS";
    $scope.currentPage = 1;

    $scope.pageChanged = function () {
        GetAllMTEFService.get({page: $scope.currentPage}, function (data) {
            $scope.mtefs = data;
        });
    };
    $scope.edit = function (mtefToEdit, currentPage) {
        console.log(mtefToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: 'edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateMTEFService, AllDecisionLevelService, AllFinancialYearService, AllAdminHierarchyService) {
                AllDecisionLevelService.query(function (data) {
                    $scope.sectors = data;
                    $scope.mtefToEdit = angular.copy(mtefToEdit);
                });
                AllFinancialYearService.query(function (data) {
                    $scope.financialYears = data;
                });
                AllAdminHierarchyService.query(function (data) {
                    $scope.adminHierarchies = data;
                });
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    UpdateMTEFService.update({page: currentPage}, $scope.mtefToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.mtefs = data.mtefs;
                $scope.currentPage = $scope.mtefs.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete MTEF!', 'Are sure you want to delete this?').then(function () {
                //console.log("YES");
                DeleteMTEFService.delete({mtef_id: id, page: currentPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.mtefs = data.mtefs;
                        $scope.currentPage = $scope.mtefs.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    }
    $scope.toggleISFinal = function (mtef_id, is_final) {
        $scope.mtefToActivate = {};
        $scope.mtefToActivate.id = mtef_id;
        $scope.mtefToActivate.is_final = is_final;
        ToggleISFinalService.toggleISFinal($scope.mtefToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    }
}

MTEFController.resolve = {
    AllMTEFModel: function (GetAllMTEFService, $q) {
        var deferred = $q.defer();
        GetAllMTEFService.get({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};