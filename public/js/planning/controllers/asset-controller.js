function AssetsController($scope,
                          LoadParentAssetsService,
                          $uibModal,
                          UpdateAssetService,
                          DeleteAssetService,
                          ConfirmDialogService) {


    LoadParentAssetsService.query({}, function (data) {
        $scope.assets = data;
    });

    $scope.title = "TRANSPORT_FACILITY";

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/planning/assets/create.html',
            backdrop: false,

            controller: function ($scope, $uibModalInstance, CreateAssetService, LoadParentAssetsService) {

                LoadParentAssetsService.query(function (data) {
                    $scope.assets = data;
                });

                $scope.assetToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createAssetForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateAssetService.store($scope.assetToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new budget class
                $scope.successMessage = data.successMessage;
                $scope.assets = data.assets;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (assetToEdit) {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/planning/assets/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, LoadParentAssetsService) {
                LoadParentAssetsService.query(function (data) {
                    $scope.assets = data;
                });

                $scope.assetToEdit = angular.copy(assetToEdit);

                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    $uibModalInstance.close($scope.assetToEdit);
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (assetToEdit) {
                //Service to create new asset
                UpdateAssetService.update(assetToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage = data.successMessage;
                        $scope.assets = data.assets;
                    },
                    function (error) {

                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Asset Deletion!',
            'Are sure you want to delete this?')
            .then(function () {
                    console.log("YES");
                    DeleteAssetService.delete({assetId: id},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.assets = data.assets;
                        }, function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }

}

AssetsController.resolve = {
    BudgetClassesModel: function (LoadParentAssetsService, $q) {
        var deferred = $q.defer();
        LoadParentAssetsService.query({}, function (data) {
            deferred.resolve(data);
            console.log(data);
        });
        return deferred.promise;
    }
};