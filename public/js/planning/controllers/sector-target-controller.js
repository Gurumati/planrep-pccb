function SectorTargetController($scope, AllSectorTargetModel, $uibModal,
                                    ConfirmDialogService,
                                    DeleteSectorTargetService, GetAllSectorTargetService) {

    $scope.sectorTargets = AllSectorTargetModel;
    $scope.title = "TITLE_SECTOR_TARGETS";

    $scope.currentPage = 1;

    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        GetAllSectorTargetService.get({page: $scope.currentPage,perPage:$scope.perPage}, function (data) {
            $scope.sectorTargets = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating financial year
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/planning/sector_target/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateSectorTargetService, AllSectorService, AllLongTermTargetService) {
                $scope.sectorTargetToCreate = {};
                //Function to store data and close modal when Create button clicked
                AllSectorService.query(function (data) {
                    $scope.sectors = data;
                });
                AllLongTermTargetService.query(function (data) {
                    $scope.longTermTargets = data;
                });
                $scope.store = function () {
                    if ($scope.createSectorTargetForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateSectorTargetService.store({perPage:$scope.perPage},$scope.sectorTargetToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.sectorTargets = data.sectorTargets;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (sectorTargetToEdit, currentPage,perPage) {
        console.log(sectorTargetToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/planning/sector_target/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,UpdateSectorTargetService, AllSectorService, AllLongTermTargetService) {
                AllSectorService.query(function (data) {
                    $scope.sectors = data;
                    $scope.sectorTargetToEdit = angular.copy(sectorTargetToEdit);
                });
                AllLongTermTargetService.query(function (data) {
                    $scope.longTermTargets = data;
                });
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    UpdateSectorTargetService.update({page: currentPage,perPage:perPage}, $scope.sectorTargetToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.sectorTargets = data.sectorTargets;
                $scope.currentPage = $scope.sectorTargets.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id, currentPage,perPage) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Delete Sector Target!',
            'Are sure you want to delete this?')
            .then(function () {
                    console.log("YES");
                    DeleteSectorTargetService.delete({sector_target_id: id, page: currentPage,perPage:perPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.sectorTargets = data.sectorTargets;
                            $scope.currentPage = $scope.sectorTargets.current_page;
                        }, function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }
}

SectorTargetController.resolve = {
    AllSectorTargetModel: function (GetAllSectorTargetService, $q) {
        var deferred = $q.defer();
        GetAllSectorTargetService.get({page: 1,perPage:10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};