(function () {
    'use strict';

    angular.module('planning-module').controller('AddOrRemoveActivityFacilityController', AddOrRemoveActivityFacilityController);
    AddOrRemoveActivityFacilityController.$inject = ['$scope',  '$location', '$uibModalInstance', 'activity', 'facility', 'params', 'GlobalService', 'PreExecutionService', 'ActivityFacility', 'FacilityService', 'FundSourceService', '$q'];

    function AddOrRemoveActivityFacilityController($scope,  $location, $uibModalInstance, activity, facility, params, GlobalService, PreExecutionService, ActivityFacility, FacilityService, FundSourceService, $q) {

        $scope.currentPage = 1;
        $scope.selectedActivity = angular.copy(activity);
        $scope.originActivity = angular.copy(activity);
        $scope.isHomeFacility = params.isHomeFacility;
        $scope.queryParams = $location.search();

        $scope.facilityToAdd = {
            facility: ($scope.isHomeFacility) ? facility : undefined,
            activity_facility_fund_sources: []
        };

        $scope.budgetType = params.budgetType;
        $scope.isLocked = params.isLocked;
        $scope.isFinal = params.isFinal;

        $scope.toggleAccountType = function () {
            $scope.showChangeAccountWarning = false;
            ActivityFacility.deleteAllFromActivity({
                activityId: $scope.selectedActivity.id,
                toggleAccountType: 1
            }, function (data) {
                $scope.originActivity = angular.copy($scope.selectedActivity);
                $scope.pageChanged();
            });
        };

        $scope.accountChanged = function () {
            if ($scope.originActivity.is_facility_account !== $scope.selectedActivity.is_facility_account) {
                $scope.showChangeAccountWarning = true;
            } else {
                $scope.showChangeAccountWarning = false;
            }
        };

        $scope.cancelChangeType = function () {
            $scope.showChangeAccountWarning = false;
            $scope.selectedActivity.is_facility_account = $scope.originActivity.is_facility_account;
        };

        $scope.pageChanged = function () {
            $scope.activityFacilityIdLoading = true;
            ActivityFacility.getByActivityPaginate({
                activityId: activity.id,
                page: $scope.currentPage,
                searchQuery: $scope.searchQuery
            }, function (data) {
                $scope.activityFacilities = data;
                $scope.activityFacilityIdLoading = false;
            }, function (error) {
                $scope.message = error.data.errorMessage;
                $scope.activityFacilityIdLoading = true;
            });
        };
        var lastSearch = "";
        $scope.searchExistingFacility = function () {
            if ($scope.searchQuery !== undefined && ($scope.searchQuery.length >= 3 || $scope.searchQuery.length < lastSearch.length)) {
                $scope.pageChanged();
            }
            lastSearch = angular.copy($scope.searchQuery);
        };
        $scope.toExclude = [];
        $scope.fundSources = [];

        $scope.searchFacility = function ($query) {
            var deferred = $q.defer();
            if ($query !== undefined && $query !== '' && $query.length >= 1) {
                $scope.facilityLoading = true;
                var facilityToExclude = {
                    'facilityIdsToExclude': $scope.toExclude
                };
                FacilityService.searchByPlanningSection({
                        mtefSectionId: $scope.selectedActivity.mtef_section_id,
                        searchQuery: $query,
                        activityId: $scope.selectedActivity.id,
                        isFacilityAccount: true,
                        noLoader: true,
                        realoc: $scope.queryParams.realoc,
                        facilityId: $scope.queryParams.facilityId
                    },
                    facilityToExclude,
                    function (data) {
                        $scope.facilityLoading = false;
                        // if ($scope.selectedActivity.is_facility_account) {
                        //     data.facilities.unshift({
                        //         id: "%",
                        //         name: "All facilities"
                        //     });
                        // }
                        deferred.resolve(data.facilities);
                    });
            } else {
                deferred.resolve([]);
            }
            return deferred.promise;
        };
        $scope.loadBudgetClassFundSources = function () {
            FundSourceService.getByBudgetClassAndSector({
                    budgetClassId: $scope.selectedActivity.budget_class_id,
                    sectionId: params.sectionId,
                    financialYearId: params.financialYearId,
                    fundSourceId : $scope.queryParams.fundSourceId
                },
                function (data) {
                    $scope.fundSources = data.fundSources;
                });
        };

        GlobalService.projectTypes(function (data) {
            $scope.projectTypes = data.items;
        });

        $scope.loadExpenditureCategories = function (project_type_id) {
            PreExecutionService.expenditureCategoryByTaskNature({
                project_type_id: project_type_id,
                activity_task_nature_id: activity.activity_task_nature_id
            }, function (data) {
                $scope.expenditureCategories = data.items;
            });
        };

        $scope.loadProjectOutputs = function (expenditureCategoryId) {
            PreExecutionService.projectOutputs({
                expenditureCategoryId: expenditureCategoryId
            }, function (data) {
                $scope.projectOutputs = data.items;
            });
        };

        $scope.addFacility = function (facilityToAdd) {
            ActivityFacility.addForActivity({
                activityId: $scope.selectedActivity.id
            }, facilityToAdd, function (data) {
                $scope.successMessage = data.successMessage;
                $scope.facilityToAdd = {
                    activity_facility_fund_sources: []
                };
                $scope.pageChanged();
            });
        };
        $scope.deleteExistingFacility = function (activityFacilityId) {
            ActivityFacility.deleteFromActivity({
                activityFacilityId: activityFacilityId
            }, function (data) {
                $scope.successMessage = data.successMessage;
                $scope.pageChanged();
            }, function (error) {
                $scope.errorMessage = error.data.errorMessage;
            });
        };

        /** edit existing activity  facility */
        $scope.editExistingFacility = function(obj){
            //get project details
            $scope.selectedFacility = obj;
            ActivityFacility.getActivityFacilityProject({
                activity_id : obj.activity_id,
                facility_id : obj.facility_id
            }, function(data){
                $scope.activity_facility_project = data.items[0];
                $scope.activity_facility_project.facility_id = obj.facility_id; 
            });
        }

        /** update activity project output */
        $scope.updateActivityFacilityProject = function(data){
            PreExecutionService.addProjectOutput({activityId: data.id, facility_id: data.facility_id,
                 value: data.output_value, projectOutputId: data.project_output_id},
                function(data){
                    $scope.successMessage = data.successMessage;
                    $scope.activity_facility_project = undefined;
                    $scope.selectedFacility = undefined;
                    $scope.pageChanged();
                }, function(error){
                    $scope.errorMessage = error.data.errorMessage;
                    
                });
        }

        $scope.loadBudgetClassFundSources();
        $scope.pageChanged();
        $scope.close = function () {
            $uibModalInstance.close(activity);
        };
    }
})();