/**
 * Created by mrisho on 08/Mar/2017.
 */
function AnnualTargetController($scope, AnnualTargetsModel, $uibModal,
                                  CreateAnnualTargetService,
                                  UpdateAnnualTargetService,
                                  ConfirmDialogService,
                                  DeleteAnnualTargetService,lowestPlanChainType,
                                  AnnualTargetsService,LongTermTargetsNoAnnualService,PlanChainService) {

    $scope.annualTargets = AnnualTargetsModel;
    $scope.title = "ANNUAL_TARGETS";
    $scope.dateFormat = 'yyyy-MM-dd';

    // PlanChainService.query({}, function (data) {
    //     $scope.planChains =data;
    // });
    console.log(lowestPlanChainType);
    $scope.planChains=lowestPlanChainType.plan_chains;

    LongTermTargetsNoAnnualService.query({}, function (data) {
        $scope.longTermTargets=data;
    });


    $scope.loadAnnualTargets=function (planChain) {

        $scope.planChainId = planChain.id;

        $scope.filteredLongTermTargets=_.where($scope.longTermTargets,{plan_chain_id:planChain.id});

        console.log($scope.annualTargets);
        $scope.filteredAnnualTargets=_.filter($scope.annualTargets,function (t) {
             return parseInt(t.long_term_target.plan_chain_id,10) === parseInt($scope.planChainId,10);
        });

        $scope.selectedPlanChain = planChain;

    };



    $scope.create = function (selectedPlanChain) {
        //Modal Form for creating annualTarget

        var modalInstance = $uibModal.open({
            templateUrl: 'create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,CreateAnnualTargetService,  PlanChainService, LongTermTargetsNoAnnualService) {

                PlanChainService.query({}, function (data) {
                    $scope.planChains =data;
                });

                LongTermTargetsNoAnnualService.query({}, function (data) {

                    $scope.filteredLongTermTargets=_.where(data,{plan_chain_id:selectedPlanChain.id});
                });

                $scope.loadTargetCode=function () {
                var id = $scope.annualTargetToCreate.long_term_target_id;
                var ltt=_.where($scope.filteredLongTermTargets,{id:id});
                $scope.annualTargetToCreate.code = ltt[0]['code'];
                };

                $scope.annualTargetToCreate = {};
                //$scope.objective=selectedPlanChain.id;
               // $scope.annualTargetToCreate.code = selectedPlanChain.code;
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createAnnualTargetForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateAnnualTargetService.store($scope.annualTargetToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage=error.data.errorMessage;
                        }
                    );

                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage=data.successMessage;
                $scope.annualTargets=data.annualTargets;
                $scope.loadAnnualTargets($scope.selectedPlanChain);

            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (annualTargetToEdit, selectedPlanChain) {
        console.log(annualTargetToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: 'edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdateAnnualTargetService, PlanChainService, LongTermTargetsService) {

                PlanChainService.query({}, function (data) {
                    $scope.planChains =data;
                });

                LongTermTargetsService.query({}, function (data) {

                    $scope.filteredLongTermTargets=_.where(data,{id:annualTargetToEdit.long_term_target_id});
                });

                $scope.loadTargetCode=function () {
                    var id = $scope.annualTargetToEdit.long_term_target_id;
                    var ltt=_.where($scope.filteredLongTermTargets,{id:id});
                    $scope.annualTargetToEdit.code = ltt[0]['code'];
                };


                $scope.annualTargetToEdit = angular.copy(annualTargetToEdit);
                // $scope.annualTargetToEdit.start_date=new Date($scope.annualTargetToEdit.start_date);
                // $scope.annualTargetToEdit.end_date=new Date($scope.annualTargetToEdit.end_date);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    $uibModalInstance.close($scope.annualTargetToEdit);
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (annualTargetToEdit) {
                //Service to create new financial year
                UpdateAnnualTargetService.update({},annualTargetToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage=data.successMessage;
                        $scope.annualTargets = data.annualTargets; //After save return all financial years and update $scope.annualTargets
                        $scope.loadAnnualTargets( $scope.selectedPlanChain);
                    },
                    function (error) {

                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog(
            'Confirm Delete AnnualTarget!',
            'Are sure you want to delete this?')
            .then(function () {
                    console.log("YES");
                    DeleteAnnualTargetService.delete({annualTargetId: id},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            //$scope.message = "The financial year has been deleted successfully";
                            $scope.annualTargets = data.annualTargets;
                            $scope.loadAnnualTargets( $scope.selectedPlanChain);
                        }, function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    };

}

AnnualTargetController.resolve = {
    AnnualTargetsModel: function (AnnualTargetsService, $q) {
        var deferred = $q.defer();
        AnnualTargetsService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    },
    lowestPlanChainType:function ($q,LowestPlanChainsService) {
        var deferred=$q.defer();
        LowestPlanChainsService.query(function (data) {
            deferred.resolve(data[0]);
        });
        return deferred.promise;
    }
};