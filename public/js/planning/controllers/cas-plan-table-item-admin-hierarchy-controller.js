function CasPlanTableItemAdminHierarchyController($scope,
    $uibModal,
    CasPlanService,
    CasPlanTabularTableService,
    CreateCasPlanTableItemAdminHierarchyService,
    CasPlanTableItemByTableService,
    CasPlanTableItemAdminHierarchyModel,
    CasPlanTableItemAdminHierarchyService,
    ConfirmDialogService) {

  $scope.casPlans = CasPlanTableItemAdminHierarchyModel;
  $scope.title = "TITLE_COMPREHENSIVE_PLAN_TABLE_ITEM_SETUP";
  $scope.currentPage = 1;

  $scope.loadTables = function (cas_plan_id) {
    CasPlanTabularTableService.query({cas_plan_id: cas_plan_id},
        function (data) {
          $scope.casPlanTables = data;
        }
        );
  };

  $scope.loadItems = function (cas_plan_table_id) {
    CasPlanTableItemByTableService.query({cas_plan_table_id: cas_plan_table_id},
        function (data) {
          $scope.casPlanItems = data;
        }
        );
    //load default items
    CasPlanTableItemAdminHierarchyService.query({cas_plan_table_id: cas_plan_table_id},
        function (data) {
          $scope.casPlanItemValues = data;
        }
        );
  };

  $scope.store = function () {

    $scope.casPlanTableItems = {cas_plan_table_id: $scope.selectedTable, items: $scope.items};

    CreateCasPlanTableItemAdminHierarchyService.store($scope.casPlanTableItems, function (data) {
      $scope.successMessage = data.successMessage;
      $scope.casPlanItemValues   = data.casPlanItemValues;
      console.log($scope.casPlanItems);
    });

  };

  $scope.edit = function (item) {
    var modalInstance = $uibModal.open({
      templateUrl: '/pages/planning/cas_plan_table_item_admin_hierarchy/edit.html',
      backdrop: false,
      controller: function ($scope, $uibModalInstance, UpdateFunderService) {
        $scope.item = angular.copy(item);
        //Function to store data and close modal when Create button clicked
        $scope.update = function () {
          if ($scope.item.sort_order) {
            $scope.formHasErrors = true;
            return;
          }
          UpdateCasPlanTableItemOrderService.update($scope.item,
              function (data) {
                //Successful function when
                $uibModalInstance.close(data);
              },
              function (error) {
                $scope.errorMessage = error.data.errorMessage;
              }
              );

        };
        //Function to close modal when cancel button clicked
        $scope.close = function () {
          $uibModalInstance.dismiss('cancel');
        };
      }
    });
    //Called when modal is close by cancel or to store data
    modalInstance.result.then(function (data) {
      $scope.successMessage = data.successMessage;
      $scope.casPlanItemValues = data.casPlanItemValues;
    },
    function () {
      //If modal is closed
      console.log('Modal dismissed at: ' + new Date());
    });
  };

}

CasPlanTableItemAdminHierarchyController.resolve = {
  CasPlanTableItemAdminHierarchyModel: function (CasPlanService, $q) {
    var deferred = $q.defer();
    CasPlanService.query({}, function (data) {
      deferred.resolve(data);
    });
    return deferred.promise;
  }
};
