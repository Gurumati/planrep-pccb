function ComprehensivePlansController($scope,
    $uibModal,
    $location,
    Upload,
    $timeout,
    CasPlansModel,
    CasPlanContentsService,
    CasPlanTableByContentService,
    CasPlanTableColumnByTableService,
    CasPlanTableItemByTableService,
    CasPlanTableDetailsService,
    LoadCasPlanTableItemValueService,
    localStorageService,
    GetFacilityBySectorService,
    BaselineStatisticValuesService,
    CasPlanTableSelectFacilityListService,
    CasPlanTableSelectOptionItemsService,
    casPlanTableIsEditableService,
    PostCasPlanTableItemValueService,
    FacilityCountService,
    casPlanPeriodsService,
    casPlanSubmissionService,
    AllFinancialYearService,
    GlobalService,
    ConfirmDialogService,
    PostCasPlanTableItemConstantValueService) {

$scope.casPlans = CasPlansModel;
$scope.title = "TITLE_COMPREHENSIVE_PLANS";
$scope.destinationFolder = 'uploads/comprehensive_plans';
$scope.fieldValue=[];
$scope.fieldError = [];
$scope.fieldData =[];
$scope.casPlanItemValues = {};
$scope.dataConstants    = {};
$scope.facility_count = 0;
$scope.verticalTotal = [];

$('#planTable').hide();

AllFinancialYearService.query(function (data) {
    $scope.financialYears = data;
});


$scope.loadContents = function (cas_plan_id) {
  CasPlanContentsService.query({cas_plan_id: cas_plan_id},function (data) {
    $scope.casPlanContents = data;
    $('#planTable').hide();
    $('#planContents').show();
  });

  //check if facility
  if($scope.selectedPlan.is_facility)
  {
    $scope.isEditable = false;
    //load facility
    var user = angular.fromJson(localStorageService.get(localStorageKeys.USER));
    var sector_id = user['sector'].id;
    var admin_hierarchy_id = user['admin_hierarchy_id'];
    GetFacilityBySectorService.query({sector_id: sector_id, council: admin_hierarchy_id}, function(data){
      $scope.facilities = data;
    });
  }
  //end

  //check if the plan is editable
  casPlanTableIsEditableService.get({casPlanId: cas_plan_id},function(data){
    $scope.isEditable = data.status;
  });

  //get number of facilities
  FacilityCountService.facilityCount({},function(data){
    $scope.facility_count = data.count;
  });

};

$scope.loadSelectedPlanForm = function (selectedPlanTable) {
  $scope.lowerColumns     = [];
  $scope.secondColumns    = [];
  $scope.firstColumns     = [];
  $scope.columns          = [];
  $scope.rows             = [];
  $scope.rounds           = [];
  $scope.is_facility_list = false;
  $scope.dimension        = 1;
  $scope.HasverticalTotal = false;

//flatter Column function
  $scope.flattenColumn = function(arrayData)
  {
  var opt_groups = [];
  //find no of rows
  angular.forEach(arrayData, function (value,key) {
   angular.forEach(value, function (value2,key2) {
    if($scope.dimension < 2) {
       $scope.dimension = 2;
    }
    angular.forEach(value2, function (value3, key3) {
       $scope.dimension = 3;
    });
   });
  }) ;
  //end

  angular.forEach(arrayData,function(value,key){
    var colspan = value.children.length;
    if (colspan == 0) {
      var rowspan = $scope.dimension;
      colspan = 1;
    }else {
      var rowspan = 1;
    }

  //check if is list
  if(value.is_list && key == 0){
    $scope.is_facility_list = true;
    $scope.getList(value.list_item);
  }

  if(value.is_lowest == true) {
    $scope.columns.push({id: value.id, type: value.type, formula: value.formula, code: value.code, is_list: value.is_list,
          vertical_total: value.vertical_total, opt_group_id: value.cas_plan_table_select_option_id,
          is_constant: value.is_constant, admin_hierarchy_level_id: value.admin_hierarchy_level_id}) ;

    if(value.vertical_total){
      $scope.HasverticalTotal = true;
    }
    if(value.cas_plan_table_select_option_id !== null){
      opt_groups.push(value.cas_plan_table_select_option_id);
    }
  }

  $scope.firstColumns.push({id: value.id, name: value.name, type: value.type, rowspan: rowspan, colspan: colspan});

  angular.forEach(value.children, function (value2, key2) {
    var colspan2 = value2.children.length;
    if (colspan2 == 0) {
      var rowspan = $scope.dimension -1 ;
      colspan2 = 1;
    }else {
      var rowspan = 1;
    }
    if(value2.is_lowest == true) {
      $scope.columns.push({id: value2.id, type: value2.type, formula: value2.formula,code: value2.code, is_list: value2.is_list,
                vertical_total: value2.vertical_total,opt_group_id: value2.cas_plan_table_select_option_id,
                is_constant: value2.is_constant, admin_hierarchy_level_id: value2.admin_hierarchy_level_id}) ;

      if(value2.vertical_total){
        $scope.HasverticalTotal = true;
      }
      if(value2.cas_plan_table_select_option_id !== null){
      opt_groups.push(value2.cas_plan_table_select_option_id);
      }
    }
    $scope.firstColumns[$scope.firstColumns.length -1].colspan = colspan * colspan2;
    $scope.secondColumns.push({id: value2.id, name: value2.name, colspan: colspan2,rowspan: rowspan, type: value2.type});

  angular.forEach(value2.children, function (value3, key3) {
    $scope.lowerColumns.push({id: value3.id, name: value3.name, type: value3.type});
    $scope.columns.push({id: value3.id, type: value3.type, formula: value3.formula, code: value3.code,is_list: value3.is_list,
              vertical_total: value3.vertical_total, opt_group_id: value2.cas_plan_table_select_option_id,
              is_constant: value3.is_constant, admin_hierarchy_level_id: value3.admin_hierarchy_level_id}) ;

    if(value3.vertical_total){
      $scope.HasverticalTotal = true;
    }
    if(value3.cas_plan_table_select_option_id !== null){
      opt_groups.push(value3.cas_plan_table_select_option_id);
    }
  });

  });
});

  //get all select options
  if(opt_groups.length > 0){
    CasPlanTableSelectOptionItemsService.query({group_id: opt_groups},function (data) {
      $scope.selectOptions = data;
    });
  }

};
//end flatten column

//get list
$scope.getList = function (list_item) {
  if(list_item == 'Facility')
  {
    CasPlanTableSelectFacilityListService.query({},function (data) {
      $scope.Itemlists = data;
    });
    $scope.facilities = [];
    $scope.getRounds($scope.facility_count);
  }else {
    $scope.getRounds(25);
  }

};

$scope.addRound = function (x) {
  $scope.rounds.push(x);

};

$scope.getRounds = function (limit) {
  for(var i=0; i < limit; i++)
  {
    $scope.rounds.push(i);
    $scope.init(i);
  }

};
//end
//flatter row function
  $scope.flattenRow = function (arrayData) {

//find no of rows
angular.forEach(arrayData, function (value,key) {
  angular.forEach(value.children, function (value2,key2) {
    if($scope.dimension < 2) {
      $scope.dimension = 2;
    }
  angular.forEach(value2.children, function (value3, key3) {
      $scope.dimension = 3;
    });
  });
}) ;
//end
$scope.dimensionRow = $scope.dimension;
angular.forEach(arrayData,function(value,key){
  var rowspan = value.children.length;
  var colspan = $scope.dimension;
  if (rowspan == 0) {
    rowspan = 1;
  }else {
    colspan = 1;
  }

  var row = {id: value.id, name: value.description, colspan: colspan, rowspan: rowspan };
  $scope.rows.push(row);
  angular.forEach(value.children, function (value2, key2) {
    var rowspan2 = value2.children.length;
    var colspan2 = 1;
    if (rowspan2 == 0) {
      rowspan2 = 1;
      var row2 = {id: value2.id,name: value2.description, colspan: colspan2, rowspan: rowspan2};
      if(key2 == 0){
         $scope.rows[$scope.rows.length -1 ].children = row2;
      }else
      {
         $scope.rows.push(row2);
      }

    }else {
      colspan2 = 1;
      var row2 = {id: value2.id, name: value2.description, colspan: colspan2, rowspan: rowspan2};
    }

    angular.forEach(value2.children, function (value3, key3) {
    var colspan3 = 1;
    var rowspan3 = 1;
    var row3 = {id: value3.id,name: value3.description, colspan: colspan3, rowspan: rowspan3 };
    if(key3 == 0)
    {
      $scope.rows[$scope.rows.length -1 ].children = row3;
    }else{
      $scope.rows.push(row3);
    }
    });
  });
});
};
//end flatter row
//load columns
  CasPlanTableColumnByTableService.query({cas_plan_table_id:selectedPlanTable},
  function (data) {
    $scope.flattenColumn(data);
  });
//load Rows
  CasPlanTableItemByTableService.query({cas_plan_table_id:selectedPlanTable},
  function (data) {
    $scope.dimension = 1; //reset dimension
    $scope.flattenRow(data);
    $scope.planTableItems = data;
  });

};

$scope.defaultTable = null;
$scope.period_id = null;
$scope.$watchGroup(['defaultTable','period_id'], function() {
  if($scope.defaultTable != null)
  {
    $scope.description = '';
    $scope.singleTypeTable = false;
    $scope.Itemlists = [];

    $scope.loadSelectedPlanForm($scope.defaultTable);
    //check if facility is selected
    if($scope.selectedPlan.is_facility){
      $scope.facility = $scope.facility;
    }else{
       $scope.facility = 0;
    }
    if($scope.is_periodic && $scope.period_id != null){
      $("#save_button").prop('disabled',false);
    }else {
      $scope.period_id = null;
    }
    //load default values
    $scope.loadSavedValues = function(table_id,financial_year_id){
      LoadCasPlanTableItemValueService.get({table_id:table_id, facility: $scope.facility,
            period_id: $scope.period_id,financial_year_id:financial_year_id},
          function (data) {
            $scope.dataConstants         = data.dataConstants;
            $scope.admin_hierarchy_level = data.admin_hierarchy_level;
            $scope.fieldValue            = data.casPlanItemValues;
            if($scope.selectedPlan.is_facility == false){
              $scope.facilities            = data.facilities;
            }

            if($scope.rounds.length == 15){
              var l = Object.keys($scope.fieldValue).length;
              if(l > 15){
                for(var i = 15; i < l; i++){
                  $scope.addRound(i);
                }
              }
            }

            $scope.loadInitialValue();
          }
      );
    }

    //single or default table display
    $scope.singleValuedTable($scope.defaultTable);
  }
});

$scope.changePlanTable = function (cas_plan_table_id) {
  $scope.defaultTable = cas_plan_table_id;
};

//load baseline statistics data
BaselineStatisticValuesService.query({}, function (data) {
  $scope.baselineStatisticValues = data;
});
//end

$scope.getBaselineData =function (id) {
  var r = false;

  angular.forEach($scope.baselineStatisticValues, function (value, key) {
    if(id == value.baseline_statistic_id){
      r = value.value;
    }
  });

  return r;
};
$scope.loadFinancialYearPeriods = function(cas_plan_table_id,financialYearId){
  $scope.loadSavedValues(cas_plan_table_id,financialYearId);
  GlobalService.financialYearPeriods({ financialYearId: financialYearId }, function (data) {
    $scope.financialYearPeriods = data.items;
  }, function (error) {

  });
}
$scope.loadPlanTables = function (cas_plan_content_id) {
  CasPlanTableByContentService.get({cas_plan_content_id: cas_plan_content_id},function (resp) {
      var data = resp.cas_plan_table;
    $scope.planTables = data;
$scope.isPeriodic = data[0].is_periodic;
      /**load periods */
      if($scope.planTables[0].id){
        $scope.is_periodic = true;
        $scope.loadPeriods(data[0].id);
        $("#save_button").prop('disabled',true);
      }else {
        $scope.is_periodic = false;
        $("#save_button").prop('disabled',false);
      }
      if($scope.planTables[0].report_url !== null && $scope.planTables[0].type !== 2){
        // var url = 'https://'+$location.$$host+':'+$location.$$port+'/report#!/cas-preview';
        var url = 'http://'+$location.$$host+':'+$location.$$port+'/report#!/cas-preview';
        window.open(url,'_blank');
      } else {
        $scope.defaultTable  = data[0].id;
        $('#planContents').hide();
        $('#planTable').show();
      }
  } );

};

$scope.getInitialValue = function (row, column){

  angular.forEach($scope.casPlanItemValues, function (value, key) {
    //check if it is a number
    if ( (!isNaN(value.value) && angular.isNumber(+value.value))) {
      value.value = parseInt(value.value);
    }

    if($scope.is_facility_list){
      if(value.cas_plan_table_column_id == column && value.sort_order == row)
      {
        if(value.facility_id !== null){
          $scope.facilities[row] = value.facility_id;
        }
        $scope.fieldValue[row][column] =value.value;
        $scope.fieldError[row][column] = false;
      }
    }else {
      if(value.cas_plan_table_column_id == column && value.cas_plan_table_item_id == row){
        $scope.fieldValue[row][column] =value.value;
        $scope.fieldError[row][column] = false;
      }
    }
  });
};

$scope.getConstant = function (row, column){
  if($scope.fieldValue[row] == undefined){
      $scope.fieldValue[row] = [];
  }
  angular.forEach( $scope.dataConstants, function (value, key) {
    if(value.cas_plan_table_column_id == column && value.cas_plan_table_item_id == row){
      $scope.fieldValue[row][column] = value.value;
      $scope.fieldError[row][column] = false;
    }
  });
};

$scope.loadInitialValue = function () {

  if($scope.is_facility_list == false) {
    angular.forEach($scope.rows, function (value, key) {
      angular.forEach($scope.columns, function (value2, key2) {
        if(value2.is_constant){
          $scope.getConstant(value.id,value2.id);
        }
      });
    });
  }else{
    angular.forEach($scope.rounds, function (value, key) {
      angular.forEach($scope.columns, function (value2, key2) {
        if(value2.is_constant){
          $scope.getConstant(value,value2.id);
        }
      });
    });
  }

};

$scope.getInitialFieldValue = function (row, obj_column) {
  if(obj_column.is_constant){
    $scope.getConstant(row, obj_column.id);
  }else{
    $scope.getInitialValue(row,obj_column.id);
  }
};

$scope.getFieldValue = function (row, column) {

if($scope.fieldValue !== undefined){
     //get formula
  var formula  = column.formula;
  //check if there is vertical total
  var patt = /SUM:ID[0-9]+/mg;
  if(formula != null)
  {
    var result = formula.match(patt);
    angular.forEach(result, function (value, key) {
      var col = value.replace('SUM:ID','');
      if( $scope.fieldValue[row] !== undefined ){
        var x = $scope.getVerticalTotal(col);
        formula = formula.replace(value, x);
      }else{
        formula = formula.replace(value, 0);
      }
    });
  }
  //end vertical total
  //check for basic statistics data
  patt = /BS[0-9]+/mg;
  if(formula != null)
  {
    var result = formula.match(patt);

     angular.forEach(result, function (value, key) {
      var col = value.replace('BS','');
      var x = $scope.getBaselineData(col);
      if(x == false){
        return 'Error baseline!!';
      }else {
        formula = formula.replace(value, x);
      }
    });
  }
  //end
  patt = /ID[0-9]+/mg;
  if(formula != null){
    var result = formula.match(patt);

    angular.forEach(result, function (value, key) {
      var col = value.replace('ID','');
      if($scope.fieldValue[row] !== undefined){
        var x = $scope.fieldValue[row][col];
        formula = formula.replace(value, x);
      }else{
        formula = formula.replace(value, 0);
      }
    });

    try {
      var returnValue =  $scope.$eval(formula);

      if(returnValue > 0){
        if($.isNumeric(returnValue))
        {
          if(returnValue % 1 != 0)
          {
            returnValue = returnValue.toFixed(2);
          }
        }
        $scope.fieldValue[row][column.id] = returnValue;
        return returnValue;
      }else {
        return '';
      }
     } catch (e) {
      //error
    }

  }
}

};

$scope.getVerticalTotal = function (column) {

  var result = 0;
  if($scope.rounds.length > 0){
    angular.forEach($scope.rounds, function (value, key) {
      if($scope.fieldValue.length >0){
        if($scope.fieldValue[value] !== undefined){
          var x = $scope.fieldValue[value][column];
          if(angular.isNumber($scope.$eval(x))) {
            if(x % 1 !== 0)
            {
              result += parseFloat(x);
            }else
            {
              result += parseInt(x);
            }
          }
          if(column.type == 'Formula'){
            if(!isNaN(x))
            {
              result += parseFloat(x);
            }
          }
        }

      }
    });
  }else
  {
    angular.forEach($scope.rows, function (value, key) {
     if($scope.fieldValue !== undefined){
        if($scope.fieldValue[value.id] !== undefined){
          var x = $scope.fieldValue[value.id][column];
          if(angular.isNumber($scope.$eval(x))){
            if(x % 1 !== 0)
            {
              result += parseFloat(x);
            }else
            {
              result += parseInt(x);
            }
          }
          if(column.type == 'Formula'){
            if(!isNaN(x))
            {
              result += parseFloat(x);
            }
          }
        }
     }
    });
  }

  return result;
};

//clean empty array
$scope.cleanData = function (array) {
  var result = [];
  angular.forEach(array, function (value, key) {
    if(value !== null)
    {
      angular.forEach(value, function (value2, key2) {
        result.push({row_id: key, column_id: key2, value: value2});
      });
    }
  });
  return result;
};
//end
//check if data entered is constant
$scope.isConstant = function () {
  var x = false;
  angular.forEach($scope.columns, function (value, key) {
    if(value.is_constant == true && value.admin_hierarchy_level_id == $scope.admin_hierarchy_level)
    {
      x = true;
    }
  });
  return x;
};
//end
$scope.store = function (period) {
  $scope.fieldData = $scope.cleanData($scope.fieldValue);
  //check if data is for facility
  if($scope.selectedPlan.is_facility)
  {
    $scope.facility = $scope.facility;
  }else
  {
    $scope.facility = 0;
  }

  var mydata = {is_facility_list : $scope.is_facility_list, facilities: $scope.facilities,
                inputFields: $scope.fieldData, facility: $scope.facility,
    financial_year_id:$scope.financial_year_id, period_id : $scope.period_id, table_id : $scope.defaultTable};
  if($scope.isConstant())
  {
    PostCasPlanTableItemConstantValueService.store(mydata,
    function (data) {
      $scope.successMessage = data.successMessage;
    });
  }else
  {
    PostCasPlanTableItemValueService.store(mydata,
    function (data) {
      $scope.successMessage = data.successMessage;
    });
  }
};

$scope.viewDetails = function(selectedPlanTable, facility, period_id){
var modalInstance = $uibModal.open({
templateUrl: '/pages/planning/comprehensive_plans/table_details.html',
backdrop: false,
controller: function ($scope, $uibModalInstance,CreateCasPlanTableDetailsService, EditCasPlanTableDetailsService) {
if(facility == undefined)
{
  facility = 0;
}
var user = angular.fromJson(localStorageService.get(localStorageKeys.USER));
var admin_hierarchy_id = user['admin_hierarchy_id'];
CasPlanTableDetailsService.query({cas_plan_table_id: selectedPlanTable,
facility: facility, admin_hierarchy_id: admin_hierarchy_id, period_id: period_id},function (data) {
//Capture data from the cas_plan_table_details table
$scope.casPlanTableDetails = data[0];

checkEditCreate();
});

var checkEditCreate = function(){

if ($scope.casPlanTableDetails !== undefined){
//The data is not empty therefore create the model to edit data
$scope.editDetailsMode = true;
$scope.createDetailsMode = false;
$scope.casPlanTableDetailsToEdit = angular.copy($scope.casPlanTableDetails);

} else {
//The data is empty therefore create the model to create data
$scope.editDetailsMode = false;
$scope.createDetailsMode = true;
$scope.casPlanTableDetailsToCreate = {};
$scope.casPlanTableDetailsToCreate.cas_plan_table_id = selectedPlanTable;
}
};


//The function to store comments
$scope.store = function () {
if ($scope.createCasPlanTableDetailsForm.$invalid) {
$scope.formHasErrors = true;
return;
}
$scope.casPlanTableDetailsToCreate.cas_plan_table_id = selectedPlanTable;

$scope.casPlanTableDetailsToCreate.facility  = facility;
$scope.casPlanTableDetailsToCreate.period_id = period_id;
$scope.casPlanTableDetailsToCreate.financial_year_id = $scope.financial_year_id;



CreateCasPlanTableDetailsService.store($scope.casPlanTableDetailsToCreate,
function (data) {
  $scope.successMessage = data.successMessage;
    $timeout($scope.close(), 30000);
},
function (error) {
$scope.errorMessage = error.data.errorMessage;
}
);
};
//The store function ends here
//The function to update the data in to the database when the store button is clicked
    $scope.update = function () {
        $scope.casPlanTableDetailsToEdit.facility = facility;
        EditCasPlanTableDetailsService.update({id:selectedPlanTable}, $scope.casPlanTableDetailsToEdit,
            function (data) {
                $scope.successMessage = data.successMessage;
                $timeout($scope.close(), 30000);

            },
            function (error) {
                $scope.errorMessage = error.data.errorMessage;
            }
        );
    }
    //The update function ends here
//Function to close modal when cancel button clicked
$scope.close = function () {
  $uibModalInstance.dismiss('cancel');
 };
}
});
};

$scope.singleValuedTable = function (selectedPlanTable) {
angular.forEach($scope.planTables, function (value, key) {
if(value.id == selectedPlanTable && value.type==1)
{
$scope.singleTypeTable = true;
$scope.report_url = value.report_url;
}
});
if($scope.singleTypeTable)
{
//load url
$scope.loadUrl(selectedPlanTable);
}
};

$scope.uploadFile = function (file) {
if($scope.isPeriodic && $scope.period_id == null){
  $scope.errorMessage = "Period was not selected.";
}else {
  if(file == undefined)
  {
  $scope.invalidFileInput = true;
  }else
  {
  if($scope.facility == undefined){
  $scope.facility = 0;
  }
  $scope.invalidFileInput = false; //reset error msg
  file.upload = Upload.upload({
  url: '/comprehensive_plans/upload', //route to controller in laravel
  method: 'POST',
  data: {plan_table_id: $scope.defaultTable, description: $scope.description, facility: $scope.facility,
    financial_year_id: $scope.financial_year_id, period_id: $scope.period_id, file: file}
  });


  file.upload.then(function (response) {
    $timeout(function () {
      file.result = response.data;
      $scope.successMessage = response.data.successMessage;
    });
    }, function (response) {
      if (response.status > 0){
        $scope.errorMsg = response.status + ': ' + response.data;
        $scope.errorMessage = response.data.errorMessage;
        $scope.successMessage = response.data.successMessage;
        $scope.loadUrl($scope.defaultTable);
      }
    }, function (evt) {
      // Math.min is to fix IE which reports 200% sometimes
      file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
    });
  }
}
};

$scope.loadUrl = function (cas_plan_table_id) {
  if($scope.facility == undefined){
    $scope.facility = 0;
  }
  var user = angular.fromJson(localStorageService.get(localStorageKeys.USER));
  var admin_hierarchy_id = user['admin_hierarchy_id'];
  CasPlanTableDetailsService.query({cas_plan_table_id: cas_plan_table_id,
  facility: $scope.facility, admin_hierarchy_id: admin_hierarchy_id, period_id: $scope.period_id, financial_year_id: $scope.financial_year_id},function (data) {
    $scope.casPlanTableUrl = data[0];
    if(data.length > 0)
    {
      $scope.description  = $scope.casPlanTableUrl.description;
    }else
    {
      $scope.description  = '';
    }
  });
};

$scope.backToContent = function () {
 $scope.loadContents($scope.selectedPlan.id);
};

//validate inputs
$scope.inputIsNumber = function (row, column) {
  var val = $scope.fieldValue[row][column];
  $scope.fieldError[row][column] = false;

  if ( !(!isNaN(val) && angular.isNumber(+val))) {
    //flag error
    $scope.fieldError[row][column] = true;
    alert("Enter numbers only!!");
    $("#save_button").prop('disabled',true);
  }else {
    $("#save_button").prop('disabled',false);
  }
};

$scope.init = function (x) {
$scope.fieldValue[x]=[];
$scope.fieldError[x]=[];
};

//get data
$scope.loadFacilityByType = function () {
  $scope.facilityByType = [];
  angular.forEach($scope.facilities,function(value,key){
    if(value.facility_type_id == $scope.facility_type)
    {
      $scope.facilityByType.push(value);
    }
  });
};

  $scope.loadAssessment = function(selectedPlan){

   var financial_year_type = selectedPlan.financial_year_type;
   var cas_plan = selectedPlan.id;

   ConfirmDialogService.showConfirmDialog('Confirm Submission!', 'Are you sure you want to submit a plan?').then(function () {
      casPlanSubmissionService.save({financial_year_type : financial_year_type, cas_plan : cas_plan}, function(data){
          $scope.loadContents(cas_plan);
          $scope.successMessage = data.successMessage;
        }, function(error){
          $scope.errorMessage = error.errorMessage;
      });
    },
    function () {
    });

  }

$scope.loadPeriods = function(table_id){
  /** load periods */
  casPlanPeriodsService.query({table_id: table_id}, function(data){
    $scope.periods = data;
  });
};
}

ComprehensivePlansController.resolve = {
  CasPlansModel: function (CasPlanService, $q) {
    var deferred = $q.defer();
    CasPlanService.query({}, function (data) {
      deferred.resolve(data);
    });
   return deferred.promise;
  }
};
