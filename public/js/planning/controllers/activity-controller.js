(function () {
  "use strict";

  angular
    .module("planning-module")
    .controller("ActivityController", ActivityController);
  ActivityController.$inject = [
    "$scope",
    "$uibModal",
    "$timeout",
    "FacilityService",
    "AnnualTarget",
    "$route",
    "DeleteActivityService",
    "ConfirmDialogService",
    "CommentDialogService",
    "$q",
    "Activity",
    "$location",
    "$window",
  ];

  function ActivityController(
    $scope,
    $uibModal,
    $timeout,
    FacilityService,
    AnnualTarget,
    $route,
    DeleteActivityService,
    ConfirmDialogService,
    CommentDialogService,
    $q,
    Activity,
    $location,
    $window
  ) {
    $scope.title = "Activities";

    $scope.selectedToReallocationActivity = [];
    $scope.queryParams = $location.search();
    $scope.routeParams = $route.current.params;

    $scope.activitiesLoading = false;
    $scope.planLoaded = false;
    $scope.planningDataLoaded = false;
    var selectedFacilityIds = [];

    $scope.onBudgetInfoLoaded = function (budgetInfo) {
      $scope.plan = budgetInfo;
      loadTargets();
      getHomeFacilityAndSuperviedFacilityTypes();
      $scope.planLoaded = true;
    };
    //Facility Filter functions start
    var allSupervisedFacilities = {
      id: "%",
      name: "All Facilities",
    };

    $scope.hasToAddress = function (comments) {
      var notAddressed = _.where(comments, { addressed: false });
      return notAddressed.length === 0 ? false : true;
    };

    $scope.addressComments = function (type, title, comments) {
      CommentDialogService.showDialog(type, title, comments).then(function () {
        if (type === "MTEF_SECTION") {
          hasComments();
        }
        if (type === "ACTIVITY") {
          //  $scope.pageChanged();
        }
      });
    };

    var getHomeFacilityAndSuperviedFacilityTypes = function () {
      FacilityService.getHomeFacilityAndSuperviedFacilityTypes(
        {
          adminHierarchyId: $scope.plan.mtef.admin_hierarchy_id,
          sectionId: $scope.plan.section_id,
        },
        function (data) {
          $scope.facilities = data;
          $scope.isHomeFacility = false;
          if (data.homeFacility != null) {
            $scope.isHomeFacility = true;
            selectedFacilityIds.push(data.homeFacility.id);
          }
          if (data.supervisedFacilityTypes.length) {
            allSupervisedFacilities.type = data.supervisedFacilityTypes[0].name;
          }
        }
      );
    };

    $scope.isHomeFacilityChange = function () {
      selectedFacilityIds = [];
      if ($scope.isHomeFacility) {
        selectedFacilityIds.push($scope.facilities.homeFacility.id);
      } else {
        $scope.selectedSupervisedFacility = allSupervisedFacilities;
      }
      getActivitiesByTargetAndFacility();
    };

    $scope.searchFacility = function ($query) {
      var deferred = $q.defer();
      if (
        $query !== null &&
        $query !== undefined &&
        $query !== "" &&
        $query.length >= 1
      ) {
        $scope.facilityLoading = true;
        var facilityToExclude = {
          facilityIdsToExclude: [],
        };
        FacilityService.searchByPlanningSection(
          {
            mtefSectionId: $scope.routeParams.mtefSectionId,
            isFacilityAccount: true,
            searchQuery: $query,
            noLoader: true,
          },
          facilityToExclude,
          function (data) {
            $scope.facilityLoading = false;
            // data.facilities.unshift(allSupervisedFacilities);
            deferred.resolve(data.facilities);
          },
          function (error) {
            console.log(error);
            deferred.reject("Error");
          }
        );
      } else {
        deferred.resolve([
          {
            id: 0,
            name: "All",
            type: "Facilities",
          },
        ]);
      }
      return deferred.promise;
    };

    $scope.supervisedFacilityChanged = function (facility) {
      selectedFacilityIds = [];
      if (facility !== null && facility !== undefined) {
        selectedFacilityIds.push(facility.id);
        $scope.selectedSupervisedFacility = facility;
      }
      // else{
      //     selectedFacilityIds.push(allSupervisedFacilities.id);
      //     $scope.selectedSupervisedFacility = allSupervisedFacilities;
      // }
      getActivitiesByTargetAndFacility();
    };

    //Facility Filter end

    //Plan Chain Filter Functions Start here
    $scope.planChainChanged = function (selectedPlanChainId) {
      $scope.selectedPlanChainId = selectedPlanChainId;
      loadTargets();
    };
    //Plan Chain Filter end

    //Targer Filter Function Start
    var loadTargets = function () {
      if ($scope.selectedPlanChainId === undefined) {
        return;
      }
      $scope.allAnnualTargets = [];
      $scope.targetIsLoading = true;
      AnnualTarget.byMtefSectionAndPlanChain(
        {
          mtefId: $scope.plan.mtef.id,
          mtefSectionId: $scope.plan.id,
          budgetType: $scope.routeParams.budgetType,
          planChainId: $scope.selectedPlanChainId,
        },
        function (data) {
          $scope.targetIsLoading = false;
          $scope.allAnnualTargets = data.annualTargets;
          if ($scope.queryParams.targetId !== undefined) {
            $scope.selectedTarget = _.findWhere($scope.allAnnualTargets, {
              id: parseInt($scope.queryParams.targetId, 10),
            });
            if ($scope.selectedTarget) {
              getActivitiesByTargetAndFacility();
            }
          }
        },
        function (error) {
          $scope.targetIsLoading = false;
          $scope.errorMessage = error.data.errorMessage;
          $scope.planningDataLoaded = true;
        }
      );
    };

    $scope.targetChanged = function () {
      $location.search("targetId", $scope.selectedTarget.id);
      getActivitiesByTargetAndFacility();
    };
    //Target Filter end

    //Activities Function Start
    var getActivitiesByTargetAndFacility = function () {
      // $scope.selectedTargetActivitiesPage = undefined;
      //    $scope.seletedTargetActivities = [];
      if ($scope.selectedTarget === undefined) {
        return;
      }
      $scope.activitiesLoading = true;
      Activity.paginate(
        {
          mtefSectionId: $scope.plan.id,
          budgetType: $scope.routeParams.budgetType,
          targetId: $scope.selectedTarget.id,
          page: $scope.currentPage,
          perPage: $scope.perPage,
          searchQuery: $scope.searchQuery,
          noLoader: true,
        },
        {
          selectedFacilityIds: selectedFacilityIds,
        },
        function (data) {
          $scope.activitiesLoading = false;
          $scope.seletedTargetActivities = data.activities.data;
          $scope.selectedTargetActivitiesPage = data.activities;
        },
        function (error) {
          console.log(error);
          $scope.activitiesLoading = false;
          $scope.errorMessage = error.data.errorMessage;
        }
      );
    };

    $scope.pageChanged = function () {
      getActivitiesByTargetAndFacility();
    };

    $scope.searchQueryChanged = function () {
      _.debounce(getActivitiesByTargetAndFacility(), 1000);
    };

    $scope.hasPeriod = function (a, id) {
      var exist = _.where(a.periods, {
        period_id: id,
      });
      if (exist.length > 0) {
        return true;
      } else {
        return false;
      }
    };

    $scope.showActivityDetails = function (code) {
      $scope.showActivityCode = code;
    };
    $scope.hideActivityDetails = function () {
      $scope.showActivityCode = undefined;
    };

    $scope.setSelectedCode = function (code) {
      $scope.showActivityCode = code;
    };

    $scope.refreshCode = function (a) {
      a.isRefreshingCode = true;
      Activity.refreshCode(
        {
          id: a.id,
        },
        function (data) {
          a.code = data.updated.code;
          a.isRefreshingCode = false;
        },
        function (error) {
          a.isRefreshingCode = false;
          console.log(error);
          $scope.errorMessage = error.data.errorMessage;
        }
      );
    };

    //Activities end
    $scope.goBack = function () {
      $window.location =
        "planning#!/planning-targets/" +
        $scope.routeParams.mtefSectionId +
        "/" +
        $scope.routeParams.budgetType;
    };
    $scope.loadInputs = function (url) {
      $window.location = url;
    };
    $scope.clearMessage = function () {
      $timeout(function () {
        $scope.successMessage = undefined;
        $scope.errorMessage = undefined;
      }, 2000);
    };

    $scope.continueToReallocationInput = function () {
      var url =
        "/budgeting#!/inputs/" +
        $scope.plan.id +
        "?planType=REALLOCATION&facility=all&isPopup=true&fundSourceId=" +
        $scope.queryParams.fundSourceId +
        "&activityIds=" +
        JSON.stringify($scope.selectedToReallocationActivity);

      $window.location = url;
    };

    $scope.toggleSelectedReallocatedToActivity = function (id) {
      var idx = $scope.selectedToReallocationActivity.indexOf(id);
      if (idx === -1) {
        $scope.selectedToReallocationActivity.push(id);
      } else {
        $scope.selectedToReallocationActivity.splice(idx, 1);
      }
    };

    $scope.createOrUpdateActivity = function (activity) {
      var modalInstance = $uibModal.open({
        templateUrl: "/pages/planning/partials/create-or-update-activity.html",
        backdrop: false,
        resolve: {
          facility: $scope.isHomeFacility
            ? $scope.facilities.homeFacility
            : $scope.selectedSupervisedFacility,
          selectedTarget: $scope.selectedTarget,
          activity: activity,
          params: {
            financialYearId: $scope.plan.mtef.financial_year_id,
            budgetType: $scope.routeParams.budgetType,
            adminHierarchyId: $scope.plan.mtef.admin_hierarchy_id,
            sectionId: $scope.plan.section.id,
            mtefSectionId: $scope.plan.id,
            fundSourceId: $scope.queryParams.fundSourceId,
            isHomeFacility: $scope.isHomeFacility,
            isFacilityAccount:
              !$scope.isHomeFacility ||
                ($scope.isHomeFacility && $scope.isHomeFacility.isFacilityAccount)
                ? true
                : false,
          },
          planningData: function ($q, PlanningService) {
            var deferred = $q.defer();
            PlanningService.get(
              {
                mtefSectionId: $scope.plan.id,
                targetId: $scope.selectedTarget.long_term_target.id,
                fundSourceId: $scope.queryParams.fundSourceId,
                budgetType: $scope.routeParams.budgetType,
              },
              function (data) {
                deferred.resolve(data);
              },
              function (error) {
                deferred.reject(error);
              }
            );
            return deferred.promise;
          },
          priorityAreas: function ($q, PriorityArea) {
            var deferred = $q.defer();
            PriorityArea.withInterventionAndProblems(
              {
                linkLevel: 2,
                planChainId:
                  $scope.selectedTarget.long_term_target.plan_chain_id,
                financialYearId: $scope.plan.mtef.financial_year_id,
                adminHierarchyId: $scope.plan.mtef.admin_hierarchy_id,
                sectionId: $scope.plan.section.id,
              },
              function (data) {
                deferred.resolve(data.priorityWithInterventionAndProblems);
              },
              function (error) {
                console.log(error);
                deferred.reject(error);
              }
            );
            return deferred.promise;
          },
        },
        controller: "CreateOrUpdateActivityController",
      });
      modalInstance.result.then(
        function (data) {
          $scope.addOrRemoveActivityFacility(data.activity);
          $scope.successMessage = data.successMessage;
          $scope.clearMessage();
          loadTargets();
        },
        function () {
          console.log("Modal dismissed at: " + new Date());
        }
      );

    };

    $scope.addOrRemoveActivityFacility = function (activity) {
      var modalInstance = $uibModal.open({
        templateUrl:
          "/pages/planning/partials/add-or-remove-activity-facility.html",
        backdrop: false,
        resolve: {
          activity: activity,
          facility: $scope.isHomeFacility
            ? $scope.facilities.homeFacility
            : $scope.selectedSupervisedFacility,
          params: {
            sectionId: $scope.plan.section_id,
            budgetType: $scope.routeParams.budgetType,
            isFinal: $scope.plan.is_locked,
            isLocked: $scope.plan.mtef.is_final,
            isHomeFacility: $scope.isHomeFacility,
            financialYearId: $scope.plan.mtef.financial_year_id,
          },
        },
        controller: "AddOrRemoveActivityFacilityController",
      });
      modalInstance.result.then(
        function (a) {
          if ($scope.queryParams.realoc === undefined) {
            $scope.pageChanged();
            return;
          }
          $scope.loadInputs(
            "/budgeting#!/activity-facility-fund-sources/" +
            $scope.routeParams.mtefSectionId +
            "/" +
            $scope.routeParams.budgetType +
            "?searchQuery=" +
            a.code +
            "&isPopup=true&realoc=true"
          );
        },
        function () {
          $scope.pageChanged();
          console.log("Modal dismissed at: " + new Date());
        }
      );
    };

    $scope.delete = function (id) {
      var mtefSectionId = $route.current.params.mtefSectionId;
      if (!mtefSectionId) {
        mtefSectionId = 0;
      }
      ConfirmDialogService.showConfirmDialog(
        "TITLE_CONFIRM_ACTIVITY",
        "CONFIRM_DELETE"
      ).then(
        function () {
          DeleteActivityService.delete(
            {
              activityId: id,
              mtefSectionId: mtefSectionId,
              planType: $scope.queryParams.planType,
              financialYear: $scope.queryParams.financialYear,
              facility: $scope.queryParams.facility,
            },
            function (data) {
              $scope.successMessage = data.successMessage;
              $scope.clearMessage();
              loadTargets();
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        },
        function () { }
      );
    };

    $scope.disApprove = function (id) {
      ConfirmDialogService.showConfirmDialog(
        "Disapprove activity",
        "Confirm disapprove activity"
      ).then(
        function () {
          Activity.disApprove(
            {
              id: id,
            },
            function (data) {
              $scope.successMessage = data.successMessage;
              $scope.clearMessage();
              loadTargets();
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        },
        function () { }
      );
    };
    $scope.toggleFacilityAccount = function (a) {
      Activity.toggleFacilityAccount(
        { id: a.id, isFacilityAccount: a.is_facility_account },
        function (data) { },
        function (error) {
          $scope.errorMessage = error.data.errorMessage;
        }
      );
    };
    $scope.approve = function (id) {
      ConfirmDialogService.showConfirmDialog(
        "Approve activity",
        "Confirm approve activity"
      ).then(
        function () {
          Activity.approve(
            {
              id: id,
            },
            function (data) {
              $scope.successMessage = data.successMessage;
              $scope.clearMessage();
              loadTargets();
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        },
        function () { }
      );
    };
  }
})();
