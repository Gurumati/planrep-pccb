(function () {
    'use strict';

    angular.module('planning-module').controller('PlanningTargetController',PlanningTargetController);
    PlanningTargetController.$inject = ['$scope','$location','$window','AnnualTarget', '$route','selectedPlanChain','CofogService'];

    function PlanningTargetController($scope,$location, $window,AnnualTarget, $route,selectedPlanChain,CofogService) {

    $scope.title = "Financial year plan targets";
    $scope.selectedToReallocationActivity = [];
    $scope.queryParams = $location.search();
    $scope.currentPage = ($scope.queryParams.page !== undefined)?$scope.queryParams.page : 1;
    $scope.routeParams = $route.current.params;
    $scope.planLoaded = false;

    $scope.onBudgetInfoLoaded = function (budgetInfo) {
        $scope.plan = budgetInfo;
        loadTargets();
        $scope.planLoaded = true;
    };

    var loadTargets = function () {
        $scope.targetIsLoading = true;
        AnnualTarget.byMtefSectionPaginated(
            {
                mtefId : $scope.plan.mtef.id,
                mtefSectionId : $scope.plan.id,
                budgetType : $scope.routeParams.budgetType,
                noLoader : true,
                perPage : $scope.perPage,
                page: $scope.currentPage
            },
            function (data) {
                $scope.planningDataLoaded = true;
                $scope.allAnnualTargets = data;
                $scope.targetIsLoading = false;
            }, function (error) {
                $scope.planningDataLoaded = true;
                $scope.targetIsLoading = false;

            });
    };
    $scope.loadActivities = function (t) {
         var url = '/planning#!/activities/'+$scope.routeParams.mtefSectionId+'/'+$scope.routeParams.budgetType+'/?targetId='+t.id+'&targetPage='+$scope.currentPage;
        selectedPlanChain.set(t.long_term_target.plan_chain.parent,1);
        selectedPlanChain.set(t.long_term_target.plan_chain,2);
        $window.location = url;

        // console.log(t.long_term_target_id);
        // CofogService.getCofogIdFromLongTermTargetReference(t.long_term_target_id,function(data){
        //     console.log("Our services")
        //     console.log(data)
        // })
    };

    $scope.pageChanged =function () {
       // $location.search('page',$scope.currentPage);
        loadTargets();
    };
}
})();

