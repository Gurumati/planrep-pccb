function MtefAnnualTargetController($scope, MtefAnnualTargetModel, $uibModal,UpdateMtefAnnualTargetService,ConfirmDialogService,
    DeleteMtefAnnualTargetService,MtefAnnualTargetsPaginationService) {

    $scope.mtefAnnualTargets = MtefAnnualTargetModel;
    $scope.title = "MTEF_ANNUAL_TARGETS";
    $scope.dateFormat = 'yyyy-MM-dd';
    $scope.currentPage = 1;

    $scope.pageChanged = function () {
        MtefAnnualTargetsPaginationService.get({
            page: $scope.currentPage
        }, function (data) {
            $scope.mtefAnnualTargets = data;
        });
    };

    $scope.create = function () {
        //Modal Form for creating annualTarget

        var modalInstance = $uibModal.open({
            templateUrl: 'create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateMtefAnnualTargetService, LongTermTargetService, PlanChainService) {

                LongTermTargetService.query({}, function (data) {
                    $scope.longTermTargets = data;
                });

                PlanChainService.query({}, function (data) {
                    $scope.planChains = data;
                });


                $scope.mtefAnnualTargetToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createMtefAnnualTargetForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateMtefAnnualTargetService.store($scope.mtefAnnualTargetToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );

                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.mtefAnnualTargets = data.mtefAnnualTargets;

            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (mtefAnnualTargetToEdit, currentPage) {
        console.log(mtefAnnualTargetToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: 'edit.html',
            backdrop: false,

            controller: function ($scope, $uibModalInstance, CreateMtefAnnualTargetService, LongTermTargetService, PlanChainService) {

                LongTermTargetService.query({}, function (data) {
                    $scope.longTermTargets = data;
                });

                PlanChainService.query({}, function (data) {
                    $scope.planChains = data;
                });

                $scope.mtefAnnualTargetToEdit = angular.copy(mtefAnnualTargetToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    $uibModalInstance.close($scope.mtefAnnualTargetToEdit);
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (mtefAnnualTargetToEdit) {
                //Service to create new financial year
                UpdateMtefAnnualTargetService.update({
                        page: currentPage
                    }, mtefAnnualTargetToEdit,
                    function (data) {
                        //Successful function when
                        $scope.successMessage = data.successMessage;
                        $scope.mtefAnnualTargets = data.mtefAnnualTargets;
                    },
                    function (error) {

                    }
                );
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id) {
        ConfirmDialogService.showConfirmDialog(
                'Confirm Delete AnnualTarget!',
                'Are sure you want to delete this?')
            .then(function () {
                    console.log("YES");
                    DeleteMtefAnnualTargetService.delete({
                            mtefAnnualTargetId: id
                        },
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            //$scope.message = "The financial year has been deleted successfully";
                            $scope.mtefAnnualTargets = data.mtefAnnualTargets;
                        },
                        function (error) {

                        }
                    );
                },
                function () {
                    console.log("NO");
                });
    }

}

MtefAnnualTargetController.resolve = {
    MtefAnnualTargetModel: function (MtefAnnualTargetsPaginationService, $q) {
        var deferred = $q.defer();
        MtefAnnualTargetsPaginationService.get({
            page: 1
        }, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};