/**
 * Controller for listing cost centres which budget
 */
(function () {
    'use strict';

    angular.module('planning-module').controller('PlanningController',PlanningController);
    PlanningController.$inject     =  ['$scope','$route','$window','Plan'];

    function PlanningController($scope,$route,$window,Plan) {

        $scope.title = "TITLE_PLANS";
        $scope.stage = $route.current.params.stage;
        $scope.openPlan = function (url) {
            $window.location = url;
        };

        /**
         * @description
         * Main filter changed set scoped filter variables and load plans
         * @param filter {Object} {selectedBudgetType, selectedFinancialYearId, selectedAdminHierarchyId,selectedSectionId}
         */
        $scope.filterChanged = function (filter) {
            $scope.selectedFinancialYearId = filter.selectedFinancialYearId;
            $scope.selectedBudgetType = filter.selectedBudgetType;
            $scope.selectedAdminHierarchyId = filter.selectedAdminHierarchyId;
            $scope.selectedSectionId = filter.selectedSectionId;
            $scope.searchQuery = filter.searchQuery;
          loadPlans();
        };

        /**
         * @description
         * Load Plans by budgeType, financialYearId, adminHierarchyId,sectionId and searchQuery
         */
        function loadPlans() {
            $scope.planIsLoading = true;
            if( $scope.selectedBudgetType === undefined ||
                $scope.selectedFinancialYearId === undefined ||
                $scope.selectedAdminHierarchyId === undefined ||
                $scope.selectedSectionId === undefined){
                return;
            }
            Plan.filter({
                budgetType: $scope.selectedBudgetType,
                financialYearId: $scope.selectedFinancialYearId,
                adminHierarchyId: $scope.selectedAdminHierarchyId,
                sectionId : $scope.selectedSectionId,
                searchQuery : $scope.searchQuery
            },function (data) {
                $scope.plans = data.plans;
                $scope.planIsLoading = false;
            },function (error) {
                $scope.erroMessage = error.data.errorMessage;
                $scope.planIsLoading = false;
            });
        }

    }
})();


