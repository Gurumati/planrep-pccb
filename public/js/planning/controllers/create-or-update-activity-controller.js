(function () {
  "use strict";

  angular
    .module("planning-module")
    .controller(
      "CreateOrUpdateActivityController",
      CreateOrUpdateActivityController
    );

  CreateOrUpdateActivityController.$inject = [
    "$scope",
    "$uibModalInstance",
    "facility",
    "selectedTarget",
    "activity",
    "params",
    "planningData",
    "priorityAreas",
    "PlanningService",
    "Activity",
    "ResponsiblePersonsService",
    "CreateResponsiblePersonService",
    "GenericActivity",
  ];

  function CreateOrUpdateActivityController(
    $scope,
    $uibModalInstance,
    facility,
    selectedTarget,
    activity,
    params,
    planningData,
    priorityAreas,
    PlanningService,
    Activity,
    ResponsiblePersonsService,
    CreateResponsiblePersonService,
    GenericActivity
  ) {
    $scope.facility = facility;
    $scope.isHomeFacility = params.isHomeFacility;
    $scope.indicatorValue = /^[0-9]+$/;
    $scope.mobileValid = /^[0-9]{10}$/;

    $scope.periods =
      params.budgetType === "CARRYOVER"
        ? [planningData.periods[0],planningData.periods[1],planningData.periods[2],planningData.periods[3]]
        : planningData.periods;
    $scope.budgetClasses = planningData.budgetClasses;
    $scope.activityCategory = planningData.activityCategory;
    $scope.taskNatures = planningData.taskNatures;
    $scope.referenceTypes = planningData.referenceTypes;
    $scope.cofogs = planningData.cofogs;
    $scope.surplusCategories = planningData.surplusCategories;
    $scope.piscType = planningData.piscType.code;

    $scope.cleared = true;
    $scope.addNewResponsible = false;
    $scope.selectedTarget = selectedTarget;
    $scope.genericLoaded = false;
    $scope.paramSet = false;
    $scope.priorityWithInterventionAndProblems = priorityAreas;

    $scope.isFacilityAccount = params.isFacilityAccount;

    $scope.loadResponsiblePerson = function () {
      ResponsiblePersonsService.getByAdminAreaAndSector(
        {
          adminHierarchyId: params.adminHierarchyId,
          sectionId: params.sectionId,
        },
        function (data) {
          $scope.responsiblePersons = data.responsiblePersons;
        }
      );
    };

    $scope.loadBudgetClasses = function (budgetClass) {
      $scope.childBudgetClasses = budgetClass;
    };
    $scope.loadTaskNature = function (id) {
      $scope.taskNaturesByCategory = _.where($scope.taskNatures, {
        activity_category_id: id,
      });
    };

    $scope.loadProjects = function () {
      $scope.projectLoading = true;
      PlanningService.getProjects(
        {
          budgetClassId: $scope.activityToCreate.budget_class_id,
          sectionId: params.sectionId,
          noLoader: true,
        },
        function (data) {
          $scope.projects = data.projects;
          $scope.projectLoading = false;
        },
        function (error) {
          $scope.projectLoading = false;
        }
      );
    };
    $scope.createResponsiblePerson = function () {
      if ($scope.responsiblePersonToCreateForm.$invalid) {
        $scope.userFormHasErrors = true;
        return;
      }
      $scope.responsiblePersonToCreate.section_id = params.sectionId;
      CreateResponsiblePersonService.store(
        {},
        $scope.responsiblePersonToCreate,
        function (data) {
          $scope.addNewResponsible = false;
          $scope.userCreated = true;
          $scope.responsiblePersonToCreate = undefined;
          $scope.userFormHasErrors = false;
          $scope.loadResponsiblePerson();
        },
        function (error) {
          $scope.data = error.data.errorInfo;
          $scope.errorMessage2 = $scope.data.substr(error.data.errorInfo);
          $scope.createUserError = true;
          $scope.userFormHasErrors = false;
        }
      );
    };

    $scope.closeUserForm = function () {
      $scope.addNewResponsible = false;
      $scope.userFormHasErrors = false;
    };

    if (activity === undefined) {
      $scope.activityToCreate = {
        mtef_annual_target_id: selectedTarget.id,
        mtef_section_id: params.mtefSectionId,
        references: [],
        periods: [],
        progress_status: "OPEN",
        budget_type: params.budgetType,
        is_facility_account: params.isFacilityAccount,
      };

      $scope.editMode = false;
      $scope.genericLoading = true;

      GenericActivity.getByTarget(
        {
          targetId: selectedTarget.id,
        },
        function (data) {
          $scope.genericActivities = data.genericActivities;
          $scope.genericLoaded = true;
          $scope.genericLoading = false;
          if (
            $scope.genericActivities !== undefined ||
            $scope.genericActivities.length === 0
          ) {
            $scope.paramSet = true;
          }
        },
        function (error) {
          $scope.genericLoading = false;
          $scope.genericLoaded = true;
        }
      );
    } else {
      $scope.genericLoaded = true;
      $scope.paramSet = true;
      var originalActivity = angular.copy(activity);
      $scope.activityToCreate = activity;
      // let referencesdata = JSON.stringify(activity.references);
      // $scope.someData = JSON.parse(referencesdata);
      // console.log("someData")
      // console.log($scope.someData)

      var budgetClassSelected = _.findWhere($scope.budgetClasses, {
        id: activity.budget_class.parent_id,
      });
      $scope.activityToCreate.budget_class_parent = budgetClassSelected;
      if (budgetClassSelected) {
        $scope.loadBudgetClasses(budgetClassSelected.children);
        $scope.loadProjects();
      }

      $scope.loadTaskNature(activity.activity_category_id);
      $scope.activityToCreate.budget_class_id =
        originalActivity.budget_class_id;

      if (activity.intervention) {
        $scope.activityToCreate.priority_area = _.findWhere(
          $scope.priorityWithInterventionAndProblems,
          {
            id: activity.intervention.priority_area_id,
          }
        );
      }
      $scope.editMode = true;
    }

    $scope.loadResponsiblePerson();

    $scope.showParam = function (selectedActivity) {
      $scope.selectedActivityParams = [];

      var params = selectedActivity.params.split(",");

      angular.forEach(params, function (p) {
        var x = {};
        x.name = p;
        x.value = undefined;
        $scope.selectedActivityParams.push(x);
      });
    };

    $scope.createFromTemplate = function (template, params) {
      var description = template.description;

      angular.forEach(params, function (p) {
        var replace = p.name;
        var re = new RegExp(replace, "g");
        description = description.replace(re, p.value);
      });

      $scope.activityToCreate.description = description;

      if (
        template.priority_area !== undefined &&
        template.priority_area !== null
      ) {
        setSelectedPriorityAreas(template.priority_area);
      }
      if (
        template.intervention !== undefined &&
        template.intervention !== null
      ) {
        setSelectedIntervention(template.intervention);
      }
      if (
        template.generic_sector_problem !== undefined &&
        template.generic_sector_problem !== null
      ) {
        setSelectedSectorProblem(template.generic_sector_problem);
      }
      if (
        template.budget_class !== undefined &&
        template.budget_class !== null
      ) {
        setSelectedBudgetClass(template.budget_class);
      }

      $scope.activityToCreate.generic_activity_id = template.id;
      $scope.paramSet = true;
    };
    var setSelectedPriorityAreas = function (priorityArea) {
      if ($scope.priorityWithInterventionAndProblems.length > 0) {
        $scope.activityToCreate.priority_area = priorityArea;
      }
    };
    var setSelectedIntervention = function (intervention) {
      var priorityArea = _.findWhere(
        $scope.priorityWithInterventionAndProblems,
        {
          id: intervention.priority_area_id,
        }
      );
      $scope.activityToCreate.priority_area = priorityArea;
      if (priorityArea) {
        $scope.activityToCreate.intervention = intervention;
        $scope.activityToCreate.sector_problem_id = _.findWhere(
          priorityArea.sector_problems,
          {
            generic_sector_problem_id: intervention,
          }
        ).id;
      }
    };
    var setSelectedSectorProblem = function (generic_sector_problem) {
      var priorityArea = _.findWhere(
        $scope.priorityWithInterventionAndProblems,
        {
          id: generic_sector_problem.priority_area_id,
        }
      );
      if (priorityArea) {
        $scope.activityToCreate.sector_problem_id = _.findWhere(
          priorityArea.sector_problems,
          {
            generic_sector_problem_id: generic_sector_problem.id,
          }
        ).id;
      }
    };
    var setSelectedBudgetClass = function (budgetClass) {
      var budgetClassSelected = _.findWhere($scope.budgetClasses, {
        id: budgetClass.parent_id,
      });

      $scope.activityToCreate.budget_class_parent = budgetClassSelected;

      if (budgetClassSelected) {
        $scope.loadBudgetClasses(budgetClassSelected.children);
      }

      $scope.activityToCreate.budget_class_id = budgetClass.id;
    };
    $scope.getInitReferences = function (refType) {
       return _.findWhere($scope.activityToCreate.references, {
        reference_type_id: refType.id,
      });
    };
    $scope.getInitPriority = function (priorityAreaId) {
      return _.findWhere($scope.priorityWithInterventionAndProblems, {
        id: priorityAreaId,
      });
    };

    $scope.createOrUpdate = function () {
      if ($scope.createActivityForm.$invalid) {
        $scope.formHasErrors = true;
        return;
      }
      Activity.createOrUpdate(
        {
          financialYearId: params.financialYearId,
          budgetType: params.budgetType,
          adminHierarchyId: params.adminHierarchyId,
          sectionId: params.sectionId,
        },
        Object.assign({}, $scope.activityToCreate, {
          budget_class_parent: undefined,
          annual_target: undefined,
          budget_class: undefined,
          activity_category: undefined,
          project: undefined,
        }),

        function (data) {
          $uibModalInstance.close(data);
        },
        function (error) {
          $scope.errorMessage = error.data.errorMessage;
          $scope.errors = error.data.errors;
        }
      );
    };

    $scope.hasPeriod = function (id) {
      var exist = _.where($scope.activityToCreate.periods, {
        period_id: id,
      });
      if (exist.length > 0) {
        return true;
      } else {
        return false;
      }
    };
    $scope.togglePeriodSelection = function toggleSelection(p) {
      Array.prototype.getIndexBy = function (name, value) {
        for (var i = 0; i < this.length; i++) {
          if (this[i][name] === value) {
            return i;
          }
        }
        return -1;
      };
      var x = {
        activity_id: null,
        period_id: p.id,
      };
      var idx = $scope.activityToCreate.periods.getIndexBy("period_id", p.id);

      // Is currently selected
      if (idx > -1) {
        $scope.activityToCreate.periods.splice(idx, 1);
      }
      // Is newly selected
      else {
        $scope.activityToCreate.periods.push(x);
      }
    };
    $scope.close = function () {
      $uibModalInstance.dismiss("cancel");
    };
  }
})();
