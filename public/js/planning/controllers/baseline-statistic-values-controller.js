function BaselineStatisticValuesController($scope,
                                           FinancialYearModel,
                                           $uibModal,
                                           CreateBaselineStatisticValuesService,
                                           ConfirmDialogService,
                                           BaselineStatisticValuesService,
                                           BaselineStatisticsService
                                           ) {
  $scope.financialYears = FinancialYearModel;
    //$scope.baselineDataValues = BaselineStatisticValuesModel;
    $scope.title = "BASELINE_DATA_VALUES";

    $scope.loadBaselineDataValues = function(financialYearId){
      BaselineStatisticsService.query({financialYearId:financialYearId},function (data) {
        $scope.baselineDataValues = data;
        $scope.getBaselineDataValues();
      });
    }
    $scope.getBaselineDataValues = function () {
        angular.forEach($scope.baselineDataValues, function (value, key) {
            angular.forEach($scope.baselineStatisticValues, function (value2,key2) {
                if  (value2.baseline_statistic_id === value.id){
                    if(value.is_common)
                    {
                        value.value = value.default_value;
                    }else
                    {
                        value.value = value2.value;
                    }

                }
            });
        });

    };


    $scope.saveValues = function(){
        $scope.baselineDataValuesToCreate = angular.copy($scope.baselineDataValues);
        CreateBaselineStatisticValuesService.store($scope.baselineDataValuesToCreate,
            function (data) {
                $scope.successMessage = data.successMessage;
                // console.log(data);
            },
            function (error) {
                $scope.errorMessage=error.data.errorMessage;
            }
        );
    };

}

BaselineStatisticValuesController.resolve = {
    // BaselineStatisticValuesModel: function (BaselineStatisticsService, $q) {
    //     var deferred = $q.defer();
    //     BaselineStatisticsService.query({type:'BLD'},function (data) {
    //          console.log(data);
    //          deferred.resolve(data);
    //     });
    //     return deferred.promise;
    // },
  FinancialYearModel: function (AllFinancialYearService, $q) {
    let deferred = $q.defer();
    AllFinancialYearService.query(function (data) {
      deferred.resolve(data);
    });
    return deferred.promise;
  }
};
