(function () {
  "use strict";

  angular
    .module("planning-module")
    .controller("LongTermTargetController", LongTermTargetController);
  LongTermTargetController.$inject = [
    "$scope",
    "$location",
    "$route",
    "Target",
    "$uibModal",
    "AnnualTarget",
    "ReferenceTypeWithReferencesService",
    "ConfirmDialogService",
    "ReferenceDocumentService",
    "DeleteLongTermTargetService",
    "LowestPlanChainsService",
    "PlanChainService",
    "AllFinancialYearService",
    "LongTermTargetsService",
  ];

  function LongTermTargetController(
    $scope,
    $location,
    $route,
    Target,
    $uibModal,
    AnnualTarget,
    ReferenceTypeWithReferencesService,
    ConfirmDialogService,
    ReferenceDocumentService,
    DeleteLongTermTargetService,
    LowestPlanChainsService,
    PlanChainService,
    AllFinancialYearService,
    LongTermTargetsService
  ) {
    $scope.title = "LONG_TERM_TARGET";
    $scope.dateFormat = "yyyy-MM-dd";

    $scope.queryParams = $location.search();
    $scope.currentPage = ($scope.queryParams.page !== undefined)?$scope.queryParams.page : 1;
    $scope.routeParams = $route.current.params;

    AllFinancialYearService.query(function (data) {
      $scope.financialYears = data;
    });

    LowestPlanChainsService.query(function (data) {
      $scope.planChainLoaded = true;
      $scope.lowestPlanChainType = data[0];
    });

    ReferenceDocumentService.query({}, function (data) {
      $scope.referenceDocLoaded = true;
      $scope.referenceDocuments = data;
    });

    ReferenceTypeWithReferencesService.query(
      {
        linkLevel: "TARGET",
      },
      function (data) {
        $scope.referenceTypes = data;
      }
    );

    $scope.getSize = function (code) {
      return code.length;
    };
    $scope.refreshCode = function (t) {
      t.isRefreshingCode = true;
      LongTermTargetsService.refreshCode(
        {
          id: t.id,
        },
        function (data) {
          t.code = data.code;
          t.isRefreshingCode = false;
        },
        function (error) {
          t.isRefreshingCode = false;
          $scope.errorMessage = error.data.errorMessage;
        }
      );
    };

    $scope.pageChanged =function () {
      $scope.loadTargets();
   };

    $scope.loadTargets = function (planChain) {
      if (planChain === undefined) {
        return;
      }
      Target.paginateByPlanChain(
        {
          planChainId: planChain.id,
          page: $scope.currentPage,
          perPage : $scope.perPage != undefined?$scope.perPage:1000
        },
        function (data) {
          $scope.targets = data;
        },
        function (error) {
          console.log(error);
        }
      );
    };

    $scope.hasFinalAnnualTarget = function (annualTargets, financialYearId) {
      var a = _.findWhere(annualTargets, {
        financial_year_id: financialYearId,
        is_final: true,
      });
      if (a === undefined) {
        return false;
      }
      return true;
    };

    $scope.hasAnnualTargetToFinilize = function (
      annualTargets,
      financialYearId
    ) {
      var a = _.findWhere(annualTargets, {
        financial_year_id: financialYearId,
        is_final: true,
      });
      if (a === undefined) {
        return true;
      }
      return false;
    };

    $scope.confirmOrViewTarget = function (
      annualTargets,
      financialYear,
      longTermTargetId
    ) {
      var target = _.findWhere(annualTargets, {
        financial_year_id: financialYear.id,
      });
      if (target == undefined) {
        AnnualTarget.initiate(
          {
            financialYearId: financialYear.id,
            longTermTargetId: longTermTargetId,
          },
          function (data) {
            confirm(data, financialYear);
          }
        );
      } else {
        confirm(target, financialYear);
      }
    };

    var confirm = function (target, financialYear) {
      var modalInstance = $uibModal.open({
        templateUrl: "/pages/planning/partials/confirm-target.html",
        backdrop: false,
        resolve: {
          target: target,
          financialYear: financialYear,
        },
        controller: function (
          $scope,
          $uibModalInstance,
          target,
          financialYear,
          AnnualTarget
        ) {
          $scope.target = target;
          $scope.financialYear = financialYear;

          $scope.save = function () {
            AnnualTarget.confirm(
              {
                id: $scope.target.id,
              },
              $scope.target,
              function (data) {
                $uibModalInstance.close(data);
              }
            );
          };

          $scope.close = function () {
            $uibModalInstance.dismiss("cancel");
          };
        },
      });
      modalInstance.result.then(
        function (data) {
          $scope.loadTargets($scope.planChain);
        },
        function () {
          console.log("Modal dismissed at: " + new Date());
        }
      );
    };

    $scope.create = function (selectedPlanChain, planChains, referenceTypes) {
      var modalInstance = $uibModal.open({
        templateUrl: "/pages/planning/partials/create-long-term-target.html",
        backdrop: false,
        controller: function (
          $scope,
          $uibModalInstance,
          CofogService,
          LongTermTargetsService,
          CreateLongTermTargetService,
          PlanChainSectorOnlyService,
          SectorsService,
          UserCanTargetSectionsService,
          ReferenceDocumentService,
          ActivityCategory,
          TargetTypeService,
          PerformanceIndicators
        ) {
          //target types
          ActivityCategory.query(function (data) {
            $scope.tTypes = data;
          });
         

          $scope.longTermTargetToCreate = {
            references: [],
            indicators: [],
          };
          $scope.referenceTypes = referenceTypes;
          $scope.selectedPlanChain = selectedPlanChain;
          $scope.genericLoaded = false;
          $scope.paramSet = false;

          //get parent cofogs
          CofogService.getParent(function (data) {
            $scope.cofogs = data.cofog;
          });

          $scope.loadIndicators = function (planChainId) {
            PerformanceIndicators.getByPlanChain(
              {
                planChainId: planChainId,
              },
              function (data) {
                $scope.perfomanceIndicators = data.indicators;
              }
            );
          };

          $scope.loadGenerics = function (planChainId) {
            $scope.genericLoaded = false;
            $scope.allThingsLoades = true;
            $scope.paramSet = false;
            $scope.selectedTarget = undefined;
            $scope.longTermTargetToCreate.generic_target_id = undefined;
            LongTermTargetsService.getGenericTarget(
              {
                planChainId: planChainId,
              },
              function (data) {
                $scope.genericTargets = data.genericTargets;
                $scope.genericLoaded = true;
                if ($scope.genericTargets.length > 0) {
                } else {
                  $scope.paramSet = true;
                }

                if ($scope.genericLoaded && $scope.paramSet) {
                  $scope.allThingsLoades = true;
                } else {
                  $scope.allThingsLoades = false;
                }
              }
            );
            $scope.loadIndicators(planChainId);
          };
          $scope.loadGenerics($scope.selectedPlanChain.id);

          $scope.showParam = function (selectedTarget) {
            $scope.temp = selectedTarget.description;
            $scope.tempCopy = angular.copy($scope.temp);
            $scope.selectedTargetParams = [];
            var params = selectedTarget.params.split(",");
            angular.forEach(params, function (p) {
              var x = {};
              x.name = p;
              x.value = undefined;
              $scope.selectedTargetParams.push(x);
            });
          };
          $scope.changeTemp = function (params) {
            var t = angular.copy($scope.tempCopy);
            angular.forEach(params, function (p) {
              if (p.value !== undefined) {
                var replace = p.name;
                var re = new RegExp(replace, "g");
                t = t.replace(re, p.value);
              }
            });
            $scope.temp = t;
          };

          $scope.createFromTemplate = function (template, params) {
            var description = template.description;
            angular.forEach(params, function (p) {
              var replace = p.name;
              var re = new RegExp(replace, "g");
              description = description.replace(re, p.value);
            });
            $scope.longTermTargetToCreate.description = description;
            $scope.longTermTargetToCreate.performance_indicator_id =
              template.performance_indicator_id;
            $scope.longTermTargetToCreate.generic_target_id = template.id;
            $scope.paramSet = true;
          };

          ReferenceDocumentService.query({}, function (data) {
            $scope.referenceDocuments = data;
          });
          UserCanTargetSectionsService.query(function (data) {
            $scope.canTargetSections = data;
            if ($scope.canTargetSections.length === 1)
              $scope.longTermTargetToCreate.section_id =
                $scope.canTargetSections[0].id;
          });

          //   $scope.loadIndicators($scope.selectedPlanChain.id);

          $scope.planChains = planChains;

          TargetTypeService.query({}, function (data) {
            $scope.targetTypes = data;
          });

          $scope.loadTargetCode = function () {
            var id = $scope.longTermTargetToCreate.target_type_id;
          };
          $scope.selection = [];

          PlanChainSectorOnlyService.query({}, function (data) {
            var id = $scope.longTermTargetToCreate.plan_chain_id;
            var planSectors = _.where(data, {
              plan_chain_id: id,
            });
            $scope.selection = [];
            angular.forEach(planSectors, function (value, index) {
              $scope.selection.push(value.sector_id);
            });
          });

          $scope.loadChainCode = function () {
            var id = $scope.longTermTargetToCreate.plan_chain_id;
            var plan_chain = _.where($scope.planChains, {
              id: id,
            });
            $scope.longTermTargetToCreate.code = plan_chain[0]["code"];

            /// $scope.planChains =_.where(data,{parent_id:null});

            PlanChainSectorOnlyService.query({}, function (data) {
              var id = $scope.longTermTargetToCreate.plan_chain_id;
              var planSectors = _.where(data, {
                plan_chain_id: id,
              });
              $scope.selection = [];
              angular.forEach(planSectors, function (value, index) {
                $scope.selection.push(value.sector_id);
              });
            });
          };

          SectorsService.query({}, function (data) {
            $scope.sectors = data;
          });

          // Toggle selection for a given sector by id
          $scope.toggleIndicatorSelection = function (p) {
            var idx = $scope.longTermTargetToCreate.indicators.indexOf(p.id);
            // Is currently selected
            if (idx > -1) {
              $scope.longTermTargetToCreate.indicators.splice(idx, 1);
            }
            // Is newly selected
            else {
              $scope.longTermTargetToCreate.indicators.push(p.id);
            }
          };

          $scope.selectAll = function () {
            $scope.longTermTargetToCreate.indicators = [];
            angular.forEach($scope.perfomanceIndicators, function (
              value,
              index
            ) {
              $scope.longTermTargetToCreate.indicators.push(value.id);
            });
          };

          $scope.unSelectAll = function () {
            $scope.longTermTargetToCreate.indicators = [];
          };

          $scope.longTermTargetToCreate.plan_chain_id = selectedPlanChain.id;
          $scope.longTermTargetToCreate.code = selectedPlanChain.code;
          //Function to store data and close modal when Create button clicked
          $scope.save = function () {
            $scope.longTermTargetToCreate.sectors = $scope.selection;
            if ($scope.createLongTermTargetForm.$invalid) {
              $scope.formHasErrors = true;
              return;
            }

            CreateLongTermTargetService.store(
              $scope.longTermTargetToCreate,
              function (data) {
                $uibModalInstance.close(data);
              },
              function (error) {
                $scope.errorMessage = error.data.errorMessage;
              }
            );
          };
          $scope.close = function () {
            $uibModalInstance.close();
          };
          //Function to close modal when cancel button clicked
        },
      });
      //Called when modal is close by cancel or to store data
      modalInstance.result.then(
        function (data) {
          //Service to create new financial year
          $scope.successMessage = data.successMessage;
          $scope.loadTargets($scope.planChain);
        },
        function () {
          //If modal is closed
          console.log("Modal dismissed at: " + new Date());
        }
      );
    };

    $scope.edit = function (longTermTargetToEdit, planChains, referenceTypes) {
      var modalInstance = $uibModal.open({
        templateUrl: "/pages/planning/partials/edit-long-term-target.html",
        backdrop: false,
        controller: function (
          $scope,
          $uibModalInstance,
          CofogService,
          UpdateLongTermTargetService,
          PlanChainSectorOnlyService,
          ReferenceDocumentService,
          SectorTargetOnlyService,
          PlanChainService,
          SectorsService,
          TargetTypeService,
          UserCanTargetSectionsService,
          PerformanceIndicators
        ) {
          //  $scope.priorityWithInterventionAndProblems = priorityWithInterventionAndProblems;
          $scope.referenceTypes = referenceTypes;
          longTermTargetToEdit.indicators = [];

          angular.forEach(
            longTermTargetToEdit.performance_indicators,
            function (ind) {
              longTermTargetToEdit.indicators.push(ind.id);
            }
          );

          if (longTermTargetToEdit.intervention) {
            var priorityArea = _.findWhere(
              $scope.priorityWithInterventionAndProblems,
              {
                id: longTermTargetToEdit.intervention.priority_area_id,
              }
            );
            $scope.longTermTargetToEdit = angular.copy(longTermTargetToEdit);
            $scope.longTermTargetToEdit.priority_area = priorityArea;
          }
          $scope.getInitReferences = function (refType) {
            return _.findWhere($scope.longTermTargetToEdit.references, {
              reference_type_id: refType.id,
            });
          };
          $scope.getInitPriority = function (priorityAreaId) {
            return _.findWhere($scope.priorityWithInterventionAndProblems, {
              id: priorityAreaId,
            });
          };
          PerformanceIndicators.getByPlanChain(
            {
              planChainId: longTermTargetToEdit.plan_chain_id,
            },
            function (data) {
              $scope.perfomanceIndicators = data.indicators;
            }
          );

          UserCanTargetSectionsService.query(function (data) {
            $scope.canTargetSections = data;
          });

          ReferenceDocumentService.query({}, function (data) {
            $scope.referenceDocuments = data;
          });
          $scope.planChains = _.where(planChains, {
            id: longTermTargetToEdit.plan_chain_id,
          });
          //get parent cofogs
          CofogService.getParent(function (data) {
            $scope.cofogs = data.cofog;
          });

          TargetTypeService.query({}, function (data) {
            $scope.targetTypes = data;
          });

          SectorsService.query({}, function (data) {
            $scope.sectors = data;
          });

          $scope.selection = [];
          $scope.longTermTargetToEdit = angular.copy(longTermTargetToEdit);

          SectorTargetOnlyService.query({}, function (data) {
            angular.forEach(data, function (value, index) {
              if (value.long_term_target_id == longTermTargetToEdit.id) {
                $scope.selection.push(value.sector_id);
              }
            });
          });

          // Toggle selection for a given sector by id
          $scope.toggleIndicatorSelection = function (p) {
            var idx = $scope.longTermTargetToEdit.indicators.indexOf(p.id);
            // Is currently selected
            if (idx > -1) {
              $scope.longTermTargetToEdit.indicators.splice(idx, 1);
            }
            // Is newly selected
            else {
              $scope.longTermTargetToEdit.indicators.push(p.id);
            }
          };

          $scope.selectAll = function () {
            $scope.longTermTargetToEdit.indicators = [];
            angular.forEach($scope.perfomanceIndicators, function (
              value,
              index
            ) {
              $scope.longTermTargetToEdit.indicators.push(value.id);
            });
          };

          $scope.unSelectAll = function () {
            $scope.longTermTargetToEdit.indicators = [];
          };

          $scope.update = function () {
            if ($scope.editLongTermTargetForm.$invalid) {
              $scope.formHasErrors = true;
              return;
            }
            UpdateLongTermTargetService.update(
              {},
              $scope.longTermTargetToEdit,
              function (data) {
                //Successful function when
                $uibModalInstance.close(data);
              },
              function (error) {
                $scope.erroMessage = error.data.erroMessage;
              }
            );
          };
          //Function to close modal when cancel button clicked
          $scope.close = function () {
            $uibModalInstance.dismiss("cancel");
          };
        },
      });
      //Called when modal is close by cancel or to store data
      modalInstance.result.then(
        function (data) {
          $scope.successMessage = data.successMessage;
          $scope.loadTargets($scope.planChain);
        },
        function () {
          //If modal is closed
          console.log("Modal dismissed at: " + new Date());
        }
      );
    };

    $scope.delete = function (id) {
      ConfirmDialogService.showConfirmDialog(
        "Confirm Delete LongTermTarget!",
        "Are sure you want to delete this?"
      ).then(
        function () {
          DeleteLongTermTargetService.delete(
            {
              longTermTargetId: id,
            },
            function (data) {
              $scope.successMessage = data.successMessage;
              $scope.loadTargets($scope.planChain);
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        },
        function () {
          console.log("NO");
        }
      );
    };
  }
})();
