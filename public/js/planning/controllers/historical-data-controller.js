function HistoricalDataController($scope, AdminHierarchiesService, AllFinancialYearService, $timeout, FinancialYearsService, localStorageService, DataModel, $uibModal, GlobalService, ConfirmDialogService, HistoricalDataService) {

    $scope.historicalDataTables = DataModel;

    $scope.title = "Custom Historical Data";

    $scope.hierarchy_position = localStorageService.get(localStorageKeys.HIERARCHY_POSITION);

    AdminHierarchiesService.getUserAdminHierarchy({}, function (data) {
        $scope.userAdminHierarchy = data;
        $scope.userAdminHierarchyCopy = angular.copy($scope.userAdminHierarchy);
    });

    GlobalService.previousFinancialYears(function (data) {
        $scope.financialYears = data.financialYears;
    });

    GlobalService.sectors(function (data) {
        $scope.sectors = data.sectors;
        $scope.items = {};
    });

    $scope.loadDepartments = function (sector_id) {
        GlobalService.sectorDepartments({sector_id: sector_id}, function (data) {
            $scope.departments = data.departments;
            $scope.items = {};
        });
    };

    $scope.loadCostCentres = function (department_id) {
        GlobalService.departmentCostCentres({department_id: department_id}, function (data) {
            $scope.sections = data.sections;
            $scope.items = {};
        });
    };

    $scope.loadFundSources = function (sector_id, section_id) {
        GlobalService.sectorDepartmentFundSources({
            sector_id: sector_id,
            section_id: section_id
        }, function (data) {
            $scope.fundSources = data.fundSources;
            $scope.items = {};
        });
    };

    $scope.loadRevenueFundSources = function () {
        GlobalService.revenueFundSources(function (data) {
            $scope.fundSources = data.fundSources;
            $scope.items = {};
        });
    };

    $scope.resetYear = function () {
        $scope.dataItems.financial_year_id = {};
        $scope.items = {};
    };

    $scope.resetHierarchy = function (historical_data_table) {
        $scope.userAdminHierarchy = angular.copy($scope.userAdminHierarchyCopy);
        $scope.dataItems = {};
        $scope.items = {};

        if (historical_data_table.type === "REVENUE") {
            GlobalService.revenueFundSources(function (data) {
                $scope.fundSources = data.fundSources;
            });
        }

        if (historical_data_table.type === "EXPENDITURE") {
            $scope.fundSources = {};
        }
    };

    $scope.loadChildren = function (a) {
        var level_id = parseInt(a.admin_hierarchy_level_id);
        if (level_id > 0) {
            if (a.children === undefined) {
                a.children = [];
            }
            AdminHierarchiesService.getAdminHierarchyChildren({adminHierarchyId: a.id}, function (data) {
                a.children = data.adminHierarchies;
            });
        }
    };

    $scope.loadRows = function (admin_hierarchy, historical_data_table, fund_source, sector,department,section) {
        HistoricalDataService.dataRows({
            page: 1,
            perPage: 10,
            historical_data_table_id: historical_data_table.id,
            historical_data_table_type: historical_data_table.type,
            admin_hierarchy_id: admin_hierarchy.id,
            /*financial_year_id: financial_year_id,*/
            fund_source_id: fund_source.id,
            state: 'LOAD',
            section_id: section.id,
            sector_id: sector.id,
        }, function (data) {
            $scope.items = data.items;
            $scope.dataColumns = data.columns;
            console.log($scope.dataItems);
            $scope.dataItems = data.dataItems;
            /*$scope.dataItems.financial_year_id = data.dataItems.financial_year_id;*/
            $scope.dataItems.admin_hierarchy_id = data.dataItems.admin_hierarchy_id;
            $scope.dataItems.sector_id = data.dataItems.sector_id;
            $scope.dataItems.section_id = data.dataItems.section_id;
            $scope.sector_name = data.dataItems.sector_name;
            $scope.department_name = data.dataItems.department_name;
            $scope.council = data.dataItems.council;
            $scope.financial_year_name = data.dataItems.financial_year_name;

            GlobalService.previousFinancialYears(function (data) {
                $scope.financialYears = data.financialYears;
                /* $scope.dataItems.financial_year_id = financial_year_id;*/
                $scope.dataItems.sector = sector;
                $scope.dataItems.department = department;
                $scope.dataItems.section = section;
                $scope.admin_hierarchy = admin_hierarchy;
                $scope.dataItems.fund_source = fund_source;
                $scope.dataItems.admin_hierarchy = admin_hierarchy;
            });

            $scope.columnTotal = function (col) {

                var values = $scope.dataItems.values;
                var total = 0;
                angular.forEach(values, function (columns, row) {
                    angular.forEach(columns, function (value, key) {
                        if (key == col) {

                            total += parseFloat(value);
                        }
                    });
                });
                return total;
            };

        }, function (error) {
            console.log(error);
        });

        $scope.currentPage = 1;
        $scope.perPage = 10;
        $scope.maxSize = 4;

        $scope.pageChanged = function () {
            HistoricalDataService.dataRows({
                page: $scope.currentPage,
                perPage: $scope.perPage,
                historical_data_table_id: historical_data_table.id,
                historical_data_table_type: historical_data_table.type,
                admin_hierarchy_id: admin_hierarchy.id,
                /*financial_year_id: financial_year_id,*/
                fund_source_id: fund_source.id,
                state: 'PAGINATION',
                section_id: section.id,
                sector_id: sector.id,
            }, function (data) {
                $scope.items = data.items;
                $scope.dataColumns = data.columns;
                $scope.dataItems = data.dataItems;
                /*$scope.dataItems.financial_year_id = data.dataItems.financial_year_id;*/
                $scope.dataItems.admin_hierarchy_id = data.dataItems.admin_hierarchy_id;
                $scope.dataItems.sector_id = data.dataItems.sector_id;
                $scope.dataItems.section_id = data.dataItems.section_id;
                $scope.sector_name = data.dataItems.sector_name;
                $scope.department_name = data.dataItems.department_name;
                $scope.council = data.dataItems.council;
                $scope.financial_year_name = data.dataItems.financial_year_name;

                GlobalService.previousFinancialYears(function (data) {
                    $scope.financialYears = data.financialYears;
                    /*$scope.dataItems.financial_year_id = financial_year_id;*/
                    $scope.dataItems.sector = sector;
                    $scope.dataItems.department = department;
                    $scope.dataItems.section = section;
                    $scope.admin_hierarchy = admin_hierarchy;
                    $scope.dataItems.fund_source = fund_source;
                    $scope.dataItems.admin_hierarchy = admin_hierarchy;
                });


                $scope.columnTotal = function (col) {

                    var values = $scope.dataItems.values;
                    var total = 0;
                    angular.forEach(values, function (columns, row) {
                        angular.forEach(columns, function (value, key) {
                            if (key == col) {
                                total += parseFloat(value);
                            }
                        });
                    });
                    return total;
                };

            }, function (error) {
                console.log(error);
            });
        };

        $scope.saveValues = function (currentPage, perPage, searchString) {
            if ($scope.dataForm.$invalid) {
                $scope.formHasErrors = true;
                return;
            }

            HistoricalDataService.saveRowValues({
                historical_data_table_id: historical_data_table.id,
                historical_data_table_type: historical_data_table.type,
                admin_hierarchy_id: admin_hierarchy.id,
                /*financial_year_id: financial_year_id,*/
                fund_source_id: fund_source.id,
                section_id: section.id,
                state: 'SAVE',
                sector_id: sector.id,
                page: currentPage, perPage: perPage,
                searchString: searchString
            }, $scope.dataItems, function (data) {
                $scope.items = data.items;
                $scope.dataColumns = data.columns;
                $scope.dataItems = data.dataItems;
                /*$scope.dataItems.financial_year_id = data.dataItems.financial_year_id;*/
                $scope.dataItems.admin_hierarchy_id = data.dataItems.admin_hierarchy_id;
                $scope.dataItems.sector_id = data.dataItems.sector_id;
                $scope.dataItems.section_id = data.dataItems.section_id;
                $scope.successMessage = data.successMessage;
                $timeout(function () {
                    $scope.successMessage = '';
                }, 5000);
                $scope.currentPage = $scope.items.current_page;
                $scope.sector_name = data.dataItems.sector_name;
                $scope.department_name = data.dataItems.department_name;
                $scope.council = data.dataItems.council;
                $scope.financial_year_name = data.dataItems.financial_year_name;

                GlobalService.previousFinancialYears(function (data) {
                    $scope.financialYears = data.financialYears;
                    /*$scope.dataItems.financial_year_id = financial_year_id;*/
                    $scope.dataItems.sector = sector;
                    $scope.dataItems.department = department;
                    $scope.dataItems.section = section;
                    $scope.admin_hierarchy = admin_hierarchy;
                    $scope.dataItems.fund_source = fund_source;
                    $scope.dataItems.admin_hierarchy = admin_hierarchy;
                });


                $scope.columnTotal = function (col) {

                    var values = $scope.dataItems.values;
                    var total = 0;
                    angular.forEach(values, function (columns, row) {
                        angular.forEach(columns, function (value, key) {
                            if (key == col) {
                                total += parseFloat(value);
                            }
                        });
                    });
                    return total;
                };

            }, function (error) {
                console.log(error);
            });
        };

        $scope.search = function (searchString) {
            HistoricalDataService.dataRows({
                page: 1,
                perPage: 10,
                historical_data_table_id: historical_data_table.id,
                historical_data_table_type: historical_data_table.type,
                admin_hierarchy_id: admin_hierarchy.id,
                /*financial_year_id: financial_year_id,*/
                fund_source_id: fund_source.id,
                state: 'SEARCH',
                sector_id: sector.id,
                section_id: section.id,
                searchString: searchString
            }, function (data) {
                $scope.items = data.items;
                $scope.dataColumns = data.columns;
                $scope.dataItems = data.dataItems;
                /*$scope.dataItems.financial_year_id = data.dataItems.financial_year_id;*/
                $scope.dataItems.admin_hierarchy_id = data.dataItems.admin_hierarchy_id;
                $scope.dataItems.sector_id = data.dataItems.sector_id;
                $scope.dataItems.section_id = data.dataItems.section_id;
                $scope.sector_name = data.dataItems.sector_name;
                $scope.department_name = data.dataItems.department_name;
                $scope.council = data.dataItems.council;
                $scope.financial_year_name = data.dataItems.financial_year_name;

                GlobalService.previousFinancialYears(function (data) {
                    $scope.financialYears = data.financialYears;
                    /*$scope.dataItems.financial_year_id = financial_year_id;*/
                    $scope.dataItems.sector = sector;
                    $scope.dataItems.department = department;
                    $scope.dataItems.section = section;
                    $scope.admin_hierarchy = admin_hierarchy;
                    $scope.dataItems.fund_source = fund_source;
                    $scope.dataItems.admin_hierarchy = admin_hierarchy;
                });


                $scope.columnTotal = function (col) {

                    var values = $scope.dataItems.values;
                    var total = 0;
                    angular.forEach(values, function (columns, row) {
                        angular.forEach(columns, function (value, key) {
                            if (key == col) {

                                total += parseFloat(value);
                            }
                        });
                    });
                    return total;
                };

            }, function (error) {
                console.log(error);
            });
        };
    };
}

HistoricalDataController.resolve = {
    DataModel: function (GlobalService, $q) {
        var deferred = $q.defer();
        GlobalService.historicalDataTables(function (data) {
            deferred.resolve(data.items);
        }, function (error) {
            console.log(error);
        });
        return deferred.promise;
    }
};