function PEContributionController($scope, GetAllPEContributionModel, $uibModal,
                               ConfirmDialogService,
                               DeletePEContributionService,TogglePEContributionService, GetAllPEContributionService) {

    $scope.pe_contributions = GetAllPEContributionModel;
    $scope.title = "TITLE_PE_CONTRIBUTION";
    $scope.dateFormat = 'yyyy-MM-dd';
    $scope.currentPage = 1;

    $scope.pageChanged = function () {
        GetAllPEContributionService.get({page: $scope.currentPage}, function (data) {
            $scope.pe_contributions = data;
        });
    };
    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreatePEContributionService) {
                $scope.pe_contributionToCreate = {};
                //Function to store data and close modal when Create button clicked
                $scope.store = function () {
                    if ($scope.createPEContributionForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreatePEContributionService.store($scope.pe_contributionToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                $scope.successMessage = data.successMessage;
                $scope.pe_contributions = data.pe_contributions;
                $scope.currentPage = data.current_page;

            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (pe_contributionToEdit, currentPage) {
        var modalInstance = $uibModal.open({
            templateUrl: 'edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, UpdatePEContributionService) {
                $scope.pe_contributionToEdit = angular.copy(pe_contributionToEdit);
                //Function to store data and close modal when Create button clicked
                $scope.update = function () {
                    if ($scope.updatePEContributionForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    UpdatePEContributionService.update({page: currentPage}, $scope.pe_contributionToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );


                }
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                console.log(data);
                $scope.successMessage = data.successMessage;
                $scope.pe_contributions = data.pe_contributions;
                $scope.currentPage = $scope.pe_contributions.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };

    $scope.delete = function (id,currentPage) {
        ConfirmDialogService.showConfirmDialog(
            'TITLE_CONFIRM_DELETE_PE_CONTRIBUTION',
            'CONFIRM_DELETE')
            .then(function () {
                    DeletePEContributionService.delete({pe_contribution_id: id,page:currentPage},
                        function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.pe_contributions = data.pe_contributions;
                            $scope.currentPage = $scope.pe_contributions.current_page;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                },
                function () {

                });
    }
    $scope.togglePEContribution = function (pe_contribution_id, active) {
        $scope.pe_contributionToActivate = {};
        $scope.pe_contributionToActivate.id = pe_contribution_id;
        $scope.pe_contributionToActivate.is_active = active;
        TogglePEContributionService.togglePEContribution($scope.pe_contributionToActivate,
            function (data) {
                $scope.action = data.action;
                $scope.alertType = data.alertType;
            });
    }
}

PEContributionController.resolve = {
    GetAllPEContributionModel: function (GetAllPEContributionService, $q) {
        var deferred = $q.defer();
        GetAllPEContributionService.get({page: 1}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};