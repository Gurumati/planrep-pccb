(function () {
    'use strict';

    angular.module('planning-module').controller('PerformanceIndicatorBaselineValueController', PerformanceIndicatorBaselineValueController);
    PerformanceIndicatorBaselineValueController.$inject = ['$scope', 'PerformanceIndicators', 'FinancialYearsService', 'AllDataSourceService'];

    function PerformanceIndicatorBaselineValueController($scope, PerformanceIndicators, FinancialYearsService, AllDataSourceService) {

        $scope.title = "PERFORMANCE_INDICATOR_BASELINE_VALUES";
        $scope.currentPage = 1;
        $scope.maxSize = 3;
        $scope.performanceIndicatorLoaded = false;
        $scope.financialyearloaded = false;
        $scope.perPage = 10;

        FinancialYearsService.forIndicatorProjection({
            noLoader: true
        }, function (data) {
            $scope.financialyearloaded = true;
            $scope.financialYears = data.financialYears;
        });
        AllDataSourceService.query({}, function (data) {
            $scope.dataSourcesLoaded = true;
            $scope.dataSources = data;
        });
        $scope.pageChanged = function () {
            $scope.save(false);
            $scope.loadPerformanceIndicators();
        };
        $scope.enterToSearch = function (keyEvent) {
            if (keyEvent.charCode === 13) {
                $scope.searchIndicator();
            }
        };

        $scope.loadPerformanceIndicators = function () {
            PerformanceIndicators.getForUser({
                searchQuery: $scope.searchQuery,
                page: $scope.currentPage,
                perPage: $scope.perPage
            }, function (data) {
                $scope.performanceIndicatorLoaded = true;
                $scope.items = data;
                $scope.perPage = data.per_page;
            }, function (error) {
                $scope.performanceIndicatorLoaded = true;
                $scope.errorMessage = error.data.errorMessage;
            });
        };
        $scope.loadPerformanceIndicators();
        $scope.searchIndicator = function () {
            $scope.loadPerformanceIndicators();
        };

        $scope.notInFinancialYear = function (startId, endId, f) {
            var start = _.findWhere($scope.financialYears, {
                id: startId
            });
            var end = _.findWhere($scope.financialYears, {
                id: endId
            });
            if (start.start_date <= f.start_date && end.end_date >= f.end_date) {
                return false;
            } else {
                return true;
            }
        };
        $scope.getFyInitialValues = function (baseline, f) {
            var x = {};
            x.financial_year_id = f.id;
            if (baseline !== undefined && baseline.financial_year_values !== undefined) {
                var existing = _.findWhere(baseline.financial_year_values, {
                    financial_year_id: f.id
                });
                if (existing !== undefined) {
                    x.planned_value = existing.planned_value;
                    x.actual_value = existing.actual_value;
                    x.id = existing.id;
                } else {
                    x.planned_value = undefined;
                    x.actual_value = undefined;
                }
            }
            return x;
        };
        $scope.getDataSourceValue = function (baseline) {
            if (baseline !== undefined && baseline.financial_year_values !== undefined) {
                var x = _.filter(baseline.financial_year_values, function (fv) {
                    return fv.data_source_id !== null;
                });
                if (x.length) {
                    return x[0].data_source_id;
                } else {
                    return undefined;
                }
            }
        };

        $scope.save = function (showMessage) {
            console.log($scope.items.data);
            var toSave = {
                indicators: $scope.items.data
            };
            PerformanceIndicators.saveValues(toSave, function (data) {
                if (showMessage) {
                    $scope.successMessage = data.successMessage;
                }
            });
        };

    }
})();