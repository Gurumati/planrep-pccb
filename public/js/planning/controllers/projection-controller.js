(function () {
  'use strict';

  angular
    .module('budgeting-module')
    .controller('ProjectionController', ProjectionController);

  ProjectionController.$inject = [
    '$scope',
    '$uibModal',
    'CanProjectFundSourceService',
    'FundSourceGfsCodeService',
    'ForwardFinancialYearsService',
    'CreateProjectionService',
    'PlanningPeriodService',
    'ConfirmDialogService'];

  function ProjectionController(
    $scope,
    $uibModal,
    CanProjectFundSourceService,
    FundSourceGfsCodeService,
    ForwardFinancialYearsService,
    CreateProjectionService,
    PlanningPeriodService,
    ConfirmDialogService) {

    $scope.title = "PROJECTIONS";
    $scope.currentPage = 1;

    CanProjectFundSourceService.get(function (data) {
      $scope.fundSources = data.fund_sources;
      if ($scope.fundSources.length === 1) {
        $scope.selectedFundSourceId = $scope.fundSources[0].id;
      }
      $scope.totalFundSources = data.total;
      $scope.fundSourceLoaded = true;
    });

    $scope.filterChanged = function (filter) {
      $scope.selectedFinancialYearId = filter.selectedFinancialYearId;
      $scope.selectedFinancialYearName = filter.selectedFinancialYearName;
      $scope.selectedBudgetType = filter.selectedBudgetType;
      $scope.selectedAdminHierarchyId = filter.selectedAdminHierarchyId;
      $scope.selectedAdminHierarchyName = filter.selectedAdminHierarchyName;
      $scope.selectedAdminHierarchyLevelPosition = filter.selectedAdminHierarchyLevelPosition;
      $scope.selectedSectionLevelPosition = filter.selectedSectionLevelPosition;
      $scope.selectedSectionId = filter.selectedSectionId;
      $scope.selectedSectionName = filter.selectedSectionName;
      $scope.searchQuery = filter.searchQuery;
      $scope.errorMessage = undefined;
      $scope.projectionToCreate.gfsCodes = [];
      $scope.forwardFinancialYears = [];
      clearData();
      loadForwardFinancialYear();
      loadQuarters();
      loadGfsCodes();

    };

    $scope.projectionToCreate = {};

    function clearData() {
      $scope.periods = [];
      $scope.forwardFinancialYears = [];
      $scope.projections = undefined
    }

    function filterValid() {
      if ($scope.selectedFinancialYearId === undefined ||
        $scope.selectedBudgetType === undefined ||
        $scope.selectedAdminHierarchyId === undefined ||
        $scope.selectedSectionId === undefined ||
        $scope.selectedFundSourceId === undefined
      ) {
        return false;
      }
      return true;
    }

    function loadQuarters() {
      if ($scope.selectedFinancialYearId === undefined) {
        return;
      }
      PlanningPeriodService.byYear({
        financialYearId: $scope.selectedFinancialYearId,
      }, function (data) {
        $scope.periods = data.periods;
        $scope.periodsLoaded = true;
        loadProjection();
      });
    }

    function loadForwardFinancialYear() {
      ForwardFinancialYearsService.query({
        financialYearId: $scope.selectedFinancialYearId,
      }, function (data) {
        $scope.forwardFinancialYears = data;
        $scope.forwardFinancialYearsLoaded = true;
      }, function (error) {
        $scope.forwardFinancialYearsLoaded = true;
        $scope.errorMessage = 'Failed to load forward financial years';
      });

    }

    $scope.fundSourceChange = function () {
      loadGfsCodes();
      loadProjection();
    }

    $scope.pageChanged = function (page) {
      loadProjection()
    };

    function loadProjection() {
      if (!filterValid()) {
        return;
      }
      FundSourceGfsCodeService.getProjections({
        searchQuery: $scope.searchQuery,
        page: $scope.currentPage,
        budgetType: $scope.selectedBudgetType,
        adminHierarchyId: $scope.selectedAdminHierarchyId,
        financialYearId: $scope.selectedFinancialYearId,
        sectionId: $scope.selectedSectionId,
        fundSourceId: $scope.selectedFundSourceId,
        page: $scope.currentPage,
        perPage: $scope.perPage,
        searchQuery: $scope.searchQuery
      },
        function (data) {
          $scope.totalRevenue = data.totalRevenue;
          $scope.projections = data.projections;
          $scope.fundSourceTitle = data.fundSourceTitle;
          $scope.mtef = data.mtef;
        },
        function (error) {
          $scope.errorMessage = error.data.errorMessage;
          $scope.projections = [];
          $scope.forwardFinancialYears = [];
        }
      );

    }

    $scope.delete = function (pro) {
      ConfirmDialogService.showConfirmDialog(
        'Confirm delete projection',
        'CONFIRM_DELETE')
        .then(function () {
          FundSourceGfsCodeService.deleteProjection({
            id: pro.id
          },
            function (data) {
              $scope.successMessage = data.successMessage;
              loadProjection();
            },
            function (error) {
              $scope.errorMessage = error.data.errorMessage;
            }
          );
        },
          function () {

          });
    }

    /**
     * load revenue projection gfs codes
     */
    function loadGfsCodes() {
      if (!filterValid()) { return; }

      FundSourceGfsCodeService.projectionGfsCodes({
        searchQuery: $scope.searchQuery,
        page: $scope.currentPage,
        budgetType: $scope.selectedBudgetType,
        adminHierarchyId: $scope.selectedAdminHierarchyId,
        financialYearId: $scope.selectedFinancialYearId,
        sectionId: $scope.selectedSectionId,
        fundSourceId: $scope.selectedFundSourceId
      },
        function (data) {
          $scope.gfs_codes = data.gfs_codes;
          $scope.gfsCodeLoaded = true;
        },
        function (error) {
          $scope.errorMessage = error.data.errorMessage;
          $scope.projectionToCreate.gfsCodes = [];
          $scope.forwardFinancialYears = [];
        }
      );
    };

    $scope.getForwardTotal = function getForwardTotal(projrctionAmount,index) {
      var formular 
    }

    $scope.periodTotal = function (projection) {
      if (projection === undefined) {
        return
      }
      var total = 0;
      projection.admin_hierarchy_ceiling_periods.forEach(pc => {
        if (pc.amount !== undefined) {
          total += parseFloat(pc.amount)
        }
      });
      projection.amount = total;
      var count = 1;
      projection.admin_hierarchy_ceiling_forwards.forEach(ahcf => {
        var factor = Math.pow(1.1, count);
        var amount = (factor *  projection.amount);
        projection.admin_hierarchy_ceiling_forwards[count-1].amount = amount;
        count ++;
      });
    };

    $scope.save = function (ceiling) {
      if (
        $scope.selectedFinancialYearId === undefined ||
        $scope.selectedBudgetType === undefined ||
        $scope.selectedAdminHierarchyId === undefined ||
        $scope.selectedSectionId === undefined ||
        $scope.selectedFundSourceId === undefined
      ) {
        return;
      }
      CreateProjectionService.creatOrUpdate({
        budgetType: $scope.selectedBudgetType,
        financialYearId: $scope.selectedFinancialYearId,
        adminHierarchyId: $scope.selectedAdminHierarchyId,
        sectionId: $scope.selectedSectionId,
        fundSourceId: $scope.selectedFundSourceId,

      }, ceiling,
        function (data) {
          $scope.successMessage = data.successMessage;
          $scope.totalRevenue = data.totalRevenue;
        },
        function (error) {
          $scope.errorMessage = error.data.errorMessage;
        }
      );
    };
    $scope.initiateProjection = function (gfsCodeId) {
      if (
        $scope.selectedFinancialYearId === undefined ||
        $scope.selectedBudgetType === undefined ||
        $scope.selectedAdminHierarchyId === undefined ||
        $scope.selectedSectionId === undefined ||
        $scope.selectedFundSourceId === undefined ||
        gfsCodeId === undefined
      ) {
        return;
      }
      CreateProjectionService.init({
        budgetType: $scope.selectedBudgetType,
        financialYearId: $scope.selectedFinancialYearId,
        adminHierarchyId: $scope.selectedAdminHierarchyId,
        sectionId: $scope.selectedSectionId,
        fundSourceId: $scope.selectedFundSourceId,
        gfsCodeId: gfsCodeId,

      },
        function (data) {
          loadGfsCodes()
          loadProjection()
        },
        function (error) {
          $scope.errorMessage = error.data.errorMessage;
        }
      );
    };

    /**
     *
     * @param {fundSource} fundSourceId
     * @param {fundName} fundSourceTitle
     */
    $scope.allocate = function (fundSourceId, fundSourceTitle) {
      if (
        $scope.selectedFinancialYearId === undefined ||
        $scope.selectedBudgetType === undefined ||
        $scope.selectedAdminHierarchyId === undefined ||
        $scope.selectedSectionId === undefined ||
        fundSourceId === undefined
      ) {
        return;
      }
      var modalInstance = $uibModal.open({
        templateUrl: '/pages/planning/projection/allocation.html',
        backdrop: false,
        resolve: {
          params: {
            budgetType: $scope.selectedBudgetType,
            financialYearId: $scope.selectedFinancialYearId,
            adminHierarchyId: $scope.selectedAdminHierarchyId,
            sectionId: $scope.selectedSectionId,
          }
        },
        controller: function ($scope, $filter, params, $uibModalInstance, AllocationCeilingsService, SaveAllocationCeilingsService) {
          $scope.fundSourceTitle = fundSourceTitle;
          $scope.byAmount = true;
          AllocationCeilingsService.get({
            fundSourceId: fundSourceId,
            budgetType: params.budgetType,
            financialYearId: params.financialYearId,
            adminHierarchyId: params.adminHierarchyId,
            sectionId: params.sectionId,
          }, function (data) {
            $scope.allocationCeilings = data.ceilings;
            $scope.totalRevenue = data.totalRevenue;
            $scope.setTotalAllocation();
          });
          $scope.close = function () {
            $uibModalInstance.dismiss('cancel');
          };
          $scope.save = function () {
            var data = {
              ceilings: $scope.allocationCeilings
            };
            SaveAllocationCeilingsService.save(data, function (data) {
              $scope.successMessage = data.successMessage;
            });
          };
          $scope.getInitialPercentage = function (amount) {
            var percentage = amount * 100 / $scope.totalRevenue;
            return $filter('number')(percentage, 2);
          };
          $scope.setTotalAllocation = function () {
            $scope.total = 0;
            angular.forEach($scope.allocationCeilings, function (value, index) {
              $scope.total += parseFloat(value.amount);
            });
          };
          $scope.setPercentage = function (c) {
            if (c !== undefined) {
              var percentage = c.amount * 100 / $scope.totalRevenue;
              c.percentage = $filter('number')(percentage, 2);
              $scope.setTotalAllocation();
            }
          };
          $scope.setAmount = function (c) {
            if (c !== undefined) {
              c.amount = ($scope.totalRevenue * c.percentage) / 100;
              $scope.setTotalAllocation();
            }
          };
        }
      });
      modalInstance.result.then(function (data) {
        $scope.successMessage = data.successMessage;
        $scope.fundSources = data.fundSources;

      },
        function () {
          console.log('Modal dismissed at: ' + new Date());
        });
    };
    var lastSearch = "";
    $scope.searchGfs = function () {
      if ($scope.searchQuery !== undefined && ($scope.searchQuery.length >= 3 || $scope.searchQuery.length < lastSearch.length)) {
        loadProjection();
      }
      lastSearch = angular.copy($scope.searchQuery);
    };
  }
})();
