function AdminHierarchySectMappingsController($scope,
                                              AdminSectorMappingModel,
                                              AllAdminHierarchyLevelsService,
                                              SectionLevelsService,
                                              CreateAdminSectionMappingService
) {

    $scope.adminSectMapping = AdminSectorMappingModel;
    $scope.title = "ADMIN_HIERARCHY_SECTION_MAPPING";
    $scope.dateFormat = 'yyyy-MM-DD';
    $scope.adminMappings = {};
    $scope.dataToCreate = {};
    $scope.father = '';
    $scope.adminMappingToCreate={};


    AllAdminHierarchyLevelsService.query(
        function (data) {
            $scope.hierarchyLevels = data;
        },
        function (error) {
            $scope.errorMessage = error.data.errorMessage;
        });

    //console.log($scope.adminSectMapping);
    $scope.getInitialValue = function (admin,section,field_item) {
        var result = [];
        angular.forEach($scope.adminSectMapping, function (value, key) {
            if(value.admin_hierarchy_level_id == admin && value.section.section_level_id == section)
            {
                result = value;

            }
        });



        if(result.id !== undefined)
        {
            if(field_item == 'can_budget')
            {
                return result.can_budget;
            }
            if(field_item == 'can_target')
            {
                return result.can_target;
            }
            if(field_item == 'in_use')
            {
                //console.log(section+' '+admin+' '+field_item);
                return true; //always true when row is found
            }
        }
        return false;
    };


    $scope.loadSection = function(){
        SectionLevelsService.query({},
            function (data) {
                $scope.dataToCreate = {}; //empty data
                $scope.sections = data;
            },
            function (error) {
                $scope.errorMessage = error.data.errorMessage;
            });
    };
    //load section at the first time page load
    $scope.loadSection();
    //when page is changed, load section

    $scope.store = function(){

        var array = $.map($scope.adminMappingToCreate, function(value, index) {
            var array2 = $.map(value, function(value2, index2) {
                var x={"sectionId":index2,"values":value2};
                return [x];
            });
            var y={"adminHierarchyLevelId":index,"sections":array2};
            return [y];
        });

        $scope.dataToCreate = array;
        // return;
        CreateAdminSectionMappingService.store($scope.dataToCreate,
            function (data) {
                //success message
                $scope.adminSectMapping = data.adminSectMapping;
                $scope.successMessage= data.successMessage;
            },
            function (error) {
                // console.log(error);
                $scope.errorMessage=error.data.errorMessage;
            }
        );
    };
    //console.log($scope.adminSectMapping);
}

AdminHierarchySectMappingsController.resolve = {

    AdminSectorMappingModel: function (adminHierarchySectMappingService, $q) {

        var deferred = $q.defer();
        adminHierarchySectMappingService.query({}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};