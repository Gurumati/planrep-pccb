function MtefSectorProblemController($scope, $uibModal,MtefSectorProblemModel, ConfirmDialogService, DeleteMtefSectorProblemService,
                                PaginatedMtefSectorProblemService) {

    $scope.mtefSectorProblems = MtefSectorProblemModel;
    $scope.title = "MTEF_SECTOR_PROBLEMS";
    $scope.currentPage = 1;
    $scope.maxSize = 3;
    $scope.pageChanged = function () {
        PaginatedMtefSectorProblemService.get({page: $scope.currentPage, perPage: $scope.perPage}, function (data) {
            $scope.mtefSectorProblems = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/planning/mtef_sector_problem/create.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, CreateMtefSectorProblemService,AllSectorProblemService, AllMTEFService) {

                AllMTEFService.query(function (data) {
                    $scope.mtefs = data;
                });

                AllSectorProblemService.query(function (data) {
                    $scope.sector_problems = data;
                });
                
                $scope.mtefSectorProblemToCreate = {};

                $scope.store = function () {
                    if ($scope.createMtefSectorProblemForm.$invalid) {
                        $scope.formHasErrors = true;
                        return;
                    }
                    CreateMtefSectorProblemService.store({perPage: $scope.perPage}, $scope.mtefSectorProblemToCreate,
                        function (data) {
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.mtefSectorProblems = data.mtefSectorProblems;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };

    $scope.edit = function (mtefSectorProblemToEdit, currentPage, perPage) {
        //console.log(mtefSectorProblemToEdit);
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/planning/mtef_sector_problem/edit.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance,AllSectorProblemService,AllMTEFService, UpdateMtefSectorProblemService) {
                AllMTEFService.query(function (data) {
                    $scope.mtefs = data;
                    $scope.mtefSectorProblemToEdit = angular.copy(mtefSectorProblemToEdit);
                });
                AllSectorProblemService.query(function (data) {
                    $scope.sector_problems = data;
                });
                $scope.update = function () {
                    UpdateMtefSectorProblemService.update({page: currentPage, perPage: perPage}, $scope.mtefSectorProblemToEdit,
                        function (data) {
                            //Successful function when
                            $uibModalInstance.close(data);
                        },
                        function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.mtefSectorProblems = data.mtefSectorProblems;
                $scope.currentPage = $scope.mtefSectorProblems.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });
    };
    $scope.delete = function (id, currentPage, perPage) {
        ConfirmDialogService.showConfirmDialog('Confirm Delete!', 'Are sure you want to delete this?').then(function () {
                DeleteMtefSectorProblemService.delete({mtef_sector_problem_id: id, page: currentPage, perPage: perPage},
                    function (data) {
                        $scope.successMessage = data.successMessage;
                        $scope.mtefSectorProblems = data.mtefSectorProblems;
                        $scope.currentPage = $scope.mtefSectorProblems.current_page;
                    }, function (error) {

                    }
                );
            },
            function () {
                console.log("NO");
            });
    };

    $scope.trash = function () {
        var modalInstance = $uibModal.open({
            templateUrl: '/pages/planning/mtef_sector_problem/trash.html',
            backdrop: false,
            controller: function ($scope, $uibModalInstance, TrashedMtefSectorProblemService,
                                  RestoreMtefSectorProblemService, EmptyMtefSectorProblemTrashService, PermanentDeleteMtefSectorProblemService) {
                TrashedMtefSectorProblemService.query(function (data) {
                    $scope.trashedMtefSectorProblems = data;
                });
                $scope.restoreMtefSectorProblem = function (id) {
                    RestoreMtefSectorProblemService.restore({id: id, currentPage: $scope.currentPage, perPage: $scope.perPage}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedMtefSectorProblems = data.trashedMtefSectorProblems;
                        }, function (error) {

                        }
                    );
                };
                $scope.permanentDelete = function (id) {
                    PermanentDeleteMtefSectorProblemService.permanentDelete({
                            id: id,
                            currentPage: $scope.currentPage,
                            perPage: $scope.perPage
                        }, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedMtefSectorProblems = data.trashedMtefSectorProblems;
                        }, function (error) {

                        }
                    );
                };

                $scope.emptyTrash = function () {
                    EmptyMtefSectorProblemTrashService.get({}, function (data) {
                            $scope.successMessage = data.successMessage;
                            $scope.trashedMtefSectorProblems = data.trashedMtefSectorProblems;
                        }, function (error) {
                            $scope.errorMessage = error.data.errorMessage;
                        }
                    );
                };
                //Function to close modal when cancel button clicked
                $scope.close = function () {
                    $uibModalInstance.dismiss('cancel');
                }
            }
        });
        //Called when modal is close by cancel or to store data
        modalInstance.result.then(function (data) {
                //Service to create new financial year
                $scope.successMessage = data.successMessage;
                $scope.mtefSectorProblems = data.mtefSectorProblems;
                $scope.currentPage = data.current_page;
            },
            function () {
                //If modal is closed
                console.log('Modal dismissed at: ' + new Date());
            });

    };
}

MtefSectorProblemController.resolve = {
    MtefSectorProblemModel: function (PaginatedMtefSectorProblemService, $q) {
        var deferred = $q.defer();
        PaginatedMtefSectorProblemService.get({page: 1, perPage: 10}, function (data) {
            deferred.resolve(data);
        });
        return deferred.promise;
    }
};