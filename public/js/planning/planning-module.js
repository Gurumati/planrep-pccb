var planning = angular.module('planning-module', ['master-module', 'ngRoute', 'planningServices']);
planning.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/plans/:stage/:budgetType', {
        controller: 'PlanningController',
        templateUrl: '/pages/planning/planning.html',
        reloadOnSearch: false,
        rights: ['planning.manage.plans']
    }).when('/planning', {
        controller: 'PlanningController',
        templateUrl: '/pages/planning/planning.html',
        reloadOnSearch: false,
        rights: ['planning']
    }).when('/planning-targets/:mtefSectionId/:budgetType', {
        controller : 'PlanningTargetController',
        templateUrl : '/pages/planning/planning-target.html',
        reloadOnSearch : false,
        rights : ['planning']
    }).when('/activities/:mtefSectionId/:budgetType', {
        controller : 'ActivityController',
        templateUrl : '/pages/planning/activity.html',
        reloadOnSearch : false,
        rights : ['planning']
    }).when('/adminSectorMapping', {
        controller: AdminHierarchySectMappingsController,
        templateUrl: '/pages/planning/admin_sector_mapping/index.html',
        resolve: AdminHierarchySectMappingsController.resolve,
        rights: ['settings.manage.budget_settings']
    }).when('/admin-sector-mapping', {
        controller: AdminHierarchySectMappingsController,
        templateUrl: '/pages/planning/admin_sector_mapping/index.html',
        resolve: AdminHierarchySectMappingsController.resolve,
        rights: ['settings.manage.budget_settings']
    }).when('/sector-targets', {
        controller: SectorTargetController,
        templateUrl: '/pages/planning/sector_target/index.html',
        resolve: SectorTargetController.resolve,
        rights: ['planning']
    }).when('/targets', {
        controller: 'LongTermTargetController',
        templateUrl: '/pages/planning/long-term-target.html',
        rights: ['planning.manage.long_term_targets']
    }).when('/mtef-list', {
        controller: MTEFController,
        templateUrl: '/pages/planning/mtef.html',
        resolve: MTEFController.resolve,
        rights: ['planning']
    }).when('/annual-targets', {
        controller: AnnualTargetController,
        templateUrl: '/pages/planning/annual-target.html',
        resolve: AnnualTargetController.resolve,
        rights: ['planning']
    }).when('/mtef-sector-problems', {
        controller: MtefSectorProblemController,
        templateUrl: '/pages/planning/mtef_sector_problem/index.html',
        resolve: MtefSectorProblemController.resolve,
        rights: ['planning.manage.mtef_sector_problems']
    }).when('/comprehensive-plans', {
        controller: ComprehensivePlansController,
        templateUrl: '/pages/planning/comprehensive_plans/index.html',
        resolve: ComprehensivePlansController.resolve,
        rights: ['planning.manage.comprehensive_plans']
    }).when('/cas-item-setup', {
        controller: CasPlanTableItemAdminHierarchyController,
        templateUrl: '/pages/planning/cas_plan_table_item_admin_hierarchy/index.html',
        resolve: CasPlanTableItemAdminHierarchyController.resolve,
        rights: ['planning']
    }).when('/baseline-data-values', {
        controller: BaselineStatisticValuesController,
        templateUrl: '/pages/planning/baseline_data_values/index.html',
        resolve: BaselineStatisticValuesController.resolve,
        rights: ['planning']
    }).when('/transport-facilities', {
        controller: TransportFacilityController,
        templateUrl: '/pages/planning/transport_facility/index.html',
        resolve: TransportFacilityController.resolve,
        rights: ['planning.manage.assets']
    }).when('/performance-indicator-baseline-values', {
        controller: 'PerformanceIndicatorBaselineValueController',
        templateUrl: '/pages/planning/performance_indicator_baseline_value/index.html',
        rights: ['planning.manage.performance_indicator_base_line_values']
    }).when('/facilities', {
        controller: FacilityController,
        templateUrl: '/pages/planning/facility/index.html',
        resolve: FacilityController.resolve,
        rights: ['planning.manage.facilities']
    }).when("/reference-documents/:isNationalGuideline", {
        controller: 'ReferenceDocumentController',
        templateUrl: "/pages/setup/reference_document/index.html",
        reloadOnSearch :false,
        rights: ['settings.manage.planning_settings']
    }).when('/project-data-form-answers', {
        controller: ProjectDataFormQuestionItemValueController,
        templateUrl: '/pages/setup/project_data_form_item_values/index.html',
        rights: ['planning.manage.project_data_forms']
    }).when('/historical-data', {
        controller: HistoricalDataController,
        templateUrl: '/pages/planning/historical-data/index.html',
        resolve: HistoricalDataController.resolve,
        rights: ['planning.manage.historical_data']
    }).otherwise({redirectTo: '/plans/1'});
}]);
