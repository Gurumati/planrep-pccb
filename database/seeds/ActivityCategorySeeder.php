<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivityCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activity_categories')->insert([
            'id' => 1,
            'name' => 'Capacity Building',
            'code' => 'C',
            'description' => 'Capacity Building',
            'sort_order' => 1,
            'is_active' => true,
        ]);

        DB::table('activity_categories')->insert([
            'id' => 2,
            'name' => 'Capital Investment',
            'code' => 'D',
            'description' => 'Capital Investment',
            'sort_order' => 2,
            'is_active' => true,
        ]);

        DB::table('activity_categories')->insert([
            'id' => 3,
            'name' => 'Service Delivery',
            'code' => 'S',
            'description' => 'Service Delivery',
            'sort_order' => 3,
            'is_active' => true,
        ]);
    }
}
