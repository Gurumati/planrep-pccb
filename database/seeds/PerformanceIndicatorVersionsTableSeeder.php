<?php

use Illuminate\Database\Seeder;
use App\Models\Setup\PerformanceIndicatorVersion;
use Illuminate\Support\Facades\DB;

class PerformanceIndicatorVersionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = DB::select("select id from performance_indicators where deleted_at is null");
        $versionId = 2;
        foreach ($items as $item) {
            $count = DB::table('performance_indicators as p')
                ->join('performance_indicator_versions as pv','p.id','pv.performance_indicator_id')
                ->where('p.id', $item->id)
                ->where('pv.version_id', $versionId)->count();
            if ($count == 0) {
                $obj = new PerformanceIndicatorVersion();
                $obj->performance_indicator_id = $item->id;
                $obj->version_id = $versionId;
                $obj->save();
            }
        }
    }
}
