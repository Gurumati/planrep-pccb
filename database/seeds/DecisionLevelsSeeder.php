<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DecisionLevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('decision_levels')->insert([
            'name' => 'Vote',
            'description' => 'Vote',
            'sort_order' => 1,
            'is_active' => true,
            'section_level_id' => 1,
            'admin_hierarchy_level_position' => 1,
            'is_default' => true
        ]);

        DB::table('decision_levels')->insert([
            'name' => 'Bureau',
            'description' => 'Bureau',
            'sort_order' => 1,
            'is_active' => true,
            'section_level_id' => 1,
            'admin_hierarchy_level_position' => 2,
            'is_default' => true
        ]);

        DB::table('decision_levels')->insert([
            'name' => 'Directorate/Department/Unit',
            'description' => 'Directorate/Department/Unit',
            'sort_order' => 1,
            'is_active' => true,
            'section_level_id' => 2,
            'admin_hierarchy_level_position' => 2,
            'is_default' => true
        ]);

        DB::table('decision_levels')->insert([
            'name' => 'Cost center',
            'description' => 'Cost center',
            'sort_order' => 1,
            'is_active' => true,
            'section_level_id' => 3,
            'admin_hierarchy_level_position' => 2,
            'is_default' => true
        ]);
    }
}
