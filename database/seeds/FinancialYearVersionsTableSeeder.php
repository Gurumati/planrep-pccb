<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FinancialYearVersionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('financial_year_versions')->insert([
            'financial_year_id' => 1,
            'version_id' => 1
        ]);
        DB::table('financial_year_versions')->insert([
            'financial_year_id' => 1,
            'version_id' => 2
        ]);
        DB::table('financial_year_versions')->insert([
            'financial_year_id' => 1,
            'version_id' => 3
        ]);
        DB::table('financial_year_versions')->insert([
            'financial_year_id' => 1,
            'version_id' => 4
        ]);
        DB::table('financial_year_versions')->insert([
            'financial_year_id' => 1,
            'version_id' => 5
        ]);
        DB::table('financial_year_versions')->insert([
            'financial_year_id' => 1,
            'version_id' => 6
        ]);
    }
}
