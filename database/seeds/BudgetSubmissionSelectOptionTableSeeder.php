<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BudgetSubmissionSelectOptionTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('budget_submission_select_option')->delete();

        DB::table('budget_submission_select_option')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Position',
                'parent_id' => NULL,
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'active' => true,
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Salary scale',
                'parent_id' => NULL,
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'active' => true,
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'HUMAN RESOURCE OFFICER II',
                'parent_id' => 1,
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'active' => true,
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'HUMAN RESOURCE OFFICER I',
                'parent_id' => 1,
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'active' => true,
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'TGS D',
                'parent_id' => 2,
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'active' => true,
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'TGS E',
                'parent_id' => 2,
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'active' => true,
            ),
            6 =>
            array (
                'id' => 7,
                'name' => 'TGS F',
                'parent_id' => 2,
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'active' => true,
            ),
        ));


    }
}
