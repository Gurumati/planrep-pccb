<?php

use App\Models\Setup\Sector;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class projectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo '-----Seeding Project ---------'."\r\n";
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/projects.xlsx');
        $projectsSheet = $objPHPExcel->getSheet(0);
        $date = Carbon\Carbon::now();

        //Projects and project Sector
        $counter = 0;   
        for ($row = 1; $row <=$projectsSheet->getHighestRow(); ++$row) {
            if($projectsSheet->getCell('A'.$row)->getValue() && $counter > 0){
               
                DB::table('projects')->insert([
                    'id' => $row,
                    'name' => $projectsSheet->getCell('C'.$row)->getValue(),
                    'code' => $projectsSheet->getCell('B'.$row)->getValue(),
                    'description' => $projectsSheet->getCell('C'.$row)->getValue(),
                    'created_at' => $date,
                    'updated_at' => $date,
                ]);

                DB::table('project_versions')->insert([
                    'project_id' => $row,
                    'version_id' => 8,
                    'created_at' => $date,
                    'updated_at' => $date
                ]);

                $sectors = explode(",",$projectsSheet->getCell('D'.$row)->getValue());
                foreach ($sectors as &$sectorId) {
                    DB::table('sector_projects')->insert([
                        'sector_id' => $sectorId,
                        'project_id' => $row,
                        'created_at' => $date,
                        'updated_at' => $date,
                    ]);
                }
            }
            $counter ++;
        }
        DB::select( DB::raw("SELECT setval(pg_get_serial_sequence('projects' , 'id'), coalesce((max(id) + 1),1), false) FROM  projects"));
        DB::select( DB::raw("SELECT setval(pg_get_serial_sequence('sector_projects' , 'id'), coalesce((max(id) + 1),1), false) FROM  sector_projects"));
    }
}
