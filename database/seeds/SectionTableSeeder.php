<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seedSectorSection();
        $pics = DB::table('admin_hierarchies as ah')
        ->join('admin_hierarchy_levels as ahl','ah.admin_hierarchy_level_id','=','ahl.id')
        ->where('ahl.hierarchy_position', 2)
        ->orderBy('ah.code', 'asc')
        ->pluck('ah.id','ah.code');
        $all_piscs = json_decode(json_encode($pics),true);

        $all_sectors = json_decode(json_encode(
            DB::table('sectors')->pluck('id','name')
        ),true);






        $facilityTypeId = DB::table('facility_types')->where('name', 'HQ')->first()->id;

        $objPHPExcel = PHPExcel_IOFactory::load('database/data/AllPISCSwithCostCentre.xlsx');
        $costcenterSheet = $objPHPExcel->getSheet(0);

        $existingDept ='';
        $existingSector = '';
        $existingPiscs ='';
        $deptId = DB::table('sections')->max('id') + 1;
        $parent= 0;
        $sectorId = null;

        for ($row = 1; $row <=$costcenterSheet->getHighestRow(); ++$row) {
            if($row > 1){
                $trno = [$costcenterSheet->getCell('F'.$row)->getValue()];
                $sect = $costcenterSheet->getCell('G'.$row)->getValue();
                $dept = $costcenterSheet->getCell('H'.$row)->getValue();


                $filtered = array_filter($all_piscs,function ($key) use ($trno) {
                    return in_array($key, $trno);
                },ARRAY_FILTER_USE_KEY);

                if($filtered){

                    $adminId = reset($filtered);
                    //Get Sector Id
                    if($existingSector !==  $sect){
                        $filteredSector = array_filter($all_sectors,function ($key) use ($sect) {
                            return in_array($key, [trim($sect)]);
                        },ARRAY_FILTER_USE_KEY);

                        $sectorId = reset($filteredSector);
                    }
                    echo "Sector- ". trim($sect) ."\r\n";
                    echo "Sector-Id ". $sectorId ."\r\n";

                     //Creating Service Provider HQ
                     if($existingPiscs !== $costcenterSheet->getCell('F'.$row)->getValue()){
                        //Creating HQ
                        echo "Creating HQ for ".  $costcenterSheet->getCell('E'.$row)->getValue() ."\r\n";

                        DB::table('facilities')->insert([
                            'name' => $costcenterSheet->getCell('E'.$row)->getValue(),
                            'facility_code' => '00000000',
                            'description' => $costcenterSheet->getCell('E'.$row)->getValue(),
                            'facility_type_id' =>$facilityTypeId,
                            'admin_hierarchy_id' =>  $adminId,
                            'number_of_villages_served' => 10000,
                            'is_active' => true,
                            'geo_location_id' => 2043
                        ]);
                    }

                    // Creating CostCenter and Department Based On PISCs


                    $deptId = $deptId + 1;

                    if($dept !== $existingDept){
                        $parent =  $deptId;

                        echo "Creating DepartMent with PISCs =>" . $adminId ." ID ".  $deptId ."\r\n";
                        DB::table('sections')->insert([
                            'id' => $deptId,
                            'name' => $dept,
                            'code' => $costcenterSheet->getCell('J'.$row)->getValue(),
                            'section_level_id' => 3,
                            'parent_id' =>  $sectorId +1,
                            'sort_order' => $row,
                            'is_active' => true,
                            'sector_id' => $sectorId
                        ]);

                        //Creating PISCs Section Relation Mappig
                        echo "Creating Section PIcs Relation" ."\r\n";
                        DB::table('admin_hierarchy_sections')->insert([
                            'admin_hierarchy_id' => $adminId,
                            'section_id' => $deptId
                        ]);

                        $deptId = $deptId + 1;
                        echo "Creating  Cost Center with parent => ".$parent ." ID =>".($deptId)."\r\n";
                        DB::table('sections')->insert([
                            'id' => $deptId,
                            'name' => $costcenterSheet->getCell('K'.$row)->getValue(),
                            'code' => $costcenterSheet->getCell('L'.$row)->getValue(),
                            'section_level_id' => 4,
                            'parent_id' => $parent,
                            'sort_order' => $row,
                            'is_active' => true,
                            'sector_id' => $sectorId
                        ]);

                        //Creating PISCs Section Relation Mappig
                        echo "Creating Section PIcs Relation" ."\r\n";
                        DB::table('admin_hierarchy_sections')->insert([
                            'admin_hierarchy_id' => $adminId,
                            'section_id' => $deptId
                        ]);


                    } else {
                        $deptId = $deptId + 1;
                        echo "Creating Cost Center with parent =>".$parent ." ID =>".($deptId)."\r\n";

                        DB::table('sections')->insert([
                            'id' => $deptId,
                            'name' => $costcenterSheet->getCell('K'.$row)->getValue(),
                            'code' => $costcenterSheet->getCell('L'.$row)->getValue(),
                            'section_level_id' => 4,
                            'parent_id' => $parent,
                            'sort_order' => $row,
                            'is_active' => true,
                            'sector_id' => $sectorId
                        ]);

                         //Creating PISCs Section Relation Mappig
                         echo "Creating Section PIcs Relation" ."\r\n";
                        DB::table('admin_hierarchy_sections')->insert([
                            'admin_hierarchy_id' => $adminId,
                            'section_id' => $deptId
                        ]);
                    }
                    $existingDept = $dept;
                    $existingPiscs = $costcenterSheet->getCell('F'.$row)->getValue();
                    $existingSector =  $sect;
                }
            }

        }

    }

    public function seedSectorSection(){
        DB::table('sections')->insert([
            'id' => 1,
            'name' => 'Bureau',
            'code' => 'B001',
            'section_level_id' => 1,
            'parent_id' => null,
            'sort_order' => 1,
            'is_active' => true,
            'sector_id' => null,
        ]);

        DB::table('sections')->insert([
            'id' => 2,
            'name' => 'Public Administration',
            'code' => 'PA01',
            'section_level_id' => 2,
            'parent_id' => 1,
            'sort_order' => 2,
            'is_active' => true,
            'sector_id' => 1,
        ]);
        // DB::table('sections')->insert([
        //     'id' => 3,
        //     'name' => 'Health and Community Development',
        //     'code' => 'HD01',
        //     'section_level_id' => 2,
        //     'parent_id' => 1,
        //     'sort_order' => 3,
        //     'is_active' => true,
        //     'sector_id' => 2,
        // ]);
        // DB::table('sections')->insert([
        //     'id' => 4,
        //     'name' => 'Agriculture',
        //     'code' => 'AG01',
        //     'section_level_id' => 2,
        //     'parent_id' => 1,
        //     'sort_order' => 4,
        //     'is_active' => true,
        //     'sector_id' => 3,
        // ]);
        // DB::table('sections')->insert([
        //     'id' => 5,
        //     'name' => 'Education and Training',
        //     'code' => 'ET01',
        //     'section_level_id' => 2,
        //     'parent_id' => 1,
        //     'sort_order' => 5,
        //     'is_active' => true,
        //     'sector_id' => 4,
        // ]);
        // DB::table('sections')->insert([
        //     'id' => 6,
        //     'name' => 'Construction and Land Development',
        //     'code' => 'CD01',
        //     'section_level_id' => 2,
        //     'parent_id' => 1,
        //     'sort_order' => 6,
        //     'is_active' => true,
        //     'sector_id' => 5,
        // ]);
        // DB::table('sections')->insert([
        //     'id' => 7,
        //     'name' => 'Water',
        //     'code' => 'WT01',
        //     'section_level_id' => 2,
        //     'parent_id' => 1,
        //     'sort_order' => 7,
        //     'is_active' => true,
        //     'sector_id' => 6,
        // ]);
        // DB::table('sections')->insert([
        //     'id' => 8,
        //     'name' => 'Energy',
        //     'code' => 'EG01',
        //     'section_level_id' => 2,
        //     'parent_id' => 1,
        //     'sort_order' => 8,
        //     'is_active' => true,
        //     'sector_id' => 7,
        // ]);
        // DB::table('sections')->insert([
        //     'id' => 9,
        //     'name' => 'Transport and Communication',
        //     'code' => 'TC01',
        //     'section_level_id' => 2,
        //     'parent_id' => 1,
        //     'sort_order' => 9,
        //     'is_active' => true,
        //     'sector_id' => 8,
        // ]);
        // DB::table('sections')->insert([
        //     'id' => 10,
        //     'name' => 'Industries and Trade',
        //     'code' => 'IT01',
        //     'section_level_id' => 2,
        //     'parent_id' => 1,
        //     'sort_order' => 10,
        //     'is_active' => true,
        //     'sector_id' => 9,
        // ]);
        // DB::table('sections')->insert([
        //     'id' => 11,
        //     'name' => 'Mining',
        //     'code' => 'MG01',
        //     'section_level_id' => 2,
        //     'parent_id' => 1,
        //     'sort_order' => 11,
        //     'is_active' => true,
        //     'sector_id' => 10,
        // ]);
        // DB::table('sections')->insert([
        //     'id' => 12,
        //     'name' => 'Natural Resources and Tourism',
        //     'code' => 'NT01',
        //     'section_level_id' => 2,
        //     'parent_id' => 1,
        //     'sort_order' => 12,
        //     'is_active' => true,
        //     'sector_id' => 11,
        // ]);
        // DB::table('sections')->insert([
        //     'id' => 13,
        //     'name' => 'Livestock',
        //     'code' => 'LT01',
        //     'section_level_id' => 2,
        //     'parent_id' => 1,
        //     'sort_order' => 12,
        //     'is_active' => true,
        //     'sector_id' => 12,
        // ]);
        // DB::table('sections')->insert([
        //     'id' => 14,
        //     'name' => 'Finance',
        //     'code' => 'FN01',
        //     'section_level_id' => 2,
        //     'parent_id' => 1,
        //     'sort_order' => 14,
        //     'is_active' => true,
        //     'sector_id' => 13,
        // ]);

    }

}
