<?php

use Illuminate\Database\Seeder;
use  App\Models\Setup\InterventionVersion;
use Illuminate\Support\Facades\DB;

class InterventionVersionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = DB::select("select id,number from interventions where deleted_at is null");
        $versionId = 3;
        foreach ($items as $item) {
            $count = DB::table('interventions as i')
                ->join('intervention_versions as iv','i.id','iv.intervention_id')
                ->where('i.id',$item->id)
                ->where('i.number',$item->number)
                ->where('iv.version_id',$versionId)->count();
            if($count == 0) {
                $obj = new InterventionVersion();
                $obj->intervention_id = $item->id;
                $obj->version_id = $versionId;
                $obj->save();
            }
        }
    }
}
