<?php

use Illuminate\Database\Seeder;
use App\Models\Setup\BudgetClassVersion;
use Illuminate\Support\Facades\DB;

class BudgetClassVersionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = DB::select("select id,code from budget_classes where deleted_at is null");
        $versionId = 6;
        foreach ($items as $item) {
            $count = DB::table('budget_classes as b')
                ->join('budget_class_versions as bv','b.id','bv.budget_class_id')
                ->where('b.id', $item->id)
                ->where('b.code', $item->code)
                ->where('bv.version_id', $versionId)->count();
            if ($count == 0) {
                $obj = new BudgetClassVersion();
                $obj->budget_class_id = $item->id;
                $obj->version_id = $versionId;
                $obj->save();
            }
        }
    }
}
