<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SectionLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('section_levels')->insert([
            'name' => 'Bureau',
            'code' => 'B001',
            'hierarchy_position' => 1,
            'description' => 'Bureau/Top Level Decision Maker',
            'sort_order' => 1,
            'is_active' => true,
            'next_budget_level' => null,
        ]);
        DB::table('section_levels')->insert([
            'name' => 'Sector',
            'code' => 'S001',
            'hierarchy_position' => 2,
            'description' => 'Sector',
            'sort_order' => 2,
            'is_active' => true,
            'next_budget_level' => null,
        ]);
        DB::table('section_levels')->insert([
            'name' => 'Directorate/Department/Unit',
            'code' => 'D001',
            'hierarchy_position' => 3,
            'description' => 'Directorate/Department/Unit/ Decision Maker',
            'sort_order' => 3,
            'is_active' => true,
            'next_budget_level' => null,
        ]);
        DB::table('section_levels')->insert([
            'name' => 'Cost Center',
            'code' => 'C003',
            'hierarchy_position' => 4,
            'description' => 'Cost Center/ Decision Maker',
            'sort_order' => 4,
            'is_active' => true,
            'next_budget_level' => null,
        ]);
        DB::table('section_levels')->insert([
            'name' => 'Service Provider',
            'code' => 'SP04',
            'hierarchy_position' => 5,
            'description' => 'Service Provider/ Decision Maker',
            'sort_order' => 5,
            'is_active' => true,
            'next_budget_level' => null,
        ]);

    }
}
