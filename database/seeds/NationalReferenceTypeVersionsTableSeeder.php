<?php

use Illuminate\Database\Seeder;
use App\Models\Setup\ReferenceTypeVersion;
use Illuminate\Support\Facades\DB;

class NationalReferenceTypeVersionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = DB::select("select id,name from reference_types where deleted_at is null");
        $versionId = 7;
        foreach ($items as $item) {
            $count = DB::table('reference_types as r')
                ->join('reference_type_versions as rv','r.id','rv.reference_type_id')
                ->where('r.id', $item->id)
                ->where('r.name', $item->name)
                ->where('rv.version_id', $versionId)->count();
            if ($count == 0) {
                $obj = new ReferenceTypeVersion();
                $obj->reference_type_id = $item->id;
                $obj->version_id = $versionId;
                $obj->save();
            }
        }
    }
}
