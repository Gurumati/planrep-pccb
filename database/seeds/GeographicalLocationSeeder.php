<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeographicalLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo '--------- Start Seeding geographical locations ------- ' ;

        $objPHPExcel = PHPExcel_IOFactory::load('database/data/AllGeographicalLocations.xlsx');
        $LocationsSheet = $objPHPExcel->getSheet(0);

        for ($row = 1; $row <=$LocationsSheet->getHighestRow(); ++$row) {
            if($row>1 && $LocationsSheet->getCell('A'.$row)->getValue() != null){
                DB::table('geographical_locations')->insert([
                    'id' => $LocationsSheet->getCell('A'.$row)->getValue(),
                    'name' => $LocationsSheet->getCell('B'.$row)->getValue(),
                    'code' => $LocationsSheet->getCell('A'.$row)->getValue(),
                    'geo_location_level_id' => $LocationsSheet->getCell('D'.$row)->getValue(),
                    'sort_order' => $row,
                    'is_active' => true,
                    'parent_id' => $LocationsSheet->getCell('C'.$row)->getValue()==''?null:$LocationsSheet->getCell('C'.$row)->getValue()
                ]);
            }
        }

    }
}
