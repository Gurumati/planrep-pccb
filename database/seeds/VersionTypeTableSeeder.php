<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VersionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('version_types')->insert([
            //PLAN_CHAIN
            'id' => 1,
            'code' => 'PC',
            'name' => 'PLAN_CHAIN'
        ]);
        DB::table('version_types')->insert([
            //PERFORMANCE_INDICATOR
            'id' => 2,
            'code' => 'PI',
            'name' => 'PERFORMANCE_INDICATOR'
        ]);
        DB::table('version_types')->insert([
            //INTERVENTION
            'id' => 3,
            'code' => 'IN',
            'name' => 'INTERVENTION'
        ]);
        DB::table('version_types')->insert([
            //PRIORITY_AREA
            'id' => 4,
            'code' => 'PA',
            'name' => 'PRIORITY_AREA'
        ]);
        DB::table('version_types')->insert([
            //FUND_SOURCE
            'id' => 5,
            'code' => 'FS',
            'name' => 'FUND_SOURCE'
        ]);
        DB::table('version_types')->insert([
            //BUDGET_CLASS
            'id' => 6,
            'code' => 'BC',
            'name' => 'BUDGET_CLASS'
        ]);
        DB::table('version_types')->insert([
            //NATIONAL_REFERENCE_TYPE
            'id' => 7,
            'code' => 'NR',
            'name' => 'NATIONAL_REFERENCE_TYPE'
        ]);

        DB::table('version_types')->insert([
            //NATIONAL_REFERENCE_TYPE
            'id' => 8,
            'code' => 'BA',
            'name' => 'BANK_ACCOUNT'
        ]);

        DB::table('version_types')->insert([
            //NATIONAL_REFERENCE_TYPE
            'id' => 9,
            'code' => 'PR',
            'name' => 'PROJECT'
        ]);
    }
}
