<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FundSourceCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fund_source_categories')->insert([
            'id' => 1,
            'name' => 'Own Source',
            'code' => 'FSC1',
        ]);

        DB::table('fund_source_categories')->insert([
            'id' => 2,
            'name' => 'Central Government Grants',
            'code' => 'FSC2',
        ]);

        DB::table('fund_source_categories')->insert([
            'id' => 3,
            'name' => 'NGO Partners',
            'code' => 'FSC3',
        ]);

        DB::table('fund_source_categories')->insert([
            'id' => 4,
            'name' => 'Local Borrowing',
            'code' => 'FSC4',
        ]);

        DB::table('fund_source_categories')->insert([
            'id' => 5,
            'name' => 'UN - International Organizations',
            'code' => 'FSC5',
        ]);

        DB::table('fund_source_categories')->insert([
            'id' => 6,
            'name' => 'Basket Fund',
            'code' => 'FSC6',
        ]);

        DB::table('fund_source_categories')->insert([
            'id' => 7,
            'name' => 'Other International Organizations',
            'code' => 'FSC7',
        ]);

        DB::table('fund_source_categories')->insert([
            'id' => 8,
            'name' => 'Central Government Devt Grants',
            'code' => 'FSC8',
        ]);

        DB::table('fund_source_categories')->insert([
            'id' => 9,
            'name' => 'International Borrowing',
            'code' => 'FSC9',
        ]);
    }
}
