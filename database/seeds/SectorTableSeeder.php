<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SectorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sectors')->insert([
            'id'=> 1,
            'name' => 'Public Administration',
            'code' => '1',
            'description' => 'Public Administration',
            'is_active' => true,
        ]);

        DB::table('sectors')->insert([
            'id'=>2,
            'name' => 'Health and Community Development',
            'code' => '2',
            'description' => 'Health and Community Development',
            'is_active' => true,
        ]);
        DB::table('sectors')->insert([
            'id'=>3,
            'name' => 'Agriculture',
            'code' => '3',
            'description' => 'Agriculture',
            'is_active' => true,
        ]);
        DB::table('sectors')->insert([
            'id'=>4,
            'name' => 'Education and Training',
            'code' => '4',
            'description' => 'Education and Training',
            'is_active' => true,
        ]);
        DB::table('sectors')->insert([
            'id'=>5,
            'name' => 'Construction and Land Development',
            'code' => '5',
            'description' => 'Construction and Land Development',
            'is_active' => true,
        ]);
        DB::table('sectors')->insert([
            'id'=>6,
            'name' => 'Water',
            'code' => '6',
            'description' => 'Water',
            'is_active' => true,
        ]);
        DB::table('sectors')->insert([
            'id'=>7,
            'name' => 'Energy',
            'code' => '7',
            'description' => 'Energy',
            'is_active' => true,
        ]);
        DB::table('sectors')->insert([
            'id'=>8,
            'name' => 'Transport and Communication',
            'code' => '8',
            'description' => 'Transport and Communication',
            'is_active' => true,
        ]);
        DB::table('sectors')->insert([
            'id'=>9,
            'name' => 'Industries and Trade',
            'code' => '9',
            'description' => 'Industries and Trade',
            'is_active' => true,
        ]);
        DB::table('sectors')->insert([
            'id'=>10,
            'name' => 'Mining',
            'code' => '10',
            'description' => 'Mining',
            'is_active' => true,
        ]);
        DB::table('sectors')->insert([
            'id'=>11,
            'name' => 'Natural Resources and Tourism',
            'code' => '11',
            'description' => 'Natural Resources and Tourism',
            'is_active' => true,
        ]);
        DB::table('sectors')->insert([
            'id'=>12,
            'name' => 'Livestock',
            'code' => '12',
            'description' => 'Livestock',
            'is_active' => true,
        ]);
        DB::table('sectors')->insert([
            'id'=>13,
            'name' => 'Finance',
            'code' => '13',
            'description' => 'Finance',
            'is_active' => true,
        ]);
    }
}
