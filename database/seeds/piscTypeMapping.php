<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class piscTypeMapping extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        echo '--------- Start Mapping hierarchies -------' ;

        $objCofogwithpisc = PHPExcel_IOFactory::load('database/data/piscAdminHierarchmap.xlsx');
        $cofogwithpiscSheet = $objCofogwithpisc->getSheet(0);
        for($row2 = 1; $row2 <=$cofogwithpiscSheet->getHighestRow(); ++$row2){
            if($row2 >1) {
                $trNumber = $cofogwithpiscSheet->getCell('A'.$row2)->getValue();
                $type = $cofogwithpiscSheet->getCell('B'.$row2)->getValue();
                $piscsCount = DB::table('admin_hierarchies')->where('code', $trNumber)->where('admin_hierarchy_level_id',4)->count();
                if($piscsCount == 1) {

                            DB::table("admin_hierarchies")->where('code', $trNumber)
                                ->update([
                                    'pisc_type_id' => $type
                                ]);

                }
            }

        }

    }
}
