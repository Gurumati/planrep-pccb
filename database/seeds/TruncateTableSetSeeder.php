<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TruncateTableSetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('truncate_table_sets')->insert([
            'table_name' => 'activities',
            'sort_order' => 1,
        ]);
        DB::table('truncate_table_sets')->insert([
            'table_name' => 'cas_plan_table_item_values',
            'sort_order' => 2,
        ]);
        DB::table('truncate_table_sets')->insert([
            'table_name' => 'cas_plan_table_item_constants',
            'sort_order' => 3,
        ]);
        DB::table('truncate_table_sets')->insert([
            'table_name' => 'cas_plan_table_details',
            'sort_order' => 4,
        ]);
        DB::table('truncate_table_sets')->insert([
            'table_name' => 'budget_submission_lines',
            'sort_order' => 5,
        ]);
        DB::table('truncate_table_sets')->insert([
            'table_name' => 'long_term_targets',
            'sort_order' => 6,
        ]);
        DB::table('truncate_table_sets')->insert([
            'table_name' => 'cas_assessment_result_detail_replies',
            'sort_order' => 7,
        ]);
        DB::table('truncate_table_sets')->insert([
            'table_name' => 'cas_assessment_result_replies',
            'sort_order' => 8,
        ]);
        DB::table('truncate_table_sets')->insert([
            'table_name' => 'cas_assessment_result_details',
            'sort_order' => 9,
        ]);
        DB::table('truncate_table_sets')->insert([
            'table_name' => 'cas_assessment_results',
            'sort_order' => 10,
        ]);
        DB::table('truncate_table_sets')->insert([
            'table_name' => 'reference_documents',
            'sort_order' => 11,
        ]);
        DB::table('truncate_table_sets')->insert([
            'table_name' => 'admin_hierarchy_ceilings',
            'sort_order' => 12,
        ]);

    }
}
