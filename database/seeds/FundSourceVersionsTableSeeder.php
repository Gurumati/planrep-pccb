<?php

use Illuminate\Database\Seeder;
use App\Models\Setup\FundSourceVersion;
use Illuminate\Support\Facades\DB;

class FundSourceVersionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = DB::select("select id,code from fund_sources where deleted_at is null");
        $versionId = 5;
        foreach ($items as $item) {
            $count = DB::table('fund_sources as f')
                ->join('fund_source_versions as fv','f.id','fv.fund_source_id')
                ->where('f.id', $item->id)
                ->where('f.code', $item->code)
                ->where('fv.version_id', $versionId)->count();
            if ($count == 0) {
                $obj = new FundSourceVersion();
                $obj->fund_source_id = $item->id;
                $obj->version_id = $versionId;
                $obj->save();
            }
        }
    }
}
