<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminHierarchyLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_hierarchy_levels')->insert([
            'id' => 1,
            'name' => 'Vote',
            'code' => 'Vote',
            'hierarchy_position' => 1,
            'sort_order' => 1,
            'is_active' => true,
        ]);
        DB::table('admin_hierarchy_levels')->insert([
            'id' => 2,
            'name' => 'Bureau',
            'code' => 'Bureau',
            'hierarchy_position' => 2,
            'sort_order' => 2,
            'is_active' => true,
        ]);
    }
}
