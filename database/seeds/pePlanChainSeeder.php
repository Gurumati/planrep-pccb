<?php

use App\Models\Planning\PlanChain;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class pePlanChainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo '-----Seeding PE Settings ---------'."\r\n";
        $sectors = DB::table('sectors')->whereNull('deleted_at')->get();
        $maxId = PlanChain::max('id') + 1;

        DB::table('plan_chains')->insert([
            'id' =>$maxId,
            'description' => 'Pay Personal Emolments',
            'code' => '0',
            'is_active' => true,
            'created_at' => Carbon::now(),
            'updated_at' =>Carbon::now(),
            'plan_chain_type_id' => 2,
            'parent_id' => null
        ]);

        DB::table('plan_chain_versions')->insert([
            'plan_chain_id' => $maxId,
            'version_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        foreach ($sectors as $sector) {
             DB::table('plan_chain_sectors')->insert([
            'plan_chain_id' => $maxId,
            'sector_id' => $sector->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        }

        $serviceOutId = $maxId + 1;
        
        DB::table('plan_chains')->insert([
            'id' =>$serviceOutId,
            'description' => 'Objective Code',
            'code' => '0',
            'is_active' => true,
            'created_at' => Carbon::now(),
            'updated_at' =>Carbon::now(),
            'plan_chain_type_id' => 2,
            'parent_id' => null
        ]);

        DB::table('plan_chain_versions')->insert([
            'plan_chain_id' => $serviceOutId,
            'version_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        foreach ($sectors as $sector) {
             DB::table('plan_chain_sectors')->insert([
            'plan_chain_id' => $serviceOutId,
            'sector_id' => $sector->id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        }
    }
}
