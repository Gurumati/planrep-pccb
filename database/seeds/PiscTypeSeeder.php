<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use JasperPHP\JasperPHP;

class PiscTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pisc_types')->insert([
            'id' => 1,
            'name' => 'Commercial',
            'code' => 'PT01'
        ]);
        DB::table('pisc_types')->insert([
            'id' => 2,
            'name' => 'Non-Commercial',
            'code' => 'PT02'
        ]);
    }
}
