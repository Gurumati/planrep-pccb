<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/account_types.xlsx');
        $accountTypeSheet = $objPHPExcel->getSheet(0);

        for ($row = 1; $row <=$accountTypeSheet->getHighestRow(); ++$row) {
            DB::table('account_types')->insert([
                'id' => $accountTypeSheet->getCell('A'.$row)->getValue(),
                'name' => $accountTypeSheet->getCell('B'.$row)->getValue(),
                'code' => $accountTypeSheet->getCell('C'.$row)->getValue(),
                'description' => $accountTypeSheet->getCell('D'.$row)->getValue(),
                'balance_type' => $accountTypeSheet->getCell('K'.$row)->getValue()
            ]);
        }

    }

}
