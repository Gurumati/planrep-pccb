<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CasGroupColumnsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('cas_group_columns')->delete();

        DB::table('cas_group_columns')->insert(array (
            0 =>
            array (
                'id' => 3,
                'name' => 'Admissions',
                'cas_group_type_id' => 6,
                'code' => 'adm',
                'sort_order' => 1,
                'deleted_at' => NULL,
                'created_by' => 11,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-05-07 15:29:51',
                'updated_at' => '2017-05-07 15:29:51',
            ),
            1 =>
            array (
                'id' => 4,
                'name' => 'Deaths',
                'cas_group_type_id' => 6,
                'code' => 'D',
                'sort_order' => 2,
                'deleted_at' => NULL,
                'created_by' => 11,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-05-07 15:30:21',
                'updated_at' => '2017-05-07 15:30:21',
            ),
            2 =>
            array (
                'id' => 5,
                'name' => '<5 years',
                'cas_group_type_id' => 2,
                'code' => 'yrs',
                'sort_order' => 1,
                'deleted_at' => NULL,
                'created_by' => 11,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-05-07 15:31:04',
                'updated_at' => '2017-05-07 15:31:04',
            ),
            3 =>
            array (
                'id' => 6,
                'name' => '5+ years',
                'cas_group_type_id' => 2,
                'code' => 'yrs',
                'sort_order' => 2,
                'deleted_at' => NULL,
                'created_by' => 11,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-05-07 15:31:28',
                'updated_at' => '2017-05-07 15:31:28',
            ),
            4 =>
            array (
                'id' => 2,
                'name' => 'F',
                'cas_group_type_id' => 1,
                'code' => 'Female',
                'sort_order' => 2,
                'deleted_at' => NULL,
                'created_by' => 11,
                'deleted_by' => NULL,
                'update_by' => 15,
                'created_at' => '2017-05-07 15:29:01',
                'updated_at' => '2017-05-08 13:00:19',
            ),
            5 =>
            array (
                'id' => 1,
                'name' => 'M',
                'cas_group_type_id' => 1,
                'code' => 'Male',
                'sort_order' => 1,
                'deleted_at' => NULL,
                'created_by' => 11,
                'deleted_by' => NULL,
                'update_by' => 2,
                'created_at' => '2017-05-07 15:25:31',
                'updated_at' => '2017-05-25 15:57:25',
            ),
            6 =>
            array (
                'id' => 7,
                'name' => 'LYR',
                'cas_group_type_id' => 9,
                'code' => 'c7',
                'sort_order' => 1,
                'deleted_at' => NULL,
                'created_by' => 28,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-05-26 06:41:28',
                'updated_at' => '2017-05-26 06:41:28',
            ),
            7 =>
            array (
                'id' => 8,
                'name' => 'CYR',
                'cas_group_type_id' => 9,
                'code' => 'c8',
                'sort_order' => 2,
                'deleted_at' => NULL,
                'created_by' => 28,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-05-26 06:41:51',
                'updated_at' => '2017-05-26 06:41:51',
            ),
            8 =>
            array (
                'id' => 9,
                'name' => 'Death',
                'cas_group_type_id' => 10,
                'code' => 'c1',
                'sort_order' => 1,
                'deleted_at' => NULL,
                'created_by' => 28,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-05-26 14:49:08',
                'updated_at' => '2017-05-26 14:49:08',
            ),
            9 =>
            array (
                'id' => 10,
                'name' => 'CFR',
                'cas_group_type_id' => 10,
                'code' => 'c2',
                'sort_order' => 2,
                'deleted_at' => NULL,
                'created_by' => 28,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-05-26 14:49:30',
                'updated_at' => '2017-05-26 14:49:30',
            ),
            10 =>
            array (
                'id' => 12,
                'name' => 'Rehabilitation',
                'cas_group_type_id' => 11,
                'code' => 'c2',
                'sort_order' => 2,
                'deleted_at' => NULL,
                'created_by' => 28,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-05-26 15:09:39',
                'updated_at' => '2017-05-26 15:09:39',
            ),
            11 =>
            array (
                'id' => 13,
                'name' => 'Equipment',
                'cas_group_type_id' => 11,
                'code' => 'c3',
                'sort_order' => 3,
                'deleted_at' => NULL,
                'created_by' => 28,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-05-26 15:09:57',
                'updated_at' => '2017-05-26 15:09:57',
            ),
            12 =>
            array (
                'id' => 11,
                'name' => 'Construction',
                'cas_group_type_id' => 12,
                'code' => 'c1',
                'sort_order' => 1,
                'deleted_at' => NULL,
                'created_by' => 28,
                'deleted_by' => NULL,
                'update_by' => 27,
                'created_at' => '2017-05-26 15:08:38',
                'updated_at' => '2017-05-27 08:30:55',
            ),
            13 =>
            array (
                'id' => 15,
                'name' => 'Total',
                'cas_group_type_id' => 14,
                'code' => '12',
                'sort_order' => 3,
                'deleted_at' => NULL,
                'created_by' => 28,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-05-28 15:02:26',
                'updated_at' => '2017-05-28 15:02:26',
            ),
            14 =>
            array (
                'id' => 14,
                'name' => '% of Total OPD Cases',
                'cas_group_type_id' => 13,
                'code' => 'c1',
                'sort_order' => 4,
                'deleted_at' => NULL,
                'created_by' => 28,
                'deleted_by' => NULL,
                'update_by' => 28,
                'created_at' => '2017-05-28 14:56:49',
                'updated_at' => '2017-05-28 15:02:39',
            ),
            15 =>
            array (
                'id' => 17,
            'name' => 'VHDs(VHWs)',
                'cas_group_type_id' => 15,
                'code' => 'c2',
                'sort_order' => 2,
                'deleted_at' => '2017-05-29 08:57:11',
                'created_by' => 28,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-05-29 08:50:45',
                'updated_at' => '2017-05-29 08:57:11',
            ),
            16 =>
            array (
                'id' => 16,
                'name' => 'cIMCI',
                'cas_group_type_id' => 15,
                'code' => '1',
                'sort_order' => 1,
                'deleted_at' => '2017-05-29 08:57:25',
                'created_by' => 28,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-05-29 08:50:04',
                'updated_at' => '2017-05-29 08:57:25',
            ),
            17 =>
            array (
                'id' => 18,
                'name' => 'Priority Area',
                'cas_group_type_id' => 16,
                'code' => '1',
                'sort_order' => 1,
                'deleted_at' => '2017-05-29 09:28:15',
                'created_by' => 28,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-05-29 09:15:11',
                'updated_at' => '2017-05-29 09:28:15',
            ),
            18 =>
            array (
                'id' => 19,
                'name' => 'Priority area',
                'cas_group_type_id' => 16,
                'code' => '1',
                'sort_order' => 1,
                'deleted_at' => '2017-05-29 09:46:39',
                'created_by' => 28,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-05-29 09:40:22',
                'updated_at' => '2017-05-29 09:46:39',
            ),
            19 =>
            array (
                'id' => 20,
                'name' => 'Cash',
                'cas_group_type_id' => 17,
                'code' => '1',
                'sort_order' => 1,
                'deleted_at' => NULL,
                'created_by' => 29,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-06-02 01:28:59',
                'updated_at' => '2017-06-02 01:28:59',
            ),
            20 =>
            array (
                'id' => 21,
                'name' => 'In Kind',
                'cas_group_type_id' => 17,
                'code' => '2',
                'sort_order' => 2,
                'deleted_at' => NULL,
                'created_by' => 29,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'created_at' => '2017-06-02 01:29:21',
                'updated_at' => '2017-06-02 01:29:21',
            ),
        ));


    }
}
