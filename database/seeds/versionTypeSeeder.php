<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class versionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/version_types.xlsx');
        $versionTypeSheet = $objPHPExcel->getSheet(0);

        for ($row = 1; $row <=$versionTypeSheet->getHighestRow(); ++$row) {
            try {
                DB::table('version_types')->insert([
                    'name' =>$versionTypeSheet->getCell('B'.$row)->getValue(),
                    'code' => $versionTypeSheet->getCell('F'.$row)->getValue()
                ]);
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }

        }
    }
}
