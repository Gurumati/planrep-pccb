<?php

use Illuminate\Database\Seeder;

class peSubmissionFormsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BudgetSubmissionFormsTableSeeder::class);
        $this->call(BudgetSubmissionSubFormsTableSeeder::class);
        $this->call(BudgetSubmissionDefinitionsTableSeeder::class);
        $this->call(BudgetSubmissionSelectOptionTableSeeder::class);
        $this->call(PeSubmissionFormsReportsTableSeeder::class);
    }
}
