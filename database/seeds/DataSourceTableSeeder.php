<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DataSourceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('data_sources')->insert([
            'name' => 'MUSE',
            'code' => '2',
        ]);
        DB::table('data_sources')->insert([
            'name' => 'ENTRY',
            'code' => '3',
        ]);
        DB::table('data_sources')->insert([
            'name' => 'OTHER',
            'code' => '1',
        ]);
    }
}
