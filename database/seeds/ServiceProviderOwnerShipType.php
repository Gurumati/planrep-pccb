<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceProviderOwnerShipType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('facility_ownerships')->insert([
            'name' => 'Public',
            'code' => '1'
        ]);
    }
}
