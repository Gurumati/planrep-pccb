<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BudgetSubmissionSubFormsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('budget_submission_sub_forms')->delete();

        DB::table('budget_submission_sub_forms')->insert(array (
            0 =>
            array (
                'id' => 4,
                'parent_id' => NULL,
                'is_lowest' => false,
                'created_at' => '2017-05-27 07:45:22',
                'updated_at' => '2017-05-27 07:45:22',
                'deleted_at' => Carbon::now(),
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'code' => '08',
                'name' => 'Form No. 8',
                'budget_submission_form_id' => 4,
                'sort_order' => NULL,
            ),
            1 =>
            array (
                'id' => 7,
                'parent_id' => 4,
                'is_lowest' => false,
                'created_at' => '2017-05-27 07:53:32',
                'updated_at' => '2017-05-27 07:53:32',
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'code' => '08c',
                'name' => 'Form No. 8c: Existing employees on Payroll',
                'budget_submission_form_id' => 4,
                'sort_order' => NULL,
            ),
            2 =>
            array (
                'id' => 8,
                'parent_id' => 4,
                'is_lowest' => false,
                'created_at' => '2017-05-27 07:54:45',
                'updated_at' => '2017-05-27 07:54:45',
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'code' => '08d',
                'name' => 'Form No. 8d: Existing employees not on payroll',
                'budget_submission_form_id' => 4,
                'sort_order' => NULL,
            ),
            3 =>
            array (
                'id' => 9,
                'parent_id' => 4,
                'is_lowest' => false,
                'created_at' => '2017-05-27 07:56:12',
                'updated_at' => '2017-05-27 07:56:12',
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'code' => '08e',
                'name' => 'Form No. 8e: New Employees to be recruited',
                'budget_submission_form_id' => 4,
                'sort_order' => NULL,
            ),
            4 =>
            array (
                'id' => 10,
                'parent_id' => NULL,
                'is_lowest' => false,
                'created_at' => '2017-05-27 07:58:13',
                'updated_at' => '2017-05-27 07:58:13',
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'code' => '08f',
                'name' => 'Form No. 8F: List of Employees to be Deleted on the payroll',
                'budget_submission_form_id' => 5,
                'sort_order' => NULL,
            ),
            5 =>
            array (
                'id' => 11,
                'parent_id' => NULL,
                'is_lowest' => false,
                'created_at' => '2017-05-27 07:58:53',
                'updated_at' => '2017-05-27 07:59:29',
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'code' => '09',
                'name' => 'Form No. 9: Establishment and Strength',
                'budget_submission_form_id' => 6,
                'sort_order' => NULL,
            ),
            6 =>
            array (
                'id' => 13,
                'parent_id' => 12,
                'is_lowest' => false,
                'created_at' => '2017-05-27 09:31:15',
                'updated_at' => '2017-05-27 09:33:19',
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'code' => '08a',
                'name' => 'Form No.8a: Summary Exist employee on, not on pay roll and new employees',
                'budget_submission_form_id' => 7,
                'sort_order' => NULL,
            ),
        ));


    }
}
