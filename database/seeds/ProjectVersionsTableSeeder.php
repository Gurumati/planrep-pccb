<?php

use Illuminate\Database\Seeder;
use App\Models\Setup\ProjectVersion;
use Illuminate\Support\Facades\DB;

class ProjectVersionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = DB::select("select id,code from projects where deleted_at is null");
        $projectVersionId = 9;
        foreach ($items as $item) {
            $count = DB::table('projects as p')
                ->join('project_versions as pv','p.id','pv.project_id')
                ->where('p.id',$item->id)
                ->where('p.code',$item->code)
                ->where('version_id',$projectVersionId)->count();
            if($count == 0) {
                $obj = new ProjectVersion();
                $obj->project_id = $item->id;
                $obj->version_id = $projectVersionId;
                $obj->save();
            }
        }
    }
}
