<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/Roles.xlsx');
        $rolesSheet = $objPHPExcel->getSheet(0);

        for ($row = 1; $row <=$rolesSheet->getHighestRow(); ++$row) {
            if($row > 1){
                DB::table('roles')->insert([
                    'slug' => $rolesSheet->getCell('A'.$row)->getValue(),
                    'name' => $rolesSheet->getCell('B'.$row)->getValue(),
                    'admin_hierarchy_level_id' => $rolesSheet->getCell('D'.$row)->getValue(),
                    'permissions' =>'{}',
                    'is_super_admin' => false,
                ]);
            }
        }
    }
}
