<?php

use Illuminate\Database\Seeder;

class GFSCodeCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/gfs_code_categories.xlsx');
        $gfscategorySheet = $objPHPExcel->getSheet(0);

        for ($row = 1; $row <=$gfscategorySheet->getHighestRow(); ++$row) {
            DB::table('gfs_code_categories')->insert([
                'id' => $gfscategorySheet->getCell('A'.$row)->getValue(),
                'name' => $gfscategorySheet->getCell('B'.$row)->getValue(),
                'description' => $gfscategorySheet->getCell('C'.$row)->getValue(),
                'sort_order' => $gfscategorySheet->getCell('J'.$row)->getValue()
            ]);
        }
    }
}
