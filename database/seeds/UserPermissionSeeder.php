<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/access_rights.xlsx');
        $permissionSheet = $objPHPExcel->getSheet(0);

        for ($row = 1; $row <=$permissionSheet->getHighestRow(); ++$row) {
            try {
                DB::table('access_rights')->insert([
                    'description' => $permissionSheet->getCell('C'.$row)->getValue(),
                    'name' => $permissionSheet->getCell('B'.$row)->getValue()
                ]);
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }

        }
    }

}
