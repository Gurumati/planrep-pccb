<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PeSubmissionFormsReportsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('pe_submission_forms_reports')->delete();

        DB::table('pe_submission_forms_reports')->insert(array (
            0 =>
            array (
                'id' => 3,
                'name' => 'Summary of Personal Emoluments Estimates',
                'title' => 'Summary: Existing Employees on PayRoll',
                'params' => '7',
                'parent_id' => NULL,
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'active' => true,
            'code' => '8(c)',
                'is_aggregate' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'sql_query' => 'select bs.name,
bv.budget_submission_definition_id as column_id,
bf.field_name as column_name,
s.id as row_id,
s.code || \'  :  \' || s.name as row_name,
sum(cast(bv.field_value as float))  as field_value
from
budget_submission_line_values bv inner join budget_submission_definitions bf
on bv.budget_submission_definition_id=bf.id inner join budget_submission_lines bl
on bv.budget_submission_line_id = bl.id inner join budget_submission_sub_forms bs
on bl.budget_submission_sub_form_id = bs.id inner join sections s
on bl.section_id = s.id inner join activities a
on bl.activity_id = a.id inner join mtef_sections ms
on a.mtef_section_id = ms.id inner join mtefs m
on ms.mtef_id = m.id
where
bl.budget_submission_sub_form_id = ? and
m.admin_hierarchy_id = ? and
m.financial_year_id = ?  and
bl.fund_source_id =?
group by
bs.name,
bv.budget_submission_definition_id,
bf.field_name,
s.id,
s.name
order by
s.id',
            ),
            1 =>
            array (
                'id' => 5,
                'name' => 'Summary of Personal Emoluments Estimates',
                'title' => 'Summary: New Employees to be Recruited',
                'params' => '9',
                'parent_id' => NULL,
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'active' => true,
            'code' => '8(e)',
                'is_aggregate' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'sql_query' => 'select bs.name,
bv.budget_submission_definition_id as column_id,
bf.field_name as column_name,
s.id as row_id,
s.code || \'  :  \' || s.name as row_name,
sum(cast(bv.field_value as float))  as field_value
from
budget_submission_line_values bv inner join budget_submission_definitions bf
on bv.budget_submission_definition_id=bf.id inner join budget_submission_lines bl
on bv.budget_submission_line_id = bl.id inner join budget_submission_sub_forms bs
on bl.budget_submission_sub_form_id = bs.id inner join sections s
on bl.section_id = s.id inner join activities a
on bl.activity_id = a.id inner join mtef_sections ms
on a.mtef_section_id = ms.id inner join mtefs m
on ms.mtef_id = m.id
where
bl.budget_submission_sub_form_id = ? and
m.admin_hierarchy_id = ? and
m.financial_year_id = ?  and
bl.fund_source_id =?
group by
bs.name,
bv.budget_submission_definition_id,
bf.field_name,
s.id,
s.name
order by
s.id',
            ),
            2 =>
            array (
                'id' => 2,
                'name' => 'Summary of Personal Emoluments Estimates',
                'title' => 'Summary ITEM I,II,III',
                'params' => '7,8,9',
                'parent_id' => NULL,
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'active' => true,
            'code' => '8(b)',
                'is_aggregate' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'sql_query' => 'select bs.name,
bv.budget_submission_definition_id as column_id,
bf.field_name as column_name,
s.id as row_id,
s.name as row_name,
sum(cast(bv.field_value as float))  as field_value
from
budget_submission_line_values bv inner join budget_submission_definitions bf
on bv.budget_submission_definition_id=bf.id inner join budget_submission_lines bl
on bv.budget_submission_line_id = bl.id inner join budget_submission_sub_forms bs
on bl.budget_submission_sub_form_id = bs.id inner join sections s
on bl.section_id = s.id inner join activities a
on bl.activity_id = a.id inner join mtef_sections ms
on a.mtef_section_id = ms.id inner join mtefs m
on ms.mtef_id = m.id
where
bl.budget_submission_sub_form_id in (?,?,?) and
m.admin_hierarchy_id = ? and
m.financial_year_id = ?  and
bl.fund_source_id =?
group by
bs.name,
bv.budget_submission_definition_id,
bf.field_name,
s.id,
s.name
order by
s.id',
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'Summary of Personal Emoluments Estimates',
                'title' => 'Summary:Existing Employees Not on PayRoll',
                'params' => '8',
                'parent_id' => NULL,
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'active' => true,
            'code' => '8(d)',
                'is_aggregate' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'sql_query' => 'select bs.name,
bv.budget_submission_definition_id as column_id,
bf.field_name as column_name,
s.id as row_id,
s.code || \'  :  \' || s.name as row_name,
sum(cast(bv.field_value as float))  as field_value
from
budget_submission_line_values bv inner join budget_submission_definitions bf
on bv.budget_submission_definition_id=bf.id inner join budget_submission_lines bl
on bv.budget_submission_line_id = bl.id inner join budget_submission_sub_forms bs
on bl.budget_submission_sub_form_id = bs.id inner join sections s
on bl.section_id = s.id inner join activities a
on bl.activity_id = a.id inner join mtef_sections ms
on a.mtef_section_id = ms.id inner join mtefs m
on ms.mtef_id = m.id
where
bl.budget_submission_sub_form_id = ? and
m.admin_hierarchy_id = ? and
m.financial_year_id = ?  and
bl.fund_source_id =?
group by
bs.name,
bv.budget_submission_definition_id,
bf.field_name,
s.id,
s.name
order by
s.id',
            ),
            4 =>
            array (
                'id' => 7,
                'name' => 'Summary of Personal Emoluments Estimates',
                'title' => 'Salaries for GS2 and above',
                'params' => '11',
                'parent_id' => NULL,
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'active' => true,
                'code' => '9',
                'is_aggregate' => false,
                'created_at' => NULL,
                'updated_at' => NULL,
                'sql_query' => 'select bs.name,
bv.budget_submission_definition_id as column_id,
bf.field_name as column_name,
s.id as row_id,
s.code || \'  :  \' || s.name as row_name,
bv.field_value as float  as field_value
from
budget_submission_line_values bv inner join budget_submission_definitions bf
on bv.budget_submission_definition_id=bf.id inner join budget_submission_lines bl
on bv.budget_submission_line_id = bl.id inner join budget_submission_sub_forms bs
on bl.budget_submission_sub_form_id = bs.id inner join sections s
on bl.section_id = s.id inner join activities a
on bl.activity_id = a.id inner join mtef_sections ms
on a.mtef_section_id = ms.id inner join mtefs m
on ms.mtef_id = m.id
where
bl.budget_submission_sub_form_id = ? and
m.admin_hierarchy_id = ? and
m.financial_year_id = ?  and
bl.fund_source_id =?
order by
s.id',
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'List of Employees to be Retired',
                'title' => 'List of Employees to be Retired',
                'params' => '10',
                'parent_id' => NULL,
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'active' => true,
            'code' => '8(f)',
                'is_aggregate' => false,
                'created_at' => NULL,
                'updated_at' => NULL,
                'sql_query' => 'select bs.name,
bv.budget_submission_definition_id as column_id,
bf.field_name as column_name,
s.id as row_id,
s.code || \'  :  \' || s.name as row_name,
bv.field_value as  field_value
from
budget_submission_line_values bv inner join budget_submission_definitions bf
on bv.budget_submission_definition_id=bf.id inner join budget_submission_lines bl
on bv.budget_submission_line_id = bl.id inner join budget_submission_sub_forms bs
on bl.budget_submission_sub_form_id = bs.id inner join sections s
on bl.section_id = s.id inner join activities a
on bl.activity_id = a.id inner join mtef_sections ms
on a.mtef_section_id = ms.id inner join mtefs m
on ms.mtef_id = m.id
where
bl.budget_submission_sub_form_id = ? and
m.admin_hierarchy_id = ? and
m.financial_year_id = ?  and
bl.fund_source_id =?
order by
s.id',
            ),
            6 =>
            array (
                'id' => 1,
                'name' => 'ddasda',
                'title' => 'dasdsadsad',
                'params' => '4',
                'parent_id' => 2,
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'active' => true,
                'code' => 'qewqewqe',
                'is_aggregate' => true,
                'created_at' => '2017-06-12 06:37:29',
                'updated_at' => '2017-06-12 06:37:29',
                'sql_query' => 'qwewqewe',
            ),
        ));


    }
}
