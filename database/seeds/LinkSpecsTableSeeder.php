<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LinkSpecsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('link_specs')->insert([
            'name' => 'Task Nature',
            'code' => '1',
            'has_gfs_codes' => true,
        ]);
        DB::table('link_specs')->insert([
            'name' => 'Cost Centre',
            'code' => '2',
            'has_gfs_codes' => false,
        ]);
        DB::table('link_specs')->insert([
            'name' => 'Activity Type',
            'code' => '3',
            'has_gfs_codes' => false,
        ]);
        DB::table('link_specs')->insert([
            'name' => 'LGA Level',
            'code' => '4',
            'has_gfs_codes' => false,
        ]);
    }
}
