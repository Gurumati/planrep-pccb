<?php

use App\Models\Setup\FinancialYear;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FinancialYearsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('financial_years')->insert([
            'name'=>'2021/22',
            'description' =>'Prev FY',
            'start_date' =>'2021-07-01',
            'end_date'=>'2022-06-30',
            'status' => -1,
            'sort_order'=>null,
            'is_active'=>true,
            'previous'=>null,
            'is_current'=>false
        ]);
        DB::table('financial_years')->insert([
            'name'=>'2022/203',
            'description' =>'Prev FY',
            'start_date' =>'2022-07-01',
            'end_date'=>'2023-06-30',
            'status' =>2,
            'sort_order'=>null,
            'is_active'=>true,
            'previous'=>null,
            'is_current'=>false
        ]);
        DB::table('financial_years')->insert([
            'name'=>'2023/24',
            'description' =>'Current',
            'start_date' =>'2023-07-01',
            'end_date'=>'2024-06-30',
            'status' => 1,
            'sort_order'=>null,
            'is_active'=>true,
            'previous'=>null,
            'is_current'=>false
        ]);
        DB::table('financial_years')->insert([
            'name'=>'2024/25',
            'description' =>'Next Financial Years',
            'start_date' =>'2024-07-01',
            'end_date'=>'2025-06-30',
            'status' => 3,
            'sort_order'=>null,
            'is_active'=>true,
            'previous'=>null,
            'is_current'=>false
        ]);
        DB::table('financial_years')->insert([
            'name'=>'2025/26',
            'description' =>'Next Financial Years',
            'start_date' =>'2025-07-01',
            'end_date'=>'2026-06-30',
            'status' => 0,
            'sort_order'=>null,
            'is_active'=>true,
            'previous'=>null,
            'is_current'=>false
        ]);
    }
}
