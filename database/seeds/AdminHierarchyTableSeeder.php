<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminHierarchyTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $objPHPExcel = PHPExcel_IOFactory::load('database/data/AdminHierachy.xlsx');
        $Sheet = $objPHPExcel->getSheet(0);

        echo '--------- Start Seeding Admin hierarchies -------' ;

        for ($row = 1; $row <=$Sheet->getHighestRow(); ++$row) {
            if($row > 1){
                DB::table('admin_hierarchies')->insert([
                    'id' => $Sheet->getCell('A'.$row)->getValue(),
                    'name' => $Sheet->getCell('B'.$row)->getValue(),
                    'code' => $Sheet->getCell('C'.$row)->getValue(),
                    'admin_hierarchy_level_id' => $Sheet->getCell('D'.$row)->getValue(),
                    'sort_order' => $row-1,
                    'is_active' => true,
                    'pisc_type_id' =>$Sheet->getCell('G'.$row)->getValue() == ''?null:$Sheet->getCell('G'.$row)->getValue(),
                    'parent_id' => $Sheet->getCell('F'.$row)->getValue() == ''? null : $Sheet->getCell('F'.$row)->getValue() ,
                ]);
            }
        }


    }
}
