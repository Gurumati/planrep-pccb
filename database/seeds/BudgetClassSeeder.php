<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BudgetClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('budget_classes')->insert([
            'name' => 'Recurrent Budget Class',
            'code' => '100',
            'is_active' => true
        ]);

        DB::table('budget_classes')->insert([
            'name' => 'Development Budget Class',
            'code' => '200',
            'is_active' => true
        ]);

        DB::table('budget_classes')->insert([
            'name' => 'Revenue Budget Class',
            'code' => '500',
            'is_active' => true
        ]);

        DB::table('budget_classes')->insert([
            'name' => 'Recurrent Expenditure - Personnel Emoluments (PE)',
            'code' => '101',
            'is_active' => true,
            'parent_id' => 1
        ]);

        DB::table('budget_classes')->insert([
            'name' => 'Recurrent Expenditure - Other Charges (OC)',
            'code' => '102',
            'is_active' => true,
            'parent_id' => 1
        ]);

        DB::table('budget_classes')->insert([
            'name' => 'Own Source Recurrent',
            'code' => '103',
            'is_active' => true,
            'parent_id' => 1
        ]);

        DB::table('budget_classes')->insert([
            'name' => 'Development Expenditure - Local',
            'code' => '201',
            'is_active' => true,
            'parent_id' => 2
        ]);

        DB::table('budget_classes')->insert([
            'name' => 'Development Expenditure - Foreign',
            'code' => '202',
            'is_active' => true,
            'parent_id' => 2
        ]);

        DB::table('budget_classes')->insert([
            'name' => 'Own Source Development',
            'code' => '203',
            'is_active' => true,
            'parent_id' => 2
        ]);

        DB::table('budget_classes')->insert([
            'name' => 'Special Funds',
            'code' => '204',
            'is_active' => true,
            'parent_id' => 2
        ]);

        DB::table('budget_classes')->insert([
            'name' => 'Revenue',
            'code' => '501',
            'is_active' => true,
            'parent_id' => 3
        ]);

        DB::table('budget_classes')->insert([
            'name' => 'Revenue BoT',
            'code' => '502',
            'is_active' => true,
            'parent_id' => 3
        ]);
    }
}
