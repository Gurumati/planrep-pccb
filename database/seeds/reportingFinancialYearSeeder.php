<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class reportingFinancialYearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configuration_settings')->where('key','AUTO_FINANCIAL_YEAR_TO_REPORT')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'AUTO_FINANCIAL_YEAR_TO_REPORT',
            'value' => null,
            'group_name'=>'Financial Year Initialization',
            'name' => 'Activate default financial year to report',
            'description' => 'Activate default financial year to report',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value , name as name from financial_years',
            'is_configurable' => true,
        ]);
    }
}
