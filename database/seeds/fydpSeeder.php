<?php

use App\Models\Setup\Reference;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class fydpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo '-----Seeding SDG ---------'."\r\n";
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/sdg.xlsx');
        $sdgSheet = $objPHPExcel->getSheet(0);

        $date = Carbon::now();

        //sdg Sheet
        for ($row = 1; $row <=$sdgSheet->getHighestRow(); ++$row) {
            if($row > 1){
                DB::table('reference_docs')->insert([
                    'id' => $sdgSheet->getCell('A'.$row)->getValue(),
                    'name' => $sdgSheet->getCell('B'.$row)->getValue(),
                    'code' => $sdgSheet->getCell('D'.$row)->getValue(),
                    'description' => $sdgSheet->getCell('B'.$row)->getValue(),
                    'is_active' => true,
                    'reference_type_id' => 2,
                    'parent_id' => $sdgSheet->getCell('C'.$row)->getValue()?$sdgSheet->getCell('C'.$row)->getValue():null,
                    'link_level' => trim($sdgSheet->getCell('E'.$row)->getValue()),
                    'created_at' => Carbon::now(),
                ]);
            }
        }

        echo '-----FInishing Seeding References ---------'."\r\n";
    }
}
