<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class setMaxValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo '-----Setting Table id Sequence ---------'."\r\n";

        $TableList = array();
        $TableList = ['plan_chains',
        'plan_chain_versions',
        'account_types',
    'activity_categories','activity_statuses','activity_task_natures','admin_hierarchy_levels',
    'admin_hierarchies','budget_submission_definitions','budget_submission_forms',
'budget_submission_select_option','budget_submission_sub_forms',
'cas_group_columns','cas_group_types','cas_plan_contents','cas_plans','cas_plan_table_columns',
'cas_plan_table_items','cas_plan_table_report_constraints',
'cas_plan_table_select_options','cas_plan_tables',
'geographical_location_levels','geographical_locations',
'gfs_code_categories','gfs_codes','gfs_code_sub_categories',
'pe_submission_forms_reports','lga_levels','plan_chain_types',
'procurement_types','units','versions','version_types','fund_sources','fund_source_versions',
'budget_classes','budget_class_versions','sectors','sections','configuration_settings','fund_source_categories','ceilings'];

        for ($table = 0; $table < sizeof($TableList); ++$table) {
            echo 'Setting Sequency to '.$TableList[$table]."\r\n";
            $results = DB::select( DB::raw("SELECT setval(pg_get_serial_sequence('$TableList[$table]' , 'id'), coalesce((max(id) + 1),1), false) FROM ".$TableList[$table]));
            echo 'Sequency To '.$TableList[$table].'Set to  '. $results[0]->setval ."\r\n";
        }

    }
}
