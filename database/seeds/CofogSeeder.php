<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CofogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/cofogs.xlsx');
        $cofogSheet = $objPHPExcel->getSheet(0);


        for ($row = 1; $row <=$cofogSheet->getHighestRow(); ++$row) {
            if($row >1){
                DB::table('cofogs')->insert([
                    'id' => $cofogSheet->getCell('A'.$row)->getValue(),
                    'name' => $cofogSheet->getCell('B'.$row)->getValue(),
                    'code' => $cofogSheet->getCell('A'.$row)->getValue(),
                    'parent_id' =>$cofogSheet->getCell('C'.$row)->getValue()?$cofogSheet->getCell('C'.$row)->getValue():null
                ]);
            }
        }

    }
}
