<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PE_Definition extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $collumn_no = 100;
        $definitions =  DB::select( DB::raw("select id ,field_name,budget_submission_form_id,column_number,parent_id from budget_submission_definitions where column_number in (1,2,3,4,5,9)
        and id NOT IN (58) and budget_submission_form_id not in  (5,6)
        order by  id") );
        foreach ($definitions as &$value) {
            $collumn_no = $collumn_no + 1;
            echo 'Updating Definitions '.$value->field_name. "\r\n";
            DB::table('budget_submission_definitions')->where('id', $value->id)->update(['column_number' => $collumn_no]);
        }
    }
}
