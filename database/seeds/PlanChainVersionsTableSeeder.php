<?php

use Illuminate\Database\Seeder;
use App\Models\Setup\PlanChainVersion;
use Illuminate\Support\Facades\DB;

class PlanChainVersionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = DB::select("select id,code from plan_chains where deleted_at is null");
        $versionId = 1;
        foreach ($items as $item) {
            $count = DB::table('plan_chains as a')
                ->join('plan_chain_versions as b','a.id','b.plan_chain_id')
                ->where('a.id', $item->id)
                ->where('a.code', $item->code)
                ->where('b.version_id', $versionId)->count();
            if ($count == 0) {
                $obj = new PlanChainVersion();
                $obj->plan_chain_id = $item->id;
                $obj->version_id = $versionId;
                $obj->save();
            }
        }
    }
}
