<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlanChainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plan_chain_types')->insert([
            'id' => 1,
            'name' => 'Objective',
            'hierarchy_level' => 1,
            'is_incremental' => true,
            'is_active' => true,
            'is_sectoral' => true,
        ]);

        DB::table('plan_chain_types')->insert([
            'id' => 2,
            'name' => 'Service Output',
            'hierarchy_level' => 2,
            'is_incremental' => false,
            'is_active' => true,
            'is_sectoral' => false,
        ]);

        $objPHPExcel = PHPExcel_IOFactory::load('database/data/plan_chains.xlsx');
        $planChainSheet = $objPHPExcel->getSheet(0);

        for ($row = 1; $row <=$planChainSheet->getHighestRow(); ++$row) {
            if( $planChainSheet->getCell('G'.$row)->getValue() == null){
                DB::table('plan_chains')->insert([
                    'id' => $planChainSheet->getCell('A'.$row)->getValue(),
                    'description' =>$planChainSheet->getCell('B'.$row)->getValue(),
                    'is_active' => $planChainSheet->getCell('D'.$row)->getValue(),
                    'parent_id' => $planChainSheet->getCell('K'.$row)->getValue() == ''?null:$planChainSheet->getCell('K'.$row)->getValue(),
                    'code' => $planChainSheet->getCell('C'.$row)->getValue(),
                    'plan_chain_type_id' => $planChainSheet->getCell('L'.$row)->getValue()
                ]);
            }

        }


    }
}
