<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BudgetSubmissionFormsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('budget_submission_forms')->delete();

        DB::table('budget_submission_forms')->insert(array (
            0 =>
            array (
                'id' => 3,
                'name' => 'HRO',
                'description' => 'PE',
                'created_at' => '2017-05-27 06:22:49',
                'updated_at' => '2017-05-27 06:27:56',
                'deleted_at' => '2017-05-27 06:27:56',
                'created_by' => 78,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'budget_classes' => '4',
                'fund_sources' => '25',
            ),
            1 =>
            array (
                'id' => 4,
                'name' => 'Form No. 8',
            'description' => 'Personal Emoluments (8a to 8e)',
                'created_at' => '2017-05-27 07:30:30',
                'updated_at' => '2017-05-27 07:30:30',
                'deleted_at' => NULL,
                'created_by' => 58,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'budget_classes' => '4,7',
                'fund_sources' => '26,58',
            ),
            2 =>
            array (
                'id' => 5,
                'name' => 'Form No. 8F',
                'description' => 'List of employees to be Deleted on Payroll',
                'created_at' => '2017-05-27 07:34:58',
                'updated_at' => '2017-05-27 07:34:58',
                'deleted_at' => NULL,
                'created_by' => 58,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'budget_classes' => '4,7',
                'fund_sources' => '26,58',
            ),
            3 =>
            array (
                'id' => 6,
                'name' => 'Form No. 9',
                'description' => 'Establishment',
                'created_at' => '2017-05-27 07:38:47',
                'updated_at' => '2017-05-27 07:38:47',
                'deleted_at' => NULL,
                'created_by' => 58,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'budget_classes' => '4,7',
                'fund_sources' => '26,58',
            ),
            4 =>
            array (
                'id' => 7,
                'name' => 'Form No. 8a',
                'description' => 'Summary Exist employee on, not on pay roll and new employees',
                'created_at' => '2017-05-27 08:56:56',
                'updated_at' => '2017-05-28 10:44:18',
                'deleted_at' => '2017-05-28 10:44:18',
                'created_by' => 58,
                'deleted_by' => NULL,
                'updated_by' => NULL,
                'budget_classes' => '4,7',
                'fund_sources' => '26,58',
            ),
        ));


    }
}
