<?php

use App\Models\Budgeting\Ceiling;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ceilingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Mapping BudgetClass GFS FundSource
        echo 'Creating Ceilings' . "\r\n";
        $date = Carbon::now();

        $objPHPExcel = PHPExcel_IOFactory::load('database/data/ceiling.xlsx');
        $CeilingSheet = $objPHPExcel->getSheet();
        $maxId = Ceiling::max('id') + 1;
        for ($row = 1; $row <= $CeilingSheet->getHighestRow(); ++$row) {
            if ($row > 1) {
                $maxId++;
                $gfsCode = $CeilingSheet->getCell('B' . $row)->getValue();
                $fundSourceCode = $CeilingSheet->getCell('C' . $row)->getValue();
                $budgetClassCode = $CeilingSheet->getCell('A' . $row)->getValue();

                $aggrigate_fundSource_id = null;
                $gfsCodeId = null;
                $isaggrigate = false;
                $name = null;

                $budgetClassId = DB::table('budget_classes')->where('code', $budgetClassCode)->first()->id;
                if ($fundSourceCode != '') {
                    $aggrigate_fundSource_id = DB::table('fund_sources')->where('code', $fundSourceCode)->first()->id;
                    $isaggrigate = true;
                }
                if ($gfsCode != '') {
                    $gfs_code = DB::table('gfs_codes')->where('code', $gfsCode)->first();
                    $gfsCodeId = $gfs_code != null?$gfs_code->id:2754;//99999999 to reserve gfs_code in ceiling.xlsx not in gfs_codes table
                    $gfs_name = DB::table('gfs_codes')->where('code', $gfsCode)->first();
                    $name = $gfs_name != null?$gfs_name->name:'Reserved for future system use';
                } else {
                    $name = DB::table('budget_classes')->where('code', $budgetClassCode)->first()->name;
                }

                $count = Ceiling::where('gfs_code_id', $gfsCodeId)
                    ->where('budget_class_id', $budgetClassId)
                    ->count();
                if ($count < 1) {
                    //Insert Into ceilings
                    DB::table('ceilings')->insert([
                        'id' => $maxId,
                        'name' => $name,
                        'budget_class_id' => $budgetClassId,
                        'aggregate_fund_source_id' => $aggrigate_fundSource_id,
                        'is_aggregate' => $isaggrigate,
                        'created_at' => $date,
                        'updated_at' => $date,
                        'is_active' => true,
                        'gfs_code_id' => $gfsCodeId
                    ]);
                }

            }
        }
    }
}
