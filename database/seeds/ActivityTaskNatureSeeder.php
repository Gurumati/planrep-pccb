<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivityTaskNatureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/activity_task_natures.xlsx');
        $sheet = $objPHPExcel->getSheet(0);

        for ($row = 1; $row <= $sheet->getHighestRow(); ++$row) {
            DB::table('activity_task_natures')->insert([
                'id' => $sheet->getCell('A' . $row)->getValue(),
                'name' => $sheet->getCell('B' . $row)->getValue(),
                'activity_category_id' => $sheet->getCell('I' . $row)->getValue(),
                'code' => $sheet->getCell('J' . $row)->getValue(),
                'is_active' => true,
            ]);
        }
    }
}
