<?php

use Illuminate\Database\Seeder;
use App\Models\Setup\PriorityAreaVersion;
use Illuminate\Support\Facades\DB;

class PriorityAreaVersionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = DB::select("select id,number from priority_areas where deleted_at is null");
        $versionId = 4;
        foreach ($items as $item) {
            $count = DB::table('priority_areas as p')
                ->join('priority_area_versions as pv','p.id','pv.priority_area_id')
                ->where('p.id',$item->id)
                ->where('p.number',$item->number)
                ->where('pv.version_id',$versionId)->count();
            if($count == 0) {
                $obj = new PriorityAreaVersion();
                $obj->priority_area_id = $item->id;
                $obj->version_id = $versionId;
                $obj->save();
            }
        }
    }
}
