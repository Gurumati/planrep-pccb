<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivityStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/activity_statuses.xlsx');
        $sheet = $objPHPExcel->getSheet(0);

        for ($row = 1; $row <= $sheet->getHighestRow(); ++$row) {
            DB::table('activity_statuses')->insert([
                'id' => $sheet->getCell('A' . $row)->getValue(),
                'name' => $sheet->getCell('B' . $row)->getValue(),
                'position' => $sheet->getCell('C' . $row)->getValue(),
                'is_default' => true,
            ]);
        }
    }
}
