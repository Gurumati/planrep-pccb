<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigurationSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/configuration_settings.xlsx');
        $settingsSheet = $objPHPExcel->getSheet(0);

        for ($row = 1; $row <=$settingsSheet->getHighestRow(); ++$row) {

            DB::table('configuration_settings')->insert([
                'id' => $settingsSheet->getCell('A'.$row)->getValue(),
                'key' => $settingsSheet->getCell('B'.$row)->getValue(),
                'value' => $settingsSheet->getCell('C'.$row)->getValue() == ''?null:$settingsSheet->getCell('C'.$row)->getValue(),
                'group_name'=> $settingsSheet->getCell('E'.$row)->getValue(),
                'name' => $settingsSheet->getCell('D'.$row)->getValue(),
                'description' => $settingsSheet->getCell('F'.$row)->getValue(),
                'sort_order' => $settingsSheet->getCell('G'.$row)->getValue(),
                'value_type' => $settingsSheet->getCell('H'.$row)->getValue(),
                'value_options' => $settingsSheet->getCell('I'.$row)->getValue(),
                'value_options_query' => $settingsSheet->getCell('J'.$row)->getValue(),
                'is_configurable' => $settingsSheet->getCell('K'.$row)->getValue(),
            ]);
        }
    }
}
