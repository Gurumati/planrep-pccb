<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiceProviderType extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('facility_types')->insert([
            'name' => 'HQ',
            'code' => '001',
            'admin_hierarchy_level_id' => 2,
            'lga_level_id' => 10
        ]);
        DB::table('facility_types')->insert([
            'name' => 'Zones',
            'code' => '002',
            'admin_hierarchy_level_id' => 2,
            'lga_level_id' => 11
        ]);
        DB::table('facility_types')->insert([
            'name' => 'Campus',
            'code' => '003',
            'admin_hierarchy_level_id' => 2,
            'lga_level_id' => 11
        ]);
        DB::table('facility_types')->insert([
            'name' => 'Plants',
            'code' => '004',
            'admin_hierarchy_level_id' => 2,
            'lga_level_id' => 11
        ]);
        DB::table('facility_types')->insert([
            'name' => 'Regional Office',
            'code' => '005',
            'admin_hierarchy_level_id' => 2,
            'lga_level_id' => 11
        ]);
        DB::table('facility_types')->insert([
            'name' => 'District Office',
            'code' => '006',
            'admin_hierarchy_level_id' => 2,
            'lga_level_id' => 11
        ]);
    }
}
