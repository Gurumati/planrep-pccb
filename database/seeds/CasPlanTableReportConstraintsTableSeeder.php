<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CasPlanTableReportConstraintsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('cas_plan_table_report_constraints')->delete();

        DB::table('cas_plan_table_report_constraints')->insert(array (
            0 =>
            array (
                'id' => 6,
                'limit' => 5,
                'order_by' => '[512]',
                'with_value' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'created_by' => NULL,
                'deleted_by' => NULL,
                'update_by' => 39,
                'cas_plan_table_id' => 1,
            ),
            1 =>
            array (
                'id' => 8,
                'limit' => 2,
                'order_by' => '[569]',
                'with_value' => true,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'created_by' => 39,
                'deleted_by' => NULL,
                'update_by' => NULL,
                'cas_plan_table_id' => 6,
            ),
        ));


    }
}
