<?php

use Illuminate\Database\Seeder;

class ImportVersionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(VersionTypeTableSeeder::class);
        $this->call(VersionsTableSeeder::class);
        $this->call(FinancialYearVersionsTableSeeder::class);
        $this->call(PlanChainVersionsTableSeeder::class);
        $this->call(PerformanceIndicatorVersionsTableSeeder::class);
        $this->call(InterventionVersionsTableSeeder::class);
        $this->call(PriorityAreaVersionsTableSeeder::class);
        $this->call(BudgetClassVersionsTableSeeder::class);
        $this->call(FundSourceVersionsTableSeeder::class);
        $this->call(NationalReferenceTypeVersionsTableSeeder::class);
    }
}
