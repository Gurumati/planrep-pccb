<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FundTransferCode extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    /**run this to execute php artisan db:seed --class=FundTransferCode */
    public function run()
    {
        /**education transfer */
        $existing= DB::table('configuration_settings')->where('key','EDUCATION_TRANSFER')->first();
        DB::table('configuration_settings')->where('key','EDUCATION_TRANSFER')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'EDUCATION_TRANSFER',
            'value' => isset($existing)?$existing->value:'26312106',
            'group_name'=>'Data',
            'name' => 'Education transfer',
            'description' => 'Education funds transfere gfscode',
            'sort_order' => 3,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        /**health transfer */
        $existing= DB::table('configuration_settings')->where('key','HEALTH_TRANSFER')->first();
        DB::table('configuration_settings')->where('key','HEALTH_TRANSFER')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'HEALTH_TRANSFER',
            'value' => isset($existing)?$existing->value:'26312107',
            'group_name'=>'Data',
            'name' => 'Health transfer',
            'description' => 'Health funds transfere gfscode',
            'sort_order' => 3,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);
    }
}
