<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CasPlansTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('cas_plans')->insert([
            'id' => 2,
            'name' => 'Operational Plans',
            'sector_id' => null,
        ]);
        DB::table('cas_plans')->insert([
            'id' => 3,
            'name' => 'MTEF',
            'sector_id' => null,
        ]);
        DB::table('cas_plans')->insert([
            'id' => 1,
            'name' => 'OTR Level Reports',
            'sector_id' => null,
        ]);
    }
}
