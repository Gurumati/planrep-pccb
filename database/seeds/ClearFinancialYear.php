<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClearFinancialYear extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      DB::table('activity_input_breakdowns')->delete();
      DB::table('activity_input_periods')->delete();
      DB::table('activity_inputs')->delete();
      DB::table('activity_funders')->delete();
      DB::table('activity_fund_sources')->delete();
      DB::table('activity_indicators')->delete();
      DB::table('activity_periods')->delete();
      DB::table('act_gen_line_items')->delete();
      DB::table('activities')->delete();
      DB::table('mtef_annual_targets')->delete();
      DB::table('admin_hierarchy_ceilings')->delete();
      DB::table('mtef_sections')->delete();
      DB::table('mtefs')->delete();
      DB::table('financial_years')->update(array('status'=>0));

    }
}
