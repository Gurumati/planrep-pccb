<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class budgetTransactionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = array();
        $types = ['LOCAL CURRENCY','Budget Disapprove','Budget Approve','Change Activity Code','Change Activity Budget Class','Acknowledgement','ACTIVITY','PROJECT','SERVICE PROVIDER'];
        foreach($types as $type){
            DB::table('budget_transaction_types')->insert([
                'name' => $type
                ]);
        }

    }
}
