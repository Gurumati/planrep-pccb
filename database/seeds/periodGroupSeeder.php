<?php

use Illuminate\Database\Seeder;

class periodGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('period_groups')->insert([
            'name' => 'Annually',
            'number' => '1'
        ]);

        DB::table('period_groups')->insert([
            'name' => 'Semi Annual',
            'number' => '2'
        ]);

        DB::table('period_groups')->insert([
            'name' => 'Quaterly',
            'number' => '3'
        ]);

        DB::table('period_groups')->insert([
            'name' => 'Monthly',
            'number' => '4'
        ]);

    }
}
