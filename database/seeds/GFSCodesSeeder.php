<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GFSCodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/all_gfs_codes.xlsx');
        $gfsSheet = $objPHPExcel->getSheet(0);
        $duplicates = '';

        for ($row = 1; $row <=$gfsSheet->getHighestRow(); ++$row) {
            try {
                DB::table('gfs_codes')->insert([
                    'id' => $gfsSheet->getCell('A'.$row)->getValue(),
                    'code' => $gfsSheet->getCell('B'.$row)->getValue(),
                    'description' => $gfsSheet->getCell('C'.$row)->getValue(),
                    'name' => $gfsSheet->getCell('L'.$row)->getValue(),
                    'account_type_id' => $gfsSheet->getCell('D'.$row)->getValue(),
                    'sort_order' => $gfsSheet->getCell('H'.$row)->getValue(),
                    'is_procurement' => $gfsSheet->getCell('M'.$row)->getValue(),
                    'is_active' => $gfsSheet->getCell('N'.$row)->getValue(),
                    'gfs_code_sub_category_id' => $gfsSheet->getCell('P'.$row)->getValue()
                ]);;
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }

        }
    }
}
