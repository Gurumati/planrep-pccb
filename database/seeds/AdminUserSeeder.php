<?php

use Illuminate\Database\Seeder;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $credentials = [
            'user_name'    => 'admin',
            'password' => 'Pccb@2019',
            'email' => 'admin@pccb.go.tz',
            'first_name' => 'PCCB',
            'last_name' => 'ADMIN',
            'is_superuser' => true,
            'admin_hierarchy_id' => 1,
            'section_id' => 1
        ];
        $user = Sentinel::registerAndActivate($credentials);
        $user->permissions = [
            'settings.manage' => true,
            'settings.manage.users' => true,
            'settings.manage.roles' => true,
            'settings.manage.rights' => true
        ];
        $user->save();
    }
}
