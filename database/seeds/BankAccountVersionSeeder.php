<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BankAccountVersionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bankAccount = DB::table('bank_accounts')->select('id','name')->get();
        foreach ($bankAccount as $account) {
            DB::table('bank_account_versions')->insert([
                'bank_account_id' => $account->id,
                'version_id' => 7
            ]);
        }
    }
}
