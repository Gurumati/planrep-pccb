<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UnitsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/units.xlsx');
        $unitsSheet = $objPHPExcel->getSheet(0);

        for ($row = 1; $row <=$unitsSheet->getHighestRow(); ++$row) {
            DB::table('units')->insert([
                'id' => $unitsSheet->getCell('A'.$row)->getValue(),
                'name' => $unitsSheet->getCell('B'.$row)->getValue(),
                'sort_order' => $unitsSheet->getCell('G'.$row)->getValue(),
                'symbol' => $unitsSheet->getCell('C'.$row)->getValue(),
                'is_active' => true
            ]);
        }
    }
}
