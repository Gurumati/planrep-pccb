<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PiscTypeSeeder::class);
        $this->call(AdminHierarchyLevelTableSeeder::class);
        $this->call(AdminHierarchyTableSeeder::class);
        $this->call(ServiceProviderOwnerShipType::class);
        $this->call(ServiceProviderType::class);
        $this->call(SectionLevelTableSeeder::class);
        $this->call(SectorTableSeeder::class);
        $this->call(GeographicalLocationLevelsSeeder::class);
        $this->call(GeographicalLocationSeeder::class);
        $this->call(SectionTableSeeder::class);
        $this->call(UserPermissionSeeder::class);
        $this->call(AdminUserSeeder::class);
        $this->call(ConfigurationSettingSeeder::class);
        $this->call(CalendarEventTableSeeder::class);
        $this->call(SystemTableSeeder::class);
        $this->call(CasPlansTableSeeder::class);
        $this->call(BudgetSubmissionFormsTableSeeder::class);
        $this->call(BudgetSubmissionSubFormsTableSeeder::class);
        $this->call(BudgetSubmissionDefinitionsTableSeeder::class);
        $this->call(PeSubmissionFormsReportsTableSeeder::class);
        $this->call(DecisionLevelsSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(PiscLevelSeeder::class);
        $this->call(AccountTypesSeeder::class);
        $this->call(ProcurementTypesSeeder::class);
        $this->call(UnitsSeeder::class);
        $this->call(FundTypesSeeder::class);
        $this->call(FundSourceCategorySeeder::class);
        $this->call(VersionBudgetFundSourceSeeder::class);
        $this->call(periodGroupSeeder::class);
        $this->call(ActivityCategorySeeder::class);
        $this->call(ActivityTaskNatureSeeder::class);
        $this->call(ActivityStatusSeeder::class);
        $this->call(budgetTransactionTypeSeeder::class);
        $this->call(BankAccountSeeder::class);
        $this->call(CofogSeeder::class);
        $this->call(GFSCodeCategoryAndSubCategory::class);
        $this->call(BankAccountVersionSeeder::class);
        $this->call(FundSourceBClassGfsSectionMappingSeeder::class);
        $this->call(projectSeeder::class);
        $this->call(referenceTypes::class);
        $this->call(fydpSeeder::class);
        $this->call(piscTypeMapping::class);
        $this->call(PiscCofogSeeder::class);
        $this->call(ceilingSeeder::class);
        $this->call(setMaxValueSeeder::class);

    }
}
