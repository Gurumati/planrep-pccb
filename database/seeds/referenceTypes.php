<?php

use App\Models\Setup\Reference;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class referenceTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        echo '-----Seeding References ---------'."\r\n";
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/Ilani_SDG.xlsx');
        $ilaniSheet = $objPHPExcel->getSheet(0);

        $date = Carbon::now();


        //add Refference Types
        DB::table('reference_types')->insert([
            'id' => 1,
            'description' => 'Ruling Part Manifesto - Manifesto 2020 -2025',
            'name' => 'Ruling Part Manifesto - Manifesto 2020 -2025',
            'multiple_select' => true,
            'link_level' => 1,
            'created_at' => $date,
            'updated_at' => $date
        ]);
        DB::table('reference_type_versions')->insert([
            'reference_type_id' => 1,
            'version_id' => 9,
            'created_at' => $date,
            'updated_at' => $date
        ]);
        DB::table('reference_types')->insert([
            'id' => 2,
            'name' => 'Sustainable Development Goals - SDG',
            'description' => 'Sustainable Development Goals - SDG',
            'multiple_select' => true,
            'link_level' => 1,
            'created_at' => $date,
            'updated_at' => $date
        ]);
        DB::table('reference_type_versions')->insert([
            'reference_type_id' => 2,
            'version_id' => 9,
            'created_at' => $date,
            'updated_at' => $date
        ]);
        DB::table('reference_types')->insert([
            'id' => 3,
            'name' => 'Five Years Development Plan Phase III - FYDP-III',
            'description' => 'Five Years Development Plan Phase III - FYDP-III',
            'multiple_select' => true,
            'link_level' => 1,
            'created_at' => $date,
            'updated_at' => $date
        ]);
        DB::table('reference_type_versions')->insert([
            'reference_type_id' => 3,
            'version_id' => 9,
            'created_at' => $date,
            'updated_at' => $date
        ]);

        // Ilani
        for ($row = 1; $row <=$ilaniSheet->getHighestRow(); ++$row) {
            if($row > 1){

                DB::table('reference_docs')->insert([
                    'id' => $ilaniSheet->getCell('A'.$row)->getValue(),
                    'name' => $ilaniSheet->getCell('B'.$row)->getValue(),
                    'code' => $ilaniSheet->getCell('D'.$row)->getValue(),
                    'description' => $ilaniSheet->getCell('B'.$row)->getValue(),
                    'is_active' => true,
                    'reference_type_id' => 1,
                    'parent_id' => $ilaniSheet->getCell('C'.$row)->getValue()?$ilaniSheet->getCell('C'.$row)->getValue():null,
                    'link_level' => trim($ilaniSheet->getCell('E'.$row)->getValue()),
                    'created_at' => Carbon::now(),
                ]);
            }
        }

        echo '-----FInishing Seeding References ---------'."\r\n";
    }
}
