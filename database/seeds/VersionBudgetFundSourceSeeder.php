<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class VersionBudgetFundSourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/version_types.xlsx');
        $objPHPExcelSource = PHPExcel_IOFactory::load('database/data/FundSource.xlsx');
        $objPHPExcelBudgetClass = PHPExcel_IOFactory::load('database/data/budget_classes.xlsx');
        $objPHPExcelPlanChain = PHPExcel_IOFactory::load('database/data/plan_chains.xlsx');
        $planChainSheet = $objPHPExcelPlanChain->getSheet(0);
        $versionTypeSheet = $objPHPExcel->getSheet(0);
        $FundSourceSheet = $objPHPExcelSource->getSheet(0);
        $BudgetClassSheet = $objPHPExcelBudgetClass->getSheet(0);

        for ($row = 1; $row <=$versionTypeSheet->getHighestRow(); ++$row) {
            try {
                DB::table('version_types')->insert([
                    'id' =>$versionTypeSheet->getCell('A'.$row)->getValue(),
                    'name' =>$versionTypeSheet->getCell('B'.$row)->getValue(),
                    'code' => $versionTypeSheet->getCell('F'.$row)->getValue()
                ]);

                DB::table('versions')->insert([
                    'id' =>$row,
                    'name' =>'version 1',
                    'date' => date('Y-m-d'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                    'is_current' => true,
                    'published' => false,
                    'version_type_id' => $versionTypeSheet->getCell('A'.$row)->getValue()

                ]);
                if( $versionTypeSheet->getCell('A'.$row)->getValue() == 5){
                    for ($row1 = 1; $row1 <=$FundSourceSheet->getHighestRow(); ++$row1) {
                        if($row1 >1 &&  $FundSourceSheet->getCell('A'.$row1)->getValue() != ''){
                            DB::table('fund_sources')->insert([
                                'id' => $row1,
                                'code' => $FundSourceSheet->getCell('A'.$row1)->getValue(),
                                'name' => $FundSourceSheet->getCell('B'.$row1)->getValue(),
                                'description' => $FundSourceSheet->getCell('B'.$row1)->getValue(),
                                'fund_source_id' =>$row1,
                                'fund_source_category_id' => (integer)$FundSourceSheet->getCell('C'.$row1)->getValue(),
                                'can_project' => $FundSourceSheet->getCell('E'.$row1)->getValue(),
                                'is_foreign' => $FundSourceSheet->getCell('F'.$row1)->getValue(),
                            ]);

                            $Sectors = explode(",", $FundSourceSheet->getCell('D'.$row1)->getValue());

                            foreach ($Sectors as &$sector) {
                                $selected_sector = trim($sector);
                                DB::table('sector_fund_sources')->insert([
                                    'fund_source_id' => $row1,
                                    'sector_id' => $selected_sector,
                                ]);
                            }

                            DB::table('fund_source_versions')->insert([
                                'fund_source_id' => $row1,
                                'version_id' => $row
                            ]);
                        }

                    }
                }

                if( $versionTypeSheet->getCell('A'.$row)->getValue() == 6){
                    for ($row2 = 1; $row2 <=$BudgetClassSheet->getHighestRow(); ++$row2) {

                        DB::table('budget_classes')->insert([
                            'id' => $BudgetClassSheet->getCell('A'.$row2)->getValue(),
                            'code' => $BudgetClassSheet->getCell('O'.$row2)->getValue(),
                            'name' => $BudgetClassSheet->getCell('B'.$row2)->getValue(),
                            'description' => $BudgetClassSheet->getCell('B'.$row2)->getValue(),
                            'is_active' => true,
                            'is_automatic'=> false,
                            'parent_id' => $BudgetClassSheet->getCell('M'.$row2)->getValue() == ''?null:$BudgetClassSheet->getCell('M'.$row2)->getValue()
                        ]);

                        DB::table('budget_class_versions')->insert([
                            'budget_class_id' => $BudgetClassSheet->getCell('A'.$row2)->getValue(),
                            'version_id' => $row
                        ]);

                    }
                }

                if( $versionTypeSheet->getCell('A'.$row)->getValue() == 1){

                    DB::table('plan_chain_types')->insert([
                        'id' => 1,
                        'name' => 'Service Output',
                        'hierarchy_level' => 2,
                        'is_incremental' => true,
                        'is_active' => true,
                        'is_sectoral' => true,
                    ]);

                    DB::table('plan_chain_types')->insert([
                        'id' => 2,
                        'name' => 'Objective',
                        'hierarchy_level' => 1,
                        'is_incremental' => false,
                        'is_active' => true,
                        'is_sectoral' => false,
                    ]);

                    for ($row3 = 1; $row3 <=$planChainSheet->getHighestRow(); ++$row3) {

                        if( $planChainSheet->getCell('G'.$row3)->getValue() == null){

                            DB::table('plan_chains')->insert([
                                'id' => $planChainSheet->getCell('A'.$row3)->getValue(),
                                'description' =>$planChainSheet->getCell('B'.$row3)->getValue(),
                                'is_active' => $planChainSheet->getCell('D'.$row3)->getValue(),
                                'parent_id' => $planChainSheet->getCell('K'.$row3)->getValue() == ''?null:$planChainSheet->getCell('K'.$row3)->getValue(),
                                'code' => $planChainSheet->getCell('C'.$row3)->getValue(),
                                'plan_chain_type_id' => $planChainSheet->getCell('L'.$row3)->getValue()
                            ]);

                            try {
                                DB::table('plan_chain_versions')->insert([
                                    'plan_chain_id' => $planChainSheet->getCell('A'.$row3)->getValue(),
                                    'version_id' => $row
                                ]);
                            }
                            catch (exception $e) {
                               echo $e;
                            }

                        }

                    }

                }

            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }

        }

    }
}
