<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigurationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $existing=DB::table('configuration_settings')->where('key','AUTO_BUDGET_TARGET_TYPE')->first();
        DB::table('configuration_settings')->where('key','AUTO_BUDGET_TARGET_TYPE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'AUTO_BUDGET_TARGET_TYPE',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Financial Year Initialization',
            'name' => 'Target Type For Auto Budget',
            'description' => 'Target type for auto budget',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value , name as name from activity_categories',
            'is_configurable' => true,
        ]);
        $existing=DB::table('configuration_settings')->where('key','AUTO_BUDGET_SECTION')->first();
        DB::table('configuration_settings')->where('key','AUTO_BUDGET_SECTION')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'AUTO_BUDGET_SECTION',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Financial Year Initialization',
            'name' => 'AUTO_BUDGET_SECTION',
            'description' => 'Auto Budget Section',
            'sort_order' => 2,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from sections',
            'is_configurable' => true,
        ]);
        $existing=DB::table('configuration_settings')->where('key','REVENUE_GFS_ACCOUNT_TYPE')->first();
        DB::table('configuration_settings')->where('key','REVENUE_GFS_ACCOUNT_TYPE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'REVENUE_GFS_ACCOUNT_TYPE',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Setup',
            'name' => 'Revenue GFS Account Type',
            'description' => 'Default Fund Source Revenue GFS account Types',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from account_types',
            'is_configurable' => true,
        ]);
        $existing=DB::table('configuration_settings')->where('key','REVENUE_GFS_SUB_CATEGORY')->first();
        DB::table('configuration_settings')->where('key','REVENUE_GFS_SUB_CATEGORY')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'REVENUE_GFS_SUB_CATEGORY',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Setup',
            'name' => 'Revenue GFS Sub Category',
            'description' => 'Default Fund Source Revenue GFS Sub Category',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from gfs_code_sub_categories',
            'is_configurable' => true,
        ]);
        $existing=DB::table('configuration_settings')->where('key','SERVICE_OUTPUT_CODE')->first();
        DB::table('configuration_settings')->where('key','SERVICE_OUTPUT_CODE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'SERVICE_OUTPUT_CODE',
            'value' =>  isset($existing)?$existing->value:null,
            'group_name'=>'Budgeting',
            'name' => 'Auto Service Output Code',
            'description' => 'Auto Service Output Code',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from plan_chain_types',
            'is_configurable' => true,
        ]);
        $existing=DB::table('configuration_settings')->where('key','OBJECTIVE_CODE')->first();
        DB::table('configuration_settings')->where('key','OBJECTIVE_CODE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'OBJECTIVE_CODE',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Budgeting',
            'name' => 'Auto Budget Objective Code',
            'description' => 'Objective Code for Auto Budget',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from plan_chain_types',
            'is_configurable' => true,
        ]);
        $existing=DB::table('configuration_settings')->where('key','COUNCIL_FACILITY_TYPE')->first();
        DB::table('configuration_settings')->where('key','COUNCIL_FACILITY_TYPE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'COUNCIL_FACILITY_TYPE',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Budgeting',
            'name' => 'COUNCIL_FACILITY_TYPE',
            'description' => 'Service Provider Type ID for PISCs',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from facility_types',
            'is_configurable' => true,
        ]);

        $existing=DB::table('configuration_settings')->where('key','CEILING_START_ADMIN_HIERARCHY_LEVEL')->first();
        DB::table('configuration_settings')->where('key','CEILING_START_ADMIN_HIERARCHY_LEVEL')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'CEILING_START_ADMIN_HIERARCHY_LEVEL',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Financial Year Initialization',
            'name' => 'Admin Hierarchy Level Which Starts Ceilings',
            'description' => 'Ceiling Start Level',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from admin_hierarchy_levels where is_active=true',
            'is_configurable' => true,
        ]);
        $existing=DB::table('configuration_settings')->where('key','MINIMUM_YEARS_FOR_A_REFERENCE_DOCUMENT')->first();
        DB::table('configuration_settings')->where('key','MINIMUM_YEARS_FOR_A_REFERENCE_DOCUMENT')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'MINIMUM_YEARS_FOR_A_REFERENCE_DOCUMENT',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Strategic Plan',
            'name' => 'Minimum number of years for a Reference Document',
            'description' => 'Number of Years for Strategic Plan Long Term Targets',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','STRATEGIC_LONG_TERM_REFERENCE_DOCUMENT_TYPE')->first();
        DB::table('configuration_settings')->where('key','STRATEGIC_LONG_TERM_REFERENCE_DOCUMENT_TYPE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'STRATEGIC_LONG_TERM_REFERENCE_DOCUMENT_TYPE',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Strategic Plan',
            'name' => 'Long Term Strategic Plan Reference Document Type',
            'description' => 'Strategic Plan Long Term Target Document Type',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from reference_document_types',
            'is_configurable' => true,
        ]);
        $existing= DB::table('configuration_settings')->where('key','PLANNING_PERIODS_GROUPS')->first();
        DB::table('configuration_settings')->where('key','PLANNING_PERIODS_GROUPS')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'PLANNING_PERIODS_GROUPS',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Financial Year Initialization',
            'name' => 'Planning Period Groups',
            'description' => 'Planning Periods',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from period_groups',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','HQ_FACILITY_TYPE')->first();
        DB::table('configuration_settings')->where('key','HQ_FACILITY_TYPE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'HQ_FACILITY_TYPE',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Planning',
            'name' => 'Service Provider Type for HeadQuarters',
            'description' => 'Planning HQ Service Provider Type',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from facility_types',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','EPICOR_BUDGET_EXPORT_QUEUE_NAME')->first();
        DB::table('configuration_settings')->where('key','EPICOR_BUDGET_EXPORT_QUEUE_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'EPICOR_BUDGET_EXPORT_QUEUE_NAME',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Execution',
            'name' => 'The name of the queue to be used for budget export',
            'description' => 'MUSE Budget export queue name',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','LGRCIS_BUDGET_EXPORT_QUEUE_NAME')->first();
        DB::table('configuration_settings')->where('key','LGRCIS_BUDGET_EXPORT_QUEUE_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'LGRCIS_BUDGET_EXPORT_QUEUE_NAME',
            'value' => isset($existing)?$existing->value:'MUSE',
            'group_name'=>'Execution',
            'name' => 'The name of the queue to be used for budget export',
            'description' => 'LGRCIS Budget Export Queue Name',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','FFARS_BUDGET_EXPORT_QUEUE_NAME')->first();
        DB::table('configuration_settings')->where('key','FFARS_BUDGET_EXPORT_QUEUE_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'FFARS_BUDGET_EXPORT_QUEUE_NAME',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Execution',
            'name' => 'FFARS budget export queue name',
            'description' => 'FFARS Budget export queue name',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','BUDGET_APPROVAL_TRANSACTION_TYPE')->first();
        DB::table('configuration_settings')->where('key','BUDGET_APPROVAL_TRANSACTION_TYPE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'BUDGET_APPROVAL_TRANSACTION_TYPE',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Execution',
            'name' => 'Transaction Type for Budget Approval',
            'description' => 'Budget Approval Transaction Type',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_transaction_types',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','BUDGET_DISAPPROVAL_TRANSACTION_TYPE')->first();
        DB::table('configuration_settings')->where('key','BUDGET_DISAPPROVAL_TRANSACTION_TYPE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'BUDGET_DISAPPROVAL_TRANSACTION_TYPE',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Execution',
            'name' => 'Transaction Type for Budget Disapprove',
            'description' => 'Budget DisApproval Transaction Type',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_transaction_types',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','CHANGE_OF_ACTIVITY_CODE_TRANSACTION_TYPE')->first();
        DB::table('configuration_settings')->where('key','CHANGE_OF_ACTIVITY_CODE_TRANSACTION_TYPE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'CHANGE_OF_ACTIVITY_CODE_TRANSACTION_TYPE',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Execution',
            'name' => 'Transaction Type for Change Activity Code',
            'description' => 'Transaction Type for Change Activity Code',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_transaction_types',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','CHANGE_OF_ACTIVITY_BUDGET_CLASS_TRANSACTION_TYPE')->first();
        DB::table('configuration_settings')->where('key','CHANGE_OF_ACTIVITY_BUDGET_CLASS_TRANSACTION_TYPE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'CHANGE_OF_ACTIVITY_BUDGET_CLASS_TRANSACTION_TYPE',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Execution',
            'name' => 'Transaction Type for Change Activity Sub Budget Class',
            'description' => 'Transaction Type for Change Activity Sub Budget Class',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_transaction_types',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','PLANREP_REALLOCATION_DATA_SOURCE')->first();
        DB::table('configuration_settings')->where('key','REALLOCATION_DATA_SOURCE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'PLANREP_REALLOCATION_DATA_SOURCE',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Execution',
            'name' => 'Data Source for Budget Reallocation',
            'description' => 'Budget Reallocation Data Source Through PlanRep',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from data_sources',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','PLANREP_REALLOCATION_IMPORT_METHOD')->first();
        DB::table('configuration_settings')->where('key','PLANREP_REALLOCATION_IMPORT_METHOD')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'PLANREP_REALLOCATION_IMPORT_METHOD',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Execution',
            'name' => 'Import method for budget reallocation',
            'description' => 'Budget Reallocation Import method Through PlanRep',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from import_methods',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','REALLOCATION_TRANSACTION_TYPE')->first();
        DB::table('configuration_settings')->where('key','REALLOCATION_TRANSACTION_TYPE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'REALLOCATION_TRANSACTION_TYPE',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Execution',
            'name' => 'The transaction Type for Budget Reallocation',
            'description' => 'Budget Reallocation Transaction Type',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_transaction_types',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','APPROVED_BUDGET_GROUP_NAME')->first();
        DB::table('configuration_settings')->where('key','APPROVED_BUDGET_GROUP_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'APPROVED_BUDGET_GROUP_NAME',
            'value' => isset($existing)?$existing->value:'APPR',
            'group_name'=>'Execution',
            'name' => 'The name of the group for Approved Budget',
            'description' => ' Approved Budget Group name',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','REALLOCATION_BUDGET_GROUP_NAME')->first();
        DB::table('configuration_settings')->where('key','REALLOCATION_BUDGET_GROUP_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'REALLOCATION_BUDGET_GROUP_NAME',
            'value' => isset($existing)?$existing->value:'REALLOCATION',
            'group_name'=>'Execution',
            'name' => 'The name of the group for budget reallocation',
            'description' => ' Reallocation Budget Group name',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','CARRIED_FORWARD_BUDGET_GROUP_NAME')->first();
        DB::table('configuration_settings')->where('key','CARRIED_FORWARD_BUDGET_GROUP_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'CARRIED_FORWARD_BUDGET_GROUP_NAME',
            'value' => isset($existing)?$existing->value:'CFB',
            'group_name'=>'Execution',
            'name' => 'The name of the group for carry over budget',
            'description' => ' Carried Forward Budget Group name',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','SUPPLEMENTARY_BUDGET_GROUP_NAME')->first();
        DB::table('configuration_settings')->where('key','SUPPLEMENTARY_BUDGET_GROUP_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'SUPPLEMENTARY_BUDGET_GROUP_NAME',
            'value' => isset($existing)?$existing->value:'ADDB',
            'group_name'=>'Execution',
            'name' => 'The name of the group for supplementary budgets',
            'description' => 'Supplementary Budget Group name',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','GFS_ACCOUNT_TYPE_FOR_EXPENSES')->first();
        DB::table('configuration_settings')->where('key','GFS_ACCOUNT_TYPE_FOR_EXPENSES')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'GFS_ACCOUNT_TYPE_FOR_EXPENSES',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Execution',
            'name' => 'This is the GFS account type for expenditure',
            'description' => 'This is the GFS Account Type for Expenditure. It is used to distinguish expenditure and revenue GFS',
            'sort_order' => 1,
            'value_type' => 'MULT_SELECT',
            'value_options' => null,
            'value_options_query' => 'Select name as name, id as value from account_types',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','GFS_ACCOUNT_TYPE_FOR_REVENUE')->first();
        DB::table('configuration_settings')->where('key','GFS_ACCOUNT_TYPE_FOR_REVENUE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'GFS_ACCOUNT_TYPE_FOR_REVENUE',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Execution',
            'name' => 'This is the GFS Account Type for Revenue',
            'description' => 'This is the GFS Account Type for Revenue. It is used to distingush Expenditure and Revenue GFS codes',
            'sort_order' => 1,
            'value_type' => 'MULT_SELECT',
            'value_options' => null,
            'value_options_query' => 'Select name as name, id as value from account_types',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','BUDGET_CONTROL_CODE')->first();
        DB::table('configuration_settings')->where('key','BUDGET_CONTROL_CODE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'BUDGET_CONTROL_CODE',
            'value' => isset($existing)?$existing->value:'XXX-XXXX-0000-000-00000000-0000-00000000-0-000-99999993',
            'group_name'=>'Financial System Parameters',
            'name' => 'Budget Control Code',
            'description' => 'This is the Control Account which is used for Budget. It is used for Data Interchange with Epicor',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','EXPENDITURE_CONTROL_CODE')->first();
        DB::table('configuration_settings')->where('key','EXPENDITURE_CONTROL_CODE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'EXPENDITURE_CONTROL_CODE',
            'value' => isset($existing)?$existing->value:'XXX-XXXX-0000-000-00000000-0000-00000000-0-000-33082101',
            'group_name'=>'Financial System Parameters',
            'name' => 'Expenditure Control Account',
            'description' => 'This is the Control Account to be used in for Data Interchange with Epicor',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','CASH_ACCOUNT_CONTROL_CODE')->first();
        DB::table('configuration_settings')->where('key','CASH_ACCOUNT_CONTROL_CODE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'CASH_ACCOUNT_CONTROL_CODE',
            'value' => isset($existing)?$existing->value:'XXX-XXXX-0000-000-00000000-0000-00000000-0-000-62020104',
            'group_name'=>'Financial System Parameters',
            'name' => 'The Control Code for Cash Accounts',
            'description' => 'It is the code used for cash accounts for Data Interchange with MUSE',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','BUDGET_BOOK_ID')->first();
        DB::table('configuration_settings')->where('key','BUDGET_BOOK_ID')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'BUDGET_BOOK_ID',
            'value' => isset($existing)?$existing->value:'BUDGETP',
            'group_name'=>'Financial System Parameters',
            'name' => 'The name of the book to be used for Budget in MUSE',
            'description' => 'Budget Book ID',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','MAIN_BOOK_ID')->first();
        DB::table('configuration_settings')->where('key','MAIN_BOOK_ID')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'MAIN_BOOK_ID',
            'value' => isset($existing)?$existing->value:'MAINP',
            'group_name'=>'Financial System Parameters',
            'name' => 'Main Book ID to be used for MUSE',
            'description' => 'Main Book ID',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','REVENUE_BOOK_ID')->first();
        DB::table('configuration_settings')->where('key','REVENUE_BOOK_ID')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'REVENUE_BOOK_ID',
            'value' => isset($existing)?$existing->value:'REVENUEP',
            'group_name'=>'Financial System Parameters',
            'name' => 'REVENUE_BOOK_ID',
            'description' => 'Revenue Book ID',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);


        $existing= DB::table('configuration_settings')->where('key','COA_SEGMENT_FEEDBACK_QUEUE')->first();
        DB::table('configuration_settings')->where('key','COA_SEGMENT_FEEDBACK_QUEUE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'COA_SEGMENT_FEEDBACK_QUEUE',
            'value' => isset($existing)?$existing->value:'COASegments_Queue_FeedBack',
            'group_name'=>'Financial System Parameters',
            'name' => 'Name of Chart Of Account Feedback Queue',
            'description' => 'The name of the Chart Of Account Segment Feedback Queue',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','BUDGET_CLASS_TO_EXCLUDE_IN_PLANNING')->first();
        DB::table('configuration_settings')->where('key','BUDGET_CLASS_TO_EXCLUDE_IN_PLANNING')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'BUDGET_CLASS_TO_EXCLUDE_IN_PLANNING',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Planning',
            'name' => 'Budget class to exclude in Planning',
            'description' => 'List of Budget Classes not included in Planning',
            'sort_order' => 1,
            'value_type' => 'MULT_SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_classes',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','COA_SEGMENT_QUEUE_NAME')->first();
        DB::table('configuration_settings')->where('key','COA_SEGMENT_QUEUE_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'COA_SEGMENT_QUEUE_NAME',
            'value' => isset($existing)?$existing->value:'COASegments_Queue',
            'group_name'=>'Financial System Parameters',
            'name' => 'Name of Chart Of Account Queue',
            'description' => 'The name of the Chart Of Account Segment Queue',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','BUDGET_QUEUE_NAME')->first();
        DB::table('configuration_settings')->where('key','BUDGET_QUEUE_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'BUDGET_QUEUE_NAME',
            'value' => isset($existing)?$existing->value:'BUDGET_Queue',
            'group_name'=>'Financial System Parameters',
            'name' => 'Name of the Budget Queue',
            'description' => 'The name of the Budget Queue',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','GL_ACCOUNT_NAME')->first();
        DB::table('configuration_settings')->where('key','GL_ACCOUNT_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'GL_ACCOUNT_NAME',
            'value' => isset($existing)?$existing->value:'GLAccounts_Queue',
            'group_name'=>'Financial System Parameters',
            'name' => 'Name of the GL Account Queue',
            'description' => 'The name of the GL Account Queue',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','OWN_SOURCE_REVENUE_DEFAULT_COA_COST_CENTRE')->first();
        DB::table('configuration_settings')->where('key','OWN_SOURCE_REVENUE_DEFAULT_COA_COST_CENTRE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'OWN_SOURCE_REVENUE_DEFAULT_COA_COST_CENTRE',
            'value' => isset($existing)?$existing->value:'500A',
            'group_name'=>'Execution',
            'name' => 'Own Source Default Cost Centre',
            'description' => 'Own Source default Cost Centre',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);
        /*FFARS*/
        $existing= DB::table('configuration_settings')->where('key','COA_SEGMENT_FFARS_QUEUE_NAME')->first();
        DB::table('configuration_settings')->where('key','COA_SEGMENT_FFARS_QUEUE_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'COA_SEGMENT_FFARS_QUEUE_NAME',
            'value' => isset($existing)?$existing->value:'SEGMENT_TO_FFARS_QUEUE',
            'group_name'=>'Financial System Parameters',
            'name' => 'Send Segments to FFARS Queue Name',
            'description' => 'Send Segments to FFARS Queue Name',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','BUDGET_TO_FFARS_QUEUE_NAME')->first();
        DB::table('configuration_settings')->where('key','BUDGET_TO_FFARS_QUEUE_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'BUDGET_TO_FFARS_QUEUE_NAME',
            'value' => isset($existing)?$existing->value:'BUDGET_TO_FFARS_QUEUE',
            'group_name'=>'Financial System Parameters',
            'name' => 'Send Budget to FFARS Queue Name',
            'description' => 'Send Budget to FFARS Queue Name',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','ACTUAL_FROM_FFARS_QUEUE_FEEDBACK')->first();
        DB::table('configuration_settings')->where('key','ACTUAL_FROM_FFARS_QUEUE_FEEDBACK')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'ACTUAL_FROM_FFARS_QUEUE_FEEDBACK',
            'value' => isset($existing)?$existing->value:'ACTUAL_FROM_FFARS_QUEUE_FEEDBACK',
            'group_name'=>'Financial System Parameters',
            'name' => 'Send Budget to FFARS Queue Feedback',
            'description' => 'Send Budget to FFARS Queue Feedback',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','PE_CEILINGS')->first();
        DB::table('configuration_settings')->where('key','PE_CEILINGS')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'PE_CEILINGS',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Dashboard',
            'name' => 'PE Ceilings Budget Classes',
            'description' => 'PE Ceilings Budget Classes',
            'sort_order' => 1,
            'value_type' => 'MULT_SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_classes',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','PE_FUND_SOURCES')->first();
        DB::table('configuration_settings')->where('key','PE_FUND_SOURCES')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'PE_FUND_SOURCES',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Dashboard',
            'name' => 'PE Fund Sources',
            'description' => 'PE Fund Sources',
            'sort_order' => 1,
            'value_type' => 'MULT_SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from fund_sources',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','BUDGET_CLASS_TO_MOVE')->first();
        DB::table('configuration_settings')->where('key','BUDGET_CLASS_TO_MOVE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'BUDGET_CLASS_TO_MOVE',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Data',
            'name' => 'Budget to move Budget Classes',
            'description' => 'Budget to move Budget Classes',
            'sort_order' => 1,
            'value_type' => 'MULT_SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_classes',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','FUND_SOURCE_TO_MOVE')->first();
        DB::table('configuration_settings')->where('key','FUND_SOURCE_TO_MOVE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'FUND_SOURCE_TO_MOVE',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Data',
            'name' => 'Budget to move Fund Sources',
            'description' => 'Budget to move Fund Sources',
            'sort_order' => 1,
            'value_type' => 'MULT_SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from fund_sources',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','COST_CENTRE_NOT_BUDGETING')->first();
        DB::table('configuration_settings')->where('key','COST_CENTRE_NOT_BUDGETING')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'COST_CENTRE_NOT_BUDGETING',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Budgeting',
            'name' => 'Cost Centre not budgeting',
            'description' => 'Cost Centre not budgeting',
            'sort_order' => 1,
            'value_type' => 'MULT_SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from sections where id not  in (Select parent_id from sections where parent_id is not null)',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','HS_FUND_SOURCES')->first();
        DB::table('configuration_settings')->where('key','HS_FUND_SOURCES')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'HS_FUND_SOURCES',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Mass Reallocation',
            'name' => 'HEALTH SECTOR Fund Sources',
            'description' => 'HEALTH SECTOR Fund Sources',
            'sort_order' => 2,
            'value_type' => 'MULT_SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from fund_sources order by name',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','WS_FUND_SOURCES')->first();
        DB::table('configuration_settings')->where('key','WS_FUND_SOURCES')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'WS_FUND_SOURCES',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Mass Reallocation',
            'name' => 'WATER SECTOR Fund Sources',
            'description' => 'WATER SECTOR Fund Sources',
            'sort_order' => 4,
            'value_type' => 'MULT_SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from fund_sources order by name',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','ES_FUND_SOURCES')->first();
        DB::table('configuration_settings')->where('key','ES_FUND_SOURCES')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'ES_FUND_SOURCES',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Mass Reallocation',
            'name' => 'EDUCATION SECTOR Fund Sources',
            'description' => 'EDUCATION SECTOR Fund Sources',
            'sort_order' => 4,
            'value_type' => 'MULT_SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from fund_sources order by name',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','AS_FUND_SOURCES')->first();
        DB::table('configuration_settings')->where('key','AS_FUND_SOURCES')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'AS_FUND_SOURCES',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Mass Reallocation',
            'name' => 'ADMINISTRATION SECTOR Fund Sources',
            'description' => 'ADMINISTRATION SECTOR Fund Sources',
            'sort_order' => 4,
            'value_type' => 'MULT_SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from fund_sources order by name',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','BUDGET_CLASSES_WITH_PROJECTS')->first();
        DB::table('configuration_settings')->where('key','BUDGET_CLASSES_WITH_PROJECTS')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'BUDGET_CLASSES_WITH_PROJECTS',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Planning',
            'name' => 'BUDGET CLASSESS WITH PROJECTS',
            'description' => 'BUDGET CLASSESS WITH PROJECTS',
            'sort_order' => 4,
            'value_type' => 'MULT_SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_classes where parent_id is not null order by name',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','HS_FROM_BUDGET_CLASS')->first();
        DB::table('configuration_settings')->where('key','HS_FROM_BUDGET_CLASS')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'HS_FROM_BUDGET_CLASS',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Mass Reallocation',
            'name' => 'HEALTH SECTOR FROM Budget Class',
            'description' => 'HEALTH SECTOR FROM Budget Class',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_classes order by name',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','HS_TO_BUDGET_CLASS')->first();
        DB::table('configuration_settings')->where('key','HS_BUDGET_CLASS')->delete();
        DB::table('configuration_settings')->where('key','HS_TO_BUDGET_CLASS')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'HS_TO_BUDGET_CLASS',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Mass Reallocation',
            'name' => 'HEALTH SECTOR TO Budget Class',
            'description' => 'HEALTH SECTOR TO Budget Class',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_classes order by name',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','WS_FROM_BUDGET_CLASS')->first();
        DB::table('configuration_settings')->where('key','WS_FROM_BUDGET_CLASS')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'WS_FROM_BUDGET_CLASS',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Mass Reallocation',
            'name' => 'WATER SECTOR FROM Budget Class',
            'description' => 'WATER SECTOR FROM Budget Class',
            'sort_order' => 3,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_classes order by name',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','WS_TO_BUDGET_CLASS')->first();
        DB::table('configuration_settings')->where('key','WS_BUDGET_CLASS')->delete();
        DB::table('configuration_settings')->where('key','WS_TO_BUDGET_CLASS')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'WS_TO_BUDGET_CLASS',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Mass Reallocation',
            'name' => 'WATER SECTOR TO Budget Class',
            'description' => 'WATER SECTOR TO Budget Class',
            'sort_order' => 3,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_classes order by name',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','ES_FROM_BUDGET_CLASS')->first();
        DB::table('configuration_settings')->where('key','ES_FROM_BUDGET_CLASS')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'ES_FROM_BUDGET_CLASS',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Mass Reallocation',
            'name' => 'EDUCATION SECTOR FROM Budget Class',
            'description' => 'EDUCATION SECTOR FROM Budget Class',
            'sort_order' => 3,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_classes order by name',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','ES_TO_BUDGET_CLASS')->first();
        DB::table('configuration_settings')->where('key','ES_TO_BUDGET_CLASS')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'ES_TO_BUDGET_CLASS',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Mass Reallocation',
            'name' => 'EDUCATION SECTOR TO Budget Class',
            'description' => 'EDUCATION SECTOR TO Budget Class',
            'sort_order' => 3,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_classes order by name',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','AS_FROM_BUDGET_CLASS')->first();
        DB::table('configuration_settings')->where('key','AS_FROM_BUDGET_CLASS')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'AS_FROM_BUDGET_CLASS',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Mass Reallocation',
            'name' => 'ADMINISTRATION SECTOR FROM Budget Class',
            'description' => 'ADMINISTRATION SECTOR FROM Budget Class',
            'sort_order' => 3,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_classes order by name',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','AS_TO_BUDGET_CLASS')->first();
        DB::table('configuration_settings')->where('key','AS_TO_BUDGET_CLASS')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'AS_TO_BUDGET_CLASS',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Mass Reallocation',
            'name' => 'ADMINISTRATION SECTOR TO Budget Class',
            'description' => 'ADMINISTRATION SECTOR TO Budget Class',
            'sort_order' => 3,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from budget_classes order by name',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','FFARS_ACTUAL_EXPENDITURE_QUEUE_NAME')->first();
        DB::table('configuration_settings')->where('key','FFARS_ACTUAL_EXPENDITURE_QUEUE_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'FFARS_ACTUAL_EXPENDITURE_QUEUE_NAME',
            'value' => isset($existing)?$existing->value:'EXPENDITURES_FROM_FFARS_QUEUE',
            'group_name'=>'Financial System Parameters',
            'name' => 'Name of the transaction queue to read expenditure from FFARS',
            'description' => 'Name of transaction queue for FFARS expenditure',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','FFARS_ACTUAL_EXPENDITURE_FEEDBACK_QUEUE_NAME')->first();
        DB::table('configuration_settings')->where('key','FFARS_ACTUAL_EXPENDITURE_FEEDBACK_QUEUE_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'FFARS_ACTUAL_EXPENDITURE_FEEDBACK_QUEUE_NAME',
            'value' => isset($existing)?$existing->value:'EXPENDITURE_FROM_FFARS_QUEUE_FEEDBACK',
            'group_name'=>'Financial System Parameters',
            'name' => 'Name of feedback queue to send feedback to FFARS after expenditure has been consumed',
            'description' => 'Queue to be used by PlanRep to send feedback to FFARS after actual expenditure has been read',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','FFARS_ACTUAL_REVENUE_QUEUE_NAME')->first();
        DB::table('configuration_settings')->where('key','FFARS_ACTUAL_REVENUE_QUEUE_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'FFARS_ACTUAL_REVENUE_QUEUE_NAME',
            'value' => isset($existing)?$existing->value:'REVENUES_FROM_FFARS_QUEUE',
            'group_name'=>'Financial System Parameters',
            'name' => 'Name of transaction queue for revenue from FFARS',
            'description' => 'Queue to be used by PlanRep to read revenue from FFARS',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','FFARS_ACTUAL_REVENUE_FEEDBACK_QUEUE_NAME')->first();
        DB::table('configuration_settings')->where('key','FFARS_ACTUAL_REVENUE_FEEDBACK_QUEUE_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'FFARS_ACTUAL_REVENUE_FEEDBACK_QUEUE_NAME',
            'value' => isset($existing)?$existing->value:'REVENUE_FROM_FFARS_QUEUE_FEEDBACK',
            'group_name'=>'Financial System Parameters',
            'name' => 'Name of feedback queue for revenue from FFARS',
            'description' => 'Queue to be used by PlanRep to read send revenue feedback to FFARS',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','EPICOR_GL_ACTUAL_SUMMARY_QUEUE_NAME')->first();
        DB::table('configuration_settings')->where('key','EPICOR_GL_ACTUAL_SUMMARY_QUEUE_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'EPICOR_GL_ACTUAL_SUMMARY_QUEUE_NAME',
            'value' => isset($existing)?$existing->value:'GLActualSummary_Queue',
            'group_name'=>'Financial System Parameters',
            'name' => 'The name of the queue to be used by PlanRep to read actuals from MUSE',
            'description' => 'The name of the GL Actual Summary Queue for actuals from MUSE',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','PUBLIC_FACILITY_OWNERSHIP')->first();
        DB::table('configuration_settings')->where('key','PUBLIC_FACILITY_OWNERSHIP')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'PUBLIC_FACILITY_OWNERSHIP',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Service Provider',
            'name' => 'Public Service Provider Ownership',
            'description' => 'Public Service Provider ownership',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from facility_ownerships order by name',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','COA_SEGMENT_FFARS_QUEUE_NAME')->first();
        DB::table('configuration_settings')->where('key','COA_SEGMENT_FFARS_QUEUE_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'BUDGET_REQUEST_QUEUE_NAME',
            'value' => isset($existing)?$existing->value:'BUDGET_REQUEST_QUEUE',
            'group_name'=>'Financial System Parameters',
            'name' => 'Budget request from FFARS Queue Name',
            'description' => 'Budget request from FFARS Queue Name',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','PLANREP_REALLOCATION_DEFAULT_DECISION_LEVEL')->first();
        DB::table('configuration_settings')->where('key','PLANREP_REALLOCATION_DEFAULT_DECISION_LEVEL')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'PLANREP_REALLOCATION_DEFAULT_DECISION_LEVEL',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Execution',
            'name' => 'Default Decision level for Budget Reallocation',
            'description' => 'Default Decision Level Through PlanRep',
            'sort_order' => 1,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value, name as name from decision_levels',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','MAX_UNDELIVERED_GL_TO_RESTRICT_EXPORT')->first();
        DB::table('configuration_settings')->where('key','MAX_UNDELIVERED_GL_TO_RESTRICT_EXPORT')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'MAX_UNDELIVERED_GL_TO_RESTRICT_EXPORT',
            'value' => isset($existing)?$existing->value:0,
            'group_name'=>'Execution',
            'name' => 'Max # of undelivered GL to restrict Export',
            'description' => 'Maximum number of un delivered GL to restrict Export',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','RE_ENFANCED_OWN_SOURCE_REVENUE')->first();
        DB::table('configuration_settings')->where('key','RE_ENFANCED_OWN_SOURCE_REVENUE')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'RE_ENFANCED_OWN_SOURCE_REVENUE',
            'value' => isset($existing)?$existing->value:'0',
            'group_name'=>'Revenue Export',
            'name' => 'Re enfanced Revenue Source ',
            'description' => 'Re enfanced Revenue Source',
            'sort_order' => 1,
            'value_type' => 'MULT_SELECT',
            'value_options' => null,
            'value_options_query' => 'Select name as name, id as value from fund_sources order by name',
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','FFARS_ALLOCATION_TO_PLANREP_QUEUE_NAME')->first();
        DB::table('configuration_settings')->where('key','FFARS_ALLOCATION_TO_PLANREP_QUEUE_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'FFARS_ALLOCATION_TO_PLANREP_QUEUE_NAME',
            'value' => isset($existing)?$existing->value:'FFARS_ALLOCATION_TO_PLANREP',
            'group_name'=>'Financial System Parameters',
            'name' => 'The name of the queue to be used by PlanRep to read fund allocations from FFARS',
            'description' => 'The name of the queue to be used by PlanRep to read fund allocations from FFARS',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);

        $existing= DB::table('configuration_settings')->where('key','FFARS_ALLOCATION_TO_PLANREP_FEEDBACK_QUEUE_NAME')->first();
        DB::table('configuration_settings')->where('key','FFARS_ALLOCATION_TO_PLANREP_FEEDBACK_QUEUE_NAME')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'FFARS_ALLOCATION_TO_PLANREP_FEEDBACK_QUEUE_NAME',
            'value' => isset($existing)?$existing->value:'FFARS_ALLOCATION_TO_PLANREP_FEEDBACK',
            'group_name'=>'Financial System Parameters',
            'name' => 'The name of the queue to be used by PlanRep to send fund allocationsn feedback to FFARS',
            'description' => 'The name of the queue to be used by PlanRep to send fund allocationsn feedback to FFARS',
            'sort_order' => 1,
            'value_type' => 'TEXT',
            'value_options' => null,
            'value_options_query' => null,
            'is_configurable' => true,
        ]);
    }
}
