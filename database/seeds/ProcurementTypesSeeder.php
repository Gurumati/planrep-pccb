<?php

use Illuminate\Database\Seeder;

class ProcurementTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/procurement_types.xlsx');
        $procurementTypeSheet = $objPHPExcel->getSheet(0);

        for ($row = 1; $row <=$procurementTypeSheet->getHighestRow(); ++$row) {
            DB::table('procurement_types')->insert([
                'id' => $procurementTypeSheet->getCell('A'.$row)->getValue(),
                'name' => $procurementTypeSheet->getCell('B'.$row)->getValue(),
                'sort_order' => $procurementTypeSheet->getCell('G'.$row)->getValue(),
                'description' => $procurementTypeSheet->getCell('C'.$row)->getValue(),
                'is_active' => true
            ]);
        }
    }
}
