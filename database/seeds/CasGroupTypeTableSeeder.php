<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CasGroupTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cas_group_types')->insert([
            'id' => 1,
            'name' => 'Gender',
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 2,
            'name' => 'Age'
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 3,
            'name' => '5+ years'
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 4,
            'name' => 'Admissions'
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 5,
            'name' => 'Deaths'
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 6,
            'name' => 'Encounters'
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 7,
            'name' => 'Mix'
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 8,
            'name' => 'Mix'
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 9,
            'name' => 'Year'
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 10,
            'name' => 'Case'
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 11,
            'name' => 'CRE'
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 12,
            'name' => 'CONC'
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 13,
            'name' => 'OPD'
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 14,
            'name' => 'Jumla'
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 15,
            'name' => 'Medicines, medical equipment, medical and diagnostic supplies management system'
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 16,
            'name' => 'Priority Area'
        ]);
        DB::table('cas_group_types')->insert([
            'id' => 17,
            'name' => 'Payment mode'
        ]);
    }
}
