<?php

use Illuminate\Database\Seeder;

class GFSCodeSubCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/gfs_code_sub_categories.xlsx');
        $gfsSubcategorySheet = $objPHPExcel->getSheet(0);

        for ($row = 1; $row <=$gfsSubcategorySheet->getHighestRow(); ++$row) {
            DB::table('gfs_code_sub_categories')->insert([
                'id' => $gfsSubcategorySheet->getCell('A'.$row)->getValue(),
                'name' => $gfsSubcategorySheet->getCell('B'.$row)->getValue(),
                'gfs_code_category_id' => $gfsSubcategorySheet->getCell('C'.$row)->getValue(),
                'is_procurement' => $gfsSubcategorySheet->getCell('J'.$row)->getValue()
            ]);
        }
    }
}
