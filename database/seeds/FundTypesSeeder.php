<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FundTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fund_types')->insert([
            'name' => 'Recurrent',
            'current_budget_code' => '1',
            'carried_over_budget_code' => '6',
            'exported_to_ffars' => false
        ]);

        DB::table('fund_types')->insert([
            'name' => 'Development',
            'current_budget_code' => '2',
            'carried_over_budget_code' => '7',
            'exported_to_ffars' => false
        ]);
    }
}
