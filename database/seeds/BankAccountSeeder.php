<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BankAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/BankAccount.xlsx');
        $bankSheet = $objPHPExcel->getSheet(0);
        for ($row = 1; $row <=$bankSheet->getHighestRow(); ++$row) {
            try {
                DB::table('bank_accounts')->insert([
                    'code' => $bankSheet->getCell('A'.$row)->getValue(),
                    'name' => $bankSheet->getCell('B'.$row)->getValue()
                ]);
            } catch (Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }
        }
    }
}
