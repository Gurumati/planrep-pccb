<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CalendarEventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('calendar_events')->insert([
            'name' => 'Planning',
            'number' => '1',
            'before_start_reminder_sms' => 'before_start_reminder_sms',
            'before_end_reminder_sms' => 'before_end_reminder_sms',
            'before_start_reminder_days' => '28',
            'before_end_reminder_days' => '10',
            'is_system_event'=>true
        ]);

        DB::table('calendar_events')->insert([
            'name' => 'Budgeting',
            'number' => '2',
            'before_start_reminder_sms' => 'before_start_reminder_sms',
            'before_end_reminder_sms' => 'before_end_reminder_sms',
            'before_start_reminder_days' => '28',
            'before_end_reminder_days' => '10',
            'is_system_event'=>true
        ]);

        DB::table('calendar_events')->insert([
            'name' => 'Scrutinization',
            'number' => '3',
            'before_start_reminder_sms' => 'before_start_reminder_sms',
            'before_end_reminder_sms' => 'before_end_reminder_sms',
            'before_start_reminder_days' => '28',
            'before_end_reminder_days' => '10',
            'is_system_event'=>true
        ]);

        DB::table('calendar_events')->insert([
            'name' => 'Execution',
            'number' => '4',
            'before_start_reminder_sms' => 'before_start_reminder_sms',
            'before_end_reminder_sms' => 'before_end_reminder_sms',
            'before_start_reminder_days' => '28',
            'before_end_reminder_days' => '10',
            'is_system_event'=>true
        ]);

        DB::table('calendar_events')->insert([
            'name' => 'Close Financial Year',
            'number' => '5',
            'before_start_reminder_sms' => 'before_start_reminder_sms',
            'before_end_reminder_sms' => 'before_end_reminder_sms',
            'before_start_reminder_days' => '28',
            'before_end_reminder_days' => '10',
            'is_system_event'=>true
        ]);
    }
}
