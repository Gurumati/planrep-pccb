<?php

use Illuminate\Database\Seeder;

class PiscLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lga_levels')->insert([
            'id' => 10,
            'name' => 'Higher PISC Level (HPL)',
            'description' => 'Higher PISC Level (HPL)'
        ]);
        DB::table('lga_levels')->insert([
            'id' => 11,
            'name' => 'Lower PISC Level (LPL)',
            'description' => 'Lower PISC Level (LPL)'
        ]);
    }
}
