<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;


class GFSCodeCategoryAndSubCategory extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objPHPExcel = PHPExcel_IOFactory::load('database/data/gfs_codes.xlsx');
        $gfsSheet = $objPHPExcel->getSheetNames();
        $counter = 0;
        foreach ($gfsSheet as $sheet) {
            $gfsDataSheet = $objPHPExcel->getSheet($counter);
            $accounttype = DB::table('account_types')->where('name', $sheet)->first();
            $accountTypeId = $accounttype != null ? $accounttype->id : null;
            $ExistingGfsCodeCategory = '';
            $ExistingGfsCodesubCategory = '';
            $gfsCategoryId = DB::table('gfs_code_categories')->max('id');
            $gfsSubCategoryId = DB::table('gfs_code_sub_categories')->max('id');
            $gfsId = DB::table('gfs_codes')->max('id');
            $tempParentId = null;

            // $gfsCategoryList = json_decode(json_encode(DB::table('gfs_code_categories')->pluck('id', 'name')), true);

            for ($row = 1; $row <= $gfsDataSheet->getHighestRow(); ++$row) {
                $gfsCodeCategoryName = strtolower(trim($gfsDataSheet->getCell('B' . $row)->getValue()));
                $gfsCodeSubCategoryName = strtolower(trim($gfsDataSheet->getCell('D' . $row)->getValue()));

                // $filtered = array_filter($gfsCategoryList, function ($key) use ($gfsCodeCategoryName) {
                //     return in_array($key, [$gfsCodeCategoryName]);
                // }, ARRAY_FILTER_USE_KEY);

                if ($gfsCodeCategoryName != null) {
                    if ($ExistingGfsCodeCategory != $gfsCodeCategoryName) {
                        $gfsSubCategoryId++;
                        $gfsCategoryId++;
                        $gfsId++;
                        // if (!$filtered) {
                            DB::table('gfs_code_categories')->insert([
                                'id' => $gfsCategoryId,
                                'name' => $gfsCodeCategoryName,
                                'description' => $gfsCodeCategoryName
                            ]);
                        // } else {
                        //     $tempParentId = reset($filtered);
                        //     echo 'temp-'. $tempParentId ."\r\n";
                        // }

                        DB::table('gfs_code_sub_categories')->insert([
                            'id' => $gfsSubCategoryId,
                            'name' => $gfsCodeSubCategoryName,
                            'is_procurement' => false,
                            'gfs_code_category_id' => $gfsCategoryId
                        ]);

                        DB::table('gfs_codes')->insert([
                            'id' => $gfsId,
                            'code' => $gfsDataSheet->getCell('E' . $row)->getValue(),
                            'description' => $gfsDataSheet->getCell('F' . $row)->getValue(),
                            'name' => $gfsDataSheet->getCell('F' . $row)->getValue(),
                            'is_procurement' => false,
                            'is_active' => true,
                            'account_type_id' => $accountTypeId,
                            'created_at' => Carbon::now(),
                            'gfs_code_sub_category_id' => $gfsSubCategoryId
                        ]);;
                    } else {
                        if ($ExistingGfsCodesubCategory != $gfsCodeSubCategoryName) {
                            $gfsSubCategoryId++;
                            $gfsId++;
                            DB::table('gfs_code_sub_categories')->insert([
                                'id' => $gfsSubCategoryId,
                                'name' => $gfsCodeSubCategoryName,
                                'is_procurement' => false,
                                'gfs_code_category_id' => $gfsCategoryId
                            ]);

                            DB::table('gfs_codes')->insert([
                                'id' => $gfsId,
                                'code' => $gfsDataSheet->getCell('E' . $row)->getValue(),
                                'description' => $gfsDataSheet->getCell('F' . $row)->getValue(),
                                'name' => $gfsDataSheet->getCell('F' . $row)->getValue(),
                                'is_active' => true,
                                'is_procurement' => false,
                                'account_type_id' => $accountTypeId,
                                'created_at' => Carbon::now(),
                                'gfs_code_sub_category_id' => $gfsSubCategoryId
                            ]);;
                        } else {
                            $gfsId++;
                            DB::table('gfs_codes')->insert([
                                'id' => $gfsId,
                                'code' => $gfsDataSheet->getCell('E' . $row)->getValue(),
                                'description' => $gfsDataSheet->getCell('F' . $row)->getValue(),
                                'name' => $gfsDataSheet->getCell('F' . $row)->getValue(),
                                'is_procurement' => false,
                                'is_active' => true,
                                'account_type_id' => $accountTypeId,
                                'created_at' => Carbon::now(),
                                'gfs_code_sub_category_id' => $gfsSubCategoryId
                            ]);;
                        }
                    }
                }
                $ExistingGfsCodeCategory = strtolower(trim($gfsDataSheet->getCell('B' . $row)->getValue()));
                $ExistingGfsCodesubCategory = strtolower(trim($gfsDataSheet->getCell('D' . $row)->getValue()));
            }
            $counter++;
        }
    }
}
