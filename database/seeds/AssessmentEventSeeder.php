<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AssessmentEventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configuration_settings')->where('key','ASSESSMENT_EVENT')->delete();
        DB::table('configuration_settings')->insert([
            'key' => 'ASSESSMENT_EVENT',
            'value' => isset($existing)?$existing->value:null,
            'group_name'=>'Setup',
            'name' => 'Assessment Event',
            'description' => 'Assessment Event',
            'sort_order' => 3,
            'value_type' => 'SELECT',
            'value_options' => null,
            'value_options_query' => 'Select id as value , name as name from calendar_events',
            'is_configurable' => true,
        ]);
    }
}
