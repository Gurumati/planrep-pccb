<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FundsourceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fund_sources')->insert([
            'name' => ' African Development Bank (AfDB)',
            'code' => '0AB',
        ]);

        DB::table('fund_sources')->insert([
            'name' => ' Austria',
            'code' => '0AS',
        ]);

        DB::table('fund_sources')->insert([
            'name' => ' Arab Bank for Economic Development in Africa (BADEA)',
            'code' => '0BA',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Belgium Development Agency (BTC)',
            'code' => '0BC',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Belgium',
            'code' => '0BE',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Basket Fund',
            'code' => '0BF',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Canada',
            'code' => '0CA',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'China',
            'code' => '0CN',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'CUAMM Trustee',
            'code' => '0CT',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Development Bank of Southern Africa (DBSA)',
            'code' => '0DA',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Department for International Development (DFID)',
            'code' => '0DF',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Danish International Development Agency (DANIDA)',
            'code' => '0DI',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Denmark',
            'code' => '0DN',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Global Environmental Facility',
            'code' => '0EF',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'European Investment Bank (EIB)',
            'code' => '0EI',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'European Union (EU)',
            'code' => '0EU',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Abu Dhabi Fund for Development (ADFD)',
            'code' => '0FD',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Finland',
            'code' => '0FN',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Food and Agriculture Organization (FAO)',
            'code' => '0FO',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'French Development Agency (FAD)',
            'code' => '0FR',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Green Climate Fund',
            'code' => '0GC',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Germany',
            'code' => '0GE',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Global Fund to Fight AIDS, Tuberculosis and Malaria (GFATM)',
            'code' => '0GF',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Geothermal Risk Mitigation Facility (GRMF)',
            'code' => '0GR',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Government of Tanzania',
            'code' => '0GT',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Germany Agency for International Cooperation (GIZ)',
            'code' => '0GZ',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'World Health Organization (WHO)',
            'code' => '0HO',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'International Development Association (IDA)',
            'code' => '0IA',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'International Fund for Agricultural Development (IFAD)',
            'code' => '0IF',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'International Labor Organisation (ILO)',
            'code' => '0IL',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'India',
            'code' => '0IN',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Italian Agency for Cooperation and Development (AICS)',
            'code' => '0IS',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Italia',
            'code' => '0IT',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'IMMA Health World',
            'code' => '0IW',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Japan International Development Cooperation Agency (JICA)',
            'code' => '0JA',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Japan',
            'code' => '0JP',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Korea International Development Cooperation Agency (KOICA)',
            'code' => '0KA',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Kuwait Fund For Arab Economic Development (KFAED)',
            'code' => '0KF',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'South Korea',
            'code' => '0KR',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'KFW Development Bank (KFW)',
            'code' => '0KW',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Norwegian Agency for Development (NORAD)',
            'code' => '0ND',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Norway',
            'code' => '0NR',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Organization Of Petroleum Exporting Countries (OPEC)',
            'code' => '0OP',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Poland',
            'code' => '0PO',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Irish Aid',
            'code' => '0RI',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Swedish International Development Cooperation Agency (SIDA)',
            'code' => '0SA',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Switzerland - Swiss Agency for Development and Cooperation',
            'code' => '0SD',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Spain',
            'code' => '0SP',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Sweden',
            'code' => '0SW',
        ]);

        DB::table('fund_sources')->insert([
            'name' => "United Nations International Children's Emergency Fund (UNICEF)",
            'code' => '0UC',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'United Nations Educational, Scientific and Cultural Organisation(UNESCO)',
            'code' => '0UE',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'United Nations Development Programme (UNDP)',
            'code' => '0UN',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'United Nations Population Fund (UNFPA)',
            'code' => '0UP',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'United States Agency for International Development (USAID)',
            'code' => '0US',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Word Bank',
            'code' => '0WB',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Loan Fund',
            'code' => '60A',
        ]);

        DB::table('fund_sources')->insert([
            'name' => 'Own source',
            'code' => '10A',
        ]);
    }
}
