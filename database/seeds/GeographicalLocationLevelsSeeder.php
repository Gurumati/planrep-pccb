<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GeographicalLocationLevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('geographical_location_levels')->insert([
            'id' => 1,
            'name' => 'Country',
            'code' => 'COUNTRY',
            'geo_level_position' => 1,
            'sort_order' => 1,
            'is_active' => true,
        ]);
        DB::table('geographical_location_levels')->insert([
            'id' => 2,
            'name' => 'Region',
            'code' => 'REGION',
            'geo_level_position' => 2,
            'sort_order' => 2,
            'is_active' => true,
        ]);
        DB::table('geographical_location_levels')->insert([
            'id' => 3,
            'name' => 'Council',
            'code' => 'COUNCIL',
            'geo_level_position' => 3,
            'sort_order' => 3,
            'is_active' => true,
        ]);
        DB::table('geographical_location_levels')->insert([
            'id' => 4,
            'name' => 'Ward',
            'code' => 'WARD',
            'geo_level_position' => 4,
            'sort_order' => 4,
            'is_active' => true,
        ]);
        DB::table('geographical_location_levels')->insert([
            'id' => 5,
            'name' => 'Village/Mtaa',
            'code' => 'VAILLAGE',
            'geo_level_position' => 5,
            'sort_order' => 5,
            'is_active' => true,
        ]);
        DB::table('geographical_location_levels')->insert([
            'id' => 6,
            'name' => 'Hamlet',
            'code' => 'HAMLET',
            'geo_level_position' => 6,
            'sort_order' => 6,
            'is_active' => true,
        ]);
    }
}
