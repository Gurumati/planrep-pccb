<?php

use App\Models\Budgeting\Ceiling;
use App\Models\Budgeting\CeilingSector;
use App\Models\Setup\BudgetClass;
use App\Models\Setup\CeilingGfsCode;
use App\Models\Setup\FundSourceBudgetClass;
use App\Models\Setup\GfsCode;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FundSourceBClassGfsSectionMappingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Mapping BudgetClass GFS FundSource
        echo 'mapping Gfs Costcenter and SUB budget' . "\r\n";

        $date = Carbon::now();

        $objPHPExcel = PHPExcel_IOFactory::load('database/data/FundSourceBudgetClassGfsSection.xlsx');
        $OwnSourceSheet = $objPHPExcel->getSheet(1);
        $OtherSourceSheet = $objPHPExcel->getSheet(2);

        $GfsCodes = json_decode(json_encode(DB::table('gfs_codes')->where('is_active', true)->where('deleted_at', null)->pluck('id', 'code')), true);
        $FundSources = json_decode(json_encode(DB::table('fund_sources')->where('deleted_at', null)->pluck('id', 'code')), true);
        $BudgetClasses = json_decode(json_encode(DB::table('budget_classes')->where('is_active', true)->where('deleted_at', null)->pluck('id', 'code')), true);

        $exist = false;

        for ($row = 1; $row <= $OtherSourceSheet->getHighestRow(); ++$row) {
            if ($row > 4) {
                if ($OtherSourceSheet->getCell('A' . $row)->getValue() != null) {
                    $gfsCode = $OtherSourceSheet->getCell('A' . $row)->getValue();
                    $budgetClassCode = $OtherSourceSheet->getCell('E' . $row)->getValue();
                    $fundSourceCode = $OtherSourceSheet->getCell('C' . $row)->getValue();
                    $sectionsCode = explode(",", $OtherSourceSheet->getCell('G' . $row)->getValue());

                    $gfsCode = trim($gfsCode);
                    $budgetClassCode = trim($budgetClassCode);
                    $fundSourceCode = trim($fundSourceCode);



                    $gfsId = array_filter($GfsCodes, function ($key) use ($gfsCode) {
                        return in_array($key, [$gfsCode]);
                    }, ARRAY_FILTER_USE_KEY);
                    $fundSourceId = array_filter($FundSources, function ($key) use ($fundSourceCode) {
                        return in_array($key, [$fundSourceCode]);
                    }, ARRAY_FILTER_USE_KEY);
                    $budgetClassId = array_filter($BudgetClasses, function ($key) use ($budgetClassCode) {
                        return in_array($key, [$budgetClassCode]);
                    }, ARRAY_FILTER_USE_KEY);

                    $budget_classes_array = array();

                    if ($budgetClassId && $fundSourceId) {
                        //Insert Into fund_source_budget_classes
                        DB::table('fund_source_budget_classes')->insert([
                            'fund_source_id' => reset($fundSourceId),
                            'budget_class_id' => reset($budgetClassId),
                            'fund_type_id' => $OtherSourceSheet->getCell('H' . $row)->getValue()
                        ]);

                        array_push($budget_classes_array, reset($budgetClassId));
                    }

                    if ($gfsId && $fundSourceId) {
                        if ($OtherSourceSheet->getCell('G' . $row)->getValue() != null) {

                            //iNsert Gfs and fund_sources in gfscode_fundsources table;
                            DB::table('gfscode_fundsources')->insert([
                                'fund_source_id' => reset($fundSourceId),
                                'gfs_code_id' => reset($gfsId),
                                'created_at' => $date,
                                'updated_at' => $date,
                            ]);

                            foreach ($sectionsCode as &$code) {
                                $code = trim($code);
                                $Sections = DB::table('sections')
                                    ->select('id')
                                    ->where('code', $code)
                                    ->where('is_active', true)
                                    ->where('deleted_at', null)->get()->toArray();

                                foreach ($Sections as &$section) {
                                    // Insert into Gfs Sections
                                    DB::table('gfs_code_sections')->insert([
                                        'section_id' => $section->id,
                                        'gfs_code_id' => reset($gfsId),
                                    ]);
                                }
                            }

                            foreach ($budget_classes_array as $budgId) {
                                $count = Ceiling::where('gfs_code_id', reset($gfsId))
                                    ->where('budget_class_id', $budgId)
                                    ->count();
                                if ($count < 1) {
                                    $maxId = Ceiling::max('id') + 1;

                                    $obj = new Ceiling();
                                    $obj->id = $maxId;
                                    $obj->name = GfsCode::select('description')->where('id', reset($gfsId))->first()->description;
                                    $obj->gfs_code_id = reset($gfsId);
                                    $obj->budget_class_id = $budgId;
                                    $obj->save();

                                    $ceilingGfs = new CeilingGfsCode();
                                    $ceilingGfs->gfs_code_id = reset($gfsId);
                                    $ceilingGfs->ceiling_id = $maxId;
                                    $ceilingGfs->is_inclusive = false;
                                    $ceilingGfs->created_at = date('Y-m-d H:i:s');
                                    $ceilingGfs->save();

                                    $fundSector = DB::table('sector_fund_sources')->where('fund_source_id', reset($fundSourceId))->select('sector_id')->get()->toArray();
                                    foreach ($fundSector as $sector) {
                                        $ceilingSector = new CeilingSector();
                                        $ceilingSector->sector_id = $sector->sector_id;
                                        $ceilingSector->ceiling_id = $maxId;
                                        $ceilingSector->created_at = date('Y-m-d H:i:s');
                                        $ceilingSector->save();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        for ($row = 1; $row <= $OwnSourceSheet->getHighestRow(); ++$row) {

            if ($row > 5) {
                $gfsCode = $OwnSourceSheet->getCell('A' . $row)->getValue();
                $budgetClassRevCode = $OwnSourceSheet->getCell('E' . $row)->getValue();
                $budgetClassExpRecCode = $OwnSourceSheet->getCell('H' . $row)->getValue();
                $budgetClassExpDevCode = $OwnSourceSheet->getCell('K' . $row)->getValue();
                $budgetClassExpDevCodePE = $OwnSourceSheet->getCell('O' . $row)->getValue();
                $fundSourceCode = $OwnSourceSheet->getCell('C' . $row)->getValue();
                $sectionsCode = explode(",", $OwnSourceSheet->getCell('N' . $row)->getValue());


                $gfsCode = trim($gfsCode);
                $budgetClassRevCode = trim($budgetClassRevCode);
                $budgetClassExpRecCode = trim($budgetClassExpRecCode);
                $budgetClassExpDevCode = trim($budgetClassExpDevCode);
                $budgetClassExpPE = trim($budgetClassExpDevCodePE);
                $fundSourceCode = trim($fundSourceCode);

                $gfsId = array_filter($GfsCodes, function ($key) use ($gfsCode) {
                    return in_array($key, [$gfsCode]);
                }, ARRAY_FILTER_USE_KEY);
                $fundSourceId = array_filter($FundSources, function ($key) use ($fundSourceCode) {
                    return in_array($key, [$fundSourceCode]);
                }, ARRAY_FILTER_USE_KEY);
                $budgetClassRevId = array_filter($BudgetClasses, function ($key) use ($budgetClassRevCode) {
                    return in_array($key, [$budgetClassRevCode]);
                }, ARRAY_FILTER_USE_KEY);
                $budgetClassExpRecId = array_filter($BudgetClasses, function ($key) use ($budgetClassExpRecCode) {
                    return in_array($key, [$budgetClassExpRecCode]);
                }, ARRAY_FILTER_USE_KEY);
                $budgetClassExpDevId = array_filter($BudgetClasses, function ($key) use ($budgetClassExpDevCode) {
                    return in_array($key, [$budgetClassExpDevCode]);
                }, ARRAY_FILTER_USE_KEY);
                $budgetClassExpDevPEId = array_filter($BudgetClasses, function ($key) use ($budgetClassExpPE) {
                    return in_array($key, [$budgetClassExpPE]);
                }, ARRAY_FILTER_USE_KEY);

                if ($gfsId && $fundSourceId) {
                    if ($OwnSourceSheet->getCell('N' . $row)->getValue() != null) {
                        //iNsert Gfs and fund_sources in gfscode_fundsources table;
                        DB::table('gfscode_fundsources')->insert([
                            'fund_source_id' => reset($fundSourceId),
                            'gfs_code_id' => reset($gfsId),
                            'created_at' => $date,
                            'updated_at' => $date,
                        ]);

                        foreach ($sectionsCode as &$code) {

                            $code = trim($code);
                            $Sections = DB::table('sections')
                                ->select('id')
                                ->where('code', $code)
                                ->where('is_active', true)
                                ->where('deleted_at', null)->get()->toArray();

                            foreach ($Sections as &$section) {
                                // Insert into Gfs Sections
                                DB::table('gfs_code_sections')->insert([
                                    'section_id' => $section->id,
                                    'gfs_code_id' => reset($gfsId),
                                ]);
                            }
                        }
                    }
                }

                if (!$exist) {
                    if ($budgetClassRevId && $fundSourceId) {
                        //Insert Into fund_source_budget_classes
                        DB::table('fund_source_budget_classes')->insert([
                            'fund_source_id' => reset($fundSourceId),
                            'budget_class_id' => reset($budgetClassRevId),
                            'fund_type_id' => $OwnSourceSheet->getCell('G' . $row)->getValue()
                        ]);
                        array_push($budget_classes_array, reset($budgetClassId));
                    }

                    if ($budgetClassExpRecId && $fundSourceId) {
                        //Insert Into fund_source_budget_classes
                        DB::table('fund_source_budget_classes')->insert([
                            'fund_source_id' => reset($fundSourceId),
                            'budget_class_id' => reset($budgetClassExpRecId),
                            'fund_type_id' => $OwnSourceSheet->getCell('J' . $row)->getValue()
                        ]);
                        array_push($budget_classes_array, reset($budgetClassId));
                    }

                    if ($budgetClassExpDevPEId && $fundSourceId) {
                        // Insert Into fund_source_budget_classes
                        DB::table('fund_source_budget_classes')->insert([
                            'fund_source_id' => reset($fundSourceId),
                            'budget_class_id' => reset($budgetClassExpDevPEId),
                            'fund_type_id' => $OwnSourceSheet->getCell('Q' . $row)->getValue()
                        ]);
                        array_push($budget_classes_array, reset($budgetClassExpDevPEId));
                    }

                    if ($budgetClassExpDevId && $fundSourceId) {
                        // Insert Into fund_source_budget_classes
                        DB::table('fund_source_budget_classes')->insert([
                            'fund_source_id' => reset($fundSourceId),
                            'budget_class_id' => reset($budgetClassExpDevId),
                            'fund_type_id' => $OwnSourceSheet->getCell('M' . $row)->getValue()
                        ]);
                        array_push($budget_classes_array, reset($budgetClassId));
                    }


                    if ($gfsId && $fundSourceId) {
                        if ($OwnSourceSheet->getCell('N' . $row)->getValue() != null) {

                            $fundBgts = FundSourceBudgetClass::where('fund_source_id', reset($fundSourceId))->get();
                            foreach ($fundBgts as $fundBgt) {
                                $count = Ceiling::where('gfs_code_id', reset($gfsId))
                                    ->where('budget_class_id', $fundBgt->budget_class_id)
                                    ->count();

                                if ($count < 1) {

                                    $maxId = Ceiling::max('id') + 1;

                                    var_dump($maxId);

                                    $obj = new Ceiling();
                                    $obj->id = $maxId;
                                    $obj->name = BudgetClass::select('description')->where('id', $fundBgt->budget_class_id)->first()->description;
                                    $obj->budget_class_id = $fundBgt->budget_class_id;
                                    $obj->is_aggregate = true;
                                    $obj->is_active = true;
                                    $obj->aggregate_fund_source_id = reset($fundSourceId);
                                    $obj->save();

                                    $fundSector = DB::table('sector_fund_sources')->where('fund_source_id', reset($fundSourceId))->select('sector_id')->get()->toArray();
                                    foreach ($fundSector as $sector) {
                                        $ceilingSector = new CeilingSector();
                                        $ceilingSector->sector_id = $sector->sector_id;
                                        $ceilingSector->ceiling_id =  $maxId;
                                        $ceilingSector->created_at = date('Y-m-d H:i:s');
                                        $ceilingSector->save();
                                    }

                                    $ceilingGfs = new CeilingGfsCode();
                                    $ceilingGfs->gfs_code_id = reset($gfsId);
                                    $ceilingGfs->ceiling_id =  $maxId;
                                    $ceilingGfs->is_inclusive = false;
                                    $ceilingGfs->created_at = date('Y-m-d H:i:s');
                                    $ceilingGfs->save();
                                }
                            }
                        }
                    }

                    $exist = true;
                }
            }
        }
    }
}
