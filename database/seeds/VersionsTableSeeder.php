<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VersionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('versions')->insert([
            //PLAN_CHAIN
            'id' => 1,
            'name' => 'Version 1',
            'version_type_id' => 1,
            'date' => date("Y-m-d"),
        ]);
        DB::table('versions')->insert([
            //PERFORMANCE_INDICATOR
            'id' => 2,
            'name' => 'Version 1',
            'version_type_id' => 2,
            'date' => date("Y-m-d"),
        ]);
        DB::table('versions')->insert([
            //INTERVENTION
            'id' => 3,
            'name' => 'Version 1',
            'version_type_id' => 3,
            'date' => date("Y-m-d"),
        ]);
        DB::table('versions')->insert([
            //PRIORITY_AREA
            'id' => 4,
            'name' => 'Version 1',
            'version_type_id' => 4,
            'date' => date("Y-m-d"),
        ]);
        DB::table('versions')->insert([
            //FUND_SOURCE
            'id' => 5,
            'name' => 'Version 1',
            'version_type_id' => 5,
            'date' => date("Y-m-d"),
        ]);
        DB::table('versions')->insert([
            //BUDGET_CLASS
            'id' => 6,
            'name' => 'Version 1',
            'version_type_id' => 6,
            'date' => date("Y-m-d"),
        ]);
        DB::table('versions')->insert([
            //NATIONAL_REFERENCE_TYPES
            'id' => 7,
            'name' => 'Version 1',
            'version_type_id' => 7,
            'date' => date("Y-m-d"),
        ]);

        DB::table('versions')->insert([
            //BANK ACCOUNT
            'id' => 8,
            'name' => 'Version 1',
            'version_type_id' => 8,
            'date' => date("Y-m-d"),
        ]);

        DB::table('versions')->insert([
            //BANK ACCOUNT
            'id' => 9,
            'name' => 'Version 1',
            'version_type_id' => 9,
            'date' => date("Y-m-d"),
        ]);
    }
}
