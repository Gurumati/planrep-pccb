<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAcoountBalancesAddAdminCeilingId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_balances', function(Blueprint $table){
            $table->integer('admin_hierarchy_ceiling_id');
        });
        Schema::table('account_balances', function(Blueprint $table){
            $table->foreign('admin_hierarchy_ceiling_id')->references('id')->on('admin_hierarchy_ceilings')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
