<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReallocationAddDoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_reallocations', function(Blueprint $table){
            $table->string('document_url2')->nullable();
            $table->string('document_url3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_reallocations', function(Blueprint $table){
            $table->dropColumn('document_url2');
            $table->dropColumn('document_url3');
        });
    }
}
