<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetClassVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_class_versions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('budget_class_id')->unsigned();
            $table->integer('version_id')->unsigned();
            $table->foreign('budget_class_id')->references('id')->on('budget_classes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('version_id')->references('id')->on('versions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_class_versions');
    }
}
