<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoricalDataItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historical_data_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('row_id');
            $table->integer('column_id');
            $table->decimal('value',20,2);
            $table->integer('financial_year_id')->unsigned();
            $table->integer('admin_hierarchy_id')->unsigned();
            $table->unique(['row_id','column_id','financial_year_id','admin_hierarchy_id']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historical_data_items');
    }
}
