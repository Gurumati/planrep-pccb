<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBdcExpenditureCentreValuesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("bdc_expenditure_centre_values",function (Blueprint $blueprint){
            $blueprint->string('min_value')->nullable()->change();
            $blueprint->string('max_value')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("bdc_expenditure_centre_values",function (Blueprint $blueprint){
            $blueprint->string('min_value')->default(0)->change();
            $blueprint->string('max_value')->default(0)->change();
        });
    }
}
