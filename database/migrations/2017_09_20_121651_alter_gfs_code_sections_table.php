<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGfsCodeSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gfs_code_sections',function (Blueprint $table){
            $table->foreign('gfs_code_id')->references('id')->on('gfs_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('section_id')->references('id')->on('sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gfs_code_sections',function (Blueprint $table){
            $table->dropForeign('gfs_code_sections_gfs_code_id_foreign');
            $table->dropForeign('gfs_code_sections_section_id_foreign');
        });
    }
}
