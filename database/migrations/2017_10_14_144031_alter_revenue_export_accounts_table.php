<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRevenueExportAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('revenue_export_accounts', function (Blueprint $table) {
            $table->boolean('is_sent')->default(false);
            $table->boolean('is_delivered')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('revenue_export_accounts', function (Blueprint $table) {
            $table->dropColumn('is_sent');
            $table->dropColumn('is_delivered');
        });
    }
}
