<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVersionsTableDropType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('versions',function (Blueprint $blueprint){
            $blueprint->dropColumn('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('versions',function (Blueprint $blueprint){
            $blueprint->enum('type',['PLAN_CHAIN','PRIORITY_AREA','INTERVENTION','PERFORMANCE_INDICATOR','FUND_SOURCE'])->PLAN_CHAIN('');
        });
    }
}
