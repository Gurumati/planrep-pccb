<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableFacilitiesRemoveFfarsAndMuse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facilities', function (Blueprint $table) {
            $table->dropColumn(['exported_to_ffars', 'ffars_response','is_delivered_to_ffars']);
        });

        Schema::table('facilities', function (Blueprint $table) {
            $table->boolean('exported_to_muse')->after('update_by')->nullable()->default(false);
            $table->string('muse_response', 150)->after('exported_to_muse')->nullable()->default("0");
            $table->boolean('is_delivered_to_muse')->after('muse_response')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('exported_to_muse');
        Schema::drop('muse_response');
        Schema::drop('is_delivered_to_muse');

        Schema::table('facilities', function (Blueprint $table) {
            $table->boolean('exported_to_ffars')->nullable();
            $table->string('ffars_response')->nullable();
            $table->boolean('is_delivered_to_ffars')->nullable();
        });
        
    }
}
