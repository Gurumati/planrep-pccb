<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityPeriodsTableAddGeneralComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("activity_periods",function (Blueprint $blueprint){
            $blueprint->text("overall_achievement")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("activity_periods",function (Blueprint $blueprint){
            $blueprint->dropColumn("overall_achievement");
        });
    }
}
