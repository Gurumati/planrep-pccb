<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFacilityTypesTableAddAdminHierarchyLevelColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facility_types',function (Blueprint $blueprint){
            $blueprint->integer('admin_hierarchy_level_id')->unsigned()->nullable();
            $blueprint->foreign('admin_hierarchy_level_id')->references('id')->on('admin_hierarchy_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('facility_types',function (Blueprint $blueprint){
            $blueprint->dropForeign('admin_hierarchy_level_id_foreign');
        });

        Schema::table('facility_types',function (Blueprint $blueprint){
            $blueprint->dropColumn('admin_hierarchy_level_id');
        });
    }
}
