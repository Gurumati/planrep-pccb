<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBodInterventionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("bod_interventions",function (Blueprint $blueprint){
            $blueprint->dropForeign("bod_interventions_intervention_category_id_foreign");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("bod_interventions",function (Blueprint $blueprint){
            $blueprint->foreign('intervention_category_id')->references('id')->on('intervention_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }
}
