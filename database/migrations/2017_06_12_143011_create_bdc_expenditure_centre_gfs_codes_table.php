<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBdcExpenditureCentreGfsCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bdc_expenditure_centre_gfs_codes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('gfs_code_id');
			$table->integer('expenditure_centre_id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->unique(['gfs_code_id','expenditure_centre_id'], 'bdc_expenditure_centre_gfs_codes_gfs_code_id_expenditure_centre');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bdc_expenditure_centre_gfs_codes');
	}

}
