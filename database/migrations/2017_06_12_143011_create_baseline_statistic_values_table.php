<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBaselineStatisticValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('baseline_statistic_values', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('baseline_statistic_id');
			$table->integer('admin_hierarchy_id');
			$table->integer('financial_year_id');
			$table->string('value', 191)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('baseline_statistic_values');
	}

}
