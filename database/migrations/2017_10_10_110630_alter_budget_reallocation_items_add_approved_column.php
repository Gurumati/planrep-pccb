<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetReallocationItemsAddApprovedColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_reallocation_items',function (Blueprint $table){
            $table->integer('budget_export_to_account_id')->unsigned();
            $table->boolean('is_approved')->unsigned()->default(false);
            $table->integer('approved_by')->unsigned();
            $table->foreign('budget_export_to_account_id')->references('id')->on('budget_export_accounts')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('approved_by')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_reallocation_items',function (Blueprint $table){
            $table->dropColumn('budget_export_to_account_id');
            $table->dropColumn('is_approved');
            $table->integer('approved_by')->unsigned();

        });
    }
}
