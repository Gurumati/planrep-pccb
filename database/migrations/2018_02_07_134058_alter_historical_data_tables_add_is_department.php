<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterHistoricalDataTablesAddIsDepartment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('historical_data_tables',function (Blueprint $blueprint){
            $blueprint->string('grouping')->default('COUNCIL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historical_data_tables',function (Blueprint $blueprint){
            $blueprint->dropColumn('grouping');
        });
    }
}
