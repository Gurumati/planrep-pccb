<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivityFacilityFundSourceInputForwardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activity_facility_fund_source_input_forwards', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('quantity');
			$table->integer('frequency');
			$table->integer('financial_year_id');
			$table->integer('financial_year_order');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('update_by')->nullable();
			$table->integer('activity_facility_fund_source_input_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activity_facility_fund_source_input_forwards');
	}

}
