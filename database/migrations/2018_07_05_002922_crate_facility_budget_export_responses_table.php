<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateFacilityBudgetExportResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facility_budget_export_responses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("facility_id")->unsigned();
            $table->integer("financial_year_id")->unsigned();
            $table->boolean("is_sent");
            $table->boolean("is_delivered")->default(false);
            $table->string('response')->nullable();
            $table->foreign('facility_id')->references('id')->on('facilities')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facility_budget_export_responses');
    }
}
