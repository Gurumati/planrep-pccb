<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenditureCentreTaskNaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bdc_expenditure_centre_task_natures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('expenditure_centre_id')->unsigned();
            $table->integer('task_nature_id')->unsigned();
            $table->unique(['expenditure_centre_id','task_nature_id']);
            $table->foreign('expenditure_centre_id')->references('id')->on('bdc_expenditure_centres')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('task_nature_id')->references('id')->on('activity_task_natures')->onDelete('restrict')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bdc_expenditure_centre_task_natures');
    }
}
