<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReferenceTypeVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reference_type_versions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reference_type_id')->unsigned();
            $table->integer('version_id')->unsigned();
            $table->foreign('reference_type_id')->references('id')->on('reference_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('version_id')->references('id')->on('versions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reference_type_versions');
    }
}
