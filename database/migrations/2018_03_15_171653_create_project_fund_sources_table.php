<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectFundSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("project_fund_sources",function (Blueprint $blueprint){
            $blueprint->integer('id',true);
            $blueprint->integer('project_id')->unsigned();
            $blueprint->integer('fund_source_id')->unsigned();
            $blueprint->foreign('project_id', 'project')->references('id')->on('projects')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $blueprint->foreign('fund_source_id', 'fund_source')->references('id')->on('fund_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("project_fund_sources");
    }
}
