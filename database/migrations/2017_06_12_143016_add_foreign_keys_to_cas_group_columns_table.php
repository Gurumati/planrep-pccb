<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasGroupColumnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cas_group_columns', function(Blueprint $table)
		{
			$table->foreign('cas_group_type_id')->references('id')->on('cas_group_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cas_group_columns', function(Blueprint $table)
		{
			$table->dropForeign('cas_group_columns_cas_group_type_id_foreign');
		});
	}

}
