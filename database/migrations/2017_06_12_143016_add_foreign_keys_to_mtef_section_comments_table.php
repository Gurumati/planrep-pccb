<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMtefSectionCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mtef_section_comments', function(Blueprint $table)
		{
			$table->foreign('from_section_level')->references('id')->on('section_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('mtef_comment_id')->references('id')->on('mtef_comments')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('mtef_section_id')->references('id')->on('mtef_sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('to_section_level')->references('id')->on('section_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mtef_section_comments', function(Blueprint $table)
		{
			$table->dropForeign('mtef_section_comments_from_section_level_foreign');
			$table->dropForeign('mtef_section_comments_mtef_comment_id_foreign');
			$table->dropForeign('mtef_section_comments_mtef_section_id_foreign');
			$table->dropForeign('mtef_section_comments_to_section_level_foreign');
		});
	}

}
