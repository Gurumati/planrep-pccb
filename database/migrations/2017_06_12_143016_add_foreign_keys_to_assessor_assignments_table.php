<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAssessorAssignmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('assessor_assignments', function(Blueprint $table)
		{
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('period_id')->references('id')->on('periods')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('cas_assessment_round_id')->references('id')->on('cas_assessment_rounds')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('admin_hierarchy_level_id')->references('id')->on('admin_hierarchy_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('cas_assessment_category_version_id')->references('id')->on('cas_assessment_category_versions')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('assessor_assignments', function(Blueprint $table)
		{
			$table->dropForeign('assessor_assignments_user_id_foreign');
			$table->dropForeign('assessor_assignments_admin_hierarchy_id_foreign');
			$table->dropForeign('assessor_assignments_period_id_foreign');
			$table->dropForeign('assessor_assignments_cas_assessment_round_id_foreign');
			$table->dropForeign('assessor_assignments_admin_hierarchy_level_id_foreign');
			$table->dropForeign('assessor_assignments_cas_assessment_category_version_id_foreign');
		});
	}

}
