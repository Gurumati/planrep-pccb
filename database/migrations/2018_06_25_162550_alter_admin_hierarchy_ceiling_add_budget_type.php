<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAdminHierarchyCeilingAddBudgetType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_hierarchy_ceilings', function (Blueprint $table){
            $table->enum('budget_type',\App\Models\Budgeting\BudgetType::getAll())->default(\App\Models\Budgeting\BudgetType::getDefault());
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_hierarchy_ceilings', function (Blueprint $table){
           $table->dropColumn('budget_type');
        });
    }
}
