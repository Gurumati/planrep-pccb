<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCasAssessmentSubCriteriaOptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cas_assessment_sub_criteria_options', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cas_assessment_criteria_option_id');
			$table->string('name', 1000)->nullable();
			$table->integer('serial_number');
			$table->string('how_to_assess', 1000);
			$table->string('how_to_score', 191);
			$table->integer('score_value');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->boolean('is_free_score')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cas_assessment_sub_criteria_options');
	}

}
