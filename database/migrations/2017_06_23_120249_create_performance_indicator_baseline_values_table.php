<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformanceIndicatorBaselineValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performance_indicator_baseline_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('start_financial_year_id')->unsigned();
            $table->string('baseline_value');
            $table->integer('end_financial_year_id')->unsigned();
            $table->string('targeted_value');
            $table->integer('admin_hierarchy_id')->unsigned();
            $table->integer('performance_indicator_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('start_financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('end_financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('performance_indicator_id')->references('id')->on('performance_indicators')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performance_indicator_baseline_values');
    }
}
