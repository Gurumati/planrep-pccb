<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDecisionLevelAdminHierarchiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('decision_level_admin_hierarchies', function(Blueprint $table)
		{
			$table->integer('decision_level_id');
			$table->integer('admin_hierarchy_level_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('decision_level_admin_hierarchies');
	}

}
