<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMtefCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mtef_comments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('mtef_id');
			$table->text('comments');
			$table->string('document', 191)->nullable();
			$table->integer('to_decision_level');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('sort_order')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('update_by')->nullable();
			$table->integer('from_decision_level');
			$table->integer('direction')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mtef_comments');
	}

}
