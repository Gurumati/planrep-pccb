<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasPlanTableSelectOptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cas_plan_table_select_options', function(Blueprint $table)
		{
			$table->foreign('parent_id')->references('id')->on('cas_plan_table_select_options')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cas_plan_table_select_options', function(Blueprint $table)
		{
			$table->dropForeign('cas_plan_table_select_options_parent_id_foreign');
		});
	}

}
