<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPerformanceIndicatorFinancialYearValueDataSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('performance_indicator_financial_year_value_data_sources',function (Blueprint $table){
            $table->text('other_source')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('performance_indicator_financial_year_value_data_sources',function (Blueprint $table){
            $table->dropColumn('other_source');
        });
    }
}
