<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCasAssessmentCategoryVersionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cas_assessment_category_versions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('financial_year_id');
			$table->integer('reference_document_id');
			$table->integer('minimum_passmark');
			$table->integer('cas_assessment_state_id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('cas_assessment_category_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cas_assessment_category_versions');
	}

}
