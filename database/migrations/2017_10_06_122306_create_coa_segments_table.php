<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoaSegmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coa_segments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('coa_code');
            $table->integer('segment_number');
            $table->string('segment_code');
            $table->string('segment_name');
            $table->string('segment_description');
            $table->string('normal_balance');
            $table->string('category');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->boolean('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coa_segments');
    }
}
