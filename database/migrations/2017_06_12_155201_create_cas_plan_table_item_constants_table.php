<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasPlanTableItemConstantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cas_plan_table_item_constants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cas_plan_table_column_id')->unsigned();
            $table->integer('cas_plan_table_item_id')->unsigned();
            $table->integer('start_financial_year_id')->unsigned();
            $table->integer('end_financial_year_id')->unsigned();
            $table->integer('admin_hierarchy_id')->unsigned();
            $table->string('value');
            $table->timestamps();
            $table->dateTime('deleted_at')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('cas_plan_table_column_id')->references('id')->on('cas_plan_table_columns')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('cas_plan_table_item_id')->references('id')->on('cas_plan_table_items')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('start_financial_year_id')->references('id')->on('financial_years')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('end_financial_year_id')->references('id')->on('financial_years')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cas_plan_table_item_constants');
    }
}
