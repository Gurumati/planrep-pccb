<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableScrutinizationComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scrutinization_comments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('scrutinization_id');
            $table->string('budget_item_type');
            $table->integer('budget_item_id');
            $table->text('comments')->nullable();
            $table->boolean('resolved')->default(false);
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('scrutinization_id')->references('id')->on('scrutinizations')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('scrutinization_comments');
    }
}
