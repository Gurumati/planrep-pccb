<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFfarsSegmentTablesAddIsdelivered extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("facilities",function (Blueprint $blueprint){
            $blueprint->boolean('is_delivered_to_ffars')->default(false);
        });

        Schema::table("budget_classes",function (Blueprint $blueprint){
            $blueprint->boolean('is_delivered_to_ffars')->default(false);
        });

        Schema::table("projects",function (Blueprint $blueprint){
            $blueprint->boolean('is_delivered_to_ffars')->default(false);
        });

        Schema::table("gfs_codes",function (Blueprint $blueprint){
            $blueprint->boolean('is_delivered_to_ffars')->default(false);
        });

        Schema::table("fund_sources",function (Blueprint $blueprint){
            $blueprint->boolean('is_delivered_to_ffars')->default(false);
        });

        Schema::table("fund_types",function (Blueprint $blueprint){
            $blueprint->boolean('is_delivered_to_ffars')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("facilities",function (Blueprint $blueprint){
            $blueprint->dropColumn('is_delivered_to_ffars_to_ffars');
        });

        Schema::table("budget_classes",function (Blueprint $blueprint){
            $blueprint->dropColumn('is_delivered_to_ffars');
        });

        Schema::table("projects",function (Blueprint $blueprint){
            $blueprint->dropColumn('is_delivered_to_ffars');
        });

        Schema::table("gfs_codes",function (Blueprint $blueprint){
            $blueprint->dropColumn('is_delivered_to_ffars');
        });

        Schema::table("fund_sources",function (Blueprint $blueprint){
            $blueprint->dropColumn('is_delivered_to_ffars');
        });

        Schema::table("fund_types",function (Blueprint $blueprint){
            $blueprint->dropColumn('is_delivered_to_ffars');
        });
    }
}
