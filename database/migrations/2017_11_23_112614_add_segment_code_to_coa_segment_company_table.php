<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSegmentCodeToCoaSegmentCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coa_segment_company', function(Blueprint $blueprint){
            $blueprint->string('segment_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coa_segment_company', function(Blueprint $blueprint){
            $blueprint->dropColumn('segment_code');
        });
    }
}
