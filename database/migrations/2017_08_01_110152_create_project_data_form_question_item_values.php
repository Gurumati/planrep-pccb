<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectDataFormQuestionItemValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_data_form_question_item_values',function(Blueprint $table){
            $table->increments('id');
            $table->integer('admin_hierarchy_id')->unsigned();
            $table->integer('financial_year_id')->unsigned();
            $table->integer('project_data_form_question_id')->unsigned();
            $table->string('value');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('financial_year_id')->references('id')->on('financial_years')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('project_data_form_question_id')->references('id')->on('project_data_forms')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_data_form_question_item_values');
    }
}
