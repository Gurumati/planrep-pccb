<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReferenceTypeSectorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reference_type_sectors', function(Blueprint $table)
		{
			$table->foreign('reference_type_id')->references('id')->on('reference_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('sector_id')->references('id')->on('sectors')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reference_type_sectors', function(Blueprint $table)
		{
			$table->dropForeign('reference_type_sectors_reference_type_id_foreign');
			$table->dropForeign('reference_type_sectors_sector_id_foreign');
		});
	}

}
