<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasPlanTableDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cas_plan_table_details', function(Blueprint $table)
		{
			$table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('cas_plan_table_id')->references('id')->on('cas_plan_tables')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cas_plan_table_details', function(Blueprint $table)
		{
			$table->dropForeign('cas_plan_table_details_admin_hierarchy_id_foreign');
			$table->dropForeign('cas_plan_table_details_cas_plan_table_id_foreign');
			$table->dropForeign('cas_plan_table_details_financial_year_id_foreign');
		});
	}

}
