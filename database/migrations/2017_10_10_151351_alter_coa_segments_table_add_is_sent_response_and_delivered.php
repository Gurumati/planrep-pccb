<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCoaSegmentsTableAddIsSentResponseAndDelivered extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coa_segments', function(Blueprint $blueprint){
            $blueprint->boolean('is_sent')->nullable();
            $blueprint->boolean('is_delivered')->nullable();
            $blueprint->string('response')->nullable();
            $blueprint->integer('created_by')->nullable()->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coa_segments', function(Blueprint $blueprint){
            $blueprint->dropColumn('is_sent');
            $blueprint->dropColumn('is_delivered');
            $blueprint->dropColumn('response');
            $blueprint->integer('created_by')->change();
        });
    }
}
