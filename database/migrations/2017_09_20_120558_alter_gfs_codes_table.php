<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGfsCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gfs_codes',function (Blueprint $table){
            $table->foreign('account_type_id')->references('id')->on('account_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('fund_source_id')->references('id')->on('fund_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gfs_codes',function (Blueprint $table){
            $table->dropForeign('gfs_codes_account_type_id_foreign');
            $table->dropForeign('gfs_codes_fund_source_id_foreign');
        });
    }
}
