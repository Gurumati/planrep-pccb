<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCasAssessmentResultsRepliesAddUserRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

        public function up()
    {
        Schema::table('cas_assessment_result_detail_replies',function (Blueprint $blueprint){
            $blueprint->boolean('is_addressed')->default('false');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_assessment_result_detail_replies', function (Blueprint $table) {
            $table->dropColumn('is_addressed');
        });
    }
}
