<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activities', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('description');
			$table->integer('facility_id')->nullable();
			$table->integer('budget_class_id');
			$table->integer('activity_category_id')->nullable();
			$table->integer('responsible_person_id')->nullable();
			$table->string('progress_status');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('update_by')->nullable();
			$table->integer('mtef_annual_target_id');
			$table->integer('mtef_section_id');
			$table->boolean('locked')->default(0);
			$table->integer('project_id')->nullable();
			$table->string('code', 191);
			$table->integer('intervention_id')->nullable();
			$table->integer('sector_problem_id')->nullable();
			$table->boolean('is_facility_account')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activities');
	}

}
