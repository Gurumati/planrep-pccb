<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterHistoricalDataColumnsRows extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('historical_data_columns',function (Blueprint $blueprint){
            $blueprint->integer('historical_data_table_id')->unsigned();
        });

        Schema::table('historical_data_rows',function (Blueprint $blueprint){
            $blueprint->integer('historical_data_table_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historical_data_columns',function (Blueprint $blueprint){
            $blueprint->dropColumn('historical_data_table_id');
        });

        Schema::table('historical_data_rows',function (Blueprint $blueprint){
            $blueprint->dropColumn('historical_data_table_id');
        });
    }
}
