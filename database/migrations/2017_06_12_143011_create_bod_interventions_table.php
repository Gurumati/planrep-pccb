<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBodInterventionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bod_interventions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('bod_version_id');
			$table->integer('intervention_category_id');
			$table->integer('fund_source_id')->nullable();
			$table->string('arithmetic_operator', 191);
			$table->string('formula', 191);
			$table->string('formula_type');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bod_interventions');
	}

}
