<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCasPlanTableColumnsAddIsLeadingColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('cas_plan_table_columns',function (Blueprint $blueprint){
                 $blueprint->boolean('is_leading_column')->default(false);
              });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_plan_table_columns', function (Blueprint $blueprint){
                   $blueprint->dropColumn('is_leading_column');
                });
    }
}
