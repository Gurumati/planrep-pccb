<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCasPlanTableItemValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cas_plan_table_item_values', function(Blueprint $table)
        {
            $table->foreign('cas_plan_table_column_id')->references('id')->on('cas_plan_table_columns')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_plan_table_item_values', function(Blueprint $table)
        {
            $table->dropForeign('cas_plan_table_item_values_cas_plan_table_column_id_foreign');
        });
    }
}
