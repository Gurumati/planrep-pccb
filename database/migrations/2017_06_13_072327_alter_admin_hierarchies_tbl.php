<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdminHierarchiesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_hierarchies',function (Blueprint $blueprint){
            $blueprint->dropColumn('code');
        });
        Schema::table('admin_hierarchies',function (Blueprint $blueprint){
            $blueprint->string('code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_hierarchies',function (Blueprint $blueprint){
            $blueprint->string('code')->nullable()->unique();
        });
    }
}
