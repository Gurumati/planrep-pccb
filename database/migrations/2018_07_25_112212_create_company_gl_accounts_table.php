<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyGlAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_gl_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("budget_export_account_id");
            $table->boolean("is_sent");
            $table->boolean("is_delivered");
            $table->string('company');
            $table->foreign('budget_export_account_id')->references('id')->on('budget_export_accounts')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_gl_accounts');
    }
}
