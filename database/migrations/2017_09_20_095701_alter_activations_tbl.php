<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivationsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::select("DELETE FROM activations WHERE user_id NOT IN (SELECT id from users)");
        Schema::table('activations',function (Blueprint $blueprint){
            $blueprint->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activations',function (Blueprint $blueprint){
            $blueprint->dropForeign('activations_user_id_foreign');
        });
    }
}
