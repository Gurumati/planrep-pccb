<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasPlanTableItemValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cas_plan_table_item_values', function(Blueprint $table)
		{
			$table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('facility_id')->references('id')->on('facilities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cas_plan_table_item_values', function(Blueprint $table)
		{
			$table->dropForeign('cas_plan_table_item_values_admin_hierarchy_id_foreign');
			$table->dropForeign('cas_plan_table_item_values_facility_id_foreign');
			$table->dropForeign('cas_plan_table_item_values_financial_year_id_foreign');
		});
	}

}
