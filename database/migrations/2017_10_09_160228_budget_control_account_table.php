<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BudgetControlAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_control_accounts', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->string('code');
            $table->integer('admin_hierarchy_id');
            $table->boolean('is_sent');
            $table->boolean('is_delivered')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('sort_order')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->integer('update_by')->nullable();
            $table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_control_accounts');
    }
}
