<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityProjectOutputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_project_outputs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("activity_id")->unsigned();
            $table->integer("project_output_id")->unsigned();
            $table->string("value")->default("0");
            $table->foreign('activity_id')->references('id')->on('activities')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('project_output_id')->references('id')->on('project_outputs')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_project_outputs');
    }
}
