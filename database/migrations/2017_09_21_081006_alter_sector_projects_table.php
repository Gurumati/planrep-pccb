<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSectorProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sector_projects',function (Blueprint $table){
            $table->foreign('sector_id')->references('id')->on('sectors')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('project_id')->references('id')->on('projects')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sector_projects',function (Blueprint $table){
            $table->dropForeign('sector_projects_sector_id_foreign');
            $table->dropForeign('sector_projects_project_id_foreign');
        });
    }
}
