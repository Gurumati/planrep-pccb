<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminCeilingIndexAndRevenueImport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE INDEX admin_financ_section_budget_type 
                       ON admin_hierarchy_ceilings (admin_hierarchy_id, financial_year_id, section_id, budget_type)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP INDEX admin_financ_section_budget_type");
    }
}
