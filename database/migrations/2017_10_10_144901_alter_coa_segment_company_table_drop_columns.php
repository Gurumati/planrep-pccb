<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCoaSegmentCompanyTableDropColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coa_segment_company', function(Blueprint $blueprint){
            $blueprint->dropForeign('coa_segment_company_coa_segment_id_foreign');
            $blueprint->dropForeign('coa_segment_company_admin_hierarchy_id_foreign');
            $blueprint->dropColumn('created_by');
            $blueprint->dropColumn('updated_by');
        });

        Schema::dropIfExists('coa_segment_company');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('coa_segment_company', function(Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->integer('coa_segment_id');
            $blueprint->integer('admin_hierarchy_id');
            $blueprint->boolean('is_sent');
            $blueprint->boolean('is_delivered');
            $blueprint->string('response');
            $blueprint->integer('created_by');
            $blueprint->integer('updated_by');
            $blueprint->timestamps();
            $blueprint->foreign('coa_segment_id')->references('id')->on('coa_segments')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $blueprint->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }
}
