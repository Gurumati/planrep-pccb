<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCasAssessmentCriteriaOptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cas_assessment_criteria_options', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cas_assessment_category_version_id');
			$table->integer('number');
			$table->string('name', 1000)->nullable();
			$table->string('how_to_assess', 191);
			$table->string('how_to_score', 191);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cas_assessment_criteria_options');
	}

}
