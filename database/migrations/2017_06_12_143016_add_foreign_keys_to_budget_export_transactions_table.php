<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBudgetExportTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('budget_export_transactions', function(Blueprint $table)
		{
			$table->foreign('budget_transaction_type_id')->references('id')->on('budget_transaction_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('budget_export_transactions', function(Blueprint $table)
		{
			$table->dropForeign('budget_export_transactions_budget_transaction_type_id_foreign');
		});
	}

}
