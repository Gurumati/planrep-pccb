<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFundSourcesTble extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_sources',function (Blueprint $blueprint){
            $blueprint->dropUnique('fund_sources_name_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('fund_sources',function (Blueprint $blueprint){
            $blueprint->unique('name');
        });
    }
}
