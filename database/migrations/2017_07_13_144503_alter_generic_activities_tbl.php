<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGenericActivitiesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('generic_activities',function (Blueprint $table){
            $table->renameColumn('generic_problem_id','generic_sector_problem_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('generic_activities',function (Blueprint $table){
            $table->renameColumn('generic_sector_problem_id','generic_problem_id');
        });
    }
}
