<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevenueExportTransactionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revenue_export_transaction_items', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('revenue_export_transaction_id');
            $table->integer('revenue_export_account_id');
            $table->decimal('amount', 20)->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('revenue_export_transaction_id')->references('id')->on('revenue_export_transactions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('revenue_export_account_id')->references('id')->on('revenue_export_accounts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revenue_export_transaction_items');
    }
}
