<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFundAllocatedTransactionItemsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_allocated_transaction_items',function (Blueprint $table){
            $table->foreign('fund_allocated_from_financial_system_id')->references('id')->on('fund_allocated_from_financial_systems')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_allocated_transaction_items',function (Blueprint $table){
            $table->dropForeign('fund_allocated_transaction_items_fund_allocated_from_financial_');
        });
    }
}
