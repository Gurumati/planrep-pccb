<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdminHierarchyCeilingsAddFacilityId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_hierarchy_ceilings',function (Blueprint $table){
           $table->integer('facility_id')->unsigned()->nullable();
           $table->boolean('is_facility')->default(false);
           $table->foreign('facility_id')->references('id')->on('facilities')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_hierarchy_ceilings',function (Blueprint $table){
            $table->dropColumn('facility_id');
            $table->dropColumn('is_facility');
        });
    }
}
