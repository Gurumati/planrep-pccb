<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFundAllocatedTransactionsTableAddBudgetControlAccountsIdAndMakeBudgetExportAccountIdNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_allocated_transaction_items',function(Blueprint $blueprint){
            $blueprint->integer('budget_control_account_id')->unsigned()->nullable();
            $blueprint->integer('budget_export_account_id')->unsigned()->nullable()->change();
            $blueprint->foreign('budget_control_account_id')->references('id')->on('budget_control_accounts')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_allocated_transaction_items',function(Blueprint $blueprint){
            $blueprint->dropForeign('fund_allocated_transaction_items_budget_control_account_id_fore');
        });

        Schema::table('fund_allocated_transaction_items',function(Blueprint $blueprint){
            $blueprint->dropColumn('budget_control_account_id');
            $blueprint->integer('budget_export_account_id')->unsigned()->change();
        });
    }
}
