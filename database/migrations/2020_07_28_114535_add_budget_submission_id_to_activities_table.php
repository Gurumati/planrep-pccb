<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBudgetSubmissionIdToActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activities',function (Blueprint $table){
            $table->dropForeign('activities_responsible_person_id_foreign');
            $table->foreign('responsible_person_id')->references('id')->on('budget_submission_select_option');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities',function (Blueprint $table){
            $table->dropForeign('activities_responsible_person_id_foreign');
            $table->foreign('responsible_person_id')->references('id')->on('users');
           
        });
    }
}
