<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSectorTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sector_targets',function (Blueprint $table){
            $table->foreign('sector_id')->references('id')->on('sectors')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('long_term_target_id')->references('id')->on('long_term_targets')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sector_targets',function (Blueprint $table){
            $table->dropForeign('sector_targets_sector_id_foreign');
            $table->dropForeign('sector_targets_long_term_target_id_foreign');
        });
    }
}
