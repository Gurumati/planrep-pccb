<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CofogSectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cofog_sections', function(Blueprint $table){
            $table->increments('id');
            $table->integer("cofog_id")->unsigned();
            $table->integer("section_id")->unsigned();
            $table->unique(['cofog_id','section_id']);
            $table->foreign('cofog_id')->references('id')->on('cofogs')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('section_id')->references('id')->on('sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cofog_sections');
    }
}
