<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetImportMissingAccountsAddFinancialSystemCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_import_missing_accounts', function(Blueprint $blueprint){
            $blueprint->string('financial_system_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_import_missing_accounts', function(Blueprint $blueprint){
            $blueprint->dropColumn('financial_system_code');
        });
    }
}
