<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityImplementationsAddPhysicalProgressRemarks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("activity_implementations",function (Blueprint $blueprint){
            $blueprint->text("remarks")->nullable()->comment("Remarks on Physical Progress");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("activity_implementations",function (Blueprint $blueprint){
            $blueprint->dropColumn("remarks");
        });
    }
}
