<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBdcMainGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bdc_main_groups', function(Blueprint $table)
		{
			$table->foreign('sector_id')->references('id')->on('sectors')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bdc_main_groups', function(Blueprint $table)
		{
			$table->dropForeign('bdc_main_groups_sector_id_foreign');
		});
	}

}
