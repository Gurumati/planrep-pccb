<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReallocationItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_reallocation_items', function (Blueprint $table) {
            $table->boolean('is_rejected')->default(false);
            $table->string('comments')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_reallocation_items', function (Blueprint $table) {
            $table->dropColumn('is_rejected');
            $table->dropColumn('comments');
        });
    }
}
