<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdminHierarchyGfsCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_hierarchy_gfs_codes',function (Blueprint $table){
            $table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('gfs_code_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_hierarchy_gfs_codes',function (Blueprint $table){
            $table->dropForeign('admin_hierarchy_gfs_codes_gfs_code_id_foreign');
            $table->dropForeign('admin_hierarchy_gfs_codes_admin_hierarchy_id_foreign');
        });
    }
}
