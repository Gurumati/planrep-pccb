<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCalenderAddSection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calendars',function (Blueprint $table){
            $table->integer('section_level_id')->unsigned()->nullable();
            $table->foreign('section_level_id')->references('id')->on('section_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calendars',function (Blueprint $table){
            $table->dropColumn('section_level_id');
        });
    }
}
