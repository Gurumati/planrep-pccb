<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUidToReceivedFundMissingAccountsTable extends Migration
{
     /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("received_fund_missing_accounts",function (Blueprint $blueprint){
            $blueprint->integer("uid")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("received_fund_missing_accounts",function (Blueprint $blueprint){
            $blueprint->dropColumn("uid");
        });
    }
}
