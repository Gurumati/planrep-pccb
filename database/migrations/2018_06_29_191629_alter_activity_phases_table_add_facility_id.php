<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityPhasesTableAddFacilityId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("activity_phase_milestones",function (Blueprint $blueprint){
            $blueprint->integer("facility_id")->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("activity_phase_milestones",function (Blueprint $blueprint){
            $blueprint->dropColumn("facility_id");
        });
    }
}
