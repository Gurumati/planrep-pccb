<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExpenditureCentresDropTaskNaturesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bdc_expenditure_centres', function (Blueprint $table) {
            $table->dropColumn('task_nature_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bdc_expenditure_centres', function (Blueprint $table) {
            $table->integer('task_nature_id')->unsigned()->nullable();
            $table->foreign('task_nature_id')->references('id')->on('activity_task_natures')->onDelete('restrict')->onUpdate('cascade');
        });
    }
}
