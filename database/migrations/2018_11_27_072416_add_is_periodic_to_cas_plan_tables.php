<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsPeriodicToCasPlanTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("cas_plan_tables",function (Blueprint $blueprint){
            $blueprint->boolean("is_periodic")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("cas_plan_tables",function (Blueprint $blueprint){
            $blueprint->dropColumn("is_periodic");
        });
    }
}
