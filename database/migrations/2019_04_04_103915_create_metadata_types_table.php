<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetadataTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metadata_types',function(Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->timestamps();
            $blueprint->string('metadata_type_name');
            $blueprint->integer('metadata_source_id')->nullable();
            $blueprint->string('api')->nullable();
            $blueprint->foreign('metadata_source_id')->references('id')->on('metadata_sources')->onUpdate('cascade')->onDelete('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('metadata_types');
    }
}
