<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAssetsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets',function (Blueprint $table){
            $table->integer('used_for')->nullable()->unsigned();
            $table->integer('condition')->nullable()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets',function (Blueprint $table){
            $table->dropColumn('used_for');
            $table->dropColumn('condition');
        });
    }
}
