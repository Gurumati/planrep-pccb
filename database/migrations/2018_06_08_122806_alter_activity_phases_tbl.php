<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityPhasesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("activity_phases",function (Blueprint $table){
            $table->integer("period_id")->unsigned()->nullable();
            $table->foreign('period_id')->references('id')->on('periods')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->double("amount",20,2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("activity_phases",function (Blueprint $table){
            $table->dropColumn("period_id");
            $table->dropColumn("amount");
        });
    }
}
