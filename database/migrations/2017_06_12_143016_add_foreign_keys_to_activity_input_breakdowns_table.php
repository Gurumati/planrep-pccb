<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActivityInputBreakdownsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('activity_input_breakdowns', function(Blueprint $table)
		{
			$table->foreign('activity_input_id')->references('id')->on('activity_inputs')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('budget_submission_definition_id', 'activity_input_breakdowns_budget_submission_definition_id_forei')->references('id')->on('budget_submission_definitions')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('unit_id')->references('id')->on('units')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('activity_input_breakdowns', function(Blueprint $table)
		{
			$table->dropForeign('activity_input_breakdowns_activity_input_id_foreign');
			$table->dropForeign('activity_input_breakdowns_budget_submission_definition_id_forei');
			$table->dropForeign('activity_input_breakdowns_unit_id_foreign');
		});
	}

}
