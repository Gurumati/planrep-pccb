<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMetadataListItemsAddCategoryOptionCombo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('metadata_list_items', function (Blueprint $blueprint){
            $blueprint->string('category_option_combos',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metadata_list_items', function (Blueprint $blueprint){
            $blueprint->dropColumn('category_option_combos');
        });
    }
}
