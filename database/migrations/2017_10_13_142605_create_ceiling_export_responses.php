<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCeilingExportResponses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

   public function up()
   {
       Schema::create('ceiling_export_responses', function (Blueprint $table) {
           $table->bigIncrements('id');
           $table->integer('admin_hierarchy_ceiling_id');
           $table->string('period');
           $table->string('financial_system_code');
           $table->boolean('is_sent')->default(false);
           $table->boolean('is_delivered')->default(false);
           $table->string('response');
           $table->foreign('admin_hierarchy_ceiling_id')->references('id')->on('admin_hierarchy_ceilings')->onUpdate('CASCADE')->onDelete('RESTRICT');
           $table->integer('created_by');
           $table->integer('updated_by')->nullable();
           $table->timestamps();
       });
   }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ceiling_export_responses',function (Blueprint $table){
            $table->dropForeign('ceiling_export_response_admin_hierarchy_ceiling_id_foreign');
        });

        Schema::dropIfExists('ceiling_export_responses');
    }
}
