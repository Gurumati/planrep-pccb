<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProcurementTypeIdToGfsCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('gfs_codes', function(Blueprint $table)
		{
			$table->integer('procurement_type_id')->nullable();
            $table->foreign('procurement_type_id')->references('id')->on('gfs_codes');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gfs_codes', function (Blueprint $table) {
            $table->dropColumn('procurement_type_id');
        });
    }
}
