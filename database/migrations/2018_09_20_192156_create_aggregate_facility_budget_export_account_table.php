<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAggregateFacilityBudgetExportAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aggregate_facility_budget_export_accounts', function(Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->integer('budget_export_account_id')->unsigned();
            $blueprint->integer('aggregate_facility_budget_id')->unsigned();
            $blueprint->timestamps();
            $blueprint->foreign('budget_export_account_id')->references('id')->on('budget_export_accounts')->onUpdate('cascade')->onDelete('restrict');
            $blueprint->foreign('aggregate_facility_budget_id')->references('id')->on('aggregate_facility_budget')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aggregate_facility_budget_export_accounts');
    }
}
