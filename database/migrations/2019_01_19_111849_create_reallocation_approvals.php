<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReallocationApprovals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_reallocation_approvals', function(Blueprint $table){
            $table->increments('id');
            $table->integer('budget_reallocation_id')->unsigned();
            $table->integer('decision_level_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('comments');
            $table->string('document_url')->nullable();
            $table->timestamps();
            $table->foreign('budget_reallocation_id')->references('id')->on('budget_reallocations')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('decision_level_id')->references('id')->on('decision_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropTable('budget_reallocation_approvals');
    }
}
