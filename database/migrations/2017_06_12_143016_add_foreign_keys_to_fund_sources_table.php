<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFundSourcesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fund_sources', function(Blueprint $table)
		{
			$table->foreign('fund_source_category_id')->references('id')->on('fund_source_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('fund_source_id')->references('id')->on('fund_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fund_sources', function(Blueprint $table)
		{
			$table->dropForeign('fund_sources_fund_source_category_id_foreign');
			$table->dropForeign('fund_sources_fund_source_id_foreign');
		});
	}

}
