<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCalendarsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calendars', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('description');
			$table->integer('financial_year_id');
			$table->date('start_date');
			$table->date('end_date');
			$table->integer('calendar_event_id');
			$table->integer('hierarchy_position');
			$table->integer('sector_id')->nullable();
			$table->text('before_start_reminder_sms');
			$table->text('before_end_reminder_sms');
			$table->integer('before_start_reminder_days');
			$table->integer('before_end_reminder_days');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('calendars');
	}

}
