<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReferenceDocsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reference_docs', function(Blueprint $table)
		{
			$table->foreign('parent_id', 'generic_line_items_generic_line_item_id_foreign')->references('id')->on('reference_docs')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('reference_type_id', 'generic_line_items_generic_type_id_foreign')->references('id')->on('reference_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reference_docs', function(Blueprint $table)
		{
			$table->dropForeign('generic_line_items_generic_line_item_id_foreign');
			$table->dropForeign('generic_line_items_generic_type_id_foreign');
		});
	}

}
