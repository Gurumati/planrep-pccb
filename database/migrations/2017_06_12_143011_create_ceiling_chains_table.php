<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCeilingChainsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ceiling_chains', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('for_admin_hierarchy_level_position');
			$table->integer('admin_hierarchy_level_position');
			$table->integer('next_id')->nullable();
			$table->integer('section_level_position');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ceiling_chains');
	}

}
