<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBudgetExportTransactionItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('budget_export_transaction_items', function(Blueprint $table)
		{
			$table->foreign('budget_export_account_id', 'budget_export_transaction_items_budget_export_account_id_foreig')->references('id')->on('budget_export_accounts')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('budget_export_transaction_id', 'budget_export_transaction_items_budget_export_transaction_id_fo')->references('id')->on('budget_export_transactions')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('budget_reallocation_item_id', 'budget_export_transaction_items_budget_reallocation_item_id_for')->references('id')->on('budget_reallocation_items')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('budget_export_transaction_items', function(Blueprint $table)
		{
			$table->dropForeign('budget_export_transaction_items_budget_export_account_id_foreig');
			$table->dropForeign('budget_export_transaction_items_budget_export_transaction_id_fo');
			$table->dropForeign('budget_export_transaction_items_budget_reallocation_item_id_for');
		});
	}

}
