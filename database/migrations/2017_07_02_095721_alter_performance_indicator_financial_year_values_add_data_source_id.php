<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPerformanceIndicatorFinancialYearValuesAddDataSourceId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('performance_indicator_financial_year_values', function (Blueprint $table){
            $table->integer('data_source_id')->nullable();
            $table->foreign('data_source_id')->references('id')->on('data_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('performance_indicator_financial_year_values', function (Blueprint $table){
            $table->dropColumn('data_source_id');
        });
    }
}
