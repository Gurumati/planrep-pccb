<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToGenericTemplatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('generic_templates', function(Blueprint $table)
		{
			$table->foreign('generic_template_id', 'generic_templates_parent_id_foreign')->references('id')->on('generic_templates')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('plan_chain_id')->references('id')->on('plan_chains')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('generic_templates', function(Blueprint $table)
		{
			$table->dropForeign('generic_templates_parent_id_foreign');
			$table->dropForeign('generic_templates_plan_chain_id_foreign');
		});
	}

}
