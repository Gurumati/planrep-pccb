<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFfarsActualControlIds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ffars_actual_control_ids', function(Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->integer('transaction_id');
            $blueprint->string('transaction_type');
            $blueprint->timestamps();        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ffars_actual_control_ids');
    }
}
