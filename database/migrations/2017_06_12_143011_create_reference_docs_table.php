<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReferenceDocsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reference_docs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('code', 191)->unique('generic_line_items_code_unique');
			$table->text('name');
			$table->boolean('is_active');
			$table->text('description');
			$table->integer('reference_type_id')->nullable();
			$table->integer('parent_id')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reference_docs');
	}

}
