<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReceivedFundsTableMakeImportMethodAndDataSourceIdNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('received_funds',function (Blueprint $blueprint){
            $blueprint->integer('data_source_id')->unsigned()->nullable()->change();
            $blueprint->integer('import_method_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('received_funds',function (Blueprint $blueprint){
            $blueprint->integer('data_source_id')->nullable()->change();
            $blueprint->integer('import_method_id')->nullable()->change();
        });
    }
}
