<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaselineStatisticFinancialYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baseline_statistic_financial_years', function (Blueprint $table) {
            $table->increments('id');

            $table->integer("baseline_statistic_id")->unsigned();
            $table->integer("financial_year_id")->unsigned();
            $table->unique(['baseline_statistic_id','financial_year_id']);
            $table->foreign('baseline_statistic_id')->references('id')->on('baseline_statistics')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('baseline_statistic_financial_years');
    }
}
