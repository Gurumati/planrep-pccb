<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLongTermTargetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('long_term_targets', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('plan_chain_id');
			$table->text('description');
			$table->boolean('is_active');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->string('code', 191);
			$table->integer('reference_document_id');
			$table->integer('generic_template_id')->nullable();
			$table->integer('intervention_id')->nullable();
			$table->integer('sector_problem_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('long_term_targets');
	}

}
