<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionCumulativeExpenditure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('create or replace function func_cumulative_expenditure (period_code varchar, admin_hierarchy int, financial_year int)
        returns table (
         financial_year_id int,
         admin_hierarchy_id int,
         department_id int,
         section_id int,
         activity_id int,
         budget_class_id int,
         fund_source_id int,
         gfs_id int,
         facility_id int,
         budget_type varchar,
         amount numeric
        )
        as $$
        begin
        return query
                 select
                  fy.id as financial_year_id,
                  ah.id as admin_hierarchy_id,
                  dpt.id as department_id,
                  s.id as section_id,
                  ac.id as activity_id,
                  bc.id as budget_class_id,
                  fs.id as fund_source_id,
                  gfs.id as gfs_id,
                  f.id as facility_id,
                  ac.budget_type,
                  sum(i.debit_amount::numeric(15,2)) - sum(i.credit_amount::numeric(15,2)) as amount
              from
                  budget_import_items i join
                  budget_classes bc on bc.id = i.budget_class_id join
                  fund_sources fs on fs.id = i.fund_source_id join
                  gfs_codes gfs on gfs.id = i.gfs_code_id join
                  periods p on p.id = i.period_id join
                  budget_export_accounts a on a.id = i.budget_export_account_id join
                  financial_years fy on fy.id = a.financial_year_id join
                  admin_hierarchies ah on ah.id = a.admin_hierarchy_id join
                  activities  ac on ac.id = a.activity_id join
                  activity_facilities af on af.activity_id = ac.id  join
                  facilities f on f.id = af.facility_id join
                  mtef_sections ms on ms.id = ac.mtef_section_id join
                  sections s on s.id = ms.section_id join
                  sections dpt on dpt.id = s.parent_id
              where ah.id = admin_hierarchy and fy.id = financial_year
                and p.code = ANY (case period_code when \'Q2\' then ARRAY[\'Q1\', \'Q2\'] when \'Q3\' then ARRAY[\'Q1\', \'Q2\', \'Q3\'] when \'Q4\' then ARRAY[\'Q1\', \'Q2\',\'Q3\',\'Q4\']
                else ARRAY[\'Q1\'] end)
              group by
                  fy.id,
                  ah.id,
                  dpt.id,
                  s.id,
                  ac.id,
                  bc.id,
                  fs.id,
                  gfs.id,
                  ac.budget_type,
                  f.id;
                end; $$
                LANGUAGE \'plpgsql\';');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('Drop function func_cumulative_expenditure');

    }
}
