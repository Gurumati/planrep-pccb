<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFundTypeIdToFundSourceBudgetClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("fund_source_budget_classes",function (Blueprint $blueprint){
            $blueprint->integer("fund_type_id")->unsigned()->nullable();
            $blueprint->foreign('fund_type_id')->references('id')->on('fund_types')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("fund_source_budget_classes",function (Blueprint $blueprint){
            $blueprint->dropColumn("fund_type_id");
        });
    }
}
