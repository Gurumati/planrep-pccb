<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCasAssessmentResultDetailsChangeSubCriteriaScoreToFloat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cas_assessment_result_details', function(Blueprint $blueprint){
            $blueprint->float('sub_criteria_score')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_assessment_result_details', function(Blueprint $blueprint){
            $blueprint->integer('sub_criteria_score')->change();
        });
    }
}
