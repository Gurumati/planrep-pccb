<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFuncCumDptExpenditure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE OR REPLACE FUNCTION func_cumulative_dpt_expenditure(period_code character varying, admin_hierarchy integer, financial_year integer)
        RETURNS TABLE(financial_year_id integer, admin_hierarchy_id integer, department_id integer, budget_class_id integer, fund_source_id integer, gfs_code_id integer, cum_expenditure numeric)
        LANGUAGE plpgsql
       AS $$
               begin
               return query
               select 
                   expall.financial_year_id,
                   expall.admin_hierarchy_id,
                   expall.department_id,
                   expall.budget_class_id,
                   expall.fund_source_id,
                   expall.gfs_code_id,
                   coalesce(sum(amount),0.00) as cum_expenditure from 
                   (
                   --start  gl matched
                   select
                           a.admin_hierarchy_id,
                           a.financial_year_id,
                           dpt.id as department_id,
                           i.budget_class_id,
                           i.fund_source_id,
                           a.gfs_code_id,
                           sum(i.debit_amount::numeric(15,2)) - sum(i.credit_amount::numeric(15,2)) as amount
                       from budget_import_items i 
                       join budget_classes bc on bc.id = i.budget_class_id 
                       join fund_sources fs on fs.id = i.fund_source_id 
                       join periods p on p.id = i.period_id 
                       join budget_export_accounts a on a.id = i.budget_export_account_id
                       join sections as sec on sec.id = a.section_id
                       join sections as dpt on dpt.id = sec.parent_id
                       where
                           a.admin_hierarchy_id = admin_hierarchy and 
                           a.financial_year_id = financial_year and 
                           p.code = ANY (case period_code when 'Q2' then ARRAY['Q1', 'Q2'] when 'Q3' then ARRAY['Q1', 'Q2', 'Q3'] when 'Q4' then ARRAY['Q1', 'Q2','Q3','Q4']
                               else ARRAY['Q1'] end)
                       group by 
                           a.admin_hierarchy_id,
                           a.financial_year_id,
                           dpt.id,
                           i.budget_class_id,
                           i.fund_source_id,
                           a.gfs_code_id
                   --end  gl matched
                   union all 
                   -- Start gl not matched
                   select
                           bima.admin_hierarchy_id,
                           bima.financial_year_id,
                           dpt.id as department_id,
                           bc.id as budget_class_id,
                           fu.id as fund_source_id,
                           gfs.id as gfs_code_id,
                           sum(bima.debit_amount::numeric(15,2)) - sum(bima.credit_amount::numeric(15,2)) as amount
                       from budget_import_missing_accounts as bima 
                       join fund_sources as fu on fu.code = substring(bima.chart_of_accounts, 44,3)
                       join budget_classes as bc on bc.code = substring(bima.chart_of_accounts, 15,3)
                       join sections as sec on sec.code = substring(bima.chart_of_accounts, 10,4)
                       join sections as dpt on dpt.id = sec.parent_id
                       join periods as p on p.start_date <= bima.\"JEDate\" and p.end_date >= bima.\"JEDate\"
                       join gfs_codes as gfs on gfs.code = substring(bima.chart_of_accounts, 48,8)
                       where 
                           \"bookID\"='MAINP' and 
                           bima.transaction_type = 'EXPENDITURE' and 
                           bima.admin_hierarchy_id = admin_hierarchy and 
                           bima.financial_year_id = financial_year and 
                           p.code = ANY (case period_code when 'Q2' then ARRAY['Q1', 'Q2'] when 'Q3' then ARRAY['Q1', 'Q2', 'Q3'] when 'Q4' then ARRAY['Q1', 'Q2','Q3','Q4']
                               else ARRAY['Q1'] end)
                       group by
                           bima.admin_hierarchy_id,
                           bima.financial_year_id,
                           dpt.id,
                           bc.id, 
                           fu.id,
                           gfs.id
                   -- End gl not matched
                   ) as expall
               group by 
                   expall.financial_year_id,
                   expall.admin_hierarchy_id,
                   expall.department_id,
                   expall.budget_class_id,
                   expall.fund_source_id,
                   expall.gfs_code_id;
       end; $$
       ;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP FUNCTION func_cumulative_dpt_expenditure");
    }
}
