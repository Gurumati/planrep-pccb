<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFacilitiesTable94949 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("facilities", function (Blueprint $blueprint) {
            $blueprint->dropColumn(['bank', 'account_number', 'account_name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("facilities", function (Blueprint $blueprint) {
            $blueprint->string('bank')->nullable();
            $blueprint->string('account_number')->nullable();
            $blueprint->string('account_name')->nullable();
        });
    }
}
