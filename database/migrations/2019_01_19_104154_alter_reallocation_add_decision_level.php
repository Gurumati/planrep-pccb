<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReallocationAddDecisionLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_reallocations', function(Blueprint $table){
            $table->integer('decision_level_id')->unsigned()->nullable();
            $table->string('document_url')->nullable();
            $table->boolean('is_open')->default(true);
            $table->foreign('decision_level_id')->references('id')->on('decision_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_reallocations', function(Blueprint $table){
            $table->dropColumn('decision_level_id');
            $table->dropColumn('is_open');
            $table->dropColumn('document_url');
        });
    }
}
