<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBdcExpenditureCentreValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bdc_expenditure_centre_values', function(Blueprint $table)
		{
			$table->foreign('expenditure_centre_id')->references('id')->on('bdc_expenditure_centres')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bdc_expenditure_centre_values', function(Blueprint $table)
		{
			$table->dropForeign('bdc_expenditure_centre_values_expenditure_centre_id_foreign');
			$table->dropForeign('bdc_expenditure_centre_values_financial_year_id_foreign');
		});
	}

}
