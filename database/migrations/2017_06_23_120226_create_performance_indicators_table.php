<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformanceIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performance_indicators', function (Blueprint $table) {
            $table->increments('id');
            $table->text("description");
            $table->string("number")->unique();
            $table->integer("plan_chain_id")->unsigned();
            $table->integer("data_source_id")->unsigned()->nullable();
            $table->string("other_source")->nullable();
            $table->boolean("is_qualitative")->default(false);
            $table->boolean("less_is_good")->default(false);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('plan_chain_id')->references('id')->on('plan_chains')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('data_source_id')->references('id')->on('data_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performance_indicators');
    }
}
