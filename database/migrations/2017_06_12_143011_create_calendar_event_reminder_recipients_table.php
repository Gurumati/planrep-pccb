<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCalendarEventReminderRecipientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calendar_event_reminder_recipients', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('calendar_event_id');
			$table->string('email', 191);
			$table->string('mobile_number', 191)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->unique(['calendar_event_id','email','mobile_number'], 'calendar_event_reminder_recipients_calendar_event_id_email_mobi');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('calendar_event_reminder_recipients');
	}

}
