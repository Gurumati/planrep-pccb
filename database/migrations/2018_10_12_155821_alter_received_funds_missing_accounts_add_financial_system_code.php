<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReceivedFundsMissingAccountsAddFinancialSystemCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('received_fund_missing_accounts', function (Blueprint $blueprint){
            $blueprint->string('financial_system_code')->nullable();
            $blueprint->string('transaction_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('received_fund_missing_accounts', function (Blueprint $blueprint){
            $blueprint->dropColumn('financial_system_code');
            $blueprint->dropColumn('transaction_type');
        });
    }
}
