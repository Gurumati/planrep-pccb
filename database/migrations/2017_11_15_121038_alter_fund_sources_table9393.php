<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFundSourcesTable9393 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_sources',function (Blueprint $blueprint){
            $blueprint->boolean('is_conditional')->default('false')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_sources',function (Blueprint $blueprint){
            $blueprint->boolean('is_conditional')->default('false')->change();
        });
    }
}
