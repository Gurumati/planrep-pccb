<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCasPlanTableItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cas_plan_table_items', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('code', 191);
			$table->integer('cas_plan_table_id');
			$table->text('description');
			$table->integer('sort_order');
			$table->string('min_value', 191)->nullable();
			$table->string('max_value', 191)->nullable();
			$table->integer('value_type_id')->nullable();
			$table->integer('best_value_id')->nullable();
			$table->integer('good_nature_trend_id')->nullable();
			$table->integer('aggregation_operation_id')->nullable();
			$table->integer('statistics_id')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('cas_plan_table_item_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cas_plan_table_items');
	}

}
