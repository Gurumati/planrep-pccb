<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFfarsActualControlIdsAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ffars_actual_control_ids', function (Blueprint $table){
            $table->integer('ffars_uid')->nullable()->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ffars_actual_control_ids', function (Blueprint $table){
            $table->dropColumn('ffars_uid');
        });
    }
}
