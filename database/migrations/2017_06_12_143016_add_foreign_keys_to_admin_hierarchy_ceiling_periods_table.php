<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAdminHierarchyCeilingPeriodsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('admin_hierarchy_ceiling_periods', function(Blueprint $table)
		{
			$table->foreign('admin_hierarchy_ceiling_id', 'admin_hierarchy_ceiling_periods_admin_hierarchy_ceiling_id_fore')->references('id')->on('admin_hierarchy_ceilings')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('period_id')->references('id')->on('periods')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('admin_hierarchy_ceiling_periods', function(Blueprint $table)
		{
			$table->dropForeign('admin_hierarchy_ceiling_periods_admin_hierarchy_ceiling_id_fore');
			$table->dropForeign('admin_hierarchy_ceiling_periods_period_id_foreign');
		});
	}

}
