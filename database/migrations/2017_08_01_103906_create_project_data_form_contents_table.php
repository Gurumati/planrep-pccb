<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectDataFormContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_data_form_contents',function(Blueprint $table){
            $table->increments('id');
            $table->integer('project_data_form_id')->unsigned();
            $table->boolean('is_lowest')->nullable();
            $table->integer('sort_order');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('description');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('project_data_form_id')->references('id')->on('project_data_forms')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('parent_id')->references('id')->on('project_data_form_contents')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_data_form_contents');
    }
}
