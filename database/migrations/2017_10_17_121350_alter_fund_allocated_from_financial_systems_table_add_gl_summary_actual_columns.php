<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFundAllocatedFromFinancialSystemsTableAddGlSummaryActualColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_allocated_from_financial_systems',function(Blueprint $blueprint){
            $blueprint->string('BookID');
            $blueprint->date('JEDate');
            $blueprint->date('FiscalYear');
            $blueprint->decimal('DebitAmount',20,2);
            $blueprint->decimal('CreditAmount',20,2);
            $blueprint->dropColumn('amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_allocated_from_financial_systems',function(Blueprint $blueprint){
            $blueprint->dropColumn('BookID');
            $blueprint->dropColumn('JEDate');
            $blueprint->dropColumn('FiscalYear');
            $blueprint->dropColumn('DebitAmount');
            $blueprint->dropColumn('CreditAmount');
            $blueprint->decimal('amount',20,2);
        });
    }
}
