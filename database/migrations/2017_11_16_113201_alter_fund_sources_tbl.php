<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFundSourcesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_sources',function (Blueprint $blueprint){
            $blueprint->integer('fund_source_category_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_sources',function (Blueprint $blueprint){
            $blueprint->integer('fund_source_category_id')->unsigned()->change();
        });
    }
}
