<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGenericActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('generic_activities', function(Blueprint $table){
            $table->integer('priority_area_id')->unsigned()->nullable();
            $table->dropColumn('national_target_id');
            $table->dropColumn('performance_indicator_id');
            $table->foreign('priority_area_id')->references('id')->on('priority_areas')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
