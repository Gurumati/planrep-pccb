<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivityInputPeriodsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activity_input_periods', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('activity_input_id');
			$table->integer('period_id');
			$table->float('percentage', 10, 0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activity_input_periods');
	}

}
