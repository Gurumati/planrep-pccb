<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUidToMissingGlAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("budget_import_missing_accounts",function (Blueprint $blueprint){
            $blueprint->integer("uid")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("budget_import_missing_accounts",function (Blueprint $blueprint){
            $blueprint->dropColumn()("uid");
        });
    }
}
