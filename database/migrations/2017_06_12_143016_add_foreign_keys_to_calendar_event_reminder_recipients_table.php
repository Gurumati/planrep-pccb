<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCalendarEventReminderRecipientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('calendar_event_reminder_recipients', function(Blueprint $table)
		{
			$table->foreign('calendar_event_id')->references('id')->on('calendar_events')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('calendar_event_reminder_recipients', function(Blueprint $table)
		{
			$table->dropForeign('calendar_event_reminder_recipients_calendar_event_id_foreign');
		});
	}

}
