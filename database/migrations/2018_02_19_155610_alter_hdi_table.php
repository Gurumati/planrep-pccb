<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHdiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('historical_data_items',function (Blueprint $blueprint){
            $blueprint->foreign('row_id')->references('id')->on('historical_data_rows')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $blueprint->foreign('column_id')->references('id')->on('historical_data_columns')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historical_data_columns',function (Blueprint $blueprint){
            $blueprint->dropForeign("historical_data_items_column_id_foreign");
            $blueprint->dropForeign("historical_data_items_row_id_foreign");
        });
    }
}
