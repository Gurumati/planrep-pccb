<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetExportAccountGlaccountExportIssuesTableMakeResponsesText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_export_account_glaccount_export_issues', function (Blueprint $blueprint){
           $blueprint->text('response_text')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_export_account_glaccount_export_issues', function (Blueprint $blueprint){
            $blueprint->string('response_text')->change();
        });
    }
}
