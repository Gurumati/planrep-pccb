<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterLongTermTargetAddSectionId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('long_term_targets',function (Blueprint $table){

            $table->integer('section_id')->unsigned()->nullable();
            $table->foreign('section_id')
                ->references('id')
                ->on('sections')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('long_term_targets', function (Blueprint $table) {
            $table->dropColumn('section_id');
            $table->dropForeign('');
        });
    }
}
