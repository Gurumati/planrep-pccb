<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GfsCodeFundSourcePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gfscode_fundsources', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('fund_source_id');
            $table->integer('gfs_code_id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('fund_source_id')->references('id')->on('fund_sources')->onDelete('cascade');
            $table->foreign('gfs_code_id')->references('id')->on('gfs_codes')->onDelete('cascade');
            $table->unique(['fund_source_id', 'gfs_code_id'], 'fund_source_id_gfs_code_id_fund_source_id_gfs_code_id_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gfsCode_fundSources');
    }
}
