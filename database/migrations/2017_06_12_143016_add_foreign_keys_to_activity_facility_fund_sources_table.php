<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActivityFacilityFundSourcesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('activity_facility_fund_sources', function(Blueprint $table)
		{
			$table->foreign('activity_facility_id')->references('id')->on('activity_facilities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('fund_source_id')->references('id')->on('fund_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('activity_facility_fund_sources', function(Blueprint $table)
		{
			$table->dropForeign('activity_facility_fund_sources_activity_facility_id_foreign');
			$table->dropForeign('activity_facility_fund_sources_fund_source_id_foreign');
		});
	}

}
