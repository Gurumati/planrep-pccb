<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityFacilityFundSourceInputPeriodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_facility_fund_source_input_periods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('activity_facility_fund_source_input_id')->unsigned();
            $table->integer('period_id')->unsigned();
            $table->decimal('percentage',5,2)->default(0);
            $table->foreign('activity_facility_fund_source_input_id')->references('id')->on('activity_facility_fund_source_inputs')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('period_id')->references('id')->on('periods')->onUpdate('cascade')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_facility_fund_source_input_periods');
    }
}
