<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMtefSectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mtef_sections', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('mtef_id');
			$table->integer('section_id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('sort_order')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('update_by')->nullable();
			$table->integer('section_level_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mtef_sections');
	}

}
