<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGenericTargetNationalReferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generic_target_national_references', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('generic_target_id');
            $table->integer('national_reference_id');
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('generic_target_id')->references('id')->on('generic_targets')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('national_reference_id')->references('id')->on('reference_docs')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('generic_target_national_references');
    }
}
