<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBdcExpCentresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bdc_expenditure_centres',function (Blueprint $table){
            $table->integer('section_id')->nullable()->unsigned();
            $table->integer('parent_id')->nullable()->unsigned();
            $table->string('link_specs')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bdc_expenditure_centres',function (Blueprint $table){
            $table->dropColumn('section_id');
            $table->dropColumn('parent_id');
            $table->dropColumn('link_specs');
        });
    }
}
