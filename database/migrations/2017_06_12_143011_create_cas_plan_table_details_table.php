<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCasPlanTableDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cas_plan_table_details', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('admin_hierarchy_id');
			$table->integer('cas_plan_table_id');
			$table->integer('financial_year_id');
			$table->text('comment')->nullable();
			$table->text('key')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->string('url', 191)->nullable();
			$table->string('description', 191)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cas_plan_table_details');
	}

}
