<div class="env"></div><?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCasAssessmentCasAssessmentResultRepliesChangeStringToText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cas_assessment_result_replies', function (Blueprint $blueprint){
            $blueprint->text('comment')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_assessment_result_replies', function (Blueprint $blueprint){
            $blueprint->string('comment')->change();
        });
    }
}
