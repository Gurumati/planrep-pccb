<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAssestsTableAddOwnershipAndInsuranceColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("assets",function (Blueprint $table){
            $table->text('ownership')->nullable();
            $table->text('insurance_type')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("assets",function (Blueprint $table){
            $table->dropColumn('ownership');
            $table->dropColumn('insurance_type');
        });
    }
}
