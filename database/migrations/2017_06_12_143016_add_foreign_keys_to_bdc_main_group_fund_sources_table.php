<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBdcMainGroupFundSourcesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bdc_main_group_fund_sources', function(Blueprint $table)
		{
			$table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('fund_source_id')->references('id')->on('fund_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('main_group_id')->references('id')->on('bdc_main_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('reference_document_id')->references('id')->on('reference_documents')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bdc_main_group_fund_sources', function(Blueprint $table)
		{
			$table->dropForeign('bdc_main_group_fund_sources_financial_year_id_foreign');
			$table->dropForeign('bdc_main_group_fund_sources_fund_source_id_foreign');
			$table->dropForeign('bdc_main_group_fund_sources_main_group_id_foreign');
			$table->dropForeign('bdc_main_group_fund_sources_reference_document_id_foreign');
		});
	}

}
