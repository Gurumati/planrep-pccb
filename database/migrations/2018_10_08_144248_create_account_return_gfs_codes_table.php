<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountReturnGfsCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_return_gfs_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("gfs_code_id")->unsigned();
            $table->integer("account_return_id")->unsigned();
            $table->foreign('gfs_code_id')->references('id')->on('gfs_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('account_return_id')->references('id')->on('account_returns')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_return_gfs_codes');
    }
}
