<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCasPlanTableItemValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cas_plan_table_item_values', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cas_plan_table_column_id');
			$table->integer('cas_plan_table_item_id')->nullable();
			$table->string('value', 191);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('financial_year_id');
			$table->integer('admin_hierarchy_id');
			$table->integer('facility_id')->nullable();
			$table->integer('sort_order')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cas_plan_table_item_values');
	}

}
