<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProjectionPeriodsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('projection_periods', function(Blueprint $table)
		{
			$table->foreign('period_id')->references('id')->on('periods')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('projection_id')->references('id')->on('projections')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projection_periods', function(Blueprint $table)
		{
			$table->dropForeign('projection_periods_period_id_foreign');
			$table->dropForeign('projection_periods_projection_id_foreign');
		});
	}

}
