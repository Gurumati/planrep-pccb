<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBdcExpCentresTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bdc_expenditure_centres',function (Blueprint $table){
            $table->integer('activity_category_id')->nullable()->unsigned();
            $table->integer('lga_level_id')->nullable()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bdc_expenditure_centres',function (Blueprint $table){
            $table->dropColumn(['activity_category_id','lga_level_id']);
        });
    }
}
