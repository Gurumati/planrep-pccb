<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBdcExpenditureCentresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bdc_expenditure_centres',function (Blueprint $table){
            $table->foreign('section_id')->references('id')->on('sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('parent_id')->references('id')->on('bdc_expenditure_centres')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('activity_category_id')->references('id')->on('activity_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('lga_level_id')->references('id')->on('lga_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('link_spec_id')->references('id')->on('link_specs')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bdc_expenditure_centres',function (Blueprint $table){
            $table->dropForeign('bdc_expenditure_centres_section_id_foreign');
            $table->dropForeign('bdc_expenditure_centres_parent_id_foreign');
            $table->dropForeign('bdc_expenditure_centres_activity_category_id_foreign');
            $table->dropForeign('bdc_expenditure_centres_lga_level_id_foreign');
            $table->dropForeign('bdc_expenditure_centres_link_spec_id_foreign');
        });
    }
}
