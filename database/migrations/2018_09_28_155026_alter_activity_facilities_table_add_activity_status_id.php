<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityFacilitiesTableAddActivityStatusId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("activity_facilities",function (Blueprint $blueprint){
            $blueprint->integer("activity_status_id")->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("activity_facilities",function (Blueprint $blueprint){
            $blueprint->dropColumn("activity_status_id");
        });
    }
}
