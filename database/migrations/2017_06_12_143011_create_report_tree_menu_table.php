<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReportTreeMenuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('report_tree_menu', function(Blueprint $table)
		{
			$table->integer('NodeID', true);
			$table->integer('ParentID')->nullable();
			$table->string('NodeName', 191)->nullable();
			$table->string('NodeIcon', 191)->nullable();
			$table->integer('NodeLevel')->nullable();
			$table->string('Expanded', 191)->nullable();
			$table->string('NodePosition', 191)->nullable();
			$table->string('Description', 191)->nullable();
			$table->boolean('Active')->default(1);
			$table->string('SSMA_TimeStamp', 50);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('report_tree_menu');
	}

}
