<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSectorProblemsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sector_problems',function (Blueprint $table){
            $table->foreign('priority_area_id')->references('id')->on('priority_areas')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sector_problems',function (Blueprint $table){
            $table->dropForeign('sector_problems_admin_hierarchy_id_foreign');
            $table->dropForeign('sector_problems_priority_area_id_foreign');
        });
    }
}
