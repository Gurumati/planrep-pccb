<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoaSegmentCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coa_segment_company', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('coa_segment_id');
            $table->integer('admin_hierarchy_id');
            $table->boolean('is_sent');
            $table->boolean('is_delivered');
            $table->string('response');
            $table->foreign('coa_segment_id')->references('id')->on('coa_segments')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coa_segment_company',function (Blueprint $table){
            $table->dropForeign('coa_segment_company_coa_segment_id_foreign');
            $table->dropForeign('coa_segment_company_admin_hierarchy_id_foreign');
        });

        Schema::dropIfExists('coa_segment_company');
    }
}
