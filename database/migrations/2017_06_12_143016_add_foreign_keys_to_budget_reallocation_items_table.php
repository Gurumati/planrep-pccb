<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBudgetReallocationItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('budget_reallocation_items', function(Blueprint $table)
		{
			$table->foreign('budget_export_account_id')->references('id')->on('budget_export_accounts')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('budget_reallocation_items', function(Blueprint $table)
		{
			$table->dropForeign('budget_reallocation_items_budget_export_account_id_foreign');
		});
	}

}
