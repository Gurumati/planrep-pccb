<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasAssessmentResultDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cas_assessment_result_details', function(Blueprint $table)
		{
			$table->foreign('cas_assessment_result_id')->references('id')->on('cas_assessment_results')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('cas_assessment_sub_criteria_option_id', 'cas_assessment_result_details_cas_assessment_sub_criteria_optio')->references('id')->on('cas_assessment_sub_criteria_options')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('cas_assessment_sub_criteria_possible_score_id', 'cas_assessment_result_details_cas_assessment_sub_criteria_possi')->references('id')->on('cas_assessment_sub_criteria_possible_scores')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cas_assessment_result_details', function(Blueprint $table)
		{
			$table->dropForeign('cas_assessment_result_details_cas_assessment_result_id_foreign');
			$table->dropForeign('cas_assessment_result_details_cas_assessment_sub_criteria_optio');
			$table->dropForeign('cas_assessment_result_details_cas_assessment_sub_criteria_possi');
		});
	}

}
