<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableBudgetImportItemsChangeUid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_import_items', function (Blueprint $table) {
            $table->string('uid', 191)->nullable();
            $table->string('bookID')->nullable()->change();
            $table->string('msg_id')->nullable();
        });

        Schema::table('budget_import_missing_accounts', function (Blueprint $table) {
            $table->string('uid', 191)->nullable()->change();
            $table->string('bookID')->nullable()->change();
            $table->string('msg_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_import_items', function (Blueprint $table) {
            $table->dropColumn(['uid','bookID','msg_id']);
        });

        Schema::table('budget_import_missing_accounts', function (Blueprint $table) {
            $table->dropColumn(['uid','bookID','msg_id']);
        });
    }
}
