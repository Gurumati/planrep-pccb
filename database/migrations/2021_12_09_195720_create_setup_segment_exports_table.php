<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupSegmentExportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_segment_exports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('segment_detail_id')->unsigned();
            $table->date('apply_date')->unsigned();
            $table->boolean("is_sent")->default(false);
            $table->boolean("is_delivered")->default(false);
            $table->text("response")->nullable();
            $table->integer("admin_hierarchy_id")->nullable();
            $table->integer("financial_year_id")->nullable();
            $table->text("budget_type")->nullable();
            $table->text("segment_name")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_segment_exports');
    }
}
