<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimestampToAggregateFacilityBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DELETE FROM aggregate_facility_budget_export_accounts");
        DB::statement("DELETE FROM aggregate_facility_budgets");
        Schema::table("aggregate_facility_budgets",function (Blueprint $blueprint){
            $blueprint->string("timestamp");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("aggregate_facility_budgets",function (Blueprint $blueprint){
            $blueprint->dropColumn("timestamp");
        });
    }
}
