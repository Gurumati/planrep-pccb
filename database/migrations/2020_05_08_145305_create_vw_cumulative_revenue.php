<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVwCumulativeRevenue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS vw_cumulative_revenue');
        DB::statement('CREATE VIEW  vw_cumulative_revenue AS
SELECT
  financial_year_id,
  financial_year_name,
  admin_hierarchy_id,
  admin_hiearchy_name,
  admin_hierarchy_code,
  gfs_code_id,
  gfs_code_name,
  gfs_code,
  period_id,
  period_name,
  period_code,
  cost_centre_id,
  cost_centre_name,
  cost_centre_code,
  budget_class_id,
  budget_class_name,
  budget_class_code,
  facility_id,
  facility_name,
  facility_code,
  fund_source_id,
  fund_source_name,
  fund_source_code,
  amount as revenue,
  SUM(amount) OVER
    (PARTITION BY
      financial_year_id,
      admin_hierarchy_id,
      cost_centre_id,
      fund_source_id,
      gfs_code_id,
      facility_id,
      budget_class_id
    ORDER BY period_id )as cumulative_revenue
FROM
  (select
     rea.financial_year_id,
     fy.name as financial_year_name,
     rea.admin_hierarchy_id,
     ah.name as admin_hiearchy_name,
     ah.code as admin_hierarchy_code,
     gc.name as gfs_code_name,
     gc.code as gfs_code,
     gc.id as gfs_code_id,
     p.name as period_name,
     p.code as period_code,
     s2.name as cost_centre_name,s2.code as cost_centre_code,s2.id as cost_centre_id,
     fs.code as fund_source_code,fs.name as fund_source_name,
     bc.id as budget_class_id,bc.name budget_class_name,bc.code as budget_class_code,
     f.id as facility_id,f.name as facility_name,f.facility_code,
     rfi.fund_source_id,
     rfi.period_id,
     (sum(rfi.credit_amount::numeric(15,2)) - sum(rfi.debit_amount::numeric(15,2))) as amount
   from
     received_fund_items rfi
       join revenue_export_accounts rea on rea.id = rfi.revenue_export_account_id
       join financial_years fy on rea.financial_year_id = fy.id
       join admin_hierarchies ah on rea.admin_hierarchy_id = ah.id
       join gfs_codes gc on rea.gfs_code_id = gc.id
       join periods p on p.id = rfi.period_id
       join fund_sources fs on rfi.fund_source_id = fs.id
       join sections s2 on rea.section_id = s2.id
       join sections s3 on s2.parent_id = s3.id
       join sections s on s3.parent_id = s.id
       join admin_hierarchy_ceilings ahc on rea.admin_hierarchy_ceiling_id = ahc.id
       join ceilings c on c.id = ahc.ceiling_id
       join budget_classes bc on c.budget_class_id = bc.id
       left join facilities f on ahc.facility_id = f.id

   group by
     rea.financial_year_id,
     rea.admin_hierarchy_id,
     rea.section_id,
     rfi.fund_source_id,
     rfi.period_id,fy.id,ah.id,gc.id,p.id,fs.id,s2.id,bc.id,f.id
  ) as revenue');
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS vw_cumulative_revenue');
    }
}
