<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBodVersionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bod_versions', function(Blueprint $table)
		{
			$table->foreign('bod_list_id')->references('id')->on('bod_lists')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('end_financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('start_financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bod_versions', function(Blueprint $table)
		{
			$table->dropForeign('bod_versions_bod_list_id_foreign');
			$table->dropForeign('bod_versions_end_financial_year_id_foreign');
			$table->dropForeign('bod_versions_start_financial_year_id_foreign');
		});
	}

}
