<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCompletionPercentageToActivityImplementationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity_implementations', function (Blueprint $table) {
            $table->decimal('completion_percentage', 20)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     Schema::table('activity_implementations', function (Blueprint $table) {
            $table->dropColumn('completion_percentage');
        });
    }
}
