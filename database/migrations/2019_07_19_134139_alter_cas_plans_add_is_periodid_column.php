<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCasPlansAddIsPeriodidColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cas_plans', function(Blueprint $blueprint){
           $blueprint->boolean('is_periodic')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_plans', function(Blueprint $blueprint){
            $blueprint->dropColumn('is_periodic');
        });
    }
}
