<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReallocationApprovalAddIsApproved extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_reallocation_approvals', function($table){
            $table->boolean('is_approved')->default(false);
            $table->integer('user_id')->nullable()->change();
            $table->string('comments')->nullable()->change();
            $table->string('document_url')->nullable()->change();
            $table->string('updated_at')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_reallocation_approvals', function($table){
            $table->dropColumn('is_approved');
        });
    }
}
