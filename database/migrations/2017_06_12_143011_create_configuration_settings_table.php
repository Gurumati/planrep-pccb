<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConfigurationSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('configuration_settings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('key', 191);
			$table->string('value', 2000)->nullable();
			$table->string('name', 191);
			$table->string('group_name', 191);
			$table->string('description', 191)->nullable();
			$table->integer('sort_order');
			$table->string('value_type', 191);
			$table->string('value_options', 1000)->nullable();
			$table->text('value_options_query')->nullable();
			$table->boolean('is_configurable')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('configuration_settings');
	}

}
