<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAggregateFacilityBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aggregate_facility_budget', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("financial_year_id")->unsigned();
            $table->integer("admin_hierarchy_id")->unsigned();
            $table->string('chart_of_accounts')->nullable();
            $table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aggregate_facility_budget');
    }
}
