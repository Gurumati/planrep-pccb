<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetExportAccountsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('budget_export_accounts',function (Blueprint $table){
            $table->dropUnique('budget_export_accounts_chart_of_accounts_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_export_accounts',function (Blueprint $table){
            $table->unique('accounts_unique');
        });
    }
}
