<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFundSourceBudgetClassesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fund_source_budget_classes', function(Blueprint $table)
		{
			$table->foreign('budget_class_id')->references('id')->on('budget_classes')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('fund_source_id')->references('id')->on('fund_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fund_source_budget_classes', function(Blueprint $table)
		{
			$table->dropForeign('fund_source_budget_classes_budget_class_id_foreign');
			$table->dropForeign('fund_source_budget_classes_fund_source_id_foreign');
		});
	}

}
