<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectDataFormItemValuesTableAddProjectIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_data_form_question_item_values',function(Blueprint $blueprint){
            $blueprint->integer('project_id')->unsigned();
            $blueprint->foreign('project_id')->references('id')->on('projects')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_data_form_question_item_values',function(Blueprint $blueprint){
            $blueprint->dropForeign('project_data_form_question_item_values_project_id_foreign');
        });

        Schema::table('project_data_form_question_item_values',function(Blueprint $blueprint){
            $blueprint->dropColumn('project_id');
        });
    }
}
