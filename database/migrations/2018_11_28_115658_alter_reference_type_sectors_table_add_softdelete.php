<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReferenceTypeSectorsTableAddSoftdelete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("reference_type_sectors",function (Blueprint $blueprint){
            $blueprint->timestamps();
            $blueprint->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("reference_type_sectors",function (Blueprint $blueprint){
            $blueprint->dropTimestamps();
            $blueprint->dropSoftDeletes();
        });
    }
}
