<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityAddTaskNature extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activities',function (Blueprint $table){
            $table->integer('activity_task_nature_id')->unsigned()->nullable();
            $table->foreign('activity_task_nature_id')->references('id')->on('activity_task_natures')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities',function (Blueprint $table){
            $table->dropColumn('activity_task_nature_id');
        });
    }
}
