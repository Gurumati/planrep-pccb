<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterScrutinizationItemAddScrutinId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mtef_section_item_comments',function (Blueprint $table){
            $table->integer('scrutinization_id')->unsigned();
            $table->foreign('scrutinization_id')->references('id')->on('scrutinizations')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtef_section_item_comments',function (Blueprint $table){
            $table->dropColumn('scrutinization_id');
        });
    }
}
