<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveFinancialYearToCasPlanTableItemConstantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cas_plan_table_item_constants',function (Blueprint $blueprint){
            $blueprint->dropColumn('start_financial_year_id');
            $blueprint->dropColumn('end_financial_year_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_plan_table_item_constants',function (Blueprint $blueprint){
            $blueprint->integer('start_financial_year_id');
            $blueprint->integer('end_financial_year_id');
        });
    }
}
