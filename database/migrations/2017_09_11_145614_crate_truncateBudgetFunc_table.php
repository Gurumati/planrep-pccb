<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CrateTruncateBudgetFuncTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE OR REPLACE FUNCTION truncateBudget() 
                    RETURNS void AS $$
                    DECLARE table_nam varchar(50);
                    BEGIN
                      FOR table_nam IN SELECT table_name FROM truncate_table_sets ORDER BY sort_order
                      loop
                      EXECUTE
                       \'truncate table \'||table_nam||\' CASCADE\';
                      END loop; 
                    END;
                    $$ LANGUAGE plpgsql;');
        DB::update('update mtef_sections set decision_level_id = 8,is_locked = false');

        DB::update('update mtefs set decision_level_id = 8,locked = false');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       DB::raw('DROP FUNCTION public.truncatebudget();');
    }
}
