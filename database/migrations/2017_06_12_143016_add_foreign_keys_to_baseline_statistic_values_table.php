<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBaselineStatisticValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('baseline_statistic_values', function(Blueprint $table)
		{
			$table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('baseline_statistic_id')->references('id')->on('baseline_statistics')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('baseline_statistic_values', function(Blueprint $table)
		{
			$table->dropForeign('baseline_statistic_values_admin_hierarchy_id_foreign');
			$table->dropForeign('baseline_statistic_values_baseline_statistic_id_foreign');
			$table->dropForeign('baseline_statistic_values_financial_year_id_foreign');
		});
	}

}
