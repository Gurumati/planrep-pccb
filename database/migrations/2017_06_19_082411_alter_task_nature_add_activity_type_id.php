<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTaskNatureAddActivityTypeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bdc_task_natures',function (Blueprint $table){
            $table->integer('activity_category_id')->unsigned()->nullable();
            $table->foreign('activity_category_id')->references('id')->on('activity_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bdc_task_natures',function (Blueprint $table){
            $table->dropColumn('activity_category_id');
        });
    }
}
