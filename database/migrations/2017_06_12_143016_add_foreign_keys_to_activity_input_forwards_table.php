<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActivityInputForwardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('activity_input_forwards', function(Blueprint $table)
		{
			$table->foreign('activity_input_id')->references('id')->on('activity_inputs')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('activity_input_forwards', function(Blueprint $table)
		{
			$table->dropForeign('activity_input_forwards_activity_input_id_foreign');
			$table->dropForeign('activity_input_forwards_financial_year_id_foreign');
		});
	}

}
