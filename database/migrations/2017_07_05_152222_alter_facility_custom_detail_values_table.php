<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFacilityCustomDetailValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facility_custom_detail_values',function (Blueprint $blueprint){
            $blueprint->renameColumn('value','detail_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('facility_custom_detail_values',function (Blueprint $blueprint){
            $blueprint->renameColumn('detail_value','value');
        });
    }
}
