<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMtefAnnualTargetsTableAddIsFinal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('mtef_annual_targets', function (Blueprint $table) {
            $table->boolean("is_final")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtef_annual_targets', function (Blueprint $table) {
            $table->dropColumn("is_final");
        });
    }
}
