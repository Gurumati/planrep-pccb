<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateTableMtefSectionItemComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::transaction(function (){
            Schema::table('scrutinizations',function (Blueprint $table){
                $table->dropColumn('scrutinized_admin_hierarchy_id');
                $table->dropColumn('scrutinized_section_id');
                $table->integer('mtef_section_id');
                $table->boolean('is_assigned')->default(false);
                $table->foreign('mtef_section_id')->references('id')->on('mtef_sections')->onUpdate('CASCADE')->onDelete('RESTRICT');

            });

            Schema::create('mtef_section_items_comments',function (Blueprint $table){
                $table->increments('id');
                $table->integer('mtef_section_comment_id');
                $table->string('item_type');
                $table->integer('item_id');
                $table->text('comment')->nullable();
                $table->string('commented_by')->nullable();
                $table->boolean('addressed')->default(false);
                $table->string('addressed_by')->nullable();
                $table->foreign('mtef_section_comment_id')->references('id')->on('mtef_section_comments')->onUpdate('CASCADE')->onDelete('RESTRICT');

            });
            Schema::table('mtef_section_comments',function (Blueprint $table){
                $table->integer('scrutinization_id');
                $table->integer('from_decision_level_id');
                $table->integer('to_decision_level_id');
                $table->dropColumn('from_section_level');
                $table->dropColumn('to_section_level');
                $table->boolean('addressed')->default('false');
                $table->string('addressed_by')->nullable();
                $table->foreign('scrutinization_id')->references('id')->on('scrutinizations')->onUpdate('CASCADE')->onDelete('RESTRICT');
                $table->foreign('from_decision_level_id')->references('id')->on('decision_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
                $table->foreign('to_decision_level_id')->references('id')->on('decision_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');

            });
            Schema::table('mtef_comments',function (Blueprint $table){
                $table->boolean('addressed')->default('false');
                $table->string('addressed_by')->nullable();

            });
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
