<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCeilingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ceilings', function(Blueprint $table)
		{
			$table->foreign('aggregate_fund_source_id')->references('id')->on('fund_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('gfs_code_id')->references('id')->on('gfs_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ceilings', function(Blueprint $table)
		{
			$table->dropForeign('ceilings_aggregate_fund_source_id_foreign');
			$table->dropForeign('ceilings_gfs_code_id_foreign');
		});
	}

}
