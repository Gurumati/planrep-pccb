<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFacilitiesAddVillagesServiced extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facilities',function (Blueprint $blueprint){
            $blueprint->integer("number_of_villages_served")->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('facilities',function (Blueprint $blueprint){
            $blueprint->dropColumn("number_of_villages_served");
        });
    }
}
