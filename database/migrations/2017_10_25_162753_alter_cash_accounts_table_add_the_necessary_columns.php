<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCashAccountsTableAddTheNecessaryColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cash_accounts', function(Blueprint $blueprint){
            $blueprint->dropColumn('name');
            $blueprint->integer('admin_hierarchy_id')->unsigned();
            $blueprint->boolean('is_sent')->default(false);
            $blueprint->boolean('is_delivered')->default(false);
            $blueprint->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onDelete('restrict')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cash_accounts', function(Blueprint $blueprint){
            $blueprint->dropForeign('cash_accounts_admin_hierarchy_id_foreign');
        });

        Schema::table('cash_accounts', function(Blueprint $blueprint){
            $blueprint->string('name');
            $blueprint->dropColumn('admin_hierarchy_id');
            $blueprint->dropColumn('is_sent');
            $blueprint->dropColumn('is_delivered');
        });
    }
}
