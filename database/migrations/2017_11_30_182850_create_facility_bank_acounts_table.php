<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilityBankAcountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facility_bank_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("facility_id");
            $table->string("account_number");
            $table->string("account_name");
            $table->string("bank");
            $table->boolean("is_primary")->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facility_bank_accounts');
    }
}
