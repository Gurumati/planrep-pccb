<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBudgetSubmissionDefinitionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('budget_submission_definitions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('field_name', 191);
			$table->integer('gfs_code_id')->nullable();
			$table->integer('unit_id')->nullable();
			$table->integer('parent_id')->nullable();
			$table->boolean('is_input')->default(1);
			$table->boolean('has_breakdown')->default(0);
			$table->integer('budget_submission_form_id')->nullable();
			$table->boolean('is_active')->default(1);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('sort_order')->default(1);
			$table->integer('column_number')->nullable();
			$table->string('formula', 191)->nullable();
			$table->string('type', 191)->nullable();
			$table->integer('select_option')->nullable();
			$table->boolean('is_vertical_total')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('budget_submission_definitions');
	}

}
