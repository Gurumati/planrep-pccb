<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsSentAndIsDeliveredToBudgetExportAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_export_accounts',function (Blueprint $table){
            $table->boolean('is_sent')->default(false);
            $table->boolean('is_delivered')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_export_accounts',function (Blueprint $table){
            $table->dropColumn('is_sent');
            $table->dropColumn('is_delivered');
        });
    }
}
