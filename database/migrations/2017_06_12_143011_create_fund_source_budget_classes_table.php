<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFundSourceBudgetClassesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fund_source_budget_classes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('fund_source_id');
			$table->integer('budget_class_id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->unique(['fund_source_id','budget_class_id'], 'fund_source_budget_classes_fund_source_id_budget_class_id_uniqu');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fund_source_budget_classes');
	}

}
