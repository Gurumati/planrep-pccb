<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMtefAnnualTargetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mtef_annual_targets', function(Blueprint $table)
		{
			$table->foreign('long_term_target_id')->references('id')->on('long_term_targets')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('mtef_id')->references('id')->on('mtefs')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mtef_annual_targets', function(Blueprint $table)
		{
			$table->dropForeign('mtef_annual_targets_long_term_target_id_foreign');
			$table->dropForeign('mtef_annual_targets_mtef_id_foreign');
		});
	}

}
