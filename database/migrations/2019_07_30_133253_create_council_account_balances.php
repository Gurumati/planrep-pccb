<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouncilAccountBalances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('council_account_balances', function (Blueprint $blueprint){
           $blueprint->increments('id');
           $blueprint->timestamps();
           $blueprint->string('chart_of_accounts');
           $blueprint->integer('financial_year_id');
           $blueprint->integer('admin_hierarchy_id');
           $blueprint->integer('bank_account_id');
           $blueprint->date('je_date');
           $blueprint->float('debit_amount');
           $blueprint->float('credit_amount');
           $blueprint->foreign('bank_account_id')->references('id')->on('bank_accounts')->onUpdate('cascade')->onDelete('restrict');
           $blueprint->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('cascade')->onDelete('restrict');
           $blueprint->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('council_account_balances');
    }
}
