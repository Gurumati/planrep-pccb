<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterActivityPlanTypeToBudgetType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activities', function (Blueprint $table){
            $table->renameColumn('plan_type','budget_type');
        });
        Schema::table('activity_facility_fund_source_inputs', function (Blueprint $table){
            $table->dropColumn('budget_type');
        });
        Schema::table('activity_facility_fund_source_inputs', function (Blueprint $table){
            $table->renameColumn('plan_type','budget_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities', function (Blueprint $table){
            $table->renameColumn('budget_type','plan_type');
        });
        Schema::table('activity_facility_fund_source_inputs', function (Blueprint $table){
            $table->renameColumn('budget_type','plan_type');
        });
    }
}
