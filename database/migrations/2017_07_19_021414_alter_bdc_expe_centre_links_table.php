<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBdcExpeCentreLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bdc_expenditure_centre_links',function (Blueprint $table){
            $table->integer('activity_category_id')->unsigned()->nullable();
            $table->integer('lga_level_id')->unsigned()->nullable();
            $table->foreign('activity_category_id')->references('id')->on('activity_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('lga_level_id')->references('id')->on('lga_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bdc_expenditure_centre_links',function (Blueprint $table){
            $table->dropColumn('activity_category_id');
            $table->dropColumn('lga_level_id');
            $table->dropForeign('bdc_expenditure_centre_links_activity_category_id_foreign');
            $table->dropForeign('bdc_expenditure_centre_links_lga_level_id_foreign');
        });
    }
}
