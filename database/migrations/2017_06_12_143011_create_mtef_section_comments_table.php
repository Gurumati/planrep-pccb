<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMtefSectionCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mtef_section_comments', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('mtef_section_id');
			$table->integer('mtef_comment_id')->nullable();
			$table->integer('from_section_level')->nullable();
			$table->integer('to_section_level')->nullable();
			$table->text('comments')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('direction')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mtef_section_comments');
	}

}
