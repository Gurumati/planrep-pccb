<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCasAssessmentResultDetailsReplies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cas_assessment_result_detail_replies', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('cas_assessment_result_reply_id');
            $table->integer('cas_assessment_result_detail_id');
            $table->string('comment', 191);
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('cas_assessment_result_reply_id')->references('id')->on('cas_assessment_result_replies')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('cas_assessment_result_detail_id')->references('id')->on('cas_assessment_result_details')->onDelete('restrict')->onUpdate('cascade');

        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_assessment_result_detail_replies', function (Blueprint $table) {
            $table->dropForeign('cas_assessment_result_detail_replies_cas_assessment_result_reply_id_foreign');
            $table->dropForeign('cas_assessment_result_detail_replies_cas_assessment_result_detail_id_foreign');
        });

        Schema::drop('cas_assessment_result_detail_replies');
    }
}
