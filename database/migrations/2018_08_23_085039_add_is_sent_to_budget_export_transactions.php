<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsSentToBudgetExportTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("budget_export_transactions",function (Blueprint $blueprint){
            $blueprint->boolean('is_sent')->default('false');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("budget_export_transactions",function (Blueprint $blueprint){
            $blueprint->dropColumn('is_sent');
        });
    }
}
