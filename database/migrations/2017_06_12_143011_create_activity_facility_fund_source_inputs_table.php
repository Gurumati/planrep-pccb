<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivityFacilityFundSourceInputsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activity_facility_fund_source_inputs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('activity_facility_fund_source_id');
			$table->integer('gfs_code_id');
			$table->integer('unit_id');
			$table->integer('quantity');
			$table->integer('frequency');
			$table->decimal('unit_price', 20);
			$table->boolean('has_breakdown');
			$table->boolean('is_in_kind');
			$table->boolean('is_procurement');
			$table->integer('procurement_type_id')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('update_by')->nullable();
			$table->integer('budget_type')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activity_facility_fund_source_inputs');
	}

}
