<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterBudgetCeilingView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE MATERIALIZED VIEW view_incomplete_ceiling_and_budget
        AS
        WITH C AS (
            SELECT
              adc.financial_year_id,
              adc.budget_type as budget_type,
              adc.admin_hierarchy_id as adminId,
              adc.section_id as secId,
              adc.facility_id as facId,
              bc.id as bid,
              CASE WHEN c.is_aggregate THEN c.aggregate_fund_source_id
                  ELSE
                    f.id
                END as fid,
              MAX(c.name) as name,
              coalesce(SUM(adc.amount),0) as ceiling FROM admin_hierarchy_ceilings AS adc
              JOIN ceilings AS c ON c.id = adc.ceiling_id
              LEFT JOIN gfs_codes as gfs ON gfs.id=c.gfs_code_id
              JOIN gfsCode_fundSources gfcf ON gfcf.gfs_code_id = gfs.id
              JOIN fund_sources f ON f.id = gfcf.fund_source_id
              LEFT JOIN fund_sources as fp on fp.id = c.aggregate_fund_source_id
              JOIN budget_classes as bc on bc.id = c.budget_class_id
              INNER JOIN facilities as fac ON fac.id= adc.facility_id
              INNER JOIN sections AS s ON s.id = adc.section_id
              INNER JOIN section_levels sl on sl.id = s.section_level_id
            WHERE
              (f.can_project = false or fp.can_project = true)
              and  c.is_active=true 
              and adc.budget_type in ('CURRENT')
              and sl.hierarchy_position = 4
            GROUP BY
              adc.financial_year_id,
              budget_type,
              adminId,
              secId,
              facId,
              bid,
              fid
      )
      SELECT
        B.financial_year_id,
        B.budget_type,
        B.adminId,
        B.adminArea,
        B.secId,
        B.section,
        B.facility,
        B.facId,
        B.fund_source,
        B.budget_class,
        coalesce(C.ceiling,0) as ceiling,
        coalesce(B.budget,0) as budget,
        CASE WHEN ceiling = 0 THEN 0
             ELSE ((coalesce(budget,0)/ceiling)*100)::integer
        END as completion
      FROM C
      RIGHT  JOIN
            (
              SELECT
              m.financial_year_id,
              a.budget_type as budget_type,
              adm.name as adminArea,
              adm.id as adminId,
              s.name as section,
              s.id as secId,
              fac.id as facId,
              fac.name as facility,
              bc.id  AS bid,
              bc.name as budget_class,
              fu.id  AS fid,
              fu.name as fund_source,
              SUM(i.unit_price * i.quantity * i.frequency) AS budget
              FROM activity_facility_fund_source_inputs AS i
              INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
              INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
              INNER JOIN facilities AS fac ON fac.id = af.facility_id
              INNER JOIN activities AS a ON a.id = af.activity_id
              INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
              INNER JOIN mtefs AS m ON m.id = ms.mtef_id
              INNER JOIN admin_hierarchies as adm ON adm.id= m.admin_hierarchy_id
              INNER JOIN sections AS s ON s.id = ms.section_id
              INNER JOIN budget_classes AS bc ON bc.id = a.budget_class_id
              INNER JOIN fund_sources as fu on fu.id = aff.fund_source_id
              WHERE i.deleted_at is null
              and fac.is_active = true
              and a.budget_type in ('CURRENT')
              GROUP BY
              m.financial_year_id,
              a.budget_type,
              adminId,
              secId,
              facId,
              bid,
              fid
            ) AS B on
              C.financial_year_id = B.financial_year_id
              and C.budget_type = B.budget_type
              and C.adminId=B.adminId
              and C.secId = B.secId
              and C.facId = B.facId
              and C.bid=B.bid
              and C.fid=B.fid
      
      WHERE (coalesce(C.ceiling,0) >= 0 OR coalesce(B.budget,0) >= 0) and abs(coalesce(C.ceiling,0) - coalesce(B.budget,0)) >= 1000
      
      union  
      
      SELECT * FROM (
      
      WITH C AS(
        SELECT
          fac.name as facility,
          fac.id as facId,
          adc.financial_year_id,
          adc.budget_type,
          adm.name as adminArea,
          s.name as section,
          adm.id as adminId,
          s.id as secId,
          bc.id as bid,
          bc.name as budget_class,
          CASE WHEN c.is_aggregate THEN c.aggregate_fund_source_id
               ELSE
                 f.id
            END as fid,
          CASE WHEN c.is_aggregate THEN fp.name
               ELSE
                 f.name
            END as fund_source,
          MAX(c.name) as name,
          coalesce(SUM(adc.amount),0) as ceiling FROM admin_hierarchy_ceilings AS adc
          JOIN ceilings AS c ON c.id = adc.ceiling_id
          LEFT JOIN gfs_codes as gfs ON gfs.id=c.gfs_code_id
          JOIN gfsCode_fundSources gfcf ON gfcf.gfs_code_id = gfs.id
          JOIN fund_sources f ON f.id = gfcf.fund_source_id
          LEFT JOIN fund_sources as fp on fp.id = c.aggregate_fund_source_id
          LEFT JOIN budget_classes as bc on bc.id = c.budget_class_id
          INNER JOIN admin_hierarchies as adm ON adm.id= adc.admin_hierarchy_id
          INNER JOIN facilities as fac ON fac.id= adc.facility_id
          INNER JOIN sections AS s ON s.id = adc.section_id
          INNER JOIN section_levels sl on sl.id = s.section_level_id
        WHERE
          (f.can_project = false or fp.can_project = true)
          and  c.is_active=true
          and adc.budget_type in ('CURRENT')
          and sl.hierarchy_position = 4
        GROUP BY
          adc.financial_year_id,
          budget_type,
          adminId,
          secId,
          fac.id,
          bid,
          fid,
          fund_source
      )
      
      SELECT
        C.financial_year_id,
        C.budget_type,
        C.adminId,
        C.adminArea,
        C.secId,
        C.section,
        C.facility,
        C.facId,
        C.fund_source,
        C.budget_class,
        coalesce(C.ceiling,0) as ceiling,
        coalesce(B.budget,0) as budget,
        CASE WHEN ceiling = 0 THEN 0
             ELSE ((coalesce(budget,0)/ceiling)*100)::integer
        END as completion
      FROM C
           left  JOIN
        (
           SELECT
           m.financial_year_id,
           a.budget_type,
           adm.name as adminArea,
           adm.id as adminId,
           s.name as section,
           s.id as secId,
           fac.id as facId,
           fac.name as facility,
           bc.id  AS bid,
           bc.name as budget_class,
           fu.id  AS fid,
           fu.name as fund_source,
           SUM(i.unit_price * i.quantity * i.frequency) AS budget
           FROM activity_facility_fund_source_inputs AS i
           INNER JOIN activity_facility_fund_sources AS aff ON aff.id = i.activity_facility_fund_source_id
           INNER JOIN activity_facilities AS af ON af.id = aff.activity_facility_id
           INNER JOIN facilities AS fac ON fac.id = af.facility_id
           INNER JOIN activities AS a ON a.id = af.activity_id
           INNER JOIN mtef_sections AS ms ON ms.id = a.mtef_section_id
           INNER JOIN mtefs AS m ON m.id = ms.mtef_id
           INNER JOIN admin_hierarchies as adm ON adm.id= m.admin_hierarchy_id
           INNER JOIN sections AS s ON s.id = ms.section_id
           INNER JOIN budget_classes AS bc ON bc.id = a.budget_class_id
           INNER JOIN fund_sources as fu on fu.id = aff.fund_source_id
           WHERE i.deleted_at is null
           and fac.is_active = true
           and a.budget_type in ('CURRENT')
           GROUP BY
           m.financial_year_id,
           a.budget_type,
           adminId,
           secId,
           facId,
           bid,
           fid
        )
        AS B on
               C.budget_type = B.budget_type
               and C.adminId=B.adminId
               and C.secId = B.secId
               and C.facId = B.facId
               and C.bid=B.bid
               and C.fid=B.fid
      WHERE (coalesce(C.ceiling,0) >= 0 OR coalesce(B.budget,0) >= 0) and abs(coalesce(C.ceiling,0) - coalesce(B.budget,0)) >= 1000) L  order by adminarea
        WITH DATA;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       DB::statement("DROP MATERIALIZED VIEW view_incomplete_ceiling_and_budget");
    }
}
