<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignkeyToAdminHierarchyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_hierarchy', function(Blueprint $table){
            $table->integer('geo_location_id');
            $table->foreign('geo_location_id')->references('id')->on('geographical_locations')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_hierarchy', function (Blueprint $blueprint){
            $blueprint->dropColumn('geo_location_id');
         });
    }
}
