<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMtefSectionCommentsAddAddressComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mtef_section_item_comments', function($table)
        {
            $table->text('address_comments')->nullable();
        });
        Schema::table('mtef_section_comments', function($table)
        {
            $table->text('address_comments')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtef_section_item_comments', function($table)
        {
            $table->dropColumn('address_comments');
        });
        Schema::table('mtef_section_comments', function($table)
        {
            $table->dropColumn('address_comments');
        });
    }
}
