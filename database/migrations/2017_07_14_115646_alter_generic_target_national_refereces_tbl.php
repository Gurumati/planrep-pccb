<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGenericTargetNationalReferecesTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('generic_target_national_references',function (Blueprint $blueprint){
            $blueprint->unique(['generic_target_id','national_reference_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('generic_target_national_references',function (Blueprint $blueprint){
            $blueprint->dropUnique('generic_target_national_references_generic_target_id_national_r');
        });
    }
}
