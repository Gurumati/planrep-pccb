<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFundAllocatedTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_allocated_transactions',function (Blueprint $table){
            $table->foreign('data_source_id')->references('id')->on('data_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('import_method_id')->references('id')->on('import_methods')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_allocated_transactions',function (Blueprint $table){
            $table->dropForeign('fund_allocated_transactions_data_source_id_foreign');
            $table->dropForeign('fund_allocated_transactions_import_method_id_foreign');
        });
    }
}
