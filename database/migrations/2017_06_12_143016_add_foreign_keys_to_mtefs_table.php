<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMtefsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mtefs', function(Blueprint $table)
		{
			$table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('decision_level_id')->references('id')->on('decision_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mtefs', function(Blueprint $table)
		{
			$table->dropForeign('mtefs_admin_hierarchy_id_foreign');
			$table->dropForeign('mtefs_decision_level_id_foreign');
			$table->dropForeign('mtefs_financial_year_id_foreign');
		});
	}

}
