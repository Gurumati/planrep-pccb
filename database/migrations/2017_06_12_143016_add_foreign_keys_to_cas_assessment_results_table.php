<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasAssessmentResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cas_assessment_results', function(Blueprint $table)
		{
			$table->foreign('admin_hierarchy_level_id')->references('id')->on('admin_hierarchy_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('cas_assessment_category_version_id', 'cas_assessment_results_cas_assessment_category_version_id_forei')->references('id')->on('cas_assessment_category_versions')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('cas_assessment_round_id')->references('id')->on('cas_assessment_rounds')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('cas_assessment_state_id')->references('id')->on('cas_assessment_states')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('mtef_id')->references('id')->on('mtefs')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('period_id')->references('id')->on('periods')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cas_assessment_results', function(Blueprint $table)
		{
			$table->dropForeign('cas_assessment_results_admin_hierarchy_level_id_foreign');
			$table->dropForeign('cas_assessment_results_cas_assessment_category_version_id_forei');
			$table->dropForeign('cas_assessment_results_cas_assessment_round_id_foreign');
			$table->dropForeign('cas_assessment_results_cas_assessment_state_id_foreign');
			$table->dropForeign('cas_assessment_results_mtef_id_foreign');
			$table->dropForeign('cas_assessment_results_period_id_foreign');
			$table->dropForeign('cas_assessment_results_user_id_foreign');
		});
	}

}
