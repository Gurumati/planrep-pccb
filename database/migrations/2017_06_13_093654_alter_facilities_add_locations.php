<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFacilitiesAddLocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facilities',function (Blueprint $blueprint){
            $blueprint->decimal('longitude',10,7)->nullable();
            $blueprint->decimal('latitude',10,7)->nullable();
            $blueprint->string('gnl')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('facilities',function (Blueprint $blueprint){
            $blueprint->dropColumn('longitude');
            $blueprint->dropColumn('latitude');
            $blueprint->dropColumn('gnl');
        });
    }
}
