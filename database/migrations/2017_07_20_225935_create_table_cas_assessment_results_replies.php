<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCasAssessmentResultsReplies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('cas_assessment_result_replies', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('cas_assessment_result_id');
            $table->string('comment', 191);
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('admin_hierarchy_level_id');
            $table->foreign('cas_assessment_result_id')->references('id')->on('cas_assessment_results')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('admin_hierarchy_level_id')->references('id')->on('admin_hierarchy_levels')->onDelete('restrict')->onUpdate('cascade');

        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_assessment_result_replies', function (Blueprint $table) {
            $table->dropForeign('cas_assessment_result_replies_cas_assessment_result_id_foreign');
            $table->dropForeign('cas_assessment_result_replies_admin_hierarchy_level_id_foreign');
        });

        Schema::drop('cas_assessment_result_replies');
    }

}
