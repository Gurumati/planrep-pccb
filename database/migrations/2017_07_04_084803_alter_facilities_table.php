<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('facilities',function (Blueprint $blueprint){
            $blueprint->integer('catchment_population')->default(0);
            $blueprint->integer('facility_ownership_id')->unsigned()->nullable();
            $blueprint->integer('population_within_area')->default(0);
            $blueprint->integer('nearest_similar_facility_id')->unsigned()->nullable();
            $blueprint->integer('nearest_similar_facility_distance')->default(0);
            $blueprint->integer('furthest_ward_served_id')->unsigned()->nullable();
            $blueprint->integer('furthest_ward_served_distance')->default(0);
            $blueprint->integer('furthest_village_served_id')->unsigned()->nullable();
            $blueprint->integer('furthest_village_served_distance')->default(0);
            $blueprint->boolean('has_title_deed')->default(true);
            $blueprint->integer('physical_state_id')->unsigned()->nullable();
            $blueprint->integer('star_rating_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('facilities',function (Blueprint $blueprint){
            $blueprint->dropColumn('catchment_population');
            $blueprint->dropColumn('facility_ownership_id');
            $blueprint->dropColumn('population_within_area');
            $blueprint->dropColumn('nearest_similar_facility_id');
            $blueprint->dropColumn('nearest_similar_facility_distance');
            $blueprint->dropColumn('furthest_ward_served_id');
            $blueprint->dropColumn('furthest_ward_served_distance');
            $blueprint->dropColumn('furthest_village_served_id');
            $blueprint->dropColumn('furthest_village_served_distance');
            $blueprint->dropColumn('has_title_deed');
            $blueprint->dropColumn('physical_state_id');
            $blueprint->dropColumn('star_rating_id');
        });
    }
}
