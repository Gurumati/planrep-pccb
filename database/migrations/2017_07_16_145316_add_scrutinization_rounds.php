<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddScrutinizationRounds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       DB::transaction(function (){
           Schema::create('scrutinization_rounds',function (Blueprint $table){
               $table->increments('id');
               $table->integer('round');
               $table->integer('status');
               $table->integer('start_decision_level_id');
               $table->integer('mtef_section_id');
               $table->foreign('mtef_section_id')->references('id')->on('mtef_sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
               $table->foreign('start_decision_level_id')->references('id')->on('decision_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
           });
           Schema::table('scrutinizations',function (Blueprint $table){
               $table->dropColumn('mtef_section_id');
               $table->dropColumn('comments');
               $table->dropColumn('financial_year_id');
               $table->integer('scrutinization_round_id');
               $table->foreign('scrutinization_round_id')->references('id')->on('scrutinization_rounds')->onUpdate('CASCADE')->onDelete('RESTRICT');

           });
       });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       DB::transaction(function (){
           Schema::table('scrutinizations',function (Blueprint $table){
               $table->integer('mtef_section_id');
               $table->text('comments');
               $table->integer('financial_year_id');
               $table->dropColumn('scrutinization_round_id');

           });
           Schema::drop('scrutinization_rounds');
       });

    }
}
