<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateRevenueProjectionView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS revenue_projection_view');
        DB::statement('CREATE VIEW  revenue_projection_view AS 
                        SELECT 
                        ahc.id as admin_hierarchy_ceiling_id,
                        a.id as admin_hierarchy_id,
                        a.name as admin_hierarchy,
                        a.code as admin_hierarchy_code,
                        fy.id as financial_year_id,
                        fy.name as financial_year,
                        s.id as section_id,
                        s.name as section_name,
                        s.code as section_code,
                        fs.id as fund_source_id,
                        fs.name as fund_source,
                        gfs.id as gfs_code_id,
                        gfs.name as gfs_name,
                        gfs.code as gfs_code, 
                        ahc.budget_type,
                        sum(ahc.amount) as amount
                        FROM 
                        admin_hierarchy_ceilings ahc
                        inner join ceilings c on ahc.ceiling_id = c.id
                        inner join gfs_codes gfs on c.gfs_code_id = gfs.id
                        inner join gfs_code_sections gcs on gcs.gfs_code_id = gfs.id
                        JOIN gfsCode_fundSources gfcf ON gfcf.gfs_code_id = gfs.id
                        JOIN fund_sources fs ON fs.id = gfcf.fund_source_id
                        inner join admin_hierarchies a on ahc.admin_hierarchy_id = a.id
                        inner join financial_years fy on ahc.financial_year_id = fy.id
                        left join financial_years fy2 on fy2.previous = fy.id
                        left join financial_years fy3 on fy3.previous = fy2.id
                        inner join sections s on gcs.section_id = s.id
                        where
                        fs.can_project = true and ahc.amount > 0 and s.section_level_id = 4
                        group by
                        ahc.id, a.id, fy.id, s.id, fs.id, gfs.id, ahc.budget_type');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS revenue_projection_view');
    }
}
