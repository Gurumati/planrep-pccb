<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableBcdExpenditureCentreSections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('bdc_expenditure_centre_sections',function (Blueprint $table){
           $table->increments('id');
           $table->integer('bdc_expenditure_centre_id')->unsigned();
           $table->integer('section_id')->unsigned();
           $table->foreign('bdc_expenditure_centre_id')->references('id')->on('bdc_expenditure_centres')->onUpdate('CASCADE')->onDelete('RESTRICT');
           $table->foreign('section_id')->references('id')->on('sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
