<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFinancialYearVersionsTableAddIsCurrent extends Migration
{
    public function up()
    {
        Schema::table("financial_year_versions",function (Blueprint $blueprint){
            $blueprint->boolean('is_current')->default(false);
        });
    }

    public function down()
    {
        Schema::table("financial_year_versions",function (Blueprint $blueprint){
            $blueprint->boolean('is_current')->default(false);
        });
    }
}
