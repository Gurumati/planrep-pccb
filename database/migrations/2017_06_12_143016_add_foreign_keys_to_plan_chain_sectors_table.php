<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlanChainSectorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plan_chain_sectors', function(Blueprint $table)
		{
			$table->foreign('plan_chain_id')->references('id')->on('plan_chains')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('sector_id')->references('id')->on('sectors')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plan_chain_sectors', function(Blueprint $table)
		{
			$table->dropForeign('plan_chain_sectors_plan_chain_id_foreign');
			$table->dropForeign('plan_chain_sectors_sector_id_foreign');
		});
	}

}
