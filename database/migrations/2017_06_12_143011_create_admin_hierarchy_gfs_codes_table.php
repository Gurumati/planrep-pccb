<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminHierarchyGfsCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_hierarchy_gfs_codes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('admin_hierarchy_id');
			$table->integer('gfs_code_id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->unique(['gfs_code_id','admin_hierarchy_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin_hierarchy_gfs_codes');
	}

}
