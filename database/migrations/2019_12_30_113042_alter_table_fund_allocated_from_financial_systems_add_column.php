<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableFundAllocatedFromFinancialSystemsAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_allocated_from_financial_systems', function (Blueprint $table){
            $table->integer('uid')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_allocated_from_financial_systems', function (Blueprint $table){
            $table->dropColumn('uid');
        });
    }
}
