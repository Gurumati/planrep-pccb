<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityImplementationsTableAddFacilityId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("activity_implementations",function (Blueprint $blueprint){
            $blueprint->integer("facility_id")->nullable()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("activity_implementations",function (Blueprint $blueprint){
            $blueprint->dropColumn("facility_id");
        });
    }
}
