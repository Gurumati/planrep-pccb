<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBodinterventionsNullFundSource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("bod_interventions",function (Blueprint $blueprint){
            $blueprint->integer('fund_source_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("bod_interventions",function (Blueprint $blueprint){
            $blueprint->integer('fund_source_id')->nullable(false)->change();
        });
    }
}
