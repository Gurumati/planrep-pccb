<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCasAssessmentCategoryVersionStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cas_assessment_category_version_states', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cas_assessment_category_version_id');
			$table->integer('cas_assessment_state_id');
			$table->integer('min_value');
			$table->integer('max_value');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cas_assessment_category_version_states');
	}

}
