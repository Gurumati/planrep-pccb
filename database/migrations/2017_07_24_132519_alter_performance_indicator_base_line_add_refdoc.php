<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPerformanceIndicatorBaseLineAddRefdoc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('performance_indicator_baseline_values',function (Blueprint $table){
            $table->dropColumn('start_financial_year_id');
            $table->dropColumn('end_financial_year_id');
            $table->integer('reference_document_id')->unsigned()->nullable();
            $table->foreign('reference_document_id')->references('id')->on('reference_documents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('performance_indicator_baseline_values',function (Blueprint $table){
            $table->integer('start_financial_year_id');
            $table->integer('end_financial_year_id');
            $table->dropColumn('end_financial_year_id');
            $table->foreign('start_financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('end_financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }
}
