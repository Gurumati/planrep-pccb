<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRevenueExportAccountIdToCompanyGlAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("company_gl_accounts",function (Blueprint $blueprint){
            $blueprint->integer('revenue_export_account_id')->nullable();
            $blueprint->integer('budget_export_account_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("company_gl_accounts",function (Blueprint $blueprint){
            $blueprint->dropColumn('revenue_export_account_id');
            $blueprint->integer('budget_export_account_id')->change();
        });
    }
}
