<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivityFundersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activity_funders', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('activity_id');
			$table->integer('funder_id');
			$table->decimal('amount', 20);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('sort_order')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('update_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activity_funders');
	}

}
