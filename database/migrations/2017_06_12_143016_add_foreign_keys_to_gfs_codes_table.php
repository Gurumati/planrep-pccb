<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToGfsCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('gfs_codes', function(Blueprint $table)
		{
			$table->foreign('gfs_code_sub_category_id')->references('id')->on('gfs_code_sub_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('gfs_codes', function(Blueprint $table)
		{
			$table->dropForeign('gfs_codes_gfs_code_sub_category_id_foreign');
		});
	}

}
