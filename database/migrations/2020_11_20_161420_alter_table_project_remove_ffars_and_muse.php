<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProjectRemoveFfarsAndMuse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn(['exported_to_ffars', 'ffars_response','is_delivered_to_ffars']);
        });

        Schema::table('projects', function (Blueprint $table) {
            $table->boolean('exported_to_muse')->after('update_by')->nullable()->default(false);
            $table->string('muse_response', 150)->after('exported_to_muse')->nullable();
            $table->boolean('is_delivered_to_muse')->after('muse_response')->nullable()->default(false);;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('exported_to_muse');
        Schema::drop('muse_response');
        Schema::drop('is_delivered_to_muse');

        Schema::table('projects', function (Blueprint $table) {
            $table->boolean('exported_to_ffars')->nullable();
            $table->string('ffars_response')->nullable();
            $table->boolean('is_delivered_to_ffars')->nullable();
        });
        
    }
}
