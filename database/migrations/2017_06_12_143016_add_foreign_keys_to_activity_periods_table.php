<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActivityPeriodsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('activity_periods', function(Blueprint $table)
		{
			$table->foreign('activity_id')->references('id')->on('activities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('period_id')->references('id')->on('periods')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('activity_periods', function(Blueprint $table)
		{
			$table->dropForeign('activity_periods_activity_id_foreign');
			$table->dropForeign('activity_periods_period_id_foreign');
		});
	}

}
