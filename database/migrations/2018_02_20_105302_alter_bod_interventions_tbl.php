<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBodInterventionsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("bod_interventions",function (Blueprint $blueprint){
            $blueprint->dropColumn("intervention_category_id");
            $blueprint->integer("intervention_id")->unsigned()->nullable();
            $blueprint->foreign('intervention_id')->references('id')->on('interventions')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("bod_interventions",function (Blueprint $blueprint){
            $blueprint->dropForeign("bod_interventions_intervention_id_foreign");
            $blueprint->dropColumn("intervention_id");
            $blueprint->integer("intervention_category_id")->unsigned()->nullable();
            $blueprint->foreign('intervention_category_id')->references('id')->on('intervention_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }
}
