<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsMultipleToBudgetSubmissionSubFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_submission_sub_forms',function (Blueprint $blueprint){
            $blueprint->boolean('is_multiple')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_submission_sub_forms',function (Blueprint $blueprint){
            $blueprint->dropColumn('is_multiple');
        });
    }
}
