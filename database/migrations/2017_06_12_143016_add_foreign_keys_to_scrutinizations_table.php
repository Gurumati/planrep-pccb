<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToScrutinizationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('scrutinizations', function(Blueprint $table)
		{
			$table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('decision_level_id')->references('id')->on('decision_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('scrutinized_admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('scrutinized_section_id')->references('id')->on('sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('section_id')->references('id')->on('sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('user_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('scrutinizations', function(Blueprint $table)
		{
			$table->dropForeign('scrutinizations_admin_hierarchy_id_foreign');
			$table->dropForeign('scrutinizations_decision_level_id_foreign');
			$table->dropForeign('scrutinizations_financial_year_id_foreign');
			$table->dropForeign('scrutinizations_scrutinized_admin_hierarchy_id_foreign');
			$table->dropForeign('scrutinizations_scrutinized_section_id_foreign');
			$table->dropForeign('scrutinizations_section_id_foreign');
			$table->dropForeign('scrutinizations_user_id_foreign');
		});
	}

}
