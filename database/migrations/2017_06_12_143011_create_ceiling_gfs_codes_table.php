<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCeilingGfsCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ceiling_gfs_codes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('ceiling_id');
			$table->integer('gfs_code_id');
			$table->boolean('is_inclusive');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->unique(['ceiling_id','gfs_code_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ceiling_gfs_codes');
	}

}
