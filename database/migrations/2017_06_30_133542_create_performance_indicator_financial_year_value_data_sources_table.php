<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformanceIndicatorFinancialYearValueDataSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performance_indicator_financial_year_value_data_sources', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('performance_indicator_financial_year_value_id')->unsigned();
            $table->integer('data_source_id')->unsigned();
            $table->unique(['performance_indicator_financial_year_value_id','data_source_id']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performance_indicator_financial_year_value_data_sources');
    }
}
