<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetExportAccountsRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_export_accounts',function (Blueprint $table){
            $table->decimal('fund_allocated_amount',20,2)->default(0);
            $table->decimal('expenditure_amount',20,2)->default(0);
            $table->boolean('has_issues')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_export_accounts',function (Blueprint $table){
            $table->dropColumn('fund_allocated_amount');
            $table->dropColumn('expenditure_amount');
            $table->dropColumn('has_issues');
        });
    }
}
