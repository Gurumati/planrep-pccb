<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBdcExpenditureCentresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bdc_expenditure_centres', function(Blueprint $table)
		{
			$table->foreign('expenditure_centre_group_id', 'bdc_expenditure_centres_expenditure_group_id_foreign')->references('id')->on('bdc_expenditure_centre_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('task_nature_id')->references('id')->on('bdc_task_natures')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bdc_expenditure_centres', function(Blueprint $table)
		{
			$table->dropForeign('bdc_expenditure_centres_expenditure_group_id_foreign');
			$table->dropForeign('bdc_expenditure_centres_task_nature_id_foreign');
		});
	}

}
