<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPayloadToFacilityBudgetExportResponses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("facility_budget_export_responses",function (Blueprint $blueprint){
            $blueprint->text('payload')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("facility_budget_export_responses",function (Blueprint $blueprint){
            $blueprint->dropColumn('payload');
        });
    }
}
