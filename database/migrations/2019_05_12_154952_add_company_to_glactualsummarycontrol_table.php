<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyToGlactualsummarycontrolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("gl_actual_summary_control_ids",function (Blueprint $blueprint){
            $blueprint->string("company")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("gl_actual_summary_control_ids",function (Blueprint $blueprint){
            $blueprint->dropColumn("company");
        });
    }
}
