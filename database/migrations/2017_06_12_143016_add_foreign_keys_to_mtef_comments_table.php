<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMtefCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mtef_comments', function(Blueprint $table)
		{
			$table->foreign('to_decision_level', 'mtef_comments_decision_level_id_foreign')->references('id')->on('decision_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('from_decision_level')->references('id')->on('decision_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('mtef_id')->references('id')->on('mtefs')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mtef_comments', function(Blueprint $table)
		{
			$table->dropForeign('mtef_comments_decision_level_id_foreign');
			$table->dropForeign('mtef_comments_from_decision_level_foreign');
			$table->dropForeign('mtef_comments_mtef_id_foreign');
		});
	}

}
