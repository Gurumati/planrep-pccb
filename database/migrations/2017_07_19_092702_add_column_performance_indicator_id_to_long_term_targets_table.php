<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPerformanceIndicatorIdToLongTermTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('long_term_targets', function (Blueprint $table) {
            $table->integer('performance_indicator_id')->nullable();
            $table->foreign('performance_indicator_id')->references('id')->on('performance_indicators')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     
    public function down()
    {
        Schema::table('long_term_targets', function (Blueprint $table) {
            $table->dropForeign('long_term_targets_performance_indicator_id_foreign');
            $table->dropColumn('performance_indicator_id');
        });
    }
}
  