<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBudgetSubmissionLineValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('budget_submission_line_values', function(Blueprint $table)
		{
			$table->foreign('activity_facility_fund_source_input_id', 'budget_submission_line_values_activity_facility_fund_source_inp')->references('id')->on('activity_facility_fund_source_inputs')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('budget_submission_line_id')->references('id')->on('budget_submission_lines')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('budget_submission_line_values', function(Blueprint $table)
		{
			$table->dropForeign('budget_submission_line_values_activity_facility_fund_source_inp');
			$table->dropForeign('budget_submission_line_values_budget_submission_line_id_foreign');
		});
	}

}
