<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetImportItemsTableMakeBudgetExportAccountIdNullableAndAddBudgetControlAccountId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_import_items',function (Blueprint $blueprint){
           $blueprint->integer('budget_export_account_id')->unsigned()->nullable()->change();
           $blueprint->integer('budget_control_account_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_import_items',function (Blueprint $blueprint){
            $blueprint->integer('budget_export_account_id')->unsigned()->change();
            $blueprint->dropColumn('budget_control_account_id');
        });
    }
}
