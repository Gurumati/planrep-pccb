<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetImportsTableMakeImportMethodNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_imports', function (Blueprint $blueprint){
            $blueprint->dropColumn('import_method_id');
        });

        Schema::table('budget_imports', function (Blueprint $blueprint){
                $blueprint->integer('import_method_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
