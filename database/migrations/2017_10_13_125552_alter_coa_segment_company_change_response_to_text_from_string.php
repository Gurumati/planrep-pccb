<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCoaSegmentCompanyChangeResponseToTextFromString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coa_segment_company', function (Blueprint $blueprint){
           $blueprint->text('response')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coa_segment_company', function (Blueprint $blueprint){
            $blueprint->string('response')->change();
        });
    }
}
