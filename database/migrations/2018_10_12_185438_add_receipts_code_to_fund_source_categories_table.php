<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReceiptsCodeToFundSourceCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("fund_source_categories",function (Blueprint $blueprint){
            $blueprint->string('receipt_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("fund_source_categories",function (Blueprint $blueprint){
            $blueprint->dropColumn('receipt_code');
        });
    }
}
