<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivedFundRawTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('received_fund_raw_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date')->nullable();
            $table->decimal('amount',20,2)->default(0);
            $table->decimal('base_amount',20,2)->default(0);
            $table->string('reference_code');
            $table->string('gl_account')->nullable();
            $table->decimal('base_bank_fee_amount',20,2)->default(0);
            $table->decimal('base_bank_fee_tax_amount',20,2)->default(0);
            $table->string('rate_type_code')->nullable();
            $table->decimal('fee_tax_amount',20,2)->default(0);
            $table->boolean('voided')->default(false);
            $table->string('cash_desk_id')->nullable();
            $table->string('gain_loss_type')->nullable();
            $table->string('reference_number')->nullable();
            $table->boolean('reverse_gain_loss')->default(false);
            $table->date('revaluation_date')->nullable();
            $table->string('module')->nullable();
            $table->decimal('revaluation_balance',20,2)->default(0);
            $table->decimal('doc_revaluation_balance')->default(0);
            $table->decimal('head_num',20,2)->default(0);
            $table->decimal('rpt1_revaluation_balance',20,2)->default(0);
            $table->decimal('rpt2_revaluation_balance',20,2)->default(0);
            $table->decimal('rpt3_revaluation_balance',20,2)->default(0);
            $table->decimal('base_total_amount',20,2)->default(0);
            $table->decimal('bank_total',20,2)->default(0);
            $table->string('legal_number')->nullable();
            $table->decimal('base_cleared_amount',20,2)->default(0);
            $table->decimal('bank_cleared_amount',20,2)->default(0);
            $table->string('transaction_document_type')->nullable();
            $table->string('cash_rec_currency')->nullable();
            $table->string('cr_rate_group_code')->nullable();
            $table->string('currency_id')->nullable();
            $table->decimal('cr_trans_cleared',20,2)->default(0);
            $table->decimal('cust_amount',20,2)->default(0);
            $table->decimal('bank_rec_gain_loss',20,2)->default(0);
            $table->decimal('rpt1_bank_rec_gain_loss',20,2)->default(0);
            $table->string('description')->nullable();
            $table->boolean('is_filtered')->default(false);
            $table->decimal('rpt2_bank_rec_gain_loss',20,2)->default(0);
            $table->decimal('rpt3_bank_rec_gain_loss',20,2)->default(0);
            $table->string('tax_id')->nullable();
            $table->date('reconciled_date')->nullable();
            $table->decimal('doc_bank_rec_gain_loss',20,2)->default(0);
            $table->date('internal_date')->nullable();
            $table->boolean('disp_amt_reverse')->default(false);
            $table->decimal('rpt_tranl_amt',20,2)->default(0);
            $table->decimal('rpt_tran_amt',20,2)->default(0);
            $table->decimal('rpt_cleared_amt',20,2)->default(0);
            $table->boolean('disable_bank_amt')->default(false);
            $table->string('legal_number_message')->nullable();
            $table->string('legal_num_style')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('received_fund_raw_transactions');
    }
}
