<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToGuidelinesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('guidelines', function(Blueprint $table)
		{
			$table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('guidelines', function(Blueprint $table)
		{
			$table->dropForeign('guidelines_financial_year_id_foreign');
		});
	}

}
