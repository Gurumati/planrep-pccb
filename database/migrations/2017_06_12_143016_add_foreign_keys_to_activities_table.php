<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActivitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('activities', function(Blueprint $table)
		{
			$table->foreign('activity_category_id')->references('id')->on('activity_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('budget_class_id')->references('id')->on('budget_classes')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('facility_id')->references('id')->on('facilities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('intervention_id')->references('id')->on('interventions')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('mtef_annual_target_id')->references('id')->on('mtef_annual_targets')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('mtef_section_id')->references('id')->on('mtef_sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('project_id')->references('id')->on('projects')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('responsible_person_id')->references('id')->on('responsible_persons')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('sector_problem_id')->references('id')->on('sector_problems')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('activities', function(Blueprint $table)
		{
			$table->dropForeign('activities_activity_category_id_foreign');
			$table->dropForeign('activities_budget_class_id_foreign');
			$table->dropForeign('activities_facility_id_foreign');
			$table->dropForeign('activities_intervention_id_foreign');
			$table->dropForeign('activities_mtef_annual_target_id_foreign');
			$table->dropForeign('activities_mtef_section_id_foreign');
			$table->dropForeign('activities_project_id_foreign');
			$table->dropForeign('activities_responsible_person_id_foreign');
			$table->dropForeign('activities_sector_problem_id_foreign');
		});
	}

}
