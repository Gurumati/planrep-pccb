<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResponsiblePersonsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('responsible_persons', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 191);
			$table->string('mobile', 191)->unique();
			$table->string('email', 191);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('sort_order')->nullable();
			$table->boolean('is_active');
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('update_by')->nullable();
			$table->integer('section_id');
			$table->integer('admin_hierarchy_id');
			$table->string('title', 191)->nullable();
			$table->string('cheque_number', 191)->nullable()->unique();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('responsible_persons');
	}

}
