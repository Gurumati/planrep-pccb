<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCasAssessmentSubCriteriaReportSetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cas_assessment_sub_criteria_report_sets', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cas_assessment_sub_criteria_option_id');
			$table->integer('item_to_access_id')->nullable();
			$table->integer('cas_plan_content_id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->string('name', 191)->nullable();
			$table->string('report_path', 191)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cas_assessment_sub_criteria_report_sets');
	}

}
