<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTrigRefreshVwFinancialYears extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(
            '
            CREATE OR REPLACE FUNCTION public.trig_refresh_vw_financial_years()
            RETURNS trigger
            LANGUAGE plpgsql
            AS $function$
            BEGIN
               REFRESH MATERIALIZED VIEW vw_financial_years;
               RETURN NULL;
            END;
            $function$;
            '
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
