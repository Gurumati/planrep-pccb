<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToProjectFundersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('project_funders', function(Blueprint $table)
		{
			$table->foreign('funder_id')->references('id')->on('funders')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('project_id')->references('id')->on('projects')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('project_funders', function(Blueprint $table)
		{
			$table->dropForeign('project_funders_funder_id_foreign');
			$table->dropForeign('project_funders_project_id_foreign');
		});
	}

}
