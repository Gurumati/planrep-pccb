<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentToCasPlanTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cas_plans', function (Blueprint $table) {
            $table->boolean('is_facility')->default(false);
            $table->integer('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')->on('cas_plan_tables')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_plans', function (Blueprint $table) {
            $table->dropColumn('is_facility');
            $table->dropColumn('parent_id');
        });
    }
}
