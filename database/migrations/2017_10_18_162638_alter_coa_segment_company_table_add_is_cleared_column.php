<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCoaSegmentCompanyTableAddIsClearedColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coa_segment_company', function(Blueprint $blueprint){
            $blueprint->boolean('is_cleared')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coa_segment_company', function(Blueprint $blueprint){
            $blueprint->dropColumn('is_cleared');
        });
    }
}
