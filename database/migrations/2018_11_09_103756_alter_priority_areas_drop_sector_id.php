<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPriorityAreasDropSectorId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("priority_areas",function (Blueprint $blueprint){
            $blueprint->dropColumn("sector_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("priority_areas",function (Blueprint $blueprint){
            $blueprint->integer("sector_id")->unsigned()->nullable();
        });
    }
}
