<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFfarsActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("ffars_activities",function (Blueprint $blueprint){
            $blueprint->string("admin_hierarchy")->nullable();
            $blueprint->string("facility_type")->nullable();
            $blueprint->string("facility")->nullable();
            $blueprint->string("project")->nullable();
            $blueprint->string("sub_budget_class")->nullable();
            $blueprint->string("financial_year_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table("ffars_activities",function (Blueprint $blueprint){
            $blueprint->dropColumn("admin_hierarchy");
            $blueprint->dropColumn("facility_type");
            $blueprint->dropColumn("facility");
            $blueprint->dropColumn("project");
            $blueprint->dropColumn("sub_budget_class");
            $blueprint->dropColumn("financial_year_id");
        });
    }
}
