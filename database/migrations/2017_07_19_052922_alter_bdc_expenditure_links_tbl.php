<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBdcExpenditureLinksTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bdc_expenditure_centre_links',function (Blueprint $table){
            $table->dropUnique('bdc_expenditure_centre_gfs_codes_gfs_code_id_expenditure_centre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bdc_expenditure_centre_links',function (Blueprint $table){
            $table->unique(['gfs_code_id','expenditure_centre_id']);
        });
    }
}
