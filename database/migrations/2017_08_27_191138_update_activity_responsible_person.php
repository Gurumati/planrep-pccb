<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class UpdateActivityResponsiblePerson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function (){
            Schema::table('activities',function (Blueprint $table){
                $table->dropColumn('responsible_person_id');
                
            });
            Schema::table('activities',function (Blueprint $table){
                $table->integer('responsible_person_id')->nullable();
                $table->foreign('responsible_person_id')->references('id')->on('users')->onUpdate('CASCADE')->onDelete('RESTRICT');
            });
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
