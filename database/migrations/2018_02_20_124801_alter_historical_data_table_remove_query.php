<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHistoricalDataTableRemoveQuery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("historical_data_tables",function (Blueprint $blueprint){
            $blueprint->dropColumn("query_string");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("historical_data_tables",function (Blueprint $blueprint){
            $blueprint->string("query_string")->nullable();
        });
    }
}
