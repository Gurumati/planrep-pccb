<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasPlanTableColumnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cas_plan_table_columns', function(Blueprint $table)
		{
			$table->foreign('cas_group_column_id')->references('id')->on('cas_group_columns')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('cas_plan_table_select_option_id')->references('id')->on('cas_plan_table_select_options')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cas_plan_table_columns', function(Blueprint $table)
		{
			$table->dropForeign('cas_plan_table_columns_cas_group_column_id_foreign');
			$table->dropForeign('cas_plan_table_columns_cas_plan_table_select_option_id_foreign');
		});
	}

}
