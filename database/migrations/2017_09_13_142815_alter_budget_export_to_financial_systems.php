<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetExportToFinancialSystems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_export_to_financial_systems',function (Blueprint $table){
            $table->dropUnique('budget_export_to_financial_systems_queue_name_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_export_to_financial_systems',function (Blueprint $table){
            $table->string('queue_name')->unique();
        });
    }
}
