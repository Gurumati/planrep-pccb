<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMtefSectionAllDecisionLevelAndLock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('mtef_sections',function (Blueprint $table){
           $table->integer('decision_level_id')->nullable();
           $table->boolean('is_locked')->default('false');
           $table->foreign('decision_level_id')->references('id')->on('decision_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');

       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtef_sections',function (Blueprint $table){
            $table->dropColumn('decision_level_id');
            $table->dropColumn('is_locked')->default('false');

        });
    }
}
