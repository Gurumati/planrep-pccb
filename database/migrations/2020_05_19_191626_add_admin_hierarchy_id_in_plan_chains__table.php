<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminHierarchyIdInPlanChainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_chains', function(Blueprint $table)
        {
            $table->integer('admin_hierarchy_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_chains',function (Blueprint $table){
            $table->dropColumn('admin_hierarchy_id');
        });
    }
}
