<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateScrutinizationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('scrutinizations', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('admin_hierarchy_id');
			$table->integer('section_id');
			$table->integer('decision_level_id');
			$table->integer('user_id');
			$table->integer('status');
			$table->text('comments')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('round')->nullable();
			$table->integer('financial_year_id')->nullable();
			$table->integer('scrutinized_admin_hierarchy_id')->nullable();
			$table->integer('scrutinized_section_id')->nullable();
			$table->integer('recommendation')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('scrutinizations');
	}

}
