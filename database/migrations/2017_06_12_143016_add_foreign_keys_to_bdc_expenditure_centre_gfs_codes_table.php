<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBdcExpenditureCentreGfsCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bdc_expenditure_centre_gfs_codes', function(Blueprint $table)
		{
			$table->foreign('expenditure_centre_id')->references('id')->on('bdc_expenditure_centres')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('gfs_code_id')->references('id')->on('gfs_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bdc_expenditure_centre_gfs_codes', function(Blueprint $table)
		{
			$table->dropForeign('bdc_expenditure_centre_gfs_codes_expenditure_centre_id_foreign');
			$table->dropForeign('bdc_expenditure_centre_gfs_codes_gfs_code_id_foreign');
		});
	}

}
