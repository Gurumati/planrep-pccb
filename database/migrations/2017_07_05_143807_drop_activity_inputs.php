<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropActivityInputs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('activity_input_breakdowns');
        Schema::dropIfExists('activity_input_forwards');
        Schema::dropIfExists('activity_input_periods');
        Schema::dropIfExists('activity_fund_sources');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('activity_input_breakdowns',function (Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->text('item');
            $blueprint->integer('activity_input_id')->unsigned();
            $blueprint->integer('unit_id')->unsigned();
            $blueprint->decimal('unit_price',20,2);
            $blueprint->integer('quantity');
            $blueprint->integer('frequency');
            $blueprint->integer('budget_submission_definition_id')->unsigned();
            $blueprint->softDeletes();
            $blueprint->timestamps();
        });

        Schema::create('activity_periods',function (Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->integer('activity_input_id')->unsigned();
            $blueprint->integer('period_id')->unsigned();
            $blueprint->decimal('percentage',5,2);
            $blueprint->softDeletes();
            $blueprint->timestamps();
        });

        Schema::create('activity_input_forwards',function (Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->integer('quantity');
            $blueprint->integer('frequency');
            $blueprint->integer('activity_input_id')->unsigned();
            $blueprint->integer('financial_year_id')->unsigned();
            $blueprint->integer('financial_year_order');
            $blueprint->softDeletes();
            $blueprint->timestamps();
        });
    }
}
