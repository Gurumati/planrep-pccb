<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBdcExpCentreGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bdc_expenditure_centre_groups',function (Blueprint $table){
            $table->string('type')->nullable();
            $table->integer('fund_source_id')->unsigned()->nullable();
            $table->integer('reference_document_id')->unsigned()->nullable();
            $table->boolean('active')->default(true);
            $table->integer('section_id')->unsigned()->nullable();

            $table->foreign('fund_source_id')->references('id')->on('fund_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('reference_document_id')->references('id')->on('reference_documents')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('section_id')->references('id')->on('sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bdc_expenditure_centre_groups',function (Blueprint $table){
            $table->dropForeign('bdc_expenditure_centre_groups_fund_source_id_foreign');
            $table->dropForeign('bdc_expenditure_centre_groups_reference_document_id_foreign');
            $table->dropForeign('bdc_expenditure_centre_groups_section_id_foreign');
            $table->dropColumn('type');
            $table->dropColumn('fund_source_id');
            $table->dropColumn('reference_document_id');
            $table->dropColumn('active');
            $table->dropColumn('section_id');
        });
    }
}
