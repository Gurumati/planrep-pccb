<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLongTermTargetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('long_term_targets', function(Blueprint $table)
		{
			$table->foreign('intervention_id')->references('id')->on('interventions')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('plan_chain_id')->references('id')->on('plan_chains')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('reference_document_id')->references('id')->on('reference_documents')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('sector_problem_id')->references('id')->on('sector_problems')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('long_term_targets', function(Blueprint $table)
		{
			$table->dropForeign('long_term_targets_intervention_id_foreign');
			$table->dropForeign('long_term_targets_plan_chain_id_foreign');
			$table->dropForeign('long_term_targets_reference_document_id_foreign');
			$table->dropForeign('long_term_targets_sector_problem_id_foreign');
		});
	}

}
