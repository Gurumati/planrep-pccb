<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFfarsActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ffars_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description')->unsigned();
            $table->string('activity_code')->unsigned();
            $table->string('facility_code')->unsigned();
            $table->string('sub_budget_class_code')->unsigned();
            $table->string('project_code')->unsigned();
            $table->date('apply_date')->unsigned();

            $table->boolean("exported_to_ffars")->default(false);
            $table->boolean("is_delivered_to_ffars")->default(false);
            $table->text("ffars_response")->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ffars_activities');
    }
}
