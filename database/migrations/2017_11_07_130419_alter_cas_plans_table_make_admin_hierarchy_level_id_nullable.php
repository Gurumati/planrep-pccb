<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCasPlansTableMakeAdminHierarchyLevelIdNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cas_plans',function (Blueprint $blueprint){
            $blueprint->integer('admin_hierarchy_level_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_plans',function (Blueprint $blueprint){
            $blueprint->integer('admin_hierarchy_level_id')->unsigned()->change();
        });
    }
}
