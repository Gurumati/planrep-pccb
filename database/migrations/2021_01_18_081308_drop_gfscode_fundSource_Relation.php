<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropGfscodeFundSourceRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gfs_codes', function (Blueprint $table) {
            $table->dropColumn('fund_source_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gfs_codes', function (Blueprint $table) {
            $table->dropColumn('fund_source_id');
        });
    }
}
