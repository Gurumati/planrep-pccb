<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActivityFacilityFundSourceInputsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('activity_facility_fund_source_inputs', function(Blueprint $table)
		{
			$table->foreign('activity_facility_fund_source_id', 'activity_facility_fund_source_inputs_activity_facility_fund_sou')->references('id')->on('activity_facility_fund_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('gfs_code_id')->references('id')->on('gfs_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('procurement_type_id', 'activity_facility_fund_source_inputs_procurement_type_id_foreig')->references('id')->on('procurement_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('unit_id')->references('id')->on('units')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('activity_facility_fund_source_inputs', function(Blueprint $table)
		{
			$table->dropForeign('activity_facility_fund_source_inputs_activity_facility_fund_sou');
			$table->dropForeign('activity_facility_fund_source_inputs_gfs_code_id_foreign');
			$table->dropForeign('activity_facility_fund_source_inputs_procurement_type_id_foreig');
			$table->dropForeign('activity_facility_fund_source_inputs_unit_id_foreign');
		});
	}

}
