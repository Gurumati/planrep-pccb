<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGfsCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gfs_codes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('code', 191)->unique();
			$table->text('description');
			$table->integer('account_type_id')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('sort_order')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('update_by')->nullable();
			$table->string('name', 191);
			$table->boolean('is_procurement');
			$table->boolean('is_active');
			$table->integer('fund_source_id')->nullable();
			$table->integer('gfs_code_sub_category_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gfs_codes');
	}

}
