<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityImplementationAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity_implementation_attachments',function (Blueprint $table){
            $table->foreign('implementation_id')->references('id')->on('activity_implementations')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity_implementation_attachments',function (Blueprint $table){
            $table->dropForeign('activity_implementation_attachments_implementation_id_foreign');
        });
    }
}
