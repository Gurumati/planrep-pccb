<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetBalanceTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('budget_balance_transactions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('chart_of_accounts', 191);
			$table->decimal('budget',  20)->default(0);
			$table->decimal('balance',  20)->default(0)->nullable();
            $table->timestamp('request_time');
            $table->timestamp('response_time')->nullable();
			$table->text('response_message')->nullable();
			$table->string('transaction_id', 191);
			$table->integer('financial_year_id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('budget_balance_transactions');

    }
}
