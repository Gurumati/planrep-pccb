<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFundSourceCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fund_source_categories', function(Blueprint $table)
		{
			$table->foreign('fund_type_id')->references('id')->on('fund_types')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fund_source_categories', function(Blueprint $table)
		{
			$table->dropForeign('fund_source_categories_fund_type_id_foreign');
		});
	}

}
