<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityImplementationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity_implementations',function (Blueprint $table){
            $table->foreign('status_id')->references('id')->on('activity_statuses')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('period_id')->references('id')->on('periods')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity_implementations',function (Blueprint $table){
            $table->dropForeign('activity_implementations_period_id_foreign');
            $table->dropForeign('activity_implementations_status_id_foreign');
        });
    }
}
