<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExchequerBudgetClassProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('exchequer_budget_class_projects', function(Blueprint $table)
		{
			$table->foreign('exch_budget_class_id')->references('id')->on('exchequer_budget_classes')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('project_id')->references('id')->on('projects')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('exchequer_budget_class_projects', function(Blueprint $table)
		{
			$table->dropForeign('exchequer_budget_class_projects_exch_budget_class_id_foreign');
			$table->dropForeign('exchequer_budget_class_projects_project_id_foreign');
		});
	}

}
