<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReceivedFundItemsAddNecesssaryColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('received_fund_items', function(Blueprint $blueprint){
            $blueprint->integer('revenue_export_account_id')->unsigned()->nullable();
            $blueprint->decimal('debit_amount',20,2)->nullable();
            $blueprint->decimal('credit_amount',20,2)->nullable();
            $blueprint->string('fiscal_year');
            $blueprint->string('account');
            $blueprint->date('JEDate');
            $blueprint->integer('budget_control_account_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('received_fund_items', function(Blueprint $blueprint){
            $blueprint->dropColumn('revenue_export_account_id');
            $blueprint->dropColumn('debit_amount');
            $blueprint->dropColumn('credit_amount');
            $blueprint->dropColumn('fiscal_year');
            $blueprint->dropColumn('account');
            $blueprint->dropColumn('JEDate');
            $blueprint->dropColumn('budget_control_account_id');
        });
    }
}
