<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGenericActivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generic_problems', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->text('description');
            $table->integer('priority_area_id');
            $table->string('params')->nullable();
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('priority_area_id')->references('id')->on('priority_areas')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

        Schema::create('generic_activities', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->text('description');
            $table->integer('generic_target_id');
            $table->string('params')->nullable();
            $table->integer('intervention_id')->nullable();
            $table->integer('budget_class_id');
            $table->integer('generic_problem_id')->nullable();
            $table->integer('performance_indicator_id')->nullable();
            $table->integer('national_target_id')->nullable();
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('budget_class_id')->references('id')->on('budget_classes')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('generic_target_id')->references('id')->on('generic_targets')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('intervention_id')->references('id')->on('interventions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('generic_problem_id')->references('id')->on('generic_problems')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('performance_indicator_id')->references('id')->on('performance_indicators')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('national_target_id')->references('id')->on('national_targets')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('generic_activities');
        Schema::drop('generic_problems');

    }
}
