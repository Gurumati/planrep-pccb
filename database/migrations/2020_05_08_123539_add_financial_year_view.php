<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddFinancialYearView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // DB::statement("DROP MATERIALIZED VIEW IF EXISTS vw_financial_years");
        DB::statement("CREATE MATERIALIZED VIEW public.vw_financial_years
                        AS
    select fy.id as financial_year_id,
       fy.name as financial_year_name,
       fy.start_date as financial_year_start_date,
       fy.end_date as financial_year_end_date,
       q1.id as Q1_id,q1.name as Q1_name,q1.code as q1_code,
       q2.id as Q2_id,q2.name as Q2_name, q2.code as q2_code,
       q3.id as Q3_id,q3.name as Q3_name,q3.code as q3_code,
       q4.id as Q4_id,q4.name as Q4_name, q4.code as q4_code
 from financial_years as fy
 left join periods as q1 on (fy.id = q1.financial_year_id  and q1.code = 'Q1')
 left join periods as q2 on (fy.id = q2.financial_year_id  and q2.code = 'Q2')
 left join periods as q3 on (fy.id = q3.financial_year_id  and q3.code = 'Q3')
 left join periods as q4 on (fy.id = q4.financial_year_id  and q4.code = 'Q4')
 WITH DATA
");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP MATERIALIZED VIEW IF EXISTS vw_financial_years");
    }
}
