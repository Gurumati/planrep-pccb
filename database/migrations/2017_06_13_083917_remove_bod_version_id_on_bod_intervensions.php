<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveBodVersionIdOnBodIntervensions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
     {
        Schema::table('bod_interventions', function (Blueprint $table) {
            $table->dropColumn('bod_version_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
{
        Schema::table('bod_interventions', function (Blueprint $table) {
            $table->integer('bod_version_id');
        });
    }
   
    
}
