<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCummulativeExpenditureAddDeptmanet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DROP VIEW IF EXISTS vw_cumulative_expenditure');
        DB::statement('create view vw_cumulative_expenditure as 
            SELECT
            financial_year_id,
            financial_year_name,
            admin_hierarchy_id,
            admin_hierarchy_name,
            admin_hierarchy_code,
            department_id,
            department_name,
            department_code,
            section_id,
            section_name,
            section_code,
            activity_id,
            activity,
            budget_class_id,
            budget_class_name,
            budget_class_code,
            fund_source_id,
            fund_source_name,
            fund_source_code,
            gfs_id,
            gfs_name,
            gfs_code,
            period_id,
            period_name,
            period_code,
            facility_id,
            facility_name,
            facility_code,
            budget_type,
            amount as expenditure,
            SUM(amount) OVER
                (PARTITION BY financial_year_id, admin_hierarchy_id,department_id,section_id,budget_class_id, fund_source_id,gfs_id,facility_id, budget_type ORDER BY period_id )as cumulative_expenditure
        from
            (
                select
                    fy.id as financial_year_id,
                    fy.name as financial_year_name,
                    ah.id as admin_hierarchy_id,
                    ah.name as admin_hierarchy_name,
                    ah.code as admin_hierarchy_code,
                    dpt.id as department_id,
                    dpt.name as department_name,
                    dpt.code as department_code,
                    s.id as section_id,
                    s.name as section_name,
                    s.code as section_code,
                    ac.id as activity_id,
                    ac.description as activity,
                    bc.id as budget_class_id,
                    bc.name as budget_class_name,
                    bc.code as budget_class_code,
                    fs.id as fund_source_id,
                    fs.name as fund_source_name,
                    fs.code as fund_source_code,
                    gfs.id as gfs_id,
                    gfs.name as gfs_name,
                    gfs.code as gfs_code,
                    p.id as period_id,
                    p.code as period_code,
                    p.name as period_name,
                    f.id as facility_id,
                    f.name as facility_name,
                    f.facility_code,
                    sum(i.debit_amount::numeric(15,2)) - sum(i.credit_amount::numeric(15,2)) as amount,
                    ac.budget_type
                from
                    budget_import_items i join
                    budget_classes bc on bc.id = i.budget_class_id join
                    fund_sources fs on fs.id = i.fund_source_id join
                    gfs_codes gfs on gfs.id = i.gfs_code_id join
                    periods p on p.id = i.period_id join
                    budget_export_accounts a on a.id = i.budget_export_account_id join
                    financial_years fy on fy.id = a.financial_year_id join
                    admin_hierarchies ah on ah.id = a.admin_hierarchy_id join
                    activities  ac on ac.id = a.activity_id join
                    activity_facilities af on af.activity_id = ac.id  join
                    facilities f on f.id = af.facility_id join
                    mtef_sections ms on ms.id = ac.mtef_section_id join
                    sections s on s.id = ms.section_id join
                    sections dpt on dpt.id = s.parent_id
                group by
                    fy.id,
                    ah.id,
                    dpt.id,
                    s.id,
                    ac.id,
                    bc.id,
                    fs.id,
                    gfs.id,
                    ac.budget_type,
                    f.id,
                    p.id
            ) as x');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW IF EXISTS vw_cumulative_expenditure');

    }
}
