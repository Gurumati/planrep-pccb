<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPerformanceIndicatorValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('performance_indicator_baseline_values',function (Blueprint $table){
            $table->integer('section_id')->nullable()->unsigned();
            $table->foreign('section_id')->references('id')->on('sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('performance_indicator_baseline_values',function (Blueprint $table){
            $table->dropColumn('section_id');
        });
    }
}
