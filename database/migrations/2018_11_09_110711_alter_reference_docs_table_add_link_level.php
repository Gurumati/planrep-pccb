<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReferenceDocsTableAddLinkLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reference_docs', function (Blueprint $table) {
            $table->enum("link_level",['TARGET','ACTIVITY'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reference_docs', function (Blueprint $table) {
            $table->dropColumn("link_level");
        });
    }
}
