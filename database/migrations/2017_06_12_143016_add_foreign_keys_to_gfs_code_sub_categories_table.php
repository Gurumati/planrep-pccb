<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToGfsCodeSubCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('gfs_code_sub_categories', function(Blueprint $table)
		{
			$table->foreign('gfs_code_category_id')->references('id')->on('gfs_code_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('gfs_code_sub_categories', function(Blueprint $table)
		{
			$table->dropForeign('gfs_code_sub_categories_gfs_code_category_id_foreign');
		});
	}

}
