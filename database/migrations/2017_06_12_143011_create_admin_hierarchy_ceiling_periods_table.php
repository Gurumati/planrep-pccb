<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminHierarchyCeilingPeriodsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_hierarchy_ceiling_periods', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('admin_hierarchy_ceiling_id');
			$table->integer('period_id');
			$table->decimal('amount', 20);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->unique(['admin_hierarchy_ceiling_id','period_id'], 'admin_hierarchy_ceiling_periods_admin_hierarchy_ceiling_id_peri');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin_hierarchy_ceiling_periods');
	}

}
