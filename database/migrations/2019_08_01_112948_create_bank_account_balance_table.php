<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountBalanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_account_balances', function(Blueprint $table){
            $table->increments('id');
            $table->integer('bank_account_id');
            $table->integer('financial_year_id');
            $table->integer('admin_hierarchy_id');
            $table->decimal('amount',14,2);
            $table->timestamps();
            $table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bank_account_balances');
    }
}
