<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivityInputBreakdownsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activity_input_breakdowns', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('item');
			$table->integer('activity_input_id');
			$table->integer('unit_id');
			$table->decimal('unit_price', 20);
			$table->integer('quantity');
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
			$table->integer('frequency');
			$table->integer('budget_submission_definition_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activity_input_breakdowns');
	}

}
