<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCeilingChainsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ceiling_chains', function(Blueprint $table)
		{
			$table->foreign('next_id')->references('id')->on('ceiling_chains')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ceiling_chains', function(Blueprint $table)
		{
			$table->dropForeign('ceiling_chains_next_id_foreign');
		});
	}

}
