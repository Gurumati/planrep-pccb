<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToDecisionLevelAdminHierarchiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('decision_level_admin_hierarchies', function(Blueprint $table)
		{
			$table->foreign('admin_hierarchy_level_id', 'decision_level_admin_hierarchies_admin_hierarchy_level_id_forei')->references('id')->on('admin_hierarchy_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('decision_level_id')->references('id')->on('decision_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('decision_level_admin_hierarchies', function(Blueprint $table)
		{
			$table->dropForeign('decision_level_admin_hierarchies_admin_hierarchy_level_id_forei');
			$table->dropForeign('decision_level_admin_hierarchies_decision_level_id_foreign');
		});
	}

}
