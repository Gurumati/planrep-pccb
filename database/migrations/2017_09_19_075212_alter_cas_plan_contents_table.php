<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCasPlanContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cas_plan_contents', function(Blueprint $table)
        {
            $table->foreign('cas_plan_id')->references('id')->on('cas_plans')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('cas_plan_content_id')->references('id')->on('cas_plan_contents')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_plan_contents', function(Blueprint $table)
        {
            $table->dropForeign('cas_plan_contents_cas_plan_content_id_foreign');
            $table->dropForeign('cas_plan_contents_cas_plan_id_foreign');
        });
    }
}
