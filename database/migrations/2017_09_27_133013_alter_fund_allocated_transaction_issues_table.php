<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFundAllocatedTransactionIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_allocated_transaction_issues',function (Blueprint $table){
            $table->foreign('budget_import_issue_type_id')->references('id')->on('budget_import_issue_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('fund_allocated_transaction_item_id')->references('id')->on('fund_allocated_transaction_items')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_allocated_transaction_issues',function (Blueprint $table){
            $table->dropForeign('fund_allocated_transaction_issues_budget_import_issue_type_id_f');
            $table->dropForeign('fund_allocated_transaction_issues_fund_allocated_transaction_it');
        });
    }
}
