<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePeSubmissionFormsReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pe_submission_forms_reports', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 191);
			$table->string('title', 191);
			$table->string('params', 191)->nullable();
			$table->integer('parent_id')->nullable();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->boolean('active')->default(1);
			$table->string('code', 191)->nullable();
			$table->boolean('is_aggregate')->default(1);
			$table->timestamps();
			$table->text('sql_query')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pe_submission_forms_reports');
	}

}
