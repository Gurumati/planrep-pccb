<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanChainVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_chain_versions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("plan_chain_id")->unsigned();
            $table->integer("version_id")->unsigned();
            $table->unique(['plan_chain_id','version_id']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_chain_versions');
    }
}
