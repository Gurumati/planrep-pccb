<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBudgetSubmissionSubFormsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('budget_submission_sub_forms', function(Blueprint $table)
		{
			$table->foreign('budget_submission_form_id')->references('id')->on('budget_submission_forms')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('budget_submission_sub_forms', function(Blueprint $table)
		{
			$table->dropForeign('budget_submission_sub_forms_budget_submission_form_id_foreign');
		});
	}

}
