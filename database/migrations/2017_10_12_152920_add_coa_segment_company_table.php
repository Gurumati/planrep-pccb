<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoaSegmentCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coa_segments', function(Blueprint $blueprint){
            $blueprint->dropColumn('is_sent');
            $blueprint->dropColumn('is_delivered');
            $blueprint->dropColumn('response');
        });

        Schema::create('coa_segment_company', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('coa_segment_id');
            $table->integer('admin_hierarchy_id');
            $table->boolean('is_sent')->default(false);
            $table->boolean('is_delivered')->default(false);
            $table->string('response')->nullable();
            $table->foreign('coa_segment_id')->references('id')->on('coa_segments')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coa_segments', function(Blueprint $blueprint){
            $blueprint->boolean('is_sent')->nullable();
            $blueprint->boolean('is_delivered')->nullable();
            $blueprint->string('response')->nullable();
        });
        Schema::dropIfExists('coa_segment_company');

    }
}
