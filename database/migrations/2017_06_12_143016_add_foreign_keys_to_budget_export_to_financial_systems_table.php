<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBudgetExportToFinancialSystemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('budget_export_to_financial_systems', function(Blueprint $table)
		{
			$table->foreign('financial_year_id', 'budget_export_to_epicors_financial_year_id_foreign')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('budget_export_transaction_item_id', 'budget_export_to_epicors_budget_export_transaction_item_id_fore')->references('id')->on('budget_export_transaction_items')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('budget_export_to_financial_systems', function(Blueprint $table)
		{
			$table->dropForeign('budget_export_to_epicors_financial_year_id_foreign');
			$table->dropForeign('budget_export_to_epicors_budget_export_transaction_item_id_fore');
		});
	}

}
