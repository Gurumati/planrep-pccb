<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminCeilingReferenceDocs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_hierarchy_ceiling_docs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('admin_hierarchy_id');
            $table->string('financial_year_id');
            $table->string('budget_type');
            $table->string('document_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admin_hierarchy_ceiling_docs');
    }
}
