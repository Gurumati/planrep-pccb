<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserProfileKeysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_profile_keys', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('key', 191);
			$table->string('default_value', 191)->nullable();
			$table->string('group', 191)->nullable();
			$table->text('options')->nullable();
			$table->text('option_query')->nullable();
			$table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_profile_keys');
	}

}
