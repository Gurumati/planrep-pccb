<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBudgetClassTemplatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('budget_class_templates', function(Blueprint $table)
		{
			$table->foreign('budget_class_id')->references('id')->on('budget_classes')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('budget_class_templates', function(Blueprint $table)
		{
			$table->dropForeign('budget_class_templates_budget_class_id_foreign');
		});
	}

}
