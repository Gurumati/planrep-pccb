<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBudgetExportAccountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('budget_export_accounts', function(Blueprint $table)
		{
			$table->foreign('activity_id')->references('id')->on('activities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('gfs_code_id')->references('id')->on('gfs_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('budget_export_accounts', function(Blueprint $table)
		{
			$table->dropForeign('budget_export_accounts_activity_id_foreign');
			$table->dropForeign('budget_export_accounts_gfs_code_id_foreign');
		});
	}

}
