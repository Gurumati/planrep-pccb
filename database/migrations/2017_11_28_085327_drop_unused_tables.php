<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropUnusedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists("exchequer_budget_class_projects");
        Schema::dropIfExists("exchequer_budget_class_sections");
        Schema::dropIfExists("exchequer_budget_classes");
        Schema::dropIfExists("exchequers");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
