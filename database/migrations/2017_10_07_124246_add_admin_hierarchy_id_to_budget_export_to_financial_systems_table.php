<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminHierarchyIdToBudgetExportToFinancialSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_export_to_financial_systems',function (Blueprint $table){
            $table->integer('admin_hierarchy_id');
            $table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_export_to_financial_systems',function (Blueprint $table){
            $table->dropColumn('admin_hierarchy_id');
        });
    }
}
