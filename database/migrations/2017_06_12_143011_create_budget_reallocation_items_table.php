<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBudgetReallocationItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('budget_reallocation_items', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('from_chart_of_accounts', 191);
			$table->string('to_chart_of_accounts', 191);
			$table->decimal('amount', 10);
			$table->integer('budget_export_account_id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('update_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('budget_reallocation_items');
	}

}
