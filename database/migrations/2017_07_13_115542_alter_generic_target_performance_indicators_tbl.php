<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGenericTargetPerformanceIndicatorsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('generic_target_performance_indicators',function (Blueprint $blueprint){
            $blueprint->unique(['generic_target_id','performance_indicator_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('generic_target_performance_indicators',function (Blueprint $blueprint){
            $blueprint->dropUnique('generic_target_performance_indicators_generic_target_id_perform');
        });
    }
}
