<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddControlNumberToRevenueExportTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('revenue_export_transactions',function (Blueprint $blueprint){
            $blueprint->string('control_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('revenue_export_transactions',function (Blueprint $blueprint){
            $blueprint->dropColumn('control_number');
        });
    }
}
