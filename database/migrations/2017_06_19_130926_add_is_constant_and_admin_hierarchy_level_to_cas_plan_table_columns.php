<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsConstantAndAdminHierarchyLevelToCasPlanTableColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cas_plan_table_columns',function (Blueprint $blueprint){
            $blueprint->boolean('is_constant')->default('false');
            $blueprint->integer('admin_hierarchy_level_id')->nullable();
            $blueprint->foreign('admin_hierarchy_level_id')->references('id')->on('admin_hierarchy_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_plan_table_columns',function (Blueprint $blueprint){
            $blueprint->dropColumn('is_constant');
            $blueprint->dropColumn('admin_hierarchy_level_id');
        });
    }
}
