<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRevenueExportAccountIdToBudgetExportAccountGlaccountExportIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_export_account_glaccount_export_issues', function(Blueprint $blueprint){
            $blueprint->integer('revenue_export_account_id')->nullable()->unsigned();
            $blueprint->integer('budget_export_account_id')->nullable()->change();
            $blueprint->foreign('revenue_export_account_id')->references('id')->on('revenue_export_accounts')->onDelete('restrict')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_export_account_glaccount_export_issues', function(Blueprint $blueprint){
            $blueprint->dropForeign('budget_export_account_glaccount_export_issues_revenue_export_ac');
        });

        Schema::table('budget_export_account_glaccount_export_issues', function(Blueprint $blueprint){
            $blueprint->dropColumn('revenue_export_account_id');
            $blueprint->integer('budget_export_account_id')->change();
        });
    }
}
