<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFundersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('funders', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 191)->unique();
			$table->string('code', 191)->unique();
			$table->boolean('is_local');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('sort_order')->nullable();
			$table->boolean('is_active');
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('update_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('funders');
	}

}
