<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilityCustomDetailMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facility_custom_detail_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('facility_custom_detail_id')->unsigned();
            $table->integer('facility_type_id')->unsigned();
            $table->integer('facility_ownership_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facility_custom_detail_mappings');
    }
}
