<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasPlanTableItemAdminHierarchiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cas_plan_table_item_admin_hierarchies', function(Blueprint $table)
		{
			$table->foreign('admin_hierarchy_id', 'cas_plan_table_column_admin_hierarchies_admin_hierarchy_id_fore')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('cas_plan_table_id', 'cas_plan_table_column_admin_hierarchies_cas_plan_table_id_forei')->references('id')->on('cas_plan_tables')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('cas_plan_table_item_id', 'cas_plan_table_item_admin_hierarchies_cas_plan_table_item_id_fo')->references('id')->on('cas_plan_table_items')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cas_plan_table_item_admin_hierarchies', function(Blueprint $table)
		{
			$table->dropForeign('cas_plan_table_column_admin_hierarchies_admin_hierarchy_id_fore');
			$table->dropForeign('cas_plan_table_column_admin_hierarchies_cas_plan_table_id_forei');
			$table->dropForeign('cas_plan_table_item_admin_hierarchies_cas_plan_table_item_id_fo');
		});
	}

}
