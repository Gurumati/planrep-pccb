<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBudgetImportItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('budget_import_items', function(Blueprint $table)
		{
			$table->foreign('budget_export_account_id')->references('id')->on('budget_export_accounts')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('budget_import_id')->references('id')->on('budget_imports')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('period_id')->references('id')->on('periods')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('budget_import_items', function(Blueprint $table)
		{
			$table->dropForeign('budget_import_items_budget_export_account_id_foreign');
			$table->dropForeign('budget_import_items_budget_import_id_foreign');
			$table->dropForeign('budget_import_items_period_id_foreign');
		});
	}

}
