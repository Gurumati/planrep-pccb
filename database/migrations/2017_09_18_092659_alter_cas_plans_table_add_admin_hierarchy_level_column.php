<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCasPlansTableAddAdminHierarchyLevelColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cas_plans',function (Blueprint $blueprint){
            $blueprint->integer('admin_hierarchy_level_id')->unsigned()->nullable();
            $blueprint->foreign('admin_hierarchy_level_id')->references('id')->on('admin_hierarchy_levels')->onUpdate('cascade')->onDelete("restrict");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_plans',function (Blueprint $blueprint){
            $blueprint->dropForeign('cas_plans_admin_hierarchy_level_id_foreign');
            $blueprint->dropColumn('admin_hierarchy_level_id');
        });
    }
}
