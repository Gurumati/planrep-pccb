<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivityInputsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activity_inputs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('activity_id');
			$table->integer('gfs_code_id');
			$table->integer('unit_id');
			$table->integer('quantity');
			$table->decimal('unit_price', 20);
			$table->integer('frequency');
			$table->boolean('is_procurement');
			$table->integer('procurement_type_id')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->text('is_in_kind');
			$table->boolean('has_breakdown');
			$table->string('description', 191);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activity_inputs');
	}

}
