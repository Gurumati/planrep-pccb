<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetReceivedFundItemsTableRemoveReceivedFundIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('received_fund_items',function(Blueprint $blueprint){
            $blueprint->dropColumn('received_fund_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('received_fund_items',function(Blueprint $blueprint){
            $blueprint->integer('received_fund_id')->unsigned()->nullable();
        });
    }
}
