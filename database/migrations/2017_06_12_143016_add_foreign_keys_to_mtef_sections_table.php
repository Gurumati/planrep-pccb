<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMtefSectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('mtef_sections', function(Blueprint $table)
		{
			$table->foreign('mtef_id')->references('id')->on('mtefs')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('section_id')->references('id')->on('sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('mtef_sections', function(Blueprint $table)
		{
			$table->dropForeign('mtef_sections_mtef_id_foreign');
			$table->dropForeign('mtef_sections_section_id_foreign');
		});
	}

}
