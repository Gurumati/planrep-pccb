<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHistoricalDataItemsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('historical_data_items',function (Blueprint $blueprint){
            $blueprint->decimal('value',20,2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historical_data_items',function (Blueprint $blueprint){
            $blueprint->decimal('value',20,2)->default(0)->change();
        });
    }
}
