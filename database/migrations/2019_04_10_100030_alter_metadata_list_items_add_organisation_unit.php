<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMetadataListItemsAddOrganisationUnit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('metadata_list_items',function(Blueprint $blueprint){
            $blueprint->string('orgUnit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metadata_list_items',function(Blueprint $blueprint){
            $blueprint->dropColumn('orgUnit');
        });
    }
}
