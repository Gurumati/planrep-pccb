<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GfsCodeSubCategoriesTableAddIsProcurement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gfs_code_sub_categories', function (Blueprint $table) {
            $table->boolean("is_procurement")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gfs_code_sub_categories', function (Blueprint $table) {
            $table->dropColumn("is_procurement");
        });
    }
}
