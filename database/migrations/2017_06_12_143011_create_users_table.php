<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('email', 191)->unique();
			$table->string('password', 191);
			$table->text('permissions')->nullable();
			$table->dateTime('last_login')->nullable();
			$table->string('first_name', 191)->nullable();
			$table->string('last_name', 191)->nullable();
			$table->integer('admin_hierarchy_id')->nullable();
			$table->integer('section_id')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('per_page')->default(10);
			$table->string('user_name', 191)->default('defaultusername');
			$table->integer('decision_level_id')->nullable();
			$table->string('cheque_number', 191)->nullable()->unique();
			$table->boolean('is_superuser')->default(0);
			$table->string('profile_picture_url', 191)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
