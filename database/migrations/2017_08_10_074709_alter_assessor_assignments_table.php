<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAssessorAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assessor_assignments',function (Blueprint $table){
            $table->unique([
                'user_id',
                'admin_hierarchy_id',
                'period_id',
                'cas_assessment_round_id',
                'admin_hierarchy_level_id',
                'cas_assessment_category_version_id'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
