<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropInputProjectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('input_projections');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('input_projections', function (Blueprint $blueprint) {
            $blueprint->increments('id');
            $blueprint->integer('activity_input_id')->unsigned();
            $blueprint->string('year');
            $blueprint->integer('quantity');
            $blueprint->softDeletes();
            $blueprint->timestamps();
        });
    }
}
