<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAdminHierarchyCeilingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('admin_hierarchy_ceilings', function(Blueprint $table)
		{
			$table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('section_id')->references('id')->on('sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('admin_hierarchy_ceilings', function(Blueprint $table)
		{
			$table->dropForeign('admin_hierarchy_ceilings_admin_hierarchy_id_foreign');
			$table->dropForeign('admin_hierarchy_ceilings_financial_year_id_foreign');
			$table->dropForeign('admin_hierarchy_ceilings_section_id_foreign');
		});
	}

}
