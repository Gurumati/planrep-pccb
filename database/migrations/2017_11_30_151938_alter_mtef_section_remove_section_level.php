<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterMtefSectionRemoveSectionLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mtef_sections',function (Blueprint $table){
           $table->dropColumn('section_level_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtef_sections',function (Blueprint $table){
            $table->integer('section_level_id')->unsigned();
        });
    }
}
