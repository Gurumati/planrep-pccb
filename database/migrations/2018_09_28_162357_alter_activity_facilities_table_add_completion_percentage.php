<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityFacilitiesTableAddCompletionPercentage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("activity_facilities",function (Blueprint $blueprint){
            $blueprint->decimal("completion_percentage",5,2)->default('0.00');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("activity_facilities",function (Blueprint $blueprint){
            $blueprint->dropColumn("completion_percentage");
        });
    }
}
