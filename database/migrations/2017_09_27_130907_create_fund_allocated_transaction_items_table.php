<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFundAllocatedTransactionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fund_allocated_transaction_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fund_allocated_transaction_id')->unsigned();
            $table->integer('budget_export_account_id')->unsigned();
            $table->decimal('amount',20,2)->default(0);
            $table->date('allocation_date');
            $table->integer('fund_allocated_from_financial_system_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fund_allocated_transaction_items');
    }
}
