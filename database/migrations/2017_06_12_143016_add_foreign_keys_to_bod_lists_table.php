<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBodListsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bod_lists', function(Blueprint $table)
		{
			$table->foreign('sector_id')->references('id')->on('sectors')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bod_lists', function(Blueprint $table)
		{
			$table->dropForeign('bod_lists_sector_id_foreign');
		});
	}

}
