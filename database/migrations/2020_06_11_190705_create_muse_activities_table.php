<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMuseActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('muse_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('activity_id')->unsigned();
            $table->date('apply_date')->unsigned();
            $table->boolean("exported_to_muse")->default(false);
            $table->boolean("delivered_to_muse")->default(false);
            $table->text("muse_response")->nullable();
            $table->integer("admin_hierarchy_id")->nullable();
            $table->integer("financial_year_id")->nullable();
            $table->text("budget_type")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('muse_activities');

    }
}
