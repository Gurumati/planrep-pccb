<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityImplementationsTableAddLatLong extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("activity_implementations",function (Blueprint $blueprint){
            $blueprint->decimal("latitude",11,8)->nullable();
            $blueprint->decimal("longitude",11,8)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("activity_implementations",function (Blueprint $blueprint){
            $blueprint->dropColumn("latitude");
            $blueprint->dropColumn("longitude");
        });
    }
}
