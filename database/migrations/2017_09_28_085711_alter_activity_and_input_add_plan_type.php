<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityAndInputAddPlanType extends Migration
{
    /**
     * Run the migrations.
     * Plan_type: CURRENT, REALLOCATED,SUPPLEMENTARY
     *
     * @return void
     */
    public function up()
    {
        Schema::table("activities",function (Blueprint $table){
           $table->string("plan_type",15)->default("CURRENT");
        });
        Schema::table("activity_facility_fund_source_inputs",function (Blueprint $table){
           $table->string("plan_type",15)->default("CURRENT");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("activities",function (Blueprint $table){
            $table->dropColumn("plan_type");
        });
        Schema::table("activity_facility_fund_source_inputs",function (Blueprint $table){
            $table->string("plan_type");
        });
    }
}
