<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBodInterventionsTb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("bod_interventions",function (Blueprint $blueprint){
            $blueprint->string('formula')->nullable()->change();
            $blueprint->string('formula_type')->nullable()->change();
            $blueprint->string('arithmetic_operator')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("bod_interventions",function (Blueprint $blueprint){
            $blueprint->string('formula')->nullable(false)->change();
            $blueprint->string('formula_type')->nullable(false)->change();
            $blueprint->string('arithmetic_operator')->nullable(false)->change();
        });
    }
}
