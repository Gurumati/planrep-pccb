<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPeriodIdToCasPlanTableItemValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("cas_plan_table_item_values",function (Blueprint $blueprint){
            $blueprint->integer("period_id")->nullable();
            $blueprint->foreign('period_id')->references('id')->on('periods')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("cas_plan_table_item_values",function (Blueprint $blueprint){
            $blueprint->dropColumn("period_id");
        });
    }
}
