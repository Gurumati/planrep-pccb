<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetImportMissingAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_import_missing_accounts', function(Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->string('chart_of_accounts');
            $blueprint->integer('financial_year_id')->unsigned();
            $blueprint->integer('admin_hierarchy_id')->unsigned();
            $blueprint->double('debit_amount')->nullable();
            $blueprint->double('credit_amount')->nullable();
            $blueprint->date('JEDate');
            $blueprint->string('bookID');
            $blueprint->string('reason');
            $blueprint->string('transaction_type');
            $blueprint->timestamps();
            $blueprint->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('cascade')->onDelete('restrict');
            $blueprint->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_import_missing_accounts');
    }
}
