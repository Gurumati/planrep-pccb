<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropBudgetControlAccountIdFromFundAllocatedTransactionItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_allocated_transaction_items', function(Blueprint $blueprint){
            $blueprint->dropColumn('budget_control_account_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_allocated_transaction_items', function(Blueprint $blueprint){
            $blueprint->integer('budget_control_account_id')->nullable();
        });
    }
}
