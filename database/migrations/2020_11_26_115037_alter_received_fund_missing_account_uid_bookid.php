<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReceivedFundMissingAccountUidBookid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('received_fund_missing_accounts', function (Blueprint $table) {
            $table->string('uid', 191)->nullable()->change();
            $table->string('bookID')->nullable()->change();
            $table->string('msg_id')->nullable();
        });

        Schema::table('received_fund_items', function (Blueprint $table) {
            $table->string('uid', 191)->nullable();
            $table->string('msg_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('received_fund_missing_accounts', function (Blueprint $table) {
            $table->dropColumn(['uid','bookID','msg_id']);
        });
        
        Schema::table('received_fund_items', function (Blueprint $table) {
            $table->dropColumn(['uid','msg_id']);
        });
    }
}
