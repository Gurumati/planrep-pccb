<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIfrsgfsMappingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ifrsgfs_mapping', function(Blueprint $table)
        {
            $table->integer('id', true);
            $table->integer('gfs_code_id');
            $table->integer('gfs_code_sub_category_id');
            $table->timestamps();
            $table->softDeletes();
            $table->unique(['gfs_code_id','gfs_code_sub_category_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ifrsgfs_mapping');
    }
}
