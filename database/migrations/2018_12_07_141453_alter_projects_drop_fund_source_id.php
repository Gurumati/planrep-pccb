<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectsDropFundSourceId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("projects",function (Blueprint $blueprint){
            $blueprint->dropColumn("fund_source_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("projects",function (Blueprint $blueprint){
            $blueprint->integer("fund_source_id")->unsigned()->nullable();
        });
    }
}
