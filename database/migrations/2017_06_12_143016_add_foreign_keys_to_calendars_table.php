<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCalendarsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('calendars', function(Blueprint $table)
		{
			$table->foreign('calendar_event_id')->references('id')->on('calendar_events')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('sector_id')->references('id')->on('sectors')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('calendars', function(Blueprint $table)
		{
			$table->dropForeign('calendars_calendar_event_id_foreign');
			$table->dropForeign('calendars_financial_year_id_foreign');
			$table->dropForeign('calendars_sector_id_foreign');
		});
	}

}
