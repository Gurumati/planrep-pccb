<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMtefsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('mtefs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('financial_year_id');
			$table->integer('admin_hierarchy_id');
			$table->integer('decision_level_id');
			$table->boolean('locked');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->boolean('is_final');
			$table->string('plan_type', 191)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mtefs');
	}

}
