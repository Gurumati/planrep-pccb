<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableRevenueExportTransactionsRemoveUnuqueKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('revenue_export_transactions',function (Blueprint $blueprint){
            $blueprint->dropUnique('revenue_export_transactions_budget_transaction_type_id_unique');
            //$blueprint->integer('budget_transaction_type_id')->unique(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('revenue_export_transactions',function (Blueprint $blueprint){
            $blueprint->integer('budget_transaction_type_id')->unique()->change();
        });
    }
}
