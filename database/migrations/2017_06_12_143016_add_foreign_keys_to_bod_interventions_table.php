<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBodInterventionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bod_interventions', function(Blueprint $table)
		{
			$table->foreign('bod_version_id')->references('id')->on('bod_versions')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('fund_source_id')->references('id')->on('fund_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('intervention_category_id')->references('id')->on('intervention_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bod_interventions', function(Blueprint $table)
		{
			$table->dropForeign('bod_interventions_bod_version_id_foreign');
			$table->dropForeign('bod_interventions_fund_source_id_foreign');
			$table->dropForeign('bod_interventions_intervention_category_id_foreign');
		});
	}

}
