<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHistoricalDataItemsTble extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("historical_data_items",function (Blueprint $blueprint){
            $blueprint->integer('fund_source_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("historical_data_items",function (Blueprint $blueprint){
            $blueprint->dropColumn('fund_source_id');
        });
    }
}
