<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFundAllocatedFromFinancialSystemsTableChangeJeDateToString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_allocated_from_financial_systems', function (Blueprint $blueprint){
           $blueprint->string('JEDate')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_allocated_from_financial_systems', function (Blueprint $blueprint){
            $blueprint->date('JEDate')->change();
        });
    }
}
