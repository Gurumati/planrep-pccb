<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCasAssessmentResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cas_assessment_results', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('period_id');
			$table->integer('mtef_id');
			$table->integer('cas_assessment_state_id');
			$table->integer('cas_assessment_category_version_id');
			$table->integer('highest_available_score');
			$table->integer('score_value');
			$table->integer('user_id');
			$table->integer('admin_hierarchy_level_id');
			$table->boolean('is_selected');
			$table->boolean('is_confirmed');
			$table->string('remarks', 191)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('cas_assessment_round_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cas_assessment_results');
	}

}
