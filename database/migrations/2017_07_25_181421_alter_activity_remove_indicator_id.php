<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityRemoveIndicatorId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('activities',function (Blueprint $table){
           //$table->dropColumn('performance_indicator_id');
           $table->decimal('indicator_value',20)->nullable()->default(null)->change();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities',function (Blueprint $table){
            //$table->integer('performance_indicator_id');
            $table->decimal('indicator_value',20)->default(0)->change();
        });
    }
}
