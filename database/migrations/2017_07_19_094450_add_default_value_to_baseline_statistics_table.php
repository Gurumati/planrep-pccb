<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValueToBaselineStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('baseline_statistics',function (Blueprint $table){
            $table->string('default_value')->nullable();
            $table->boolean('is_common')->default('false');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('baseline_statistics',function (Blueprint $table){
            $table->dropColumn('default_value');
            $table->dropColumn('is_common');
        });
    }
}
