<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUpdatedAtToBudgetSubmissionSelectOptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_submission_select_option', function(Blueprint $blueprint){
            $blueprint->timestamp('updated_at')->nullable();
            $blueprint->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_submission_select_option', function(Blueprint $blueprint){
            $blueprint->dropColumn('updated_at');
            $blueprint->dropColumn('created_at');
        });
    }
}
