<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCasPlanTableColumnsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cas_plan_table_columns', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('code', 191);
			$table->integer('cas_plan_table_id');
			$table->string('name', 191);
			$table->string('type', 191)->nullable();
			$table->integer('cas_plan_table_column_id')->nullable();
			$table->boolean('is_lowest')->default(0);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('sort_order')->nullable();
			$table->integer('cas_group_column_id')->nullable();
			$table->string('formula', 191)->nullable();
			$table->boolean('vertical_total')->nullable();
			$table->boolean('is_list')->nullable();
			$table->string('list_item', 191)->nullable();
			$table->integer('cas_plan_table_select_option_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cas_plan_table_columns');
	}

}
