<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReferenceDocTypeAddIsNationGuideline extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reference_document_types', function (Blueprint $table){
            $table->boolean('is_national_guideline')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reference_document_types', function (Blueprint $table){
            $table->dropColumn('is_national_guideline');
        });
    }
}
