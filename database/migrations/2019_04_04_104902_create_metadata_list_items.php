<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetadataListItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metadata_list_items',function(Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->timestamps();
            $blueprint->text('name');
            $blueprint->string('period')->nullable();
            $blueprint->string('api')->nullable();
            $blueprint->integer('metadata_type_id');
            $blueprint->string('code')->nullable();
            $blueprint->text('sql_query')->nullable();
            $blueprint->string('uid');
            $blueprint->foreign('metadata_type_id')
                      ->references('id')->on('metadata_types')
                      ->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('metadata_list_items');
    }
}
