<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToGroupRightsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('group_rights', function(Blueprint $table)
		{
			$table->foreign('group_id')->references('id')->on('user_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('right_id')->references('id')->on('access_rights')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('group_rights', function(Blueprint $table)
		{
			$table->dropForeign('group_rights_group_id_foreign');
			$table->dropForeign('group_rights_right_id_foreign');
		});
	}

}
