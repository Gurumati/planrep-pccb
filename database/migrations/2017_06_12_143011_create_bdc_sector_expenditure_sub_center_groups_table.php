<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBdcSectorExpenditureSubCenterGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bdc_sector_expenditure_sub_center_groups', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('bdc_group_id');
			$table->integer('bdc_sector_expenditure_sub_centre_id')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bdc_sector_expenditure_sub_center_groups');
	}

}
