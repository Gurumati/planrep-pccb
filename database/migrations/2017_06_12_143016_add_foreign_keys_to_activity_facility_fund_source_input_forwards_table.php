<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActivityFacilityFundSourceInputForwardsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('activity_facility_fund_source_input_forwards', function(Blueprint $table)
		{
			$table->foreign('activity_facility_fund_source_input_id', 'activity_facility_fund_source_input_forwards_activity_facility_')->references('id')->on('activity_facility_fund_source_inputs')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('financial_year_id', 'activity_facility_fund_source_input_forwards_financial_year_id_')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('activity_facility_fund_source_input_forwards', function(Blueprint $table)
		{
			$table->dropForeign('activity_facility_fund_source_input_forwards_activity_facility_');
			$table->dropForeign('activity_facility_fund_source_input_forwards_financial_year_id_');
		});
	}

}
