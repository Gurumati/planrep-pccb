<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetExportAccountIssuesAddIsClearColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_export_account_glaccount_export_issues', function(Blueprint $blueprint){
            $blueprint->boolean('is_cleared')->default('false');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_export_account_glaccount_export_issues', function(Blueprint $blueprint){
            $blueprint->dropColumn('is_cleared');
        });
    }
}
