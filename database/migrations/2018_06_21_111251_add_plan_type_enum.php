<?php

use Illuminate\Database\Migrations\Migration;

class AddPlanTypeEnum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE TYPE budget_types AS ENUM ('CURRENT', 'CARRYOVER','REALLOCATION', 'SUPPLEMENTARY')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP TYPE budget_types");
    }
}
