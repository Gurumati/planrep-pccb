<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBudgetImportItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('budget_import_items', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('budget_import_id');
			$table->integer('budget_export_account_id');
			$table->decimal('amount', 20)->default(0);
			$table->decimal('date_imported');
			$table->integer('period_id');
			$table->boolean('cancelled')->default(0);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('budget_import_items');
	}

}
