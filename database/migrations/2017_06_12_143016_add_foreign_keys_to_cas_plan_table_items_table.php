<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasPlanTableItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cas_plan_table_items', function(Blueprint $table)
		{
			$table->foreign('cas_plan_table_item_id')->references('id')->on('cas_plan_table_items')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cas_plan_table_items', function(Blueprint $table)
		{
			$table->dropForeign('cas_plan_table_items_cas_plan_table_item_id_foreign');
		});
	}

}
