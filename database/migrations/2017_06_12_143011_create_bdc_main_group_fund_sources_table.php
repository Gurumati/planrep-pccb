<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBdcMainGroupFundSourcesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bdc_main_group_fund_sources', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('main_group_id');
			$table->integer('financial_year_id');
			$table->integer('fund_source_id');
			$table->integer('budget_percent')->default(0);
			$table->boolean('is_active')->default(1);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('reference_document_id')->nullable();
			$table->unique(['main_group_id','financial_year_id','fund_source_id'], 'bdc_main_group_fund_sources_main_group_id_financial_year_id_fun');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bdc_main_group_fund_sources');
	}

}
