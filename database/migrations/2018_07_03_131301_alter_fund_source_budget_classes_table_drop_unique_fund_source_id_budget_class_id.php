<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFundSourceBudgetClassesTableDropUniqueFundSourceIdBudgetClassId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("fund_source_budget_classes",function (Blueprint $blueprint){
            $blueprint->dropUnique("fund_source_budget_classes_fund_source_id_budget_class_id_uniqu");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("fund_source_budget_classes",function (Blueprint $blueprint){
            $blueprint->unique(['fund_source_id','budget_class_id']);
        });
    }
}
