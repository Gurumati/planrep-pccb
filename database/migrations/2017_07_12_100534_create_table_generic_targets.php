<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGenericTargets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generic_targets', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('code')->nullable();
            $table->text('description');
            $table->integer('plan_chain_id');
            $table->integer('intervention_id')->nullable();
            $table->integer('section_id')->nullable();
            $table->boolean('is_active')->default(true);
            $table->timestamps();
            $table->softDeletes();
            $table->integer('created_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('plan_chain_id')->references('id')->on('plan_chains')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('intervention_id')->references('id')->on('interventions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('section_id')->references('id')->on('sections')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
        Schema::table('long_term_targets', function(Blueprint $table)
        {
            $table->renameColumn('generic_template_id','generic_target_id');
            $table->foreign('generic_target_id')->references('id')->on('generic_targets')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('generic_targets');
        Schema::table('long_term_targets', function(Blueprint $table)
        {
            $table->renameColumn('generic_target_id','generic_template_id');
        });

    }
}
