<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityPhaseMilestonesTableAddAmountAndPeriod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("activity_phase_milestones",function (Blueprint $table){
            $table->double("amount",20,2)->default(0);
            $table->integer("period_id")->unsigned()->nullable();
            $table->foreign('period_id')->references('id')->on('periods')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("activity_phase_milestones",function (Blueprint $table){
            $table->dropColumn("amount");
            $table->dropColumn("period_id");
        });
    }
}
