<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActivityFacilitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('activity_facilities', function(Blueprint $table)
		{
			$table->foreign('activity_id')->references('id')->on('activities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('facility_id')->references('id')->on('facilities')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('activity_facilities', function(Blueprint $table)
		{
			$table->dropForeign('activity_facilities_activity_id_foreign');
			$table->dropForeign('activity_facilities_facility_id_foreign');
		});
	}

}
