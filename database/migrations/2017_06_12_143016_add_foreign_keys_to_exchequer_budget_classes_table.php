<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExchequerBudgetClassesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('exchequer_budget_classes', function(Blueprint $table)
		{
			$table->foreign('budget_class_id')->references('id')->on('budget_classes')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('exchequer_id')->references('id')->on('exchequers')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('exchequer_budget_classes', function(Blueprint $table)
		{
			$table->dropForeign('exchequer_budget_classes_budget_class_id_foreign');
			$table->dropForeign('exchequer_budget_classes_exchequer_id_foreign');
		});
	}

}
