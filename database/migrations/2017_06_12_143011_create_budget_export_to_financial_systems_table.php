<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBudgetExportToFinancialSystemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('budget_export_to_financial_systems', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('budget_export_transaction_item_id');
			$table->string('chart_of_accounts', 191);
			$table->text('description');
			$table->boolean('is_sent');
			$table->boolean('is_received');
			$table->text('response');
			$table->integer('financial_year_id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('update_by')->nullable();
			$table->decimal('amount', 10);
			$table->string('financial_system_code', 191);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('budget_export_to_financial_systems');
	}

}
