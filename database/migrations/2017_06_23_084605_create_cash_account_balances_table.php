<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashAccountBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_account_balances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cash_account_id')->unsigned();
            $table->integer('admin_hierarchy_id')->unsigned();
            $table->integer('period_id')->unsigned();
            $table->decimal('opening_balance',20,2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_account_balances');
    }
}
