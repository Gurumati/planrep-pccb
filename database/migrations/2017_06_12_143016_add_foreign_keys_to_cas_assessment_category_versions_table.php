<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasAssessmentCategoryVersionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cas_assessment_category_versions', function(Blueprint $table)
		{
			$table->foreign('cas_assessment_category_id', 'cas_assessment_category_versions_cas_assessment_category_id_for')->references('id')->on('cas_assessment_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('reference_document_id')->references('id')->on('reference_documents')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cas_assessment_category_versions', function(Blueprint $table)
		{
			$table->dropForeign('cas_assessment_category_versions_cas_assessment_category_id_for');
			$table->dropForeign('cas_assessment_category_versions_financial_year_id_foreign');
			$table->dropForeign('cas_assessment_category_versions_reference_document_id_foreign');
		});
	}

}
