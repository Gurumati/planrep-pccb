<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTriggerToRefreshVwFinancialYears extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement(
            '
            CREATE OR REPLACE FUNCTION public.trig_refresh_vw_financial_years()
            RETURNS trigger
            LANGUAGE plpgsql
            AS $function$
            BEGIN
               REFRESH MATERIALIZED VIEW CONCURRENTLY vw_financial_years;
               RETURN NULL;
            END;
            $function$;
            '
        );
        DB::statement('
        CREATE  TRIGGER financial_year_change AFTER INSERT OR UPDATE OR DELETE
        ON financial_years
        FOR EACH STATEMENT EXECUTE PROCEDURE trig_refresh_vw_financial_years();
        ');
        DB::statement('
        CREATE TRIGGER period_change AFTER INSERT OR UPDATE OR DELETE
        ON periods
        FOR EACH STATEMENT EXECUTE PROCEDURE trig_refresh_vw_financial_years();
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP TRIGGER financial_year_change on financial_years');
        DB::statement('DROP TRIGGER period_change on periods');
    }
}
