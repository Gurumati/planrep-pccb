<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBdcExpenditureCentreValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bdc_expenditure_centre_values', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('min_value', 191);
			$table->string('max_value', 191);
			$table->integer('financial_year_id');
			$table->integer('expenditure_centre_id');
			$table->boolean('is_active')->default(1);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->unique(['financial_year_id','expenditure_centre_id'], 'bdc_expenditure_centre_values_financial_year_id_expenditure_cen');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bdc_expenditure_centre_values');
	}

}
