<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSectionLevelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('section_levels', function(Blueprint $table)
		{
			$table->foreign('next_budget_level')->references('id')->on('section_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('section_levels', function(Blueprint $table)
		{
			$table->dropForeign('section_levels_next_budget_level_foreign');
		});
	}

}
