<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInputsQuantitiesSetNumeric extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function (){
            Schema::table('activity_facility_fund_source_inputs',function (Blueprint $table){
                $table->decimal('quantity',8,2)->change();
            });
            Schema::table('activity_facility_fund_source_input_forwards',function (Blueprint $table){
                $table->decimal('quantity',8,2)->change();
            });
            Schema::table('activity_facility_fund_source_input_breakdowns',function (Blueprint $table){
                $table->decimal('quantity',8,2)->change();
            });
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function (){
            Schema::table('activity_facility_fund_source_inputs',function (Blueprint $table){
                $table->integer('quantity')->change();
            });
            Schema::table('activity_facility_fund_source_input_forwards',function (Blueprint $table){
                $table->integer('quantity')->change();
            });
            Schema::table('activity_facility_fund_source_input_breakdowns',function (Blueprint $table){
                $table->integer('quantity')->change();
            });
        });
    }
}
