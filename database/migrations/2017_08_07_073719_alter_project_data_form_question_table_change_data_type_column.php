<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectDataFormQuestionTableChangeDataTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_data_form_questions',function (Blueprint $blueprint){
            $blueprint->text('input_type')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_data_form_questions',function (Blueprint $blueprint){
            $blueprint->json('input_type')->change();
        });
    }
}
