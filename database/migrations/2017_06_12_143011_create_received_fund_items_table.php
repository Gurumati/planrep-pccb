<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReceivedFundItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('received_fund_items', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('admin_hierarchy_ceiling_id');
			$table->decimal('amount', 20)->default(0);
			$table->date('date_received');
			$table->string('reference_code', 191)->nullable()->unique();
			$table->integer('period_id');
			$table->integer('received_fund_id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('fund_source_id')->nullable();
			$table->integer('gfs_code_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('received_fund_items');
	}

}
