<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterScrutinizationItemAddDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mtef_section_item_comments',function (Blueprint $table){
            $table->dateTime('date_commented')->nullable();
            $table->dateTime('date_addressed')->nullable();
        });
        Schema::table('mtef_section_comments',function (Blueprint $table){
            $table->dateTime('date_commented')->nullable();
            $table->dateTime('date_addressed')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtef_section_item_comments',function (Blueprint $table){
            $table->dropColumn('date_commented')->nullable();
            $table->dropColumn('date_addressed')->nullable();
        });
        Schema::table('mtef_section_comments',function (Blueprint $table){
            $table->dropColumn('date_commented')->nullable();
            $table->dropColumn('date_addressed')->nullable();
        });
    }
}
