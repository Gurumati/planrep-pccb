<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFundAllocatedTransactionIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fund_allocated_transaction_issues', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('budget_import_issue_type_id')->unsigned();
            $table->integer('fund_allocated_transaction_item_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fund_allocated_transaction_issues');
    }
}
