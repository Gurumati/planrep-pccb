<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetImportItemsTableMakeDateImportedDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_import_items', function(Blueprint $blueprint){
           $blueprint->dropColumn('date_imported');
        });

        Schema::table('budget_import_items', function(Blueprint $blueprint){
            $blueprint->date('date_imported');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_import_items', function(Blueprint $blueprint){
            $blueprint->dropColumn('date_imported');
        });

        Schema::table('budget_import_items', function(Blueprint $blueprint){
            $blueprint->integer('date_imported');
        });
    }
}
