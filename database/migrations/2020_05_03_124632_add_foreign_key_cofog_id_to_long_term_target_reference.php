<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyCofogIdToLongTermTargetReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('long_term_target_references', function(Blueprint $table)
        {
           
            $table->integer('reference_id')->nullable()->change();
            $table->integer('cofog_id')->nullable();
            $table->foreign('cofog_id')->references('id')->on('cofogs')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('cofog_id');
        
        Schema::table("long_term_target_references",function (Blueprint $table){
            $table->dropColumn('cofog_id');
        });
    }
}
