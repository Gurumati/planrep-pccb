<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasPlanTableReportConstraintsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cas_plan_table_report_constraints', function(Blueprint $table)
		{
			$table->foreign('cas_plan_table_id')->references('id')->on('cas_plan_tables')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cas_plan_table_report_constraints', function(Blueprint $table)
		{
			$table->dropForeign('cas_plan_table_report_constraints_cas_plan_table_id_foreign');
		});
	}

}
