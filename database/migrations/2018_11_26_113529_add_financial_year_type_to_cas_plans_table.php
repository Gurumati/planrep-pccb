<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinancialYearTypeToCasPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("cas_plans",function (Blueprint $blueprint){
            $blueprint->string("financial_year_type")->default('planning');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("cas_plans",function (Blueprint $blueprint){
            $blueprint->dropColumn('financial_year_type');
        });
    }
}
