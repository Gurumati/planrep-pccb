<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActivityReferencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('activity_references', function(Blueprint $table)
		{
			$table->foreign('activity_id', 'act_gen_line_items_activity_id_foreign')->references('id')->on('activities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('reference_id')->references('id')->on('reference_docs')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('activity_references', function(Blueprint $table)
		{
			$table->dropForeign('act_gen_line_items_activity_id_foreign');
			$table->dropForeign('activity_references_reference_id_foreign');
		});
	}

}
