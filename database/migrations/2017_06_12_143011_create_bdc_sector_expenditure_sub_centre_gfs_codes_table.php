<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBdcSectorExpenditureSubCentreGfsCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bdc_sector_expenditure_sub_centre_gfs_codes', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('bdc_sector_expenditure_sub_centre_id');
			$table->integer('gfs_code_id');
			$table->timestamps();
			$table->softDeletes();
			$table->unique(['bdc_sector_expenditure_sub_centre_id','gfs_code_id'], 'bdc_sector_expenditure_sub_centre_gfs_codes_bdc_sector_expendit');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bdc_sector_expenditure_sub_centre_gfs_codes');
	}

}
