<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFundSourceVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fund_source_versions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("fund_source_id")->unsigned();
            $table->integer("version_id")->unsigned();
            $table->unique(['fund_source_id','version_id']);
            $table->foreign('fund_source_id')->references('id')->on('fund_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('version_id')->references('id')->on('versions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fund_source_versions');
    }
}
