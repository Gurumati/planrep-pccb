<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropForeignKeyProjectDataFormQuestionIdOnProjectDataFormQuestionItemValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_data_form_question_item_values',function (Blueprint $blueprint){
            $blueprint->dropForeign('project_data_form_question_item_values_project_data_form_questi');
        });

        Schema::table('project_data_form_question_item_values',function (Blueprint $blueprint){
            $blueprint->foreign('project_data_form_question_id')->references('id')->on('project_data_form_questions')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_data_form_question_item_values',function (Blueprint $blueprint){
            $blueprint->foreign('project_data_form_question_id')->references('id')->on('project_data_forms')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }
}
