<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetReallocationItemsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_reallocation_items',function (Blueprint $blueprint){
            $blueprint->decimal('amount',20,2)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_reallocation_items',function (Blueprint $blueprint){
            $blueprint->decimal('amount',12,2)->change();
        });
    }
}
