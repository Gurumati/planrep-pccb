<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReceivedFundItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('received_fund_items', function(Blueprint $table)
		{
			$table->foreign('admin_hierarchy_ceiling_id')->references('id')->on('admin_hierarchy_ceilings')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('period_id')->references('id')->on('periods')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('received_fund_id')->references('id')->on('received_funds')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('received_fund_items', function(Blueprint $table)
		{
			$table->dropForeign('received_fund_items_admin_hierarchy_ceiling_id_foreign');
			$table->dropForeign('received_fund_items_period_id_foreign');
			$table->dropForeign('received_fund_items_received_fund_id_foreign');
		});
	}

}
