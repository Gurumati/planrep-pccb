<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExchequerBudgetClassSectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('exchequer_budget_class_sections', function(Blueprint $table)
		{
			$table->foreign('exch_budget_class_id')->references('id')->on('exchequer_budget_classes')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('exchequer_budget_class_sections', function(Blueprint $table)
		{
			$table->dropForeign('exchequer_budget_class_sections_exch_budget_class_id_foreign');
		});
	}

}
