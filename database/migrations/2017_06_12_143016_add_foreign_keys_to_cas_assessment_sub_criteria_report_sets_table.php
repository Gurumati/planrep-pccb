<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasAssessmentSubCriteriaReportSetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cas_assessment_sub_criteria_report_sets', function(Blueprint $table)
		{
			$table->foreign('cas_assessment_sub_criteria_option_id', 'cas_assessment_sub_criteria_report_sets_cas_assessment_sub_crit')->references('id')->on('cas_assessment_sub_criteria_options')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cas_assessment_sub_criteria_report_sets', function(Blueprint $table)
		{
			$table->dropForeign('cas_assessment_sub_criteria_report_sets_cas_assessment_sub_crit');
		});
	}

}
