<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetExportAccountAddLockedColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_export_accounts',function (Blueprint $table){
            $table->boolean('is_locked')->unsigned()->default(false);
            $table->text('comments')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_export_accounts',function (Blueprint $table){
            $table->dropColumn('is_locked');
            $table->dropColumn('comments');
        });
    }
}
