<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTableAdminHiearchyCeilingForwards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_hierarchy_ceiling_forwards',function (Blueprint $table){
            $table->increments('id');
            $table->integer('admin_hierarchy_ceiling_id');
            $table->decimal('amount',20,2);
            $table->integer('financial_year_id');
            $table->integer('financial_year_order');
            $table->foreign("admin_hierarchy_ceiling_id")->references("id")->on("admin_hierarchy_ceilings")->onDelete("RESTRICT")->onUpdate("CASCADE");
            $table->foreign("financial_year_id")->references("id")->on("financial_years")->onDelete("RESTRICT")->onUpdate("CASCADE");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("admin_hierarchy_ceiling_forwards");
    }
}
