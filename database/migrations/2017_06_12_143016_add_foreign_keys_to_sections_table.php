<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sections', function(Blueprint $table)
		{
			$table->foreign('parent_id')->references('id')->on('sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('section_level_id')->references('id')->on('section_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('sector_id')->references('id')->on('sectors')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sections', function(Blueprint $table)
		{
			$table->dropForeign('sections_parent_id_foreign');
			$table->dropForeign('sections_section_level_id_foreign');
			$table->dropForeign('sections_sector_id_foreign');
		});
	}

}
