<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFundAllocatedToFinancialSystemsRenameDebitCreditAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_allocated_from_financial_systems', function (Blueprint $blueprint){
            $blueprint->dropColumn('DebitAmount');
            $blueprint->dropColumn('CreditAmount');
        });

        Schema::table('fund_allocated_from_financial_systems', function (Blueprint $blueprint){
            $blueprint->double('debit_amount',12,2)->nullable();
            $blueprint->double('credit_amount',12,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_allocated_from_financial_systems', function (Blueprint $blueprint){
            $blueprint->dropColumn('debit_amount');
            $blueprint->dropColumn('credit_amount');
        });

        Schema::table('fund_allocated_from_financial_systems', function (Blueprint $blueprint){
            $blueprint->double('DebitAmount',12,2)->nullable();
            $blueprint->double('CreditAmount',12,2)->nullable();
        });
    }
}
