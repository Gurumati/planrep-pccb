<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBudgetImportIssuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('budget_import_issues', function(Blueprint $table)
		{
			$table->foreign('budget_import_item_id')->references('id')->on('budget_import_items')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('issue_type_id')->references('id')->on('budget_import_issue_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('budget_import_issues', function(Blueprint $table)
		{
			$table->dropForeign('budget_import_issues_budget_import_item_id_foreign');
			$table->dropForeign('budget_import_issues_issue_type_id_foreign');
		});
	}

}
