<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBudgetReallocationResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_reallocation_responses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('budget_reallocation_item_id');
            $table->boolean('is_approved')->nullable();
            $table->string('response')->nullable();
            $table->foreign('budget_reallocation_item_id')->references('id')->on('budget_reallocation_items')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_reallocation_responses');
    }
}
