<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanningMatrixAdminhierarchies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planning_matrix_adminhierarchies', function(Blueprint $table){
            $table->integer('planning_matrix_id')->unsigned();
            $table->integer('admin_hierarchy_id')->unsigned();
            $table->foreign('planning_matrix_id')->references('id')->on('planning_matrices')->onUpdate('CASCADE')->ondelet('RESTRICT');
            $table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropTable('planning_matrix_adminhierarchies');
    }
}
