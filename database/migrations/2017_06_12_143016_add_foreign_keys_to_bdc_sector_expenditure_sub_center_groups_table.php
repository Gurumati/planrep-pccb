<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBdcSectorExpenditureSubCenterGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bdc_sector_expenditure_sub_center_groups', function(Blueprint $table)
		{
			$table->foreign('bdc_group_id')->references('id')->on('bdc_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('bdc_sector_expenditure_sub_centre_id', 'bdc_sector_expenditure_sub_center_groups_bdc_sector_expenditure')->references('id')->on('bdc_sector_expenditure_sub_centres')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bdc_sector_expenditure_sub_center_groups', function(Blueprint $table)
		{
			$table->dropForeign('bdc_sector_expenditure_sub_center_groups_bdc_group_id_foreign');
			$table->dropForeign('bdc_sector_expenditure_sub_center_groups_bdc_sector_expenditure');
		});
	}

}
