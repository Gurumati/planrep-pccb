<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_balances',function (Blueprint $blueprint){
           $blueprint->bigIncrements('id');
           $blueprint->timestamps();
           $blueprint->integer('ffars_id');
           $blueprint->string('chart_of_accounts');
           $blueprint->date('JEDate');
           $blueprint->float('debit_amount')->nullable();
           $blueprint->float('credit_amount')->nullable();
           $blueprint->integer('gfs_code_id')->references('id')->on('gfs_codes')->onUpdate('cascade')->onDelete('restrict');
           $blueprint->integer('fund_source_id')->references('id')->on('fund_sources')->onUpdate('cascade')->onDelete('restrict');
           $blueprint->integer('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('cascade')->onDelete('restrict');
           $blueprint->integer('budget_class_id')->references('id')->on('budget_classes')->onUpdate('cascade')->onDelete('restrict');
           $blueprint->integer('financial_year_id')->references('id')->on('financial_years')->onUpdate('cascade')->onDelete('restrict');
           $blueprint->integer('section_id')->references('id')->on('sections')->onUpdate('cascade')->onDelete('restrict');
           $blueprint->integer('facility_id')->references('id')->on('facilities')->onUpdate('cascade')->onDelete('restrict');
           $blueprint->boolean('is_successful')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('account_balances');
    }
}
