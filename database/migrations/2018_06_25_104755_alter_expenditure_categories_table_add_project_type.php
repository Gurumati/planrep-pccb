<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExpenditureCategoriesTableAddProjectType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("expenditure_categories",function (Blueprint $blueprint){
            $blueprint->integer("project_type_id")->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("expenditure_categories",function (Blueprint $blueprint){
            $blueprint->dropColumn("project_type_id");
        });
    }
}
