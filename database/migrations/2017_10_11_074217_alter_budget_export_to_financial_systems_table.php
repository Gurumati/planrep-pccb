<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetExportToFinancialSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_export_to_financial_systems',function (Blueprint $table){
            $table->integer('budget_export_transaction_item_id')->nullable()->change();
            $table->integer('revenue_export_transaction_item_id')->nullable();
            $table->foreign('revenue_export_transaction_item_id')->references('id')->on('revenue_export_transaction_items')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_export_to_financial_systems',function (Blueprint $table){
            $table->integer('budget_export_transaction_item_id')->change();
            $table->dropColumn('revenue_export_transaction_item_id');
        });
    }
}
