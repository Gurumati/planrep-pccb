<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActivityInputsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('activity_inputs', function(Blueprint $table)
		{
			$table->foreign('activity_id')->references('id')->on('activities')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('gfs_code_id')->references('id')->on('gfs_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('procurement_type_id')->references('id')->on('procurement_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('unit_id')->references('id')->on('units')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('activity_inputs', function(Blueprint $table)
		{
			$table->dropForeign('activity_inputs_activity_id_foreign');
			$table->dropForeign('activity_inputs_gfs_code_id_foreign');
			$table->dropForeign('activity_inputs_procurement_type_id_foreign');
			$table->dropForeign('activity_inputs_unit_id_foreign');
		});
	}

}
