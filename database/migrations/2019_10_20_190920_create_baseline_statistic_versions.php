<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaselineStatisticVersions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('baseline_statistic_versions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("baseline_statistic_id")->unsigned();
            $table->integer("version_id")->unsigned();
            $table->unique(['baseline_statistic_id','version_id']);
            $table->foreign('baseline_statistic_id')->references('id')->on('baseline_statistics')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('version_id')->references('id')->on('versions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("baseline_statistic_versions");
    }
}
