<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivityFacilityFundSourceInputBreakdownsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activity_facility_fund_source_input_breakdowns', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('activity_facility_fund_source_input_id');
			$table->string('item', 191);
			$table->integer('unit_id');
			$table->integer('quantity');
			$table->integer('frequency');
			$table->decimal('unit_price', 20);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('update_by')->nullable();
			$table->integer('budget_submission_definition_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activity_facility_fund_source_input_breakdowns');
	}

}
