<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCasAssessmentResultDetailRepliesAddStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cas_assessment_result_replies',function (Blueprint $blueprint){
            $blueprint->foreign('updated_by')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_assessment_result_replies', function (Blueprint $table) {
            $table->dropForeign('cas_assessment_result_replies_updated_by_foreign');
        });
    }
}
