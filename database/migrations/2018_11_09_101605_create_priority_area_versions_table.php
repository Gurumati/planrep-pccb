<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePriorityAreaVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('priority_area_versions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("priority_area_id")->unsigned();
            $table->integer("version_id")->unsigned();
            $table->unique(['priority_area_id','version_id']);
            $table->foreign('priority_area_id')->references('id')->on('priority_areas')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('version_id')->references('id')->on('versions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('priority_area_versions');
    }
}
