<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBodVersionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bod_versions', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('start_financial_year_id');
			$table->integer('end_financial_year_id')->nullable();
			$table->string('reference_doc', 191)->nullable();
			$table->string('bod_value', 191);
			$table->integer('bod_list_id');
			$table->boolean('active')->default(1);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bod_versions');
	}

}
