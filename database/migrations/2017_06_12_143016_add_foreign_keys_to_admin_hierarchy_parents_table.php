<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAdminHierarchyParentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('admin_hierarchy_parents', function(Blueprint $table)
		{
			$table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('parent_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('admin_hierarchy_parents', function(Blueprint $table)
		{
			$table->dropForeign('admin_hierarchy_parents_admin_hierarchy_id_foreign');
			$table->dropForeign('admin_hierarchy_parents_parent_id_foreign');
		});
	}

}
