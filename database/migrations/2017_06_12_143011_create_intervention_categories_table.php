<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInterventionCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('intervention_categories', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 191);
			$table->text('description');
			$table->text('footer_text')->nullable();
			$table->integer('sort_order');
			$table->integer('intervention_category_id')->nullable();
			$table->boolean('is_active')->default(1);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('intervention_categories');
	}

}
