<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCoaSegmentsTableSetDefaultValueForIsSentAndIsDelivered extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coa_segments', function (Blueprint $table) {
            $table->boolean('is_sent')->default(false)->change();
            $table->boolean('is_delivered')->default(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coa_segments', function (Blueprint $table) {
            $table->boolean('is_sent')->change();
            $table->boolean('is_delivered')->change();
        });
    }
}
