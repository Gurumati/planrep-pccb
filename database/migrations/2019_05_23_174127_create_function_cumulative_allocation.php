<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionCumulativeAllocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
        create or replace function func_cumulative_allocation (period_code varchar, admin_hierarchy int, financial_year int)
        returns table (
            financial_year_id int,
            admin_hierarchy_id int,
            section_id int,
            activity_id int,
            budget_class_id int,
            fund_source_id int,
            gfs_id int,
            facility_id int,
            budget_type varchar,
            amount numeric
        )
        as $$
        begin
        return query
        SELECT
                fy_id,
                admin_id,
                cc_id,
                ac_id,
                bc_id,
                f_id,
                gfs,
                fa_id,
                bt,
                alloc
        from
            (
            select
                fy.id as fy_id,
                ah.id as admin_id,
                s.id as cc_id,
                ac.id as ac_id,
                bc.id as bc_id,
                fs.id as f_id,
                gfs.id as gfs,
                f.id as fa_id,
                ac.budget_type as bt,
                sum(i.debit_amount::numeric(15,2)) - sum(i.credit_amount::numeric(15,2)) as alloc
            from
                fund_allocated_transaction_items i join
                budget_export_accounts a on a.id = i.budget_export_account_id join
                activity_facility_fund_source_inputs ai on ai.id = a.activity_facility_fund_source_input_id join
                activity_facility_fund_sources aff on aff.id = ai.activity_facility_fund_source_id join
                activity_facilities af on af.id = aff.activity_facility_id join
                activities  ac on ac.id = af.activity_id join
                mtef_sections ms on ms.id = ac.mtef_section_id join
                mtefs m on m.id = ms.mtef_id join
                budget_classes bc on bc.id = ac.budget_class_id join
                fund_sources fs on fs.id = aff.fund_source_id join
                financial_years fy on fy.id = m.financial_year_id join
                admin_hierarchies ah on ah.id = m.admin_hierarchy_id join
                facilities f on f.id = af.facility_id join
                sections s on s.id = ms.section_id join
                gfs_codes gfs on gfs.id = ai.gfs_code_id join
                periods p on p.id = i.period_id
            where ah.id = admin_hierarchy and fy.id = financial_year
            and p.code = ANY (case period_code when \'Q2\' then ARRAY[\'Q1\', \'Q2\'] when \'Q3\' then ARRAY[\'Q1\', \'Q2\', \'Q3\'] when \'Q4\' then ARRAY[\'Q1\', \'Q2\',\'Q3\',\'Q4\']
        else ARRAY[\'Q1\'] end)    
        group by
        fy.id,
        ah.id,
        s.id,
        ac.id,
        bc.id,
        fs.id,
        gfs.id,
        ac.budget_type,
        f.id

        UNION ALL

        select
            fy.id as fy_id,
            ah.id as admin_id,
            s.id as cc_id,
            ac.id as ac_id,
            bc.id as bc_id,
            fs.id as f_id,
            gfs.id as gfs,
            f.id as fa_id,
            ac.budget_type as bt,
            sum(i.debit_amount::numeric(15,2)) - sum(i.credit_amount::numeric(15,2)) as alloc
        from
            budget_import_items i join
            budget_classes bc on bc.id = i.budget_class_id join
            fund_sources fs on fs.id = i.fund_source_id join
            gfs_codes gfs on gfs.id = i.gfs_code_id join
            periods p on p.id = i.period_id join
            budget_export_accounts a on a.id = i.budget_export_account_id join
            financial_years fy on fy.id = a.financial_year_id join
            admin_hierarchies ah on ah.id = a.admin_hierarchy_id join
            activities  ac on ac.id = a.activity_id join
            activity_facilities af on af.activity_id = ac.id  join
            facilities f on f.id = af.facility_id join
            mtef_sections ms on ms.id = ac.mtef_section_id join
            sections s on s.id = ms.section_id
        where ac.is_facility_account = true and f.facility_code != \'00000000\' and ah.id = admin_hierarchy and fy.id = financial_year
        and p.code = ANY (case period_code when \'Q2\' then ARRAY[\'Q1\', \'Q2\'] when \'Q3\' then ARRAY[\'Q1\', \'Q2\', \'Q3\'] when \'Q4\' then ARRAY[\'Q1\', \'Q2\',\'Q3\',\'Q4\']
        else ARRAY[\'Q1\'] end) 
        group by
            fy.id,
            ah.id,
            s.id,
            ac.id,
            bc.id,
            fs.id,
            gfs.id,
            ac.budget_type,
            f.id
        ) AS x  ;
        end;$$
        LANGUAGE \'plpgsql\';
                ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop function func_cumulative_allocation');
    }
}
