<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFfarsActivitiesTableAddAdminHierarchyId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("ffars_activities",function (Blueprint $blueprint){
            $blueprint->integer("admin_hierarchy_id")->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("ffars_activities",function (Blueprint $blueprint){
            $blueprint->dropColumn("admin_hierarchy_id");
        });
    }
}
