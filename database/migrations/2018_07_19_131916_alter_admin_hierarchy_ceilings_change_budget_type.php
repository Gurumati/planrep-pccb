<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAdminHierarchyCeilingsChangeBudgetType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('admin_hierarchy_ceilings',function (Blueprint $table){
           $table->dropColumn('budget_type');
       });
       Schema::table('admin_hierarchy_ceilings',function (Blueprint $table){
           $table->string('budget_type')->default('CURRENT');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
