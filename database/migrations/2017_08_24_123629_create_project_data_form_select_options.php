<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectDataFormSelectOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_data_form_select_options',function(Blueprint $table){
            $table->increments('id');
            $table->integer('project_data_form_question_id')->unsigned();
            $table->string('label');
            $table->text('option_value');
            $table->timestamps();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->foreign('project_data_form_question_id')->references('id')->on('project_data_form_questions')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_data_form_select_options');
    }
}
