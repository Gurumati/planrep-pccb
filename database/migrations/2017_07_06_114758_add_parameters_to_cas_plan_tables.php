<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParametersToCasPlanTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cas_plan_tables',function (Blueprint $blueprint){
            $blueprint->string('parameters')->nullable();
            $blueprint->boolean('is_html')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_plan_tables',function (Blueprint $blueprint){
            $blueprint->dropColumn('parameters');
            $blueprint->dropColumn('is_html');
        });
    }
}
