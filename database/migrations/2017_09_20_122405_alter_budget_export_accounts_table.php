<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetExportAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_export_accounts',function (Blueprint $table){
            $table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('section_id')->references('id')->on('sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('activity_facility_fund_source_input_id')->references('id')->on('activity_facility_fund_source_inputs')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_export_accounts',function (Blueprint $table){
            $table->dropForeign('budget_export_accounts_activity_facility_fund_source_input_id_f');
            $table->dropForeign('budget_export_accounts_admin_hierarchy_id_foreign');
            $table->dropForeign('budget_export_accounts_financial_year_id_foreign');
            $table->dropForeign('budget_export_accounts_section_id_foreign');
        });
    }
}
