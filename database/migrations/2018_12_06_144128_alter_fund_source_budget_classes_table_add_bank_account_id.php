<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFundSourceBudgetClassesTableAddBankAccountId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("fund_source_budget_classes",function (Blueprint $blueprint){
            $blueprint->integer("bank_account_id")->nullable()->unsigned();
            $blueprint->foreign('bank_account_id')->references('id')->on('bank_accounts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("fund_source_budget_classes",function (Blueprint $blueprint){
            $blueprint->dropForeign("fund_source_budget_classes_bank_account_id_foreign");
            $blueprint->dropColumn("bank_account_id");
        });
    }
}
