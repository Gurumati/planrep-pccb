<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGenericTargetsTble extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("generic_targets",function (Blueprint $blueprint){
            $blueprint->integer('priority_area_id')->nullable()->change();
            $blueprint->integer('generic_sector_problem_id')->nullable()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("generic_targets",function (Blueprint $blueprint){
            $blueprint->dropColumn('generic_sector_problem_id');
            $blueprint->integer('priority_area_id')->unsigned()->change();
        });
    }
}
