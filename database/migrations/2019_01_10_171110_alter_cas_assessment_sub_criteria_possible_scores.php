<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCasAssessmentSubCriteriaPossibleScores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cas_assessment_sub_criteria_possible_scores',function (Blueprint $blueprint){
            $blueprint->decimal('value')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_assessment_sub_criteria_possible_scores',function (Blueprint $blueprint){
            $blueprint->integer('value');
        });
    }
}
