<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectBudgetExportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //build table structure
        Schema::create('project_budget_exports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->decimal('amount', 20)->default(0);
            $table->integer('admin_hierarchy_id')->nullable();
            $table->integer('financial_year_id')->nullable();
            $table->string('system_code');
            $table->boolean('is_sent')->default(false);
            $table->boolean('is_delivered')->default(false);
            $table->text('response')->nullable();
            $table->text('transaction_type')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //here drop the table
        Schema::dropIfExists('project_budget_exports');

    }
}
