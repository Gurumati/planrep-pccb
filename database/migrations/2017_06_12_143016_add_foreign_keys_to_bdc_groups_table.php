<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBdcGroupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bdc_groups', function(Blueprint $table)
		{
			$table->foreign('bdc_main_group_fund_source_id')->references('id')->on('bdc_main_group_fund_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bdc_groups', function(Blueprint $table)
		{
			$table->dropForeign('bdc_groups_bdc_main_group_fund_source_id_foreign');
			$table->dropForeign('bdc_groups_financial_year_id_foreign');
		});
	}

}
