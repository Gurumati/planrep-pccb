<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionCumulativeRevenue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('create or replace function func_cumulative_revenue(period_code varchar, admin_hierarchy int, financial_year int)
        returns table (
          financial_year_id int,
          admin_hierarchy_id int,
          section_id int,
          budget_class_id int,
          facility_id int,
          fund_source_id int,
          gfs_id int,
          amount numeric
        )
        as $$
        begin
        return query
      SELECT
       fy_id,
       ah_id,
       cc_id,
       bc_id,
       fac_id,
       fs_id,
       gc_id,
       revenue 
      FROM
        (select
           fy.id as fy_id,
           ah.id as ah_id,
           s2.id as cc_id,
           bc.id as bc_id,
           f.id  as fac_id,
           fs.id as fs_id,
           gc.id as gc_id,
           (sum(rfi.credit_amount::numeric(15,2)) - sum(rfi.debit_amount::numeric(15,2))) as revenue
         from
           received_fund_items rfi
             join revenue_export_accounts rea on rea.id = rfi.revenue_export_account_id
             join financial_years fy on rea.financial_year_id = fy.id
             join admin_hierarchies ah on rea.admin_hierarchy_id = ah.id
             join gfs_codes gc on rea.gfs_code_id = gc.id
             join periods p on p.id = rfi.period_id
             join fund_sources fs on rfi.fund_source_id = fs.id
             join sections s2 on rea.section_id = s2.id
             join sections s3 on s2.parent_id = s3.id
             join sections s on s3.parent_id = s.id
             join admin_hierarchy_ceilings ahc on rea.admin_hierarchy_ceiling_id = ahc.id
             join ceilings c on c.id = ahc.ceiling_id
             join budget_classes bc on c.budget_class_id = bc.id
             left join facilities f on ahc.facility_id = f.id
        where ah.id = admin_hierarchy and fy.id = financial_year
            and p.code = ANY (case period_code when \'Q2\' then ARRAY[\'Q1\', \'Q2\'] when \'Q3\' then ARRAY[\'Q1\', \'Q2\', \'Q3\'] when \'Q4\' then ARRAY[\'Q1\', \'Q2\',\'Q3\',\'Q4\']
        else ARRAY[\'Q1\'] end)
         group by
           fy.id,
           ah.id,
           s2.id,
           bc.id,
           f.id,
           fs.id,
           gc.id
           
        ) as x;
        end; $$
        LANGUAGE \'plpgsql\';');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('Drop function func_cumulative_revenue');
    }
}
