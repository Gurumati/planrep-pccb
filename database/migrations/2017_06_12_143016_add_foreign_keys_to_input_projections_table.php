<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToInputProjectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('input_projections', function(Blueprint $table)
		{
			$table->foreign('activity_input_id')->references('id')->on('activity_inputs')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('input_projections', function(Blueprint $table)
		{
			$table->dropForeign('input_projections_activity_input_id_foreign');
		});
	}

}
