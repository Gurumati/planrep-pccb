<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBudgetImportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('budget_imports', function(Blueprint $table)
		{
			$table->foreign('data_source_id')->references('id')->on('data_sources')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('import_method_id')->references('id')->on('import_methods')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('budget_imports', function(Blueprint $table)
		{
			$table->dropForeign('budget_imports_data_source_id_foreign');
			$table->dropForeign('budget_imports_import_method_id_foreign');
		});
	}

}
