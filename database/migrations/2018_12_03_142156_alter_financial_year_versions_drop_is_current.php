<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFinancialYearVersionsDropIsCurrent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("financial_year_versions",function (Blueprint $blueprint){
            $blueprint->dropColumn('is_current');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("financial_year_versions",function (Blueprint $blueprint){
            $blueprint->boolean('is_current')->default(false);
        });
    }
}
