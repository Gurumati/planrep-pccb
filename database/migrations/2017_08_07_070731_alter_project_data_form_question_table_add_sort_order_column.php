<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectDataFormQuestionTableAddSortOrderColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_data_form_questions',function(Blueprint $blueprint){
            $blueprint->integer('sort_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_data_form_questions',function(Blueprint $blueprint){
            $blueprint->dropColumn('sort_order');
        });
    }
}
