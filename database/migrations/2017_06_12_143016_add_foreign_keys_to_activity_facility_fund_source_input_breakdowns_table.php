<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActivityFacilityFundSourceInputBreakdownsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('activity_facility_fund_source_input_breakdowns', function(Blueprint $table)
		{
			$table->foreign('activity_facility_fund_source_input_id',
                'activity_facility_fund_source_input_breakdowns_activity_facilit')->references('id')->on('activity_facility_fund_source_inputs')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('budget_submission_definition_id', 'activity_facility_fund_source_input_breakdowns_budget_submissio')->references('id')->on('budget_submission_definitions')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('unit_id')->references('id')->on('units')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('activity_facility_fund_source_input_breakdowns', function(Blueprint $table)
		{
			$table->dropForeign('activity_facility_fund_source_input_breakdowns_activity_facilit');
			$table->dropForeign('activity_facility_fund_source_input_breakdowns_budget_submissio');
			$table->dropForeign('activity_facility_fund_source_input_breakdowns_unit_id_foreign');
		});
	}

}
