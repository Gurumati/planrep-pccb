<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetImportItemsTableRenameColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_import_items', function (Blueprint $blueprint){
            $blueprint->dropColumn('amount');
            $blueprint->dropColumn('CreditAmount');
        });

        Schema::table('budget_import_items', function (Blueprint $blueprint){
            $blueprint->double('debit_amount',12,2)->nullable();
            $blueprint->double('credit_amount',12,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_import_items', function (Blueprint $blueprint){
            $blueprint->dropColumn('debit_amount');
            $blueprint->dropColumn('credit_amount');
        });

        Schema::table('budget_import_items', function (Blueprint $blueprint){
            $blueprint->double('DebitAmount',12,2)->nullable();
            $blueprint->double('CreditAmount',12,2)->nullable();
        });
    }
}
