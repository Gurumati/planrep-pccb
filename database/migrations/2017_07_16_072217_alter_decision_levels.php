<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDecisionLevels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('decision_levels',function (Blueprint $table){
            $table->integer('admin_hierarchy_level_position')->nullable();
            $table->integer('section_level_id')->nullable();
            $table->dropColumn('parent_id');
            $table->foreign('section_level_id')->references('id')->on('section_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::create('decision_level_next_decisions',function (Blueprint $table){
            $table->increments('id');
            $table->integer('decision_level_id');
            $table->integer('next_level_id');
            $table->foreign('decision_level_id')->references('id')->on('decision_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('next_level_id')->references('id')->on('decision_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('decision_level_next_decisions');
        Schema::table('decision_levels',function (Blueprint $table){
            $table->dropColumn('admin_hierarchy_level_position');
            $table->dropColumn('section_level_id');
            $table->integer('parent_id');
            $table->foreign('parent_id')->references('id')->on('decision_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }
}
