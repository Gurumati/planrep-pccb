<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasAssessmentCategoryVersionStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cas_assessment_category_version_states', function(Blueprint $table)
		{
			$table->foreign('cas_assessment_category_version_id', 'cas_assessment_category_version_states_cas_assessment_category_')->references('id')->on('cas_assessment_category_versions')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('cas_assessment_state_id', 'cas_assessment_category_version_states_cas_assessment_state_id_')->references('id')->on('cas_assessment_states')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cas_assessment_category_version_states', function(Blueprint $table)
		{
			$table->dropForeign('cas_assessment_category_version_states_cas_assessment_category_');
			$table->dropForeign('cas_assessment_category_version_states_cas_assessment_state_id_');
		});
	}

}
