<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMtefSectionCommentsAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mtef_section_item_comments',function (Blueprint $table){
            $table->renameColumn('comment','comments');
            $table->string('decision_level')->nullable();

        });
        Schema::table('mtef_section_comments',function (Blueprint $table){
            $table->string('commented_by')->nullable();
            $table->string('decision_level')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtef_section_item_comments',function (Blueprint $table){
            $table->renameColumn('comments','comment');
            $table->dropColumn('decision_level');

        });
        Schema::table('mtef_section_comments',function (Blueprint $table){
            $table->dropColumn('commented_by');
            $table->dropColumn('decision_level');
        });
    }
}
