<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHdrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('historical_data_rows',function (Blueprint $blueprint){
            $blueprint->foreign('historical_data_table_id')->references('id')->on('historical_data_tables')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historical_data_columns',function (Blueprint $blueprint){
            $blueprint->dropForeign("historical_data_rows_historical_data_table_id_foreign");
        });
    }
}
