<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCasPlanTablesTableAddParentId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cas_plan_tables',function (Blueprint $blueprint){
            $blueprint->integer('parent_id')->unsigned()->nullable();
            $blueprint->foreign('parent_id')->references('id')->on('cas_plan_tables')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cas_plan_tables',function (Blueprint $blueprint){
            $blueprint->dropForeign('parent_id');
        });

        Schema::table('cas_plan_tables',function (Blueprint $blueprint){
            $blueprint->dropColumn('parent_id');
        });
    }
}
