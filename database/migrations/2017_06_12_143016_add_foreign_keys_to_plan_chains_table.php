<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPlanChainsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('plan_chains', function(Blueprint $table)
		{
			$table->foreign('parent_id')->references('id')->on('plan_chains')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('plan_chain_type_id')->references('id')->on('plan_chain_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('plan_chains', function(Blueprint $table)
		{
			$table->dropForeign('plan_chains_parent_id_foreign');
			$table->dropForeign('plan_chains_plan_chain_type_id_foreign');
		});
	}

}
