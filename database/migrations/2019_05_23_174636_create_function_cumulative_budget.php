<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionCumulativeBudget extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('
        create or replace function func_cumulative_budget (period_code varchar, admin_hierarchy int, financial_year int)
        returns table (
          admin_hierarchy_id int,
          admin_hierarchy_name varchar,
          financial_year_name varchar,
          financial_year_id int,
          facility_id int,
          facility_name varchar,
          cost_centre_id int,
          cost_centre_name varchar,
          cost_centre_code varchar,
          fund_source_id int,
          fund_source_name varchar,
          fund_source_code varchar,
          sub_budget_class_id int,
          sub_budget_class_name varchar,
          gfs_code_id int,
          gfs_code_name varchar,
          gfs_code varchar,
          budget_type varchar,
          cumulative_budget numeric
        )
        as $$
        begin
        return query
        SELECT 
          ah.id as admin_hierarchy_id,
          ah.name as admin_hierarchy_name,
          fy.name as financial_year_name,
          fy.id as financial_year,
          fac.id as facility_id,
          fac.name as facility_name,
          s.id as cost_centre_id,
          s.name as cost_centre_name,
          s.code as cost_centre_code,
          f.id as fund_source_id,
          f.name as fund_source_name,
          f.code as fund_source_code,
          bc.id as sub_budget_class_id,
          bc.name as sub_budget_class_name,
          gfs.id as gfs_code_id,
          gfs.name as gfs_code_name,
          gfs.code as gfs_code,
          a.budget_type,
          sum(affi.frequency * affi.unit_price * affi.quantity * percent.percentage) AS cumulative_budget
         FROM mtefs m
           JOIN mtef_sections ms ON ms.mtef_id = m.id
           JOIN activities a ON a.mtef_section_id = ms.id
           JOIN sections s ON s.id = ms.section_id
           JOIN activity_periods ap ON ap.activity_id = a.id
           JOIN periods p ON p.id = ap.period_id
           JOIN activity_facilities af ON af.activity_id = a.id
           JOIN activity_facility_fund_sources aff ON aff.activity_facility_id = af.id
           JOIN activity_facility_fund_source_inputs affi ON affi.activity_facility_fund_source_id = aff.id
           join gfs_codes gfs on gfs.id = affi.gfs_code_id
           join facilities fac on fac.id = af.facility_id
           JOIN fund_sources f ON f.id = aff.fund_source_id
           JOIN fund_source_categories fsc ON f.fund_source_category_id = fsc.id
           JOIN admin_hierarchies ah ON ah.id = m.admin_hierarchy_id
           JOIN financial_years fy ON fy.id = m.financial_year_id
           join activity_percentage percent on percent.activity_id = a.id
           join budget_classes bc on bc.id = a.budget_class_id
           join budget_classes bcc on bcc.id = bc.parent_id
        where m.admin_hierarchy_id = admin_hierarchy and m.financial_year_id = financial_year
        and p.code = ANY (case period_code when \'Q2\' then ARRAY[\'Q1\', \'Q2\'] when \'Q3\' then ARRAY[\'Q1\', \'Q2\', \'Q3\'] when \'Q4\' then ARRAY[\'Q1\', \'Q2\',\'Q3\',\'Q4\']
        else ARRAY[\'Q1\'] end)
        GROUP BY 
        ah.id, fac.id, fy.id, p.id, s.id, f.id, bc.id, bcc.id, gfs.id, a.budget_type; 
        end; $$
        LANGUAGE \'plpgsql\';');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('drop function func_cumulative_budget');
    }
}
