<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasPlanContentVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cas_plan_content_versions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cas_plan_content_id');
            $table->integer('version_id');
            $table->foreign('cas_plan_content_id')->references('id')->on('cas_plan_contents')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('version_id')->references('id')->on('versions')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->unique(['cas_plan_content_id','version_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cas_plan_content_versions');
    }
}
