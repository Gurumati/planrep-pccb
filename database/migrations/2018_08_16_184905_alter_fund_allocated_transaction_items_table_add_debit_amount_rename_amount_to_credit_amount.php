<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFundAllocatedTransactionItemsTableAddDebitAmountRenameAmountToCreditAmount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_allocated_transaction_items', function (Blueprint $blueprint){
            $blueprint->renameColumn('amount','debit_amount')->change();
            $blueprint->double('credit_amount', 12,2)->nullable();
        });

        Schema::table('fund_allocated_transaction_items', function (Blueprint $blueprint){
            $blueprint->double('amount', 12,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_allocated_transaction_items', function (Blueprint $blueprint){
            $blueprint->renameColumn('debit_amount','amount')->change();
            $blueprint->dropColumn('credit_amount', 12,2)->nullable();
            $blueprint->dropColumn('amount', 12,2)->nullable();
        });
    }
}
