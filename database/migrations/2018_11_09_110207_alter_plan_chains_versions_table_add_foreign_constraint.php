<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPlanChainsVersionsTableAddForeignConstraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_chain_versions', function (Blueprint $table) {
            $table->foreign('plan_chain_id')->references('id')->on('plan_chains')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('version_id')->references('id')->on('versions')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_chain_versions', function (Blueprint $table) {
            $table->dropForeign("plan_chain_versions_plan_chain_id_foreign");
            $table->dropForeign("plan_chain_versions_version_id_foreign");
        });
    }
}
