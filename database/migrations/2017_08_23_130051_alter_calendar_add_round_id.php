<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCalendarAddRoundId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calendars',function (Blueprint $table){
            $table->integer('cas_assessment_round_id')->nullable();
            $table->foreign('cas_assessment_round_id',
                'calendars_cas_assessment_round_id')
                ->references('id')
                ->on('cas_assessment_rounds')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calendars', function(Blueprint $table)
        {
            $table->dropForeign('calendars_cas_assessment_round_id');
            $table->dropColumn('cas_assessment_round_id');
        });
    }
}
