<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCasAssessmentResultDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cas_assessment_result_details', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('cas_assessment_result_id');
			$table->integer('cas_assessment_sub_criteria_option_id');
			$table->integer('sub_criteria_score');
			$table->string('remarks', 191);
			$table->integer('free_score_value');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('cas_assessment_sub_criteria_possible_score_id')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cas_assessment_result_details');
	}

}
