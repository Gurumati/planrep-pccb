<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFacilitiesTable949497 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("facilities", function (Blueprint $blueprint) {
            $blueprint->string("postal_address")->nullable();
            $blueprint->string("email")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("facilities", function (Blueprint $blueprint) {
            $blueprint->dropColumn("postal_address");
            $blueprint->dropColumn("email");
        });
    }
}
