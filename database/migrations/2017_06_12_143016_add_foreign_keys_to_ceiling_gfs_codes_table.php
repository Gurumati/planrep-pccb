<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCeilingGfsCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ceiling_gfs_codes', function(Blueprint $table)
		{
			$table->foreign('ceiling_id')->references('id')->on('ceilings')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('gfs_code_id')->references('id')->on('gfs_codes')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ceiling_gfs_codes', function(Blueprint $table)
		{
			$table->dropForeign('ceiling_gfs_codes_ceiling_id_foreign');
			$table->dropForeign('ceiling_gfs_codes_gfs_code_id_foreign');
		});
	}

}
