<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdminHierarhyCeilingAddErmsResponse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_hierarchy_ceilings', function (Blueprint $table) {
            $table->boolean('is_exported')->after('update_by')->nullable()->default(false);
            $table->string('export_response', 150)->after('is_exported')->nullable();
            $table->boolean('is_delivered')->after('export_response')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('admin_hierarchy_ceilings', function (Blueprint $table) {
            $table->dropColumn(['is_exported','export_response','is_delivered']);
        });
    }
}
