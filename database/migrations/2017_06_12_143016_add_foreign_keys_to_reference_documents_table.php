<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToReferenceDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reference_documents', function(Blueprint $table)
		{
			$table->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('end_financial_year')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('reference_document_type_id')->references('id')->on('reference_document_types')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('start_financial_year')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reference_documents', function(Blueprint $table)
		{
			$table->dropForeign('reference_documents_admin_hierarchy_id_foreign');
			$table->dropForeign('reference_documents_end_financial_year_foreign');
			$table->dropForeign('reference_documents_reference_document_type_id_foreign');
			$table->dropForeign('reference_documents_start_financial_year_foreign');
		});
	}

}
