<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExchequersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('exchequers', function(Blueprint $table)
		{
			$table->foreign('mtef_id')->references('id')->on('mtefs')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('exchequers', function(Blueprint $table)
		{
			$table->dropForeign('exchequers_mtef_id_foreign');
		});
	}

}
