<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('reports', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 191)->nullable();
			$table->text('description')->nullable();
			$table->integer('parent_id')->nullable();
			$table->boolean('expanded')->default(0);
			$table->integer('level')->nullable();
			$table->integer('position')->nullable();
			$table->string('icon', 191)->nullable();
			$table->boolean('active')->default(1);
			$table->timestamps();
			$table->softDeletes();
			$table->string('parameters', 191);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('reports');
	}

}
