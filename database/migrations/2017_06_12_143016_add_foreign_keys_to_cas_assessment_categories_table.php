<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCasAssessmentCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cas_assessment_categories', function(Blueprint $table)
		{
			$table->foreign('cas_plan_id', 'cas_assessment_categories _cas_plan_id_foreign')->references('id')->on('cas_plans')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('period_group_id')->references('id')->on('period_groups')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cas_assessment_categories', function(Blueprint $table)
		{
			$table->dropForeign('cas_assessment_categories _cas_plan_id_foreign');
			$table->dropForeign('cas_assessment_categories_period_group_id_foreign');
		});
	}

}
