<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterFrequncyDataType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //\Illuminate\Support\Facades\DB::statement("DROP MATERIALIZED VIEW budget_master_view");
        Schema::table('activity_facility_fund_source_inputs',function (Blueprint $table){
            $table->decimal('frequency',8,2)->change();
        });
        Schema::table('activity_facility_fund_source_input_breakdowns',function (Blueprint $table){
            $table->decimal('frequency',8,2)->change();
        });
        Schema::table('activity_facility_fund_source_input_forwards',function (Blueprint $table){
            $table->decimal('frequency',8,2)->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
