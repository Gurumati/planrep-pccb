<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsSentIsDeliveredToAggregateFacilityBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("aggregate_facility_budget",function (Blueprint $blueprint){
            $blueprint->boolean('is_sent')->default(false);
            $blueprint->boolean('is_delivered')->default(false);
            $blueprint->float('amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("aggregate_facility_budget",function (Blueprint $blueprint){
            $blueprint->dropColumn('is_sent');
            $blueprint->dropColumn('is_delivered');
            $blueprint->dropColumn('amount');
        });
    }
}
