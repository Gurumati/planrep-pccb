<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMetadataListItemsChangeOrgUnit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("metadata_list_items",function (Blueprint $blueprint){
            $blueprint->dropColumn("orgUnit");
            $blueprint->text("org_unit")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("metadata_list_items",function (Blueprint $blueprint){
            $blueprint->string("orgUnit")->nullable();
            $blueprint->dropColumn("org_unit");
        });
    }
}
