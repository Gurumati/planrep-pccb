<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityPhasesTableDropStartDateEndDateWd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("activity_phases",function (Blueprint $blueprint){
            $blueprint->dropColumn("start_date");
            $blueprint->dropColumn("end_date");
            $blueprint->dropColumn("working_days");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("activity_phases",function (Blueprint $blueprint){
            $blueprint->date("start_date")->nullable();
            $blueprint->date("end_date")->nullable();
            $blueprint->integer("working_days")->default(0);
        });
    }
}
