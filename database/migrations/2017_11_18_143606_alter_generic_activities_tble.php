<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGenericActivitiesTble extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("generic_activities", function (Blueprint $blueprint) {
            $blueprint->integer('budget_class_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("generic_activities", function (Blueprint $blueprint) {
            $blueprint->integer('budget_class_id')->nullable()->change();
        });
    }
}
