<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetImportItemsTableRemoveBudgetImportIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_import_items', function(Blueprint $blueprint){
            $blueprint->dropColumn('budget_import_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_import_items', function(Blueprint $blueprint){
            $blueprint->integer('budget_import_id')->unsigned()->nullable();
        });
    }
}
