<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExpenditureCentresDropTaskNatures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bdc_expenditure_centres', function (Blueprint $table) {
            $table->dropForeign('bdc_expenditure_centres_task_nature_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bdc_expenditure_centres', function (Blueprint $table) {
            $table->foreign('task_nature_id')->references('id')->on('activity_task_natures')->onDelete('restrict')->onUpdate('cascade');
        });
    }
}
