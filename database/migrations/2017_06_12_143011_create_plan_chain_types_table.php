<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanChainTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plan_chain_types', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 191)->unique();
			$table->integer('hierarchy_level');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('update_by')->nullable();
			$table->boolean('is_active')->default(1);
			$table->boolean('is_incremental')->nullable();
			$table->boolean('is_sectoral')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plan_chain_types');
	}

}
