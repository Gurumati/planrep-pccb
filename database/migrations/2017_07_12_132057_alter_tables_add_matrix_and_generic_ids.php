<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablesAddMatrixAndGenericIds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('generic_targets', function(Blueprint $table)
        {
            $table->integer('planning_matrix_id');
            $table->foreign('planning_matrix_id')->references('id')->on('planning_matrices')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('generic_problems', function(Blueprint $table)
        {
            $table->integer('planning_matrix_id');
            $table->foreign('planning_matrix_id')->references('id')->on('planning_matrices')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::table('activities', function(Blueprint $table)
        {
            $table->integer('generic_activity_id')->nullable();
            $table->foreign('generic_activity_id')->references('id')->on('generic_activities')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
        Schema::rename('generic_problems','generic_sector_problems');

        Schema::table('sector_problems', function(Blueprint $table)
        {
            $table->integer('generic_sector_problem_id')->nullable();
            $table->foreign('generic_sector_problem_id')->references('id')->on('generic_sector_problems')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sector_problems', function(Blueprint $table)
        {
            $table->dropColumn('generic_sector_problem_id');
        });
        Schema::rename('generic_sector_problems','generic_problems');
        Schema::table('activities', function(Blueprint $table)
        {
            $table->dropColumn('generic_activity_id');
        });
        Schema::table('generic_problems', function(Blueprint $table)
        {
            $table->dropColumn('planning_matrix_id');
        });
        Schema::table('generic_targets', function(Blueprint $table)
        {
            $table->dropColumn('planning_matrix_id');
        });

    }
}
