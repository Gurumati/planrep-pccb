<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInterventionCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('intervention_categories',function (Blueprint $table){
            $table->foreign('intervention_category_id')->references('id')->on('intervention_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('intervention_categories',function (Blueprint $table){
            $table->dropForeign('intervention_categories_intervention_category_id_foreign');
        });
    }
}
