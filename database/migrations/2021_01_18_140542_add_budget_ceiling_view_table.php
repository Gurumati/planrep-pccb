<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddBudgetCeilingViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW IF EXISTS vw_ceiling_budget;");
        DB::statement("         
          CREATE OR REPLACE VIEW public.vw_ceiling_budget(
    financial_year_id,
    admin_hierarchy_id,
    cost_center_id,
    cost_center,
    section_name,
    fund_source_name,
    fund_source_id,
    budget_class_id,
    budget_type,
    ceiling)
AS
  SELECT adc.financial_year_id,
         adc.admin_hierarchy_id,
         s.id AS cost_center_id,
         s.name AS cost_center,
         sec.name AS section_name,
         f.name AS fund_source_name,
         f.id AS fund_source_id,
         c.budget_class_id,
         adc.budget_type,
         sum(adc.amount) AS ceiling
  FROM admin_hierarchy_ceilings adc
       JOIN ceilings c ON c.id = adc.ceiling_id
       JOIN sections s ON s.id = adc.section_id
       JOIN sectors sec ON sec.id = s.sector_id
       JOIN gfs_codes gfs ON gfs.id = c.gfs_code_id
       JOIN gfsCode_fundSources gfcf ON gfcf.gfs_code_id = gfs.id
       JOIN fund_sources f ON f.id = gfcf.fund_source_id
       JOIN section_levels sl ON sl.id = s.section_level_id
  WHERE f.can_project = false AND
        adc.is_facility = false AND
        c.is_active = true AND
        sl.hierarchy_position = 4
  GROUP BY adc.financial_year_id,
           adc.admin_hierarchy_id,
           s.id,
           sec.name,
           f.id,
           adc.budget_type,
           c.budget_class_id
  UNION ALL
  SELECT adc.financial_year_id,
         adc.admin_hierarchy_id,
         s.id AS cost_center_id,
         s.name AS cost_center,
         sec.name AS section_name,
         f.name AS fund_source_name,
         f.id AS fund_source_id,
         c.budget_class_id,
         adc.budget_type,
         sum(adc.amount) AS ceiling
  FROM admin_hierarchy_ceilings adc
       JOIN ceilings c ON c.id = adc.ceiling_id
       JOIN sections s ON s.id = adc.section_id
       JOIN sectors sec ON sec.id = s.sector_id
       JOIN fund_sources f ON c.aggregate_fund_source_id = f.id
       JOIN section_levels sl ON sl.id = s.section_level_id
  WHERE c.is_aggregate = true AND
        adc.is_facility = false AND
        c.is_active = true AND
        sl.hierarchy_position = 4
  GROUP BY adc.financial_year_id,
           adc.admin_hierarchy_id,
           s.id,
           sec.name,
           f.id,
           adc.budget_type,
           c.budget_class_id;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_ceiling_budget;");
    }
}
