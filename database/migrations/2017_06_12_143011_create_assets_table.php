<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAssetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('assets', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('asset_name', 191);
			$table->string('registration_number', 191)->nullable();
			$table->integer('parent_id')->nullable();
			$table->integer('admin_hierarchy_id');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->date('date_of_acquisition')->nullable();
			$table->string('used_for', 191)->nullable();
			$table->string('condition', 191)->nullable();
			$table->string('mileage', 191)->nullable();
			$table->string('type', 191)->nullable();
			$table->text('comments')->nullable();
			$table->string('station', 191)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('assets');
	}

}
