<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAccountReturnsTableAddSectionId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("account_returns",function (Blueprint $blueprint){
            $blueprint->integer("section_id")->nullable()->unsigned();
            $blueprint->foreign('section_id')->references('id')->on('sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("account_returns",function (Blueprint $blueprint){
            $blueprint->dropColumn("section_id");
        });
    }
}
