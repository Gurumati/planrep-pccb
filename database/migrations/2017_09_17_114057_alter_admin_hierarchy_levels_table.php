<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAdminHierarchyLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_hierarchy_levels',function (Blueprint $blueprint){
            $blueprint->boolean('is_active')->default(true)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admin_hierarchy_levels',function (Blueprint $blueprint){
            $blueprint->boolean('is_active')->default(true)->change();
        });
    }
}
