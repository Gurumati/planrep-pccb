<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterCalendarEventAddValuesQuery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calendar_events',function (Blueprint $table){
            $table->text('expected_value_query')->nullable();
            $table->text('actual_value_query')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calendar_events',function (Blueprint $table){
            $table->dropColumn('expected_value_query');
            $table->dropColumn('actual_value_query');
        });
    }
}
