<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReceivedFundItemsTableAddPeriodGfsCodeFundSource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_import_items', function(Blueprint $blueprint){
            $blueprint->integer('gfs_code_id')->unsigned()->nullable();
            $blueprint->integer('budget_class_id')->unsigned()->nullable();
            $blueprint->integer('fund_source_id')->unsigned()->nullable();
            $blueprint->foreign('budget_class_id')->references('id')->on('budget_classes')->onDelete('restrict')->onUpdate('cascade');
            $blueprint->foreign('fund_source_id')->references('id')->on('fund_sources')->onDelete('restrict')->onUpdate('cascade');
            $blueprint->foreign('gfs_code_id')->references('id')->on('gfs_codes')->onDelete('restrict')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_import_items', function(Blueprint $blueprint){
            $blueprint->dropForeign('budget_import_items_budget_class_id_foreign');
            $blueprint->dropForeign('budget_import_items_fund_source_id_foreign');
            $blueprint->dropForeign('budget_import_items_gfs_code_id_foreign');
        });


        Schema::table('budget_import_items', function(Blueprint $blueprint){
            $blueprint->dropColumn('budget_class_id');
            $blueprint->dropColumn('fund_source_id');
            $blueprint->dropColumn('gfs_code_id');
        });
    }
}
