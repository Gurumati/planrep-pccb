<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivityIndicatorDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('activity_indicator_data', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('activity_indicator_id');
			$table->date('date_collected');
			$table->string('target_value', 191);
			$table->string('actual_value', 191);
			$table->text('info_source');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('update_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('activity_indicator_data');
	}

}
