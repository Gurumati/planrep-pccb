<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBdcSectorExpenditureSubCentreGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bdc_sector_expenditure_sub_center_groups',function (Blueprint $table){
            $table->unique(['bdc_group_id','bdc_sector_expenditure_sub_centre_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bdc_sector_expenditure_sub_center_groups',function (Blueprint $table){
            $table->dropUnique('bdc_sector_expenditure_sub_center_groups_bdc_group_id_bdc_secto');
        });
    }
}
