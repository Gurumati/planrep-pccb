<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToLongTermTargetReferencesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('long_term_target_references', function(Blueprint $table)
		{
			$table->foreign('long_term_target_id')->references('id')->on('long_term_targets')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('reference_id')->references('id')->on('reference_docs')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('long_term_target_references', function(Blueprint $table)
		{
			$table->dropForeign('long_term_target_references_long_term_target_id_foreign');
			$table->dropForeign('long_term_target_references_reference_id_foreign');
		});
	}

}
