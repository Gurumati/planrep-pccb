<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetExportAccountGlaccountExportIssues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_export_account_glaccount_export_issues', function(Blueprint $blueprint){
           $blueprint->increments('id');
           $blueprint->integer('budget_export_account_id')->unsigned();
           $blueprint->string('response_text');
           $blueprint->timestamps();
           $blueprint->foreign('budget_export_account_id')->references('id')->on('budget_export_accounts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_export_account_glaccount_export_issues', function(Blueprint $blueprint){
            $blueprint->dropForeign('budget_export_account_id');
        });

        Schema::drop('budget_export_account_glaccount_export_issues');
    }
}
