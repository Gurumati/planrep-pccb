<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFundAllocatedTransactionsTableMakeImportMethodIdAndDataSourceIdNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_allocated_transactions',function(Blueprint $blueprint){
            $blueprint->integer('data_source_id')->unsiged()->nullable()->change();
            $blueprint->integer('import_method_id')->unsiged()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_allocated_transactions',function(Blueprint $blueprint){
            $blueprint->integer('data_source_id')->unsiged()->change();
            $blueprint->integer('import_method_id')->unsiged()->change();
        });
    }
}
