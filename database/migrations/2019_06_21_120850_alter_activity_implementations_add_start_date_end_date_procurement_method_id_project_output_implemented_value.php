<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivityImplementationsAddStartDateEndDateProcurementMethodIdProjectOutputImplementedValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("activity_implementations",function (Blueprint $table){
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('project_output_implemented_value')->default(0);
            $table->integer('procurement_method_id')->nullable()->unsigned();
            $table->foreign('procurement_method_id')->references('id')->on('procurement_methods')->onUpdate('CASCADE')->onDelete('RESTRICT');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("activity_implementations",function (Blueprint $table){
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
            $table->dropColumn('project_output_implemented_value');
            $table->dropColumn('procurement_method_id');
        });
    }
}
