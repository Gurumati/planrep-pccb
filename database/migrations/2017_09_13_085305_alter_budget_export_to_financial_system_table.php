<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetExportToFinancialSystemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_export_to_financial_systems',function (Blueprint $table){
            $table->dropColumn(['is_received','response','financial_system_code']);
            $table->string('queue_name')->unique()->comment('identifying channel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_export_to_financial_systems',function (Blueprint $table){
            $table->dropColumn('queue_name');
            $table->boolean('is_received')->default(false);
            $table->text('response')->nullable();
            $table->string('financial_system_code');
        });
    }
}
