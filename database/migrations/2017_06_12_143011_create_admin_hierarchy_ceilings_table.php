<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdminHierarchyCeilingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admin_hierarchy_ceilings', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('ceiling_id');
			$table->decimal('amount', 20)->default(0);
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->timestamps();
			$table->integer('dissemination_status')->nullable();
			$table->integer('use_status')->nullable();
			$table->integer('admin_hierarchy_id');
			$table->integer('financial_year_id');
			$table->integer('section_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admin_hierarchy_ceilings');
	}

}
