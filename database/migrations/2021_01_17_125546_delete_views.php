<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class DeleteViews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::delete("delete from migrations where migration ILIKE '%_231758_create_revenue_projection_view'");
        DB::delete("delete from migrations where migration ILIKE '%_152037_add_view_ceiling_by_gfs_code'");
        DB::delete("delete from migrations where migration ILIKE '%_144149_add_ceiling_view'");
        DB::delete("delete from migrations where migration ILIKE '%_094403_create_incomplete_ceiling materialized_view'");
        DB::delete("delete from migrations where migration ILIKE '%_084404_create_view_received_expenditure_balance'");
        DB::delete("delete from migrations where migration ILIKE '%_add_budget_ceiling_view_table'");

        DB::statement('DROP VIEW IF EXISTS revenue_projection_view');
        DB::statement('DROP MATERIALIZED VIEW IF EXISTS view_incomplete_ceiling_and_budget');
        DB::statement('DROP VIEW IF EXISTS vw_ceiling_budget');
        DB::statement('DROP VIEW IF EXISTS revenue_ifrs_view');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
