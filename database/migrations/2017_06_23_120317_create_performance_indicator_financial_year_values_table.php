<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerformanceIndicatorFinancialYearValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('performance_indicator_financial_year_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('performance_indicator_baseline_value_id')->unsigned();
            $table->integer('financial_year_id')->unsigned();
            $table->string('planned_value');
            $table->string('actual_value');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('performance_indicator_baseline_value_id')->references('id')->on('performance_indicator_baseline_values')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('performance_indicator_financial_year_values');
    }
}
