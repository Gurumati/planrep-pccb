<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFundAllocatedTransactionItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_allocated_transaction_items',function (Blueprint $table){
            $table->foreign('fund_allocated_transaction_id')->references('id')->on('fund_allocated_transactions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('budget_export_account_id')->references('id')->on('budget_export_accounts')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_allocated_transaction_items',function (Blueprint $table){
            $table->dropForeign('fund_allocated_transaction_items_fund_allocated_transaction_id_');
            $table->dropForeign('fund_allocated_transaction_items_budget_export_account_id_forei');
        });
    }
}
