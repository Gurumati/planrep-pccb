<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGlIsDeliveredToAggregateFacilityBudgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("aggregate_facility_budgets",function (Blueprint $blueprint){
            $blueprint->boolean("gl_is_delivered")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("aggregate_facility_budgets",function (Blueprint $blueprint){
            $blueprint->dropColumn("gl_is_delivered");
        });
    }
}
