<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropActivityInputsTbl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('activity_inputs');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('activity_inputs', function (Blueprint $blueprint) {
            $blueprint->increments('id');
            $blueprint->integer('activity_id')->unsigned();
            $blueprint->integer('gfs_code_id')->unsigned();
            $blueprint->integer('unit_id')->unsigned();
            $blueprint->integer('quantity');
            $blueprint->decimal('unit_price', 20, 2);
            $blueprint->integer('frequency');
            $blueprint->boolean('is_procurement')->default(0);
            $blueprint->integer('procurement_type_id')->unsigned();
            $blueprint->softDeletes();
            $blueprint->timestamps();
        });
    }
}
