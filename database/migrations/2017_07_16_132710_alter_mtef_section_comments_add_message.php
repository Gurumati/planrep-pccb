<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMtefSectionCommentsAddMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mtef_comments',function (Blueprint $table){
           $table->text('forward_message')->nullable();
        });
        Schema::table('mtef_section_comments',function (Blueprint $table){
            $table->text('forward_message')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mtef_comments',function (Blueprint $table){
            $table->dropColumn('forward_message')->nullable();
        });
        Schema::table('mtef_section_comments',function (Blueprint $table){
            $table->dropColumn('forward_message')->nullable();
        });
    }
}
