<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGeneriTargetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('generic_targets', function(Blueprint $table){
        $table->integer('performance_indicator_id')->unsigned()->nullable();
        $table->dropColumn('priority_area_id');
        $table->dropColumn('section_id');
        $table->dropColumn('generic_sector_problem_id');
        $table->foreign('performance_indicator_id')->references('id')->on('performance_indicators')->onUpdate('CASCADE')->onDelete('RESTRICT');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
