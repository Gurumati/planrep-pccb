<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHistoricalDataTableAddType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("historical_data_tables",function (Blueprint $blueprint){
            $blueprint->enum("type",['REVENUE','EXPENDITURE'])->default("REVENUE");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("historical_data_tables",function (Blueprint $blueprint){
            $blueprint->dropColumn("type");
        });
    }
}
