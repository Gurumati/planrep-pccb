<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetImportItemsAddMissingColumnsForGlSummaryActuals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_import_items', function (Blueprint $blueprint){
            $blueprint->string('BookID');
            $blueprint->string('FiscalYear');
            $blueprint->date('JEDate');
            $blueprint->string('Account');
            $blueprint->string('CreditAmount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_import_items', function (Blueprint $blueprint){
            $blueprint->dropColumn('BookID');
            $blueprint->dropColumn('FiscalYear');
            $blueprint->dropColumn('JEDate');
            $blueprint->dropColumn('Account');
            $blueprint->dropColumn('CreditAmount');
        });
    }
}
