<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectOutputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_outputs', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name");
            $table->integer("expenditure_category_id")->unsigned();
            $table->integer("sector_id")->unsigned();
            $table->foreign('expenditure_category_id')->references('id')->on('expenditure_categories')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('sector_id')->references('id')->on('sectors')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_outputs');
    }
}
