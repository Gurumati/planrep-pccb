<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBudgetExportAccountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('budget_export_accounts', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->timestamps();
			$table->softDeletes();
			$table->integer('created_by')->nullable();
			$table->integer('deleted_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->integer('admin_hierarchy_id');
			$table->integer('section_id');
			$table->integer('financial_year_id')->nullable();
			$table->integer('activity_facility_fund_source_input_id');
			$table->integer('activity_id')->nullable();
			$table->integer('gfs_code_id')->nullable();
			$table->string('chart_of_accounts', 191)->unique();
			$table->string('financial_system_code', 191);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('budget_export_accounts');
	}

}
