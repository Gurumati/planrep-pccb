<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToActivityIndicatorGuidelinesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('activity_indicator_guidelines', function(Blueprint $table)
		{
			$table->foreign('activity_indicator_id')->references('id')->on('activity_indicators')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('guideline_id')->references('id')->on('guidelines')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('activity_indicator_guidelines', function(Blueprint $table)
		{
			$table->dropForeign('activity_indicator_guidelines_activity_indicator_id_foreign');
			$table->dropForeign('activity_indicator_guidelines_guideline_id_foreign');
		});
	}

}
