<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBudgetImportItemsAddBudgetTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_import_items', function (Blueprint $blueprint){
            $blueprint->string('budget_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_import_items', function (Blueprint $blueprint){
            $blueprint->dropColumn('budget_type');
        });
    }
}
