<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMtefIdToReallocation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * $t = table
         */
        Schema::table('budget_reallocations', function(Blueprint $t){
            $t->integer('admin_hierarchy_id')->unsigned()->nullable();
            $t->integer('financial_year_id')->unsigned()->nullable();
            $t->foreign('admin_hierarchy_id')->references('id')->on('admin_hierarchies')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $t->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_reallocations', function(Blueprint $t){
            $t->dropColumn('admin_hierarchy_id');
            $t->dropColumn('financial_year_id');
        });
    }
}
