<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAssetsTableAddFinancialYearId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function($blueprint){
            $blueprint->integer('financial_year_id')->unsigned()->nullable();
            $blueprint->foreign('financial_year_id')->references('id')->on('financial_years')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function($blueprint){
            $blueprint->dropForeign('financial_year_id_foreign')->unsigned()->nullable();
        });

        Schema::table('assets', function($blueprint){
            $blueprint->dropColumn('financial_year_id')->unsigned()->nullable();
        });
    }
}
