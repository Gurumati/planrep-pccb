<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAdminHierarchyLevSecMappingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('admin_hierarchy_lev_sec_mappings', function(Blueprint $table)
		{
			$table->foreign('admin_hierarchy_level_id', 'admin_hierarchy_lev_sec_mappings_admin_hierarchy_level_id_forei')->references('id')->on('admin_hierarchy_levels')->onUpdate('CASCADE')->onDelete('RESTRICT');
			$table->foreign('section_id')->references('id')->on('sections')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('admin_hierarchy_lev_sec_mappings', function(Blueprint $table)
		{
			$table->dropForeign('admin_hierarchy_lev_sec_mappings_admin_hierarchy_level_id_forei');
			$table->dropForeign('admin_hierarchy_lev_sec_mappings_section_id_foreign');
		});
	}

}
