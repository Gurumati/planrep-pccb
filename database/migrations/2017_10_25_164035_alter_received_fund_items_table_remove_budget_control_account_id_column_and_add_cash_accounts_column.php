<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReceivedFundItemsTableRemoveBudgetControlAccountIdColumnAndAddCashAccountsColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('received_fund_items',function(Blueprint $blueprint){
            $blueprint->dropColumn('budget_control_account_id');
        });

        Schema::table('received_fund_items',function(Blueprint $blueprint){
            $blueprint->integer('cash_account_id')->unsigned()->nullable();
            $blueprint->foreign('cash_account_id')->references('id')->on('cash_accounts')->onDelete('restrict')->onUpdate('cascade');
            $blueprint->foreign('revenue_export_account_id')->references('id')->on('revenue_export_accounts')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('received_fund_items',function(Blueprint $blueprint){
            $blueprint->dropForeign('cash_account_id');
            $blueprint->dropForeign('revenue_export_account_id');

        });

        Schema::table('received_fund_items',function(Blueprint $blueprint){
            $blueprint->integer('budget_control_account_id')->unsigned()->nullable();
            $blueprint->dropColumn('cash_account_id');
        });
    }
}
