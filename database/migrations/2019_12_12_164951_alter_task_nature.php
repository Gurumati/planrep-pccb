<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTaskNature extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity_task_natures', function($table) {
            $table->boolean('is_active')->default(true);
        });
        Schema::table('expenditure_categories', function($table) {
            $table->boolean('is_active')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity_task_natures', function($table) {
            $table->$table->dropColumn('is_active');
        });
        Schema::table('expenditure_categories', function($table) {
            $table->$table->dropColumn('is_active');
        });
    }
}
