<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAssetsTble extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets',function (Blueprint $table){
            $table->foreign('condition')->references('id')->on('asset_conditions')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('used_for')->references('id')->on('asset_uses')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets',function (Blueprint $table){
            $table->dropForeign('assets_condition_foreign');
            $table->dropForeign('assets_used_for_foreign');
        });
    }
}
