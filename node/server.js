var express = require('express');
var http = require('http');
var app = express();
var httpServer = http.Server(app);
var io = require('socket.io').listen(httpServer);
var Redis = require('ioredis');
var port = 3000;

var redis  = new Redis({
    port : 6379,
    host : '127.0.0.1',
    family :4,
    db : 0,
});

redis.subscribe('user_notifications');

redis.on('message',function (channel, message) {
    var newMessage = JSON.parse(message);
    console.log(newMessage);
    console.log('the channel', channel);
    console.log('the event', newMessage.event);
    io.emit(channel + ":" + newMessage.event, newMessage);
});

io.on('connection', function (socket) {
    console.log('socket is connected');
});

httpServer.listen(port, '127.0.0.1');
console.log('listening on port',port);
