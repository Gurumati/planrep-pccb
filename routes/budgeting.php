<?php
/**
 * Created by PhpStorm.
 * User: Kachinga
 * Date: 3/4/2017
 * Time: 12:38 PM
 */
Route::group(['middleware' => 'authentication'], function () {
    Route::get('/budgeting', 'Budgeting\MainController@index');
});

Route::group(['middleware' => ['ajaxAuthentication','hasPermission:planning,budgeting']], function () {

    Route::get('/json/budgets', 'Budgeting\MainController@jsonBudgets');
    Route::get('/json/budget-types', 'Budgeting\BudgetTypeController@index');

    Route::get('/json/peBudgets','Budgeting\PeBudgetController@index');
    Route::get('/json/paginatedPeBudgets','Budgeting\PeBudgetController@paginated');
    Route::post('/json/createPeBudget','Budgeting\PeBudgetController@store');
    Route::post('/json/updatePeBudget','Budgeting\PeBudgetController@update');
    Route::get('/json/deletePeBudget/{id}','Budgeting\PeBudgetController@delete');
    Route::get('/json/allMtefs', 'Budgeting\PeBudgetController@allMtefs');

    Route::post('/json/activity-facility-fund-source-inputs/create-or-update', 'Budgeting\ActivityFacilityFundSourceInputController@createOrUpdate');
    Route::get('/json/activity-facility-fund-sources/{id}', 'Budgeting\ActivityFacilityFundSourceController@find');
    Route::get('/json/activity-facility-fund-sources/by-mtef-section/{mtefSectionId}/by-budget-type/{budgetType}', 'Budgeting\ActivityFacilityFundSourceController@paginate');
    Route::get('/json/activity-facility-fund-source-inputs/{activityFacilityFundSourceId}', 'Budgeting\ActivityFacilityFundSourceInputController@paginate');
    Route::delete('/json/activity-facility-fund-source-inputs/{id}', 'Budgeting\ActivityFacilityFundSourceInputController@delete');

    //CEILINGS
    Route::group(['middleware' => ['hasPermission:budgeting.manage.ceilings']], function () {
        Route::get('/json/ceilings', 'Budgeting\CeilingController@index');
        Route::get('/json/paginatedCeilings', 'Budgeting\CeilingController@paginated');
        Route::get('/json/paginatedCeilingSectors', 'Budgeting\CeilingSectorController@paginated');
        Route::post('/json/createCeiling', 'Budgeting\CeilingController@store');
        Route::post('/json/updateCeiling', 'Budgeting\CeilingControl]ler@update');
        Route::get('/json/deleteCeiling/{id}', 'Budgeting\CeilingController@delete');
        Route::put('/json/toggleActiveCeiling/{id}', 'Budgeting\CeilingController@toggleActivate');
        Route::get('/json/bank-accounts', 'Setup\BankAccountController@all');
        Route::get('/json/bank-accounts/balance-and-ceiling/{id}/{financialYearId}/{adminHierarchyId}/{sectionId}', 'Setup\BankAccountController@balaceAndCeiling');
        Route::get('/json/admin-hierarchy-ceiling/next-lower-level/{ceilingId}/{budgetType}/{financialYearId}/{parentAdminHierarchyId}/{parentSectionId}', 'Budgeting\AdminHierarchyCeilingController@firstLowerLevel');
        Route::get('/json/adminHierarchyCeilings', 'Budgeting\AdminHierarchyCeilingController@index');
        Route::get('/json/admin-hierarchy-ceiling/paginate/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}', 'Budgeting\AdminHierarchyCeilingController@paginate');
        Route::get('/json/admin-hierarchy-ceiling/ceiling-to-initiate/paginate/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}', 'Budgeting\AdminHierarchyCeilingController@paginateCeilingToInitiate');
        Route::put('/json/admin-hierarchy-ceiling/initiate/{ceilingId}/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}', 'Budgeting\AdminHierarchyCeilingController@initiateAdminHierarchyCeiling');
        Route::post('/json/createAdminHierarchyCeiling', 'Budgeting\AdminHierarchyCeilingController@store');
        Route::post('/json/updateAdminHierarchyCeiling', 'Budgeting\AdminHierarchyCeilingController@update');
        Route::post('/json/updateLowerCeilings/{adminHierarchyCeilingId}', 'Budgeting\AdminHierarchyCeilingController@updateLowerLevel');
        Route::get('/json/deleteAdminHierarchyCeilings/{ceilingId}', 'Budgeting\AdminHierarchyCeilingController@pullingBack');
        Route::get('/json/removeCeilingSector/{id}', 'Budgeting\CeilingSectorController@delete');
        Route::get('/json/admin-hierarchy-ceiling/next-ceiling-chain/{adminHierarchyId}/{sectionId}', 'Budgeting\AdminHierarchyCeilingController@getNextCeilingChain');
        Route::get('/json/admin-hierarchy-ceiling/fund-source-ceiling/{mtefSectionId}/{budgetType}/{budgetClassId}/{fundSourceId}/{isFacilityAccount}/{facilityId}/{financialYearId}', 'Budgeting\AdminHierarchyCeilingController@getFundSourceCeiling');
        Route::put('/json/admin-hierarchy-ceiling/toggle-locked/{adminHierarchyCeilingId}/{isLocked}', 'Budgeting\AdminHierarchyCeilingController@toggleLocked');
        Route::put('/json/admin-hierarchy-ceiling/toggle-all-locked', 'Budgeting\AdminHierarchyCeilingController@toggleAllLocked');
        Route::delete('/json/admin-hierarchy-ceiling/delete/{id}', 'Budgeting\AdminHierarchyCeilingController@delete');
        Route::get('/json/admin-hierarchy-ceiling/get-by-ceiling/{ceilingId}', 'Budgeting\AdminHierarchyCeilingController@getByCeiling');

        Route::get('/json/allCeilingSectors/{id}', 'Budgeting\CeilingController@allCeilingSectors');
        Route::get('/json/deleteCeilingSector/{ceiling_sector_id}/{ceiling_id}', 'Budgeting\CeilingController@delete_ceiling_sector');
        Route::post('/json/addSector', 'Budgeting\CeilingController@addSector');
        Route::get('/json/deleteAdminHierarchyCeiling/{ceiling_id}', 'Budgeting\CeilingController@delete');

        Route::post('/json/removeCeilingSector/{ceilingId}/{sectorId}', 'Budgeting\CeilingController@removeCeilingSector');
        Route::post('/json/addCeilingSector/{ceilingId}/{sectorId}', 'Budgeting\CeilingController@addCeilingSector');
        Route::post('/json/toggleExtendsToFacility/{ceilingId}/{extendsToFacility}', 'Budgeting\CeilingController@toggleExtendsToFacility');
        Route::post('/json/saveAllocationCeilings', 'Planning\ProjectionController@saveAllocationCeilings');
        Route::get('/json/getAllocationCeilings/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}/{fundSourceId}', 'Planning\ProjectionController@getAllocationCeilings');
    });

    //THE ROUTES OF THE BUDGET SUBMISSION FORMS BEGIN HERE
    Route::group(['middleware' => ['hasPermission:budgeting.manage.submission.form']], function () {
        Route::get('/json/budgetSubmissionForms', 'Budgeting\BudgetSubmissionFormController@index');
        Route::get('/json/paginateBudgetSubmissionDefinitions', 'Budgeting\BudgetSubmissionDefinitionController@paginateIndex');
        Route::get('/json/downloadBudgetSubmissionFormExcel/{form_id}', 'Budgeting\BudgetSubmissionDefinitionController@generateBudgetFormTemplate');
        Route::post('/json/updateBudgetSubmissionForm', 'Budgeting\BudgetSubmissionFormController@update');
        Route::post('/json/updateBudgetSubmissionDefinition', 'Budgeting\BudgetSubmissionDefinitionController@update');
        Route::post('/json/createBudgetSubmissionForm', 'Budgeting\BudgetSubmissionFormController@store');
        Route::post('/json/DeleteBudgetSubmissionValues', 'Budgeting\BudgetSubmissionLineController@delete');
        Route::post('/json/EditBudgetSubmissionValues', 'Budgeting\BudgetSubmissionLineController@edit');
        Route::get('/json/deleteBudgetSubmissionForm/{id}', 'Budgeting\BudgetSubmissionFormController@delete');

        //budget sumbmission definition
        Route::post('/json/deleteBudgetSubmissionDefinition', 'Budgeting\BudgetSubmissionDefinitionController@delete');
        Route::get('/json/budgetSubmissionDefinitions/{form_id}', 'Budgeting\BudgetSubmissionDefinitionController@index');
        Route::post('/json/createBudgetSubmissionColumn', 'Budgeting\BudgetSubmissionDefinitionController@store');
        Route::get('/json/loadParentBudgetColumn/{column_id}/{form_id}', 'Budgeting\BudgetSubmissionDefinitionController@loadParent');
        Route::get('/json/BudgetSubmissionBudgetClasses/{sub_form_id}/{financial_year_id}', 'Budgeting\BudgetSubmissionLineController@loadBudgetClasses');
        Route::post('/pe_budget_submission_line/upload', 'Budgeting\BudgetSubmissionLineController@uploadPE');
        //THE ROUTES OF THE BUDGET SUBMISSION FORMS END HERE

        //Budget submission sub forms
        Route::get('/json/budgetSubmissionSubForms', 'Budgeting\BudgetSubmissionSubFormController@index');
        Route::get('/json/institutionEmployees/getEmployeesDetails', 'Budgeting\BudgetSubmissionSubFormController@getEmployeesDetails');
        Route::post('/json/createBudgetSubmissionSubForm', 'Budgeting\BudgetSubmissionSubFormController@store');
        Route::post('/json/updateBudgetSubmissionSubForm', 'Budgeting\BudgetSubmissionSubFormController@update');
        Route::get('/json/deleteBudgetSubmissionSubForm/{id}', 'Budgeting\BudgetSubmissionSubFormController@delete');
        Route::get('/json/parentBudgetSubmissionForm', 'Budgeting\BudgetSubmissionSubFormController@parentFormLevel');
        Route::get('/json/parentBudgetSubmissionSubForm/{id}', 'Budgeting\BudgetSubmissionSubFormController@parentSubForm');

        //Budget submission lines
        Route::post('/json/updateBudgetSubmissionLine', 'Budgeting\BudgetSubmissionLineController@store');
        Route::get('/json/loadBudgetSections', 'Budgeting\BudgetSubmissionLineController@loadSection');
        Route::get('/json/BudgetSubmissionSelectOptions', 'Budgeting\BudgetSubmissionLineController@getChildren');
        Route::get('/json/budgetSubmissionActivity/{budget_class_id}', 'Budgeting\BudgetSubmissionLineController@BudgetSectionActivity');
        Route::get('/json/budgetSubFormDefinitions/{form_id}', 'Budgeting\BudgetSubmissionLineController@budgetDefinition');
        Route::get('/json/budgetSubmissionFieldValues', 'Budgeting\BudgetSubmissionLineController@loadBudget');
        Route::get('/json/loadBudgetSubmissionActivity', 'Budgeting\BudgetSubmissionLineController@loadActivity');
        Route::post('/json/resetContributions', 'Budgeting\BudgetSubmissionLineController@resetContributions');
        //end submission line items

        //submission forms select options
        Route::get('/json/SubmissionFormSelectGroup', 'Budgeting\BudgetSubmissionSelectOptionController@index');
        Route::get('/json/SubmissionFormSelectOptions/{select_group}', 'Budgeting\BudgetSubmissionSelectOptionController@getOptions');
        Route::get('/json/budgetDistribution/getByExpenditureCentres', 'Budgeting\BudgetDistributionController@getByExpenditureCentres');
    });

        Route::post('/json/deleteInput/{id}/{mtefSectionId}', 'Planning\ActivityController@deleteInput');

    Route::get('/json/budget-preview-trees/{mtef_section_id}', 'Setup\PlanChainController@getFullChainIndex');
    Route::get('/json/budget-preview-trees/{adminHierarchyId}/{sectionId}/{planType}', 'Setup\PlanChainController@getFullChainIndex');

//    Scrutin Area
//    Route::group(['middleware' => ['hasPermission:budgeting.manage.scrutinization']], function () {
        Route::get('/json/getUserScrutinizations', 'Budgeting\ScrutinizationController@getUserScrutinizations');
        Route::get('/json/getUserScrutinizations/{id}', 'Budgeting\ScrutinizationController@getScrutinizationById');
        Route::post('/json/scrutinization/assignUser/{userId}', 'Budgeting\ScrutinizationController@assignUser');
        Route::post('/json/scrutinization/saveItemComments', 'Budgeting\ScrutinizationController@saveItemComments');
        Route::get('/json/scrutinization/updateStatus/{scrutinId}/{status}', 'Budgeting\ScrutinizationController@updateStatus');
        Route::get('/json/scrutinization/getReturnDecisionLevels/{scrutinId}/{mtefSectionId}', 'Budgeting\ScrutinizationController@getReturnDecisionLevels');
        Route::post('/json/scrutinization/returnMtefSection', 'Budgeting\ScrutinizationController@returnMtefSection');
        Route::get('/json/scrutinization/getUserNextDecisionLevels', 'Budgeting\ScrutinizationController@getUserNextDecisionLevels');
        Route::post('/json/scrutinization/forwardAllByUser', 'Budgeting\ScrutinizationController@forwardAllByUser');
        Route::post('/json/scrutinization/setAddressed/{itemType}/{itemId}', 'Budgeting\ScrutinizationController@setAddressed');
        Route::post('/json/scrutinization/setRecommendation/{scrutinId}/{recommendation}/{status}', 'Budgeting\ScrutinizationController@setRecommendation');
        Route::post('/json/forwardMtefSectionService', 'Budgeting\ScrutinizationController@forwardToSection');
        Route::get('/json/test', 'Budgeting\ScrutinizationController@getMySupervisedSections');
        Route::get('/json/scrutinization/getAllReturnDecisionLevels', 'Budgeting\ScrutinizationController@getAllReturnDecisionLevels');
        Route::post('/json/scrutinization/returnAllByUser', 'Budgeting\ScrutinizationController@returnAllByUser');
        Route::get('/json/scrutinization/get-user-expected-scrutins', 'Budgeting\ScrutinizationController@getUserExpectedScrutins');

        Route::get('/json/scrutinization/get-p1-admin', 'Budgeting\ScrutinizationController@getP1Admin');
        Route::get('/json/scrutinization/get-admin/by-p1', 'Budgeting\ScrutinizationController@getAdmin');
        Route::get('/json/scrutinization/get-sectors/by-admin', 'Budgeting\ScrutinizationController@getSectors');
        Route::get('/json/scrutinization/get-scrutins/by-sector', 'Budgeting\ScrutinizationController@getScrutins');
        Route::delete('/json/scrutinization/clear/{id}', 'Budgeting\ScrutinizationController@clearScrutin');

    /**
     * Scrutiny new routes
     */
    Route::get('/json/scrutiny/get-distinct-submitted-hierarchies/{parentAdminHierarchyId}/{parentSectionId}', 'Budgeting\ScrutinyController@getDistinctSubmittedHierarchy');
    Route::get('/json/scrutiny/get-distinct-submitted-sectors/{parentAdminHierarchyId}/{parentSectionId}/{mtefId}', 'Budgeting\ScrutinyController@getDistinctSubmittedSectors');
    Route::get('/json/scrutiny/get-distinct-submitted-departments/{parentAdminHierarchyId}/{parentSectionId}/{mtefId}/{sectorId}', 'Budgeting\ScrutinyController@getDistinctSubmittedDepartments');
    Route::get('/json/scrutiny/get-distinct-submitted-cost-centres/{parentAdminHierarchyId}/{parentSectionId}/{mtefId}/{sectorId}', 'Budgeting\ScrutinyController@getDistinctSubmittedCostCentres');
    Route::post('/json/scrutiny/move-by-mtef/{mtefId}/{fromDecisionLevelId}/{toDecisionLevelId}', 'Budgeting\ScrutinyController@moveByMtef');
    Route::post('/json/scrutiny/move-by-sector/{mtefId}/{sectorId}/{fromDecisionLevelId}/{toDecisionLevelId}', 'Budgeting\ScrutinyController@moveBySector');
    Route::post('/json/scrutiny/move-by-department/{mtefId}/{departmentId}/{fromDecisionLevelId}/{toDecisionLevelId}', 'Budgeting\ScrutinyController@moveByDepartment');
    Route::post('/json/scrutiny/move-by-mtef-section/{mtefSectionId}/{fromDecisionLevelId}/{toDecisionLevelId}', 'Budgeting\ScrutinyController@moveByMtefSection');
    Route::post('/json/scrutiny/assign-user/by-sector/{mtefId}/{sectorId}/{userId}', 'Budgeting\ScrutinyController@assignUserBySector');
    Route::post('/json/scrutiny/assign-user/by-cost-centre/{mtefSectionId}/{userId}', 'Budgeting\ScrutinyController@assignUserByCostCentre');
//    });

    Route::get('/json/forwardFinancialYears/{financialYearId}', 'Setup\FinancialYearController@forwardFinancialYears');
    Route::post('/json/approveMtef/{adminHierarchyId}', 'Planning\MtefController@approveMtef');
    Route::get('/json/admin_hierarchy_ceiling_periods/paginated', 'Budgeting\AdminHierarchyCeilingPeriodController@paginated');
    Route::get('/json/getSectionBudgetService/{adminHierarchyId}/{sectionId}/{planType}', 'Planning\MtefController@getSectionBudget');


        //EXPENDITURE CENTRE VALUES
        Route::get('/json/expenditure_centre_values/paginated', 'Budgeting\ExpenditureCentreValueController@paginated');
        Route::get('/json/expenditure_centre_values/all', 'Budgeting\ExpenditureCentreValueController@index');
        Route::post('/json/expenditure_centre_value/create', 'Budgeting\ExpenditureCentreValueController@store');
        Route::post('/json/expenditure_centre_value/update', 'Budgeting\ExpenditureCentreValueController@update');
        Route::post('/json/expenditure_centre_value/toggleActive', 'Budgeting\ExpenditureCentreValueController@toggleActive');
        Route::get('/json/expenditure_centre_value/delete/{id}', 'Budgeting\ExpenditureCentreValueController@delete');
        Route::get('/json/expenditure_centre_values/trashed', 'Budgeting\ExpenditureCentreValueController@trashed');
        Route::get('/json/expenditure_centre_value/restore/{id}', 'Budgeting\ExpenditureCentreValueController@restore');
        Route::get('/json/expenditure_centre_value/permanentDelete/{id}', 'Budgeting\ExpenditureCentreValueController@permanentDelete');
        Route::get('/json/expenditure_centre_values/emptyTrash', 'Budgeting\ExpenditureCentreValueController@emptyTrash');

        //BUDGET DISTRIBUTION CONDITION FUND SOURCES
    Route::get('/json/main_group_fund_sources/paginated', 'Budgeting\BdcMainGroupFundSourceController@paginated');
    Route::get('/json/main_group_fund_sources/all', 'Budgeting\BdcMainGroupFundSourceController@index');
    Route::post('/json/main_group_fund_source/create', 'Budgeting\BdcMainGroupFundSourceController@store');
    Route::post('/json/main_group_fund_source/update', 'Budgeting\BdcMainGroupFundSourceController@update');
    Route::post('/json/main_group_fund_source/toggleActive', 'Budgeting\BdcMainGroupFundSourceController@toggleActive');
    Route::get('/json/main_group_fund_source/delete/{id}', 'Budgeting\BdcMainGroupFundSourceController@delete');
    Route::get('/json/main_group_fund_sources/trashed', 'Budgeting\BdcMainGroupFundSourceController@trashed');
    Route::get('/json/main_group_fund_source/restore/{id}', 'Budgeting\BdcMainGroupFundSourceController@restore');
    Route::get('/json/main_group_fund_source/permanentDelete/{id}', 'Budgeting\BdcMainGroupFundSourceController@permanentDelete');
    Route::get('/json/main_group_fund_sources/emptyTrash', 'Budgeting\BdcMainGroupFundSourceController@emptyTrash');


    //GROUP DISTRIBUTION FINANCIAL YEAR VALUES
    Route::get('/json/group_financial_year_values/paginated', 'Budgeting\BdcGroupFinancialYearValueController@paginated');
    Route::get('/json/group_financial_year_values/all', 'Budgeting\BdcGroupFinancialYearValueController@index');
    Route::post('/json/group_financial_year_value/create', 'Budgeting\BdcGroupFinancialYearValueController@store');
    Route::post('/json/group_financial_year_value/update', 'Budgeting\BdcGroupFinancialYearValueController@update');
    Route::get('/json/group_financial_year_value/delete/{id}', 'Budgeting\BdcGroupFinancialYearValueController@delete');
    Route::get('/json/group_financial_year_values/trashed', 'Budgeting\BdcGroupFinancialYearValueController@trashed');
    Route::get('/json/group_financial_year_value/restore/{id}', 'Budgeting\BdcGroupFinancialYearValueController@restore');
    Route::get('/json/group_financial_year_value/permanentDelete/{id}', 'Budgeting\BdcGroupFinancialYearValueController@permanentDelete');
    Route::get('/json/group_financial_year_values/emptyTrash', 'Budgeting\BdcGroupFinancialYearValueController@emptyTrash');


    Route::get('/json/getCurrentFinancialYearPlans', 'Planning\MtefController@getCurrentFinancialYearPlans');
    Route::get('/json/approveCurrentFinancialYearPlan', 'Planning\MtefController@approveMtef');
    Route::get('/json/resetCurrentFinancialYearPlan', 'Planning\MtefController@resetMtef');
    Route::get('/json/disapproveCurrentFinancialYearPlan', 'Planning\MtefController@disapproveMtef');
    Route::get('/json/approve-ceiling', 'Budgeting\AdminHierarchyCeilingController@approveByAdminHierarchy');
    Route::get('/json/dis-approve-ceiling', 'Budgeting\AdminHierarchyCeilingController@disApproveByAdminHierarchy');
    Route::get('/json/getAggregateBudget/{admin_hierarchy_id}/{hierarchy_position}/{financial_year_id}/{budget_type}', 'Planning\MtefController@getBudget');

    /*BUDGET DISTRIBUTION CONDITION GROUPS*/
    Route::get('/json/bdcMainGroupFundSources/fetchAll', 'Budgeting\BdcMainGroupFundSourceController@fetchAll');

    Route::get('/json/get-user-planning-fund-sources/{mtefSectionId}/{budgetType}', 'Setup\FundSourceController@getUserPlanningFundSources');
    Route::get('/json/admin-ceiling-docs/{financialYearId}/{adminHierarchyId}/{budgetType}', 'Budgeting\AdminHierarchyCeilingDocController@get');
    Route::post('/api/admin-ceiling-docs/upload', 'Budgeting\AdminHierarchyCeilingDocController@upload');
    Route::post('/api/admin-ceiling-docs/delete', 'Budgeting\AdminHierarchyCeilingDocController@delete');

});

