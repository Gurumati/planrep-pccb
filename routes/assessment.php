<?php
/**
 * Created by PhpStorm.
 * User: Kachinga
 * Date: 3/4/2017
 * Time: 12:43 PM
 */
Route::group(['middleware' => 'authentication'], function () {
    Route::get('/assessment','Assessment\MainController@index');

    Route::group(['middleware' => ['hasPermission:budgeting.manage.assessment, report']], function () {
    //CAS Assessment Category
    Route::get('/json/casAssessmentCategories/paginated', 'Assessment\CasAssessmentCategoryController@paginated');
    Route::get('/json/casAssessmentCategories/all', 'Assessment\CasAssessmentCategoryController@index');
    Route::post('/json/casAssessmentCategory/create', 'Assessment\CasAssessmentCategoryController@store');
    Route::post('/json/casAssessmentCategory/update', 'Assessment\CasAssessmentCategoryController@update');
    Route::get('/json/casAssessmentCategory/delete/{id}', 'Assessment\CasAssessmentCategoryController@delete');
    Route::get('/json/casAssessmentCategories/trashed', 'Assessment\CasAssessmentCategoryController@trashed');
    Route::get('/json/casAssessmentCategory/restore/{id}', 'Assessment\CasAssessmentCategoryController@restore');
    Route::get('/json/casAssessmentCategory/permanentDelete/{id}', 'Assessment\CasAssessmentCategoryController@permanentDelete');
    Route::get('/json/casAssessmentCategories/emptyTrash', 'Assessment\CasAssessmentCategoryController@emptyTrash');

    //CAS Assessment States
    Route::get('/json/casAssessmentStates/paginated', 'Assessment\CasAssessmentStateController@paginated');
    Route::get('/json/casAssessmentStates/all', 'Assessment\CasAssessmentStateController@index');
    Route::post('/json/casAssessmentState/create', 'Assessment\CasAssessmentStateController@store');
    Route::post('/json/casAssessmentState/update', 'Assessment\CasAssessmentStateController@update');
    Route::get('/json/casAssessmentState/delete/{id}', 'Assessment\CasAssessmentStateController@delete');
    Route::get('/json/casAssessmentStates/trashed', 'Assessment\CasAssessmentStateController@trashed');
    Route::get('/json/casAssessmentState/restore/{id}', 'Assessment\CasAssessmentStateController@restore');
    Route::get('/json/casAssessmentState/permanentDelete/{id}', 'Assessment\CasAssessmentStateController@permanentDelete');
    Route::get('/json/casAssessmentStates/emptyTrash', 'Assessment\CasAssessmentStateController@emptyTrash');

    //CAS ASSESSMENT SUB CRITERIA OPTIONS
    Route::get('/json/casAssessmentSubCriteriaOptions/all', 'Assessment\CasAssessmentSubCriteriaOptionController@index');
    Route::get('/json/casAssessmentSubCriteriaByCriteria/all/{criteria_id}', 'Assessment\CasAssessmentSubCriteriaOptionController@subCriteriaByCriteria');
    Route::get('/json/casAssessmentCriteriaByCategory/all/{category_id}', 'Assessment\CasAssessmentCriteriaOptionController@CriteriaByCategory');

    //cas assessment round start
    Route::get('/json/CasAssessmentRounds', 'Assessment\CasAssessmentRoundController@index');
    Route::get('/json/assessment_rounds/all', 'Assessment\CasAssessmentRoundController@index');
    Route::post('/json/CreateCasAssessmentRounds', 'Assessment\CasAssessmentRoundController@store');
    Route::post('/json/UpdateCasAssessmentRounds', 'Assessment\CasAssessmentRoundController@update');
    Route::get('/json/DeleteCasAssessmentRounds/{id}', 'Assessment\CasAssessmentRoundController@delete');
    //cas assessment round end

    //cas assessment possible score start
    Route::get('/json/CasAssessmentSubCriteriaPossibleScore/{sub_criteria_id}', 'Assessment\CasAssessmentSubCriteriaPossibleScoreController@index');
    Route::post('/json/CreateCasAssessmentSubCriteriaPossibleScore', 'Assessment\CasAssessmentSubCriteriaPossibleScoreController@store');
    Route::post('/json/UpdateCasAssessmentSubCriteriaPossibleScore', 'Assessment\CasAssessmentSubCriteriaPossibleScoreController@update');
    Route::get('/json/DeleteCasAssessmentSubCriteriaPossibleScore/{id}', 'Assessment\CasAssessmentSubCriteriaPossibleScoreController@delete');
    //cas assessment possible score end

    //CAS Assessment Category Version
    Route::get('/json/casAssessmentCategoryVersions/paginated', 'Assessment\CasAssessmentCategoryVersionController@paginated');
    Route::get('/json/casAssessmentCategoryVersions/all', 'Assessment\CasAssessmentCategoryVersionController@index');
    Route::post('/json/casAssessmentCategoryVersion/create', 'Assessment\CasAssessmentCategoryVersionController@store');
    Route::post('/json/casAssessmentCategoryVersion/update', 'Assessment\CasAssessmentCategoryVersionController@update');
    Route::get('/json/casAssessmentCategoryVersion/delete/{id}', 'Assessment\CasAssessmentCategoryVersionController@delete');
    Route::get('/json/casAssessmentCategoryVersions/trashed', 'Assessment\CasAssessmentCategoryVersionController@trashed');
    Route::get('/json/casAssessmentCategoryVersion/restore/{id}', 'Assessment\CasAssessmentCategoryVersionController@restore');
    Route::get('/json/casAssessmentCategoryVersion/permanentDelete/{id}', 'Assessment\CasAssessmentCategoryVersionController@permanentDelete');
    Route::get('/json/casAssessmentCategoryVersions/emptyTrash', 'Assessment\CasAssessmentCategoryVersionController@emptyTrash');

    //CAS Assessment Category Version
    Route::get('/json/casAssessmentCategoryVersionStates/paginated', 'Assessment\CasAssessmentCategoryVersionStateController@paginated');
    Route::get('/json/casAssessmentCategoryVersionStates/all', 'Assessment\CasAssessmentCategoryVersionStateController@index');
    Route::post('/json/casAssessmentCategoryVersionState/create', 'Assessment\CasAssessmentCategoryVersionStateController@store');
    Route::post('/json/casAssessmentCategoryVersionState/update', 'Assessment\CasAssessmentCategoryVersionStateController@update');
    Route::get('/json/casAssessmentCategoryVersionState/delete/{id}', 'Assessment\CasAssessmentCategoryVersionStateController@delete');
    Route::get('/json/casAssessmentCategoryVersionStates/trashed', 'Assessment\CasAssessmentCategoryVersionStateController@trashed');
    Route::get('/json/casAssessmentCategoryVersionState/restore/{id}', 'Assessment\CasAssessmentCategoryVersionStateController@restore');
    Route::get('/json/casAssessmentCategoryVersionState/permanentDelete/{id}', 'Assessment\CasAssessmentCategoryVersionStateController@permanentDelete');
    Route::get('/json/casAssessmentCategoryVersionStates/emptyTrash', 'Assessment\CasAssessmentCategoryVersionStateController@emptyTrash');

    //CAS ASSESSMENT CRITERIA OPTIONS
    Route::get('/json/casAssessmentCriteriaOptions/paginated/{category_version_id}', 'Assessment\CasAssessmentCriteriaOptionController@paginated');
    Route::get('/json/casAssessmentCriteriaOptions/all', 'Assessment\CasAssessmentCriteriaOptionController@index');
    Route::post('/json/casAssessmentCriteriaOption/create', 'Assessment\CasAssessmentCriteriaOptionController@store');
    Route::post('/json/casAssessmentCriteriaOption/update', 'Assessment\CasAssessmentCriteriaOptionController@update');
    Route::get('/json/casAssessmentCriteriaOption/delete/{id}', 'Assessment\CasAssessmentCriteriaOptionController@delete');
    Route::get('/json/casAssessmentCriteriaOptions/trashed', 'Assessment\CasAssessmentCriteriaOptionController@trashed');
    Route::get('/json/casAssessmentCriteriaOption/restore/{id}', 'Assessment\CasAssessmentCriteriaOptionController@restore');
    Route::get('/json/casAssessmentCriteriaOption/permanentDelete/{id}', 'Assessment\CasAssessmentCriteriaOptionController@permanentDelete');
    Route::get('/json/casAssessmentCriteriaOptions/emptyTrash', 'Assessment\CasAssessmentCriteriaOptionController@emptyTrash');

    //CAS ASSESSMENT CRITERIA OPTIONS
    Route::get('/json/casAssessmentSubCriteriaOptions/paginated/', 'Assessment\CasAssessmentSubCriteriaOptionController@paginated');
    Route::get('/json/casAssessmentSubCriteriaOptions/fetchAll', 'Assessment\CasAssessmentSubCriteriaOptionController@fetchAll');
    Route::post('/json/casAssessmentSubCriteriaOption/create', 'Assessment\CasAssessmentSubCriteriaOptionController@store');
    Route::post('/json/casAssessmentSubCriteriaOption/update', 'Assessment\CasAssessmentSubCriteriaOptionController@update');
    Route::get('/json/casAssessmentSubCriteriaOption/delete/{id}', 'Assessment\CasAssessmentSubCriteriaOptionController@delete');
    Route::get('/json/casAssessmentSubCriteriaOptions/trashed', 'Assessment\CasAssessmentSubCriteriaOptionController@trashed');
    Route::get('/json/casAssessmentSubCriteriaOption/restore/{id}', 'Assessment\CasAssessmentSubCriteriaOptionController@restore');
    Route::get('/json/casAssessmentSubCriteriaOption/permanentDelete/{id}', 'Assessment\CasAssessmentSubCriteriaOptionController@permanentDelete');
    Route::get('/json/casAssessmentSubCriteriaOptions/emptyTrash', 'Assessment\CasAssessmentSubCriteriaOptionController@emptyTrash');
    Route::post('/json/casAssessmentSubCriteriaOption/toggleFreeScore', 'Assessment\CasAssessmentSubCriteriaOptionController@toggleFreeScore');

    //cas assessment report set start
    Route::get('/json/CasAssessmentSubCriteriaReportSet/{sub_criteria_id}', 'Assessment\CasAssessmentSubCriteriaReportSetController@index');
    Route::post('/json/UpdateResults', 'Assessment\CasAssessmentResultDetailController@update_result');
    Route::post('/json/UpdateResultDetails', 'Assessment\CasAssessmentResultDetailController@update_result_details');
    Route::post('/json/CreateCasAssessmentSubCriteriaReportSet', 'Assessment\CasAssessmentSubCriteriaReportSetController@store');
    Route::post('/json/UpdateCasAssessmentSubCriteriaReportSet', 'Assessment\CasAssessmentSubCriteriaReportSetController@update');
    Route::get('/json/DeleteCasAssessmentSubCriteriaReportSet/{id}', 'Assessment\CasAssessmentSubCriteriaReportSetController@delete');
    //cas assessment report set end

    //cas assessment results
    Route::get('/json/casAssessmentFilteredCriteria/{criteria_version_id}', 'Assessment\CasAssessmentResultDetailController@filteredCriteria');
    Route::get('/json/casAssessmentCategoryVersions/filtered', 'Assessment\CasAssessmentResultDetailController@filteredCategoryVersions');
    Route::get('/json/casAssessmentCategoryVersions', 'Assessment\CasAssessmentResultDetailController@CategoryVersions');
    Route::get('/json/allCouncils/{category_version_id}', 'Assessment\CasAssessmentResultDetailController@councils');
    Route::get('/json/allRounds/{category_version_id}/{council_id}', 'Assessment\CasAssessmentResultDetailController@rounds');
    Route::get('/json/assigned_Rounds/{category_version_id}/{council_id}', 'Assessment\CasAssessmentResultDetailController@Assignedrounds');
    Route::get('/json/periods/{category_version_id}/{council_id}/{round_id}/{financial_year_id}', 'Assessment\CasAssessmentResultDetailController@periods');
    Route::get('/json/assigned_periods/{category_version_id}/{council_id}/{round_id}/{financial_year_id}', 'Assessment\CasAssessmentResultDetailController@AssignedPeriods');
    Route::get('/json/subCriteria/{criteria_id}/{result_id}', 'Assessment\CasAssessmentResultDetailController@filteredSubCriteria');
    Route::get('/json/strategicPlan/{adminHierarchId}/{financialYearId}', 'Assessment\CasAssessmentResultDetailController@getStrategicPlan');
    Route::get('/json/possibleScores/{sub_criteria_id}', 'Assessment\CasAssessmentResultDetailController@filteredSubCriteriaScores');
    Route::get('/json/OpenAssessmentResult/{category_version_id}/{period_id}/{round_id}/{admin_hierarchy_id}', 'Assessment\CasAssessmentResultDetailController@council_round_results');
    Route::get('/json/MyAssessmentResults', 'Assessment\CasAssessmentResultDetailController@my_assessment_results');
    Route::get('/json/ReportSet/{sub_criteria_id}', 'Assessment\CasAssessmentResultDetailController@sub_criteria_report_set');
    Route::get('/json/UpdateReply/{id}/{comment}/{category}/{category_id}/{status}', 'Assessment\CasAssessmentResultDetailController@update_reply');
    Route::get('/json/GetReply/{id}/{category}/{category_id}', 'Assessment\CasAssessmentResultDetailController@get_reply');
    });
});
