<?php

/**
 * Route for setup services with permission comma separated.
 */
Route::group(['middleware' => 'authentication'], function () {
    Route::get('/setup', 'Setup\MainController@index');
});

Route::get('/api/test', function () {
    return ['name' => 'Planrep'];
});
Route::get('/json/ping', function () {
    $time = (microtime(true) - LARAVEL_START);
    return $time . " seconds";
});
Route::get('/json/clearCacheByKey/{key}', 'Controller@clearCacheByKey');
Route::get('/json/clearAllCache', 'Controller@clearAllCache');

Route::get('/json/ping-db', function () {
    $beforeQuery = (microtime(true) - LARAVEL_START);
    $all = \Illuminate\Support\Facades\DB::table('admin_hierarchies')->get();
    $count = sizeof($all);
    $afterQuery = (microtime(true) - LARAVEL_START);
    $queryTime = ($afterQuery - $beforeQuery);
    return "Query  " . $count . " Admin hierarchies; Before query=" . $beforeQuery . " secs ; After query=" . $afterQuery . " secs; Query time=" . $queryTime . " secs";
});

Route::group(['middleware' => ['ajaxAuthentication', 'hasPermission:settings.manage,planning,rights,budgeting,execution,assessment,report']], function () {
//INTERNAL API

    Route::get('/json/pushToFacility', 'Budgeting\AdminHierarchyCeilingController@pushToFacility');

    Route::get('/api/sectors/userSectors', 'Setup\SectorController@userSectors');
    Route::get('/api/sectors/departments', 'Setup\SectorController@departments');
    Route::get('/api/sections/costCentres', 'Setup\SectorController@departmentCostCentres');
    Route::get('/api/gfsCodes/costCentres', 'Setup\GfsCodeController@costCentres');
    Route::post('/api/gfsCodes/addCostCentres', 'Setup\GfsCodeController@addCostCentres');
    Route::get('/api/gfsCodes/removeCostCentre', 'Setup\GfsCodeController@removeCostCentre');

//Route::group(['middleware' => ['hasPermission:settings.manage.configurations']], function () {
    Route::get('/json/configurations', 'Setup\ConfigurationSettingController@index');
    Route::post('/json/updateConfigurations', 'Setup\ConfigurationSettingController@update');
//});

//Route::group(['middleware' => ['hasPermission:settings.manage.admin_settings, report']], function () {
    /**Admin Hierarchy Levels protected*/
    Route::get('/json/adminHierarchyLevels', 'Setup\AdminHierarchyLevelController@index');
    Route::get('/json/adminHierarchyLevels/fetchAll', 'Setup\AdminHierarchyLevelController@fetchAll');
    Route::post('/json/createAdminHierarchyLevel', 'Setup\AdminHierarchyLevelController@store');
    Route::post('/json/updateAdminHierarchyLevel', 'Setup\AdminHierarchyLevelController@update');
    Route::post('/json/deleteAdminHierarchyLevel/{id}', 'Setup\AdminHierarchyLevelController@delete');
    Route::post('/json/toggleAdminHierarchyLevel', 'Setup\AdminHierarchyLevelController@toggleAdminHierarchyLevel');
    Route::get('/json/admin_hierarchy_levels/trashed', 'Setup\AdminHierarchyLevelController@trashed');
    Route::get('/json/admin_hierarchy_level/restore/{id}', 'Setup\AdminHierarchyLevelController@restore');
    Route::get('/json/deleteAdminHierarchyLevel/{id}', 'Setup\AdminHierarchyLevelController@delete');
    Route::get('/json/admin_hierarchy_level/permanentDelete/{id}', 'Setup\AdminHierarchyLevelController@permanentDelete');
    Route::get('/json/admin_hierarchy_level/emptyTrash', 'Setup\AdminHierarchyLevelController@emptyTrash');

/**Admin Hierarchies protected*/
    Route::post('/json/createAdminHierarchy', 'Setup\AdminHierarchyController@store');
    Route::post('/json/updateAdminHierarchy', 'Setup\AdminHierarchyController@update');
    Route::get('/json/deleteAdminHierarchy/{id}', 'Setup\AdminHierarchyController@delete');
    Route::post('/admin-hierarchies/upload', 'Setup\AdminHierarchyController@upload');

/**Decision levels protected*/
    Route::get('/json/paginatedDecisionLevels', 'Setup\DecisionLevelController@paginated');
    Route::post('/json/createDecisionLevel', 'Setup\DecisionLevelController@store');
    Route::post('/json/updateDecisionLevel', 'Setup\DecisionLevelController@update');
    Route::post('/json/deleteDecisionLevel/{id}', 'Setup\DecisionLevelController@delete');
    Route::post('/json/defaultDecisionLevel', 'Setup\DecisionLevelController@defaultDecisionLevel');

/**Sector protected*/
    Route::get('/json/sectorsPagination', 'Setup\SectorController@indexPagination');
    Route::post('/json/createSector', 'Setup\SectorController@store');
    Route::post('/json/updateSector', 'Setup\SectorController@update');
    Route::post('/json/deleteSector/{id}', 'Setup\SectorController@delete');

/**Section Level protected*/
    Route::post('/json/createSectionLevel', 'Setup\SectionLevelController@store');
    Route::post('/json/updateSectionLevel', 'Setup\SectionLevelController@update');
    Route::get('/json/deleteSectionLevel/{id}', 'Setup\SectionLevelController@delete');
    Route::post('/json/toggleSectionLevel', 'Setup\SectionLevelController@toggleSectionLevel');
    Route::get('/json/paginatedSectionLevels', 'Setup\SectionLevelController@paginated');

/**Section protected*/
    Route::get('/json/PaginateSections', 'Setup\SectionController@paginateSection');
    Route::post('/json/createSection', 'Setup\SectionController@store');
    Route::post('/json/updateSection', 'Setup\SectionController@update');
    Route::post('/json/deleteSection/{id}', 'Setup\SectionController@delete');
    Route::get('/json/sections-to-be-activated', 'Setup\SectionController@sectionsToActivate');
    Route::post('/json/activate-section', 'Setup\SectionController@activateSections');

/**Calendar Event*/
    Route::get('/json/calendarEvents/paginated', 'Setup\CalendarEventController@paginated');
    Route::post('/json/calendarEvents', 'Setup\CalendarEventController@store');
    Route::put('/json/calendarEvents/{id}', 'Setup\CalendarEventController@update');
    Route::delete('/json/calendarEvents/{id}', 'Setup\CalendarEventController@delete');
    Route::get('/json/calendarEvents/trashed', 'Setup\CalendarEventController@trashed');
    Route::get('/json/calendarEvents/{id}/restore', 'Setup\CalendarEventController@restore');
    Route::get('/json/calendarEvents/{id}/permanentDelete', 'Setup\CalendarEventController@permanentDelete');
    Route::get('/json/calendarEvents/emptyTrash', 'Setup\CalendarEventController@emptyTrash');

/**Calendar*/
    Route::get('/json/calendars/paginated', 'Setup\CalendarController@paginated');
    Route::post('/json/calendars', 'Setup\CalendarController@store');
    Route::put('/json/calendars/{id}', 'Setup\CalendarController@update');
    Route::delete('/json/calendars/{id}', 'Setup\CalendarController@delete');
    Route::get('/json/calendars/trashed', 'Setup\CalendarController@trashed');
    Route::get('/json/calendars/{id}/restore', 'Setup\CalendarController@restore');
    Route::get('/json/calendars/{id}/permanentDelete', 'Setup\CalendarController@permanentDelete');
    Route::get('/json/calendars/emptyTrash', 'Setup\CalendarController@emptyTrash');

/**Reminder Receipt*/
    Route::get('/json/eventRecipients/paginated', 'Setup\CalendarEventReminderRecipientController@paginated');
    Route::post('/json/eventRecipients', 'Setup\CalendarEventReminderRecipientController@store');
    Route::put('/json/eventRecipients/{id}', 'Setup\CalendarEventReminderRecipientController@update');
    Route::delete('/json/eventRecipients/{id}', 'Setup\CalendarEventReminderRecipientController@delete');
    Route::get('/json/eventRecipients/trashed', 'Setup\CalendarEventReminderRecipientController@trashed');
    Route::get('/json/eventRecipients/{id}/restore', 'Setup\CalendarEventReminderRecipientController@restore');
    Route::get('/json/eventRecipients/{id}/permanentDelete', 'Setup\CalendarEventReminderRecipientController@permanentDelete');
    Route::get('/json/eventRecipients/emptyTrash', 'Setup\CalendarEventReminderRecipientController@emptyTrash');
//});

//Route::group(['middleware' => ['hasPermission:settings.manage.location_settings, report']], function () {
    /**Geographical Location Levels*/
    //Route::group(['middleware' => ['hasPermission:settings.manage.location_settings']], function () {
    Route::get('/json/geoLocationLevels', 'Setup\GeoLocationLevelController@index');
    Route::get('/json/allGeolocationLevels', 'Setup\GeoLocationLevelController@all');
    Route::get('/json/geoLocationLevels/fetchAll', 'Setup\GeoLocationLevelController@fetchAll');
    Route::post('/json/createGeolocationLevel', 'Setup\GeoLocationLevelController@store');
    Route::post('/json/updateGeoLocationLevel', 'Setup\GeoLocationLevelController@update');
    Route::get('/json/deleteGeoLocationLevel/{id}', 'Setup\GeoLocationLevelController@delete');
    Route::post('/json/toggleGeoLocationLevel', 'Setup\GeoLocationLevelController@toggleGeoLocationLevel');
    Route::get('/json/geo_location_levels/trashed', 'Setup\GeoLocationLevelController@trashed');
    Route::get('/json/geo_location_level/restore/{id}', 'Setup\GeoLocationLevelController@restore');
    Route::get('/json/geo_location_level/permanentDelete/{id}', 'Setup\GeoLocationLevelController@permanentDelete');
    Route::get('/json/geo_location_level/emptyTrash', 'Setup\GeoLocationLevelController@emptyTrash');

    /**Geographical location*/
    Route::get('/json/geoLocations', 'Setup\GeoLocationController@index');
    Route::post('/json/createGeoLocation', 'Setup\GeoLocationController@store');
    Route::post('/json/updateGeoLocation', 'Setup\GeoLocationController@update');
    Route::get('/json/deleteGeoLocation/{id}', 'Setup\GeoLocationController@delete');
    Route::post('/geoLocations/upload', 'Setup\GeoLocationController@upload');

    Route::get('/json/loadParentGeoLocations/{childGeoLocationLevelId}', 'Setup\GeoLocationController@loadParentGeoLocation');
    Route::get('/json/geoLocations/globalGeoLocations', 'Setup\GeoLocationController@globalGeoLocations');
    Route::get('/json/geoLocations/wards', 'Setup\GeoLocationController@wards');
    Route::get('/json/geoLocations/councilWards', 'Setup\GeoLocationController@councilWards');
    Route::get('/json/geoLocations/villages', 'Setup\GeoLocationController@villages');
    Route::get('/json/geoLocations/wardVillages', 'Setup\GeoLocationController@wardVillages');
    Route::get('/json/geoLocations/parentGeoLocation', 'Setup\GeoLocationController@parentGeoLocation');
    Route::get('/json/geoLocations/getGeoLocationChildren/{id}', 'Setup\GeoLocationController@getGeoLocationChildren');
    Route::get('/json/geoLocations/search', 'Setup\GeoLocationController@search');

///Get geographical locations
    Route::get('/json/geoLocations/fetch-all-regions', 'Setup\GeoLocationController@fetchAllRegions');
    Route::get('/json/geoLocations/get-council-from-regions', 'Setup\GeoLocationController@getCouncilFromRegion');
//});

//Route::group(['middleware' => ['hasPermission:settings.manage.finance_settings, report']], function () {
    /**reference document start **/
    Route::get('/json/reference-documents', 'Setup\ReferenceDocumentController@index');
    Route::get('/json/reference-documents/fetchAll', 'Setup\ReferenceDocumentController@fetchAll');
    Route::get('/json/reference-documents/national-guidelines', 'Setup\ReferenceDocumentController@nationalGuideLineRefDocs');
    Route::get('/json/reference-documents/{isNationalGuideline}/paginated', 'Setup\ReferenceDocumentController@paginated');
/** create with file upload*/
    Route::post('/form/reference-documents/{isNationalGuideline}', 'Setup\ReferenceDocumentController@store');
    Route::post('/json/reference-documents/{isNationalGuideline}', 'Setup\ReferenceDocumentController@store');
/** Update with file upload **/
    Route::post('/form/reference-documents/{isNationalGuideline}/{id}', 'Setup\ReferenceDocumentController@update');
    Route::put('/json/reference-documents/{isNationalGuideline}/{id}', 'Setup\ReferenceDocumentController@update');
    Route::delete('/json/reference-documents/{isNationalGuideline}/{id}', 'Setup\ReferenceDocumentController@delete');
/**reference document ends **/
//});

/**Admin Hierarchy Levels Shared*/
    Route::get('/json/allAdminHierarchyLevels', 'Setup\AdminHierarchyLevelController@all');
    Route::get('/json/adminHierarchyLevelsByUserService', 'Setup\AdminHierarchyLevelController@getByUser');
    Route::get('/json/admin_hierarchy_levels/admin_hierarchies', 'Setup\AdminHierarchyLevelController@admin_hierarchies');

/**Admin Hierarchies shared*/
    Route::get('/json/adminHierarchies', 'Setup\AdminHierarchyController@index');
    Route::get('/json/allAdminHierarchies', 'Setup\AdminHierarchyController@index');
    Route::get('/json/userHierarchies', 'Setup\AdminHierarchyController@userHierarchies');
    Route::get('/json/loadParentAdminHierarchies/{childAdminHierarchyLevelId}', 'Setup\AdminHierarchyController@loadParentAdminHierarchy');
    Route::get('/json/onMinistryloadParentAdminHierarchies/{ministryId}', 'Setup\AdminHierarchyController@onMinistryloadParentAdminHierarchy');
    Route::get('/json/Load-parent-by-id', 'Setup\AdminHierarchyController@loadParentById');
    Route::get('/json/getAdminHierarchyByFacilityTypeId/{id}', 'Setup\AdminHierarchyController@getAdminHierarchiesByFacilityTypeID');
    Route::get('/json/adminHierarchies/globalAdminHierarchies', 'Setup\AdminHierarchyController@globalAdminHierarchies');
    Route::get('/json/adminHierarchies/globalAdminHierarchies', 'Setup\AdminHierarchyController@globalAdminHierarchies');
    Route::get('/json/adminHierarchies/wards', 'Setup\AdminHierarchyController@wards');
    Route::get('/json/adminHierarchies/councilWards', 'Setup\AdminHierarchyController@councilWards');
    Route::get('/json/adminHierarchies/villages', 'Setup\AdminHierarchyController@villages');
    Route::get('/json/adminHierarchies/wardVillages', 'Setup\AdminHierarchyController@wardVillages');
    Route::get('/json/adminHierarchies/userAdminHierarchy', 'Setup\AdminHierarchyController@userAdminHierarchy');
    Route::get('/json/adminHierarchies/getBudgetingChildLevels', 'Setup\AdminHierarchyController@getBudgetingChildLevels');
    Route::get('/json/adminHierarchies/parentAdminHierarchy', 'Setup\AdminHierarchyController@parentAdminHierarchy');
    Route::get('/json/adminHierarchies/getAdminHierarchyChildren/{adminHierarchyId}', 'Setup\AdminHierarchyController@getAdminHierarchyChildren');
    Route::get('/json/adminHierarchies/search', 'Setup\AdminHierarchyController@search');
    Route::get('/json/adminHierarchy/piscs', 'Setup\AdminHierarchyController@piscs');

/** get PICS name from service provider */
    Route::get('/json/admin_hierarchies/get-pics-name-from-sp', 'Setup\AdminHierarchyController@getPicsNameFromSp');
    Route::get('/json/adminHierarchy/service-provider-type', 'Setup\AdminHierarchyController@serviceProviderType');

/**Decision levels shared*/
    Route::get('/json/decisionLevels', 'Setup\DecisionLevelController@index');
    Route::get('/json/allDecisionLevels', 'Setup\DecisionLevelController@index');
    Route::get('/json/decisionLevelsByAdminHierarchyLevel/{adminHierarchyLevelId}', 'Setup\DecisionLevelController@decisionLevelByAdminHierarchyLevel');

/**Sector shared*/
    Route::get('/json/sectors', 'Setup\SectorController@index');
    Route::get('/json/planChainSectorsOnly', 'Setup\PlanChainSectorController@indexNoRelation');
    Route::get('/json/sectors/{sector_id}/planning_units', 'Setup\SectorController@planning_units');
    Route::get('/json/sectors/sections', 'Setup\SectorController@sections');
    Route::get('/json/sectors/fetchAll', 'Setup\SectorController@fetchAll');
    Route::get('/json/sectorsPsc/fetch', 'Setup\AdminHierarchyController@getBysectorId');
    Route::get('/json/sectors/filterSections', 'Setup\SectorController@sectorCostCentres');

/**Section Levels*/
    Route::get('/json/sectionLevels', 'Setup\SectionLevelController@index');
    Route::get('/json/allSectionLevelsByUser', 'Setup\SectionLevelController@getByUser');
    Route::get('/json/loadParentLevelSectionLevel/{sectionLevelId}', 'Setup\SectionLevelController@parentSectionLevel');

/**Sections*/
    Route::get('/json/sections', 'Setup\SectionController@index');
    Route::get('/json/piscBysector/{childSectorId}', 'Setup\SectionController@getPiscBySector');
    Route::get('/json/loadParentSections/{childSectionLevelId}', 'Setup\SectionController@loadParentSections');
    Route::get('/json/sectionsByLevel/{levelId}/{adminHierarchyId}', 'Setup\SectionController@getByLevel');
    Route::get('/json/sectionsByLevelFilter/{levelId}', 'Setup\SectionController@getByLevelFilter');
    Route::get('/json/allSections', 'Setup\SectionController@allSections');
    Route::get('/json/section/filter/{id}', 'Setup\SectionController@filter');
    Route::get('/json/section/filter/{id}/{sector_id}', 'Setup\SectionController@filter');
    Route::get('/json/section_by_admin/filter/{id}/{sector_id}/{admin_id}', 'Setup\SectionController@getSectionsByAdmin');
    Route::get('/json/section_by_admin/filter/{id}/{admin_id}', 'Setup\SectionController@getSectionsByAdmin');
    Route::get('/json/sections/fetchAll', 'Setup\SectionController@fetchAll');
    Route::get('/json/sections/planningUnits', 'Setup\SectionController@planningUnits');
    Route::get('/json/sections/sectorCostCentres', 'Setup\SectionController@sectorCostCentres');
    Route::get('/json/sections/by-section-level/{sectionLevelId}/by-facility/{facilityId}/by-fund-source/{fundSourceId}', 'Setup\SectionController@byFacility');

/**Calender Events*/
    Route::get('/json/calendarEvents/all', 'Setup\CalendarEventController@index');
    Route::get('/json/calendar-events/get-by-year-and-level/{financialYearId}/{hierarchyPosition}', 'Setup\CalendarEventController@getByYearAndLevel');

/** Calendar*/
    Route::get('/json/calendars/all', 'Setup\CalendarController@index');

/**Remainder Receipt*/
    Route::get('/json/eventRecipients/all', 'Setup\CalendarEventReminderRecipientController@index');
/**Notification*/

/**Financial Year*/
    Route::get('/json/financialYears', 'Setup\FinancialYearController@index');
    Route::get('/json/allFinancialYears', 'Setup\FinancialYearController@index');
    Route::post('/json/createFinancialYear', 'Setup\FinancialYearController@store');
    Route::post('/json/updateFinancialYear', 'Setup\FinancialYearController@update');
    Route::post('/json/deleteFinancialYear/{id}', 'Setup\FinancialYearController@delete');
    Route::post('/json/openFinancialYear/{id}', 'Setup\FinancialYearController@open');
    Route::post('/json/openFinancialYearPreRequest/{id}', 'Setup\FinancialYearController@openPreRequest');
    Route::get('/json/open-financial-year-by-section/{sectionId}', 'Setup\FinancialYearController@openBySection');
    Route::get('/json/financial-year', 'Setup\FinancialYearController@all');
    Route::put('/json/financial-year/execute/{id}', 'Setup\FinancialYearController@execute');
    Route::put('/json/financial-year/reExecute/{id}', 'Setup\FinancialYearController@reExecute');
    Route::put('/json/financial-year/close/{id}', 'Setup\FinancialYearController@close');
    Route::get('/json/lastFinancialYear', 'Setup\FinancialYearController@lastFinancialYear');
    Route::get('/json/calenderFinancialYear', 'Setup\FinancialYearController@getCalenderFinancialYears');
    Route::get('/json/financialYears/nextFinancialYears', 'Setup\FinancialYearController@nextFinancialYears');
    Route::get('/json/financialYears/{start_financial_year_id}/{end_financial_year_id}/projectionFinancialYears', 'Setup\FinancialYearController@projectionFinancialYears');
    Route::get('/json/financialYears/forIndicatorProjection', 'Setup\FinancialYearController@forIndicatorProjection');
    Route::get('/json/financialYears/previousFinancialYears', 'Setup\FinancialYearController@previousFinancialYears');
    Route::get('/json/financialYears/allFinancialYears', 'Setup\FinancialYearController@fetchAll');
    Route::get('/json/financialYears/versions', 'Setup\FinancialYearController@versions');
    Route::get('/json/financialYears/otherFinancialYearVersions', 'Setup\FinancialYearController@otherFinancialYearVersions');
    Route::post('/json/financialYears/addVersion', 'Setup\FinancialYearController@addVersion');
    Route::get('/json/financialYears/removeVersion', 'Setup\FinancialYearController@removeVersion');

//Route::group(['middleware' => ['hasPermission:settings.manage.rights, report']], function () {
    Route::get('/json/accessRights', 'Auth\AccessRightController@index');
    Route::post('/json/createAccessRight', 'Auth\AccessRightController@store');
    Route::post('/json/updateAccessRight', 'Auth\AccessRightController@update');
    Route::post('/json/deleteAccessRight/{id}', 'Auth\AccessRightController@delete');
//});

    Route::get('/json/referenceTypes', 'Setup\ReferenceTypeController@index');
    Route::get('/json/referenceTypesReference/{linkLevel}', 'Setup\ReferenceTypeController@typeWithReference');
    Route::post('/json/createReferenceType', 'Setup\ReferenceTypeController@store');
    Route::post('/json/updateReferenceType', 'Setup\ReferenceTypeController@update');
    Route::post('/json/deleteReferenceType/{id}', 'Setup\ReferenceTypeController@delete');
    Route::get('/json/getByReferenceTypeId/{referenceTypeId}', 'Setup\ReferenceController@getByReferenceTypeId');
    Route::get('/json/getParentByReferenceTypeId/{referenceTypeId}/{childReferenceId}', 'Setup\ReferenceController@getParentByReferenceTypeId');
    Route::post('/json/createReference', 'Setup\ReferenceController@store');
    Route::post('/json/updateReference', 'Setup\ReferenceController@update');

    Route::post('/upload/adminHierarchies', 'Misc\UploadController@showUploadFile');
    Route::get('/json/funders', 'Setup\FunderController@index');
    Route::get('/json/paginateFunders', 'Setup\FunderController@paginateIndex');
    Route::post('/json/createFunder', 'Setup\FunderController@store');
    Route::post('/json/updateFunder', 'Setup\FunderController@update');
    Route::get('/json/deleteFunder/{id}', 'Setup\FunderController@delete');
    Route::post('/json/activateFunder/', 'Setup\FunderController@activate');

    Route::get('/json/paginatedResponsiblePersons', 'Setup\ResponsiblePersonController@paginated');
    Route::get('/json/responsible-persons-by-admin-area-sector/{adminHierarchyId}/{sectionId}', 'Setup\ResponsiblePersonController@byAdminAreaAndSector');
    Route::post('/json/createResponsiblePerson', 'Setup\ResponsiblePersonController@store');
    Route::post('/json/updateResponsiblePerson', 'Setup\ResponsiblePersonController@update');
    Route::get('/json/deleteResponsiblePerson/{id}', 'Setup\ResponsiblePersonController@delete');

//Route::group(['middleware' => ['hasPermission:settings.manage.gfs_codes_settings, settings.manage.gfs_codes, report']], function () {
    Route::get('/json/units', 'Setup\UnitController@index');
    Route::get('/json/paginate', 'Setup\UnitController@paginate');
    Route::post('/json/createUnit', 'Setup\UnitController@store');
    Route::post('/json/updateUnit', 'Setup\UnitController@update');
    Route::post('/json/deleteUnit/{id}', 'Setup\UnitController@delete');
    Route::post('/json/toggleUnit/', 'Setup\UnitController@toggleUnit');
    Route::post('/json/units/upload', 'Setup\UnitController@upload');
//});

    Route::get('/json/userGroups', 'Auth\UserGroupController@index');
    Route::post('/json/createUserGroup', 'Auth\UserGroupController@store');
    Route::post('/json/updateUserGroup', 'Auth\UserGroupController@update');
    Route::post('/json/deleteUserGroup/{id}', 'Auth\UserGroupController@delete');
    Route::post('/json/toggleUserGroup/', 'Auth\UserGroupController@toggleUserGroup');

//The routes which control budget classes start here
    Route::get('/json/budgetClasses', 'Setup\BudgetClassController@index');
    Route::get('/json/budgetClasses/copy', 'Setup\BudgetClassController@copy');
    Route::get('/json/parentBudgetClasses/{childId}/{financialYearId}/{versionId}', 'Setup\BudgetClassController@parentBudget');
    Route::get('/json/planningBudgetClasses', 'Setup\BudgetClassController@planningBudgetClasses');
    Route::post('/json/createBudgetClass', 'Setup\BudgetClassController@store');
    Route::post('/json/updateBudgetClass', 'Setup\BudgetClassController@update');
    Route::delete('/json/deleteBudgetClass/{id}', 'Setup\BudgetClassController@delete');

//Route::group(['middleware' => ['hasPermission:settings.manage.facilities_settings, report']], function () {
    //The routes which control facility types start here
    Route::get('/json/facilityTypes', 'Setup\FacilityTypeController@index');
    Route::get('/json/getAllFacilityTypes', 'Setup\FacilityTypeController@index');
    Route::get('/json/paginatedFacilityTypes', 'Setup\FacilityTypeController@paginated');
    Route::post('/json/createFacilityType', 'Setup\FacilityTypeController@store');
    Route::post('/json/updateFacilityType', 'Setup\FacilityTypeController@update');
    Route::get('/json/deleteFacilityType/{id}', 'Setup\FacilityTypeController@delete');
    Route::get('/json/facilityTypes/code', 'Setup\FacilityTypeController@code');
    Route::get('/json/facilityTypes/facilityTypeAdminHierarchies', 'Setup\FacilityController@facilityTypeAdminHierarchies');
//The routes which control facility types end here

//The routes which control LGA Levels start here
    Route::get('/json/allLgaLevels', 'Setup\LgaLevelController@index');
//Ends here
    //});

//Route::group(['middleware' => ['hasPermission:settings.manage.facilities, report']], function () {
    //The routes which control facilities start here
    Route::get('/json/facilities', 'Setup\FacilityController@index');
    Route::get('/json/paginatedFacilities', 'Setup\FacilityController@paginated');
    Route::post('/json/createFacility', 'Setup\FacilityController@store');
    Route::post('/json/updateFacility', 'Setup\FacilityController@update');
    Route::post('/json/deleteFacility/{id}', 'Setup\FacilityController@delete');
    Route::post('/json/facilities/upload', 'Setup\FacilityController@upload');
    Route::get('/json/facilities/search', 'Setup\FacilityController@search');
    Route::get('/json/facilitiesBySector/{sector_id}/{council}', 'Setup\FacilityController@facilityBySector');
    Route::get('/json/facilities/uploadFFARSFacilities', 'Setup\FacilityController@uploadFFARSFacilities');
    Route::post('/json/facilities/setBankAccount', 'Setup\FacilityController@setBankAccount');
    Route::get('/json/facilities/deleteBankAccount', 'Setup\FacilityController@deleteBankAccount');
    Route::get('/json/facilities/facilityTypeCustomDetails', 'Setup\FacilityController@facilityTypeCustomDetails');
    Route::post('/json/facilities/setCustomDetails', 'Setup\FacilityController@setCustomDetails');
    Route::get('/json/facilities/deleteCustomDetail', 'Setup\FacilityController@deleteCustomDetail');
    Route::get('/json/facilities/search-facility', 'Setup\FacilityController@searchFacility');
    Route::get('/files/facilities/export/excel', 'Setup\FacilityController@excel');
    Route::get('/json/facilities/facilityHasFacility', 'Setup\FacilityController@facilityHasFacility');
//The routes which control facilities end here
    //});

//Route::group(['middleware' => ['hasPermission:settings.manage.planning_settings, report']], function () {
    /**Activity Categories Start*/
    Route::get('/json/activity-categories', 'Setup\ActivityCategoryController@index');
    Route::get('/json/activityCategories/fetchAll', 'Setup\ActivityCategoryController@fetchAll');
    Route::get('/json/activity-categories/paginated', 'Setup\ActivityCategoryController@paginated');
    Route::put('/json/activity-categories/toggle-active', 'Setup\ActivityCategoryController@toggleActive');
    Route::post('/json/activity-categories', 'Setup\ActivityCategoryController@store');
    Route::put('/json/activity-categories/{id}', 'Setup\ActivityCategoryController@update');
    Route::delete('/json/activity-categories/{id}', 'Setup\ActivityCategoryController@delete');
/**Activity Categories End*/
//});

//Route::group(['middleware' => ['hasPermission:settings.manage.gfs_codes_settings, settings.manage.gfs_codes, report']], function () {
    Route::get('/json/paginatedProcurementTypes', 'Setup\ProcurementTypeController@paginated');
    Route::get('/json/procurementTypes', 'Setup\ProcurementTypeController@index');
    Route::post('/json/createProcurementType', 'Setup\ProcurementTypeController@store');
    Route::post('/json/updateProcurementType', 'Setup\ProcurementTypeController@update');
    Route::post('/json/toggleProcurementType', 'Setup\ProcurementTypeController@toggleProcurementType');
    Route::get('/json/deleteProcurementType/{id}', 'Setup\ProcurementTypeController@delete');

    Route::get('/json/accountTypes', 'Setup\AccountTypeController@index');
    Route::post('/json/createAccountType', 'Setup\AccountTypeController@store');
    Route::post('/json/updateAccountType', 'Setup\AccountTypeController@update');
    Route::get('/json/deleteAccountType/{id}', 'Setup\AccountTypeController@delete');
//});

    Route::get('/json/planChains', 'Setup\PlanChainController@index');
    Route::get('/json/planChainsType/{planChainTypeId}', 'Setup\PlanChainController@loadPlanChainType');
    Route::post('/json/createPlanChain', 'Setup\PlanChainController@store');
    Route::post('/json/updatePlanChain', 'Setup\PlanChainController@update');
    Route::get('/json/deletePlanChain/{id}', 'Setup\PlanChainController@delete');
    Route::post('/json/activatePlanChain/', 'Setup\PlanChainController@activate');
    Route::get('/files/plan-chains/export', 'Setup\PlanChainController@exportToCsv');
    Route::get('/json/plan-chains/get-top-level', 'Setup\PlanChainController@getTopLevel');
    Route::get('/json/plan-chains/get-children/{parentId}', 'Setup\PlanChainController@getChildren');

    Route::get('/json/projectFunders', 'Setup\ProjectFunderController@index');
    Route::get('/json/paginateProjectFunders', 'Setup\ProjectFunderController@paginateIndex');
    Route::post('/json/createProjectFunder', 'Setup\ProjectFunderController@store');
    Route::post('/json/updateProjectFunder', 'Setup\ProjectFunderController@update');
    Route::get('/json/deleteProjectFunder/{id}', 'Setup\ProjectFunderController@delete');
    Route::post('/json/activateProjectFunder/', 'Setup\ProjectFunderController@activate');

    Route::get('/json/periods', 'Setup\PeriodController@index');
    Route::get('/json/periods/executionPeriods', 'Setup\PeriodController@executionPeriods');
    Route::post('/json/createPeriod', 'Setup\PeriodController@store');
    Route::post('/json/updatePeriod', 'Setup\PeriodController@update');
    Route::get('/json/deletePeriod/{id}', 'Setup\PeriodController@delete');
    Route::post('/json/togglePeriod', 'Setup\PeriodController@togglePeriod');
    Route::get('/json/periods/get-by-activity/{activityId}', 'Setup\PeriodController@getByActivity');
    Route::get('/json/periods/getByFinancialYearId', 'Setup\PeriodController@getByFinancialYearId');
    Route::get('/api/executionYearPeriods', 'Setup\PeriodController@executionYearPeriods');

//Route::group(['middleware' => ['hasPermission:settings.manage.gfs_codes_settings, settings.manage.gfs_codes, report']], function () {
    Route::get('/json/gfsCodeCategories', 'Setup\GfsCodeCategoryController@index');
    Route::get('/json/paginatedGfsCodeCategories', 'Setup\GfsCodeCategoryController@paginated');
    Route::post('/json/createGfsCodeCategory', 'Setup\GfsCodeCategoryController@store');
    Route::post('/json/updateGfsCodeCategory', 'Setup\GfsCodeCategoryController@update');
    Route::get('/json/deleteGfsCodeCategory/{id}', 'Setup\GfsCodeCategoryController@delete');
    Route::post('/json/gfs_code_categories/upload', 'Setup\GfsCodeCategoryController@upload');
//});

//Route::group(['middleware' => ['hasPermission:settings.manage.facilities, report']], function () {
    Route::get('/json/facilities', 'Setup\FacilityController@index');
    Route::post('/json/createFacility', 'Setup\FacilityController@store');
    Route::post('/json/updateFacility', 'Setup\FacilityController@update');
    Route::get('/json/deleteFacility/{id}', 'Setup\FacilityController@delete');
    Route::post('/json/toggleFacility', 'Setup\FacilityController@toggleFacility');
    Route::get('/json/all-sections', 'Setup\FacilityController@sections');
    Route::get('/json/all-facility-types', 'Setup\FacilityController@facility_types');
    Route::get('/json/all-admin-hierarchies', 'Setup\FacilityController@admin_hierarchies');
//});

//Route::group(['middleware' => ['hasPermission:settings.manage.gfs_codes_settings, settings.manage.gfs_codes, report']], function () {
    //The routes which control gfs codes start here
    Route::get('/json/gfsCodes', 'Setup\GfsCodeController@indexExpenditure');
    Route::get('/json/paginateGfsCodes', 'Setup\GfsCodeController@paginateIndex');
    Route::post('/json/createGfsCode', 'Setup\GfsCodeController@store');
    Route::get('/json/get-GfsCode-mapping', 'Setup\GfsCodeController@ifrsGfsMapping');
    Route::post('/json/GfsCode-mapping', 'Setup\GfsCodeController@storeIfrs');
    Route::post('/json/updateGfsCode', 'Setup\GfsCodeController@update');
    Route::post('/json/deleteIfrsGfsCode/{id}', 'Setup\GfsCodeController@deletemapping');
    Route::post('//json/deleteGfsCode/{id}', 'Setup\GfsCodeController@delete');
    Route::get('/json/loadGfsCodeAccountTypes', 'Setup\GfsCodeController@loadAccountTypes');
    Route::get('/json/loadGfsCodeCategories', 'Setup\GfsCodeController@loadGfsCodeCategories');

    // Govella
    Route::get('/json/loadProcurementTypes', 'Setup\GfsCodeController@loadProcurementTypes');

    Route::post('/json/activateGfsCode', 'Setup\GfsCodeController@activateGfsCode');
    Route::post('/json/activateIsProcurement', 'Setup\GfsCodeController@activateIsProcurement');
    Route::post('/gfs-codes/upload', 'Setup\GfsCodeController@upload');
    Route::get('/files/gfs-codes/export', 'Setup\GfsCodeController@downloadExcel');
    Route::get('/json/gfsCodes/fetchAll', 'Setup\GfsCodeController@fetchAll');
    Route::get('/json/gfsCodes/onlyRevenueOnes', 'Setup\GfsCodeController@onlyRevenueOnes');
//The routes which control gfs codes end here
    //});

//The routes which control generic template start here
    Route::get('/json/allPlanChains', 'Setup\GenericTemplateController@plan_chains');
    Route::get('/json/getByPlanChainId/{plan_chain_id}', 'Setup\GenericTemplateController@getByPlanChainId');
    Route::get('/json/getParentByPlanChainId/{plan_chain_id}/{childTemplateId}', 'Setup\GenericTemplateController@getParentByPlanChainId');
    Route::post('/json/createGenericTemplate', 'Setup\GenericTemplateController@store');
    Route::post('/json/updateGenericTemplate', 'Setup\GenericTemplateController@update');
//The routes which control generic template ends here

//Route::group(['middleware' => ['hasPermission:planning.manage.objectives, report']], function () {
    //The routes which control plan chain types starts here
    Route::get('/json/paginatedPlanChainTypes', 'Setup\PlanChainTypeController@paginated');
    Route::get('/json/planChainTypes', 'Setup\PlanChainTypeController@index');
    Route::post('/json/createPlanChainType', 'Setup\PlanChainTypeController@store');
    Route::post('/json/updatePlanChainType', 'Setup\PlanChainTypeController@update');
    Route::get('/json/deletePlanChainType/{id}', 'Setup\PlanChainTypeController@delete');
    Route::post('/json/activatePlanChainType/', 'Setup\PlanChainTypeController@activate');
    Route::get('/json/loadParentPlanChain/{childId}/{financialYearId}/{versionId}', 'Setup\PlanChainController@loadParentPlanChain');
    Route::get('/json/paginatePlanChain', 'Setup\PlanChainController@paginateIndex');
//});

//Route::group(['middleware' => ['hasPermission:settings.manage.finance_settings, report']], function () {
    //The routes which control projects start here
    Route::get('/json/paginatedProjects', 'Setup\ProjectController@paginated');
    Route::get('/json/projects', 'Setup\ProjectController@index');
    Route::get('/json/projects-by-sector/{sectorId}', 'Setup\ProjectController@getBySector');
    Route::get('/json/budgetedProjects', 'Setup\ProjectController@index');
    Route::post('/json/createProject', 'Setup\ProjectController@store');
    Route::post('/json/updateProject', 'Setup\ProjectController@update');
    Route::post('/json/deleteProject/{id}', 'Setup\ProjectController@delete');
    Route::get('/json/getUsedProjects/{formId}', 'Setup\ProjectController@getUsedProjects');
    Route::get('/files/projects/export', 'Setup\ProjectController@exportToCsv');
    Route::post('/files/projects/upload', 'Setup\ProjectController@upload');
    Route::get('/json/projects/fetchAll', 'Setup\ProjectController@fetchAll');
    Route::get('/json/projects/search', 'Setup\ProjectController@search');
    Route::get('/json/projects/copy', 'Setup\ProjectController@copy');
    Route::post('/json/projects/addFundSources', 'Setup\ProjectController@addFundSources');
    Route::get('/json/projects/fundSources', 'Setup\ProjectController@fundSources');
    Route::get('/json/projects/removeFundSource', 'Setup\ProjectController@removeFundSource');
    Route::get('/json/projects/by-budget-class/{budgetClassId}/by-section/{sectionId}', 'Setup\ProjectController@getByBudgetClassAndSection');
//The routes which control projects ends here
    //});

//Route::group(['middleware' => ['hasPermission:settings.manage.finance_settings, report']], function () {
    //The routes which control fundSource here
    Route::get('/json/fundSources', 'Setup\FundSourceController@index');
    Route::get('/json/fundSources/paginated', 'Setup\FundSourceController@paginated');
    Route::post('/json/createFundSource', 'Setup\FundSourceController@store');
    Route::post('/json/addCeilingSectors', 'Setup\FundSourceController@store_sectors');
    Route::post('/json/editCeilingSectors', 'Setup\FundSourceController@edit_sectors');
    Route::post('/json/addFundSourceRevenueCodes', 'Setup\FundSourceController@store_revenue_codes');
    Route::post('/json/updateFundSource', 'Setup\FundSourceController@update');
    Route::get('/json/ceilingGfsCodes/{fund_source}', 'Setup\FundSourceController@fundSourceCeilingGfs');
    Route::get('/json/ceilingSectors/{fund_source}', 'Setup\FundSourceController@fundSourceCeilingSectors');
    Route::post('/json/deleteFundSource/{id}/{financialYearId}/{versionId}', 'Setup\FundSourceController@delete');
    Route::get('/json/loadParentFundSources/{childFundSourceCategoryId}', 'Setup\FundSourceController@loadParentFundSources');
    Route::get('/json/fundSources/budgetedByFacility/{budgetType}/{financialYearId}/{adminHiearchyId}/{sectionId}/{facilityId}', 'Setup\FundSourceController@getBudgetedByFacility');
    Route::get('/json/fund-sources/by-financial-year/{financialYearId}/by-budget-class/{budgetClassId}/by-sector/{sectionId}', 'Setup\FundSourceController@getByBudgetClassAndSector');
    Route::get('/json/fund-sources/by-financial-year/{financialYearId}/by-main-budget-class/{fundSourceId}/by-sector/{sectionId}', 'Setup\FundSourceController@getByMainBudgetClassAndSector');
    Route::get('/files/fund-sources/export', 'Setup\FundSourceController@exportToCsv');
    Route::get('/json/fundSources/searchFundSource', 'Setup\FundSourceController@searchFundSource');
//The routes which control fundSource ends here
    //});

    Route::get('/json/paginatedAccountTypes', 'Setup\AccountTypeController@paginated');

    Route::get('/json/paginatedPeriods', 'Setup\PeriodController@paginated');
    Route::get('/json/paginatedFinancialYears', 'Setup\FinancialYearController@paginateIndex');
    Route::get('/json/financial-years/by-budget-type/{budgetType}', 'Setup\FinancialYearController@getByBudgetType');
    Route::post('/json/toggleFinancialYear', 'Setup\FinancialYearController@toggleFinancialYear');
    Route::get('/json/getByPlanChainId/{plan_chain_id}', 'Setup\GenericTemplateController@getByPlanChainId');

    Route::get('/json/adminHierarchiesByLevel/{levelId}', 'Setup\AdminHierarchyController@getByLevel');

    Route::get('/json/targetTypes', 'Setup\TargetTypeController@index');

//Route::group(['middleware' => ['hasPermission:settings.manage.finance_settings, report']], function () {
    Route::get('/json/referenceDocumentTypes', 'Setup\ReferenceDocumentTypeController@index');
    Route::get('/json/unusedReferenceDocumentTypes/{isNationalGuideline}', 'Setup\ReferenceDocumentTypeController@unUsed');
    Route::get('/json/paginateReferenceDocumentTypes', 'Setup\ReferenceDocumentTypeController@paginateIndex');
    Route::post('/json/createReferenceDocumentType', 'Setup\ReferenceDocumentTypeController@store');
    Route::post('/json/updateReferenceDocumentType/{id}', 'Setup\ReferenceDocumentTypeController@update');
    Route::post('/json/deleteReferenceDocumentType/{id}', 'Setup\ReferenceDocumentTypeController@delete');
//});

    Route::get('/json/allReferenceTypes', 'Setup\ReferenceTypeController@index');

// PE_ITEM_CATEGORY
    Route::get('/json/peItemCategories', 'Setup\PeItemCategoryController@index');
    Route::get('/json/paginatedPEItemCategories', 'Setup\PeItemCategoryController@paginated');
    Route::post('/json/createPeItemCategory', 'Setup\PeItemCategoryController@store');
    Route::post('/json/updatePeItemCategory', 'Setup\PeItemCategoryController@update');
    Route::get('/json/deletePeItemCategory/{id}', 'Setup\PeItemCategoryController@delete');

//budget class template
    Route::post('/json/createBudgetTemplate', 'Setup\BudgetClassTemplateController@storeTemplate');
    Route::get('/json/loadBudgetTemplate/{template_type}/{budget_class_id}', 'Setup\BudgetClassTemplateController@loadTemplate');
    Route::get('/json/allBudgetTemplate/{budget_class_id}', 'Setup\BudgetClassTemplateController@index');
    Route::post('/json/deleteBudgetTemplate/{budgetTemplateId}', 'Setup\BudgetClassTemplateController@deleteTemplate');

//Target Types
    Route::get('/json/paginatedTargetTypes', 'Setup\TargetTypeController@paginated');
    Route::post('/json/createTargetType', 'Setup\TargetTypeController@store');
    Route::post('/json/updateTargetType', 'Setup\TargetTypeController@update');
    Route::get('/json/deleteTargetType/{id}', 'Setup\TargetTypeController@delete');
    Route::post('/json/toggleTargetType', 'Setup\TargetTypeController@toggleTargetType');

//ADMIN HIERARCHY PROJECTS
    Route::get('/json/paginatedAdminHierarchyProjects', 'Setup\AdminHierarchyProjectController@paginated');
    Route::post('/json/createAdminHierarchyProject', 'Setup\AdminHierarchyProjectController@store');
    Route::post('/json/updateAdminHierarchyProject', 'Setup\AdminHierarchyProjectController@update');
    Route::get('/json/deleteAdminHierarchyProject/{id}', 'Setup\AdminHierarchyProjectController@delete');

//SECTOR PROJECTS
    Route::get('/json/paginatedSectorProjects', 'Setup\SectorProjectController@paginated');
    Route::post('/json/createSectorProject', 'Setup\SectorProjectController@store');
    Route::post('/json/updateSectorProject', 'Setup\SectorProjectController@update');
    Route::get('/json/deleteSectorProject/{id}', 'Setup\SectorProjectController@delete');

    Route::get('/json/allAdminHierarchyProjects/{id}', 'Setup\AdminHierarchyController@allAdminHierarchyProjects');
    Route::get('/json/delete_AdminHierarchyProject/{admin_hierarchy_project_id}/{admin_hierarchy_id}', 'Setup\AdminHierarchyController@delete_admin_hierarchy_project');
    Route::post('/json/addAdminHierarchyProject', 'Setup\AdminHierarchyController@addProject');

    Route::get('/json/allFinancialYearPeriods/{id}', 'Setup\FinancialYearController@allFinancialYearPeriods');
    Route::get('/json/deleteFinancialYearPeriod/{period_id}/{financial_year_id}', 'Setup\FinancialYearController@delete_financial_year_period');
    Route::post('/json/addFinancialYearPeriod', 'Setup\FinancialYearController@addFinancialYearPeriod');

//FINANCIAL YEAR TRASH IMPLEMENTATION
    Route::get('/json/financial_years/trashed', 'Setup\FinancialYearController@trashed');
    Route::get('/json/financial_year/restore/{id}', 'Setup\FinancialYearController@restore');
    Route::get('/json/financial_year/permanentDelete/{id}', 'Setup\FinancialYearController@permanentDelete');
    Route::get('/json/financial_year/emptyTrash', 'Setup\FinancialYearController@emptyTrash');

// ACCOUNT TYPES TRASH IMPLEMENTATION
    Route::get('/json/account_types/trashed', 'Setup\AccountTypeController@trashed');
    Route::get('/json/account_type/restore/{id}', 'Setup\AccountTypeController@restore');
    Route::get('/json/account_type/permanentDelete/{id}', 'Setup\AccountTypeController@permanentDelete');
    Route::get('/json/account_type/emptyTrash', 'Setup\AccountTypeController@emptyTrash');

// PERIODS TRASH IMPLEMENTATION
    Route::get('/json/periods/trashed', 'Setup\PeriodController@trashed');
    Route::get('/json/period/restore/{id}', 'Setup\PeriodController@restore');
    Route::get('/json/period/permanentDelete/{id}', 'Setup\PeriodController@permanentDelete');
    Route::get('/json/period/emptyTrash', 'Setup\PeriodController@emptyTrash');

//FACILITY TYPE - TRASH IMPLEMENTATION
    Route::get('/json/facility_types/trashed', 'Setup\FacilityTypeController@trashed');
    Route::get('/json/facility_type/restore/{id}', 'Setup\FacilityTypeController@restore');
    Route::get('/json/facility_type/permanentDelete/{id}', 'Setup\FacilityTypeController@permanentDelete');
    Route::get('/json/facility_type/emptyTrash', 'Setup\FacilityTypeController@emptyTrash');

//PRIORITY AREAS
    Route::get('/json/priority_areas/paginated', 'Setup\PriorityAreaController@paginated');
    Route::get('/json/priority_areas/all', 'Setup\PriorityAreaController@index');
    Route::post('/json/priority_area/create', 'Setup\PriorityAreaController@store');
    Route::post('/json/priority_area/update', 'Setup\PriorityAreaController@update');
    Route::post('/json/priority_area/toggle', 'Setup\PriorityAreaController@toggleActive');
    Route::get('/json/priority_area/delete/{id}', 'Setup\PriorityAreaController@delete');
    Route::get('/json/priority_areas/sectors', 'Setup\PriorityAreaController@sectors');
    Route::get('/json/priority_areas/removeSector', 'Setup\PriorityAreaController@removeSector');
    Route::post('/json/priority_areas/addSector', 'Setup\PriorityAreaController@addSector');
    Route::get('/json/priority_areas/copyPriorityAreas', 'Setup\PriorityAreaController@copyPriorityAreas');
    Route::get('/json/priority_areas/removePriorityArea', 'Setup\PriorityAreaController@delete');
    Route::get('/json/priority_areas-with-intervations/by-link-level/{linkLeve}/by-plan-chain/{planChainId}/{financialYearId}/by-admin-hierarchy/{adminHierarchyId}/by-section/{sectionId}', 'Setup\PriorityAreaController@priorityWithInterventionAndProblems');
    Route::get('/json/priority-areas/get-current-version', 'Setup\PriorityAreaController@getForCurrentVersion');

//INTERVENTION CATEGORY
    Route::get('/json/intervention_categories/all', 'Setup\InterventionCategoryController@index');
    Route::get('/json/intervention_categories/wholeTree', 'Setup\InterventionCategoryController@wholeTree');
    Route::get('/json/intervention_categories/entireTree/{id}', 'Setup\InterventionCategoryController@entireTree');
    Route::post('/json/intervention_category/create', 'Setup\InterventionCategoryController@store');
    Route::post('/json/intervention_category/update', 'Setup\InterventionCategoryController@update');
    Route::get('/json/intervention_category/delete/{id}', 'Setup\InterventionCategoryController@delete');
    Route::get('/json/intervention_categories/trashed', 'Setup\InterventionCategoryController@trashed');
    Route::get('/json/intervention_category/restore/{id}', 'Setup\InterventionCategoryController@restore');
    Route::get('/json/intervention_category/permanentDelete/{id}', 'Setup\InterventionCategoryController@permanentDelete');
    Route::get('/json/intervention_categories/emptyTrash', 'Setup\InterventionCategoryController@emptyTrash');
    Route::get('/json/intervention-categories/parents', 'Setup\InterventionCategoryController@parents');
    Route::get('/json/intervention-categories/children', 'Setup\InterventionCategoryController@children');

//INTERVENTIONS
    Route::get('/json/interventions/paginated', 'Setup\InterventionController@paginated');
    Route::get('/json/interventions/all', 'Setup\InterventionController@index');
    Route::post('/json/intervention/create', 'Setup\InterventionController@store');
    Route::post('/json/intervention/update', 'Setup\InterventionController@update');
    Route::post('/json/intervention/toggleActive', 'Setup\InterventionController@toggleActive');
    Route::post('/json/intervention/togglePrimary', 'Setup\InterventionController@togglePrimary');
    Route::get('/json/intervention/delete/{id}', 'Setup\InterventionController@delete');
    Route::get('/json/interventions/trashed', 'Setup\InterventionController@trashed');
    Route::get('/json/intervention/restore/{id}', 'Setup\InterventionController@restore');
    Route::get('/json/intervention/permanentDelete/{id}', 'Setup\InterventionController@permanentDelete');
    Route::get('/json/interventions/emptyTrash', 'Setup\InterventionController@emptyTrash');
    Route::get('/json/interventions/generateCodes', 'Setup\InterventionController@generateCodes');
    Route::get('/json/interventions/filterInterventionByCategory', 'Setup\InterventionCategoryController@filterInterventionByCategory');

//Route::group(['middleware' => ['hasPermission:settings.manage.finance_settings, report']], function () {
    //cas plans
    Route::get('/json/casPlans', 'Setup\CasPlanController@index');
    Route::post('/json/createCasPlan', 'Setup\CasPlanController@store');
    Route::post('/json/updateCasPlan', 'Setup\CasPlanController@update');
    Route::get('/json/deleteCasPlan/{casPlanId}', 'Setup\CasPlanController@delete');
    Route::get('/json/cas_plan_periods', 'Setup\CasPlanController@getPlanPeriods');

//cas plans end

//CAS Plan Tables
    Route::get('/json/casPlanTable/{cas_plan_id}/{page}/{perPage}', 'Setup\CasPlanTableController@paginateIndex');
    Route::get('/json/casPlanTablesSingle', 'Setup\CasPlanTableController@index');
    Route::get('/json/casPlanTableSearchByName', 'Setup\CasPlanTableController@searchByName');
    Route::get('/json/casPlanTableIndex', 'Setup\CasPlanTableController@indexPaginated');
    Route::post('/json/createCasPlanTable', 'Setup\CasPlanTableController@store');
    Route::post('/json/updateCasPlanTable', 'Setup\CasPlanTableController@update');
    Route::get('/json/deleteCasPlanTable/{id}/{cas_plan_id}', 'Setup\CasPlanTableController@delete');
    Route::get('/json/casPlanTableByContent/{cas_plan_content_id}', 'Setup\CasPlanTableController@getPlanTables');
    Route::get('/json/casPlanTableIsEditable/{casPlanId}', 'Setup\CasPlanController@planIsSubmitted');
    Route::get('/json/getAllCasPlanTables', 'Setup\CasPlanTableController@getAllCasPlanTables');

//End CAS Plan Tables

//CAS Plan Contents
    Route::get('/json/casPlanContents/{cas_plan_id}', 'Setup\CasPlanContentController@index');
    Route::post('/json/createCasPlanContent', 'Setup\CasPlanContentController@store');
    Route::post('/json/updateCasPlanContent', 'Setup\CasPlanContentController@update');
    Route::get('/json/deleteCasPlanContent/{id}', 'Setup\CasPlanContentController@delete');
    Route::get('/json/getCasContentById/{id}', 'Setup\CasPlanContentController@getCasContentById');
//End CAS Plan Contents

//The routes for the cas group types start here
    Route::get('/json/GetCasPlanTableGroupTypes', 'Setup\CasGroupTypeController@Index');
    Route::get('/json/PaginatedCasPlanTableGroupTypes', 'Setup\CasGroupTypeController@paginateIndex');
    Route::post('/json/CreateCasGroupType', 'Setup\CasGroupTypeController@store');
    Route::post('/json/UpdateCasGroupType', 'Setup\CasGroupTypeController@update');
    Route::get('/json/DeleteCasGroupType/{id}', 'Setup\CasGroupTypeController@delete');

//Cas group columns start here
    Route::get('/json/GetCasGroupColumnByType/{group_type_id}', 'Setup\CasGroupColumnController@getCasGroupColumnById');
    Route::get('/json/PaginateCasGroupColumn', 'Setup\CasGroupColumnController@paginateIndex');
    Route::post('/json/CreateCasGroupColumn', 'Setup\CasGroupColumnController@store');
    Route::post('/json/UpdateCasGroupColumn', 'Setup\CasGroupColumnController@update');
    Route::get('/json/DeleteCasGroupColumn/{id}', 'Setup\CasGroupColumnController@delete');
//Cas group columns end here

//cas plan table select options
    Route::get('/json/casPlanTableSelectOptions', 'Setup\casPlanTableSelectOptionController@index');
    Route::post('/json/casPlanTableSelectOptions', 'Setup\casPlanTableSelectOptionController@store');
    Route::post('/json/UpdateCasPlanTableSelectOptions', 'Setup\casPlanTableSelectOptionController@update');
    Route::delete('/json/casPlanTableSelectOptions/{id}', 'Setup\casPlanTableSelectOptionController@delete');
//end

//The routes which belong to the baseline statistical data start here
    Route::get('/json/getBaselineStatistics', 'Setup\BaselineStatisticController@index');
    Route::get('/json/getBaselineStatisticsSingle', 'Setup\BaselineStatisticController@indexSingle');
    Route::post('/json/createBaselineStatistics', 'Setup\BaselineStatisticController@store');
    Route::post('/json/editBaselineStatistics', 'Setup\BaselineStatisticController@update');
    Route::get('/json/deleteBaselineStatistics/{baselineStatisticId}', 'Setup\BaselineStatisticController@delete');
//The routes which belong to the baseline statistical data end here

    Route::get('/api/accountReturns/paged', 'Setup\AccountReturnController@paginated');
    Route::get('/api/accountReturns', 'Setup\AccountReturnController@index');
    Route::post('/api/accountReturns', 'Setup\AccountReturnController@store');
    Route::put('/api/accountReturns/{id}', 'Setup\AccountReturnController@update');
    Route::delete('/api/accountReturns/{id}', 'Setup\AccountReturnController@delete');
    Route::get('/api/accountReturns/gfsCodes', 'Setup\AccountReturnController@gfsCodes');
    Route::post('/api/accountReturns/addGfsCodes', 'Setup\AccountReturnController@addGfsCodes');
    Route::get('/api/accountReturns/removeGfsCode', 'Setup\AccountReturnController@removeGfsCode');

//});

//SECTOR PROBLEMS
    Route::get('/json/sector_problems/paginated', 'Setup\SectorProblemController@paginated');
    Route::get('/json/sector_problems/all', 'Setup\SectorProblemController@index');
    Route::post('/json/sector_problem/create', 'Setup\SectorProblemController@store');
    Route::post('/json/sector_problem/update', 'Setup\SectorProblemController@update');
    Route::post('/json/sector_problem/toggleActive', 'Setup\SectorProblemController@toggleActive');
    Route::get('/json/sector_problem/delete/{id}', 'Setup\SectorProblemController@delete');
    Route::get('/json/sector_problems/trashed', 'Setup\SectorProblemController@trashed');
    Route::get('/json/sector_problem/restore/{id}', 'Setup\SectorProblemController@restore');
    Route::get('/json/sector_problem/permanentDelete/{id}', 'Setup\SectorProblemController@permanentDelete');
    Route::get('/json/sector_problems/emptyTrash', 'Setup\SectorProblemController@emptyTrash');

//PROJECTION SERVICES
    Route::get('/json/fund_source/gfs_codes/{fund_source_id}', 'Setup\FundSourceController@gfs_codes');
    Route::get('/json/fund_source/gfs_codes/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}/{fundSourceId}', 'Setup\FundSourceController@gfs_codes');
    Route::get('/json/fund_source/projections/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}/{fundSourceId}', 'Setup\FundSourceController@getProjection');
    Route::delete('/json/fund_source/projections/{id}', 'Setup\FundSourceController@deleteProjection');
    Route::get('/json/fund_source/canProjectGfsCodes/{id}', 'Setup\FundSourceController@canProjectGfsCodes');
    Route::get('/json/financial_year/periods', 'Setup\FinancialYearController@periods');
    Route::get('/json/currentFinancialYear/{financialYearId}/periods', 'Setup\FinancialYearController@currentPeriods');
    Route::get('/json/currentFinancialYear/planningPeriods', 'Setup\FinancialYearController@planningPeriods');
    Route::get('/json/period-by-financial-year/{financialYearId}', 'Setup\FinancialYearController@periodsByYear');
    Route::get('/json/fund_sources/can_project', 'Setup\FundSourceController@can_project');
    Route::get('/json/fund_sources/for_report', 'Setup\FundSourceController@for_report');
    Route::post('/json/fund_source/assignGfsCodes', 'Setup\FundSourceController@assignGfsCodes');
    Route::get('/json/section/gfs_codes', 'Setup\GfsCodeSectionController@gfs_codes');

//ADMIN_HIERARCHY_GFS_CODES
    Route::get('/json/admin_hierarchy_gfs_codes/paginated', 'Setup\AdminHierarchyGfsCodeController@paginated');
    Route::get('/json/admin_hierarchy_gfs_codes/all', 'Setup\AdminHierarchyGfsCodeController@index');
    Route::get('/json/gfs_codes/all', 'Setup\GfsCodeController@index');
    Route::post('/json/admin_hierarchy_gfs_code/create', 'Setup\AdminHierarchyGfsCodeController@store');
    Route::get('/json/admin_hierarchy_gfs_code/delete/{id}', 'Setup\AdminHierarchyGfsCodeController@delete');

//SECTION GFS CODES
    Route::get('/json/section_gfs_codes/paginated', 'Setup\GfsCodeSectionController@paginated');
    Route::get('/json/section_gfs_codes/all', 'Setup\GfsCodeSectionController@index');
    Route::post('/json/section_gfs_code/create', 'Setup\GfsCodeSectionController@store');
    Route::get('/json/section_gfs_code/delete/{id}', 'Setup\GfsCodeSectionController@delete');
    Route::get('/json/expenditureGfsCodes', 'Setup\GfsCodeController@expenditureGfsCodes');
    Route::post('/json/expenditureGfsCodes/search', 'Setup\GfsCodeController@searchExpenditureGfsCodes');
    Route::get('/json/revenueGfsCodes', 'Setup\GfsCodeController@revenueGfsCodes');
    Route::get('/json/revenueGfsCodes/{fundSourceVersionId}/{financialYearId}', 'Setup\GfsCodeController@revenueByFundSourceVersion');
    Route::get('/json/expenditureGfsCodes/searchGfs', 'Setup\GfsCodeController@searchExpdGfsCodes');

//CAS Plan Table Columns
    Route::get('/json/casPlanTableColumns', 'Setup\CasPlanTableColumnController@index');
    Route::get('/json/paginateCasPlanTableColumns', 'Setup\CasPlanTableColumnController@paginateIndex');
    Route::post('/json/createCasPlanTableColumn', 'Setup\CasPlanTableColumnController@store');
    Route::post('/json/casPlanTableColumnFormula', 'Setup\CasPlanTableColumnController@updateFormula');
    Route::post('/json/updateCasPlanTableColumn', 'Setup\CasPlanTableColumnController@update');
    Route::get('/json/deleteCasPlanTableColumn/{id}', 'Setup\CasPlanTableColumnController@delete');
    Route::get('/json/casPlanTableColumn/{table_id}', 'Setup\CasPlanTableColumnController@getCasPlanTableColumnsByTableId');
    Route::get('/json/paginatedCasPlanTableColumn', 'Setup\CasPlanTableColumnController@paginateIndex');
    Route::get('/json/CasPlanTableColumnsByTable/{cas_plan_table_id}', 'Setup\CasPlanTableColumnController@getTableColumns');

//End CAS Plan Table Columns

//CAS Plan table items
    Route::get('/json/casPlanTableItems', 'Setup\CasPlanTableItemController@paginateIndex');
    Route::post('/json/CreateCasPlanTableItem', 'Setup\CasPlanTableItemController@store');
    Route::post('/json/UpdateCasPlanTableItem', 'Setup\CasPlanTableItemController@update');
    Route::get('/json/DeleteCasPlanTableItem/{id}', 'Setup\CasPlanTableItemController@delete');
    Route::get('/json/CasPlanTableItemByTable/{cas_plan_table_id}', 'Setup\CasPlanTableItemController@getTableRows');

    Route::get('/json/gfs_codes/search', 'Setup\GfsCodeController@search');

//LGA LEVELS
    Route::get('/json/lga_levels/paginated', 'Setup\LgaLevelController@paginated');
    Route::post('/json/lga_level/create', 'Setup\LgaLevelController@store');
    Route::post('/json/lga_level/update', 'Setup\LgaLevelController@update');
    Route::get('/json/lga_level/delete/{id}', 'Setup\LgaLevelController@delete');
    Route::get('/json/lga_levels/trashed', 'Setup\LgaLevelController@trashed');
    Route::get('/json/lga_level/restore/{id}', 'Setup\LgaLevelController@restore');
    Route::get('/json/lga_level/permanentDelete/{id}', 'Setup\LgaLevelController@permanentDelete');
    Route::get('/json/lga_levels/emptyTrash', 'Setup\LgaLevelController@emptyTrash');

//LINK LEVELS
    Route::get('/json/link_levels/all', 'Setup\LinkLevelController@index');
    Route::get('/json/link_levels/paginated', 'Setup\LinkLevelController@paginated');
    Route::post('/json/link_level/create', 'Setup\LinkLevelController@store');
    Route::post('/json/link_level/update', 'Setup\LinkLevelController@update');
    Route::get('/json/link_level/delete/{id}', 'Setup\LinkLevelController@delete');
    Route::get('/json/link_levels/truncateData', 'Setup\LinkLevelController@truncateData');
    Route::get('/json/link_levels/trashed', 'Setup\LinkLevelController@trashed');
    Route::get('/json/link_level/restore/{id}', 'Setup\LinkLevelController@restore');
    Route::get('/json/link_level/permanentDelete/{id}', 'Setup\LinkLevelController@permanentDelete');
    Route::get('/json/link_levels/emptyTrash', 'Setup\LinkLevelController@emptyTrash');

    //Route::group(['middleware' => ['hasPermission:settings.manage.budget_settings']], function () {
    //EXPENDITURE CENTRE GROUPS
    Route::get('/json/expenditure_centre_groups/paginated', 'Setup\ExpenditureCentreGroupController@paginated');
    Route::get('/json/expenditure_centre_groups/all', 'Setup\ExpenditureCentreGroupController@index');
    Route::post('/json/expenditure_centre_group/create', 'Setup\ExpenditureCentreGroupController@store');
    Route::post('/json/expenditure_centre_group/update', 'Setup\ExpenditureCentreGroupController@update');
    Route::get('/json/expenditure_centre_group/delete/{id}', 'Setup\ExpenditureCentreGroupController@delete');
    Route::get('/json/expenditure_centre_groups/trashed', 'Setup\ExpenditureCentreGroupController@trashed');
    Route::get('/json/expenditure_centre_group/restore/{id}', 'Setup\ExpenditureCentreGroupController@restore');
    Route::get('/json/expenditure_centre_group/permanentDelete/{id}', 'Setup\ExpenditureCentreGroupController@permanentDelete');
    Route::get('/json/expenditure_centre_groups/emptyTrash', 'Setup\ExpenditureCentreGroupController@emptyTrash');
    Route::post('/json/expenditure_centre_groups/toggleActive', 'Setup\ExpenditureCentreGroupController@toggleActive');

    //EXPENDITURE CENTRE
    Route::get('/json/expenditure_centres/paginated', 'Setup\ExpenditureCentreController@paginated');
    Route::get('/json/expenditure_centres/all', 'Setup\ExpenditureCentreController@index');
    Route::post('/json/expenditure_centre/create', 'Setup\ExpenditureCentreController@store');
    Route::post('/json/expenditure_centre/update', 'Setup\ExpenditureCentreController@update');
    Route::get('/json/expenditure_centre/delete/{id}', 'Setup\ExpenditureCentreController@delete');
    Route::get('/json/expenditure_centres/trashed', 'Setup\ExpenditureCentreController@trashed');
    Route::get('/json/expenditure_centre/restore/{id}', 'Setup\ExpenditureCentreController@restore');
    Route::get('/json/expenditure_centre/permanentDelete/{id}', 'Setup\ExpenditureCentreController@permanentDelete');
    Route::get('/json/expenditure_centres/emptyTrash', 'Setup\ExpenditureCentreController@emptyTrash');
    //});
    //Budget Distribution Condition Main Sector Groups
    Route::get('/json/bdc_main_groups/paginated', 'Setup\BdcMainGroupController@paginated');
    Route::get('/json/bdc_main_groups/all', 'Setup\BdcMainGroupController@index');
    Route::post('/json/bdc_main_group/create', 'Setup\BdcMainGroupController@store');
    Route::post('/json/bdc_main_group/update', 'Setup\BdcMainGroupController@update');
    Route::get('/json/bdc_main_group/delete/{id}', 'Setup\BdcMainGroupController@delete');
    Route::get('/json/bdc_main_groups/trashed', 'Setup\BdcMainGroupController@trashed');
    Route::get('/json/bdc_main_group/restore/{id}', 'Setup\BdcMainGroupController@restore');
    Route::get('/json/bdc_main_group/permanentDelete/{id}', 'Setup\BdcMainGroupController@permanentDelete');
    Route::get('/json/bdc_main_groups/emptyTrash', 'Setup\BdcMainGroupController@emptyTrash');

//Budget Distribution Condition Sector Groups
    Route::get('/json/bdc_groups/paginated', 'Setup\BdcGroupController@paginated');
    Route::get('/json/bdc_groups/all', 'Setup\BdcGroupController@index');
    Route::post('/json/bdc_group/create', 'Setup\BdcGroupController@store');
    Route::post('/json/bdc_group/update', 'Setup\BdcGroupController@update');
    Route::get('/json/bdc_group/delete/{id}', 'Setup\BdcGroupController@delete');
    Route::get('/json/bdc_groups/trashed', 'Setup\BdcGroupController@trashed');
    Route::get('/json/bdc_group/restore/{id}', 'Setup\BdcGroupController@restore');
    Route::get('/json/bdc_group/permanentDelete/{id}', 'Setup\BdcGroupController@permanentDelete');
    Route::get('/json/bdc_groups/emptyTrash', 'Setup\BdcGroupController@emptyTrash');

//EXPENDITURE CENTRE GFS CODES
    Route::get('/json/expenditure_centre_gfs_codes/paginated', 'Setup\ExpenditureCentreGfsCodeController@paginated');
    Route::get('/json/expenditure_centre_gfs_codes/all', 'Setup\ExpenditureCentreGfsCodeController@index');
    Route::post('/json/expenditure_centre_gfs_code/create', 'Setup\ExpenditureCentreGfsCodeController@store');
    Route::post('/json/expenditure_centre_gfs_code/update', 'Setup\ExpenditureCentreGfsCodeController@update');
    Route::get('/json/expenditure_centre_gfs_code/delete/{id}', 'Setup\ExpenditureCentreGfsCodeController@delete');
    Route::get('/json/expenditure_centre_gfs_codes/trashed', 'Setup\ExpenditureCentreGfsCodeController@trashed');
    Route::get('/json/expenditure_centre_gfs_code/restore/{id}', 'Setup\ExpenditureCentreGfsCodeController@restore');
    Route::get('/json/expenditure_centre_gfs_code/permanentDelete/{id}', 'Setup\ExpenditureCentreGfsCodeController@permanentDelete');
    Route::get('/json/expenditure_centre_gfs_codes/emptyTrash', 'Setup\ExpenditureCentreGfsCodeController@emptyTrash');

//SECTOR EXPENDITURE CENTRE SUB CENTRES
    Route::get('/json/sectorExpenditureSubCentres/paginated', 'Setup\SectorExpenditureSubCentreController@paginated');
    Route::get('/json/sectorExpenditureSubCentres/all', 'Setup\SectorExpenditureSubCentreController@index');
    Route::post('/json/sectorExpenditureSubCentre/create', 'Setup\SectorExpenditureSubCentreController@store');
    Route::post('/json/sectorExpenditureSubCentre/update', 'Setup\SectorExpenditureSubCentreController@update');
    Route::get('/json/sectorExpenditureSubCentre/delete/{id}', 'Setup\SectorExpenditureSubCentreController@delete');
    Route::get('/json/sectorExpenditureSubCentres/trashed', 'Setup\SectorExpenditureSubCentreController@trashed');
    Route::get('/json/sectorExpenditureSubCentre/restore/{id}', 'Setup\SectorExpenditureSubCentreController@restore');
    Route::get('/json/sectorExpenditureSubCentre/permanentDelete/{id}', 'Setup\SectorExpenditureSubCentreController@permanentDelete');
    Route::get('/json/sectorExpenditureSubCentres/emptyTrash', 'Setup\SectorExpenditureSubCentreController@emptyTrash');

//Route::group(['middleware' => ['hasPermission:settings.manage.finance_settings, report']], function () {
    Route::get('/json/subBudgetClassesService', 'Setup\BudgetClassController@subBudgetClasses');
    Route::get('/json/budgetClasses/children', 'Setup\BudgetClassController@childrenBudgetClasses');
    Route::get('/json/fundSourceBudgetClasses/{fund_source_id}', 'Setup\FundSourceController@fundSourceBudgetClasses');
    Route::get('/json/fundSourceRevenueCodes/{fund_source_id}', 'Setup\FundSourceController@fundSourceRevenueCodes');
//});

//Route::group(['middleware' => ['hasPermission:settings.manage.finance_settings, report']], function () {
    Route::get('/json/fundSourceCeiling/{fund_source_id}', 'Budgeting\CeilingController@fundSourceCeilings');
//});
    Route::get('/json/move-ceiling/getCeiling/{financialYearId}/{budgetType}/{adminHierarchyId}/{sectorId}', 'Budgeting\CeilingController@getCeiling');
    Route::get('/json/move-ceiling/getAdminCeiling/{financialYearId}/{budgetType}/{adminHierarchyId}/{sectorId}/{ceilingId}', 'Budgeting\AdminHierarchyCeilingController@getAdminCeiling');
    Route::get('/json/move-ceiling/moveAdminCeiling/{financialYearId}/{budgetType}/{adminHierarchyId}/{sectorId}/{ceilingId}/{newCeilingId}', 'Budgeting\AdminHierarchyCeilingController@moveAdminCeiling');

//GFS CODES - REDONE
    Route::get('/json/gfs_codes/all', 'Setup\GfsCodeController@index');
    Route::post('/json/gfs_code/create', 'Setup\GfsCodeController@store');
    Route::post('/json/gfs_code/update', 'Setup\GfsCodeController@update');
    Route::post('/json/ifrsgfs_code/update', 'Setup\GfsCodeController@updateifrs');
    Route::get('/json/gfs_code/delete/{id}', 'Setup\GfsCodeController@delete');
    Route::get('/json/gfs_code/single/{id}', 'Setup\GfsCodeController@single');

//Route::group(['middleware' => ['hasPermission:settings.manage.gfs_codes_settings, settings.manage.gfs_codes, report']], function () {
    //GFS CODE SUB CATEGORY
    Route::get('/json/gfs_code_sub_categories/paginated', 'Setup\GfsCodeSubCategoryController@paginated');
    Route::get('/json/gfs_code_sub_categories/all', 'Setup\GfsCodeSubCategoryController@index');
    Route::post('/json/gfs_code_sub_category/create', 'Setup\GfsCodeSubCategoryController@store');
    Route::post('/json/gfs_code_sub_category/update', 'Setup\GfsCodeSubCategoryController@update');
    Route::get('/json/gfs_code_sub_category/delete/{id}', 'Setup\GfsCodeSubCategoryController@delete');
    Route::get('/json/gfs_code_sub_categories/filter/{id}', 'Setup\GfsCodeSubCategoryController@filter');
    Route::post('/json/gfs_code_sub_categories/upload', 'Setup\GfsCodeSubCategoryController@upload');
    Route::post('/json/gfs_code_sub_categories/toggle', 'Setup\GfsCodeSubCategoryController@toggleProcurable');
//});

//cas plan table item values
    Route::post('/json/PostCasPlanTableItemValue', 'Setup\CasPlanTableItemValueController@store');
    Route::post('/json/PostCasPlanTableItemConstantValue', 'Setup\CasPlanTableItemValueController@updateConstant');
    Route::get('/json/LoadCasPlanTableItemValue', 'Setup\CasPlanTableItemValueController@index');
    Route::post('/comprehensive_plans/upload', 'Setup\CasPlanTableItemValueController@uploadFile');

    Route::get('/json/budget_classes/parents', 'Setup\BudgetClassController@parents');
    Route::get('/json/budget_classes/subs/{id}', 'Setup\BudgetClassController@subs');

//SECTOR PROJECTS
    Route::get('/json/sector_projects/sectors/{id}', 'Setup\SectorProjectController@sectors');
    Route::get('/json/sector_project/delete/{sector_project_id}/{project_id}', 'Setup\SectorProjectController@delete_sector');
    Route::post('/json/sector_projects/create', 'Setup\SectorProjectController@add_sector');

//EPICOR
    Route::get('/json/systems/all', 'Setup\SystemController@index');

    Route::get('/json/sectorExpenditureSubCentreGroups/paginated', 'Setup\BdcSectorExpenditureSubCentreGroupController@paginated');
    Route::get('/json/sectorExpenditureSubCentreGroups/all', 'Setup\BdcSectorExpenditureSubCentreGroupController@index');
    Route::post('/json/sectorExpenditureSubCentreGroup/create', 'Setup\BdcSectorExpenditureSubCentreGroupController@store');
    Route::post('/json/sectorExpenditureSubCentreGroup/update', 'Setup\BdcSectorExpenditureSubCentreGroupController@update');
    Route::get('/json/sectorExpenditureSubCentreGroup/delete/{id}', 'Setup\BdcSectorExpenditureSubCentreGroupController@delete');
    Route::get('/json/sectorExpenditureSubCentreGroups/trashed', 'Setup\BdcSectorExpenditureSubCentreGroupController@trashed');
    Route::get('/json/sectorExpenditureSubCentreGroup/restore/{id}', 'Setup\BdcSectorExpenditureSubCentreGroupController@restore');
    Route::get('/json/sectorExpenditureSubCentreGroup/permanentDelete/{id}', 'Setup\BdcSectorExpenditureSubCentreGroupController@permanentDelete');
    Route::get('/json/sectorExpenditureSubCentreGroups/emptyTrash', 'Setup\BdcSectorExpenditureSubCentreGroupController@emptyTrash');

//BOD_LIST

    Route::get('/json/bodLists/paginated', 'Setup\BodListController@paginated');
    Route::get('/json/bodLists/all', 'Setup\BodListController@index');
    Route::post('/json/bodLists/create', 'Setup\BodListController@store');
    Route::post('/json/bodLists/update', 'Setup\BodListController@update');
    Route::get('/json/bodLists/delete/{id}', 'Setup\BodListController@delete');
    Route::get('/json/bodLists/trashed', 'Setup\BodListController@trashed');
    Route::get('/json/bodLists/restore/{id}', 'Setup\BodListController@restore');
    Route::get('/json/bodLists/permanentDelete/{id}', 'Setup\BodListController@permanentDelete');
    Route::get('/json/bodLists/emptyTrash', 'Setup\BodListController@emptyTrash');
    Route::get('/json/bodLists/xxx', 'Setup\BodListController@xxx');

//BOD_VERSION

    Route::get('/json/bodVersions/paginated', 'Setup\BodVersionController@paginated');
    Route::get('/json/bodVersions/all', 'Setup\BodVersionController@index');
    Route::post('/json/bodVersions/create', 'Setup\BodVersionController@store');
    Route::post('/json/bodVersions/update', 'Setup\BodVersionController@update');
    Route::get('/json/bodVersions/delete/{id}', 'Setup\BodVersionController@delete');
    Route::get('/json/bodVersions/trashed', 'Setup\BodVersionController@trashed');
    Route::get('/json/bodVersions/restore/{id}', 'Setup\BodVersionController@restore');
    Route::get('/json/bodVersions/permanentDelete/{id}', 'Setup\BodVersionController@permanentDelete');
    Route::get('/json/bodVersions/emptyTrash', 'Setup\BodVersionController@emptyTrash');

    Route::get('/json/cas_plans/all', 'Setup\CasPlanController@index');
//Route::group(['middleware' => ['hasPermission:settings.manage.finance_settings, report']], function () {
    Route::get('/json/fund_source_categories/all', 'Setup\FundSourceCategoryController@index');
//});

//Route::group(['middleware' => ['hasPermission:settings.manage.finance_settings, report']], function () {
    //FUND TYPES
    Route::get('/json/fundTypes/paginated', 'Setup\FundTypeController@paginated');
    Route::get('/json/fundTypes/all', 'Setup\FundTypeController@index');
    Route::post('/json/fundType/create', 'Setup\FundTypeController@store');
    Route::post('/json/fundType/update', 'Setup\FundTypeController@update');
    Route::get('/json/fundType/delete/{id}', 'Setup\FundTypeController@delete');
    Route::get('/json/fundTypes/trashed', 'Setup\FundTypeController@trashed');
    Route::get('/json/fundType/restore/{id}', 'Setup\FundTypeController@restore');
    Route::get('/json/fundType/permanentDelete/{id}', 'Setup\FundTypeController@permanentDelete');
    Route::get('/json/fundTypes/emptyTrash', 'Setup\FundTypeController@emptyTrash');
    Route::get('/json/fundTypes/fetchAll', 'Setup\FundTypeController@fetchAll');
//});

//Route::group(['middleware' => ['hasPermission:settings.manage.finance_settings, report']], function () {
    //CEILING CHAINS
    Route::get('/json/ceilingChains/paginated', 'Setup\CeilingChainController@paginated');
    Route::get('/json/ceilingChains/all', 'Setup\CeilingChainController@index');
    Route::post('/json/ceilingChains', 'Setup\CeilingChainController@store');
    Route::put('/json/ceilingChains/{id}', 'Setup\CeilingChainController@update');
    Route::delete('/json/ceilingChains/{id}', 'Setup\CeilingChainController@delete');
    Route::get('/json/ceilingChains/trashed', 'Setup\CeilingChainController@trashed');
    Route::get('/json/ceilingChains/{id}/restore', 'Setup\CeilingChainController@restore');
    Route::get('/json/ceilingChains/{id}/permanentDelete', 'Setup\CeilingChainController@permanentDelete');
    Route::get('/json/ceilingChains/emptyTrash', 'Setup\CeilingChainController@emptyTrash');
//});

    Route::get('/json/bodInterventions/paginated', 'Setup\BodInterventionController@paginated');
    Route::get('/json/bodInterventions/all', 'Setup\BodInterventionController@index');
    Route::post('/json/bodInterventions/create', 'Setup\BodInterventionController@store');
    Route::post('/json/bodInterventions/update', 'Setup\BodInterventionController@update');
    Route::get('/json/bodInterventions/delete/{id}', 'Setup\BodInterventionController@delete');
    Route::get('/json/bodInterventions/trashed', 'Setup\BodInterventionController@trashed');
    Route::get('/json/bodInterventions/bodrestore/{id}', 'Setup\BodInterventionController@restore');
    Route::get('/json/bodInterventions/permanentDelete/{id}', 'Setup\BodInterventionController@permanentDelete');
    Route::get('/json/bodInterventions/emptyTrash', 'Setup\BodInterventionController@emptyTrash');

//EXPENDITURE GFS CODES
    Route::get('/json/gfs_codes/expenditure', 'Setup\GfsCodeController@expenditure_gfs_codes');
    Route::get('/json/gfs_codes/revenue', 'Setup\GfsCodeController@revenue_gfs_codes');

    Route::get('/json/expenditureCentreGfsCode/{id}/gfs_codes', 'Setup\ExpenditureCentreGfsCodeController@gfs_codes');
    Route::post('/json/expenditureCentreGfsCode/add_gfs_code', 'Setup\ExpenditureCentreGfsCodeController@add_gfs_code');
    Route::get('/json/expenditureCentreGfsCode/{expenditure_centre_gfs_code_id}/{expenditure_centre_id}/delete', 'Setup\ExpenditureCentreGfsCodeController@delete_gfs_code');

    Route::get('/json/expenditureSubCentreGfsCode/{id}/gfs_codes', 'Setup\SectorExpenditureSubCentreGfsCodeController@gfs_codes');
    Route::post('/json/expenditureSubCentreGfsCode/add_gfs_code', 'Setup\SectorExpenditureSubCentreGfsCodeController@add_gfs_code');
    Route::get('/json/expenditureSubCentreGfsCode/{id}/{parent_id}/delete', 'Setup\SectorExpenditureSubCentreGfsCodeController@delete_gfs_code');

//Route::group(['middleware' => ['hasPermission:settings.manage.finance_settings, report']], function () {
    //FUND SOURCE CATEGORIES
    Route::get('/json/fund_source_categories/paginated', 'Setup\FundSourceCategoryController@paginated');
    Route::get('/json/fund_source_categories/all', 'Setup\FundSourceCategoryController@index');
    Route::get('/json/fundSourceCategories', 'Setup\FundSourceCategoryController@index');
    Route::post('/json/fund_source_categories', 'Setup\FundSourceCategoryController@store');
    Route::put('/json/fund_source_categories/{id}', 'Setup\FundSourceCategoryController@update');
    Route::delete('/json/fund_source_categories/{id}', 'Setup\FundSourceCategoryController@delete');
    Route::get('/json/fund_source_categories/trashed', 'Setup\FundSourceCategoryController@trashed');
    Route::get('/json/fund_source_categories/{id}/restore', 'Setup\FundSourceCategoryController@restore');
    Route::get('/json/fund_source_categories/{id}/permanentDelete', 'Setup\FundSourceCategoryController@permanentDelete');
    Route::get('/json/fund_source_categories/emptyTrash', 'Setup\FundSourceCategoryController@emptyTrash');
    Route::post('/json/fund_source_categories/upload', 'Setup\FundSourceCategoryController@upload');
//});

//PERIOD GROUP
    Route::get('/json/periodGroups/paginated', 'Setup\PeriodGroupController@paginated');
    Route::get('/json/periodGroups/all', 'Setup\PeriodGroupController@index');
    Route::post('/json/periodGroups/create', 'Setup\PeriodGroupController@store');
    Route::post('/json/periodGroups/update', 'Setup\PeriodGroupController@update');
    Route::get('/json/periodGroups/delete/{id}', 'Setup\PeriodGroupController@delete');
    Route::get('/json/periodGroups/trashed', 'Setup\PeriodGroupController@trashed');
    Route::get('/json/periodGroups/restore/{id}', 'Setup\PeriodGroupController@restore');
    Route::get('/json/periodGroups/permanentDelete/{id}', 'Setup\PeriodGroupController@permanentDelete');
    Route::get('/json/periodGroups/emptyTrash', 'Setup\PeriodGroupController@emptyTrash');

//REPORT MENU
    Route::get('/json/reports/all', 'Setup\ReportController@tree');
    Route::get('/json/reports', 'Setup\ReportController@index');
    Route::post('/json/reports', 'Setup\ReportController@store');
    Route::put('/json/reports/{id}', 'Setup\ReportController@update');
    Route::delete('/json/reports/{id}', 'Setup\ReportController@delete');

    Route::get('/json/admin_hierarchy_levels/admin_hierarchies', 'Setup\AdminHierarchyLevelController@admin_hierarchies');
    Route::get('/json/admin_hierarchy_levels/regions_by_level', 'Setup\AdminHierarchyLevelController@region_by_levels');
    Route::get('/json/sectors/{sector_id}/planning_units', 'Setup\SectorController@planning_units');

//The routes for the cas plan table select options begin here
    Route::get('/json/casPlanTableOptions', 'Setup\CasPlanTablesSelectOptionController@index');
    Route::get('/json/casPlanTableOptionItems/{group_id}', 'Setup\CasPlanTablesSelectOptionController@getOptions');
    Route::get('/json/casPlanTableSelectFacilityList', 'Setup\CasPlanTablesSelectOptionController@getFacilityList');
    Route::get('/json/casPlanTableSelectFacilityByCouncil/{admin_hierarchy}', 'Setup\CasPlanTablesSelectOptionController@getFacilityByCouncil');

    Route::get('/json/FacilityCount', 'Setup\CasPlanTablesSelectOptionController@getFacilityCount');
//The routes for the cas plan table select options end here

//BOD_VERSION

    Route::get('/json/bodVersions/paginated', 'Setup\BodVersionController@paginated');
    Route::get('/json/bodVersions/all', 'Setup\BodVersionController@index');
    Route::post('/json/bodVersions/create', 'Setup\BodVersionController@store');
    Route::post('/json/bodVersions/update', 'Setup\BodVersionController@update');
    Route::get('/json/bodVersions/delete/{id}', 'Setup\BodVersionController@delete');
    Route::get('/json/bodVersions/trashed', 'Setup\BodVersionController@trashed');
    Route::get('/json/bodVersions/restore/{id}', 'Setup\BodVersionController@restore');
    Route::get('/json/bodVersions/permanentDelete/{id}', 'Setup\BodVersionController@permanentDelete');
    Route::get('/json/bodVersions/emptyTrash', 'Setup\BodVersionController@emptyTrash');

//USER PROFILE KEYS
    Route::get('/json/userProfileKeys/paginated', 'Setup\UserProfileKeyController@paginated');
    Route::get('/json/userProfileKeys/all', 'Setup\UserProfileKeyController@index');
    Route::post('/json/userProfileKeys', 'Setup\UserProfileKeyController@store');
    Route::put('/json/userProfileKeys/{id}', 'Setup\UserProfileKeyController@update');
    Route::delete('/json/userProfileKeys/{id}', 'Setup\UserProfileKeyController@delete');
    Route::get('/json/userProfileKeys/trashed', 'Setup\UserProfileKeyController@trashed');
    Route::get('/json/userProfileKeys/{id}/restore', 'Setup\UserProfileKeyController@restore');
    Route::get('/json/userProfileKeys/{id}/permanentDelete', 'Setup\UserProfileKeyController@permanentDelete');
    Route::get('/json/userProfileKeys/emptyTrash', 'Setup\UserProfileKeyController@emptyTrash');

//The routes of the pe submission form reports start here
    //Route::group(['middleware' => ['hasPermission:settings.manage.budgeting.submission_form']], function () {
    Route::get('/json/peSubmissionFormReports', 'Setup\PeSubmissionFormReportController@index');
    Route::get('/json/getAllPeReports', 'Setup\PeSubmissionFormReportController@allReports');
    Route::post('/json/peSubmissionFormReports', 'Setup\PeSubmissionFormReportController@store');
    Route::post('/json/updatePeSubmissionFormReport', 'Setup\PeSubmissionFormReportController@update');
//    Route::delete('/json/peSubmissionFormReports/{id}', 'Setup\PeSubmissionFormReportController@delete');
    //});

    Route::post('/json/togglePeSubmissionFormReport', 'Setup\PeSubmissionFormReportController@toggleReport');
//The routes of the pe submission form reports end here

    Route::get('/json/userProfile', 'Setup\UserProfileController@get');
    Route::put('/json/userProfile/{id}', 'Setup\UserProfileController@update');
    Route::post('/form/uploadUserProfilePicture', 'Setup\UserProfileController@uploadProfilePicture');
    Route::put('/json/userProfileKeys/{id}', 'Setup\UserProfileController@update');

//The routes of the pe submission form reports start here
    Route::post('/json/getCasPlanTableConstraints', 'Setup\CasPlanTableReportConstraintController@getCasPlanTableConstraints');
    Route::post('/json/casPlanTableConstraints', 'Setup\CasPlanTableReportConstraintController@store');
//The routes of the pe submission form reports end here

    Route::get('/json/api/admin_hierarchies/import', 'Setup\AdminHierarchyController@import');

//BOD_LIST

//ASSESSOR ASSIGNMENTS
    Route::get('/json/assessorAssignments/paginated', 'Setup\AssessorAssignmentController@paginated');
    Route::get('/json/assessorAssignments/all', 'Setup\AssessorAssignmentController@index');
    Route::post('/json/assessorAssignments', 'Setup\AssessorAssignmentController@store');
    Route::post('/json/assessorAssignments/toggleActive', 'Setup\AssessorAssignmentController@toggleActive');
    Route::put('/json/assessorAssignments/{id}', 'Setup\AssessorAssignmentController@update');
    Route::delete('/json/assessorAssignments/{id}', 'Setup\AssessorAssignmentController@delete');
    Route::get('/json/assessorAssignments/trashed', 'Setup\AssessorAssignmentController@trashed');
    Route::get('/json/assessorAssignments/{id}/restore', 'Setup\AssessorAssignmentController@restore');
    Route::get('/json/assessorAssignments/{id}/permanentDelete', 'Setup\AssessorAssignmentController@permanentDelete');
    Route::get('/json/assessorAssignments/emptyTrash', 'Setup\AssessorAssignmentController@emptyTrash');
    Route::get('/json/assessorAssignments/periods', 'Setup\AssessorAssignmentController@periods');
    Route::get('/json/admin_hierarchies/regions', 'Setup\AdminHierarchyController@regions');
    Route::get('/json/admin_hierarchies/{region_id}/lgas', 'Setup\AdminHierarchyController@lgas');
    Route::get('/json/admin_hierarchies/admin_hierarchies', 'Setup\AdminHierarchyController@admin_hierarchies');
    Route::get('/json/adminHierarchies/userRegions', 'Setup\AdminHierarchyController@userRegions');
    Route::get('/json/adminHierarchies/userCouncils', 'Setup\AdminHierarchyController@userCouncils');
    Route::get('/json/adminHierarchies/levelCouncils', 'Setup\AdminHierarchyController@levelCouncils');
    Route::get('/json/adminHierarchies/cleanAdminHierarchies', 'Setup\AdminHierarchyController@cleanAdminHierarchies');

    Route::get('/json/bodVersions/bodInterventions', 'Setup\BodVersionController@bodInterventions');

//USERS
    Route::get('/json/users/all', 'Auth\UserController@index');
    Route::get('/json/users/validateChequeNumber/{userId}/{chequesNumber}', 'Auth\UserController@validateChequeNumber');
    Route::get('json/users/validateEmail/{userId}/{email}', 'Auth\UserController@validateEmail');
    Route::get('/json/users/validateUserName/{userId}/{userName}', 'Auth\UserController@validateUserName');
    Route::get('/json/users/levelUsers', 'Auth\UserController@levelUsers');

//Route::group(['middleware' => ['hasPermission:settings.manage.finance_settings, report']], function () {
    //Surplus categories
    Route::get('/json/activitySurplusCategories/paginated', 'SurplusCategoryController@paginated');
    Route::get('/json/activitySurplusCategories', 'SurplusCategoryController@fetchAll');
    Route::get('/json/activitySurplusCategories/all', 'SurplusCategoryController@index');
    Route::post('/json/activitySurplusCategories', 'SurplusCategoryController@store');
    Route::put('/json/activitySurplusCategories/{id}', 'SurplusCategoryController@update');
    Route::delete('/json/activitySurplusCategories/{id}', 'SurplusCategoryController@delete');
    Route::get('/json/activitySurplusCategories/trashed', 'SurplusCategoryController@trashed');
    Route::get('/json/activitySurplusCategories/{id}/restore', 'SurplusCategoryController@restore');
    Route::get('/json/activitySurplusCategories/{id}/permanentDelete', 'SurplusCategoryController@permanentDelete');
    Route::get('/json/activitySurplusCategories/emptyTrash', 'SurplusCategoryController@emptyTrash');
    Route::get('/json/activitySurplusCategories-changeCatStatus', 'SurplusCategoryController@toggleActive');

//PERFORMANCE INDICATORS
    Route::get('/json/performanceIndicators/paginated', 'Setup\PerformanceIndicatorController@paginated');
    Route::get('/json/performanceIndicators/getByPlanChainId', 'Setup\PerformanceIndicatorController@getByPlanChainId');
    Route::get('/json/performanceIndicators/copy', 'Setup\PerformanceIndicatorController@copy');
    Route::get('/json/performanceIndicators/all', 'Setup\PerformanceIndicatorController@index');
    Route::post('/json/performanceIndicators', 'Setup\PerformanceIndicatorController@store');
    Route::put('/json/performanceIndicators/{id}', 'Setup\PerformanceIndicatorController@update');
    Route::delete('/json/performanceIndicators/{id}', 'Setup\PerformanceIndicatorController@delete');
    Route::get('/json/performanceIndicators/trashed', 'Setup\PerformanceIndicatorController@trashed');
    Route::get('/json/performanceIndicators/{id}/restore', 'Setup\PerformanceIndicatorController@restore');
    Route::get('/json/performanceIndicators/{id}/permanentDelete', 'Setup\PerformanceIndicatorController@permanentDelete');
    Route::get('/json/performanceIndicators/emptyTrash', 'Setup\PerformanceIndicatorController@emptyTrash');
    Route::post('/json/performanceIndicators/toggleQualitative', 'Setup\PerformanceIndicatorController@toggleQualitative');
    Route::post('/json/performanceIndicators/toggleLessIsGood', 'Setup\PerformanceIndicatorController@toggleLessIsGood');
    Route::post('/performanceIndicators/upload', 'Setup\PerformanceIndicatorController@upload');
    Route::get('/json/performanceIndicators/search', 'Setup\PerformanceIndicatorController@search');
    Route::get('/files/performance-indicators/export', 'Setup\PerformanceIndicatorController@exportToCsv');

//ACTIVITY STATUS
    Route::get('/json/activityStatuses', 'Setup\ActivityStatusController@index');
    Route::get('/json/activityStatuses/paginated', 'Setup\ActivityStatusController@paginated');
    Route::get('/json/activityStatuses/all', 'Setup\ActivityStatusController@index');
    Route::post('/json/activityStatuses/create', 'Setup\ActivityStatusController@store');
    Route::post('/json/activityStatuses/update', 'Setup\ActivityStatusController@update');
    Route::get('/json/activityStatuses/delete/{id}', 'Setup\ActivityStatusController@delete');
    Route::get('/json/activityStatuses/trashed', 'Setup\ActivityStatusController@trashed');
    Route::get('/json/activityStatuses/restore/{id}', 'Setup\ActivityStatusController@restore');
    Route::get('/json/activityStatuses/permanentDelete/{id}', 'Setup\ActivityStatusController@permanentDelete');
    Route::get('/json/activityStatuses/emptyTrash', 'Setup\ActivityStatusController@emptyTrash');
    Route::post('/json/toggleBodVersion', 'Setup\BodVersionController@toggleBodVersion');
    Route::post('/json/toggleActivityStatus/', 'Setup\ActivityStatusController@toggleActivityStatus');

//TASK NATURE
    Route::get('/json/task_natures/paginated', 'Setup\TaskNatureController@paginated');
    Route::get('/json/task_natures/all', 'Setup\TaskNatureController@index');
    Route::post('/json/task_nature/create', 'Setup\TaskNatureController@store');
    Route::post('/json/task_nature/update', 'Setup\TaskNatureController@update');
    Route::get('/json/task_nature/delete/{id}', 'Setup\TaskNatureController@delete');
    Route::get('/json/task_natures/trashed', 'Setup\TaskNatureController@trashed');
    Route::get('/json/task_nature/restore/{id}', 'Setup\TaskNatureController@restore');
    Route::get('/json/task_nature/permanentDelete/{id}', 'Setup\TaskNatureController@permanentDelete');
    Route::get('/json/task_natures/emptyTrash', 'Setup\TaskNatureController@emptyTrash');

//ACTIVITY TASK NATURES
    Route::get('/json/activityTaskNatures/paginated', 'Setup\ActivityTaskNatureController@paginated');
    Route::get('/json/activityTaskNatures', 'Setup\ActivityTaskNatureController@fetchAll');
    Route::get('/json/activityTaskNatures/all', 'Setup\ActivityTaskNatureController@index');
    Route::post('/json/activityTaskNatures', 'Setup\ActivityTaskNatureController@store');
    Route::put('/json/activityTaskNatures/{id}', 'Setup\ActivityTaskNatureController@update');
    Route::delete('/json/activityTaskNatures/{id}', 'Setup\ActivityTaskNatureController@delete');
    Route::get('/json/activityTaskNatures/trashed', 'Setup\ActivityTaskNatureController@trashed');
    Route::get('/json/activityTaskNatures/{id}/restore', 'Setup\ActivityTaskNatureController@restore');
    Route::get('/json/activityTaskNatures/{id}/permanentDelete', 'Setup\ActivityTaskNatureController@permanentDelete');
    Route::get('/json/activityTaskNatures/emptyTrash', 'Setup\ActivityTaskNatureController@emptyTrash');
    Route::get('/json/activityTaskNatures/toggleActive/{id}', 'Setup\ActivityTaskNatureController@toggleActive');

//});

//FACILITY OWNERSHIP
    Route::get('/json/facilityOwnerships/paginated', 'Setup\FacilityOwnershipController@paginated');
    Route::get('/json/facilityOwnerships/all', 'Setup\FacilityOwnershipController@index');
    Route::post('/json/facilityOwnerships', 'Setup\FacilityOwnershipController@store');
    Route::put('/json/facilityOwnerships/{id}', 'Setup\FacilityOwnershipController@update');
    Route::delete('/json/facilityOwnerships/{id}', 'Setup\FacilityOwnershipController@delete');
    Route::get('/json/facilityOwnerships/trashed', 'Setup\FacilityOwnershipController@trashed');
    Route::get('/json/facilityOwnerships/{id}/restore', 'Setup\FacilityOwnershipController@restore');
    Route::get('/json/facilityOwnerships/{id}/permanentDelete', 'Setup\FacilityOwnershipController@permanentDelete');
    Route::get('/json/facilityOwnerships/emptyTrash', 'Setup\FacilityOwnershipController@emptyTrash');

//FACILITY PHYSICAL STATES
    Route::get('/json/facilityPhysicalStates/paginated', 'Setup\FacilityPhysicalStateController@paginated');
    Route::get('/json/facilityPhysicalStates/all', 'Setup\FacilityPhysicalStateController@index');
    Route::post('/json/facilityPhysicalStates', 'Setup\FacilityPhysicalStateController@store');
    Route::put('/json/facilityPhysicalStates/{id}', 'Setup\FacilityPhysicalStateController@update');
    Route::delete('/json/facilityPhysicalStates/{id}', 'Setup\FacilityPhysicalStateController@delete');
    Route::get('/json/facilityPhysicalStates/trashed', 'Setup\FacilityPhysicalStateController@trashed');
    Route::get('/json/facilityPhysicalStates/{id}/restore', 'Setup\FacilityPhysicalStateController@restore');
    Route::get('/json/facilityPhysicalStates/{id}/permanentDelete', 'Setup\FacilityPhysicalStateController@permanentDelete');
    Route::get('/json/facilityPhysicalStates/emptyTrash', 'Setup\FacilityPhysicalStateController@emptyTrash');

//FACILITY STAR RATINGS
    Route::get('/json/facilityStarRatings/paginated', 'Setup\FacilityStarRatingController@paginated');
    Route::get('/json/facilityStarRatings/all', 'Setup\FacilityStarRatingController@index');
    Route::post('/json/facilityStarRatings', 'Setup\FacilityStarRatingController@store');
    Route::put('/json/facilityStarRatings/{id}', 'Setup\FacilityStarRatingController@update');
    Route::delete('/json/facilityStarRatings/{id}', 'Setup\FacilityStarRatingController@delete');
    Route::get('/json/facilityStarRatings/trashed', 'Setup\FacilityStarRatingController@trashed');
    Route::get('/json/facilityStarRatings/{id}/restore', 'Setup\FacilityStarRatingController@restore');
    Route::get('/json/facilityStarRatings/{id}/permanentDelete', 'Setup\FacilityStarRatingController@permanentDelete');
    Route::get('/json/facilityStarRatings/emptyTrash', 'Setup\FacilityStarRatingController@emptyTrash');

//FACILITY CUSTOM DETAILS
    Route::get('/json/facilityCustomDetails/paginated', 'Setup\FacilityCustomDetailController@paginated');
    Route::get('/json/facilityCustomDetails/all', 'Setup\FacilityCustomDetailController@index');
    Route::post('/json/facilityCustomDetails', 'Setup\FacilityCustomDetailController@store');
    Route::put('/json/facilityCustomDetails/{id}', 'Setup\FacilityCustomDetailController@update');
    Route::delete('/json/facilityCustomDetails/{id}', 'Setup\FacilityCustomDetailController@delete');
    Route::get('/json/facilityCustomDetails/trashed', 'Setup\FacilityCustomDetailController@trashed');
    Route::get('/json/facilityCustomDetails/{id}/restore', 'Setup\FacilityCustomDetailController@restore');
    Route::get('/json/facilityCustomDetails/{id}/permanentDelete', 'Setup\FacilityCustomDetailController@permanentDelete');
    Route::get('/json/facilityCustomDetails/emptyTrash', 'Setup\FacilityCustomDetailController@emptyTrash');

//FACILITY CUSTOM DETAIL MAPPINGS
    Route::get('/json/facilityCustomDetailMappings/paginated', 'Setup\FacilityCustomDetailMappingController@paginated');
    Route::get('/json/facilityCustomDetailMappings/all', 'Setup\FacilityCustomDetailMappingController@index');
    Route::post('/json/facilityCustomDetailMappings', 'Setup\FacilityCustomDetailMappingController@store');
    Route::put('/json/facilityCustomDetailMappings/{id}', 'Setup\FacilityCustomDetailMappingController@update');
    Route::delete('/json/facilityCustomDetailMappings/{id}', 'Setup\FacilityCustomDetailMappingController@delete');
    Route::get('/json/facilityCustomDetailMappings/trashed', 'Setup\FacilityCustomDetailMappingController@trashed');
    Route::get('/json/facilityCustomDetailMappings/{id}/restore', 'Setup\FacilityCustomDetailMappingController@restore');
    Route::get('/json/facilityCustomDetailMappings/{id}/permanentDelete', 'Setup\FacilityCustomDetailMappingController@permanentDelete');
    Route::get('/json/facilityCustomDetailMappings/emptyTrash', 'Setup\FacilityCustomDetailMappingController@emptyTrash');
    Route::get('/json/facilityCustomDetailMappings/loadFacilityCustomDetails', 'Setup\FacilityCustomDetailMappingController@loadFacilityCustomDetails');

    Route::get('/json/facilityTypes/fetchAll', 'Setup\FacilityTypeController@fetchAll');
    Route::get('/json/facilities/fetchAll', 'Setup\FacilityController@fetchAll');
    Route::get('/json/facilities/similarFacilities', 'Setup\FacilityController@similarFacilities');
    Route::get('/json/facilities/allFacilities', 'Setup\FacilityController@allFacilities');
    Route::get('/json/facilities/myFacilityCustomDetails', 'Setup\FacilityController@myFacilityCustomDetails');

//NATIONAL TARGETS
    Route::get('/json/nationalTargets/paginated', 'Setup\NationalTargetController@paginated');
    Route::get('/json/nationalTargets/all', 'Setup\NationalTargetController@index');
    Route::post('/json/nationalTargets', 'Setup\NationalTargetController@store');
    Route::put('/json/nationalTargets/{id}', 'Setup\NationalTargetController@update');
    Route::delete('/json/nationalTargets/{id}', 'Setup\NationalTargetController@delete');
    Route::get('/json/nationalTargets/trashed', 'Setup\NationalTargetController@trashed');
    Route::get('/json/nationalTargets/{id}/restore', 'Setup\NationalTargetController@restore');
    Route::get('/json/nationalTargets/{id}/permanentDelete', 'Setup\NationalTargetController@permanentDelete');
    Route::get('/json/nationalTargets/emptyTrash', 'Setup\NationalTargetController@emptyTrash');
    Route::post('/files/national-targets/upload', 'Setup\NationalTargetController@upload');

//SECTORS

//PRIORITY AREAS
    Route::get('/json/priorityAreas/fetchAll', 'Setup\PriorityAreaController@fetchAll');
    Route::get('/json/priorityAreas/interventions', 'Setup\PriorityAreaController@interventions');
    Route::get('/json/priorityAreas/genericSectorProblems', 'Setup\PriorityAreaController@genericSectorProblems');

//PLANNING MATRIX
    Route::get('/json/planningMatrices/paginated', 'Setup\PlanningMatrixController@paginated');
    Route::get('/json/planningMatrices/all', 'Setup\PlanningMatrixController@index');
    Route::post('/json/planningMatrices', 'Setup\PlanningMatrixController@store');
    Route::put('/json/planningMatrices/{id}', 'Setup\PlanningMatrixController@update');
    Route::delete('/json/planningMatrices/{id}', 'Setup\PlanningMatrixController@delete');
    Route::get('/json/planningMatrices/trashed', 'Setup\PlanningMatrixController@trashed');
    Route::get('/json/planningMatrices/{id}/restore', 'Setup\PlanningMatrixController@restore');
    Route::get('/json/planningMatrices/{id}/permanentDelete', 'Setup\PlanningMatrixController@permanentDelete');
    Route::get('/json/planningMatrices/emptyTrash', 'Setup\PlanningMatrixController@emptyTrash');
    Route::post('/json/planningMatrices/toggleActive', 'Setup\PlanningMatrixController@toggleActive');

//GENERIC SECTOR PROBLEMS
    Route::get('/json/genericSectorProblems/paginated', 'Setup\GenericSectorProblemController@paginated');
    Route::get('/json/genericSectorProblems/all', 'Setup\GenericSectorProblemController@index');
    Route::post('/json/genericSectorProblems', 'Setup\GenericSectorProblemController@store');
    Route::put('/json/genericSectorProblems/{id}', 'Setup\GenericSectorProblemController@update');
    Route::delete('/json/genericSectorProblems/{id}', 'Setup\GenericSectorProblemController@delete');
    Route::get('/json/genericSectorProblems/trashed', 'Setup\GenericSectorProblemController@trashed');
    Route::get('/json/genericSectorProblems/{id}/restore', 'Setup\GenericSectorProblemController@restore');
    Route::get('/json/genericSectorProblems/{id}/permanentDelete', 'Setup\GenericSectorProblemController@permanentDelete');
    Route::get('/json/genericSectorProblems/emptyTrash', 'Setup\GenericSectorProblemController@emptyTrash');
    Route::post('/json/genericSectorProblems/toggleActive', 'Setup\GenericSectorProblemController@toggleActive');
    Route::get('/json/genericSectorProblems/getByPriorityArea/{priorityAreaId}', 'Setup\GenericSectorProblemController@getByPriorityArea');
    Route::get('/json/genericSectorProblems/genericTargets', 'Setup\GenericSectorProblemController@genericTargets');
    Route::get('/json/genericSectorProblems/generateCodes', 'Setup\GenericSectorProblemController@generateCodes');

//GENERIC TARGETS
    Route::get('/json/genericTargets/{planningMatrixId}/paginated', 'Setup\GenericTargetController@paginated');
    Route::get('/json/genericTargets/all', 'Setup\GenericTargetController@index');
    Route::post('/json/genericTargets', 'Setup\GenericTargetController@createOrUpdate');
    Route::get('/json/genericTargets/byPlanningMatrix/{planningMatrixId}', 'Setup\GenericTargetController@byPlanningMatrix');
    Route::put('/json/genericTargets/{id}', 'Setup\GenericTargetController@update');
    Route::delete('/json/genericTargets/{id}', 'Setup\GenericTargetController@delete');
    Route::get('/json/genericTargets/trashed', 'Setup\GenericTargetController@trashed');
    Route::get('/json/genericTargets/{id}/restore', 'Setup\GenericTargetController@restore');
    Route::get('/json/genericTargets/{id}/permanentDelete', 'Setup\GenericTargetController@permanentDelete');
    Route::get('/json/genericTargets/emptyTrash', 'Setup\GenericTargetController@emptyTrash');
    Route::post('/json/genericTargets/toggleActive', 'Setup\GenericTargetController@toggleActive');

    Route::get('/json/genericTargets/performanceIndicators', 'Setup\GenericTargetController@performanceIndicators');
    Route::post('/json/genericTargets/addPerformanceIndicator', 'Setup\GenericTargetController@addPerformanceIndicator');
    Route::get('/json/genericTargets/removePerformanceIndicator', 'Setup\GenericTargetController@removePerformanceIndicator');

    Route::get('/json/genericTargets/nationalReferences', 'Setup\GenericTargetController@nationalReferences');
    Route::post('/json/genericTargets/addNationalReference', 'Setup\GenericTargetController@addNationalReference');
    Route::get('/json/genericTargets/removeNationalReference', 'Setup\GenericTargetController@removeNationalReference');

    Route::get('/json/genericTargets/costCentres', 'Setup\GenericTargetController@costCentres');
    Route::post('/json/genericTargets/addCostCentre', 'Setup\GenericTargetController@addCostCentre');
    Route::get('/json/genericTargets/removeCostCentre', 'Setup\GenericTargetController@removeCostCentre');
    Route::get('/files/genericTargets/downloadExcel/{planningMatrixId}', 'Setup\GenericTargetController@downloadExcel');

    Route::get('/json/planChains/fetchAll', 'Setup\PlanChainController@fetchAll');
    Route::get('/json/planChains/search', 'Setup\PlanChainController@search');
    Route::get('/json/planChains/lowestPlanChains', 'Setup\PlanChainController@lowestPlanChains');
    Route::get('/json/planChains/copyPlanChains', 'Setup\PlanChainController@copy');
    Route::get('/json/planChains/priorityAreas', 'Setup\PlanChainController@priorityAreas');
    Route::post('/json/planChains/addPriorityAreas', 'Setup\PlanChainController@addPriorityAreas');
    Route::get('/json/planChains/removePriorityArea', 'Setup\PlanChainController@removePriorityArea');
    Route::get('/json/planChains/performanceIndicators', 'Setup\PlanChainController@performanceIndicators');
    Route::get('/json/planChains/nextFinancialYearVersion/{currentFinancialYearId}/{planChainId}', 'Setup\PlanChainController@getNextFinancialYearVersion');
    Route::post('/json/planChain/{planChainId}/add-to-version/{versionId}', 'Setup\PlanChainController@addToVersion');
    Route::get('/json/planChains/validateObjCode/{objCode}', 'Setup\PlanChainController@validateObjCode');

//GENERIC ACTIVITIES
    Route::get('/json/genericActivities/paginate/{planningMatrixId}', 'Setup\GenericActivityController@paginate');
    Route::post('/json/genericActivities', 'Setup\GenericActivityController@createOrUpdate');
    Route::delete('/json/genericActivities/{id}', 'Setup\GenericActivityController@delete');

    Route::get('/json/genericActivities/all', 'Setup\GenericActivityController@index');
    Route::put('/json/genericActivities/{id}', 'Setup\GenericActivityController@update');
    Route::get('/json/genericActivities/trashed', 'Setup\GenericActivityController@trashed');
    Route::get('/json/genericActivities/{id}/restore', 'Setup\GenericActivityController@restore');
    Route::get('/json/genericActivities/{id}/permanentDelete', 'Setup\GenericActivityController@permanentDelete');
    Route::get('/json/genericActivities/emptyTrash', 'Setup\GenericActivityController@emptyTrash');
    Route::get('/json/genericActivities/setCostCentre', 'Setup\GenericActivityController@setCostCentre');
    Route::get('/json/genericActivities/deleteCostCentre', 'Setup\GenericActivityController@deleteCostCentre');
    Route::get('/json/genericActivities/downloadExcel', 'Setup\GenericActivityController@downloadExcel');
    Route::get('/files/genericActivities/downloadExcel/{planningMatrixId}', 'Setup\GenericActivityController@downloadExcel');

    Route::get('/json/interventions/fetchAll', 'Setup\InterventionController@fetchAll');
    Route::get('/json/budgetClasses/fetchAll', 'Setup\BudgetClassController@fetchAll');
    Route::get('/json/nationalReferences/fetchAll', 'Setup\ReferenceController@fetchAll');

    Route::get('/json/planChains/sectors', 'Setup\PlanChainController@sectors');
    Route::get('/json/planChains/loadParentPlanChains', 'Setup\PlanChainController@loadParentPlanChains');

//EXPENDITURE CENTRE GROUPS
    Route::get('/json/expenditureCentreGroups/paginated', 'Setup\ExpenditureCentreGroupController@paginated');
    Route::get('/json/expenditureCentreGroups/groupCostCentres', 'Setup\ExpenditureCentreGroupController@groupCostCentres');
    Route::get('/json/expenditureCentreGroups/loadExpenditureGroupCostCentres', 'Setup\ExpenditureCentreGroupController@loadExpenditureGroupCostCentres');
    Route::get('/json/expenditureCentreGroups/all', 'Setup\ExpenditureCentreGroupController@index');
    Route::post('/json/expenditureCentreGroups', 'Setup\ExpenditureCentreGroupController@store');
    Route::put('/json/expenditureCentreGroups/{id}', 'Setup\ExpenditureCentreGroupController@update');
    Route::delete('/json/expenditureCentreGroups/{id}', 'Setup\ExpenditureCentreGroupController@delete');
    Route::get('/json/expenditureCentreGroups/trashed', 'Setup\ExpenditureCentreGroupController@trashed');
    Route::get('/json/expenditureCentreGroups/groupExpenditureCentres', 'Setup\ExpenditureCentreGroupController@groupExpenditureCentres');
    Route::get('/json/expenditureCentreGroups/{id}/restore', 'Setup\ExpenditureCentreGroupController@restore');
    Route::get('/json/expenditureCentreGroups/{id}/permanentDelete', 'Setup\ExpenditureCentreGroupController@permanentDelete');
    Route::get('/json/expenditureCentreGroups/emptyTrash', 'Setup\ExpenditureCentreGroupController@emptyTrash');
    Route::post('/json/expenditureCentreGroups/toggleActive', 'Setup\ExpenditureCentreGroupController@toggleActive');

    Route::get('/json/fundSources/fetchAll', 'Setup\FundSourceController@fetchAll');
    Route::get('/json/fundSources/copy', 'Setup\FundSourceController@copy');
    Route::post('/files/fund-sources/upload', 'Setup\FundSourceController@upload');
    Route::get('/json/fund-sources/sectorDepartmentFundSources', 'Setup\FundSourceController@sectorDepartmentFundSources');
    Route::get('/json/fund-sources/revenueFundSources', 'Setup\FundSourceController@revenueFundSources');

//EXPENDITURE CENTRE

    Route::get('/json/expenditureCentres/paginated', 'Setup\ExpenditureCentreController@paginated');
    Route::get('/json/expenditureCentres/all', 'Setup\ExpenditureCentreController@index');
    Route::post('/json/expenditureCentres', 'Setup\ExpenditureCentreController@store');
    Route::put('/json/expenditureCentres/{id}', 'Setup\ExpenditureCentreController@update');
    Route::delete('/json/expenditureCentres/{id}', 'Setup\ExpenditureCentreController@delete');
    Route::get('/json/expenditureCentres/trashed', 'Setup\ExpenditureCentreController@trashed');
    Route::get('/json/expenditureCentres/{id}/restore', 'Setup\ExpenditureCentreController@restore');
    Route::get('/json/expenditureCentres/{id}/permanentDelete', 'Setup\ExpenditureCentreController@permanentDelete');
    Route::get('/json/expenditureCentres/emptyTrash', 'Setup\ExpenditureCentreController@emptyTrash');
    Route::get('/json/expenditureCentres/fetchAll', 'Setup\ExpenditureCentreController@fetchAll');
    Route::get('/json/expenditureCentres/links', 'Setup\ExpenditureCentreController@links');
    Route::post('/json/expenditureCentres/addLink', 'Setup\ExpenditureCentreController@addLink');
    Route::get('/json/expenditureCentres/removeLink', 'Setup\ExpenditureCentreController@removeLink');
    Route::get('/json/expenditureCentres/fetchAll', 'Setup\ExpenditureCentreController@fetchAll');
    Route::post('/json/expenditureCentres/addValue', 'Setup\ExpenditureCentreController@addValue');
    Route::get('/json/expenditureCentres/removeValue', 'Setup\ExpenditureCentreController@removeValue');
    Route::get('/json/expenditureCentres/values', 'Setup\ExpenditureCentreController@values');
    Route::get('/json/expenditureCentres/taskNatures', 'Setup\ExpenditureCentreController@taskNatures');
    Route::post('/json/expenditureCentres/addTaskNature', 'Setup\ExpenditureCentreController@addTaskNature');
    Route::get('/json/expenditureCentres/removeTaskNature', 'Setup\ExpenditureCentreController@removeTaskNature');
    Route::get('/json/expenditureCentres/costCentres', 'Setup\ExpenditureCentreController@costCentres');
    Route::post('/json/expenditureCentres/addCostCentre', 'Setup\ExpenditureCentreController@addCostCentre');
    Route::get('/json/expenditureCentres/removeCostCentre', 'Setup\ExpenditureCentreController@removeCostCentre');

    Route::get('/json/lgaLevels/fetchAll', 'Setup\LgaLevelController@fetchAll');
    Route::get('/json/gfs_codes/onlyRevenueOnes', 'Setup\GfsCodeController@onlyRevenueOnes');

//FACILITIES
    Route::get('/json/facilities/facilitiesByTypeAndHierarchy', 'Setup\FacilityController@facilitiesByTypeAndHierarchy');
    Route::get('/json/adminHierarchies/councils', 'Setup\AdminHierarchyController@councils');

//The routes of the project data forms start here
    Route::get('/json/ProjectDataForms', 'Setup\ProjectDataFormController@index');
    Route::post('/json/createProjectDataForm', 'Setup\ProjectDataFormController@store');
    Route::post('/json/UpdateProjectDataForms', 'Setup\ProjectDataFormController@update');
    Route::delete('/json/DeleteProjectDataForms/{id}', 'Setup\ProjectDataFormController@delete');
//The routes of the project data forms end here

//The routes of the project data form contents start here
    Route::get('/json/projectDataFormContents/{form_id}', 'Setup\ProjectDataFormContentController@index');
    Route::get('/json/ListFormContents/{form_id}', 'Setup\ProjectDataFormContentController@listFormContents');
    Route::get('/json/allProjectDataFormContents/{form_id}', 'Setup\ProjectDataFormContentController@indexAll');
    Route::post('/json/createProjectDataFormContents', 'Setup\ProjectDataFormContentController@store');
    Route::post('/json/updateProjectDataFormContents', 'Setup\ProjectDataFormContentController@update');
    Route::get('/json/deleteProjectDataFormContents/{id}', 'Setup\ProjectDataFormContentController@delete');
//The routes of the project data form contents end here

//The routes of the project data form questions start here
    Route::get('/json/projectDataFormQuestions', 'Setup\ProjectDataFormQuestionController@index');
    Route::get('/json/GetProjectDataFormQuestions/{content_id}', 'Setup\ProjectDataFormQuestionController@getQuestionByContent');
    Route::post('/json/createProjectDataFormQuestions', 'Setup\ProjectDataFormQuestionController@store');
    Route::post('/json/updateProjectDataFormQuestions', 'Setup\ProjectDataFormQuestionController@update');
    Route::get('/json/deleteProjectDataFormQuestions/{id}', 'Setup\ProjectDataFormQuestionController@delete');
//The routes of the project data form questions end here

//links specifications table
    Route::get('/json/linkSpecs/paginated', 'Setup\LinkSpecController@paginated');
    Route::get('/json/linkSpecs/all', 'Setup\LinkSpecController@index');
    Route::get('/json/linkSpecs/fetchAll', 'Setup\LinkSpecController@fetchAll');
    Route::post('/json/linkSpecs', 'Setup\LinkSpecController@store');
    Route::put('/json/linkSpecs/{id}', 'Setup\LinkSpecController@update');
    Route::delete('/json/linkSpecs/{id}', 'Setup\LinkSpecController@delete');
    Route::get('/json/linkSpecs/trashed', 'Setup\LinkSpecController@trashed');
    Route::get('/json/linkSpecs/{id}/restore', 'Setup\LinkSpecController@restore');
    Route::get('/json/linkSpecs/{id}/permanentDelete', 'Setup\LinkSpecController@permanentDelete');
    Route::get('/json/linkSpecs/emptyTrash', 'Setup\LinkSpecController@emptyTrash');
    Route::get('/json/periodGroups/fetchAll', 'Setup\PeriodGroupController@fetchAll');

//The routes for the project dat form question values begin here
    Route::get('/json/getProjectDataFormQuestionValues/{id}', 'Setup\ProjectDataFormQuestionItemValueController@index');
    Route::get('/json/GetQuestionValues/{project_id}/{content_id}', 'Setup\ProjectDataFormQuestionItemValueController@getQuestionValues');
    Route::post('/json/saveProjectDataFormQuestionValues', 'Setup\ProjectDataFormQuestionItemValueController@store');
//The routes for the project dat form question values end here

//ASSET CONDITIONS
    Route::get('/json/assetConditions/paginated', 'Setup\AssetConditionController@paginated');
    Route::get('/json/assetConditions/all', 'Setup\AssetConditionController@index');
    Route::get('/json/assetConditions/fetchAll', 'Setup\AssetConditionController@fetchAll');
    Route::post('/json/assetConditions', 'Setup\AssetConditionController@store');
    Route::put('/json/assetConditions/{id}', 'Setup\AssetConditionController@update');
    Route::delete('/json/assetConditions/{id}', 'Setup\AssetConditionController@delete');
    Route::get('/json/assetConditions/trashed', 'Setup\AssetConditionController@trashed');
    Route::get('/json/assetConditions/{id}/restore', 'Setup\AssetConditionController@restore');
    Route::get('/json/assetConditions/{id}/permanentDelete', 'Setup\AssetConditionController@permanentDelete');
    Route::get('/json/assetConditions/emptyTrash', 'Setup\AssetConditionController@emptyTrash');

//ASSET USES
    Route::get('/json/assetUses/paginated', 'Setup\AssetUseController@paginated');
    Route::get('/json/assetUses/all', 'Setup\AssetUseController@index');
    Route::get('/json/assetUses/fetchAll', 'Setup\AssetUseController@fetchAll');
    Route::post('/json/assetUses', 'Setup\AssetUseController@store');
    Route::put('/json/assetUses/{id}', 'Setup\AssetUseController@update');
    Route::delete('/json/assetUses/{id}', 'Setup\AssetUseController@delete');
    Route::get('/json/assetUses/trashed', 'Setup\AssetUseController@trashed');
    Route::get('/json/assetUses/{id}/restore', 'Setup\AssetUseController@restore');
    Route::get('/json/assetUses/{id}/permanentDelete', 'Setup\AssetUseController@permanentDelete');
    Route::get('/json/assetUses/emptyTrash', 'Setup\AssetUseController@emptyTrash');

//TRANSPORT FACILITY TYPES
    Route::get('/json/transportFacilityTypes/paginated', 'Setup\TransportFacilityTypeController@paginated');
    Route::get('/json/transportFacilityTypes/all', 'Setup\TransportFacilityTypeController@index');
    Route::get('/json/transportFacilityTypes/fetchAll', 'Setup\TransportFacilityTypeController@fetchAll');
    Route::post('/json/transportFacilityTypes', 'Setup\TransportFacilityTypeController@store');
    Route::put('/json/transportFacilityTypes/{id}', 'Setup\TransportFacilityTypeController@update');
    Route::delete('/json/transportFacilityTypes/{id}', 'Setup\TransportFacilityTypeController@delete');
    Route::get('/json/transportFacilityTypes/trashed', 'Setup\TransportFacilityTypeController@trashed');
    Route::get('/json/transportFacilityTypes/{id}/restore', 'Setup\TransportFacilityTypeController@restore');
    Route::get('/json/transportFacilityTypes/{id}/permanentDelete', 'Setup\TransportFacilityTypeController@permanentDelete');
    Route::get('/json/transportFacilityTypes/emptyTrash', 'Setup\TransportFacilityTypeController@emptyTrash');

//section levels
    Route::get('/json/sectionLevels/getLevelsByUser', 'Setup\SectionLevelController@getLevelsByUser');
    Route::get('/json/sectionLevels/getSections', 'Setup\SectionLevelController@getSections');
    Route::get('/json/sectorProblems/spnumber', 'Setup\SectorProblemController@spnumber');
    Route::get('/json/facilityTypes/sector', 'Setup\FacilityTypeController@sector');
    Route::get('/files/documents/download/{id}', 'Setup\DocumentController@download');

    Route::get('/json/financialYears/fetchAll', 'Setup\FinancialYearController@fetchAll');
    Route::get('/json/referenceDocumentTypes/fetchAll', 'Setup\ReferenceDocumentTypeController@fetchAll');

    Route::post('/files/template-files/upload', 'Setup\UploadController@uploadTemplates');

/*
 * Budget submission select option controller
 */
    //Route::group(['middleware' => ['hasPermission:settings.manage.budgeting.submission_form']], function () {
    Route::get('/json/BudgetSubmissionSelectOption', 'Setup\BudgetSubmissionSelectOptionsController@index');
    Route::get('/json/BudgetSubmissionSelectOption/getChildren', 'Setup\BudgetSubmissionSelectOptionsController@getChildren');
    Route::post('/json/BudgetSubmissionSelectOption', 'Setup\BudgetSubmissionSelectOptionsController@store');
    Route::put('/json/BudgetSubmissionSelectOption/{id}', 'Setup\BudgetSubmissionSelectOptionsController@update');
    Route::delete('/json/BudgetSubmissionSelectOption/{id}', 'Setup\BudgetSubmissionSelectOptionsController@destroy');
    //});

    Route::get('/json/sections/by-sector/{sectorId}', 'Setup\SectionController@getBySector');

    Route::get('/json/calendars/get-user-todos', 'Setup\CalendarController@getUserTodos');
    Route::get('/json/admin-hierarchy-levels/get-user-level-and-children', 'Setup\AdminHierarchyLevelController@getUserLevelAndChildren');

    Route::get('/json/facilities/facilitiesByTypeCouncilUser', 'Setup\FacilityController@facilitiesByTypeCouncilUser');
    Route::get('/json/facilities/facilitiesAllByTypeCouncilUser', 'Setup\FacilityController@facilitiesAllByTypeCouncilUser');
    Route::get('/json/facilities/facilitiesByTypeCouncilSetup', 'Setup\FacilityController@facilitiesByTypeCouncilSetup');

/*HISTORICAL DATA*/
    Route::get('/api/historicalDataTables/paginated', 'Setup\HistoricalDataController@paginated');
    Route::get('/api/historicalDataTables/columns', 'Setup\HistoricalDataController@columns');
    Route::get('/api/historicalDataTables/fetchAll', 'Setup\HistoricalDataController@fetchAll');
    Route::post('/api/historicalDataTables', 'Setup\HistoricalDataController@store');
    Route::post('/api/historicalDataTables/addColumn', 'Setup\HistoricalDataController@addColumn');
    Route::post('/api/historicalDataTables/updateDataColumn', 'Setup\HistoricalDataController@updateDataColumn');
    Route::get('/api/historicalDataTables/removeColumn', 'Setup\HistoricalDataController@removeColumn');
    Route::get('/api/historicalDataTables/dataRows', 'Setup\HistoricalDataController@dataRows');
    Route::post('/api/historicalDataTables/saveRowValues', 'Setup\HistoricalDataController@saveRowValues');
    Route::put('/api/historicalDataTables/{id}', 'Setup\HistoricalDataController@update');
    Route::delete('/api/historicalDataTables/{id}', 'Setup\HistoricalDataController@delete');

//PROJECT TYPES
    Route::get('/json/projectTypes/paged', 'Setup\ProjectTypeController@paginated');
    Route::get('/json/projectTypes', 'Setup\ProjectTypeController@index');
    Route::post('/json/projectTypes', 'Setup\ProjectTypeController@store');
    Route::put('/json/projectTypes/{id}', 'Setup\ProjectTypeController@update');
    Route::delete('/json/projectTypes/{id}', 'Setup\ProjectTypeController@delete');

//EXPENDITURE CATEGORY
    Route::get('/json/expenditureCategories/paged', 'Setup\ExpenditureCategoryController@paginated');
    Route::get('/json/expenditureCategories/expenditureCategoryByTaskNature', 'Setup\ExpenditureCategoryController@expenditureCategoryByTaskNature');
    Route::get('/json/expenditureCategories', 'Setup\ExpenditureCategoryController@index');
    Route::post('/json/expenditureCategories', 'Setup\ExpenditureCategoryController@store');
    Route::put('/json/expenditureCategories/{id}', 'Setup\ExpenditureCategoryController@update');
    Route::delete('/json/expenditureCategories/{id}', 'Setup\ExpenditureCategoryController@delete');
    Route::get('/json/expenditureCategories/toggleActive/{id}', 'Setup\ExpenditureCategoryController@toggleActive');

//PROJECT OUTPUT
    Route::get('/json/projectOutputs/paged', 'Setup\ProjectOutputController@paginated');
    Route::get('/json/projectOutputs', 'Setup\ProjectOutputController@index');
    Route::get('/json/expenditureCategories/projectOutputs', 'Setup\ProjectOutputController@getByExpenditureCategory');
    Route::post('/json/projectOutputs', 'Setup\ProjectOutputController@store');
    Route::put('/json/projectOutputs/{id}', 'Setup\ProjectOutputController@update');
    Route::delete('/json/projectOutputs/{id}', 'Setup\ProjectOutputController@delete');

//Route::group(['middleware' => ['hasPermission:settings.manage.finance_settings, report']], function () {
    //FUND SOURCE BUDGET CLASSES
    Route::get('/api/fundSourceBudgetClasses/paged', 'Setup\FundSourceBudgetClassController@paginated');
    Route::get('/files/fundSourceBudgetClasses/exportToCsv', 'Setup\FundSourceBudgetClassController@exportToCsv');
    Route::get('/api/fundSourceBudgetClasses', 'Setup\FundSourceBudgetClassController@index');
    Route::post('/api/fundSourceBudgetClasses', 'Setup\FundSourceBudgetClassController@store');
    Route::put('/api/fundSourceBudgetClasses/{id}', 'Setup\FundSourceBudgetClassController@update');
    Route::delete('/api/fundSourceBudgetClasses/{id}', 'Setup\FundSourceBudgetClassController@delete');
//});

    Route::get('/api/versionTypes', 'Setup\VersionTypeController@index');
    Route::get('/api/versionTypes/paged', 'Setup\VersionTypeController@paginated');
    Route::post('/api/versionTypes', 'Setup\VersionTypeController@store');
    Route::put('/api/versionTypes/{id}', 'Setup\VersionTypeController@update');
    Route::get('/api/versionTypes/{id}', 'Setup\VersionTypeController@show');
    Route::delete('/api/versionTypes/{id}', 'Setup\VersionTypeController@delete');

    Route::get('/api/versions', 'Setup\VersionController@index');
    Route::get('/api/versions/paged', 'Setup\VersionController@paginated');
    Route::get('/api/versions/toggleCurrent', 'Setup\VersionController@toggleCurrent');
    Route::post('/api/versions', 'Setup\VersionController@store');
    Route::put('/api/versions/{id}', 'Setup\VersionController@update');
    Route::get('/api/versions/{id}', 'Setup\VersionController@show');
    Route::delete('/api/versions/{id}', 'Setup\VersionController@delete');
    Route::get('/api/versions/nextFinancialYearVersion/{versionType}/{currentFinancialYearId}/{itemId}', 'Setup\VersionController@getNextFinancialYearVersion');
    Route::post('/api/versions/{versionType}/{itemId}/add-to-version/{versionId}', 'Setup\VersionController@addToVersion');

//Route::group(['middleware' => ['hasPermission:settings.manage.gfs_codes_settings, settings.manage.gfs_codes, report']], function () {
    Route::get('/api/gfsCodeSubCategories', 'Setup\GfsCodeSubCategoryController@index');
    Route::get('/api/gfsCodeSubCategories/paged', 'Setup\GfsCodeSubCategoryController@paginated');
    Route::post('/api/gfsCodeSubCategories', 'Setup\GfsCodeSubCategoryController@store');
    Route::post('/api/gfsCodeSubCategories/toggleProcurement', 'Setup\GfsCodeSubCategoryController@toggleProcurable');
    Route::post('/api/gfsCodeSubCategories/upload', 'Setup\GfsCodeSubCategoryController@upload');
    Route::put('/api/gfsCodeSubCategories/{id}', 'Setup\GfsCodeSubCategoryController@update');
    Route::get('/api/gfsCodeSubCategories/{id}', 'Setup\GfsCodeSubCategoryController@show');
    Route::get('/api/gfsCodeSubCategories/getByCategory', 'Setup\GfsCodeSubCategoryController@filter');
    Route::delete('/api/gfsCodeSubCategories/{id}', 'Setup\GfsCodeSubCategoryController@delete');
//});

    Route::get('/api/interventions', 'Setup\InterventionController@index');
    Route::get('/api/interventions/paged', 'Setup\InterventionController@paginated');
    Route::post('/api/interventions', 'Setup\InterventionController@store');
    Route::put('/api/interventions/{id}', 'Setup\InterventionController@update');
    Route::get('/api/interventions/active', 'Setup\InterventionController@active');
    Route::get('/api/interventions/primary', 'Setup\InterventionController@primary');
    Route::delete('/api/interventions/{id}', 'Setup\InterventionController@delete');
    Route::get('/api/interventions/copy', 'Setup\InterventionController@copy');

    Route::get('/api/referenceTypes', 'Setup\ReferenceTypeController@index');
    Route::post('/api/referenceTypes', 'Setup\ReferenceTypeController@store');
    Route::put('/api/referenceTypes/{id}', 'Setup\ReferenceTypeController@update');
    Route::delete('/api/referenceTypes/{id}/{versionId}', 'Setup\ReferenceTypeController@delete');
    Route::get('/api/referenceTypes/copy', 'Setup\ReferenceTypeController@copy');

//Route::group(['middleware' => ['hasPermission:settings.manage.finance_settings, report']], function () {
    Route::get('/api/bankAccounts', 'Setup\BankAccountController@index');
    Route::get('/api/bankAccounts/paged', 'Setup\BankAccountController@paginated');
    Route::get('/api/bankAccounts/copyBankAccounts', 'Setup\BankAccountController@copy');
    Route::post('/api/bankAccounts', 'Setup\BankAccountController@store');
    Route::put('/api/bankAccounts/{id}', 'Setup\BankAccountController@update');
    Route::delete('/api/bankAccounts/{id}', 'Setup\BankAccountController@delete');
//});

    Route::get('/json/reports/cdr', 'Report\MainController@cdr');
    Route::get('/files/reports/downloadCdr', 'Report\MainController@downloadCdr');

    Route::get('/json/reports/cdrConsolidated', 'Report\MainController@consolidatedCdr');
    Route::get('/files/reports/consolidatedCdrDownload', 'Report\MainController@consolidatedCdrDownload');

    Route::post('/json/create-metadata-sources', 'Setup\MetadataSourcesController@create');
    Route::get('/json/get-all-metadata-sources', 'Setup\MetadataSourcesController@show');

    Route::post('/json/create-metadata-types', 'Setup\MetadataTypesController@create');
    Route::get('/json/get-all-metadata-types', 'Setup\MetadataTypesController@show');

//OPRAS Exports
    Route::get('/json/opras/oprasSetups', 'Misc\MainController@oprasSetups');

    Route::get('/json/read-imes-metadata', 'Setup\MetadataListItemsController@readApiData');
    Route::get('/json/update-data-elements', 'Setup\MetadataListItemsController@updateDataElements');
// Route::get('/json/get-data-values-periodically', 'Setup\MetadataListItemsController@getDataValuesPeriodically');
    Route::get('/api/metadataSources', 'Setup\MetadataSourcesController@index');
    Route::get('/api/metadataSources/paged', 'Setup\MetadataSourcesController@paged');
    Route::post('/api/metadataSources', 'Setup\MetadataSourcesController@store');
    Route::put('/api/metadataSources/{id}', 'Setup\MetadataSourcesController@update');
    Route::delete('/api/metadataSources/{id}', 'Setup\MetadataSourcesController@delete');

    Route::get('/api/metadataTypes', 'Setup\MetadataTypesController@index');
    Route::get('/api/metadataTypes/paged', 'Setup\MetadataTypesController@paged');
    Route::post('/api/metadataTypes', 'Setup\MetadataTypesController@store');
    Route::put('/api/metadataTypes/{id}', 'Setup\MetadataTypesController@update');
    Route::delete('/api/metadataTypes/{id}', 'Setup\MetadataTypesController@delete');

    Route::get('/api/metadataItems', 'Setup\MetadataListItemsController@index');
    Route::get('/api/metadataItems/paged', 'Setup\MetadataListItemsController@paged');
    Route::post('/api/metadataItems', 'Setup\MetadataListItemsController@store');
    Route::put('/api/metadataItems/{id}', 'Setup\MetadataListItemsController@update');
    Route::delete('/api/metadataItems/{id}', 'Setup\MetadataListItemsController@delete');
    Route::get('/api/metadataItems/search', 'Setup\MetadataListItemsController@search');
    Route::get('/api/metadataItems/dataSets', 'Setup\MetadataListItemsController@dataSets');
    Route::get('/api/metadataItems/dataElements', 'Setup\MetadataListItemsController@dataElements');
    Route::get('/api/metadataItems/sync', 'Setup\MetadataListItemsController@sync');
    Route::get('/api/metadataItems/dataSourceDataSets', 'Setup\MetadataListItemsController@dataSourceDataSets');

//Route::group(['middleware' => ['hasPermission:settings.manage.gfs_codes_settings, settings.manage.gfs_codes, report']], function () {
    Route::get('/api/procurementMethods', 'Setup\ProcurementMethodController@index');
    Route::get('/api/procurementMethods/paged', 'Setup\ProcurementMethodController@paginated');
    Route::post('/api/procurementMethods', 'Setup\ProcurementMethodController@store');
    Route::put('/api/procurementMethods/{id}', 'Setup\ProcurementMethodController@update');
    Route::delete('/api/procurementMethods/{id}', 'Setup\ProcurementMethodController@delete');
//});

//Route::group(['middleware' => ['hasPermission:settings.manage.advertisement, report']], function () {
    //advertisements routes
    Route::get('/api/advertisements', 'AdvertismentController@index');
    Route::get('/api/advertisements/paged', 'AdvertismentController@paginated');
    Route::post('/api/advertisements', 'AdvertismentController@store');
    Route::post('/api/upload-advertisements', 'AdvertismentController@uploadAdvertisement');
    Route::get('/api/advertisements-changeAdStatus', 'AdvertismentController@changeAdStatus');
    Route::put('/api/advertisements/{id}', 'AdvertismentController@update');
    Route::put('/api/advertisements-remove', 'AdvertismentController@removeAdvertisement');
    Route::delete('/api/advertisements/{id}', 'AdvertismentController@delete');
//});

//Route::group(['middleware' => ['hasPermission:settings.manage.logo, report']], function () {
    //logo  getLogoByPisc
    Route::get('/api/get-by-pisc', 'Setup\LogoController@getLogoByPisc');
    Route::get('/api/logos/paged', 'Setup\LogoController@getLogoByPisc');
    Route::post('/api/upload-logo', 'Setup\LogoController@uploadLogo');
    Route::post('/api/save-logo-to-db', 'Setup\LogoController@saveLogo');
    Route::put('/api/logo-remove', 'Setup\LogoController@removeLogo');
    Route::delete('/api/logo/{id}', 'Setup\LogoController@deleteLogo');
//});

//cofog
    Route::get('/api/get-parent-cofog', 'Setup\CofogController@getParentCofog');
    Route::get('/api/get-child-cofog', 'Setup\CofogController@getChildCofog');
    Route::get('/api/getCofogIdFromLongTermTargetReference/{id}', 'Setup\CofogController@getBytargetId');
    Route::get('/api/get-allCofog', 'Setup\AdminHierarchyController@getAllParentCofog');

//pisc type
    Route::get('/api/get-pisc-type', 'Setup\AdminHierarchyController@getPiscType');

    Route::get('/api/feature-notifications', 'Setup\FeatureNotificationController@paginate');
    Route::post('/api/feature-notifications', 'Setup\FeatureNotificationController@update');
    Route::delete('/api/feature-notifications/{id}', 'Setup\FeatureNotificationController@delete');
    Route::get('/api/feature-notifications/get-active', 'Setup\FeatureNotificationController@getActive');

});
Route::get('/api/advertisements', 'AdvertismentController@index');
Route::get('/json/get-data-values-periodically', 'Setup\MetadataListItemsController@getDataValuesPeriodically');
Route::get('/api/refreshMaterializedView/{viewName}', 'Setup\ConfigurationSettingController@refreshMaterializedView');
Route::get('/json/importCarrOver/{financialYearId}/{adminHierarchyId}/{sectionId}/{facilityId}/{fundSourceId}/{budgetClassId}/{amount}', 'Budgeting\AdminHierarchyCeilingController@importCarryOverCeiling');
