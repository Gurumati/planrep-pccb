<?php
/**
 * Created by PhpStorm.
 * User: Kachinga
 * Date: 3/4/2017
 * Time: 12:41 PM
 */

Route::get('/download/{file}', 'Misc\DownloadController@download');
Route::get('/support/chat', 'Misc\MainController@pusher');