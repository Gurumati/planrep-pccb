<?php
/**
 * Created by PhpStorm.
 * User: Kachinga
 * Date: 3/4/2017
 * Time: 12:38 PM
 */
Route::group(['middleware' => 'authentication'], function () {
    Route::get('/planning', 'Planning\MainController@index');
});

Route::group(['middleware' => ['ajaxAuthentication', 'hasPermission:planning,budgeting']], function () {

    Route::group(['middleware' => ['hasPermission:planning.manage.plans']], function () {

        /**
         * Routes to fix invalid activity codes i.e codes with more that 8 characters
         */
        Route::get('/json/get-total-activity-with-invalid-chain-code', 'Planning\ActivityController@getTotalActivityWithInvalidChainCode');
        Route::post('/json/fix-activity-with-invalid-chain-code/{limit}', 'Planning\ActivityController@fixActivityWithInvalidChainCode');
        Route::post('/json/refresh-activity-code/{activityId}', 'Planning\ActivityController@refreshCode');

        /**
         * Routes to fix invalid activity codes i.e codes with more that 8 characters
         */
        Route::get('/json/get-total-activity-with-invalid-code', 'Planning\ActivityController@getTotalActivityWithInvalidCode');
        Route::post('/json/fix-activity-with-invalid-code/{limit}', 'Planning\ActivityController@fixActivityWithInvalidCode');
        /**
         * Routes to fix invalid activity codes i.e codes with more that 8 characters
         */

        Route::get('/json/get-total-activity-with-duplicate-code', 'Planning\ActivityController@getTotalActivityWithDuplicateCode');
        Route::post('/json/fix-activity-with-duplicate-code/{limit}', 'Planning\ActivityController@fixActivityWithDuplicateCode');

        Route::get('/json/plans', 'Planning\MainController@jsonPlans');
        Route::get('/json/plan/filter/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}', 'Planning\MtefController@filter');
        Route::post('/json/activities/{mtefSectionId}/{budgetType}', 'Planning\ActivityController@save');
        Route::get('/json/getPlan/{mtefSectionId}', 'Planning\ActivityController@getByMtefSectionId');
        //Get painated Activities with post body contains facility Ids to be filtered
        Route::post('/json/activities/paginated/{mtefSectionId}/{targetId}/{budgetType}', 'Planning\ActivityController@paginated');
        Route::get('/json/activities/by-mtef-section/{mtefSectionId}/by-budget-type/{budgetType}', 'Planning\ActivityController@getByMtefSectionAndBudgetType');
        Route::get('/json/activities/disapprove/{id}', 'Planning\ActivityController@disApprove');
        Route::get('/json/activities/disapprove-by-system/{adminHierarchyId}/{financialYearId}/{budgetType}/{financialSystem}', 'Planning\ActivityController@disApproveBySystem');
        Route::get('/json/activities/approve-by-system/{adminHierarchyId}/{financialYearId}/{budgetType}/{financialSystem}', 'Planning\ActivityController@approveBySystem');
        Route::put('/json/activities/toggle-facility-account/{id}/{isFacilityAccount}', 'Planning\ActivityController@toggleFacilityAccount');
        Route::get('/json/activities/approve/{id}', 'Planning\ActivityController@approve');
        Route::get('/json/activityFacilities/{activityId}', 'Planning\ActivityFacilityController@getByActivity');
        Route::get('/json/activitiesBySectionAndBudgetClass/{mtefSectionId}/{budgetClassId}', 'Planning\ActivityController@getActivitiesWithInput');
        Route::post('/json/deleteActivity/{id}/{mtefSectionId}', 'Planning\ActivityController@delete');
        Route::get('/json/planningData/{mtefSectionId}/{targetId}', 'Planning\ActivityController@getPlanningData');
        Route::get('/json/annual-targets/getActivityCategories', 'Planning\AnnualTargetController@getActivityCategories');
        Route::get('/json/getAnnualTargets/{mtef_id}', 'Planning\ActivityController@getAnnualTargets');
        Route::get('/json/annual-targets/by-mtef-section/paginated/{mtefId}/{mtefSectionId}/{budgetType}', 'Planning\AnnualTargetController@byMtefSectionPaginated');
        Route::get('/json/annual-targets/by-mtef-section/by-plan-chain/{mtefId}/{mtefSectionId}/{budgetType}/{planChainId}', 'Planning\AnnualTargetController@byMtefSectionAndPlanChain');
        Route::put('/json/annual-target/confirm/{id}', 'Planning\AnnualTargetController@confirm');
        Route::post('/json/annual-target/initiate/{financialYearId}/{longTermTargetId}', 'Planning\AnnualTargetController@initiate');
        Route::post('/json/createOrUpdateInputs/{facilityFundSourceId}/{mtefSectionId}/{budgetClassId}', 'Planning\ActivityController@createOrEditInputs');
        Route::post('/json/createOrUpdateActivity/{mtefSectionId}', 'Planning\ActivityController@createOrEditActivity');
        Route::get('/json/activities/getGenericActivitiesByTarget/{targetId}', 'Planning\ActivityController@getGenericActivitiesByTarget');

    });

    //Route::group(['middleware' => ['hasPermission:planning.manage.long_term_targets']], function () {
        Route::get('/json/longTermTargetsNoAnnual', 'Planning\LongTermTargetController@indexNoAnnual');
        Route::get('/json/longTermTargetsPagination', 'Planning\LongTermTargetController@indexPagination');
        Route::post('/json/createLongTermTarget', 'Planning\LongTermTargetController@store');
        Route::post('/json/updateLongTermTarget', 'Planning\LongTermTargetController@update');
        Route::post('/json/deleteLongTermTarget/{id}', 'Planning\LongTermTargetController@delete');
        Route::post('/json/createAnnualTarget', 'Planning\AnnualTargetController@store');
        Route::post('/json/updateAnnualTarget', 'Planning\AnnualTargetController@update');
        Route::get('/json/deleteAnnualTarget/{id}', 'Planning\AnnualTargetController@delete');
        Route::get('/json/targets/by-plan-chain/{planChainId}/{perPage}', 'Planning\LongTermTargetController@paginateByPlanChain');

    //});

    //Route::group(['middleware' => ['hasPermission:planning.manage.performance_indicator_base_line_values']], function () {
        Route::get('/json/performanceIndicatorBaselineValues/paginated', 'Planning\PerformanceIndicatorBaselineValueController@paginated');
        Route::get('/json/performanceIndicatorBaselineValues/all', 'Planning\PerformanceIndicatorBaselineValueController@index');
        Route::post('/json/performanceIndicatorBaselineValues', 'Planning\PerformanceIndicatorBaselineValueController@store');
        Route::put('/json/performanceIndicatorBaselineValues/{id}', 'Planning\PerformanceIndicatorBaselineValueController@update');
        Route::delete('/json/performanceIndicatorBaselineValues/{id}', 'Planning\PerformanceIndicatorBaselineValueController@delete');
        Route::get('/json/performanceIndicatorBaselineValues/trashed', 'Planning\PerformanceIndicatorBaselineValueController@trashed');
        Route::get('/json/performanceIndicatorBaselineValues/{id}/restore', 'Planning\PerformanceIndicatorBaselineValueController@restore');
        Route::get('/json/performanceIndicatorBaselineValues/{id}/permanentDelete', 'Planning\PerformanceIndicatorBaselineValueController@permanentDelete');
        Route::get('/json/performanceIndicatorBaselineValues/emptyTrash', 'Planning\PerformanceIndicatorBaselineValueController@emptyTrash');
        Route::get('/json/performanceIndicatorBaselineValues/financialYearBaselineValues', 'Planning\PerformanceIndicatorBaselineValueController@financialYearBaselineValues');
        Route::post('/json/performanceIndicatorBaselineValues/addProjection', 'Planning\PerformanceIndicatorBaselineValueController@addProjection');
        Route::get('/json/performanceIndicatorBaselineValues/deleteProjection', 'Planning\PerformanceIndicatorBaselineValueController@deleteProjection');
    //});

    Route::get('/json/getUsableFinancialYears', 'Setup\FinancialYearController@usableFinancialYears');
   
    Route::get('/json/projections', 'Planning\ProjectionController@index');
    Route::get('/json/paginatedProjections', 'Planning\ProjectionController@paginated');
    Route::post('/json/createProjection/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}/{fundSourceId}', 'Planning\ProjectionController@store');
    Route::post('/json/initializeProjection/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}/{fundSourceId}/{gfsCodeId}', 'Planning\ProjectionController@initProjection');
    // Route::post('/json/createProjection/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}', 'Planning\ProjectionController@store');
    Route::post('/json/updateProjection', 'Planning\ProjectionController@update');
    Route::get('/json/deleteProjection/{id}', 'Planning\ProjectionController@delete');

    Route::get('/json/getLowestPlanChains', 'Setup\PlanChainController@getLowestPlanChains');
    Route::get('/json/objectives/fetchAll', 'Planning\PlanChainController@fetchAll');
    Route::get('/json/allProjectionBudgetClasses/{id}', 'Planning\ProjectionController@allProjectionBudgetClasses');
    Route::post('/json/updateProjectionBudgetClassService', 'Planning\ProjectionController@updateAmounts');


    Route::get('/json/annualTargets', 'Planning\AnnualTargetController@index');



    Route::group(['middleware' => ['hasPermission:settings.manage.budget_settings']], function () {
        //admin hierarchy sector mapping
        Route::get('/json/adminHierarchySectMapping', 'Planning\AdminHierarchySectMappingsController@index');
        Route::post('/json/createAdminHierarchySectMapping', 'Planning\AdminHierarchySectMappingsController@store');
        //end here
    });

    Route::get('/json/allFinancialYears', 'Setup\FinancialYearController@index');
    Route::get('/json/adminHierarchySectMapping', 'Planning\AdminHierarchySectMappingsController@index');
    Route::post('/json/createAdminHierarchySectMapping', 'Planning\AdminHierarchySectMappingsController@store');

    Route::get('/json/allBudgetClasses', 'Setup\BudgetClassController@index');
    Route::get('/json/allFundSources', 'Setup\FundSourceController@allFundSources');
    Route::get('/json/allAdminHierarchies', 'Setup\AdminHierarchyController@allAdminHierarchies');
    Route::get('/json/allProjectionPeriods/{id}', 'Planning\ProjectionController@allProjectionPeriods');
    Route::post('/json/updateProjectionPeriodPercentage', 'Planning\ProjectionController@updateProjectionPeriodPercentage');

    //PE CONTRIBUTIONS
    Route::get('/json/pe_contributions', 'Planning\PEContributionController@index');
    Route::get('/json/paginatedPEContributions', 'Planning\PEContributionController@paginated');
    Route::post('/json/createPEContribution', 'Planning\PEContributionController@store');
    Route::post('/json/updatePEContribution', 'Planning\PEContributionController@update');
    Route::get('/json/deletePEContribution/{id}', 'Planning\PEContributionController@delete');
    Route::post('/json/togglePEContribution', 'Planning\PEContributionController@togglePEContribution');


    Route::post('/json/plan-chain/by-type/{planChainTypeId}/by-section/{mtefSectionId}', 'Setup\PlanChainController@getByTypeAndSection');



    //SECTOR TARGET
    Route::get('/json/sectorTargets', 'Planning\SectorTargetController@index');
    Route::get('/json/sectorTargetsOnly', 'Planning\SectorTargetController@indexNoRelation');
    Route::get('/json/paginatedSectorTargets', 'Planning\SectorTargetController@paginated');
    Route::post('/json/createSectorTarget', 'Planning\SectorTargetController@store');
    Route::post('/json/updateSectorTarget', 'Planning\SectorTargetController@update');
    Route::get('/json/deleteSectorTarget/{id}', 'Planning\SectorTargetController@delete');
    Route::get('/json/allLongTermTargets', 'Planning\SectorTargetController@allLongTermTargets');
    Route::get('/json/allSectors', 'Planning\SectorTargetController@allSectors');

    //LONG TERM TARGET
    Route::get('/json/longTermTargets', 'Planning\LongTermTargetController@index');
    Route::get('/json/getLongTermTargetByRefDocId/{doc_Id}', 'Planning\LongTermTargetController@getLongTermTargetByRefDocId');
    Route::get('/json/referenceDocuments', 'Planning\ReferenceDocumentController@index');
    Route::get('/json/longTermTargets/getGenericTargetByPlanChain/{planChainId}', 'Planning\LongTermTargetController@genericTargetsByPlanChain');
    Route::get('/json/longTermTargets/refreshCode/{id}', 'Planning\LongTermTargetController@refreshCode');

    //ANNUAL TARGET
    Route::get('/json/annualTargets', 'Planning\AnnualTargetController@index');

    //END ANNUAL TARGET
    Route::get('/json/mtefAnnualTargets', 'Planning\MtefAnnualTargetController@index');

    Route::get('/json/annualTargetByUser/{mtefId}/{mtefSectionId}', 'Planning\AnnualTargetController@annualTargetByUser');
    Route::get('/json/userCanTargetSections', 'Setup\SectionController@userTargetingSections');
    Route::get('/json/userCanBudgetSections', 'Setup\SectionController@userCanBudgetSections');
    Route::post('/json/createMtefAnnualTarget', 'Planning\MtefAnnualTargetController@store');
    Route::post('/json/updateMtefAnnualTarget', 'Planning\MtefAnnualTargetController@update');
    Route::get('/json/deleteMtefAnnualTarget/{id}', 'Planning\MtefAnnualTargetController@delete');
    Route::get('/json/mtefAnnualTargetsPagination', 'Planning\MtefAnnualTargetController@indexPagination');
    Route::get('/json/facilitiesByHierarchyAndSection', 'Setup\FacilityController@getByHierarchyAndSection');
    Route::get('/json/allMTEFSections', 'Planning\MtefSectionController@allMTEFSections');
    Route::get('/json/allMtefs', 'Planning\MtefController@index');


    //The route below autocompletes a facility
    Route::get('/json/autoCompleteFacility/{key_word}', 'Setup\FacilityController@search');
    Route::get('/json/sectionBudgetDecisionLevel', 'Planning\MtefController@sectionBudgetDecisionLevel');
    Route::get('/json/mainBudgetDecisionLevels', 'Planning\MtefController@MainBudgetDecisionLevels');

    //MTEF SECTOR PROBLEMS
    Route::get('/json/mtef_sector_problems/paginated', 'Planning\MtefSectorProblemController@paginated');
    Route::get('/json/mtef_sector_problems/all', 'Planning\MtefSectorProblemController@index');
    Route::post('/json/mtef_sector_problem/create', 'Planning\MtefSectorProblemController@store');
    Route::post('/json/mtef_sector_problem/update', 'Planning\MtefSectorProblemController@update');
    Route::get('/json/mtef_sector_problem/delete/{id}', 'Planning\MtefSectorProblemController@delete');
    Route::get('/json/mtef_sector_problems/trashed', 'Planning\MtefSectorProblemController@trashed');
    Route::get('/json/mtef_sector_problem/restore/{id}', 'Planning\MtefSectorProblemController@restore');
    Route::get('/json/mtef_sector_problem/permanentDelete/{id}', 'Planning\MtefSectorProblemController@permanentDelete');
    Route::get('/json/mtef_sector_problems/emptyTrash', 'Planning\MtefSectorProblemController@emptyTrash');

    Route::get('/json/mtefs/all', 'Planning\MtefController@adminHierarchyMtefs');


    //ACTIVITY
    Route::get('/json/activities/search', ['uses' => 'Planning\ActivityController@search', 'as' => '/json/activities/search']);
    Route::get('/json/activities/all', 'Planning\ActivityController@index');
    Route::get('/json/activities/fetchAll', 'Planning\ActivityController@fetchAll');
    Route::post('/json/activities/{financialYearId}/{budgetType}/{adminHierarchyId}/{sectionId}', 'Planning\ActivityController@createOrUpdate');

    //cas plan table item admin hierarchy
    Route::get(' /json/CasPlanTabularTable/{cas_plan_id}', 'Planning\CasPlanTableItemAdminHierarchyController@getTabularTable');
    Route::get(' /json/CasPlanTableItemAdminHierarchy/{cas_plan_table_id}', 'Planning\CasPlanTableItemAdminHierarchyController@index');
    Route::post(' /json/CreateCasPlanTableItemAdminHierarchy', 'Planning\CasPlanTableItemAdminHierarchyController@store');
    //cas plan table item admin hierarchy end

    //The routes for the cas plan table details begin here
    Route::get('/json/casPlanTableDetails', 'Planning\CasPlanTableDetailsController@getCasPlanTableDetails');
    Route::post('/json/createCasPlanTableDetails', 'Planning\CasPlanTableDetailsController@store');
    Route::post(' /json/updateCasPlanTableDetails/{id}', 'Planning\CasPlanTableDetailsController@update');
    //The routes for the cas plan table details end here

    //The routes of the baseline data values begin here
    Route::get('/json/baselineStatistics', 'Setup\BaselineStatisticController@indexSingle');
    Route::get('/json/baselineStatisticValues', 'Planning\BaselineStatisticValueController@index');
    Route::post('/json/createBaselineStatisticValues', 'Planning\BaselineStatisticValueController@store');
    //The routes of the baseline data values end here

    //The routes of the assets begin here
    Route::get('/json/assets', 'Planning\AssetController@index');
    Route::post('/json/createAsset', 'Planning\AssetController@store');
    Route::post('/json/updateAsset', 'Planning\AssetController@update');
    Route::get('/json/deleteAsset/{id}', 'Planning\AssetController@delete');
    //Route::get('/json/loadParentAssets', 'Planning\AssetController@loadParentAssets');
    //The routes of the assets end here

    //PERFORMANCE_INDICATOR_BASELINE_VALUES
    Route::get('/json/performanceIndicators/getByTarget/{targetId}', 'Setup\PerformanceIndicatorController@getByTarget');
    Route::get('/json/performanceIndicators/getByPlanChain/{planChainId}', 'Setup\PerformanceIndicatorController@getByPlanChain');
    Route::get('/json/performanceIndicators/getForUser', 'Planning\PerformanceIndicatorBaselineValueController@paginated');
    Route::post('/json/performanceIndicators/saveValues', 'Planning\PerformanceIndicatorBaselineValueController@saveValues');

    //TRANSPORT FACILITIES
    Route::get('/json/transportFacilities/paginated', 'Planning\AssetController@paginated');
    Route::get('/json/transportFacilities/all', 'Planning\AssetController@index');
    Route::get('/json/transportFacilities/fetchAll', 'Planning\AssetController@fetchAll');
    Route::post('/json/transportFacilities', 'Planning\AssetController@store');
    Route::put('/json/transportFacilities/{id}', 'Planning\AssetController@update');
    Route::delete('/json/transportFacilities/{id}', 'Planning\AssetController@delete');
    Route::get('/json/transportFacilities/trashed', 'Planning\AssetController@trashed');
    Route::get('/json/transportFacilities/{id}/restore', 'Planning\AssetController@restore');
    Route::get('/json/transportFacilities/{id}/permanentDelete', 'Planning\AssetController@permanentDelete');
    Route::get('/json/transportFacilities/emptyTrash', 'Planning\AssetController@emptyTrash');
    Route::post('/json/performanceIndicators/saveActualValues', 'Planning\PerformanceIndicatorBaselineValueController@saveActualValues');

    Route::get('/json/planningFundSources', 'Setup\FundSourceController@getPlanningFundSources');

    //Facility By user section
    Route::get('/json/facilityByUserSection', 'Setup\FacilityController@facilityByUserSection');
    Route::post('/json/facility/searchByPlanningSection/{mtefSectionId}/{isFacilityAccount}', 'Setup\FacilityController@searchByPlanningSection');
    Route::get('/json/facility/searchByAdmiAreaAndSection/{adminHierarchyId}/{sectionId}/{isFacilityUser}', 'Setup\FacilityController@searchByAdmiAreaAndSection');
    Route::post('/json/facility/getAllByPlanningSection/{mtefSectionId}/{isFacilityAccount}', 'Setup\FacilityController@getAllByPlanningSection');
    Route::get('/json/facility/get-by-user/{userId}', 'Setup\FacilityController@getByUser');
    Route::get('/json/activity-facilities/get-by-activity/{activityId}/paginate', 'Planning\ActivityController@getPaginatedActivityFacilities');
    Route::post('/json/activity-facilities/add-for-activity/{activityId}', 'Planning\ActivityController@addFacilityForActivity');
    Route::delete('/json/activity-facilities/delete-from-activity/{activityFacilityId}', 'Planning\ActivityController@deleteFacilityFromActivity');
    Route::delete('/json/activity-facilities/delete-all-from-activity/{activityId}', 'Planning\ActivityController@deleteAllFacilityFromActivity');
    Route::get('/json/facility/get-home-facility-and-supervised-facility-types/{adminHierarchyId}/{sectionId}', 'Setup\FacilityController@getHomeFacilityAndSuperviedFacilityTypes');
    Route::get('/json/activity-facility-project', 'Planning\ActivityController@getActivityFacilityProject');
    Route::get('/json/move-activity-data', 'Planning\MtefController@getToMoveData');
    Route::get('/json/move-activity-mtefs', 'Planning\MtefController@getMtefs');
    Route::get('/json/move-activity-mtef-sections/{mtefId}', 'Planning\MtefController@getMtefSections');
    Route::get('/json/move-activity-activities/{budgetType}/{mtefId}/{mtefSectionId}', 'Planning\MtefController@getToMoveActivities');
    Route::put('/json/move-activity-budget-class/{activityId}/{budgetClassId}', 'Planning\MtefController@moveActivityBudgetClass');
    Route::put('/json/move-facility-fund-source/{activityFacilityFundSourceId}/{fundSourceId}', 'Planning\MtefController@moveFacilityToFundSource');
    Route::put('/json/move-activity-cost-centre/{activityId}/{metfSectionId}', 'Planning\MtefController@moveActivityCostCentre');
    Route::put('/json/move-activity-facility/{activityFacilityId}/{facilityId}', 'Planning\MtefController@moveActivityFacility');
    Route::put('/json/move-activity-target/{activityId}/{targetId}', 'Planning\MtefController@moveActivityTarget');

    //submit cas plan submission for assessment
    Route::post('/api/casPlanSubmission', 'Planning\CasPlanSubmissionController@submitPlan');

    Route::get('/json/execution/pre-execution/project-output-filled', 'Planning\ActivityController@projectOutputFilled');
    Route::get('/api/pre-execution/get_not_filled_reallocation', 'Planning\ActivityController@downloadProjectOutputFilled');

});


