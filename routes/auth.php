<?php
/**
 * Created by PhpStorm.
 * User: Kachinga
 * Date: 3/4/2017
 * Time: 12:38 PM
 */

Route::get('/auth/user/register','Auth\UserController@create');
Route::get('/auth/user/activate/{email}/{activationCode}','Auth\UserController@activate');

Route::get('/auth/user/login','Auth\UserController@login');
Route::post('/auth/user/login','Auth\UserController@authenticate');
Route::get('/auth/user/logout','Auth\UserController@logout');
Route::get('/auth','Auth\MainController@index');

Route::get('/auth/user/forgot_password','Auth\UserController@forgot_password');
Route::post('/auth/user/forgot_password','Auth\UserController@reset_password');

Route::post('/json/updatePassword','Auth\UserController@updatePassword');

Route::get('/auth/user/new_password/{email}/{reset_code}','Auth\UserController@new_password');
Route::post('/auth/user/new_password/{email}/{reset_code}','Auth\UserController@create_new_password');

Route::group(['middleware' => 'authentication'], function () {

    Route::get('/setup','Setup\MainController@index');
    Route::post('/auth/user/store','Auth\UserController@store');
    Route::get('/auth/users','Auth\UserController@index');
    Route::get('/auth','Auth\UserController@login');
    Route::get('/auth/user/create','Auth\UserController@create');
    Route::get('/auth/user/{id}/edit','Auth\UserController@edit');
    Route::post('/auth/user/{id}/update','Auth\UserController@update');
    Route::get('/auth/user/{id}/delete','Auth\UserController@destroy');

    //Route::group(['middleware' => ['hasPermission:settings.manage.roles, report']], function () {
    Route::get('/json/roles','Auth\RoleController@index');
    Route::get('/json/roleByUser','Auth\RoleController@rolesByUser');
    Route::post('/json/createRole','Auth\RoleController@store');
    Route::post('/json/updateRole','Auth\RoleController@update');
    //});

    Route::group(['middleware' => ['hasPermission:settings.manage.users, report']], function () {
    Route::get('/json/users','Auth\UserController@index2');
    Route::post('/json/createUser','Auth\UserController@store2');
    Route::post('/json/updateUser','Auth\UserController@update2');
    Route::put('/json/activateUser/{userId}','Auth\UserController@activateUser');
    Route::put('/json/deActivateUser/{userId}','Auth\UserController@deActivateUser');
    Route::get('/json/userDetails/getUsersDetails','Setup\UsersTrackingController@getUsersDetails');
    });

    Route::get('/user','User\MainController@index');
    Route::get('/json/user/profile','User\MainController@profile');
    Route::get('/json/user/messages','User\MainController@messages');
    Route::get('/json/user/get-events/{userId}','Auth\UserController@getUserEvents');
    Route::put('/json/user/add-facility/{userId}/{facilityId}','Auth\UserController@addFacility');
    Route::put('/json/user/remove-facility/{userId}/{facilityId}','Auth\UserController@removeFacility');

    //user profile keys
});
