<?php
/**
 * Created by PhpStorm.
 * User: Kachinga
 * Date: 3/4/2017
 * Time: 12:41 PM
 */
Route::get('/','Dashboard\DashboardController@index');

Route::get('/test-page','test\TestController@index');

Route::get('/json/dashBoard/getBudgetBySector/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@budgetBySector');
Route::get('/json/dashBoard/getBudgetByBudgetClass/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@budgetByBudgetClass');
Route::get('/json/dashBoard/getBudgetByFundSource/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@budgetByFundSource');
Route::get('/json/dashBoard/getBudgetByCostCentre/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@budgetByCostCentre');
Route::get('/json/dashBoard/getAdminHierarchyBudgetWithChildren/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@getAdminHierarchyBudgetWithChildren');
Route::get('/json/dashBoard/getPercentageByDecisionLevel/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@getPercentageByDecisionLevel');
Route::get('/json/dashBoard/getPercentageByStatus/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@getPercentageByStatus');
Route::get('/json/dashBoard/getProjection/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@getProjection');
Route::get('/json/dashBoard/getUserSection','Dashboard\DashboardController@getUserSection');
Route::get('/json/dashBoard/getBudgetByCeiling/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@budgetByCeiling');
Route::get('/json/dashBoard/getTotalCeilingAgainstTotalBudget/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@getTotalCeilingAgainstTotalBudget');
Route::get('/json/dashBoard/getTotalBudgetByClass/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@getTotalBudgetByClass');
Route::get('/json/dashBoard/getProcurableAgainstTotalBudget/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@getProcurableAgainstTotalBudget');
Route::get('/json/dashBoard/getBudgetAndCeilingBySector/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@getBudgetAndCeilingBySector');
Route::get('/json/dashBoard/get-total-ceiling-problem/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@getTotalCeilingProblem');
Route::get('/json/dashBoard/get-ceiling-problem/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@getCeilingProblem');
Route::get('/json/dashBoard/get-allocation-problem/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@getAllocationProblem');
Route::get('/json/dashBoard/get-incomp-ceiling-budget/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}/{isTotal}','Dashboard\DashboardController@getIncompleteCeilingAndBudget');
Route::get('/json/dashBoard/download-incomp-ceiling-budget/{budgetType}/{financialYearId}/{adminHierarchyId}','Dashboard\DashboardController@downloadIncomplete');

Route::get('/json/dashBoard/getTotalPECeilingAndTotalPEBudget/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@getTotalPECeilingAndTotalPEBudget');
Route::get('/json/dashBoard/getPEBudgetByPECeiling/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}','Dashboard\DashboardController@getPEBudgetByPECeiling');