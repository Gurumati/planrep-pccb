<?php
/**
 * Created by PhpStorm.
 * User: Kachinga
 * Date: 3/4/2017
 * Time: 12:43 PM
 */

Route::group(['middleware' => 'authentication'], function () {
    Route::get('/report','Report\MainController@index');
    Route::get('/report_home','Report\MainController@report_home');
    Route::get('/display_report/{id}','Report\MainController@display_report');
    Route::get('/json/export_report/{id}/{report_name}/{folder}','Report\MainController@export_pdf');
    Route::post('/display_report','Report\MainController@post_parameters');
    Route::get('/printout/{report}','Report\MainController@report_no_parameters');
    Route::get('/pdf/{name}','Report\MainController@getDownload');
    Route::get('/logo','Report\MainController@getLogo');
    Route::get('/json/reportDashlets','Report\MainController@dashlets');
    Route::get('/json/reports/bar-chart/{report_name}/{id}','Report\MainController@barChart');
    Route::get('/json/reports/bar-chart','Report\MainController@reportList');

    //projection
    Route::get('/report/projection/{fund_source_id}','Report\ProjectionController@projection');

    Route::get('/report/procurements/paginated', 'Report\ProcurementController@paginated');
    Route::get('/report/procurements/{id}/items', 'Report\ProjectionController@items');

    //The route below queries for all councils
    Route::post('/json/peBudgetSubmissionFormReport','Report\PeSubmissionFormsReportController@getReport');

    //cchp report
    Route::get('/json/casPlanTableItemValueReport', 'Setup\CasPlanTableItemValueController@getItemValues');

    //PERFORMANCE INDICATOR FINANCIAL YEAR VALUES
    Route::get('/report/performanceIndicatorFinancialYearValues/paginated', 'Report\PerformanceIndicatorFinancialYearValueController@paginated');
    Route::put('/report/performanceIndicatorFinancialYearValues/{id}', 'Report\PerformanceIndicatorFinancialYearValueController@update');

    //jasper report for comprehensive plans
    Route::post('/json/getJasperCasReport','Report\MainController@casReport');
    //comprehensive plan parameters
    Route::post('/json/LoadReportParameters', 'Report\MainController@casReportParams');

    Route::get('/report/budgetAggregations/summary', 'Report\BudgetController@summary');
    Route::get('/report/budgetAggregations/summaryV2', 'Report\BudgetController@summaryV2');
    Route::get('/report/facilityStatusReports/summary', 'Report\FacilityReportController@summary');
    Route::get('/report/facilityStatusReports/printPDF', 'Report\FacilityReportController@printPDF');
    Route::get('/report/procurement/printPDF', 'Report\ProcurementController@printPDF');

    //CEILINGS
    Route::get('/report/ceilings/fetchAll', 'Report\CeilingController@fetchAll');
    Route::get('/report/ceilings/hierarchy_ceilings', 'Report\CeilingController@hierarchy_ceilings');
    Route::get('/report/ceilings/hierarchyYearCeilings', 'Report\CeilingController@hierarchyYearCeilings');

    //PROJECTIONS
    Route::get('/report/projections/fetchAll', 'Report\ProjectionController@fetchAll');
    Route::get('/report/projections/hierarchy_projections', 'Report\ProjectionController@hierarchy_projections');
    Route::get('/report/projections/hierarchyYearProjections', 'Report\ProjectionController@hierarchyYearProjections');

    Route::get('/api/financialYears/getUsableFinancialYears', 'Setup\FinancialYearController@usableFinancialYears');

    //Generate a single file for a cas plan

//Get the urls of the cas plan table detail uploads
    Route::post('/json/downLoadCasPlanInSingleFile', 'Report\MainController@generateSingleDocument');
    Route::get('/json/periodsByFinancialYear/{id}', 'Setup\PeriodController@periodsByFinancialYear');
});

Route::group(['middleware' => 'ajaxAuthentication'], function () {
    //PE budget forms report
    Route::get('/json/pe_submission_report/all','Report\PeSubmissionFormsReportController@index');
    Route::get('/json/peFundSources', 'Report\PeSubmissionFormsReportController@getPeFundSources');

});
