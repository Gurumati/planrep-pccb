<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
require_once("auth.php");
require_once("setup.php");
require_once("dashboard.php");
require_once("planning.php");
require_once("budgeting.php");
require_once("report.php");
require_once("authentication.php");
require_once("misc.php");
require_once("execution.php");
require_once("assessment.php");
require_once("api.php");
require_once ("audits.php");
