<?php
Route::get('/json/allAuditTrails', 'Security\AuditController@index');
Route::get('allAuditTrails/{id}', 'Security\AuditController@show');
Route::get('allAuditTrails/user/{id}', 'Security\AuditController@auditsByUser');

//Route::group(['middleware' => 'Authentication'], function() {
//});
