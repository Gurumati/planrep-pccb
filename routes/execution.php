<?php
/**
 * Created by PhpStorm.
 * User: Kachinga
 * Date: 3/4/2017
 * Time: 12:43 PM MuseIntegrationController
 */

 //integration points
 Route::post('/museplanrepotr/api/v1/request', 'Execution\MuseIntegrationController@getDataFromMuse');
 Route::post('/npmisplanrepotr/api/v1/request', 'Execution\NpimsIntegrationController@getDataFromNpims');
 Route::post('/npmisplanrepotr/api/v2/request', 'Execution\NpimsIntegrationController@getProjectBudgetRequest');
 Route::post('/nestplanrepotr/api/v1/request', 'Execution\NestIntegrationController@getDataFromNest');
 Route::post('/navisionplanrepotr/api/v1/request', 'Execution\SocialSecurityIntegrationController@getDataFromNavision');
 Route::post('/ermsplanrepotr/api/v1/request', 'Execution\MuseIntegrationController@getDataFromErms');
 Route::post('/museplanrepotr/api/v1/request-test', 'Execution\MuseIntegrationController@sendRequestTomuse');
 //End

Route::group(['middleware' => 'authentication'], function () {
    Route::get('/execution', 'Execution\MainController@index');

    //DATA SOURCES
    Route::get('/json/data_sources/all', 'Execution\DataSourceController@index');

    //IMPORT METHODS
    Route::get('/json/import_methods/paginated', 'Execution\ImportMethodController@paginated');
    Route::get('/json/import_methods/all', 'Execution\ImportMethodController@index');
    Route::post('/json/import_method/create', 'Execution\ImportMethodController@store');
    Route::post('/json/import_method/update', 'Execution\ImportMethodController@update');
    Route::get('/json/import_method/delete/{id}', 'Execution\ImportMethodController@delete');
    Route::get('/json/import_methods/trashed', 'Execution\ImportMethodController@trashed');
    Route::get('/json/import_method/restore/{id}', 'Execution\ImportMethodController@restore');
    Route::get('/json/import_method/permanentDelete/{id}', 'Execution\ImportMethodController@permanentDelete');
    Route::get('/json/import_methods/emptyTrash', 'Execution\ImportMethodController@emptyTrash');

    // RECEIVED FUND
    Route::get('/json/receivedFunds/paginated', 'Execution\ReceivedFundController@paginated');
    Route::get('/json/receivedFunds/all', 'Execution\ReceivedFundController@index');
    Route::post('/json/receivedFund/create', 'Execution\ReceivedFundController@store');
    Route::post('/json/receivedFund/update', 'Execution\ReceivedFundController@update');
    Route::get('/json/receivedFund/delete/{id}', 'Execution\ReceivedFundController@delete');
    Route::get('/json/receivedFunds/trashed', 'Execution\ReceivedFundController@trashed');
    Route::get('/json/receivedFund/restore/{id}', 'Execution\ReceivedFundController@restore');
    Route::get('/json/receivedFund/permanentDelete/{id}', 'Execution\ReceivedFundController@permanentDelete');
    Route::get('/json/receivedFunds/emptyTrash', 'Execution\ReceivedFundController@emptyTrash');

    // notifications
    Route::get('/read_notification/{notification_id}', 'notifications\UserNotificationController@read_notifications');
    Route::get('/json/notifications/{user_id}', 'notifications\UserNotificationController@unread_notifications');
    Route::get('/json/all_notifications', 'notifications\UserNotificationController@all_notifications');
    Route::get('/notifications', 'notifications\UserNotificationController@index');
    Route::get('/listen_from_rabbit', 'Execution\RabbitMQController@receive');

    // coa-segments
    Route::get('/json/coa_segments', 'Execution\CoaSegmentController@getSegments');
    Route::get('/json/clear_coa_segments/{id}', 'Execution\CoaSegmentController@clearCoaSegments');
    Route::get('/json/generate_coa_segments', 'Execution\CoaSegmentController@runSegmentJob');
    Route::get('/json/gl_account_issues', 'Execution\BudgetExportAccountController@getGLAccountsWithIssues');
    Route::get('/json/clear_gl_account_issues/{id}', 'Execution\BudgetExportAccountController@clearGLAccountIssues');

    // DATA SOURCE
    Route::get('/json/dataSources/paginated', 'Execution\DataSourceController@paginated');
    Route::get('/json/dataSources/all', 'Execution\DataSourceController@index');
    Route::post('/json/dataSources/create', 'Execution\DataSourceController@store');
    Route::post('/json/dataSources/update', 'Execution\DataSourceController@update');
    Route::get('/json/dataSources/delete/{id}', 'Execution\DataSourceController@delete');
    Route::get('/json/dataSources/trashed', 'Execution\DataSourceController@trashed');
    Route::get('/json/dataSources/restore/{id}', 'Execution\DataSourceController@restore');
    Route::get('/json/dataSources/permanentDelete/{id}', 'Execution\DataSourceController@permanentDelete');
    Route::get('/json/dataSources/emptyTrash', 'Execution\DataSourceController@emptyTrash');
    //RECEIVED FUND ITEM
    Route::get('/json/receivedFundItems/paginated', 'Execution\ReceivedFundItemController@paginated');
    Route::get('/json/receivedFundItems/all', 'Execution\ReceivedFundItemController@index');
    Route::post('/json/receivedFundItem/create', 'Execution\ReceivedFundItemController@store');
    Route::post('/json/receivedFundItem/update', 'Execution\ReceivedFundItemController@update');
    Route::get('/json/receivedFundItem/delete/{id}', 'Execution\ReceivedFundItemController@delete');
    Route::get('/json/receivedFundItems/trashed', 'Execution\ReceivedFundItemController@trashed');
    Route::get('/json/receivedFundItem/restore/{id}', 'Execution\ReceivedFundItemController@restore');
    Route::get('/json/receivedFundItem/permanentDelete/{id}', 'Execution\ReceivedFundItemController@permanentDelete');
    Route::get('/json/receivedFundItems/emptyTrash', 'Execution\ReceivedFundItemController@emptyTrash');
    Route::get('/json/receivedFundItems/pdf', 'Execution\ReceivedFundItemController@pdf');

    //BUDGET EXPORT ACCOUNT
    Route::get('/json/budgetExportAccounts/all', 'Execution\BudgetExportAccountController@index');
    Route::get('/json/budgetExportAccounts/paginated', 'Execution\BudgetExportAccountController@paginated');
    Route::get('/json/budgetExportAccounts/loadInputs', 'Execution\BudgetExportAccountController@loadInputs');
    Route::get('/json/budgetExportAccounts/clearInputs', 'Execution\BudgetExportAccountController@clearInputs');
    Route::get('/json/budgetExportAccounts/reloadInputs', 'Execution\BudgetExportAccountController@reloadInputs');
    Route::get('/json/budgetExportAccounts/exportAccounts', 'Execution\BudgetExportAccountController@exportAccounts');
    Route::get('/json/budgetExportAccounts/returnsentAccounts/{adminHierarchyId}/{financialYearId}/{budgetType}/{financialSystem}', 'Execution\BudgetExportAccountController@returnsentAccounts');
    Route::get('/json/budgetExportAccounts/budgetClassSummary', 'Execution\BudgetExportAccountController@budgetClassSummary');
    Route::get('/json/budgetExportAccounts/referenceNumbers', 'Execution\BudgetExportAccountController@referenceNumbers');
    Route::get('/json/budgetExportAccounts/getTotals', 'Execution\BudgetExportAccountController@getTotals');
    Route::get('/json/budgetExportAccounts/glAccountExist', 'Execution\BudgetExportAccountController@glAccountExist');
    Route::get('/json/budgetExportAccounts/sendAccountSegment', 'Execution\BudgetExportAccountController@generateSegments');
    Route::post('/json/budgetExportAccounts/sendGLAccount', 'Execution\BudgetExportAccountController@sendGLAccount');
    Route::get('/files/budget-accounts/export', 'Execution\BudgetExportAccountController@downloadExcel');

    //TO FINANCIAL SYSTEMS
    Route::get('/json/toFinancialSystems/paginated', 'Execution\BudgetExportToFinancialSystemController@paginated');
    Route::get('/json/toFinancialSystems/{id}/send', 'Execution\BudgetExportToFinancialSystemController@send');

    //BUDGET TRANSACTION TYPES
    Route::get('/json/budgetTransactionTypes/paginated', 'Execution\BudgetTransactionTypeController@paginated');
    Route::get('/json/budgetTransactionTypes/all', 'Execution\BudgetTransactionTypeController@index');
    Route::post('/json/budgetTransactionType/create', 'Execution\BudgetTransactionTypeController@store');
    Route::post('/json/budgetTransactionType/update', 'Execution\BudgetTransactionTypeController@update');
    Route::get('/json/budgetTransactionType/delete/{id}', 'Execution\BudgetTransactionTypeController@delete');
    Route::get('/json/budgetTransactionTypes/trashed', 'Execution\BudgetTransactionTypeController@trashed');
    Route::get('/json/budgetTransactionType/restore/{id}', 'Execution\BudgetTransactionTypeController@restore');
    Route::get('/json/budgetTransactionType/permanentDelete/{id}', 'Execution\BudgetTransactionTypeController@permanentDelete');
    Route::get('/json/budgetTransactionTypes/emptyTrash', 'Execution\BudgetTransactionTypeController@emptyTrash');

    //BUDGET EXPORT TRANSACTIONS
    Route::get('/json/budgetExportTransactions/paginated', 'Execution\BudgetExportTransactionController@paginated');
    Route::get('/json/budgetExportTransactions/all', 'Execution\BudgetExportTransactionController@index');
    Route::post('/json/budgetExportTransaction/create', 'Execution\BudgetExportTransactionController@store');
    Route::post('/json/budgetExportTransaction/update', 'Execution\BudgetExportTransactionController@update');
    Route::get('/json/budgetExportTransaction/delete/{id}', 'Execution\BudgetExportTransactionController@delete');
    Route::get('/json/budgetExportTransactions/trashed', 'Execution\BudgetExportTransactionController@trashed');
    Route::get('/json/budgetExportTransaction/restore/{id}', 'Execution\BudgetExportTransactionController@restore');
    Route::get('/json/budgetExportTransaction/permanentDelete/{id}', 'Execution\BudgetExportTransactionController@permanentDelete');
    Route::get('/json/budgetExportTransactions/emptyTrash', 'Execution\BudgetExportTransactionController@emptyTrash');

    // BUDGET IMPORT ISSUE TYPES
    Route::get('/json/budgetImportIssueTypes/paginated', 'Execution\BudgetImportIssueTypeController@paginated');
    Route::get('/json/budgetImportIssueTypes/all', 'Execution\BudgetImportIssueTypeController@index');
    Route::post('/json/budgetImportIssueTypes/create', 'Execution\BudgetImportIssueTypeController@store');
    Route::post('/json/budgetImportIssueTypes/update', 'Execution\BudgetImportIssueTypeController@update');
    Route::get('/json/budgetImportIssueTypes/delete/{id}', 'Execution\BudgetImportIssueTypeController@delete');
    Route::get('/json/budgetImportIssueTypes/trashed', 'Execution\BudgetImportIssueTypeController@trashed');
    Route::get('/json/budgetImportIssueTypes/restore/{id}', 'Execution\BudgetImportIssueTypeController@restore');
    Route::get('/json/budgetImportIssueTypes/permanentDelete/{id}', 'Execution\BudgetImportIssueTypeController@permanentDelete');
    Route::get('/json/budgetImportIssueTypes/emptyTrash', 'Execution\BudgetImportIssueTypeController@emptyTrash');

    // BUDGET IMPORTS
    Route::get('/json/budgetImports/paginated', 'Execution\BudgetImportController@paginated');
    Route::get('/json/budgetImports/all', 'Execution\BudgetImportController@index');
    Route::post('/json/budgetImports/create', 'Execution\BudgetImportController@store');
    Route::post('/json/budgetImports/update', 'Execution\BudgetImportController@update');
    Route::get('/json/budgetImports/delete/{id}', 'Execution\BudgetImportController@delete');
    Route::get('/json/budgetImports/trashed', 'Execution\BudgetImportController@trashed');
    Route::get('/json/budgetImports/restore/{id}', 'Execution\BudgetImportController@restore');
    Route::get('/json/budgetImports/permanentDelete/{id}', 'Execution\BudgetImportController@permanentDelete');
    Route::get('/json/budgetImports/emptyTrash', 'Execution\BudgetImportController@emptyTrash');

    // Activity implementation
    Route::get('/json/activityImplementation/paginated', 'Execution\ActivityImplementationController@paginated');
    Route::get('/json/activityImplementation/paginatedPlan', 'Execution\ActivityImplementationController@paginatedPlan');
    Route::get('/json/activityImplementation/activityFacilityBudgetExpenditure', 'Execution\ActivityImplementationController@activityFacilityBudgetExpenditure');
    Route::get('/json/activityImplementation/all', 'Execution\ActivityImplementationController@index');
    Route::post('/json/activityImplementation', 'Execution\ActivityImplementationController@store');
    Route::get('/json/activityImplementation/delete/{id}', 'Execution\ActivityImplementationController@delete');
    Route::post('/json/activityImplementation-document', 'Execution\ActivityImplementationController@upload_document');
    Route::get('/json/activityImplementation/delete/{id}', 'Execution\ActivityImplementationController@delete');
    Route::get('/json/activityImplementation/implementation-byActivity', 'Execution\ActivityImplementationController@getActivityImplementationHistory');
    Route::post('/json/activityImplementation-document', 'Execution\ActivityImplementationController@upload_document');
    Route::put('/json/activityImplementation/document-remove', 'Execution\ActivityImplementationController@upload_document_remove');
    Route::put('/json/activityImplementation', 'Execution\ActivityImplementationController@update');
    Route::get('/json/activityImplementation/facilities', 'Execution\ActivityImplementationController@facilities');
    Route::get('/json/activityImplementation/facility-types', 'Execution\ActivityImplementationController@facilityTypes');
    Route::get('/json/activityImplementation/budgetClasses', 'Execution\ActivityImplementationController@subBudgetClasses');
    Route::get('/json/activityImplementation/fundSources', 'Execution\ActivityImplementationController@costCentreFundSources');
    Route::get('/json/activityImplementation/implementationHistory', 'Execution\ActivityImplementationController@implementationHistory');

    //BUDGET REALLOCATION
    Route::get('/json/budgetReAllocations/reallocationAccount/{selectedFromActivityFundSourceId}', 'Execution\BudgetReallocationController@getReallocationAccount');
    Route::get('/json/budgetReAllocations/toReallocationAccount', 'Execution\BudgetReallocationController@getToReallocationAccount');
    Route::get('/json/budgetReAllocations/reallocationActivities/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}/{facilityId}/{fundSourceId}', 'Execution\BudgetReallocationController@getReallocationActivities');
    Route::get('/json/budgetReAllocations/get-open', 'Execution\BudgetReallocationController@getOpen');
    Route::get('/json/budgetReAllocations/get-open/{id}', 'Execution\BudgetReallocationController@getOpenById');
    Route::get('/json/budgetReAllocations/get-items/{id}', 'Execution\BudgetReallocationController@getItems');
    Route::delete('/json/budgetReAllocations/delete-item/{itemId}', 'Execution\BudgetReallocationController@deleteItem');
    Route::put('/json/budgetReAllocations/reuse-item/{itemId}', 'Execution\BudgetReallocationController@reUseItem');
    Route::post('/json/budgetReAllocations/submit/{id}/{decisionLevelId}', 'Execution\BudgetReallocationController@submit');
    Route::post('/form/budgetReAllocations/open', 'Execution\BudgetReallocationController@open');
    Route::get('/json/budgetReAllocations/mtefSectionToReallocate/{sectionId}', 'Execution\BudgetReallocationController@getMtefSectionToReallocate');
    Route::get('/json/budgetReAllocations/accountReallocations/{accountId}', 'Execution\BudgetReallocationController@getAccountReallocations');
    Route::post('/json/budgetReAllocations/createReallocation/{reallocationId}', 'Execution\BudgetReallocationController@createReallocation');
    Route::get('/json/budgetReAllocations/reallocationToApprove/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectionId}', 'Execution\BudgetReallocationController@getReallocationToApprove');
    Route::get('/json/budgetReAllocations/getApprovedReallocation', 'Execution\BudgetReallocationController@getApprovedReallocation');
    Route::get('/json/budgetReAllocations/getFailedReallocation', 'Execution\BudgetReallocationController@getFailedReallocation');
    Route::post('/json/budgetReAllocations/approveReallocation', 'Execution\BudgetReallocationController@approveReallocation');
    Route::post('/json/budgetReAllocations/rejectReallocation', 'Execution\BudgetReallocationController@rejectReallocation');
    Route::post('/json/budgetReAllocations/exportReallocation', 'Execution\BudgetReallocationController@sendReallocation');
    Route::post('/json/budgetReAllocations/createAndGetReallocationToAccounts/{mtefSectionId}', 'Execution\BudgetReallocationController@createAndGetReallocationToAccounts');
    Route::get('/json/budget-mass-reallocation/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectorId}/{level}', 'Execution\BudgetMassReallocationController@getReallocations');
    Route::post('/json/budget-mass-reallocation/reallocate/{budgetType}/{financialYearId}/{adminHierarchyId}/{sectorId}/{exportTransactionsToEpicor}/{exportTransactionsToFfars}', 'Execution\BudgetMassReallocationController@reallocate');

    //Reallocation within vote pisc to Pisc
    Route::get('/json/openReallocation/{piscFrom}/{piscTo}/{finencialYear}', 'Execution\ReallocationWithinVoteController@getOpenedReallocation');
    Route::post('/json/openReallocation/{inputData}/{attachement}', 'Execution\ReallocationWithinVoteController@createPiscReallocation');
    Route::post('/json/openReallocation/{budget}', 'Execution\ReallocationWithinVoteController@storePiscReallocations');
    //Reallocation within vote pisc to Pisc
    //RABBIT MQ
    Route::get('/exchange/rabbitmq/send', 'Execution\RabbitMQController@send');
    Route::get('/exchange/rabbitmq/receive', 'Execution\RabbitMQController@receive');

    //revenue export account
    Route::get('/json/revenue-export-accounts/by-admin-hierarchy/{adminHierarchyId}/by-financial-year/{financialYearId}', 'Execution\RevenueExportAccountController@getRevenueAccounts');
    Route::put('/json/revenue-export-accounts/approve/by-selection/{bySelection}/by-admin-hierarchy/{adminHierarchyId}/by-financial-year/{financialYearId}/{budgetType}', 'Execution\RevenueExportAccountController@approveRevenueBudget');
    Route::get('/json/revenue-export-accounts/get-approved/by-admin-hierarchy', 'Execution\RevenueExportAccountController@getApprovedRevenueAccounts');
    Route::get('/json/revenueExportAccount', 'Execution\RevenueExportAccountController@getRevenueAccounts');
    Route::get('/json/revenue-export-accounts/exportRevenue', 'Execution\RevenueExportAccountController@exportRevenue');
    Route::get('/json/revenue-export-feedback', 'Execution\RevenueExportAccountController@revenueFeedback');
    Route::get('/json/revenue-export-account/sendTransaction', 'Execution\RevenueExportAccountController@sendTransaction');
    Route::get('/json/revenue-export-accounts/sendAccountSegment', 'Execution\RevenueExportAccountController@sendAccountSegment');
    Route::get('/json/revenue-export-accounts/glAccountExist', 'Execution\RevenueExportAccountController@sendGLAccount');
    Route::get('/files/revenue-accounts/export', 'Execution\RevenueExportAccountController@downloadExcel');

    //Budget export responses
    Route::get('/json/budgetExportResponses/paginated', 'Execution\BudgetExportResponseController@paginated');
    Route::get('/json/FFARSbudgetExportResponses/paginated', 'Execution\BudgetExportResponseController@ffarsFeedback');
    Route::get('/json/FFARSbudgetExportResponses/resend_budget', 'Execution\BudgetExportAccountController@resendFFARSBudget');
    Route::get('/json/budgetExportAccounts/getPE', 'Execution\BudgetExportAccountController@getPEBudget');
    Route::get('/json/budgetExportAccounts/sendPE', 'Execution\BudgetExportAccountController@sendPEBudget');
    Route::get('/json/budgetExportAccounts/sendPESegment', 'Execution\BudgetExportAccountController@sendPESegments');
    Route::get('/json/budgetExportAccounts/sendPEGL', 'Execution\BudgetExportAccountController@sendPEGL');
    Route::get('/json/budgetExportAccounts/getTransactions', 'Execution\BudgetExportAccountController@getTransactions');
    Route::get('/json/budgetExportAccounts/getExportAccounts', 'Execution\BudgetExportAccountController@getExportAccounts');
    Route::get('/json/budgetExportAccounts/sendTransactionSegment', 'Execution\BudgetExportAccountController@sendTransactionSegment');
    Route::get('/json/budgetExportAccounts/sendTransactionGL', 'Execution\BudgetExportAccountController@sendTransactionGL');
    Route::get('/json/budgetExportAccounts/sendTransaction', 'Execution\BudgetExportAccountController@sendTransaction');

    //Exported Transactions routes
    Route::get('/json/exportedTransactions', 'Execution\BudgetExportAccountController@exportedTransactions');

    //Budget export responses
    Route::get('/json/budgetExportResponses/paginated', 'Execution\BudgetExportResponseController@paginated');
    Route::get('/json/budgetExportResponses/clearExportIssue', 'Execution\BudgetExportResponseController@clearExportIssue');

    Route::get('/json/rabbitMqFeedback/getCoaSegmentFeedBack', 'Execution\ListenToRabbitMQController@getCoaSegmentFeedBack');
    Route::get('/json/rabbitMqFeedback/getGLAccountsQueueFeedBack', 'Execution\ListenToRabbitMQController@getGLAccountsQueueFeedBack');
    Route::get('/json/rabbitMqFeedback/getBudgetQueueFeedBack', 'Execution\ListenToRabbitMQController@getBudgetQueueFeedBack');

    Route::post('/json/facilities/exportAllSelectedFacilities', 'Execution\SetupSegmentSenderController@exportAllSelectedFacilities');
    Route::get('/json/facilities/exportAllFacilities', 'Execution\SetupSegmentSenderController@exportAllFacilities');

    Route::post('/json/budgetClasses/exportAllSelectedSubBudgetClasses', 'Execution\SetupSegmentSenderController@exportAllSelectedSubBudgetClasses');
    Route::get('/json/budgetClasses/exportAllSubBudgetClasses', 'Execution\SetupSegmentSenderController@exportAllSubBudgetClasses');

    Route::post('/json/fundTypes/exportAllSelectedFundTypes', 'Execution\SetupSegmentSenderController@exportAllSelectedFundTypes');
    Route::get('/json/fundTypes/exportAllFundTypes', 'Execution\SetupSegmentSenderController@exportAllFundTypes');

    Route::post('/json/fundSources/exportAllSelectedFundSources', 'Execution\SetupSegmentSenderController@exportAllSelectedFundSources');
    Route::get('/json/fundSources/exportAllFundSources', 'Execution\SetupSegmentSenderController@exportAllFundSources');

    Route::post('/json/projects/exportAllSelectedProjects', 'Execution\SetupSegmentSenderController@exportAllSelectedProjects');
    Route::get('/json/projects/exportAllProjects', 'Execution\SetupSegmentSenderController@exportAllProjects');

    Route::post('/json/gfsCodes/exportAllSelectedGfsCodes', 'Execution\SetupSegmentSenderController@exportAllSelectedGfsCodes');
    Route::get('/json/gfsCodes/exportAllGfsCodes', 'Execution\SetupSegmentSenderController@exportAllGfsCodes');

    Route::get('/json/activities/export', 'Execution\SetupSegmentSenderController@activitiesToFinancialSystems');
    Route::get('/json/facilities/activities', 'Execution\SetupSegmentSenderController@facilityLevelActivities');
    Route::post('/json/activities/exportAllSelectedActivities', 'Execution\SetupSegmentSenderController@exportAllSelectedActivities');
    Route::post('/json/activities/exportAllSelectedActivitiesToMUSE', 'Execution\SetupSegmentSenderController@exportAllSelectedActivities');
    Route::get('/json/activities/exportAllActivities', 'Execution\SetupSegmentSenderController@exportAllActivities');
    //Route::get('/json/activities/exportAllActivitiesToMUSE', 'Execution\SetupSegmentSenderController@exportAllActivities');

    //Objectives Setups
    Route::post('/json/opras/exportAllSelectedObjectives', 'Execution\SetupSegmentSenderController@exportAllSelectedObjectivesToOpras');
    Route::get('/json/opras/exportAllObjectives', 'Execution\SetupSegmentSenderController@exportAllObjectivesToOpras');
    Route::get('/json/objectives/exportAllObjectives', 'Execution\SetupSegmentSenderController@exportAllObjectives');
    Route::get('/json/institutionEmployees/exportAllInstitutionEmployees', 'Execution\SetupSegmentSenderController@exportAllInstitutionEmployees');

    Route::post('/json/opras/exportAllSelectedTargets', 'Execution\SetupSegmentSenderController@exportAllSelectedTargetsToOpras');
    Route::get('/json/opras/exportAllTargets', 'Execution\SetupSegmentSenderController@exportAllTargetsToOpras');

    Route::post('/json/opras/exportAllSelectedOprasActivities', 'Execution\SetupSegmentSenderController@exportAllSelectedActivitiesToOpras');
    Route::get('/json/opras/exportAllOprasActivities', 'Execution\SetupSegmentSenderController@exportAllActivitiesToOpras');
    //
    Route::put('/json/execution/pre-execution/{id}', 'Execution\ActivityPhaseController@update');
    Route::delete('/json/execution/pre-execution/{id}', 'Execution\ActivityPhaseController@delete');
    Route::get('/json/execution/pre-execution/paginated', 'Execution\ActivityPhaseController@paginated');
    Route::get('/json/execution/pre-execution/activities', 'Execution\ActivityPhaseController@activities');
    Route::post('/json/execution/pre-execution/addMileStone', 'Execution\ActivityPhaseController@addMileStone');
    Route::post('/json/execution/pre-execution/updateMainTaskData', 'Execution\ActivityPhaseController@updateMainTaskData');
    Route::post('/json/execution/pre-execution/updateMileStoneData', 'Execution\ActivityPhaseController@updateMileStoneData');
    Route::post('/json/execution/pre-execution/editProjectOutput', 'Execution\ActivityPhaseController@editProjectOutput');
    Route::post('/json/execution/pre-execution/addProjectOutput', 'Execution\ActivityPhaseController@addProjectOutput');
    Route::get('/json/execution/pre-execution/removeMileStone', 'Execution\ActivityPhaseController@removeMileStone');
    Route::get('/json/execution/pre-execution/removeOutput', 'Execution\ActivityPhaseController@removeOutput');
    Route::get('/json/execution/pre-execution/milestones', 'Execution\ActivityPhaseController@milestones');
    Route::get('/json/execution/pre-execution/outputs', 'Execution\ActivityPhaseController@outputs');
    Route::post('/json/execution/pre-execution/addPhase', 'Execution\ActivityPhaseController@addPhase');
    Route::get('/json/execution/pre-execution/removePhase', 'Execution\ActivityPhaseController@removePhase');
    Route::get('/json/execution/pre-execution/periods', 'Execution\ActivityPhaseController@periods');
    Route::get('/json/execution/pre-execution/costCentreFundSources', 'Execution\ActivityPhaseController@costCentreFundSources');
    Route::get('/json/execution/pre-execution/sectionBudgetClassesWithActivity', 'Execution\ActivityPhaseController@sectionBudgetClassesWithActivity');
    Route::get('/json/execution/pre-execution/activityMilestones', 'Execution\ActivityPhaseController@activityMilestones');
    Route::get('/json/execution/pre-execution/activityProjectOutputs', 'Execution\ActivityPhaseController@activityProjectOutputs');
    Route::get('/json/execution/pre-execution/retrieveMilestone', 'Execution\ActivityPhaseController@retrieveMilestone');
    Route::get('/json/execution/pre-execution/retrieveProjectOutput', 'Execution\ActivityPhaseController@retrieveProjectOutput');
    Route::get('/json/execution/pre-execution/restoreLostData', 'Execution\ActivityPhaseController@restoreLostData');
    Route::get('/api/execution/ffarsActivityAdminHierarchies', 'Execution\SetupSegmentSenderController@ffarsActivityAdminHierarchies');
    Route::get('/api/execution/createFfarsActivities/{adminHierarchyCode}/{financialYearId}/{budgetType}', 'Planning\MtefController@createFfarsActivities');
    Route::get('/api/execution/otrPisc', 'Setup\AdminHierarchyController@otrPisc');

    Route::get('/api/activityPeriods/paged', 'Execution\ActivityPeriodController@paginated');
    Route::post('/api/activityPeriods/comment', 'Execution\ActivityPeriodController@comment');

    /** approve/forward reallocation */
    Route::get('/api/getbudgetReallocations', 'Execution\BudgetReallocationApproveController@getReallocations');
    Route::get('/api/budgetReallocationCouncils', 'Execution\BudgetReallocationApproveController@getCouncils');
    Route::post('/api/approveBudgetReallocation', 'Execution\BudgetReallocationApproveController@approveReallocation');
    Route::post('/api/budgetReallocationApprove/reject', 'Execution\BudgetReallocationApproveController@rejectReallocation');
    Route::post('/api/returnBudgetReallocation/{id}/{decisionLevelId}', 'Execution\BudgetReallocationApproveController@returnReallocation');
    /** activity expenditures from other systems routes reconciliation */
    Route::get('/api/activityExpenditure', 'Execution\ActivityExpenditureController@index');
    Route::get('/api/getMatchedItems', 'Execution\ActivityExpenditureController@getMatchedItems');
    Route::get('/api/MatchItem', 'Execution\ActivityExpenditureController@matchItem');
    Route::post('/api/autoMatchItems', 'Execution\ActivityExpenditureController@autoMatchItems');

    Route::get('/api/revenueAllocationExpenditure', 'Execution\RevenueAllocationExpenditureController@index');
    Route::get('/api/reallocationFeedback', 'Execution\ReallocationFeedbackController@index');
    Route::get('/api/activityFacilityProjectOut', 'Execution\ActivityImplementationController@activityFacilityProjectOut');
//    reallocation within vote
    Route::get('/json/reallocation-within-vote/get-votes', 'Execution\ReallocationWithinVoteController@getVotes');
    Route::get('/json/reallocation-within-vote/get-reallocation-btn-pisc', 'Execution\ReallocationWithinVoteController@getAllRealocation');
    Route::get('/json/reallocation-within-vote/get-reallocation-items', 'Execution\ReallocationWithinVoteController@getReallocationItems');
    Route::get('/json/reallocation-within-vote/delete-item', 'Execution\ReallocationWithinVoteController@deleteItem');
    Route::get('/json/reallocation-within-vote/get-piscs', 'Execution\ReallocationWithinVoteController@getPiscs');
    Route::get('/json/reallocation-within-vote/get-fund-sources', 'Execution\ReallocationWithinVoteController@getFundSources');
    Route::get('/json/reallocation-within-vote/get-sub-budget-classes', 'Execution\ReallocationWithinVoteController@getSubBudgetClasses');
    Route::get('/json/reallocation-within-vote/get-activities', 'Execution\ReallocationWithinVoteController@getActivities');
    Route::post('/json/approveReallocation/{comments}/{data}/{status}/{reallocationId}', 'Execution\ReallocationWithinVoteController@approve');

    //reallocation within vote
    Route::get('/json/CeilingExportAmount/fetch-financial-years', 'Execution\CeilingExportAmountController@getFinancialYears');
    Route::get('/json/CeilingExportAmount/get-ceilings-amount', 'Execution\CeilingExportAmountController@getCeilings');
    Route::post('/json/CeilingExportAmount/export-ceiling-to-financial-system', 'Execution\CeilingExportAmountController@exportToFinancialSystem');

});
