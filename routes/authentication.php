<?php
/**
 * Created by PhpStorm.
 * User: mrisho
 * Date: 07/May/2019
 * Time: 17:38
 */
Route::get('/users','Authentication\UserController@index');
Route::get('/register','Authentication\RegistrationController@register');
Route::post('/register','Authentication\RegistrationController@postRegister');
Route::get('/attachroles/{email}','Authentication\RegistrationController@attachRoles');
Route::get('/login','Authentication\LoginController@login');
Route::post('/login','Authentication\LoginController@postLogin');
Route::get('/logout','Authentication\LoginController@logout');
Route::get('/activate/{email}{activationCode}','Authentication\ActivationController@activate');
Route::post('/attachroles','Authentication\RegistrationController@postAttachRoles');

Route::get('/user','Authentication\LoginController@user');
